
jQuery(document).ready(function(){    
    // Right cart
    jQuery(".sidebar .block-shipping .btn-cart-item-remove").live('click',function(event) {
        event.preventDefault();
        var deleteUrl = jQuery(this).data('deleteUrl');
        var deleteConfirmMsg = jQuery(this).data('deleteConfirmMsg');
        var items_data = jQuery(this).parent().data();
        if (confirm(deleteConfirmMsg)) {
            jQuery.ajax({
                url : deleteUrl,
                type: 'POST',
                data: {},
                dataType: 'json',
                beforeSend: function()
                {
                },
                success: function(result)
                {
                    if(result.status)
                    {
                        updateTopItemsCount(result.items_count_text);
                        slideUpRemove('.box_item_' + items_data.id);
                        updateSidebarShipping(result.html_block_shipping);
                        // refreshCartRight();
                    }
                    else
                    {
                        alert(result.message);
                    }
                }
            });
        }
    });
});


function refreshCartRight()
{
    jQuery.ajax({
        url: ajax_getblockshipping_url,
        type: 'GET',
        data: {},
        dataType: 'json',
        beforeSend: function()
        {
        },
        success: function(result)
        {
            if(result.status)
            {
                updateSidebarShipping(result.html_block_shipping);
            }
            else
            {
                alert(result.message);
            }
        }
    });
}

function updateSidebarShipping(html)
{
    jQuery(".sidebar .block-shipping").html(html);
}

function updateTopItemsCount(str)
{
    jQuery(".top-link-cart span").text( str );
}

function slideUpRemove(el, delay_value)
{
    jQuery( el ).delay(delay_value).animate({ height: 0, opacity: 0 }, 'slow', function(){
        jQuery(this).remove();
    });
}