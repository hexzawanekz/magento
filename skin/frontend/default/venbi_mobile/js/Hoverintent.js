(function(jQuery){jQuery.fn.hoverIntent=function(f,g){var cfg={sensitivity:9,interval:100,timeout:0};cfg=jQuery.extend(cfg,g?{over:f,out:g}:f);var cX,cY,pX,pY;var track=function(ev){cX=ev.pageX;cY=ev.pageY;};var compare=function(ev,ob){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t);if((Math.abs(pX-cX)+Math.abs(pY-cY))<cfg.sensitivity){jQuery(ob).unbind("mousemove",track);ob.hoverIntent_s=1;return cfg.over.apply(ob,[ev]);}else{pX=cX;pY=cY;ob.hoverIntent_t=setTimeout(function(){compare(ev,ob);},cfg.interval);}};var delay=function(ev,ob){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t);ob.hoverIntent_s=0;return cfg.out.apply(ob,[ev]);};var handleHover=function(e){var p=(e.type=="mouseover"?e.fromElement:e.toElement)||e.relatedTarget;while(p&&p!=this){try{p=p.parentNode;}catch(e){p=this;}}if(p==this){return false;}var ev=jQuery.extend({},e);var ob=this;if(ob.hoverIntent_t){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t);}if(e.type=="mouseover"){pX=ev.pageX;pY=ev.pageY;jQuery(ob).bind("mousemove",track);if(ob.hoverIntent_s!=1){ob.hoverIntent_t=setTimeout(function(){compare(ev,ob);},cfg.interval);}}else{jQuery(ob).unbind("mousemove",track);if(ob.hoverIntent_s==1){ob.hoverIntent_t=setTimeout(function(){delay(ev,ob);},cfg.timeout);}}};return this.mouseover(handleHover).mouseout(handleHover);};})(jQuery);

// button hover event
jQuery(document).ready(function () {
    if (navigator.platform.indexOf("iPad") != -1) {
        InitPadQuickShop();
    }
    else {
        jQuery(".showImg").hoverIntent(function () {
            jQuery(".quickViewBtn").fadeOut(50);
            jQuery(this).find(".quickViewBtn").addClass("quickViewBtnShowing");
            jQuery(this).find(".quickViewBtn").fadeIn(50, function () {
                jQuery(this).removeClass("quickViewBtnShowing");
            });
        }, function () {
            this.canShow = false;
            jQuery(this).find(".quickViewBtn").addClass("quickViewBtnShowing");
            jQuery(".quickViewBtn").fadeOut(50);
        });

        jQuery(".quickViewBtn").hover(function () {
            jQuery(this).addClass("quickViewBtnShowing");
            jQuery(this).fadeIn(50, function () {
                jQuery(this).removeClass("quickViewBtnShowing");
            });
        });

        jQuery(".quickViewBtn").bind("click", function () { quickView(this); });
    }
    initCommonDropdownList();
});

//for Dropdown List event
function initCommonDropdownList() {
    jQuery(".dropDownSelect").live('click', function () {
        jQuery(this).each(function () {
            var closeTimeOut = null;
            jQuery(this).find(".dropDownContent").find("ul").width(jQuery(this).find(".dropDownSelectContent").outerWidth() + jQuery(this).find(".dropDownArrowBtn").outerWidth() - 3);
            jQuery(this).find(".dropDownContent").find("ul li:last").css("border", "none");
            if (jQuery(this).find(".dropDownContent").css("display") == "none") {
                jQuery(this).find(".dropDownContent[id!='falvorDrownDiv']").show();
            }
            else {
                jQuery(this).find(".dropDownContent[id!='falvorDrownDiv']").css("display", "none");
                clearTimeout(closeTimeOut);
            }
            var dropDown = this;
            jQuery(this).hover(
                function () { clearTimeout(closeTimeOut); },
                function () { closeTimeOut = closeSelect(dropDown, closeTimeOut, 300); }
            );
            jQuery(this).find("ul li").click(function () {
                var ListTitle = jQuery(this).children("a").html();
                jQuery(this).parent().find("li").removeClass("selected");
                jQuery(this).addClass("selected");
                var dropDownSelectElement = jQuery(this).closest(".dropDownSelect");
                dropDownSelectElement.find(".dropDownSelectContent").html(ListTitle);
                if (jQuery(this).children("a").hasClass("topLine") || jQuery(this).children("a").hasClass("boldFont"))
                    dropDownSelectElement.find(".dropDownSelectContent").html("Select Food Brand");
                if (ListTitle == "Don't know yet") {
                    dropDownSelectElement.find(".GenderContent").val("Unknow");
                } else if (ListTitle == "Select One") {
                    dropDownSelectElement.find(".GenderContent").val("");
                } else {
                    dropDownSelectElement.find(".GenderContent").val(ListTitle);
                }
                closeTimeOut = closeSelect(dropDown, closeTimeOut, 30);
            });
        });
    });
    function closeSelect(obj, t,time) {
        if (t == null) {
            t = setTimeout(function () {
                jQuery(".dropDownContent", obj).css("display", "none");
                if (jQuery("#searchDropDown.dropDownShow").length > 0) {
                    jQuery("#searchDropDown").removeClass("dropDownShow");
                }
            }, time);
        } else {
            t = null;
        }
        return t;
    }
}


function showValueDropdownList(obj){
    var listValue = obj.find("li.selected").html();
    obj.find(".dropDownSelectContent").html(listValue);
}






