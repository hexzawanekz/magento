// <!-- Cart Modal -->
jQuery(document).ready(function(){

    // in Modal

    var intUpdateQty = window.clearInterval(intUpdateQty);
    
    jQuery("#shopping-cart-table input.qty").live('change',function(event) {
        event.preventDefault();
        intUpdateQty = window.clearInterval(intUpdateQty);

        var qty = jQuery(this).val();
        var items_data = jQuery(this).parent().parent().parent().data();
        var related_product = '';
        var super_attributeArr = "super_attribute["+items_data.super_attribute_value+"]=" + items_data.super_attribute_key;
        var super_attributeArra = {};
        super_attributeArra[items_data.super_attribute_key] = items_data.super_attribute_value;

        jQuery.ajax({
            url: ajax_updateItemOptions_url,
            type: 'POST',
            data:   { 
                        'id': items_data.id,
                        'product': items_data.product,
                        'related_product': related_product,
                        'super_attribute': super_attributeArra,
                        'qty': qty
                    },
            dataType: 'json',
            beforeSend: function()
            {
            },
            success: function(result)
            {
                if(result.status)
                {
                    updateTopItemsCount(result.items_count_text);
                    jQuery(".sidebar .block-cart").html(result.html_cart_right);
                    jQuery(".cart-modal").html(result.html_cart_modal).modal();
                }
                else
                {
                    refreshCartModal();
                    // jQuery(".sidebar .block-cart").html(result.html_cart_right);
                    // jQuery(".cart-modal").html(result.html_cart_modal).modal();
                }
                //showBlockMessage(".block_message", items_data.id, result.message);
                //hideBlockMessage(".block_message", items_data.id);
            }
        });
    });

    jQuery("#shopping-cart-table .btn-cart-item-remove").live('click',function(event) {
        event.preventDefault();
        var deleteUrl = jQuery(this).data('deleteUrl');
        var items_data = jQuery(this).parent().parent().data();
        
        jQuery.ajax({
            url : deleteUrl,
            type: 'POST',
            data: {},
            dataType: 'json',
            beforeSend: function()
            {
            },
            success: function(result)
            {
                if(result.status)
                {
                    updateTopItemsCount(result.items_count_text);
                    slideUpRemove('.tr_item_' + items_data.id);
                    jQuery(".sidebar .block-cart").html(result.html_cart_right);
                    jQuery(".cart-modal").html(result.html_cart_modal).modal();
                }
                else
                {
                    alert(result.message);
                }
                //showBlockMessage(".block_message", items_data.id, result.message);
                //hideBlockMessage(".block_message", items_data.id);
            }
        });
    });

    jQuery("#shopping-cart-table .btn-gty-up").live('click',function(event) {
        var inputName = jQuery(this).parent().data('inputName');
        var qty = jQuery("#shopping-cart-table input[name='"+inputName+"']").val();
        intUpdateQty = window.clearInterval(intUpdateQty);

        qty++;
        jQuery("#shopping-cart-table input[name='"+inputName+"']").val(qty);
        intUpdateQty = window.setInterval ( "jQuery('#shopping-cart-table input.qty').change()", 500 );
    });

    jQuery("#shopping-cart-table .btn-gty-down").live('click',function(event) {
        var inputName = jQuery(this).parent().data('inputName');
        var qty = jQuery("#shopping-cart-table input[name='"+inputName+"']").val();
        intUpdateQty = window.clearInterval(intUpdateQty);

        if( qty > 1)
        {
            qty--;
            jQuery("#shopping-cart-table input[name='"+inputName+"']").val(qty);
            intUpdateQty = window.setInterval ( "jQuery('#shopping-cart-table input.qty').change()", 500 );
        }

        return;
    });

    // in Modal

    // Add button
    jQuery(".btn-cart-modal-add").click(function(event) {
        event.preventDefault();
        var submitUrl = jQuery(this).data('submitUrl');

        jQuery(".cart-modal").html("Loading...").modal();

        jQuery.ajax({
            url: submitUrl,
            type: 'POST',
            data: jQuery("form[id=product_addtocart_form]").serialize(),
            dataType: 'json',
            beforeSend: function()
            {
            },
            success: function(result)
            {
                // After add, get popup
                if(result.status)
                {
                    updateTopItemsCount(result.items_count_text);
                    jQuery(".sidebar .block-cart").html(result.html_cart_right);
                    jQuery(".cart-modal").html(result.html_cart_modal).modal();
                }
                else
                {
                    alert(result.message);
                }
            }
        });
    });

    // Show modal cart
    jQuery(".btn-cart-modal").live('click',function(event) {
        event.preventDefault();
        jQuery(".cart-modal").html("Loading...").modal();
        refreshCartModal();
    });

    // Right cart
    jQuery(".sidebar .block-cart").html("Loading...");
    refreshCartRight(); // First load
    jQuery(".sidebar .block-cart .btn-cart-item-remove").live('click',function(event) {
        event.preventDefault();
        var deleteUrl = jQuery(this).data('deleteUrl');
        var items_data = jQuery(this).parent().data();
        
        jQuery.ajax({
            url : deleteUrl,
            type: 'POST',
            data: {},
            dataType: 'json',
            beforeSend: function()
            {
            },
            success: function(result)
            {
                if(result.status)
                {
                    updateTopItemsCount(result.items_count_text);
                    refreshCartRight();
                    slideUpRemove('.box_item_' + items_data.id);
                }
                else
                {
                    alert(result.message);
                }
            }
        });
    });
});

function refreshCartModal()
{
    // TODO
    // Refresh all price in page and modal
    jQuery.ajax({
        url: ajax_getcartmodal_url,
        type: 'GET',
        data: {},
        dataType: 'json',
        beforeSend: function()
        {
        },
        success: function(result)
        {
            if(result.status)
            {
                jQuery(".sidebar .block-cart").html(result.html_cart_right); // right cart
                jQuery(".cart-modal").html(result.html_cart_modal).modal();
            }
            else
            {
                alert(result.message);
            }
        }
    });
}

function refreshCartRight()
{
    jQuery.ajax({
        url: ajax_getcartright_url,
        type: 'GET',
        data: {},
        dataType: 'json',
        beforeSend: function()
        {
        },
        success: function(result)
        {
            if(result.status)
            {
                jQuery(".sidebar .block-cart").html(result.block_cart_right);
            }
            else
            {
                alert(result.message);
            }
        }
    });
}

function updateTopItemsCount(str)
{
    jQuery(".top-link-cart span").text( str );
}
function showBlockMessage(el, block_id, message)
{
    jQuery(el).append('<div class="message_id_'+block_id+'">' + message + '</div>');
    fadeInShow( el + ' .message_id_'+block_id );
}

function hideBlockMessage(el, block_id)
{
    slideUpRemove( el + ' .message_id_'+block_id, 5000);
}

function fadeInShow(el)
{
    jQuery(el).hide().fadeIn('fast');
}

function slideUpRemove(el, delay_value)
{
    jQuery( el ).delay(delay_value).animate({ height: 0, opacity: 0 }, 'slow', function(){
        jQuery(this).remove();
    });
}
// <!-- Cart Modal -->