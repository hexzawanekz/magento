jQuery(window).load(function() {
	jQuery("#slideshow").css("overflow", "hidden");
	
	jQuery("ul#slides").cycle({
		fx: 'fade',
		pause: 1,
		prev: '#prev',
		next: '#next'
	});

	jQuery("#slideshow").hover(function() {
		jQuery("ul#slide-nav").fadeIn(200);
		// jQuery("#slides li > div").stop().slideDown(200);
  	},
  		function() {
	    	jQuery("ul#slide-nav").fadeOut(200);
	    	// jQuery("#slides li > div").stop().slideUp(200);
  	});
	
});