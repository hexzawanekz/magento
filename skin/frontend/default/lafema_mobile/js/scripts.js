jQuery(function($) {
    var menu = $('#menu-btn'),
        body = $('body'),
        overlay = $('#overlay');
    // Menu:
    menu.on('click touchstart', function(e) {
        e.preventDefault();
        body.addClass('active');
    });
    overlay.on('click touchstart', function(e) {
        e.preventDefault();
        body.removeClass('active');
    });

    // Home navigation:
    $('#nav-index > li > a').on('click', function(e) {
        var subMenu = $(this).next();
        if (subMenu.length) {
            e.preventDefault();
            $('#nav-index').hide();
            $('#nav-index-hidden').html(subMenu.html()).show();
        }
    });
});
