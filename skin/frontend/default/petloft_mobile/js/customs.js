/*WT-14 change qty and update price on group product detail*/
jQuery(document).ready(function () {
    /*detail page*/
    var qty_group = jQuery('.numbers-row.qty_group #number_grouped_detail');
    jQuery(document.body).on("click", '.numbers-row.qty_group #inc-qty', function () {
        actionChangeQtyGroup(qty_group, true, true);
    });
    jQuery(document.body).on("click", '.numbers-row.qty_group #dec-qty', function () {
        actionChangeQtyGroup(qty_group, false, true);
    });

    jQuery(document.body).on("change", '.numbers-row.qty_group #number_grouped_detail', function () {
        if (!jQuery.isNumeric(qty_group.val()) || parseInt(qty_group.val()) < 1) {
            changeQtyGroupProducts(1);
            qty_group.val(1)
        }
        else {
            changeQtyGroupProducts(qty_group.val());
        }
    });
    /*end detail page*/

    /*list page*/
    jQuery(document.body).on("click", '#confirmOverlay #messageBox #product_addtocart_form .change-group-am-qty .am-qty-button-up', function () {
        var qty_group_popup_list_page = jQuery('#confirmOverlay #messageBox #product_addtocart_form tbody td.a-center input.qty');
        var qty_group_popup_list_page_Show = jQuery('#confirmOverlay #messageBox #product_addtocart_form .change-group-am-qty input.qty');
        actionChangeQtyGroup(qty_group_popup_list_page, true, true);
        actionChangeQtyGroup(qty_group_popup_list_page_Show, true, true);
    });
    jQuery(document.body).on("click", '#confirmOverlay #messageBox #product_addtocart_form .change-group-am-qty .am-qty-button-down', function () {
        var qty_group_popup_list_page = jQuery('#confirmOverlay #messageBox #product_addtocart_form tbody td.a-center input.qty');
        var qty_group_popup_list_page_Show = jQuery('#confirmOverlay #messageBox #product_addtocart_form .change-group-am-qty input.qty');
        actionChangeQtyGroup(qty_group_popup_list_page, false, true);
        actionChangeQtyGroup(qty_group_popup_list_page_Show, false, true);
    });
    jQuery(document.body).on("change", '#confirmOverlay #messageBox #product_addtocart_form .change-group-am-qty input.qty', function () {
        var qty_group_popup_list_page_Show = jQuery('#confirmOverlay #messageBox #product_addtocart_form .change-group-am-qty input.qty');
        if (!jQuery.isNumeric(qty_group_popup_list_page_Show.val()) || parseInt(qty_group_popup_list_page_Show.val()) < 1) {
            changeQtyGroupProducts(1);
            qty_group_popup_list_page_Show.val(1);
        }
        else {
            changeQtyGroupProducts(qty_group_popup_list_page_Show.val());
        }
    });
    /*end list page*/

    if(jQuery('.checkout-banner').length >0){
        jQuery('#admin_messages').insertAfter('.checkout-banner').css({
            'text-align': 'center',
            'text-transform': 'capitalize',
            'color':'red',
            'font-size': '14px',
            'font-weight': 'bold'
        });
    }
});

function actionChangeQtyGroup(qty_group, up, detail_page) {
    var val_qty_group = parseInt(qty_group.val());
    if (up == true) {
        if (jQuery.isNumeric(val_qty_group) && val_qty_group >= 1) {
            qty_group.val(val_qty_group + 1);
        } else {
            qty_group.val(1);
        }
    } else {
        if (jQuery.isNumeric(val_qty_group) && val_qty_group > 1) {
            qty_group.val(val_qty_group - 1);
        } else {
            qty_group.val(1);
        }
    }
    if (detail_page == true) {
        changeQtyGroupProducts(qty_group.val());
    }
}

function changeQtyGroupProducts(qty) {
    jQuery('table#super-product-table tbody .a-center input.qty').each(function (i, element) {
        jQuery(element).val(qty);
    });
}
/*End WT-14 change qty and update price on group product detail*/

/**WT-234**/
jQuery(document).ready(function() {

    jQuery("a[href^=http]").each(function(){

        // NEW - excluded domains list
        var excludes = [
            'www.petloft.com',
            'www.venbi.com',
            'www.sanoga.com',
            'www.lafema.com',
            'www.moxy.co.th',
            'petloft.com',
            'venbi.com',
            'sanoga.com',
            'lafema.com',
            'moxy.co.th',
            'dev.petloft.com',
            'dev.venbi.com',
            'dev.sanoga.com',
            'dev.lafema.com',
            'dev.moxyst.com',
            'stage.petloft.com',
            'stage.venbi.com',
            'stage.sanoga.com',
            'stage.lafema.com',
            'stage.moxyst.com'
        ];
        for(i=0; i<excludes.length; i++) {
            if(this.href.indexOf(excludes[i]) != -1) {
                return true; // continue each() with next link
            }
        }

        if(this.href.indexOf(location.hostname) == -1) {

            // attach a do-nothing event handler to ensure we can 'trigger' a click on this link
            jQuery(this).click(function() { return true; });

            jQuery(this).attr({
                target: "_blank",
                title: "Opens in a new window"
            });

            jQuery(this).click(); // trigger it
        }
    })
});
/**WT-234**/