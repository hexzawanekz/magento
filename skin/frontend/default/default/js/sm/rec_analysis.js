/**
 *
 * To make function works, should add class named "rec-analysis" into grid item's tag element
 * and add two custom attributes as "data-web-section, data-product-id"
 *
 */
jQuery(document).ready(function () {
    jQuery(document).on('click', '.rec-analysis a', function (e) {
        e.preventDefault();
        var rootElement     = jQuery(this).closest('.rec-analysis');
        var product_id      = rootElement.attr('data-product-id');
        var referrer_id      = jQuery('#rec-analysis-referrer').attr('data-referrer-id');
        var section         = rootElement.attr('data-web-section');
        var that = jQuery(this);
        if(product_id && section){
            if(product_id != false){
                // check cookie
                jQuery.ajax({
                    url         : window.location.protocol + "//" + window.location.host + "/" + "rec-analysis/index/saveClick",
                    type        : 'post',
                    data        : 'product='+product_id+'&section='+section+'&referrer='+referrer_id,
                    dataType    : 'json',
                    async       : true,
                    success     : function (re) {
                        document.location.href = that.attr('href');
                    },
                    done:function (re) {
                        document.location.href = that.attr('href');
                    }
                });
            }
        }


    });

});


function saveClick(e){
    var rootElement     = e.closest('.rec-analysis');
    var product_id      = rootElement.attr('data-product-id');
    var section         = rootElement.attr('data-web-section');
    var referrer_id      = jQuery('#rec-analysis-referrer').attr('data-referrer-id');
    if(product_id && section){
        if(product_id != false){
            // check cookie
            jQuery.ajax({
                url         : window.location.protocol + "//" + window.location.host + "/" + "rec-analysis/index/saveClick",
                type        : 'post',
                data        : 'product='+product_id+'&section='+section+'&referrer='+referrer_id,
                dataType    : 'json',
                async       : true,
                success     : function (re) {
                }
            });
        }
    }
}
