var AjaxCart_Updaters = Class.create();
AjaxCart_Updaters.prototype = {
    selectors: null,
    parentSelector: null,
    name: null,

    initialize: function(name, selectors, parentSelector) {
        this.name = name;
        this.selectors = selectors || null;
        this.parentSelector = parentSelector || null;
    },

    beforeUpdate: function(html) {
        return;
    },

    afterUpdate: function(html) {
        return;
    },

    update: function(html) {
        this.beforeUpdate(html);

        var me = this;
        var selectors = this.selectors;
        if (selectors === null) {
            selectors = this._getRootSelectors(html);
        }
        var storage = new Element('div');
        storage.innerHTML = html;
        if (storage.childElements().length != selectors.length && storage.childElements().length > 0) {
            return false;
        }
        if (!this._checkSelectorsOnUnique(selectors)) {
            return false;
        }

        selectors.each(function(cssSelector){
            var part = storage.select(cssSelector)[0];
            var target = null;
            me._getSelectorsToTarget(cssSelector).each(function(selector){
                if (target !== null) {
                    return;
                }
                if ($$(selector).length > 0) {
                    target = $$(selector)[0];
                }
            });
            if (!target) {
                return;
            }
            if (!part) {
                target.parentNode.removeChild(target);
                return;
            }
            target.parentNode.replaceChild(part, target);
        });
        delete storage;

        this._evalScripts(html);

        this.afterUpdate(html, selectors);
        return true;
    },

    _getRootSelectors: function(html) {
        var div = new Element('div');
        div.innerHTML = html;
        var selectors = [];
        div.childElements().each(function(el){
            selectors.push(this._getCssSelectorsByElement(el));
        }, this);
        delete div;
        return selectors;
    },

    _checkSelectorsOnUnique: function(selectors) {
        var isUnique = true;
        selectors.each(function(cssSelector){
            var possibleSelectors = this._getSelectorsToTarget(cssSelector);
            var selectorToCheck = null;
            possibleSelectors.each(function(selector){
                if (selectorToCheck !== null) {
                    return;
                }
                if ($$(selector) > 0) {
                    selectorToCheck = $$(selector)[0];
                }
            });
            if ($$(selectorToCheck).length > 1) {
                isUnique = false;
            }
        }, this);
        return isUnique;
    },

    _getSelectorsToTarget: function(selector) {
        var selectors = [];
        if (this.parentSelector !== null) {
            this.parentSelector.each(function(parent){
                var selectorToTarget = parent + ' ' + selector;
                selectors.push(selectorToTarget);
            });
        } else {
            selectors.push(selector);
        }
        return selectors;
    },

    _getCssSelectorsByElement: function(element) {
        element = $(element);
        var tagName = element.tagName.toLowerCase();
        var cssSelector = tagName;
        $H({'id': 'id', 'className': 'class'}).each(function(pair) {
            var property = pair.first(),
            attribute = pair.last(),
            value = (element[property] || '').toString();
            if (value) {
                if (attribute === 'id') {
                    cssSelector += '#' + value;
                } else {
                    value = value.split(' ').join('.');
                    cssSelector += '.' + value;
                }
            }
        });
        return cssSelector;
    },

    _evalScripts: function(html) {
        var scripts = html.extractScripts();
        scripts.each(function(script){
            try {
                //FIX CDATA comment
                script = script.replace('//<![CDATA[', '').replace('//]]>', '');
                script = script.replace('/*<![CDATA[*/', '').replace('/*]]>*/', '');
                eval(script.replace(/var /gi, ""));
            }
            catch(e){
                if(window.console) {
                    console.log(e.message);
                }
            }
        });
    }
};
