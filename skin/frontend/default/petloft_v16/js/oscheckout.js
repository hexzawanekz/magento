/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Onestepcheckout to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_Onestepcheckout
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
*/
var Checkout = Class.create();
Checkout.prototype = {
    initialize: function(form,saveUrl,successUrl,hide){        
        this.form = form;
        this.saveUrl = saveUrl;
        this.successUrl = successUrl;
		this.hideShippingAddress = hide;
		this.oneSave = this.saveResponse.bindAsEventListener(this);
		this.validator = new Validation(this.form);
		this.waiting = false;
		this.onComplete = this.removeSubmitWaiting.bindAsEventListener(this);
    },
    ajaxFailure: function(){
        location.href = this.failureUrl;
    }, 
	save: function() {
		//this.showWaiting();		
		if(!billing.validate()) {
			return ;
		}
		if(typeof(shipping)!='undefined') {
			if(!shipping.validate()) return;
		} 
		if(!shippingMethod.validate() || !payment.validate()) {
			return;
		}
		
        if (this.validator.validate() && !this.waiting) {
			this.waiting = true;
			var params = Form.serialize(this.form);
			this.submitWaiting();
			var request = new Ajax.Request(
					this.saveUrl,
					{
						method: 'post',
						onComplete: this.onComplete,
						onSuccess: this.oneSave,
						onFailure: this.ajaxFailure,
						parameters: params
					}
				);
		}
	},
	saveResponse: function(transport){
        if (transport && transport.responseText) {
            try{
                response = eval('(' + transport.responseText + ')');
            }
            catch (e) {
                response = {};
            }
            if (response.redirect) {                
                location.href = response.redirect;
                return;
            }
            if (response.success) {                
                window.location = this.successUrl;				
            }
            else{
                var msg = response.error_messages;
                
				if(!msg) {
					msg = response.message;
				}
                if (typeof(msg)=='object') {
                    msg = msg.join("\n");
                }
                if (msg) {
                    alert(msg);
                    this.removeWaiting();
                }
            }
        }
    },
	showWaiting: function() {
		this.waiting = true;
		this.disableForm();
		var review = $('checkout-review-load');
		if (review != null) {
			var size = review.getDimensions();
			review.setStyle({
                    'width': size.width + 'px',
                    'height': size.height/3 + 'px'
                }).update('').addClassName('loading');
		}				
	},
	submitWaiting: function() {
		$('review-please-wait').show();
		this.disableForm();
	},
	removeSubmitWaiting: function() {
		$('review-please-wait').hide();
		this.enableForm();
	},
	disableForm: function(){
		Form.getElements(this.form).collect(function(elm) {			
			elm.disabled = true;
		});
	},
	enableForm: function(){
		Form.getElements(this.form).collect(function(elm) {			
			elm.disabled = false;
		});
	},
	removeWaiting: function() {	
		this.waiting = false;	
		this.enableForm();
		shippingMethod.removeLoading();
		payment.removeLoading();
		review = $('checkout-review-load');
		if (review != null) {
			review.setStyle({
				'width': 'auto',
				'height': 'auto'
			}).removeClassName('loading');
		}
	},
	
}

// billing
var Billing = Class.create();
Billing.prototype = {
    initialize: function(billingOptions,observerFields){
        this.billingForm = billingOptions.billingForm;
        this.optionsName = billingOptions.optionsName;
        this.useForShipping = billingOptions.useForShipping;		
		this.observerFields = observerFields;
		this.estimateUrl = billingOptions.estimateUrl;	
		this.setAddressUrl = billingOptions.setAddressUrl;	
		this.checkedAddress = '';
		this.newBt = $('billing-bt');
		this.observeBillingChange();
		this.onComplete = this.removeWaiting.bindAsEventListener(this);
		// this.waiting = false;
		//this.checkAndUpdate();
    },
	observeBillingChange: function() {
		var billingUpdate = this;		
		this.observerFields.each(function(e) {
			$(e).observe('change',function(el) {
				billingUpdate.setCity();
				billingUpdate.checkAndUpdate();				
			});
		});
	},
	setCity: function() {
		$('billing:city').value = $('billing:region_id').title;
		// $('')
	},
	checkAndUpdate: function() {
		var check = true;
		if($(this.useForShipping) && !$(this.useForShipping).checked) {
			return;
		}
		this.observerFields.each(function(ob) {
			if(!$(ob).value) {
				check = false;
			}
		});
		
		if(check) {
			this.update();
		}
	},
	update: function() {
		if(typeof(checkout)!="undefined" && checkout.waiting) {
			return;
		}
		
		params = Form.serializeElements(this.getElements());
		if(typeof(checkout)!="undefined"){
			checkout.waiting = true;
		}
		this.showWaiting();
		var request = new Ajax.Request(
                this.estimateUrl,
                {
                    method: 'post',
                    onComplete: this.onComplete,
                    onSuccess: this.updateSection,
                    onFailure: checkout.ajaxFailure.bind(checkout),
                    parameters: params
                }
            );
	},
	updateSection: function(transport) {
	
		if (transport && transport.responseText){
            try{
                response = eval('(' + transport.responseText + ')');
            }
            catch (e) {
                response = {};
            }
        }
		if(response.update_section) {
			response.update_section.each(function(section) {
				$('checkout-'+section.name+'-load').update(section.html);
			})			
		}
        if (response.error){
            if ((typeof response.message) == 'string') {
                alert(response.message);
            } else {                
                alert(response.message.join("\n"));
            }

            return false;
        }
	},
	getElements: function() {
		var elements = new Array,
			element,
			arr = [ ],
			serializers = Form.Element.Serializers;
		this.observerFields.each(function(e) {
			elements.push($(e));
		})
		for (var i = 0; element = elements[i]; i++) {
		  arr.push(element);
		}
		return arr.inject([], function(elements, child) {
		  if (serializers[child.tagName.toLowerCase()])
			elements.push(Element.extend(child));
		  return elements;
		})
	},
	setAddress: function(address) {				
		if(address) {			
			this.newAddress(false);
			// if(this.checkedAddress == address.id) {
				// return;
			// }			
			this.checkedAddress = address.id;
			
		}		
		if($(this.useForShipping)&&!$(this.useForShipping).checked) {
			return;
		}
		var check = false;
		if($(this.billingForm).getStyle('display')=='none') {
			var option = $$('input[name = '+this.optionsName+']');
			if(option){
				option.each(function(e) {				
					if(e.checked) {					
						check = true;
					}
				})
			}
			$$('input[name = billing[save_in_address_book]]').each(function(e){
				e.value = 0
			});
		}
		else {
			check = true;
		}
		
		if(!check) {
			return;
		}
		
		checkout.waiting = true;
		var params =  Form.serialize(checkout.form);
		this.showWaiting();	
		var request = new Ajax.Request(
                this.setAddressUrl,
                {
                    method: 'post',
                    onComplete: this.onComplete,
                    onSuccess: this.updateSection,
                    onFailure: checkout.ajaxFailure.bind(checkout),
                    parameters: params
                }
            );		
	},
	showWaiting: function() {		
		checkout.showWaiting();
		shippingMethod.showLoading();
		payment.showLoading();
		if(this.newBt){
			this.newBt.disabled = true;		
		}
	},
	removeWaiting: function() {	
		checkout.waiting = false;
		checkout.removeWaiting();
		if(this.newBt){
			this.newBt.disabled = false;		
		}
	},
    newAddress: function(isNew){
        if (isNew) {
			this.checkedAddress = '';
            this.resetSelectedAddress();
            $(this.billingForm).show();
        } else {
            $(this.billingForm).hide();
        }		
    },
	toggleAddressForm: function() {
		this.resetSelectedAddress();
		$(this.billingForm).toggle();
	},
	setUseForShipping: function() {
		if($(this.useForShipping)&&$(this.useForShipping).checked) {
			this.setAddress();
		}
	},
    resetSelectedAddress: function(){
		var addresses = $$('input[name = '+this.optionsName+']');
		if(!addresses) {
			return;
		}	
		var billingUpdate = this;
        addresses.each(function(e) {	
			
			if(e.checked) {
				billingUpdate.checkAndUpdate();
				billingUpdate.checkedAddress = e.id;
			}
			else if(e.id == billingUpdate.checkedAddress) {
				e.checked = true;
				billingUpdate.setAddress();
				return;
			}
			e.checked = false;
		})
    },
	validate: function() {
		var check = false;
		
		if($(this.billingForm).getStyle('display')=='none'){
			var addresses = $$('input[name = '+this.optionsName+']');
			if(!addresses) {
				check = false;
			}
			else {
				addresses.each(function(e) {
					if(e.checked) {
						check = true;
					}
				});
			}
		}
		else {
			check = true;
		}
		if(!check) {
			alert(Translator.translate('Please choose a billing address'));
		}
		return check;
	}
}

// shipping
var Shipping = Class.create();
Shipping.prototype = {
    initialize: function(shippingOptions,observerFields){
        this.shippingForm = shippingOptions.shippingForm;
        this.optionsName = shippingOptions.optionsName;
		this.estimateUrl = shippingOptions.estimateUrl;	
		this.setAddressUrl = shippingOptions.setAddressUrl;	
		this.newBt = $('shipping-bt');
		this.observerFields = observerFields;		
		this.checkedAddress = '';
		// checkout.waiting = false;
		this.onComplete = this.removeWaiting.bindAsEventListener(this);
		this.observeBillingChange();
    },
    observeBillingChange: function() {
		var billingUpdate = this;		
		this.observerFields.each(function(e) {
			$(e).observe('change',function() {
				var check = true;
				billingUpdate.observerFields.each(function(ob) {
					if($(ob).value == '') {
						check = false;
					}
				});
				if(check) {
					billingUpdate.update();
				}
			});
		});
	},
	update: function() {
		if(checkout.waiting) {
			return;
		}
		checkout.waiting = true;		
		var params = Form.serializeElements(this.getElements());	
		this.showWaiting();
		var request = new Ajax.Request(
                this.estimateUrl,
                {
                    method: 'post',
                    onComplete: this.onComplete,
                    onSuccess: this.updateSection,
                    onFailure: checkout.ajaxFailure.bind(checkout),
                    parameters: params
                }
            );
	},
	updateSection: function(transport) {
	
		if (transport && transport.responseText){
            try{
                response = eval('(' + transport.responseText + ')');
            }
            catch (e) {
                response = {};
            }
        }
		if(response.update_section) {
			response.update_section.each(function(section) {
				$('checkout-'+section.name+'-load').update(section.html);
			})			
		}
        if (response.error){
            if ((typeof response.message) == 'string') {
                alert(response.message);
            } else {                
                alert(response.message.join("\n"));
            }

            return false;
        }
	},
	getElements: function() {
		var elements = new Array,
			element,
			arr = [ ],
			serializers = Form.Element.Serializers;
		this.observerFields.each(function(e) {
			elements.push($(e));
		})
		for (var i = 0; element = elements[i]; i++) {
		  arr.push(element);
		}
		return arr.inject([], function(elements, child) {
		  if (serializers[child.tagName.toLowerCase()])
			elements.push(Element.extend(child));
		  return elements;
		})
	},
	setAddress: function(address) {	
		if(checkout.waiting) return;
		checkout.waiting = true;
		if(address) {
			this.newAddress(false);
			if(this.checkedAddress == address.id) {
				return;
			}
			this.checkedAddress = address.id;
		}
		var check = false;
		if($(this.shippingForm).getStyle('display')=='none') {
			var option = $$('input[name = '+this.optionsName+']');
			if(option){
				option.each(function(e) {				
					if(e.checked) {					
						check = true;
					}
				})
			}
			$$('input[name = shipping[save_in_address_book]]').each(function(e){
				e.value = 0
			});
		}
		else {
			check = true;
		}
		if(!check) {
			return;
		}
		var params = Form.serialize(checkout.form);
		this.showWaiting();				
		var request = new Ajax.Request(
                this.setAddressUrl,
                {
                    method: 'post',
                    onComplete: this.onComplete,
                    onSuccess: this.updateSection,
                    onFailure: checkout.ajaxFailure.bind(checkout),
                    parameters: params
                }
            );		
	},
	showWaiting: function() {		
			checkout.showWaiting();
			shippingMethod.showLoading();
			payment.showLoading();
			if(this.newBt){
				this.newBt.disabled = true;
			}
		
	},
	removeWaiting: function() {
		checkout.waiting = false;
		checkout.removeWaiting();
		if(this.newBt){
			this.newBt.disabled = false;		
		}
	},
    newAddress: function(isNew){
        if (isNew) {
			this.checkedAddress = '';
            this.resetSelectedAddress();
            $(this.shippingForm).show();
        } else {
            $(this.shippingForm).hide();
        }		
    },
	toggleAddressForm: function() {
		this.resetSelectedAddress();
		$(this.shippingForm).toggle();
	},
    resetSelectedAddress: function(){
		var addresses = $$('input[name = '+this.optionsName+']');
		if(!addresses) {
			return;
		}	
		var shippingUpdate = this;
        addresses.each(function(e) {
			if(e.checked) {
				shippingUpdate.checkedAddress = e.id;
			}
			else if(e.id == shippingUpdate.checkedAddress) {
				e.checked = true;
				shippingUpdate.setAddress();
				return;
			}
			e.checked = false;
		})
    },
	validate: function() {
		check = false;
		if(!$(billing.useForShipping).checked) {
			if($(this.shippingForm).getStyle('display')=='none') {
				var addresses = $$('input[name = '+this.optionsName+']');
				if(!addresses) {
					check = false;
				}
				else {
					addresses.each(function(e) {
						if(e.checked) {
							check = true;
						}
					});
				}
			}
			else {
				check = true
			}
		}
		else {
			check = true;
		}
		if(!check) {
			alert(Translator.translate('Please choose a shipping address'));
		}
		return check;
	}
}

// shipping method
var ShippingMethod = Class.create();
ShippingMethod.prototype = {
    initialize: function(settings){	
		this.methodName = settings.methodName;
		this.saveUrl = settings.saveUrl;
		// this.saveUrl = settings.saveUrl;
		this.methods = document.getElementsByName(this.methodName);
		this.observeMethods();
		// checkout.waiting = false;
		this.methodId = '';
		this.onComplete = this.removeWaiting.bindAsEventListener(this);
    },
	observeMethods: function() {
		var shippingUpdate = this;
		for (var i=0; i<this.methods.length; i++) {
            this.methods[i].observe('click',function(e) {
				if(shippingUpdate.methodId == this.id) return;
				shippingUpdate.methodId = this.id;
				shippingUpdate.save();
			});
        }
	},
	save: function() {
		if(checkout.waiting) {
			return;
		}
		checkout.waiting = true;		
		// params = Form.serializeElements(this.getElements());		
		var params = Form.serialize(checkout.form);
		this.showWaiting();
		var request = new Ajax.Request(
                this.saveUrl,
                {
                    method: 'post',
                    onComplete: this.onComplete,
                    onSuccess: this.updateSection,
                    onFailure: checkout.ajaxFailure.bind(checkout),
                    parameters: params
                }
            );
	},
	updateSection: function(transport) {
	
		if (transport && transport.responseText){
            try{
                response = eval('(' + transport.responseText + ')');
            }
            catch (e) {
                response = {};
            }
        }
		if(response.update_section) {
			response.update_section.each(function(section) {
				$('checkout-'+section.name+'-load').update(section.html);
			})			
		}
        if (response.error){
            if ((typeof response.message) == 'string') {
                alert(response.message);
            } else {                
                alert(response.message.join("\n"));
            }

            return false;
        }
	},
	showWaiting: function() {		
			checkout.showWaiting();
			payment.showLoading();	
	},
	showLoading: function() {
		var shippingLoad = $('checkout-shipping-method-load');
		var size = shippingLoad.getDimensions();
		shippingLoad.setStyle({
                    'width': size.width + 'px',
                    'height': size.height + 'px'
                }).update('').addClassName('loading');
	},
	removeLoading: function() {
		
		shippingLoad = $('checkout-shipping-method-load');
		if (shippingLoad != null) {
			shippingLoad.setStyle({
				'width': 'auto',
				'height': 'auto'
			}).removeClassName('loading');
		}
	},
	removeWaiting: function() {
		checkout.waiting = false;
		
		checkout.removeWaiting();
	},
    validate: function() {		
        var methods = this.methods;
        if (methods.length==0) {
            alert(Translator.translate('Your order cannot be completed at this time as there is no shipping methods available for it. Please make necessary changes in your shipping address.').stripTags());
            return false;
        }
        for (var i=0; i<methods.length; i++) {
            if (methods[i].checked) {
                return true;
            }
        }
        alert(Translator.translate('Please specify shipping method.').stripTags());
        return false;
    }
}


// payment
var Payment = Class.create();
Payment.prototype = {
    beforeInitFunc:$H({}),
    afterInitFunc:$H({}),
    beforeValidateFunc:$H({}),
    afterValidateFunc:$H({}),
    initialize: function(settings){	
		this.methodName = settings.methodName;
		this.saveUrl = settings.saveUrl;
		// this.saveUrl = settings.saveUrl;
		this.methods = document.getElementsByName(this.methodName);		
		this.observeMethods();
		// checkout.waiting = false;
		this.onComplete = this.removeWaiting.bindAsEventListener(this);
		this.methodId = '';
    },
	observeMethods: function() {
		var paymentUpdate = this;		
		for (var i=0; i<this.methods.length; i++) {			
            this.methods[i].observe('click',function(e) {
				if(paymentUpdate.methodId == this.id) return;
				paymentUpdate.methodId = this.id;
				paymentUpdate.save();
			});
        }
	},
	save: function() {
		if(checkout.waiting) {
			return;
		}
		checkout.waiting = true;		
		// params = Form.serializeElements(this.getElements());		
		var params = Form.serialize(checkout.form);
		this.showWaiting();
		var request = new Ajax.Request(
                this.saveUrl,
                {
                    method: 'post',
                    onComplete: this.onComplete,
                    onSuccess: this.updateSection,
                    onFailure: checkout.ajaxFailure.bind(checkout),
                    parameters: params
                }
            );
	},
	updateSection: function(transport) {
	
		if (transport && transport.responseText){
            try{
                response = eval('(' + transport.responseText + ')');
            }
            catch (e) {
                response = {};
            }
        }
		if(response.update_section) {
			response.update_section.each(function(section) {
				$('checkout-'+section.name+'-load').update(section.html);
			})			
		}
        if (response.error){
            if ((typeof response.message) == 'string') {
                alert(response.message);
            } else {                
                alert(response.message.join("\n"));
            }

            return false;
        }
		if (response.redirect) {
            location.href = response.redirect;
            return true;
        }
	},
	showWaiting: function() {
		checkout.showWaiting();
	},
	removeWaiting: function() {
		checkout.removeWaiting();
		checkout.waiting = false;
	},
	showLoading: function() {
		var paymentLoad = $('checkout-payment-method-load');
		var size = paymentLoad.getDimensions();
		paymentLoad.setStyle({
                    'width': size.width + 'px',
                    'height': size.height + 'px'
                }).update('').addClassName('loading');
	},
	removeLoading: function() {
		paymentLoad = $('checkout-payment-method-load');
		if (paymentLoad != null) {
			paymentLoad.setStyle({
				'width': 'auto',
				'height': 'auto'
			}).removeClassName('loading');
		}
	},
    addBeforeInitFunction : function(code, func) {
        this.beforeInitFunc.set(code, func);
    },

    beforeInit : function() {
        (this.beforeInitFunc).each(function(init){
           (init.value)();;
        });
    },

    init : function () {
        this.beforeInit();
        var elements = this.methods;
        var method = null;
        for (var i=0; i<elements.length; i++) {
            if (elements[i].name=='payment[method]') {
                if (elements[i].checked) {
                    method = elements[i].value;
                }
            } else {
                elements[i].disabled = true;
            }
            elements[i].setAttribute('autocomplete','off');
        }
        if (method) this.switchMethod(method);
        this.afterInit();
    },

    addAfterInitFunction : function(code, func) {
        this.afterInitFunc.set(code, func);
    },

    afterInit : function() {
        (this.afterInitFunc).each(function(init){
            (init.value)();
        });
    },

    switchMethod: function(method){
        if (this.currentMethod && $('payment_form_'+this.currentMethod)) {
            this.changeVisible(this.currentMethod, true);
            $('payment_form_'+this.currentMethod).fire('payment-method:switched-off', {method_code : this.currentMethod});
        }
        if ($('payment_form_'+method)){
            this.changeVisible(method, false);
            $('payment_form_'+method).fire('payment-method:switched', {method_code : method});
        } else {
            //Event fix for payment methods without form like "Check / Money order"
            document.body.fire('payment-method:switched', {method_code : method});
        }
        if (method) {
            this.lastUsedMethod = method;
        }
        this.currentMethod = method;
    },
    addBeforeValidateFunction : function(code, func) {
        this.beforeValidateFunc.set(code, func);
    },

    beforeValidate : function() {
        var validateResult = true;
        var hasValidation = false;
        (this.beforeValidateFunc).each(function(validate){
            hasValidation = true;
            if ((validate.value)() == false) {
                validateResult = false;
            }
        }.bind(this));
        if (!hasValidation) {
            validateResult = false;
        }
        return validateResult;
    },
	changeVisible: function(method, mode) {
        var block = 'payment_form_' + method;
        [block + '_before', block, block + '_after'].each(function(el) {
            element = $(el);
            if (element) {
                element.style.display = (mode) ? 'none' : '';
                element.select('input', 'select', 'textarea', 'button').each(function(field) {
                    field.disabled = mode;
                });
            }
        });
    },

    validate: function() {
        var result = this.beforeValidate();
        if (result) {
            return true;
        }
        var methods = document.getElementsByName('payment[method]');
        if (methods.length==0) {
            alert(Translator.translate('Your order cannot be completed at this time as there is no payment methods available for it.').stripTags());
            return false;
        }
        for (var i=0; i<methods.length; i++) {
            if (methods[i].checked) {
                return true;
            }
        }
        result = this.afterValidate();
        if (result) {
            return true;
        }
        alert(Translator.translate('Please specify payment method.').stripTags());
        return false;
    },

    addAfterValidateFunction : function(code, func) {
        this.afterValidateFunc.set(code, func);
    },

    afterValidate : function() {
        var validateResult = true;
        var hasValidation = false;
        (this.afterValidateFunc).each(function(validate){
            hasValidation = true;
            if ((validate.value)() == false) {
                validateResult = false;
            }
        }.bind(this));
        if (!hasValidation) {
            validateResult = false;
        }
        return validateResult;
    },

    initWhatIsCvvListeners: function(){
        $$('.cvv-what-is-this').each(function(element){
            Event.observe(element, 'click', toggleToolTip);
        });
    }
}
