function AmAjaxEnableElement(buttonClass){
    jQuery(buttonClass).each(function(i){
        var oldLink = jQuery(this).attr('onclick');
        jQuery(this).attr('onclick','');
        jQuery(this).attr('oldEvent',oldLink);

        jQuery(this).click(function(){
            var quantity = jQuery('#qty').val();
            var product_id = jQuery(this).data('product');
            var url = jQuery(this).data('url');

            jQuery.ajax({
                url: url,
                type: "POST",
                success: function (data) {
                    jQuery('#minicart-product-message').html(data);
                    jQuery('#mini_cart_message_content').fadeIn();
                    setTimeout(function(){jQuery('#mini_cart_message_content').fadeOut()}, 3500);
                    jQuery("#tglr").addClass('noti');
                },
                error: function (data) {
                    alert('Some Problem Happen.');
                },
                data: {
                    "product_id": product_id,
                    "quantity": quantity,
                },
                async:false
            });
        });
    });
}

jQuery(document).ready(function() {
    AmAjaxEnableElement('button.btn-add-cart');
});