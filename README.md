# Setup Local Development with Docker

## Pre-requisites

- You will need to have Docker installed
- You will need a DB copied from someone else's local environment
- You will also need to have an app/etc/local.xml file from someone else's environment


## Installing Docker

- Linux: Please follow your distro guidelines
- Mac: https://docs.docker.com/docker-for-mac/install/#install-and-run-docker-for-mac
- Windows: https://docs.docker.com/docker-for-windows/install/

## Setup configuration

This development setup assumes your local environment will be located at `https://orami.local` and `https://petloft.local`.
This environment sets up an nginx, mysql, redis and hhvm (as a replacement for php-fpm). hhvm might be changed in the future
should our development environment breaks.

These are the steps to run your local development environment running correctly.

### Import DB from someone else's environment

Our environment uses DB to 127.0.0.1, with username, password and DB name specified in `docker-compose.yml`. You will need
to import using the following command:

> mysql -umagento -h127.0.0.1 -p magento < _your_sql_dump_file.sql_

### Modify your app/etc/local.xml

Please copy your local.xml from someone else's environment as well. The are to note here is the `<crypt><key>` value
as it will determine whether you can login or not. 

Please make sure that when connecting between hosts in Docker, you use the following hostnames:

- MySQL server's hostname: **db**
- HHVM server's hostname: **hhvm**
- Redis server's hostname: **redis**

### Modify your app/etc/fpc.xml

Since our redis server's hostname is **redis**, you will need to update app/etc/fpc.xml to update the host of FPC's hostname to **redis** as well.
However, please make sure that you **don't commit** this file.

### Update your hosts file

Before you can access `https://orami.local` and `https://petloft.local`, please make sure that you add these domains
entry in your hosts file. 

Hosts file location:

- Unix/Linux/Mac: **/etc/hosts**
- On Windows: **c:\Windows\System32\Drivers\etc\hosts**

### Update your browser's certificate entry

Since our site is mostly running on HTTPS, we created a custom certificate so that `https://orami.local` and `https://petloft.local`
can be run on HTTPS without browser's warning.

There is a **rootCA.pem** in **docker_templates/ssl/** directory. You will need to import this pem file to your browser.

- Chrome: https://wiki.wmtransfer.com/projects/webmoney/wiki/Installing_root_certificate_in_Google_Chrome

## Run your local development environment

Whenever you want to start running your local environment, please make sure you run the following command first:

> docker-compose up

The above command will make sure that MySQL, redis, nginx and HHVM are running.

Then, you can access local environment in:

- `https://orami.local/`
- `https://petloft.local/`

When you are done, you can quit by pressing **^C** (Ctrl-C).
# My project's README
