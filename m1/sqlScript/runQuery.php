<?php

/**
 * Config to database server
 */
$host       = 'oramith.csgheamo30fd.ap-southeast-1.rds.amazonaws.com';
$user       = 'oramith';
$pass       = 'Vj)8NBZ8Z=uKJM';
$dbName     = 'orami';
$filePath   = '/var/www/whatsnew-thailand/media/xml-record/'; // path to output folder
$sqlPath    = '/var/www/whatsnew-thailand/sqlScript/'; // path to SQL query file

$queryStr           = null;
$xmlRootName        = null;
$xmlNodeName        = null;
$xmlAttributeName   = null;
$outputFileName     = null;

if(isset($argv[1]) && $argv[1] == 'help'){
    echo 'This script was used to query and export some kind of data from database server to XML Document.'.PHP_EOL;
    echo 'Just use command like:'.PHP_EOL;
    echo 'php runQuery.php <sql-file> <xml-root-name> <xml-node-name> <main-attribute-name> <output-file-name>'.PHP_EOL.PHP_EOL;
    echo 'If failure when connecting to database server, should use Vim to edit database configurations on top of this script.'.PHP_EOL;
    exit();
}elseif($argv[1] != 'help' && isset($argv[2]) && isset($argv[3]) && isset($argv[4]) && isset($argv[5])){
    if($str = file_get_contents($sqlPath . $argv[1])){
        $queryStr = $str;
        $xmlRootName = $argv[2];
        $xmlNodeName = $argv[3];
        $xmlAttributeName = $argv[4];
        $outputFileName = $argv[5];
        // just continue below
    }else{
        echo 'SQL file can not be found. Try again.'.PHP_EOL;
        exit();
    }
}else{
    echo 'Use command "php runQuery.php help" for more information.'.PHP_EOL;
    exit();
}

function utf8_for_xml($string)
{
    return preg_replace ('/[^\x{0009}\x{000a}\x{000d}\x{0020}-\x{D7FF}\x{E000}-\x{FFFD}]+/u', ' ', $string);
}

$xml = new DOMDocument('1.0', 'UTF-8');
$xml->formatOutput = true;
$h = $xml->appendChild($xml->createElement($xmlRootName));

// create connection
$con = mysqli_connect($host, $user, $pass, $dbName);

// check connection
if (mysqli_connect_errno())
{
    echo "Failed to connect to MySQL: " . mysqli_connect_error();
    exit();
}

mysqli_query($con, "SET NAMES 'utf8'");

// fetch query
$result = mysqli_query($con, $queryStr);

// loop through database results and fill array
while($row = mysqli_fetch_array($result))
{
    if(is_array($row)){

        $node1 = null;
        $tempNode = array();

        foreach($row as $key => $value){

            if(!is_numeric($key)){
                $elementName = str_replace(' ', '_', strtolower($key));

                if($elementName == $xmlAttributeName){
                    $node1 = $h->appendChild($xml->createElement($xmlNodeName));
                    $attribute1 = $xml->createAttribute($xmlAttributeName);
                    $attribute1->value = utf8_for_xml(trim($value));
                    $node1->appendChild($attribute1);
                }elseif(is_null($node1) && $value){
                    $tempNode[$elementName] = utf8_for_xml(trim($value));
                }elseif($value){
                    $node2 = $node1->appendChild($xml->createElement($elementName));
                    $node2->appendChild($xml->createTextNode(utf8_for_xml(trim($value))));
                }
            }
        }

        if(!is_null($node1) && count($tempNode) > 0){
            foreach($tempNode as $eleName => $eleValue){
                $node3 = $node1->appendChild($xml->createElement($eleName));
                $node3->appendChild($xml->createTextNode(trim($eleValue)));
            }
        }
    }
}

mysqli_close($con);
echo "Processing is completed.".PHP_EOL;

$directoryPath = $filePath.$outputFileName;
if(!is_dir($filePath)){
    mkdir($filePath, 0777);
}
if(file_exists($directoryPath)){
    chmod($directoryPath, 0777);
    unlink($directoryPath);
}
$xml->save($directoryPath);

exit();
?>
