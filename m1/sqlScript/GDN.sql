select cpe.entity_id AS "product_id",
cpev2.value AS "product_name",
CONCAT("https://www.orami.co.th/",cpev3.value) AS "product_url",
CONCAT("https://www.orami.co.th/media/catalog/product",REPLACE(CONCAT("/",cpev4.value),"//","/")) AS "image_url",
FORMAT(LEAST(IFNULL(IF(now()>=IFNULL(cped2.value, ADDDATE(now(), INTERVAL -99999 DAY)) && now()<= IFNULL(cped3.value, ADDDATE(now(), INTERVAL 999999 DAY)), cped4.value, cped5.value),cped5.value), cped5.value),0)  AS "price"

from catalog_product_entity cpe

left join catalog_product_entity_int cpei ON cpe.entity_id = cpei.entity_id AND cpei.attribute_id = 273 and cpei.store_id = 0 -- enabled status
left join catalog_product_entity_int cpei2 ON cpe.entity_id = cpei2.entity_id AND cpei2.attribute_id = 526 AND cpei2.store_id = 0 -- visibility
left join catalog_product_entity_varchar cpev2 ON cpe.entity_id = cpev2.entity_id AND cpev2.attribute_id = 96 AND cpev2.store_id = 0 -- product name
left join catalog_product_entity_varchar cpev3 ON cpe.entity_id = cpev3.entity_id AND cpev3.attribute_id = 570 AND cpev3.store_id = 0 -- product url
left join catalog_product_entity_varchar cpev4 ON cpe.entity_id = cpev4.entity_id AND cpev4.attribute_id = 106 AND cpev4.store_id = 0 -- image url
left join catalog_product_entity_datetime cped2 ON cpe.entity_id = cped2.entity_id AND cped2.attribute_id = 568 AND cped2.store_id = 0  -- special price from date -- needed for website price
left join catalog_product_entity_datetime cped3 ON cpe.entity_id = cped3.entity_id AND cped3.attribute_id = 569 AND cped3.store_id = 0 -- special price to date -- needed for website price
left join catalog_product_entity_decimal cped4 ON cpe.entity_id = cped4.entity_id AND cped4.attribute_id = 567 AND cped4.store_id = 0 -- special price -- needed for website price
left join catalog_product_entity_decimal cped5 ON cpe.entity_id = cped5.entity_id AND cped5.attribute_id = 99 AND cped5.store_id = 0 -- price -- needed for website price
left join cataloginventory_stock_item csi ON cpe.entity_id = csi.product_id -- inventory

where cpei.value = 1 AND (cpei2.value = 2 OR cpei2.value = 4) AND cpe.type_id = "simple" AND IF(IF(csi.manage_stock+csi.use_config_manage_stock =0,0,1) = 0,1, IF(csi.qty = 0,0,1)) = 1 -- only items that are enabled, visible, simple and in stock

order by cpe.sku