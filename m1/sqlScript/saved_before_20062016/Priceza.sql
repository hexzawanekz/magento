select cpe.entity_id AS "product_id", 
cpev2.value AS "product_name", 
FORMAT(LEAST(IFNULL(IF(now()>=IFNULL(cped2.value, ADDDATE(now(), INTERVAL -99999 DAY)) && now()<= IFNULL(cped3.value, ADDDATE(now(), INTERVAL 999999 DAY)), cped4.value, cped5.value),cped5.value), cped5.value),0)  AS "price",
cpev5.value AS "product_description",
categorySub2.categoryID AS "category_id_level2", 
categorySub2.categoryName AS "category_name_level2",
categorySub3.categoryID AS "category_id_level3", 
categorySub3.categoryName AS "category_name_level3",
CONCAT("http://www.orami.co.th/",cpev3.value) AS "product_url", 
CONCAT("http://www.orami.co.th/media/catalog/product",REPLACE(CONCAT("/",cpev4.value),"//","/")) AS "image_url",
eaov.value AS "brand",
cpev6.value AS "UPC Code"

from catalog_product_entity cpe 

left join catalog_product_entity_int cpei ON cpe.entity_id = cpei.entity_id AND cpei.attribute_id = 273 and cpei.store_id = 0 -- enabled status 
left join catalog_product_entity_int cpei2 ON cpe.entity_id = cpei2.entity_id AND cpei2.attribute_id = 526 AND cpei2.store_id = 0 -- visibility 
left join catalog_product_entity_varchar cpev2 ON cpe.entity_id = cpev2.entity_id AND cpev2.attribute_id = 96 AND cpev2.store_id = 0 -- product name
left join catalog_product_entity_varchar cpev3 ON cpe.entity_id = cpev3.entity_id AND cpev3.attribute_id = 570 AND cpev3.store_id = 0 -- product url
left join catalog_product_entity_varchar cpev4 ON cpe.entity_id = cpev4.entity_id AND cpev4.attribute_id = 106 AND cpev4.store_id = 0 -- image url
left join catalog_product_entity_varchar cpev5 ON cpe.entity_id = cpev5.entity_id AND cpev5.attribute_id = 1058 AND cpev5.store_id = 0 -- product title
left join catalog_product_entity_varchar cpev6 ON cpe.entity_id = cpev6.entity_id AND cpev6.attribute_id = 1059 AND cpev6.store_id = 0 -- supplier SKU (UPC Code)
left join catalog_product_entity_datetime cped2 ON cpe.entity_id = cped2.entity_id AND cped2.attribute_id = 568 AND cped2.store_id = 0  -- special price from date -- needed for website price
left join catalog_product_entity_datetime cped3 ON cpe.entity_id = cped3.entity_id AND cped3.attribute_id = 569 AND cped3.store_id = 0 -- special price to date -- needed for website price
left join catalog_product_entity_decimal cped4 ON cpe.entity_id = cped4.entity_id AND cped4.attribute_id = 567 AND cped4.store_id = 0 -- special price -- needed for website price
left join catalog_product_entity_decimal cped5 ON cpe.entity_id = cped5.entity_id AND cped5.attribute_id = 99 AND cped5.store_id = 0 -- price -- needed for website price
left join cataloginventory_stock_item csi ON cpe.entity_id = csi.product_id -- inventory
left join catalog_product_entity_int cpei3 ON cpe.entity_id = cpei3.entity_id AND cpei3.attribute_id = 1281  AND cpei3.store_id = 0 -- brand ID
left join eav_attribute_option_value eaov ON cpei3.value = eaov.option_id AND eaov.store_id = 0 -- brand name

-- category 2
left join 
(
select cpe.entity_id, ccp.category_id AS categoryID, ccev.value AS categoryName from catalog_product_entity cpe
left join catalog_category_product ccp ON  cpe.entity_id = ccp.product_id
left join catalog_category_entity cce ON ccp.category_id = cce.entity_id 
left join catalog_category_entity_varchar ccev ON ccp.category_id = ccev.entity_id AND ccev.store_id = 0 and ccev.attribute_id = 111
where cce.level = 2 AND ccev.value NOT IN ("Deal", "Welcome Splash", "Sanook Super Deal")
group by cpe.entity_id
) categorySub2 ON cpe.entity_id = categorySub2.entity_id

left join 
(
select cpe.entity_id, ccp.category_id AS categoryID, ccev.value AS categoryName, cce.parent_id AS parentID from catalog_product_entity cpe
left join catalog_category_product ccp ON  cpe.entity_id = ccp.product_id
left join catalog_category_entity cce ON ccp.category_id = cce.entity_id 
left join catalog_category_entity_varchar ccev ON ccp.category_id = ccev.entity_id AND ccev.store_id = 0 and ccev.attribute_id = 111
where cce.level = 3 AND ccev.value NOT IN ("All Deals", "Best Sellers", "Brands", "Deals", "New Arrivals", "New Products", "What's Hot")
group by cpe.entity_id
) categorySub3 ON cpe.entity_id = categorySub3.entity_id AND categorySub2.categoryID = categorySub3.parentID

where cpei.value = 1 AND (cpei2.value = 2 OR cpei2.value = 4) AND cpe.type_id = "simple" AND IF(IF(csi.manage_stock+csi.use_config_manage_stock =0,0,1) = 0,1, IF(csi.qty = 0,0,1)) = 1 -- only items that are enabled, visible, simple and in stock

order by cpe.sku