<!DOCTYPE html>
<html>
<head>
    <title>Pets Popup</title>
    <meta charset="utf-8" />

    <style>
        *{margin: 0px; padding: 0px;}
        body, html{ height: 550px; width: 360px;}
        .popup-container{
            height: 550px;
            width: 360px;
            background: url(images/popup-bg.jpg) transparent no-repeat 50% 50%;
            margin: 0px auto;
            position: relative;
            font-size: 14px;
            font-family: Tahoma, 'Trebuchet MS', 'Lucida Sans Unicode', 'Lucida Grande', 'Lucida Sans', Arial, sans-serif
        }

        .input-email{
            border: 1px solid #73787b;
            height: 27px;
            line-height: 27px;
            width: 233px;
            margin: 0px auto;
            padding: 0px 10px;
            font-size: 14px;
        }

        .form-container {
            width: 100%;
            text-align: center;
            position: absolute;
            top: 390px;
            bottom: 130px;
        }

        .btns-container {
            position: absolute;
            left: 0px;
            right: 0px;
            height: 31px;
            width: 253px;
            margin:0px auto;
            margin-top: 10px;
        }

        .dogs, .cats {
            height: 31px;
            color: #fff;
            background: #1fbeca;
            border: none;
            font-size: 18px;
            padding: 0px 10px;
            cursor: pointer;
            width: 100px;
        }

        .dogs {
            float: left;
        }

        .cats{
            float: right;
        }
    </style>


</head>

<body>
<!-- Google Tag Manager -->
<noscript>
    <iframe src="//www.googletagmanager.com/ns.html?id=GTM-T3ZMK9" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<script>(function (w, d, s, l, i) {
        w[l] = w[l] || [];
        w[l].push({'gtm.start': new Date().getTime(), event: 'gtm.js'});
        var f = d.getElementsByTagName(s)[0],
            j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
        j.async = true;
        j.src =
            '//www.googletagmanager.com/gtm.js?id=' + i + dl;
        f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-T3ZMK9');</script>
<!-- End Google Tag Manager -->
<?php
function getUtm()
{
    $utm = array();
    if (!empty($_COOKIE['__utmz'])) {
        $pattern = "/(utmcsr=([^\|]*)[\|]?)|(utmccn=([^\|]*)[\|]?)|(utmcmd=([^\|]*)[\|]?)|(utmctr=([^\|]*)[\|]?)|(utmcct=([^\|]*)[\|]?)/i";
        preg_match_all($pattern, $_COOKIE['__utmz'], $matches);
        if (!empty($matches[0])) {
            foreach ($matches[0] as $match) {
                $pair = null;
                $match = trim($match, "|");
                list($k, $v) = explode("=", $match);
                $utm[$k] = $v;
            }
        }
    }
    return $utm;
}

?>
<?php
$utm_array = getUtm();

$utm_source = "";
$utm_medium = "";
$utm_campaign = "";
$utm_content = "";
$utm_term = "";

if (isset($_GET["utm_source"])) {
    $utm_source = $_GET["utm_source"];
} else {
    if (isset($utm_array["utmcsr"])) {
        $utm_source = $utm_array["utmcsr"];
    } else {
        $utm_source = "";
    }
}

if (isset($_GET["utm_medium"])) {
    $utm_medium = $_GET["utm_medium"];
} else {
    if (isset($utm_array["utmcmd"])) {
        $utm_medium = $utm_array["utmcmd"];
    } else {
        $utm_medium = "";
    }
}

if (isset($_GET["utm_campaign"])) {
    $utm_campaign = $_GET["utm_campaign"];
} else {
    if (isset($utm_array["utmccn"])) {
        $utm_campaign = $utm_array["utmccn"];
    } else {
        $utm_campaign = "";
    }
}

if (isset($_GET["utm_content"])) {
    $utm_content = $_GET["utm_content"];
} else {
    if (isset($utm_array["utmcct"])) {
        $utm_content = $utm_array["utmcct"];
    } else {
        $utm_content = "";
    }
}

if (isset($_GET["utm_term"])) {
    $utm_term = $_GET["utm_term"];
} else {
    if (isset($utm_array["utmctr"])) {
        $utm_term = $utm_array["utmctr"];
    } else {
        $utm_term = "";
    }
}

$uri_parts = explode('?', $_SERVER['REQUEST_URI'], 2);

?>
<div class="popup-container">

    <div class="form-container">
        <form name="petloft-popup" method="POST" action="http://whatsnew.createsend.com/t/i/s/ckuv/">
            <input id="fieldmtux" name="cm-f-mijir" type="hidden" value="<?php echo $utm_source; ?>"/>
            <input id="fieldmtum" name="cm-f-mijiy" type="hidden" value="<?php echo $utm_medium; ?>"/>
            <input id="fieldmtuc" name="cm-f-mijij" type="hidden" value="<?php echo $utm_campaign; ?>"/>
            <input id="fieldmtuq" name="cm-f-mijit" type="hidden" value="<?php echo $utm_content; ?>"/>
            <input id="fieldmtua" name="cm-f-mijii" type="hidden" value="<?php echo $utm_term; ?>"/>
            <input id="fieldmtuf" name="cm-f-mijid" type="hidden" value="<?php echo $_SERVER ['HTTP_REFERER']; ?>"/>
            <input id="fieldcdktl" name="cm-f-cdktl" type="hidden" value="popup"/>
            <input id="fieldEmail" class="input-email email-field" type="email" name="cm-ckuv-ckuv" placeholder="แน่นอน! กรอกอีเมลล์รับรหัสส่วนลดทันท" required x-autocomplete size="100" />

            <div class="btns-container">
                <button type="submit" class="dogs email-button-dog" name="cm-f-kicl" id="dogs" title="Dogs" value="dog">ฉันรักหมา</button>
                <button type="submit" class="cats email-button-cat" name="cm-f-kicl" id="cats" title="Cats" value="cat">ฉันรักแมว</button>
            </div>
        </form>
    </div>
</div>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
<script src="js/jquery.email-autocomplete.js"></script>
<script src="js/jquery.cookie.js"></script>
<script>
    (function ($) {
        $(function () {
            $("#fieldEmail").emailautocomplete({
                domains: ["petloft.com"] //add your own domains
            });
        });
    }(jQuery));
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $.cookie("currentURL", document.referrer, { path: '/', expires: 1, domain: '.petloft.com' });
    });

    function validateEmail(sEmail) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(sEmail);
    }

    jQuery(document).ready(function(){
        //Emarsys
        var animalOpt = null;
        jQuery('button[name="cm-f-kicl"]').click(function(){
            animalOpt = jQuery(this).val();
            if(animalOpt == 'dog') { animalOpt = 1;} else { animalOpt = 2;}
            var email = jQuery('#fieldEmail').val();
            if(!validateEmail(email)){return false;}
            //Emarsys
            try {
                //https://suite9.emarsys.net/u/register_bg.php?owner_id=410525792&key_id=3&f=611&optin=[Opt-in]&inp_3=' + email + '&inp_9015[]="+animalOpt
                var emarsysUrl = 'https://suite9.emarsys.net/u/register_bg.php?owner_id=410525792&key_id=3&f=611&optin=y&inp_3=' + email + '&inp_9015[]='+animalOpt;
                var baseUrl = '<?php printf("%s://%s/emarsys/", isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http', $_SERVER['SERVER_NAME']);?>';
                jQuery.ajax({
                    type: 'GET',
                    url: baseUrl,
                    data : {'url' : emarsysUrl},
                    async : false,
                    success : function () {return;}
                });
            } catch ($e) {
                console.log($e);
            }
            //End Emarsys
        });
    });
</script>
</body>

</html> 