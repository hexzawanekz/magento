<?php
/**
 * NOTICE OF LICENSE
 *
 * You may not give, sell, distribute, sub-license, rent, lease or lend
 * any portion of the Software or Documentation to anyone.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade to newer
 * versions in the future.
 *
 * @category   ET
 * @package    ET_ExtraChargeForCartRules
 * @copyright  Copyright (c) 2012 ET Web Solutions (http://etwebsolutions.com)
 * @contacts   support@etwebsolutions.com
 * @license    http://shop.etwebsolutions.com/etws-license-commercial-v1/   ETWS Commercial License (ECL1)
 */

class ET_ExtraChargeForCartRules_Model_Observer
{
    /**
     * Change Discount Label if need (Discount or Extra Charge)
     *
     * @param Varien_Event_Observer $observer
     */
    public function changeDiscountLabel(Varien_Event_Observer $observer)
    {
        /** @var $quote Mage_Sales_Model_Quote */
        $quote = $observer->getEvent()->getQuote();

        foreach ($quote->getAllAddresses() as $address) {
            /* @var $address Mage_Sales_Model_Quote_Address */
            //echo $address->getDiscountDescription() . "<br>";
            $totalsArray = $address->getTotals();
            /** @var $total Mage_Sales_Model_Quote_Address_Total */
            foreach ($totalsArray as $total) {
                echo $total->getCode() . " - " . $total->toJson() . "<br>";
                if ($total->getCode() == 'discount') {
                    if ($total->getValue() > 0) {
                        $total->setTitle('Extra charge');
                        //echo $total->toJson();
                    }
                }
                //echo $total->toJson();
                //echo get_class($total) . "<br>";
            }
        }

    }

}