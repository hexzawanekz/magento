<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   Phoenix
 * @package    Phoenix_CashOnDelivery
 * @copyright  Copyright (c) 2008-2009 Andrej Sinicyn, Mik3e
 * @copyright  Copyright (c) 2010 Phoenix Medien GmbH & Co. KG (http://www.phoenix-medien.de)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Phoenix_CashOnDelivery_Block_Adminhtml_Sales_Order_Creditmemo_Totals_Cod extends Mage_Adminhtml_Block_Sales_Order_Creditmemo_Totals
{
    
    public function initTotals()
    {
		parent::_initTotals();
		if ($this->getSource()->getCodFee()) {
			$this->getParentBlock()->addTotalBefore(new Varien_Object(array(
				'code'      => 'cod_fee_incl_tax',
				'value'     => $this->getSource()->getCodFee()+$this->getSource()->getCodTaxAmount(),
				'base_value'=> $this->getSource()->getBaseCodFee()+$this->getSource()->getBaseCodTaxAmount(),
				'label'     => $this->helper('sales')->__('COD Fee (Incl. Tax)')
			)),'discount');
		}
        return $this;
    }
}