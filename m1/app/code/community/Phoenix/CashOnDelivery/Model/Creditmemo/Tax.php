<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Sales
 * @copyright   Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


class Phoenix_CashOnDelivery_Model_Creditmemo_Tax extends Mage_Sales_Model_Order_Creditmemo_Total_Tax
{
    public function collect(Mage_Sales_Model_Order_Creditmemo $creditmemo)
    {
		parent::collect($creditmemo);
        $codTaxAmount     = 0;
        $baseCodTaxAmount = 0;

        $order = $creditmemo->getOrder();
		$totalTax 		= $creditmemo->getTaxAmount();
		$baseTotalTax 	= $creditmemo->getBaseTaxAmount();
		
        if ($invoice = $creditmemo->getInvoice()) {
            //recalculate tax amounts in case if refund shipping value was changed
            if ($order->getBaseCodFee() && $creditmemo->getBaseCodFee()) {
                $taxFactor = $creditmemo->getBaseCodFee()/$order->getBaseCodFee();
                $codTaxAmount          	= $invoice->getCodTaxAmount()*$taxFactor;
                $baseCodTaxAmount       = $invoice->getBaseCodTaxAmount()*$taxFactor;
				//-------------			= -------------
                $codTaxAmount           = $creditmemo->roundPrice($codTaxAmount);
                $baseCodTaxAmount       = $creditmemo->roundPrice($baseCodTaxAmount, 'base');
				
                $totalTax                   += $codTaxAmount;
                $baseTotalTax               += $baseCodTaxAmount;
            }
        } else {
            $orderCodAmount = $order->getCodFee();
            $baseOrderCodAmount = $order->getBaseCodFee();

            $codTaxAmount = 0;
            $baseCodTaxAmount = 0;

            if ($baseOrderCodAmount > $creditmemo->getBaseCodFee()) {
                $part       = $creditmemo->getCodFee()/$orderCodAmount;
                $basePart   = $creditmemo->getBaseCodFee()/$baseOrderCodAmount;
                $codTaxAmount          = $creditmemo->roundPrice($order->getCodTaxAmount()*$part);
                $baseCodTaxAmount      = $creditmemo->roundPrice($order->getBaseCodTaxAmount()*$basePart,'base');
            } elseif ($baseOrderCodAmount == $creditmemo->getBaseCodFee()) {
                $codTaxAmount          = $order->getCodTaxAmount();
                $baseCodTaxAmount      = $order->getBaseCodTaxAmount();
            }
            $totalTax           += $codTaxAmount;
            $baseTotalTax       += $baseCodTaxAmount;
        }

        $allowedTax     = $order->getTaxAmount() - $order->getTaxRefunded();
        $allowedBaseTax = $order->getBaseTaxAmount() - $order->getBaseTaxRefunded();
        $totalTax           = min($allowedTax, $totalTax);
        $baseTotalTax       = min($allowedBaseTax, $baseTotalTax);

        $creditmemo->setTaxAmount($totalTax);
        $creditmemo->setBaseTaxAmount($baseTotalTax);


        $creditmemo->setCodTaxAmount($codTaxAmount);
        $creditmemo->setBaseCodTaxAmount($baseCodTaxAmount);

        $creditmemo->setGrandTotal($creditmemo->getGrandTotal() + $codTaxAmount);
        $creditmemo->setBaseGrandTotal($creditmemo->getBaseGrandTotal() + $baseCodTaxAmount);
        return $this;
    }
}
