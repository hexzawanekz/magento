<?php 
/**
 * Mas_Mascartdirect extension by Makarovsoft.com
 * 
 * @category   	Mas
 * @package		Mas_Mascartdirect
 * @copyright  	Copyright (c) 2014
 * @license		http://makarovsoft.com/license.txt
 * @author		makarovsoft.com
 */
/**
 * Mascartdirect default helper
 *
 * @category	Mas
 * @package		Mas_Mascartdirect
 * 
 */
class Mas_Mascartdirect_Helper_Data extends Mage_Core_Helper_Abstract{
	public function getCheckoutLink($id = null)
	{
		if (!$id && Mage::registry('current_product')) {
			$id = Mage::registry('current_product')->getId();
		}
		return Mage::getUrl('mascartdirect/index/add', array('product' => $id, 'bypass' => true));
	}
	
	public function getCartLink($id = null)
	{
		if (!$id && Mage::registry('current_product')) {
			$id = Mage::registry('current_product')->getId();
		}
		return Mage::getUrl('mascartdirect/index/add', array('product' => $id , 'addtocart' => true));
	}
	
}