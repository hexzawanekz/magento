<?php
/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 /***************************************
 *         MAGENTO EDITION USAGE NOTICE *
 *****************************************/
 /* This package designed for Magento COMMUNITY edition
 * BelVG does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * BelVG does not provide extension support in case of
 * incorrect edition usage.
 /***************************************
 *         DISCLAIMER   *
 *****************************************/
 /* Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future.
 *****************************************************
 * @category   Belvg
 * @package    Belvg_Ajaxcart
 * @copyright  Copyright (c) 2010 - 2011 BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */?>
<?php
class Belvg_Ajaxcart_FrontController extends Mage_Core_Controller_Front_Action
{

    protected function _getCart()
    {
        return Mage::getSingleton('checkout/cart');
    }


    protected function _getSession()
    {
        return Mage::getSingleton('checkout/session');
    }


    /**
		Ajax adding to cart
	 */
    public function addtocartAction(){            
           $_helper = Mage::helper('catalog/output');
           //$formData = $this->getRequest()->getPost('options');
           if (empty ($_POST)) {
               header("Location: ".$_SESSION['back_url']);
               $this->_redirect(str_replace(Mage::getBaseUrl(),'',$_SESSION['back_url']));

           }
           $formData = $this->getRequest()->getPost('super_attribute');		
	   $cOpts = $this->getRequest()->getPost('options');
           $product_id = $this->getRequest()->getPost('pro_id');
	   $url = $this->getRequest()->getPost('url');
           $qty = $this->getRequest()->getPost('qty');
           $type = $this->getRequest()->getPost('type');
           if (!isset($qty))
                   $qty = 1;
           if (isset($product_id)){
                   try{
                           $request = Mage::app()->getRequest();
                           $product = Mage::getModel('catalog/product')->load($product_id);
                           $session = Mage::getSingleton('core/session', array('name'=>'frontend'));
                           $cart = Mage::helper('checkout/cart')->getCart();
                           if (isset($formData)){
                                   $arr = array(
                                           'qty' => $qty,
                                           'super_attribute' => $formData					    
                                   );
                                   $cart->addProduct($product, $arr);}
                           elseif(isset($cOpts)){
				   $arr = array(
                                           'qty' => $qty,
                                           'options' => $cOpts					    
                                   );
                                   $cart->addProduct($product, $arr);
			    }
			   else
                                   $cart->addProduct($product, $qty);
                           $session->setLastAddedProductId($product->getId());
                           $session->setCartWasUpdated(true);
                           $cart->save();
                           $_cart = Mage::helper('checkout/cart')->getCart();
                           $_cartcount = 0;
                           $_cartsubtotal = 0;
                           $items = $_cart->getItems();
			   $_SESSION['back_url'] = $url;
                           $result = $this->getLayout()->createBlock('checkout/cart_sidebar')->setTemplate('ajaxcart/cart/sidebar.phtml')
                                   ->addItemRender('simple','checkout/cart_item_renderer','checkout/cart/sidebar/default.phtml')
                                  ->addItemRender('configurable','checkout/cart_item_renderer_configurable','checkout/cart/sidebar/default.phtml')
                                  ->addItemRender('grouped','checkout/cart_item_renderer_grouped','checkout/cart/sidebar/default.phtml')
                                  ->toHtml();                          
                           echo $result;						   
                           
                   } catch (Exception $e) {
                          echo 1;
                   }
           }else{
                          echo 0;

           }
    }
    protected function getDeleteUrl($id,$url = null)
    {
        return Mage::getUrl(
            'checkout/cart/delete',
            array(
                'id'=>$id,
                Mage_Core_Controller_Front_Action::PARAM_NAME_URL_ENCODED => Mage::helper('core/url')->getEncodedUrl($url)
            )
        );
    }

    public function getOptionsAction()
    {
        $pro_id = $this->getRequest()->getParam('proid');
        if ($pro_id){
              $_product = Mage::getModel('catalog/product')->load($pro_id);
              Mage::unregister('product');
              Mage::register('product',$_product);
              $result = $this->getLayout()->createBlock('catalog/product_view_type_configurable')->setTemplate('catalog/product/view/type/options/configurable.phtml')->toHtml();
              $result.= '  <button type="button" title="'.$this->__('Add to Cart').'" class="button btn-cart" onclick="addtocart('.$_product->getId().',$(\'qty_'.$_product->getId().'\').value);"><span><span>'.$this->__('Add to Cart').'</span></span></button><a class="btn-remove" onclick="closeOptions(this);" title="Close" href="javascript:void(0)">Close</a>';
              echo $result;
        }else{
            return false;
        }

    }

    public function getSimpleoptionsAction()
    {
        $pro_id = $this->getRequest()->getParam('proid');
        if ($pro_id){
              $_product = Mage::getModel('catalog/product')->load($pro_id);
	       foreach ($_product->getOptions() as $o) {
		    $optionType = $o->getType();
		    $result = $o->getTitle();
		    if ($optionType == 'drop_down') {
			$result .= '<select name="options['.$o->getOptionId().']" class=" required-entry product-custom-option">';
			$values = $o->getValues();
			foreach ($values as $k => $v) {
			    print_r($v->getOptionTypeId());
			    $result .= '<option value="'.$v->getOptionTypeId().'">'.$v->getTitle().'</option>';
			}
			$result .= '</select>';
		    }
		    else {
			//print_r($o);
		    }
		}	    
            //  Mage::unregister('current_product');
             // Mage::register('current_product',$_product);
             // $result = $this->getLayout()->createBlock('catalog/product_view_options')->setProduct($_product)->setTemplate('catalog/product/view/options.phtml')->toHtml();
              $result.= '  <button type="button" title="'.$this->__('Add to Cart').'" class="button btn-cart" onclick="addtocart('.$_product->getId().',$(\'qty_'.$_product->getId().'\').value);"><span><span>'.$this->__('Add to Cart').'</span></span></button><a class="btn-remove" onclick="closeOptions(this);" title="Close" href="javascript:void(0)">Close</a>';
              echo $result;
        }else{
            return false;
        }

    }


    public function updatePostAction(){
        if (empty ($_POST)) {
              // print_r(str_replace(Mage::getBaseUrl(),'',$_SESSION['back_url']));die;              
              header("Location: ".$_SESSION['back_url']);
              $this->_redirect(str_replace(Mage::getBaseUrl(),'',$_SESSION['back_url']));
        }
        $url = $this->getRequest()->getPost('url');
        $_SESSION['back_url'] = $url;
        try {
            $cartData = $this->getRequest()->getParam('cart');
            if (is_array($cartData)) {
                $filter = new Zend_Filter_LocalizedToNormalized(
                    array('locale' => Mage::app()->getLocale()->getLocaleCode())
                );
                foreach ($cartData as $index => $data) {
                    if (isset($data['qty'])) {
                        $cartData[$index]['qty'] = $filter->filter($data['qty']);
                    }
                }
                $cart = $this->_getCart();
                if (! $cart->getCustomerSession()->getCustomer()->getId() && $cart->getQuote()->getCustomerId()) {
                    $cart->getQuote()->setCustomerId(null);
                }

               if (Mage::getVersion() != '1.4.1.1' ) $cartData = $cart->suggestItemsQty($cartData);
                $cart->updateItems($cartData)
                    ->save();
            }
            $this->_getSession()->setCartWasUpdated(true);
        } catch (Mage_Core_Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        } catch (Exception $e) {
            $this->_getSession()->addException($e, $this->__('Cannot update shopping cart.'));
            Mage::logException($e);
        }
         $result = $this->getLayout()->createBlock('checkout/cart_sidebar')->setTemplate('ajaxcart/cart/sidebar.phtml')
                                   ->addItemRender('simple','checkout/cart_item_renderer','checkout/cart/sidebar/default.phtml')
                                  ->addItemRender('configurable','checkout/cart_item_renderer_configurable','checkout/cart/sidebar/default.phtml')
                                  ->addItemRender('grouped','checkout/cart_item_renderer_grouped','checkout/cart/sidebar/default.phtml')
                                  ->toHtml();

         echo $result;

    }
	
	
	 public function deleteAction()
    {
        $id = (int) $this->getRequest()->getParam('id');
        if ($id) {
            try {
                $this->_getCart()->removeItem($id)
                  ->save();
            } catch (Exception $e) {
                $this->_getSession()->addError($this->__('Cannot remove the item.'));
            }
        }
         header("Location: ".$_SESSION['back_url']);
              $this->_redirect(str_replace(Mage::getBaseUrl(),'',$_SESSION['back_url']));
    }
}
?>