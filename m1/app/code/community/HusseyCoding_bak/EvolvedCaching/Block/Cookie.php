<?php
class HusseyCoding_EvolvedCaching_Block_Cookie extends Mage_Page_Block_Html
{
    public function useCookie()
    {
        $params = Mage::app()->getRequest()->getParams();
        if (Mage::getStoreConfig('evolvedcaching/cookie/use') && Mage::app()->useCache('evolved') && $this->isSetup() && @class_exists('evolved') && !isset($params['shownames']) && !isset($params['disabled'])):
            return true;
        endif;
        
        return false;
    }
    
    public function getPageModifiers()
    {
        return Mage::helper('evolvedcaching')->getPageModifiers();
    }
    
    public function getLayeredModifiers()
    {
        return Mage::helper('evolvedcaching')->getLayeredModifiers();
    }
    
    public function isSetup()
    {
        $config = Mage::getStoreConfig(Mage::helper('evolvedcaching')->getSetupPath());
        $key = substr($config, -7);
        $config = substr($config, 0, -7);
        $details = '';
        $config = base64_decode($config);
        for ($i = 0; $i < strlen($config); $i++):
            $c = ord(substr($config, $i));
            $c -= ord(substr($key, (($i + 1) % strlen($key))));
            $details .= chr(abs($c) & 0xFF);
        endfor;

        $details = explode(',', $details);
        $host = Mage::app()->getRequest()->getHttpHost();
        foreach ($details as $detail):
            if ($detail == $host):
                return true;
            endif;
        endforeach;

        return false;
    }
}