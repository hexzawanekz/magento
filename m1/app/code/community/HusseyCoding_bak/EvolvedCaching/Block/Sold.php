<?php
class HusseyCoding_EvolvedCaching_Block_Sold extends Mage_Core_Block_Template
{
    public function getOrderIds()
    {
        switch ($this->getOrderType()):
            case 'multi':
                $ids = Mage::getSingleton('core/session')->getOrderIds(true);
                break;
            case 'onepage':
                $id = Mage::getSingleton('checkout/session')->getLastOrderId();
                $ids = array($id);
                break;
        endswitch;
        
        if (!empty($ids) && is_array($ids)):
            return implode(',', $ids);
        endif;
        
        return '';
    }
}