<?php
class HusseyCoding_EvolvedCaching_Model_Observer_Render
{
    private $_blocks;
    private $_excluded;
    private $_enabled;
    private $_usecookie;
    private $_processed = array();
    private $_crprocessed = array();
    private $_cprocessed = array();
    private $_pprocessed = array();
    private $_exclusionurls;
    private $_thisstore;
    private $_price;
    private $_creview;
    private $_cart;
    private $_preview;
    private $_tier;
    private $_welcome;
    private $_products = array();
    private $_dynamic;
    private $_listblock;
    
    public function frontendCoreBlockAbstractToHtmlAfter($observer)
    {
        $params = Mage::app()->getRequest()->getParams();
        
        if (!isset($params['evolvedupdate']) && !isset($params['evolvedforward'])):
            $excluded = $this->_getExcluded();
            $name = $observer->getBlock()->getNameInLayout();
            if ($this->_isEnabled()):
                $this->_collectParams();

                if ($this->_dynamic):
                    $module = strtolower(Mage::app()->getRequest()->getModuleName());
                    if ($this->_welcome && $name == 'header'):
                        $this->_insertWelcomeElement($observer);
                    endif;
                    if ((Mage::registry('current_category') || $module == 'catalogsearch') && !Mage::registry('current_product')):
                        $this->_insertCategoryElements($observer, $name);
                    elseif (Mage::registry('current_product')):
                        $this->_insertProductElements($observer, $name);
                    endif;
                endif;
            endif;

            $restrict = $name == 'header' || $name == 'footer' ? true : false;
            if (!in_array($name, $this->_processed) && $this->_isEnabled()):
                $this->_processed[] = $name;
                if ($this->_isEnabled()):
                    if (!isset($this->_usecookie)):
                        $this->_usecookie = Mage::getStoreConfig('evolvedcaching/cookie/use');
                    endif;

                    $transport = $observer->getTransport();

                    if ($this->_usecookie):
                        $this->_insertCookieData($transport);
                    endif;

                    if (!$restrict && !in_array($name, $excluded)):
                        $blocks = $this->_getBlocks();
                        $block = $observer->getBlock();
                        $name = $block->getNameInLayout();

                        if (in_array($name, $blocks)):
                            $id = preg_replace('/[^A-Za-z0-9]{1}/', '_', $name);
                            $useajax = Mage::getStoreConfig('evolvedcaching/general/useajax');
                            if ($holdinghtml = $this->_hasHoldingHtml($name)):
                                $style = '';
                                $class = ' evolved_holding';
                            else:
                                $style = ' style="display:none"';
                                $class = '';
                            endif;
                            $open = $useajax ? '<div' . $style . ' class="evolved_class' . $class . ' evolved_id-' . $id . '">' : '<!-- evolved_id-' . $id . ' -->';
                            $close = $useajax ? '</div>' : '<!-- close -->';
                            $html = !empty($holdinghtml) ? $holdinghtml : $transport->getHtml();
                            $transport->setHtml($open . $html . $close);
                        endif;
                    endif;
                endif;
            endif;
        else:
            if (isset($params['shownames'])):
                $excluded = $this->_getExcluded();
                $name = $observer->getBlock()->getNameInLayout();
                $restrict = $name == 'header' || $name == 'footer' ? true : false;
                if (!in_array($name, $this->_processed)):
                    $this->_processed[] = $name;
                    $this->_insertBlockLayoutNames($observer, $name, $excluded, $restrict);
                endif;
            elseif ($this->_isEnabled() && Mage::getStoreConfig('evolvedcaching/cookie/use')):
                $transport = $observer->getTransport();
                $this->_insertCookieData($transport);
            endif;
        endif;
    }
    
    private function _collectParams()
    {
        if (!isset($this->_dynamic)):
            $this->_price = (bool) Mage::getStoreConfig('evolvedcaching/exclude/price');
            $this->_creview = (bool) Mage::getStoreConfig('evolvedcaching/exclude/creview');
            $this->_cart = (bool) Mage::getStoreConfig('evolvedcaching/exclude/cart');
            $this->_preview = (bool) Mage::getStoreConfig('evolvedcaching/exclude/preview');
            $this->_tier = (bool) Mage::getStoreConfig('evolvedcaching/exclude/tier');
            $this->_welcome = (bool) Mage::getStoreConfig('evolvedcaching/exclude/welcome');

            $this->_dynamic = $this->_price || $this->_creview || $this->_cart || $this->_preview || $this->_tier || $this->_welcome;
        endif;
    }
    
    private function _insertWelcomeElement($observer)
    {
        $transport = $observer->getTransport();
        $html = $transport->getHtml();
        preg_match('/<p class="welcome-msg">.*<\/p>/Us', $html, $match);
        if (isset($match[0])):
            $match = $match[0];
            $useajax = Mage::getStoreConfig('evolvedcaching/general/useajax');
            if ($holdinghtml = $this->_hasHoldingHtml('welcome')):
                $style = '';
                $class = ' evolved_holding';
            else:
                $style = ' style="display:none"';
                $class = '';
            endif;
            $open = $useajax ? '<div' . $style . ' class="evolved_class' . $class . ' evolved_id-welcome">' : '<!-- evolved_id-welcome -->';
            $close = $useajax ? '</div>' : '<!-- close -->';
            $replace = $open . $match . $close;
            if (!empty($holdinghtml)) $replace = $open . $holdinghtml . $close;
            $html = str_replace($match, $replace, $html);
            $transport->setHtml($html);
        endif;
    }
    
    private function _insertCategoryElements($observer, $name)
    {
        if (strpos($name, 'ANONYMOUS') !== false && ($this->_price || $this->_creview)):
            $this->_insertCategoryAnonymousElements($observer);
        elseif (($name == 'product_list' || $name == 'search_result_list') && $this->_cart):
            $this->_collectListProducts($name);
            $this->_insertProductListElements($observer);
        endif;
    }
    
    private function _insertCategoryAnonymousElements($observer)
    {
        $product = $observer->getBlock()->getProduct();
        if ($product):
            foreach (debug_backtrace() as $class):
                if ($class['function'] == 'getPriceHtml' && $this->_price):
                    $id = 'eprice_' . $product->getId();
                    $class = ' evolved_price';
                    break;
                elseif ($class['function'] == 'getReviewsSummaryHtml' && $this->_creview):
                    $id = 'creview_' . $product->getId();
                    $class = ' evolved_creview';
                    break;
                endif;
            endforeach;
            $transport = $observer->getTransport();
            if (isset($id) && !in_array($id, $this->_crprocessed)):
                $this->_crprocessed[] = $id;
                $useajax = Mage::getStoreConfig('evolvedcaching/general/useajax');
                $block = str_replace(' evolved_', '', $class);
                if ($holdinghtml = $this->_hasHoldingHtml($block)):
                    $style = '';
                    $class .= ' evolved_holding';
                else:
                    $style = ' style="display:none"';
                endif;
                $open = $useajax ? '<div' . $style . ' class="evolved_class' . $class . ' evolved_id-' . $id . '">' : '<!-- evolved_id-' . $id . ' -->';
                $close = $useajax ? '</div>' : '<!-- close -->';
                $html = !empty($holdinghtml) ? $open . $holdinghtml . $close : $open . $transport->getHtml() . $close;
                $transport->setHtml($html);
            endif;
        endif;
    }
    
    private function _collectListProducts($name)
    {
        if (empty($this->_products)):
            if ($collection = Mage::app()->getLayout()->getBlock($name)->getLoadedProductCollection()):
                foreach ($collection as $product):
                    if (!in_array($product->getId(), $this->_products)):
                        $this->_products[] = (int) $product->getId();
                    endif;
                endforeach;
            endif;
        endif;
    }
    
    private function _insertProductListElements($observer)
    {
        $transport = $observer->getTransport();
        $html = $transport->getHtml();
        $mode = $observer->getBlock()->getMode();
        
        if ($mode == 'list'):
            preg_match_all('/(<p>\s*<button[^>]*class[^>]*btn-cart[^>]*>.*<\/button>\s*<\/p>|<p[^>]*class[^>]*availability[^>]*>\s*<span>.*<\/span>\s*<\/p>)/Us', $html, $matches);
            $matches = !empty($matches[0]) ? $matches[0] : array();
        else:
            preg_match_all('/<div[^>]*class[^>]*actions.*(<(button|p).+(p|button)>)/Us', $html, $matches);
            $matches = !empty($matches[1]) ? $matches[1] : array();
        endif;

        $replacements = array();
        foreach ($matches as $key => $match):
            if (isset($this->_products[$key])):
                $id = 'ecart_' . $this->_products[$key];
            elseif (isset($id)):
                unset($id);
            endif;
            $class = ' evolved_cart';

            if (isset($id) && !in_array($id, $this->_cprocessed)):
                $this->_cprocessed[] = $id;
                $html = preg_replace('/' . preg_quote($match, '/') . '/', '<!--' . $id . '-->', $html, 1);
                $useajax = Mage::getStoreConfig('evolvedcaching/general/useajax');
                if ($holdinghtml = $this->_hasHoldingHtml('cart')):
                    $style = '';
                    $class .= ' evolved_holding';
                else:
                    $style = ' style="display:none"';
                endif;
                $open = $useajax ? '<div' . $style . ' class="evolved_class' . $class . ' evolved_id-' . $id . '">' : '<!-- evolved_id-' . $id . ' -->';
                $close = $useajax ? '</div>' : '<!-- close -->';
                $match = !empty($holdinghtml) ? $holdinghtml : $match;
                $replacements[$id] = $open . $match . $close;
            endif;
        endforeach;

        foreach ($replacements as $key => $content):
            $html = str_replace('<!--' . $key . '-->', $content, $html);
        endforeach;

        $transport->setHtml($html);
    }
    
    private function _insertProductElements($observer, $name)
    {
        if ((strpos($name, 'ANONYMOUS') !== false || $name == 'product_review_list.count') && $this->_preview):
            $this->_insertProductReviewElements($observer, $name);
        elseif ($name == "product.info" && $this->_tier):
            $this->_insertProductTierPricingElements($observer);
        endif;
    }
    
    private function _insertProductReviewElements($observer, $name)
    {
        $product = Mage::registry('current_product');
        if ($product):
            foreach (debug_backtrace() as $class):
                if ($class['function'] == 'getReviewsSummaryHtml'):
                    $id = 'preview_' . $product->getId();
                    $class = ' evolved_preview';
                    break;
                endif;
            endforeach;
            $transport = $observer->getTransport();
            if (isset($id) && !in_array($id, $this->_pprocessed)):
                $this->_pprocessed[] = $id;
                $useajax = Mage::getStoreConfig('evolvedcaching/general/useajax');
                if ($holdinghtml = $this->_hasHoldingHtml('preview')):
                    $style = '';
                    $class .= ' evolved_holding';
                else:
                    $style = ' style="display:none"';
                endif;
                $open = $useajax ? '<div' . $style . ' class="evolved_class' . $class . ' evolved_id-' . $id . '">' : '<!-- evolved_id-' . $id . ' -->';
                $close = $useajax ? '</div>' : '<!-- close -->';
                $html = !empty($holdinghtml) ? $open . $holdinghtml . $close : $open . $transport->getHtml() . $close;
                $transport->setHtml($html);
            elseif ($name == 'product_review_list.count'):
                $transport->setHtml('');
            endif;
        endif;
    }
    
    private function _insertProductTierPricingElements($observer)
    {
        $transport = $observer->getTransport();
        $html = $transport->getHtml();
        preg_match('/<ul class="tier-prices.*<\/ul>/Us', $html, $match);
        if (isset($match[0]) && $match[0]):
            $useajax = Mage::getStoreConfig('evolvedcaching/general/useajax');
            if ($holdinghtml = $this->_hasHoldingHtml('tier')):
                $style = '';
                $class = ' evolved_holding';
            else:
                $style = ' style="display:none"';
                $class = '';
            endif;
            $open = $useajax ? '<div' . $style . ' class="evolved_class' . $class . ' evolved_id-tier">' : '<!-- evolved_id-tier -->';
            $close = $useajax ? '</div>' : '<!-- close -->';
            $replace = !empty($holdinghtml) ? $open . $holdinghtml . $close : $open . $match[0] . $close;
            $html = str_replace($match[0], $replace, $html);
            $transport->setHtml($html);
        endif;
    }
    
    private function _insertBlockLayoutNames($observer, $name, $excluded, $restrict)
    {
        if (!in_array($name, $excluded)):
            if (strpos($name, 'ANONYMOUS') === false && !$restrict):
                $transport = $observer->getTransport();
                $transport->setHtml(
                    '<div style="border:1px solid #f00; margin:10px">
                        <div style="float:left; color:#f00; font-weight:bold; font-size:12px; margin:5px; padding:3px 5px; border:1px solid #000; background-color:#fff">' . $name . '</div>
                        '. $transport->getHtml() . '
                        <div style="clear:both"></div>
                    </div>'
                );
            endif;
        endif;
    }
    
    private function _insertCookieData($transport)
    {
        if (!isset($this->_exclusionurls)):
            $this->_exclusionurls = Mage::helper('evolvedcaching')->getExclusionUrls();
        endif;
        if (!isset($this->_thisstore)):
            $this->_thisstore = Mage::app()->getStore()->getCode();
        endif;
        $html = $transport->getHtml();

        preg_match_all('/(<a.*)>.*<\/a>/Us', $html, $links);
        if (isset($links[0])):
            $this->_insertCookieAnchorData($links, $transport, $html);
        endif;
        
        $html = $transport->getHtml();
        
        preg_match_all('/(<select.*)>.*<\/select>/Us', $html, $selects);
        if (isset($selects[0])):
            $this->_insertCookieSelectsData($selects, $transport, $html);
        endif;
    }
    
    private function _insertCookieAnchorData($links, $transport, $html)
    {
        foreach ($links[0] as $index => $link):
            if (!preg_match('/evolved_area/s', $link)):
                preg_match('/(href="|\')(.*)("|\')/U', $link, $url);
                $host = Mage::app()->getRequest()->getHttpHost();
                if (isset($url[2])):
                    if ($urlstore = Mage::helper('evolvedcaching')->getUrlStore($url[2], $host)):
                        $request = $url[2];
                        $request = explode('?', $url[2]);
                        $request = reset($request);
                        $request = rtrim($request, '/');
                        $request = preg_replace('/http(s)?:\/\/[^\/]*/', '', $request);
                        if (!empty($this->_exclusionurls[$urlstore]) && is_array($this->_exclusionurls[$urlstore])):
                            foreach ($this->_exclusionurls[$urlstore] as $exclusionurl):
                                if (strpos($request, trim($exclusionurl, '/')) !== false):
                                    $attribute = ' evolved_excluded="1"';
                                    $html = str_replace($links[1][$index], $links[1][$index] . $attribute, $html);
                                    break;
                                endif;
                            endforeach;
                        endif;

                        if ($urlstore != $this->_thisstore):
                            $attribute = ' evolved_store="' . $urlstore . '"';
                            $html = str_replace($links[1][$index], $links[1][$index] . $attribute, $html);
                        endif;

                        $area = evolved::getBlockArea($url[2]);
                        if ($area):
                            $attribute = ' evolved_area="' . $area . '"';
                            $html = str_replace($links[1][$index], $links[1][$index] . $attribute, $html);
                        endif;
                    endif;
                endif;
            endif;
        endforeach;
        $transport->setHtml($html);
    }
    
    private function _insertCookieSelectsData($selects, $transport, $html)
    {
        foreach ($selects[0] as $index => $select):
            preg_match('/<select.*>/Us', $select, $match);
            preg_match('/onchange=("|\').*("|\')/Us', $match[0], $onchange);
            $onchange = isset($onchange[0]) ? strtolower($onchange[0]) : '';
            if (strpos($onchange, 'location') !== false):
                preg_match_all('/(<option.*)>.*<\/option>/Us', $select, $options);
                if (isset($options[0])):
                    foreach ($options[0] as $oindex => $option):
                        if (!preg_match('/evolved_area/s', $option)):
                            preg_match('/(value="|\')(.*)("|\')/Us', $option, $url);
                            $host = Mage::app()->getRequest()->getHttpHost();
                            if ($urlstore = Mage::helper('evolvedcaching')->getUrlStore($url[2], $host)):
                                $request = $url[2];
                                $request = explode('?', $url[2]);
                                $request = reset($request);
                                $request = rtrim($request, '/');
                                $request = preg_replace('/http(s)?:\/\/[^\/]*/', '', $request);
                                foreach ($this->_exclusionurls[$urlstore] as $exclusionurl):
                                    if (strpos($request, trim($exclusionurl, '/')) !== false):
                                        $attribute = ' evolved_excluded="1"';
                                        $html = str_replace($options[1][$oindex], $options[1][$oindex] . $attribute, $html);
                                        break;
                                    endif;
                                endforeach;

                                if ($urlstore != $this->_thisstore):
                                    $attribute = ' evolved_store="' . $urlstore . '"';
                                    $html = str_replace($options[1][$oindex], $options[1][$oindex] . $attribute, $html);
                                endif;

                                $area = evolved::getBlockArea($url[2]);
                                if ($area):
                                    $attribute = ' evolved_area="' . $area . '"';
                                    $html = str_replace($options[1][$oindex], $options[1][$oindex] . $attribute, $html);
                                endif;
                            endif;
                        endif;
                    endforeach;
                endif;
            endif;
        endforeach;
        $transport->setHtml($html);
    }
    
    private function _getBlocks()
    {
        if (!isset($this->_blocks)):
            $this->_blocks = Mage::helper('evolvedcaching')->getBlocks();
        endif;
        
        return $this->_blocks;
    }
    
    private function _getExcluded()
    {
        if (!isset($this->_excluded)):
            return Mage::helper('evolvedcaching')->getExcluded(true);
        endif;
        
        return $this->_excluded;
    }
    
    private function _isEnabled()
    {
        if (!isset($this->_enabled)):
            $params = Mage::app()->getRequest()->getParams();
            $status = false;
            $config = Mage::getStoreConfig(Mage::helper('evolvedcaching')->getSetupPath());
            $key = substr($config, -7);
            $config = substr($config, 0, -7);
            $details = '';
            $config = base64_decode($config);
            for ($i = 0; $i < strlen($config); $i++):
                $c = ord(substr($config, $i));
                $c -= ord(substr($key, (($i + 1) % strlen($key))));
                $details .= chr(abs($c) & 0xFF);
            endfor;

            $details = explode(',', $details);
            $host = Mage::app()->getRequest()->getHttpHost();
            foreach ($details as $detail):
                if ($detail == $host):
                    $status = true;
                endif;
            endforeach;
            $this->_enabled = @class_exists('evolved') && Mage::app()->useCache('evolved') && !isset($params['disabled']) && $status;
        endif;
        
        return $this->_enabled;
    }
    
    public function frontendControllerActionLayoutGenerateBlocksBefore($observer)
    {
        $request = Mage::app()->getRequest();
        if ($request->getPost('evolvedupdate')):
            $this->_updateSession();
            $layout = $observer->getLayout();
            $controller = $observer->getAction();
            
            $content = array();
            $blocks = Mage::app()->getRequest()->getPost();
            unset($blocks['evolvedupdate']);
            if (isset($blocks['evolved'])):
                unset($blocks['evolved']);
            endif;
            $isreview = isset($blocks['id']) && $blocks['id'] != 'false' ? true : false;
            if (isset($blocks['id'])):
                unset($blocks['id']);
            endif;
            
            if (!evolved::getUseAjax()):
                foreach (Mage::helper('evolvedcaching')->getBlocks() as $exclude):
                    $santised = preg_replace('/[^A-Za-z0-9]{1}/', '_', $exclude);
                    if (array_key_exists($santised, $blocks)):
                        unset($blocks[$santised]);
                    endif;
                    $blocks[$exclude] = $exclude;
                endforeach;
            endif;
            
            if ($blocks):
                $content = $this->_getBlocksContent($layout, $blocks, $content, $isreview, $controller);
            endif;

            if ($content):
                $params = Mage::app()->getRequest()->getParams();
                if (isset($params['evolved'])):
                    $response = Zend_Json::encode(array('content' => $content, 'showelements' => true));
                else:
                    $response = Zend_Json::encode(array('content' => $content, 'showelements' => false));
                endif;
            else:
                $response = Zend_Json::encode('');
            endif;
            
            if (!evolved::getUseAjax()):
                evolved::parseDynamicContent($response);
            else:
                Mage::app()->getResponse()->setBody($response)->sendResponse();
                exit();
            endif;
        endif;
    }
    
    private function _updateSession()
    {
        $this->_updateCategorySession();
    }
    
    private function _updateCategorySession()
    {
        if ($this->_isEnabled()):
            $url = Mage::helper('core/url')->getCurrentUrl();
            $area = evolved::getBlockArea($url);
            if (!empty($area) && $area == 'category'):
                $this->_updateSortingParams();
            endif;
        endif;
    }
    
    private function _updateSortingParams()
    {
        $session = Mage::getSingleton('catalog/session');
        if (!$session->getParamsMemorizeDisabled()):
            $params = Mage::app()->getRequest()->getParams();
            if (!empty($params) && is_array($params)):
                foreach ($params as $key => $value):
                    switch ($key):
                        case 'mode':
                            if ($key):
                                if ($mode = $session->getDisplayMode()):
                                    if ($mode != $value):
                                        $session->setData('display_mode', $value);
                                    endif;
                                else:
                                    $session->setData('display_mode', $value);
                                endif;
                            endif;
                            break;
                        case 'dir':
                            if ($key):
                                if ($dir = $session->getSortDirection()):
                                    if ($dir != $value):
                                        $session->setData('sort_direction', $value);
                                    endif;
                                else:
                                    $session->setData('sort_direction', $value);
                                endif;
                            endif;
                            break;
                        case 'limit':
                            if ($key):
                                if ($limit = $session->getLimitPage()):
                                    if ($limit != $value):
                                        $session->setData('limit_page', $value);
                                    endif;
                                else:
                                    $session->setData('limit_page', $value);
                                endif;
                            endif;
                            break;
                        case 'order':
                            if ($key):
                                if ($order = $session->getSortOrder()):
                                    if ($order != $value):
                                        $session->setData('sort_order', $value);
                                    endif;
                                else:
                                    $session->setData('sort_order', $value);
                                endif;
                            endif;
                            break;
                    endswitch;
                endforeach;
            endif;
        endif;
    }
    
    private function _getBlocksContent($layout, $blocks, $content, $isreview, $controller)
    {
        $this->_createLayout($layout, $blocks, $controller);
        
        foreach ($blocks as $block):
            if ($block == 'welcome'):
                $header = $layout->createBlock('page/html_header');
                if (Mage::helper('core')->isModuleEnabled('Mage_Persistent')):
                    $layout->addBlock('persistent/header_additional', 'header.additional');
                    $name = trim(Mage::helper('persistent/session')->getCustomer()->getName());
                    if (!empty($name) && !Mage::helper('customer')->isLoggedIn()):
                        $header->setAdditionalHtml($layout->getBlock('header.additional')->toHtml());
                    endif;
                else:
                    $header->setAdditionalHtml('');
                endif;
                if (isset($header) && is_object($header)):
                    $content[$block] = '<p class="welcome-msg">' . $header->getWelcome() . ' ' . $header->getAdditionalHtml() . '</p>';
                endif;
            elseif ($block == 'tier'):
                $content[$block] = $layout->createBlock('catalog/product_view')->getTierPriceHtml();
            elseif (strpos($block, 'eprice_') !== false):
                $product = explode('_', $block);
                $product = end($product);
                $product = Mage::getModel('catalog/product')->load((int) $product);
                if ($product):
                    if (!isset($this->_listblock)):
                        $this->_listblock = $layout->createBlock('catalog/product_list');
                    endif;
                    $content[$block] = $this->_listblock->getPriceHtml($product, true);
                endif;
            elseif (strpos($block, 'creview_') !== false):
                $product = explode('_', $block);
                $product = end($product);
                $product = Mage::getModel('catalog/product')->load((int) $product);
                if (!isset($this->_listblock)):
                    $this->_listblock = $layout->createBlock('catalog/product_list');
                endif;
                $content[$block] = $this->_listblock->getReviewsSummaryHtml($product, 'short');
            elseif (strpos($block, 'ecart_') !== false):
                $product = explode('_', $block);
                $product = end($product);
                $product = Mage::getModel('catalog/product')->load((int) $product);
                if (!isset($this->_listblock)):
                    $this->_listblock = $layout->createBlock('catalog/product_list');
                endif;
                $categorymode = Mage::helper('evolvedcaching')->getCategoryMode();
                $prepend = $categorymode == 'list' ? '<p>' : '';
                $append = $categorymode == 'list' ? '</p>' : '';
                if($product->isSaleable()):
                    $content[$block] = $prepend . '<button type="button" title="' . $this->_listblock->__('Add to Cart') . '" class="button btn-cart" onclick="setLocation(\'' . $this->_listblock->getAddToCartUrl($product) . '\')"><span><span>' . $this->_listblock->__('Add to Cart') . '</span></span></button>' . $append;
                else:
                    $content[$block] = '<p class="availability out-of-stock"><span>' . $this->_listblock->__('Out of stock') . '</span></p>';
                endif;
            elseif (strpos($block, 'preview_') !== false):
                $product = explode('_', $block);
                $product = end($product);
                $product = Mage::getModel('catalog/product')->load((int) $product);
                if ($isreview):
                    if (!Mage::registry('product')):
                        Mage::register('product', $product);
                    endif;
                    $review = $layout->createBlock('review/product_view');
                else:
                    $review = $layout->createBlock('catalog/product_list');
                endif;
                $content[$block] = $review->getReviewsSummaryHtml($product, false, true);
            else:
                if ($block == 'messages' || $block == 'global_messages'):
                    $controller->initLayoutMessages(
                        array(
                            'catalog/session',
                            'checkout/session',
                            'catalogsearch/session',
                            'customer/session',
                            'paypal/session',
                            'review/session',
                            'tag/session',
                            'wishlist/session'
                        )
                    );
                    $content[$block] = $layout->getMessagesBlock()->toHtml();
                elseif ($thisblock = $layout->getBlock($block)):
                    if ($block == 'breadcrumbs'):
                        $url = Mage::helper('core/url')->getCurrentUrl();
                        $area = evolved::getBlockArea($url);
                        if (!empty($area)):
                            if ($area == 'category' || $area == 'product'):
                                $layout->createBlock('catalog/breadcrumbs');
                            elseif ($area == 'cms' || $area == 'home'):
                                $layout->createBlock('cms/page');
                            endif;
                        endif;
                    endif;
                    $content[$block] = $thisblock->toHtml();
                endif;
            endif;
        endforeach;
        
        return $content;
    }
    
    private function _createLayout($layout, $blocks, $controller)
    {
        $xml = simplexml_load_string($layout->getXmlString(), Mage::getConfig()->getModelClassName('core/layout_element'));
        $newxml = simplexml_load_string('<layout/>', Mage::getConfig()->getModelClassName('core/layout_element'));
        $types = array('block', 'reference', 'action');
        
        foreach ($blocks as $block):
            if ($block != 'messages' && $block != 'global_messages'):
                foreach ($types as $type):
                    $nodes = $xml->xpath('//' . $type . '[@name="' . $block . '"]');
                    foreach ($nodes as $node):
                        $newxml->appendChild($node);
                    endforeach;
                endforeach;
            endif;
        endforeach;
        
        $layout->setXml($newxml);
        
        $layout->generateBlocks();
    }
    
    private function _hasHoldingHtml($block)
    {
        
        if (!Mage::getStoreConfig('evolvedcaching/general/useajax')) return '';
        
        switch ($block):
            case 'price':
                if (Mage::getStoreConfig('evolvedcaching/exclude/price')):
                    if ($html = Mage::getStoreConfig('evolvedcaching/exclude/price_holding')):
                        return $html;
                    endif;
                endif;
                break;
            case 'creview':
               if (Mage::getStoreConfig('evolvedcaching/exclude/creview')):
                   if ($html = Mage::getStoreConfig('evolvedcaching/exclude/creview_holding')):
                       return $html;
                   endif;
               endif;
                break;
            case 'cart':
                if (Mage::getStoreConfig('evolvedcaching/exclude/cart')):
                    if ($html = Mage::getStoreConfig('evolvedcaching/exclude/cart_holding')):
                        return $html;
                    endif;
                endif;
                break;
            case 'preview':
                if (Mage::getStoreConfig('evolvedcaching/exclude/preview')):
                    if ($html = Mage::getStoreConfig('evolvedcaching/exclude/preview_holding')):
                        return $html;
                    endif;
                endif;
                break;
            case 'tier':
                if (Mage::getStoreConfig('evolvedcaching/exclude/tier')):
                    if ($html = Mage::getStoreConfig('evolvedcaching/exclude/tier_holding')):
                        return $html;
                    endif;
                endif;
                break;
            case 'welcome':
                if (Mage::getStoreConfig('evolvedcaching/exclude/welcome')):
                    if ($html = Mage::getStoreConfig('evolvedcaching/exclude/welcome_holding')):
                        return $html;
                    endif;
                endif;
                break;
            default:
                $holdinghtml = Mage::getStoreConfig('evolvedcaching/exclude/blocks_holding');
                if ($holdinghtml = Zend_Json::decode($holdinghtml)):
                    if (!empty($holdinghtml[$block])):
                        return $holdinghtml[$block];
                    endif;
                endif;
                break;
        endswitch;

        return '';
    }
    
    public function frontendHttpResponseSendBefore($observer)
    {
        $request = Mage::app()->getRequest();
        if ($request->getPost('evolvedupdate')):
            $response = $observer->getResponse();
            if ($response->isRedirect()):
                $headers = $response->getHeaders();
                if (!empty($headers) && is_array($headers)):
                    foreach ($headers as $id => $header):
                        if (!empty($header['name']) && strtolower($header['name']) == 'location'):
                            if (!empty($header['value'])):
                                $url = $header['value'];
                                $data = $request->getPost();
                                if (!empty($data) && is_array($data)):
                                    $args = array();
                                    foreach ($data as $key => $value):
                                        $args[] = $key . '=' . $value;
                                    endforeach;
                                    $args = implode('&', $args);
                                    if (strpos($url, '?')):
                                        $url = $url . '&' . $args;
                                    else:
                                        $url = $url . '?' . $args;
                                    endif;
                                    $response->setHeader('Location', $url, true);
                                endif;
                            endif;
                        endif;
                    endforeach;
                endif;
            endif;
        endif;
    }
}