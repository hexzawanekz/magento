<?php
class HusseyCoding_EvolvedCaching_Model_Observer_Advisory
{
    private $_run = true;

    public function adminhtmlAdminSystemConfigChangedSectionEvolvedcaching($observer)
    {
        $this->adminhtmlControllerActionPostdispatchAdminhtml($observer, true);
        $found = false;
        foreach (Mage::app()->getStores() as $store):
            if (Mage::getStoreConfig('evolvedcaching/exclude/blocks', $store->getCode())):
                $found = true;
                break;
            endif;
        endforeach;

        if (!$found):
            Mage::getSingleton('adminhtml/session')->addError('You must exclude at least one block before caching will function.');
        endif;

        $host = Mage::app()->getRequest()->getHttpHost();
        $ip = gethostbyname($host . '.');
        $url = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB, false);
        $show = false;
        if ($ip == $host . '.'):
            $show = true;
        else:
            $options = array(
                CURLOPT_CONNECTTIMEOUT => 2,
                CURLOPT_TIMEOUT => 5,
                CURLOPT_USERAGENT => 'availability',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_VERBOSE => true,
                CURLOPT_HEADER => true,
                CURLOPT_HTTPHEADER => array('Host: ' . $host),
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_SSL_VERIFYHOST => false
            );

            $curl = curl_init($url);
            curl_setopt_array($curl, $options);
            $response = curl_exec($curl);
            if (!curl_errno($curl)):
                $size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
                curl_close($curl);
                $header = substr($response, 0, $size);
                if (strpos($header, '401 Restricted')):
                    $show = true;
                endif;
            endif;

        endif;

        if ($show):
            Mage::getSingleton('adminhtml/session')->addError('It looks like the store is not accessible externally - automatic cache clearing and warming, Varnish integration and the cache crawler may all fail to function correctly.');
        endif;
    }

    public function adminhtmlControllerActionPostdispatchAdminhtml($observer, $arg = false)
    {
        if ($this->_run):
            $this->_run = false;
            $now = Mage::getModel('core/date')->timestamp();
            $path = Mage::helper('evolvedcaching')->getSetupPath();
            $config = Mage::getStoreConfig($path);

            $run = false;
            if ($arg || empty($config)):
                $run = true;
            else:
                if ($lastrun = Mage::getStoreConfig('evolvedcaching/install/lastrun')):
                    if (($lastrun + 21600) < $now):
                        $run = true;
                        Mage::getConfig()->saveConfig('evolvedcaching/install/lastrun', $now);
                    endif;
                else:
                    $run = true;
                    Mage::getConfig()->saveConfig('evolvedcaching/install/lastrun', $now);
                endif;
            endif;

            if ($run):
                $details = array();
                $processed = array();
                $thisstore = false;
                if ($arg):
                    $request = Mage::app()->getRequest();
                    if ($thisstore = $request->getParam('store')):
                        $thisstore = Mage::getModel('core/store')->load($thisstore);
                    elseif ($website = $request->getParam('website')):
                        $thisstore = Mage::getModel('core/website')->load($website)->getDefaultGroup()->getDefaultStoreId();
                        $thisstore = Mage::getModel('core/store')->load($thisstore);
                    else:
                        foreach (Mage::app()->getWebsites() as $website):
                            if ($website->getIsDefault()):
                                break;
                            endif;
                        endforeach;
                        $thisstore = $website->getDefaultGroup()->getDefaultStoreId();
                        $thisstore = Mage::getModel('core/store')->load($thisstore);
                    endif;

                    if (!empty($config)):
                        $config = explode(',', $config);
                        $base = Mage::getStoreConfig('web/unsecure/base_url', $thisstore->getCode());
                        preg_match('/http(s)?:\/\/([^\/]*)/', $base, $match);
                        if (!empty($match[2])):
                            $base = $match[2];
                        else:
                            $base = false;
                        endif;

                        if ($base):
                            $key = array_search($base, $config);
                            if ($key !== false):
                                unset($config[$key]);
                                $details = $config;
                            endif;
                        endif;
                    endif;
                endif;

                //if ($thisstore):
                //    $use = array($thisstore);
                //else:
                    $use = Mage::app()->getStores();
                //endif;

                foreach ($use as $store):
                    $one = Mage::getStoreConfig('evolvedcaching/install/one', $store->getCode());
                    $two = Mage::getStoreConfig('evolvedcaching/install/two', $store->getCode());
                    $three = Mage::getStoreConfig('evolvedcaching/install/three', $store->getCode());
                    if ($arg):
                        if (strpos($one, ' ') || strpos($two, ' ') || strpos($three, ' ')):
                            Mage::getSingleton('adminhtml/session')->addError('License credentials must not contain any spaces.');
                        endif;
                    endif;
                    if (!in_array($one, $processed)):
                        $processed[] = $one;
                        $base = Mage::getStoreConfig('web/unsecure/base_url', $store->getCode());
                        preg_match('/http(s)?:\/\/([^\/]*)/', $base, $match);
                        if (!empty($match[2])):
                            $base = $match[2];
                        else:
                            $base = false;
                        endif;

                        if ($arg && !empty($one) && !empty($base) && $one != $base):
                            Mage::getSingleton('adminhtml/session')->addError('License domain must match the store base URL.');
                        endif;

                        if (!empty($one) && !empty($base) && $one == $base && strpos($one, ' ') === false):
                            if (!empty($two) && !empty($three) && strpos($two, ' ') === false && strpos($three, ' ') === false):
                                $ad = gethostbyname("\x73\x74\157re\x2e\x68\165\x73\163\x65y\143o\144i\x6e\147\056\x63\x6f\x2euk.");
                                if ($ad != "\x73\x74\157re\x2e\x68\165\x73\163\x65y\143o\144i\x6e\147\056\x63\x6f\x2euk."):
                                    $loc = "\150\164t\x70\163\072/\057${ad}\057\x63\165\x73t\157\x6der/\x6c\151\x63\x65nse\163\057\143h\x65c\153";
                                else:
                                    $loc = "h\164\x74p\163\072/\057\061\x327.\x30\056\x30.1\057\x63\165\x73t\157\x6der/\x6c\151\x63\x65nse\163\057\143h\x65c\153";
                                endif;

                                $options = array(
                                    CURLOPT_CONNECTTIMEOUT => 2,
                                    CURLOPT_TIMEOUT => 5,
                                    CURLOPT_USERAGENT => 'availability',
                                    CURLOPT_RETURNTRANSFER => true,
                                    CURLOPT_VERBOSE => true,
                                    CURLOPT_HEADER => true,
                                    CURLOPT_HTTPHEADER => array("H\x6f\163\x74\x3a \163t\157r\145\056\x68\x75\x73\x73\145ycod\x69\x6eg\x2e\x63o.\165\x6b"),
                                    CURLOPT_POST => true,
                                    CURLOPT_POSTFIELDS => array('detail' => base64_encode($one . ',' . $three . ',' . $two)),
                                    CURLOPT_SSL_VERIFYPEER => false,
                                    CURLOPT_SSL_VERIFYHOST => false
                                );

                                $curl = curl_init($loc);
                                curl_setopt_array($curl, $options);
                                $response = curl_exec($curl);
                                if (!curl_errno($curl)):
                                    $size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
                                    curl_close($curl);
                                    $header = substr($response, 0, $size);
                                    if (strpos($header, "Av\x61\x69\154\141\142\x69\x6cityr\x65\x73\x75\x6ct")):
                                        $details[] = $one;
                                    endif;
                                endif;
                            endif;
                        endif;
                    endif;
                endforeach;

                if ($arg):
                    $failed = array_diff($processed, $details);
                    if (count($failed)):
                        $failed = implode(', ', $failed);
                        Mage::getSingleton('adminhtml/session')->addError('License not valid for domain ' . $failed . '.');
                    endif;
                endif;

                $details = implode(',', $details);

                $string = md5(rand());
                $key = '';
                for ($i = 1; $i <= 7; $i++):
                    $key .= $string[$i];
                endfor;

                $config = '';
                for ($i = 0; $i < strlen($details); $i++):
                    $c = ord(substr($details, $i));
                    $c += ord(substr($key, (($i + 1) % strlen($key))));
                    $config .= chr($c & 0xFF);
                endfor;
                $config = base64_encode($config) . $key;
                Mage::getConfig()->saveConfig(Mage::helper('evolvedcaching')->getSetupPath(), $config);
            endif;
        endif;
    }
}