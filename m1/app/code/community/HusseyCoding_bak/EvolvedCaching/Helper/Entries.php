<?php
class HusseyCoding_EvolvedCaching_Helper_Entries extends HusseyCoding_EvolvedCaching_Helper_Data
{
    private $_memcached;
    private $_mexists;
    private $_aexists;
    private $_categoryids = array();
    private $_match = false;
    private $_cronwarm;
    private $_ips = array();
    private $_queue = array();
    private $_missing = array();
    private $_stores = array();
    
    public function __construct()
    {
        $this->_mexists = @extension_loaded('memcache') && @class_exists('Memcache');
        $this->_aexists = @extension_loaded('apc') && @ini_get('apc.enabled');
    }
    
    public function validateEntries($collection)
    {
        foreach ($collection as $entry):
            $cachekey = 'page_' . $entry->getCachekey();
            switch ($entry->getStorage()):
                case 'files':
                    if ($this->_validateFilesEntry($cachekey)):
                        $entry->setExpired(false);
                    else:
                        $entry->setExpired(true);
                    endif;
                    break;
                case 'memcached':
                    if ($this->_validateMemcachedEntry($cachekey)):
                        $entry->setExpired(false);
                    else:
                        $entry->setExpired(true);
                    endif;
                    break;
                case 'apc':
                    if ($this->_validateApcEntry($cachekey)):
                        $entry->setExpired(false);
                    else:
                        $entry->setExpired(true);
                    endif;
            endswitch;
        endforeach;
    }
    
    public function refreshEntry($entry)
    {
        $cachekey = 'page_' . $entry->getCachekey();
        switch ($entry->getStorage()):
            case 'files':
                $this->_verifyFilesEntry($cachekey, $entry);
                break;
            case 'memcached':
                $this->_verifyMemcachedEntry($cachekey, $entry);
                break;
            case 'apc':
                $this->_verifyApcEntry($cachekey, $entry);
                break;
            default:
                $this->_clearLogEntry($entry);
        endswitch;
    }
    
    private function _validateFilesEntry($cachekey)
    {
        $path = @class_exists('evolved') ? EVOLVED_ROOT : MAGENTO_ROOT;
        $path = $path . DS . 'var' . DS . 'evolved' . DS . $cachekey;
        if (!file_exists($path)):
            return false;
        endif;
        
        return true;
    }
    
    private function _verifyFilesEntry($cachekey, $entry)
    {
        $path = @class_exists('evolved') ? EVOLVED_ROOT : MAGENTO_ROOT;
        $path = $path . DS . 'var' . DS . 'evolved' . DS . $cachekey;
        if (!file_exists($path)):
            $this->_clearLogEntry($entry);
        endif;
    }
    
    private function _validateMemcachedEntry($cachekey)
    {
        if ($this->_mexists):
            if ($memcached = $this->_getMemcachedServer()):
                if (!$memcached->get($cachekey)):
                    return false;
                endif;
            else:
                return false;
            endif;
        else:
            return false;
        endif;
        
        return true;
    }
    
    private function _verifyMemcachedEntry($cachekey, $entry)
    {
        if ($this->_mexists):
            if ($memcached = $this->_getMemcachedServer()):
                if (!$memcached->get($cachekey)):
                    $this->_clearLogEntry($entry);
                endif;
            else:
                $this->_clearLogEntry($entry);
            endif;
        else:
            $this->_clearLogEntry($entry);
        endif;
    }
    
    private function _validateApcEntry($cachekey)
    {
        if ($this->_aexists):
            if (!apc_fetch($cachekey)):
                return false;
            endif;
        else:
            return false;
        endif;
        
        return true;
    }
    
    private function _verifyApcEntry($cachekey, $entry)
    {
        if ($this->_aexists):
            if (!apc_fetch($cachekey)):
                $this->_clearLogEntry($entry);
            endif;
        else:
            $this->_clearLogEntry($entry);
        endif;
    }
    
    private function _clearLogEntry($entry)
    {
        $entry->delete();
    }
    
    public function clearCacheEntry($id)
    {
        $entry = Mage::getModel('evolvedcaching/entries')->load((int) $id);
        
        $this->_clearVarnishEntry($entry);
        
        $cachekey = 'page_' . $entry->getCachekey();
        switch ($entry->getStorage()):
            case 'files':
                $cachedir = @class_exists('evolved') ? EVOLVED_ROOT : MAGENTO_ROOT;
                if ($cachedir == 'MAGENTO_ROOT'):
                    $cachedir = getcwd();
                endif;
                if ($cachedir):
                    $cachedir .= DS . 'var' . DS . 'evolved' . DS;
                    $file = $cachedir . $cachekey;
                    if (is_file($file)):
                        @unlink($file);
                    endif;
                endif;
                break;
            case 'memcached':
                if ($this->_mexists):
                    if ($memcached = $this->_getMemcachedServer()):
                        if ($memcached->get($cachekey)):
                            $memcached->delete($cachekey);
                        endif;
                    endif;
                endif;
                break;
            case 'apc':
                if ($this->_aexists):
                    if (apc_fetch($cachekey)):
                        apc_delete($cachekey);
                    endif;
                endif;
        endswitch;
        
        $this->_clearLogEntry($entry);
    }
    
    private function _getMemcachedServer()
    {
        if (!isset($this->_memcached)):
            $host = Mage::getStoreConfig('evolvedcaching/storage/host');
            $port = Mage::getStoreConfig('evolvedcaching/storage/port');
            $persistent = Mage::getStoreConfig('evolvedcaching/storage/persistent');
            $expires = Mage::getStoreConfig('evolvedcaching/storage/memcachedexpires');
            $storage = $this->getStorage();
            $expires = $storage['type'] == 'memcached' && $expires ? $expires : false;

            if ($host && $port):
                $this->_memcached = new Memcache;
                if ($persistent):
                    if (!$this->_memcached->pconnect($host, $port)):
                        Mage::getSingleton('adminhtml/session')->addError('Failed to connect to memcached server.');
                        $this->_memcached = false;
                        return false;
                    endif;
                else:
                    if (!$this->_memcached->connect($host, $port)):
                        Mage::getSingleton('adminhtml/session')->addError('Failed to connect to memcached server.');
                        $this->_memcached = false;
                        return false;
                    endif;
                endif;
            endif;
        endif;
        
        return $this->_memcached;
    }
    
    public function clearEntriesOfType($type = 'files')
    {
        $resource = Mage::getSingleton('core/resource');
        $table = $resource->getTableName('evolvedcaching/evolved_caching');
        $connection = $resource->getConnection('core_write');
        $sql = 'DELETE FROM `' . $table . '` WHERE `storage` = \'' . $type . '\';';
        $connection->query($sql);
    }
    
    public function clearProductCache($product)
    {
        foreach ($this->getProductPaths($product) as $path):
            if (!$this->_clearCacheByPath($path) && Mage::getStoreConfig('evolvedcaching/autowarm/always')):
                $this->_missing[] = $path;
            endif;
        endforeach;
    }
    
    public function getProductPaths($product)
    {
        $paths = $this->_getProductRewrites($product);
        $categoryids = array_merge($this->_getCategoryIds(), $product->getCategoryIds());
        $categoryids = $this->_addAnchorCategories($categoryids);
        foreach ($categoryids as $id):
            $category = Mage::getModel('catalog/category')->load((int) $id);
            $paths = $this->_getCategoryRewrites($category, $paths);
        endforeach;
        
        return $paths;
    }
    
    public function clearCategoryCache($category)
    {
        foreach ($this->_getCategoryRewrites($category) as $path):
            if (!$this->_clearCacheByPath($path) && Mage::getStoreConfig('evolvedcaching/autowarm/always')):
                $this->_missing[] = $path;
            endif;
        endforeach;
    }
    
    public function clearCmsCache($cms)
    {
        $identifier = $cms->getIdentifier();
        $stores = $cms->getStores();
        if (empty($stores)):
            $stores = $cms->getStoreId();
        endif;
        foreach ($this->_getStores($stores) as $store):
            $defaulthome = Mage::getStoreConfig('web/default/cms_home_page', $store->getCode());
            if ($identifier == $defaulthome):
                if (!$this->_clearCacheByPath('') && Mage::getStoreConfig('evolvedcaching/autowarm/always')):
                    $this->_missing[] = '';
                    $this->_stores[$store->getId()][] = '';
                endif;
                if (Mage::getStoreConfig('web/url/use_store')):
                    if (!$this->_clearCacheByPath($store->getCode()) && Mage::getStoreConfig('evolvedcaching/autowarm/always')):
                        $this->_missing[] = $store->getCode();
                        $this->_stores[$store->getId()][] = $store->getCode();
                    endif;
                endif;
            else:
                $path = Mage::helper('cms/page')->getPageUrl($cms->getId());
                $path = explode('/', $path);
                $path = end($path);

                if (!$this->_clearCacheByPath($path) && Mage::getStoreConfig('evolvedcaching/autowarm/always')):
                    $this->_missing[] = $path;
                    $this->_stores[$store->getId()][] = $path;
                endif;

                if (Mage::getStoreConfig('web/url/use_store')):
                    if (!$this->_clearCacheByPath($store->getCode() . '/' . $path) && Mage::getStoreConfig('evolvedcaching/autowarm/always')):
                        $this->_missing[] = $path;
                        $this->_stores[$store->getId()][] = $path;
                    endif;
                endif;
            endif;
        endforeach;
    }
    
    private function _getStores($ids)
    {
        $all = false;
        $storeids = array();
        foreach ($ids as $id):
            if ($id == '0'):
                $all = true;
                break;
            else:
                $storeids[] = $id;
            endif;
        endforeach;

        $stores = array();
        if ($all):
            foreach (Mage::app()->getStores() as $store):
                $stores[] = $store;
            endforeach;
        else:
            foreach ($storeids as $id):
                $stores[] = Mage::getModel('core/store')->load((int) $id);
            endforeach;
        endif;
        
        return $stores;
    }
    
    private function _clearCacheByPath($path)
    {
        $collection = Mage::getModel('evolvedcaching/entries')->getCollection();
        if ($path):
            $collection->getSelect()->where('request = ?', '/' . $path);
        else:
            $collection->getSelect()->where('request = ?', $path);
        endif;
        
        $found = false;
        foreach ($collection as $entry):
            $found = true;
            if (!$this->_match):
                $this->_match = true;
            endif;
            $warmrequest = $this->_warmByStrategy($entry);
            $this->clearCacheEntry($entry->getId());
            if ($warmrequest):
                $this->_queue[] = $warmrequest;
            endif;
        endforeach;
        
        return $found;
    }
    
    private function _getProductRewrites($product, $urls = null)
    {
        $urls = isset($urls) ? $urls : array();
        $products = array();
        
        $type = $product->getTypeInstance(true);
        $products[] = $product;
        if (!$type->getChildrenIds($product->getId(), false)):
            if ($parents = $this->_getProductParents($product)):
                foreach ($parents as $parent):
                    $products[] = $parent;
                endforeach;
            endif;
        endif;
        
        foreach ($products as $product):
            $path = 'catalog/product/view/id/' . $product->getId();
            $rewrites = Mage::getModel('core/url_rewrite')->getCollection();
            $rewrites->getSelect()
                ->where('target_path = ?', $path)
                ->orWhere('target_path LIKE ?', $path . '/%');

            foreach ($rewrites as $rewrite):
                if ($url = $rewrite->getRequestPath()):
                    if (!in_array($url, $urls)):
                        $urls[] = $url;
                        foreach ($this->_getStores($product->getStoreIds()) as $store):
                            $this->_stores[$store->getId()][] = $url;
                        endforeach;
                        if (Mage::getStoreConfig('web/url/use_store')):
                            foreach ($this->_getStores($product->getStoreIds()) as $store):
                                $newurl = $store->getCode() . '/' . $url;
                                if (!in_array($newurl, $urls)):
                                    $urls[] = $newurl;
                                    $this->_stores[$store->getId()][] = $newurl;
                                endif;
                            endforeach;
                        endif;
                    endif;
                endif;
            endforeach;

            if (!in_array($path, $urls)):
                $urls[] = $path;
            endif;
        endforeach;
        
        return $urls;
    }
    
    private function _getCategoryRewrites($category, $urls = null)
    {
        $urls = isset($urls) ? $urls : array();
        $path = 'catalog/category/view/id/' . $category->getId();
        $rewrites = Mage::getModel('core/url_rewrite')->getCollection();
        $rewrites->getSelect()
            ->where('target_path = ?', $path)
            ->orWhere('target_path LIKE ?', $path . '/%');
        
        foreach ($rewrites as $rewrite):
            if ($url = $rewrite->getRequestPath()):
                if (!in_array($url, $urls)):
                    $urls[] = $url;
                    foreach ($this->_getStores($category->getStoreIds()) as $store):
                        $this->_stores[$store->getId()][] = $url;
                    endforeach;
                    if (Mage::getStoreConfig('web/url/use_store')):
                        foreach ($this->_getStores($category->getStoreIds()) as $store):
                            $newurl = $store->getCode() . '/' . $url;
                            if (!in_array($newurl, $urls)):
                                $urls[] = $newurl;
                                $this->_stores[$store->getId()][] = $newurl;
                            endif;
                        endforeach;
                    endif;
                endif;
            endif;
        endforeach;
        
        if (!in_array($path, $urls)):
            $urls[] = $path;
        endif;
        
        return $urls;
    }
    
    private function _getProductParents($product)
    {
        $parents = array();
        $categoryids = array();
        $products = array();
        
        $type = Mage::getModel('catalog/product_type_grouped');
        foreach ($type->getParentIdsByChild($product->getId()) as $parentid):
            $products[] = Mage::getModel('catalog/product')->load((int) $parentid);
        endforeach;
        
        $type = Mage::getModel('catalog/product_type_configurable');
        foreach ($type->getParentIdsByChild($product->getId()) as $parentid):
            $products[] = Mage::getModel('catalog/product')->load((int) $parentid);
        endforeach;
        
        $type = Mage::getModel('bundle/product_type');
        foreach ($type->getParentIdsByChild($product->getId()) as $parentid):
            $products[] = Mage::getModel('catalog/product')->load((int) $parentid);
        endforeach;
        
        foreach ($products as $product):
            foreach ($product->getCategoryIds() as $id):
                $categoryids[] = $id;
            endforeach;
            $parents[] = $product;
        endforeach;
        
        $this->_setCategoryIds($categoryids);
        
        return $parents;
    }
    
    public function clearBlockHtmlCache()
    {
        Mage::app()->getCacheInstance()->cleanType('block_html');
    }
    
    private function _addAnchorCategories($categoryids)
    {
        foreach ($categoryids as $categoryid):
            $category = Mage::getModel('catalog/category')->load($categoryid);
            $category = $category->getParentCategory();
            $escape = 50;
            while ($category->getId() && $escape > 0):
                if ($category->getIsAnchor()):
                    if (!in_array($category->getId(), $categoryids)):
                        $categoryids[] = $category->getId();
                    endif;
                endif;
                $category = $category->getParentCategory();
                $escape--;
            endwhile;
        endforeach;
        
        return $categoryids;
    }
    
    private function _setCategoryIds($categoryids)
    {
        $this->_categoryids = $categoryids;
    }
    
    private function _getCategoryIds()
    {
        return $this->_categoryids;
    }
    
    private function _buildWarmRequest($entry)
    {
        if (Mage::getStoreConfig('evolvedcaching/autowarm/enabled')):
            if ($entry->getCachekey()):
                $cachekey = 'page_' . $entry->getCachekey();
                switch ($entry->getStorage()):
                    case 'files':
                        if (!$this->_validateFilesEntry($cachekey)):
                            return false;
                        endif;
                        break;
                    case 'memcached':
                        if (!$this->_validateMemcachedEntry($cachekey)):
                            return false;
                        endif;
                        break;
                    case 'apc':
                        if (!$this->_validateApcEntry($cachekey)):
                            return false;
                        endif;
                endswitch;
            endif;
            
            foreach (Mage::app()->getStores() as $store):
                if ($store->getCode() == $entry->getStorecode()):
                    $secure = $entry->getProtocol() == 'http' ? false : true;
                    $baseurl = $store->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB, $secure);
                endif;
            endforeach;

            if (isset($baseurl)):
                $host = preg_replace('/'. $entry->getProtocol() . ':\/\//', '', $baseurl);
                $host = trim($host, '/');
                if (!isset($this->_ips[$host])):
                    $this->_ips[$host] = gethostbyname($host . '.');
                endif;
                if ($this->_ips[$host] != $host . '.'):
                    $url = $entry->getProtocol() . '://' . $this->_ips[$host] . '/' . ltrim($entry->getRequest(), '/');
                else:
                    $url = $entry->getProtocol() . '://127.0.0.1/' . ltrim($entry->getRequest(), '/');
                endif;
                
                $append = array();

                if ($arguments = $entry->getCategorymodifier()):
                    $arguments = explode('<br />', $arguments);
                    foreach ($arguments as $argument):
                        $argument = explode(': ', $argument);
                        switch ($argument[0]):
                            case 'Direction':
                                $append[] = 'dir=' . $argument[1];
                                break;
                            case 'Limit':
                                $append[] = 'limit=' . $argument[1];
                                break;
                            case 'Mode':
                                $append[] = 'mode=' . $argument[1];
                                break;
                            case 'Order':
                                $append[] = 'order=' . $argument[1];
                                break;
                            case 'Page':
                                $append[] = 'p=' . $argument[1];
                                break;
                        endswitch;
                    endforeach;
                endif;

                if ($arguments = $entry->getLayeredmodifier()):
                    $arguments = explode('<br />', $arguments);
                    foreach ($arguments as $argument):
                        $argument = explode(': ', $argument);
                        $append[] = $argument[0] . '=' . $argument[1];
                    endforeach;
                endif;

                if ($append):
                    $append = implode('&', $append);
                    $url = $url . '?' . $append;
                endif;
                
                $url = array($url);
                
                if ($entry->getTax() && $entry->getCurrency()):
                    $options = array(
                        CURLOPT_POST => true,
                        CURLOPT_POSTFIELDS => array('warmrequest' => 'true', 'tax' => $entry->getTax(), 'currency' => $entry->getCurrency()),
                        CURLOPT_CONNECTTIMEOUT => 2,
                        CURLOPT_TIMEOUT => 5,
                        CURLOPT_SSL_VERIFYPEER => false,
                        CURLOPT_SSL_VERIFYHOST => false,
                        CURLOPT_COOKIE => 'currency=' . $entry->getCurrency() . ';evolved_tax=' . $entry->getTax() . ';store=' . $entry->getStorecode(),
                        CURLOPT_HTTPHEADER => array('Host: ' . $host)
                    );
                
                    $agent = explode('<br />', $entry->getAgentmodifier());
                    $agent = reset($agent);
                    if ($agent && !is_array($agent)):
                        $options[CURLOPT_USERAGENT] = $agent;
                    else:
                        $options[CURLOPT_USERAGENT] = 'evolvedcaching_crawler';
                    endif;
                else:
                    $options = array(
                        CURLOPT_POST => true,
                        CURLOPT_POSTFIELDS => array('warmrequest' => 'true'),
                        CURLOPT_CONNECTTIMEOUT => 2,
                        CURLOPT_TIMEOUT => 5,
                        CURLOPT_SSL_VERIFYPEER => false,
                        CURLOPT_SSL_VERIFYHOST => false,
                        CURLOPT_COOKIE => 'store=' . $entry->getStorecode(),
                        CURLOPT_HTTPHEADER => array('Host: ' . $host),
                        CURLOPT_USERAGENT => 'evolvedcaching_crawler'
                    );
                endif;
                
                return array('url' => $url, 'options' => $options);
            endif;
        endif;
        
        return false;
    }
    
    private function _execWarmRequest($warmrequest)
    {
        if ($warmrequest):
            $adapter = new Varien_Http_Adapter_Curl();
            ob_start();
            $adapter->multiRequest($warmrequest['url'], $warmrequest['options']);
            ob_end_clean();
        endif;
    }
    
    private function _reportStatus()
    {
        if (Mage::app()->useCache('evolved')):
            if (Mage::getStoreConfig('evolvedcaching/autowarm/enabled')):
                if ($this->_match):
                    if (Mage::getStoreConfig('evolvedcaching/autowarm/cron')):
                        Mage::getSingleton('adminhtml/session')->addSuccess('Full page cache warming requests have been successfully queued.');
                    else:
                        Mage::getSingleton('adminhtml/session')->addSuccess('Full page cache has been warmed.');
                    endif;
                else:
                    Mage::getSingleton('adminhtml/session')->addNotice('No full page cache entries to warm.');
                endif;
            endif;
        endif;
    }
    
    private function _warmByStrategy($entry)
    {
        if (Mage::app()->useCache('evolved')):
            if (!isset($this->_cronwarm)):
                $this->_cronwarm = Mage::getStoreConfig('evolvedcaching/autowarm/cron') ? true : false;
            endif;

            if ($this->_cronwarm):
                $this->_addWarmEntry($entry);
            else:
                return $this->_buildWarmRequest($entry);
            endif;
        endif;
        
        return false;
    }
    
    private function _addWarmEntry($entry)
    {
        $queue = Mage::getModel('evolvedcaching/warming');
        if ($warmrequest = $this->_buildWarmRequest($entry)):
            $warmrequest = Zend_Json::Encode($warmrequest);
            $queue->setRequest($warmrequest)->save();
        endif;
    }
    
    public function warmCache()
    {
        foreach (Mage::getModel('evolvedcaching/warming')->getCollection() as $warmer):
            $warmrequest = $warmer->getRequest();
            $warmrequest = Zend_Json::decode($warmrequest);
            $this->_execWarmRequest($warmrequest);
            $warmer->delete();
        endforeach;
        
        $this->userCleanup();
    }
    
    public function processWarmQueue($cms = false)
    {
        $this->_warmMissing($cms);
        
        foreach ($this->_queue as $warmrequest):
            $this->_execWarmRequest($warmrequest);
        endforeach;
        
        $this->_reportStatus();
    }
    
    private function _warmMissing($cms = false)
    {
        foreach ($this->_missing as $missing):
            if (!$cms):
                foreach ($this->_stores as $id => $urls):
                    if (in_array($missing, $urls)):
                        $store = Mage::getModel('core/store')->load((int) $id);
                        break;
                    endif;
                endforeach;
                
                if (!empty($store) && $store->getId() && !$this->_checkExcluded($missing, $store)):
                    $entry = Mage::getModel('evolvedcaching/entries');
                    $protocol = strpos($store->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB, false), 'https') !== false ? 'https' : 'http';

                    $entry
                        ->setStorecode($store->getCode())
                        ->setRequest('/' . $missing)
                        ->setProtocol($protocol);

                    if ($warmrequest = $this->_warmByStrategy($entry)):
                        $this->_queue[] = $warmrequest;
                    endif;

                    if (!$this->_match):
                        $this->_match = true;
                    endif;
                endif;
            else:   
                foreach ($this->_getStores($cms->getStores()) as $store):
                    if (!empty($store) && $store->getId() && !$this->_checkExcluded($missing, $store)):
                        $protocol = strpos($store->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB, false), 'https') !== false ? 'https' : 'http';
                        $entry = Mage::getModel('evolvedcaching/entries');

                        $entry
                            ->setStorecode($store->getCode())
                            ->setRequest('/' . $missing)
                            ->setProtocol($protocol);

                        if ($warmrequest = $this->_warmByStrategy($entry)):
                            $this->_queue[] = $warmrequest;
                        endif;

                        if (!$this->_match):
                            $this->_match = true;
                        endif;
                    endif;
                endforeach;
            endif;
        endforeach;
    }
    
    private function _checkExcluded($url, $store)
    {
        $rewrites = Mage::getStoreConfig('web/seo/use_rewrites', $store->getCode());
        if (strpos($url, '/view/id/') !== false && $rewrites) return true;
                
        $config = Mage::getStoreConfig('evolvedcaching/exclude/pages', $store->getCode());
        $config = explode(',', $config);
        
        foreach ($config as $exclude):
            $exclude = trim($exclude);
            if (strpos($url, trim($exclude, '/')) !== false):
                return true;
            endif;
        endforeach;
        
        return false;
    }
    
    public function getCachePageHtml($id)
    {
        if ($id):
            $entry = Mage::getModel('evolvedcaching/entries')->load((int) $id);
            if ($entry->getId()):
                $cachekey = 'page_' . $entry->getCachekey();
                switch ($entry->getStorage()):
                    case 'files':
                        $cachedir = @class_exists('evolved') ? EVOLVED_ROOT : MAGENTO_ROOT;
                        if ($cachedir):
                            $cachedir .= DS . 'var' . DS . 'evolved' . DS;
                            $file = $cachedir . $cachekey;
                            if (is_file($file)):
                                if ($html = file_get_contents($file)):
                                    return $this->_stripCacheTags($html);
                                endif;
                            endif;
                        endif;
                        break;
                    case 'memcached':
                        if ($this->_mexists):
                            if ($memcached = $this->_getMemcachedServer()):
                                if ($html = $memcached->get($cachekey)):
                                    return $this->_stripCacheTags($html);
                                endif;
                            endif;
                        endif;
                        break;
                    case 'apc':
                        if ($this->_aexists):
                            if ($html = apc_fetch($cachekey)):
                                return $this->_stripCacheTags($html);
                            endif;
                        endif;
                endswitch;
            endif;
        endif;
        
        return '';
    }
    
    private function _stripCacheTags($html)
    {
        $html = unserialize($html);
        $html = preg_replace('/<!-- evolved_id-.*<!-- close -->/Us', '', $html);
        
        return $html;
    }
    
    public function userCleanup()
    {
        $cachedir = Mage::getBaseDir();
        if ($cachedir):
            $cachedir .= DS . 'var' . DS . 'evolved' . DS;

            if (is_dir($cachedir)):
                $lifetime = array();
                foreach (Mage::app()->getStores() as $store):
                    $lifetime[] = (int) Mage::getStoreConfig('web/cookie/cookie_lifetime', $store->getCode());
                endforeach;
                $lifetime = max($lifetime) ? max($lifetime) : 86400;
                $users = 'user_*';
                foreach (glob($cachedir . $users) as $file):
                    $time = time();
                    $modified = filemtime($file);
                    if ($time > ($modified + $lifetime)):
                        @unlink($file);
                    endif;
                endforeach;
            endif;
        endif;
    }
    
    private function _clearVarnishEntry($entry)
    {
        Mage::helper('evolvedcaching/varnish')->clearByEntry($entry);
    }
    
    protected function _clearStoreCache($store)
    {
        $collection = Mage::getResourceModel('evolvedcaching/entries_collection');
        $collection->getSelect()->where('storecode = ?', $store);
        
        foreach ($collection as $entry):
            $this->clearCacheEntry($entry->getId());
        endforeach;
    }
}