<?php

/**
 * LINE Pay
 * Block_Form
 *
 * @category    LINE
 * @package     LINEPay
 * @copyright   Copyright (c) 2015 LINEPay
 */
class LINE_LINEPay_Block_Form extends Mage_Payment_Block_Form {
	protected function _construct() {
		parent::_construct();
		$this->setTemplate('linepay/form.phtml');
	}
}