<?php

/**
 * LINE Pay
 * Model_Service_Invoice
 *
 * @category    LINE
 * @package     LINEPay
 * @copyright   Copyright (c) 2015 LINEPay
 */
class LINE_LINEPay_Model_Service_Invoice {
	
	/**
	 * Order 정보로 Invoice를 생성 후 반환
	 * 
	 * @param	Mage_Sales_Model_Order $order
	 * @return	Mage_Sales_Model_Order_Invoice
	 */
	public function create($order) {
		
		return $order->prepareInvoice()->register();
	}
	
}