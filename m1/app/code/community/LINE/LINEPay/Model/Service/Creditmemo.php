<?php

/**
 * LINE Pay
 * Model_Service_Creditmemo
 *
 * @category    LINE
 * @package     LINEPay
 * @copyright   Copyright (c) 2015 LINEPay
 */
class LINE_LINEPay_Model_Service_Creditmemo {

	/**
	 * creditmemo 생성시 필요한 정보를 구성 후 반환
	 *
	 * @param	Mage_Sales_Model_Order $order
	 * @return	array
	 */
	protected function _createData($order) {
		$shippginAmount = $order->getShippingAmount();
		$data = array(
				'shipping_amount' => $shippginAmount,
		);
		$items = array();
		foreach ($order->getAllVisibleItems() as $item) {
			$items[$item->getId()] = array(
					'back_to_stock' => $item->getQtyOrdered()
			);
		}
	
		$data['items'] = $items;
	
		return $data;
	}
	
	/**
	 * Order 정보로 Crediememo 생성 후 반환
	 * 
	 * Creditmemo로 생성된 Order를 취소, 환불 시킬 수 있음 
	 * 
	 * @param	Mage_Sales_Model_Order $order
	 * @return	Mage_Sales_Model_Order_Creditmemo
	 */
	public function create($order, $invoice = null) {
		$helper = Mage::helper('linepay');
		
		$data = $this->_createData($order);
		$savedData = $data['items'];
		
		// 상품수량 복귀정보 구성
		$qtys = array();
		$backToStock = array();
		foreach ($savedData as $orderItemId =>$itemData) {
			if (isset($itemData['qty'])) {
				$qtys[$orderItemId] = $itemData['qty'];
			}
			if (isset($itemData['back_to_stock'])) {
				$backToStock[$orderItemId] = true;
			}
		}
		$data['qtys'] = $qtys;
		
		// creditmemo 생성
		$service = Mage::getModel('sales/service_order', $order);
		if (empty($invoice)) {
			$invoice = $helper->getInvoiceService()->create($order);
		}
		
		$creditmemo = $service->prepareInvoiceCreditmemo($invoice, $data);
		
		// creditmemo에 상품복구정보 저장
		foreach ($creditmemo->getAllItems() as $creditmemoItem) {
			$orderItem = $creditmemoItem->getOrderItem();
			$parentId = $orderItem->getParentItemId();
			if (isset($backToStock[$orderItem->getId()])) {
				$creditmemoItem->setBackToStock(true);
			} elseif ($orderItem->getParentItem() && isset($backToStock[$parentId]) && $backToStock[$parentId]) {
				$creditmemoItem->setBackToStock(true);
			} elseif (empty($savedData)) {
				$creditmemoItem->setBackToStock(Mage::helper('cataloginventory')->isAutoReturnEnabled());
			} else {
				$creditmemoItem->setBackToStock(false);
			}
		}
		
		return $creditmemo;
	}
}