<?php

/**
 * LINE Pay
 * Model_System_Config_Backend_Enabled
 *
 * @category    LINE
 * @package     LINEPay
 * @copyright   Copyright (c) 2015 LINEPay
 */
class LINE_LINEPay_Model_System_Config_Backend_Enabled extends Mage_Core_Model_Config_Data {
	
	const VALUE_ENABLED		= 1;
	const VALUE_DISABLED	= 0;
	
	/**
	 * 관리자페이지 Configuration - Payment Method - LINE Pay 탭 정보를 저장시 호출
	 * 정보를 저장하기 전 유효성 검증 진행
	 * 
	 * @see Mage_Core_Model_Abstract::save()
	 */
    public function save() {
    	$this->validateData();
        
        return parent::save();
    }

    /**
     * 저장 전 기록되어 있는 정보를 바탕으로 LINEPay 플러그인 사용가능여부 판단
     * 
     * 사용할 수 없는 경우
     * 1. Error 메시지 반환
     * 2. 플러그인 비활성화 처리
     * 
     * 판단 정보
     * 1. 사용가능 통화
     * 2. 환경별 채널정보
     */
    protected function validateData() {
    	$data = $this->getFieldsetData();
    	$isEnabled = $this->getValue();
    	if ($isEnabled == self::VALUE_ENABLED) {
    		$helper = Mage::helper('linepay');
    		$errorInfo = array(
    				'flag' => true,
    				'msg' => ''
    		);
    		
    		// channel data
    		if ($data['sandbox_mode']) {
    			$channelId = $data['sandbox_channel_id'];
    			$channelKey = $data['sandbox_channel_secret_key'];
    		}
    		else {
    			$channelId = $data['channel_id'];
    			$channelKey = $data['channel_secret_key'];
    		}
    		
    		$baseCurrencyCode = $helper->getBaseCurrencyCode();    		
    		if (!$helper->validCurrencyCode($baseCurrencyCode)) {
    			$errorInfo['msg'] = $baseCurrencyCode . ': ' .  $helper->__('Unsupported currency.');
    		}
    		else if (empty($channelId) || empty($channelKey)) {
    			$errorInfo['msg'] = $helper->__('The channel info you provided contains an error. Please re-enter it.');
    		}
    		
    		// 정상상태
    		else {
    			$errorInfo['flag'] = false;
    		}
    		
    		// error시 처리
    		if ($errorInfo['flag']) {
    			Mage::getSingleton('core/session')->addError(
    					sprintf('[CAUTION][%s] - %s', LINE_LINEPay_Model_Config::CONFIG_LINEPAY_NAME, $errorInfo['msg']));
    			$this->setValue(self::VALUE_DISABLED);
    		}
    	}

    }
    
}
