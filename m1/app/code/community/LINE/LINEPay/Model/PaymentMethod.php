<?php

/**
 * LINE Pay
 * Model_PaymentMethod
 *
 * @category    LINE
 * @package     LINEPay
 * @copyright   Copyright (c) 2015 LINEPay
 */
class LINE_LINEPay_Model_PaymentMethod extends Mage_Payment_Model_Method_Abstract {
	
	const PAYMENT_METHOD_CODE	= 'linepay';
	
	const TYPE_CAPTURE_RESERVE	= 'capture_reserve';
	const TYPE_CAPTURE_CONFIRM	= 'capture_confirm';
	const TYPE_CAPTURE_CANCEL	= 'capture_cancel';
	const TYPE_CAPTURE_FAILURE	= 'capture_failure';
	const TYPE_REFUDN			= 'refund';
	
	protected $_code			= self::PAYMENT_METHOD_CODE;
	protected $_formBlockType	= 'linepay/form';
	
	protected $_canOrder			= true;
	protected $_canCapture			= true;
	protected $_canRefund			= true;
	protected $_isInitializeNeeded	= true;
	
	/**
	 * Reserve API 호출이 성공시 결제페이지로 이동
	 * 
	 * @return	string
	 */
	public function getOrderPlaceRedirectUrl() {
		
		return Mage::getUrl(LINE_LINEPay_Helper_Data::REDIRECT_URI, array ('_secure' => true));
	}
	
	/**
	 * 
	 * $this->_isInitializeNeeded 변수가 true인 경우 실행됨
	 * 
	 * Mage_Sales_Model_Order_Payment의 place()와
	 * 다른 방식으로 결제를 처리하기 위해 initialize 메소드를 재정의 함
	 * LINE Pay 주문 처리방식은 order메소드에 위임
	 * 
	 * @see	Mage_Payment_Model_Method_Abstract::initialize()
	 * @see	Mage_Sales_Model_Order_Payment::place()
	 * @see	LINE_LINEPay_Model_PaymentMethod::order()
	 * 
	 * @param	string $paymentAction
	 * @param	Varien_Object $stateObject
	 */
	public function initialize($paymentAction, $stateObject) {

		if ($payment = $this->getInfoInstance()) {
			$order = $payment->getOrder();
			$this->setStore($order->getStoreId());
			$this->order($payment, $order->getBaseTotalDue());
		}
		
		$stateObject->setState(Mage_Sales_Model_Order::STATE_PENDING_PAYMENT);
		$stateObject->setStatus('pending');
	}
	
	/**
	 * LINEPay의 결제방식을 정의
	 * 지금은 Authorize-capture 방식만 지원
	 * 
	 * @see Mage_Payment_Model_Method_Abstract::order()
	 * 
	 * @param	Mage_Sales_Model_Order_Payment $payment
	 * @param	number $amount
	 * @return	LINE_LINEPay_Model_PaymentMethod
	 */
	public function order(Varien_Object $payment, $amount) {
		$helper	= Mage::helper('linepay');
		$order	= $payment->getOrder();
		$payment->addTransaction(Mage_Sales_Model_Order_Payment_Transaction::TYPE_ORDER, null, false);
		
		$paymentAction = $helper->getPaymentAction();
		switch ($paymentAction) {
			case self::ACTION_AUTHORIZE_CAPTURE :
				$this->capture($payment, $amount, LINE_LINEPay_Helper_Data::REQUEST_TYPE_RESERVE);
				break;
			
			case self::ACTION_AUTHORIZE :
			default :
				$helper->getLogger()->log('PaymentMethod.order',
					sprintf('[order id: %d][payment action: %s] - %s',
						$order->getIncrementId(), $paymentAction, $helper->__('Unsupported feature.')));
				
				break;
		}
		
		return $this;
	}
	
	/**
	 * 매입 처리
	 * 
	 * LINEPay에서 매입은 두 단계로 이루어진다.
	 * 1. reserve
	 * 2. confirm
	 * 
	 * 요청된 type 정보에 따라 reserve 혹은 confirm을 처리
	 *  
	 * @see Mage_Payment_Model_Method_Abstract::capture()
	 * 
	 * @param	Mage_Sales_Model_Order_Payment $payment 
	 * @param	number $amount
	 * @param	string $type LINE_LINEPay_Helper_Data::REQUEST_TYPE_RESERVE|REQUEST_TYPE_CONFIRM
	 * @param	string $reservedTransactionId
	 * @return	LINE_LINEPay_Model_PaymentMethod
	 */
	public function capture(Varien_Object $payment, $amount, $type = null, $reservedTransactionId = null) {
		$helper		= Mage::helper('linepay');
		
		/*
		 * invoice 생성시 종료
		 * invoice 생성시 내부적으로 capture() 메소드를 호출
		 * - 그 때 $type 인자는 null
		 */ 
		if (empty($type)) {
			
			return $this;
		}
		
		try {
			
			if (!$this->canCapture()) {
				Mage::throwException($helper->__('Capture action is not available.'));
			}

			// reserve, confirm 처리 분기
			switch ($type) {
				case LINE_LINEPay_Helper_Data::REQUEST_TYPE_RESERVE :
					$this->_reserveTransaction($payment);
			
					break;
			
				case LINE_LINEPay_Helper_Data::REQUEST_TYPE_CONFIRM :
					$this->_confirmTransaction($payment, $reservedTransactionId);
			
					break;
				
				default :
					Mage::throwException(sprintf('[request type: %s] - %s', $type, $helper->__('Unsupported transaction request.')));
					
					break;
			}
			
		}
		catch (Exception $e) {
			$helper->getLogger()->log('PaymentMethod.capture',
					sprintf('[order id: %d] - %s', $payment->getOrder()->getIncrementId(), $e->getMessage()));
			
			$errorMsg = $helper->__('Unable to make payment.') . ' ' . $helper->__('Please try again.');
			
			// line exception인 경우, code를 따로 출력
			if ($e instanceof LINE_LINEPay_Model_Exception) {
				$errorMsg = $e->getCodeMsg() . ' ' . $errorMsg;
			}

			Mage::throwException($errorMsg);
		}
		
		return $this;
	}
	
	/**
	 * 환불 처리
	 * 
	 * @see Mage_Payment_Model_Method_Abstract::refund()
	 * 
	 * @param	Mage_Sales_Model_Order_Payment $pament
	 * @param	number $refundAmount
	 * @return	LINE_LINEPay_Model_PaymentMethod
	 */
	public function refund(Varien_Object $payment, $refundAmount) {
		$helper = Mage::helper('linepay');
	
		if (!$this->canRefund()) {
			Mage::throwException($helper->__('Unable to refund order.'));
		}
	
		try {
			$order		= $payment->getOrder();
			$currency	= $order->getBaseCurrencyCode();
			
			// 요청된 금액의 scale 확인
			if (!$helper->validCurrencyScale($currency, $refundAmount)) {
				Mage::throwException(sprintf('[currency: %s][refund amount: %f] - %s',
						$currency, $refundAmount, $helper->__('Invalid decimal places for this currency.')));
			}
			
			// api 요청
			$info = $helper->getAPI()->refund($payment->getLastTransId(), $helper->getStandardized($refundAmount, $currency));
			
			// 환불 transaction id 기록
			$payment->setTransactionId($info->refundTransactionId . '');
			$payment->setShouldCloseParentTransaction(false);

			return $this;
		}
		catch (Exception $e) {
			$helper->getLogger()->log('PaymentMethod.refund',
					sprintf('[order id: %d] - %s', $payment->getOrder()->getIncrementId(), $e->getMessage()));
			
			$errorMsg = $helper->__('Unable to process refund.') . ' ' . $helper->__('Please try again.');
			
			// line exception인 경우, code를 따로 출력
			if ($e instanceof LINE_LINEPay_Model_Exception) {
				$errorMsg = $e->getCodeMsg() . ' ' . $errorMsg;
			}

			Mage::throwException($errorMsg);
		}
		
	}
	
	/**
	 * 주문 취소
	 * 
	 * @see		Mage_Payment_Model_Method_Abstract::cancel()
	 * @param	Mage_Sales_Model_Order_Payment $pament
	 * @return	LINE_LINEPay_Model_PaymentMethod
	 */
	public function cancel($payment) {
		$helper = Mage::helper('linepay');
		
		try {
			$transactionSave = Mage::getModel('core/resource_transaction');
			$order = $payment->getOrder();
			
			// cart 복귀
			$quote = $helper->restoreCart($order->getQuoteId());
			
			// order 취소 처리
			$order->cancel();
			
			$transactionSave
			->addObject($quote)
			->addObject($order);
			
			$transactionSave->save();

			return $this;
		}

		catch (Exception $e) {
			$helper->getLogger()->log('PaymentMethod.cancel',
					$helper->__('Unable to cancel order. Please try again.') . 
					$helper->addNewLine($e->getMessage()));
		}
	}
	
	protected function _enforceRefund($payment, $transactionId) {
		$helper = Mage::helper('linepay');
		
		try {
			// 강제 전체환불 진행
			$info = $helper->getAPI()->refund($transactionId, 0, true);
		
			// order cancel 처리
			$this->cancel($payment);
		
			return true;
		}
		catch (Exception $e) {
		
			$helper->getLogger()->log('PaymentMethod._enforceRefund',
					sprintf('[order id: %d] - %s', $payment->getOrder()->getIncrementId(), $e->getMessage()));
		
			return false;
		}
		
	}
	
	/**
	 * Reserve API 호출 후 결과 반환
	 * 
	 * 호출 성공시
	 * 1. Transaction 기록
	 * 
	 * @param	Mage_Sales_Model_Order_Payment $pament
	 * @return	LINE_LINEPay_Model_PaymentMethod
	 */
	protected function _reserveTransaction($payment) {
		$helper		= Mage::helper('linepay');
		$order		= $payment->getOrder();
		$currency	= $order->getBaseCurrencyCode();
		$amount		= $order->getBaseTotalDue();
		
		// 스케일 검증
		if (!$helper->validCurrencyScale($currency, $amount)) {
			Mage::throwException($helper->addNewLine(sprintf('[_reserveTransaction][currency: %s][amount: %f] - %s',
					$currency, $amount, $helper->__('Invalid decimal places for this currency.'))));
		}
		
		// reserve 요청
		$info = $helper->getAPI()->reserve($order);
		
		// transaction 정보 변경
		$payment->setTransactionId($info->transactionId . '');
		$payment->setIsTransactionClosed(false);
		$payment->addTransaction(
				Mage_Sales_Model_Order_Payment_Transaction::TYPE_CAPTURE, null, false,
				'[' . self::PAYMENT_METHOD_CODE . '] ' . self::TYPE_CAPTURE_RESERVE);
		
		$order->setLinepayRedirectPaymentUrl($info->paymentUrl->web);
		
		return $this;
		
	}
	

	/**
	 * Confirm API 호출 후 결과 반환
	 * 
	 * 호출 성공시
	 * 1. Transaction 정보 갱신
	 * 2. Invoice 생성 후 저장
	 * 
	 * 호출 전
	 *  - 요청된 금액과 DB 금액을 비교
	 *  - 다를경우 예외 발생
	 * 
	 * 호출 후
	 *  - 요청된 금액과 호출결과 금액을 비교
	 *  - 다를경우 환불 진행, 예외 발생
	 * 
	 * @param	Mage_Sales_Model_Order_Payment $pament
	 * @param	string $reservedTransactionId
	 * @return	LINE_LINEPay_Model_PaymentMethod
	 */
	protected function _confirmTransaction($payment, $reservedTransactionId) {		
		$helper		= Mage::helper('linepay');
		$order		= $payment->getOrder();
		$currency	= $order->getBaseCurrencyCode();
		
		$invoice	= null;
		$info		= null;
		$success	= false;
		
		// 청구서를 만들 수 없는 경우 종료
		if (!$order->canInvoice()) {
			// order cancel 처리
			$this->cancel($payment);
			
			Mage::throwException($helper->__('Unable to create bill.'));
		}
		
		// DB에서 order 재검색
		$newOrder				= $helper->getSalesOrder($order->getIncrementId());
		$stdReservedAmountInDB	= $helper->getStandardized($newOrder->getBaseGrandTotal(), $currency);
		$stdRequestAmount		= $helper->getStandardized($order->getBaseTotalDue(), $currency);
		
		// 가격검증1. DB에 저장된 금액과 요청금액 검증
		if ($stdRequestAmount !== $stdReservedAmountInDB) {
			// order cancel 처리
 			$this->cancel($payment);
				
			Mage::throwException($helper->addNewLine(sprintf('[_confirmTransaction][request amount: %s][reserved amount: %s] - %s %s',
					$stdRequestAmount, $stdReservedAmountInDB,
					$helper->__('Requested amount does not match refund amount.'), $helper->__('Payment canceled.'))));
		}
		
		// confirm 요청
		try {
			$info = $helper->getAPI()->confirm($order, $reservedTransactionId);
		}
		catch (Exception $e) {
			// order cancel 처리
			$this->cancel($payment);
			
			throw $e;
		}
		
		$confirmedTransactionId = $info->transactionId;
		
		// confirm 결과 저장
		try {
			$transactionSave = Mage::getModel('core/resource_transaction');
			
			// transaction 정보 변경
			$payment->setTransactionId($confirmedTransactionId . '');
			$payment->setParentTransactionId($reservedTransactionId . '');
			
			$transaction = $payment->addTransaction(
					Mage_Sales_Model_Order_Payment_Transaction::TYPE_CAPTURE, null, true,
					'[' . self::PAYMENT_METHOD_CODE . '] ' . self::TYPE_CAPTURE_CONFIRM);
			
			// invoice 생성
			$invoice = $helper->getInvoiceService()->create($order);
			$invoice->setTransactionId($confirmedTransactionId);
			$invoice->capture();
			
			$order->addRelatedObject($invoice);
			$transactionSave
			->addObject($transaction)
			->addObject($invoice)
			->addObject($order);
			
			$transactionSave->save();
			$success = true;
		}
		catch (Exception $e) {
			
			Mage::throwException(
					$helper->addNewLine(sprintf('[_confirmTransaction][confirmed tnx id: %s] - %s',
						$confirmedTransactionId, $helper->__('Unable to make payment due to a temporary error. Please try again.'))) .
					$helper->addNewLine($e->getMessage()));
		}
		
		// confirmed amount
		$stdConfirmedAmount	= $helper->getStandardized($info->payInfo[0]->amount, $currency);
		
		// 가격검증2. confirm transaction 성공, confirm amount 오류시 환불 진행
		if ($success && ($stdRequestAmount !== $stdConfirmedAmount)) {
			$errorMsg = '';
			
			try {
				$this->_refundTransaction($order, $invoice);
				$errorMsg = $helper->__('Refund processed.');
			}
			catch (Exception $e) {
				$errorMsg = $helper->__('This order needs to be canceled.') . $e->getMessage();
			}
			
			Mage::throwException($helper->addNewLine(sprintf('[_confirmTransaction][request amount: %s][confirmed amount: %s] - %s %s',
				$stdRequestAmount, $stdConfirmedAmount, $helper->__('Requested amount does not match refund amount.'), $errorMsg)));
			
		}		
		
		return $this;
	}
	
	/**
	 * Creditmemo 생성 후 환불 처리
	 * 
	 * @param	Mage_Sales_Model_Order $order
	 * @param	Mage_Sales_Model_Order_Invoice
	 * @param	string $comment
	 * @return	boolean
	 */
	protected function _refundTransaction($order, $invoice = null, $comment = '') {
		$helper = Mage::helper('linepay');
		
		try {
			$transactionSave = Mage::getModel('core/resource_transaction');
			
			// creditmemo 생성
			$creditmemo = $helper->getCreditmemoService()->create($order, $invoice);
			
			if (($creditmemo->getGrandTotal() <= 0) && (!$creditmemo->getAllowZeroGrandTotal())) {
				$helper->getLogger()->log('PaymentMethod._refundTransaction', 'Credit memo\'s total must be positive.');
				
				return false;
			}
			
			// 환불 처리
			$creditmemo->addComment($comment);
			$creditmemo->setRefundRequested(true);
			$creditmemo->register();
			
			// cart 복귀
			$quote = $helper->restoreCart($order->getQuoteId());
			
			if ($creditmemo->getInvoice()) {
				$transactionSave->addObject($creditmemo->getInvoice());
			}
			
			$transactionSave
			->addObject($creditmemo)
			->addObject($creditmemo->getOrder())
			->addObject($quote);
			
			$transactionSave->save();
			
			return true;
		}
		catch (Exception $e) {
			
			Mage::throwException($helper->addNewLine(sprintf('[_refundTransaction][order id: %s] - %s', $order->getIncrementId(), $e->getMessage())));
		}
	
	}
	
}