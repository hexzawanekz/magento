<?php

$installer = $this;
$installer->startSetup();
$installer->run("
	ALTER TABLE `{$installer->getTable('sales/order')}` ADD `linepay_redirect_payment_url` VARCHAR( 255 );
");
$installer->endSetup();