<?php
/**
 * @category    Bubble
 * @package     Bubble_Elasticsearch
 * @version     3.1.3
 * @copyright   Copyright (c) 2015 BubbleShop (https://www.bubbleshop.net)
 */
abstract class Bubble_Elasticsearch_Helper_Indexer_Abstract extends Bubble_Elasticsearch_Helper_Data
{
    /**
     * @var string Date format
     * @link https://www.elastic.co/guide/en/elasticsearch/reference/current/mapping-date-format.html
     */
    protected $_dateFormat = 'date';

    /**
     * Export data according to optional filters
     *
     * @param array $filters
     * @return array
     */
    abstract public function export($filters = array());

    /**
     * Builds store index properties
     *
     * @param null $store
     * @return mixed
     */
    abstract public function getStoreIndexProperties($store = null);

    /**
     * Returns potential additional fields to add to Elasticsearch query
     *
     * @return array
     */
    public function getAdditionalFields()
    {
        return array();
    }

    /**
     * Returns attribute properties for indexation
     *
     * @param Mage_Eav_Model_Entity_Attribute $attribute
     * @param mixed $store
     * @return array
     */
    public function getAttributeProperties(Mage_Eav_Model_Entity_Attribute $attribute, $store = null)
    {
        $indexSettings = $this->getStoreIndexSettings($store);
        $type = $this->getAttributeType($attribute);
        $weight = $attribute->getSearchWeight();

        if ($type === 'option') {
            // Define field for option label
            $properties = array(
                'type' => 'string',
                'analyzer' => 'std',
                'index_options' => 'docs', // do not use tf/idf for options
                'norms' => array('enabled' => false), // useless for options
                'searchable' => (bool) $attribute->getIsSearchable(), // for internal use
                'weight' => $weight > 0 ? intval($weight) : 1, // boost at query time
                'fields' => array(
                    'std' => array(
                        'type' => 'string',
                        'analyzer' => 'std',
                        'index_options' => 'docs', // do not use tf/idf for options
                        'norms' => array('enabled' => false), // useless for options
                    ),
                ),
            );
            if (isset($indexSettings['analysis']['analyzer']['language'])) {
                $properties['analyzer'] = 'language';
            }
        } elseif ($type !== 'string') {
            $properties = array(
                'type' => $type,
                'searchable' => (bool) $attribute->getIsSearchable(), // for internal use
                'search_analyzer' => 'std',
            );
            if ($type === 'integer') {
                $properties['ignore_malformed'] = true;
            }
        } else {
            $properties = array(
                'type' => 'string',
                'analyzer' => 'std',
                'searchable' => (bool) $attribute->getIsSearchable(), // for internal use
                'weight' => $weight > 0 ? intval($weight) : 1, // boost at query time
                'fields' => array(
                    'std' => array(
                        'type' => 'string',
                        'analyzer' => 'std',
                    ),
                ),
            );
            if ($attribute->getBackendType() != 'text') {
                $properties['fields'] = array_merge($properties['fields'], array(
                    'prefix' => array(
                        'type' => 'string',
                        'analyzer' => 'text_prefix',
                        'search_analyzer' => 'std',
                    ),
                    'suffix' => array(
                        'type' => 'string',
                        'analyzer' => 'text_suffix',
                        'search_analyzer' => 'std',
                    ),
                ));
            }
            if (isset($indexSettings['analysis']['analyzer']['language'])) {
                $properties['analyzer'] = 'language';
            }
        }

        return $properties;
    }

    /**
     * Returns attribute type for indexation
     *
     * @param Mage_Eav_Model_Entity_Attribute $attribute
     * @return string
     */
    public function getAttributeType(Mage_Eav_Model_Entity_Attribute $attribute)
    {
        $type = 'string';
        if ($attribute->getBackendType() == 'decimal') {
            $type = 'double';
        } elseif ($attribute->getSourceModel() == 'eav/entity_attribute_source_boolean') {
            $type = 'boolean';
        } elseif ($attribute->getBackendType() == 'datetime') {
            $type = 'date';
        } elseif ($this->isAttributeUsingOptions($attribute)) {
            $type = 'option'; // custom type
        } elseif ($attribute->usesSource() && $attribute->getBackendType() == 'int'
            || $attribute->getFrontendClass() == 'validate-digits')
        {
            $type = 'integer';
        }

        return $type;
    }

    /**
     * Retrieves store search fields of specific analyzer if specified
     *
     * @param mixed $store
     * @param mixed $analyzer
     * @param bool $withBoost
     * @return array
     */
    public function getStoreSearchFields($store = null, $analyzer = false, $withBoost = true)
    {
        $properties = $this->getStoreIndexProperties($store);
        $fields = array();
        foreach ($properties as $fieldName => $property) {
            // If field is not searchable, ignore it
            if (!isset($property['searchable']) || !$property['searchable']) {
                continue;
            }

            $weight = 1;
            if ($withBoost && isset($property['weight'])) {
                $weight = intval($property['weight']);
            }

            if (!$analyzer || (isset($property['analyzer']) && $property['analyzer'] == $analyzer)) {
                $fields[] = $fieldName . ($weight > 1 ? '^' . $weight : '');
            }

            if (isset($property['fields'])) {
                foreach ($property['fields'] as $key => $field) {
                    if (!$analyzer || (isset($field['analyzer']) && $field['analyzer'] == $analyzer)) {
                        $fields[] = $fieldName . '.' . $key . ($weight > 1 ? '^' . $weight : '');
                    }
                }
            }
        }

        $fields = new Varien_Object($fields);

        Mage::dispatchEvent('bubble_elasticsearch_search_fields', array(
            'indexer' => $this,
            'store' => Mage::app()->getStore($store),
            'analyzer' => $analyzer,
            'fields' => $fields,
            'with_boost' => $withBoost,
        ));

        return $fields->getData();
    }
}