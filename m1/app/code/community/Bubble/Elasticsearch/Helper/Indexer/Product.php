<?php
/**
 * @category    Bubble
 * @package     Bubble_Elasticsearch
 * @version     3.1.3
 * @copyright   Copyright (c) 2015 BubbleShop (https://www.bubbleshop.net)
 */
class Bubble_Elasticsearch_Helper_Indexer_Product extends Bubble_Elasticsearch_Helper_Indexer_Abstract
{
    /**
     * Searchable attributes
     *
     * @var array
     */
    protected $_searchableAttributes;

    /**
     * Export products according to optional filters
     *
     * @param array $filters
     * @param int $split
     * @return array
     */
    public function export($filters = array(), $split = 2000)
    {
        set_time_limit(0); // export might be a bit slow
        @ini_set('memory_limit', '-1'); // trying to have enough memory

        $result             = array();
        $product            = Mage::getModel('catalog/product');
        $attributesByTable  = $product->getResource()->loadAllAttributes($product)->getAttributesByTable();
        $mainTable          = $product->getResource()->getTable('catalog_product_entity');
        $resource           = $this->_getResource();
        $adapter            = $this->_getAdapter();

        foreach (Mage::app()->getStores() as $store) {
            /** @var Mage_Core_Model_Store $store */
            if (!$store->getIsActive()) {
                continue;
            }

            $storeId = (int) $store->getId();
            $categoryNames = $this->getCategoryNames($store);

            if (isset($filters['store_id'])) {
                if (!is_array($filters['store_id'])) {
                    $filters['store_id'] = array($filters['store_id']);
                }
                if (!in_array($storeId, $filters['store_id'])) {
                    continue;
                }
            }

            $this->handleMessage(' > Exporting products of store %s', $store->getCode());

            $result[$storeId] = array();
            $select = $adapter->select()->from(array('e' => $mainTable), 'entity_id');

            // Filter products that are enabled for current store website
            $select->join(
                array('product_website' => $resource->getTableName('catalog/product_website')),
                'product_website.product_id = e.entity_id AND ' . $adapter->quoteInto('product_website.website_id = ?', $store->getWebsiteId()),
                array()
            );

            // Index only in stock products if showing out of stock products is not needed
            if (!$this->isIndexOutOfStockProducts($store)) {
                $manageStock = $store->getConfig(Mage_CatalogInventory_Model_Stock_Item::XML_PATH_MANAGE_STOCK);
                $condArr = array(
                    'stock.use_config_manage_stock = 0 AND stock.manage_stock = 1 AND stock.is_in_stock = 1',
                    'stock.use_config_manage_stock = 0 AND stock.manage_stock = 0',
                );
                if ($manageStock) {
                    $condArr[] = 'stock.use_config_manage_stock = 1 AND stock.is_in_stock = 1';
                } else {
                    $condArr[] = 'stock.use_config_manage_stock = 1';
                }
                $cond = '((' . implode(') OR (', $condArr) . '))';
                $select->joinLeft(
                    array('stock' => $resource->getTableName('cataloginventory_stock_item')),
                    '(stock.product_id = e.entity_id) AND ' . $cond,
                    array()
                );
            }

            if (!empty($filters)) {
                foreach ($filters as $field => $value) {
                    if ($field == 'store_id' || $value === null) {
                        continue;
                    }
                    if (is_array($value)) {
                        $select->where("e.$field IN (?)", $value);
                    } else {
                        $select->where("e.$field = ?", $value);
                    }
                }
            }

            // Handle enabled products
            $attributeId = Mage::getSingleton('eav/entity_attribute')
                ->getIdByCode(Mage_Catalog_Model_Product::ENTITY, 'status');
            if ($attributeId) {
                $enabled = Mage_Catalog_Model_Product_Status::STATUS_ENABLED;
                $select->join(
                    array('status' => $resource->getTableName('catalog_product_entity_int')),
                    "status.attribute_id = $attributeId AND status.entity_id = e.entity_id",
                    array()
                );
                $select->where('status.value = ?', $enabled);
                $select->where('status.store_id IN (?)', array(0, $storeId));
            }

            // Fetch entity ids that match
            $allEntityIds = $adapter->fetchCol($select);
            $allEntityIds = array_unique($allEntityIds);
            $this->handleMessage(' > Found %d products', count($allEntityIds));

            $allEntityIds = array_chunk($allEntityIds, $split);
            $countChunks = count($allEntityIds);
            if ($countChunks > 1) {
                $this->handleMessage(' > Split products array into %d chunks for better performances', $split);
            }
            $attrOptionLabels = array();

            // Loop through products
            foreach ($allEntityIds as $i => $entityIds) {
                if ($countChunks > 1) {
                    $this->handleMessage(' > %d/%d', $i + 1, $countChunks);
                }
                $products = array();
                foreach ($attributesByTable as $table => $allAttributes) {
                    $allAttributes = array_chunk($allAttributes, 25);
                    foreach ($allAttributes as $attributes) {
                        $select = $adapter->select()
                            ->from(array('e' => $mainTable), array('id' => 'entity_id', 'sku'));

                        foreach ($attributes as $attribute) {
                            if (!$this->isAttributeIndexable($attribute)) {
                                continue;
                            }
                            $attributeId = $attribute->getAttributeId();
                            $attributeCode = $attribute->getAttributeCode();

                            if (!isset($attrOptionLabels[$attributeCode]) && $this->isAttributeUsingOptions($attribute)) {
                                $options = $attribute->setStoreId($storeId)
                                    ->getSource()
                                    ->getAllOptions();
                                foreach ($options as $option) {
                                    if (!$option['value']) {
                                        continue;
                                    }
                                    $attrOptionLabels[$attributeCode][$option['value']] = $option['label'];
                                }
                            }
                            $alias1 = $attributeCode . '_default';
                            $select->joinLeft(
                                array($alias1 => $adapter->getTableName($table)),
                                "$alias1.attribute_id = $attributeId AND $alias1.entity_id = e.entity_id AND $alias1.store_id = 0",
                                array()
                            );
                            $alias2 = $attributeCode . '_store';
                            $valueExpr = $adapter->getCheckSql("$alias2.value IS NULL", "$alias1.value", "$alias2.value");
                            $select->joinLeft(
                                array($alias2 => $adapter->getTableName($table)),
                                "$alias2.attribute_id = $attributeId AND $alias2.entity_id = e.entity_id AND $alias2.store_id = {$store->getId()}",
                                array($attributeCode => $valueExpr)
                            );
                        }

                        $select->where('e.entity_id IN (?)', $entityIds);
                        $query = $adapter->query($select);

                        while ($row = $query->fetch()) {
                            $row = array_filter($row, 'strlen');
                            $row['id'] = (int) $row['id'];
                            $productId = $row['id'];
                            if (!isset($products[$productId])) {
                                $products[$productId] = array();
                            }
                            foreach ($row as $code => &$value) {
                                if (isset($attributesByTable[$table][$code])) {
                                    $value = $this->_formatValue($attributesByTable[$table][$code], $value);
                                }
                                if (isset($attrOptionLabels[$code])) {
                                    if (is_array($value)) {
                                        $label = array();
                                        foreach ($value as $val) {
                                            if (isset($attrOptionLabels[$code][$val])) {
                                                $label[] = $attrOptionLabels[$code][$val];
                                            }
                                        }
                                        if (!empty($label)) {
                                            $row[$code] = $label;
                                        }
                                    } elseif (isset($attrOptionLabels[$code][$value])) {
                                        $row[$code] = $attrOptionLabels[$code][$value];
                                    }
                                }
                            }
                            unset($value);
                            $products[$productId] = array_merge($products[$productId], $row);
                        }
                    }
                }

                // Add parent products in order to retrieve products that have associated products
                $key = 'parent_ids';
                $select = $adapter->select()
                    ->from($resource->getTableName('catalog_product_relation'),
                        array('parent_id', 'child_id'))
                    ->where('child_id IN (?)', $entityIds);
                $query = $adapter->query($select);
                while ($row = $query->fetch()) {
                    $productId = $row['child_id'];
                    if (!isset($products[$productId][$key])) {
                        $products[$productId][$key] = array();
                    }
                    $products[$productId][$key][] = (int) $row['parent_id'];
                }

                // Add categories
                $columns = array(
                    'product_id'    => 'product_id',
                    'category_ids'  => new Zend_Db_Expr(
                        "TRIM(
                            BOTH ',' FROM CONCAT(
                                TRIM(BOTH ',' FROM GROUP_CONCAT(IF(is_parent = 0, category_id, '') SEPARATOR ',')),
                                ',',
                                TRIM(BOTH ',' FROM GROUP_CONCAT(IF(is_parent = 1, category_id, '') SEPARATOR ','))
                            )
                        )"),
                );
                $select = $adapter->select()
                    ->from(array($resource->getTableName('catalog_category_product_index')), $columns)
                    ->where('product_id IN (?)', $entityIds)
                    ->where('store_id = ?', $storeId)
                    ->where('category_id > 1') // ignore global root category
                    ->where('category_id != ?', $store->getRootCategoryId()) // ignore store root category
                    ->group('product_id');
                $query = $adapter->query($select);
                while ($row = $query->fetch()) {
                    $categoryIds = explode(',', $row['category_ids']);
                    if (empty($categoryIds)) {
                        continue;
                    }
                    $productId = $row['product_id'];
                    if (!isset($products[$productId]['categories'])) {
                        $products[$productId]['categories'] = array();
                    }
                    foreach ($categoryIds as $categoryId) {
                        if (isset($categoryNames[$categoryId])) {
                            $products[$productId]['categories'][] = $categoryNames[$categoryId];
                        }
                    }
                    $products[$productId]['categories'] = array_values(array_unique($products[$productId]['categories']));
                }

                if (!empty($products)) {
                    $result[$storeId] = array_merge($result[$storeId], $products);
                }
            }

            $this->handleMessage(' > Products exported');
        }

        return $result;
    }

    /**
     * Returns additional fields to add to Elasticsearch query
     *
     * @return array
     */
    public function getAdditionalFields()
    {
        return array('parent_ids'); // product id is already implicitly included
    }

    /**
     * Retrieve store category names mapping
     *
     * @param null $store
     * @return array
     */
    public function getCategoryNames($store = null)
    {
        $store = Mage::app()->getStore($store);
        $adapter = $this->_getAdapter();
        $attributeId = Mage::getSingleton('eav/entity_attribute')
            ->getIdByCode(Mage_Catalog_Model_Category::ENTITY, 'name');
        $select = $adapter->select()
            ->from($this->_getResource()->getTableName('catalog_category_entity_varchar'), array('entity_id', 'value'))
            ->where('attribute_id = ?', $attributeId) // only category name attribute values
            ->where('store_id IN (?)', array(0, $store->getId())) // use default value if not overriden in store view scope
            ->order(array('entity_id ASC', 'store_id ASC')); // used to handle store view overrides

        return $adapter->fetchPairs($select);
    }

    /**
     * Retrieves all searchable product attributes
     * Possibility to filter attributes by backend type
     *
     * @param array $backendType
     * @return array
     */
    public function getSearchableAttributes($backendType = null)
    {
        if (null === $this->_searchableAttributes) {
            $this->_searchableAttributes = array();
            $entityType = $this->getEavConfig()->getEntityType('catalog_product');
            $entity = $entityType->getEntity();

            /* @var Mage_Catalog_Model_Resource_Product_Attribute_Collection $productAttributeCollection */
            $productAttributeCollection = Mage::getResourceModel('catalog/product_attribute_collection')
                ->setEntityTypeFilter($entityType->getEntityTypeId())
                ->addVisibleFilter()
                ->addToIndexFilter(true);

            $attributes = $productAttributeCollection->getItems();
            foreach ($attributes as $attribute) {
                /** @var Mage_Catalog_Model_Resource_Eav_Attribute $attribute */
                $attribute->setEntity($entity);
                $this->_searchableAttributes[$attribute->getAttributeCode()] = $attribute;
            }
        }

        if (null !== $backendType) {
            $backendType = (array) $backendType;
            $attributes = array();
            foreach ($this->_searchableAttributes as $attribute) {
                /** @var Mage_Catalog_Model_Resource_Eav_Attribute $attribute */
                if (in_array($attribute->getBackendType(), $backendType)) {
                    $attributes[$attribute->getAttributeCode()] = $attribute;
                }
            }

            return $attributes;
        }

        return $this->_searchableAttributes;
    }

    /**
     * Builds store index properties for indexation
     *
     * @param mixed $store
     * @return array
     */
    public function getStoreIndexProperties($store = null)
    {
        $store = Mage::app()->getStore($store);
        $cacheId = 'elasticsearch_product_index_properties_' . $store->getId();
        if (Mage::app()->useCache('config')) {
            $properties = Mage::app()->loadCache($cacheId);
            if ($properties) {
                return unserialize($properties);
            }
        }

        $properties = array();
        $indexSettings = $this->getStoreIndexSettings($store);

        $attributes = $this->getSearchableAttributes(array('varchar', 'int'));
        foreach ($attributes as $attribute) {
            /** @var Mage_Catalog_Model_Resource_Eav_Attribute $attribute */
            if ($this->isAttributeIndexable($attribute)) {
                $key = $attribute->getAttributeCode();
                $properties[$key] = $this->getAttributeProperties($attribute, $store);
            }
        }

        $attributes = $this->getSearchableAttributes('text');
        foreach ($attributes as $attribute) {
            /** @var Mage_Catalog_Model_Resource_Eav_Attribute $attribute */
            $key = $attribute->getAttributeCode();
            $properties[$key] = $this->getAttributeProperties($attribute, $store);
        }

        $attributes = $this->getSearchableAttributes(array('static', 'varchar', 'decimal', 'datetime'));
        foreach ($attributes as $attribute) {
            /** @var Mage_Catalog_Model_Resource_Eav_Attribute $attribute */
            $key = $attribute->getAttributeCode();
            if ($this->isAttributeIndexable($attribute) && !isset($properties[$key])) {
                $type = $this->getAttributeType($attribute);
                if ($type === 'option') {
                    continue;
                }
                $weight = $attribute->getSearchWeight();
                $properties[$key] = array(
                    'type' => $type,
                    'searchable' => (bool) $attribute->getIsSearchable(), // for internal use
                    'weight' => $weight > 0 ? intval($weight) : 1, // boost at query time
                );
                if ($key == 'sku') {
                    $properties[$key]['fields'] = array(
                        'keyword' => array(
                            'type' => 'string',
                            'analyzer' => 'keyword',
                        ),
                        'prefix' => array(
                            'type' => 'string',
                            'analyzer' => 'keyword_prefix',
                            'search_analyzer' => 'keyword',
                        ),
                    );
                }
                if ($key == 'price') {
                    $properties[$key]['fields'] = array(
                        'keyword' => array(
                            'type' => 'string',
                            'analyzer' => 'keyword',
                        ),
                    );
                }
                if ($attribute->getBackendType() == 'datetime') {
                    $properties[$key]['format'] = $this->_dateFormat;
                    $properties[$key]['ignore_malformed'] = true;
                }
            }
        }

        // Add categories field
        $properties['categories'] = array(
            'type' => 'string',
            'searchable' => true,
            'analyzer' => 'std',
            'position_offset_gap' => 100, // separate each phrase positions by 100
        );
        if (isset($indexSettings['analysis']['analyzer']['language'])) {
            $properties['categories']['analyzer'] = 'language';
        }

        // Add parent_ids field
        $properties['parent_ids'] = array(
            'type' => 'integer',
            'store' => true,
            'index' => 'no',
        );

        $properties = new Varien_Object($properties);

        Mage::dispatchEvent('bubble_elasticsearch_index_properties', array(
            'indexer' => $this,
            'store' => $store,
            'properties' => $properties,
        ));

        $properties = $properties->getData();

        if (Mage::app()->useCache('config')) {
            $lifetime = $this->getCacheLifetime();
            Mage::app()->saveCache(serialize($properties), $cacheId, array('config'), $lifetime);
        }

        return $properties;
    }

    /**
     * @param null $store
     * @return bool
     */
    public function isIndexOutOfStockProducts($store = null)
    {
        return Mage::getStoreConfigFlag(Mage_CatalogInventory_Helper_Data::XML_PATH_SHOW_OUT_OF_STOCK, $store);
    }
}