<?php
/**
 * Search indexer override
 *
 * @category    Bubble
 * @package     Bubble_Elasticsearch
 * @version     3.1.3
 * @copyright   Copyright (c) 2015 BubbleShop (https://www.bubbleshop.net)
 */
class Bubble_Elasticsearch_Model_Indexer_Fulltext extends Mage_CatalogSearch_Model_Indexer_Fulltext
{
    /**
     * @var Bubble_Elasticsearch_Helper_Data
     */
    protected $_helper;

    /**
     * @var Bubble_Elasticsearch_Model_Resource_Engine
     */
    protected $_engine;

    /**
     * Indexer initialization
     */
    protected function _construct()
    {
        $this->_helper = Mage::helper('elasticsearch');
        $this->_engine = Mage::helper('catalogsearch')->getEngine();
    }

    /**
     * @return bool
     */
    public function isActiveEngine()
    {
        return $this->_helper->isActiveEngine();
    }

    /**
     * @return string
     */
    public function getName()
    {
        if ($this->_helper->isElasticsearchEnabled()) {
            return $this->_helper->__('Elasticsearch Product');
        }

        return parent::getName();
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        if ($this->_helper->isElasticsearchEnabled()) {
            return $this->_helper->__('Rebuild product fulltext search index');
        }

        return parent::getDescription();
    }

    /**
     * Rebuild all index data
     *
     * @return void
     */
    public function reindexAll()
    {
        if (!$this->isActiveEngine()) {
            parent::reindexAll();
        } else {
            try {
                foreach ($this->_helper->getStoreTypes() as $type) {
                    $this->_engine->rebuildIndex(null, null, $type);
                }
            } catch (\Elastica\Exception\ResponseException $e) {
                $this->_helper->handleError($e->getMessage());
                throw $e;
            }
        }
    }
}