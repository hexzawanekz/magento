<?php
/**
 * Search indexer override
 *
 * @category    Bubble
 * @package     Bubble_Elasticsearch
 * @version     3.1.3
 * @copyright   Copyright (c) 2015 BubbleShop (https://www.bubbleshop.net)
 */
class Bubble_Elasticsearch_Model_Indexer_Category extends Mage_Index_Model_Indexer_Abstract
{
    const EVENT_MATCH_RESULT_KEY = 'catalog_category_match_result';

    /**
     * @var Bubble_Elasticsearch_Helper_Data
     */
    protected $_helper;

    /**
     * Initialize indexer
     */
    protected function _construct()
    {
        $this->_helper = Mage::helper('elasticsearch');
    }

    /**
     * Indexer must match entities
     *
     * @var array
     */
    protected $_matchedEntities = array(
        Mage_Catalog_Model_Category::ENTITY => array(
            Mage_Index_Model_Event::TYPE_SAVE,
            Mage_Index_Model_Event::TYPE_DELETE,
        )
    );

    /**
     * Retrieve indexer name
     *
     * @return string
     */
    public function getName()
    {
        return $this->_helper->__('Elasticsearch Category');
    }

    /**
     * Retrieve indexer description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->_helper->__('Rebuild category fulltext search index');
    }

    /**
     * Reindex all category in Elasticsearch
     *
     * @throws Exception
     */
    public function reindexAll()
    {
        if (!$this->_helper->isActiveEngine()) {
            Mage::throwException($this->_helper->__('Elasticsearch server is not reachable'));
        }

        $this->_getEngine()->rebuildIndex(null, null, 'category');
    }

    /**
     * Retrieve indexer instance
     *
     * @return Bubble_Elasticsearch_Model_Resource_Engine
     */
    protected function _getEngine()
    {
        return Mage::helper('catalogsearch')->getEngine();
    }

    /**
     * Register data required by process in event object
     *
     * @param Mage_Index_Model_Event $event
     */
    protected function _registerEvent(Mage_Index_Model_Event $event)
    {
        $event->addNewData(self::EVENT_MATCH_RESULT_KEY, true);
        switch ($event->getEntity()) {
            case Mage_Catalog_Model_Category::ENTITY:
                $this->_registerCatalogCategoryEvent($event);
                break;
        }
    }

    /**
     * Get data required for category reindex
     *
     * @param Mage_Index_Model_Event $event
     * @return $this
     */
    protected function _registerCatalogCategoryEvent(Mage_Index_Model_Event $event)
    {
        /* @var Mage_Catalog_Model_Category $category */
        $category = $event->getDataObject();
        switch ($event->getType()) {
            case Mage_Index_Model_Event::TYPE_SAVE:
                $event->addNewData('elasticsearch_update_category', $category->getId());
                break;
            case Mage_Index_Model_Event::TYPE_DELETE:
                $event->addNewData('elasticsearch_delete_category', $category->getId());
                break;
        }

        return $this;
    }

    /**
     * Process event
     *
     * @param Mage_Index_Model_Event $event
     */
    protected function _processEvent(Mage_Index_Model_Event $event)
    {
        if ($this->_helper->isActiveEngine()) {
            $data = $event->getNewData();

            if (!empty($data['elasticsearch_update_category'])) {
                $this->_getEngine()->rebuildIndex(null, array($data['elasticsearch_update_category']), 'category');
            }

            if (!empty($data['elasticsearch_delete_category'])) {
                $this->_getEngine()->cleanIndex(null, array($data['elasticsearch_delete_category']), 'category');
            }
        }
    }

    /**
     * @return bool
     */
    public function isVisible()
    {
        return $this->_helper->isElasticsearchEnabled();
    }
}