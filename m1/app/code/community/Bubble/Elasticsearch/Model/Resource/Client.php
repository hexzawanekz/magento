<?php
/**
 * Elasticsearch client
 *
 * @category    Bubble
 * @package     Bubble_Elasticsearch
 * @version     3.1.3
 * @copyright   Copyright (c) 2015 BubbleShop (https://www.bubbleshop.net)
 */
class Bubble_Elasticsearch_Model_Resource_Client extends \Elastica\Client
{
    /**
     * @var Bubble_Elasticsearch_Helper_Data
     */
    protected $_helper;

    /**
     * @var bool Saves search engine availability
     */
    protected $_test;

    /**
     * Initializes search engine configuration
     */
    public function __construct()
    {
        $this->_helper = Mage::helper('elasticsearch');
        $config = $this->_helper->getEngineConfigData();
        parent::__construct($config);
    }

    /**
     * Cleans store index
     *
     * @param mixed $store
     * @param int $ids
     * @param string $type
     * @return Bubble_Elasticsearch_Model_Resource_Client
     */
    public function cleanStoreIndex($store = null, $ids = null, $type = 'product')
    {
        $index = $this->getStoreIndexName($store);
        if (empty($ids)) {
            // Delete ALL docs from specific index
            $this->getIndex($index)->getType($type)->delete();
        } else {
            // Delete matching ids from specific index
            $this->getIndex($index)->getType($type)->deleteIds((array) $ids);
        }

        return $this;
    }

    /**
     * Creates document in a clean format
     *
     * @param string $index
     * @param string $id
     * @param array $data
     * @param string $type
     * @return \Elastica\Document
     */
    public function createDoc($index, $id = '', array $data = array(), $type = 'product')
    {
        return new \Elastica\Document($id, $data, $type, $index);
    }

    /**
     * Returns indexer of specified type
     *
     * @param string $type
     * @return Bubble_Elasticsearch_Helper_Indexer_Abstract
     */
    public function getIndexer($type = 'product')
    {
        return Mage::helper('elasticsearch/indexer_' . $type);
    }

    /**
     * Returns query operator
     *
     * @return string
     */
    public function getQueryOperator()
    {
        return $this->getConfig('query_operator');
    }

    /**
     * @param $store
     * @param null $ids
     * @param string $type
     * @return mixed
     */
    public function getStoreData($store = null, $ids = null, $type = 'product')
    {
        $store = Mage::app()->getStore($store);
        $filters = array('store_id' => $store->getId());
        if (!empty($ids)) {
            $filters['entity_id'] = array_unique($ids);
        }
        $data = $this->getIndexer($type)->export($filters);

        return $data[$store->getId()];
    }

    /**
     * @param mixed $store
     * @param bool $new
     * @return \Elastica\Index
     * @throws Exception
     */
    public function getStoreIndex($store = null, $new = false)
    {
        if (!$this->isSafeReindexEnabled()) {
            $new = false; // reindex on current index and not on a temporary one
        }
        $name = $this->getStoreIndexName($store, $new);
        $index = $this->getIndex($name);
        if ($new && $this->indexExists($name)) {
            $index->delete(); // delete index if exists because we are indexing all documents
        }
        if (!$this->indexExists($name)) {
            $index->create(array('settings' => $this->_helper->getStoreIndexSettings($store)));

            foreach ($this->_helper->getStoreTypes($store) as $type) {
                $mapping = new \Elastica\Type\Mapping();
                $mapping->setType($index->getType($type));
                if (!$this->isSourceEnabled()) {
                    $mapping->disableSource();
                }
                $mapping->enableAllField(false); // not needed since we specify search fields manually
                $mapping->setProperties($this->getIndexer($type)->getStoreIndexProperties($store));

                Mage::dispatchEvent('bubble_elasticsearch_mapping_send_before', array(
                    'client' => $this,
                    'store' => $store,
                    'mapping' => $mapping,
                    'type' => $type,
                ));

                $mapping->send();
            }
        }

        return $index;
    }

    /**
     * @param mixed $store
     * @return string
     */
    public function getStoreIndexAlias($store)
    {
        $prefix = $this->getConfig('index_prefix');

        return $prefix . Mage::app()->getStore($store)->getCode();
    }

    /**
     * @param mixed $store
     * @param bool $new
     * @return string
     */
    public function getStoreIndexName($store, $new = false)
    {
        $alias = $this->getStoreIndexAlias($store);
        $name = $alias . '_idx1'; // index name must be different than alias name
        foreach ($this->getStatus()->getIndicesWithAlias($alias) as $indice) {
            if ($new) {
                $name = $indice->getName() != $name ? $name : $alias . '_idx2';
            } else {
                $name = $indice->getName();
            }
        }

        return $name;
    }

    /**
     * Retrieve suggest fields
     *
     * @return array
     */
    public function getSuggestFields()
    {
        $fields = new Varien_Object(array('name.std'));

        Mage::dispatchEvent('bubble_elasticsearch_suggest_fields', array(
            'client' => $this,
            'fields' => $fields,
        ));

        return $fields->getData();
    }

    /**
     * Checks if given index already exists
     * Here because of a bug when calling exists() method directly on index object during index process
     *
     * @param mixed $index
     * @return bool
     */
    public function indexExists($index)
    {
        return $this->getStatus()->indexExists($index);
    }

    /**
     * Checks if fuzzy query is enabled
     *
     * @link https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-flt-query.html
     * @return bool
     */
    public function isFuzzyQueryEnabled()
    {
        return (bool) $this->getConfig('enable_fuzzy_query');
    }

    /**
     * Checks if we reindex in a temporary index or not
     *
     * @return bool
     */
    public function isSafeReindexEnabled()
    {
        return (bool) $this->getConfig('safe_reindex');
    }

    /**
     * Checks if data should be either stored or not
     *
     * @return bool
     */
    public function isSourceEnabled()
    {
        return (bool) $this->getConfig('enable_source');
    }

    /**
     * Checks if suggestion is enabled
     *
     * @link https://www.elastic.co/guide/en/elasticsearch/reference/current/search-suggesters-phrase.html
     * @return bool
     */
    public function isSuggestEnabled()
    {
        return (bool) $this->getConfig('enable_suggest');
    }

    /**
     * Saves entities to specified index
     *
     * @param $index
     * @param $data
     * @param string $type
     * @return $this
     */
    public function saveEntities($index, $data, $type = 'product')
    {
        if (is_string($index)) {
            $index = $this->getIndex($index);
        }
        $chunks = array_chunk($data, 100);
        foreach ($chunks as $docs) {
            foreach ($docs as $k => $doc) {
                $docs[$k] = $this->createDoc($index, $doc['id'], $doc, $type);
            }
            try {
                $this->addDocuments($docs);
            } catch (Exception $e) {
                $this->_helper->handleError($e->getMessage());
                $this->_helper->handleError(print_r($docs, true));
            }
        }
        $index->refresh();

        return $this;
    }

    /**
     * Saves store entities
     *
     * @param mixed $store
     * @param array $ids
     * @param string $type
     * @return Bubble_Elasticsearch_Model_Resource_Client
     */
    public function saveStoreEntities($store = null, $ids = null, $type = 'product')
    {
        $data = $this->getStoreData($store, $ids, $type);
        $new = empty($ids) && $type == 'product'; // create a new index if full product reindexation
        $index = $this->getStoreIndex($store, $new);
        $this->saveEntities($index, $data, $type);

        if ($new) {
            $this->switchStoreIndex($index, $store);
        }

        return $this;
    }

    /**
     * @param string $q
     * @param mixed $store
     * @param array $params
     * @param string $type
     * @return array|\Elastica\ResultSet
     */
    public function search($q, $store = null, $params = array(), $type = 'product')
    {
        $index = $this->getStoreIndex($store);
        $indexer = $this->getIndexer($type);

        if (empty($params)) {
            $params = array('limit' => 10000); // should be enough
        }

        $q = $this->_helper->prepareQueryText($q);

        $bool = new \Elastica\Query\Bool();

        /**
         * Using cross-fields because it seems the best approach for entity search (products, categories, ...).
         * Cross-fields multi-match query has to work on fields that have the same analyzer.
         * So we build such a query for each configured analyzers.
         *
         * @link https://www.elastic.co/guide/en/elasticsearch/guide/current/_cross_fields_entity_search.html
         */
        $analyzers = $this->_helper->getStoreAnalyzers($store);
        foreach ($analyzers as $analyzer) {
            $fields = $indexer->getStoreSearchFields($store, $analyzer);
            if (!empty($fields)) {
                $query = new \Elastica\Query\MultiMatch();
                $query->setQuery($q);
                $query->setType('cross_fields');
                $query->setFields($fields);
                $query->setOperator($this->getQueryOperator());
                $query->setTieBreaker(.1);
                $bool->addShould($query);
            }
        }

        if ($this->isFuzzyQueryEnabled()) {
            // Standard search fields + fuzziness
            $fields = $indexer->getStoreSearchFields($store, 'std');
            if (!empty($fields)) {
                $query = new \Elastica\Query\FuzzyLikeThis();
                $query->setLikeText($q);
                $query->setParam('fields', $fields);
                $query->setPrefixLength(1);
                $query->setParam('fuzziness', 'auto');
                $bool->addShould($query);
            }
        }

        $search = $index->getType($type)->createSearch($bool, $params);

        $additionalFields = $indexer->getAdditionalFields();
        if (!empty($additionalFields)) {
            // Return additional fields (entity id is already implicitly included)
            $search->getQuery()->setFields($additionalFields);
        }

        if ($this->isSuggestEnabled()) {
            $suggest = new \Elastica\Suggest();
            $fields = $indexer->getStoreSearchFields($store, 'std', false);
            foreach ($this->getSuggestFields() as $field) {
                if (in_array($field, $fields)) {
                    $suggestField = new \Elastica\Suggest\Phrase($field, $field);
                    $suggestField->setText($q);
                    $suggestField->setGramSize(1);
                    $suggestField->setMaxErrors(.9);
                    $candidate = new \Elastica\Suggest\CandidateGenerator\DirectGenerator($field);
                    $candidate->setParam('min_word_length', 3);
                    $suggestField->addCandidateGenerator($candidate);
                    $suggest->addSuggestion($suggestField);
                    $search->getQuery()->setSuggest($suggest);
                }
            }
        }

        Mage::dispatchEvent('bubble_elasticsearch_before_search', array(
            'client' => $this,
            'search' => $search,
            'store' => $store,
            'type' => $type,
        ));


        Varien_Profiler::start('ELASTICA_SEARCH');

        $result = $search->search();

        Varien_Profiler::stop('ELASTICA_SEARCH');

        return $result;
    }

    /**
     * Switch index of specified store by linking alias on it
     *
     * @param mixed $index
     * @param mixed $store
     * @return Bubble_Elasticsearch_Model_Resource_Client
     */
    public function switchStoreIndex($index, $store = null)
    {
        if (is_string($index)) {
            $index = $this->getIndex($index);
        }
        $alias = $this->getStoreIndexAlias($store);
        foreach ($this->getStatus()->getIndicesWithAlias($alias) as $indice) {
            if ($indice->getName() != $index->getName()) {
                $indice->delete(); // remove old indice that was linked to the alias
            }
        }
        $index->addAlias($alias, true);

        return $this;
    }

    /**
     * Test if Elasticsearch server is reachable
     *
     * @return bool
     */
    public function test()
    {
        if (null === $this->_test) {
            try {
                $this->getStatus();
                $this->_test = true;
            } catch (Exception $e) {
                $this->_test = false;
                $this->_helper->handleError('Elasticsearch server is not reachable');
            }
        }

        return $this->_test;
    }
}