<?php
/**
 * Display suggestions in catalog search results
 *
 * @category    Bubble
 * @package     Bubble_Elasticsearch
 * @version     3.1.3
 * @copyright   Copyright (c) 2015 BubbleShop (https://www.bubbleshop.net)
 */
class Bubble_Elasticsearch_Block_Catalogsearch_Result extends Mage_Core_Block_Template
{
    /**
     * @var Bubble_Elasticsearch_Helper_Data
     */
    protected $_helper;

    /**
     * @var Mage_Catalog_Model_Resource_Category_Collection
     */
    protected $_categoriesWithPathNames;

    /**
     * Initialization
     */
    protected function _construct()
    {
        parent::_construct();
        $this->_helper = Mage::helper('elasticsearch');
    }

    /**
     * Retrieve categories matching text query
     *
     * @return Mage_Catalog_Model_Resource_Category_Collection
     */
    public function getCategoryCollection()
    {
        return $this->_helper->getCategoryCollection($this->getQueryText());
    }

    /**
     * Return given category path name with specified separator
     *
     * @param Mage_Catalog_Model_Category $category
     * @param string $separator
     * @return string
     */
    public function getCategoryPathName(Mage_Catalog_Model_Category $category, $separator = ' &gt; ')
    {
        $categoryWithPathNames = $this->getCategoriesWithPathNames($category->getStore())
            ->getItemById($category->getId());

        if ($categoryWithPathNames) {
            return implode($separator, (array) $categoryWithPathNames->getPathNames());
        }

        return $category->getName();
    }

    /**
     * Retrieve all categories of given store with path names
     *
     * @param mixed $store
     * @return Mage_Catalog_Model_Resource_Collection_Abstract
     * @throws Mage_Core_Exception
     */
    public function getCategoriesWithPathNames($store = null)
    {
        if (!$this->_categoriesWithPathNames) {
            $store = Mage::app()->getStore($store);
            $collection = Mage::getModel('catalog/category')->getCollection()
                ->addAttributeToSelect('name')
                ->setStoreId($store->getId());
            foreach ($collection as $category) {
                $category->setPathNames(new ArrayObject());
                $pathIds = array_slice($category->getPathIds(), 2);
                if (!empty($pathIds)) {
                    foreach ($pathIds as $pathId) {
                        $name = $collection->getItemById($pathId)->getName();
                        $category->getPathNames()->append($name);
                    }
                }
            }

            $this->_categoriesWithPathNames = $collection;
        }

        return $this->_categoriesWithPathNames;
    }

    /**
     * Retrieve CMS pages matching text query
     *
     * @return Mage_Cms_Model_Resource_Page_Collection
     */
    public function getPageCollection()
    {
        return $this->_helper->getPageCollection($this->getQueryText());
    }

    /**
     * Returns current text query
     *
     * @return string
     */
    public function getQueryText()
    {
        return $this->helper('catalogsearch')->getQueryText();
    }
}