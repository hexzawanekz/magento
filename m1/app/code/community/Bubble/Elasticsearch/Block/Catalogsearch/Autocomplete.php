<?php
/**
 * @category    Bubble
 * @package     Bubble_Elasticsearch
 * @version     3.1.3
 * @copyright   Copyright (c) 2015 BubbleShop (https://www.bubbleshop.net)
 */
class Bubble_Elasticsearch_Block_Catalogsearch_Autocomplete extends Bubble_Elasticsearch_Block_Catalogsearch_Result
{
    /**
     * @var Bubble_Elasticsearch_Helper_Data
     */
    protected $_helper;

    /**
     * @var Mage_Catalog_Model_Resource_Category_Collection
     */
    protected $_categories;

    /**
     * @var Mage_Catalog_Model_Resource_Category_Collection
     */
    protected $_categoriesWithPathNames;

    /**
     * @var Mage_Cms_Model_Resource_Page_Collection
     */
    protected $_pages;

    /**
     * @var Mage_Catalog_Model_Resource_Product_Collection
     */
    protected $_products;

    /**
     * @var Mage_Core_Block_Template
     */
    protected $_priceBlock;

    /**
     * Initialization
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('bubble/elasticsearch/autocomplete.phtml');
        $this->_helper = Mage::helper('elasticsearch');
    }

    /**
     * Initialize price template block
     *
     * @return Mage_Core_Block_Abstract
     */
    protected function _prepareLayout()
    {
        $this->setChild(
            'price_template',
            $this->getLayout()->createBlock('catalog/product_price_template', 'catalog_product_price_template')
        );

        return parent::_prepareLayout();
    }

    /**
     * Forward to default autocomplete if Elasticsearch autocomplete is disabled
     *
     * @return string
     */
    protected function _toHtml()
    {
        if (!Mage::getStoreConfigFlag('elasticsearch/autocomplete/enable')) {
            $block = new Mage_CatalogSearch_Block_Autocomplete();

            return $block->toHtml();
        }

        return parent::_toHtml();
    }

    /**
     * Retrieve categories matching text query
     *
     * @return Mage_Catalog_Model_Resource_Category_Collection
     */
    public function getCategoryCollection()
    {
        if (!$this->_helper->isAutocompleteEnabled('category')) {
            return new Varien_Data_Collection(); // empty collection
        }

        if (!$this->_categories) {
            $collection = parent::getCategoryCollection();

            if ($limit = $this->getLimit()) {
                $collection->getSelect()->limit($limit);
            }

            $this->_categories = $collection;
        }

        return $this->_categories;
    }

    /**
     * Retrieve CMS pages matching text query
     *
     * @return Mage_Cms_Model_Resource_Page_Collection
     */
    public function getPageCollection()
    {
        if (!$this->_helper->isAutocompleteEnabled('cms')) {
            return new Varien_Data_Collection(); // empty collection
        }

        if (!$this->_pages) {
            $collection = parent::getPageCollection();

            if ($limit = $this->getLimit()) {
                $collection->getSelect()->limit($limit);
            }

            $this->_pages = $collection;
        }

        return $this->_pages;
    }

    /**
     * Retrieve products matching text query
     *
     * @return Mage_Catalog_Model_Resource_Product_Collection
     */
    public function getProductCollection()
    {
        if (!$this->_products) {
            $query = $this->getQueryText();
            $queryObject = Mage::helper('catalogsearch')->getQuery();
            if ($queryObject->getSynonymFor()) {
                $query = $queryObject->getSynonymFor();
            }
            $collection = $this->_helper->getProductCollection($query);

            $collection->addAttributeToSelect($this->_helper->getProductAttributes())
                ->addMinimalPrice()
                ->addFinalPrice()
                ->addTaxPercents()
                ->addUrlRewrite()
                ->setVisibility(Mage::getSingleton('catalog/product_visibility')->getVisibleInSearchIds())
                ->addAttributeToFilter('status',
                    array('in' => Mage::getSingleton('catalog/product_status')->getVisibleStatusIds())
                );

            if ($limit = $this->getLimit()) {
                $collection->getSelect()->limit($limit);
            }

            $this->_products = $collection;
        }

        return $this->_products;
    }

    /**
     * Returns limit
     *
     * @return int
     */
    public function getLimit()
    {
        return Mage::getStoreConfig('elasticsearch/autocomplete/limit', $this->getStore());
    }

    /**
     * Returns current store
     *
     * @return Mage_Core_Model_Store
     */
    public function getStore()
    {
        return Mage::app()->getStore();
    }

    /**
     * Returns current text query
     *
     * @return string
     */
    public function getQueryText()
    {
        return $this->helper('catalogsearch')->getQueryText();
    }

    /**
     * Returns reset URL
     *
     * @return string
     */
    public function getResultUrl()
    {
        return $this->helper('catalogsearch')->getResultUrl($this->getQueryText());
    }

    /**
     * Checks if we should display product prices in autocomplete suggestions
     *
     * @return bool
     */
    public function shouldDisplayPrice()
    {
        return Mage::getStoreConfigFlag('elasticsearch/autocomplete/display_price', $this->getStore());
    }

    /**
     * Returns price HTML of given product
     *
     * @param Mage_Catalog_Model_Product $product
     * @return string
     */
    public function getPriceHtml(Mage_Catalog_Model_Product $product)
    {
        return $this->getPriceBlock()
            ->setUseLinkForAsLowAs(false)
            ->getPriceHtml($product, true);
    }

    /**
     * Returns price block
     *
     * @return Mage_Core_Block_Template
     */
    public function getPriceBlock()
    {
        if (null === $this->_priceBlock) {
            $this->_priceBlock = $this->getLayout()->createBlock('elasticsearch/catalog_product_price');
        }

        return $this->_priceBlock;
    }

    /**
     * Checks if we should display product images in autocomplete suggestions
     *
     * @return bool
     */
    public function shouldDisplayImage()
    {
        return Mage::getStoreConfigFlag('elasticsearch/autocomplete/display_image', $this->getStore());
    }

    /**
     * Returns configured image size
     *
     * @return int
     */
    public function getImageSize()
    {
        return (int) Mage::getStoreConfig('elasticsearch/autocomplete/image_size', $this->getStore());
    }
}