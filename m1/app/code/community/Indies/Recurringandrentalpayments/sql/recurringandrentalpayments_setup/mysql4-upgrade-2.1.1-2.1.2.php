<?php

$installer = $this;

$installer->startSetup();

$installer->run("
DROP TABLE IF EXISTS {$this->getTable('recurringandrentalpayments/termsprice')};
CREATE TABLE IF NOT EXISTS {$this->getTable('recurringandrentalpayments/termsprice')} (
  `id` int(11) unsigned NOT NULL auto_increment,
  `plan_id` tinyint(5) unsigned NOT NULL DEFAULT '0',
  `term_id` tinyint(5) unsigned NOT NULL DEFAULT '0',
  `product_id` varchar(8) NOT NULL,
  `term_number` int(3) unsigned NOT NULL,
  `term_price` float NOT NULL,
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");


$installer->endSetup();


