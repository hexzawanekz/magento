<?php
$installer = $this;
$installer->startSetup();

$installer->getConnection()
    ->addColumn('recurringandrentalpayments_sequence','2c2p_order_id', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'nullable'  => true,
        'length'    => 255,
        'after'     => null,
        'comment'   => '2c2p Order ID'
    ));
$installer->endSetup();

?>