<?php
class Indies_Recurringandrentalpayments_Model_Payment_Method_Normal2c2p extends Indies_Recurringandrentalpayments_Model_Payment_Method_Abstract
{
    public function processOrder(Mage_Sales_Model_Order $PrimaryOrder, Mage_Sales_Model_Order $Order = null)
    {
        // Set order as pending
        $Order->addStatusToHistory('pending', '', false)->save();
        // Throw exception to suspend subscription
        throw new Indies_Recurringandrentalpayments_Exception("Suspending subscription till order status change to completed");
    }

    public function getSubscriptionId($OrderItem)
    {
        return 1;
    }
}

?>