<?php
class TM_Pagespeed_Block_Adminhtml_Smush_Images_List_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('tm_pagespeed_smushit_grid');
        $this->setDefaultSort('path');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        //Varien_Data_Collection_Filesystem
        $collection = new TM_Pagespeed_Model_Smushit_Images_Collection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {

        $this->addColumn('path', array(
            'header'    => Mage::helper('TM_Pagespeed')->__('Path (basedir - %s)', Mage::getBaseDir()),
            'align'     => 'left',
            'index'     => 'path',
            // 'frame_callback' => array($this, 'decoratePath')
        ));

        $this->addColumn('ctime', array(
            'header'        => Mage::helper('TM_Pagespeed')->__('Created'),
            'align'         => 'left',
            'type'          => 'datetime',
            'width'         => '150px',
            'index'         => 'ctime',
        ));

        $this->addColumn('mtime', array(
            'header'        => Mage::helper('TM_Pagespeed')->__('Modified'),
            'align'         => 'left',
            'type'          => 'datetime',
            'width'         => '150px',
            'index'         => 'mtime',
        ));

        $this->addColumn('size', array(
            'header'    => Mage::helper('TM_Pagespeed')->__('Size'),
            'align'     =>'right',
            'width'     => '50px',
            'index'     => 'size',
            'type'      => 'number'
        ));

        $this->addColumn('perms', array(
            'header'    => Mage::helper('TM_Pagespeed')->__('Permissions'),
            'align'     => 'left',
            'index'     => 'perms',
            'width'     => '20px',
            // 'frame_callback' => array($this, 'decoratePath')
        ));

//        $this->addExportType('*/*/exportCsv', Mage::helper('TM_Pagespeed')->__('CSV'));
//        $this->addExportType('*/*/exportXml', Mage::helper('TM_Pagespeed')->__('XML'));

        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('path');
        $this->getMassactionBlock()->setFormFieldName('paths');
        $this->getMassactionBlock()->setUseSelectAll(false);

        $this->getMassactionBlock()->addItem('delete', array(
            'label'   => Mage::helper('TM_Pagespeed')->__('Mass Optimize'),
            'url'     => $this->getUrl('*/*/massOptimize', array('' => '')),// public function massDeleteAction() in Mage_Adminhtml_Tax_RateController
            'confirm' => Mage::helper('TM_Pagespeed')->__('Are you sure?')
        ));

        return $this;
    }
}
