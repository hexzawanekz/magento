<?php
class TM_Pagespeed_Block_Adminhtml_Smush_List_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('tm_pagespeed_smushit_grid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('TM_Pagespeed/Smushit')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('id', array(
            'header'    => Mage::helper('TM_Pagespeed')->__('ID'),
            'align'     =>'right',
            'width'     => '25px',
            'index'     => 'id',
            'type'      => 'number'
        ));
        $this->addColumn('path', array(
            'header'    => Mage::helper('TM_Pagespeed')->__('Path (basedir - %s)', Mage::getBaseDir()),
            'align'     => 'left',
            'index'     => 'path',
            'frame_callback' => array($this, 'decoratePath')
        ));
        $this->addColumn('src_size', array(
            'header'    => Mage::helper('TM_Pagespeed')->__('Source size'),
            'align'     =>'right',
            'width'     => '50px',
            'index'     => 'src_size',
            'type'      => 'number'
        ));
        $this->addColumn('dest_size', array(
            'header'    => Mage::helper('TM_Pagespeed')->__('Optimized size'),
            'align'     =>'right',
            'width'     => '50px',
            'index'     => 'dest_size',
            'type'      => 'number'
        ));
        $this->addColumn('percent', array(
            'header'    => Mage::helper('TM_Pagespeed')->__('Percent'),
            'align'     =>'right',
            'width'     => '25px',
            'index'     => 'percent',
            'type'      => 'number'
        ));

        $this->addColumn('smushed', array(
            'header'    => Mage::helper('TM_Pagespeed')->__('Optimized'),
            'align'     => 'left',
            'width'     => '80px',
            'index'     => 'smushed',
            'type'      => 'options',
            'options'   => array(
                '1' => Mage::helper('adminhtml')->__('Yes'),
                '0' => Mage::helper('adminhtml')->__('No')
            ),
        ));

        $this->addColumn('created', array(
            'header'        => Mage::helper('TM_Pagespeed')->__('Created date'),
            'align'         => 'left',
            'type'          => 'datetime',
            'width'         => '150px',
            'index'         => 'created',
        ));

        $this->addColumn('actions', array(
            'header'   => Mage::helper('tmcore')->__('Actions'),
            'width'    => '200px',
            'filter'   => false,
            'sortable' => false,
            'renderer' => 'tm_pagespeed/adminhtml_smush_list_grid_renderer_actions'
        ));

//        $this->addExportType('*/*/exportCsv', Mage::helper('TM_Pagespeed')->__('CSV'));
//        $this->addExportType('*/*/exportXml', Mage::helper('TM_Pagespeed')->__('XML'));

        return parent::_prepareColumns();
    }

    public function decoratePath($value, $row, $column, $isExport)
    {
        $baseDir = Mage::getBaseDir();
        $source  = $row['source'];
        $path    = $row['path'];

        $html = '';
        if (file_exists($source)) {
            $_url = str_replace(
                array($baseDir . DS . 'media' . DS, $baseDir . DS . 'skin' . DS),
                array(Mage::getBaseUrl('media'), Mage::getBaseUrl('skin')),
                $source
            );

            $html .= str_replace($baseDir, '{{basedir}}', $source)
             . ' size of : ' . filesize($source) . '<br/>';
            $html .= '<img ';
            $html .= 'id="' . $column->getId() . '_source" ';
            $html .= 'src="' . $_url . '"';
            $html .= 'class="grid-image"/>';
        }

        if (file_exists($path)) {
            $_url = str_replace(
                array($baseDir . DS . 'media' . DS, $baseDir . DS . 'skin' . DS),
                array(Mage::getBaseUrl('media'), Mage::getBaseUrl('skin')),
                $path
            );

            $html .= '<br/>' . str_replace($baseDir, '{{basedir}}', $path)
                . ' size of : '  . filesize($path) . '<br/><img ';
            $html .= 'id="' . $column->getId() . '_path" ';
            $html .= 'src="' . $_url . '"';
            $html .= 'class="grid-image"/>';
        }

        $imageSizes = @getimagesize($path);
        $width = 300;
        if (isset($imageSizes[0]) && $imageSizes[0] > 300) {
            $width = $imageSizes[0];
        }
        $height = 150;
        if (isset($imageSizes[1]) && $imageSizes[1] > 150) {
            $height = $imageSizes[1];
        }
        $title = str_replace($baseDir, '{{basedir}}', $value);
        $id = $row['id'];
        $js = "<script type=\"text/javascript\">
        var html" . $id . " = '" . $html . "', config" . $id . " = {
            closable:true,
            resizable:true,
            draggable:true,
            className:'magento',
            windowClassName:'popup-window',
            title:'" . $title . "',
            top:50,
            width:" . ($width + 50). ",
            height: " . (2 * $height + 50) . ",
            zIndex:1000,
            recenterAuto:false,
            hideEffect:Element.hide,
            showEffect:Element.show,
            id:'browser_window" . $row['id'] . "'
        };
        </script>
        ";
        return  $js . '<a href="#" onclick="Dialog.info(html' . $id . ', config' . $id . ')" >' . $title . '</a>'
        ;
    }

//    public function getRowUrl($row)
//    {
//        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
//    }
//
//
//        return $this;
//    }
}