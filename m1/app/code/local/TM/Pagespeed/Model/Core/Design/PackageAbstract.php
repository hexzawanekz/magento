<?php
if (Mage::helper('core')->isModuleOutputEnabled('TM_MobileSwitcher')) {
    class TM_Pagespeed_Model_Core_Design_PackageAbstract extends TM_MobileSwitcher_Model_Core_Design_Package {}
} elseif (Mage::helper('core')->isModuleOutputEnabled('TM_Argento')) {
    class TM_Pagespeed_Model_Core_Design_PackageAbstract extends TM_Argento_Model_Core_Design_Package {}
} elseif (Mage::helper('core')->isModuleOutputEnabled('Fooman_SpeedsterAdvanced')) {
    class TM_Pagespeed_Model_Core_Design_PackageAbstract extends Fooman_SpeedsterAdvanced_Model_Core_Design_Package {}
} else {
    class TM_Pagespeed_Model_Core_Design_PackageAbstract extends Mage_Core_Model_Design_Package {}
}
