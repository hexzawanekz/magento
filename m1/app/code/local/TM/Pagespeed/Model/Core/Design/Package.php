<?php
class TM_Pagespeed_Model_Core_Design_Package extends TM_Pagespeed_Model_Core_Design_PackageAbstract
{
    const MINIFY_JS_ENABLE_CONFIG  = 'tm_pagespeed/minify_js/enabled';
    const MINIFY_JS_TYPE_CONFIG    = 'tm_pagespeed/minify_js/type';
    const MINIFY_CSS_ENABLE_CONFIG = 'tm_pagespeed/minify_css/enabled';
    const MINIFY_CSS_TYPE_CONFIG   = 'tm_pagespeed/minify_css/type';
    const MINIFY_REMOVE_IMPORTANT_COMMENTS = 'tm_pagespeed/general/remove_important_comments';
    const MINIFY_JS_ENABLE_ON_ADMIN_CONFIG = 'tm_pagespeed/general/enable_on_admin';

    protected function _getLastModifiedTime(array $files)
    {
        $time = false;
        foreach ($files as $file) {
            if (is_file($file)) {
                $time = max($time, filemtime($file));
            }
        }
        return $time;
    }

    public function getMergedJsUrl($files)
    {
        $isEnable = (bool) Mage::getStoreConfig(self::MINIFY_JS_ENABLE_CONFIG);
        if (!$isEnable) {
            return parent::getMergedJsUrl($files);
        }
        $isEnableOnAdmin = (bool) Mage::getStoreConfig(self::MINIFY_JS_ENABLE_ON_ADMIN_CONFIG);
        if (Mage::app()->getStore()->isAdmin() && !$isEnableOnAdmin) {
            return parent::getMergedJsUrl($files);
        }

        $mtime = $this->_getLastModifiedTime($files);
        $compressionType = Mage::getStoreConfig(self::MINIFY_JS_TYPE_CONFIG);

        $targetFilename = md5(implode(',', $files))
            . '.' . $mtime . '.' . $compressionType . '.js';
        $targetFilename = str_replace(" ", '-', $targetFilename);

        $targetDir = $this->_initMergerDir('js');
        if (!$targetDir) {
            return '';
        }
        $beforeMergeCallback = array($this, 'beforeMergeJs');
        foreach ($files as &$file) {
            list($file) = explode('?', $file, 2);
        }
        if ($this->_mergeFiles($files, $targetDir . DS . $targetFilename, false, $beforeMergeCallback, 'js')) {
            return Mage::getBaseUrl('media', Mage::app()->getRequest()->isSecure()) . 'js/' . $targetFilename;
        }
        return '';
    }

    /**
     * Before merge JS callback function
     *
     * @param string $file
     * @param string $contents
     *
     * @return string
     */
    public function beforeMergeJs($file, $contents)
    {
        try {
            if (preg_match('/@ sourceMappingURL=([^\s]*)/s', $contents, $matches)) {
                $contents = str_replace($matches[0], '', $contents);
            }

            //convert important comments to usual commets
            $isRemoveImportantComments = (bool) Mage::getStoreConfig(self::MINIFY_REMOVE_IMPORTANT_COMMENTS);
            if ($isRemoveImportantComments) {
                $contents = str_replace('/*!', '/*', $contents);
            }

            $compressionType = Mage::getStoreConfig(self::MINIFY_JS_TYPE_CONFIG);
            $model = Mage::getModel('TM_Pagespeed/minify_javascript');

            $contents = $model->compress($contents, $compressionType);
        } catch (Exception $e) {
            $message = "Source file " . $file . "\n" . $e->getMessage();
            $e = new Mage_Exception($message);
            Mage::logException($e);
            $log = Mage::getStoreConfig('dev/log/exception_file');
            $contents = "\n/*! {$file} cannot be minified with {$compressionType}. Look at {{base_dir}}/var/log/{$log} for more info. */\n" . $contents;
        }

        return $contents . "\n";
    }

    public function getMergedCssUrl($files)
    {
        $isEnable = (bool) Mage::getStoreConfig(self::MINIFY_CSS_ENABLE_CONFIG);
        if (!$isEnable) {
            return parent::getMergedCssUrl($files);
        }
        $isEnableOnAdmin = (bool) Mage::getStoreConfig(self::MINIFY_JS_ENABLE_ON_ADMIN_CONFIG);
        if (Mage::app()->getStore()->isAdmin() && !$isEnableOnAdmin) {
            return parent::getMergedCssUrl($files);
        }

        // secure or unsecure
        $isSecure = Mage::app()->getRequest()->isSecure();
        $mergerDir = $isSecure ? 'css_secure' : 'css';
        $targetDir = $this->_initMergerDir($mergerDir);
        if (!$targetDir) {
            return '';
        }

        // base hostname & port
        $baseMediaUrl = Mage::getBaseUrl('media', $isSecure);
        $hostname = parse_url($baseMediaUrl, PHP_URL_HOST);
        $port = parse_url($baseMediaUrl, PHP_URL_PORT);
        if (false === $port) {
            $port = $isSecure ? 443 : 80;
        }
        foreach ($files as &$file) {
            list($file) = explode('?', $file, 2);
        }
        // merge into target file
        $mtime = $this->_getLastModifiedTime($files);
        $compressionType = Mage::getStoreConfig(self::MINIFY_CSS_TYPE_CONFIG);
        $targetFilename = md5(implode(',', $files) . "|{$hostname}|{$port}")
            . '.' . $mtime . '.' . $compressionType . '.css';
        $targetFilename = str_replace(" ", '-', $targetFilename);

        $mergeFilesResult = $this->_mergeFiles(
            $files, $targetDir . DS . $targetFilename,
            false,
            array($this, 'beforeMergeCss'),
            'css'
        );
        if ($mergeFilesResult) {
            return $baseMediaUrl . $mergerDir . '/' . $targetFilename;
        }
        return '';
    }

    /**
     * Before merge css callback function
     *
     * @param string $file
     * @param string $contents
     * @return string
     */
    public function beforeMergeCss($file, $contents)
    {
        try {
            $this->_setCallbackFileDir($file);

            $cssImport = '/@import\\s+([\'"])(.*?)[\'"]/';
            $contents = preg_replace_callback($cssImport, array($this, '_cssMergerImportCallback'), $contents);

            $cssUrl = '/url\\(\\s*(?!data:)([^\\)\\s]+)\\s*\\)?/';
            $contents = preg_replace_callback($cssUrl, array($this, '_cssMergerUrlCallback'), $contents);

            //convert important comments to usual commets
            $isRemoveImportantComments = (bool) Mage::getStoreConfig(self::MINIFY_REMOVE_IMPORTANT_COMMENTS);
            if ($isRemoveImportantComments) {
                $contents = str_replace('/*!', '/*', $contents);
            }

            $compressionType = Mage::getStoreConfig(self::MINIFY_CSS_TYPE_CONFIG);
            $model = Mage::getModel('TM_Pagespeed/minify_css');

            $contents = $model->compress($contents, $compressionType);
        } catch (Exception $e) {
            $message = "Source file " . $file . "\n" . $e->getMessage();
            $e = new Mage_Exception($message);
            Mage::logException($e);
            $log = Mage::getStoreConfig('dev/log/exception_file');
            $contents = "\n/* {$file} cannot be minified with {$compressionType}. Look at {$log} for more info. */\n" . $contents;
        }
       return $contents . "\n";
    }
}