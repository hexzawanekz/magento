<?php
class TM_Pagespeed_Model_System_Config_Source_Minify_Css_Type
{
    const YUI_TYPE     = 'yui compressor';
    const CSSMIN_TYPE  = 'cssmin';
    const DEFAULT_TYPE = 'default';

    /**
     *
     * @return array
     */
    public function toOptionHash()
    {
        return $this->_toOptionHash();
    }

    /**
     *
     * @return array
     */
    protected function _toOptionHash()
    {
        $result = array();
        $consts = array(self::DEFAULT_TYPE, self::CSSMIN_TYPE);
        if(function_exists('exec')) {
            $consts[] = self::YUI_TYPE;
        }
        $model = Mage::getModel('TM_Pagespeed/minify_css');

        $filename = BP . DS . 'skin' . DS . 'frontend' . DS . 'default' . DS . 'default' . DS . 'css' . DS
                // . 'email-inline.css';
             . 'styles.css';
        $contents = file_get_contents($filename);
        $size = strlen($contents);
        $isTest = Mage::getSingleton('adminhtml/session')->getCheckCssOnConfig();
        if ($isTest) {
            Varien_Profiler::enable();
        }
        foreach ($consts as $const) {
            try {
                $title = Mage::helper('TM_Pagespeed')->__(ucfirst($const));

                if ($isTest) {
                    Varien_Profiler::start($const);
                    $_contents = $model->compress($contents, $const);
                    Varien_Profiler::stop($const);
                    $_size = strlen($_contents);

                    // $title = str_pad($title, 16, ' ') . ' : ' . $_size . " : -"
                    //     . (100 - round(($_size / $size) * 100, 2)) . '% : '
                    //     . round(Varien_Profiler::fetch($const), 6);
                    $title = str_pad($title, 16, ' ')
                        . " ~compression "
                        . (100 - round(($_size / $size) * 100, 2)) . '% ('
                        . round(Varien_Profiler::fetch($const), 3). ' msec)';

                    Mage::getSingleton('adminhtml/session')->addSuccess(
                        Mage::helper('TM_Pagespeed')->__($title)
                    );
                }
                $result[$const] = $title;
            } catch (Exception $e) {
                Mage::logException($e);
                $logFile = Mage::getStoreConfig("dev/log/exception_file");
                Mage::getSingleton('adminhtml/session')->addNotice(
                    Mage::helper('TM_Pagespeed')->__(
                        "Compression method '%s' is not supported, look at the log file %s for more information", $title, $logFile
                    )
                );
            }
        }
        return $result;
    }

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        $result = array();
        foreach ($this->_toOptionHash() as $key => $value) {
            $result[] = array('value' => $key, 'label' => $value);
        }
        return $result;
    }
}
