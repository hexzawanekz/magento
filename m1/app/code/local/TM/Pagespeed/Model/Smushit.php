<?php
class TM_Pagespeed_Model_Smushit extends Mage_Core_Model_Abstract
{
    const SMUSHIT_SCAN_CONFIG    = 'tm_pagespeed/smushit/scan';

    protected $_smushedFiles = array();

    protected $_archiveFiles = array();

    protected $_allowedExtensions = array('gif', 'jpg', 'jpeg', 'png');

    protected $_useThrowException = true;

    public function _construct()
    {
        parent::_construct();
        $this->_init('TM_Pagespeed/smushit');
    }

    /**
     *
     * @param  boolean $status
     * @return TM_Pagespeed_Model_Smushit
     */
    public function useThrowException($status = true)
    {
        $this->_useThrowException = (bool) $status;
        return $this;
    }

    /**
     *
     * @param array|string $extensions [description]
     */
    public function setAllowedExtensions($extensions)
    {
        if (is_string($extensions)) {
            $extensions = array($extensions);
        }
        $this->_allowedExtensions = $extensions;
        return $this;
    }

    /**
     *
     * @param string $filename
     * @return boolean
     */
    protected function _isImage($filename)
    {
        if (!file_exists($filename)
            || is_dir($filename)
            || !is_readable($filename)) {

            return false;
        }
        $extension = pathinfo($filename, PATHINFO_EXTENSION);
        if (!in_array($extension, $this->_allowedExtensions)) {
            return false;
        }
        $info = @getimagesize($filename);
        if (!$info || !isset($info[2]) || !in_array($info[2], array(1, 2, 3)) ) { // 1-gif, 2-jpeg, 3-png
            return false;
        }

        return true;
    }

    /**
     *
     * @param string $filename
     * @return boolean
     */
    protected function _isSmushedBefore($filename)
    {
        if (empty($this->_smushedFiles)) {
            $collection = $this->getCollection();
            $files = array();
            foreach ($collection as $row) {
                $files[$row->getPath()] = $row->getCreated();
            }
            $this->_smushedFiles = $files;
        }
        if (isset($this->_smushedFiles[$filename])) {
            $_smushedTime = strtotime($this->_smushedFiles[$filename]);
            if (filemtime($filename) > $_smushedTime) {
                return false;
            }
            return true;
        }

        return false;
    }

    /**
     *
     * @param  string  $filename
     * @return boolean
     */
    protected function _isArchive($filename)
    {
        if (substr_count(basename($filename), '.') == 1) {
            return false;
        }
        if (empty($this->_archiveFiles)) {
            $collection = $this->getCollection();
            $files = array();
            foreach ($collection as $row) {
                $files[] = $row->getSource();
            }
            $files = array_unique($files);
            $this->_archiveFiles = $files;
        }

        return in_array($filename, $this->_archiveFiles);
    }

    /**
     *
     * @param string $filename
     * @return boolean
     */
    protected function _isIgnoredFile($filename)
    {
        $ignores = explode("\n", Mage::getStoreConfig('tm_pagespeed/smushit/ignore'));
        $baseDir = Mage::getBaseDir();
        foreach ($ignores as $ignore) {
            $ignore = trim($ignore, " \r");
            if (empty($ignore)) {
                continue;
            }
            $placeholder = rtrim($baseDir, DS) . DS. $ignore;
            if (strstr($filename, $placeholder)) {
                return true;
            }
        }
        return false;
    }

    /**
     *
     * @param  string  $filename
     * @return boolean
     */
    protected function _isOwnerIdentical($filename)
    {
        if (!function_exists('posix_getpwuid')) {
            return true;
        }
        $owner = posix_getpwuid(fileowner($filename));
        $user = posix_getpwuid(posix_geteuid());

        return  $owner['name'] == $user['name'];
    }

    /**
     *
     * @param string $path
     * @return array
     */
    protected function _getImagesByDir($path)
    {
        $iterator = new RecursiveDirectoryIterator($path);
        $iterator->setFlags(RecursiveDirectoryIterator::SKIP_DOTS);

        $iterator = new RecursiveIteratorIterator(
            $iterator,
            RecursiveIteratorIterator::CHILD_FIRST
        );

        $files = array();
        foreach ($iterator as $path) {
            if ($path->isDir()) {
                continue;
            }
            $_path = $path->__toString();

            if ($this->_isImage($_path)
                && !$this->_isSmushedBefore($_path)
                && !$this->_isIgnoredFile($_path)
                // && $this->_isOwnerIdentical($_path)
                && !$this->_isArchive($_path)) {

                $files[] = $_path;
            }
        }
        return $files;
    }

    protected function _getService()
    {
        $type = Mage::getStoreConfig('tm_pagespeed/smushit/type');
        switch ($type) {
            case TM_Pagespeed_Model_System_Config_Source_Smush_Type::YAHOO:
                $serviceClass = 'TM_Pagespeed_Model_Service_Yahoo_SmushIt';
                break;
            case TM_Pagespeed_Model_System_Config_Source_Smush_Type::RESMUSH:
                $serviceClass = 'TM_Pagespeed_Model_Service_ReSmushIt';
                break;
            case TM_Pagespeed_Model_System_Config_Source_Smush_Type::UTIL:
            default:
                $serviceClass = 'TM_Pagespeed_Model_Service_Util';
                break;
        }
        $service = new $serviceClass();
        return $service;
    }

    public function restore()
    {
        $service = $this->_getService();
        if ($service->restore($this->getSource(), $this->getPath())) {
            $size = $this->getSrcSize();

            $data = array_merge($this->getData(), array(
                'src_size'  => $size,
                'dest_size' => $size,
                'percent'   => 0,
                'smushed'   => false,
                'created'   => now(),
            ));
            $this->setData($data);
            $this->save();
        }

        return $this;
    }

    public function resmush()
    {
        $service = $this->_getService();
        $filename = $this->getPath();
        $data = array_merge($this->getData(), $service->smush($filename));
        $this->setData($data);
        $this->save();

        return $this;
    }

    public function check()
    {
        $service = $this->_getService();
        $service->check();

        return $this;
    }

    /**
     *
     * @param string|array of string $paths
     */
    public function smush($paths, $count = 50)
    {
        if (is_string($paths)) {
            $paths = array($paths);
        }

        $files = $this->getFiles($paths);
        $files = array_slice($files, 0, $count);
        // $count = 1;
        // $files = array_slice($files, rand(0, count($files) - $count), $count);
       // Zend_Debug::dump($files);

        // Take a batch of three files
        $service = $this->_getService();
        foreach ($files as $filename) {
            $data = $service->smush($filename);
            if (!$data || !isset($data['path']) || empty($data['path'])) {
                $logFilename = "tm_pagespeed_util.log";
                $error = 'Error within smushing ' . $filename . " progress; result: " . (is_array($data) ? implode(',', $data) : (string) $data);
                Mage::log($error, null, $logFilename);
                if ($this->_useThrowException) {
                    throw new Mage_Exception($error, 1);
                }
                break;
            }
            $model = Mage::getModel('TM_Pagespeed/smushit');
            $model->setData($data);
            $model->save();
        }
        return $this;
    }

    public function getPaths()
    {
        $_path = getcwd() . DS;
//        $paths = explode(',', Mage::getStoreConfig(self::SMUSHIT_SCAN_CONFIG));
        $paths = explode("\n", Mage::getStoreConfig(self::SMUSHIT_SCAN_CONFIG));

        foreach ($paths as $i => $path) {
            $path = trim($path, " \r");
            if (empty($path)) {
                continue;
            }
            $paths[$i] = $_path .  $path;
        }

        return $paths;
    }

    public function getFiles($paths)
    {
        if (is_string($paths)) {
            $paths = array($paths);
        }

        $files = array();
        foreach ($paths as $path) {
            if (is_dir($path)) {
                $files = array_merge($files, $this->_getImagesByDir($path));
            } elseif (is_file($path)
                && $this->_isImage($path)
                && !$this->_isSmushedBefore($path)
                && !$this->_isIgnoredFile($path)
                // && $this->_isOwnerIdentical($path)
                && !$this->_isArchive($path)) {

                $files[] = $path;
            }
        }

        $files = array_unique($files);
        return $files;
    }
}
