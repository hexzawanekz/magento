<?php

// $minifyLibPath = BP . DS . 'lib' . DS . 'pagespeed' . DS . 'min';
// set_include_path( $minifyLibPath . PATH_SEPARATOR . get_include_path());

require_once 'pagespeed/min/JSMin.php';
require_once 'pagespeed/min/JSMinPlus.php';
require_once 'pagespeed/min/Minify/YUICompressor.php';
require_once 'pagespeed/min/Minify/ClosureCompiler.php';

class TM_Pagespeed_Model_Minify_Javascript //extends Minify_Javascript
{
    const YUI_JAVA_CONFIG    = 'tm_pagespeed/yui/javaExecutable';
    const YUI_JAR_CONFIG     = 'tm_pagespeed/yui/jarFile';
    const YUI_TEMPDIR_CONFIG = 'tm_pagespeed/yui/tempDir';

    const CC_JAVA_CONFIG    = 'tm_pagespeed/closure_compiler/javaExecutable';
    const CC_JAR_CONFIG     = 'tm_pagespeed/closure_compiler/jarFile';
    const CC_TEMPDIR_CONFIG = 'tm_pagespeed/closure_compiler/tempDir';

    public function minify($js)
    {
        return trim(JSMin::minify($js), "\s\n\t");
    }

    public function jsminplus_minify($js)
    {
        return trim(JSMinPlus::minify($js), "\s\n\t");
    }

    /**
     * yui_minify
     * @param  string $js
     * @return string
     */
    public function yui_minify($js)
    {
        $javaExecutable = (string) Mage::getStoreConfig(self::YUI_JAVA_CONFIG);

        $jarFile = (string) Mage::getStoreConfig(self::YUI_JAR_CONFIG);
        $jarFile = str_replace('{{base_dir}}', BP, $jarFile);

        $tempDir = (string) Mage::getStoreConfig(self::YUI_TEMPDIR_CONFIG);
        $tempDir = str_replace('{{base_dir}}', BP, $tempDir);
        if (empty($tempDir)) {
            $tempDir = Mage::getBaseDir('var') . DS . 'cache';
        }
        if (!file_exists($tempDir)) {
            mkdir($tempDir, 0777, true);
        }

        Minify_YUICompressor::$javaExecutable = $javaExecutable;
        Minify_YUICompressor::$jarFile = $jarFile;
        Minify_YUICompressor::$tempDir = $tempDir;
        Minify_YUICompressor::$isDebug = true;
        return trim(Minify_YUICompressor::minifyJs($js), "\s\n\t");
    }

    public function closure_compiler_minify($js)
    {
        $javaExecutable = (string) Mage::getStoreConfig(self::CC_JAVA_CONFIG);

        $jarFile = (string) Mage::getStoreConfig(self::CC_JAR_CONFIG);
        $jarFile = str_replace('{{base_dir}}', BP, $jarFile);

        $tempDir = (string) Mage::getStoreConfig(self::CC_TEMPDIR_CONFIG);
        $tempDir = str_replace('{{base_dir}}', BP, $tempDir);
        if (empty($tempDir)) {
            $tempDir = Mage::getBaseDir('var') . DS . 'cache';
        }
        if (!file_exists($tempDir)) {
            mkdir($tempDir, 0777, true);
        }

        Minify_ClosureCompiler::$javaExecutable = $javaExecutable;
        Minify_ClosureCompiler::$jarFile = $jarFile;
        Minify_ClosureCompiler::$tempDir = $tempDir;
        Minify_ClosureCompiler::$isDebug = true;
        return trim(Minify_ClosureCompiler::minify($js), "\s\n\t");
    }

    public function compress($js, $type = TM_Pagespeed_Model_System_Config_Source_Minify_Js_Type::JSMIN_TYPE)
    {
        switch ($type) {
            case TM_Pagespeed_Model_System_Config_Source_Minify_Js_Type::YUI_TYPE:
               $contents = $this->yui_minify($js);
               break;
            case TM_Pagespeed_Model_System_Config_Source_Minify_Js_Type::CLOSURE_COMPILER_TYPE:
               $contents = $this->closure_compiler_minify($js);
               break;
            case TM_Pagespeed_Model_System_Config_Source_Minify_Js_Type::JSMINPLUS_TYPE:
               $contents = $this->jsminplus_minify($js);
               break;
            case TM_Pagespeed_Model_System_Config_Source_Minify_Js_Type::JSMIN_TYPE:
            default:
               $contents = $this->minify($js);
               break;
        }
        return $contents;
    }
}