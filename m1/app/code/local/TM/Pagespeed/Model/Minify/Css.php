<?php

// $minifyLibPath = BP . DS . 'lib' . DS . 'pagespeed' . DS . 'min';
// set_include_path( $minifyLibPath . PATH_SEPARATOR . get_include_path());

require_once 'pagespeed/min/Minify/CSS/Compressor.php';
require_once 'pagespeed/min/Minify/CommentPreserver.php';
require_once 'pagespeed/min/Minify/CSS/UriRewriter.php';
require_once 'pagespeed/min/CSSmin.php';
require_once 'pagespeed/min/Minify/YUICompressor.php';

class TM_Pagespeed_Model_Minify_Css
{
    const YUI_JAVA_CONFIG    = 'tm_pagespeed/yui/javaExecutable';
    const YUI_JAR_CONFIG     = 'tm_pagespeed/yui/jarFile';
    const YUI_TEMPDIR_CONFIG = 'tm_pagespeed/yui/tempDir';

   /**
     * Minify a CSS string
     *
     * @param string $css
     *
     * @param array $options available options:
     *
     * 'preserveComments': (default true) multi-line comments that begin
     * with "/*!" will be preserved with newlines before and after to
     * enhance readability.
     *
     * 'removeCharsets': (default true) remove all @charset at-rules
     *
     * 'prependRelativePath': (default null) if given, this string will be
     * prepended to all relative URIs in import/url declarations
     *
     * 'currentDir': (default null) if given, this is assumed to be the
     * directory of the current CSS file. Using this, minify will rewrite
     * all relative URIs in import/url declarations to correctly point to
     * the desired files. For this to work, the files *must* exist and be
     * visible by the PHP process.
     *
     * 'symlinks': (default = array()) If the CSS file is stored in
     * a symlink-ed directory, provide an array of link paths to
     * target paths, where the link paths are within the document root. Because
     * paths need to be normalized for this to work, use "//" to substitute
     * the doc root in the link paths (the array keys). E.g.:
     * <code>
     * array('//symlink' => '/real/target/path') // unix
     * array('//static' => 'D:\\staticStorage')  // Windows
     * </code>
     *
     * 'docRoot': (default = $_SERVER['DOCUMENT_ROOT'])
     * see Minify_CSS_UriRewriter::rewrite
     *
     * @return string
     */
    public static function minify($css, $options = array())
    {
        $options = array_merge(array(
            'compress'            => true,
            'removeCharsets'      => true,
            'preserveComments'    => true,
            'currentDir'          => null,
            'docRoot'             => $_SERVER['DOCUMENT_ROOT'],
            'prependRelativePath' => null,
            'symlinks'            => array(),
        ), $options);

        if ($options['removeCharsets']) {
            $css = preg_replace('/@charset[^;]+;\\s*/', '', $css);
        }
        if ($options['compress']) {
            if (! $options['preserveComments']) {
                $css = Minify_CSS_Compressor::process($css, $options);
            } else {
                $css = Minify_CommentPreserver::process(
                    $css,
                    array('Minify_CSS_Compressor', 'process'),
                    array($options)
                );
            }
        }
        if (! $options['currentDir'] && ! $options['prependRelativePath']) {
            return $css;
        }
        if ($options['currentDir']) {
            return Minify_CSS_UriRewriter::rewrite(
                $css,
                $options['currentDir'],
                $options['docRoot'],
                $options['symlinks']
            );
        } else {
            return Minify_CSS_UriRewriter::prepend(
                $css,
                $options['prependRelativePath']
            );
        }
    }

    /**
     * php port yui css compressor
     * https://github.com/tubalmartin/YUI-CSS-compressor-PHP-port
     *
     * @param  string $css
     * @return string
     */
    public static function cssmin_minify($css)
    {
        $compressor = new CSSmin(false);
        return $compressor->run($css);
    }

    /**
     * yui_minify
     * @param  string $css
     * @return string
     */
    public static function yui_minify($css)
    {
        $javaExecutable = (string) Mage::getStoreConfig(self::YUI_JAVA_CONFIG);
        if (empty($javaExecutable)) {
            $javaExecutable = 'java';
        }
        $jarFile = (string) Mage::getStoreConfig(self::YUI_JAR_CONFIG);
        $jarFile = str_replace('{{base_dir}}', BP, $jarFile);
        if (empty($jarFile)) {
            $jarFile = BP . DS . 'lib' . DS . 'pagespeed' . DS . 'min' . DS . 'yuicompressor-2.4.8.jar';
        }
        $tempDir = (string) Mage::getStoreConfig(self::YUI_TEMPDIR_CONFIG);
        $tempDir = str_replace('{{base_dir}}', BP, $tempDir);
        if (empty($tempDir)) {
            $tempDir = Mage::getBaseDir('var') . DS . 'cache';
        }
        if (!file_exists($tempDir)) {
            mkdir($tempDir, 0777, true);
        }

        Minify_YUICompressor::$javaExecutable = $javaExecutable;
        Minify_YUICompressor::$jarFile = $jarFile;
        Minify_YUICompressor::$tempDir = $tempDir;
        $css = Minify_YUICompressor::minifyCss($css);
        return $css;
    }

    public function compress($css, $type = TM_Pagespeed_Model_System_Config_Source_Minify_Css_Type::DEFAULT_TYPE)
    {
        switch ($type) {
            case TM_Pagespeed_Model_System_Config_Source_Minify_Css_Type::YUI_TYPE:
               $contents = self::yui_minify($css);
               break;
            case TM_Pagespeed_Model_System_Config_Source_Minify_Css_Type::CSSMIN_TYPE:
               $contents = self::cssmin_minify($css);
               break;
            case TM_Pagespeed_Model_System_Config_Source_Minify_Css_Type::DEFAULT_TYPE:
            default:
                $options = array(
                    // currentDir overrides prependRelativePath
                    //'currentDir'         => implode(DS, $completeFilePathComponents),
                    'preserveComments'   => false,
                    //'prependRelativePath'=> $prependRelativePath,
                    'symlinks'           => array('//' => BP)
                );
                $contents = self::minify($css, $options);
                break;
        }

        return trim($contents, "\s\n\t");
    }
}