<?php
class TM_Lazyload_Model_Observer
{
    const ENABLE_CONFIG          = 'tm_pagespeed/general/enabled';
    const LAZYLOAD_ENABLE_CONFIG = 'tm_pagespeed/lazyload/enabled';
    const LAZYLOAD_IGNORE_CONFIG = 'tm_pagespeed/lazyload/ignore';

    protected $_ignore = null;

    /**
     *
     * @return boolean
     */
    protected function _isAjax()
    {
        return Mage::app()->getRequest()->isAjax();
    }

    protected function _isActual($config)
    {
        return ((bool) Mage::getStoreConfig(self::ENABLE_CONFIG))
            && !Mage::app()->getRequest()->isAjax()
            && !Mage::app()->getStore()->isAdmin()
            && ((bool) Mage::getStoreConfig($config))
        ;
    }

    protected function _isRootBlock($observer)
    {
        $block = $observer->getBlock();
        if (!$block) {
            return false;
        }
        $blockName = $block->getNameInLayout();
        if (empty($blockName) || $blockName !== 'root') {
            return false;
        }
        return true;
    }

    /**
     *
     * @param string $src
     * @return boolean
     */
    protected function _isIgnored($src)
    {
        if (null === $this->_ignore) {
            $ignores = explode("\n", Mage::getStoreConfig(self::LAZYLOAD_IGNORE_CONFIG));
            foreach ($ignores as &$ignore) {
                $ignore = trim($ignore, " \r");
            }
            $this->_ignore = $ignores;
        }
        foreach ($this->_ignore as $ignore) {
            if (strstr($src, $ignore)) {
                return true;
            }
        }
        return false;
    }

    //https://github.com/aFarkas/lazysizes
    //https://github.com/aFarkas/respimage
    public function prepare($observer)
    {
        if (!Mage::registry('current_product')) {
            if (!$this->_isActual(self::LAZYLOAD_ENABLE_CONFIG)
                || !$this->_isRootBlock($observer)
            ) {
                return;
            }
            Varien_Profiler::enable();
            $timerName = __METHOD__;
            Varien_Profiler::start($timerName);
            $transport = $observer->getTransport();
            $html = $transport->getHtml();
            $matches = array();
            preg_match_all('/<img[\s\r\n]+.*?>/is', $html, $matches);
            $placeholder = '';//"data:image/gif;base64,R0lGODlhAQABAIAAAAAAAAAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==";
            $dom = new Zend_Dom_Query();
            foreach ($matches[0] as $_img) {
                $dom->setDocumentHtml($_img);
                $node = $dom->query('img')->current();
                $class = $node->getAttribute('class') . ' lazyload';
                $node->setAttribute('class', trim($class));
                foreach (array('src', 'srcset') as $attrName) {
                    $attrValue = $node->getAttribute($attrName);
                    if (!empty($attrValue) && !$this->_isIgnored($attrValue)) {
                        $node->setAttribute('data-' . $attrName, $attrValue);
                        $node->setAttribute($attrName, $placeholder);
//                    $node->removeAttribute($attrName);
                    }
                }
                //add alt for fix chrome width 0 (auto) bug
                $alt = $node->getAttribute('alt');
                if (empty($alt)) {
                    $node->setAttribute('alt', 'lazyload');
                }
                $html = str_replace($_img, utf8_decode($node->C14N()), $html);
            }
            $transport->setHtml($html);
            Varien_Profiler::stop($timerName);
//        Zend_Debug::dump(Varien_Profiler::fetch($timerName));
        }
    }
}
