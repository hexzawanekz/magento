<?php
class TM_DeferJs_Model_Observer
{
    const ENABLE_CONFIG = 'tm_pagespeed/deferjs/enabled';

    protected function isEnable()
    {
        return (bool) Mage::getStoreConfig(self::ENABLE_CONFIG)
            && !Mage::app()->getRequest()->isAjax()
            && !Mage::app()->getStore()->isAdmin()
        ;
    }

    public function deferJsInOutput($observer)
    {
        if (!$this->isEnable()) {
            return;
        }
        $response = $observer->getResponse();
        $html = $response->getBody();
        if (empty($html) || stripos($html, '<!DOCTYPE html') === false) {
            return;
        }

        $dom = new DOMDocument();
        $dom->preserveWhiteSpace = false;
        $dom->formatOutput = true;
        // silence warnings and erros during parsing.
        // More at http://php.net/manual/en/function.libxml-use-internal-errors.php
        $oldUseErrors = libxml_use_internal_errors(true);
        $dom->loadHTML($html);
        // restore old value
        libxml_use_internal_errors($oldUseErrors);

        $scripts = [];
        while (($script = $dom->getElementsByTagName("script")) && $script->length) {
            $node = $script->item(0);

            $tempDom = new DOMDocument();
            $clonedNode = $node->cloneNode(true);
            $tempDom->appendChild($tempDom->importNode($clonedNode, true));
            $scripts[] = $tempDom->saveHTML();

            $node->parentNode->removeChild($node);
        }

        $html = $dom->saveHTML();
        $scripts = implode("", $scripts);
        $html = str_replace('</body>', $scripts . '</body>', $html);

        $response->setBody($html);
    }
}
