<?php

class Mage_Page_Block_Html_Topmenu_Custom extends Mage_Page_Block_Html_Topmenu {
	protected function _getHtml(Varien_Data_Tree_Node $menuTree, $childrenWrapClass)
    {
        $html = '';

        $children = $menuTree->getChildren();
        $parentLevel = $menuTree->getLevel();
        $childLevel = is_null($parentLevel) ? 0 : $parentLevel + 1;

        $counter = 1;
        $childrenCount = $children->count();

        $parentPositionClass = $menuTree->getPositionClass();
        $itemPositionClassPrefix = $parentPositionClass ? $parentPositionClass . '-' : 'nav-';

        foreach ($children as $child) {

            $child->setLevel($childLevel);
            $child->setIsFirst($counter == 1);
            $child->setIsLast($counter == $childrenCount);
            $child->setPositionClass($itemPositionClassPrefix . $counter);

            $outermostClassCode = '';
            $outermostClass = $menuTree->getOutermostClass();

            // Get ID of child
            $nodeId = $child->getId();
			$ChildId = str_replace('category-node-', '', $nodeId);

            if ($childLevel == 0 && $outermostClass) {
            	// If on category page
            	if (!empty(Mage::registry('current_category'))) {
            		// Check if child if match current category id, add active class
	            	$active = "";
	            	if (Mage::registry('current_category')->getId() == $ChildId) $active = "active";
            	}
            	

                $outermostClassCode = ' class="' . $outermostClass . " " . $active . ' nav-link " ';
                $child->setClass($outermostClass);
            }

            $html .= '<li ' . $this->_getRenderedMenuItemAttributes($child) . '>';
            $html .= '<a href="' . $child->getUrl() . '" ' . $outermostClassCode . '><span>'
                . $this->escapeHtml($child->getName()) . '</span></a>';

            /* HIDE level 2
            if ($child->hasChildren()) {
                if (!empty($childrenWrapClass)) {
                    $html .= '<div class="' . $childrenWrapClass . '">';
                }
                $html .= '<ul class="level' . $childLevel . '">';
                $html .= $this->_getHtml($child, $childrenWrapClass);
                $html .= '</ul>';

                if (!empty($childrenWrapClass)) {
                    $html .= '</div>';
                }
            } */
            $html .= '</li>';

            $counter++;
        }

        return $html;
    }
}
