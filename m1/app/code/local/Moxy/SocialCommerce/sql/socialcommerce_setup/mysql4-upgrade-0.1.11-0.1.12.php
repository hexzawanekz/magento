<?php
/** @var Mage_Core_Model_Resource_Setup $installer */
$installer = $this;

$installer->startSetup();

//==========================================================================
// Collection Banner
//==========================================================================

/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter(array(0), $withAdmin = false)
    ->addFieldToFilter('identifier', 'collection_banner_block')
    ->getFirstItem();

if ($block->getId()) $block->delete();// if exists then delete
$content = <<<EOD
 <div class="collection-top-banner">
    <div class="static_cms">
        <div class="row">
            <div class="container">
                <a href="https://www.orami.co.id/collections/category/world-tourism-day"
                   target="_blank"><img style="width: 100%; display: inline;" alt=""
                                        src="./index_files/WorldTourismDay-TopBanner.jpg"></a>
            </div>
        </div>
    </div>
</div>
EOD;

$block = Mage::getModel('cms/block');
$block->setTitle('Collection Top Banner');
$block->setIdentifier('collection_banner_block');
$block->setStores(array(0));
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================

$installer->endSetup();