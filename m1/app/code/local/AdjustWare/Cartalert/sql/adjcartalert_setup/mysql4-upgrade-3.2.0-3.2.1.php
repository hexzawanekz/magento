<?php
$installer = $this;
$installer->startSetup();
	$conn = $installer->getConnection();
	$tranTbl = $installer->getTable('adjcartalert/coupon_history');
	if(!$conn->isTableExists($tranTbl)) {
		$tranTbl = $conn->newTable($tranTbl)
			->addColumn('history_id',Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
				'unsigned'		=> true,
				'nullable'		=> false,
				'auto_increment'=> true,
				'primary'		=> true,
			),'Coupon History ID')
			
			->addColumn('customer_email',Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(				
				'nullable'	=> false,
				'default'	=> null,
			),'Customer Email')
			->addColumn('coupon',Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
				'nullable'  => true,
				'default'   => null,
			),'All coupons were sent for customer via email')
			->addColumn('coupon_usesage',Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
				'nullable'  => true,
				'default'   => 0,
			),'Time of sending coupon via email')
			->setComment('Coupon history');
		$conn->createTable($tranTbl);
	}
$installer->endSetup(); 