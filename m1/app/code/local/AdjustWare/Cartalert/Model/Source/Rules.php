<?php
/**
 * Abandoned Carts Alerts Pro for 1.3.x-1.7.0.0 - 18/06/13
 *
 * @category:    AdjustWare
 * @package:     AdjustWare_Cartalert
 * @version      3.2.0
 * @license:     drgC8qINeCMngTvUvXAZ1xPvlqyPFUMfKab7Ba1HhZ
 * @copyright:   Copyright (c) 2014 AITOC, Inc. (http://www.aitoc.com)
 */
?>
<?php
class AdjustWare_Cartalert_Model_Source_Rules extends Varien_Object
{
    public function toOptionArray()
    {
        $rules = Mage::getModel('salesrule/rule')->getCollection()->addFieldToFilter('is_active',array('eq'=>1));
		$_rules=array();
		foreach($rules as $rule){
			$_rules[]=array('value' => $rule->getId(), 'label' => Mage::helper('adjcartalert')->__($rule->getName()));
		}
        array_unshift($_rules,array('value'=>'', 'label'=>Mage::helper('adjcartalert')->__('-- Please Select --')));
        return $_rules;
    }
} 