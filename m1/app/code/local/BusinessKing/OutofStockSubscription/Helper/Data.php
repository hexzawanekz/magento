<?php

/**
 * @category   BusinessKing
 * @package    BusinessKing_OutofStockSubscription
 */

/**
 * Out of Stock Subscription data helper
 */
class BusinessKing_OutofStockSubscription_Helper_Data extends Mage_Core_Helper_Abstract {

    protected $_utm_array = array();

    public function getUtm() {
        $utm = array();
        if (!empty($_COOKIE['__utmz'])) {
            $pattern = "/(utmcsr=([^\|]*)[\|]?)|(utmccn=([^\|]*)[\|]?)|(utmcmd=([^\|]*)[\|]?)|(utmctr=([^\|]*)[\|]?)|(utmcct=([^\|]*)[\|]?)/i";
            preg_match_all($pattern, $_COOKIE['__utmz'], $matches);
            if (!empty($matches[0])) {
                foreach ($matches[0] as $match) {
                    $pair = null;
                    $match = trim($match, "|");
                    list($k, $v) = explode("=", $match);
                    $utm[$k] = $v;
                }
            }
        }
        $this->_utm_array = $utm;
        return $utm;
    }

    public function getUtmSource() {
        return $this->_utm_array['utmcsr'] ? $this->_utm_array['utmcsr'] : '';
    }

    public function getUtmMedium() {
        return $this->_utm_array['utm_medium'] ? $this->_utm_array['utm_medium'] : '';
    }

    public function getUtmCampaign() {
        return $this->_utm_array['utmccn'] ? $this->_utm_array['utmccn'] : '';
    }

    public function getUtmContent() {
        return $this->_utm_array['utmcct'] ? $this->_utm_array['utmcct'] : '';
    }

    public function getUtmTerm() {
        return $this->_utm_array['utmctr'] ? $this->_utm_array['utmctr'] : '';
    }
}
