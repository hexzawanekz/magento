<?php

/**
 * @category    BusinessKing
 * @package     BusinessKing_OutofStockSubscription
 */
class BusinessKing_OutofStockSubscription_Model_Observer
{
    const OUTOFSTOCKSUBSCRIPTION_MAIL_TEMPLATE = 'outofstock_subscription';

    public function sendEmailToOutofStockSubscription($observer)
    {
        $product = $observer->getEvent()->getProduct();
        if ($product) {
            if ($product->getStockItem()) {
                /*send email when status of all item in group product is in stock*/
                /*Case 1: update child product (simple product) of group product in admin*/
                if($product->getTypeId() == 'simple' ) {
                    $productId = $product->getId();
                    $parentIds = Mage::getModel('catalog/product_type_grouped')->getParentIdsByChild($productId);
                    if($parentIds) {
                        foreach ($parentIds as $idParent) {
                            $parentProd = Mage::getModel('catalog/product')->load($idParent);
                            $this->actionSendMail($parentProd);
                        }
                    }
                }
                /*End Case 1: update child product (simple product) of group product in admin*/
                /*end send email when status of all item in group product is in stock*/

                $this->actionSendMail($product);
            }
        }
        //return $this;
    }

    public function actionSendMail($product) {
        $thaiStore = Mage::app()->getStore(17);
        Mage::app()->setCurrentStore($thaiStore);

        /*send email when status of all item in group product is in stock*/
        /*Case 2: update group product in admin*/
        $isInStockAllItemGroup = true;
        if ($product->getTypeId() == 'grouped') {
            $_associatedProducts = $product->getTypeInstance(true)->getAssociatedProducts($product);
            foreach ($_associatedProducts as $_item) {
                if (!$_item->getIsInStock()) {
                    $isInStockAllItemGroup = false;
                    break;
                }
            }
        }
        /*end Case 2: update group product in admin*/
        /*end send email when status of all item in group product is in stock*/

        $stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product->getId());
        //$isInStock = $product->getStockItem()->getIsInStock();
        $isInStock = $stockItem->getIsInStock();

        if ($isInStock>=1 && $isInStockAllItemGroup) {
            $subscriptions = Mage::getResourceModel('outofstocksubscription/info')->getSubscriptions($product->getId());
            if (count($subscriptions) > 0) {
                $translate = Mage::getSingleton('core/translate');
                foreach ($subscriptions as $subscription) {
                    // instead of getting system default email and name,
                    // get email from out of stock configuration first
                    $storeId = $subscription['store_id'];
//                    $storeIds = $_product->getStoreIds();
//                    $prodUrl = Mage::app()->getStore($storeId)->getBaseUrl();
//                    $prodUrl = str_replace("/index.php", "", $prodUrl);
//                    $prodUrl = $prodUrl.$product->getUrlPath();
                    $prodUrl = $product->getUrlInStore(array("_store" => "moxyen",'_ignore_category' => true));
                    // get email template
                    $emailTemplate = Mage::getStoreConfig('outofstocksubscription/mail/template', $storeId);
                    if (!is_numeric($emailTemplate)) {
                        $emailTemplate = self::OUTOFSTOCKSUBSCRIPTION_MAIL_TEMPLATE;
                    }
                    $_sender = '';
                    if(Mage::getStoreConfig('outofstocksubscription/mail/senderemail', $storeId)
                        && Mage::getStoreConfig('outofstocksubscription/mail/sendername', $storeId)
                    ) {
                        $_sender['email'] = Mage::getStoreConfig('outofstocksubscription/mail/senderemail', $storeId);
                        $_sender['name'] = Mage::getStoreConfig('outofstocksubscription/mail/sendername', $storeId);
                    }
                    else {
                        $_sender = 'support';
                    } // else it's default value of extension
                    $translate->setTranslateInline(false);

                    // Get customer name
                    /** @var Mage_Customer_Model_Customer $customerModel */
                    $customerModel = Mage::getModel('customer/customer');
                    $loadByMail = $customerModel->loadByEmail($subscription['email']);

                    if($loadByMail->getFirstname()) {
                        $name = $loadByMail->getFirstname();
                    } else {
                        $name = substr($subscription['email'], 0, strpos($subscription['email'], "@"));
                        $name = ucfirst($name);
                    }

                    Mage::getModel('core/email_template')
                        ->setDesignConfig(array('area'=>'frontend', 'store'=> $storeId))
                        ->sendTransactional(
                            $emailTemplate,
                            $_sender,
                            $subscription['email'],
                            '',
                            array(
                                'customer' => $name,
                                'product' => $product->getName(),
                                'product_url'  => $prodUrl,
                                'product_image'=> Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . 'catalog/product' . $product->getThumbnail(),
                                'date' => $subscription['date'],
                                'product_price' => ($product->getPrice())?'Price :'.Mage::helper('core')->currency($product->getPrice(), true, false):''
                            ),
                            $storeId
                        );
                    $translate->setTranslateInline(true);
                    Mage::getResourceModel('outofstocksubscription/info')->deleteSubscription($subscription['id']);
                }
            }
        }
        Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
    }

    public function cancelOrderItem($observer)
    {
        $item = $observer->getEvent()->getItem();

        $productId = $item->getProductId();
        if ($productId) {
            $subscriptions = Mage::getResourceModel('outofstocksubscription/info')->getSubscriptions($productId);
            if (count($subscriptions) > 0) {

                $product = Mage::getModel('catalog/product')->load($productId);
                $prodUrl = Mage::getBaseUrl();
                $prodUrl = str_replace("/index.php", "", $prodUrl);
                $prodUrl = $prodUrl.$product->getData('url_path');

                $storeId = Mage::app()->getStore()->getId();

                // get email template
                $emailTemplate = Mage::getStoreConfig('outofstocksubscription/mail/template', $storeId);
                if (!is_numeric($emailTemplate)) {
                    $emailTemplate = self::OUTOFSTOCKSUBSCRIPTION_MAIL_TEMPLATE;
                }

                $translate = Mage::getSingleton('core/translate');

                foreach ($subscriptions as $subscription) {

                    $translate->setTranslateInline(false);
                    Mage::getModel('core/email_template')
                        ->setDesignConfig(array('area'=>'frontend', 'store'=>$storeId))
                        ->sendTransactional(
                            $emailTemplate,
                            'support',
                            $subscription['email'],
                            '',
                            array(
                                'product'     => $product->getName(),
                                'product_url' => $prodUrl,
                            ));
                    $translate->setTranslateInline(true);

                    Mage::getResourceModel('outofstocksubscription/info')->deleteSubscription($subscription['id']);
                }
            }
        }
    }
}
