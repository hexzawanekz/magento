<?php
/**
 * Developed by Acommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.Acommerce.asia/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade CustomerHistory to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_CustomerHistory
 * @copyright   Copyright (c) 2012 Acommerce (http://www.Acommerce.asia)
 * @license     http://www.Acommerce.asia/LICENSE-E.txt
*/

class Acommerce_CustomerHistory_Helper_Data extends Mage_Core_Helper_Abstract
{
	const CONFIG_PATH = 'customerhistory/saleshistory/';
	public function getConfigData($field=null) {
		if($field) {
			return Mage::getStoreConfig($this->_getConfigFieldPath($field));
		}
		return null;
	}
	protected function _getConfigFieldPath($field) {
		return self::CONFIG_PATH.$field;
	}
	public function showNameEmail() {
		return (bool)$this->getConfigData('showname_email');
	}
}
