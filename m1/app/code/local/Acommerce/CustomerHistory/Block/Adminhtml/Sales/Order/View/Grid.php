<?php
/**
 * Developed by Acommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.Acommerce.asia/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade CustomerHistory to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_CustomerHistory
 * @copyright   Copyright (c) 2012 Acommerce (http://www.Acommerce.asia)
 * @license     http://www.Acommerce.asia/LICENSE-E.txt
*/

class Acommerce_CustomerHistory_Block_Adminhtml_Sales_Order_View_Grid extends Acommerce_CustomerHistory_Block_Adminhtml_Customer_View_Grid
{
	protected function _prepareCollection() {
		$collection = Mage::getModel('customerhistory/history')->getCollection()
						->addFieldToFilter('status',Acommerce_CustomerHistory_Model_Source_Status::STATUS_ENABLE)
						->addFieldToFilter('customer_id',$this->_getCustomerId());						
		$this->setCollection($collection);
		
		return Mage_Adminhtml_Block_Widget_Grid::_prepareCollection();
	}
	protected function _getCustomerId() {
		if($id = $this->getRequest()->getParam('customer_id')) {
			return $id;
		}
		if($order = Mage::registry('current_order')) {
			return $order->getCustomerId();
		}
		return null;
	}
	public function getGridUrl() {
		return $this->getUrl('adminhtml/customer_history/salesgrid',array('customer_id'=>$this->_getCustomerId()));	
	}
	protected function _prepareColumns() {		
		parent::_prepareColumns();
		$this->removeColumn('status');
		$this->removeColumn('action');
		$this->_exportTypes = array();
		return $this;
	}
}
