<?php
/**
 * Developed by Acommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.Acommerce.asia/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade CustomerHistory to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_CustomerHistory
 * @copyright   Copyright (c) 2012 Acommerce (http://www.Acommerce.asia)
 * @license     http://www.Acommerce.asia/LICENSE-E.txt
*/

class Acommerce_CustomerHistory_Block_Adminhtml_Customer_View extends Mage_Adminhtml_Block_Template
 implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
	protected function _prepareLayout() {
		parent::_prepareLayout();
		$this->setChild('history_grid',$this->getLayout()->createBlock('customerhistory/adminhtml_customer_view_grid'));		
		$this->setChild('add',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                    'label'     => Mage::helper('adminhtml')->__('Add History'),
                    'onclick'   => "window.open('".$this->getUrl('adminhtml/customer_history/new',array('customer_id'=>Mage::registry('current_customer')->getId()))."')",
                ))
        );
        return $this;
    }
    protected function _toHtml() {        
        $html = parent::_toHtml();
        $html .= $this->getChildHtml('history_grid');
        $html .= $this->getChildHtml('add');
        return $html;
    }
    public function getTabLabel() {
        return Mage::helper('customerhistory')->__('History');
    }

    public function getTabTitle() {
        return Mage::helper('customer')->__('History');
    }

    public function canShowTab() {
        if (Mage::registry('current_customer')->getId()) {
            return true;
        }
        return false;
    }

    public function isHidden() {
        if (Mage::registry('current_customer')->getId()) {
            return false;
        }
        return true;
    }
}
