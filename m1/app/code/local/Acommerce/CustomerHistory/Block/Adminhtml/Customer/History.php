<?php
/**
 * Developed by Acommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.Acommerce.asia/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade CustomerHistory to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_CustomerHistory
 * @copyright   Copyright (c) 2012 Acommerce (http://www.Acommerce.asia)
 * @license     http://www.Acommerce.asia/LICENSE-E.txt
*/

class Acommerce_CustomerHistory_Block_Adminhtml_Customer_History extends Mage_Adminhtml_Block_Widget_Grid_Container
{
	public function __construct() {			
		$this->_controller = 'adminhtml_customer_history';
		$this->_blockGroup = 'customerhistory';
		$this->_headerText = Mage::helper('customerhistory')->__('History Manager');
		$this->_addButtonLabel = Mage::helper('customerhistory')->__('Add History');
		parent::__construct();
	}
	public function getCreateUrl() {
        return $this->getUrl('*/*/new');
    }
}
