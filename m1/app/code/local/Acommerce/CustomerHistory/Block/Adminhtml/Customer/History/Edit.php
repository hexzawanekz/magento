<?php
/**
 * Developed by Acommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.Acommerce.asia/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade CustomerHistory to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_CustomerHistory
 * @copyright   Copyright (c) 2012 Acommerce (http://www.Acommerce.asia)
 * @license     http://www.Acommerce.asia/LICENSE-E.txt
*/

class Acommerce_CustomerHistory_Block_Adminhtml_Customer_History_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct() {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'customerhistory';
        $this->_controller = 'adminhtml_customer_history';
        
        $this->_updateButton('save', 'label', Mage::helper('customerhistory')->__('Save'));
        $this->_updateButton('delete', 'label', Mage::helper('customerhistory')->__('Delete'));
		
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('customerhistory_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'customerhistory_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'customerhistory_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    public function getHeaderText() {
        if( Mage::registry('customerhistory_data') && Mage::registry('customerhistory_data')->getId() ) {
            return Mage::helper('customerhistory')->__("Edit History");
        } else {
            return Mage::helper('customerhistory')->__('Add History');
        }
    }
    public function getBackUrl() {
		$data = Mage::registry('customerhistory_data');
        return ($data->getId())?$this->getUrl('*/*/'):$this->getUrl('*/*/new');
    }
}
