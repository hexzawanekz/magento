<?php
/**
 * Developed by Acommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.Acommerce.asia/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade CustomerHistory to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_CustomerHistory
 * @copyright   Copyright (c) 2012 Acommerce (http://www.Acommerce.asia)
 * @license     http://www.Acommerce.asia/LICENSE-E.txt
*/
$installer = $this;
$installer->startSetup();
$conn = $installer->getConnection();
$history = $installer->getTable('customerhistory/history');
if(!$conn->isTableExists($history)) {
	$history = $conn->newTable($history)
		->addColumn('history_id',Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
			'unsigned'			=> true,
			'nullable'			=> false,
			'identity'			=> true,
			'primary'			=> true,
		),'History ID')
		->addColumn('customer_id',Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
			'unsigned'	=> true,
			'nullable'	=> false,
			'primary'	=> true,
		),'Customer ID')
		->addColumn('content',Varien_Db_Ddl_Table::TYPE_TEXT, 20, array(
			'nullable'  => true,
			'default'   => null,
		),'History Content')
		->addColumn('created_at',Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
			'nullable'  => false,
			'default'   => '0000-00-00 00:00:00',
		),'History created at')			
		->addColumn('updated_at',Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
			'nullable'  => false,
			'default'   => Varien_Db_Ddl_Table::TIMESTAMP_INIT_UPDATE,
		),'History updated at')
		->addColumn('status',Varien_Db_Ddl_Table::TYPE_TINYINT, null, array(
			'nullable'  => false,
			'default'   => 1,
		),'History Status')						
		->setComment('Customer History');
	$conn->createTable($history);
}
$installer->endSetup();
