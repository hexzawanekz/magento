<?php
/**
 * Developed by Acommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.Acommerce.asia/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade SyncFiles to newer
 * versions in the future.
 *
 * @category    Acommerce
 * @package     Acommerce_SyncFiles
 * @copyright   Copyright (c) 2012 Acommerce (http://www.Acommerce.asia)
 * @license     http://www.Acommerce.asia/LICENSE-E.txt
*/

class Acommerce_SyncFiles_Adminhtml_SyncFilesController extends Mage_Adminhtml_Controller_Action
{
	public function indexAction() {
		//$url = 'http://admin.petloft.com/cronjobs/syncfiles.php';
		//$http = new Varien_Http_Adapter_Curl();
		//$http->write(Zend_Http_Client::GET,$url, '1.1', array(),null);
        //$result = $http->read();
		//if (Zend_Http_Response::extractCode($result) == 200) {
		//	$this->_getSession()->addSuccess(Mage::helper('adminhtml')->__('Files were synchronized'));
		//}
		//else {
		//	$this->_getSession()->addError(Mage::helper('adminhtml')->__('Files were failed to sync'));
		//}
		exec('sudo /usr/sbin/syncsite > /dev/null');
		$this->_getSession()->addSuccess(Mage::helper('adminhtml')->__('Files were synchronized'));
		$this->_redirect('adminhtml/cache');
	}
}
