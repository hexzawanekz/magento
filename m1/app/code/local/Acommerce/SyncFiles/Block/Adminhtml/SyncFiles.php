<?php
/**
 * Developed by Acommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.Acommerce.asia/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade SyncFiles to newer
 * versions in the future.
 *
 * @category    Acommerce
 * @package     Acommerce_SyncFiles
 * @copyright   Copyright (c) 2012 Acommerce (http://www.Acommerce.asia)
 * @license     http://www.Acommerce.asia/LICENSE-E.txt
*/

class Acommerce_SyncFiles_Block_Adminhtml_SyncFiles extends Mage_Adminhtml_Block_Template
{
	
	protected function _prepareLayout() {
        $this->getLayout()->getBlock('cache')
		->addButton('syncfiles', array(
            'label'     => Mage::helper('core')->__('Sync Files'),
            'onclick'   => 'setLocation(\'' . $this->getSyncFilesUrl() .'\')',
            'class'     => 'save',
        ),0,-1);
        return parent::_prepareLayout();
    }
	public function getSyncFilesUrl() {
		return $this->getUrl('adminhtml/syncFiles/');
	}
}
