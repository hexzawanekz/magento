<?php
/**
 * Acommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Webtex EULA that is bundled with
 * this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.acommerce.asia/LICENSE-1.0.html
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@webtex.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extension
 * to newer versions in the future. If you wish to customize the extension
 * for your needs please refer to http://www.acommerce.asia/ for more information
 * or send an email to sales@webtex.com
 *
 * @category   Acommerce
 * @package    Acommerce_Publishers
 * @copyright  Copyright (c) 2010 Acommerce (http://www.acommerce.asia/)
 * @license    http://www.acommerce.asia/LICENSE-1.0.html
 */

/**
 * Publishers extension
 *
 * @category   Acommerce
 * @package    Acommerce_Publishers
 * @author     Acommerce Dev Team <dev@acommerce.asia>
 */

/* @var $installer Acommerce_Publishers_Model_Mysql4_Setup */
$installer = $this;
$installer->startSetup();
//$this->_conn->addColumn($this->getTable('sales_flat_order'), 'is_exported', 'INTEGER(1) DEFAULT 0');
//$this->_conn->addColumn($this->getTable('sales_flat_order_grid'), 'is_exported', 'INTEGER(1) DEFAULT 0');
$installer->endSetup();