<?php

class MageExt_FixRound_Model_Store extends Mage_Core_Model_Store
{
     public function roundPrice($price)
    {
		return $price;
    }

    public function formatPrice($price, $includeContainer = true)
    {
        if ($this->getCurrentCurrency()) {
            return $this->getCurrentCurrency()->format(round($price,2), array(), $includeContainer);
        }
        return $price;
    }
}
