<?php
$installer = $this;
/* @var $installer Mage_Catalog_Model_Resource_Setup */
$installer->startSetup();

$sql = <<<SQLTEXT
ALTER TABLE `freebies` ADD COLUMN `exception_mode` TINYINT NULL;
SQLTEXT;

$installer->run($sql);

$installer->endSetup();