<?php
$installer = $this;
/* @var $installer Mage_Catalog_Model_Resource_Setup */
$installer->startSetup();

$sql = <<<SQLTEXT
ALTER TABLE `freebies` MODIFY COLUMN `price_from` INT(11), MODIFY COLUMN `freebie_num` INT(11);
SQLTEXT;

$installer->run($sql);

$installer->endSetup();