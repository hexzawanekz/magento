<?php

class SM_Freebies_Adminhtml_FreebiesController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->loadLayout()->_setActiveMenu("freebies/freebies")->_addBreadcrumb(Mage::helper("adminhtml")->__("Freebies  Manager"), Mage::helper("adminhtml")->__("Freebies Manager"));
        return $this;
    }

    public function categoriesJsonAction()
    {
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('freebies/adminhtml_freebies_edit_tab_addition')
                ->getCategoryChildrenJson($this->getRequest()->getParam('category'))
        );
    }

    public function indexAction()
    {
        $this->_title($this->__("Freebies Rules Manager"));

        $this->_initAction();
        $this->renderLayout();
    }

    public function editAction()
    {
        $this->_title($this->__("Edit Item"));

        $id = $this->getRequest()->getParam("id");
        $model = Mage::getModel("freebies/freebies")->load($id);
        if ($model->getId()) {
            Mage::register("freebies_data", $model);
            $this->loadLayout();
            $this->_setActiveMenu("freebies/freebies");
            $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Freebies Manager"), Mage::helper("adminhtml")->__("Freebies Manager"));
            $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Freebies Description"), Mage::helper("adminhtml")->__("Freebies Description"));
            $this->getLayout()->getBlock("head")->setCanLoadExtJs(true);
            $this->_addContent($this->getLayout()->createBlock("freebies/adminhtml_freebies_edit"))->_addLeft($this->getLayout()->createBlock("freebies/adminhtml_freebies_edit_tabs"));
            $this->renderLayout();
        } else {
            Mage::getSingleton("adminhtml/session")->addError(Mage::helper("freebies")->__("Item does not exist."));
            $this->_redirect("*/*/");
        }
    }

    public function newAction()
    {
        $this->_title($this->__("New Item"));

        $id = $this->getRequest()->getParam("id");
        $model = Mage::getModel("freebies/freebies")->load($id);

        $data = Mage::getSingleton("adminhtml/session")->getFormData(true);
        if (!empty($data)) {
            $model->setData($data);
        }

        Mage::register("freebies_data", $model);

        $this->loadLayout();
        $this->_setActiveMenu("freebies/freebies");

        $this->getLayout()->getBlock("head")->setCanLoadExtJs(true);

        $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Freebies Manager"), Mage::helper("adminhtml")->__("Freebies Manager"));
        $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Freebies Description"), Mage::helper("adminhtml")->__("Freebies Description"));


        $this->_addContent($this->getLayout()->createBlock("freebies/adminhtml_freebies_edit"))->_addLeft($this->getLayout()->createBlock("freebies/adminhtml_freebies_edit_tabs"));

        $this->renderLayout();

    }

    public function saveAction()
    {

        $post_data = $this->getRequest()->getPost();

        if(isset($post_data['category_ids'])){
            $arrIds = explode(',', $post_data['category_ids']);
            $arrIds = array_unique($arrIds);
            foreach($arrIds as $key => $value){
                if($value == '')
                    unset($arrIds[$key]);
            }
            $post_data['category_ids'] = implode(',', $arrIds);
        }

        if ($post_data) {

            try {

                if (!$this->getRequest()->getParam("id")) {
                    $checker = Mage::getModel("freebies/freebies")
                        ->getCollection()
                        ->addFieldToSelect('*')
                        ->addFieldToFilter('store_id', $this->getRequest()->getParam('store_id'));
                    if ($checker->count() > 0) {
                        Mage::getSingleton("adminhtml/session")->addError(Mage::helper("adminhtml")->__("Freebie rule for current store view already exists."));
                        $this->_redirect("*/*/");
                        return;
                    }
                }

                $model = Mage::getModel("freebies/freebies")
                    ->addData($post_data)
                    ->setId($this->getRequest()->getParam("id"))
                    ->save();

                Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Freebie rule has been saved successfully."));
                Mage::getSingleton("adminhtml/session")->setFreebiesData(false);

                if ($this->getRequest()->getParam("back")) {
                    $this->_redirect("*/*/edit", array("id" => $model->getId()));
                    return;
                }
                $this->_redirect("*/*/");
                return;
            } catch (Exception $e) {
                Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                Mage::getSingleton("adminhtml/session")->setFreebiesData($this->getRequest()->getPost());
                $this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
                return;
            }

        }
        $this->_redirect("*/*/");
    }


    public function deleteAction()
    {
        if ($this->getRequest()->getParam("id") > 0) {
            try {
                $model = Mage::getModel("freebies/freebies");
                $model->setId($this->getRequest()->getParam("id"))->delete();
                Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Freebie rule has been deleted successfully."));
                $this->_redirect("*/*/");
            } catch (Exception $e) {
                Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                $this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
            }
        }
        $this->_redirect("*/*/");
    }


    public function massRemoveAction()
    {
        try {
            $ids = $this->getRequest()->getPost('ids', array());
            foreach ($ids as $id) {
                $model = Mage::getModel("freebies/freebies");
                $model->setId($id)->delete();
            }
            Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Freebie rule(s) has been deleted successfully."));
        } catch (Exception $e) {
            Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
        }
        $this->_redirect('*/*/');
    }

}
