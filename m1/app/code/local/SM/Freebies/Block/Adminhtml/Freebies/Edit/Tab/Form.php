<?php

class SM_Freebies_Block_Adminhtml_Freebies_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {

        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset("freebies_form", array("legend" => Mage::helper("freebies")->__("Information")));


        $fieldset->addField("price_from", "text", array(
            "label"     => Mage::helper("freebies")->__("Minimum Product's Total Price"),
            "class"     => "required-entry validate-number validate-number-range validate-not-negative-number",
            "required"  => true,
            "name"      => "price_from",
        ));

        $fieldset->addField("freebie_num", "text", array(
            "label"     => Mage::helper("freebies")->__("Able to pick Freebie Products"),
            "class"     => "required-entry validate-number validate-number-range validate-not-negative-number",
            "required"  => true,
            "name"      => "freebie_num",
        ));

        // get store switcher or a hidden field with its id
        if (!Mage::app()->isSingleStoreMode()) {
            $fieldset->addField(
                'store_id',
                'select',
                array(
                    'label'     => Mage::helper("freebies")->__('Store View'),
                    'class'     => 'required-entry',
                    'required'  => true,
                    'name'      => 'store_id',
                    'values'    => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(true, false),
                    'disabled'  => $this->getRequest()->getParam('id') ? true : false,
                    'readonly'  => $this->getRequest()->getParam('id') ? true : false,
                )
            );
        } else {
            $fieldset->addField(
                'store_id',
                'hidden',
                array(
                    'name'      => 'store_id',
                    'value'     => Mage::app()->getStore(true)->getId(),
                )
            );
        }


        if (Mage::getSingleton("adminhtml/session")->getFreebiesData()) {
            $form->setValues(Mage::getSingleton("adminhtml/session")->getFreebiesData());
            Mage::getSingleton("adminhtml/session")->setFreebiesData(null);
        } elseif (Mage::registry("freebies_data")) {
            $form->setValues(Mage::registry("freebies_data")->getData());
        }
        return parent::_prepareForm();
    }
}
