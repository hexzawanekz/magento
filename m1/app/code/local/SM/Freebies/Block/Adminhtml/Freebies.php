<?php


class SM_Freebies_Block_Adminhtml_Freebies extends Mage_Adminhtml_Block_Widget_Grid_Container
{

    public function __construct()
    {

        $this->_controller = "adminhtml_freebies";
        $this->_blockGroup = "freebies";
        $this->_headerText = Mage::helper("freebies")->__("Freebies Rules");
        $this->_addButtonLabel = Mage::helper("freebies")->__("Add New Rule");
        parent::__construct();

    }

}