<?php

class SM_Freebies_Block_Adminhtml_Freebies_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct()
    {
        parent::__construct();
        $this->setId("freebies_tabs");
        $this->setDestElementId("edit_form");
        $this->setTitle(Mage::helper("freebies")->__("Freebie Rule"));
    }

    protected function _beforeToHtml()
    {
        $this->addTab("form_section", array(
            "label" => Mage::helper("freebies")->__("Base Information"),
            "title" => Mage::helper("freebies")->__("Base Information"),
            "content" => $this->getLayout()->createBlock("freebies/adminhtml_freebies_edit_tab_form")->toHtml(),
        ));
        if($this->getRequest()->getParam('id')){
            $this->addTab("form_section2", array(
                "label" => Mage::helper("freebies")->__("Additional Rule"),
                "title" => Mage::helper("freebies")->__("Additional Rule"),
                "content" => $this->getLayout()->createBlock("freebies/adminhtml_freebies_edit_tab_addition")->toHtml(),
            ));
        }
        return parent::_beforeToHtml();
    }

}
