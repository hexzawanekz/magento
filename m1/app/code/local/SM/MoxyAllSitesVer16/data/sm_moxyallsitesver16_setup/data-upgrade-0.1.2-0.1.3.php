<?php

// init Magento
Mage::app();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

// get store ids
$en     = Mage::getModel('core/store')->load('en', 'code')->getId();// Petloft
$th     = Mage::getModel('core/store')->load('th', 'code')->getId();
$len    = Mage::getModel('core/store')->load('len', 'code')->getId();// Lafema
$lth    = Mage::getModel('core/store')->load('lth', 'code')->getId();
$ven    = Mage::getModel('core/store')->load('ven', 'code')->getId();// Venbi
$vth    = Mage::getModel('core/store')->load('vth', 'code')->getId();
$sen    = Mage::getModel('core/store')->load('sen', 'code')->getId();// Sanoga
$sth    = Mage::getModel('core/store')->load('sth', 'code')->getId();
$moxyen = Mage::getModel('core/store')->load('moxyen', 'code')->getId();// Moxy
$moxyth = Mage::getModel('core/store')->load('moxyth', 'code')->getId();

$allStoresTH = array($th, $lth, $vth, $sth, $moxyth);
$allStoresEN = array($en, $len, $ven, $sen, $moxyen);


//==========================================================================
// header_right_image en store
//==========================================================================
$blockTitle = "Header right image";
$blockIdentifier = "header_right_image";
$blockStores = $allStoresEN;
$blockIsActive = 1;

// delete old block if have
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');

$content = <<<EOD
<a href="{{store url}}">
    <img style="width: 100%;" src="{{media url='wysiwyg/header_right_image.png'}}" alt="" />
</a>
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$enBlock = Mage::getModel('cms/block');
$enBlock->setTitle($blockTitle);
$enBlock->setIdentifier('header_right_image');
$enBlock->setStores($blockStores);
$enBlock->setIsActive(1);
$enBlock->setContent($content);
$enBlock->save();
//==========================================================================
//==========================================================================

//==========================================================================
// header_right_image th store
//==========================================================================
$blockTitle = "Header right image Thai";
$blockIdentifier = "header_right_image";
$blockStores = $allStoresTH;
$blockIsActive = 1;

// delete old block if have
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');

$content = <<<EOD
<a href="{{store url}}">
    <img style="width: 100%;" src="{{media url='wysiwyg/header_right_image_th.png'}}" alt="" />
</a>
EOD;

/** @var Mage_Cms_Model_Block $enBlock */
$enBlock = Mage::getModel('cms/block');
$enBlock->setTitle($blockTitle);
$enBlock->setIdentifier($blockIdentifier);
$enBlock->setStores($blockStores);
$enBlock->setIsActive(1);
$enBlock->setContent($content);
$enBlock->save();
//==========================================================================
//==========================================================================