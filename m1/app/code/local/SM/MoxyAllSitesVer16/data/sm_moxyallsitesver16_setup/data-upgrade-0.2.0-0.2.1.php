<?php

// init Magento
Mage::app();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

// get store ids
$en     = Mage::getModel('core/store')->load('en', 'code')->getId();// Petloft
$th     = Mage::getModel('core/store')->load('th', 'code')->getId();
$len    = Mage::getModel('core/store')->load('len', 'code')->getId();// Lafema
$lth    = Mage::getModel('core/store')->load('lth', 'code')->getId();
$ven    = Mage::getModel('core/store')->load('ven', 'code')->getId();// Venbi
$vth    = Mage::getModel('core/store')->load('vth', 'code')->getId();
$sen    = Mage::getModel('core/store')->load('sen', 'code')->getId();// Sanoga
$sth    = Mage::getModel('core/store')->load('sth', 'code')->getId();
$moxyen = Mage::getModel('core/store')->load('moxyen', 'code')->getId();// Moxy
$moxyth = Mage::getModel('core/store')->load('moxyth', 'code')->getId();



//==========================================================================
// SEO Keyword (Lafema English)
//==========================================================================
$blockTitle = "Beauty SEO Keyword";
$blockIdentifier = "seo-keyword";
$blockStores = array($len);
$blockIsActive = 1;
$blockContent = <<<EOD
<div style="margin: 50px auto 30px;">
<p>LAFEMA is MOXY’s Beauty, Cosmetics and Fragrance shopping website in Thailand.</p>
<p>&nbsp;</p>
<p>Because Beauty gives Confidence, we believe that everybody should be able to be the best version of themselves. MOXY helps you chose the right products to be pretty and look young and healthy. Glow with beautiful skin, lips, and eyes. Be unforgettable thanks to seductive fragrance. Have fun and express yourself with colorful nails. Moisture and protect your skin for long-lasting youth and softness. All humans are different and beautiful in their own, unique way. MOXY reveals Thailand’s best beauty secrets. Discover what will make YOU shine!</p>
<p>&nbsp;</p>
<p>All of our products are 100% authentic and we work directly with international brands to distribute their products. Our wide selection includes more than 100 brands, including M.A.C., SKII, Dior, Lancome, Yves Saint Laurent, Urban Decay, Sleek, Clinique, CandyDoll, Catrice, Makeup Forever, Bisous Bisous, NYX, Tarte, Smasbox, Stila, Jill Stuart, Bareminerals, Sisley, Chanel, Giorgio Armani, Marc Jacobs, Tom Ford, Burberry,, Shiseido, Suqqu, Nars, Illamasqua, Laura Mercier, and many others.</p>
<p>&nbsp;</p>
<p>As the online destination for Women in Thailand, MOXY is dedicated to provide the best customer service: easy, fast and reliable. Your satisfaction is our priority. Please feel free to contact our customer service for any information. Multiple payment methods are available such as credit card, Pay Pal, and cash-on-delivery. Your order will be delivered to your doorstep within 2 to 3 days in Bangkok and 3 to 5 days in other regions of Thailand. Make your shopping easier than ever!</p>
<p>&nbsp;</p>
<p>MOXY is looking forward to serving you.</p>
</div>
EOD;
/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// SEO Keyword (Lafema Thai)
//==========================================================================
$blockTitle = "Beauty SEO Keyword";
$blockIdentifier = "seo-keyword";
$blockStores = array($lth);
$blockIsActive = 1;
$blockContent = <<<EOD
<div style="margin: 50px auto 30px;">
<p>Lafema รวมเครื่องสำอาง น้ำหอม และผลิตภัณฑ์เพื่อความงามของ คัดสรรโดย MOXY เพื่อสาวๆ โดยเฉพาะ</p>
<p>&nbsp;</p>
<p>เราเชื่อว่าความสวยทำให้ผู้หญิงรู้สึกมั่นใจ และสาวๆทุกคนสามารถสวยได้ในสไตล์ของตัวเอง MOXY จะมาเป็นตัวช่วยของคุณในการแนะนำสินค้าที่ดีที่สุด และเหมาะกับคุณที่สุด เพื่อคุณจะได้สวยและดูดีที่สุดในแบบของคุณเอง เผยออร่าที่อยู่ในตัวคุณผ่านผิวเปล่งประกาย ริมฝีปากอวบอิ่ม และดวงตาที่คมชัดให้คนอื่นได้ชื่นชม สนุกไปกับการโชว์ความสวยในแบบของคุณให้โลกได้รู้และค้นพบความงามที่ซ่อนอยู่โดยที่คุณไม่เคยรู้มาก่อน ทุกความลับเรื่องความสวย MOXY รวมไว้ที่นี่ที่เดียว</p>
<p>&nbsp;</p>
<p>สินค้าทั้งหมดของเราเป็นของแท้ 100% ซึ่งเราติดต่อกับแบรนด์ และตัวแทนจำหน่ายโดยตรงกับแบรนด์ดังกว่า 100 แบรนด์ทั่วโลก ไม่ว่าจะเป็น M.A.C., SKII, Dior, Lancome, Yves Saint Laurent, Urban Decay, Sleek, Clinique, CandyDoll, Catrice, Makeup Forever, Bisous Bisous, NYX, Tarte, Smasbox, Stila, Jill Stuart, Bareminerals, Sisley, Chanel, Giorgio Armani, Marc Jacobs, Tom Ford, Burberry, Shiseido, Suqqu, Nars, Illamasqua, Laura Mercier และแบรนด์ดังอื่นๆอีกมากมาย</p>
<p>&nbsp;</p>
<p>สะดวก รวดเร็ว และมั่นใจได้ คือสิ่งที่ MOXY บริการให้คุณ เพราะความพึงพอใจของคุณคือสิ่งที่สำคัญที่สุดสำหรับเรา คุณสามารถติดต่อ MOXY ผ่านทางศูนย์บริการลูกค้าเพื่อสอบถามเกี่ยวกับข้อมูลต่างๆได้ตลอดเวลา และยังสามารถเลือกวิธีการชำระเงินได้ตามสะดวกไม่ว่าจะเป็น ชำระเงินผ่านบัตรเครดิตการ์ด Paypal หรือชำระเงินปลายทาง จากนั้นก็เตรียมตัวรับสินค้าที่จะถูกส่งตรงถึงหน้าบ้านคุณภายใน 2-3 วันทำการในเขตกรุงเทพฯ และ 3-5 วันทำการในเขตอื่น ไม่ว่าคุณจะอยู่ที่ไหน การช้อปปิ้งออนไลน์จะกลายเป็นเรื่องสนุกที่คุณทำได้ทุกวัน</p>
<p>&nbsp;</p>
<p>MOXY พร้อมที่จะให้บริการคุณแล้ววันนี้</p>
</div>
EOD;
/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// SEO Keyword (Moxy Fashion English)
//==========================================================================
$blockTitle = "Fashion SEO Keyword";
$blockIdentifier = "seo-keyword-fashion";
$blockStores = array($moxyen);
$blockIsActive = 1;
$blockContent = <<<EOD
<div style="margin: 50px auto 30px;">
<p>MOXY loves Fashion Accessories! Our selection covers jewelry, eyewear, bags, clutches and lingerie.</p>
<p>&nbsp;</p>
<p>Discover the best of Thai designers and International brands. All of our products are 100% authentic and we work directly with the brands to distribute their products.</p>
<p>&nbsp;</p>
<p>As the online destination for Women in Thailand, MOXY is dedicated to provide the best customer service: easy, fast and reliable. Your satisfaction is our priority. Please feel free to contact our customer service for any information. Multiple payment methods are available such as credit card, Pay Pal, and cash-on-delivery. Your order will be delivered to your doorstep within 2 to 3 days in Bangkok and 3 to 5 days in other regions of Thailand. Make your shopping easier than ever!</p>
<p>&nbsp;</p>
<p>MOXY is looking forward to serving you.</p>
</div>
EOD;
/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// SEO Keyword (Moxy Fashion Thai)
//==========================================================================
$blockTitle = "Fashion SEO Keyword";
$blockIdentifier = "seo-keyword-fashion";
$blockStores = array($moxyth);
$blockIsActive = 1;
$blockContent = <<<EOD
<div style="margin: 50px auto 30px;">
<p>MOXY รักแฟชั่น! เครื่องประดับ แว่นตา กระเป๋า คลัช และชุดชั้นในสุดเซ็กซี่ที่ทำให้สาวๆโดดเด่นมีสไตล์</p>
<p>&nbsp;</p>
<p>ช้อปสินค้าดีไซน์จากไทยดีไซน์เนอร์และแบรนด์ดังจากต่างประเทศ สินค้าทั้งหมดของเราเป็นของแท้ 100% ซึ่งเราติดต่อกับแบรนด์ และตัวแทนจำหน่ายโดยตรง</p>
<p>&nbsp;</p>
<p>สะดวก รวดเร็ว และมั่นใจได้ คือสิ่งที่ MOXY บริการให้คุณ เพราะความพึงพอใจของคุณคือสิ่งที่สำคัญที่สุดสำหรับเรา คุณสามารถติดต่อ MOXY ผ่านทางศูนย์บริการลูกค้าเพื่อสอบถามเกี่ยวกับข้อมูลต่างๆได้ตลอดเวลา และยังสามารถเลือกวิธีการชำระเงินได้ตามสะดวกไม่ว่าจะเป็น ชำระเงินผ่านบัตรเครดิตการ์ด Paypal หรือชำระเงินปลายทาง จากนั้นก็เตรียมตัวรับสินค้าที่จะถูกส่งตรงถึงหน้าบ้านคุณภายใน 2-3 วันทำการในเขตกรุงเทพฯ และ 3-5 วันทำการในเขตอื่น ไม่ว่าคุณจะอยู่ที่ไหน การช้อปปิ้งออนไลน์จะกลายเป็นเรื่องสนุกที่คุณทำได้ทุกวัน</p>
<p>&nbsp;</p>
<p>MOXY พร้อมที่จะให้บริการคุณแล้ววันนี้</p>
</div>
EOD;
/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// SEO Keyword (Moxy Electronics English)
//==========================================================================
$blockTitle = "Electronics SEO Keyword";
$blockIdentifier = "seo-keyword-electronics";
$blockStores = array($moxyen);
$blockIsActive = 1;
$blockContent = <<<EOD
<div style="margin: 50px auto 30px;">
<p>MOXY loves Tech & Gadgets! Discover our selection of modern, connected and feminine Electronics. Release your inner geek… with style!</p>
<p>&nbsp;</p>
<p>On Moxy,  you can find mobiles (phones, tablets, laptops), computers, audio (headphones, earphones, speakers), cameras (polaroid, digital…) and many others (smart robots, power banks, vacuum, etc).</p>
<p>&nbsp;</p>
<p>All of our products are 100% authentic and we work directly with international brands to distribute their products. Our wide selection includes brands, like Apple, Samsung, Arken, House of Marley, Ihealth, Moleskine and many others.</p>
<p>&nbsp;</p>
<p>As the online destination for Women in Thailand, MOXY is dedicated to provide the best customer service: easy, fast and reliable. Your satisfaction is our priority. Please feel free to contact our customer service for any information. Multiple payment methods are available such as credit card, Pay Pal, and cash-on-delivery. Your order will be delivered to your doorstep within 2 to 3 days in Bangkok and 3 to 5 days in other regions of Thailand. Make your shopping easier than ever!</p>
<p>&nbsp;</p>
<p>MOXY is looking forward to serving you.</p>
</div>
EOD;
/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// SEO Keyword (Moxy Electronics Thai)
//==========================================================================
$blockTitle = "Electronics SEO Keyword";
$blockIdentifier = "seo-keyword-electronics";
$blockStores = array($moxyth);
$blockIsActive = 1;
$blockContent = <<<EOD
<div style="margin: 50px auto 30px;">
<p>แก็ตเจ็ตและอุปการณ์ไฮเทคนี่แหละที่ MOXY หลงใหล ช้อปสินค้าอิเล็คทรอนิกส์มีสไตล์กับเรา</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>ที่ MOXY เรามีมือถือ แท็ปเล็ต คอมพิวเตอร์โน๊ตบุค หูฟัง ลำโพง กล้องโพลารอยด์ อุปกรณ์ชาร์จไฟ เพาเวอร์แบงค์ และสินค้าอิเล็คทรอนิกส์ทุกชิ้นที่มีดีไซน์โดดเด่นและแตกต่าง บ่งบอกความเป็นตัวตนของสาวๆที่ใช้ ไม่ว่าหยิบขึ้นมาใช้ตอนไหนเพื่อนๆก็ต้องทัก</p>
<p>&nbsp;</p>
<p>สินค้าทั้งหมดของเราเป็นของแท้ 100% ซึ่งเราติดต่อกับแบรนด์ และตัวแทนจำหน่ายโดยตรงกับแบรนด์ดังทั่วโลก เช่น Apple, Samsung, Arken, House of Marley, Ihealth, Moleskine และแบรนด์ดังอื่นๆอีกมากมาย</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>สะดวก รวดเร็ว และมั่นใจได้ คือสิ่งที่ MOXY บริการให้คุณ เพราะความพึงพอใจของคุณคือสิ่งที่สำคัญที่สุดสำหรับเรา คุณสามารถติดต่อ MOXY ผ่านทางศูนย์บริการลูกค้าเพื่อสอบถามเกี่ยวกับข้อมูลต่างๆได้ตลอดเวลา และยังสามารถเลือกวิธีการชำระเงินได้ตามสะดวกไม่ว่าจะเป็น ชำระเงินผ่านบัตรเครดิตการ์ด Paypal หรือชำระเงินปลายทาง จากนั้นก็เตรียมตัวรับสินค้าที่จะถูกส่งตรงถึงหน้าบ้านคุณภายใน 2-3 วันทำการในเขตกรุงเทพฯ และ 3-5 วันทำการในเขตอื่น ไม่ว่าคุณจะอยู่ที่ไหน การช้อปปิ้งออนไลน์จะกลายเป็นเรื่องสนุกที่คุณทำได้ทุกวัน</p>
<p>&nbsp;</p>
<p>MOXY พร้อมที่จะให้บริการคุณแล้ววันนี้</p>
</div>
EOD;
/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// SEO Keyword (Moxy Home Living English)
//==========================================================================
$blockTitle = "Home Living SEO Keyword";
$blockIdentifier = "seo-keyword-homeliving";
$blockStores = array($moxyen);
$blockIsActive = 1;
$blockContent = <<<EOD
<div style="margin: 50px auto 30px;">
<p>There is no place like Home, Sweet Home. Moxy has selected everything you need for a cosy and convenient living, from Home Décor and furniture to Home Appliances. Every room is covered: living room, bedding, bath, kitchen and dining, as well as lighting and appliances.</p>
<p>&nbsp;</p>
<p>All of our products are 100% authentic and we work directly with international brands to distribute their products. Our wide selection includes brands like Café Caps, Henry Cats & Friends, On Time, Springmate, Tin Home Toy, Pakamian, Ikea and many others.</p>
<p>&nbsp;</p>
<p>As the online destination for Women in Thailand, MOXY is dedicated to provide the best customer service: easy, fast and reliable. Your satisfaction is our priority. Please feel free to contact our customer service for any information. Multiple payment methods are available such as credit card, Pay Pal, and cash-on-delivery. Your order will be delivered to your doorstep within 2 to 3 days in Bangkok and 3 to 5 days in other regions of Thailand. Make your shopping easier than ever!</p>
<p>&nbsp;</p>
<p>MOXY is looking forward to serving you.</p>
</div>
EOD;
/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// SEO Keyword (Moxy Home Living Thai)
//==========================================================================
$blockTitle = "Home Living SEO Keyword";
$blockIdentifier = "seo-keyword-homeliving";
$blockStores = array($moxyth);
$blockIsActive = 1;
$blockContent = <<<EOD
<div style="margin: 50px auto 30px;">
<p>บ้านคือวิมานของเรา MOXY เลือกสรรของแต่งบ้าน เฟอร์นิเจอร์ และเครื่องใช้ในบ้านที่มอบความรู้สึกสะดวกและสบายให้กับทุกห้องในบ้านของคุณ ไม่ว่าจะเป็นห้องนั่งเล่น ห้องนอน ห้องน้ำ ห้องครัว และห้องทานอาหาร</p>
<p>&nbsp;</p>
<p>สินค้าทั้งหมดของเราเป็นของแท้ 100% ซึ่งเราติดต่อกับแบรนด์ และตัวแทนจำหน่ายโดยตรงกับแบรนด์ดังทั่วโลก เช่น Café Caps, Henry Cats & Friends, On Time, Springmate, Tin Home Toy, Pakamian, Ikea อื่นแบรนด์ดังอื่นๆอีกมาก</p>
<p>&nbsp;</p>
<p>สะดวก รวดเร็ว และมั่นใจได้ คือสิ่งที่ MOXY บริการให้คุณ เพราะความพึงพอใจของคุณคือสิ่งที่สำคัญที่สุดสำหรับเรา คุณสามารถติดต่อ MOXY ผ่านทางศูนย์บริการลูกค้าเพื่อสอบถามเกี่ยวกับข้อมูลต่างๆได้ตลอดเวลา และยังสามารถเลือกวิธีการชำระเงินได้ตามสะดวกไม่ว่าจะเป็น ชำระเงินผ่านบัตรเครดิตการ์ด Paypal หรือชำระเงินปลายทาง จากนั้นก็เตรียมตัวรับสินค้าที่จะถูกส่งตรงถึงหน้าบ้านคุณภายใน 2-3 วันทำการในเขตกรุงเทพฯ และ 3-5 วันทำการในเขตอื่น ไม่ว่าคุณจะอยู่ที่ไหน การช้อปปิ้งออนไลน์จะกลายเป็นเรื่องสนุกที่คุณทำได้ทุกวัน</p>
<p>&nbsp;</p>
<p>MOXY พร้อมที่จะให้บริการคุณแล้ววันนี้</p>
</div>
EOD;
/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// SEO Keyword (Mom and Baby English)
//==========================================================================
$blockTitle = "Mom and Baby SEO Keyword";
$blockIdentifier = "seo-keyword";
$blockStores = array($ven);
$blockIsActive = 1;
$blockContent = <<<EOD
<div style="margin: 50px auto 30px;">
<p>VENBI is MOXY’s Mom & Baby shopping website in Thailand.</p>
<p>&nbsp;</p>
<p>Shopping with us means that you will no longer have to drive around for hours looking for the best products for your babies. Everything can now be purchased from one place. We sell baby products for age groups between infancy up to 2 years of age. From Moxy, you can get your <a href="http://www.venbi.com/en/diapers.html" title="diapers">diapers</a> , <a href="http://www.venbi.com/en/baby-care/type/baby-wipes.html" title="wipes">wipes</a> , <a href="http://www.venbi.com/en/clothing-46/clothing/clothing.html" title="cloths">cloths</a> , <a href="http://www.venbi.com/en/catalogsearch/result/?q=bath" title="bathing products">bathing products</a> , <a title="toys" href="http://www.venbi.com/en/toys.html">toys</a> , accessories for moms, <a href="http://www.venbi.com/en/formula-feeding/formula/formula-baby.html" title="powdered milk">powdered milk</a> , and many other products.</p>
<p>&nbsp;</p>
<p>All of our products are 100% authentic and we work directly with international brands to distribute their products. Our wide selection includes <a href="http://www.venbi.com/en/diapers/popular-brands/mamy-poko.html" title="Mamy Poko">Mamy Poko</a> , <a href="http://www.venbi.com/en/diapers/popular-brands/merries.html" title="Merries">Merries</a> , <a href="http://www.venbi.com/en/formula-feeding/popular-brands/enfa.html" title="Enfalac">Enfalac</a> , <a href="http://www.venbi.com/en/catalogsearch/result/?q=enfa+pro" title="Enfapro">Enfapro</a> , <a href="http://www.venbi.com/en/diapers/popular-brands/drypers.html" title="Drypers">Drypers</a> , <a href="http://www.venbi.com/en/diapers/popular-brands/huggies.html" title="Huggies">Huggies</a> , <a title="DG" href="http://www.venbi.com/en/formula-feeding/popular-brands/dg.html">DG</a> , <a title="Peachy" href="http://www.venbi.com/en/formula-feeding/popular-brands/peachy.html">Peachy</a> , <a title="Badger" href="http://www.venbi.com/en/catalogsearch/result/?q=badger">Badger</a> , <a title="Babylove" href="http://www.venbi.com/en/diapers/popular-brands/baby-love.html">Babylove</a> , <a title="Sassy" href="http://www.venbi.com/en/toys/popular-brands/sassy.html">Sassy</a> , <a title="Similac" href="http://www.venbi.com/en/formula-feeding/popular-brands/similac.html">Similac</a> , <a title="Pigeon" href="http://www.venbi.com/en/baby-care/popular-brands/pigeon.html">Pigeon</a> , <a title="S-26" href="http://www.venbi.com/en/formula-feeding/popular-brands/s-26.html">S-26</a> , <a title="Bright Starts" href="http://www.venbi.com/en/toys/popular-brands/bright-starts.html">Bright Starts</a> , <a title="Tiny love" href="http://www.venbi.com/en/toys/popular-brands/tiny-love.html">Tiny love</a>  and many more.</p>
<p>&nbsp;</p>
<p>As the online destination for Women in Thailand, MOXY is dedicated to provide the best customer service: easy, fast and reliable. Your satisfaction is our priority. Please feel free to contact our customer service for any information. Multiple payment methods are available such as credit card, Pay Pal, and cash-on-delivery. Your order will be delivered to your doorstep within 2 to 3 days in Bangkok and 3 to 5 days in other regions of Thailand. Make your shopping easier than ever!</p>
<p>&nbsp;</p>
<p>MOXY is looking forward to serving you.</p>
</div>
EOD;
/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// SEO Keyword (Mom and Baby Thai)
//==========================================================================
$blockTitle = "Mom and Baby SEO Keyword";
$blockIdentifier = "seo-keyword";
$blockStores = array($vth);
$blockIsActive = 1;
$blockContent = <<<EOD
<div style="margin: 50px auto 30px;">
<p>Venbi รวมสินค้าแม่และเด็กที่ดีที่สุด คัดสรรโดย MOXY</p>
<p>&nbsp;</p>
<p>MOXY ประหยัดเวลาในการตระเวนหาของที่ดีที่สุดให้ลูกคุณ เพราะเราคัดสรรแล้วว่านี่แหละ คือสิ่งที่ดีที่สุด ครบทุกอย่างในเว็บเดียว ไม่ว่าจะเป็นผ้าอ้อม เสื้อผ้า เบบี้แคร์ ของเล่น และนมผง สินค้าสำหรับเด็กแรกเกิดถึง 2ขวบ  คุณแม่จึงมีเวลาอยู่กับลูกมากขึ้นนั่นเอง</p>
<p>&nbsp;</p>
<p>สินค้าทั้งหมดของเราเป็นของแท้ 100% ซึ่งเราติดต่อกับแบรนด์ และตัวแทนจำหน่ายโดยตรงกับแบรนด์ดังทั่วโลก เช่น <a href="http://www.venbi.com/en/diapers/popular-brands/mamy-poko.html" title="Mamy Poko">Mamy Poko</a> , <a href="http://www.venbi.com/en/diapers/popular-brands/merries.html" title="Merries">Merries</a> , <a href="http://www.venbi.com/en/formula-feeding/popular-brands/enfa.html" title="Enfalac">Enfalac</a> , <a href="http://www.venbi.com/en/catalogsearch/result/?q=enfa+pro" title="Enfapro">Enfapro</a> , <a href="http://www.venbi.com/en/diapers/popular-brands/drypers.html" title="Drypers">Drypers</a> , <a href="http://www.venbi.com/en/diapers/popular-brands/huggies.html" title="Huggies">Huggies</a> , <a title="DG" href="http://www.venbi.com/en/formula-feeding/popular-brands/dg.html">DG</a> , <a title="Peachy" href="http://www.venbi.com/en/formula-feeding/popular-brands/peachy.html">Peachy</a> , <a title="Badger" href="http://www.venbi.com/en/catalogsearch/result/?q=badger">Badger</a> , <a title="Babylove" href="http://www.venbi.com/en/diapers/popular-brands/baby-love.html">Babylove</a> , <a title="Sassy" href="http://www.venbi.com/en/toys/popular-brands/sassy.html">Sassy</a> , <a title="Similac" href="http://www.venbi.com/en/formula-feeding/popular-brands/similac.html">Similac</a> , <a title="Pigeon" href="http://www.venbi.com/en/baby-care/popular-brands/pigeon.html">Pigeon</a> , <a title="S-26" href="http://www.venbi.com/en/formula-feeding/popular-brands/s-26.html">S-26</a> , <a title="Bright Starts" href="http://www.venbi.com/en/toys/popular-brands/bright-starts.html">Bright Starts</a> , <a title="Tiny love" href="http://www.venbi.com/en/toys/popular-brands/tiny-love.html">Tiny love</a> และแบรนด์ดังอื่นๆอีกมากมาย</p>
<p>&nbsp;</p>
<p>สะดวก รวดเร็ว และมั่นใจได้ คือสิ่งที่ MOXY บริการให้คุณ เพราะความพึงพอใจของคุณคือสิ่งที่สำคัญที่สุดสำหรับเรา คุณสามารถติดต่อ MOXY ผ่านทางศูนย์บริการลูกค้าเพื่อสอบถามเกี่ยวกับข้อมูลต่างๆได้ตลอดเวลา และยังสามารถเลือกวิธีการชำระเงินได้ตามสะดวกไม่ว่าจะเป็น ชำระเงินผ่านบัตรเครดิตการ์ด Paypal หรือชำระเงินปลายทาง จากนั้นก็เตรียมตัวรับสินค้าที่จะถูกส่งตรงถึงหน้าบ้านคุณภายใน 2-3 วันทำการในเขตกรุงเทพฯ และ 3-5 วันทำการในเขตอื่น ไม่ว่าคุณจะอยู่ที่ไหน การช้อปปิ้งออนไลน์จะกลายเป็นเรื่องสนุกที่คุณทำได้ทุกวัน</p>
<p>&nbsp;</p>
<p>MOXY พร้อมที่จะให้บริการคุณแล้ววันนี้</p>
</div>
EOD;
/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// SEO Keyword (Health English)
//==========================================================================
$blockTitle = "Health SEO Keyword";
$blockIdentifier = "seo-keyword";
$blockStores = array($sen);
$blockIsActive = 1;
$blockContent = <<<EOD
<div style="margin: 50px auto 30px;">
<p>SANOGA is MOXY’s Health & Sport online store in Thailand.</p>
<p>&nbsp;</p>
<p>No more looking around for the best deal, MOXY is a one-stop health store where you can find 100% genuine products from household names. It is cheaper, easier and you can select what’s best for you 24/7 with just one click!</p>
<p>&nbsp;</p>
<p>MOXY offers over 1,000 Health products and Sport gear from well-known brands including Health Care, Vitamins, Collagen, Beauty, Sexual Well-being, Workout & Fitness, Weight Loss including Blackmores, Beauty Wise, Body Shape, CLG500, Centrum, Collagen Star, Colly, Dr.Absolute, DYMATIZE, Durex, Eucerin, Genesis, Hi-Balanz, K-Palette, Meiji, Mega We Care, Okamoto, ProFlex, Real Elixir, Sebamed, Seoul Secret, Taurus, etc.</p>
<p>&nbsp;</p>
<p>All products on MOXY are certified by FDA Thailand so you can trust in quality and safety.</p>
<p>&nbsp;</p>
<p>As the online destination for Women in Thailand, MOXY is dedicated to provide the best customer service: easy, fast and reliable. Your satisfaction is our priority. Please feel free to contact our customer service for any information. Multiple payment methods are available such as credit card, Pay Pal, and cash-on-delivery. Your order will be delivered to your doorstep within 2 to 3 days in Bangkok and 3 to 5 days in other regions of Thailand. Make your shopping easier than ever!</p>
<p>&nbsp;</p>
<p>MOXY is looking forward to serving you.</p>
</div>
EOD;
/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// SEO Keyword (Health Thai)
//==========================================================================
$blockTitle = "Health SEO Keyword";
$blockIdentifier = "seo-keyword";
$blockStores = array($sth);
$blockIsActive = 1;
$blockContent = <<<EOD
<div style="margin: 50px auto 30px;">
<p>Sanoga รวมสินค้าเพื่อสุขภาพและอุปกรณ์ออกกำลังกาย คัดสรรโดย MOXY</p>
<p>&nbsp;</p>
<p>คุณสามารถใช้เวลาได้อย่างเต็มที่ในการเลือกผลิตภัณฑ์ที่ต้องการ MOXY มีสินค้าให้คุณเลือกอย่างจุใจในราคาสุดคุ้ม และเราได้จัดทำข้อมูลอย่างละเอียดสำหรับสินค้าแต่ละชนิด พร้อมทั้งวิธีการสั่งซื้อที่สะดวกสบายตลอด 24 ชั่วโมง ทำให้การดูแลสุขภาพของคุณครั้งนี้ง่ายขึ้นกว่าเดิมในคลิ๊กเดียว</p>
<p>&nbsp;</p>
<p>MOXY มีสินค้าเพื่อสุขภาพและอุปกรณ์ออกกำลังกายครบครัน ไม่ว่าจะเป็นอาหารเสริม วิตามิน คอลลาเจน ผลิตภัณฑ์เสริมสมรรถภาพร่างกาย  อาหารเสริมเพื่อการออกกำลังกายและฟิตเนส เวย์โปรตีน และอาหารเสริมลดน้ำหนักจากกว่า 1,000 แบรนดํดังทั่วโลก เช่น Blackmores, Beauty Wise, Body Shape, CLG500, Centrum, Collagen Star, Colly, Dr.Absolute, DYMATIZE, Durex, Eucerin, Genesis, Hi-Balanz, K-Palette, Meiji, Mega We Care, Okamoto, ProFlex, Real Elixir, Sebamed, Seoul Secret, Taurus และอื่นๆ</p>
<p>&nbsp;</p>
<p>สินค้าทุกชิ้นของ MOXY ได้รับมาตราฐานการรับรองจากองค์การอาหารและยาแห่งประเทศไทย(อย.) จึงมั่นใจได้ในคุณภาพและความปลอดภัย</p>
<p>&nbsp;</p>
<p>สะดวก รวดเร็ว และมั่นใจได้ คือสิ่งที่ MOXY บริการให้คุณ เพราะความพึงพอใจของคุณคือสิ่งที่สำคัญที่สุดสำหรับเรา คุณสามารถติดต่อ MOXY ผ่านทางศูนย์บริการลูกค้าเพื่อสอบถามเกี่ยวกับข้อมูลต่างๆได้ตลอดเวลา และยังสามารถเลือกวิธีการชำระเงินได้ตามสะดวกไม่ว่าจะเป็น ชำระเงินผ่านบัตรเครดิตการ์ด Paypal หรือชำระเงินปลายทาง จากนั้นก็เตรียมตัวรับสินค้าที่จะถูกส่งตรงถึงหน้าบ้านคุณภายใน 2-3 วันทำการในเขตกรุงเทพฯ และ 3-5 วันทำการในเขตอื่น ไม่ว่าคุณจะอยู่ที่ไหน การช้อปปิ้งออนไลน์จะกลายเป็นเรื่องสนุกที่คุณทำได้ทุกวัน</p>
<p>&nbsp;</p>
<p>MOXY พร้อมที่จะให้บริการคุณแล้ววันนี้</p>
</div>
EOD;
/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================

//==========================================================================
// SEO Keyword (Pet Thai)
//==========================================================================
$blockTitle = "Pet SEO Keyword";
$blockIdentifier = "seo-keyword";
$blockStores = array($th);
$blockIsActive = 1;
$blockContent = <<<EOD
<div style="margin: 50px auto 30px;">
<p>&nbsp;</p>
<h2>Petloft รวมสินค้าสำหรับสัตว์เลี้ยงทุกประเภทไว้มากที่สุด โดย MOXY</h2>
<p>&nbsp;</p>
<p>ไม่ต้องเสียเวลาเดินหาซื้อและขนสินค้ากลับเองให้เมื่อยอีกต่อไป เพราะเรามีสินค้าสำหรับสัตว์เลี้ยงทุกชนิดไม่ว่าจะเป็น สุนุข แมว ปลา กระต่าย หนู สัตว์เลื้อยคลานและนก คุณสามารถซื้ออาหารหมา อาหารแมว ทรายแมว ของเล่น เครื่องประดับ เสื้อผ้า ยา และแชมพูให้กับสัตว์เลี้ยงของคุณได้ในคลิ๊กเดียว แบรนด์สินค้าขายดีของเราได้แก่&nbsp;&nbsp;<strong> <a href="{{store url='catalogsearch/result' _query='q=pedigree'}}">Pedigree</a> , <a href="{{store url='catalogsearch/result' _query='q=royal+canin'}}">Royal Canin</a> , <a href="{{store url='catalogsearch/result' _query='q=pinnacle'}}">Pinnacle</a> , <a href="{{store url='catalogsearch/result' _query='q=maxima'}}">Maxima</a> , <a href="{{store url='catalogsearch/result' _query='q=smart+heart'}}">SmartHeart</a> , <a href="{{store url='catalogsearch/result' _query='q=perfecta'}}">Perfecta</a> , <a href="{{store url='catalogsearch/result' _query='q=meo'}}">Me-O</a> , <a href="{{store url='catalogsearch/result' _query='q=chicken+soup'}}">Chicken Soup</a> , <a href="{{store url='catalogsearch/result' _query='q=whiskas'}}">Whiskas</a> , <a href="{{store url='catalogsearch/result' _query='q=science+diet'}}">Science Diet</a> , <a href="{{store url='catalogsearch/result' _query='q=frontline'}}">Frontline</a> , <a href="{{store url='catalogsearch/result' _query='q=cesar'}}">Cesar</a> , <a href="{{store url='catalogsearch/result' _query='q=zeal'}}">Zeal</a> , <a href="{{store url='catalogsearch/result' _query='q=anf'}}">ANF</a> , <a href="{{store url='catalogsearch/result' _query='q=dog+n+joy'}}">Dog N&rsquo; Joy</a> </strong> และอีกมากมาย</p>
<p>&nbsp;</p>
<p>สะดวก รวดเร็ว และมั่นใจได้ คือสิ่งที่ MOXY บริการให้คุณ เพราะความพึงพอใจของคุณคือสิ่งที่สำคัญที่สุดสำหรับเรา คุณสามารถติดต่อ MOXY ผ่านทางศูนย์บริการลูกค้าเพื่อสอบถามเกี่ยวกับข้อมูลต่างๆได้ตลอดเวลา และยังสามารถเลือกวิธีการชำระเงินได้ตามสะดวกไม่ว่าจะเป็น ชำระเงินผ่านบัตรเครดิตการ์ด Paypal หรือชำระเงินปลายทาง จากนั้นก็เตรียมตัวรับสินค้าที่จะถูกส่งตรงถึงหน้าบ้านคุณภายใน 2-3 วันทำการในเขตกรุงเทพฯ และ 3-5 วันทำการในเขตอื่น ไม่ว่าคุณจะอยู่ที่ไหน การช้อปปิ้งออนไลน์จะกลายเป็นเรื่องสนุกที่คุณทำได้ทุกวัน</p>
<p>MOXY พร้อมที่จะให้บริการคุณแล้ววันนี้</p>
</div>
EOD;
/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================

//==========================================================================
// SEO Keyword (Pet English)
//==========================================================================
$blockTitle = "Pet SEO Keyword";
$blockIdentifier = "seo-keyword";
$blockStores = array($en);
$blockIsActive = 1;
$blockContent = <<<EOD
<div style="margin: 50px auto 30px;">
<p>&nbsp;</p>
<h2>PETLOFT by MOXY is Thailand&rsquo;s number 1 online destination for Pet Lovers.</h2>
<p>&nbsp;</p>
<p>Shopping at MOXY means that you will no longer have to drive around for hours looking for the best products for your pets. Everything can now be purchased from one place. We offer all essential product for your&nbsp;&nbsp;<strong><a href="{{store url='catalogsearch/result' _query='q=dog'}}">dogs</a> , <a href="{{store url='catalogsearch/result' _query='q=cat'}}">cats</a> , <a href="{{store url='fish-products.html'}}">fish</a> , <a href="{{store url='small-pets-products.html'}}">small pets</a> , reptile ,</strong> and <strong><a href="{{store url='bird-products.html'}}">bird</a></strong> products. From MOXY, you can get your&nbsp;<strong><a href="{{store url='dog-food.html'}}">dog food</a> , <a href="{{store url='cat-food.html'}}">cat food</a> , <a href="{{store url='cat-food/cat-food-type/cat-litter.html'}}">cat litter</a> , <a href="{{store url='catalogsearch/result' _query='q=toys'}}">toys</a> , <a href="{{store url='catalogsearch/result' _query='q=accessories'}}">accessories</a> , <a href="{{store url='catalogsearch/result' _query='q=clothing'}}">clothing</a> , <a href="{{store url='catalogsearch/result' _query='q=pet+care'}}">medicine</a> , <a href="{{store url='catalogsearch/result' _query='q=shampoo'}}">shampoo</a> ,</strong> and many other products. Some of our best selling brands include <strong> <a href="{{store url='catalogsearch/result' _query='q=pedigree'}}">Pedigree</a> , <a href="{{store url='catalogsearch/result' _query='q=royal+canin'}}">Royal Canin</a> , <a href="{{store url='catalogsearch/result' _query='q=pinnacle'}}">Pinnacle</a> , <a href="{{store url='catalogsearch/result' _query='q=maxima'}}">Maxima</a> , <a href="{{store url='catalogsearch/result' _query='q=smart+heart'}}">SmartHeart</a> , <a href="{{store url='catalogsearch/result' _query='q=perfecta'}}">Perfecta</a> , <a href="{{store url='catalogsearch/result' _query='q=meo'}}">Me-O</a> , <a href="{{store url='catalogsearch/result' _query='q=chicken+soup'}}">Chicken Soup</a> , <a href="{{store url='catalogsearch/result' _query='q=whiskas'}}">Whiskas</a> , <a href="{{store url='catalogsearch/result' _query='q=science+diet'}}">Science Diet</a> , <a href="{{store url='catalogsearch/result' _query='q=frontline'}}">Frontline</a> , <a href="{{store url='catalogsearch/result' _query='q=cesar'}}">Cesar</a> , <a href="{{store url='catalogsearch/result' _query='q=zeal'}}">Zeal</a> , <a href="{{store url='catalogsearch/result' _query='q=anf'}}">ANF</a> , <a href="{{store url='catalogsearch/result' _query='q=dog+n+joy'}}">Dog N&rsquo; Joy</a> </strong> , and many more.&nbsp;</p>
<p>MOXY is dedicated to provide the best customer service: easy, fast and reliable. Your satisfaction is our priority. Please feel free to contact our customer service at &hellip;&hellip;. for any information. Multiple payment methods are available such as credit card, Pay Pal, and cash-on-delivery. Your order will be delivered to your doorstep within 2 to 3 days in Bangkok and 3 to 5 days in other regions of Thailand. Make your shopping easier than ever!</p>
<p>MOXY is looking forward to serving you.&nbsp;</p>
</div>
EOD;
/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================

// Base-name
$name           = 'moxy';
$engStoreName   = 'Moxy English';
$engStoreCode   = 'moxyen';
$thStoreName    = 'Moxy Thai';
$thStoreCode    = 'moxyth';
$storeIdMoxyEn  = Mage::getModel('core/store')->load($engStoreCode, 'code')->getId();
$storeIdMoxyTh  = Mage::getModel('core/store')->load($thStoreCode, 'code')->getId();
$newProductsCateId = 0;
$bestSellersCateId = 0;
try {

    //==========================================================================
    // About us EN Page
    //==========================================================================
    $pageTitle = "About Us";
    $pageIdentifier = "about-us";
    $pageStores = array($storeIdMoxyEn);
    $pageIsActive = 1;
    $pageUnderVersionControl = 0;
    $pageContentHeading = "About Us";
    $pageContent = <<<EOD
<div style="margin: 20px 0;">&nbsp;</div>
<div style="font: normal 14px 'Tahoma'; color: #505050; line-height: 22px;">
<p>Moxy is the only Women-focused shopping destination in Thailand.</p>
<p>Because modern Women are living very busy lives and have a lot of responsibilities, their time is precious. On Moxy, find everything you want, from daily essentials to coveted lifestyle needs. You can find tips and products in categories like Beauty,&nbsp;Fashion Accessories, Living, Electronics, Health and Mom &amp; Baby and Pets, with the convenience of it being delivered to your front door.&nbsp;</p>
<p>With a mix of both local and international brands and new items available every week, Moxy offers a large variety of quality products to suit your lifestyle, whether you are a student, a worker, a mother, a friend, a lover, a daughter&hellip; You CAN and SHOULD &ldquo;Have It All&rdquo;!</p>
<p>At Moxy, we believe that purchasing online should be easy, fun and safe. Our mission is to provide an entertaining shopping experience combined with a convenient home delivery service. Our customers can choose between various secure payment options, including cash-on-delivery everywhere in Thailand. At any moment, Customers can reach our support team at&nbsp;<span style="text-decoration: underline;"><a href="mailto:support@moxyst.com">support@moxyst.com</a></span>&nbsp;or by phone +65- 02-106-8222&nbsp;. Our customer support talents are at your service from Mon-Fri 09:00-18:00 and Sat 09:00-15:00.&nbsp;</p>
<p>&nbsp;</p>
<p>Stay connected with us on&nbsp;&nbsp;<span style="color: #333300;"><a href="https://www.facebook.com/moxyst" target="_blank"><span style="color: #333300;"><strong><span style="text-decoration: underline;">Facebook</span></strong></span></a></span>,&nbsp;<span style="color: #333300;"><a href="https://instagram.com/moxyst/" target="_blank"><span style="color: #333300;"><strong><span style="text-decoration: underline;">Instagram</span></strong></span></a>&nbsp;</span>and&nbsp;<span style="color: #333300;"><span style="color: #333300;"><strong><a href="https://twitter.com/moxyst" target="_blank"><span style="text-decoration: underline;">Twitter</span>&nbsp;</a>&nbsp;</strong></span></span>to get daily tips, our newest updates and the best shopping deals.</p>
<p>&nbsp;</p>
<p>We are looking forward to serving you !</p>
<p>With Love,</p>
<strong>The Moxy Team</strong></div>
EOD;

    $pageRootTemplate = 'one_column';
    $pageLayoutUpdateXml = <<<EOD
EOD;


    $pageCustomLayoutUpdateXML = <<<EOD
EOD;

    $pageMetaKeywords = "";
    $pageMetaDescription = "";

    $page = Mage::getModel('cms/page')->getCollection()
        ->addStoreFilter(array($storeIdMoxyEn), $withAdmin = true)
        ->addFieldToFilter('identifier', $pageIdentifier)
        ->getFirstItem();
    if ($page->getId() == 0) {
        $page = Mage::getModel('cms/page');
    }
    else
    {
        // if exists then repair
        $page = Mage::getModel('cms/page')->load($page->getId());
    }

    $page->setTitle($pageTitle);
    $page->setIdentifier($pageIdentifier);
    $page->setStores($pageStores);
    $page->setIsActive($pageIsActive);
    $page->setUnderVersionControl($pageUnderVersionControl);
    $page->setContentHeading($pageContentHeading);
    $page->setContent($pageContent);
    $page->setRootTemplate($pageRootTemplate);
    $page->setLayoutUpdateXml($pageLayoutUpdateXml);
    $page->setCustomLayoutUpdateXml($pageCustomLayoutUpdateXML);
    $page->setMetaKeywords($pageMetaKeywords);
    $page->setMetaDescription($pageMetaDescription);
    $page->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================

    //==========================================================================
    // About us TH Page
    //==========================================================================
    $pageTitle = "About us | เกี่ยวกับเรา";
    $pageIdentifier = "about-us";
    $pageStores = array($storeIdMoxyTh);
    $pageIsActive = 1;
    $pageUnderVersionControl = 0;
    $pageContentHeading = "เกี่ยวกับ Moxy";
    $pageContent = <<<EOD
<div style="margin: 20px 0;">&nbsp;</div>
<div style="font: normal 14px 'Tahoma'; color: #505050; line-height: 22px;">
<p>MOXY เว็บไซต์ช้อปปิ้งออนไลน์สำหรับผู้หญิงแห่งเดียวของเมืองไทย</p>
<p>เรารู้ว่าผู้หญิงมีไลฟ์สไตล์ที่ยุ่งเหยิง และมีสิ่งที่ต้องทำมากมายในแต่ละวันเพื่อแข่งกับเวลา MOXY จึงรวบรวมของทุกอย่างที่ผู้หญิงต้องการในชีวิตประจำวัน ไม่ว่าจะเป็นเครื่องสำอาง เครื่องประดับ ของแต่งบ้าน แก็ตเจ็ตอีเล็คทรอนิกส์ สินค้าเพื่อสุขภาพ สินค้าสำหรับแม่และเด็ก และสินค้าสำหรับสัตว์เลี้ยงที่พร้อมจะส่งตรงถึงหน้าบ้านคุณ</p>
<p>เรามีสินค้าจากแบรนด์ดังทั่วโลก ทั้งไทยและต่างประเทศ และสินค้าใหม่ๆให้ได้ช้อปกันทุกสัปดาห์ เน้นเฉพาะสินค้าที่มีคุณภาพและเหมาะกับไลฟ์สไตล์ของผู้หญิงทุกคน ไม่ว่าคุณจะเป็นนักศึกษา คนทำงาน เป็นแม่ เป็นเพื่อน เป็นคนรัก หรือเป็นลูกสาว คุณสามารถมีได้ทุกอย่างที่คุณอยากได้อย่างแน่นอน &ldquo;Have it all&rdquo;</p>
<p style="font-weight: normal;">MOXY เชื่อว่าการช้อปปิ้งออนไลน์ควรเป็นเรื่องที่สนุก สะดวก และปลอดภัย เราจึงตั้งใจสร้างประสบการณ์ช้อปปิ้งออนไลน์ที่ดีที่สุดให้กับคุณ บริการส่งสินค้าทุกที่ทั่วประเทศถึงมือคุณ&nbsp; พร้อมทั้งการชำระเงินผ่านช่องทางต่างๆที่ไว้ในได้ในความปลอดภัย หรือจะชำระเงินเมื่อได้รับสินค้าก็ยังได้ คุณสามารถติดต่อ MOXY ได้ผ่านฝ่ายบริการลูกค้า อีเมล&nbsp;&nbsp;<span style="text-decoration: underline;"><a href="mailto:support@moxyst.com">support@moxyst.com</a></span>&nbsp; หรือโทร 02-106-8222&nbsp; วันจันทร์ถึงศุกร์ 9:00-18:00 น. และวันเสาร์ 09:00-15:00 น.</p>
<p style="font-weight: normal;">&nbsp;</p>
<p>อัพเดทข่าวสารและเทรนด์ใหม่ๆกับเราได้ผ่านทาง&nbsp;<a href="https://www.facebook.com/moxyst" target="_blank"><strong><span style="text-decoration: underline;">Facebook</span></strong></a>,&nbsp;<span><a style="font-weight: bold; text-decoration: underline;" href="https://instagram.com/moxyst/" target="_blank">Instagram</a></span><span style="color: #333300;"><span style="color: #333300;"><span>, และ&nbsp;&nbsp;</span></span></span><strong><span style="text-decoration: underline;"><a href="https://twitter.com/moxyst" target="_blank">Twitter</a></span></strong><strong>&nbsp;</strong>ได้ทุกวัน</p>
<p>MOXY พร้อมที่จะให้บริการคุณแล้ววันนี้</p>
<p>ด้วยรัก</p>
<p>ทีมงาน MOXY</p>
</div>
EOD;

    $pageRootTemplate = 'one_column';
    $pageLayoutUpdateXml = <<<EOD
EOD;


    $pageCustomLayoutUpdateXML = <<<EOD
EOD;

    $pageMetaKeywords = "";
    $pageMetaDescription = "Moxy คือศูนย์รวมผลิตภัณฑ์เพื่อสุขภาพและเครื่องสำอาง อาหารเสริม คอลลาเจน และวิตามินต่างๆ คุณภาพแท้ 100% สินค้ามี อย.ทุกชิ้น จากแบรนด์ชั้นนำทั่วโลก";

    $page = Mage::getModel('cms/page')->getCollection()
        ->addStoreFilter(array($storeIdMoxyTh), $withAdmin = true)
        ->addFieldToFilter('identifier', $pageIdentifier)
        ->getFirstItem();
    if ($page->getId() == 0) {
        $page = Mage::getModel('cms/page');
    }
    else
    {
        // if exists then repair
        $page = Mage::getModel('cms/page')->load($page->getId());
    }

    $page->setTitle($pageTitle);
    $page->setIdentifier($pageIdentifier);
    $page->setStores($pageStores);
    $page->setIsActive($pageIsActive);
    $page->setUnderVersionControl($pageUnderVersionControl);
    $page->setContentHeading($pageContentHeading);
    $page->setContent($pageContent);
    $page->setRootTemplate($pageRootTemplate);
    $page->setLayoutUpdateXml($pageLayoutUpdateXml);
    $page->setCustomLayoutUpdateXml($pageCustomLayoutUpdateXML);
    $page->setMetaKeywords($pageMetaKeywords);
    $page->setMetaDescription($pageMetaDescription);
    $page->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================
} catch (Excpetion $e) {
    Mage::logException($e);
    Mage::log("ERROR IN SETUP " . $e->getMessage());
}

$category = Mage::getModel('catalog/category');
$category->setStoreId($len);
$category->load(768);
$category->setData('seo_text','<h2>Makeup</h2>
<p style="text-align:justify;">Why do we wear make-up? It is very ancient habit, tracking back to Ancient Egypt, and it has different purposes depending on people and cultures. For example, make-up is a confidence booster. From a natural, nude look focused on foundation, mascara and lip gloss, or a Femme Fatale makeup with smoky eyes and sexy red lipstick, the amount and style of makeup varies on the occasion. Sometimes we use makeup to look older, and sometimes to look younger. Some women will use makeup for a clearer complexion whereas others want to enhance their tan for a sun-kissed look. Makeup is a great tool to enhance your beauty and your personality. Have fun creating your own style with MOXY’s tips and selected products. Great shopping Ladies!</p>');
$category->save();

$category = Mage::getModel('catalog/category');
$category->setStoreId($lth);
$category->load(768);
$category->setData('seo_text','<h2>เครื่องสำอางชิ้นเด็ด ที่คัดสรรมาเพื่อสาวๆ โดยเฉพาะ</h2>
<p style="text-align:justify;">ทำไมผู้หญิงถึงแต่งหน้า? การแต่งหน้ามีมาตั้งแต่สมัยอียิปต์โบราณ และแตกต่างกันไปตามประเพณี ยกตัวอย่างเช่น การแต่งหน้าเพื่อเสริมความมั่นใน การแต่งหน้าแบบธรรมชาติจะใช้เพียงรองพื้น มาสคาร่า และลิปกลอส  หรือการแต่งหน้าเพื่อเน้นความสวยงามของผู้หญิงด้วยการแต่งตาแบบสโมคกี้อายและลิปสติกสีแดง การแต่งหน้าจะแตกต่างกันไปตามโอกาส ในบางครั้งเราแต่งหน้าเพื่อเพิ่มอายุและความน่าเชื่อถือ หรือแต่งหน้าเพื่อทำให้ดูเด็กลง ผู้หญิงบางคนแต่งหน้าเพื่อทำผิวดูขาวกระจ่างใสขึ้น แต่บางคนก็ชอบที่จะแต่งหน้าโชว์ผิวบ่มแดดแบบสุขภาพดี MOXY รวบรวมเคล็บลับการแต่งหน้าและเครื่องสำอางเด็ดๆที่จะช่วยให้ผู้หญิงสนุกไปกับการแต่งหน้าในสไตล์ที่เป็นตัวของตัวเอง ขอให้สนุกกับการช้อปปิ้งเพิ่มความสวยนะคะ!</p>');
$category->save();