<?php

// init Magento
Mage::app();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

// get store ids
$en     = Mage::getModel('core/store')->load('en', 'code')->getId();// Petloft
$th     = Mage::getModel('core/store')->load('th', 'code')->getId();
$len    = Mage::getModel('core/store')->load('len', 'code')->getId();// Lafema
$lth    = Mage::getModel('core/store')->load('lth', 'code')->getId();
$ven    = Mage::getModel('core/store')->load('ven', 'code')->getId();// Venbi
$vth    = Mage::getModel('core/store')->load('vth', 'code')->getId();
$sen    = Mage::getModel('core/store')->load('sen', 'code')->getId();// Sanoga
$sth    = Mage::getModel('core/store')->load('sth', 'code')->getId();
$moxyen = Mage::getModel('core/store')->load('moxyen', 'code')->getId();// Moxy
$moxyth = Mage::getModel('core/store')->load('moxyth', 'code')->getId();

$allStoresTH = array($th, $lth, $vth, $sth, $moxyth);
$allStoresEN = array($en, $len, $ven, $sen, $moxyen);
$allStores = array($th, $lth, $vth, $sth, $moxyth, $en, $len, $ven, $sen, $moxyen);

//==========================================================================
// guarantee_petloft EN
//==========================================================================
$blockTitle = "guarantee_petloft EN";
$blockIdentifier = "guarantee_petloft";
$blockStores = $en;
$blockIsActive = 1;

// delete old block if have
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');

$content = <<<EOD
<div class="guarantee_petloft">
<a href="{{store url='help'}}" target="_blank"><span class="guarantee_icon">Free Shipping</span></a>
<a href="{{store url='help'}}#payments" target="_blank"><span class="guarantee_icon">Pay Cash on Delivery</span></a>
<a href="{{store url='help'}}#returns" target="_blank"><span class="guarantee_icon">30 Day Free Returns</span></a>
<a href="{{store url='help'}}#payments" target="_blank"><span class="guarantee_icon">100% Secured Checkout</span></a>
<span class="free-shipping-icon">Free Delivery</span>
</div>
<span class="payent-icon-1">&nbsp;</span>
<span class="payent-icon-2">&nbsp;</span>
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$enBlock = Mage::getModel('cms/block');
$enBlock->setTitle($blockTitle);
$enBlock->setIdentifier($blockIdentifier);
$enBlock->setStores($blockStores);
$enBlock->setIsActive(1);
$enBlock->setContent($content);
$enBlock->save();

//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// guarantee_petloft TH
//==========================================================================
$blockTitle = "guarantee_petloft TH";
$blockIdentifier = "guarantee_petloft";
$blockStores = $th;
$blockIsActive = 1;

// delete old block if have
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');

$content = <<<EOD
<div class="guarantee_petloft">
<a href="{{store url='help'}}" target="_blank"><span class="guarantee_icon">ฟรีค่าจัดส่ง</span></a>
<a href="{{store url='help'}}#payments" target="_blank"><span class="guarantee_icon">ชำระเงินเมื่อได้รับสินค้า</span></a>
<a href="{{store url='help'}}#returns" target="_blank"><span class="guarantee_icon">รับคืนฟรี ภายใน 30 วัน</span></a>
<a href="{{store url='help'}}#payments" target="_blank"><span class="guarantee_icon">ระบบชำระเงินปลอดภัย 100%</span></a>
<span class="free-shipping-icon">จัดส่งฟรี</span>
</div>
<span class="payent-icon-1">&nbsp;</span>
<span class="payent-icon-2">&nbsp;</span>
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$enBlock = Mage::getModel('cms/block');
$enBlock->setTitle($blockTitle);
$enBlock->setIdentifier($blockIdentifier);
$enBlock->setStores($blockStores);
$enBlock->setIsActive(1);
$enBlock->setContent($content);
$enBlock->save();

//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// guarantee_venbi EN
//==========================================================================
$blockTitle = "guarantee_venbi EN";
$blockIdentifier = "guarantee_venbi";
$blockStores = $ven;
$blockIsActive = 1;

// delete old block if have
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');

$content = <<<EOD
<div class="guarantee_venbi">
<a href="{{store url='help'}}" target="_blank"><span class="guarantee_icon">Free Shipping</span></a>
<a href="{{store url='help'}}#payments" target="_blank"><span class="guarantee_icon">Pay Cash on Delivery</span></a>
<a href="{{store url='help'}}#returns" target="_blank"><span class="guarantee_icon">30 Day Free Returns</span></a>
<a href="{{store url='help'}}#payments" target="_blank"><span class="guarantee_icon">100% Secured Checkout</span></a>
<span class="free-shipping-icon">Free Delivery</span>
</div>
<span class="payent-icon-1">&nbsp;</span>
<span class="payent-icon-2">&nbsp;</span>
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$enBlock = Mage::getModel('cms/block');
$enBlock->setTitle($blockTitle);
$enBlock->setIdentifier($blockIdentifier);
$enBlock->setStores($blockStores);
$enBlock->setIsActive(1);
$enBlock->setContent($content);
$enBlock->save();

//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// guarantee_venbi TH
//==========================================================================
$blockTitle = "guarantee_venbi TH";
$blockIdentifier = "guarantee_venbi";
$blockStores = $vth;
$blockIsActive = 1;

// delete old block if have
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');

$content = <<<EOD
<div class="guarantee_venbi">
<a href="{{store url='help'}}" target="_blank"><span class="guarantee_icon">ฟรีค่าจัดส่ง</span></a>
<a href="{{store url='help'}}#payments" target="_blank"><span class="guarantee_icon">ชำระเงินเมื่อได้รับสินค้า</span></a>
<a href="{{store url='help'}}#returns" target="_blank"><span class="guarantee_icon">รับคืนฟรี ภายใน 30 วัน</span></a>
<a href="{{store url='help'}}#payments" target="_blank"><span class="guarantee_icon">ระบบชำระเงินปลอดภัย 100%</span></a>
<span class="free-shipping-icon">จัดส่งฟรี</span>
</div>
<span class="payent-icon-1">&nbsp;</span>
<span class="payent-icon-2">&nbsp;</span>
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$enBlock = Mage::getModel('cms/block');
$enBlock->setTitle($blockTitle);
$enBlock->setIdentifier($blockIdentifier);
$enBlock->setStores($blockStores);
$enBlock->setIsActive(1);
$enBlock->setContent($content);
$enBlock->save();

//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// guarantee_sanoga EN
//==========================================================================
$blockTitle = "guarantee_sanoga EN";
$blockIdentifier = "guarantee_sanoga";
$blockStores = $sen;
$blockIsActive = 1;

// delete old block if have
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');

$content = <<<EOD
<div class="guarantee_sanoga">
<a href="{{store url='help'}}" target="_blank"><span class="guarantee_icon">Free Shipping</span></a>
<a href="{{store url='help'}}#payments" target="_blank"><span class="guarantee_icon">Pay Cash on Delivery</span></a>
<a href="{{store url='help'}}#returns" target="_blank"><span class="guarantee_icon">30 Day Free Returns</span></a>
<a href="{{store url='help'}}#payments" target="_blank"><span class="guarantee_icon">100% Secured Checkout</span></a>
<span class="free-shipping-icon">Free Delivery</span>
</div>
<span class="payent-icon-1">&nbsp;</span>
<span class="payent-icon-2">&nbsp;</span>
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$enBlock = Mage::getModel('cms/block');
$enBlock->setTitle($blockTitle);
$enBlock->setIdentifier($blockIdentifier);
$enBlock->setStores($blockStores);
$enBlock->setIsActive(1);
$enBlock->setContent($content);
$enBlock->save();

//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// guarantee_sanoga TH
//==========================================================================
$blockTitle = "guarantee_sanoga TH";
$blockIdentifier = "guarantee_sanoga";
$blockStores = $sth;
$blockIsActive = 1;

// delete old block if have
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');

$content = <<<EOD
<div class="guarantee_sanoga">
<a href="{{store url='help'}}" target="_blank"><span class="guarantee_icon">ฟรีค่าจัดส่ง</span></a>
<a href="{{store url='help'}}#payments" target="_blank"><span class="guarantee_icon">ชำระเงินเมื่อได้รับสินค้า</span></a>
<a href="{{store url='help'}}#returns" target="_blank"><span class="guarantee_icon">รับคืนฟรี ภายใน 30 วัน</span></a>
<a href="{{store url='help'}}#payments" target="_blank"><span class="guarantee_icon">ระบบชำระเงินปลอดภัย 100%</span></a>
<span class="free-shipping-icon">จัดส่งฟรี</span>
</div>
<span class="payent-icon-1">&nbsp;</span>
<span class="payent-icon-2">&nbsp;</span>
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$enBlock = Mage::getModel('cms/block');
$enBlock->setTitle($blockTitle);
$enBlock->setIdentifier($blockIdentifier);
$enBlock->setStores($blockStores);
$enBlock->setIsActive(1);
$enBlock->setContent($content);
$enBlock->save();

//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// guarantee_lafema EN
//==========================================================================
$blockTitle = "guarantee_lafema EN";
$blockIdentifier = "guarantee_lafema";
$blockStores = $len;
$blockIsActive = 1;

// delete old block if have
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');

$content = <<<EOD
<div class="guarantee_lafema">
<a href="{{store url='help'}}" target="_blank"><span class="guarantee_icon">Free Shipping</span></a>
<a href="{{store url='help'}}#payments" target="_blank"><span class="guarantee_icon">Pay Cash on Delivery</span></a>
<a href="{{store url='help'}}#returns" target="_blank"><span class="guarantee_icon">30 Day Free Returns</span></a>
<a href="{{store url='help'}}#payments" target="_blank"><span class="guarantee_icon">100% Secured Checkout</span></a>
<span class="free-shipping-icon">Free Delivery</span>
</div>
<span class="payent-icon-1">&nbsp;</span>
<span class="payent-icon-2">&nbsp;</span>
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$enBlock = Mage::getModel('cms/block');
$enBlock->setTitle($blockTitle);
$enBlock->setIdentifier($blockIdentifier);
$enBlock->setStores($blockStores);
$enBlock->setIsActive(1);
$enBlock->setContent($content);
$enBlock->save();

//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// guarantee_lafema TH
//==========================================================================
$blockTitle = "guarantee_lafema TH";
$blockIdentifier = "guarantee_lafema";
$blockStores = $lth;
$blockIsActive = 1;

// delete old block if have
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');

$content = <<<EOD
<div class="guarantee_lafema">
<a href="{{store url='help'}}" target="_blank"><span class="guarantee_icon">ฟรีค่าจัดส่ง</span></a>
<a href="{{store url='help'}}#payments" target="_blank"><span class="guarantee_icon">ชำระเงินเมื่อได้รับสินค้า</span></a>
<a href="{{store url='help'}}#returns" target="_blank"><span class="guarantee_icon">รับคืนฟรี ภายใน 30 วัน</span></a>
<a href="{{store url='help'}}#payments" target="_blank"><span class="guarantee_icon">ระบบชำระเงินปลอดภัย 100%</span></a>
<span class="free-shipping-icon">จัดส่งฟรี</span>
</div>
<span class="payent-icon-1">&nbsp;</span>
<span class="payent-icon-2">&nbsp;</span>
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$enBlock = Mage::getModel('cms/block');
$enBlock->setTitle($blockTitle);
$enBlock->setIdentifier($blockIdentifier);
$enBlock->setStores($blockStores);
$enBlock->setIsActive(1);
$enBlock->setContent($content);
$enBlock->save();

//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// guarantee_moxy EN
//==========================================================================
$blockTitle = "guarantee_moxy EN";
$blockIdentifier = "guarantee_moxy";
$blockStores = $moxyen;
$blockIsActive = 1;

// delete old block if have
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');

$content = <<<EOD
<div class="guarantee_moxy">
<a href="{{store url='help'}}" target="_blank"><span class="guarantee_icon">Free Shipping</span></a>
<a href="{{store url='help'}}#payments" target="_blank"><span class="guarantee_icon">Pay Cash on Delivery</span></a>
<a href="{{store url='help'}}#returns" target="_blank"><span class="guarantee_icon">30 Day Free Returns</span></a>
<a href="{{store url='help'}}#payments" target="_blank"><span class="guarantee_icon">100% Secured Checkout</span></a>
<span class="free-shipping-icon">Free Delivery</span>
</div>
<span class="payent-icon-1">&nbsp;</span>
<span class="payent-icon-2">&nbsp;</span>
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$enBlock = Mage::getModel('cms/block');
$enBlock->setTitle($blockTitle);
$enBlock->setIdentifier($blockIdentifier);
$enBlock->setStores($blockStores);
$enBlock->setIsActive(1);
$enBlock->setContent($content);
$enBlock->save();

//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// guarantee_moxy TH
//==========================================================================
$blockTitle = "guarantee_moxy TH";
$blockIdentifier = "guarantee_moxy";
$blockStores = $moxyth;
$blockIsActive = 1;

// delete old block if have
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');

$content = <<<EOD
<div class="guarantee_moxy">
<a href="{{store url='help'}}" target="_blank"><span class="guarantee_icon">ฟรีค่าจัดส่ง</span></a>
<a href="{{store url='help'}}#payments" target="_blank"><span class="guarantee_icon">ชำระเงินเมื่อได้รับสินค้า</span></a>
<a href="{{store url='help'}}#returns" target="_blank"><span class="guarantee_icon">รับคืนฟรี ภายใน 30 วัน</span></a>
<a href="{{store url='help'}}#payments" target="_blank"><span class="guarantee_icon">ระบบชำระเงินปลอดภัย 100%</span></a>
<span class="free-shipping-icon">จัดส่งฟรี</span>
</div>
<span class="payent-icon-1">&nbsp;</span>
<span class="payent-icon-2">&nbsp;</span>
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$enBlock = Mage::getModel('cms/block');
$enBlock->setTitle($blockTitle);
$enBlock->setIdentifier($blockIdentifier);
$enBlock->setStores($blockStores);
$enBlock->setIsActive(1);
$enBlock->setContent($content);
$enBlock->save();

//==========================================================================
//==========================================================================
//==========================================================================