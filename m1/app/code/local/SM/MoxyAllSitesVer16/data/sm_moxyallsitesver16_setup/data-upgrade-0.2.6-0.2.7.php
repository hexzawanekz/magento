<?php

ini_set('max_execution_time', 0);
ini_set('display_error', 1);
ini_set('memory_limit', -1);
// init Magento
Mage::app();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

$moxyen = Mage::getModel('core/store')->load('moxyen', 'code')->getId();// Moxy
$moxyth = Mage::getModel('core/store')->load('moxyth', 'code')->getId();

$enStores = array($moxyen);
$thStores = array($moxyth);

// welcome-main-nav

//==========================================================================
// English
//==========================================================================
$blockTitle = "Welcome splash navigation ENG";
$blockIdentifier = "welcome-main-nav";
$blockStores = $enStores;
$blockIsActive = 1;
$content = <<<EOD
<ul class="nav">
<li class="active"><a>welcome</a></li>
<li class="beauty-nav-item"><a href="http://moxy.co.th/en/lafema.html">beauty</a></li>
<li class="fashion-nav-item"><a href="http://moxy.co.th/en/fashion">fashion</a></li>
<li class="living-nav-item"><a href="http://moxy.co.th/en/homeliving">home d&eacute;cor</a></li>
<li class="elec-nav-item"><a href="http://moxy.co.th/en/electronics">electronics</a></li>
<li class="baby-nav-item"><a href="http://moxy.co.th/en/venbi.html">mom &amp; baby</a></li>
<li class="heath-nav-item"><a href="http://moxy.co.th/en/sanoga.html">Health &amp; Sports</a></li>
<li class="pet-nav-item"><a href="http://moxy.co.th/en/petloft.html">pets</a></li>
</ul>
EOD;

// delete old block if have
/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// Thai
//==========================================================================
$blockTitle = "Welcome splash navigation THAI";
$blockIdentifier = "welcome-main-nav";
$blockStores = $thStores;
$blockIsActive = 1;
$content = <<<EOD
<ul class="nav">
<li class="active"><a>หน้าแรก</a></li>
<li class="beauty-nav-item"><a href="http://moxy.co.th/th/lafema.html">ความงาม</a></li>
<li class="fashion-nav-item"><a href="http://moxy.co.th/th/fashion">แฟชั่น</a></li>
<li class="living-nav-item"><a href="http://moxy.co.th/th/homeliving">ของแต่งบ้าน</a></li>
<li class="elec-nav-item"><a href="http://moxy.co.th/th/electronics">อิเล็กทรอนิกส์</a></li>
<li class="baby-nav-item"><a href="http://moxy.co.th/th/venbi.html">แม่และเด็ก</a></li>
<li class="heath-nav-item"><a href="http://moxy.co.th/th/sanoga.html">สุขภาพ &amp; สปอร์ต</a></li>
<li class="pet-nav-item"><a href="http://moxy.co.th/th/petloft.html">สัตว์เลี้ยง</a></li>
</ul>
EOD;

// delete old block if have
/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================
