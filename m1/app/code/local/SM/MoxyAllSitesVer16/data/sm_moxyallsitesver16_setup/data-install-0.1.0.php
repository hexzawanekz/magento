<?php

// init Magento
Mage::app();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

// get store ids
$en     = Mage::getModel('core/store')->load('en', 'code')->getId();// Petloft
$th     = Mage::getModel('core/store')->load('th', 'code')->getId();
$len    = Mage::getModel('core/store')->load('len', 'code')->getId();// Lafema
$lth    = Mage::getModel('core/store')->load('lth', 'code')->getId();
$ven    = Mage::getModel('core/store')->load('ven', 'code')->getId();// Venbi
$vth    = Mage::getModel('core/store')->load('vth', 'code')->getId();
$sen    = Mage::getModel('core/store')->load('sen', 'code')->getId();// Sanoga
$sth    = Mage::getModel('core/store')->load('sth', 'code')->getId();
$moxyen = Mage::getModel('core/store')->load('moxyen', 'code')->getId();// Moxy
$moxyth = Mage::getModel('core/store')->load('moxyth', 'code')->getId();

$arrStores = array(
    'petloft'   => array($en, $th),
    'sanoga'    => array($sen, $sth),
    'lafema'    => array($len, $lth),
    'venbi'     => array($ven, $vth),
    'moxy'      => array($moxyen, $moxyth),
);

$arrConfigs = array(
    'design/theme/template',
    'design/theme/skin',
    'design/theme/layout',
    'design/theme/default'
);

// call config object
$configurator = new Mage_Core_Model_Config();

// save config for store views
foreach($arrStores as $store => $ids){
    foreach($ids as $id){
        foreach($arrConfigs as $config){
            if($store == 'petloft' && $config == 'design/theme/default') continue;
            //
            if($config == 'design/theme/default'){
                $configurator->saveConfig($config, 'petloft_v16', 'stores', $id);
            }else{
                $configurator->saveConfig($config, $store.'_v16', 'stores', $id);
            }
        }
    }
}