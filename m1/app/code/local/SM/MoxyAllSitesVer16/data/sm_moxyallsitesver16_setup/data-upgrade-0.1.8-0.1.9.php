<?php

// init Magento
Mage::app();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

// get store ids
$en     = Mage::getModel('core/store')->load('en', 'code')->getId();// Petloft
$th     = Mage::getModel('core/store')->load('th', 'code')->getId();
$len    = Mage::getModel('core/store')->load('len', 'code')->getId();// Lafema
$lth    = Mage::getModel('core/store')->load('lth', 'code')->getId();
$ven    = Mage::getModel('core/store')->load('ven', 'code')->getId();// Venbi
$vth    = Mage::getModel('core/store')->load('vth', 'code')->getId();
$sen    = Mage::getModel('core/store')->load('sen', 'code')->getId();// Sanoga
$sth    = Mage::getModel('core/store')->load('sth', 'code')->getId();
$moxyen = Mage::getModel('core/store')->load('moxyen', 'code')->getId();// Moxy
$moxyth = Mage::getModel('core/store')->load('moxyth', 'code')->getId();


//==========================================================================
// all-brands-slider (Petloft English)
//==========================================================================
$blockTitle = "All brands slider";
$blockIdentifier = "all-brands-slider";
$blockStores = array($en);
$blockIsActive = 1;

// delete old block if have
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete

$content = <<<EOD
<div class="recommend-hd-box best-selling-brands">
<div class="headRibbon"><span class="recommend-hd">OUR BRAND SELECTION</span></div>
</div>
<p>&nbsp;</p>
<div id="brandslide" style="margin-bottom: 30px;">
<ul id="brand-slides">
<li><a style="width: 610px;" href="http://www.petloft.com/en/cat-food/cat-food-brands/toro-toro.html"><img src="{{media url="wysiwyg/petloft-torotoro-cat-food-homepage-banner.jpg"}}" alt="Toro Toro cat snacks" /></a>
<div class="caption">Toro Toro Sale</div>
</li>
<li><a href="{{store url='dog-food/dog-product-brands/cesar.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/cesar.jpg"}}" alt="Cesar" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/science-diet.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/science-diet.jpg"}}" alt="Science Diet" /> </a></li>
<li><a href="{{store url='cat-food/cat-food-brands/whiskas.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/whiskas.jpg"}}" alt="Whiskas" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/royal-canin.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/royal-canin.jpg"}}" alt="Royal Canin" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/pedigree.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/pedigree.jpg"}}" alt="Pedigree" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/alpo.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/alpo.jpg"}}" alt="Alpo" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/dog-n-joy.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/dog-n-joy.jpg"}}" alt="Dog'n Joy" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/dr-luvcare.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/dr-luvcare.jpg"}}" alt="Dr. LuvCare" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/earthborn.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/earthborn.jpg"}}" alt="Earthborn" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/jerhigh.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/jerhigh.jpg"}}" alt="Jerhigh" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/perfecta.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/perfecta.jpg"}}" alt="Perfecta" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/pro-pac.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/pro-pac.jpg"}}" alt="Pro Pac" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/pro-plan.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/pro-plan.jpg"}}" alt="Purina Proplan" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/sleeky.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/sleeky.jpg"}}" alt="Sleeky" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/smartheart.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/smartheart.jpg"}}" alt="Smartheart" /> </a></li>
<li><a href="{{store url='cat-food/cat-food-brands/bellotta.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/bellotta.jpg"}}" alt="Bellotta" /> </a></li>
<li><a href="{{store url='cat-food/cat-food-brands/friskies.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/friskies.jpg"}}" alt="Friskies" /> </a></li>
<li><a href="{{store url='cat-food/cat-food-brands/purina.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/purina.jpg"}}" alt="Purina One" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/pinnacle.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/pinnacle.jpg"}}" alt="Pinnacle" /> </a></li>
<li><a href="{{store url='cat-food/cat-food-brands/meo.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/meo.jpg"}}" alt="Me-O" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/zeal.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/zeal.jpg"}}" alt="Zeal" /> </a></li>
<li><a href="{{store url='cat-food/cat-food-brands/catsan.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/catsan.jpg"}}" alt="Catsan" /> </a></li>
<li><a href="{{store url='cat-food/cat-food-brands/cat-s-best.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/cat-s-best.jpg"}}" alt="Cat's Best" /> </a></li>
<li><a href="{{store url='cat-food/cat-food-brands/ketty-love.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/ketty-love.jpg"}}" alt="Ketty Love" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/frontline.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/frontline.jpg"}}" alt="Frontline Plus" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/heartgard.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/heartgard.jpg"}}" alt="Heartgard" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/happy-molly.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/happy-molly.jpg"}}" alt="Happy Molly" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/avoderm.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/avoderm.jpg"}}" alt="AvoDerm" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/maxima.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/maxima.jpg"}}" alt="Maxima" /> </a></li>
<li><a href="{{store url='cat-food/cat-food-brands/1st-choice.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/1st-choice.jpg"}}" alt="1st Choice" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/dental-fresh.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/dental-fresh.jpg"}}" alt="Dental Fresh" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/monge-fresh.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/monge-fresh.jpg"}}" alt="Monge" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/petsown.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/petsown.jpg"}}" alt="Pet's Own" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/petstage.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/petstage.jpg"}}" alt="Petstage" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/petzlife.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/petzlife.jpg"}}" alt="PetzLife" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/pronature-holistic.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/pronature-holistic.jpg"}}" alt="Pronature Holistic" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/richard-organic.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/richard-organic.jpg"}}" alt="Richard Organic" /> </a></li>
<li><a href="{{store url='cat-food/cat-food-brands/shed-x-dermaplex.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/shed-x-dermaplex.jpg"}}" alt="Shed-X Dermaplex" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/sukina-petto.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/sukina-petto.jpg"}}" alt="Sukina Petto" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/touchdog.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/touchdog.jpg"}}" alt="Touchdog" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/veterinary-formula.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/veterinary-formula.jpg"}}" alt="Veterinary Formula" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/vitalife.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/vitalife.jpg"}}" alt="Vitalife" /> </a></li>
<li><a href="{{store url='cat-food/cat-food-brands/x-treme-catnip.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/x-treme-catnip.jpg"}}" alt="X-Treme Catnip" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/superdesign.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/superdesign.jpg"}}" alt="Super Designs" /> </a></li>
<li><a href="{{store url='cat-food/cat-food-brands/engage.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/engage.jpg"}}" alt="Engage Eco Products" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/dogkery.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/dogkery.jpg"}}" alt="Dogkery" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/greenpet.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/greenpet.jpg"}}" alt="Green Pet" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/homepet-thailand.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/homepet-thailand.jpg"}}" alt="Homepet Thailand" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/puppe.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/puppe.jpg"}}" alt="Puppe' Pet Cloths" /> </a></li>
<li><a href="{{store url='cat-food/cat-food-brands/cat-n-joy.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/cat-n-joy.jpg"}}" alt="Cat'n Joy" /> </a></li>
<li><a href="{{store url='cat-food/cat-food-brands/condoa-a-a.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/catcondo.jpg"}}" alt="Cat Condos" /> </a></li>
<li><a href="{{store url='cat-food/cat-food-brands/petsafe.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/petsafe.jpg"}}" alt="Petsafe" /> </a></li>
<li><a href="{{store url='cat-food/cat-food-brands/petmade.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/petmade.jpg"}}" alt="Petmate" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/barketek.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/barketek.jpg"}}" alt="Barketek" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/pet-pee-pad.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/pet-pee-pad.jpg"}}" alt="Pet Pee Pad" /> </a></li>
<li><a href="{{store url='fish-products/fish-product-brands/okane.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/okane.jpg"}}" alt="Okane" /> </a></li>
<li><a href="{{store url='small-pets-products/small-pets-product-brands/kaytee.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/kaytee.jpg"}}" alt="Kaytee" /> </a></li>
</ul>
</div>
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$enBlock = Mage::getModel('cms/block');
$enBlock->setTitle($blockTitle);
$enBlock->setIdentifier($blockIdentifier);
$enBlock->setStores($blockStores);
$enBlock->setIsActive($blockIsActive);
$enBlock->setContent($content);
$enBlock->save();
//==========================================================================
//==========================================================================



//==========================================================================
// all-brands-slider (Petloft Thai)
//==========================================================================
$blockTitle = "All brands slider";
$blockIdentifier = "all-brands-slider";
$blockStores = array($th);
$blockIsActive = 1;

// delete old block if have
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete

$content = <<<EOD
<div class="recommend-hd-box best-selling-brands">
<div class="headRibbon"><span class="recommend-hd">OUR BRAND SELECTION</span></div>
</div>
<p>&nbsp;</p>
<div id="brandslide" style="margin-bottom: 30px;">
<ul id="brand-slides">
<li><a href="{{store url='dog-food/dog-product-brands/cesar.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/cesar.jpg"}}" alt="Cesar" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/science-diet.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/science-diet.jpg"}}" alt="Science Diet" /> </a></li>
<li><a href="{{store url='cat-food/cat-food-brands/whiskas.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/whiskas.jpg"}}" alt="Whiskas" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/royal-canin.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/royal-canin.jpg"}}" alt="Royal Canin" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/pedigree.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/pedigree.jpg"}}" alt="Pedigree" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/alpo.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/alpo.jpg"}}" alt="Alpo" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/dog-n-joy.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/dog-n-joy.jpg"}}" alt="Dog'n Joy" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/dr-luvcare.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/dr-luvcare.jpg"}}" alt="Dr. LuvCare" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/earthborn.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/earthborn.jpg"}}" alt="Earthborn" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/jerhigh.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/jerhigh.jpg"}}" alt="Jerhigh" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/perfecta.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/perfecta.jpg"}}" alt="Perfecta" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/pro-pac.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/pro-pac.jpg"}}" alt="Pro Pac" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/pro-plan.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/pro-plan.jpg"}}" alt="Purina Proplan" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/sleeky.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/sleeky.jpg"}}" alt="Sleeky" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/smartheart.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/smartheart.jpg"}}" alt="Smartheart" /> </a></li>
<li><a href="{{store url='cat-food/cat-food-brands/bellotta.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/bellotta.jpg"}}" alt="Bellotta" /> </a></li>
<li><a href="{{store url='cat-food/cat-food-brands/friskies.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/friskies.jpg"}}" alt="Friskies" /> </a></li>
<li><a href="{{store url='cat-food/cat-food-brands/purina.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/purina.jpg"}}" alt="Purina One" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/pinnacle.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/pinnacle.jpg"}}" alt="Pinnacle" /> </a></li>
<li><a href="{{store url='cat-food/cat-food-brands/meo.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/meo.jpg"}}" alt="Me-O" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/zeal.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/zeal.jpg"}}" alt="Zeal" /> </a></li>
<li><a href="{{store url='cat-food/cat-food-brands/catsan.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/catsan.jpg"}}" alt="Catsan" /> </a></li>
<li><a href="{{store url='cat-food/cat-food-brands/cat-s-best.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/cat-s-best.jpg"}}" alt="Cat's Best" /> </a></li>
<li><a href="{{store url='cat-food/cat-food-brands/ketty-love.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/ketty-love.jpg"}}" alt="Ketty Love" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/frontline.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/frontline.jpg"}}" alt="Frontline Plus" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/heartgard.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/heartgard.jpg"}}" alt="Heartgard" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/happy-molly.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/happy-molly.jpg"}}" alt="Happy Molly" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/avoderm.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/avoderm.jpg"}}" alt="AvoDerm" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/maxima.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/maxima.jpg"}}" alt="Maxima" /> </a></li>
<li><a href="{{store url='cat-food/cat-food-brands/1st-choice.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/1st-choice.jpg"}}" alt="1st Choice" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/dental-fresh.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/dental-fresh.jpg"}}" alt="Dental Fresh" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/monge-fresh.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/monge-fresh.jpg"}}" alt="Monge" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/petsown.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/petsown.jpg"}}" alt="Pet's Own" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/petstage.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/petstage.jpg"}}" alt="Petstage" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/petzlife.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/petzlife.jpg"}}" alt="PetzLife" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/pronature-holistic.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/pronature-holistic.jpg"}}" alt="Pronature Holistic" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/richard-organic.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/richard-organic.jpg"}}" alt="Richard Organic" /> </a></li>
<li><a href="{{store url='cat-food/cat-food-brands/shed-x-dermaplex.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/shed-x-dermaplex.jpg"}}" alt="Shed-X Dermaplex" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/sukina-petto.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/sukina-petto.jpg"}}" alt="Sukina Petto" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/touchdog.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/touchdog.jpg"}}" alt="Touchdog" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/veterinary-formula.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/veterinary-formula.jpg"}}" alt="Veterinary Formula" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/vitalife.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/vitalife.jpg"}}" alt="Vitalife" /> </a></li>
<li><a href="{{store url='cat-food/cat-food-brands/x-treme-catnip.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/x-treme-catnip.jpg"}}" alt="X-Treme Catnip" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/superdesign.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/superdesign.jpg"}}" alt="Super Designs" /> </a></li>
<li><a href="{{store url='cat-food/cat-food-brands/engage.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/engage.jpg"}}" alt="Engage Eco Products" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/dogkery.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/dogkery.jpg"}}" alt="Dogkery" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/greenpet.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/greenpet.jpg"}}" alt="Green Pet" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/homepet-thailand.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/homepet-thailand.jpg"}}" alt="Homepet Thailand" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/puppe.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/puppe.jpg"}}" alt="Puppe' Pet Cloths" /> </a></li>
<li><a href="{{store url='cat-food/cat-food-brands/cat-n-joy.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/cat-n-joy.jpg"}}" alt="Cat'n Joy" /> </a></li>
<li><a href="{{store url='cat-food/cat-food-brands/condoa-a-a.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/catcondo.jpg"}}" alt="Cat Condos" /> </a></li>
<li><a href="{{store url='cat-food/cat-food-brands/petsafe.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/petsafe.jpg"}}" alt="Petsafe" /> </a></li>
<li><a href="{{store url='cat-food/cat-food-brands/petmade.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/petmade.jpg"}}" alt="Petmate" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/barketek.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/barketek.jpg"}}" alt="Barketek" /> </a></li>
<li><a href="{{store url='dog-food/dog-product-brands/pet-pee-pad.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/pet-pee-pad.jpg"}}" alt="Pet Pee Pad" /> </a></li>
<li><a href="{{store url='fish-products/fish-product-brands/okane.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/okane.jpg"}}" alt="Okane" /> </a></li>
<li><a href="{{store url='small-pets-products/small-pets-product-brands/kaytee.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/petloft/kaytee.jpg"}}" alt="Kaytee" /> </a></li>
</ul>
</div>
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$enBlock = Mage::getModel('cms/block');
$enBlock->setTitle($blockTitle);
$enBlock->setIdentifier($blockIdentifier);
$enBlock->setStores($blockStores);
$enBlock->setIsActive($blockIsActive);
$enBlock->setContent($content);
$enBlock->save();
//==========================================================================
//==========================================================================



//==========================================================================
// all-brands-slider (Venbi English)
//==========================================================================
$blockTitle = "All brands slider";
$blockIdentifier = "all-brands-slider";
$blockStores = array($ven);
$blockIsActive = 1;

// delete old block if have
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete

$content = <<<EOD
<div class="recommend-hd-box best-selling-brands">
<div class="headRibbon"><span class="recommend-hd">OUR BRAND SELECTION</span></div>
</div>
<p>&nbsp;</p>
<div id="brandslide" style="margin-bottom: 30px;">
<ul id="brand-slides">
<li><a href="{{store url='brands/pigeon.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/venbi/Pigeon.jpg"}}" alt="Pigeon" /> </a></li>
<li><a href="{{store url='brands/mamy-poko.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/venbi/MamyPoko.jpg"}}" alt="Mamy Poko" /> </a></li>
<li><a href="{{store url='bath-baby-care/popular-brands/johnson-s-baby.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/venbi/Johnsons.jpg"}}" alt="Johnson's Baby" /> </a></li>
<li><a href="{{store url='brands/badger.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/venbi/Badger.jpg"}}" alt="Badger" /> </a></li>
<li><a href="{{store url='brands/s-26.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/venbi/S-26.jpg"}}" alt="S-26" /> </a></li>
<li><a href="{{store url='brands/drypers.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/venbi/Drypers.jpg"}}" alt="Dypers" /> </a></li>
<li><a href="{{store url='brands/babylove.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/venbi/BabyLove.jpg"}}" alt="BabyLove" /> </a></li>
<li><a href="{{store url='brands/cussons.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/venbi/Cussons.jpg"}}" alt="Cussons Baby" /> </a></li>
<li><a href="{{store url='brands/peachy.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/venbi/Peachy.jpg"}}" alt="Peachy" /> </a></li>
<li><a href="{{store url='brands/similac.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/venbi/Similac.jpg"}}" alt="Similac" /> </a></li>
<li><a href="{{store url='brands/enfa.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/venbi/Enfagrow.jpg"}}" alt="Enfa" /> </a></li>
<li><a href="{{store url='brands/pureen.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/venbi/Pureen.jpg"}}" alt="Pureen" /> </a></li>
<li><a href="{{store url='brands/dg.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/venbi/DG.jpg"}}" alt="DG" /> </a></li>
<li><a href="{{store url='catalogsearch/result' _query='q=Puma'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/venbi/Puma.jpg"}}" alt="Puma" /> </a></li>
<li><a href="{{store url='gear/popular-brands/mamas-papas.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/venbi/MamasPapas.jpg"}}" alt="Mamas Papas" /> </a></li>
<li><a href="{{store url='brands/merries.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/venbi/Merries.jpg"}}" alt="Merries" /> </a></li>
<li><a href="{{store url='diapers/types/swimming.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/venbi/Huggies.jpg"}}" alt="Huggies" /> </a></li>
<li><a href="{{store url='catalogsearch/result' _query='q=Earth+Best'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/venbi/EarthsBest.jpg"}}" alt="Earth's Best" /> </a></li>
<li><a href="{{store url='clothing-18/shoes.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/venbi/NewBalance.jpg"}}" alt="New Balance" /> </a></li>
<li><a href="{{store url='catalogsearch/result' _query='q=la+maison'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/venbi/LaMaison.jpg"}}" alt="La Maison du Savon de Marseille" /> </a></li>
<li><a href="{{store url='toys/popular-brands/playgro.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/venbi/Playgro.jpg"}}" alt="Playgro" /> </a></li>
<li><a href="{{store url='brands/applemonkey.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/venbi/AppleMonkey.jpg"}}" alt="Apple Monkey" /> </a></li>
<li><a href="{{store url='catalogsearch/result' _query='q=Vulli+Sophie+the+Giraffe'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/venbi/VulliSophietheGiraffe.jpg"}}" alt="Vulli Sophie the Giraffe" /> </a></li>
<li><a href="{{store url='catalogsearch/result' _query='q=Tiny+Love'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/venbi/TinyLove.jpg"}}" alt="Tiny Love" /> </a></li>
<li><a href="{{store url='catalogsearch/result' _query='q=Bright+Starts'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/venbi/BrightStarts.jpg"}}" alt="Bright Starts" /> </a></li>
<li><a href="{{store url='catalogsearch/result' _query='q=Buddy+Fruits'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/venbi/BuddyFruits.jpg"}}" alt="Buddy Fruits" /> </a></li>
<li><a href="{{store url='brands/goodmood.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/venbi/Goodmood.jpg"}}" alt="Good Mood" /> </a></li>
<li><a href="{{store url='catalogsearch/result' _query='q=the+First+Years'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/venbi/thefirstyears.jpg"}}" alt="the First Years" /> </a></li>
<li><a href="{{store url='catalogsearch/result' _query='q=Munchkin'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/venbi/Munchkin.jpg"}}" alt="Munchkin" /> </a></li>
<li><a href="{{store url='catalogsearch/result' _query='q=sassy'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/venbi/Sassy.jpg"}}" alt="Sassy" /> </a></li>
<li><a href="{{store url='catalogsearch/result' _query='q=Rhino+Toys'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/venbi/RhinoToys.jpg"}}" alt="Rhino Toys" /> </a></li>
<li><a href="{{store url='catalogsearch/result' _query='q=Mustachifier'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/venbi/Mustachifier.jpg"}}" alt="Mustachifier" /> </a></li>
<li><a href="{{store url='catalogsearch/result' _query='q=Kidco'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/venbi/Kidco.jpg"}}" alt="Kidco" /> </a></li>
<li><a href="{{store url='catalogsearch/result' _query='q=Little+Tikes'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/venbi/LittleTikes.jpg"}}" alt="Little Tikes" /> </a></li>
<li><a href="{{store url='catalogsearch/result' _query='q=Ciao+Baby'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/venbi/CiaoBaby.jpg"}}" alt="Ciao! Baby" /> </a></li>
</ul>
</div>
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$enBlock = Mage::getModel('cms/block');
$enBlock->setTitle($blockTitle);
$enBlock->setIdentifier($blockIdentifier);
$enBlock->setStores($blockStores);
$enBlock->setIsActive($blockIsActive);
$enBlock->setContent($content);
$enBlock->save();
//==========================================================================
//==========================================================================



//==========================================================================
// all-brands-slider (Venbi Thai)
//==========================================================================
$blockTitle = "All brands slider";
$blockIdentifier = "all-brands-slider";
$blockStores = array($vth);
$blockIsActive = 1;

// delete old block if have
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete

$content = <<<EOD
<div class="recommend-hd-box best-selling-brands">
<div class="headRibbon"><span class="recommend-hd">OUR BRAND SELECTION</span></div>
</div>
<p>&nbsp;</p>
<div id="brandslide" style="margin-bottom: 30px;">
<ul id="brand-slides">
<li><a href="{{store url='brands/pigeon.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/venbi/Pigeon.jpg"}}" alt="Pigeon" /> </a></li>
<li><a href="{{store url='brands/mamy-poko.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/venbi/MamyPoko.jpg"}}" alt="Mamy Poko" /> </a></li>
<li><a href="{{store url='bath-baby-care/popular-brands/johnson-s-baby.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/venbi/Johnsons.jpg"}}" alt="Johnson's Baby" /> </a></li>
<li><a href="{{store url='brands/badger.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/venbi/Badger.jpg"}}" alt="Badger" /> </a></li>
<li><a href="{{store url='brands/s-26.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/venbi/S-26.jpg"}}" alt="S-26" /> </a></li>
<li><a href="{{store url='brands/drypers.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/venbi/Drypers.jpg"}}" alt="Dypers" /> </a></li>
<li><a href="{{store url='brands/babylove.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/venbi/BabyLove.jpg"}}" alt="BabyLove" /> </a></li>
<li><a href="{{store url='brands/cussons.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/venbi/Cussons.jpg"}}" alt="Cussons Baby" /> </a></li>
<li><a href="{{store url='brands/peachy.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/venbi/Peachy.jpg"}}" alt="Peachy" /> </a></li>
<li><a href="{{store url='brands/similac.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/venbi/Similac.jpg"}}" alt="Similac" /> </a></li>
<li><a href="{{store url='brands/enfa.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/venbi/Enfagrow.jpg"}}" alt="Enfa" /> </a></li>
<li><a href="{{store url='brands/pureen.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/venbi/Pureen.jpg"}}" alt="Pureen" /> </a></li>
<li><a href="{{store url='brands/dg.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/venbi/DG.jpg"}}" alt="DG" /> </a></li>
<li><a href="{{store url='catalogsearch/result' _query='q=Puma'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/venbi/Puma.jpg"}}" alt="Puma" /> </a></li>
<li><a href="{{store url='gear/popular-brands/mamas-papas.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/venbi/MamasPapas.jpg"}}" alt="Mamas Papas" /> </a></li>
<li><a href="{{store url='brands/merries.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/venbi/Merries.jpg"}}" alt="Merries" /> </a></li>
<li><a href="{{store url='diapers/types/swimming.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/venbi/Huggies.jpg"}}" alt="Huggies" /> </a></li>
<li><a href="{{store url='catalogsearch/result' _query='q=Earth+Best'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/venbi/EarthsBest.jpg"}}" alt="Earth's Best" /> </a></li>
<li><a href="{{store url='clothing-18/shoes.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/venbi/NewBalance.jpg"}}" alt="New Balance" /> </a></li>
<li><a href="{{store url='catalogsearch/result' _query='q=la+maison'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/venbi/LaMaison.jpg"}}" alt="La Maison du Savon de Marseille" /> </a></li>
<li><a href="{{store url='toys/popular-brands/playgro.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/venbi/Playgro.jpg"}}" alt="Playgro" /> </a></li>
<li><a href="{{store url='brands/applemonkey.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/venbi/AppleMonkey.jpg"}}" alt="Apple Monkey" /> </a></li>
<li><a href="{{store url='catalogsearch/result' _query='q=Vulli+Sophie+the+Giraffe'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/venbi/VulliSophietheGiraffe.jpg"}}" alt="Vulli Sophie the Giraffe" /> </a></li>
<li><a href="{{store url='catalogsearch/result' _query='q=Tiny+Love'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/venbi/TinyLove.jpg"}}" alt="Tiny Love" /> </a></li>
<li><a href="{{store url='catalogsearch/result' _query='q=Bright+Starts'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/venbi/BrightStarts.jpg"}}" alt="Bright Starts" /> </a></li>
<li><a href="{{store url='catalogsearch/result' _query='q=Buddy+Fruits'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/venbi/BuddyFruits.jpg"}}" alt="Buddy Fruits" /> </a></li>
<li><a href="{{store url='brands/goodmood.html'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/venbi/Goodmood.jpg"}}" alt="Good Mood" /> </a></li>
<li><a href="{{store url='catalogsearch/result' _query='q=the+First+Years'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/venbi/thefirstyears.jpg"}}" alt="the First Years" /> </a></li>
<li><a href="{{store url='catalogsearch/result' _query='q=Munchkin'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/venbi/Munchkin.jpg"}}" alt="Munchkin" /> </a></li>
<li><a href="{{store url='catalogsearch/result' _query='q=sassy'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/venbi/Sassy.jpg"}}" alt="Sassy" /> </a></li>
<li><a href="{{store url='catalogsearch/result' _query='q=Rhino+Toys'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/venbi/RhinoToys.jpg"}}" alt="Rhino Toys" /> </a></li>
<li><a href="{{store url='catalogsearch/result' _query='q=Mustachifier'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/venbi/Mustachifier.jpg"}}" alt="Mustachifier" /> </a></li>
<li><a href="{{store url='catalogsearch/result' _query='q=Kidco'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/venbi/Kidco.jpg"}}" alt="Kidco" /> </a></li>
<li><a href="{{store url='catalogsearch/result' _query='q=Little+Tikes'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/venbi/LittleTikes.jpg"}}" alt="Little Tikes" /> </a></li>
<li><a href="{{store url='catalogsearch/result' _query='q=Ciao+Baby'}}" target="_blank"> <img src="{{media url="wysiwyg/brand-slide/venbi/CiaoBaby.jpg"}}" alt="Ciao! Baby" /> </a></li>
</ul>
</div>
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$enBlock = Mage::getModel('cms/block');
$enBlock->setTitle($blockTitle);
$enBlock->setIdentifier($blockIdentifier);
$enBlock->setStores($blockStores);
$enBlock->setIsActive($blockIsActive);
$enBlock->setContent($content);
$enBlock->save();
//==========================================================================
//==========================================================================



//==========================================================================
// all-brands-slider (Sanoga English)
//==========================================================================
$blockTitle = "All brands slider";
$blockIdentifier = "all-brands-slider";
$blockStores = array($sen);
$blockIsActive = 1;

// delete old block if have
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete

$content = <<<EOD
<div class="recommend-hd-box best-selling-brands">
<div class="headRibbon"><span class="recommend-hd">OUR BRAND SELECTION</span></div>
</div>
<div id="brandslide">
<ul id="brand-slides">
<li><a href="catalogsearch/result/?q=taurus" target="_blank"> <img src="{{media url="wysiwyg/Sanoga/taurus.jpg"}}" alt="Taurus" /> </a></li>
<li><a href="catalogsearch/result/?q=genesis" target="_blank"> <img src="{{media url="wysiwyg/Sanoga/genesis.jpg"}}" alt="Genesis" /> </a></li>
<li><a href="catalogsearch/result/?q=seoul+secret" target="_blank"> <img src="{{media url="wysiwyg/Sanoga/seoul_secret.jpg"}}" alt="Seoul Secret" /> </a></li>
<li><a href="catalogsearch/result/?q=baschi" target="_blank"> <img src="{{media url="wysiwyg/Sanoga/baschi.jpg"}}" alt="Baschi" /> </a></li>
<li><a href="catalogsearch/result/?q=bodyshape" target="_blank"> <img src="{{media url="wysiwyg/Sanoga/bodyshape.jpg"}}" alt="Bodyshape" /> </a></li>
<li><a href="catalogsearch/result/?q=clg500" target="_blank"> <img src="{{media url="wysiwyg/Sanoga/clg500.jpg"}}" alt="CLG500" /> </a></li>
<li><a href="catalogsearch/result/?q=hi-balanz" target="_blank"> <img src="{{media url="wysiwyg/Sanoga/hi-balanz.jpg"}}" alt="Hi-Balanz" /> </a></li>
<li><a href="catalogsearch/result/?q=k-palette" target="_blank"> <img src="{{media url="wysiwyg/Sanoga/k-palette.jpg"}}" alt="K-Palette" /> </a></li>
<li><a href="catalogsearch/result/?q=perorin" target="_blank"> <img src="{{media url="wysiwyg/Sanoga/perorin.jpg"}}" alt="Perorin" /> </a></li>
<li><a href="catalogsearch/result/?q=proflex" target="_blank"> <img src="{{media url="wysiwyg/Sanoga/proflex.jpg"}}" alt="ProFlex" /> </a></li>
</ul>
</div>
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$enBlock = Mage::getModel('cms/block');
$enBlock->setTitle($blockTitle);
$enBlock->setIdentifier($blockIdentifier);
$enBlock->setStores($blockStores);
$enBlock->setIsActive($blockIsActive);
$enBlock->setContent($content);
$enBlock->save();
//==========================================================================
//==========================================================================



//==========================================================================
// all-brands-slider (Sanoga Thai)
//==========================================================================
$blockTitle = "All brands slider";
$blockIdentifier = "all-brands-slider";
$blockStores = array($sth);
$blockIsActive = 1;

// delete old block if have
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete

$content = <<<EOD
<div class="recommend-hd-box best-selling-brands">
<div class="headRibbon"><span class="recommend-hd">OUR BRAND SELECTION</span></div>
</div>
<div id="brandslide">
<ul id="brand-slides">
<li><a href="catalogsearch/result/?q=seoul+secret" target="_blank"> <img src="{{media url="wysiwyg/Sanoga/seoul_secret.jpg"}}" alt="Seoul Secret" /> </a></li>
<li><a href="catalogsearch/result/?q=baschi" target="_blank"> <img src="{{media url="wysiwyg/Sanoga/baschi.jpg"}}" alt="Baschi" /> </a></li>
<li><a href="catalogsearch/result/?q=bodyshape" target="_blank"> <img src="{{media url="wysiwyg/Sanoga/bodyshape.jpg"}}" alt="Bodyshape" /> </a></li>
<li><a href="catalogsearch/result/?q=clg500" target="_blank"> <img src="{{media url="wysiwyg/Sanoga/clg500.jpg"}}" alt="CLG500" /> </a></li>
<li><a href="catalogsearch/result/?q=hi-balanz" target="_blank"> <img src="{{media url="wysiwyg/Sanoga/hi-balanz.jpg"}}" alt="Hi-Balanz" /> </a></li>
<li><a href="catalogsearch/result/?q=k-palette" target="_blank"> <img src="{{media url="wysiwyg/Sanoga/k-palette.jpg"}}" alt="K-Palette" /></a></li>
<li><a href="catalogsearch/result/?q=proflex" target="_blank"> <img src="{{media url="wysiwyg/Sanoga/proflex.jpg"}}" alt="ProFlex" /> </a></li>
</ul>
</div>
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$enBlock = Mage::getModel('cms/block');
$enBlock->setTitle($blockTitle);
$enBlock->setIdentifier($blockIdentifier);
$enBlock->setStores($blockStores);
$enBlock->setIsActive($blockIsActive);
$enBlock->setContent($content);
$enBlock->save();
//==========================================================================
//==========================================================================



//==========================================================================
// all-brands-slider (Lafema English, Thai)
//==========================================================================
$blockTitle = "All brands slider";
$blockIdentifier = "all-brands-slider";
$blockStores = array($len, $lth);
$blockIsActive = 1;

// delete old block if have
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete

$content = <<<EOD
<div class="lafema-recommend-hd-box">
<span class="lafema-recommend-hd"><span class="lafema-recommend-hd-tx">OUR BRAND SELECTION</span></span>
</div>
<div id="brandslide">
<ul id="brand-slides">
<li><a href="http://www.lafema.com/th/brands/bisous-bisous.html" target="_blank"> <img src="{{media url="wysiwyg/lafema/bisous.jpg"}}" alt="Bisous" /> </a></li>
<li><a href="http://www.lafema.com/th/brands/candydoll.html" target="_blank"> <img src="{{media url="wysiwyg/lafema/candydoll-logo.jpg"}}" alt="Candydoll" /> </a></li>
<li><a href="http://www.lafema.com/th/brands/catrice.html" target="_blank"> <img src="{{media url="wysiwyg/lafema/catrice-cosmetics-logo.jpg"}}" alt="Catrice" /> </a></li>
<li><a href="http://www.lafema.com/th/brands/etude.html" target="_blank"> <img src="{{media url="wysiwyg/lafema/etudy-logo.jpg"}}" alt="Etude" /> </a></li>
<li><a href="http://www.lafema.com/th/brands/the-face-shop.html" target="_blank"> <img src="{{media url="wysiwyg/lafema/faceshop.jpg"}}" alt="The Face Shop" /> </a></li>
<li><a href="http://www.lafema.com/th/brands/fairydrops.html" target="_blank"> <img src="{{media url="wysiwyg/lafema/fairydrops.jpg"}}" alt="Fairydrops" /> </a></li>
<li><a href="http://www.lafema.com/th/brands/kiss-me.html" target="_blank"> <img src="{{media url="wysiwyg/lafema/kiss-me-logo.jpg"}}" alt="Kiss Me" /> </a></li>
<li><a href="http://www.lafema.com/th/brands/lola.html" target="_blank"> <img src="{{media url="wysiwyg/lafema/lola-logo.jpg"}}" alt="Lola" /> </a></li>
<li><a href="http://www.lafema.com/th/brands/nyc.html" target="_blank"> <img src="{{media url="wysiwyg/lafema/nyc-logo.jpg"}}" alt="NYC" /> </a></li>
<li><a href="http://www.lafema.com/th/brands/shu-uemura.html" target="_blank"> <img src="{{media url="wysiwyg/lafema/shu-uemura-logo.jpg"}}" alt="Shu Uemura" /> </a></li>
<li><a href="http://www.lafema.com/th/brands/sleek.html" target="_blank"> <img src="{{media url="wysiwyg/lafema/sleek-logo.jpg"}}" alt="Sleek" /> </a></li>
<li><a href="http://www.lafema.com/th/brands/urban-decay.html" target="_blank"> <img src="{{media url="wysiwyg/lafema/urban-decay-logo.jpg"}}" alt="Urban Decay" /> </a></li>
<li><a href="http://www.lafema.com/th/brands/wet-n-wild.html" target="_blank"> <img src="{{media url="wysiwyg/lafema/wet-n-wild-logo.jpg"}}" alt="Wet N Wild" /> </a></li>
</ul>
</div>
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$enBlock = Mage::getModel('cms/block');
$enBlock->setTitle($blockTitle);
$enBlock->setIdentifier($blockIdentifier);
$enBlock->setStores($blockStores);
$enBlock->setIsActive($blockIsActive);
$enBlock->setContent($content);
$enBlock->save();
//==========================================================================
//==========================================================================