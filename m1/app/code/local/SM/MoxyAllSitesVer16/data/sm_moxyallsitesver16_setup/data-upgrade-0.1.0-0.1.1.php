<?php

// init Magento
Mage::app();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

// get store ids
$en     = Mage::getModel('core/store')->load('en', 'code')->getId();// Petloft
$th     = Mage::getModel('core/store')->load('th', 'code')->getId();
$len    = Mage::getModel('core/store')->load('len', 'code')->getId();// Lafema
$lth    = Mage::getModel('core/store')->load('lth', 'code')->getId();
$ven    = Mage::getModel('core/store')->load('ven', 'code')->getId();// Venbi
$vth    = Mage::getModel('core/store')->load('vth', 'code')->getId();
$sen    = Mage::getModel('core/store')->load('sen', 'code')->getId();// Sanoga
$sth    = Mage::getModel('core/store')->load('sth', 'code')->getId();
$moxyen = Mage::getModel('core/store')->load('moxyen', 'code')->getId();// Moxy
$moxyth = Mage::getModel('core/store')->load('moxyth', 'code')->getId();

// sort
$enStores = array($en, $len, $ven, $sen, $moxyen);
$thStores = array($th, $lth, $vth, $sth, $moxyth);

// delete old blocks
/** @var Mage_Cms_Model_Resource_Block_Collection $allBlocks */
$allBlocks = Mage::getModel('cms/block')->getCollection()
    ->addFieldToFilter('identifier', 'keymessage');
if($allBlocks->count() > 0){
    /** @var Mage_Cms_Model_Block $item */
    foreach($allBlocks as $item){
        if($item->getId()) $item->delete();
    }

    //==========================================================================
    // English Key Message
    //==========================================================================
    $enContent = <<<EOD
    <div class="keyMessBlock">
        <ul id="keyMess">
            <li class="key-ico keyMenu">
                <a class="tt" href="{{store url='help'}}#returns"> <span class="return-ico"><span class="tx-for-top-ico">30 Days Return Policy</span></span>
                    <div class="tooltip">
                        <div class="top">&nbsp;</div>
                        <div class="middle">In the rare circumstance that your item has a defect, you may send it back to us with a completed Returns Slip attached within 30 days upon receiving the item(s), a full refund will be issued to you.</div>
                    </div>
                </a>
            </li>
            <li class="key-ico keyMenu">
                <a class="tt"> <span class="callcenter-ico"><span class="tx-for-top-ico">Customer Service: 02-106-8222</span></span>
                    <div class="tooltip">
                        <div class="top">&nbsp;</div>
                        <div class="middle">If you have any problems, you can call us at 02-106-8222 (10am to 7pm) and we will do our best to help you out.</div>
                    </div>
                </a>
            </li>
            <li class="key-ico keyMenu">
                <a class="tt" href="{{store url='help'}}#payments"> <span class="cod-ico"><span class="tx-for-top-ico">Cash on Delivery</span> </span>
                    <div class="tooltip">
                        <div class="top">&nbsp;</div>
                        <div class="middle">Cash On delivery: With cash on delivery, we offer you the choice to pay once you have received the product from our messenger. This option carries a small charge of 50 baht/ order.</div>
                    </div>
                </a>
            </li>
        </ul>
    </div>
EOD;

    /** @var Mage_Cms_Model_Block $enBlock */
    $enBlock = Mage::getModel('cms/block');
    $enBlock->setTitle('English Key Message');
    $enBlock->setIdentifier('keymessage');
    $enBlock->setStores($enStores);
    $enBlock->setIsActive(1);
    $enBlock->setContent($enContent);
    $enBlock->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================



    //==========================================================================
    // Thailand Key Message
    //==========================================================================
    $thContent = <<<EOD
    <div class="keyMessBlock">
        <ul id="keyMess">
            <li class="key-ico keyMenu">
                <a class="tt" href="{{store url='help'}}#returns"> <span class="return-ico"><span class="tx-for-top-ico">รับคืนฟรี ภายใน 30วัน</span></span>
                    <div class="tooltip">
                        <div class="top">&nbsp;</div>
                        <div class="middle">ในบางกรณีซึ่งเป็นไปได้ยากที่สินค้าของคุณจะเกิดตำหนิ,เสียหาย หรือมีข้อผิดพลาด ดังนั้นหากเกิดกรณีดังกล่าวแล้วคุณสามารถดำเนินการขอคืนสินค้าและเงินค่าสินค้าเต็มจำนวน <strong>ภายในระยะเวลา 30 วัน</strong>หลังจากที่คุณได้รับสินค้าเรียบร้อยแล้ว</div>
                    </div>
                </a>
            </li>
            <li class="key-ico keyMenu">
                <a class="tt"> <span class="callcenter-ico"><span class="tx-for-top-ico">โทรสั่งซื้อ: 02-106-8222</span></span>
                    <div class="tooltip">
                        <div class="top">&nbsp;</div>
                        <div class="middle">ท่านสามารถติดต่อศูนย์บริการลูกค้าได้ที่ <strong>02-106-8222</strong><br />(จ-ส 10.00-19.00)<br />เรามีทีมงานที่ยินดีและพร้อมที่จะให้คำปรึกษากับท่าน กรุณาติดต่อเราหากท่านมีข้อสงสัยหรือได้รับความไม่สะดวกสบาย</div>
                    </div>
                </a>
            </li>
            <li class="key-ico keyMenu">
                <a class="tt" href="{{store url='help'}}#payments"> <span class="cod-ico"><span class="tx-for-top-ico">ชำระเงินปลายทาง</span> </span>
                    <div class="tooltip">
                        <div class="top">&nbsp;</div>
                        <div class="middle">เป็นการเก็บเงินปลายทางจากผู้รับสินค้า โดยจะมีค่าธรรมเนียมในการเก็บเงินปลายทาง 50 บาท ต่อการสั่งซื้อต่อครั้ง ค่าธรรมเนียมนี้ไม่รวมค่าสินค้า และค่าจัดส่งสินค้า</div>
                    </div>
                </a>
            </li>
        </ul>
    </div>
EOD;

    /** @var Mage_Cms_Model_Block $thBlock */
    $thBlock = Mage::getModel('cms/block');
    $thBlock->setTitle('Thailand Key Message');
    $thBlock->setIdentifier('keymessage');
    $thBlock->setStores($thStores);
    $thBlock->setIsActive(1);
    $thBlock->setContent($thContent);
    $thBlock->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================
}