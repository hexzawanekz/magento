<?php

//==========================================================================
// Order Tracking Share Links
//==========================================================================

/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter(array(0), $withAdmin = false)
    ->addFieldToFilter('identifier', 'banks_footer_order_tracking')
    ->getFirstItem();

if ($block->getId()) $block->delete();// if exists then delete
$content = <<<EOD
<a href="https://www.facebook.com/moxyst" target="_blank"><img src="{{skin url="images/fb.png"}}"></a>
<a href="https://twitter.com/moxyst" target="_blank"><img src="{{skin url="images/tw.png"}}"></a>
<a href="https://www.instagram.com/moxy_th/" target="_blank"><img src="{{skin url="images/google.png"}}"></a>
<a href="https://www.pinterest.com/MoxySEA1/" target="_blank"><img src="{{skin url="images/pet.png"}}"></a>
<a href="https://plus.google.com/+Moxyst/" target="_blank"><img src="{{skin url="images/googleplus.png"}}"></a>
EOD;

$block = Mage::getModel('cms/block');
$block->setTitle('Order Tracking Share Links');
$block->setIdentifier('banks_footer_order_tracking');
$block->setStores(array(0));
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================