<?php

// init Magento
Mage::app();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

// get store ids
$en     = Mage::getModel('core/store')->load('en', 'code')->getId();// Petloft
$th     = Mage::getModel('core/store')->load('th', 'code')->getId();
$len    = Mage::getModel('core/store')->load('len', 'code')->getId();// Lafema
$lth    = Mage::getModel('core/store')->load('lth', 'code')->getId();
$ven    = Mage::getModel('core/store')->load('ven', 'code')->getId();// Venbi
$vth    = Mage::getModel('core/store')->load('vth', 'code')->getId();
$sen    = Mage::getModel('core/store')->load('sen', 'code')->getId();// Sanoga
$sth    = Mage::getModel('core/store')->load('sth', 'code')->getId();
$moxyen = Mage::getModel('core/store')->load('moxyen', 'code')->getId();// Moxy
$moxyth = Mage::getModel('core/store')->load('moxyth', 'code')->getId();

$allStoresTH = array($th, $lth, $vth, $sth, $moxyth);
$allStoresEN = array($en, $len, $ven, $sen, $moxyen);
$allStores = array($th, $lth, $vth, $sth, $moxyth, $en, $len, $ven, $sen, $moxyen);

//==========================================================================
// header_right_image petloft en store
//==========================================================================
$blockTitle = "Site Footer Links EN";
$blockIdentifier = "footer_links_1";
$blockStores = $en;
$blockIsActive = 1;

// delete old block if have
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');

$content = <<<EOD
<ul class="footer_links" style="margin-left: 0;">
<li class="title">Information</li>
<li><a href="{{store url='about-us'}}">About Us</a></li>
<li><a href="{{store url='contacts'}}">Contact Us</a></li>
</ul>

<ul class="footer_links" style="margin-left: 42px;">
<li class="title">Security and Privacy</li>
<li><a href="{{store url='terms-and-conditions'}}">Terms &amp; Conditions</a></li>
<li><a href="{{store url='privacy-policy'}}">Privacy Policy</a></li>
</ul>

<ul id="footer_help" class="footer_links" style="margin-left: 42px;">
<li class="title">Services and Support</li>
<li><a href="{{store url='help'}}">Help and FAQ</a></li>
<li><a href="{{store url='help'}}#howtoorder">How To Order</a></li>
<li><a href="{{store url='help'}}#payments">Payments</a></li>
<li><a href="{{store url='help'}}#rewardpoints">Reward Points</a></li>
</ul>

<ul class="footer_links" style="margin-left: -20px; margin-top: 24px;">
<li><a href="{{store url='help'}}#shipping">Shipping &amp; Delivery</a></li>
<li><a href="{{store url='help'}}#returns">Cancellation &amp; Returns</a></li>
<li><a href="{{store url='return-policy'}}">Return Policy</a></li>
</ul>

<ul class="footer_links" style="margin-left: 34px;">
<li class="title">LINE CHAT</li>
<li><img style="width: 100px; height: 100px; margin-left: -15px;" src="{{media url="wysiwyg/PetLoft/PETLOFT_QR.jpg"}}" alt="" /></li>
</ul>
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$enBlock = Mage::getModel('cms/block');
$enBlock->setTitle($blockTitle);
$enBlock->setIdentifier($blockIdentifier);
$enBlock->setStores($blockStores);
$enBlock->setIsActive(1);
$enBlock->setContent($content);
$enBlock->save();
 
//==========================================================================
//==========================================================================

//==========================================================================
// header_right_image petloft th store
//==========================================================================
$blockTitle = "Site Footer Links TH";
$blockIdentifier = "footer_links_1";
$blockStores = $th;
$blockIsActive = 1;

// delete old block if have
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');

$content = <<<EOD
<ul class="footer_links" style="margin-left: 0;">
<li class="title">ข้อมูล</li>
<li><a href="{{store url='about-us'}}">เกี่ยวกับเรา</a></li>
<li><a href="{{store url='contacts'}}">ติดต่อเรา</a></li>
</ul>

<ul class="footer_links" style="margin-left: 42px;">
<li class="title">นโยบายความเป็นส่วนตัว</li>
<li><a href="{{store url='terms-and-conditions'}}">ข้อกำหนดและเงื่อนไข</a></li>
<li><a href="{{store url='privacy-policy'}}">ความเป็นส่วนตัว</a></li>
</ul>

<ul id="footer_help" class="footer_links" style="margin-left: 42px;">
<li class="title">ช่วยเหลือ</li>
<li><a href="{{store url='help'}}">คำถามที่พบบ่อย</a></li>
<li><a href="{{store url='help'}}#howtoorder">วิธีการสั่งซื้อ</a></li>
<li><a href="{{store url='help'}}#payments">การชำระเงิน</a></li>
<li><a href="{{store url='help'}}#rewardpoints">คะแนนสะสม</a></li>
</ul>

<ul class="footer_links" style="margin-left: 0px; margin-top: 24px;">
<li><a href="{{store url='help'}}#shipping">การจัดส่งสินค้า</a></li>
<li><a href="{{store url='help'}}#returns">การคืนสินค้าและขอคืนเงิน</a></li>
<li><a href="{{store url='return-policy'}}">นโยบายการคืนสินค้า</a></li>
</ul>

<ul class="footer_links" style="margin-left: 34px;">
<li class="title">LINE CHAT</li>
<li><img style="width: 100px; height: 100px; margin-left: -15px;" src="{{media url="wysiwyg/PetLoft/PETLOFT_QR.jpg"}}" alt="" /></li>
</ul>
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$enBlock = Mage::getModel('cms/block');
$enBlock->setTitle($blockTitle);
$enBlock->setIdentifier($blockIdentifier);
$enBlock->setStores($blockStores);
$enBlock->setIsActive(1);
$enBlock->setContent($content);
$enBlock->save();
 
//==========================================================================
//==========================================================================

//==========================================================================
// header_right_image venbi en store
//==========================================================================
$blockTitle = "Site Footer Links EN";
$blockIdentifier = "footer_links_1";
$blockStores = $ven;
$blockIsActive = 1;

// delete old block if have
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');

$content = <<<EOD
<ul class="footer_links" style="margin-left: 0;">
<li class="title">Information</li>
<li><a href="{{store url='about-us'}}">About Us</a></li>
<li><a href="{{store url='contacts'}}">Contact Us</a></li>
</ul>

<ul class="footer_links" style="margin-left: 42px;">
<li class="title">Security and Privacy</li>
<li><a href="{{store url='terms-and-conditions'}}">Terms &amp; Conditions</a></li>
<li><a href="{{store url='privacy-policy'}}">Privacy Policy</a></li>
</ul>

<ul id="footer_help" class="footer_links" style="margin-left: 42px;">
<li class="title">Services and Support</li>
<li><a href="{{store url='help'}}">Help and FAQ</a></li>
<li><a href="{{store url='help'}}#howtoorder">How To Order</a></li>
<li><a href="{{store url='help'}}#payments">Payments</a></li>
<li><a href="{{store url='help'}}#rewardpoints">Reward Points</a></li>
</ul>

<ul class="footer_links" style="margin-left: -20px; margin-top: 24px;">
<li><a href="{{store url='help'}}#shipping">Shipping &amp; Delivery</a></li>
<li><a href="{{store url='help'}}#returns">Cancellation &amp; Returns</a></li>
<li><a href="{{store url='return-policy'}}">Return Policy</a></li>
</ul>

<ul class="footer_links" style="margin-left: 34px;">
<li class="title">LINE CHAT</li>
<li><img style="width: 100px; height: 100px; margin-left: -15px;" src="{{media url="wysiwyg/Venbi/VENBI_QR.jpg"}}" alt="" /></li>
</ul>
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$enBlock = Mage::getModel('cms/block');
$enBlock->setTitle($blockTitle);
$enBlock->setIdentifier($blockIdentifier);
$enBlock->setStores($blockStores);
$enBlock->setIsActive(1);
$enBlock->setContent($content);
$enBlock->save();
 
//==========================================================================
//==========================================================================

//==========================================================================
// header_right_image venbi th store
//==========================================================================
$blockTitle = "Site Footer Links TH";
$blockIdentifier = "footer_links_1";
$blockStores = $vth;
$blockIsActive = 1;

// delete old block if have
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');

$content = <<<EOD
<ul class="footer_links" style="margin-left: 0;">
<li class="title">ข้อมูล</li>
<li><a href="{{store url='about-us'}}">เกี่ยวกับเรา</a></li>
<li><a href="{{store url='contacts'}}">ติดต่อเรา</a></li>
</ul>

<ul class="footer_links" style="margin-left: 42px;">
<li class="title">นโยบายความเป็นส่วนตัว</li>
<li><a href="{{store url='terms-and-conditions'}}">ข้อกำหนดและเงื่อนไข</a></li>
<li><a href="{{store url='privacy-policy'}}">ความเป็นส่วนตัว</a></li>
</ul>

<ul id="footer_help" class="footer_links" style="margin-left: 42px;">
<li class="title">ช่วยเหลือ</li>
<li><a href="{{store url='help'}}">คำถามที่พบบ่อย</a></li>
<li><a href="{{store url='help'}}#howtoorder">วิธีการสั่งซื้อ</a></li>
<li><a href="{{store url='help'}}#payments">การชำระเงิน</a></li>
<li><a href="{{store url='help'}}#rewardpoints">คะแนนสะสม</a></li>
</ul>

<ul class="footer_links" style="margin-left: 0px; margin-top: 24px;">
<li><a href="{{store url='help'}}#shipping">การจัดส่งสินค้า</a></li>
<li><a href="{{store url='help'}}#returns">การคืนสินค้าและขอคืนเงิน</a></li>
<li><a href="{{store url='return-policy'}}">นโยบายการคืนสินค้า</a></li>
</ul>

<ul class="footer_links" style="margin-left: 34px;">
<li class="title">LINE CHAT</li>
<li><img style="width: 100px; height: 100px; margin-left: -15px;" src="{{media url="wysiwyg/Venbi/VENBI_QR.jpg"}}" alt="" /></li>
</ul>
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$enBlock = Mage::getModel('cms/block');
$enBlock->setTitle($blockTitle);
$enBlock->setIdentifier($blockIdentifier);
$enBlock->setStores($blockStores);
$enBlock->setIsActive(1);
$enBlock->setContent($content);
$enBlock->save();
 
//==========================================================================
//==========================================================================

//==========================================================================
// header_right_image sanoga en store
//==========================================================================
$blockTitle = "Site Footer Links EN";
$blockIdentifier = "footer_links_1";
$blockStores = $sen;
$blockIsActive = 1;

// delete old block if have
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');

$content = <<<EOD
<ul class="footer_links" style="margin-left: 0;">
<li class="title">Information</li>
<li><a href="{{store url='about-us'}}">About Us</a></li>
<li><a href="{{store url='contacts'}}">Contact Us</a></li>
</ul>

<ul class="footer_links" style="margin-left: 42px;">
<li class="title">Security and Privacy</li>
<li><a href="{{store url='terms-and-conditions'}}">Terms &amp; Conditions</a></li>
<li><a href="{{store url='privacy-policy'}}">Privacy Policy</a></li>
</ul>

<ul id="footer_help" class="footer_links" style="margin-left: 42px;">
<li class="title">Services and Support</li>
<li><a href="{{store url='help'}}">Help and FAQ</a></li>
<li><a href="{{store url='help'}}#howtoorder">How To Order</a></li>
<li><a href="{{store url='help'}}#payments">Payments</a></li>
<li><a href="{{store url='help'}}#rewardpoints">Reward Points</a></li>
</ul>

<ul class="footer_links" style="margin-left: -20px; margin-top: 24px;">
<li><a href="{{store url='help'}}#shipping">Shipping &amp; Delivery</a></li>
<li><a href="{{store url='help'}}#returns">Cancellation &amp; Returns</a></li>
<li><a href="{{store url='return-policy'}}">Return Policy</a></li>
</ul>

<ul class="footer_links" style="margin-left: 34px;">
<li class="title">LINE CHAT</li>
<li><img style="width: 100px; height: 100px; margin-left: -15px;" src="{{media url="wysiwyg/Sanoga/SANOGA_QR.jpg"}}" alt="" /></li>
</ul>
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$enBlock = Mage::getModel('cms/block');
$enBlock->setTitle($blockTitle);
$enBlock->setIdentifier($blockIdentifier);
$enBlock->setStores($blockStores);
$enBlock->setIsActive(1);
$enBlock->setContent($content);
$enBlock->save();
 
//==========================================================================
//==========================================================================

//==========================================================================
// header_right_image sanoga th store
//==========================================================================
$blockTitle = "Site Footer Links TH";
$blockIdentifier = "footer_links_1";
$blockStores = $sth;
$blockIsActive = 1;

// delete old block if have
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');

$content = <<<EOD
<ul class="footer_links" style="margin-left: 0;">
<li class="title">ข้อมูล</li>
<li><a href="{{store url='about-us'}}">เกี่ยวกับเรา</a></li>
<li><a href="{{store url='contacts'}}">ติดต่อเรา</a></li>
</ul>

<ul class="footer_links" style="margin-left: 42px;">
<li class="title">นโยบายความเป็นส่วนตัว</li>
<li><a href="{{store url='terms-and-conditions'}}">ข้อกำหนดและเงื่อนไข</a></li>
<li><a href="{{store url='privacy-policy'}}">ความเป็นส่วนตัว</a></li>
</ul>

<ul id="footer_help" class="footer_links" style="margin-left: 42px;">
<li class="title">ช่วยเหลือ</li>
<li><a href="{{store url='help'}}">คำถามที่พบบ่อย</a></li>
<li><a href="{{store url='help'}}#howtoorder">วิธีการสั่งซื้อ</a></li>
<li><a href="{{store url='help'}}#payments">การชำระเงิน</a></li>
<li><a href="{{store url='help'}}#rewardpoints">คะแนนสะสม</a></li>
</ul>

<ul class="footer_links" style="margin-left: 0px; margin-top: 24px;">
<li><a href="{{store url='help'}}#shipping">การจัดส่งสินค้า</a></li>
<li><a href="{{store url='help'}}#returns">การคืนสินค้าและขอคืนเงิน</a></li>
<li><a href="{{store url='return-policy'}}">นโยบายการคืนสินค้า</a></li>
</ul>

<ul class="footer_links" style="margin-left: 34px;">
<li class="title">LINE CHAT</li>
<li><img style="width: 100px; height: 100px; margin-left: -15px;" src="{{media url="wysiwyg/Sanoga/SANOGA_QR.jpg"}}" alt="" /></li>
</ul>
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$enBlock = Mage::getModel('cms/block');
$enBlock->setTitle($blockTitle);
$enBlock->setIdentifier($blockIdentifier);
$enBlock->setStores($blockStores);
$enBlock->setIsActive(1);
$enBlock->setContent($content);
$enBlock->save();
 
//==========================================================================
//==========================================================================

//==========================================================================
// header_right_image lafema en store
//==========================================================================
$blockTitle = "Site Footer Links EN";
$blockIdentifier = "footer_links_1";
$blockStores = $len;
$blockIsActive = 1;

// delete old block if have
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');

$content = <<<EOD
<ul class="footer_links" style="margin-left: 0;">
<li class="title">Information</li>
<li><a href="{{store url='about-us'}}">About Us</a></li>
<li><a href="{{store url='contacts'}}">Contact Us</a></li>
</ul>

<ul class="footer_links" style="margin-left: 42px;">
<li class="title">Security and Privacy</li>
<li><a href="{{store url='terms-and-conditions'}}">Terms &amp; Conditions</a></li>
<li><a href="{{store url='privacy-policy'}}">Privacy Policy</a></li>
</ul>

<ul id="footer_help" class="footer_links" style="margin-left: 42px;">
<li class="title">Services and Support</li>
<li><a href="{{store url='help'}}">Help and FAQ</a></li>
<li><a href="{{store url='help'}}#howtoorder">How To Order</a></li>
<li><a href="{{store url='help'}}#payments">Payments</a></li>
<li><a href="{{store url='help'}}#rewardpoints">Reward Points</a></li>
</ul>

<ul class="footer_links" style="margin-left: -20px; margin-top: 24px;">
<li><a href="{{store url='help'}}#shipping">Shipping &amp; Delivery</a></li>
<li><a href="{{store url='help'}}#returns">Cancellation &amp; Returns</a></li>
<li><a href="{{store url='return-policy'}}">Return Policy</a></li>
</ul>

<ul class="footer_links" style="margin-left: 34px;">
<li class="title">LINE CHAT</li>
<li><img style="width: 100px; height: 100px; margin-left: -15px;" src="{{media url="wysiwyg/Lafema/LAFEMA_QR.jpg"}}" alt="" /></li>
</ul>
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$enBlock = Mage::getModel('cms/block');
$enBlock->setTitle($blockTitle);
$enBlock->setIdentifier($blockIdentifier);
$enBlock->setStores($blockStores);
$enBlock->setIsActive(1);
$enBlock->setContent($content);
$enBlock->save();
 
//==========================================================================
//==========================================================================

//==========================================================================
// header_right_image lafema th store
//==========================================================================
$blockTitle = "Site Footer Links TH";
$blockIdentifier = "footer_links_1";
$blockStores = $lth;
$blockIsActive = 1;

// delete old block if have
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');

$content = <<<EOD
<ul class="footer_links" style="margin-left: 0;">
<li class="title">ข้อมูล</li>
<li><a href="{{store url='about-us'}}">เกี่ยวกับเรา</a></li>
<li><a href="{{store url='contacts'}}">ติดต่อเรา</a></li>
</ul>

<ul class="footer_links" style="margin-left: 42px;">
<li class="title">นโยบายความเป็นส่วนตัว</li>
<li><a href="{{store url='terms-and-conditions'}}">ข้อกำหนดและเงื่อนไข</a></li>
<li><a href="{{store url='privacy-policy'}}">ความเป็นส่วนตัว</a></li>
</ul>

<ul id="footer_help" class="footer_links" style="margin-left: 42px;">
<li class="title">ช่วยเหลือ</li>
<li><a href="{{store url='help'}}">คำถามที่พบบ่อย</a></li>
<li><a href="{{store url='help'}}#howtoorder">วิธีการสั่งซื้อ</a></li>
<li><a href="{{store url='help'}}#payments">การชำระเงิน</a></li>
<li><a href="{{store url='help'}}#rewardpoints">คะแนนสะสม</a></li>
</ul>

<ul class="footer_links" style="margin-left: 0px; margin-top: 24px;">
<li><a href="{{store url='help'}}#shipping">การจัดส่งสินค้า</a></li>
<li><a href="{{store url='help'}}#returns">การคืนสินค้าและขอคืนเงิน</a></li>
<li><a href="{{store url='return-policy'}}">นโยบายการคืนสินค้า</a></li>
</ul>

<ul class="footer_links" style="margin-left: 34px;">
<li class="title">LINE CHAT</li>
<li><img style="width: 100px; height: 100px; margin-left: -15px;" src="{{media url="wysiwyg/Lafema/LAFEMA_QR.jpg"}}" alt="" /></li>
</ul>
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$enBlock = Mage::getModel('cms/block');
$enBlock->setTitle($blockTitle);
$enBlock->setIdentifier($blockIdentifier);
$enBlock->setStores($blockStores);
$enBlock->setIsActive(1);
$enBlock->setContent($content);
$enBlock->save();
 
//==========================================================================
//==========================================================================

//==========================================================================
// header_right_image moxy en store
//==========================================================================
$blockTitle = "Site Footer Links EN";
$blockIdentifier = "footer_links_1";
$blockStores = $moxyen;
$blockIsActive = 1;

// delete old block if have
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');

$content = <<<EOD
<ul class="footer_links" style="margin-left: 0;">
<li class="title">Information</li>
<li><a href="{{store url='about-us'}}">About Us</a></li>
<li><a href="{{store url='contacts'}}">Contact Us</a></li>
</ul>

<ul class="footer_links" style="margin-left: 42px;">
<li class="title">Security and Privacy</li>
<li><a href="{{store url='terms-and-conditions'}}">Terms &amp; Conditions</a></li>
<li><a href="{{store url='privacy-policy'}}">Privacy Policy</a></li>
</ul>

<ul id="footer_help" class="footer_links" style="margin-left: 42px;">
<li class="title">Services and Support</li>
<li><a href="{{store url='help'}}">Help and FAQ</a></li>
<li><a href="{{store url='help'}}#howtoorder">How To Order</a></li>
<li><a href="{{store url='help'}}#payments">Payments</a></li>
<li><a href="{{store url='help'}}#rewardpoints">Reward Points</a></li>
</ul>

<ul class="footer_links" style="margin-left: -20px; margin-top: 24px;">
<li><a href="{{store url='help'}}#shipping">Shipping &amp; Delivery</a></li>
<li><a href="{{store url='help'}}#returns">Cancellation &amp; Returns</a></li>
<li><a href="{{store url='return-policy'}}">Return Policy</a></li>
</ul>

<ul class="footer_links" style="margin-left: 34px;">
<li class="title">LINE CHAT</li>
<li><img style="width: 100px; height: 100px; margin-left: -15px;" src="{{media url="wysiwyg/Moxy/MOXY_QR.jpg"}}" alt="" /></li>
</ul>
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$enBlock = Mage::getModel('cms/block');
$enBlock->setTitle($blockTitle);
$enBlock->setIdentifier($blockIdentifier);
$enBlock->setStores($blockStores);
$enBlock->setIsActive(1);
$enBlock->setContent($content);
$enBlock->save();
 
//==========================================================================
//==========================================================================

//==========================================================================
// header_right_image moxy th store
//==========================================================================
$blockTitle = "Site Footer Links TH";
$blockIdentifier = "footer_links_1";
$blockStores = $moxyth;
$blockIsActive = 1;

// delete old block if have
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');

$content = <<<EOD
<ul class="footer_links" style="margin-left: 0;">
<li class="title">ข้อมูล</li>
<li><a href="{{store url='about-us'}}">เกี่ยวกับเรา</a></li>
<li><a href="{{store url='contacts'}}">ติดต่อเรา</a></li>
</ul>

<ul class="footer_links" style="margin-left: 42px;">
<li class="title">นโยบายความเป็นส่วนตัว</li>
<li><a href="{{store url='terms-and-conditions'}}">ข้อกำหนดและเงื่อนไข</a></li>
<li><a href="{{store url='privacy-policy'}}">ความเป็นส่วนตัว</a></li>
</ul>

<ul id="footer_help" class="footer_links" style="margin-left: 42px;">
<li class="title">ช่วยเหลือ</li>
<li><a href="{{store url='help'}}">คำถามที่พบบ่อย</a></li>
<li><a href="{{store url='help'}}#howtoorder">วิธีการสั่งซื้อ</a></li>
<li><a href="{{store url='help'}}#payments">การชำระเงิน</a></li>
<li><a href="{{store url='help'}}#rewardpoints">คะแนนสะสม</a></li>
</ul>

<ul class="footer_links" style="margin-left: 0px; margin-top: 24px;">
<li><a href="{{store url='help'}}#shipping">การจัดส่งสินค้า</a></li>
<li><a href="{{store url='help'}}#returns">การคืนสินค้าและขอคืนเงิน</a></li>
<li><a href="{{store url='return-policy'}}">นโยบายการคืนสินค้า</a></li>
</ul>

<ul class="footer_links" style="margin-left: 34px;">
<li class="title">LINE CHAT</li>
<li><img style="width: 100px; height: 100px; margin-left: -15px;" src="{{media url="wysiwyg/Moxy/MOXY_QR.jpg"}}" alt="" /></li>
</ul>
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$enBlock = Mage::getModel('cms/block');
$enBlock->setTitle($blockTitle);
$enBlock->setIdentifier($blockIdentifier);
$enBlock->setStores($blockStores);
$enBlock->setIsActive(1);
$enBlock->setContent($content);
$enBlock->save();
 
//==========================================================================
//==========================================================================

//==========================================================================
// Banks Footer petloft store
//==========================================================================
$blockTitle = "Site Footer Links TH";
$blockIdentifier = "banks_footer";
$blockStores = array($en, $th);
$blockIsActive = 1;

// delete old block if have
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');

$content = <<<EOD
<p><a href="https://plus.google.com/115593276916117018663/" target="_blank"> <span class="google-footer"><img src="{{media url="wysiwyg/footer_follow/google_follow.png"}}" alt="" width="32px" /></span> </a> <a href="https://pinterest.com/#" target="_blank"> <span class="pinterest-footer"><img src="{{media url="wysiwyg/footer_follow/pinterest_follow.png"}}" alt="" width="32px" /></span> </a> <a href="http://webstagra.ms/petloft" target="_blank"> <span class="instagram-footer"><img src="{{media url="wysiwyg/footer_follow/instagram_follow.png"}}" alt="" width="32px" /></span> </a> <a href="https://twitter.com/venbithai" target="_blank"> <span class="twitter-footer"><img src="{{media url="wysiwyg/footer_follow/twitter_follow.png"}}" alt="" width="32px" /></span> </a>  <a href="https://www.facebook.com/petloftthai" target="_blank"> <span class="facebook-footer"><img src="{{media url="wysiwyg/footer_follow/facebook_follow.png"}}" alt="" width="32px" /></span> </a></p>
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$enBlock = Mage::getModel('cms/block');
$enBlock->setTitle($blockTitle);
$enBlock->setIdentifier($blockIdentifier);
$enBlock->setStores($blockStores);
$enBlock->setIsActive(1);
$enBlock->setContent($content);
$enBlock->save();
 
//==========================================================================
//==========================================================================

//==========================================================================
// Banks Footer venbi store
//==========================================================================
$blockTitle = "Site Footer Links TH";
$blockIdentifier = "banks_footer";
$blockStores = array($ven, $vth);
$blockIsActive = 1;

// delete old block if have
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');

$content = <<<EOD
<p><a href="https://plus.google.com/115593276916117018663/" target="_blank"> <span class="google-footer"><img src="{{media url="wysiwyg/footer_follow/google_follow.png"}}" alt="" width="32px" /></span> </a> <a href="https://pinterest.com/#" target="_blank"> <span class="pinterest-footer"><img src="{{media url="wysiwyg/footer_follow/pinterest_follow.png"}}" alt="" width="32px" /></span> </a> <a href="http://webstagra.ms/venbi_thailand" target="_blank"> <span class="instagram-footer"><img src="{{media url="wysiwyg/footer_follow/instagram_follow.png"}}" alt="" width="32px" /></span> </a> <a href="https://twitter.com/venbithai" target="_blank"> <span class="twitter-footer"><img src="{{media url="wysiwyg/footer_follow/twitter_follow.png"}}" alt="" width="32px" /></span> </a> <a href="https://www.facebook.com/venbithai" target="_blank"> <span class="facebook-footer"><img src="{{media url="wysiwyg/footer_follow/facebook_follow.png"}}" alt="" width="32px" /></span> </a></p>
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$enBlock = Mage::getModel('cms/block');
$enBlock->setTitle($blockTitle);
$enBlock->setIdentifier($blockIdentifier);
$enBlock->setStores($blockStores);
$enBlock->setIsActive(1);
$enBlock->setContent($content);
$enBlock->save();
 
//==========================================================================
//==========================================================================

//==========================================================================
// Banks Footer sanoga store
//==========================================================================
$blockTitle = "Site Footer Links TH";
$blockIdentifier = "banks_footer";
$blockStores = array($sen, $sth);
$blockIsActive = 1;

// delete old block if have
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');

$content = <<<EOD
<p><a href="https://plus.google.com/115593276916117018663/" target="_blank"> <span class="google-footer"><img src="{{media url="wysiwyg/footer_follow/google_follow.png"}}" alt="" width="32px" /></span> </a> <a href="https://pinterest.com/#" target="_blank"> <span class="pinterest-footer"><img src="{{media url="wysiwyg/footer_follow/pinterest_follow.png"}}" alt="" width="32px" /></span> </a> <a href="http://webstagra.ms/venbi_thailand" target="_blank"> <span class="instagram-footer"><img src="{{media url="wysiwyg/footer_follow/instagram_follow.png"}}" alt="" width="32px" /></span> </a> <a href="https://twitter.com/venbithai" target="_blank"> <span class="twitter-footer"><img src="{{media url="wysiwyg/footer_follow/twitter_follow.png"}}" alt="" width="32px" /></span> </a> <a href="https://www.facebook.com/SanogaThailand" target="_blank"> <span class="facebook-footer"><img src="{{media url="wysiwyg/footer_follow/facebook_follow.png"}}" alt="" width="32px" /></span> </a></p>
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$enBlock = Mage::getModel('cms/block');
$enBlock->setTitle($blockTitle);
$enBlock->setIdentifier($blockIdentifier);
$enBlock->setStores($blockStores);
$enBlock->setIsActive(1);
$enBlock->setContent($content);
$enBlock->save();
 
//==========================================================================
//==========================================================================

//==========================================================================
// Banks Footer lafema store
//==========================================================================
$blockTitle = "Site Footer Links TH";
$blockIdentifier = "banks_footer";
$blockStores = array($len, $lth);
$blockIsActive = 1;

// delete old block if have
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');

$content = <<<EOD
<p><a href="https://plus.google.com/115593276916117018663/" target="_blank"> <span class="google-footer"><img src="{{media url="wysiwyg/footer_follow/google_follow.png"}}" alt="" width="32px" /></span> </a> <a href="https://pinterest.com/#" target="_blank"> <span class="pinterest-footer"><img src="{{media url="wysiwyg/footer_follow/pinterest_follow.png"}}" alt="" width="32px" /></span> </a> <a href="http://webstagra.ms/lafema.th" target="_blank"> <span class="instagram-footer"><img src="{{media url="wysiwyg/footer_follow/instagram_follow.png"}}" alt="" width="32px" /></span> </a> <a href="https://twitter.com/venbithai" target="_blank"> <span class="twitter-footer"><img src="{{media url="wysiwyg/footer_follow/twitter_follow.png"}}" alt="" width="32px" /></span> </a> <a href="https://www.facebook.com/lafemathai" target="_blank"> <span class="facebook-footer"><img src="{{media url="wysiwyg/footer_follow/facebook_follow.png"}}" alt="" width="32px" /></span> </a></p>
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$enBlock = Mage::getModel('cms/block');
$enBlock->setTitle($blockTitle);
$enBlock->setIdentifier($blockIdentifier);
$enBlock->setStores($blockStores);
$enBlock->setIsActive(1);
$enBlock->setContent($content);
$enBlock->save();
 
//==========================================================================
//==========================================================================

//==========================================================================
// Banks Footer moxy store
//==========================================================================
$blockTitle = "Site Footer Links TH";
$blockIdentifier = "banks_footer";
$blockStores = array($moxyen, $moxyth);
$blockIsActive = 1;

// delete old block if have
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');

$content = <<<EOD
<p><a href="https://plus.google.com/+Moxyst" target="_blank"> <span class="google-footer"><img src="{{media url="wysiwyg/footer_follow/google_follow.png"}}" alt="" width="32px" /></span> </a> <a href="https://pinterest.com/#" target="_blank"> <span class="pinterest-footer"><img src="{{media url="wysiwyg/footer_follow/pinterest_follow.png"}}" alt="" width="32px" /></span> </a> <a href="https://webstagra.ms/moxyst/" target="_blank"> <span class="instagram-footer"><img src="{{media url="wysiwyg/footer_follow/instagram_follow.png"}}" alt="" width="32px" /></span> </a> <a href="https://twitter.com/moxyst" target="_blank"> <span class="twitter-footer"><img src="{{media url="wysiwyg/footer_follow/twitter_follow.png"}}" alt="" width="32px" /></span> </a> <a href="https://www.facebook.com/moxyst" target="_blank"> <span class="facebook-footer"><img src="{{media url="wysiwyg/footer_follow/facebook_follow.png"}}" alt="" width="32px" /></span> </a></p>
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$enBlock = Mage::getModel('cms/block');
$enBlock->setTitle($blockTitle);
$enBlock->setIdentifier($blockIdentifier);
$enBlock->setStores($blockStores);
$enBlock->setIsActive(1);
$enBlock->setContent($content);
$enBlock->save();
 
//==========================================================================
//==========================================================================

//==========================================================================
// Bank image footer all store
//==========================================================================
$blockTitle = "Bank image footer";
$blockIdentifier = "bank_image_footer";
$blockStores = array(0);
$blockIsActive = 1;

// delete old block if have
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');

$content = <<<EOD
<div class="bank-list-images">
<p class="payment_partners_footer">Payment Partners</p>
<img src="{{media url="wysiwyg/bank_images_footer.png"}}" alt="" width="450px" />
</div>
<div class="acommerce-logo-footer">
<p class="shipping_partners_footer">Shipping Partners</p>
<img src="{{media url="wysiwyg/acommerce_footer.png"}}" alt="" width="185px" />
</div>
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$enBlock = Mage::getModel('cms/block');
$enBlock->setTitle($blockTitle);
$enBlock->setIdentifier($blockIdentifier);
$enBlock->setStores($blockStores);
$enBlock->setIsActive(1);
$enBlock->setContent($content);
$enBlock->save();
 
//==========================================================================
//==========================================================================

// save config for store views
$configurator = new Mage_Core_Model_Config();
// English
$configurator->saveConfig('design/footer/copyright', 'Copyright &copy;2015 Moxy. All rights reserved', 'default', 0);

