<?php


/** @var Mage_Core_Model_Resource_Setup $installer */
$installer = $this;

$installer->startSetup();

/** Order Cancellation Mail Template Moxy */
/** @var Mage_Adminhtml_Model_Email_Template $mailTemplateModel */
$mailTemplateModel = Mage::getModel('adminhtml/email_template');

$mailTemplateModel->loadByCode('Auction Over bid');
$mailTemplateModel->setTemplateCode('Auction Over bid');
$mailTemplateModel->setTemplateSubject('{{var store.getFrontendName()}}: Over bid # {{var bid.id}} on {{var store.getFrontendName()}}');
$mailTemplateModel->setTemplateType(2);

$mailContent = <<<HTML
<meta charset="utf-8"/>
<div style="font-family: 'gotham_htfbook', Arial, san-serif;color: #3C3D41;font-size:14px; text-align: center; width: 70%; margin: auto">
    <img src="{{skin url='images/CartReminder_650_MOXYORAMI.jpg' _area='frontend'}}" alt="{{var store.getFrontendName()}}"  style="margin-bottom:10px; width: 80%;" border="0"/>
    <h1 style="font-size:20px; margin-bottom:20px; font-weight: normal;letter-spacing: 3px;
    text-align: center;margin-top: 20px;font-family: inherit;color: inherit;">
        SOMEONE JUST OUTBID YOU
    </h1>
    <div style="float: left;width: 66%; text-align: left">
        <div style="padding: 0 15px;">
            <p>Dear  <span style="font-weight: bold">{{var bid.getCustomerName()}}</span>,</p>
            <p><span style="font-weight: bold">{{var higher_bid.getBidderName()}}</span> just made a bid on a product at <span style="color:#EE5191; font-size: 14px; font-weight: bold;">{{var higher_bid.getFormatedPrice()}}</span> at <span style="font-weight: bold;">{{var higher_bid.getFormatedTime()}}</span>.</p>
            <p>To win this product over, click <a href="{{var auction_url}}" style="color:#3C3D41;">here</a> to outbid them!</p>
            <p style="margin-bottom: 0">Warm Regards,</p>
            <p style="margin-top: 3px;">Moxy Team</p>
        </div>
    </div>
    <div style="float: right;width: 33%; border-left:1px solid #F2F2F2; text-align: center;">
        <div style="padding: 0 15px;">
            <div style="padding: 2px; width: 100%; text-align: center">
                <img src="{{var image_url}}" style="max-height: 150px;max-width: 100%;border: #E7E7E7 1px solid; ">
            </div>
            <div style="margin: 10px 0;white-space: nowrap;text-overflow: ellipsis;overflow: hidden;">
                <a href="{{var auction_url}}" style="text-decoration: none; font-weight: bold; color:#73767B;">
                    {{var higher_bid.getProductName()}}
                </a>
            </div>
            <div style="margin: 10px 0 0 0; color:#EE5191; font-size: 14px; font-weight: bold;">
                {{var higher_bid.getFormatedPrice()}}
            </div>
            <div style="margin: 15px 0;font-weight: bold;color: #73767B;font-size: 14px;">
                {{var bid.getTimeLeft()}}
            </div>
            <div style="margin-bottom: 10px;">
                <a style="background-color: #EE5191;color: #fff;padding: 6px 12px; text-decoration: none" href="{{var auction_url}}">BID AGAIN</a>
            </div>
            <div><p>Or change to <a href="{{var auction_url}}" style="color: #3C3D41;">Auto Bid</a></p></div>
        </div>
    </div>
    <div style="clear: both;"></div>
</div>
HTML;

$mailTemplateModel->setTemplateText($mailContent);
$mailTemplateModel->save();

$installer->endSetup();

