<?php

$moxyen = Mage::getModel('core/store')->load('moxyen', 'code')->getId();// Moxy
$moxyth = Mage::getModel('core/store')->load('moxyth', 'code')->getId();

//==========================================================================
// Welcome global link sites EN
//==========================================================================
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter(array($moxyen,$moxyth), $withAdmin = false)
    ->addFieldToFilter('identifier', 'welcome-mobile-follow-link')
    ->getFirstItem();

if ($block->getId()) $block->delete();// if exists then delete
$content = <<<EOD
<ul>
    <li><a href="https://www.facebook.com/moxyst"><span class="fb-icon"></span></a></li>
    <li><a href="https://twitter.com/moxyst"><span class="twitter-icon"></span></a></li>
    <li><a href="https://webstagra.ms/moxyst/"><span class="instagram-icon"></span></a></li>
    <li><a href="https://pinterest.com/#"><span class="pinterest-icon"></span></a></li>
    <li><a href="https://plus.google.com/+Moxyst"><span class="gg-icon"></span></a></li>
</ul>
EOD;
/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block');
$block->setTitle('Welcome Mobile Follow Links');
$block->setIdentifier('welcome-mobile-follow-link');
$block->setStores(array($moxyen,$moxyth));
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================