<?php

$moxyen = Mage::getModel('core/store')->load('moxyen', 'code')->getId();// Moxy
$moxyth = Mage::getModel('core/store')->load('moxyth', 'code')->getId();

//==========================================================================
// Welcome global link sites EN
//==========================================================================
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($moxyen, $withAdmin = false)
    ->addFieldToFilter('identifier', 'welcome-main-nav')
    ->getFirstItem();

if ($block->getId()) $block->delete();// if exists then delete
$content = <<<EOD
<ul class="nav">
<li class="active"><a href="{{store url}}">welcome</a></li>
<li class="beauty-nav-item"><a href="http://www.moxy.co.th/en/lafema.html">beauty</a></li>
<li class="fashion-nav-item"><a href="http://www.moxy.co.th/en/fashion.html">fashion</a></li>
<li class="living-nav-item"><a href="http://www.moxy.co.th/en/home-decor.html">home d&eacute;cor</a></li>
<li class="appliances-nav-item"><a href="http://www.moxy.co.th/en/home-appliances.html">home appliances</a></li>
<li class="elec-nav-item"><a href="http://www.moxy.co.th/en/gadgets-electronics.html">electronics</a></li>
<li class="baby-nav-item"><a href="http://www.moxy.co.th/en/venbi.html">mom &amp; baby</a></li>
<li class="heath-nav-item"><a href="http://www.moxy.co.th/en/sanoga.html">Health &amp; Sports</a></li>
<li class="pet-nav-item"><a href="http://www.moxy.co.th/en/petloft.html">pets</a></li>
<!--<li class="active"><a href="http://upcoming.moxy.co.th/magazine/">magazine</a></li>-->
</ul>
EOD;
/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block');
$block->setTitle('Welcome splash navigation ENG');
$block->setIdentifier('welcome-main-nav');
$block->setStores($moxyen);
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================


//==========================================================================
// Welcome global link sites TH
//==========================================================================
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($moxyth, $withAdmin = false)
    ->addFieldToFilter('identifier', 'welcome-main-nav')
    ->getFirstItem();

if ($block->getId()) $block->delete();// if exists then delete
$content = <<<EOD
<ul class="nav">
<li class="active"><a href="{{store url}}">หน้าแรก</a></li>
<li class="beauty-nav-item"><a href="http://www.moxy.co.th/th/lafema.html">ความงาม</a></li>
<li class="fashion-nav-item"><a href="http://www.moxy.co.th/th/fashion.html">แฟชั่น</a></li>
<li class="living-nav-item"><a href="http://www.moxy.co.th/th/home-decor.html">ของแต่งบ้าน</a></li>
<li class="appliances-nav-item"><a href="http://www.moxy.co.th/th/home-appliances.html">เครื่องใช้ไฟฟ้า<br/>ภายในบ้าน</a></li>
<li class="elec-nav-item"><a href="http://www.moxy.co.th/th/gadgets-electronics.html">อิเล็กทรอนิกส์</a></li>
<li class="baby-nav-item"><a href="http://www.moxy.co.th/th/venbi.html">แม่และเด็ก</a></li>
<li class="heath-nav-item"><a href="http://www.moxy.co.th/th/sanoga.html">สุขภาพ &amp; สปอร์ต</a></li>
<li class="pet-nav-item"><a href="http://www.moxy.co.th/th/petloft.html">สัตว์เลี้ยง</a></li>
<!--<li class="active"><a href="http://upcoming.moxy.co.th/magazine/">นิตยสาร</a></li>-->
</ul>
EOD;
/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block');
$block->setTitle('Welcome splash navigation THAI');
$block->setIdentifier('welcome-main-nav');
$block->setStores($moxyth);
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// Welcome splash shortcut link
//==========================================================================
$block = Mage::getModel('cms/block')->getCollection()
    ->addFieldToFilter('identifier', 'welcome-promotion-banner')
    ->getFirstItem();

if ($block->getId()) $block->delete();// if exists then delete
$content = <<<EOD
<div class="banner">
<div class="left"><a href="https://www.moxy.co.th/en/deal/all-deals/bogo.html" target="_self"> <img title="PROMOTION TITLE" src="{{media url="wysiwyg/welcomePage/bogo-december_welcomepage_545x190.jpg"}}" alt="PROMOTION TITLE" /> </a></div>
<div class="right"><a href="https://www.moxy.co.th/th/deal/all-deals/one-price-1212.html" target="_self"> <img title="PROMOTION TITLE" src="{{media url="wysiwyg/welcomePage/OnePricePromotion_welcomepage_545x190.jpg"}}" alt="PROMOTION TITLE" /> </a></div>
</div>
EOD;
/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block');
$block->setTitle('Welcome splash shortcut link');
$block->setIdentifier('welcome-promotion-banner');
$block->setStores(0);
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================