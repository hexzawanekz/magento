<?php
/**
 * Created by PhpStorm.
 * User: SMART
 * Date: 6/12/2015
 * Time: 6:37 PM
 */
class SM_ProductReview_Model_Observer_Ban extends Nexcessnet_Turpentine_Model_Observer_Ban {
    public function banProductReview( $eventObject ) {
        ini_set('memory_limit', '-1');
        $patterns = array();
        $review = $eventObject->getObject();
        $products = $review->getProductCollection()->getItems();
        $productIds = array_unique( array_map(
            create_function( '$p', 'return $p->getEntityId();' ),
            $products ) );
        $patterns[] = sprintf( '/review/product/list/id/(?:%s)/category/',
            implode( '|', array_unique( $productIds ) ) );
        $patterns[] = sprintf( '/review/product/view/id/%d/',
            $review->getEntityId() );
        $productPatterns = array();
        foreach ( $products as $p ) {
            $urlKey = $p->getUrlModel()->formatUrlKey( $p->getName() );
            if ( $urlKey ) {
                $productPatterns[] = $urlKey;
            }
        }
        if ( !empty($productPatterns) ) {
            $productPatterns = array_unique( $productPatterns );
            $patterns[] = sprintf( '(?:%s)', implode( '|', $productPatterns ) );
        }
        $urlPattern = implode( '|', $patterns );

        $result = $this->_getVarnishAdmin()->flushUrl( $urlPattern );
        return $this->_checkResult( $result );
    }
}