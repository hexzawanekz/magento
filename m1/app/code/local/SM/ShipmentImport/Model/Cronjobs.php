<?php
class SM_ShipmentImport_Model_Cronjobs
{
    protected $_netsuiteStatues = array(
        'Rejected By Customer',
        'Delivered',
        'In Transit',
        'Preparing Delivery',
        'Failed To Deliver',
    );

    protected $_allowedStatuses = array(
        'Rejected By Customer',
        'Delivered',
    );

    protected $_offlineMethod = array(
        'cashondelivery',
        'nocost',
    );

    public function schedule()
    {
        ini_set('memory_limit', -1);
        ini_set('max_execution_time', 0);

        /** @var SM_ShipmentImport_Helper_Data $helper */
        $helper = Mage::helper('shipmentimport');
        $sftp = $helper->open();

        if ($sftp instanceof Varien_Io_File || $sftp instanceof Varien_Io_Sftp) {
            $list = $sftp->ls();

            if ($sftp instanceof Varien_Io_Sftp) {
                $archiveDir = Mage::getStoreConfig(SM_ShipmentImport_Helper_Data::XML_PATH_SHIPMENT_IMPORT_SFTP_PATH) . DS . 'Archive';

                foreach ($list as $item) {
                    if (strpos($item['id'], '.csv') == strlen($item['id']) - 4) {
                        $content = $sftp->read($item['id']);
                        $data = $this->_strGetCsv($content);
                        $this->_updateDataFromCsv($data);

                        $sftp->mv($item['id'], $archiveDir. DS . $item['text']);
                    }
                }
            }else{
                $archiveDir = Mage::getStoreConfig(SM_ShipmentImport_Helper_Data::XML_PATH_SHIPMENT_IMPORT_INTERNAL_PATH) . DS . 'Archive';

                foreach ($list as $item) {
                    if (strpos($item['text'], '.csv') == strlen($item['text']) - 4) {
                        $content = $sftp->read($item['text']);
                        $data = $this->_strGetCsv($content);
                        $this->_updateDataFromCsv($data);

                        $sftp->mv($sftp->pwd().DS.$item['text'], $sftp->pwd(). DS .'Archive'.DS. $item['text']);
                        rename($sftp->pwd().DS.$item['text'], $sftp->pwd(). DS .'Archive'.DS. $item['text']);
                    }
                }
            }

            $sftp->close();
        }
    }

    protected function _strGetCsv($string)
    {
        $data = array();
        $rows = str_getcsv($string, "\n");

        foreach ($rows as $row) {
            $data[] = str_getcsv($row);
        }

        return $data;
    }

    protected function _updateDataFromCsv($data)
    {
        array_shift($data);
        foreach ($data as $item) {
            // check status that need to be updated to sales order
            if(in_array($item[1], $this->_allowedStatuses)){
                Mage::log("start " . $item[0], null, 'netsuite_shipment.log');
                $orderId = $item[0];
                /** @var Mage_Sales_Model_Order $order */
                $order = Mage::getModel('sales/order')->loadByIncrementId($orderId);
                if ($order->getId()) {
                    $this->_updateOrderFromItem($order, $item);
                }
                Mage::log("\n" . "end " . $item[0]. "\n ---------- \n", null, 'netsuite_shipment.log');
            }
        }
    }

    /**
     * @param Mage_Sales_Model_Order $order
     * @param array $item
     */
    protected function _updateOrderFromItem(Mage_Sales_Model_Order $order, $item)
    {
        if($item[1] == 'Delivered'){
            // Provider name
            $provider = $item[2];
            // Tracking number
            $trackingNumber = $item[3];
            // Transit date
            $dateShipped = $this->_convertDate($item[4]);

            if($dateShipped){
                $shipment = $this->_getShipment($order, $dateShipped);

                if(!$shipment->getTotalQty()){
                    return;
                }

                if (!$order->hasInvoices()) {
                    $this->_createInvoice($shipment);
                }

                if($provider && $trackingNumber){
                    $this->_updateTrackingNumber($order, $shipment->getIncrementId(), $provider, $trackingNumber);
                }

                $order->save();
            }
        }else{
            $payment = $order->getPayment();

            if ($order->canCancel()) {
                try {
                    $order->cancel();

                    $order->addStatusHistoryComment("Automatically cancelled from NetSuite's shipment import file.");

                    $order->save();
                } catch (Exception $e) {
                    Mage::log('----- '.$e->getMessage(), null, 'netsuite_shipment.log');
                }
            }else{
                if(!in_array($payment->getMethod(), $this->_offlineMethod)){
                    // online SOs
                    try {
                        $order->addStatusHistoryComment("Automatically cancelled from NetSuite's shipment import file.");

                        $order->setStatus(Mage_Sales_Model_Order::STATE_CLOSED);

                        $order->save();
                    } catch (Exception $e) {
                        Mage::log('----- '.$e->getMessage(), null, 'netsuite_shipment.log');
                    }
                }
            }
        }
    }

    /**
     * @param Mage_Sales_Model_Order $order
     * @param $date
     * @return Mage_Sales_Model_Order_Shipment
     */
    protected function _getShipment(Mage_Sales_Model_Order $order, $date)
    {
        /** @var Mage_Sales_Model_Order_Shipment $shipment */
        if (!$order->hasShipments()) {
            $shipment = $order->prepareShipment();
            if ($shipment->getTotalQty()) {
                $shipment->register();
                $shipment->addComment("Automatically updated from NetSuite's shipment import file.", false);
                $shipment->getOrder()->setIsInProcess(true);
                $shipment->setData('created_at', $date);
                $shipment->setData('updated_at', $date);
                $shipment->save();
            }
            return $shipment;
        } else {
            $shipment = $order->getShipmentsCollection()->getFirstItem();
            if($shipment) {
                $shipment->setData('created_at', $date);
                $shipment->setData('updated_at', $date);
                $shipment->save();
                return $shipment;
            }
        }
    }

    /**
     * @param Mage_Sales_Model_Order_Shipment $shipment
     * @throws Exception
     */
    protected function _createInvoice(Mage_Sales_Model_Order_Shipment $shipment)
    {
        $order = $shipment->getOrder();

        if ($order->canInvoice()) {
            /** @var Mage_Sales_Model_Order_Invoice $invoice */
            $invoice = Mage::getModel('sales/service_order', $order)->prepareInvoice();
            $invoice->setRequestedCaptureCase(Mage_Sales_Model_Order_Invoice::CAPTURE_ONLINE);
            $invoice->register();
            $invoice->save();
        }
    }

    protected function _updateTrackingNumber(Mage_Sales_Model_Order $order, $shipmentId, $provider, $trackingNumber)
    {
        if (!$order->getTracksCollection()->count()) {
            /** @var Mage_Sales_Model_Order_Shipment_Api $api */
            $api = Mage::getModel('sales/order_shipment_api');
            $api->addTrack($shipmentId, 'custom', $provider, $trackingNumber);
        } else {
            /** @var Mage_Sales_Model_Order_Shipment_Track $track */
            $track = $order->getTracksCollection()->getFirstItem();
            $track->setData('track_number', $trackingNumber);
            $track->setData('number', $trackingNumber);
            $track->setData('title', $provider);
            $track->setData('carrier_code', 'custom');
            $track->save();
        }
    }

    protected function _convertDate($date)
    {
        return preg_replace('@([0-9]+)/([0-9]+)/([0-9]+)\s([0-9]+):([0-9]+):([0-9]+)@', '$3-$2-$1 $4:$5:$6', $date);
    }
}