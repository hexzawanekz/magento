<?php

class SM_CsvMarketingFiles_Model_Observer {
    protected $_categoryPaths = array();

    protected function _drawCategoryPaths() {
        ini_set('memory_limit', -1);
        ini_set('max_execution_time', 0);
        /** @var Mage_Catalog_Model_Category $categories */
        $categories = Mage::getModel('catalog/category')->getCollection()
            ->addAttributeToSelect(array('entity_id', 'path'))
            ->load();

        /** @var Mage_Catalog_Model_Category $_category */
        foreach($categories as $_category) {
            //Split Category Id
            $categoryIds = explode('/', $_category->getPath());
            array_shift($categoryIds);
            $rootCategoryId = array_shift($categoryIds);

            //Get default store id of current category
            /** @var Mage_Core_Model_Resource_Store_Group_Collection $storeId */
            $storeId = Mage::getModel('core/store_group')
                ->getCollection()
                ->addFieldToFilter('root_category_id', $rootCategoryId)
                ->getFirstItem()
                ->getDefaultStoreId();

            //Load current category with default store id
            $categoryId = $_category->getId();
            $this->_categoryPaths[$categoryId] = '';
            $split = '';
            if(count($categoryIds) >= 1) {
                foreach ($categoryIds as $_categoryId) {
                    $curCategoryName = Mage::getModel('catalog/category')->setStoreId($storeId)->load($_categoryId)->getName();
                    $this->_categoryPaths[$categoryId] = $this->_categoryPaths[$categoryId] . $split . $curCategoryName;
                    $split = ' > ';
                }
            }
        }
    }

    /**
     * Move Files to SFTP Folder
     */
    protected function _ftpFile($path)
    {
        try {
            /** @var SM_CsvMarketingFiles_Helper_Data $helper */
            $helper = Mage::helper('sm_csvmarketingfiles');
            $sftp = $helper->open();

            if ($sftp instanceof Varien_Io_File || $sftp instanceof Varien_Io_Sftp) {
                $fileName = '';
                $pos = strrpos($path, DS);
                if ($pos !== false) {
                    $fileName = substr($path, $pos + 1, strlen($path) - ($pos + 1));
                }

                if($fileName){
                    $content = @file_get_contents($path);

                    $fileName = date('Ymd').' - '. $fileName;

                    /** @var Mage_Core_Model_Config $coreConfigModel */
                    $coreConfigModel = Mage::getModel('core/config');
                    $coreConfigModel->saveConfig(SM_CsvMarketingFiles_Helper_Data::XML_PATH_CSV_MARKETING_DATE_EXPORT, date('Ymd'), 'default', 0);

                    //Reinit Configuration
                    Mage::getConfig()->reinit();
                    Mage::app()->reinitStores();

                    $sftp->write($fileName, $content);
                }

                $sftp->close();
            }

            return true;
        } catch (Exception $e) {
            Mage::log($e->getMessage(), null, 'marketingCsvFilesUpload.log');
            return false;
        }
    }

    /**
     * Export Marketing Csv files
     * File Paths : var/export/marketingCsvFiles/MoxystAll.csv
     *              var/export/marketingCsvFiles/Moxy.csv
     *              var/export/marketingCsvFiles/Petloft.csv
     *              var/export/marketingCsvFiles/Lafema.csv
     *              var/export/marketingCsvFiles/Sanoga.csv
     *              var/export/marketingCsvFiles/Venbi.csv
     */
    public function exportCsv() {
        ini_set('memory_limit', -1);
        ini_set('max_execution_time', 0);

        // check if module is enabled
        if(Mage::getStoreConfig(SM_CsvMarketingFiles_Helper_Data::XML_PATH_CSV_MARKETING_ACTIVE)){
            $this->_drawCategoryPaths();

            /** @var Mage_Core_Model_Resource_Store_Group_Collection $groupStore */
            $groupStore = Mage::app()->getGroups();

            //Make dir to save CSV files
            //var/export/marketingCsvFiles/
            $folder = 'var' . DS . 'export' . DS . 'marketingCsvFiles' . DS;
            mkdir($folder, 0777, true);

            $fpAll = fopen($folder . 'MoxystAll.csv', 'w+');


            //Write header of CSV
            fwrite($fpAll, "item,c_title_th,title,link,c_link_th,image,price,category,available,c_site\n");

            /** @var Mage_Core_Model_Store_Group $_group */
            foreach ($groupStore as $_group) {
                Mage::log('Store Id ' . $_group->getDefaultStoreId(), null, 'marketingCsvFiles.log');

                //Get root category of current Store
                $rootCategoryId = $_group->getRootCategoryId();
                //Get default store id
                $storeId = $_group->getDefaultStoreId();
                //Get Group name to create file CSV
                $groupStoreName = $_group->getName();

                //Get All Language
                if ($groupStoreName == 'Petloft') {
                    $storeIdEn = Mage::getModel('core/store')->load('en', 'code')->getId();
                    $storeIdTh = Mage::getModel('core/store')->load('th', 'code')->getId();
                } else {
                    if ($groupStoreName == 'Venbi') {
                        $storeIdEn = Mage::getModel('core/store')->load('ven', 'code')->getId();
                        $storeIdTh = Mage::getModel('core/store')->load('vth', 'code')->getId();
                    } else {
                        if ($groupStoreName == 'Sanoga') {
                            $storeIdEn = Mage::getModel('core/store')->load('sen', 'code')->getId();
                            $storeIdTh = Mage::getModel('core/store')->load('sth', 'code')->getId();
                        } else {
                            if ($groupStoreName == 'Lafema') {
                                $storeIdEn = Mage::getModel('core/store')->load('len', 'code')->getId();
                                $storeIdTh = Mage::getModel('core/store')->load('lth', 'code')->getId();
                            } else {
                                if ($groupStoreName == 'Moxy') {
                                    $storeIdEn = Mage::getModel('core/store')->load('len', 'code')->getId();
                                    $storeIdTh = Mage::getModel('core/store')->load('lth', 'code')->getId();
                                } else {
                                    $storeIdEn = Mage::getModel('core/store')->load('en', 'code')->getId();
                                    $storeIdTh = Mage::getModel('core/store')->load('th', 'code')->getId();
                                }
                            }
                        }
                    }
                }

                //Get path of root category
                $rootpath = Mage::getModel('catalog/category')
                    ->setStoreId($storeId)
                    ->load($rootCategoryId)
                    ->getPath();

                //Get all child categories of current root category
                $categories = Mage::getModel('catalog/category')->setStoreId($storeId)
                    ->getCollection()
                    ->addAttributeToSelect('*')
                    ->addAttributeToFilter('path', array("like" => $rootpath . "/" . "%"));

                //Open file to write data
                $fp = fopen($folder . $groupStoreName . '.csv', 'w+');


                //Write header of CSV
                //fwrite($fp, "item, link, title, image, category, available, price, msrp\n");
                fwrite($fp, "item,c_title_th,title,link,c_link_th,image,price,category,available,c_site\n");

                //Reset result array
                $result = array();

                foreach ($categories as $_category) {
                    /** @var Mage_Catalog_Model_Category $_category */
                    /** @var Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Collection $products */

                    //Will error if Products of these category are shown in 2+ sites.
                    if ($_category->getId() == Mage::getStoreConfig('welcome_splash/product_list/what_hot_category_id') || $_category == Mage::getStoreConfig('welcome_splash/product_list/new_arrival_category_id')) {
                        continue;
                    }

                    Mage::log('Exporting category : ' . $_category->getName(), null, 'marketingCsvFiles.log');

                    $products = Mage::getModel('catalog/product')
                        ->getCollection()
                        ->joinField('category_id', 'catalog/category_product', 'category_id', 'product_id = entity_id', null, 'left')
                        ->addAttributeToSelect('*')
                        ->addAttributeToFilter('category_id', array('in' => array($_category->getId())));

                    /** @var Mage_Catalog_Model_Product $_product */
                    foreach ($products as $_product) {
                        if ($_product->getVisibility() == 1) {
                            continue;
                        }
                        Mage::log('Exporting Item : ' . $_product->getName(), null, 'marketingCsvFiles.log');

                        $productId = $_product->getId();
                        $_productTH = Mage::getModel('catalog/product')->setStoreId($storeIdTh)->load($productId);
                        $_productEN = Mage::getModel('catalog/product')->setStoreId($storeIdEn)->load($productId);
                        $productSku = $_product->getSku();
                        $productUrlEn = $_productEN->getProductUrl();
                        $productUrlTh = $_productTH->getProductUrl();
                        $productTitleTh = str_replace('"', '""', $_productTH->getName());
                        $productTitleEn = str_replace('"', '""', $_productEN->getName());
                        if ($_product->getImage() == 'no_selection') {
                            $productImg = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . 'catalog/product/placeholder/' . Mage::getStoreConfig("catalog/placeholder/thumbnail_placeholder");
                        } else {
                            $productImg = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . 'catalog/product' . $_product->getImage();;
                        }
                        //$productImg = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . 'catalog/product' . $_product->getImage();;
                        $categoryPath = $this->_categoryPaths[$_category->getId()];
                        $productAvailable = ($_product->getStatus() == 1) ? 'TRUE' : 'FALSE';
                        $productPrice = $_product->getPrice();
                        //$productMsrp = $_product->getMsrp();

                        if (!isset($result[$_product->getId()])) {
                            if ($productSku != '')
                                $result[$_product->getId()] = array($productId, $productTitleTh, $productTitleEn, $productUrlEn, $productUrlTh, $productImg, $productPrice, $categoryPath, $productAvailable, $groupStoreName);
                        } else {
                            $result[$_product->getId()][7] = $result[$_product->getId()][7] . '|' . $categoryPath;
                        }
                    }
                }
                foreach ($result as $_result) {
                    Mage::log('Writing ' . $_product->getName() . ' to CSV', null, 'marketingCsvFiles.log');
                    $_result = sprintf(
                        "\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\"\n",
                        $_result[0], $_result[1], $_result[2], $_result[3], $_result[4], $_result[5], $_result[6], $_result[7], $_result[8], $_result[9]
                    );
                    fwrite($fp, $_result);
                    fwrite($fpAll, $_result);
                }
                fclose($fp);
                $this->_ftpFile($folder . $groupStoreName . '.csv');
            }

            fclose($fpAll);
            $this->_ftpFile($folder . 'MoxystAll.csv');
        }
    }
}