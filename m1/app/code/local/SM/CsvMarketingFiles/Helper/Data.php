<?php

class SM_CsvMarketingFiles_Helper_Data extends Mage_Core_Helper_Abstract
{
    const XML_PATH_CSV_MARKETING_ACTIVE        = 'csvmarketingfiles/general/active';

    const XML_PATH_CSV_MARKETING_CONNECT_TYPE  = 'csvmarketingfiles/general/type';

    const XML_PATH_CSV_MARKETING_INTERNAL_PATH = 'csvmarketingfiles/general/internal_path';

    const XML_PATH_CSV_MARKETING_SFTP_HOST     = 'csvmarketingfiles/general/host';
    const XML_PATH_CSV_MARKETING_SFTP_USERNAME = 'csvmarketingfiles/general/user';
    const XML_PATH_CSV_MARKETING_SFTP_PASSWORD = 'csvmarketingfiles/general/password';
    const XML_PATH_CSV_MARKETING_SFTP_PATH     = 'csvmarketingfiles/general/path';

    const XML_PATH_CSV_MARKETING_DATE_EXPORT   = 'csvmarketingfiles/exported_files/date_export';

    /**
     * Open connection to directory folder
     *
     * @return bool|Varien_Io_File|Varien_Io_Sftp
     * @throws Exception
     */
    public function open()
    {
        try{
            // check if module is enabled
            if(Mage::getStoreConfig(self::XML_PATH_CSV_MARKETING_ACTIVE)){

                // check if connect type is via SFTP folder
                if(Mage::getStoreConfig(self::XML_PATH_CSV_MARKETING_CONNECT_TYPE)){
                    // get info to access
                    $host       = Mage::getStoreConfig(self::XML_PATH_CSV_MARKETING_SFTP_HOST);
                    $username   = Mage::getStoreConfig(self::XML_PATH_CSV_MARKETING_SFTP_USERNAME);
                    $password   = Mage::getStoreConfig(self::XML_PATH_CSV_MARKETING_SFTP_PASSWORD);
                    $path       = Mage::getStoreConfig(self::XML_PATH_CSV_MARKETING_SFTP_PATH);

                    if($host && $username && $password && $path){
                        // open accessing
                        $sftp = new Varien_Io_Sftp();
                        $sftp->open(
                            array(
                                'host'      => $host,
                                'username'  => $username,
                                'password'  => $password,
                            )
                        );

                        $cd = $path;

                        // check dir exists
                        if (!is_dir($cd)){
                            mkdir($cd, 0777, true);
                        }

                        // go to folder
                        if (!empty($cd)){
                            $sftp->cd($cd);
                        }

                        return $sftp;
                    }
                }else{
                    // check if connect type is internal folder

                    if($directoryPath = Mage::getStoreConfig(self::XML_PATH_CSV_MARKETING_INTERNAL_PATH)){

                        // check dir exists
                        if (!is_dir($directoryPath)){
                            mkdir($directoryPath, 0777, true);
                        }

                        $file = new Varien_Io_File();
                        if($file->open(array('path' => $directoryPath))){
                            return $file;
                        }
                    }
                }
            }
            return false;
        }catch(Exception $e){
            Mage::log($e, null, 'csv_marketing_connect.log');
            throw $e;
        }
    }
}