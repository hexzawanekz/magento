<?php

/** @var Mage_Eav_Model_Entity_Setup $installer */
$installer = $this;

$installer->startSetup();

$installer->addAttribute(
    Mage_Catalog_Model_Product::ENTITY,
    'sold_quantity',
    array(
        'label'                      => Mage::helper('catalog')->__('Sold Quantity'),
        'group'                      => 'General',
        'type'                       => 'int',
        'input'                      => 'hidden',
        'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'user_defined'               => false,
        'required'                   => false,
        'visible'                    => false,
        'source'                     => 'eav/entity_attribute_source_table',
        'backend'                    => null,
        'searchable'                 => false,
        'visible_in_advanced_search' => false,
        'visible_on_front'           => false,
        'is_configurable'            => false,
        'is_html_allowed_on_front'   => false,
        'filterable'                 => false,
        'filterable_in_search'       => false,
        'sort_order'                 => '50',
    )
);

$installer->endSetup();