<?php



$installer = $this;

$installer->startSetup();

$attrGroupName = 'Revenue';
if ($installer->getAttribute('catalog_product', 'revenue_last_month')) {
    $installer->removeAttribute('catalog_product', 'revenue_last_month');
}
if ($installer->getAttribute('catalog_product', 'sold_quantity')) {
    $installer->removeAttribute('catalog_product', 'sold_quantity');
}


$installer->addAttribute(Mage_Catalog_Model_Product::ENTITY,
    'revenue_last_month',array(
        'label' => 'Best Seller',
        'type' => 'int',
        'input' => 'hidden',
        'backend' => '',
        'used_in_product_listing' => 1,
        'used_for_sort_by' => 1,
        'visible' => true,
        'required' => false,
        'user_defined' => true,
        'wysiwyg_enabled' => false,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,

    )
);
$attributeId = $installer->getAttributeId('catalog_product', 'revenue_last_month');
$allAttributeSetIds = $installer->getAllAttributeSetIds('catalog_product');

foreach ($allAttributeSetIds as $attributeSetId) {

    $attributeGroup = $installer->getAttributeGroup('catalog_product', $attributeSetId, $attrGroupName);

    if (!$attributeGroup) {
        $installer->addAttributeGroup('catalog_product', $attributeSetId, $attrGroupName, 1000);
    }

    $installer->addAttributeToSet('catalog_product', $attributeSetId, $attrGroupName, $attributeId);
}
$coreConfigModel = Mage::getModel('core/config');
$coreConfigModel->saveConfig('catalog/frontend/default_sort_by', 'revenue_last_month');
$attrData = array(
    'revenue_last_month'=> 0,
);
$storeId = 0;
$productIds = Mage::getModel('catalog/product')->getCollection()->getAllIds();
Mage::getModel("catalog/product_action")->updateAttributes(
    $productIds,
    $attrData,
    $storeId
);
$attributeModel = Mage::getModel('eav/entity_attribute')->loadByCode(Mage_Catalog_Model_Product::ENTITY, 'revenue_last_month');
$write = Mage::getSingleton('core/resource')->getConnection('core_write');
$month_ini = new DateTime("first day of last month");
$query = " UPDATE  catalog_product_entity_int x LEFT JOIN
                        (
                            SELECT
                                                    product_id,
                                                    SUM(qty_ordered) AS `total_sold`,
                                                    product_price,
                                                (SUM(qty_ordered)*product_price) as `revenue`
                                                FROM
                                                    sales_bestsellers_aggregated_monthly
                                                WHERE
                                                    `period` = '".$month_ini->format('Y-m-d')."'
                                                GROUP BY
                                                    `product_id`
                                                ORDER BY
                                                    product_id
                        ) as y ON x.entity_id = y.product_id
                SET     x.`value` = y.revenue
                WHERE attribute_id = '".$attributeModel->getId()."' AND x.entity_id = y.product_id";
$write->query($query);

$coreConfigModel = Mage::getModel('core/config');

$coreConfigModel->saveConfig('catalog/frontend/default_sort_by', 'revenue_last_month');

// Re-calculate total revenue

$installer = $this;

$installer->startSetup();

$attrGroupName = 'Revenue';
if ($installer->getAttribute('catalog_product', 'revenue')) {
    $installer->removeAttribute('catalog_product', 'revenue');
}
if ($installer->getAttribute('catalog_product', 'sold_quantity')) {
    $installer->removeAttribute('catalog_product', 'sold_quantity');
}


$installer->addAttribute(Mage_Catalog_Model_Product::ENTITY,
    'revenue',array(
        'label' => 'Best Seller',
        'type' => 'int',
        'input' => 'hidden',
        'backend' => '',
        'used_in_product_listing' => 1,
        'used_for_sort_by' => 1,
        'visible' => true,
        'required' => false,
        'user_defined' => true,
        'wysiwyg_enabled' => false,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,

    )
);
$attributeId = $installer->getAttributeId('catalog_product', 'revenue');
$allAttributeSetIds = $installer->getAllAttributeSetIds('catalog_product');

foreach ($allAttributeSetIds as $attributeSetId) {

    $attributeGroup = $installer->getAttributeGroup('catalog_product', $attributeSetId, $attrGroupName);

    if (!$attributeGroup) {
        $installer->addAttributeGroup('catalog_product', $attributeSetId, $attrGroupName, 1000);
    }

    $installer->addAttributeToSet('catalog_product', $attributeSetId, $attrGroupName, $attributeId);
}


$attrData = array(
    'revenue' => 0,
);
$storeId = 0;
$productIds = Mage::getModel('catalog/product')->getCollection()->getAllIds();
Mage::getModel("catalog/product_action")->updateAttributes(
    $productIds,
    $attrData,
    $storeId
);
$attributeModel = Mage::getModel('eav/entity_attribute')->loadByCode(Mage_Catalog_Model_Product::ENTITY, 'revenue');
$query2 = " UPDATE  catalog_product_entity_int x LEFT JOIN
                        (
                            SELECT
                                                    product_id,
                                                    SUM(qty_ordered) AS `total_sold`,
                                                    product_price,
                                                (SUM(qty_ordered)*product_price) as `revenue`
                                                FROM
                                                    sales_bestsellers_aggregated_yearly
                                                GROUP BY
                                                    `product_id`
                                                ORDER BY
                                                    product_id
                        ) as y ON x.entity_id = y.product_id
                SET     x.`value` = y.revenue
                WHERE attribute_id = '".$attributeModel->getId()."' AND x.entity_id = y.product_id";
$write->query($query2);