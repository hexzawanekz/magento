<?php

$installer = $this;

$installer->startSetup();

$attrGroupName = 'Revenue';
if ($installer->getAttribute('catalog_product', 'revenue')) {
    $installer->removeAttribute('catalog_product', 'revenue');
}
if ($installer->getAttribute('catalog_product', 'sold_quantity')) {
    $installer->removeAttribute('catalog_product', 'sold_quantity');
}


$installer->addAttribute(Mage_Catalog_Model_Product::ENTITY,
    'revenue',array(
        'label' => 'Best Seller',
        'type' => 'int',
        'input' => 'hidden',
        'backend' => '',
        'used_in_product_listing' => 1,
        'used_for_sort_by' => 1,
        'visible' => true,
        'required' => false,
        'user_defined' => true,
        'wysiwyg_enabled' => false,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,

    )
);
$attributeId = $installer->getAttributeId('catalog_product', 'revenue');
$allAttributeSetIds = $installer->getAllAttributeSetIds('catalog_product');

foreach ($allAttributeSetIds as $attributeSetId) {

    $attributeGroup = $installer->getAttributeGroup('catalog_product', $attributeSetId, $attrGroupName);

    if (!$attributeGroup) {
        $installer->addAttributeGroup('catalog_product', $attributeSetId, $attrGroupName, 1000);
    }

    $installer->addAttributeToSet('catalog_product', $attributeSetId, $attrGroupName, $attributeId);
}

$coreConfigModel = Mage::getModel('core/config');

$coreConfigModel->saveConfig('catalog/frontend/default_sort_by', 'revenue');