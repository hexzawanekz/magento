<?php

class SM_BestSellerSort_Model_Cronjobs
{

    public function resetMonthlyRevenue()
    {
        $attrData = array(
            'revenue_last_month' => 0,
        );
        $storeId = 0;
        $productIds = Mage::getModel('catalog/product')->getCollection()->getAllIds();
        Mage::getModel("catalog/product_action")->updateAttributes(
            $productIds,
            $attrData,
            $storeId
        );
        $attributeModel = Mage::getModel('eav/entity_attribute')->loadByCode(Mage_Catalog_Model_Product::ENTITY, 'revenue_last_month');
        $write = Mage::getSingleton('core/resource')->getConnection('core_write');
        $month_ini = new DateTime("first day of last month");
        $query = " UPDATE  catalog_product_entity_int x LEFT JOIN
                        (
                            SELECT
                                                    product_id,
                                                    SUM(qty_ordered) AS `total_sold`,
                                                    product_price,
                                                (SUM(qty_ordered)*product_price) as `revenue`
                                                FROM
                                                    sales_bestsellers_aggregated_monthly
                                                WHERE
                                                    `period` = '" . $month_ini->format('Y-m-d') . "'
                                                GROUP BY
                                                    `product_id`
                                                ORDER BY
                                                    product_id
                        ) as y ON x.entity_id = y.product_id
                SET     x.`value` = y.revenue
                WHERE attribute_id = '" . $attributeModel->getId() . "' AND x.entity_id = y.product_id";
        $write->query($query);
    }

    public function resetRecommendationProducts()
    {
        $write = Mage::getSingleton('core/resource')->getConnection('core_write');
        $query = <<<EOD
        truncate table recommendation_products_flat;
INSERT INTO recommendation_products_flat (baseProduct,combinationProduct,weighted_sum_of_qtyOrdered,weighted_sum_of_revenue,weighted_sum_of_absolute_margin,baseCategoryID,combinationCategoryID,baseBrandID,combinationBrandID,weighted_metric,filter)
select byCustomerSub.baseProduct, byCustomerSub.combinationProduct, SUM(byCustomerSub.weighted_qty_ordered) AS weighted_sum_of_qtyOrdered, SUM(byCustomerSub.weighted_row_total) AS weighted_sum_of_revenue, SUM(byCustomerSub.weighted_absolute_margin) AS weighted_sum_of_absolute_margin, categorySubBase.categoryID AS baseCategoryID, categorySubCombination.categoryID AS combinationCategoryID, cpei.value AS baseBrandID, cpei2.value AS combinationBrandID,rpq.weight*SUM(byCustomerSub.weighted_qty_ordered)+rpr.weight*SUM(byCustomerSub.weighted_row_total)+rpm.weight*SUM(byCustomerSub.weighted_absolute_margin) AS weighted_metric,IF(IF(reb.brand_id IS NOT NULL AND cpei.value != cpei2.value, 0, 1) = 1 AND IF(categorySubBase.categoryID = categorySubCombination.categoryID,1,0) = 1, 1, 0) AS filter
FROM

(
select sfoi.product_id AS baseProduct, combinationSkuSub.product_id AS combinationProduct, sfo.customer_id, IF(sfo.entity_id = combinationSkuSub.order_id, 3, 1)*combinationSkuSub.qty_ordered AS weighted_qty_ordered, IF(sfo.entity_id = combinationSkuSub.order_id, 3, 1)*combinationSkuSub.row_total AS weighted_row_total, IF(sfo.entity_id = combinationSkuSub.order_id, 3, 1)*IFNULL(combinationSkuSub.row_total-combinationSkuSub.qty_ordered*combinationSkuSub.base_cost,0) AS weighted_absolute_margin from sales_flat_order_item sfoi
left join sales_flat_order sfo ON sfoi.order_id = sfo.entity_id
LEFT JOIN sales_flat_order_payment sfop ON sfoi.order_id = sfop.parent_id
inner join

(
SELECT sfoi.order_id, cpe.entity_id AS "product_id", sfo.customer_id, sfoi.qty_ordered, sfoi.row_total, sfoi.base_cost from sales_flat_order_item sfoi
left join sales_flat_order sfo ON sfoi.order_id = sfo.entity_id
LEFT JOIN sales_flat_order_payment sfop ON sfoi.order_id = sfop.parent_id
left join catalog_product_entity cpe ON sfoi.sku = cpe.sku

left join
(
select sfoi.sku, sfoi.order_id, COUNT(DISTINCT sfoi.product_type) AS c from sales_flat_order_item sfoi
where DATE(ADDDATE(sfoi.created_at, INTERVAL 7 HOUR)) >= ADDDATE(DATE(ADDDATE(NOW(), INTERVAL 7 HOUR)), INTERVAL -90 DAY)
group by sfoi.sku, sfoi.order_id
) configurableSub ON sfoi.sku = configurableSub.sku AND sfoi.order_id = configurableSub.order_id

where configurableSub.c = 1 OR (configurableSub.c = 2 AND sfoi.product_type = "configurable")

AND (sfo.state IN (
                    'complete',
                    'invoiced',
                    'shipped',
                    'processing'
                ) OR (sfo.state = 'new' AND sfop.method NOT IN ('paypal_standard', 'cc2p', 'bbgateway', 'AEON', 'normal2c2p')))

AND DATE(ADDDATE(sfo.created_at, INTERVAL 7 HOUR)) >= DATE(ADDDATE(ADDDATE(now(), INTERVAL 7 HOUR), INTERVAL -90 DAY))
AND cpe.entity_id IS NOT NULL
) combinationSkuSub ON sfo.customer_id = combinationSkuSub.customer_id AND sfoi.product_id != combinationSkuSub.product_id

left join
(
select sfoi.sku, sfoi.order_id, COUNT(DISTINCT sfoi.product_type) AS c from sales_flat_order_item sfoi
where DATE(ADDDATE(sfoi.created_at, INTERVAL 7 HOUR)) >= ADDDATE(DATE(ADDDATE(NOW(), INTERVAL 7 HOUR)), INTERVAL -90 DAY)
group by sfoi.sku, sfoi.order_id
) configurableSub ON sfoi.sku = configurableSub.sku AND sfoi.order_id = configurableSub.order_id

where configurableSub.c = 1 OR (configurableSub.c = 2 AND sfoi.product_type = "configurable")

AND (sfo.state IN (
                    'complete',
                    'invoiced',
                    'shipped',
                    'processing'
                ) OR (sfo.state = 'new' AND sfop.method NOT IN ('paypal_standard', 'cc2p', 'bbgateway', 'AEON', 'normal2c2p')))

AND DATE(ADDDATE(sfo.created_at, INTERVAL 7 HOUR)) >= DATE(ADDDATE(ADDDATE(now(), INTERVAL 7 HOUR), INTERVAL -90 DAY))
) byCustomerSub

left join

(select cpe.entity_id, ccp.category_id AS categoryID from catalog_product_entity cpe
left join catalog_category_product ccp ON  cpe.entity_id = ccp.product_id
left join catalog_category_entity cce ON ccp.category_id = cce.entity_id
left join catalog_category_entity_varchar ccev ON ccp.category_id = ccev.entity_id AND ccev.store_id = 0 and ccev.attribute_id = 111
where cce.level = 2 AND ccev.value NOT IN ("Deal", "Welcome Splash", "Sanook Super Deal")
group by cpe.entity_id) categorySubBase ON baseProduct = categorySubBase.entity_id

left join

(select cpe.entity_id, ccp.category_id AS categoryID from catalog_product_entity cpe
left join catalog_category_product ccp ON  cpe.entity_id = ccp.product_id
left join catalog_category_entity cce ON ccp.category_id = cce.entity_id
left join catalog_category_entity_varchar ccev ON ccp.category_id = ccev.entity_id AND ccev.store_id = 0 and ccev.attribute_id = 111
where cce.level = 2 AND ccev.value NOT IN ("Deal", "Welcome Splash", "Sanook Super Deal")
group by cpe.entity_id) categorySubCombination ON combinationProduct = categorySubCombination.entity_id

left join catalog_product_entity_int cpei ON baseProduct = cpei.entity_id AND cpei.attribute_id = 1281  AND cpei.store_id = 0
left join catalog_product_entity_int cpei2 ON combinationProduct = cpei2.entity_id AND cpei2.attribute_id = 1281  AND cpei2.store_id = 0

left join recommendation_weights rpq ON rpq.type = 'qty'
left join recommendation_weights rpr ON rpr.type = 'revenue'
left join recommendation_weights rpm ON rpm.type = 'margin'
left join recommendation_excluded_brands reb ON cpei.value = reb.brand_id

GROUP BY byCustomerSub.baseProduct, byCustomerSub.combinationProduct
ORDER BY byCustomerSub.baseProduct, weighted_metric desc;
EOD;
        $write->query($query);

    }

}