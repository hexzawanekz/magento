<?php

/** @var Mage_Core_Model_Resource_Setup $installer */
$installer = $this;

$installer->startSetup();

/** @var Mage_Core_Model_Config $coreConfigModel */
$coreConfigModel = Mage::getModel('core/config');

$coreConfigModel->saveConfig('catalog/frontend/grid_per_page', 40);

$coreConfigModel->saveConfig('catalog/frontend/grid_per_page_values', '20,40,60,80');

$coreConfigModel->saveConfig('catalog/frontend/list_per_page_values', '20,30,40,50,100');

$coreConfigModel->saveConfig('catalog/frontend/list_per_page', 40);

$coreConfigModel->saveConfig('catalog/frontend/default_sort_by', 'best_seller');

$installer->endSetup();