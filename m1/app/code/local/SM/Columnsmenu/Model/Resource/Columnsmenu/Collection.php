<?php

class SM_Columnsmenu_Model_Resource_Columnsmenu_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    public function _construct()
    {
        $this->_init('columnsmenu/columnsmenu');
    }
}