<?php

class SM_Columnsmenu_Block_Adminhtml_Columnsmenu_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /*
     * Method Prepare Form Add field
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form(array(
            'id' => 'edit_form',
            'action' => $this->getUrl('*/*/save', array(
                        '_current' => true,
                        'continue' => 0,
                    )
                ),
            'method' => 'post',
        ));
        $form->setUseContainer(true);
        $this->setForm($form);
        $fieldset = $form->addFieldset(
            'general',
            array(
                'legend' => $this->__('Information')
            )
        );
        $this->_addFieldsToFieldset($fieldset, array(
            'columnsmenu_category_id' => array(
                'name'      => 'Categoryid',
                'label'     => $this->__('Category ID'),
                'required'  => true,
                'input' => 'text',
                'class'     => 'validate-number',
            ),
            'columnsmenu_column' => array(
                'name'  => 'column',
                'label' => $this->__('Position Column'),
                'input' => 'select',
                'options' => array(
                    1 => '1',
                    2 => '2',
                    3 => '3',
                    4 => '4',
                    5 => '5',
                    6 => '6',
                ),
            ),
        ));

        return $this;
    }

    /**
     * @param Varien_Data_Form_Element_Fieldset $fieldset
     * @param $fields
     * @return $this
     *
     */
    protected function _addFieldsToFieldset(Varien_Data_Form_Element_Fieldset $fieldset, $fields)
    {
        $requestData = new Varien_Object($this->getRequest()->getPost('columnsmenuData'));

        foreach ($fields as $name => $_data) {
            if ($requestValue = $requestData->getData($name)) {
                $_data['value'] = $requestValue;
            }
            $_data['name'] = "columnsmenuData[$name]";
            $_data['title'] = $_data['label'];
            if (!array_key_exists('value', $_data)) {
                $_data['value'] = $this->_getAddress()->getData($name);
            }
            $fieldset->addField($name, $_data['input'], $_data);
        }
        return $this;
    }

    /**
     * @return columnsmenu
     */
    protected function _getAddress()
    {
        if (!$this->hasData('columnsmenu')) {
            // This will have been set in the controller.
            $address = Mage::registry('columnsmenu_data');
            // Just in case the controller does not register the address
            if (!$address instanceof  SM_Columnsmenu_Model_Columnsmenu) {
                $address = Mage::getModel('columnsmenu/columnsmenu');
            }
            $this->setData('columnsmenu', $address);
        }
        return $this->getData('columnsmenu');
    }
}