<?php

class SM_Columnsmenu_Block_Adminhtml_Columnsmenu_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();

        $this->_objectId = 'id';
        $this->_blockGroup = 'columnsmenu';
        $this->_controller = 'adminhtml_columnsmenu';
        $this->_mode = 'edit';
        $newOrEdit = $this->getRequest()->getParam('id') ? $this->__('Edit') : $this->__('New');
        $this->_headerText = $newOrEdit . ' ' . $this->__('Menu Column');
        $this->_updateButton('delete', 'label', Mage::helper('columnsmenu')->__('Delete Item'));
    }
}