<?php

class SM_Columnsmenu_Helper_Data extends Mage_Core_Helper_Abstract
{
    /*get collection with where if insert*/
    public function getCollectionAdd($where = '') {

        $collection = Mage::getModel('columnsmenu/columnsmenu')->getCollection();
        if($where != null && is_array($where)) {
            foreach($where as $key => $value) {
                $collection->addFieldToFilter($key, array($value['condition'] => $value['value']));
            }
        }
        return $collection;
    }

    /*get collection to array*/
    public function getCollectionArray() {
        $collection = Mage::getModel('columnsmenu/columnsmenu')->getCollection()->getData();
        $arr = array();
        foreach($collection as $item) {
            $arr[$item['columnsmenu_category_id']] = $item['columnsmenu_column'];
        }
        return $arr;
    }
}