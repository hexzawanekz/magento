<?php

//create table
$installer = $this;
$installer->startSetup();
$installer->run("
                DROP TABLE IF EXISTS {$this->getTable('columns_menu')};
                CREATE TABLE {$this->getTable('columns_menu')} (
                  `columnsmenu_id` INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
                  `columnsmenu_category_id` integer,
                  `columnsmenu_category_name` VARCHAR(1000),
                  `columnsmenu_category_store_name` VARCHAR(1000),
                  `columnsmenu_column` integer,
                  PRIMARY KEY (`columnsmenu_id`)
                )
                ENGINE = InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
                ");

$installer->endSetup(); 