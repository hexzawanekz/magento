<?php

class SM_Columnsmenu_Adminhtml_ColumnsmenuController extends Mage_Adminhtml_Controller_Action
{
    private $_model;
    public function _construct()
    {
        parent::_construct();
        $this->_model = Mage::getModel('columnsmenu/columnsmenu');

    }

    public function indexAction()
    {
        $this->loadLayout();
        $this->_setActiveMenu('columnsmenu');
        $this->_addContent($this->getLayout()->createBlock('columnsmenu/adminhtml_columnsmenu'));
        $this->renderLayout();
    }

    /**
     * sort grid item
     */
    public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('columnsmenu/adminhtml_columnsmenu_grid')->toHtml()
        );
    }

    /*delete item*/
    public function deleteAction()
    {
        $id = $this->getRequest()->getParam('id');
        try{
            $this->_model->load($id)->delete();
            Mage::getSingleton('adminhtml/session')->addSuccess(
                Mage::helper('columnsmenu')->__('Item was successfully delete')
            );
        }
        catch(Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        }
        $this->_redirect('*/*/index');
    }

    /*mass delete item*/
    public function massDeleteAction()
    {
        $Ids = $this->getRequest()->getParam('massid');
        if(!is_array($Ids)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('columnsmenu')->__('Please select item !'));
        } else {
            try {
                foreach ($Ids as $Id) {
                    $this->_model->load($Id)->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('columnsmenu')->__(
                        'Total of %d record(s) were deleted.', count($Ids)
                    )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    /*Function perform forward to action edit on controller item.*/
    public function newAction()
    {
        $this->_forward('edit');
    }

    /*edit item*/
    public function editAction()
    {
        $addId = $this->getRequest()->getParam('id');
        $model = $this->_model->load($addId);
        if ($model->getId() || $addId == 0) {
            $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
            if (!empty($data)) {
                $model->setData($data);
            }
            Mage::register('columnsmenu_data', $model);
            $this->loadLayout();
            $this->_addBreadcrumb(
                Mage::helper('adminhtml')->__('Columns Menu'),
                Mage::helper('adminhtml')->__('Columns Menu Manager')
            );
            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
            $this->_addContent($this->getLayout()->createBlock('columnsmenu/adminhtml_columnsmenu_edit'));
            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('columnsmenu')->__('Item does not exist !')
            );
            $this->_redirect('*/*/');
        }
    }


    /*Save item*/
    public function saveAction(){

        $addId = $this->getRequest()->getParam('id');
        $data  = $this->getRequest()->getPost();

        if ($data) {
            $categoryInput  = $data['columnsmenuData']['columnsmenu_category_id'];
            $countExist     = 0;

            //check exist category and add name category
            $thisCategory  = Mage::getModel('catalog/category')->load($categoryInput);
            if($thisCategory->getId() == null) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('columnsmenu')->__("Category Id '".$categoryInput."' does not exist.")
                );
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            } else {
                $data['columnsmenuData']['columnsmenu_category_name'] = $thisCategory->getName();
            }

            /*get store name root*/
            $categoryIds = $thisCategory->getPathIds();
            if($categoryIds[1] != null) {
                $categoryRootWebsite = Mage::getModel('catalog/category')->load($categoryIds[1])->getName();
                $data['columnsmenuData']['columnsmenu_category_store_name'] = $categoryRootWebsite;
            }
            /*end get store name root*/

            $this->_model->setData($data['columnsmenuData'])
                 ->setId($addId);

            //check data exist
            //if create new
            if ($addId == null) {
                $dataColumnsMenu   = Mage::helper('columnsmenu')->getCollectionAdd()->getData();
            } else {
                //if update
                $condition = array('columnsmenu_id' => array(
                                        'condition' => 'nin',
                                        'value'     => $addId
                                  ));
                $dataColumnsMenu   = Mage::helper('columnsmenu')->getCollectionAdd($condition)->getData();
            }

            if(isset($dataColumnsMenu)) {
                foreach($dataColumnsMenu as $item) {
                    if($item['columnsmenu_category_id'] == $categoryInput) {
                        $countExist++;
                    }
                }
            }

            //if exist then not save and return page insert or edit
            if($countExist > 0) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('columnsmenu')->__("Category Id '".$categoryInput."' already exists.")
                );
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }

            try {
                $this->_model->save();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('columnsmenu')->__('Item was successfully saved.')
                );
                Mage::getSingleton('adminhtml/session')->setFormData(false);

                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $this->_model->getId()));
                    return;
                }
                $this->_redirect('*/*/index');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        } else {
            $this->_redirect('*/*/index');
        }
    }

}