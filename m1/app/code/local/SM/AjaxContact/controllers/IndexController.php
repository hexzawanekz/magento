<?php
require_once(Mage::getModuleDir('controllers', 'Mage_Contacts') . DS . 'IndexController.php');
class SM_AjaxContact_IndexController extends Mage_Contacts_IndexController
{
    public function postAction()
    {
        $post = $this->getRequest()->getPost();
        $json = array();
        if ($post) {
            $translate = Mage::getSingleton('core/translate');
            /* @var $translate Mage_Core_Model_Translate */
            $translate->setTranslateInline(false);
            try {
                $postObject = new Varien_Object();
                $postObject->setData($post);

                $error = false;

                if (!Zend_Validate::is(trim($post['name']) , 'NotEmpty')) {
                    $error = true;
                }

                if (!Zend_Validate::is(trim($post['comment']) , 'NotEmpty')) {
                    $error = true;
                }

                if (!Zend_Validate::is(trim($post['email']), 'EmailAddress')) {
                    $error = true;
                }

                if (Zend_Validate::is(trim($post['hideit']), 'NotEmpty')) {
                    $error = true;
                }

                if ($error) {
                    $json['status'] = false;
                    $json['message'] = Mage::helper('ajaxcontact')->__('Error: Data is invalid.');

                    return $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($json));
                }
                $mailTemplate = Mage::getModel('core/email_template');
                /* @var $mailTemplate Mage_Core_Model_Email_Template */
                $mailTemplate->setDesignConfig(array('area' => 'frontend'))
                    ->setReplyTo($post['email'])
                    ->sendTransactional(
                        Mage::getStoreConfig(self::XML_PATH_EMAIL_TEMPLATE),
                        Mage::getStoreConfig(self::XML_PATH_EMAIL_SENDER),
                        Mage::getStoreConfig(self::XML_PATH_EMAIL_RECIPIENT),
                        null,
                        array('data' => $postObject)
                    );

                if (!$mailTemplate->getSentSuccess()) {
                    $json['status'] = false;
                    $json['message'] = Mage::helper('ajaxcontact')->__('Email cannot be sent.');

                    return $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($json));
                }

                $translate->setTranslateInline(true);

                $json['status'] = true;
                $json['message'] = Mage::helper('ajaxcontact')->__('Thank you for dropping us a message, our CS team will get back to you shortly');
            } catch (Exception $e) {
                $translate->setTranslateInline(true);

                $json['status'] = false;
                $json['message'] = Mage::helper('ajaxcontact')->__('Unable to submit your request. Please, try again later');
            }
        }

        return $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($json));
    }

    public function messageBoxAction(){
        $post = $this->getRequest()->getPost();
        $storeId = Mage::app()->getStore()->getId();
        if (!Zend_Validate::is(trim($post['name']) , 'NotEmpty')) {
            $error = true;
        }

        if (!Zend_Validate::is(trim($post['comment']) , 'NotEmpty')) {
            $error = true;
        }

        if (!Zend_Validate::is(trim($post['email']), 'EmailAddress')) {
            $error = true;
        }

        if (Zend_Validate::is(trim($post['hideit']), 'NotEmpty')) {
            $error = true;
        }

        if ($error) {
            $json['status'] = false;
            $json['message'] = Mage::helper('ajaxcontact')->__('Error: Data is invalid.');

            return $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($json));
        }
        $sender = Mage::getStoreConfig(self::XML_PATH_EMAIL_SENDER);
        $senderName = Mage::getStoreConfig('trans_email/ident_' . $sender . '/name', $storeId);
        $senderEmail = Mage::getStoreConfig('trans_email/ident_' . $sender . '/email', $storeId);
        $to = Mage::getStoreConfig(self::XML_PATH_EMAIL_RECIPIENT);
        $subject = "Orami By Moxi: Customer Comment";
        $message = "Name: ".$post['name'].".\n"."Email: ".$post['email'].".\n"."Comment: ".$post['comment'].".\n";
        if($post['telephone']){
            $message .= "Telephone: ".$post['telephone'].".\n";
        }
        if($post['subject']){
            $message .= "Subject: ".$post['subject'].".\n";
        }
        $headers = "From:" . $senderEmail;

        if (mail($to, $subject, $message, $headers)) {
            $json['status'] = true;
            $json['message'] = Mage::helper('ajaxcontact')->__('Thank you for dropping us a message, our CS team will get back to you shortly');
            return $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($json));
        } else {
            $json['status'] = false;
            $json['message'] = Mage::helper('ajaxcontact')->__('Unable to submit your request. Please, try again later');
            return $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($json));
        }
    }

    public function contactUsAction(){
        $post = $this->getRequest()->getPost();
        $storeId = Mage::app()->getStore()->getId();
        if (!Zend_Validate::is(trim($post['name']) , 'NotEmpty')) {
            $error = true;
        }

        if (!Zend_Validate::is(trim($post['comment']) , 'NotEmpty')) {
            $error = true;
        }

        if (!Zend_Validate::is(trim($post['email']), 'EmailAddress')) {
            $error = true;
        }

        if (Zend_Validate::is(trim($post['hideit']), 'NotEmpty')) {
            $error = true;
        }

        if ($error) {
            $json['status'] = false;
            $json['message'] = Mage::helper('ajaxcontact')->__('Error: Data is invalid.');

            return $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($json));
        }

        $sender = Mage::getStoreConfig(self::XML_PATH_EMAIL_SENDER);
        $senderName = Mage::getStoreConfig('trans_email/ident_' . $sender . '/name', $storeId);
        $senderEmail = Mage::getStoreConfig('trans_email/ident_' . $sender . '/email', $storeId);
        $to = Mage::getStoreConfig(self::XML_PATH_EMAIL_RECIPIENT);
        $subject = "Orami By Moxi: Contact Us";
        $message = "Name: ".$post['name'].".\n"."Email: ".$post['email'].".\n"."Comment: ".$post['comment'].".\n";
        if($post['telephone']){
            $message .= "Telephone: ".$post['telephone'].".\n";
        }

        $headers = "From:" . $senderEmail;

        if (mail($to, $subject, $message, $headers)) {
            Mage::getSingleton('customer/session')->addSuccess(Mage::helper('contacts')->__('Your inquiry was submitted and will be responded to as soon as possible. Thank you for contacting us.'));
            $this->_redirect('contacts/*/');
            return;
        } else {
            Mage::getSingleton('customer/session')->addError(Mage::helper('contacts')->__('Unable to submit your request. Please, try again later'));
            $this->_redirect('contacts/*/');
            return;
        }
    }
}