<?php

try {
    /** @var Mage_Core_Model_Resource_Setup $this */
    $installer = $this;
    $installer->startSetup();

    // Force the store to be admin
    Mage::app()->setUpdateMode(false);
    Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
    if (!Mage::registry('isSecureArea')) {
        Mage::register('isSecureArea', 1);
    }

    /** @var Mage_Core_Model_Config $config */
    $config = Mage::getModel('core/config');

    $moxyen = Mage::app()->getStore('moxyen')->getId();
    $moxyth = Mage::app()->getStore('moxyth')->getId();



    //==========================================================================
    // Page Not Found ENG
    //==========================================================================
    $title = 'Page Not Found';
    $identifier = 'no-route';
    $stores = array($moxyen);
    $status = 1;
    $content = <<<EOD
<div id="page-not-available-web">
    <img src="{{media url="wysiwyg/cmsNoRoute/404_errorpage_moxy_web_EN.jpg"}}" alt="" />
    <a class="to-homepage" href="{{store url=""}}" title="Back to Homepage"></a>
</div>
<a id="page-not-available-mb" href="{{store url=""}}" title="Back to Homepage">
    <img src="{{media url="wysiwyg/cmsNoRoute/404_errorpage_moxy_mobile_EN.jpg"}}" alt="" />
</a>
EOD;
    $rootTemplate = "one_column";

    /** @var Mage_Cms_Model_Page $page */
    $page = Mage::getModel('cms/page')->getCollection()
        ->addStoreFilter($stores, $withAdmin = false)
        ->addFieldToFilter('identifier', $identifier)
        ->getFirstItem();

    if ($page->getId()) $page->delete();
    $page = Mage::getModel('cms/page');
    $page->setTitle($title);
    $page->setIdentifier($identifier);
    $page->setIsActive($status);
    $page->setContent($content);
    $page->setRootTemplate($rootTemplate);
    $page->setStores($stores);
    $page->save();

    $pageId = $identifier . '|' . $page->getId();
    $config->saveConfig(Mage_Cms_Helper_Page::XML_PATH_NO_ROUTE_PAGE, $pageId, 'stores', $moxyen);
    //==========================================================================
    //==========================================================================
    //==========================================================================




    //==========================================================================
    // Page Not Found THAI
    //==========================================================================
    $title = 'Page Not Found';
    $identifier = 'no-route';
    $stores = array($moxyth);
    $status = 1;
    $content = <<<EOD
<div id="page-not-available-web">
    <img src="{{media url="wysiwyg/cmsNoRoute/404_errorpage_moxy_web_TH.jpg"}}" alt="" />
    <a class="to-homepage" href="{{store url=""}}" title="Back to Homepage"></a>
</div>
<a id="page-not-available-mb" href="{{store url=""}}" title="Back to Homepage">
    <img src="{{media url="wysiwyg/cmsNoRoute/404_errorpage_moxy_mobile_TH.jpg"}}" alt="" />
</a>
EOD;
    $rootTemplate = "one_column";

    /** @var Mage_Cms_Model_Page $page */
    $page = Mage::getModel('cms/page')->getCollection()
        ->addStoreFilter($stores, $withAdmin = false)
        ->addFieldToFilter('identifier', $identifier)
        ->getFirstItem();

    if ($page->getId()) $page->delete();
    $page = Mage::getModel('cms/page');
    $page->setTitle($title);
    $page->setIdentifier($identifier);
    $page->setIsActive($status);
    $page->setContent($content);
    $page->setRootTemplate($rootTemplate);
    $page->setStores($stores);
    $page->save();

    $pageId = $identifier . '|' . $page->getId();
    $config->saveConfig(Mage_Cms_Helper_Page::XML_PATH_NO_ROUTE_PAGE, $pageId, 'stores', $moxyth);
    //==========================================================================
    //==========================================================================
    //==========================================================================



    $installer->endSetup();

} catch (Exception $e) {
    throw $e;
}