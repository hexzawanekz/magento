<?php

require_once(Mage::getModuleDir('controllers','Mage_Cms').DS.'IndexController.php');

class SM_NoRoute_IndexController extends Mage_Cms_IndexController
{
    public function noRouteAction($coreRoute = null)
    {
        /*
         * these 404 responses make function not work correctly on nginx server
         */
        //$this->getResponse()->setHeader('HTTP/1.1','404 Not Found');
        //$this->getResponse()->setHeader('Status','404 File not found');

        /** @var Mage_Cms_Model_Page $pageNotFound */
        $pageNotFound = Mage::getModel('cms/page');
        $pageId = Mage::getStoreConfig(Mage_Cms_Helper_Page::XML_PATH_NO_ROUTE_PAGE);
        if (!is_null($pageId) && $pageId!==$pageNotFound->getId()) {
            $delimeterPosition = strrpos($pageId, '|');
            if ($delimeterPosition) {
                $pageId = substr($pageId, 0, $delimeterPosition);
            }

            $pageNotFound->setStoreId(Mage::app()->getStore()->getId());
            $pageNotFound->load($pageId);
        }

        if ($pageNotFound->getId()) {
            $this->_redirect('page-not-available.html');
        }else{
            $this->_forward('defaultNoRoute');
        }
    }

    public function notAvailableAction()
    {
        $this->getResponse()->setHeader('HTTP/1.1','404 Not Found');
        $this->getResponse()->setHeader('Status','404 File not found');

        $pageId = Mage::getStoreConfig(Mage_Cms_Helper_Page::XML_PATH_NO_ROUTE_PAGE);
        /** @var Mage_Cms_Helper_Page $helper */
        $helper = Mage::helper('cms/page');
        $helper->renderPage($this, $pageId);
    }
}