<?php
class SM_CancelEmail_Model_Observer extends Mage_Core_Model_Abstract {

    public function orderCancelAfter(Varien_Event $observer){
        if(Mage::getStoreConfig('cancel_email/general/active') != 0) {
            try {
                /** @var Mage_Sales_Model_Order $order */
                $order = $observer->getEvent()->getOrder();

                //Store
                $store = $order->getStore();
                $storeId = $order->getStoreId();

                //Email info
                $cancelEmailTemplate = Mage::getStoreConfig('cancel_email/email/template', $storeId);
                $customerEmail = $order->getCustomerEmail();
                $sender['name'] = Mage::getStoreConfig('cancel_email/email/sendername', $storeId);
                $sender['email'] = Mage::getStoreConfig('cancel_email/email/senderemail', $storeId);

                $translate = Mage::getSingleton('core/translate');

                $translate->setTranslateInline(false);

                Mage::getModel('core/email_template')
                    ->setDesignConfig(array('area' => 'frontend', 'store' => $storeId))
                    ->sendTransactional(
                        $cancelEmailTemplate,
                        $sender,
                        $customerEmail,
                        '',
                        array(
                            'store' => $store,
                            'order' => $order,

                        ));

                $translate->setTranslateInline(true);
            } catch (Exception $e){
                Mage::log($e->getMessage(), null, 'cancelEmailError.log');
            }
        }
    }
}