<?php


/** @var Mage_Core_Model_Resource_Setup $installer */
$installer = $this;

$installer->startSetup();

/** Order Cancellation Mail Template Venbi */
/** @var Mage_Adminhtml_Model_Email_Template $mailTemplateModel */
$mailTemplateModel = Mage::getModel('adminhtml/email_template');

$mailTemplateModel->loadByCode('Order Cancel Venbi');
$mailTemplateModel->setTemplateCode('Order Cancel Venbi');
$mailTemplateModel->setTemplateSubject('{{var store.getFrontendName()}}: Your order #{{var order.increment_id}} has been canceled');
$mailTemplateModel->setTemplateType(2);

$mailContent = <<<HTML
    <meta charset="utf-8"/>
<div class="mail-container" style="width:600px; padding:0px 33px; font-family:  'Trebuchet MS', Tahoma; font-size: 14px; color: #3c3d41; margin: 0px;">
	<div class="mail-header" style="width:95%; min-height: 110px; max-height: 110px; position: relative; margin:0px auto;">
		<div class="mail-logo" style="width:220px; min-height: 110px; max-height: 110px; margin:0px auto;">
			<a href="http://venbi.com/"><img src="{{skin url="images/email/venbi-logo.png"}}" style="width:220px; max-height: 110px; min-height: 110px;" /></a>
		</div>

		<div class="english-below" style="min-height: 16px; max-height: 16px; position: absolute; bottom: 0px; line-height: 16px;">
			<span><img src="{{skin url="images/email/flag-en.png"}}" height="16px;"/> Scroll down for English version</span>
		</div>
	</div>

	<img style="width: 100%; margin-top:10px;" src="{{skin _area="frontend"  url="images/email/order_cancellation_venbi.png"}}" alt="Thank for your order" />

    <div class="mail-body" style="margin:50px auto; width: 95%;">
        <p>เรียน คุณ {{htmlescape var=$order.getCustomerName()}},</p>
<br/>
        <p>คุณได้ยกเลิกรายการเลขสั่งซื้อ #{{var order.increment_id}} เรียบร้อยแล้ว.</p>
        <p>ขอบคุณค่ะ</p>
<br/>
        <p>Warm Regards,</p>
        <p>Moxy Team</p>
    </div>

	<div class="navigation-th" style="margin-top: 24px; overflow: hidden;">
		<div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #f5ea14; border-bottom: 2px solid #f5ea14; padding: 0px 0px;">
			<span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 6px;"><a href="http://www.venbi.com/th/formula-nursing.html" style="color: #3c3d41; text-decoration: none;">นมและอุปกรณ์</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 5px;"><a href="http://www.venbi.com/th/diapers.html" style="color: #3c3d41; text-decoration: none;">ผ้าอ้อมสำเร็จรูป</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 5px;"><a href="http://www.venbi.com/th/bath-baby-care.html" style="color: #3c3d41; text-decoration: none;">ผลิตภัณฑ์อาบน้ำและเบบี้แคร์</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 5px;"><a href="http://www.venbi.com/th/toys.html" style="color: #3c3d41; text-decoration: none;">ของเล่น</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 5px;"><a href="http://www.venbi.com/th/clothing.html" style="color: #3c3d41; text-decoration: none;">เสื้อผ้า</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 5px;"><a href="http://www.venbi.com/th/gear.html" style="color: #3c3d41; text-decoration: none;">เตียงและรถเข็น</a></span>
			<span class="nav-item" style="line-height: 33px; font-size: 11; padding: 0px 6px;"><a href="http://www.venbi.com/th/mom-maternity.html" style="color: #3c3d41; text-decoration: none;">แม่-การตั้งครรภ์</a></span>
		</div>
	</div>

	<div class="email-footer" style="background: #bbb; margin-top: 8px; color: #fff; padding: 0px 15px;">
		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px"><img src="{{skin url="images/email/mailbox.png"}}" style="width:18px;"/> ติดต่อเรา</p>
			<p style="font-size: 10px; margin: 0;">02-106-8222</p>
			<p style="margin: 0; font-size: 10px;color:#fff;"><a href="mailto:support@venbi.com" style="color:#fff;">support@venbi.com</a></p>
		</div>

		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">ติดตามที่</p>
			<div class="follow-icons" style="width: 100%">
				<a href="https://www.facebook.com/venbithai"><img src="{{skin url="images/email/facebook_follow.png"}}" style="width: 25px;" alt="Facebook fanpage"></a>
				<a href="https://twitter.com/venbithai"><img src="{{skin url="images/email/twitter_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
				<a href="http://webstagr.am/venbi_thailand"><img src="{{skin url="images/email/instagram_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
				<a href="https://www.pinterest.com/VenbiThailand/"><img src="{{skin url="images/email/pinterest_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
				<a href="https://plus.google.com/+VenbiThai"><img src="{{skin url="images/email/google_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
			</div>
		</div>

		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">ช่องทางการชำระเงิน</p>
			<img src="{{skin url="images/email/payment-th.png"}}" alt="ช่องทางการชำระเงิน" style="width: 109px;">
		</div>

		<div style="clear:both"></div>
	</div>

	<div class="footer" style="padding: 0px 33px; margin-top: 18px;">
		<div class="copyr" style="float: left; font-size: 12px;">&copy;2015 Whatsnew Co.,Ltd. All Rights Reserved</div>
		<div class="links" style="float: right; font-size: 12px;">
			<a href="http://www.venbi.com/th/about-us/" style="color: #3c3d41;"><span>เกี่ยวกับเรา</span></a> |
			<a href="http://www.venbi.com/th/terms-and-conditions/" style="color: #3c3d41;"><span>ข้อกำหนดและเงื่อนไข</span></a> |
			<a href="http://www.venbi.com/th/privacy-policy/" style="color: #3c3d41;"><span>ความเป็นส่วนตัว</span></a>
		</div>
	</div>
	<div style="clear:both"></div>
	<hr style="margin: 40px auto; width: 95%;">

	<!--English Version-->
	<div class="mail-body" style="margin:50px auto; width: 95%;">
        <p>Dear {{htmlescape var=$order.getCustomerName()}},</p>

        <br/>
        <p>Your order #{{var order.increment_id}} has been successfully cancelled.</p>
        <p>We hope you enjoyed shopping with us.</p>
        <br>

        <p>Warm Regards,</p>
        <p>Moxy Team</p>
    </div>

	<div class="navigation-th" style="margin-top: 24px; overflow:hidden;">
		<div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #f5ea14; border-bottom: 2px solid #f5ea14; padding: 0px 0px; overflow: hidden;">
			<span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 5px;"><a href="http://www.venbi.com/en/formula-nursing.html" style="color: #3c3d41; text-decoration: none;">Formula & Nursing</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 5px;"><a href="http://www.venbi.com/en/diapers.html" style="color: #3c3d41; text-decoration: none;">Diapers</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 5px;"><a href="http://www.venbi.com/en/bath-baby-care.html" style="color: #3c3d41; text-decoration: none;">Bath & Baby Care</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 5px;"><a href="http://www.venbi.com/en/toys.html" style="color: #3c3d41; text-decoration: none;">Toys</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 5px;"><a href="http://www.venbi.com/en/clothing.html" style="color: #3c3d41; text-decoration: none;">Clothing</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 5px;"><a href="http://www.venbi.com/en/gear.html" style="color: #3c3d41; text-decoration: none;">Gear</a></span>
			<span class="nav-item" style="line-height: 33px; font-size: 11; padding: 0px 6px;"><a href="http://www.venbi.com/en/mom-maternity.html" style="color: #3c3d41; text-decoration: none;">Mom & Maternity</a></span>
		</div>
	</div>

	<div class="email-footer" style="background: #bbb; margin-top: 8px; color: #fff; padding: 0px 15px;">
		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px"><img src="{{skin url="images/email/mailbox.png"}}" style="width:18px;"/> Contact Us</p>
			<p style="font-size: 10px; margin: 0;">02-106-8222</p>
			<p style="margin: 0; font-size: 10px;color:#fff;"><a href="mailto:support@venbi.com" style="color:#fff;">support@venbi.com</a></p>
		</div>

		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">Follow Us</p>
			<div class="follow-icons" style="width: 100%">
				<a href="https://www.facebook.com/venbithai"><img src="{{skin url="images/email/facebook_follow.png"}}" style="width: 25px;" alt="Facebook fanpage"></a>
				<a href="https://twitter.com/venbithai"><img src="{{skin url="images/email/twitter_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
				<a href="http://webstagr.am/venbi_thailand"><img src="{{skin url="images/email/instagram_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
				<a href="https://www.pinterest.com/VenbiThailand/"><img src="{{skin url="images/email/pinterest_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
				<a href="https://plus.google.com/+VenbiThai"><img src="{{skin url="images/email/google_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
			</div>
		</div>

		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">Payment Options</p>
			<img src="{{skin url="images/email/payment.png"}}" alt="Payment Options" style="width: 109px;">
		</div>

		<div style="clear:both"></div>
	</div>

	<div class="footer" style="padding: 0px 33px; margin-top: 18px;">
		<div class="copyr" style="float: left; font-size: 12px;">&copy;2015 Whatsnew Co.,Ltd. All Rights Reserved</div>
		<div class="links" style="float: right; font-size: 12px;">
			<a href="http://www.venbi.com/en/about-us/" style="color: #3c3d41;"><span>About Us</span></a> |
			<a href="http://www.venbi.com/en/terms-and-conditions/" style="color: #3c3d41;"><span>Term & Conditions</span></a> |
			<a href="http://www.venbi.com/en/privacy-policy/" style="color: #3c3d41;"><span>Privacy Policy</span></a>
		</div>
	</div>
	<div style="clear:both"></div>
</div>
HTML;

$mailTemplateModel->setTemplateText($mailContent);
$mailTemplateModel->save();

$installer->endSetup();