<?php

/** @var Mage_Core_Model_Resource_Setup $this */

$installer = $this;
$installer->startSetup();

$installer->getConnection()->addColumn($installer->getTable('belvg_facebook_customer'), 'full_name', 'VARCHAR( 100 ) NULL DEFAULT NULL');
$installer->getConnection()->addColumn($installer->getTable('belvg_facebook_customer'), 'gender', 'VARCHAR( 6 ) NULL DEFAULT NULL');
$installer->getConnection()->addColumn($installer->getTable('belvg_facebook_customer'), 'birthday', 'DATE NULL DEFAULT NULL');
$installer->getConnection()->addColumn($installer->getTable('belvg_facebook_customer'), 'email', 'VARCHAR(100) NULL DEFAULT NULL');


$installer->endSetup();