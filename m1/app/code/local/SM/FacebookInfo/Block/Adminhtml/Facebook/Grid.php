<?php

class SM_FacebookInfo_Block_Adminhtml_Facebook_Grid extends Mage_Adminhtml_Block_Widget_Grid {
    public function __construct()
    {
        parent::__construct();
        $this->setId('facebook_info_grid');
        //$this->setUseAjax(true);
        $this->setDefaultSort('ID');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('facebookall/facebookall')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('id', array(
            'index' => 'id',
            'header' => Mage::helper('sm_facebookinfo')->__('ID'),
            'type' => 'number',
            'sortable' => true,
            'width' => '20px',
        ));

        $this->addColumn('customer_id', array(
                'index' => 'customer_id',
                'header' => Mage::helper('sm_facebookinfo')->__('Customer Id'),
                'type'  => 'number',
                'sortable'  => true,
                'width' => '200px')
        );

        $this->addColumn('fb_id', array(
                'index' => 'fb_id',
                'header' => Mage::helper('sm_facebookinfo')->__('Facebook Id'),
                'type'  => 'text',
                'sortable'  => true,
                'width' => '200px')
        );

        $this->addColumn('email', array(
                'index' => 'email',
                'header' => Mage::helper('sm_facebookinfo')->__('Email'),
                'type'  => 'text',
                'sortable'=>true,
                'width'     => '250px'
            )
        );

        $this->addColumn('full_name', array(
                'index' => 'full_name',
                'header' => Mage::helper('sm_facebookinfo')->__('Full Name'),
                'type'  => 'text',
                'sortable'=>true,
                'width'     => '300px'
            )
        );

        $this->addColumn('Gender', array(
                'index' => 'gender',
                'header' => Mage::helper('sm_facebookinfo')->__('Gender'),
                'type'  => 'text',
                'sortable'=>true,
                'width'     => '50px'
            )
        );

        $this->addColumn('birthday', array(
                'index' => 'birthday',
                'header' => Mage::helper('sm_facebookinfo')->__('Birthday'),
                'type'  => 'date',
                'sortable'=>true,
                'width'     => '400px'
            )
        );

        $this->addExportType('*/*/exportCsv', Mage::helper('sm_facebookinfo')->__('CSV'));
        $this->addExportType('*/*/exportXml', Mage::helper('sm_facebookinfo')->__('XML'));

        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('slide_id');
        $this->getMassactionBlock()->setFormFieldName('slider');

        return $this;
    }

    public function prepareStatusLayout($value)
    {
        $class = '';
        switch ($value) {
            case 'pending' :
                $class = 'grid-severity-notice';
                break;
            case 'approved' :
                $class = 'grid-severity-major';
                break;
            case 'declined' :
                $class = 'grid-severity-critical';
                break;
        }
        return '<span class="'.$class.'"><span>'.$value.'</span></span>';
    }
}