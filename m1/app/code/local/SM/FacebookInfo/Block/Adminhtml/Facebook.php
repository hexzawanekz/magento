<?php
class SM_FacebookInfo_Block_Adminhtml_Facebook extends Mage_Adminhtml_Block_Widget_Grid_Container{
    protected $_blockGroup = 'sm_facebookinfo';

    public function __construct(){
        $this->_controller = 'adminhtml_facebook';
        $this->_blockGroup = 'sm_facebookinfo';
        $this->_headerText = Mage::helper('sm_facebookinfo')->__('Facebook Info');

        parent::__construct();

        $this->_removeButton('add');
    }
}