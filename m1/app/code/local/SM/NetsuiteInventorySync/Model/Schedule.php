<?php

class SM_NetsuiteInventorySync_Model_Schedule extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        $this->_init('netsuiteinventorysync/schedule');
    }
}