<?php

$installer = $this;
$setup = new Mage_Eav_Model_Entity_Setup('core_setup');
$installer->startSetup();

$setup->updateAttribute('catalog_product', 'override_inventory', 'is_global', Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL);
$installer->endSetup();