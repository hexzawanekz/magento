<?php

// Base-name
$name           = 'moxy';
$engStoreName   = 'Moxy English';
$engStoreCode   = 'moxyen';
$thStoreName    = 'Moxy Thai';
$thStoreCode    = 'moxyth';
$storeIdMoxyEn  = Mage::getModel('core/store')->load($engStoreCode, 'code')->getId();
$storeIdMoxyTh  = Mage::getModel('core/store')->load($thStoreCode, 'code')->getId();

try {
    $installer = $this;
    $installer->startSetup();

    // Force the store to be admin
    Mage::app()->setUpdateMode(false);
    Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
    if (!Mage::registry('isSecureArea'))
        Mage::register('isSecureArea', 1);

    //==========================================================================
    // keymessage moxy en block
    //==========================================================================
    $blockTitle = "keymessage moxy en";
    $blockIdentifier = "keymessage";
    $blockStores = array($storeIdMoxyEn);
    $blockIsActive = 1;
    $blockContent = <<<EOD
<div class="keyMessBlock">
<ul id="keyMess">
<li class="key-ico keyMenu">
<a class="tt" href="{{store url='help'}}#payments"> <span class="cod-ico"><span class="tx-for-top-ico">Cash On delivery</span> </span>
<div class="tooltip">
<div class="top">&nbsp;</div>
<div class="middle">Cash On delivery: With cash on delivery, we offer you the choice to pay once you have received the product from our messenger. This option carries a small charge of 50 baht/ order.</div>
</div>
</a>
</li>
<li class="key-ico keyMenu">
<a class="tt" href="{{store url='help'}}#returns"> <span class="return-ico"><span class="tx-for-top-ico">30 Day Returns</span></span>
<div class="tooltip">
<div class="top">&nbsp;</div>
<div class="middle">In the rare circumstance that your item has a defect, you may send it back to us with a completed Returns Slip attached within 30 days upon receiving the item(s), a full refund will be issued to you.</div>
</div>
</a>
</li>
<li class="key-ico keyMenu">
<a class="tt"> <span class="callcenter-ico"><span class="tx-for-top-ico">Phone Orders: 02-106-8222</span></span>
<div class="tooltip">
<div class="top">&nbsp;</div>
<div class="middle">If you have any problems, you can call us at 02-106-8222 (10am to 7pm) and we will do our best to help you out.</div>
</div>
</a>
</li>
</ul>
</div>
EOD;
    $block = Mage::getModel('cms/block')->getCollection()
        ->addStoreFilter(array($storeIdMoxyEn), $withAdmin = false)
        ->addFieldToFilter('identifier', $blockIdentifier)
        ->getFirstItem();
    if ($block->getId() == 0) {
        $block = Mage::getModel('cms/block');
    } else { // if exists then delete
        $block->delete();
        $block = Mage::getModel('cms/block');
    }
    $block->setTitle($blockTitle);
    $block->setIdentifier($blockIdentifier);
    $block->setStores($blockStores);
    $block->setIsActive($blockIsActive);
    $block->setContent($blockContent);
    $block->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================

    //==========================================================================
    // keymessage moxy th block
    //==========================================================================
    $blockTitle = "keymessage moxy th";
    $blockIdentifier = "keymessage";
    $blockStores = array($storeIdMoxyTh);
    $blockIsActive = 1;
    $blockContent = <<<EOD
<div class="keyMessBlock">
<ul id="keyMess">
<li class="key-ico keyMenu">
<a class="tt" href="{{store url='help'}}#payments"> <span class="cod-ico"><span class="tx-for-top-ico">ชำระเงินปลายทาง</span></span> <!-- <span class="tx-for-top-ico"></span> -->
<div class="tooltip">
<div class="top">&nbsp;</div>
<div class="middle">เป็นการเก็บเงินปลายทางจากผู้รับสินค้า โดยจะมีค่าธรรมเนียมในการเก็บเงินปลายทาง 50 บาท ต่อการสั่งซื้อต่อครั้ง ค่าธรรมเนียมนี้ไม่รวมค่าสินค้า และค่าจัดส่งสินค้า</div>
</div>
</a>
</li>
<li class="key-ico keyMenu">
<a class="tt" href="{{store url='help'}}#returns"> <span class="return-ico"><span class="tx-for-top-ico">รับคืนฟรี ภายใน 30วัน</span></span>
<div class="tooltip">
<div class="top">&nbsp;</div>
<div class="middle">ในบางกรณีซึ่งเป็นไปได้ยากที่สินค้าของคุณจะเกิดตำหนิ,เสียหาย หรือมีข้อผิดพลาด ดังนั้นหากเกิดกรณีดังกล่าวแล้วคุณสามารถดำเนินการขอคืนสินค้าและเงินค่าสินค้าเต็มจำนวน <strong>ภายในระยะเวลา 30 วัน</strong>หลังจากที่คุณได้รับสินค้าเรียบร้อยแล้ว</div>
</div>
</a>
</li>
<li class="key-ico keyMenu">
<a class="tt"> <span class="callcenter-ico"><span class="tx-for-top-ico" style="width: 135px;">โทรสั่งซื้อ: 02-106-8222</span></span>
<div class="tooltip">
<div class="top">&nbsp;</div>
<div class="middle">ท่านสามารถติดต่อศูนย์บริการลูกค้าได้ที่ <strong>02-106-8222</strong><br />(จ-ส 10.00-19.00)<br />เรามีทีมงานที่ยินดีและพร้อมที่จะให้คำปรึกษากับท่าน กรุณาติดต่อเราหากท่านมีข้อสงสัยหรือได้รับความไม่สะดวกสบาย</div>
</div>
</a>
</li>
</ul>
</div>
EOD;
    $block = Mage::getModel('cms/block')->getCollection()
        ->addStoreFilter(array($storeIdMoxyTh), $withAdmin = false)
        ->addFieldToFilter('identifier', $blockIdentifier)
        ->getFirstItem();
    if ($block->getId() == 0) {
        $block = Mage::getModel('cms/block');
    } else { // if exists then delete
        $block->delete();
        $block = Mage::getModel('cms/block');
    }
    $block->setTitle($blockTitle);
    $block->setIdentifier($blockIdentifier);
    $block->setStores($blockStores);
    $block->setIsActive($blockIsActive);
    $block->setContent($blockContent);
    $block->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================

    //==========================================================================
    // Site banner top right moxy en block
    //==========================================================================
    $blockTitle = "Site banner top right venbi En";
    $blockIdentifier = "site_banner_top_right";
    $blockStores = array($storeIdMoxyEn);
    $blockIsActive = 1;
    $blockContent = <<<EOD
<div id="topMklink"><a href="{{store url='help'}}"><img src="{{media url="wysiwyg/Moxy/Moxy_freeshipping_eng.jpg"}}" alt="" /></a></div>
EOD;
    $block = Mage::getModel('cms/block')->getCollection()
        ->addStoreFilter(array($storeIdMoxyEn), $withAdmin = false)
        ->addFieldToFilter('identifier', $blockIdentifier)
        ->getFirstItem();
    if ($block->getId() == 0) {
        $block = Mage::getModel('cms/block');
    } else { // if exists then delete
        $block->delete();
        $block = Mage::getModel('cms/block');
    }
    $block->setTitle($blockTitle);
    $block->setIdentifier($blockIdentifier);
    $block->setStores($blockStores);
    $block->setIsActive($blockIsActive);
    $block->setContent($blockContent);
    $block->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================

    //==========================================================================
    // Site banner top right moxy en block
    //==========================================================================
    $blockTitle = "Site banner top right venbi Th";
    $blockIdentifier = "site_banner_top_right";
    $blockStores = array($storeIdMoxyTh);
    $blockIsActive = 1;
    $blockContent = <<<EOD
<div id="topMklink"><a href="{{store url='help'}}"><img src="{{media url="wysiwyg/Moxy/Moxy_freeshipping_th.jpg"}}" alt="" /></a></div>
EOD;
    $block = Mage::getModel('cms/block')->getCollection()
        ->addStoreFilter(array($storeIdMoxyTh), $withAdmin = false)
        ->addFieldToFilter('identifier', $blockIdentifier)
        ->getFirstItem();
    if ($block->getId() == 0) {
        $block = Mage::getModel('cms/block');
    } else { // if exists then delete
        $block->delete();
        $block = Mage::getModel('cms/block');
    }
    $block->setTitle($blockTitle);
    $block->setIdentifier($blockIdentifier);
    $block->setStores($blockStores);
    $block->setIsActive($blockIsActive);
    $block->setContent($blockContent);
    $block->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================

    //==========================================================================
    // Site Banner Top Right Second en block
    //==========================================================================
    $blockTitle = "Site Banner Top Right Second";
    $blockIdentifier = "site_banner_top_right_second";
    $blockStores = array('13','1','10','5',"$storeIdMoxyEn");
    $blockIsActive = 1;
    $blockContent = <<<EOD
<div class="img_top_right_en">&nbsp;</div>
EOD;
    $block = Mage::getModel('cms/block')->getCollection()
        ->addStoreFilter(array('13','1','10','5',"$storeIdMoxyEn"), $withAdmin = false)
        ->addFieldToFilter('identifier', $blockIdentifier)
        ->getFirstItem();
    if ($block->getId() == 0) {
        $block = Mage::getModel('cms/block');
    } else { // if exists then delete
        $block->delete();
        $block = Mage::getModel('cms/block');
    }
    $block->setTitle($blockTitle);
    $block->setIdentifier($blockIdentifier);
    $block->setStores($blockStores);
    $block->setIsActive($blockIsActive);
    $block->setContent($blockContent);
    $block->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================

    //==========================================================================
    // Site Banner Top Right Second th block
    //==========================================================================
    $blockTitle = "Site Banner Top Right Second";
    $blockIdentifier = "site_banner_top_right_second";
    $blockStores = array('15','2','11','4',"$storeIdMoxyTh");
    $blockIsActive = 1;
    $blockContent = <<<EOD
<div class="img_top_right_th">&nbsp;</div>
EOD;
    $block = Mage::getModel('cms/block')->getCollection()
        ->addStoreFilter(array('15','2','11','4',"$storeIdMoxyTh"), $withAdmin = false)
        ->addFieldToFilter('identifier', $blockIdentifier)
        ->getFirstItem();
    if ($block->getId() == 0) {
        $block = Mage::getModel('cms/block');
    } else { // if exists then delete
        $block->delete();
        $block = Mage::getModel('cms/block');
    }
    $block->setTitle($blockTitle);
    $block->setIdentifier($blockIdentifier);
    $block->setStores($blockStores);
    $block->setIsActive($blockIsActive);
    $block->setContent($blockContent);
    $block->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================

    //==========================================================================
    // Site Banner Home Slider en block
    //==========================================================================
    $blockTitle = "Site Banner Home Slider";
    $blockIdentifier = "slider_1";
    $blockStores = array($storeIdMoxyEn);
    $blockIsActive = 1;
    $blockContent = <<<EOD
<div class="homepage-main">
<div id="container">
<div id="slideshow">
<ul id="slide-nav">
<li id="prev"><a class="control-s pre-s" href="#">Previous</a></li>
<li id="next"><a class="control-s nxt-s" href="#">Next</a></li>
</ul>
<ul id="slides">
<li><a href="http://www.sanoga.com/en/workout-fitness/protein/whey-proteins.html"><img src="{{media url="wysiwyg/moxy-whey150-homepage-banner.jpg"}}" alt="Whey Protein code 150" /></a>
<div class="caption">Whey Protein discount 150 bht when purchase 1500 bht</div>
</li>
<li><a href="http://www.sanoga.com/en/health-care/medical-equipment/health-monitors/maxxlife-major-ii-strip-2.html"><img src="{{media url="wysiwyg/health-moxy-homepage-banner.jpg"}}" alt="MaxxLife for health" /></a>
<div class="caption">Health Monitor</div>
</li>
<li><a href="http://www.sanoga.com/th/beauty/skin-supplements/whitening.html"><img src="{{media url="wysiwyg/moxy-skin-care-homepage-banner.jpg"}}" alt="Skin Care Supplement Sale" /></a>
<div class="caption">Skin Care Supplement up to 46%</div>
</li>
<li><a href="http://www.sanoga.com/th/catalog/category/view/s/a-a-a-a-a-a-sa-a-a/id/974/"><img src="{{media url="wysiwyg/clearance-sale-moxy-homepage-banner.jpg"}}" alt="clearance sale sanoga" /></a>
<div class="caption">CLEARANCE SALE SANOGA</div>
</li>
<li><a><img src="{{media url="wysiwyg/ktc-moxy-promotion-homepage-banner-TH.jpg"}}" alt="" /></a>
<div class="caption">KTC 0%</div>
</li>
</ul>
</div>
</div>
</div>
<div class="homepage-bannerl1"><a href="http://www.moxyst.com/th/help/#shipping"><img src="{{media url="wysiwyg/moxy-free-delivery-free-COD-small-banner.jpg"}}" alt="Free Shipping" /> </a></div>
<!-- start small banner 1 -->
<div class="homepage-bannerl1"><a href="https://www.moxyst.com/th/customer/account/create/"> <img src="{{media url="wysiwyg/moxy-sign-up-get-200-small-banner.jpg"}}" alt="Sign up, Get Free 200 Baht" /></a></div>
<!-- end small banner 1 -->
<p>&nbsp;</p>
<!-- start small banner 2 -->
<div class="homepage-bannerl2"><a href="http://amado.popsho.ps/moxyst/"> <img src="{{media url="wysiwyg/Moxy/Amado_banner.png"}}" alt="amado" /></a></div>
<!-- end small banner 2 -->
<p>&nbsp;</p>
EOD;
    $block = Mage::getModel('cms/block')->getCollection()
        ->addStoreFilter(array($storeIdMoxyEn), $withAdmin = false)
        ->addFieldToFilter('identifier', $blockIdentifier)
        ->getFirstItem();
    if ($block->getId() == 0) {
        $block = Mage::getModel('cms/block');
    } else { // if exists then delete
        $block->delete();
        $block = Mage::getModel('cms/block');
    }
    $block->setTitle($blockTitle);
    $block->setIdentifier($blockIdentifier);
    $block->setStores($blockStores);
    $block->setIsActive($blockIsActive);
    $block->setContent($blockContent);
    $block->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================

    //==========================================================================
    // Site Banner Home Slider th block
    //==========================================================================
    $blockTitle = "Site Banner Home Slider";
    $blockIdentifier = "slider_1";
    $blockStores = array($storeIdMoxyTh);
    $blockIsActive = 1;
    $blockContent = <<<EOD
<div class="homepage-main">
<div id="container">
<div id="slideshow">
<ul id="slide-nav">
<li id="prev"><a class="control-s pre-s" href="#">Previous</a></li>
<li id="next"><a class="control-s nxt-s" href="#">Next</a></li>
</ul>
<ul id="slides">
<ul id="slides">
<li><a href="http://www.sanoga.com/th/workout-fitness/protein/whey-proteins.html"><img src="{{media url="wysiwyg/moxy-whey150-homepage-banner.jpg"}}" alt="Whey Protein code 150" /></a>
<div class="caption">Whey Protein discount 150 bht when purchase 1500 bht</div>
</li>
<li><a href="http://www.sanoga.com/th/health-care/medical-equipment/health-monitors/maxxlife-major-ii-strip-2.html"><img src="{{media url="wysiwyg/health-moxy-homepage-banner.jpg"}}" alt="MaxxLife for health" /></a>
<div class="caption">Health Monitor</div>
</li>
<li><a href="http://www.sanoga.com/th/catalogsearch/result/?q=eazy2diet"><img src="{{media url="wysiwyg/moxy-eazy2diet-homepage-banner.jpg"}}" alt="eazy2diet sales" /></a>
<div class="caption">Eazy2Diet up to 24%</div>
</li>
<li><a href="http://www.sanoga.com/th/beauty/skin-supplements/whitening.html"><img src="{{media url="wysiwyg/moxy-skin-care-homepage-banner.jpg"}}" alt="Skin Care Supplement Sale" /></a>
<div class="caption">Skin Care Supplement up to 46%</div>
</li>
<li><a href="http://www.sanoga.com/th/beauty.html"><img src="{{media url="wysiwyg/moxy-friday-happy10-homepage-banner2.jpg"}}" alt="" /></a>
<div class="caption">Coupon 10% off</div>
</li>
<li><a href="http://www.sanoga.com/th/catalog/category/view/s/a-a-a-a-a-a-sa-a-a/id/974/"><img src="{{media url="wysiwyg/clearance-sale-moxy-homepage-banner.jpg"}}" alt="clearance sale sanoga" /></a>
<div class="caption">CLEARANCE SALE SANOGA</div>
</li>
<li><a><img src="{{media url="wysiwyg/ktc-moxy-promotion-homepage-banner-TH.jpg"}}" alt="" /></a>
<div class="caption">KTC 0%</div>
</li>
</ul>
</ul>
</div>
</div>
</div>
<div class="homepage-bannerl1"><a href="http://www.moxyst.com/th/help/#shipping"><img src="{{media url="wysiwyg/moxy-free-delivery-free-COD-small-banner.jpg"}}" alt="Free Shipping" /> </a></div>
<!-- start small banner 1 -->
<div class="homepage-bannerl1"><a href="http://www.moxyst.com/?popup=acommerce?popup=acommerce"> <img src="{{media url="wysiwyg/moxy-sign-up-get-200-small-banner.jpg"}}" alt="Sign up, Get Free 200 Baht" /></a></div>
<!-- end small banner 1 -->
<p>&nbsp;</p>
<!-- start small banner 2 -->
<div class="homepage-bannerl2"><a href="http://amado.popsho.ps/moxyst/"><img src="{{media url="wysiwyg/Moxy/Amado_banner.png"}}" alt="amado" /></a></div>
<!-- end small banner 2 -->
<p>&nbsp;</p>
EOD;
    $block = Mage::getModel('cms/block')->getCollection()
        ->addStoreFilter(array($storeIdMoxyTh), $withAdmin = false)
        ->addFieldToFilter('identifier', $blockIdentifier)
        ->getFirstItem();
    if ($block->getId() == 0) {
        $block = Mage::getModel('cms/block');
    } else { // if exists then delete
        $block->delete();
        $block = Mage::getModel('cms/block');
    }
    $block->setTitle($blockTitle);
    $block->setIdentifier($blockIdentifier);
    $block->setStores($blockStores);
    $block->setIsActive($blockIsActive);
    $block->setContent($blockContent);
    $block->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================

    //==========================================================================
    // Site Footer Links Moxy EN block
    //==========================================================================
    $blockTitle = "Site Footer Links Moxy EN";
    $blockIdentifier = "footer_links_1";
    $blockStores = array($storeIdMoxyEn);
    $blockIsActive = 1;
    $blockContent = <<<EOD
<ul class="footer_links" style="margin-left: 0;">
    <li class="title">Information</li>
    <li><a href="{{store url='about-us'}}">About Us</a></li>
    <li><a href="{{store url='contacts'}}">Contact Us</a></li>
    <li><a href="/newsletter-archive/">Newsletter</a></li>
    <li><a href="#">Family of Brands</a></li>
    <li><a href="#">Press Links</a></li>
</ul>
<ul id="footer_help" class="footer_links" style="margin-left: 50px;">
    <li class="title">Services and Support</li>
    <li><a href="{{store url='help'}}">Help and FAQ</a></li>
    <li><a href="{{store url='help'}}#howtoorder">How To Order</a></li>
    <li><a href="{{store url='help'}}#payments">Payments</a></li>
    <li><a href="{{store url='help'}}#rewardpoints">Reward Points</a></li>
    <li><a href="#">Refer-A-Friend</a></li>
</ul>
<ul class="footer_links" style="margin-left: 50px;">
    <li class="title">Secruity and Privacy</li>
    <li><a href="{{store url='terms-and-conditions'}}">Terms &amp; Conditions</a></li>
    <li><a href="{{store url='privacy-policy'}}">Privacy Policy</a></li>

    <li class="title" style="margin-top: 15px;">Shipping and Returns</li>
    <li><a href="{{store url='help'}}#shipping">Shipping &amp; Delivery</a></li>
    <li><a href="{{store url='help'}}#returns">Cancellation &amp; Returns</a></li>
    <li><a href="{{store url='return-policy'}}">Return Policy</a></li>
</ul>

<ul class="footer_links" style="margin-left: 60px;">
    <li class="title">LINE CHAT</li>
    <li><img style="width: 100px; height: 100px; margin-left: -15px;" src="{{media url="wysiwyg/Moxy/MOXY_QR.jpg"}}" alt="" /></li>
</ul>
EOD;
    $block = Mage::getModel('cms/block')->getCollection()
        ->addStoreFilter(array($storeIdMoxyEn), $withAdmin = false)
        ->addFieldToFilter('identifier', $blockIdentifier)
        ->getFirstItem();
    if ($block->getId() == 0) {
        $block = Mage::getModel('cms/block');
    } else { // if exists then delete
        $block->delete();
        $block = Mage::getModel('cms/block');
    }
    $block->setTitle($blockTitle);
    $block->setIdentifier($blockIdentifier);
    $block->setStores($blockStores);
    $block->setIsActive($blockIsActive);
    $block->setContent($blockContent);
    $block->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================

    //==========================================================================
    // Site Footer Links Moxy TH block
    //==========================================================================
    $blockTitle = "Site Footer Links Moxy TH";
    $blockIdentifier = "footer_links_1";
    $blockStores = array($storeIdMoxyTh);
    $blockIsActive = 1;
    $blockContent = <<<EOD
<ul class="footer_links" style="margin-left: 0;">
    <li class="title">ข้อมูล</li>
    <li><a href="{{store url='about-us'}}">เกี่ยวกับเรา</a></li>
    <li><a href="{{store url='contacts'}}">ติดต่อเรา</a></li>
    <li><a href="/newsletter-archive/">นิวส์เลตเตอร์</a></li>
    <li><a href="#">Family of Brands</a></li>
    <li><a href="#">Press Links</a></li>
</ul>
<ul id="footer_help" class="footer_links" style="margin-left: 50px;">
    <li class="title">ช่วยเหลือ</li>
    <li><a href="{{store url='help'}}">คำถามที่พบบ่อย</a></li>
    <li><a href="{{store url='help'}}#howtoorder">วิธีการสั่งซื้อ</a></li>
    <li><a href="{{store url='help'}}#payments">การชำระเงิน</a></li>
    <li><a href="{{store url='help'}}#rewardpoints">คะแนนสะสม</a></li>
    <li><a href="#">แนะนำเพื่อน</a></li>
</ul>
<ul class="footer_links" style="margin-left: 50px;">
    <li class="title">นโยบายความเป็นส่วนตัว</li>
    <li><a href="{{store url='terms-and-conditions'}}">ข้อกำหนดและเงื่อนไข</a></li>
    <li><a href="{{store url='privacy-policy'}}">ความเป็นส่วนตัว</a></li>

    <li class="title" style="margin-top: 15px;">การจัดส่งและคืนสินค้า</li>
    <li><a href="{{store url='help'}}#shipping">การจัดส่งสินค้า</a></li>
    <li><a href="{{store url='help'}}#returns">การคืนสินค้าและขอคืนเงิน</a></li>
    <li><a href="{{store url='return-policy'}}">นโยบายการคืนสินค้า</a></li>
</ul>
<ul class="footer_links" style="margin-left: 60px;">
    <li class="title">LINE CHAT</li>
    <li><img style="width: 100px; height: 100px; margin-left: -15px;" src="{{media url="wysiwyg/Moxy/MOXY_QR.jpg"}}" alt="" /></li>
</ul>
EOD;
    $block = Mage::getModel('cms/block')->getCollection()
        ->addStoreFilter(array($storeIdMoxyTh), $withAdmin = false)
        ->addFieldToFilter('identifier', $blockIdentifier)
        ->getFirstItem();
    if ($block->getId() == 0) {
        $block = Mage::getModel('cms/block');
    } else { // if exists then delete
        $block->delete();
        $block = Mage::getModel('cms/block');
    }
    $block->setTitle($blockTitle);
    $block->setIdentifier($blockIdentifier);
    $block->setStores($blockStores);
    $block->setIsActive($blockIsActive);
    $block->setContent($blockContent);
    $block->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================


    $installer->endSetup();
} catch (Excpetion $e) {
    Mage::logException($e);
    Mage::log("ERROR IN SETUP " . $e->getMessage());
}
