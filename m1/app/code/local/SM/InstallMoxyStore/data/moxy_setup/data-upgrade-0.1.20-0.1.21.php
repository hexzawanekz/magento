<?php

// init Magento
Mage::app();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

// get store ids
$en     = Mage::getModel('core/store')->load('en', 'code')->getId();// Petloft
$th     = Mage::getModel('core/store')->load('th', 'code')->getId();
$len    = Mage::getModel('core/store')->load('len', 'code')->getId();// Lafema
$lth    = Mage::getModel('core/store')->load('lth', 'code')->getId();
$ven    = Mage::getModel('core/store')->load('ven', 'code')->getId();// Venbi
$vth    = Mage::getModel('core/store')->load('vth', 'code')->getId();
$sen    = Mage::getModel('core/store')->load('sen', 'code')->getId();// Sanoga
$sth    = Mage::getModel('core/store')->load('sth', 'code')->getId();
$moxyen = Mage::getModel('core/store')->load('moxyen', 'code')->getId();// Moxy
$moxyth = Mage::getModel('core/store')->load('moxyth', 'code')->getId();


// remove old blocks
$blockIdentifier = "header_right_image";
/** @var Mage_Cms_Model_Resource_Block_Collection $oldBlocks */
$oldBlocks = Mage::getModel('cms/block')->getCollection()
    ->addFieldToFilter('identifier', $blockIdentifier);
if($oldBlocks->count() > 0){
    foreach($oldBlocks as $old){
        if ($old->getId()) {
            $old->delete();
        }
    }
}


//==========================================================================
// header_right_image en store
//==========================================================================
$blockTitle = "Header right image EN";
$blockIdentifier = "header_right_image";
$blockStores = array($moxyen);
$blockIsActive = 1;

// delete old block if have
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete

$content = <<<EOD
<a id="btn-open-popup" href="{{store url}}">
    <img style="width: 100%;" src="{{media url='wysiwyg/header_right_image.png'}}" alt="" />
</a>
<script type="text/javascript">
// <![CDATA[
    jQuery(document).ready(function () {
        jQuery('#btn-open-popup').click(function (e) {
            e.preventDefault();
            jQuery.colorbox({
                open:true,
                iframe:true,
                href:"/popup/moxy_popup/",
                innerWidth:360,
                innerHeight:550,
                scrolling:false,
                closeButton:true
            });
            return;
        });
    });
// ]]>
</script>
EOD;
/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================

//==========================================================================
// header_right_image th store
//==========================================================================
$blockTitle = "Header right image TH";
$blockIdentifier = "header_right_image";
$blockStores = array($moxyth);
$blockIsActive = 1;

// delete old block if have
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete

$content = <<<EOD
<a id="btn-open-popup" href="{{store url}}">
    <img style="width: 100%;" src="{{media url='wysiwyg/header_right_image_th.png'}}" alt="" />
</a>
<script type="text/javascript">
// <![CDATA[
    jQuery(document).ready(function () {
        jQuery('#btn-open-popup').click(function (e) {
            e.preventDefault();
            jQuery.colorbox({
                open:true,
                iframe:true,
                href:"/popup/moxy_popup/",
                innerWidth:360,
                innerHeight:550,
                scrolling:false,
                closeButton:true
            });
            return;
        });
    });
// ]]>
</script>
EOD;

/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================