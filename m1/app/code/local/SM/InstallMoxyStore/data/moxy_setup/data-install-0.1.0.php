<?php

// Base-name
$name           = 'moxy';
$engStoreName   = 'Moxy English';
$engStoreCode   = 'moxyen';
$thStoreName    = 'Moxy Thai';
$thStoreCode    = 'moxyth';

// Init Magento
Mage::app();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

// Create the root catalog
$category = Mage::getModel('catalog/category');
$categories = Mage::getModel('catalog/category')->getCollection()
    ->addAttributeToSelect('*')
    ->addAttributeToFilter('name', ucfirst($name))
    ->addAttributeToFilter('parent_id', 1)
    ->addAttributeToFilter('level', 1);
if($categories->count() > 0 && $categories->count() < 2){
    $category = $categories->getFirstItem();
}
$category->setStoreId(Mage_Core_Model_App::ADMIN_STORE_ID);
$category->setData('name', ucfirst($name));
$category->setData('url_key', $name.'-catalog');
$category->setData('display_mode', 'PRODUCTS');
$category->setData('level', 1);
$category->setData('is_active', 1);
$category->setData('include_in_menu', 1);
$category->setData('is_anchor', 1);
$category->setData('custom_use_parent_settings', 0);
$category->setData('custom_apply_to_products', 0);
$category->setData('attribute_set_id', $category->getDefaultAttributeSetId());
$category->save();
$category->setParentId(1);
$category->setPath('1/'.$category->getId());
$category->save();
$categoryId = $category->getId();


// Load the website
$website = Mage::getModel('core/website')->load('base', 'code');
if($website->getId() > 0) {
    $websiteId = $website->getId();


    // Create the group
    $group = Mage::getModel('core/store_group');
    $groups = Mage::getModel('core/store_group')->getCollection()
        ->addFieldToSelect('*')
        ->addFieldToFilter('name', array('like'=>ucfirst($name)))
        ->addFieldToFilter('website_id', $websiteId);
    if($groups->count() > 0 && $groups->count() < 2){
        $group = $groups->getFirstItem();
    }
    $group->setData('website_id', $websiteId);
    $group->setData('name', ucfirst($name));
    $group->setData('root_category_id', $categoryId);
    $group->save();
    $groupId = $group->getGroupId();


    // Create the store views

    // English
    $engStore = Mage::getModel('core/store');
    $engStoresChecker = Mage::getModel('core/store')->getCollection()
        ->addFieldToSelect('*')
        ->addFieldToFilter('code', $engStoreCode)
        ->addFieldToFilter('website_id', $websiteId)
        ->addFieldToFilter('group_id', $groupId);
    if ($engStoresChecker->count() > 0 && $engStoresChecker->count() < 2) {
        $engStore = $engStoresChecker->getFirstItem();
    }
    $engStore->setData('website_id', $websiteId);
    $engStore->setData('group_id', $groupId);
    $engStore->setData('name', $engStoreName);
    $engStore->setData('code', $engStoreCode);
    $engStore->setData('is_active', 1);
    $engStore->save();
    $engStoreId = $engStore->getStoreId();

    // Thai
    $thStore = Mage::getModel('core/store');
    $thStoresChecker = Mage::getModel('core/store')->getCollection()
        ->addFieldToSelect('*')
        ->addFieldToFilter('code', $thStoreCode)
        ->addFieldToFilter('website_id', $websiteId)
        ->addFieldToFilter('group_id', $groupId);
    if ($thStoresChecker->count() > 0 && $thStoresChecker->count() < 2) {
        $thStore = $thStoresChecker->getFirstItem();
    }
    $thStore->setData('website_id', $websiteId);
    $thStore->setData('group_id', $groupId);
    $thStore->setData('name', $thStoreName);
    $thStore->setData('code', $thStoreCode);
    $thStore->setData('is_active', 1);
    $thStore->save();
    $thStoreId = $thStore->getStoreId();


    // save config for store views
    $configurator = new Mage_Core_Model_Config();
    // English
    $configurator->saveConfig('web/unsecure/base_link_url', '{{unsecure_base_url}}en/', 'stores', $engStoreId);
    $configurator->saveConfig('web/secure/base_link_url', '{{secure_base_url}}en/', 'stores', $engStoreId);
    $configurator->saveConfig('design/theme/template', 'moxy', 'stores', $engStoreId);
    $configurator->saveConfig('design/theme/skin', 'moxy', 'stores', $engStoreId);
    $configurator->saveConfig('design/theme/layout', 'moxy', 'stores', $engStoreId);
    $configurator->saveConfig('design/theme/default', 'petloft', 'stores', $engStoreId);
    // Thai
    $configurator->saveConfig('general/locale/code', 'th_TH', 'stores', $thStoreId);
    $configurator->saveConfig('web/unsecure/base_link_url', '{{unsecure_base_url}}th/', 'stores', $thStoreId);
    $configurator->saveConfig('web/secure/base_link_url', '{{secure_base_url}}th/', 'stores', $thStoreId);
    $configurator->saveConfig('design/theme/template', 'moxy', 'stores', $thStoreId);
    $configurator->saveConfig('design/theme/skin', 'moxy', 'stores', $thStoreId);
    $configurator->saveConfig('design/theme/layout', 'moxy', 'stores', $thStoreId);
    $configurator->saveConfig('design/theme/default', 'petloft', 'stores', $thStoreId);
}