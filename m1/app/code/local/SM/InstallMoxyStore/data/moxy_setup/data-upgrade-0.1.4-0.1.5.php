<?php

// Base-name
$name           = 'moxy';
$engStoreName   = 'Moxy English';
$engStoreCode   = 'moxyen';
$thStoreName    = 'Moxy Thai';
$thStoreCode    = 'moxyth';
$storeIdMoxyEn  = Mage::getModel('core/store')->load($engStoreCode, 'code')->getId();
$storeIdMoxyTh  = Mage::getModel('core/store')->load($thStoreCode, 'code')->getId();
$newProductsCateId = 0;
$bestSellersCateId = 0;
try {
    $installer = $this;
    $installer->startSetup();

    // Force the store to be admin
    Mage::app()->setUpdateMode(false);
    Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
    if (!Mage::registry('isSecureArea'))
        Mage::register('isSecureArea', 1);

    //==========================================================================
    // About us EN Page
    //==========================================================================
    $pageTitle = "About Us";
    $pageIdentifier = "about-us";
    $pageStores = array($storeIdMoxyEn);
    $pageIsActive = 1;
    $pageUnderVersionControl = 0;
    $pageContentHeading = "About Us";
    $pageContent = <<<EOD
<div style="margin: 20px 0;">&nbsp;</div>
<div style="font: normal 14px 'Tahoma'; color: #505050; line-height: 22px;">
<p>When &lsquo;Health is your greatest wealth&rsquo;, why not do your best to preserve it?</p>
<br /> <br />
<p>At Sanoga, we believe that looking after our health is one of the most important things in life. Therefore it shouldn&rsquo;t involve spending time on travelling, popping in and out of stores just to look for the right product. So, we came up with a brilliant idea! Why not let us do all the legwork for you, saving you time and hassles?</p>
<br />
<p>Sanoga aims to make looking after your health easier. We source the best health products available, complete with important information to help you choose what&rsquo;s suitable for you. You will find a selection of popular local and international brands that can be speedily delivered to you in 2-5 business days in Bangkok and 3-7 business days for&nbsp;<span>nationwide.&nbsp;</span></p>
<br />
<p>We offer best quality and a vast selection of products. They are cheaper, more convenient, and best of all delivery is FREE!</p>
<br />
<p>Currently, we store over 1,000 health products from well-known brands including Health Care, Vitamins, collagen, Beauty, Personal well-being, Sports &amp; Fitness, Weight Loss inclluding BLACKMORES, Baschi, Beauty Wise, Body Shape, Brand's, CLG500, Centrum, Collagen Star, Colly, Dr.Absolute, DYMATIZE, Durex, Eucerin, Genesis, Hi-Balanz, K-Palette, Meiji, Mega We Care, Okamoto, OMG, ProFlex, Real Elixir, Sebamed, Seoul Secret, Taurus, etc.</p>
<br />
<p><strong>Mission</strong><br /> Provide the largest selection of health products and deliver to anywhere within Thailand in the shortest time possible.</p>
<br />
<p><strong>Core Values</strong><br />1. Always WOW the customer and over deliver<br /> 2. Offer the best QUALITY in our products and services <br /> 3. Deliver everything FASTER</p>
</div>
EOD;

    $pageRootTemplate = 'one_column';
    $pageLayoutUpdateXml = <<<EOD
EOD;


    $pageCustomLayoutUpdateXML = <<<EOD
EOD;

    $pageMetaKeywords = "";
    $pageMetaDescription = "";

    $page = Mage::getModel('cms/page')->getCollection()
        ->addStoreFilter(array($storeIdMoxyEn), $withAdmin = true)
        ->addFieldToFilter('identifier', $pageIdentifier)
        ->getFirstItem();
    if ($page->getId() == 0) {
        $page = Mage::getModel('cms/page');
    }
    else
    {
        // if exists then repair
        $page = Mage::getModel('cms/page')->load($page->getId());
    }

    $page->setTitle($pageTitle);
    $page->setIdentifier($pageIdentifier);
    $page->setStores($pageStores);
    $page->setIsActive($pageIsActive);
    $page->setUnderVersionControl($pageUnderVersionControl);
    $page->setContentHeading($pageContentHeading);
    $page->setContent($pageContent);
    $page->setRootTemplate($pageRootTemplate);
    $page->setLayoutUpdateXml($pageLayoutUpdateXml);
    $page->setCustomLayoutUpdateXml($pageCustomLayoutUpdateXML);
    $page->setMetaKeywords($pageMetaKeywords);
    $page->setMetaDescription($pageMetaDescription);
    $page->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================

    //==========================================================================
    // About us TH Page
    //==========================================================================
    $pageTitle = "About us | เกี่ยวกับเรา";
    $pageIdentifier = "about-us";
    $pageStores = array($storeIdMoxyTh);
    $pageIsActive = 1;
    $pageUnderVersionControl = 0;
    $pageContentHeading = "เกี่ยวกับ Moxy";
    $pageContent = <<<EOD
<div style="font: normal 14px 'Tahoma'; color: #505050; line-height: 22px;">
<p>ในเมื่อ &lsquo;สมบัติที่มีค่ามากที่สุดในชีวิตเรา ก็คือ สุขภาพที่สมบูรณ์&rsquo; ทำไมไม่รักษาไว้ให้ดีที่สุด</p>
<br />
<p>ที่ Sanoga เราเชื่อว่า การดูแลสุขภาพเป็นหนึ่งในสิ่งที่สำคัญที่สุดในชีวิต จึงไม่ควรที่จะทำให้เป็นเรื่องยาก ไม่ว่าต้องฝ่ารถติดเป็นชั่วโมงๆ เพื่อเดินทางไปซื้อ หรือเดินเข้าร้านนู้นออกร้านนี้ เพียงเพื่อหาสินค้าที่ต้องการ เราจึงมีไอเดียดีๆ ที่จะช่วยคุณประหยัดเวลาและลดความยุ่งยากโดยการจัดหาสิ่งที่คุณต้องการมาให้..โดยคุณไม่ต้องออกแรง</p>
<br />
<p>Sanoga ตั้งใจที่จะทำให้การดูแลสุขภาพของคุณเป็นเรื่องง่าย เราเฟ้นหาสินค้าเพื่อสุขภาพที่ดีที่สุด พร้อมกับให้ข้อมูลเพื่อช่วยให้คุณตัดสินใจได้ว่าอะไรเหมาะกับคุณที่สุด เรามีสินค้าจากแบรนด์ที่เป็นที่รู้จักจากทั้งในและต่างประเทศ โดยสามารถส่งตรงถึงมือคุณได้ภายใน 2-5 วันในกรุงเทพ และ 3-7 วันสำหรับต่างจังหวัด</p>
<br />
<p>เรามีสินค้าให้เลือกเป็นจำนวนมาก คัดสินค้าที่คุณภาพดีที่สุดสินค้า ในราคาที่ถูกกว่า ทำให้ทุกอย่างสะดวกกว่า และที่ดีไปกว่านั้นคือ เราส่งสินค้าตรงถึงมือคุณ!</p>
<br />
<p>ขณะนี้เรามีสินค้าจากแบรนด์ดังระดับโลกกว่า 1,000 รายการ อาทิ BLACKMORES, Baschi, Beauty Wise, Body Shape, Brand's, CLG500, Centrum, Collagen Star, Colly, Dr.Absolute, DYMATIZE, Durex, Eucerin, Genesis, Hi-Balanz, K-Palette, Meiji, Mega We Care, Okamoto, OMG, ProFlex, Real Elixir, Sebamed, Seoul Secret, Taurus และอื่นๆ อีกมากมาย</p>
<br />
<p><strong>พันธกิจ</strong><br /> เป็นที่รวบรวมสินค้าเพื่อสุขภาพที่มีคุณภาพมากที่สุด และจัดส่งสินค้าอย่างรวดเร็วทั่วประเทศไทยในระยะเวลาอันสั้น</p>
<br />
<p><strong>ค่านิยมองค์กร</strong><br /> 1. สร้างความประทับใจ ประหลาดใจในสินค้าและบริการจนลูกค้าต้องร้อง &lsquo;ว้าว&rsquo; เสมอ<br /> 2. เสนอแต่ผลิตภัณฑ์คุณภาพชั้นเลิศและบริการอันยอดเยี่ยมเท่านั้น <br /> 3. บริการจัดส่งสินค้าให้ได้มากกว่าคำว่ารวดเร็ว</p>
</div>
EOD;

    $pageRootTemplate = 'one_column';
    $pageLayoutUpdateXml = <<<EOD
EOD;


    $pageCustomLayoutUpdateXML = <<<EOD
EOD;

    $pageMetaKeywords = "";
    $pageMetaDescription = "Moxy คือศูนย์รวมผลิตภัณฑ์เพื่อสุขภาพและเครื่องสำอาง อาหารเสริม คอลลาเจน และวิตามินต่างๆ คุณภาพแท้ 100% สินค้ามี อย.ทุกชิ้น จากแบรนด์ชั้นนำทั่วโลก";

    $page = Mage::getModel('cms/page')->getCollection()
        ->addStoreFilter(array($storeIdMoxyTh), $withAdmin = true)
        ->addFieldToFilter('identifier', $pageIdentifier)
        ->getFirstItem();
    if ($page->getId() == 0) {
        $page = Mage::getModel('cms/page');
    }
    else
    {
        // if exists then repair
        $page = Mage::getModel('cms/page')->load($page->getId());
    }

    $page->setTitle($pageTitle);
    $page->setIdentifier($pageIdentifier);
    $page->setStores($pageStores);
    $page->setIsActive($pageIsActive);
    $page->setUnderVersionControl($pageUnderVersionControl);
    $page->setContentHeading($pageContentHeading);
    $page->setContent($pageContent);
    $page->setRootTemplate($pageRootTemplate);
    $page->setLayoutUpdateXml($pageLayoutUpdateXml);
    $page->setCustomLayoutUpdateXml($pageCustomLayoutUpdateXML);
    $page->setMetaKeywords($pageMetaKeywords);
    $page->setMetaDescription($pageMetaDescription);
    $page->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================

    //==========================================================================
    // newsletter-archive en th Page
    //==========================================================================
    $pageTitle = "Moxy's Newsletter จดหมายข่าวจาก Moxyst.com";
    $pageIdentifier = "newsletter-archive";
    $pageStores = array("$storeIdMoxyEn", "$storeIdMoxyTh");
    $pageIsActive = 1;
    $pageUnderVersionControl = 0;
    $pageContentHeading = "Moxy's Newsletter จดหมายข่าวจาก Moxyst.com";
    $pageContent = <<<EOD
<p>&nbsp;</p>
EOD;

    $pageRootTemplate = 'one_column';
    $pageLayoutUpdateXml = <<<EOD
EOD;


    $pageCustomLayoutUpdateXML = <<<EOD
EOD;

    $pageMetaKeywords = "";
    $pageMetaDescription = "";

    $page = Mage::getModel('cms/page')->getCollection()
        ->addStoreFilter(array("$storeIdMoxyEn", "$storeIdMoxyTh"), $withAdmin = true)
        ->addFieldToFilter('identifier', $pageIdentifier)
        ->getFirstItem();
    if ($page->getId() == 0) {
        $page = Mage::getModel('cms/page');
    }
    else
    {
        // if exists then repair
        $page = Mage::getModel('cms/page')->load($page->getId());
    }

    $page->setTitle($pageTitle);
    $page->setIdentifier($pageIdentifier);
    $page->setStores($pageStores);
    $page->setIsActive($pageIsActive);
    $page->setUnderVersionControl($pageUnderVersionControl);
    $page->setContentHeading($pageContentHeading);
    $page->setContent($pageContent);
    $page->setRootTemplate($pageRootTemplate);
    $page->setLayoutUpdateXml($pageLayoutUpdateXml);
    $page->setCustomLayoutUpdateXml($pageCustomLayoutUpdateXML);
    $page->setMetaKeywords($pageMetaKeywords);
    $page->setMetaDescription($pageMetaDescription);
    $page->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================

    //==========================================================================
    // Help en Page
    //==========================================================================
    $pageTitle = "Help";
    $pageIdentifier = "help";
    $pageStores = array($storeIdMoxyEn);
    $pageIsActive = 1;
    $pageUnderVersionControl = 0;
    $pageContentHeading = "Help";
    $pageContent = <<<EOD
<div>
<ul id="helpMenu">
<li><a href="javascript:void(0)" data-id="faq">FAQ</a></li>
<li><a href="javascript:void(0)" data-id="howtoorder">How To Order</a></li>
<li><a href="javascript:void(0)" data-id="payments">Payments</a></li>
<li><a href="javascript:void(0)" data-id="shipping">Shipping</a></li>
<li><a href="javascript:void(0)" data-id="rewardpoints">Reward Points</a></li>
<li><a href="javascript:void(0)" data-id="returns">Cancellation and Returns</a></li>
</ul>
</div>
<div class="helpContent">
<div id="faq" class="subcontent">
<h2>FAQ</h2>
<ol class="accordion">
<li>
<div class="accord-header">Who is Sanoga?</div>
<div class="accord-content">Sanoga is an online health store with a mission to offer what&rsquo;s best for your health and deliver it straight to you. We focus on a broad selection of health essentials, great logistics, and customer service to impress our local pet community. Our goal is to help you save time from travelling, popping in and out of health store to buy your health essentials. At Sanoga, we do the legwork for you. Our teams of health experts are all dedicated to you and your health.</div>
</li>
<li>
<div class="accord-header">How did Sanoga come about?</div>
<div class="accord-content">Sanoga aims to make looking after your health easier. As health experts, we want convenience in buying the best quality products without all the hassles. We decided to combine our passion with our vast ecommerce knowledge to offer health-conscious community a great service.</div>
</li>
<li>
<div class="accord-header">Where is Sanoga based?</div>
<div class="accord-content">We are based at<br /> Sethiwan Tower - 19th Floor<br /> 139 Pan Road, Silom, Bangrak,<br /> Bangkok, 10500 Thailand<br /> Tel: 02-106-8222 <br /> Email <a href="mailto:support@sanoga.com">support@sanoga.com</a></div>
</li>
<li>
<div class="accord-header">If I were interested in partnering with Sanoga, how would I go about it?</div>
<div class="accord-content">The company welcomes any and all business ventures and ideas that you might have that will ultimately add value to our members. To discuss this further, please contact us on <a href="mailto:support@sanoga.com">support@sanoga.com</a></div>
</li>
<li>
<div class="accord-header">Free Shipping Promotion</div>
<div class="accord-content">Free Delivery anywhere in Bangkok with the minimun purchase of 500 baht for each order
<ul style="margin-left: 30px; list-style: square !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">฿100 flat fee for shipping nationwide exclude Bangkok</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Flat shipping fee of ฿100 will be applied to the order less than ฿500 in Bangkok and ฿200 for nationwide exclude Bangkok</li>
</ul>
</div>
</li>
<li>
<div class="accord-header">I forgot my password</div>
<div class="accord-content">If you have forgotten your password, don&rsquo;t worry. We have systems in place to help you recover them. <a href="{{store url='forgot-password'}}" target="_blank">Click here</a> for more information.</div>
</li>
<li>
<div class="accord-header">The item I want is out of stock. What now?</div>
<div class="accord-content">Although we try to maintain a stock level that matches expected demand, highly popular items may sometimes be out of stock. Please contact our Customer Service team, who will quickly find a solution for you. Happy and healthy customers are important to us.</div>
</li>
<li>
<div class="accord-header">Can I place an item on hold for purchase at a later date?</div>
<div class="accord-content">Sorry, but we do not offer this option. Our goal is to ensure all customers can immediately purchase our great products.</div>
</li>
<li>
<div class="accord-header">How do I know if you carry a certain brand?</div>
<div class="accord-content">You can use the search function, which is available on the top right of the page or go through our brands list buy clicking on either dog or cat.</div>
</li>
<li>
<div class="accord-header">When can I expect new products to be listed on your website?</div>
<div class="accord-content">We constantly update our products with new lines and new brands. Please visit our website to see the latest items we have in stock.</div>
</li>
<li class="last">
<div class="accord-header">Is there somewhere I can go to view the product prior to purchasing?</div>
<div class="accord-content">Unfortunately, we do not have a retail store and for safety reasons we cannot allow customers in the warehouse. If you have any questions regarding the products, please do not hesitate to contact our Customer Service team.</div>
</li>
</ol></div>
<div id="howtoorder" class="subcontent">
<h2>How To Order</h2>
<ol class="accordion">
<li>
<div class="accord-header">How do I order?</div>
<div class="accord-content">Ordering at Sanoga.com is very easy. The steps can be found <a href="{{store url='how-to-order'}}" target="_blank">here</a>.</div>
</li>
<li>
<div class="accord-header">How do I register a new account?</div>
<div class="accord-content">Please click on 'Register', which is located on the top right hand side of the page. You will then be prompted to a new page where you will find 'New Customer' on the left hand side. Fill in your details as requested before clicking 'submit'. Registration should now be complete and you should receive a confirmation e-mail to the address you registered with.</div>
</li>
<li>
<div class="accord-header">Is there any registration fee for Sanoga?</div>
<div class="accord-content">Registration at Sanoga is completely free and easy! Any form of charge will only come from purchasing of Sanoga products.</div>
</li>
<li>
<div class="accord-header">I need personal assistance with my order. Who can I contact?</div>
<div class="accord-content">Our Customer Service team will be happy to assist you. Please e-mail us at <a href="mailto:support@sanoga.com">support@sanoga.com</a> or call us at 02-106-8222.</div>
</li>
<li class="last">
<div class="accord-header">Do you take orders over the phone?</div>
<div class="accord-content">Yes. We do take orders over phone. The payment mode possible for these orders is Cash On Delivery only. Click here for more details.</div>
</li>
</ol></div>
<div id="payments" class="subcontent">
<h2>Payments</h2>
<ol class="accordion">
<li>
<div class="accord-header">Is my credit card information safe at Sanoga?</div>
<div class="accord-content">The company will not store any credit card information on our servers. At Sanoga, we use systems set in place by banks that have the security protocols set in place to handle safe credit card transactions.</div>
</li>
<li>
<div class="accord-header">My credit card details are not being accepted. What's wrong?</div>
<div class="accord-content">Please check with your bank or financial institution to rule out errors on their behalf. If problems continue to persist, please contact Customer Service who will notify the IT department of technical difficulties.</div>
</li>
<li>
<div class="accord-header">My computer froze while processing payment. How will I know that my payment went through successfully?</div>
<div class="accord-content">All successful transactions will receive a confirmation email that contains an order tracking number. If you have not received confirmation via email, please try placing your order again. Alternatively, please contact our Customer Service team to confirm the placement of your order.</div>
</li>
<li>
<div class="accord-header">How many payment methods does Sanoga have?</div>
<div class="accord-content">At Sanoga, we provide customers the ability to pay using the following facilities:
<ul style="margin-left: 30px; list-style: square !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Credit card and Debit card</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">PayPal</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Cash on delivery (pay when we deliver the product)</li>
</ul>
<!-- <strong>Payment Fee</strong>
				<ul style="margin-left: 20px; list-style: square !important; margin-top: 7px;">
					<li style="border-bottom: 0; padding: 5px; font-weight: normal;"><strong>
						Counter Service</strong>
						<br />
						Payments can be done through counter services at Seven Eleven. The conditions are as follows; Purchases that exceed 500 baht will be charged with a 15 baht fee. As for orders below 500 baht, 25 baht will be collected for this service. After the payment is completed, we will send you a confirmation email for your orders.
					</li>
					<li style="border-bottom: 0; padding: 5px; font-weight: normal;">
						<strong>Tesco Lotus Check Out Counter</strong>
						<br />
						Payment can be done through Tesco Lotus Check Out Counters in all Tesco Lotus stores. A payment form is be printed out and then used for payment at any check out counters of Tesco Lotus with a fee of 7 baht. After the payment is completed, we will send you a confirmation email for your orders.
					</li>
					<li style="border-bottom: 0; padding: 5px; font-weight: normal;">
						<strong>Just pay</strong>
						<br />
						Payment can be completed at TOT service centers that displays a Just pay system that allows you to make payments with an additional fee of 15 baht. A payment form can be printed and presented at the cashier in every TOT service counters throughtout Thailand. After the payment is completed, we will send you a confirmation email for your orders.
					</li>
					<li style="border-bottom: 0; padding: 5px; font-weight: normal;">
						<strong>True Money Express</strong>
						<br />
						Payment can be purchased at every branch of True Money Express. A service fee of 10 baht will be acquired in order complete your payment. Print your payment form and the form is given to the cashier to complete the payment at any True Money Express throughout Thailand. After the payment is completed, we will send you a confirmation email for your orders.
					</li>
					<li style="border-bottom: 0; padding: 5px; font-weight: normal;">
						<strong>Pay@post</strong>
						<br />
						Payment can be done at every post office in Thailand. To use this service, an additional fee of 10 baht will be charged. You can present your payment form at any Post office cashier available in your area. After the payment is completed, we will send you a confirmation email for your orders.
					</li>
					<li style="border-bottom: 0; padding: 5px; font-weight: normal;">
						<strong>Bangkok Bank</strong>
						<br />
						You can complete your payment through an ATM, Bualuang i-banking service, by charging 10 baht for areas in and close to Bangkok. For other provices, a 20 baht fee will be charged. If you pay at the bank's counter service, the fee will be 15 baht for areas in and close to Bankok, and 30 baht for places outside Bangkok.
					</li>
					<li style="border-bottom: 0; padding: 5px; font-weight: normal;">
						<strong>Kasikorn Bank</strong>
						<br />
						You can pay via ATM, K-cyber Banking, and bank counter services. A fee of 10 baht will be charged in areas within Bangkok and 20 baht for areas outside of Bangkok.
					</li>
					<li style="border-bottom: 0; padding: 5px; font-weight: normal;">
						<strong>Siam Commercial Bank</strong>
						<br />
						Payments can be done through an ATM, SCB Easy Net, and bank counters. An additional fee of 15 baht is required for all areas of delivery.
					</li>
					<li style="border-bottom: 0; padding: 5px; font-weight: normal;">
						<strong>TMB</strong>
						<br />
						Payment can be completed through an ATM account,
					</li>
					<li style="border-bottom: 0; padding: 5px; font-weight: normal;">
						<strong>Krungthai Bank</strong>
						<br />
						You can pay via ATM, KTB Online, and at Bank counters. The bank will charge 10 baht extra for areas in and near Bangkok, and 15 baht extra for areas outside Bangkok.
					</li>
					<li style="border-bottom: 0; padding: 5px; font-weight: normal;">
						<strong>Krungsri Bank</strong>
						<br />
						Payments can be done through an ATM, Krungsri online, and bank counters. An additional fee of 15 baht is requred for areas in and near Bangkok, as for other provinces 30 baht is required.
					</li>
					<li style="border-bottom: 0; padding: 5px; font-weight: normal;">
						<strong>UOB</strong>
						<br />
						Payments can be completed through an ATM, UOB Cybe Banking, and bank counters. An extra 15 baht will be charged for purchases in all areas over Thailand.
					</li>
				</ul>
				* Please pay within 48 hours after recieving your order confirmation email, your order will automatically expire if not paid within the specified period.
				<br />
				* Fees are subjected to change by the terms and conditions of the service provider of each payment type. --></div>
</li>
<li>
<div class="accord-header">Are your prices in Thai Baht (THB)?</div>
<div class="accord-content">All pricing is in Thai Baht.</div>
</li>
<li class="last">
<div class="accord-header">Does your prices include taxes?</div>
<div class="accord-content">All prices are inclusive of taxes.</div>
</li>
</ol></div>
<div id="shipping" class="subcontent">
<h2>Shipping</h2>
<ol class="accordion">
<li>
<div class="accord-header">When will my order ship?</div>
<div class="accord-content">All items are shipped within 24 hours after the purchase is made and most of the time even sooner (weekdays only). All Sanoga deliveries are sent via Premium Courier. The deliveries are made between 9:00 am - 7:00 pm. Currently, we deliver from Monday to Saturday.</div>
</li>
<li>
<div class="accord-header">How long will it take until I receive my order?</div>
<div class="accord-content">We ship via our own delivery fleet and several other premium carriers. If you order by 4 pm on a business day, you will receive it in 2-3 business days. We do not yet guaranteed overnight shipping, although many areas of the country will receive their orders the next day. <br /><br /> * For Sangkhlaburi district, islands and provinces including Pattani, Yala, Naradhiwas, Mae Hong Son and Chiang Rai, delivery may take 1-2 days longer due to difficult routes. We&rsquo;ll try our best to deliver the product to you as soon as possible.</div>
</li>
<li>
<div class="accord-header">How do I track my order?</div>
<div class="accord-content">When your order ships, you will receive an email with the shipping and tracking information. You can use your tracking number on our website to trace your order. You can also find your tracking number with your order details; Click the "Order Status" button under the "My Account" menu after you have logged on. <br /><br /> Please check with your neighbors or building manager if you have not received your product even though the tracking says that it has been delivered. If the tracking information doesn't show up or shows that the item has been returned to us, please contact us by email or phone. It is possible that we may have the wrong shipping address, the driver may have unsuccessfully attempted to deliver, or the shipping carrier may have misplaced the product.</div>
</li>
<li>
<div class="accord-header">What happens if I am not at home when my order is being delivered?</div>
<div class="accord-content">In the process of delivering the product to you, our driver will contact you on the number provided to ensure that you are at the address that you have given to us. If you are not home at the time, do inform someone else in your household to beaware of our delivery so they can sign for the product on your behalf. IIf we are still unable to deliver the product to you despite our best efforts, the money paid for the product will be returned to you, but not the shipping fee (For more detail please check our &ldquo;<a href="{{store url='return-policy'}}" target="_blank">refund policy</a>&rdquo;). Undeliverable orders will not be resent and you must place a new order.</div>
</li>
<li>
<div class="accord-header">Can I have my order shipped to my office?</div>
<div class="accord-content">We are happy to delivery to your office. If you would like us to do that, please input your office address at checkout. Your office delivery will be made from the 9am to 7pm on weekdays and 9am to 12 noon on weekends.</div>
</li>
<li>
<div class="accord-header">How much is the shipping fee?</div>
<div class="accord-content">Free Delivery anywhere in Bangkok with the minimun purchase of 500 baht for each order
<ul style="margin-left: 30px; list-style: square !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">฿100 flat fee for shipping nationwide exclude Bangkok</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Flat shipping fee of ฿100 will be applied to the order less than ฿500 in Bangkok and ฿200 for nationwide exclude Bangkok</li>
</ul>
</div>
</li>
<li>
<div class="accord-header">Do you ship outside of Thailand?</div>
<div class="accord-content">Sorry, we currently do not ship outside of Thailand.</div>
</li>
<li class="last">
<div class="accord-header">Why is my shipment delayed?</div>
<div class="accord-content">You must order by 4 p.m. for 1-2-day shipping to apply. We try our best to make sure that you receive your order as soon as possible, however, there are some situations beyond our control that can delay your shipment.<br />The following orders may result in delayed shipping:
<ul style="margin-left: 30px; list-style: square !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Incorrect shipping address</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Incorrect Mobile Phone number</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Unable to reach you at provided contact number</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Being absent after courier made an appointment</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Shipping address does not match billing address</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Payment delay or issue</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Courier failure to deliver (e.g. severe weather conditions)</li>
</ul>
</div>
</li>
</ol></div>
<div id="rewardpoints" class="subcontent">
<h2>Reward Points</h2>
<ol class="accordion">
<li>
<div class="accord-header">1% Cash-back policy</div>
<div class="accord-content">A 1% cash back means that for every purchase made, you will receive 1% cash-back, which will be stored in your Sanoga account. You can then use this credit as a discount on your next purchase.</div>
</li>
<li>
<div class="accord-header">How do I get points for referring a friend?</div>
<div class="accord-content">There are many ways where you can get ฿100 by <a href="{{store url='rewardpoints/index/referral'}}" target="_blank">referring a friend</a>at Sanoga.com
<ul style="margin-left: 30px; list-style: square !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">You can share your unique link which can be found in My Accounts straight to your Facebook or Twitter page. You can also email this link straight to your friends.</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Once your friends have clicked on the link and have made a purchase on Sanoga, you will automatically be credited with ฿100 in your account in the form of credits.</li>
</ul>
</div>
</li>
<li class="last">
<div class="accord-header">Reward Points Terms &amp; Conditions</div>
<div class="accord-content">Each time you shop on our website, you will earn Reward Points on your purchases. The Reward Points will be accumulated each time you purchase the products on our website and it will be expired within 6 months from your purchasing date. <br /><br /> For example, if you earn 20 points on January from your first purchase and another 30 points on March, your Reward Point balance is 50 points. On July, your balance will be deducted to 30 points because 20 points on January are already expired. In case you have not used your point within September, you point balance will become 0 as your points earned previously on March are expired. <br /><br /> For your own interest, please kindly redeem your Reward Points in the available period of time.</div>
</li>
</ol></div>
<div id="returns" class="subcontent">
		<h2>Cancellation and Returns</h2>
		<ol class="accordion">
			<li>
			<div class="accord-header"><span></span>How do I return my purchase?</div>
			<div class="accord-content">
				<strong>Returns are FREE, fast and easy!</strong><br/>
				If you wish to return an item, please contact our customer care and request a return of the product(s) via phone 02-106-8222 or Email <a href="mailto:support@sanoga.com">support@sanoga.com</a>.  If the conditions of the returned product aligned within our return policy, please fill in the Returns Slip you received with your shipment, enclose the invoice in the package then send it back to us in its original packaging. Please send the product back to Sanoga.com at any Thailand Post location nearby your premise under domestic registered mail service.
				 <br/><br/>
				NOTE: Sorry for any inconveniences may cause you, return shipments are not eligible for scheduled pick-ups by Sanoga.com.
				 <br/><br/>

				Our address is:<br/>
				Acommerce Co.,Ltd<br/>
8/2 Rama3 Rd, Soi 53, <br/>Bang Phong Pang, Yarn-Nava, <br/>Bangkok 10120<br/><br/>


				Sanoga.com will absorb the incurred shipping costs in this registered returns back to you on the refund process. We ask that you keep the receipt/slip of this shipping cost issued by the post office as an evidence. And please send your receipt/slip back to Customer Care by submitting a photo file to the email <a href="mailto:support@sanoga.com">support@sanoga.com</a>

			</div>
			</li>
			<li>
			<div class="accord-header"><span></span>Can I cancel my order?</div>
			<div class="accord-content">
				If you wish to cancel your order, get in touch with customer service as soon as possible with your order number.
If your order has been dispatched, please do not accept delivery and contact customer service. In case of Prepaid orders Refunds will be processed as per our Return policies. Any cashback credits used in the purchase of the order will be credited back to your account.
			</div>
			</li>
			<li>
			<div class="accord-header"><span></span>Packing your return</div>
			<div class="accord-content">
				We suggest that you use the boxes that the products arrived in. Simply apply the shipping label acquired through the online return process or from our Customer Service team. Please put the return label to your box/package and drop it off at any Thailand Post location. NOTE: Return shipments are not eligible for scheduled pick-ups by Sanoga. Please contact us with any questions or concerns at 02-106-8222 or send an e-mail to <a href="mailto:support@sanoga.com">support@sanoga.com</a>
			</div>
			</li>
			<li>
			<div class="accord-header"><span></span>How long will it take for my return to be processed?</div>
			<div class="accord-content">
				Once we receive the item(s), we will process your request within 14 business days.
			</div>
			</li>
			<li>
			<div class="accord-header"><span></span>When do I receive my refund?</div>
			<div class="accord-content">
				Once we receive your item(s) and have checked that it is in original condition, a refund will be initiated immediately. Please allow up to 7 business days to receive your refund from the time we receive your return. Sanoga will refund your money through Bank Transfer or in store credit, if preferred.
			</div>
			</li>
			<li>
			<div class="accord-header"><span></span>What are the shipping charges if I return my purchased items?</div>
			<div class="accord-content">
				The shipping costs will vary according to your location and method of postage you choose. Please make sure to ship back to us via Thailand post by registered mail and we will refund the shipping fees after receiving the product.
			</div>
			</li>
			<li class="last">
			<div class="accord-header"><span></span>What if my item has a defect?</div>
			<div class="accord-content">
				Our Quality Control department tries to ensure that all products are of a high quality when they leave the warehouse. In the rare circumstance that your item has a defect, you may send it back to us with a completed Returns Slip attached within 30 days upon receiving the item(s), a full refund will be issued to you.
			</div>
			</li>
		</ol>
	</div>
</div>
<script type="text/javascript">// <![CDATA[
 jQuery(document).ready(function()
 {
  var tmpUrl = "";
  var h=window.location.hash.substring(0);
  if(h == ""){
   //alert('Hash not found!!!');
   h = "#faq";
   //alert(heart);
   jQuery( "#helpMenu li:first-child" ).addClass( "helpactive");
   jQuery('.subcontent[id='+h.substring(1)+']').fadeIn();
  }else{
   jQuery('#helpMenu li a[data-id='+h.substring(1)+']').parent().addClass( "helpactive");
  jQuery('.subcontent[id='+h.substring(1)+']').fadeIn();
  }

  //////////////////////////////
  jQuery('#keyMess li').on('click','a',function()  {
   tmpUrl=jQuery(this).attr('href');
   h=tmpUrl.substring(tmpUrl.indexOf("#")+1);

   jQuery('.subcontent:visible').fadeOut(0);
   jQuery('.subcontent[id='+h+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
  });
  jQuery('#zyncNav li').on('click','a',function()
  {
   tmpUrl=jQuery(this).attr('href');
   h=tmpUrl.substring(tmpUrl.indexOf("#")+1);
   jQuery('.subcontent:visible').fadeOut(0);
   jQuery('.subcontent[id='+h+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
  });
  jQuery('#footerBanner div').on('click','a',function()
  {
   tmpUrl=jQuery(this).attr('href');
   h=tmpUrl.substring(tmpUrl.indexOf("#")+1);
   jQuery('.subcontent:visible').fadeOut(0);
   jQuery('.subcontent[id='+h+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
  });
  jQuery('#topMklink').on('click','a',function()
  {
   tmpUrl=jQuery(this).attr('href');
   h=tmpUrl.substring(tmpUrl.indexOf("#")+1);
   jQuery('.subcontent:visible').fadeOut(0);
   jQuery('.subcontent[id='+h+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
  });
  jQuery('#footer_help li').on('click','a',function()
  {
   tmpUrl=jQuery(this).attr('href');
   h=tmpUrl.substring(tmpUrl.indexOf("#")+1);
   jQuery('.subcontent:visible').fadeOut(0);
   jQuery('.subcontent[id='+h+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
  });
 ///////////////////////////////

     jQuery('#helpMenu').on('click','a',function()
     {
         jQuery('.subcontent:visible').fadeOut(0);
         jQuery('.subcontent[id='+jQuery(this).attr('data-id')+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery(this).parent().addClass( "helpactive");
     });

///////////////////////////////

     jQuery(".accordion li .accord-header").mouseenter(function() {
	    	jQuery(this).css("cursor","pointer");
	    });
	    jQuery(".accordion li .accord-header").click(function() {
	    	jQuery(this).find("span").removeClass('active');
	      if(jQuery(this).next("div").is(":visible")){
	        jQuery(this).next("div").slideUp(300);
	      } else {
	        jQuery(".accordion li .accord-content").slideUp(400);
	        jQuery(this).next("div").slideToggle(400);
	        jQuery(".accordion li .accord-header").find("span").removeClass('active');
	        jQuery(this).find("span").addClass('active');
	      }
	    });
	  });
// ]]></script>
EOD;

    $pageRootTemplate = 'one_column';
    $pageLayoutUpdateXml = <<<EOD
EOD;


    $pageCustomLayoutUpdateXML = <<<EOD
EOD;

    $pageMetaKeywords = "";
    $pageMetaDescription = "";

    $page = Mage::getModel('cms/page')->getCollection()
        ->addStoreFilter(array($storeIdMoxyEn), $withAdmin = true)
        ->addFieldToFilter('identifier', $pageIdentifier)
        ->getFirstItem();
    if ($page->getId() == 0) {
        $page = Mage::getModel('cms/page');
    }
    else
    {
        // if exists then repair
        $page = Mage::getModel('cms/page')->load($page->getId());
    }

    $page->setTitle($pageTitle);
    $page->setIdentifier($pageIdentifier);
    $page->setStores($pageStores);
    $page->setIsActive($pageIsActive);
    $page->setUnderVersionControl($pageUnderVersionControl);
    $page->setContentHeading($pageContentHeading);
    $page->setContent($pageContent);
    $page->setRootTemplate($pageRootTemplate);
    $page->setLayoutUpdateXml($pageLayoutUpdateXml);
    $page->setCustomLayoutUpdateXml($pageCustomLayoutUpdateXML);
    $page->setMetaKeywords($pageMetaKeywords);
    $page->setMetaDescription($pageMetaDescription);
    $page->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================

    //==========================================================================
    // Help th Page
    //==========================================================================
    $pageTitle = "วิธีการสั่งซื้อ และคำถามที่พบบ่อย";
    $pageIdentifier = "help";
    $pageStores = array($storeIdMoxyTh);
    $pageIsActive = 1;
    $pageUnderVersionControl = 0;
    $pageContentHeading = "ช่วยเหลือ";
    $pageContent = <<<EOD
<div>
<ul id="helpMenu">
<li><a href="javascript:void(0)" data-id="faq">คำถามที่พบบ่อย</a></li>
<li><a href="javascript:void(0)" data-id="howtoorder">วิธีการการสั่งซื้อ</a></li>
<li><a href="javascript:void(0)" data-id="payments">การชำระเงิน</a></li>
<li><a href="javascript:void(0)" data-id="shipping">การจัดส่งสินค้า</a></li>
<li><a href="javascript:void(0)" data-id="rewardpoints">คะแนนสะสม</a></li>
<li><a href="javascript:void(0)" data-id="returns">การคืนสินค้าและขอคืนเงิน</a></li></ul>
</div>
<div class="helpContent">
<div id="faq" class="subcontent">
<h2>FAQ</h2>
<ol class="accordion">
<li>
<div class="accord-header">Sanoga คืออะไร</div>
<div class="accord-content">Sanoga เป็นศูนย์รวมสินค้าเพื่อสุขภาพเพื่อสมาชิกทุกคนในครอบครัว โดยมีระบบที่ได้รับการพัฒนาเป็นอย่างดี ซึ่งจะช่วยให้ลูกค้าสามารถเลือกซื้อสินค้าได้ตลอด 24 ชั่วโมง ไม่เสียเวลาในการเดินทางและตามหาสินค้าที่ต้องการ</div>
</li>
<li>
<div class="accord-header">Sanoga มีที่มาอย่างไร</div>
<div class="accord-content">ที่ Sanoga เราเชื่อว่า การดูแลสุขภาพเป็นหนึ่งในสิ่งที่สำคัญที่สุดในชีวิต จึงไม่ควรที่จะทำให้เป็นเรื่องยาก ไหนจะต้องฝ่ารถติดเป็นชั่วโมงๆ เพื่อเดินทางไปซื้อ เดินเข้าร้านนู้นออกร้านนี้ เพียงเพื่อหาสินค้าที่ต้องการเพื่อสุขภาพของคุณ เราจึงมีไอเดียดีๆ ที่จะช่วยคุณประหยัดเวลาและลดความยุ่งยากโดยการจัดหาสิ่งที่คุณต้องการมาให้..โดยคุณไม่ต้องออกแรง เราเฟ้นหาสินค้าเพื่อสุขภาพที่ดีที่สุด พร้อมกับให้ข้อมูลเพื่อช่วยให้คุณตัดสินใจได้ว่าอะไรเหมาะกับคุณที่สุด เรามีสินค้าจากแบรนด์ที่เป็นที่รู้จักจากทั้งในและต่างประเทศ ทั้งยังสามารถส่งตรงถึงมือคุณได้ภายใน 1-2 วัน</div>
</li>
<li>
<div class="accord-header">Sanoga ตั้งอยู่ที่ไหน</div>
<div class="accord-content">บริษัทตั้งอยู่ที่ อาคารเศรษฐีวรรณ ชั้น 19 <br /> 139 ถนนปั้น แขวงสีลม เขตบางรัก <br /> กรุงเทพฯ ประเทศไทย 10500 <br /> โทรศัพท์: 02-106-8222<br /> อีเมล์ <a href="mailto:support@sanoga.com">support@sanoga.com</a></div>
</li>
<li>
<div class="accord-header">สนใจอยากร่วมธุรกิจกับ Sanoga ทำอย่างไร</div>
<div class="accord-content">บริษัทยินดีต้อนรับผู้ที่สนใจเข้าเป็นส่วนหนึ่งกับเราได้ โดยท่านที่สนใจนำเสนอสินค้าเพื่อสุขภาพ คุณภาพสูงผ่านทางหน้าเว็บไซต์ Sanoga สามารถติดต่อได้ที่อีเมล <a href="mailto:support@sanoga.com">support@sanoga.com</a></div>
</li>
<li>
<div class="accord-header">โปรโมชั่นจัดส่งสินค้าฟรี</div>
<div class="accord-content">จัดส่งฟรีเมื่อซื้อสินค้าขั้นต่ำ 500 บาทต่อการสั่งซื้อ1ครั้ง เฉพาะเขตกรุงเทพ ในส่วนของเขตปริมณฑล และต่างจังหวัด คิดค่าบริการในการจัดส่ง 100 บาท ในกรณีซื้อสินค้าต่ำกว่า 500 บาทเสียค่าจัดส่ง 100 บาทในเขตกรุงเทพ และ 200 บาทในเขตปริมณฑลและต่างจังหวัด</div>
</li>
<li>
<div class="accord-header">วิธีขอรหัสผ่านใหม่</div>
<div class="accord-content">ถ้าคุณลืมรหัสผ่านของคุณ คุณสามารถขอรหัสผ่านใหม่ได้ <a href="{{store url='forgot-password'}}" target="_blank">คลิกที่นี่</a>เพื่อดูรายละเอียดเพิ่มเติม</div>
</li>
<li>
<div class="accord-header">ถ้าสินค้าที่ต้องการหมด จะทำอย่างไร</div>
<div class="accord-content">ถึงแม้ว่าทางเราจะพยายามจัดหาสินค้าให้มีจำนวนเหมาะสมกับระดับความต้องการของลูกค้า แต่สินค้าที่ได้รับความนิยมอย่างมากก็อาจหมดลงได้อย่างรวดเร็ว แต่เราพยายามไม่ให้ปัญหานี้เกิดขึ้น หรือถ้าเกิดขึ้นก็จะพยายามหาทางแก้ไข และหาทางออกให้กับลูกค้าอย่างแน่นอน ถ้ามีข้อสงสัย หรือปัญหาเกี่ยวกับสินค้าสามารถติดต่อได้ที่ฝ่ายลูกค้าสัมพันธ์ได้ในเวลาทำการ</div>
</li>
<li>
<div class="accord-header">ฉันสามารถจองสินค้าไว้ก่อนเพื่อทำการสั่งซื้อในวันถัดไปได้หรือไม่</div>
<div class="accord-content">ต้องขออภัย ทางเราไม่มีนโยบายการจองสินค้า</div>
</li>
<li>
<div class="accord-header">ฉันจะทราบได้อย่างไรว่ามีแบรนด์ไหนที่ขายอยู่บนเว็บไซต์ Sanoga บ้าง</div>
<div class="accord-content">คุณสามารถค้นหาสินค้าที่ต้องการได้ โดยกดปุ่มค้นหาที่หน้าเว็บไซต์ Sanoga ซึ่งอยู่ด้านบนของหน้าเว็บไซต์ หรือคุณสามารถเข้าไปคลิกเลือกแบรนด์ต่าง ๆ ได้ บนแถบเมนู</div>
</li>
<li>
<div class="accord-header">จะทราบได้อย่างไร ว่ามีสินค้าใหม่เข้ามาในเวบไซต์</div>
<div class="accord-content">ทางเราจะทำการอัพเดทเวบไซต์ใหม่ทันที ที่มีสินค้าใหม่เข้ามาในคลังสินค้า ท่านสามารถตรวจสอบสินค้าใหม่ได้ที่เวบไซต์ของเรา</div>
</li>
<li class="last">
<div class="accord-header">ฉันสามารถแสดงความคิดเห็น หรือให้ข้อแนะนำเกี่ยวกับสินค้าได้อย่างไร</div>
<div class="accord-content">เรายินดีที่รับฟังความคิดเห็น และข้อเสนอแนะเกี่ยวกับผลิตภัณฑ์ของเรา คุณสามารถแสดงความคิดเห็นของคุณมาผ่านมางทางอีเมล <a href="mailto:support@sanoga.com">support@sanoga.com</a></div>
</li>
</ol></div>
<div id="howtoorder" class="subcontent">
<h2>วิธีการการสั่งซื้อ</h2>
<ol class="accordion">
<li>
<div class="accord-header">สั่งซื้อสินค้าผ่าน Sanoga อย่างไร?</div>
<div class="accord-content">คุณสามารถสั่งซื้อสินค้ากับ Sanoga.com ด้วยวิธีง่ายๆ <a href="{{store url='how-to-order'}}" target="_blank">คลิกที่นี่</a>เพื่อดูขั้นตอนการสั่งซื้อ</div>
</li>
<li>
<div class="accord-header">สมัครเพื่อลงทะเบียนบัญชีผู้ใช้ใหม่อย่างไร?</div>
<div class="accord-content">เพียงคลิกไปที่ &ldquo;ลงทะเบียน&rdquo; ซึ่งอยู่ด้านบนขวามือของเว็บไซต์ หลังจากนั้นจะเปลี่ยนไปหน้าสร้างบัญชีผู้ใช้ใหม่ซึ่งจะอยู่ทางด้านซ้ายมือ หลังจากกรอกรายละเอียดข้อมูลของท่านเสร็จเรียบร้อยแล้วคลิกที่ &ldquo;ยืนยัน&rdquo; เพื่อเสร็จสิ้นการสมัครสมาชิก หลังจากนั้นคุณจะได้รับอีเมลยืนยันการลงทะเบียนผู้ใช้ใหม่กับทาง Sanoga เพียงเท่านี้คุณก็สามารถซื้อสินค้าผ่านทางเราได้อย่างสะดวกสบาย</div>
</li>
<li>
<div class="accord-header">มีค่าธรรมเนียมในการสมัครสมาชิกกับ Sanoga หรือไม่</div>
<div class="accord-content">การสมัครสมาชิกไม่มีค่าธรรมเนียมใด ๆ ทั้งสิ้น และจะเกิดค่าใช้จ่ายในกรณีที่มีการสั่งซื้อสินค้าเท่านั้น</div>
</li>
<li>
<div class="accord-header">ถ้าฉันต้องการที่ปรึกษาส่วนตัวสำหรับการสั่งซื้อสามารถติดต่อได้ทางใด</div>
<div class="accord-content">แผนกลูกค้าสัมพันธ์ของเรายินดีเป็นอย่างยิ่งที่จะช่วยเหลือท่านผ่านทางอีเมล <a href="mailto:support@sanoga.com">support@sanoga.com</a> หรือทางหมายเลยโทรศัพท์ 02-106-8222</div>
</li>
<li class="last">
<div class="accord-header">ฉันสามารถสั่งซื้อสินค้าผ่านทางโทรศัพท์ได้หรือไม่?</div>
<div class="accord-content">คุณสามารถสั่งซื้อสินค้าผ่านทางโทรศัพท์ได้ค่ะ เพียงโทรเข้าไปที่แผนกลูกค้าสัมพันธ์ตามหมายเลขที่แจ้งไว้ และต้องชำระค่าสินค้าด้วยวิธีเก็บเงินปลายทางเท่านั้น คลิกที่นี่เพื่อดูรายละเอียดเพิ่มเติม</div>
</li>
</ol></div>
<div id="payments" class="subcontent">
<h2>การชำระเงิน</h2>
<ol class="accordion">
<li>
<div class="accord-header">การใช้บัตรเครดิตกับทาง Sanoga ปลอดภัยหรือไม่</div>
<div class="accord-content">บริษัทดูแลข้อมูลของสมาชิก Sanoga เป็นอย่างดี ข้อมูลทุกอย่างจะถูกเก็บไว้เป็นความลับและดูแลรักษาอย่างปลอดภัย ด้านข้อมูลเกี่ยวกับบัตรเครดิตทั้งหมดจะถูกส่งผ่านระบบ SSL (Secure Socket Layer) ซึ่งเป็นระบบรักษาความปลอดภัยมาตรฐานในการส่งข้อมูลทางอินเตอร์เน็ตตรงไปยังระบบเซฟอิเล็กทรอนิกส์ของธนาคาร โดยจะไม่มีการเก็บข้อมูลเกี่ยวกับบัตรเครดิตไว้ที่ระบบเซิร์ฟเวอร์ของเว็บไซต์แต่อย่างใด</div>
</li>
<li>
<div class="accord-header">ข้อมูลบัตรเครดิตไม่สามารถใช้ได้ เกิดจากอะไร</div>
<div class="accord-content">กรุณาตรวจสอบสถานะการเงินของท่านจากทางธนาคารหากยังไม่สามารถใช้ได้กรุณาติดต่อแผนกลูกค้าสัมพันธ์ของธนาคารเพื่อแจ้งกับแผนกเทคโนโลยีสารสนเทศเพื่อแก้ไขปัญหาด้านเทคนิค</div>
</li>
<li>
<div class="accord-header">หากเครื่องคอมพิวเตอร์ขัดข้องระหว่างขั้นตอนการชำระเงิน จะทราบได้อย่างไรว่าการชำระเงินเสร็จสิ้นเรียบร้อยแล้ว</div>
<div class="accord-content">เมื่อท่านชำระเงินเรียบร้อยแล้วคำยืนยันการสั่งซื้อจะถูกส่งไปยังอีเมลของท่านหากท่านไม่ได้รับการยืนยันการสั่งซื้อทางอีเมลกรุณาลองทำการสั่งซื้ออีกครั้ง หากท่านมีข้อสงสัยกรุณาติดต่อแผนกลูกค้าสัมพันธ์เพื่อสอบถามสถานะการสั่งซื้อของท่านที่ อีเมล <a href="mailto:support@sanoga.com">support@sanoga.com</a> หรือทางโทรศัพท์หมายเลข 02-106-8222</div>
</li>
<li>
<div class="accord-header">วิธีการชำระเงินมีกี่ช่องทาง</div>
<div class="accord-content">บริษัทมีช่องทางให้ลูกค้าเลือกชำระเงินหลายช่องทาง ตามด้านล่างนี้
<ul style="margin-left: 30px; list-style: square !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">บัตรเครดิต วีซ่า มาสเตอร์ ซึ่งผ่าน่ระบบ Payment ของบริษัท 2C2P</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">PayPal ช่วยให้คุณสามารถชำระค่าสินค้าของคุณหรือชำระด้วยบัตรเครดิตชั้นนำต่างๆ ผ่านทางหน้าเว็บไซด์ได้ทันที โดยจะทำรายการผ่านระบบของ PayPal ทำให้การชำระเงินเป็นไปได้อย่างสะดวก รวดเร็วและปลอดภัย หากท่านยังไม่มีบัญชีกับ Paypal สามารถ คลิกที่นี่เพื่อสมัครบริการ Paypal</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Cash on delivery เป็นการเก็บเงินปลายทางจากผู้รับสินค้า โดยจะมีค่าธรรมเนียมในการเก็บเงินปลายทาง 50 บาท ต่อการสั่งซื้อต่อครั้ง ค่าธรรมเนียมนี้ไม่รวมค่าสินค้า และค่าจัดส่งสินค้า</li>
</ul>
<!-- <strong>อัตราค่าธรรมเนียมแต่ละช่องทาง</strong>
<ul style="margin-left: 20px; list-style: square !important; margin-top: 7px;">
	<li style="border-bottom: 0; padding: 5px; font-weight: normal;">
		<strong>เคาท์เตอร์เซอร์วิส</strong>
		<br />
		ช่องทางการชำระเงินผ่านเคาท์เตอร์เซอร์วิสที่เซเว่นอีเลเว่น มีค่าธรรมเนียมดังนี้ สั่งซื้อสินค้า 500 บาทขึ้นไป เสียค่าธรรมเนียม 15 บาท สั่งซื้อสินค้าไม่ถึง 500 บาท เสียค่าธรรมเนียม 25 บาท หลังจากชำระเงินเรียบร้อยแล้ว ทางเราจะส่งใบยืนยันการสั่งซื้อไปยังอีเมล์ของคุณ
	</li>
	<li style="border-bottom: 0; padding: 5px; font-weight: normal;">
		<strong>จุดชำระเงินเทสโก้โลตัส</strong>
		<br />
		สามารถชำระเงินผ่านจุดชำระ เทสโก้โลตัส ทุกสาขาทั่วประเทศ ในอัตราค่าบริการเพียง 7 บาท* ท่านสามารถพิมพ์แบบฟอร์มการชำระเงินเพื่อนำไปชำระที่จุดชำระเงิน เทสโก้ โลตัส ทุกสาขาทั่วประเทศ หลังจากชำระเรียบร้อยแล้ว เราจะส่งใบยืนยันการสั่งซื้อไปยังอีเมล์ของท่าน
	</li>
	<li style="border-bottom: 0; padding: 5px; font-weight: normal;">
		<strong>Just pay</strong>
		<br />
		สามารถชำระที่ศูนย์บริการลูกค้า ทีโอที ที่มีสัญลักษณ์ Just pay เพื่อชำระค่าสินค้าโดยมีอัตราค่าบริการ 15 บาท* ท่านสามารถพิมพ์แบบฟอร์มการชำระเงินเพื่อนำไปชำระที่จุดชำระเงิน ทุกสาขาทั่วประเทศ หลังจากชำระเรียบร้อยแล้ว เราจะส่งใบยืนยันการสั่งซื้อไปยังอีเมล์ของท่าน
	</li>
	<li style="border-bottom: 0; padding: 5px; font-weight: normal;">
		<strong>True Money Express</strong>
		<br />
		สามารถชำระได้ที่ True Money Express ทุกสาขา เพื่อชำระค่าสินค้าโดยมีอัตราค่าบริการ 10 บาท* ท่านสามารถพิมพ์แบบฟอร์มการชำระเงินเพื่อนำไปชำระที่จุดชำระเงิน ทุกสาขาทั่วประเทศ หลังจากชำระเรียบร้อยแล้ว เราจะส่งใบยืนยันการสั่งซื้อไปยังอีเมล์ของท่าน
	</li>
	<li style="border-bottom: 0; padding: 5px; font-weight: normal;">
		<strong>Pay@post</strong>
		<br />
		สามารถชำระได้ที่ศูนย์บริการไปรษณีย์ไทยทุกสาขา เพื่อชำระค่าสินค้าโดยมีอัตราค่าบริการ 10 บาท* ท่านสามารถพิมพ์แบบฟอร์มการชำระเงินเพื่อนำไปชำระที่จุดชำระเงิน ทุกสาขาทั่วประเทศ หลังจากชำระเรียบร้อยแล้ว เราจะทำการส่งใบยืนยันการสั่งซื้อไปยังอีเมล์ของท่าน
	</li>
	<li style="border-bottom: 0; padding: 5px; font-weight: normal;">
		<strong>ธนาคารกรุงเทพ</strong>
		<br />
		ท่านสามารถชำระค่าสินค้าผ่าน ATM, บัวหลวง i-banking,โดยธนาคารจะเรียกเก็บค่าธรรมเนียมในพื้นที่ กรุงเทพ-ปริมณฑล 10 บาท พื้นที่ต่างจังหวัด 20 บาท* เคาท์เตอร์ธนาคาร มีค่าธรรมเนียมเรียกเก็บในพื้นที่กรุงเทพ-ปริมณฑล 15 บาท พื้นที่ต่างจังหวัด 30 บาท*
	</li>
	<li style="border-bottom: 0; padding: 5px; font-weight: normal;">
		<strong>ธนาคารกสิกร</strong>
		<br />
		ท่านสามารถชำระค่าสินค้าผ่าน ATM, K-cyber Banking, เคาท์เตอร์ธนาคาร โดยธนาคารจะเรียกเก็บค่าธรรมเนียมในพื้นที่ กรุงเทพ-ปริมณฑล 10 บาท พื้นที่ต่างจังหวัด 20 บาท*
	</li>
	<li style="border-bottom: 0; padding: 5px; font-weight: normal;">
		<strong>ธนาคารไทยพาณิชย์</strong>
		<br />
		ท่านสามารถชำระค่าสินค้าผ่าน ATM, SCB Easy Net, เคาท์เตอร์ธนาคาร โดยธนาคารจะเรียกเก็บค่าธรรมเนียมในพื้นที่ กรุงเทพ-ปริมณฑล 15 บาท พื้นที่ต่างจังหวัด 30 บาท*
	</li>
	<li style="border-bottom: 0; padding: 5px; font-weight: normal;">
		<strong>ธนาคารทหารไทย (TMB)</strong>
		<br />
		ท่านสามารถชำระค่าสินค้าผ่าน ATM, TMB Internet banking, เคาท์เตอร์ธนาคาร โดยธนาคารจะเรียกเก็บค่าธรรมเนียม 15 บาท ทุกพื้นที่*
	</li>
	<li style="border-bottom: 0; padding: 5px; font-weight: normal;">
		<strong>ธนาคารกรุงไทย</strong>
		<br />
		ท่านสามารถชำระค่าสินค้าผ่าน ATM, KTB online, เคาท์เตอร์ธนาคาร โดยธนาคารจะเรียกเก็บค่าธรรมเนียมในพื้นที่ กรุงเทพ-ปริมณฑล 10 บาท พื้นที่ต่างจังหวัด 15 บาท*
	</li>
	<li style="border-bottom: 0; padding: 5px; font-weight: normal;">
		<strong>ธนาคารกรุงศรีอยูธยา</strong>
		<br />
		ท่านสามารถชำระค่าสินค้าผ่าน ATM, Krungsri online, เคาท์เตอร์ธนาคาร โดยธนาคารจะเรียกเก็บค่าธรรมเนียมในพื้นที่ กรุงเทพ-ปริมณฑล 15 บาท พื้นที่ต่างจังหวัด 30 บาท*
	</li>
	<li style="border-bottom: 0; padding: 5px; font-weight: normal;">
		<strong>ธนาคารยูโอบี</strong>
		<br />
		ท่านสามารถชำระค่าสินค้าผ่าน ATM, UOB Cybe banking, เคาท์เตอร์ธนาคาร โดยธนาคารจะเรียกเก็บค่าธรรมเนียม 15 บาท ทุกพื้นที่*
	</li>
</ul>
* กรุณาชำระเงินภายใน 48 ชั่วโมงหลังจากที่ท่านได้รับอีเมล์ยืนยันคำสั่งซื้อคำสั่งซื้อของท่านจะหมดอายุลงอัตโนมัติหากท่านไม่ได้ชำระเงินภายในระยะเวลาที่กำหนด
<br />
* ค่าบริการอาจมีการเปลี่ยนแปลงตามข้อกำหนดและเงื่อนไขของผู้ให้บริการการชำระเงินแต่ละราย --></div>
</li>
<li>
<div class="accord-header">ราคาสินค้าใช้สกุลเงินไทยหรือไม่</div>
<div class="accord-content">ราคาสินค้าทุกชิ้นของเราใช้หน่วยเงินบาทไทย</div>
</li>
<li class="last">
<div class="accord-header">ราคาสินค้ารวมภาษีมูลค่าเพิ่มหรือยังค่ะ</div>
<div class="accord-content">ราคาสินค้าของเราได้รวมภาษีมูลค่าเพิ่มแล้ว</div>
</li>
</ol></div>
<div id="shipping" class="subcontent">
<h2>การจัดส่งสินค้า</h2>
<ol class="accordion">
<li>
<div class="accord-header">บริษัททำการจัดส่งสินค้าเมื่อไหร่</div>
<div class="accord-content">หากสินค้าที่คุณสั่งซื้อมีพร้อมในคลังของเรา บริษัทจะทำการจัดส่งสินค้าออกจากคลังสินค้าภายใน 24 ชั่วโมงหลังจากคำสั่งซื้อสินค้าสมบูรณ์เรียบร้อยแล้ว หรือในเวลาที่ไม่ช้าหรือเร็วไปกว่านั้น (เฉพาะในวันทำการ) สินค้าของ Sanoga ทุกรายการจัดส่งโดยระบบที่มีประสิทธิภาพสูง (Premium Courier) ซึ่งจะดำเนินการจัดส่งสินค้าในช่วงเวลา 9.00 น. &ndash; 19.00 น. และในปัจจุบันเราให้บริการจัดส่งสินค้าเฉพาะวันจันทร์ &ndash; วันเสาร์เท่านั้น</div>
</li>
<li>
<div class="accord-header">ระยะเวลาการจัดส่งสินค้านานเท่าไหร่</div>
<div class="accord-content">การจัดส่งสินค้าดำเนินการด้วยระบบของบริษัท ร่วมกับผู้ให้บริการจัดส่งสินค้า อื่น ๆ ซึ่งมีประสิทธิภาพสูง (Premium Courier) เพื่อให้เราแน่ใจได้ว่าสินค้าจะ จัดส่งถึงมือคุณอย่างรวดเร็วที่สุด หากคุณสั่งซื้อสินค้าภายในเวลา 16.00 น. สินค้าถึงมือคุณไม่เกิน 2-3 วันทำการเฉพาะในเขตกรุงเทพ และ 3-5 วันทำการ ในเขต ปริมณฑล และต่างจังหวัด ทั้งนี้บริษัทไม่อาจรับประกันได้ว่าหลังจากคุณสั่งซื้อ สินค้าแล้วจะได้รับสินค้าภายในวันถัดไปเลยทันที แม้ว่าการจัดส่งสินค้าไปยังหลาย พื้นที่ลูกค้าจะสามารถรับสินค้าได้ในวันรุ่งขึ้นก็ตาม และในบางพื้นที่อาจจะใช้ ระยะเวลาการจัดส่งที่นานกว่านั้นก็เป็นได้ <br /><br /> *เฉพาะ ปัตตานี ยะลา นราธิวาส แม่ฮ่องสอน เชียงราย อำเภอสังขระบุรี และบางพื้นที่ที่เป็นเกาะ อาจใช้เวลาในการจัดส่งมากกว่าปกติ 1-2 วันทำการ อย่างไรก็ตามทีมงานของเราจะจัดส่งถึงมือคุณอย่างเร็วที่สุด</div>
</li>
<li>
<div class="accord-header">วิธีการตรวจสอบและติดตามสถานะการจัดส่งสินค้า</div>
<div class="accord-content">เมื่อเราทำการจัดส่งสินค้า คุณจะได้รับอีเมลเพื่อยืนยันการจัดส่งสินค้าและแจ้งหมายเลขการจัดส่งสินค้า คุณสามารถใช้หมายเลขการจัดส่งสินค้าเพื่อตรวจสอบสถานะการจัดส่งได้ที่เว็บไซต์ของเรา (ตรวจสอบสถานะการจัดส่ง) หรือสามารถล็อกอินเพื่อดูสถานะการจัดส่งสินค้าได้ที่หน้าใบสั่งซื้อของคุณ โดยคลิกที่ปุ่ม &ldquo;สถานะสินค้า&rdquo; ในกรณีที่สถานะการจัดส่งสินค้าขึ้นว่าได้ &ldquo;ส่งมอบสินค้าแล้ว&rdquo; และคุณยังไม่ได้รับสินค้ากรุณาตรวจสอบกับเพื่อนบ้านหรือผู้ดูแลอาคารก่อนเบื้องต้น และหากสถานะการจัดส่งสินค้าขึ้นว่า &ldquo;ส่งคืนบริษัท&rdquo; กรุณาติดต่อเราทางอีเมลหรือโทรศัพท์ทันที เพราะอาจจะเกิดกรณีที่อยู่จัดส่งสินค้าไม่ถูกต้อง, เกิดเหตุสุดวิสัยที่ทำให้พนักงานไม่สามารถจัดส่งสินค้าได้ หรือสินค้าสูญหายระหว่างการจัดส่ง</div>
</li>
<li>
<div class="accord-header">กรณีที่พนักงานไปจัดส่งสินค้าแล้วไม่มีผู้รอรับสินค้าที่บ้าน</div>
<div class="accord-content">การจัดส่งสินค้าเมื่อไม่มีผู้รอรับสินค้าอยู่ที่บ้าน ขึ้นอยู่กับสถานการณ์และดุลยพินิจของพนักงานจัดส่งสินค้าว่าสามารถจะทำการวางสินค้าไว้ที่หน้าบ้านได้หรือไม่ ในกรณีที่พนักงานจัดส่งสินค้าเห็นว่าจำเป็นจะต้องมีผู้ลงชื่อรับสินค้า พนักงานจัดส่งสินค้าจะฝากสินค้าไว้ที่ผู้จัดการอาคารหรือพนักงานเฝ้าประตู โดยจะพิจารณาเลือกวิธีการที่ดีที่สุดอย่างใดอย่างหนึ่ง แต่หากไม่สามารถทำการจัดส่งสินค้าได้ สินค้าจะถูกตีคืนมาที่บริษัทซึ่งจะขึ้นสถานะการจัดส่งสินค้าว่า &ldquo;ไม่สามารถจัดส่ง&rdquo; คุณสามารถทำเรื่องขอคืนเงินค่าสินค้าเต็มจำนวน แต่ไม่รวมค่าจัดส่งสินค้าได้ (อ่านรายละเอียดได้ที่ <a href="{{store url='return-policy'}}" target="_blank">นโยบายการคืนสินค้า</a> ) ในกรณีที่คำสั่งซื้อถูกปฏิเสธอย่างไม่ถูกต้อง ทางบริษัทจะไม่ทำการจัดส่งสินค้ากลับไปอีกครั้ง คุณจะต้องดำเนินการสั่งซื้อใหม่เพื่อที่จะได้รับสินค้าที่คุณต้องการ</div>
</li>
<li>
<div class="accord-header">สามารถแจ้งส่งสินค้าไปยังที่สถานที่อื่นๆ ของคุณได้หรือไม่</div>
<div class="accord-content">แน่นอน เราทำการจัดส่งสินค้าในวันจันทร์- ศุกร์ เวลา 09.00 น. &ndash; 19.00 น. และวันเสาร์ เวลา 09.00 น. &ndash; 12.00 น. หากต้องการให้เราจัดส่งสินค้าไปยังที่ทำงานของคุณ สามารถกรอกที่อยู่ที่ต้องการให้เราจัดส่งในช่องสถานที่จัดส่งสินค้า หากเกิดเหตุสุดวิสัยที่คุณไม่สามารถอยู่รอรับสินค้าได้ด้วยตนเอง กรุณาตรวจสอบสินค้ากับทางพนักงานต้อนรับ, ผู้จัดการอาคาร, พนักงานเฝ้าประตูก่อนเบื้องต้น และเพื่อป้องกันความผิดพลาดในการนัดหมายรับสินค้า กรุณารอรับการติดต่อทางโทรศัพท์จากพนักงานของเราด้วย</div>
</li>
<li>
<div class="accord-header">ค่าบริการจัดส่งสินค้า</div>
<div class="accord-content">จัดส่งฟรีเมื่อซื้อสินค้าขั้นต่ำ 500 บาทต่อการสั่งซื้อ1ครั้ง เฉพาะเขตกรุงเทพ ในส่วนของเขตปริมณฑล และต่างจังหวัด คิดค่าบริการในการจัดส่ง 100 บาท ในกรณีซื้อสินค้าต่ำกว่า 500 บาทเสียค่าจัดส่ง 100 บาทในเขตกรุงเทพ และ 200 บาทในเขตปริมณฑลและต่างจังหวัด</div>
</li>
<li>
<div class="accord-header">การจัดส่งสินค้าไปยังต่างประเทศ</div>
<div class="accord-content">ขออภัย...ณ ตอนนี้บริษัทยังไม่มีบริการจัดส่งสินค้าไปยังต่างประเทศ แต่ในอนาคตเรามีแพลนที่จะเปิดร้านค้าออนไลน์ที่ขายอาหารสัตว์ทั่วภาคพื้นเอเชียตะวันออกเฉียงใต้</div>
</li>
<li class="last">
<div class="accord-header">ทำไมการจัดส่งสินค้าถึงล่าช้า</div>
<div class="accord-content">หากทำการสั่งซื้อสินค้าภายในเวลา 16.00 น. ของวันทำการปกติ คุณจะได้รับสินค้าหลังจากคำสั่งซื้อสมบูรณ์แล้วภายใน 2-3 วันทำการ ซึ่งเราจะพยายามจัดส่งสินค้าอย่างเร็วที่สุดเท่าที่จะเป็นไปได้ แต่ก็อาจมีบางกรณีที่จะทำให้คุณได้รับสินค้าล่าช้ากว่ากำหนด<br />สาเหตุที่อาจจะทำให้การจัดส่งสินค้าล่าช้า :
<ul style="margin-left: 30px; list-style: square !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">ข้อมูลสถานที่จัดส่งสินค้าไม่ถูกต้อง</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">ข้อมูลเบอร์โทรศัพท์ของลูกค้าไม่ถูกต้อง</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">ไม่สามารถติดต่อลูกค้าตามเบอร์โทรศัพท์ที่ให้ไว้ได้</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">ไม่มีคนอยู่รับสินค้าตามสถานที่ได้นัดหมายเอาไว้</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">แจ้งสถานที่จัดส่งสินค้าไม่เหมือนกับที่อยู่ตามใบสั่งซื้อ</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">ลูกค้าชำระเงินล่าช้าหรือมีข้อผิดพลาดในขั้นตอนการชำระเงิน</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">คำสั่งซื้อที่จำเป็นต้องหยุดชะงักหรือล่าช้า อันเนื่องมาจากการสั่งซื้อสินค้าในรายการใดรายการหนึ่งเป็นจำนวนมากทำให้สินค้าในคลังของเราไม่เพียงพอต่อการจำหน่าย (เป็นกรณีที่เกิดขึ้นไม่บ่อยนัก)</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">เหตุสุดวิสัยที่ทำให้การจัดส่งสินค้าไม่เป็นไปตามกำหนด (เช่น ภัยธรรมชาติหรือสภาพอากาศที่ผิดปกติ เป็นต้น)</li>
</ul>
</div>
</li>
</ol></div>
<div id="rewardpoints" class="subcontent">
<h2>คะแนนสะสม</h2>
<ol class="accordion">
<li>
<div class="accord-header">รับเงินคืน 1% เมื่อช็อปปิ้งกับ Sanoga</div>
<div class="accord-content">เมื่อท่านได้สั่งซื้อของกับทาง Sanoga ท่านจะได้รับเงินคืนทันที 1 % โดยเงินที่ได้จะถูกเก็บไว้ที่บัญชี Sanoga ของท่าน ท่านสามารถนำมาใช้ได้ในครั้งต่อไปเมื่อท่านสั่งซื้อของผ่านทาง Sanoga โดยสามารถใช้แทนเงินสด ทุกครั้งที่ท่านต้องการใช้ ท่านสามารถกรอกจำนวนเงินในช่อง ที่หน้าสั่งซื้อสินค้า ก่อนขั้นตอนสุดท้าย</div>
</li>
<li>
<div class="accord-header">คุณจะได้รับแต้มจากการแนะนำเว็บไซต์ให้กับเพื่อนๆ ได้อย่างไร</div>
<div class="accord-content">มีหลากหลายช่องทางที่คุณจะได้รับเครดิต 100 บาท จากการแนะนำเว็บไซต์ Sanoga.com ให้กับเพื่อนๆ <a href="{{store url='rewardpoints/index/referral'}}" target="_blank">อ่านรายละเอียดเพิ่มเติมที่นี่</a>
<ul style="margin-left: 30px; list-style: square !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">คุณสามารถนำลิงค์จากในหน้าบัญชีผู้ใช้ของคุณในเว็บไซต์ Sanoga.com แชร์ผ่านเฟสบุ๊ค ทวีทเตอร์ หรือส่งลิงค์ให้กับเพื่อนๆ ผ่านทางอีเมล</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">เมื่อเพื่อนของคุณคลิกลิงค์ดังกล่าวและซื้อสินค้าบนเว็บไซต์ Sanoga.com คุณจะได้รับทันทีเครดิต 100 บาทในบัญชีของคุณ เครดิตดังกล่าวสามารถใช้แทนเงินสดหรือใช้เป็นส่วนลดในการซื้อสินค้าครั้งต่อๆ ไป</li>
</ul>
</div>
</li>
<li class="last">
<div class="accord-header">อายุการใช้งานของคะแนนสะสม (Reward Points)</div>
<div class="accord-content">เมื่อคุณซื้อสินค้ากับทางเว็บไซต์ของเราคุณจะได้รับคะแนนสะสม (Reward Points) โดยคะแนนสะสมที่คุณได้รับจากการซื้อสินค้าแต่ละครั้งนั้นจะนำมารวมกันเมื่อคุณซื้อสินค้าในครั้งต่อไปเพิ่มขึ้นเรื่อยๆ แต่ถ้าคุณเก็บคะแนนสะสมไว้เป็นเวลานาน คะแนนบางส่วนของคุณอาจหายไปได้ เนื่องจากอายุการใช้งานของคะแนนสะสมจะมีระยะเวลาไม่เท่ากัน กล่าวคือ คะแนนที่คุณได้รับหลังจากการซื้อสินค้าแต่ละครั้งจะมีอายุการใช้งานนาน 6 เดือน <br /><br /> เช่น คุณซื้อสินค้าและได้คะแนนสะสมครั้งแรกในเดือนมกราคม จำนวน 20 คะแนน ต่อมาคุณซื้อสินค้าและได้คะแนนสะสมอีกครั้งในเดือนมีนาคม จำนวน 30 คะแนน คุณจะมีคะแนนสะสมรวมเป็น 50 คะแนน แต่เมื่อถึงเดือนกรกฎาคม คะแนนของคุณจะเหลือเพียง 30 คะแนน (เนื่องจาก 20 คะแนนที่ได้จากเดือนมกราคมหมดอายุในเดือนกรกฎาคม) และถ้าคุณยังไม่ได้ใช้คะแนนที่เหลืออีกภายในเดือนกันยายนคะแนนสะสมของคุณจะกลายเป็น 0 ทันที (เนื่องจาก 30 คะแนนที่ได้จากเดือนมีนาคมหมดอายุในเดือนกันยายน) <br /><br /> ดังนั้นเพื่อเป็นการรักษาสิทธิและผลประโยชน์ของคุณ จึงควรนำคะแนนสะสมมาใช้ภายในระยะเวลาที่กำหนด</div>
</li>
</ol></div>
<div id="returns" class="subcontent">
		<h2>การคืนสินค้าและขอคืนเงิน</h2>
		<ol class="accordion">
			<li>
			<div class="accord-header"><span></span>สามารถขอคืนสินค้าได้อย่างไร</div>
			<div class="accord-content">
				<strong>บริการคืนสินค้าอย่างง่ายดาย รวดเร็ว และไม่มีค่าใช้จ่าย!</strong><br/>
				กรณีที่คุณต้องการคืนสินค้า กรุณาติดต่อแผนกลูกค้าสัมพันธ์และแจ้งความประสงค์ในการขอคืนสินค้าผ่านทางโทรศัพท์ 02-106-8222 หรือ อีเมล์ <a href="mailto:support@sanoga.com">support@sanoga.com</a> หากเงื่อนไขการขอคืนสินค้าของคุณเป็นไปตามนโยบายการขอคืนสินค้าแล้ว กรุณากรอกรายละเอียดลงในแบบฟอร์มการขอคืนสินค้าที่คุณได้รับไปพร้อมกับสินค้าเมื่อตอนจัดส่ง และแนบใบกำกับภาษี (invoice) ใส่ลงในกล่องพัสดุของ Sanoga.com กลับมาพร้อมกันด้วย โดยจัดส่งสินค้ากลับมาให้ทางเรา ในแบบพัสดุลงทะเบียนผ่านทางที่ทำการไปรษณีย์ไทยใกล้บ้านคุณ
				 <br/><br/>
				กรุณาจัดส่งสินค้ามาที่ :<br/>
				บริษัท เอคอมเมิร์ซ จำกัด<br/>
				8/2 ถนนพระราม3 ซอย53<br/> แขวงบางโพงพาง เขตยานนาวา <br/>กรุงเทพฯ 10120

				 <br/><br/>

				ทาง Sanoga.com จะชดเชยค่าจัดส่งแบบลงทะเบียนพร้อมกับการคืนเงินค่าสินค้า โดยทางเราขอให้คุณเก็บใบเสร็จรับเงิน/slip ค่าจัดส่งที่ได้รับจากที่ทำการไปรษณีย์เพื่อเป็นหลักฐานในการขอคืนเงินค่าส่งสินค้ากลับไว้ด้วย กรุณาส่งใบเสร็จรับเงิน/slip นี้กลับมาทางแผนกลูกค้าสัมพันธ์ โดยส่งเป็นไฟล์รูปภาพกลับมาทางอีเมล์ <a href="mailto:support@sanoga.com">support@sanoga.com</a>
<br/><br/>
หมายเหตุ : ทาง Sanoga.com ต้องขออภัยในความไม่สะดวกที่เรายังไม่มีบริการในการเข้าไปรับสินค้าที่ลูกค้าต้องการส่งคืน

			</div>
			</li>
			<li>
			<div class="accord-header"><span></span>การแพ็คสินค้าเพื่อส่งคืนบริษัท</div>
			<div class="accord-content">
				เราแนะนำให้คุณทำการแพ็คสินค้าคืนด้วยกล่องหรือบรรจุภัณฑ์ลักษณะเดียวกับที่เราทำการจัดส่งไปให้ เว้นแต่ว่ามันไม่ได้อยู่ในสภาพที่ดีพอสำหรับการนำมาใช้ใหม่ อย่าลืม! ที่จะแนบป้าย/ฉลากขอคืนสินค้า (return label) กลับมาด้วย จากนั้นใช้ป้ายชื่อ/ที่อยู่ในการจัดส่งสินค้า (Shipping label)
				ที่ได้รับจากพนักงานหรือจากเว็บไซต์ของเราเพื่อทำการจัดส่งสินค้าคืนบริษัทได้ที่ทำการไปรษณีย์ทุกแห่งทั่วประเทศไทย
				<br/><br/>
หมายเหตุ : การจัดส่งสินค้ากลับคืนนั้น Sanoga ไม่ได้เป็นผู้กำหนดระยะเวลาในการจัดส่งสินค้า จึงไม่สามารถบอกได้ว่าสินค้าที่ส่งคืนนั้นจะมาถึงบริษัทเมื่อไหร่
<br/><br/>
หากมีข้อสงสัยกรุณาติดต่อสอบถามเราได้ที่ 02-106-8222 หรือ อีเมล์ <a href="mailto:support@sanoga.com">support@sanoga.com</a>
			</div>
			</li>
			<li>
			<div class="accord-header"><span></span>ใช้ระยะเวลาดำเนินการคืนสินค้านานเท่าไหร่</div>
			<div class="accord-content">
				เราจะใช้เวลาดำเนินการภายใน 14 วันทำการ นับตั้งแต่วันที่ได้รับสินค้าคืนมาเรียบร้อยแล้ว
			</div>
			</li>
			<li>
			<div class="accord-header"><span></span>เมื่อไหร่ถึงจะได้รับเงินค่าสินค้าคืน</div>
			<div class="accord-content">
				เมื่อบริษัทได้รับสินค้าที่ส่งคืนเรียบร้อยแล้ว ขั้นตอนการคืนเงินจะเกิดขึ้นโดยทันที เราจะดำเนินการคืนเงินค่าสินค้ากลับไปยังบัญชีเงินฝากของคุณ กรุณารอและตรวจสอบยอดเงินหลังจากที่เราได้รับสินค้าคืนจากคุณภายใน  7 วันทำการ
			</div>
			</li>
			<li>
			<div class="accord-header"><span></span>ค่าใช้จ่ายในการจัดส่งสินค้าคืนบริษัท</div>
			<div class="accord-content">
				ค่าใช้จ่ายในการจัดส่งสินค้าจะแตกต่างกันไปตามสถานที่และวิธีการจัดส่งที่คุณเลือก กรุณาตรวจสอบให้แน่ใจว่าคุณได้ใช้ระบบจัดส่งแบบลงทะเบียนซึ่งดำเนินการโดยไปรษณีย์ไทย และเราจะคืนเงินค่าจัดส่งหลังจากที่ได้รับสินค้าคืนและยืนยันว่าได้รับสินค้าคืนเรียบร้อยแล้ว
			</div>
			</li>
			<li class="last">
			<div class="accord-header"><span></span>กรณีที่การจัดส่งสินค้าเกิดข้อผิดพลาด</div>
			<div class="accord-content">
				แผนกควบคุมคุณภาพของเราทำงานอย่างหนักเพื่อให้สินค้าทุกชิ้นอยู่ในสภาพที่ดีและมีคุณภาพสูงสุดเมื่อทำการจัดส่งออกจากคลังสินค้า แต่ก็มีในบางกรณีซึ่งเป็นไปได้ยากที่สินค้าของคุณจะเกิดตำหนิ,เสียหาย หรือมีข้อผิดพลาด ดังนั้นหากเกิดกรณีดังกล่าวแล้วคุณสามารถดำเนินการขอคืนสินค้าและเงินค่าสินค้าเต็มจำนวน ภายในระยะเวลา 30 วันหลังจากที่คุณได้รับสินค้าเรียบร้อยแล้ว
			</div>
			</li>
		</ol>
	</div>
	</div>
<script type="text/javascript">// <![CDATA[
 jQuery(document).ready(function()
 {
  var tmpUrl = "";
  var h=window.location.hash.substring(0);
  if(h == ""){
   //alert('Hash not found!!!');
   h = "#faq";
   //alert(heart);
   jQuery( "#helpMenu li:first-child" ).addClass( "helpactive");
   jQuery('.subcontent[id='+h.substring(1)+']').fadeIn();
  }else{
   jQuery('#helpMenu li a[data-id='+h.substring(1)+']').parent().addClass( "helpactive");
  jQuery('.subcontent[id='+h.substring(1)+']').fadeIn();
  }

  //////////////////////////////
  jQuery('#keyMess li').on('click','a',function()  {
   tmpUrl=jQuery(this).attr('href');
   h=tmpUrl.substring(tmpUrl.indexOf("#")+1);

   jQuery('.subcontent:visible').fadeOut(0);
   jQuery('.subcontent[id='+h+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
  });
  jQuery('#zyncNav li').on('click','a',function()
  {
   tmpUrl=jQuery(this).attr('href');
   h=tmpUrl.substring(tmpUrl.indexOf("#")+1);
   jQuery('.subcontent:visible').fadeOut(0);
   jQuery('.subcontent[id='+h+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
  });
  jQuery('#footerBanner div').on('click','a',function()
  {
   tmpUrl=jQuery(this).attr('href');
   h=tmpUrl.substring(tmpUrl.indexOf("#")+1);
   jQuery('.subcontent:visible').fadeOut(0);
   jQuery('.subcontent[id='+h+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
  });
  jQuery('#topMklink').on('click','a',function()
  {
   tmpUrl=jQuery(this).attr('href');
   h=tmpUrl.substring(tmpUrl.indexOf("#")+1);
   jQuery('.subcontent:visible').fadeOut(0);
   jQuery('.subcontent[id='+h+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
  });
  jQuery('#footer_help li').on('click','a',function()
  {
   tmpUrl=jQuery(this).attr('href');
   h=tmpUrl.substring(tmpUrl.indexOf("#")+1);
   jQuery('.subcontent:visible').fadeOut(0);
   jQuery('.subcontent[id='+h+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
  });
 ///////////////////////////////

     jQuery('#helpMenu').on('click','a',function()
     {
         jQuery('.subcontent:visible').fadeOut(0);
         jQuery('.subcontent[id='+jQuery(this).attr('data-id')+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery(this).parent().addClass( "helpactive");
     });

///////////////////////////////

     jQuery(".accordion li .accord-header").mouseenter(function() {
	    	jQuery(this).css("cursor","pointer");
	    });
	    jQuery(".accordion li .accord-header").click(function() {
	    	jQuery(this).find("span").removeClass('active');
	      if(jQuery(this).next("div").is(":visible")){
	        jQuery(this).next("div").slideUp(300);
	      } else {
	        jQuery(".accordion li .accord-content").slideUp(400);
	        jQuery(this).next("div").slideToggle(400);
	        jQuery(".accordion li .accord-header").find("span").removeClass('active');
	        jQuery(this).find("span").addClass('active');
	      }
	    });
	  });
// ]]></script>
EOD;

    $pageRootTemplate = 'one_column';
    $pageLayoutUpdateXml = <<<EOD
EOD;


    $pageCustomLayoutUpdateXML = <<<EOD
EOD;

    $pageMetaKeywords = "";
    $pageMetaDescription = "คำถามที่พบบ่อย เกี่ยวกับวิธีการการสั่งซื้อ การชำระเงิน และการจัดส่งสินค้า ตลอดจนนโยบายของ Sanoga.com ในการให้บริการลูกค้า";

    $page = Mage::getModel('cms/page')->getCollection()
        ->addStoreFilter(array($storeIdMoxyTh), $withAdmin = true)
        ->addFieldToFilter('identifier', $pageIdentifier)
        ->getFirstItem();
    if ($page->getId() == 0) {
        $page = Mage::getModel('cms/page');
    }
    else
    {
        // if exists then repair
        $page = Mage::getModel('cms/page')->load($page->getId());
    }

    $page->setTitle($pageTitle);
    $page->setIdentifier($pageIdentifier);
    $page->setStores($pageStores);
    $page->setIsActive($pageIsActive);
    $page->setUnderVersionControl($pageUnderVersionControl);
    $page->setContentHeading($pageContentHeading);
    $page->setContent($pageContent);
    $page->setRootTemplate($pageRootTemplate);
    $page->setLayoutUpdateXml($pageLayoutUpdateXml);
    $page->setCustomLayoutUpdateXml($pageCustomLayoutUpdateXML);
    $page->setMetaKeywords($pageMetaKeywords);
    $page->setMetaDescription($pageMetaDescription);
    $page->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================

    //==========================================================================
    // Terms and Conditions EN Page
    //==========================================================================
    $pageTitle = "Terms and Conditions";
    $pageIdentifier = "terms-and-conditions";
    $pageStores = array($storeIdMoxyEn);
    $pageIsActive = 1;
    $pageUnderVersionControl = 0;
    $pageContentHeading = "Terms and Conditions";
    $pageContent = <<<EOD
<div style="font: normal 14px 'Tahoma'; line-height: 22px; color: #505050;"><span style="font: bold 16px 'Tahoma';">1. Introduction</span> <br />
<p>These are the terms and conditions of our agreement which apply to all purchases of products by you from sanoga.com, owned and operated by Whatsnew Inc., including subsidiaries and affiliates provide you, its Services (herein after referred as &ldquo;Website&rdquo;) under the following conditions. Please read the following terms carefully. If you do not agree to the following Terms &amp; Conditions you may not enter or use this Website. If you continue to browse and use this Website you are agreeing to comply with and be bound by the following terms and conditions of use, which together with our privacy policy governs Whatsnew and its use by you in relation to this Website.&nbsp;</p>
<p>&nbsp;</p>
<span style="font: bold 16px 'Tahoma';">2. Information and Product Description</span> <br />
<p>While every effort is made to update the information contained on this Website, neither Whatsnew nor any third party or data or content provider make any representations or warranties, whether express, implied in law or residual, as to the sequence, accuracy, completeness or reliability of information, opinions, any pricing information, research information, data and/or content contained on the Website (including but not limited to any information which may be provided by any third party or data or content providers) and shall not be bound in any manner by any information contained on the Website.&nbsp;</p>
<p>Whatsnew reserves the right at any time to change or discontinue without notice, any aspect or feature of this Website. No information shall be construed as advice and information is offered for information purposes only and is not intended for trading purposes. You and your company rely on the information contained on this Website at your own risk. If you find an error or omission at this site, please let us know.&nbsp;</p>
<p>&nbsp;</p>
<span style="font: bold 16px 'Tahoma';">3. Trademarks and Copyright</span> <br />
<p>The trade marks, names, logos and service marks (collectively "trademarks") displayed on this Website are all registered and unregistered trademarks of Whatsnew. Nothing contained on this Website should be construed as granting any license or right to use any trade mark without the prior written permission of Whatsnew.&nbsp;</p>
<p>&nbsp;</p>
<span style="font: bold 16px 'Tahoma';">4. External Links</span> <br />
<p>External links may be provided for your convenience, but they are beyond the control of Sanoga.com and no representation is made as to their content. Use or reliance on any external links and the content thereon provided is at your own risk. When visiting external links you must refer to the terms and conditions of use for that external Website. No hypertext links shall be created from any Website controlled by you or otherwise to this Website without the express prior written permission of Whatsnew. Please contact us if you would like to link to this Website or would like to request a link to your Website.&nbsp;</p>
<p>&nbsp;</p>
<span style="font: bold 16px 'Tahoma';">5. Public Forums and User Submissions</span> <br />
<p>Whatsnew is not responsible for any material submitted to the public areas by you (which include bulletin boards, hosted pages, chat rooms, or any other public area found on the Website. Any material (whether submitted by you or any other user) is not endorsed, reviewed or approved by Whatsnew. We reserve the right to remove any material submitted or posted by you in the public areas, without notice to you, if it becomes aware and determines, in its sole and absolute discretion that you are or there is the likelihood that you may, including but not limited to</p>
<table>
<tbody>
<tr>
<td width="15">5.1</td>
<td>defame, abuse, harass, stalk, threaten or otherwise violate the rights of other users or any third parties;</td>
</tr>
<tr>
<td>5.2</td>
<td>publish, post, distribute or disseminate any defamatory, obscene, indecent or unlawful material or information;</td>
</tr>
<tr>
<td>5.3</td>
<td>post or upload files that contain viruses, corrupted files or any other similar software or programs that may damage the operation of Whatsnew and/or a third party computer system and/or network;</td>
</tr>
<tr>
<td>5.4</td>
<td>violate any copyright, trade mark, other applicable Thailand or international laws or intellectual property rights of Whatsnew or any other third party;</td>
</tr>
<tr>
<td>5.5</td>
<td>submit content containing marketing or promotional material which is intended to solicit business.</td>
</tr>
</tbody>
</table>
<br /><br /> <span style="font: bold 16px 'Tahoma';">6. Membership</span> <br />
<p>Whatsnew Website is not available to users under the age of 18, outside the demographic target, or to any members previously banned by Whatsnew. Users are allowed only one active account. Breeching these conditions could result in account termination.&nbsp;</p>
<p>Whatsnew on occasion will offer its members promotions. In connection with these promotions, any user that receives Rewards (prizes, credits, gift cards, coupons or other benefits) from Whatsnew through the use of multiple accounts, email addresses, falsified information or fraudulent conduct, shall forfeit any Rewards gained through such activity and may be liable for civil and/or criminal penalties by law.&nbsp;</p>
<p>By using the Whatsnew Website, you acknowledge that you are of legal age to form a binding contract and are not a person barred from receiving services under the laws of the Thailand or other applicable jurisdiction. You agree to provide true and accurate information about yourself when requested by Whatsnew Website. If you provide any information that is untrue, inaccurate, or incomplete, Whatsnew has the right to suspend or terminate your account and refuse future use of its services. You are responsible for maintaining the confidentiality of your account and password, and accept all responsible for activities that occur under your account.&nbsp;</p>
<br /> <span style="font: bold 16px 'Tahoma';">7. Cancellation Due To Errors</span> <br />
<p>Whatsnew has the right to cancel an order at anytime due to typographical or unforeseen errors that results in the product(s) on the site being listed inaccurately (having the wrong price or descriptions etc.). In the event a cancellation occurs and payment for the order has been received, Whatsnew shall issue a full refund for the product in the amount in question.&nbsp;</p>
<p>&nbsp;</p>
<span style="font: bold 16px 'Tahoma';">8. Specific Use </span> <br />
<p>You further agree not to use the Website to send or post any message or material that is unlawful, harassing, defamatory, abusive, indecent, threatening, harmful, vulgar, obscene, sexually orientated, racially offensive, profane, pornographic or violates any applicable law and you hereby indemnify Whatsnew against any loss, liability, damage or expense of whatever nature which Whatsnew or any third party may suffer which is caused by or attributable to, whether directly or indirectly, your use of the Website to send or post any such message or material.&nbsp;</p>
<br /> <span style="font: bold 16px 'Tahoma';">9. Warranties</span> <br />
<p>Whatsnew makes no warranties, representations, statements or guarantees (whether express, implied in law or residual) regarding the Website, the information contained on the Website, you or your company's personal information or material and information transmitted over our system.&nbsp;</p>
<br /> <span style="font: bold 16px 'Tahoma';">10. Disclaimer</span> <br />
<p>Whatsnew shall not be responsible for and disclaims all liability for any loss, liability, damage (whether direct, indirect or consequential), personal injury or expense of any nature whatsoever which may be suffered by you or any third party (including your company). Along with result of or which may be attributable, directly or indirectly, to your access and use of the Website, any information contained on the Website, you or your company's personal information or material and information transmitted over our system. In particular, neither Whatsnew nor any third party or data or content provider shall be liable in any way to you or to any other person, firm or corporation whatsoever for any loss, liability, damage (whether direct or consequential), personal injury or expense of any nature whatsoever arising from any delays, inaccuracies, errors in, or omission of any share price information or the transmission thereof, or for any actions taken in reliance thereon or occasioned thereby or by reason of non-performance or interruption, or termination thereof.&nbsp;</p>
<p>The Whatsnew Referral Credits and Rewards Programs and its benefits are offered at the discretion of Whatsnew. Whatsnew has the right to modify or discontinue, temporarily or permanently, the services offered, including the point levels, in whole or in part for any reason, at its sole discretion, with or without prior notice to its members. Whatsnew may, among other things, withdraw, limit, or modify; increase and decrease the credits required for any Special Offer related to the Referral Credits and Rewards Programs. By becoming a member of Whatsnew, you agree that the Whatsnew Referral Credits and Rewards Programs will not be liable to you or any third-party for any modification or discontinuance of such Programs.&nbsp;</p>
<br /> <span style="font: bold 16px 'Tahoma';">11. Indemnity</span> <br />
<p>User agrees to indemnify and not hold Whatsnew (and its employees, directors, suppliers, subsidiaries, joint ventures, and legal partners) from any claim or demand. Including reasonable attorneys&rsquo; fees, from and against all losses, expenses, damages and costs resulting from any violation of these terms and conditions or any activity related to user&rsquo;s membership account due negligent or wrongful conduct.&nbsp;</p>
<br /> <span style="font: bold 16px 'Tahoma';">12. Use of the Website</span> <br />
<p>Whatsnew does not make any warranty or representation that information on the Website is appropriate for use in any jurisdiction (other than Kingdom of Thailand). By accessing the Website, you warrant and represent to Whatsnew that you are legally entitled to do so and to make use of information made available via the Website.&nbsp;</p>
<br /> <span style="font: bold 16px 'Tahoma';">13. General</span> <br />
<table>
<tbody>
<tr>
<td width="15">13.1</td>
<td>Entire Agreement . These Website terms and conditions constitute the sole record of the agreement between you and Whatsnew in relation to your use of the Website. Neither you nor Whatsnew shall be bound by any expressed or implied representation, warranty, or promise not recorded herein. Unless otherwise specifically stated, these Website terms and conditions supersede and replace all prior commitments, undertakings or representations, whether written or oral, between you and Whatsnew in respect of your use of the Website.</td>
</tr>
<tr>
<td>13.2</td>
<td>Alteration . Whatsnew may at any time modify any relevant terms and conditions, policies or notices. You acknowledge that by visiting the Website from time to time, you shall become bound to the current version of the relevant terms and conditions (the "current version") and, unless stated in the current version, all previous versions shall be superseded by the current version. You shall be responsible for reviewing the current version each time you visit the Website.</td>
</tr>
<tr>
<td>13.3</td>
<td>Conflict . Where any conflict or contradiction appears between the provisions of these Website terms and conditions and any other relevant terms and conditions, policies or notices, the other relevant terms and conditions, policies or notices which relate specifically to a particular section or module of the Website shall prevail in respect of your use of the relevant section or module of the Website.</td>
</tr>
<tr>
<td>13.4</td>
<td>Waiver . No indulgence or extension of time which either you or Whatsnew may grant to the other will constitute a waiver of or, whether by law or otherwise, limit any of the existing or future rights of the grantor in terms hereof, save in the event or to the extent that the grantor has signed a written document expressly waiving or limiting such rights.</td>
</tr>
<tr>
<td>13.5</td>
<td>Cession. Whatsnew shall be entitled to cede, assign and delegate all or any of its rights and obligations in terms of any relevant terms and conditions, policies and notices to any third party.</td>
</tr>
<tr>
<td>13.6</td>
<td>Severability . All provisions of any relevant terms and conditions, policies and notices are, notwithstanding the manner in which they have been grouped together or linked grammatically, severable from each other. Any provision of any relevant terms and conditions, policies and notices, which is or becomes unenforceable in any jurisdiction, whether due to void, invalidity, illegality, unlawfulness or for any reason whatever, shall, in such jurisdiction only and only to the extent that it is so unenforceable, be treated as pro non-script and the remaining provisions of any relevant terms and conditions, policies and notices shall remain in full force and effect.</td>
</tr>
<tr>
<td>13.7</td>
<td>Applicable laws. Any relevant terms and conditions, policies and notices shall be governed by and construed in accordance with the laws of Thailand without giving effect to any principles of conflict of law. You hereby consent to the exclusive jurisdiction of the High Court of Thailand in respect of any disputes arising in connection with the Website, or any relevant terms and conditions, policies and notices or any matter related to or in connection therewith.</td>
</tr>
<tr>
<td>13.8</td>
<td>Comments or Questions . If you have any questions, comments or concerns arising from the Website, the privacy policy or any other relevant terms and conditions, policies and notices or the way in which we are handling your personal information please <a href="{{store url='contacts'}}" target="_blank">contact us</a>.</td>
</tr>
</tbody>
</table>
<br /><br /> <span style="font: bold 16px 'Tahoma';">14. Termination</span> <br />
<p>These terms and conditions are applicable to you upon your accessing the Whatsnew Website and/or completing the registration or shopping process. These terms and conditions, or any of them, may be modified or terminated by Whatsnew without notice at any time for any reason. The provisions relating to Copyrights and Trademarks, Disclaimer, Claims, Limitation of Liability, Indemnification, Applicable Laws, Arbitration and General, shall survive any termination.&nbsp;</p>
<br /> <span style="font: bold 16px 'Tahoma';">15. Cancellations and Returns</span> <br />
<p>Orders can be cancelled if their products have not yet be delivered. Once products are delivered to the users account, they are no longer permitted to cancel. Any returns after that are handled on a case-by-case basis. Within reason, we will do what we can to ensure customer satisfaction. Unless there is something wrong with the purchase, we are generally unable to offer refunds.&nbsp;</p>
<br /> <span style="font: bold 16px 'Tahoma';">16. Credits</span> <br />
<p>The Whatsnew Referral Credits and Rewards Programs and its benefits are offered at the discretion of Whatsnew. Whatsnew has the right to modify or discontinue, temporarily or permanently, the services offered, including the point levels, in whole or in part for any reason, at its sole discretion, with or without prior notice to its members. Whatsnew may, among other things, withdraw, limit, or modify; increase and decrease the credits required for any Special Offer related to the Referral Credits and Rewards Programs. By becoming a member of Whatsnew, you agree that the Whatsnew Referral Credits and Rewards Programs will not be liable to you or any third-party for any modification or discontinuance of such Programs.&nbsp;</p>
<br /> <span style="font: bold 16px 'Tahoma';">17. Privacy </span> <br />
<p>For Delivery service, we are fully aware of our duty to keep all customers' information confidential. In addition to our duty of confidentiality to customers, we shall at all times fully observe the Personal Data (Privacy) Ordinance ("the Ordinance") in collecting, maintaining and using the personal data of customers. In particular, we observe the following principles, save otherwise appropriately agreed by the customers:&nbsp;</p>
<table>
<tbody>
<tr>
<td width="15">17.1</td>
<td>Collection of personal data from customers shall be for purposes relating to the provision of logistics services or related services;</td>
</tr>
<tr>
<td>17.2</td>
<td>All practical steps will be taken to ensure that personal data are accurate and will not be kept longer than necessary or will be destroyed in accordance with our internal retention period;</td>
</tr>
<tr>
<td>17.3</td>
<td>Personal data will not be used for any purposes other than the data that were to be used at the time of collection or purposes directly related thereto</td>
</tr>
<tr>
<td>17.4</td>
<td>Personal data will be protected against unauthorized or accidental access, processing or erasure;</td>
</tr>
<tr>
<td>17.5</td>
<td>Customers shall have the right for correction of their personal data held by us and that customers' request for access or correction will be dealt with in accordance with the Ordinance.</td>
</tr>
</tbody>
</table>
<br /><br /> <span style="font: bold 16px 'Tahoma';">18. Payment Policy</span> <br />
<table border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td style="padding: 0 7px;">VISA Card and Master Card</td>
<td>You will receive an order confirmation email within 24 hours for credit card payment. If the payment fails to go through, the system will cancel the order automatically and you will be notified.</td>
</tr>
<tr>
<td style="padding: 0 7px;">PayPal</td>
<td>You can make PayPal account or by credit cards on PayPal website. It is a fast, safe and convenient way to a payment.</td>
</tr>
<tr>
<td style="padding: 0 7px;">Cash on Delivery</td>
<td>Free of charge.</td>
</tr>
</tbody>
</table>
<br /><br />A customer's personal data is classified as confidential and can only be disclosed by us where permitted by the Ordinance or otherwise legally compelled to do so.</div>
EOD;

    $pageRootTemplate = 'one_column';
    $pageLayoutUpdateXml = <<<EOD
EOD;


    $pageCustomLayoutUpdateXML = <<<EOD
EOD;

    $pageMetaKeywords = "";
    $pageMetaDescription = "";

    $page = Mage::getModel('cms/page')->getCollection()
        ->addStoreFilter(array($storeIdMoxyEn), $withAdmin = true)
        ->addFieldToFilter('identifier', $pageIdentifier)
        ->getFirstItem();
    if ($page->getId() == 0) {
        $page = Mage::getModel('cms/page');
    }
    else
    {
        // if exists then repair
        $page = Mage::getModel('cms/page')->load($page->getId());
    }

    $page->setTitle($pageTitle);
    $page->setIdentifier($pageIdentifier);
    $page->setStores($pageStores);
    $page->setIsActive($pageIsActive);
    $page->setUnderVersionControl($pageUnderVersionControl);
    $page->setContentHeading($pageContentHeading);
    $page->setContent($pageContent);
    $page->setRootTemplate($pageRootTemplate);
    $page->setLayoutUpdateXml($pageLayoutUpdateXml);
    $page->setCustomLayoutUpdateXml($pageCustomLayoutUpdateXML);
    $page->setMetaKeywords($pageMetaKeywords);
    $page->setMetaDescription($pageMetaDescription);
    $page->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================

    //==========================================================================
    // Terms and Conditions TH Page
    //==========================================================================
    $pageTitle = "ข้อกำหนดและเงื่อนไขการใช้งานเว็บไซต์";
    $pageIdentifier = "terms-and-conditions";
    $pageStores = array($storeIdMoxyTh);
    $pageIsActive = 1;
    $pageUnderVersionControl = 0;
    $pageContentHeading = "ข้อกำหนดและเงื่อนไข";
    $pageContent = <<<EOD
<div style="font: normal 14px 'Tahoma'; line-height: 22px; color: #505050;"><span style="font: bold 16px 'Tahoma';">1. บทนำ</span> <br />บริษัท Whatsnew จำกัด ได้จัดทำเว็บไซต์ชื่อ www.sanoga.com เพื่อขายและส่งเสริมการขายสินค้าของบริษัท Whatsnew จำกัด ตลอดจนเป็นช่องทางในการติดต่อสื่อสารระหว่างบริษัท Whatsnew จำกัด กับผู้ใช้บริการ เราขอความกรุณาให้คุณได้อ่านข้อตกลงดังต่อไปนี้อย่างละเอียด หากคุณไม่ยอมรับข้อตกลงและเงื่อนไขดังกล่าว กรุณาหลีกเลี่ยงหรืออย่าเข้าใช้งานที่เว็บไซต์นี้ การที่คุณใช้เว็บไซต์นี้หรือเข้าไปดูข้อมูลในหน้าใดๆของเว็บไซต์นี้ ถือว่าคุณยอมรับข้อผูกพันทางกฎหมายที่ระบุไว้ในข้อตกลงและเงื่อนไขการใช้บริการนี้ โดยรวมถึงนโยบายความเป็นส่วนตัวที่ บริษัท Whatsnew จำกัด กำหนดขึ้นด้วย<br /><br /> <span style="font: bold 16px 'Tahoma';">2. ข้อมูลต่างๆในเว็บไซต์ </span> <br />ถึงแม้ว่า จะได้พยายามทุกวิถีทางที่จะทำให้ข้อมูลต่างๆในเว็บไซต์ของเรามีความถูกต้องมากที่สุด แต่ทาง บริษัท Whatsnew ก็ ไม่สามารถรับประกันว่าข้อมูลและส่วนประกอบดังกล่าวมีความถูกต้อง สมบูรณ์เพียงพอ ทันกาลทันเวลา หรือ เหมาะสมกับวัตถุประสงค์ใดโดยเฉพาะ ทั้งนี้ บริษัท Whatsnew จะไม่รับผิดสำหรับความผิดพลาดหรือการละเว้นใดๆในข้อมูลและส่วนประกอบนั้น (รวมไปถึงข้อมูลหรือเนื้อหาใดๆก็ตามที่มีบุคคลที่สามหรือรวมไปถึง ข้อมูลและการรับประกันที่มีการนำเสนอขึ้นมา บริษัท Whatsnew ขอสงวนสิทธิ์ที่จะทำการแก้ไขเปลี่ยนแปลงส่วนหนึ่งส่วนใดของเว็บไซต์หรือยุติการให้บริการได้ทุกเมื่อ ทั้งที่เป็นการถาวรหรือชั่วคราว โดยไม่จำเป็นต้องมีการแจ้งให้ทราบล่วงหน้าก็ได้ ข้อมูลและองค์ประกอบทุกๆส่วนในเว็บไซด์นี้ มีวัตถุประสงค์เพื่อให้ข้อมูลแก่ผู้บริโภคทั่วไปเท่านั้น ไม่มีข้อมูลส่วนหนึ่งส่วนใดที่มีวัตถุประสงค์เพื่อเป็นการให้คำแนะนำ การวินิจฉัย การให้คำปรึกษา แต่อย่างใด รวมถึงมิได้มีวัตถุประสงค์เพื่อการแลกเปลี่ยน พึงระลึกไว้ว่าคุณและผู้ติดตามของคุณใช้ข้อมูลที่ประกอบขึ้นในเว็บไซต์ของเราด้วยความเสี่ยงของตัวคุณเอง หากคุณพบข้อผิดพลาดหรือการละเลย เว้นว่างในส่วนใดของเว็บไซต์ของเรา กรุณาแจ้งให้เราทราบ&nbsp;<br /><br /> <span style="font: bold 16px 'Tahoma';">3. เครื่องหมายการค้าและลิขสิทธิ์ </span> <br />บริษัท Whatsnew เป็นเจ้าของลิขสิทธิ์ เครื่องหมายการค้า สัญลักษณ์และส่วนประกอบอื่นๆ ที่ปรากฏในทุกๆหน้าของเว็บไซต์นี้ (กล่าวโดยรวมคือ "เครื่องหมายการค้า") ห้ามมิให้ผู้ใด หรือ บริษัทใด ทำการดัดแปลง จัดเก็บในระบบที่สามารถนำมาใช้งานได้ ถ่ายโอน ลอกเลียนแบบ เผยแพร่ หรือ ใช้ข้อมูลและส่วนประกอบนั้นโดยมิได้รับความยินยอมเป็นลายลักษณ์อักษรจาก บริษัท Whatsnew<br /><br /> <span style="font: bold 16px 'Tahoma';">4. การเชื่อมโยงกับเว็บไซต์อื่น&nbsp;</span> <br />เว็บไซต์ของ บริษัท Whatsnew อาจมีการเชื่อมโยงกับเว็บไซต์อื่น เพื่อวัตถุประสงค์ในการให้ข้อมูลและเพื่อความสะดวกของคุณ บริษัท Whatsnew มิได้ให้การรับรองหรือยืนยันความถูกต้องในเนื้อหาของเว็บไซต์เหล่านั้น และมิได้สื่อโดยนัยว่า บริษัท Whatsnew ให้การรับรองหรือแนะนำข้อมูลในเว็บไซต์เหล่านั้น การเข้าเยี่ยมชมหรือใช้ข้อมูลจากเว็บไซต์อื่นๆนั้นถือเป็นหน้าที่ของคุณในการพิจารณาวิเคราะห์ความเสี่ยงที่อาจเกิดขึ้นจากการใช้ข้อมูลในเว็บไซด์เหล่านั้นด้วยตนเอง การเข้าเยี่ยมชมเว็บไซต์อื่นๆนั้น เราขอแนะนำให้คุณอ่านรายละเอียดข้อตกลงและเงื่อนไขการใช้บริการ ตลอดจนนโยบายการรักษาความปลอดภัยของเว็บไซต์ที่เชื่อมโยงนั้นก่อนเข้าใช้เว็บไซต์ดังกล่าว และเราไม่อนุญาตให้คุณทำลิงค์จากเว็บไซต์ของตนเองเพื่อเชื่อมโยงไปยังเว็บไซต์เหล่านั้นโดยมิได้มีการขออนุญาตจาก บริษัท Whatsnew เป็นลายลักษณ์อักษรเสียก่อน กรุณาติดต่อเรา หากคุณต้องการทำลิงค์เชื่อมโยงไปยังเว็บไซต์ของคุณ&nbsp;<br /><br /> <span style="font: bold 16px 'Tahoma';">5. กฎ กติกา มารยาทในการใช้งานเว็บบอร์ดและการนำเสนอข้อมูลของผู้ใช้งานเว็บไซต์ </span> <br />
<p>บริษัท Whatsnew ไม่รับผิดชอบต่อเนื้อหาใดๆที่คุณได้ทำการนำเสนอต่อสาธารณชน (ทั้งนี้รวมถึงเว็บบอร์ด หน้าข้อมูลต่างๆของเว็บไซต์ ห้องแชท หรือพื้นที่สาธารณะใดๆก็ตามที่ปรากฏอยู่ในเว็บไซต์) บริษัท Whatsnew ไม่จำเป็นต้องเห็นด้วย และ ไม่มีส่วนเกี่ยวข้องกับเนื้อหาใดๆ (ที่คุณหรือผู้ใช้งานท่านอื่นแสดงความคิดเห็นขึ้นมา) บริษัท Whatsnew ขอสงวนสิทธิ์ในการใช้ดุลยพินิจที่จะลบเนื้อหาหรือความคิดเห็นของคุณได้โดยไม่ต้องแจ้งให้คุณทราบล่วงหน้า ถ้าหากเนื้อหาหรือความคิดเห็นนั้นๆมีเจตนาหรือมีข้อสงสัยว่าเป็น</p>
<table>
<tbody>
<tr>
<td width="15">5.1</td>
<td>ข้อความหรือเนื้อหาอันเป็นการทำลายชื่อเสียง หมิ่นประมาท ส่อเสียด คุกคาม ว่ากล่าวให้ร้าย แก่ สมาชิกท่านอื่นหรือบุคคลที่ 3</td>
</tr>
<tr>
<td>5.2</td>
<td>การนำเสนอ เผยแพร่ ข้อความหรือเนื้อหาที่เป็นการนำเสนอที่ส่อไปในทางลามก อนาจารหรือขัดต่อกฎหมายใดๆ</td>
</tr>
<tr>
<td>5.3</td>
<td>การเผยแพร่หรืออัพโหลดไฟล์ข้อมูลที่มีไวรัส ไฟล์ที่ส่งผลให้เกิดการทำงานที่ไม่ปกติ หรือ ซอฟท์แวร์ หรือโปรแกรมใดๆที่อาจก่อให้เกิดความเสียหายต่อการทำงานของเว็บไซต์ของ บริษัท Whatsnew และ/หรือระบบคอมพิวเตอร์ รวมถึงระบบเครือข่ายของบุคคลที่สาม</td>
</tr>
<tr>
<td>5.4</td>
<td>ข้อความหรือเนื้อหาที่เป็นการละเมิดลิขสิทธิ์หรือเครื่องหมายการค้า ที่เป็นการผิดกฎหมายของ ประเทศไทยหรือ นานาประเทศ รวมถึงการละเมิดลิขสิทธิ์ทรัพย์สินทางปัญญาของบริษัท Whatsnew หรือบุคคลที่สาม</td>
</tr>
<tr>
<td>5.5</td>
<td>การนำเสนอข้อความหรือเนื้อหาที่เป็นการโฆษณา หรือเนื้อหาที่เป็นการประชาสัมพันธ์อันมีเจตนาเพื่อ จะกระตุ้นให้เกิดการซื้อสินค้า</td>
</tr>
</tbody>
</table>
<br /><br /> <span style="font: bold 16px 'Tahoma';">6. สมาชิกภาพ </span> <br />เว็บไซต์ของ บริษัท Whatsnew ไม่ให้บริการกับผู้ใช้งานที่มีอายุต่ำกว่า 18 ปี ซึ่งเป็นผู้ที่อยู่นอกเหนือจากกลุ่มเป้าหมายของเรา หรือสมาชิกที่เคยถูก บริษัท Whatsnew ตัดสิทธิ์การใช้งานไปแล้ว ผู้ใช้งานแต่ละท่านจะได้รับอนุญาตให้มีบัญชีผู้ใช้งานได้เพียงบัญชีเดียวเท่านั้น การละเมิดเงื่อนไขที่ได้กล่าวมาแล้วนั้นอาจส่งผลให้เกิดการระงับบัญชีผู้ใช้งานได้&nbsp;<br /> <br /> บริษัท Whatsnew อาจมีโปรโมชั่น หรือ รางวัลพิเศษให้แก่สมาชิกของเราเป็นครั้งคราว ในการรับโปรโมชั่นพิเศษดังกล่าวหากสมาชิกผู้ใช้งานท่านใดที่ได้รับรางวัล (จะเป็นของรางวัล เครดิตพิเศษ บัตรของขวัญ คูปอง หรือ ผลประโยชน์อื่นใด นอกเหนือจากนี้) จาก บริษัท Whatsnew แต่มีการสมัครสมาชิกซ้ำซ้อนเอาไว้ในหลายชื่อ หรือใช้อีเมล์แอดเดรสหลายชื่อ รวมถึง การให้ข้อมูลที่เป็นเท็จ หรือ มีการกระทำอันเป็นการหลอกลวงจะถูกตัดสิทธิในการได้รับรางวัลใดๆในกิจกรรมที่ทางเราได้จัดขึ้นและอาจจะถูกดำเนินคดีตามกฎหมายหรือมีโทษปรับตามกฎหมายอาญา&nbsp;<br /> <br /> การใช้งานเว็บไซต์ของ บริษัท Whatsnew พึงตระหนักว่าคุณต้องมีอายุครบตามที่กฎหมายกำหนดในการทำสัญญากับเราและต้องมิใช่บุคคลที่เคยถูกระงับการให้บริการตามกฎหมายของประเทศไทยหรือขอบเขตกฎหมายอื่นใด และเมื่อทาง บริษัท Whatsnew แจ้งความประสงค์ไป คุณต้องยินยอมที่จะให้ข้อมูลที่เป็นจริงและถูกต้องของตนเอง หากทาง บริษัท Whatsnew ตรวจสอบพบว่า คุณได้ให้ข้อมูลอันเป็นเท็จ ไม่ถูกต้อง หรือ ไม่ครบถ้วนสมบูรณ์ บริษัท Whatsnew มีสิทธิ์ที่จะระงับสิทธิ์ในการเป็นสมาชิกชั่วคราว ตลอดจนยกเลิกการเป็นสมาชิกของคุณ หรือปฏิเสธการสมัครสมาชิกเพื่อใช้งานของคุณอีกในอนาคต คุณจะต้องเป็นผู้รับผิดชอบแต่ผู้เดียวในการเก็บรักษาชื่อบัญชีผู้ใช้งานและรหัสผ่านของตนเองให้เป็นความลับ คุณตกลงยินยอมที่จะรับผิดชอบต่อกิจกรรมใดๆที่เกิดขึ้นภายใต้ชื่อบัญชีผู้ใช้งานของคุณเอง&nbsp;<br /><br /> <span style="font: bold 16px 'Tahoma';">7. การยกเลิกอันเกิดจากความผิดพลาด </span> <br />บริษัท Whatsnew มีสิทธิอย่างเต็มที่ที่จะยกเลิกคำสั่งซื้อได้ทุกเมื่อ หากความผิดพลาดนั้นเกิดมาจากความผิดพลาดในการพิมพ์หรือความผิดพลาดที่มิได้มีการคาดการณ์ไว้ล่วงหน้า อันเป็นผลมาจากสินค้าในเว็บไซต์ที่มีการลงรายการไว้ไม่ถูกต้อง (ไม่ว่าจะเป็นการลงราคาผิด หรือ ลงรายละเอียดที่ผิด เป็นต้น) ในกรณีที่รายการสั่งซื้อนั้นเกิดการยกเลิกขึ้นและเราได้รับการชำระเงินสำหรับคำสั่งซื้อนั้นเสร็จสมบูรณ์เรียบร้อยแล้ว บริษัท Whatsnew จะทำการคืนเงินให้ท่านเต็มจำนวนราคาสินค้า&nbsp;<br /><br /> <span style="font: bold 16px 'Tahoma';">8. การใช้งานอันไม่เหมาะสม </span> <br />คุณตกลงใจที่จะไม่ใช้เว็บไซต์ของ บริษัท Whatsnew เพื่อเป็นการส่งหรือประกาศถึงเนื้อหาหรือข้อมูลใดๆอันเป็นการขัดต่อกฎหมาย ข่มขู่ รังควาน เจตนาทำลายชื่อเสียง หมิ่นประมาท หยาบคาย คุกคาม มุ่งร้าย ลามกอนาจาร ส่อไปในทางเรื่องเพศ ก้าวร้าว ทำให้ผู้อื่นไม่พอใจ ดูหมิ่นศาสนา รวมถึงรูปภาพลามกหรือผิดกฎหมายใดๆที่บังคับใช้ ด้วยเหตุนี้หากคุณได้ทำการส่งหรือประกาศถึงเนื้อหา หรือ ข้อความ รวมถึง รูปภาพใดๆที่กล่าวมาแล้วนั้น และทาง บริษัท Whatsnew หรือ บุคคลที่สามใดๆก็ตาม ได้รับความเสียหายอันเนื่องมาจากเนื้อหาหรือข้อมูลของคุณ ไม่ว่าจะทางตรงหรือทางอ้อมก็ตามที คุณจะต้องเป็นผู้รับผิดชอบความเสียหาย รวมถึงหนี้สิน ค่าเสียหายและรายจ่ายอันเกิดขึ้นจากการที่ บริษัท Whatsnew หรือ บุคคลที่ 3 ต้องสูญเสียไป&nbsp;<br /><br /> <span style="font: bold 16px 'Tahoma';">9. การรับประกัน </span> <br /> ไม่ว่าในกรณีใดๆ บริษัท Whatsnew ไม่ขอรับประกัน รับรอง หรือ เป็นตัวแทน ต่อการนำเสนอเนื้อหาของผู้ใช้งาน หรือ ความคิดเห็น คำแนะนำ หรือคำรับประกันใดๆที่ปรากฏ (ไม่ว่าจะแจ้งไว้อย่างชัดเจนหรือบอกเป็นนัยยะทางกฎหมายก็ตามที) ในเว็บไซต์ของเรา โดยหมายรวมถึงข้อมูลส่วนตัวของคุณหรือผู้ติดตามของคุณที่มีการส่งผ่านระบบของเราด้วย <br /><br /> <span style="font: bold 16px 'Tahoma';">10. ข้อจำกัดความรับผิด </span> <br />บริษัท Whatsnew จะไม่รับผิดชอบในความเสียหายใดๆ รวมถึง ความเสียหาย สูญเสียและค่าใช้จ่ายที่เกิดขึ้นไม่ว่าโดยทางตรงหรือโดยทางอ้อม โดยเฉพาะเจาะจง โดยบังเอิญ หรือ เป็นผลสืบเนื่องที่ตามมา อาการบาดเจ็บทางร่างกาย หรือ ค่าใช้จ่ายอันเกิดขึ้นจากอาการบาดเจ็บของคุณหรือบุคคลที่สามคนใดก็ตาม (รวมถึงผู้ติดตามของคุณด้วย) รวมถึงผลลัพธ์อันเนื่องมาจากการเข้าใช้งานในเว็บไซต์ของเรา ไม่ว่าจะโดยตรงหรือโดยอ้อม ทั้งข้อมูลใดๆที่ปรากฏอยู่บนเว็บไซต์ ข้อมูลส่วนตัวของคุณหรือข้อมูลผู้ติดตามของคุณ ตลอดจนเนื้อหาและข้อมูลต่างๆที่มีการส่งผ่านระบบของเรา กล่าวให้เฉพาะเจาะจงลงไปคือ บริษัท Ardent Commerce หรือ บุคคลที่สามใดๆ หรือ ข้อมูล เนื้อหาที่มีการนำเสนอขึ้นมา จะไม่ต้องรับผิดชอบทางกฎหมายใดๆต่อคุณหรือต่อบุคคลอื่นใด ทั้งต่อบริษัทใดๆก็ตามที่ต้องประสบกับความสูญเสีย เกิดค่าใช้จ่าย ไม่ว่าจะโดยตรงหรือโดยอ้อม การบาดเจ็บทางร่างกายหรือค่าใช้จ่ายอันเกิดขึ้นจากความล่าช้า ความไม่ถูกต้อง ความผิดพลาด หรือ การเว้นว่างของข้อมูลด้านราคาที่มีการแบ่งสรรปันส่วนกัน หรือ การส่งต่อกัน หรือ การกระทำใดๆอันเกิดจากความเชื่อถือในเนื้อหา หรือ เป็นเหตุให้เกิดการไม่ประสบผลสำเร็จ หรือ การหยุดชะงัก หรือ การสิ้นสุด&nbsp;<br /> <br /> โปรแกรมแนะนำเพื่อนเพื่อรับเครดิตฟรีและโปรแกรมการรับเครดิตคืน รวมถึงโปรแกรมรับผลตอบแทนอื่นๆ ของเว็บไซต์ Sanoga.com ขึ้นอยู่กับดุลพินิจของบริษัท Whatsnew จำกัดเท่านั้น บริษัทฯ ขอสงวนสิทธิ์ในการเปลี่ยนแปลง ยกเลิกโปรแกรมทั้งเป็นการชั่วคราวหรือถาวรในทุกๆโปรแกรม ทั้งโปรแกรมแนะนำเพื่อนเพื่อรับเครดิตฟรีและโปรแกรมการรับเครดิตคืนหรือโปรแกรมรับผลตอบแทนอื่นๆ โดยไม่ต้องแจ้งสมาชิกล่วงหน้า บริษัทฯ อาจทำการเพิกถอน จำกัด เปลี่ยนแปลงเครดิต เพิ่มหรือลดจำนวนเครดิตคืน ตามความเหมาะสมของที่สอดคล้องกับโปรแกรมการแนะนำเพื่อนเพื่อรับฟรีเครดิตและโปรแกรมการรับเครดิตคืน การสมัครเป็นสมาชิก Sanoga.com ถือว่าท่านได้ยอมรับเงื่อนไขข้อตกลง ของโปรแกรมการแนะนำเพื่อนเพื่อรับฟรีเครดิต และการรับเครดิตคืน ซึ่งท่านหรือบุคคลที่สามไม่มีสิทธิ์เปลี่ยนแปลงหรือยกเลิกโปรแกรมดังกล่าวได้&nbsp;<br /><br /> <span style="font: bold 16px 'Tahoma';">11. การชดเชยค่าเสียหาย</span> <br />ผู้ใช้งานยอมรับที่จะชดเชยค่าเสียหาย และค่าใช้จ่ายอันเกิดจากการละเมิดเงื่อนไขและข้อกำหนดในการให้บริการหรือการกระทำอื่นใดที่เกี่ยวข้องกับบัญชีผู้ใช้งานของสมาชิก อันเนื่องมาจากการละเลยไม่ใส่ใจหรือการกระทำที่ผิดกฎหมาย และผู้ใช้งานจะไม่ยึดให้ บริษัท Whatsnew (หรือพนักงานของบริษัททุกคน ผู้บริหารของบริษัท ผู้ผลิตสินค้า บริษัทสาขา บริษัทร่วมทุนตลอดจนหุ้นส่วนทางกฎหมาย) ต้องรับผิดชอบต่อการเรียกร้องค่าเสียหายรวมถึงค่าใช้จ่ายของทนายความที่เกิดขึ้นด้วย&nbsp;<br /><br /> <span style="font: bold 16px 'Tahoma';">12. การใช้งานเว็บไซต์ </span> <br />บริษัท Whatsnew มิได้รับรอง รับประกัน หรือ เป็นตัวแทนของข้อมูล เนื้อหาใดๆที่ปรากฏบนหน้าเว็บไซต์ซึ่งอาจนำไปใช้เพื่อขอบเขตทางกฎหมายของประเทศอื่นใด (นอกเหนือจากประเทศไทย) เมื่อคุณเข้ามาที่เว็บไซต์ของเรา ถือว่าคุณได้รับรู้และหมายถึงการแสดงตนต่อบริษัทฯ แล้วว่า คุณมีสิทธิตามกฎหมายในการทำเช่นนั้นและให้ใช้ข้อมูลเหล่านั้นได้ในเว็บไซต์ของเรา&nbsp;<br /><br /> <span style="font: bold 16px 'Tahoma';">13. บททั่วไป </span> <br />
<table>
<tbody>
<tr>
<td width="15">13.1</td>
<td>ข้อตกลงทั้งหมด เงื่อนไขการให้บริการของเว็บไซต์ Petloft.com ทั้งหมดนี้เป็นข้อตกลงเบ็ดเสร็จและเด็ดขาดระหว่างคุณและ บริษัท Whatsnew เกี่ยวกับการที่ท่านเข้ามาใช้งานเว็บไซต์ของเรา ทั้งคุณและ บริษัท Whatsnew มิได้ถูกผูกมัด หรือแสดงนัยยะถึงการเป็นตัวแทน รับรอง หรือสัญญาใดๆที่ไม่ได้บันทึกไว้ในนี้ นอกเสียจากจะมีการเอ่ยถึงไว้อย่างเป็นพิเศษ ข้อกำหนดและเงื่อนไขในการให้บริการของเว็บไซต์ของเราเป็นฉบับปัจจุบันที่ถือเป็นฉบับล่าสุดที่ใช้แทนที่ทุกความรับผิดชอบ ภาระผูกพัน หรือ การเป็นตัวแทน ที่แล้วๆมา ไม่ว่าจะเป็นลายลักษณ์อักษรหรือปากเปล่าก็ตามที ระหว่างคุณและ บริษัท Whatsnew ที่เกี่ยวข้องกับการที่คุณใช้งานเว็บไซต์ของเรา</td>
</tr>
<tr>
<td>13.2</td>
<td>การเปลี่ยนแปลงแก้ไข บริษัท Whatsnew อาจมีการเปลี่ยนแปลง แก้ไขข้อกำหนด และ เงื่อนไขเหล่านี้ได้ทุกเวลา พึงตระหนักไว้เสมอว่า การเข้าเยี่ยมชมเว็บไซต์ของเราในแต่ละครั้ง ถือว่าคุณตกลงยินยอมต่อข้อกำหนดและเงื่อนไขที่ได้รับการแก้ไขล่าสุดแล้ว และถึงแม้ว่าการเปลี่ยนแปลงแก้ไขข้อกำหนดและเงื่อนไขครั้งล่าสุดจะมิได้มีการระบุไว้ก็ตามที แต่ให้ยึดเอาข้อกำหนดและเงื่อนไขฉบับปัจจุบันเป็นสำคัญ การเข้าเยี่ยมชมเว็บไซต์ บริษัท Whatsnew ของคุณในแต่ละครั้ง จะต้องถือเอาข้อกำหนดและเงื่อนไขล่าสุดเป็นสำคัญ และเป็นความรับผิดชอบของคุณเองที่ควรจะเยี่ยมชมหน้าดังกล่าวของเว็บไซต์เป็นระยะๆ เพื่อให้ทราบถึงเนื้อหาล่าสุดของข้อบังคับและเงื่อนไขในการใช้งานที่คุณมี</td>
</tr>
<tr>
<td>13.3</td>
<td>ความขัดแย้ง กรณีที่มีการกำหนดข้อตกลงและเงื่อนไขการใช้บริการในหน้าอื่นที่ขัดแย้งกับ ข้อตกลงและเงื่อนไขการใช้บริการนี้ ไม่ว่าจะเป็น นโยบาย หรือ ประกาศ หรือ เงื่อนไขและข้อตกลงในการใช้บริการอื่นใดก็ตาม ในส่วนที่กำหนดไว้เป็นพิเศษหรือมาตรฐานของเว็บไซต์ ให้คุณถือปฏิบัติตามข้อตกลงและเงื่อนไขการใช้บริการที่ปรากฏในหน้าอื่นนั้นอย่างเป็นกรณีพิเศษก่อน และข้อกำหนดข้อนั้นจะถือว่าแยกออกจากข้อกำหนดเหล่านี้และไม่มีผลกระทบต่อการมีผลใช้บังคับของข้อตกลงที่เหลือนี้</td>
</tr>
<tr>
<td>13.4</td>
<td>การสละสิทธิ์ ทั้งคุณหรือ บริษัท Whatsnew ไม่สามารถมอบหมาย, ถ่ายโอน, หรือให้สิทธิช่วงในสิทธิของคุณส่วนหนึ่งส่วนใดหรือทั้งหมดภาย ใต้ข้อตกลงนี้โดยปราศจากการอนุญาตเป็นลายลักษณ์อักษรล่วงหน้าก่อนได้ ยกเว้นแต่จะมีการทำคำสละสิทธิ์ออกมาเป็นลายลักษณ์อักษรชัดเจนและมีการลงนาม ของบุคคลที่มีอำนาจเพียงพอจากฝ่ายที่อนุญาตให้มีการบอกเลิกสิทธิ์</td>
</tr>
<tr>
<td>13.5</td>
<td>การถือครองสิทธิ์ บริษัท Whatsnew มีสิทธิขาดในการยกให้ โอนสิทธิ หรือ มอบหมายลิขสิทธิ์และสัญญาผูกมัดทั้งหมด ให้ตัวแทนหรือบุคคลที่สามคนอื่นใดก็ได้ จะเป็นในแง่ของเงื่อนไขการให้บริการ นโยบายและประกาศใดๆที่เกี่ยวข้องกันก็ตามที</td>
</tr>
<tr>
<td>13.6</td>
<td>การเป็นโมฆะเป็นบางส่วน ข้อกำหนดทุกประการรวมถึงเงื่อนไขในการให้บริการ นโยบาย และ ประกาศใดๆก็ตามที่เกี่ยวเนื่องกัน ไม่ว่าจะพิจารณาแบบรวมเข้าด้วยกันหรือมีการเกี่ยวโยงกันตามหลักไวยากรณ์ ประการใดก็ตามที จะมีการบังคับใช้เป็นเอกเทศ แยกเป็นอิสระจากกัน หากมีข้อกำหนดใดของเงื่อนไขในการให้บริการ นโยบาย หรือ ประกาศ ซึ่งเป็น หรือ กลายเป็น การไม่สามารถบังคับใช้ขอบเขตอำนาจในการบังคับคดีได้ ไม่ว่าจะเป็น การเป็นโมฆะ การใช้งานไม่ได้ การทำผิดกฎหมาย หรือ การไม่ชอบด้วยกฎหมาย หรือด้วยเหตุผลอื่นใดก็ตามที หรือ หากศาลหรือเขตอำนาจศาลใดเห็นว่า ข้อกำหนดเงื่อนไขของสัญญานี้ หรือ ส่วนใดส่วนหนึ่ง ของสัญญานี้ ใช้บังคับไม่ได้ ให้ใช้ข้อบังคับอื่นใดนอกเหนือจากข้อนั้นๆ เท่าที่สามารถจะใช้ได้ และ โดยให้ ส่วนที่เหลือ ของสัญญานี้มีผลบังคับใช้ได้อย่างเต็มที่</td>
</tr>
<tr>
<td>13.7</td>
<td>กฎหมายที่บังคับใช้ ข้อตกลงนี้ได้รับการควบคุมโดยกฎหมายแห่งประเทศไทย โดยไม่คำนึงถึงความขัดแย้งของหลักกฎหมาย ดังนี้เองคุณจึงเห็นชอบด้วยว่าข้อโต้แย้งหรือข้อเรียกร้องที่เกิดขึ้นจาก หรือ เกี่ยวข้องกับ ข้อตกลงนี้จะได้รับการแก้ไขด้วยศาลสูงที่ตั้งอยู่ในกรุงเทพมหานคร ประเทศไทยเท่านั้น และดังนี้เอง คุณจึงได้อนุญาตและยอมรับต่ออำนาจศาลดังกล่าวในการฟ้องร้อง ข้อโต้แย้ง หรือ ข้อเรียกร้องใดๆ อันเกิดขึ้นจากการเกี่ยวพันกับเว็บไซต์ของเรา หรือ เงื่อนไขในการให้บริการข้ออื่นๆที่เกี่ยวเนื่องกัน รวมไปถึง ประกาศ นโยบาย หรือ เนื้อหาสาระข้อมูลอื่นใดที่มีความเกี่ยวข้องหรือมีความเกี่ยวโยงกัน</td>
</tr>
<tr>
<td>13.8</td>
<td>ความคิดเห็นหรือข้อสงสัย หากคุณมีข้อสงสัย หรือ คำถามประการใดเกี่ยวกับข้อกำหนดและเงื่อนไขในการให้บริการ นโยบายความเป็นส่วนตัว หรือ เงื่อนไขการให้บริการ นโยบาย และ ประกาศ หรือแนวทางที่เราบริหารจัดการข้อมูลส่วนบุคคลของคุณก็ตาม <a href="{{store url='contacts'}}" target="_blank">กรุณาติดต่อเรา</a>ได้ทันที</td>
</tr>
</tbody>
</table>
<br /><br /> <span style="font: bold 16px 'Tahoma';">14. การสิ้นสุด </span> <br />คุณต้องปฏิบัติตามและยึดเอาเงื่อนไขในการให้บริการทั้งหมดนี้เป็นสำคัญเมื่อคุณเข้าใช้งานในเว็บไซต์ของ บริษัท Whatsnew และ/หรือเมื่อคุณทำการลงทะเบียนเพื่อเป็นสมาชิก หรือ ระหว่างการดำเนินการซื้อของคุณ ข้อกำหนดและเงื่อนไขในการให้บริการทั้งหมดนี้ หรือ แม้เพียงข้อหนึ่งข้อใดก็ตาม ทาง บริษัท Whatsnew อาจจะมีการปรับเปลี่ยน แก้ไข หรือ ตัดออก โดยไม่จำเป็นต้องมีการแจ้งให้ทราบล่วงหน้า ข้อกำหนดหรือเงื่อนไขใดๆที่เกี่ยวข้องกับ ลิขสิทธิ์ เครื่องหมายการค้า การปฏิเสธความรับผิดชอบ การอ้างสิทธิ ข้อจำกัดของความรับผิดชอบ การชดเชย กฎหมายที่นำมาบังคับใช้ การตัดสินโดยอนุญาโตตุลาการ จะยังคงมีผลอยู่อย่างสมบูรณ์ถึงแม้จะมีการยกเลิกหรือ สิ้นสุดส่วนใดส่วนหนึ่งไปก็ตามที&nbsp;<br /><br /> <span style="font: bold 16px 'Tahoma';">15. การคืนและการยกเลิกคำสั่งซื้อ </span> <br />คำสั่งซื้อสามารถจะทำการยกเลิกได้ หากคำสั่งซื้อ ของท่านยังมิได้ทำการส่งไปถึงท่าน หากเมื่อ คำสั่งซื้อได้ทำการส่งไปยังบัญชีผู้ใช้งานของท่านเรียบร้อยแล้ว ท่านจะไม่สามารถทำการยกเลิกคำสั่งซื้อนั้นได้ การขอคืนหลังสิ้นสุดการสั่งซื้อนั้นจะมีการพิจารณาบนพื้นฐานเป็นกรณีๆไป เราจะพยายามรักษาผลประโยชน์ให้สมาชิกอย่างสูงสุด ภายใต้ข้อจำกัดที่เรามี ทว่าเราไม่สามารถจะรับคืนได้ ถ้าหากว่ามิได้มีสิ่งใดผิดปกติเกิดขึ้นในการซื้อ&nbsp;<br /><br /> <span style="font: bold 16px 'Tahoma';">16. ข้อตกลงและเงื่อนไขการใช้เว็บไซต์ </span> <br />โปรแกรมแนะนำเพื่อนเพื่อรับเครดิตฟรีและโปรแกรมการรับเครดิตคืน รวมถึงโปรแกรมรับผลตอบแทนอื่นๆ ของเว็บไซต์ Sanoga.com ขึ้นอยู่กับดุลพินิจของบริษัท Whatsnew เท่านั้น บริษัทฯ ขอสงวนสิทธิ์ในการเปลี่ยนแปลง ยกเลิกโปรแกรมทั้งเป็นการชั่วคราวหรือถาวรในทุกๆโปรแกรม ทั้งโปรแกรมแนะนำเพื่อนเพื่อรับเครดิตฟรีและโปรแกรมการรับเครดิตคืนหรือโปรแกรมรับผลตอบแทนอื่นๆ โดยไม่ต้องแจ้งสมาชิกล่วงหน้า บริษัทฯ อาจทำการเพิกถอน จำกัด เปลี่ยนแปลงเครดิต เพิ่มหรือลดจำนวนเครดิตคืน ตามความเหมาะสมที่สอดคล้องกับโปรแกรมการแนะนำเพื่อนเพื่อรับฟรีเครดิตและโปรแกรมการรับเครดิตคืน การสมัครเป็นสมาชิกเว็บไซต์ Sanoga.com ถือว่าท่านได้ยอมรับเงื่อนไขข้อตกลง ของโปรแกรมการแนะนำเพื่อนเพื่อรับฟรีเครดิต และการรับเครดิตคืน ซึ่งท่านหรือบุคคลที่สามไม่มีสิทธิ์เปลี่ยนแปลงหรือยกเลิกโปรแกรมดังกล่าวได้&nbsp;<br /><br /> <span style="font: bold 16px 'Tahoma';">17. ความเป็นส่วนตัว </span> <br />
<p>ในส่วนของบริการนำส่ง เราตระหนักดีถึงภาระหน้าที่ของเราในการรักษาข้อมูลความลับทั้งปวงของลูกค้า และนอกเหนือจากภาระหน้าที่รักษาความลับต่อลูกค้า เรายังปฏิบัติตามบทบัญญัติว่าด้วยข้อมูลส่วนตัว (Personal Data (Privacy) Ordinance) ("บทบัญญัติ") ตลอดเวลา ในการรวบรวม รักษา และใช้ข้อมูลส่วนตัวของลูกค้า โดยเฉพาะอย่างยิ่งเราได้ปฏิบัติตามหลักปฏิบัติดังต่อไปนี้ เว้นแต่ในกรณีที่ลูกค้าตกลงเป็นอย่างอื่นตามความเหมาะสม</p>
<table>
<tbody>
<tr>
<td width="15">17.1</td>
<td>การรวบรวมข้อมูลส่วนตัวจากลูกค้าจะเป็นไปเพื่อวัตถุประสงค์ในการจัดหาบริการโลจิสติกส์ หรือบริการ ที่เกี่ยวข้อง</td>
</tr>
<tr>
<td>17.2</td>
<td>ในทุกขั้นตอนการปฏิบัติจะมีการดำเนินการเพื่อให้แน่ใจว่าข้อมูลส่วนตัวของลูกค้ามีความถูกต้อง และ จะไม่ถูกจัดเก็บข้อมูลไว้นานเกินความจำเป็น หรือจะถูกทำลายทิ้งตามระเบียบว่าด้วยระยะเวลาการเก็บ รักษาข้อมูลภายในของเรา</td>
</tr>
<tr>
<td>17.3</td>
<td>ข้อมูลส่วนตัวจะไม่ถูกนำไปใช้เพื่อวัตถุประสงค์อื่นใดนอกเหนือไปจากวัตถุประสงค์ ณ เวลาที่รวบรวม ข้อมูลหรือวัตถุประสงค์ที่เกี่ยวข้องโดยตรง</td>
</tr>
<tr>
<td>17.4</td>
<td>ข้อมูลส่วนตัวของลูกค้าจะได้รับการคุ้มครองจากการเข้าถึง การประมวลผล หรือการลบทิ้งที่เกิดขึ้น โดยไม่ได้รับอนุญาตหรือโดยบังเอิญ</td>
</tr>
<tr>
<td>17.5</td>
<td>ลูกค้ามีสิทธิแก้ไขข้อมูลส่วนตัวของตนซึ่งเราเก็บรักษาไว้ โดยเราจะดำเนินการกับคำร้องขอเข้าถึง หรือขอแก้ไขข้อมูลของลูกค้ารายนั้น ๆ ตามบทบัญญัติดังกล่าวข้างต้น</td>
</tr>
</tbody>
</table>
<br /><br /> <span style="font: bold 16px 'Tahoma';">18. นโยบายการชำระเงิน</span> <br />
<table border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td style="padding: 0 7px;">บัตรเครดิตวีซ่า มาสเตอร์มาสเตอร์การ์ด</td>
<td>เมื่อระบบดำเนินการแล้วเสร็จ ท่านจะได้รับ คำยืนยันการสั่งซื้อ ภายใน 24 ชั่วโมง หากการชำระเงินไม่สำเร็จระบบจะยกเลิกคำสั่งซื้อโดยอัตโนมัติ แล้วจะทำการแจ้งให้ท่านทราบ</td>
</tr>
<tr>
<td style="padding: 0 7px;">PayPal</td>
<td>ช่วยให้คุณสามารถชำระค่าสินค้าด้วยบัญชี PayPal ของคุณหรือชำระด้วยบัตรเครดิตชั้นนำต่างๆ ผ่านทางหน้าเว็บไซด์ได้ทันที โดยจะทำรายการผ่านระบบของ PayPal ทำให้การชำระเงินเป็นไปได้อย่างสะดวก รวดเร็วและปลอดภัย</td>
</tr>
<tr>
<td style="padding: 0 7px;">ชำระเงินปลายทาง (Cash on Delivery)</td>
<td>ฟรีค่าธรรมเนียมในการเก็บเงินปลายทาง</td>
</tr>
</tbody>
</table>
<br /><br />ข้อมูลส่วนตัวของลูกค้าถือเป็นความลับ และเราจะสามารถเปิดเผยข้อมูลส่วนตัวของลูกค้าได้เท่าที่ บทบัญญัติอนุญาตไว้หรือตามที่กฎหมายกำหนดเท่านั้น</div>
EOD;

    $pageRootTemplate = 'one_column';
    $pageLayoutUpdateXml = <<<EOD
EOD;


    $pageCustomLayoutUpdateXML = <<<EOD
EOD;

    $pageMetaKeywords = "";
    $pageMetaDescription = "";

    $page = Mage::getModel('cms/page')->getCollection()
        ->addStoreFilter(array($storeIdMoxyTh), $withAdmin = true)
        ->addFieldToFilter('identifier', $pageIdentifier)
        ->getFirstItem();
    if ($page->getId() == 0) {
        $page = Mage::getModel('cms/page');
    }
    else
    {
        // if exists then repair
        $page = Mage::getModel('cms/page')->load($page->getId());
    }

    $page->setTitle($pageTitle);
    $page->setIdentifier($pageIdentifier);
    $page->setStores($pageStores);
    $page->setIsActive($pageIsActive);
    $page->setUnderVersionControl($pageUnderVersionControl);
    $page->setContentHeading($pageContentHeading);
    $page->setContent($pageContent);
    $page->setRootTemplate($pageRootTemplate);
    $page->setLayoutUpdateXml($pageLayoutUpdateXml);
    $page->setCustomLayoutUpdateXml($pageCustomLayoutUpdateXML);
    $page->setMetaKeywords($pageMetaKeywords);
    $page->setMetaDescription($pageMetaDescription);
    $page->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================

    //==========================================================================
    // Privacy Policy EN Page
    //==========================================================================
    $pageTitle = "Privacy Policy";
    $pageIdentifier = "privacy-policy";
    $pageStores = array($storeIdMoxyEn);
    $pageIsActive = 1;
    $pageUnderVersionControl = 0;
    $pageContentHeading = "Privacy Policy";
    $pageContent = <<<EOD
<div style="font: normal 14px 'Tahoma'; line-height: 22px; color: #505050;"><span style="font: bold 16px 'Tahoma';">Introduction </span> <br />Sanoga.com are owned and operated by Whatsnew Inc., (herein after referred as Whatsnew) including subsidiaries and affiliates provide you, its Services (herein after referred as &ldquo;Website&rdquo;) adhere to the Privacy Policy describe herein. <br /><br /> Thank you for visiting our Website (Sanoga.com). This privacy policy tells you how we use personal information collected at this Website. Please read this privacy policy before using the Website or submitting any personal information. By using the Website, you are accepting the practices described in this privacy policy. These practices may be changed, but any changes will be posted and changes will only apply to activities and information on a going forward, not retroactive basis. You are encouraged to review the privacy policy whenever you visit the Website to make sure that you understand how any personal information you provide will be used. <br /><br /> <span style="font: bold 16px 'Tahoma';">Note: </span> <br /> The privacy practices set forth in this privacy policy are for this Website only. If you link to other Websites, please review the privacy policies posted at those sites. <br /><br /> <span style="font: bold 16px 'Tahoma';">Collection of Information</span> <br /> Whatsnew collects personally identifiable information, like names, postal addresses, email addresses, etc., when voluntarily submitted by our visitors. The information you provide is used to fulfill you specific request. This information is only used to fulfill your specific request, unless you give us permission to use it in another manner, for example to add you to one of our mailing lists. <br /><br /> <span style="font: bold 16px 'Tahoma';">Tracking (Cookie) Technology</span> <br /> The Website may use cookie and tracking technology depending on the features offered. Cookie and tracking technology are useful for gathering information such as browser type and operating system, tracking the number of visitors to the Website, and understanding how visitors use the Website. Cookies can also help customize the Website for visitors. Personal information cannot be collected via cookies and other tracking technology. However, if you previously provided personally identifiable information, cookies may be tied to such information. An aggregate cookie and tracking information may be shared with third parties. <br /><br /> <span style="font: bold 16px 'Tahoma';">Distribution of Information </span> <br /> Whatsnew may share information with governmental agencies or other companies assisting us in fraud prevention or investigation. We may do so when: (1) permitted or required by law; or, (2) trying to protect against or prevent actual or potential fraud or unauthorized transactions; or, (3) investigating fraud which has already taken place. The information is not provided to these companies for marketing purposes. <br /><br /> <span style="font: bold 16px 'Tahoma';">Commitment to Data Security </span> <br /> Your personally identifiable information is kept secure. Only authorized employees, agents and contractors (who have agreed to keep information secure and confidential) will have access to this information. All emails and newsletters from this site allow you to opt-out of further mailings. <br /><br /><strong><span style="font-size: large;">Unsubscribe Newsletter</span></strong></div>
<div style="font: normal 14px 'Tahoma'; line-height: 22px; color: #505050;">If you are not willing to receive newsletter from us, you can modify your e-mail subscriptions by,</div>
<div style="font: normal 14px 'Tahoma'; line-height: 22px; color: #505050;">- Click on a link named &ldquo;Unsubscribe&rdquo; which is embedded with every email sent by us in order to not receive future messages.</div>
<div style="font: normal 14px 'Tahoma'; line-height: 22px; color: #505050;">- Modify your e-mail subscriptions on website</div>
<div style="font: normal 14px 'Tahoma'; line-height: 22px; color: #505050;">1. Log In to your account</div>
<div style="font: normal 14px 'Tahoma'; line-height: 22px; color: #505050;">2. Select &ldquo;My Account&rdquo;</div>
<div style="font: normal 14px 'Tahoma'; line-height: 22px; color: #505050;">3. Select &ldquo;Newsletter Subscriptions&rdquo; on the menu</div>
<div style="font: normal 14px 'Tahoma'; line-height: 22px; color: #505050;">4. Uncheck &ldquo;General Subscription&rdquo; box</div>
<div style="font: normal 14px 'Tahoma'; line-height: 22px; color: #505050;">5. Save changes</div>
<div style="font: normal 14px 'Tahoma'; line-height: 22px; color: #505050;">After the unsubscribe process is completed, you will receive confirm e-mail from us.</div>
<div style="font: normal 14px 'Tahoma'; line-height: 22px; color: #505050;">&nbsp;</div>
<div style="font: normal 14px 'Tahoma'; line-height: 22px; color: #505050;">
<p><span style="font: bold 16px 'Tahoma';">Privacy Contact Information</span> <br /> If you have any questions or concerns regarding privacy at Whatsnew, please contact us at <br /> <br /> Whatsnew Co., Ltd.<br /> Sethiwan Tower - 19th Floor, <br />139 Pan Road, Silom, Bangrak, <br /> Bangkok, 10500 Thailand<br /> Tel:&nbsp;02-106-8222&nbsp;<br /> Email: <a href="mailto:support@venbi.com">support@sanoga.com</a><br /> <br /> We reserves the right to make changes to this policy. Any changes to this policy will be posted on the Website.</p>
</div>
EOD;

    $pageRootTemplate = 'one_column';
    $pageLayoutUpdateXml = <<<EOD
EOD;


    $pageCustomLayoutUpdateXML = <<<EOD
EOD;

    $pageMetaKeywords = "";
    $pageMetaDescription = "";

    $page = Mage::getModel('cms/page')->getCollection()
        ->addStoreFilter(array($storeIdMoxyEn), $withAdmin = true)
        ->addFieldToFilter('identifier', $pageIdentifier)
        ->getFirstItem();
    if ($page->getId() == 0) {
        $page = Mage::getModel('cms/page');
    }
    else
    {
        // if exists then repair
        $page = Mage::getModel('cms/page')->load($page->getId());
    }

    $page->setTitle($pageTitle);
    $page->setIdentifier($pageIdentifier);
    $page->setStores($pageStores);
    $page->setIsActive($pageIsActive);
    $page->setUnderVersionControl($pageUnderVersionControl);
    $page->setContentHeading($pageContentHeading);
    $page->setContent($pageContent);
    $page->setRootTemplate($pageRootTemplate);
    $page->setLayoutUpdateXml($pageLayoutUpdateXml);
    $page->setCustomLayoutUpdateXml($pageCustomLayoutUpdateXML);
    $page->setMetaKeywords($pageMetaKeywords);
    $page->setMetaDescription($pageMetaDescription);
    $page->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================

    //==========================================================================
    // Privacy Policy TH Page
    //==========================================================================
    $pageTitle = "นโยบายความเป็นส่วนตัว ความปลอดภัยของข้อมูลส่วนบุคคล | Moxy Thailand";
    $pageIdentifier = "privacy-policy";
    $pageStores = array($storeIdMoxyTh);
    $pageIsActive = 1;
    $pageUnderVersionControl = 0;
    $pageContentHeading = "นโยบายความเป็นส่วนตัว ความปลอดภัยของข้อมูลส่วนบุคคล";
    $pageContent = <<<EOD
<div style="font: normal 14px 'Tahoma'; line-height: 22px; color: #505050;"><span style="font: bold 16px 'Tahoma';">บทนำ </span> <br />บริษัท Whatsnew จำกัด ผู้จัดทำเว็บไซต์ sanoga.com (ต่อจากนี้จะเรียกว่า Whatsnew) อันรวมถึงบริษัทสาขาและบริษัทในเครือได้นำเสนอบริการให้กับคุณ (ต่อจากนี้จะเรียกว่า "เว็บไซต์") โดยเราได้ยึดเอานโยบายความเป็นส่วนตัวตามที่ได้แจ้งรายละเอียดไว้ดังต่อไปนี้.&nbsp;<br /> <br /> เราขอขอบคุณที่คุณแวะมาเยี่ยมชมเว็บไซต์ของเรา (Sanoga.com) นโยบายความเป็นส่วนตัวนี้จะอธิบายถึงวิธีที่เรารวบรวม ใช้ เปิดเผย และดำเนินการเกี่ยวกับข้อมูลที่สามารถระบุตัวตนของคุณได้ (ข้อมูลส่วนบุคคล) ซึ่งมีความเกี่ยวข้องกับการให้บริการของเราผ่านทางเว็บไซต์ กรุณาทำความเข้าใจเกี่ยวกับนโยบายความเป็นส่วนตัวนี้ก่อนจะใช้งานเว็บไซต์หรือกรอกข้อมูลส่วนตัวใดๆของคุณกับเว็บไซต์ ในการใช้งานเว็บไซต์ คุณยอมรับข้อตกลงที่แจ้งไว้ตามนโยบายความเป็นส่วนตัวนี้แล้ว นโยบายเหล่านี้อาจจะมีการเปลี่ยนแปลงได้ ทว่าเมื่อมีการเปลี่ยนแปลง เราจะแจ้งการเปลี่ยนแปลงนั้นให้คุณทราบและการเปลี่ยนแปลงจะเริ่มต้นมีผลต่อกิจกรรมและเนื้อหาข้อมูลที่จะเกิดขึ้นต่อไปในอนาคต โดยไม่มีผลย้อนหลัง ดังนั้น คุณจึงควรแวะมาตรวจดูนโยบายความเป็นส่วนตัวนี้เป็นประจำ เมื่อไรก็ตามที่คุณแวะมาที่เว็บไซต์ของเรา เพื่อให้คุณได้เข้าใจอย่างชัดเจนถึงนโยบายและแนวทางการปฏิบัติล่าสุดของเราเกี่ยวกับข้อมูลส่วนตัวของคุณที่มอบให้กับเรา&nbsp;<br /><br /> <span style="font: bold 16px 'Tahoma';">หมายเหตุ : </span> <br />นโยบายความเป็นส่วนตัวที่กำหนดขึ้นนี้สำหรับใช้ในเว็บไซต์นี้เท่านั้น หากคุณทำการเชื่อมต่อหรือลิงค์ไปยังเว็บไซต์อื่นใด กรุณาตรวจสอบนโยบายความเป็นส่วนตัวของเว็บไซต์ที่คุณแวะไปเยี่ยมชมเหล่านั้นด้วย&nbsp;<br /><br /> <span style="font: bold 16px 'Tahoma';">การเก็บรวบรวมข้อมูล </span> <br />Whatsnew จะทำการรวบรวมข้อมูลส่วนบุคคลของคุณ ซึ่งคุณมอบให้แก่เราในระหว่างที่ใช้บริการเว็บไซต์ ข้อมูลส่วนบุคคลดังกล่าวรวมถึงชื่อ ที่อยู่ หมายเลขโทรศัพท์ หรือที่อยู่อีเมล์ และอื่นๆ ข้อมูลที่คุณให้กับเรานั้นใช้เพื่อลงทะเบียนกับเว็บไซต์ของเราตามที่คุณต้องการ และข้อมูลส่วนบุคคลเหล่านี้ใช้เพื่อลงทะเบียนกับเว็บไซต์ของเราตามที่คุณต้องการเท่านั้น นอกเสียจากว่าคุณจะอนุญาตให้เราใช้ข้อมูลเหล่านั้นเพื่อจุดประสงค์อื่นใด ตัวอย่างเช่น การเพิ่มชื่อของคุณไว้ในรายชื่อเพื่อส่งข่าวสารทางอีเมล์ของเรา&nbsp;<br /><br /> <span style="font: bold 16px 'Tahoma';">เทคโนโลยีจดจำข้อมูล (คุกกี้) </span> <br />เว็บไซต์ของเราอาจใช้ คุกกี้ และ เทคโนโลยีจดจำข้อมูลมาใช้ในเว็บไซต์ ทั้งนี้ขึ้นอยู่กับลักษณะของการให้บริการที่เรานำเสนอ คุกกี้ และ เทคโนโลยีการจดจำข้อมูลนั้นใช้เพื่อประโยชน์ในการเก็บรวบรวมข้อมูล อาทิเช่น ชนิดของบราวเซอร์ที่ใช้งานและระบบปฏิบัติการ การนับจำนวนผู้ที่เข้ามาใช้งานเว็บไซต์ และ ทำความเข้าใจถึงความต้องการของผู้ใช้งานแต่ละคนในการเข้าใช้งานในเว็บไซต์ของเรา นอกเหนือจากนี้ คุกกี้ ยังสามารถช่วยให้เว็บไซต์ของเราสามารถ "ปรับเปลี่ยน" ตนเองให้เหมาะกับผู้ใช้แต่ละคนได้ด้วย การจดจำข้อมูลเกี่ยวกับการมาเยี่ยมชมเว็บไซต์ของผู้ใช้เอาไว้ ทว่า คุกกี้ หรือ เทคโนโลยีการจดจำข้อมูลใดๆ จะมิได้มีการใช้สำหรับเก็บข้อมูลส่วนตัวของคุณ แต่อย่างไรก็ตาม หากก่อนหน้านี้คุณเคยกรอกข้อมูลส่วนตัวของคุณเอาไว้แล้ว คุกกี้ อาจจะจดจำข้อมูลเหล่านั้นเอาไว้และอาจมีการแบ่งปันผลรวมทั้งหมดของ คุกกี้ และ เทคโนโลยีจดจำข้อมูล กับบุคคลที่สามได้<br /><br /> <span style="font: bold 16px 'Tahoma';">การเปิดเผยข้อมูล </span> <br />Whatsnew อาจจะมีการเปิดเผยข้อมูลส่วนบุคคลตามคำร้องขอจากเจ้าหน้าที่ผู้รักษากฎหมาย หรือตามความจำเป็นอื่นๆ เพื่อให้สอดคล้องกับกฎหมายที่บังคับใช้ หรือกับบริษัทอื่นใดที่ทำหน้าที่เป็นผู้ช่วยเราในการสอบสวน ป้องกัน หรือเพื่อการดำเนินการใดๆ ต่อการกระทำที่ผิดกฎหมาย โดยเราอาจจะมีการเปิดเผยข้อมูลดังกรณีต่อไปนี้ (1) เมื่อมีการอนุญาตและร้องขอจากเจ้าหน้าที่ผู้รักษากฎหมาย หรือ (2) เมื่อมีความพยายามจะต่อต้าน หรือ ป้องกัน การฉ้อโกง หลอกลวง หรือ การดำเนินการสั่งซื้อโดยไม่ได้รับอนุญาต อันมีความเป็นไปได้ว่าจะเกิดขึ้น หรือ (3) เมื่อมีการสืบสวนสอบสวนการฉ้อโกงที่เกิดขึ้นแล้ว แต่ข้อมูลเหล่านี้จะไม่มีการนำไปให้บริษัทอื่นใดใช้เพื่อจุดประสงค์ทางการตลาด&nbsp;<br /><br /> <span style="font: bold 16px 'Tahoma';">ความปลอดภัยของข้อมูล </span> <br />ข้อมูลส่วนตัวของคุณที่เราทำการเก็บรักษาไว้นั้นมีการเก็บรักษาไว้อย่างเหมาะสม และปลอดภัย มีก็แต่เพียงพนักงานในบริษัทของเราที่ได้รับอนุญาต ตัวแทนของเรา หรือ ผู้รับจ้างของเรา (ผู้ซึ่งตกลงแล้วว่าจะเก็บรักษาข้อมูลเหล่านี้อย่างปลอดภัยและเป็นความลับ) เท่านั้นที่สามารถเข้าถึงข้อมูลเหล่านี้ได้ อีเมล์และจดหมายข่าวทุกฉบับจากเว็บไซต์ของเราจะเปิดโอกาสให้คุณเลือกที่จะยกเลิกการรับข่าวสารในอนาคตได้&nbsp;<br /><br /> <br /><br /> <span style="font: bold 16px 'Tahoma';">วิธีการยกเลิกรับข่าวสาร </span>
<div style="font: normal 14px 'Tahoma';">หากคุณไม่ต้องการรรับข่าวสารและโปรโมชั่นจากเรา คุณสามารถกดเพื่อยกเลิกรับข้อมูลข่าวสารได้ผ่านทางช่องทางต่างๆดังนี้
<p>- ยกเลิกการรับอีเมลผ่านจดหมายข่าวที่ได้รับ สามารถทำได้โดยการคลิกที่ปุ่ม ยกเลิกรับจดหมายข่าวสาร</p>
<p>- ยกเลิกการรับอีเมลผ่านหน้าเว็บไซต์</p>
<p>1. ล็อคอินผ่านหน้าเว็บไซต์ด้วยบัญชีที่คุณต้องการยกเลิกการรับข่าวสาร</p>
<p>2. เลือกที่ &ldquo;บัญชีผู้ใช้ของฉัน&rdquo;</p>
<p>3. เลือกเมนู &ldquo;สมัครสมาชิกจดหมายข่าว&rdquo; ซึ่งจะปรากฏอยู่ทางแถบเมนูด้านซ้ายมือของหน้าจอ</p>
<p>4.นำเครื่องหมายถูกออกจากช่อง &ldquo;การสมัครสมาชิกทั่วไป&rdquo;</p>
<p>5. กดปุ่ม &ldquo;บันทึก&rdquo;</p>
<p>เมื่อการยกเลิกเสร็จสมบูรณ์แล้ว คุณจะได้รับอีเมลเพื่อยืนยันการยกเลิกรับข้อมูลข่าวสารจากเรา</p>
</div>
<span style="font: bold 16px 'Tahoma';">ติดต่อสอบถามเกี่ยวกับนโยบายความเป็นส่วนตัว</span> <br />หากคุณมีข้อสงสัย คำถาม ข้อข้องใจ หรือ ความคิดเห็นใดๆ เกี่ยวกับนโยบายความเป็นส่วนตัวของเราคุณสามารถติดต่อเราได้ตามรายละเอียดด้านล่างนี้&nbsp;<br /> <br />
<p><strong>บริษัท Whatsnew จำกัด</strong></p>
<div style="font: normal 14px 'Tahoma'; line-height: 22px; color: #505050;">อาคารเศรษฐีวรรณ ชั้น 19<br /> 139 ถนนปั้น แขวงสีลม เขตบางรัก<br /> กรุงเทพฯ ประเทศไทย 10500<br /> โทรศัพท์: 02-106-8222</div>
<div style="font: normal 14px 'Tahoma'; line-height: 22px; color: #505050;">Facebook:&nbsp;<a href="https://www.facebook.com/SanogaThailand" target="_blank">https://www.facebook.com/SanogaThailand</a><br />อีเมล: <a href="mailto:support@sanoga.com">support@sanoga.com</a></div>
<div style="font: normal 14px 'Tahoma'; line-height: 22px; color: #505050;">&nbsp;</div>
<p><span lang="TH">บริษัท </span><span>Whatsnew <span lang="TH">ขอสงวนสิทธิ์ในการเปลี่ยนแปลงใดๆต่อนโยบายความเป็นส่วนตัวนี้ การเปลี่ยนแปลงใดๆที่จะเกิดขึ้นกับนโยบายความเป็นส่วนตัวนี้จะมีการประกาศแจ้งเอาไว้ในเว็บไซต์ของเรา</span></span></p>
</div>
EOD;

    $pageRootTemplate = 'one_column';
    $pageLayoutUpdateXml = <<<EOD
EOD;


    $pageCustomLayoutUpdateXML = <<<EOD
EOD;

    $pageMetaKeywords = "";
    $pageMetaDescription = "";

    $page = Mage::getModel('cms/page')->getCollection()
        ->addStoreFilter(array($storeIdMoxyTh), $withAdmin = true)
        ->addFieldToFilter('identifier', $pageIdentifier)
        ->getFirstItem();
    if ($page->getId() == 0) {
        $page = Mage::getModel('cms/page');
    }
    else
    {
        // if exists then repair
        $page = Mage::getModel('cms/page')->load($page->getId());
    }

    $page->setTitle($pageTitle);
    $page->setIdentifier($pageIdentifier);
    $page->setStores($pageStores);
    $page->setIsActive($pageIsActive);
    $page->setUnderVersionControl($pageUnderVersionControl);
    $page->setContentHeading($pageContentHeading);
    $page->setContent($pageContent);
    $page->setRootTemplate($pageRootTemplate);
    $page->setLayoutUpdateXml($pageLayoutUpdateXml);
    $page->setCustomLayoutUpdateXml($pageCustomLayoutUpdateXML);
    $page->setMetaKeywords($pageMetaKeywords);
    $page->setMetaDescription($pageMetaDescription);
    $page->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================

    //==========================================================================
    // Return Policy En Page
    //==========================================================================
    $pageTitle = "Return Policy";
    $pageIdentifier = "return-policy";
    $pageStores = array($storeIdMoxyEn);
    $pageIsActive = 1;
    $pageUnderVersionControl = 0;
    $pageContentHeading = "Return Policy";
    $pageContent = <<<EOD
<script type="text/javascript">// <![CDATA[
jQuery(document).ready(function()
{
jQuery('#small-box-links ul li a').click(function(){
jQuery('html, body').animate({
scrollTop: jQuery( jQuery.attr(this, 'href') ).offset().top
}, 500);
return false;
});
});
// ]]></script>
<div style="font-size: 16px; line-height: 22px;">We work very hard and do everything we can to make sure you are 100% satisfied with your shopping experience at Sanoga.com. However, if for some reason we have let you down or things did not go as planned, we have a hassle-free return policy and a dedicated team of folks to make it right.</div>
<div style="font-size: 16px; line-height: 22px;">&nbsp;</div>
<div id="small-box-container">
<div id="small-box1" class="small-box" style="font: bold 16px 'Tahoma'; color: #425d89; line-height: 22px;">Return Policy</div>
<div style="font-size: 16px; line-height: 22px;">At Sanoga, we take pride in our quality products and services and we value our relationship with you. If at any time you feel our products do not meet your needs, we will gladly accept returns of items(s) by following our return policy.</div>
<br />
<ul style="list-style: square; margin-left: 40px; font-size: 16px; line-height: 22px;">
<li>We only accept the product that fall into following categories: defects occurred from the manufacturer, product is expired, product received does not match the order placed, product damaged during the shipping process.</li>
</ul>
</div>
<p>&nbsp;</p>
<div id="small-box2" class="small-box" style="font: bold 16px 'Tahoma'; color: #425d89; line-height: 22px;">Following the Return Policy above, please check your order upon delivery. If you find that the product does not match the order, expired, or defected, please reject and do not sign for the delivery. Please contact our customer care team as soon as possible to request a refund.</div>
<p>&nbsp;</p>
<div style="font-size: 16px; line-height: 22px;">
<p><span style="font: bold 16px 'Tahoma'; color: #425d89;">Contact us</span><br /> Customer Care Email: <a href="mailto:service@sanoga.com">service@sanoga.com</a><br /> Customer Care: Phone: 02-106-8222<br /> Customer Care Hours: 10:00-19:00 Mon-Sat</p>
</div>
EOD;

    $pageRootTemplate = 'one_column';
    $pageLayoutUpdateXml = <<<EOD
EOD;


    $pageCustomLayoutUpdateXML = <<<EOD
EOD;

    $pageMetaKeywords = "";
    $pageMetaDescription = "";

    $page = Mage::getModel('cms/page')->getCollection()
        ->addStoreFilter(array($storeIdMoxyEn), $withAdmin = true)
        ->addFieldToFilter('identifier', $pageIdentifier)
        ->getFirstItem();
    if ($page->getId() == 0) {
        $page = Mage::getModel('cms/page');
    }
    else
    {
        // if exists then repair
        $page = Mage::getModel('cms/page')->load($page->getId());
    }

    $page->setTitle($pageTitle);
    $page->setIdentifier($pageIdentifier);
    $page->setStores($pageStores);
    $page->setIsActive($pageIsActive);
    $page->setUnderVersionControl($pageUnderVersionControl);
    $page->setContentHeading($pageContentHeading);
    $page->setContent($pageContent);
    $page->setRootTemplate($pageRootTemplate);
    $page->setLayoutUpdateXml($pageLayoutUpdateXml);
    $page->setCustomLayoutUpdateXml($pageCustomLayoutUpdateXML);
    $page->setMetaKeywords($pageMetaKeywords);
    $page->setMetaDescription($pageMetaDescription);
    $page->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================

    //==========================================================================
    // Return Policy TH Page
    //==========================================================================
    $pageTitle = "นโยบายการส่งคืนสินค้า เงื่อนไขการรับคืนสินค้า | Moxy Thailand";
    $pageIdentifier = "return-policy";
    $pageStores = array($storeIdMoxyTh);
    $pageIsActive = 1;
    $pageUnderVersionControl = 0;
    $pageContentHeading = "นโยบายการส่งคืนสินค้า เงื่อนไขการรับคืนสินค้า";
    $pageContent = <<<EOD
<script type="text/javascript">// <![CDATA[
jQuery(document).ready(function()
{
jQuery('#small-box-links ul li a').click(function(){
jQuery('html, body').animate({
scrollTop: jQuery( jQuery.attr(this, 'href') ).offset().top
}, 500);
return false;
});
});
// ]]></script>
<div style="font-size: 16px; line-height: 22px;">เราทุ่มเทและทำงานอย่างหนักที่จะพยายามพัฒนาระบบทุกอย่าง เพื่อให้มั่นใจร้อยเปอร์เซ็นต์ว่าคุณจะได้พบประสบการณ์ที่ยอดเยี่ยมในการช็อปปิ้งผ่าน Sanoga.com เราหวังเป็นอย่างยิ่งว่าคุณจะกลับ มาใช้บริการของเราอย่างต่อเนื่อง และบอกต่อถึงบริการอันยอดเยี่ยมนี้แก่เพื่อนของคุณ หากเกิดกรณีสุดวิสัยที่จะทำให้เกิดความผิดพลาดเกี่ยวกับสินค้าในขั้นตอนการจัดส่งเรามีนโยบายที่จะทำการรับคืนสินค้าโดยคำนึงถึงความสะดวกและยุติธรรมแก่ลูกค้ามากที่สุด&nbsp;</div>
<div style="font-size: 16px; line-height: 22px;">&nbsp;</div>
<div id="small-box-container">
<div id="small-box1" class="small-box" style="font: bold 16px 'Tahoma'; color: #425d89; line-height: 22px;">นโยบายการคืนสินค้าและขอคืนเงิน</div>
<div style="font-size: 16px; line-height: 22px;">ที่ Sanoga เรามีความภูมิใจในมาตรฐานของสินค้าและบริการอันยอดเยี่ยม เรายินดีที่จะรับคืนสินค้าเหล่านั้นภายใต้เงื่อนไขที่ระบุไว้ตามนโยบายการคืนสินค้า</div>
<br />
<div style="font-size: 16px; line-height: 22px;">เรารับคืนสินค้าตามเงื่อนไขดังต่อไปนี้เท่านั้น</div>
<ul style="list-style: square; margin-left: 40px; font-size: 16px; line-height: 22px;">
<li>สินค้ามีความบกพร่องเสียหายอันเกิดจากผู้ผลิต, สินค้าหมดอายุ, ลูกค้าได้รับสินค้าไม่ตรงตามใบสั่งซื้อ, สินค้าได้รับความเสียหายในระหว่างการจัดส่ง<strong></strong></li>
</ul>
</div>
<p>&nbsp;</p>
<div id="small-box2" class="small-box" style="font: bold 16px 'Tahoma'; color: #425d89; line-height: 22px;">ตามนโยบายการคืนสินค้าที่กำหนดข้างต้น คุณสามารถขอคืนสินค้าได้โดยการตรวจสอบความเรียบร้อยของสินค้าที่ได้รับทันทีเมื่อได้รับสินค้า หากปรากฏว่าสินค้าที่ได้รับไม่ตรงตามใบสั่ง หมดอายุ หรือเสียหายจากการจัดส่ง กรุณาปฏิเสธและงดการเซ็นรับสินค้านั้นๆ และติดต่อเจ้าหน้าที่แผนกลูกค้าสัมพันธ์เพื่อดำเนินการขอเงินคืนทันที</div>
<p>&nbsp;</p>
<div style="font-size: 16px; line-height: 22px;">
<p><span style="font: bold 16px 'Tahoma'; color: #425d89;">ติดต่อเรา</span><br /> แผนกลูกค้าสัมพันธ์<br /> อีเมล <a href="mailto:support@sanoga.com">support@sanoga.com</a><br /> โทรศัพท์ : 02-106-8222 <br /> เวลาทำการ : วันจันทร์ &ndash; เสาร์ 10:00 น. - 19:00 น.</p>
</div>
EOD;

    $pageRootTemplate = 'one_column';
    $pageLayoutUpdateXml = <<<EOD
EOD;


    $pageCustomLayoutUpdateXML = <<<EOD
EOD;

    $pageMetaKeywords = "";
    $pageMetaDescription = "";

    $page = Mage::getModel('cms/page')->getCollection()
        ->addStoreFilter(array($storeIdMoxyTh), $withAdmin = true)
        ->addFieldToFilter('identifier', $pageIdentifier)
        ->getFirstItem();
    if ($page->getId() == 0) {
        $page = Mage::getModel('cms/page');
    }
    else
    {
        // if exists then repair
        $page = Mage::getModel('cms/page')->load($page->getId());
    }

    $page->setTitle($pageTitle);
    $page->setIdentifier($pageIdentifier);
    $page->setStores($pageStores);
    $page->setIsActive($pageIsActive);
    $page->setUnderVersionControl($pageUnderVersionControl);
    $page->setContentHeading($pageContentHeading);
    $page->setContent($pageContent);
    $page->setRootTemplate($pageRootTemplate);
    $page->setLayoutUpdateXml($pageLayoutUpdateXml);
    $page->setCustomLayoutUpdateXml($pageCustomLayoutUpdateXML);
    $page->setMetaKeywords($pageMetaKeywords);
    $page->setMetaDescription($pageMetaDescription);
    $page->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================

    $installer->endSetup();
} catch (Excpetion $e) {
    Mage::logException($e);
    Mage::log("ERROR IN SETUP " . $e->getMessage());
}
