<?php

// Base-name
$name           = 'moxy';
$engStoreName   = 'Moxy English';
$engStoreCode   = 'moxyen';
$thStoreName    = 'Moxy Thai';
$thStoreCode    = 'moxyth';
$storeIdMoxyEn  = Mage::getModel('core/store')->load($engStoreCode, 'code')->getId();
$storeIdMoxyTh  = Mage::getModel('core/store')->load($thStoreCode, 'code')->getId();

try {
    $installer = $this;
    $installer->startSetup();

    // Force the store to be admin
    Mage::app()->setUpdateMode(false);
    Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
    if (!Mage::registry('isSecureArea'))
        Mage::register('isSecureArea', 1);


    //==========================================================================
    // Reorder EN block
    //==========================================================================
    $blockTitle = "Reorder EN";
    $blockIdentifier = "reorder";
    $blockStores = array('13','1','10','5',"$storeIdMoxyEn");
    $blockIsActive = 1;
    $blockContent = <<<EOD
<p><img style="width: 100%;" src="{{media url="wysiwyg/FastReorder_Page-01.jpg"}}" alt="" /></p>
EOD;
    $block = Mage::getModel('cms/block')->getCollection()
        ->addStoreFilter(array('13','1','10','5',"$storeIdMoxyEn"), $withAdmin = false)
        ->addFieldToFilter('identifier', $blockIdentifier)
        ->getFirstItem();
    if ($block->getId() == 0) {
        $block = Mage::getModel('cms/block');
    } else { // if exists then delete
        $block->delete();
        $block = Mage::getModel('cms/block');
    }
    $block->setTitle($blockTitle);
    $block->setIdentifier($blockIdentifier);
    $block->setStores($blockStores);
    $block->setIsActive($blockIsActive);
    $block->setContent($blockContent);
    $block->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================

    //==========================================================================
    // Reorder TH block
    //==========================================================================
    $blockTitle = "Reorder TH";
    $blockIdentifier = "reorder";
    $blockStores = array('15','2','11','4',"$storeIdMoxyTh");
    $blockIsActive = 1;
    $blockContent = <<<EOD
<p><img style="width: 100%;" src="{{media url="wysiwyg/FastReorder_Page_THAI-01.jpg"}}" alt="" /></p>
EOD;
    $block = Mage::getModel('cms/block')->getCollection()
        ->addStoreFilter(array('15','2','11','4',"$storeIdMoxyTh"), $withAdmin = false)
        ->addFieldToFilter('identifier', $blockIdentifier)
        ->getFirstItem();
    if ($block->getId() == 0) {
        $block = Mage::getModel('cms/block');
    } else { // if exists then delete
        $block->delete();
        $block = Mage::getModel('cms/block');
    }
    $block->setTitle($blockTitle);
    $block->setIdentifier($blockIdentifier);
    $block->setStores($blockStores);
    $block->setIsActive($blockIsActive);
    $block->setContent($blockContent);
    $block->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================

    //==========================================================================
    // mycart_content EN block
    //==========================================================================
    $blockTitle = "mycart_content en";
    $blockIdentifier = "mycart_content";
    $blockStores = array('1','10','5',"$storeIdMoxyEn");
    $blockIsActive = 1;
    $blockContent = <<<EOD
<div id="mycart_content">
<ul>
<li><a style="color: #000; text-decoration: none;" href="{{store url='help'}}" target="_blank">Free Shipping</a></li>
<li><a style="color: #000; text-decoration: none;" href="{{store url='help'}}#payments" target="_blank">Pay Cash on Delivery</a></li>
<li><a style="color: #000; text-decoration: none;" href="{{store url='help'}}#returns" target="_blank">30 Day Free Returns</a></li>
<li><a style="color: #000; text-decoration: none;" href="{{store url='help'}}#payments" target="_blank">100% Secured Checkout</a></li>
</ul>
</div>
EOD;
    $block = Mage::getModel('cms/block')->getCollection()
        ->addStoreFilter(array('1','10','5',"$storeIdMoxyEn"), $withAdmin = false)
        ->addFieldToFilter('identifier', $blockIdentifier)
        ->getFirstItem();
    if ($block->getId() == 0) {
        $block = Mage::getModel('cms/block');
    } else { // if exists then delete
        $block->delete();
        $block = Mage::getModel('cms/block');
    }
    $block->setTitle($blockTitle);
    $block->setIdentifier($blockIdentifier);
    $block->setStores($blockStores);
    $block->setIsActive($blockIsActive);
    $block->setContent($blockContent);
    $block->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================

    //==========================================================================
    // mycart_content TH block
    //==========================================================================
    $blockTitle = "mycart_content th";
    $blockIdentifier = "mycart_content";
    $blockStores = array('2','11','4',"$storeIdMoxyTh");
    $blockIsActive = 1;
    $blockContent = <<<EOD
<div id="mycart_content">
<ul>
<li><a style="color: #000; text-decoration: none;" href="{{store url='help'}}" target="_blank">ฟรีค่าจัดส่ง</a></li>
<li><a style="color: #000; text-decoration: none;" href="{{store url='help'}}#payments" target="_blank">ชำระเงินเมื่อได้รับสินค้า</a></li>
<li><a style="color: #000; text-decoration: none;" href="{{store url='help'}}#returns" target="_blank">รับคืนฟรี ภายใน 30 วัน</a></li>
<li><a style="color: #000; text-decoration: none;" href="{{store url='help'}}#payments" target="_blank">ระบบชำระเงินปลอดภัย 100%</a></li>
</ul>
</div>
EOD;
    $block = Mage::getModel('cms/block')->getCollection()
        ->addStoreFilter(array('2','11','4',"$storeIdMoxyTh"), $withAdmin = false)
        ->addFieldToFilter('identifier', $blockIdentifier)
        ->getFirstItem();
    if ($block->getId() == 0) {
        $block = Mage::getModel('cms/block');
    } else { // if exists then delete
        $block->delete();
        $block = Mage::getModel('cms/block');
    }
    $block->setTitle($blockTitle);
    $block->setIdentifier($blockIdentifier);
    $block->setStores($blockStores);
    $block->setIsActive($blockIsActive);
    $block->setContent($blockContent);
    $block->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================

    //==========================================================================
    // All Brands EN block
    //==========================================================================
    $blockTitle = "All Brands";
    $blockIdentifier = "all-brands";
    $blockStores = array('1','10','5',"$storeIdMoxyEn");
    $blockIsActive = 1;
    $blockContent = <<<EOD
<p>{{block type="core/template" name="all-brands" template="brands/all.phtml"}}</p>
<p>{{block type="cms/block" block_id="banners_home"}}</p>
<script type="text/javascript">// <![CDATA[
	jQuery(document).ready(function() {
		jQuery('ul#main-nav li:last').addClass('active');
	});
// ]]></script>
EOD;
    $block = Mage::getModel('cms/block')->getCollection()
        ->addStoreFilter(array('1','10','5',"$storeIdMoxyEn"), $withAdmin = false)
        ->addFieldToFilter('identifier', $blockIdentifier)
        ->getFirstItem();
    if ($block->getId() == 0) {
        $block = Mage::getModel('cms/block');
    } else { // if exists then delete
        $block->delete();
        $block = Mage::getModel('cms/block');
    }
    $block->setTitle($blockTitle);
    $block->setIdentifier($blockIdentifier);
    $block->setStores($blockStores);
    $block->setIsActive($blockIsActive);
    $block->setContent($blockContent);
    $block->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================

    //==========================================================================
    // All Brands Page
    //==========================================================================
    $pageTitle = "All Brands";
    $pageIdentifier = "all-brands";
    $pageStores = array("$storeIdMoxyEn", "$storeIdMoxyTh");
    $pageIsActive = 1;
    $pageUnderVersionControl = 0;
    $pageContentHeading = "";
    $pageContent = <<<EOD
<p>{{block type="core/template" name="all-brands" template="brands/all.phtml"}}</p>
<p>{{block type="cms/block" block_id="banners_home"}}</p>
<script type="text/javascript">// <![CDATA[
	jQuery(document).ready(function() {
		jQuery('ul#main-nav li:last').addClass('active');
	});
// ]]></script>
EOD;

    $pageRootTemplate = 'one_column';
    $pageLayoutUpdateXml = <<<EOD
EOD;


    $pageCustomLayoutUpdateXML = <<<EOD
EOD;

    $pageMetaKeywords = "";
    $pageMetaDescription = "";

    $page = Mage::getModel('cms/page')->getCollection()
        ->addStoreFilter(array("$storeIdMoxyEn", "$storeIdMoxyTh"), $withAdmin = true)
        ->addFieldToFilter('identifier', $pageIdentifier)
        ->getFirstItem();
    if ($page->getId() == 0) {
        $page = Mage::getModel('cms/page');
    }
    else
    {
        // if exists then repair
        $page = Mage::getModel('cms/page')->load($page->getId());
    }

    $page->setTitle($pageTitle);
    $page->setIdentifier($pageIdentifier);
    $page->setStores($pageStores);
    $page->setIsActive($pageIsActive);
    $page->setUnderVersionControl($pageUnderVersionControl);
    $page->setContentHeading($pageContentHeading);
    $page->setContent($pageContent);
    $page->setRootTemplate($pageRootTemplate);
    $page->setLayoutUpdateXml($pageLayoutUpdateXml);
    $page->setCustomLayoutUpdateXml($pageCustomLayoutUpdateXML);
    $page->setMetaKeywords($pageMetaKeywords);
    $page->setMetaDescription($pageMetaDescription);
    $page->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================

    $installer->endSetup();
} catch (Excpetion $e) {
    Mage::logException($e);
    Mage::log("ERROR IN SETUP " . $e->getMessage());
}
