<?php

// Base-name
$name           = 'moxy';
$engStoreName   = 'Moxy English';
$engStoreCode   = 'moxyen';
$thStoreName    = 'Moxy Thai';
$thStoreCode    = 'moxyth';
$storeIdMoxyEn  = Mage::getModel('core/store')->load($engStoreCode, 'code')->getId();
$storeIdMoxyTh  = Mage::getModel('core/store')->load($thStoreCode, 'code')->getId();
$newProductsCateId = 0;
$bestSellersCateId = 0;
try {
    $installer = $this;
    $installer->startSetup();


    //==========================================================================
    // Banks Footer block
    //==========================================================================
    $blockTitle = "Banks Footer";
    $blockIdentifier = "banks_footer";
    $blockStores = array("$storeIdMoxyEn","$storeIdMoxyTh");
    $blockIsActive = 1;
    $blockContent = <<<EOD
<p><a href="https://plus.google.com/+Moxyst" target="_blank"> <span class="google-footer"><img src="{{media url="wysiwyg/footer_follow/google_follow.png"}}" alt="" width="32px" /></span> </a> <a href="https://twitter.com/moxyst" target="_blank"> <span class="twitter-footer"><img src="{{media url="wysiwyg/footer_follow/twitter_follow.png"}}" alt="" width="32px" /></span> </a> <a href="https://instagram.com/moxyst/" target="_blank"> <span class="instagram-footer"><img src="{{media url="wysiwyg/footer_follow/instagram_follow.png"}}" alt="" width="32px" /></span> </a> <a href="https://www.youtube.com/user/moxyst" target="_blank"> <span class="youtube-footer"><img src="{{media url="wysiwyg/footer_follow/youtube_follow.png"}}" alt="" width="32px" /></span> </a> <a href="https://www.facebook.com/moxyst" target="_blank"> <span class="facebook-footer"><img src="{{media url="wysiwyg/footer_follow/facebook_follow.png"}}" alt="" width="32px" /></span> </a></p>
EOD;
    $block = Mage::getModel('cms/block')->getCollection()
        ->addStoreFilter(array($storeIdMoxyEn), $withAdmin = false)
        ->addFieldToFilter('identifier', $blockIdentifier)
        ->getFirstItem();
    if ($block->getId() == 0) {
        $block = Mage::getModel('cms/block');
    } else { // if exists then delete
        $block->delete();
        $block = Mage::getModel('cms/block');
    }
    $block->setTitle($blockTitle);
    $block->setIdentifier($blockIdentifier);
    $block->setStores($blockStores);
    $block->setIsActive($blockIsActive);
    $block->setContent($blockContent);
    $block->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================

    $installer->endSetup();
} catch (Excpetion $e) {
    Mage::logException($e);
    Mage::log("ERROR IN SETUP " . $e->getMessage());
}
