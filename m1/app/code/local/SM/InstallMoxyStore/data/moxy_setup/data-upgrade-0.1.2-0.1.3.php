<?php

// Base-name
$name           = 'moxy';
$engStoreName   = 'Moxy English';
$engStoreCode   = 'moxyen';
$thStoreName    = 'Moxy Thai';
$thStoreCode    = 'moxyth';
$storeIdMoxyEn  = Mage::getModel('core/store')->load($engStoreCode, 'code')->getId();
$storeIdMoxyTh  = Mage::getModel('core/store')->load($thStoreCode, 'code')->getId();
$newProductsCateId = 0;
$bestSellersCateId = 0;
try {
    $installer = $this;
    $installer->startSetup();

    // Force the store to be admin
    Mage::app()->setUpdateMode(false);
    Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
    if (!Mage::registry('isSecureArea'))
        Mage::register('isSecureArea', 1);

    // create category "New Products", and "Best Sellers"
    $category = Mage::getModel('catalog/category');
    $categories = Mage::getModel('catalog/category')->getCollection()
        ->addAttributeToSelect('*')
        ->addAttributeToFilter('name', ucfirst($name))
        ->addAttributeToFilter('parent_id', 1)
        ->addAttributeToFilter('level', 1);
    if($categories->count() > 0 && $categories->count() < 2){
        $category = $categories->getFirstItem();
    }
    if($categoryId = $category->getId()){
        $child = $category->getChildrenCategories();
        $arrSubCateNames = array();
        foreach($child as $cate){
            $arrSubCateNames[$cate->getId()] = $cate->getName();
        }
        // New Products
        if(!in_array('New Products', $arrSubCateNames)){
            $newCate = Mage::getModel('catalog/category');
            $newCate->setStoreId(Mage_Core_Model_App::ADMIN_STORE_ID);
            $newCate->setData('name', 'New Products');
            $newCate->setData('url_key', 'new-products');
            $newCate->setData('display_mode', 'PRODUCTS');
            $newCate->setData('is_active', 1);
            $newCate->setData('include_in_menu', 0);
            $newCate->setData('is_anchor', 0);
            $newCate->setData('custom_use_parent_settings', 0);
            $newCate->setData('custom_apply_to_products', 0);
            $newCate->setData('attribute_set_id', $newCate->getDefaultAttributeSetId());
            $newCate->setParentId($categoryId);
            $newCate->setPath($category->getPath());
            $newCate->save();
        }else{
            $newProductsCateId = array_search('New Products', $arrSubCateNames);
        }
        // Best Sellers
        if(!in_array('Best Sellers', $arrSubCateNames)){
            $bestCate = Mage::getModel('catalog/category');
            $bestCate->setStoreId(Mage_Core_Model_App::ADMIN_STORE_ID);
            $bestCate->setData('name', 'Best Sellers');
            $bestCate->setData('url_key', 'best-sellers');
            $bestCate->setData('display_mode', 'PRODUCTS');
            $bestCate->setData('is_active', 1);
            $bestCate->setData('include_in_menu', 0);
            $bestCate->setData('is_anchor', 0);
            $bestCate->setData('custom_use_parent_settings', 0);
            $bestCate->setData('custom_apply_to_products', 0);
            $bestCate->setData('attribute_set_id', $bestCate->getDefaultAttributeSetId());
            $bestCate->setParentId($categoryId);
            $bestCate->setPath($category->getPath());
            $bestCate->save();
        }else{
            $bestSellersCateId = array_search('Best Sellers', $arrSubCateNames);
        }
    }


    //==========================================================================
    // CMS Page: home (EN)
    //==========================================================================
    $enPageContent = <<<EOD
<p>{{block type="core/template" name="subscription" as="subscription" template="subscription/subscribe.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="$newProductsCateId" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="$bestSellersCateId" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="cms/block" block_id="all-brands-slider"}}</p>
<h2>&nbsp;</h2>
<div style="text-align: center;"><a title="Shop at PetLoft" onclick="_gaq.push(['_trackEvent', 'PetLoft Banner', 'Click', 'PetLoft Home Banner - Bottom',1,true])" href="http://www.petloft.com/"><img src="{{media url="wysiwyg/petloft-bottom-of-page.jpg"}}" alt="Shop pet products at PetLoft" /></a><a title="Shop at Venbi" onclick="_gaq.push(['_trackEvent', 'Venbi Banner', 'Click', 'Venbi Home Banner - Bottom',1,true])" href="http://www.venbi.com/"><img src="{{media url="wysiwyg/venbi-bottom-of-page.jpg"}}" style="padding: 0 10px;" alt="Shop baby and mom products at Venbi" /></a><a title="Shop at Lafema" onclick="_gaq.push(['_trackEvent', 'Lafema Banner', 'Click', 'Lafema Home Banner - Bottom',1,true])" href="http://www.lafema.com/"><img src="{{media url="wysiwyg/lafema-bottom-banner.jpg"}}" alt="Shop cosmetics at Lafema" /></a></div>
<h2>&nbsp;</h2>
<h2>SANOGA &ndash; Thailand&rsquo;s number 1 online HEALTH store</h2>
<p>&nbsp;</p>
<p>Welcome to Thailand&rsquo;s largest <strong>online health store</strong>. No more looking around for <strong>the best deal</strong>, SANOGA is a one-stop health store where you can find 100% <strong>genuine products</strong> from household names. It is cheaper, easier and you can select what&rsquo;s best for you 24/7 with just one click!</p>
<p>&nbsp;</p>
<p>SANOGA offers over 1,000 Health products from well-known brands including <strong>Health Care</strong>, <strong>Vitamins</strong>, <strong>Collagen</strong>, <strong>Beauty</strong>, <strong>Sexual Well-being</strong>, <strong>Workout &amp; Fitness</strong>, <strong>Weight Loss</strong> including Blackmores, Beauty Wise, Body Shape, CLG500, Centrum, Collagen Star, Colly, Dr.Absolute, DYMATIZE, Durex, Eucerin, Genesis, Hi-Balanz, K-Palette, Meiji, Mega We Care, Okamoto, ProFlex, Real Elixir, Sebamed, Seoul Secret, Taurus, etc.</p>
<p>&nbsp;</p>
<p>All products on SANOGA is certified by FDA Thailand so you can trust in quality and safety.</p>
<p>&nbsp;</p>
<p>Enjoy shopping worry-free with our safe and convenient payment methods including credit card, Paypal, or <strong>Cash-on-Delivery</strong>. So no more running around, all you need to do is click to order and wait for the delivery to turn up at your doorstep in just 2-3 days. It&rsquo;s easier, cheaper and more convenient!</p>
EOD;


    $enPageLayoutXml = <<<EOD
<reference name="header">
    <block type="cms/block" name="slider_1">
        <!--
        The content of this block is taken from the database by its block_id.
        You can manage it in admin CMS -> Static Blocks
        -->
        <action method="setBlockId"><block_id>slider_1</block_id></action>
    </block>
    <block type="cms/block" name="banners_home">
        <!--
        The content of this block is taken from the database by its block_id.
        You can manage it in admin CMS -> Static Blocks
        -->
        <action method="setBlockId"><block_id>banners_home</block_id></action>
    </block>
</reference>
<reference name="head">
    <block type="core/text" name="homepage.metadata" after="-">
        <action method="addText"><text><![CDATA[<meta name="google-site-verification" content="_4n5alA2_c2rhcm4BQTl-KqLMwVGw_gyHr20g1fCalA" />]]></text></action>
    </block>
</reference>
EOD;

    $enHomePage = Mage::getModel('cms/page');
    $enData = array(
        'title' => 'MOXY - Online Lifestyle Shopping',
        'identifier' => 'home',
        'stores' => array($storeIdMoxyEn),
        'is_active' => 1,
        'content' => $enPageContent,
        'root_template' => 'one_column',
        'layout_update_xml' => $enPageLayoutXml
    );
    $enHomePage->setData($enData);
    $enHomePage->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================


    //==========================================================================
    // CMS Page: home (TH)
    //==========================================================================
    $thPageContent = <<<EOD
<p>{{block type="core/template" name="subscription" as="subscription" template="subscription/subscribe.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="$newProductsCateId" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="$bestSellersCateId" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="cms/block" block_id="all-brands-slider"}}</p>
<h2>&nbsp;</h2>
<div style="text-align: center;"><a title="Shop at PetLoft" onclick="_gaq.push(['_trackEvent', 'PetLoft Banner', 'Click', 'PetLoft Home Banner - Bottom',1,true])" href="http://www.petloft.com/"><img src="http://whatsnew.local/index.php/admin/cms_wysiwyg/directive/___directive/e3ttZWRpYSB1cmw9Ind5c2l3eWcvcGV0bG9mdC1ib3R0b20tb2YtcGFnZS5qcGcifX0,/key/32cff8b807075c22a1b1c488ed9f6704/" alt="Shop pet products at PetLoft" /></a><a title="Shop at Venbi" onclick="_gaq.push(['_trackEvent', 'Venbi Banner', 'Click', 'Venbi Home Banner - Bottom',1,true])" href="http://www.venbi.com/"><img src="http://whatsnew.local/index.php/admin/cms_wysiwyg/directive/___directive/e3ttZWRpYSB1cmw9Ind5c2l3eWcvdmVuYmktYm90dG9tLW9mLXBhZ2UuanBnIn19/key/32cff8b807075c22a1b1c488ed9f6704/" style="padding: 0 10px;" alt="Shop baby and mom products at Venbi" /></a><a title="Shop at Lafema" onclick="_gaq.push(['_trackEvent', 'Lafema Banner', 'Click', 'Lafema Home Banner - Bottom',1,true])" href="http://www.lafema.com/"><img src="http://whatsnew.local/index.php/admin/cms_wysiwyg/directive/___directive/e3ttZWRpYSB1cmw9Ind5c2l3eWcvbGFmZW1hLWJvdHRvbS1iYW5uZXIuanBnIn19/key/32cff8b807075c22a1b1c488ed9f6704/" alt="Shop cosmetics at Lafema" /></a></div>
<h2>&nbsp;</h2>
<p><strong><span style="font-size: small;">SANOGA &ndash;&nbsp;สินค้าเพื่อสุขภาพและความงาม อาหารเสริม วิตามิน เครื่องสำอาง พร้อมโปรโมชั่นสุดพิเศษ</span></strong></p>
<p><strong><span style="font-size: small;"><br /></span></strong></p>
<p>SANOGA คือศูนย์รวมผลิตภัณฑ์และจัดจำหน่ายสินค้าด้าน<strong>สุขภาพและความงาม</strong> ที่ใหญ่ที่สุดในประเทศไทย ทั้ง<strong>อาหารเสริม</strong><strong> วิตามิน </strong><strong>คอลลาเจน</strong><strong> ผลิตภัณฑ์เพื่อ</strong><strong>ความงามและการดูแลสุขภาพ</strong><strong> ผลิตภัณฑ์</strong><strong>เสริมสมรรถภาพร่างกาย</strong><strong> </strong>และ<strong>เครื่องสำอาง</strong> คุณภาพแท้ 100% จากแบรนด์ชั้นนำทั่วโลก <strong>ในราคาที่ถูกที่สุด</strong> คุณสามารถใช้เวลาได้อย่างเต็มที่ในการเลือกผลิตภัณฑ์ที่ต้องการ เราได้จัดทำข้อมูลอย่างละเอียดสำหรับสินค้าแต่ละชนิด พร้อมทั้งวิธีการสั่งซื้อที่สะดวกสบายตลอด 24 ชั่วโมง ทำให้การดูแลสุขภาพของคุณครั้งนี้ง่ายขึ้นกว่าเดิม</p>
<p><strong><br /></strong></p>
<p><strong>หลากหลายแบรนด์ชั้นนำที่ขายดี และได้รับความนิยม</strong></p>
<p>&nbsp;</p>
<p>SANOGA ผู้นำด้านการ<strong>ช้อปปิ้งออนไลน์</strong>ในประเทศไทย<strong> </strong>ภูมิใจเสนอสินค้าเพื่อ<strong>สุขภาพและความงาม</strong>ภายใต้แบรนด์ดังระดับโลกกว่า 1,000 รายการ อาทิ BLACKMORES, Beauty Wise, Body Shape, Brand's, CLG 500, Centrum, Collagen Star, Colly, Dr.Absolute, DYMATIZE, Durex, Eucerin, Genesis, Hi-Balanz, K-Palette, Meiji, Mega We Care, Okamoto, ProFlex, Real Elixir, Sebamed, Seoul Secret, Taurus และอื่นๆ อีกมากมาย</p>
<p><strong><br /></strong></p>
<p><strong>มั่นใจได้ในคุณภาพและความปลอดภัย</strong><strong></strong></p>
<p>&nbsp;</p>
<p>สินค้าทุกชิ้นของ SANOGA ได้รับมาตรฐานการรับรองจากองค์การอาหารและยาแห่งประเทศไทย (อย.) จึงมั่นใจได้ใน<strong>คุณภาพ</strong>และ<strong>ความปลอดภัย</strong></p>
<p><strong><br /></strong></p>
<p><strong>สะดวก รวดเร็ว ทันใจ</strong><strong></strong></p>
<p>&nbsp;</p>
<p>เรามีบริการจัดส่งโดยที่คุณไม่ต้องเสียเวลาเดินหาซื้อและขนสินค้ากลับเองให้เมื่อยอีกต่อไป เพียงแค่เลือกสินค้าในเว็บไซต์ เลือก<a href="http://www.sanoga.com/th/help/#payments">การชำระเงิน</a>ด้วยวีธีที่ต้องการ เช่น ชำระผ่านบัตรเครดิต หรือ<strong>ชำระเงินปลายทาง</strong>กับพนักงานจัดส่งสินค้า เมื่อเลือกเสร็จแล้ว ก็รอรับสินค้าที่<strong>ส่งตรงถึงหน้าบ้าน</strong>ภายใน 1-2 วัน ได้เลย</p>
<p>&nbsp;</p>
<p>{{block type="cms/block" block_id="banner_footer"}}</p>
EOD;


    $thPageLayoutXml = <<<EOD
<reference name="header">
    <block type="cms/block" name="slider_1">
        <!--
        The content of this block is taken from the database by its block_id.
        You can manage it in admin CMS -> Static Blocks
        -->
        <action method="setBlockId"><block_id>slider_1</block_id></action>
    </block>
    <block type="cms/block" name="banners_home">
        <!--
        The content of this block is taken from the database by its block_id.
        You can manage it in admin CMS -> Static Blocks
        -->
        <action method="setBlockId"><block_id>banners_home</block_id></action>
    </block>
</reference>
<reference name="head">
    <block type="core/text" name="homepage.metadata" after="-">
        <action method="addText"><text><![CDATA[<meta name="google-site-verification" content="_4n5alA2_c2rhcm4BQTl-KqLMwVGw_gyHr20g1fCalA" />]]></text></action>
    </block>
</reference>
EOD;

    $thHomePage = Mage::getModel('cms/page');
    $thData = array(
        'title' => 'MOXY - Online Lifestyle Shopping (Thai)',
        'identifier' => 'home',
        'stores' => array($storeIdMoxyTh),
        'is_active' => 1,
        'content' => $thPageContent,
        'root_template' => 'one_column',
        'layout_update_xml' => $thPageLayoutXml
    );
    $thHomePage->setData($thData);
    $thHomePage->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================


    $installer->endSetup();
} catch (Excpetion $e) {
    Mage::logException($e);
    Mage::log("ERROR IN SETUP " . $e->getMessage());
}
