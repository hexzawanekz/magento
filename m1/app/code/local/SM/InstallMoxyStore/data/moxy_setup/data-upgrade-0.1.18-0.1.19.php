<?php

$order      = '$order';
$invoice    = '$invoice';
$shipment   = '$shipment';
$creditmemo = '$creditmemo';

/*new order Moxy en*/
$template_text = <<<HTML
<!--@subject {{var store.getFrontendName()}}: New Order # {{var order.increment_id}} @-->
<!--@vars
{"store url=\"\"":"Store Url",
"var logo_url":"Email Logo Image Url",
"var logo_alt":"Email Logo Image Alt",
"htmlescape var=$order.getCustomerName()":"Customer Name",
"var store.getFrontendName()":"Store Name",
"store url=\"customer/account/\"":"Customer Account Url",
"var order.increment_id":"Order Id",
"var order.getCreatedAtFormated('long')":"Order Created At (datetime)",
"var order.getBillingAddress().format('html')":"Billing Address",
"var payment_html":"Payment Details",
"var order.getShippingAddress().format('html')":"Shipping Address",
"var order.getShippingDescription()":"Shipping Description",
"layout handle=\"sales_email_order_items\" order=$order":"Order Items Grid",
"var order.getEmailCustomerNote()":"Email Order Note"}
@-->
<!--@styles
body,td { color:#2f2f2f; font:11px/1.35em Verdana, Arial, Helvetica, sans-serif; }
@-->

<body style="background:#e3e3e3; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; margin:0; padding:0;">
<div style="background:#e3e3e3; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; margin:0; padding:0;">
<table cellspacing="0" cellpadding="0" border="0" width="100%">
<tr>
    <td align="center" valign="top" style="padding:20px 0 20px 0">
        <table bgcolor="#FFFFFF" cellspacing="0" cellpadding="0" border="0" width="590">
            <!-- [ header starts here] -->
            <tr>
                <td valign="middle" style="padding: 0 25px;" height="80"><a href="{{store url=""}}"><img src="{{skin url='images/logo_email.gif'}}" alt="{{var logo_alt}}" border="0"/></a></td>
            </tr>
            <tr>
                <td height="10"><img src="{{skin url='images/email-line.gif'}}"  style="margin-bottom:10px;" border="0"/></td>
            </tr>
            <!-- [ middle starts here] -->
            <tr>
                <td valign="top" style="padding:15px 30px;">
                    <h1 style="font-size:24px; font-weight:bold; line-height:24px; margin:0 0 11px 0;color:#28448d;">Hello, {{htmlescape var=$order.getCustomerName()}}</h1>
                    <p style="font-size:13px; line-height:18px; margin:0;color:#505050;">
                        Thank you for your order from {{var store.getFrontendName()}}.
                        Once your package ships we will send an email with a link to track your order.
                        You can check the status of your order by <a href="{{store url="customer/account/"}}" style="color:#28438c; text-decoration:none;">logging into your account</a>.
                        If you have any questions about your order please contact us at <a href="mailto:{{config path='trans_email/ident_support/email'}}" style="color:#28438c; text-decoration:none;">{{config path='trans_email/ident_support/email'}}</a> or call us at <span class="nobr">{{config path='general/store_information/phone'}}</span>.
                    </p>
                    <p style="font-size:12px; line-height:18px; margin:0;color:#505050;">Your order confirmation is below. Thank you again for your business.</p>
            </tr>
            <tr>
                <td style="padding:0 30px 15px 30px;">
                    <span style="font-size:12px; font-weight:normal; margin:0;color:#505050;">Your Order #{{var order.increment_id}} <small>(placed on {{var order.getCreatedAtFormated('long')}})</small></span>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table cellspacing="0" cellpadding="0" border="0" width="590" style="border:3px solid #e1e1e1;">
                        <thead>
                        <tr>
                            <th align="left" width="290" style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;color:#505050;">Billing Information:</th>
                            <th align="left" width="290" style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;color:#505050;">Payment Method:</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr><td colspan="2" height="3" bgcolor="#e1e1e1"></td></tr>
                        <tr>
                            <td valign="top" style="font-size:12px; padding:7px 9px 9px 9px;color:#505050;">
                                {{var order.getBillingAddress().format('html')}}
                            </td>
                            <td valign="top" style="font-size:12px; padding:7px 9px 9px 9px; color:#505050;">
                                {{var payment_html}}
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <br/>
                    {{depend order.getIsNotVirtual()}}
                    <table cellspacing="0" cellpadding="0" border="0" width="590" style="border:3px solid #e1e1e1;">
                        <thead>
                        <tr>
                            <th align="left" width="290"  style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;color:#505050;">Shipping Information:</th>
                            <th align="left" width="290"  style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;color:#505050;">Shipping Method:</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr><td colspan="2" height="3" bgcolor="#e1e1e1"></td></tr>
                        <tr>
                            <td valign="top" style="font-size:12px; padding:7px 9px 9px 9px;color:#505050;">
                                {{var order.getShippingAddress().format('html')}}
                                &nbsp;
                            </td>
                            <td valign="top" style="font-size:12px; padding:7px 9px 9px 9px;color:#505050;">
                                {{var order.getShippingDescription()}}
                                &nbsp;
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <br/>
                    {{/depend}}
                    {{layout handle="sales_email_order_items" order=$order}}
                    <p style="font-size:12px; margin:0 0 10px 0">{{var order.getEmailCustomerNote()}}</p>
                </td>
            </tr>
            <tr>
                <td height="30"><img src="{{skin url='images/email-line.gif'}}"  style="margin-top:20px;" border="0"/></td>
            </tr>
            <tr>
                <td style="padding:20px;">
                    <span style="font:normal 11px 'Arial';color:#505050;">For Customer Service inquiries, please email <a href="mailto:{{config path='trans_email/ident_support/email'}}" style="color:#28438c; text-decoration:none;">{{config path='trans_email/ident_support/email'}}</a>.
                    Have a suggestion, comment, or general feedback? Please send an email to <a href="mailto:{{config path='trans_email/ident_support/email'}}" style="color:#28438c; text-decoration:none;">{{config path='trans_email/ident_support/email'}}</a> to let us know.</span><br/><br/>
                    <span style="font:normal 11px 'Arial';color:#505050;">
                    This email was sent to you by Moxyst.com. To ensure delivery to your inbox (not bulk or junk folders), you can add <a href="mailto:{{config path='trans_email/ident_support/email'}}" style="color:#28438c; text-decoration:none;">{{config path='trans_email/ident_support/email'}}</a> to your address book or safe list.</span><br/><br/>
                    <table cellpadding="0" cellspacing="0" width="590">
                        <tr>
                            <td width="450">
                                <span style="font:normal 11px 'Arial';color:#505050;line-height:16px;">
                                &copy; 2013 Whatsnew Co.,Ltd. All Rights Reserved.<br/>
                                Sethiwan Tower - 17th Floor, <br/>139 Pan Road, Silom, Bangrak, Bangkok, 10500 Thailand</span><br/><br/>
                                <span style="font:normal 11px 'Arial';color:#505050;line-height:16px;">
                                <a href="{{store url="privacy-policy"}}" style="color:#505050; text-decoration:none;">Privacy Policy</a> | <a href="{{store url="contacts"}}" style="color:#505050; text-decoration:none;">Contact us</a></span>
                            </td>
                            <td valign="bottom" align="right">
                                <a href="https://www.facebook.com/moxyst" target="_blank"><img src="{{skin url='images/email-facebook.gif'}}"  style="margin-right:10px;" border="0"/></a>
                                <a href="https://twitter.com/moxyst" target="_blank"><img src="{{skin url='images/email-twitter.gif'}}"  style="margin-right:10px;" border="0"/></a>
                                <a href="https://plus.google.com/+Moxyst" target="_blank"><img src="{{skin url='images/email-google.gif'}}" border="0"/></a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </td>
</tr>
</table>
</div>
</body>
HTML;

/** @var Mage_Core_Model_Email_Template $template */
$template = Mage::getModel('core/email_template');
$template->loadByCode('New Order (Moxy English)')
    ->setTemplateCode('New Order (Moxy English)')
    ->setTemplateText($template_text)
    ->setTemplateSubject('{{var store.getFrontendName()}}: New Order # {{var order.increment_id}} ')
    ->setTemplateType(Mage_Core_Model_Email_Template::TYPE_HTML);

try {
    $template->save();
} catch (Mage_Exception $e) {
    throw $e;
}

/*new order Moxy th*/
$template_text = <<<HTML
<!--@subject {{var store.getFrontendName()}}: New Order # {{var order.increment_id}} @-->
<!--@vars
{"store url=\"\"":"Store Url",
"var logo_url":"Email Logo Image Url",
"var logo_alt":"Email Logo Image Alt",
"htmlescape var=$order.getCustomerName()":"Customer Name",
"var store.getFrontendName()":"Store Name",
"store url=\"customer/account/\"":"Customer Account Url",
"var order.increment_id":"Order Id",
"var order.getCreatedAtFormated('long')":"Order Created At (datetime)",
"var order.getBillingAddress().format('html')":"Billing Address",
"var payment_html":"Payment Details",
"var order.getShippingAddress().format('html')":"Shipping Address",
"var order.getShippingDescription()":"Shipping Description",
"layout handle=\"sales_email_order_items\" order=$order":"Order Items Grid",
"var order.getEmailCustomerNote()":"Email Order Note"}
@-->
<!--@styles
body,td { color:#2f2f2f; font:11px/1.35em Verdana, Arial, Helvetica, sans-serif; }
@-->

<body style="background:#e3e3e3; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; margin:0; padding:0;">
<div style="background:#e3e3e3; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; margin:0; padding:0;">
<table cellspacing="0" cellpadding="0" border="0" width="100%">
<tr>
    <td align="center" valign="top" style="padding:20px 0 20px 0">
        <table bgcolor="#FFFFFF" cellspacing="0" cellpadding="0" border="0" width="590">
            <!-- [ header starts here] -->
            <tr>
                <td valign="middle" style="padding: 0 25px;" height="80"><a href="{{store url=""}}"><img src="{{skin url='images/logo_email.gif'}}" alt="{{var logo_alt}}" border="0"/></a></td>
            </tr>
            <tr>
                <td height="10"><img src="{{skin url='images/email-line.gif'}}"  style="margin-bottom:10px;" border="0"/></td>
            </tr>
            <!-- [ middle starts here] -->
            <tr>
                <td valign="top" style="padding:15px 30px;">
                    <h1 style="font-size:24px; font-weight:bold; line-height:24px; margin:0 0 11px 0;color:#28448d;">สวัสดีค่ะคุณ {{htmlescape var=$order.getCustomerName()}}</h1>
                    <p style="font-size:13px; line-height:18px; margin:0;color:#505050;">
                        เราขอขอบคุณสำหรับการสั่งซื้อสินค้าจาก <a href="{{store url=""}}"  style="color:#28438c; text-decoration:none;" target="_blank">{{var store.getFrontendName()}}</a> คุณสามารถเช็คสถานะการสั่งซื้อของคุณได้โดย<a href="{{store url="customer/account/"}}" style="color:#28438c; text-decoration:none;">เข้าสู่ระบบบัญชีของคุณ</a>
                        <p style="font-size:13px; line-height:18px;color:#505050; margin:0;">
หากท่านมีคำถามหรือข้อสงสัยใดๆ สามารถติดต่อเราได้ที่ <a href="mailto:{{config path='trans_email/ident_support/email'}}" style="color:#28438c; text-decoration:none;">{{config path='trans_email/ident_support/email'}}</a> หรือที่หมายเลขโทรศัพท์ {{config path='general/store_information/phone'}}
</p>
                    </p>
            </tr>
            <tr>
                <td style="padding:0 30px 15px 30px;">
                    <span style="font-size:12px; font-weight:normal; margin:0;color:#505050;">เลขที่ใบสั่งซื้อ #{{var order.increment_id}} <small>(ทำรายการเมื่อ {{var order.getCreatedAtFormated('long')}})</small></span>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table cellspacing="0" cellpadding="0" border="0" width="590" style="border:3px solid #e1e1e1;">
                        <thead>
                        <tr>
                            <th align="left" width="290" style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;color:#505050;">ข้อมูลในการออกใบเสร็จ:</th>
                            <th align="left" width="290" style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;color:#505050;">วิธีการชำระเงิน:</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr><td colspan="2" height="3" bgcolor="#e1e1e1"></td></tr>
                        <tr>
                            <td valign="top" style="font-size:12px; padding:7px 9px 9px 9px;color:#505050;">
                                {{var order.getBillingAddress().format('html')}}
                            </td>
                            <td valign="top" style="font-size:12px; padding:7px 9px 9px 9px; color:#505050;">
                                {{var payment_html}}
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <br/>
                    {{depend order.getIsNotVirtual()}}
                    <table cellspacing="0" cellpadding="0" border="0" width="590" style="border:3px solid #e1e1e1;">
                        <thead>
                        <tr>
                            <th align="left" width="290"  style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;color:#505050;">ข้อมูลการจัดส่งสินค้า:</th>
                            <th align="left" width="290"  style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;color:#505050;">วิธีการจัดส่ง:</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr><td colspan="2" height="3" bgcolor="#e1e1e1"></td></tr>
                        <tr>
                            <td valign="top" style="font-size:12px; padding:7px 9px 9px 9px;color:#505050;">
                                {{var order.getShippingAddress().format('html')}}
                                &nbsp;
                            </td>
                            <td valign="top" style="font-size:12px; padding:7px 9px 9px 9px;color:#505050;">
                                {{var order.getShippingDescription()}}
                                &nbsp;
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <br/>
                    {{/depend}}
                    {{layout handle="sales_email_order_items" order=$order}}
                    <p style="font-size:12px; margin:0 0 10px 0">{{var order.getEmailCustomerNote()}}</p>
                </td>
            </tr>
            <tr>
                <td height="30"><img src="{{skin url='images/email-line.gif'}}"  style="margin-top:20px;" border="0"/></td>
            </tr>
            <tr>
                <td style="padding:20px;">
                    <span style="font:normal 11px 'Arial';color:#505050; line-height:13px;">
                    พบข้อสงสัยในการใช้บริการ
ติดต่อแผนกลูกค้าสัมพันธ์ที่หมายเลข {{config path='general/store_information/phone'}} ในเวลาทำการ จันทร์ – ศุกร์ 10.00 – 18.00 น. <br/>นอกเหนือเวลาดังกล่าวท่านสามารถติดต่อผ่านได้ที่อีเมล์  <a href="mailto:{{config path='trans_email/ident_support/email'}}" style="color:#28438c; text-decoration:none;">{{config path='trans_email/ident_support/email'}}</a> </span>
<br/><br/>
                    <span style="font:normal 11px 'Arial';color:#505050;line-height:13px;">
                    กรุณาบันทึกอีเมล์ฉบับนี้ในสมุดที่อยู่ของคุณเพื่อจะทำให้แน่ใจว่าคุณได้รับอีเมล์ทุกฉบับจากเรา </span><br/><br/>
                    <table cellpadding="0" cellspacing="0" width="590">
                        <tr>
                            <td width="450">
                                <span style="font:normal 11px 'Arial';color:#505050;line-height:13px;">
                                อีเมล์ฉบับนี้ถูกส่งโดย Whatsnew Co., Ltd. <br/>อาคารเศรษฐีวรรณ ชั้น 17<br/>139 ถนนปั้น แขวงสีลม เขตบางรัก กรุงเทพฯ ประเทศไทย 10500</span><br/><br/>
                                <span style="font:normal 11px 'Arial';color:#505050;line-height:13px;">
                                <a href="{{store url="privacy-policy"}}" style="color:#505050; text-decoration:none;">ความเป็นส่วนตัว</a> | <a href="{{store url="contacts"}}" style="color:#505050; text-decoration:none;">ติดต่อเรา</a></span>
                            </td>
                            <td valign="bottom" align="right">
                                <a href="https://www.facebook.com/moxyst" target="_blank"><img src="{{skin url='images/email-facebook.gif'}}"  style="margin-right:10px;" border="0"/></a>
                                <a href="https://twitter.com/moxyst" target="_blank"><img src="{{skin url='images/email-twitter.gif'}}"  style="margin-right:10px;" border="0"/></a>
                                <a href="https://plus.google.com/+Moxyst" target="_blank"><img src="{{skin url='images/email-google.gif'}}" border="0"/></a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </td>
</tr>
</table>
</div>
</body>
HTML;

/** @var Mage_Core_Model_Email_Template $template */
$template = Mage::getModel('core/email_template');
$template->loadByCode('New Order (Moxy Thai)')
    ->setTemplateCode('New Order (Moxy Thai)')
    ->setTemplateText($template_text)
    ->setTemplateSubject('{{var store.getFrontendName()}}: New Order # {{var order.increment_id}}')
    ->setTemplateType(Mage_Core_Model_Email_Template::TYPE_HTML);

try {
    $template->save();
} catch (Mage_Exception $e) {
    throw $e;
}

/*new invoice Moxy en*/
$template_text = <<<HTML
<!--@subject  {{var store.getFrontendName()}}: Invoice for Order # {{var order.increment_id}} @-->
<!--@vars
{"store url=\"\"":"Store Url",
"var logo_url":"Email Logo Image Url",
"var logo_alt":"Email Logo Image Alt",
"htmlescape var=$order.getCustomerName()":"Customer Name",
"var store.getFrontendName()":"Store Name",
"store url=\"customer/account/\"":"Customer Account Url",
"var invoice.increment_id":"Invoice Id",
"var order.increment_id":"Order Id",
"var order.billing_address.format('html')":"Billing Address",
"var payment_html":"Payment Details",
"var order.shipping_address.format('html')":"Shipping Address",
"var order.shipping_description":"Shipping Description",
"layout area=\"frontend\" handle=\"sales_email_order_invoice_items\" invoice=$invoice order=$order":"Invoice Items Grid",
"var comment":"Invoice Comment"}
@-->
<!--@styles
body,td { color:#2f2f2f; font:11px/1.35em Verdana, Arial, Helvetica, sans-serif; }
@-->

<body style="background:#e3e3e3; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; margin:0; padding:0;">
<div style="background:#e3e3e3; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; margin:0; padding:0;">
<table cellspacing="0" cellpadding="0" border="0" width="100%">
<tr>
    <td align="center" valign="top" style="padding:20px 0 20px 0">
        <!-- [ header starts here] -->
        <table bgcolor="#FFFFFF" cellspacing="0" cellpadding="10" border="0" width="590">
            <tr>
                <td valign="middle" style="padding: 0 25px;" height="80"><a href="{{store url=""}}"><img src="{{skin url='images/logo_email.gif'}}" alt="{{var logo_alt}}" border="0"/></a></td>
            </tr>
            <tr>
                <td height="10"><img src="{{skin url='images/email-line.gif'}}"  style="margin-bottom:10px;" border="0"/></td>
            </tr>
        <!-- [ middle starts here] -->
            <tr>
                <td valign="top" style="padding:15px 30px;">
                    <h1 style="font-size:24px; font-weight:bold; line-height:24px; margin:0 0 11px 0;color:#28448d;">Hello, {{htmlescape var=$order.getCustomerName()}}</h1>
                    <p style="font-size:13px; line-height:18px; margin:0;color:#505050;">
                        Thank you for your order from {{var store.getFrontendName()}}.
                        You can check the status of your order by <a href="{{store url="customer/account/"}}"  style="color:#28438c; text-decoration:none;">logging into your account</a>.
                        If you have any questions about your order please contact us at <a href="mailto:{{config path='trans_email/ident_support/email'}}"  style="color:#28438c; text-decoration:none;">{{config path='trans_email/ident_support/email'}}</a> or call us at <span class="nobr">{{config path='general/store_information/phone'}}</span>.
                    </p>
                </td>
            </tr>
            <tr>
                <td style="padding:0 30px 15px 30px;">
                    <span style="font-size:12px; font-weight:normal; margin:0;color:#505050;">Your Invoice for Order #{{var order.increment_id}}</span>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table cellspacing="0" cellpadding="0" border="0" width="590" style="border:3px solid #e1e1e1;">
                        <thead>
                            <tr>
                                <th align="left" width="290" style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;color:#505050;">Billing Information:</th>
                                <th align="left" width="290" style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;color:#505050;">Payment Method:</th>
                            </tr>
                        </thead>
                        <tbody>
                        <tr><td colspan="2" height="3" bgcolor="#e1e1e1"></td></tr>
                        <tr>
                            <td valign="top" style="font-size:12px; padding:7px 9px 9px 9px;color:#505050;">
                                {{var order.billing_address.format('html')}}
                            </td>
                            <td valign="top" style="font-size:12px; padding:7px 9px 9px 9px;color:#505050;">
                                {{var payment_html}}
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <br/>
                    {{depend order.getIsNotVirtual()}}
                    <table cellspacing="0" cellpadding="0" border="0" width="590" style="border:3px solid #e1e1e1;">
                        <thead>
                        <tr>
                            <th align="left" width="290"  style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;color:#505050;">Shipping Information:</th>
                            <th align="left" width="290"  style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;color:#505050;">Shipping Method:</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr><td colspan="2" height="3" bgcolor="#e1e1e1"></td></tr>
                        <tr>
                            <td valign="top" style="font-size:12px; padding:7px 9px 9px 9px;color:#505050;">
                                {{var order.shipping_address.format('html')}}
                                &nbsp;
                            </td>
                            <td valign="top" style="font-size:12px; padding:7px 9px 9px 9px;color:#505050;">
                                {{var order.shipping_description}}&nbsp;
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <br/>
                    {{/depend}}
                    {{layout area="frontend" handle="sales_email_order_invoice_items" invoice=$invoice order=$order}}
                    <p style="font-size:12px; margin:0 10px 10px 0;">{{var comment}}</p>
                </td>
            </tr>
            <tr>
                <td height="10"><img src="{{skin url='images/email-line.gif'}}" border="0"/></td>
            </tr>
            <tr>
                <td style="padding:20px;">
                    <span style="font:normal 11px 'Arial';color:#505050;">For Customer Service inquiries, please email <a href="mailto:{{config path='trans_email/ident_support/email'}}" style="color:#28438c; text-decoration:none;">{{config path='trans_email/ident_support/email'}}</a>.
                    Have a suggestion, comment, or general feedback? Please send an email to <a href="mailto:{{config path='trans_email/ident_support/email'}}" style="color:#28438c; text-decoration:none;">{{config path='trans_email/ident_support/email'}}</a> to let us know.</span><br/><br/>
                    <span style="font:normal 11px 'Arial';color:#505050;">
                    This email was sent to you by Moxyst.com. To ensure delivery to your inbox (not bulk or junk folders), you can add <a href="mailto:{{config path='trans_email/ident_support/email'}}" style="color:#28438c; text-decoration:none;">{{config path='trans_email/ident_support/email'}}</a> to your address book or safe list.</span><br/><br/>
                    <table cellpadding="0" cellspacing="0" width="590">
                        <tr>
                            <td width="450">
                                <span style="font:normal 11px 'Arial';color:#505050;line-height:16px;">
                                &copy; 2013 Whatsnew Co., Ltd. All Rights Reserved.<br/>
                                            Sethiwan Tower - 17th Floor, <br/>139 Pan Road, Silom, Bangrak, Bangkok, 10500 Thailand</span><br/><br/>
                                <span style="font:normal 11px 'Arial';color:#505050;line-height:16px;">
                                <a href="{{store url="privacy-policy"}}" style="color:#505050; text-decoration:none;">Privacy Policy</a> | <a href="{{store url="contacts"}}" style="color:#505050; text-decoration:none;">Contact us</a></span>
                            </td>
                            <td valign="bottom" align="right">
								<a href="https://www.facebook.com/moxyst" target="_blank"><img src="{{skin url='images/email-facebook.gif'}}"  style="margin-right:10px;" border="0"/></a>
								<a href="https://twitter.com/moxyst" target="_blank"><img src="{{skin url='images/email-twitter.gif'}}"  style="margin-right:10px;" border="0"/></a>
								<a href="https://plus.google.com/+Moxyst" target="_blank"><img src="{{skin url='images/email-google.gif'}}" border="0"/></a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </td>
</tr>
</table>
</div>
</body>
HTML;

/** @var Mage_Core_Model_Email_Template $template */
$template = Mage::getModel('core/email_template');
$template->loadByCode('New Invoice (Moxy English)')
    ->setTemplateCode('New Invoice (Moxy English)')
    ->setTemplateText($template_text)
    ->setTemplateSubject('{{var store.getFrontendName()}}: Invoice for Order # {{var order.increment_id}}')
    ->setTemplateType(Mage_Core_Model_Email_Template::TYPE_HTML);

try {
    $template->save();
} catch (Mage_Exception $e) {
    throw $e;
}


/*new invoice Moxy th*/
$template_text = <<<HTML
<!--@subject  {{var store.getFrontendName()}}: Invoice for Order # {{var order.increment_id}} @-->
<!--@vars
{"store url=\"\"":"Store Url",
"var logo_url":"Email Logo Image Url",
"var logo_alt":"Email Logo Image Alt",
"htmlescape var=$order.getCustomerName()":"Customer Name",
"var store.getFrontendName()":"Store Name",
"store url=\"customer/account/\"":"Customer Account Url",
"var invoice.increment_id":"Invoice Id",
"var order.increment_id":"Order Id",
"var order.billing_address.format('html')":"Billing Address",
"var payment_html":"Payment Details",
"var order.shipping_address.format('html')":"Shipping Address",
"var order.shipping_description":"Shipping Description",
"layout area=\"frontend\" handle=\"sales_email_order_invoice_items\" invoice=$invoice order=$order":"Invoice Items Grid",
"var comment":"Invoice Comment"}
@-->
<!--@styles
body,td { color:#2f2f2f; font:11px/1.35em Verdana, Arial, Helvetica, sans-serif; }
@-->

<body style="background:#e3e3e3; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; margin:0; padding:0;">
<div style="background:#e3e3e3; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; margin:0; padding:0;">
<table cellspacing="0" cellpadding="0" border="0" width="100%">
<tr>
    <td align="center" valign="top" style="padding:20px 0 20px 0">
        <table bgcolor="#FFFFFF" cellspacing="0" cellpadding="0" border="0" width="590">
            <!-- [ header starts here] -->
            <tr>
                <td valign="middle" style="padding: 0 25px;" height="80"><a href="{{store url=""}}"><img src="{{skin url='images/logo_email.gif'}}" alt="{{var logo_alt}}" border="0"/></a></td>
            </tr>
            <tr>
                <td height="10"><img src="{{skin url='images/email-line.gif'}}"  style="margin-bottom:10px;" border="0"/></td>
            </tr>
            <!-- [ middle starts here] -->
            <tr>
                <td valign="top" style="padding:15px 30px;">
                    <h1 style="font-size:24px; font-weight:bold; line-height:24px; margin:0 0 11px 0;color:#28448d;">สวัสดีค่ะคุณ {{htmlescape var=$order.getCustomerName()}}</h1>
                    <p style="font-size:13px; line-height:18px; margin:0;color:#505050;">
                        เราขอขอบคุณสำหรับการสั่งซื้อสินค้าจาก <a href="{{store url=""}}"  style="color:#28438c; text-decoration:none;" target="_blank">{{var store.getFrontendName()}}</a>
                        คุณสามารถเช็คสถานะการสั่งซื้อของคุณได้โดยเข้าสู่ระบบบัญชีของคุณ
                    </p>
                    <p style="font-size:13px; line-height:18px;color:#505050; margin:0;">หากท่านมีคำถามหรือข้อสงสัยใดๆ สามารถติดต่อเราได้ที่ <a href="mailto:{{config path='trans_email/ident_support/email'}}" style="color:#28438c; text-decoration:none;">{{config path='trans_email/ident_support/email'}}</a> หรือที่หมายเลขโทรศัพท์ {{config path='general/store_information/phone'}}</p>
                </td>
            </tr>
            <tr>
                <td style="padding:0 30px 15px 30px;">
                    <span style="font-size:12px; font-weight:normal; margin:0;color:#505050;">เลขที่ใบเสร็จของคุณ  สำหรับใบสั่งซื้อ #{{var order.increment_id}}</span>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table cellspacing="0" cellpadding="0" border="0" width="590" style="border:3px solid #e1e1e1;">
                        <thead>
                            <tr>
                                <th align="left" width="290" style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;color:#505050;">ข้อมูลในการออกใบเสร็จ:</th>
                                <th align="left" width="290" style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;color:#505050;">วิธีการชำระเงิน:</th>
                            </tr>
                        </thead>
                        <tbody>
                        <tr><td colspan="2" height="3" bgcolor="#e1e1e1"></td></tr>
                        <tr>
                            <td valign="top" style="font-size:12px; padding:7px 9px 9px 9px;color:#505050;">
                                {{var order.billing_address.format('html')}}
                            </td>
                            <td valign="top" style="font-size:12px; padding:7px 9px 9px 9px;color:#505050;">
                                {{var payment_html}}
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <br/>
                    {{depend order.getIsNotVirtual()}}
                    <table cellspacing="0" cellpadding="0" border="0" width="590" style="border:3px solid #e1e1e1;">
                        <thead>
                        <tr>
                            <th align="left" width="290"  style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;color:#505050;">ข้อมูลการจัดส่งสินค้า:</th>
                            <th align="left" width="290"  style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;color:#505050;">วิธีการจัดส่ง:</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr><td colspan="2" height="3" bgcolor="#e1e1e1"></td></tr>
                        <tr>
                            <td valign="top" style="font-size:12px; padding:7px 9px 9px 9px;color:#505050;">
                                {{var order.shipping_address.format('html')}}
                                &nbsp;
                            </td>
                            <td valign="top" style="font-size:12px; padding:7px 9px 9px 9px;color:#505050;">
                                {{var order.shipping_description}}&nbsp;
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <br/>
                    {{/depend}}
                    {{layout area="frontend" handle="sales_email_order_invoice_items" invoice=$invoice order=$order}}
                    <p style="font-size:12px; margin:0 10px 10px 0;">{{var comment}}</p>
                </td>
            </tr>
            <tr>
                <td height="30"><img src="{{skin url='images/email-line.gif'}}"  style="margin-top:20px;" border="0"/></td>
            </tr>
            <tr>
                <td style="padding:20px;">
                    <span style="font:normal 11px 'Arial';color:#505050; line-height:13px;">
                    พบข้อสงสัยในการใช้บริการ
ติดต่อแผนกลูกค้าสัมพันธ์ที่หมายเลข {{config path='general/store_information/phone'}} ในเวลาทำการ จันทร์ – ศุกร์ 10.00 – 18.00 น. <br/>นอกเหนือเวลาดังกล่าวท่านสามารถติดต่อผ่านได้ที่อีเมล์  <a href="mailto:{{config path='trans_email/ident_support/email'}}" style="color:#28438c; text-decoration:none;">{{config path='trans_email/ident_support/email'}}</a> </span>
<br/><br/>
                    <span style="font:normal 11px 'Arial';color:#505050;line-height:13px;">
                    กรุณาบันทึกอีเมล์ฉบับนี้ในสมุดที่อยู่ของคุณเพื่อจะทำให้แน่ใจว่าคุณได้รับอีเมล์ทุกฉบับจากเรา </span><br/><br/>
                    <table cellpadding="0" cellspacing="0" width="590">
                        <tr>
                            <td width="450">
                                <span style="font:normal 11px 'Arial';color:#505050;line-height:13px;">
                                อีเมล์ฉบับนี้ถูกส่งโดย Whatsnew Co., Ltd. <br/>อาคารเศรษฐีวรรณ ชั้น 17<br/>139 ถนนปั้น แขวงสีลม เขตบางรัก กรุงเทพฯ ประเทศไทย 10500</span><br/><br/>
                                <span style="font:normal 11px 'Arial';color:#505050;line-height:13px;">
                                <a href="{{store url="privacy-policy"}}" style="color:#505050; text-decoration:none;">ความเป็นส่วนตัว</a> | <a href="{{store url="contacts"}}" style="color:#505050; text-decoration:none;">ติดต่อเรา</a></span>
                            </td>
                            <td valign="bottom" align="right">
								<a href="https://www.facebook.com/moxyst" target="_blank"><img src="{{skin url='images/email-facebook.gif'}}"  style="margin-right:10px;" border="0"/></a>
								<a href="https://twitter.com/moxyst" target="_blank"><img src="{{skin url='images/email-twitter.gif'}}"  style="margin-right:10px;" border="0"/></a>
								<a href="https://plus.google.com/+Moxyst" target="_blank"><img src="{{skin url='images/email-google.gif'}}" border="0"/></a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </td>
</tr>
</table>
</div>
</body>
HTML;

/** @var Mage_Core_Model_Email_Template $template */
$template = Mage::getModel('core/email_template');
$template->loadByCode('New Invoice (Moxy Thai)')
    ->setTemplateCode('New Invoice (Moxy Thai)')
    ->setTemplateText($template_text)
    ->setTemplateSubject('{{var store.getFrontendName()}}: Invoice for Order # {{var order.increment_id}}')
    ->setTemplateType(Mage_Core_Model_Email_Template::TYPE_HTML);

try {
    $template->save();
} catch (Mage_Exception $e) {
    throw $e;
}

/*new Shipment Moxy en*/
$template_text = <<<HTML
<!--@subject {{var store.getFrontendName()}}: Shipment for Order # {{var order.increment_id}} @-->
<!--@vars
{"store url=\"\"":"Store Url",
"var logo_url":"Email Logo Image Url",
"var logo_alt":"Email Logo Image Alt",
"htmlescape var=$order.getCustomerName()":"Customer Name",
"var store.getFrontendName()":"Store Name",
"store url=\"customer/account/\"":"Customer Account Url",
"var shipment.increment_id":"Shipment Id",
"var order.increment_id":"Order Id",
"var order.billing_address.format('html')":"Billing Address",
"var payment_html":"Payment Details",
"var order.shipping_address.format('html')":"Shipping Address",
"var order.shipping_description":"Shipping Description",
"layout handle=\"sales_email_order_shipment_items\" shipment=$shipment order=$order":"Shipment Items Grid",
"block type='core/template' area='frontend' template='email/order/shipment/track.phtml' shipment=$shipment order=$order":"Shipment Track Details",
"var comment":"Shipment Comment"}
@-->
<!--@styles
body,td { color:#2f2f2f; font:11px/1.35em Verdana, Arial, Helvetica, sans-serif; }
@-->

<body style="background:#e3e3e3; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; margin:0; padding:0;">
<div style="background:#e3e3e3; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; margin:0; padding:0;">
<table cellspacing="0" cellpadding="0" border="0" width="100%">
<tr>
    <td align="center" valign="top" style="padding:20px 0 20px 0">
        <table bgcolor="#FFFFFF" cellspacing="0" cellpadding="0" border="0" width="590">
            <!-- [ header starts here] -->
            <tr>
                <td valign="middle" style="padding: 0 25px;" height="80"><a href="{{store url=""}}"><img src="{{skin url='images/logo_email.gif'}}" alt="{{var logo_alt}}" border="0"/></a></td>
            </tr>
            <tr>
                <td height="10"><img src="{{skin url='images/email-line.gif'}}"  style="margin-bottom:10px;" border="0"/></td>
            </tr>
            <!-- [ middle starts here] -->
            <tr>
                <td valign="top" style="padding:15px 30px;">
                    <h1 style="font-size:24px; font-weight:bold; line-height:24px; margin:0 0 11px 0;color:#28448d;">Hello, {{htmlescape var=$order.getCustomerName()}}</h1>
                    <p style="font-size:13px; line-height:18px; margin:0;color:#505050;">
                        Thank you for your order from {{var store.getFrontendName()}}.
                        You can check the status of your order by <a href="{{store url="customer/account/"}}" style="color:#1E7EC8;">logging into your account</a>.
                        If you have any questions about your order please contact us at <a href="mailto:{{config path='trans_email/ident_support/email'}}" style="color:#1E7EC8;">{{config path='trans_email/ident_support/email'}}</a> or call us at <span class="nobr">{{config path='general/store_information/phone'}}</span>.
                    </p>
                    <p style="font-size:12px; line-height:18px; margin:0;color:#505050;">
                        Your shipping confirmation is below. Thank you again for your business.
                    </p>
                </td>
            </tr>
            <tr>
                <td style="padding:0 30px 15px 30px;">
                    <span style="font-size:12px; font-weight:normal; margin:0;color:#505050;">Your Shipment for Order #{{var order.increment_id}}</span>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table cellspacing="0" cellpadding="0" border="0" width="590" style="border:3px solid #e1e1e1;">
                        <thead>
                        <tr>
                            <th align="left" width="290" style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;color:#505050;">Billing Information:</th>
                            <th align="left" width="290" style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;color:#505050;">Payment Method:</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr><td colspan="2" height="3" bgcolor="#e1e1e1"></td></tr>
                        <tr>
                            <td valign="top" style="font-size:12px; padding:7px 9px 9px 9px;color:#505050;">
                                {{var order.billing_address.format('html')}}
                            </td>
                            <td valign="top" style="font-size:12px; padding:7px 9px 9px 9px; color:#505050;">
                                {{var payment_html}}
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <br/>
                    <table cellspacing="0" cellpadding="0" border="0" width="590" style="border:3px solid #e1e1e1;">
                        <thead>
                        <tr>
                            <th align="left" width="290"  style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;color:#505050;">Shipping Information:</th>
                            <th align="left" width="290"  style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;color:#505050;">Shipping Method:</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr><td colspan="2" height="3" bgcolor="#e1e1e1"></td></tr>
                        <tr>
                            <td valign="top" style="font-size:12px; padding:7px 9px 9px 9px;color:#505050;">
                                {{var order.shipping_address.format('html')}}
                                &nbsp;
                            </td>
                            <td valign="top" style="font-size:12px; padding:7px 9px 9px 9px;color:#505050;">
                                {{var order.shipping_description}}
                                &nbsp;
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <br/>
                    {{layout handle="sales_email_order_shipment_items" shipment=$shipment order=$order}}
                    {{block type='core/template' area='frontend' template='email/order/shipment/track.phtml' shipment=$shipment order=$order}}
                    <p style="font-size:12px; margin:0 10px 10px 0">{{var comment}}</p>
                </td>
            </tr>
            <tr>
                <td height="30"><img src="{{skin url='images/email-line.gif'}}"  style="margin-top:20px;" border="0"/></td>
            </tr>
            <tr>
                <td style="padding:20px;">
                    <span style="font:normal 11px 'Arial';color:#505050;">For Customer Service inquiries, please email <a href="mailto:{{config path='trans_email/ident_support/email'}}" style="color:#28438c; text-decoration:none;">{{config path='trans_email/ident_support/email'}}</a>.
                    Have a suggestion, comment, or general feedback? Please send an email to <a href="mailto:{{config path='trans_email/ident_support/email'}}" style="color:#28438c; text-decoration:none;">{{config path='trans_email/ident_support/email'}}</a> to let us know.</span><br/><br/>
                    <span style="font:normal 11px 'Arial';color:#505050;">
                    This email was sent to you by Moxyst.com. To ensure delivery to your inbox (not bulk or junk folders), you can add <a href="mailto:{{config path='trans_email/ident_support/email'}}" style="color:#28438c; text-decoration:none;">{{config path='trans_email/ident_support/email'}}</a> to your address book or safe list.</span><br/><br/>
                    <table cellpadding="0" cellspacing="0" width="590">
                        <tr>
                            <td width="450">
                                <span style="font:normal 11px 'Arial';color:#505050;line-height:16px;">
                                &copy; 2013 Whatsnew Co.,Ltd. All Rights Reserved.<br/>
                                Sethiwan Tower - 17th Floor, <br/>139 Pan Road, Silom, Bangrak, Bangkok, 10500 Thailand</span><br/><br/>
                                <span style="font:normal 11px 'Arial';color:#505050;line-height:16px;">
                                <a href="{{store url="privacy-policy"}}" style="color:#505050; text-decoration:none;">Privacy Policy</a> | <a href="{{store url="contacts"}}" style="color:#505050; text-decoration:none;">Contact us</a></span>
                            </td>
                            <td valign="bottom" align="right">
								<a href="https://www.facebook.com/moxyst" target="_blank"><img src="{{skin url='images/email-facebook.gif'}}"  style="margin-right:10px;" border="0"/></a>
								<a href="https://twitter.com/moxyst" target="_blank"><img src="{{skin url='images/email-twitter.gif'}}"  style="margin-right:10px;" border="0"/></a>
								<a href="https://plus.google.com/+Moxyst" target="_blank"><img src="{{skin url='images/email-google.gif'}}" border="0"/></a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </td>
</tr>
</table>
</div>
</body>
HTML;

/** @var Mage_Core_Model_Email_Template $template */
$template = Mage::getModel('core/email_template');
$template->loadByCode('New Shipment (Moxy English)')
    ->setTemplateCode('New Shipment (Moxy English)')
    ->setTemplateText($template_text)
    ->setTemplateSubject('{{var store.getFrontendName()}}: Shipment for Order # {{var order.increment_id}}')
    ->setTemplateType(Mage_Core_Model_Email_Template::TYPE_HTML);

try {
    $template->save();
} catch (Mage_Exception $e) {
    throw $e;
}

/*new Shipment Moxy th*/
$template_text = <<<HTML
<!--@subject {{var store.getFrontendName()}}: Shipment for Order # {{var order.increment_id}} @-->
<!--@vars
{"store url=\"\"":"Store Url",
"var logo_url":"Email Logo Image Url",
"var logo_alt":"Email Logo Image Alt",
"htmlescape var=$order.getCustomerName()":"Customer Name",
"var store.getFrontendName()":"Store Name",
"store url=\"customer/account/\"":"Customer Account Url",
"var shipment.increment_id":"Shipment Id",
"var order.increment_id":"Order Id",
"var order.billing_address.format('html')":"Billing Address",
"var payment_html":"Payment Details",
"var order.shipping_address.format('html')":"Shipping Address",
"var order.shipping_description":"Shipping Description",
"layout handle=\"sales_email_order_shipment_items\" shipment=$shipment order=$order":"Shipment Items Grid",
"block type='core/template' area='frontend' template='email/order/shipment/track.phtml' shipment=$shipment order=$order":"Shipment Track Details",
"var comment":"Shipment Comment"}
@-->
<!--@styles
body,td { color:#2f2f2f; font:11px/1.35em Verdana, Arial, Helvetica, sans-serif; }
@-->

<body style="background:#e3e3e3; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; margin:0; padding:0;">
<div style="background:#e3e3e3; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; margin:0; padding:0;">
<table cellspacing="0" cellpadding="0" border="0" width="100%">
<tr>
    <td align="center" valign="top" style="padding:20px 0 20px 0">
        <table bgcolor="#FFFFFF" cellspacing="0" cellpadding="0" border="0" width="590">
            <!-- [ header starts here] -->
            <tr>
                <td valign="middle" style="padding: 0 25px;" height="80"><a href="{{store url=""}}"><img src="{{skin url='images/logo_email.gif'}}" alt="{{var logo_alt}}" border="0"/></a></td>
            </tr>
            <tr>
                <td height="10"><img src="{{skin url='images/email-line.gif'}}"  style="margin-bottom:10px;" border="0"/></td>
            </tr>
            <!-- [ middle starts here] -->
            <tr>
                <td valign="top" style="padding:15px 30px;">
                    <h1 style="font-size:24px; font-weight:bold; line-height:24px; margin:0 0 11px 0;color:#28448d;">สวัสดีค่ะคุณ  {{htmlescape var=$order.getCustomerName()}}</h1>
                    <p style="font-size:13px; line-height:18px; margin:0;color:#505050;">
                        ขอขอบคุณสำหรับการสั่งซื้อสินค้าจาก  <a href="{{store url=""}}"  style="color:#28438c; text-decoration:none;" target="_blank">{{var store.getFrontendName()}}</a> คุณสามารถเช็คสถานะการสั่งซื้อสินค้าของคุณได้โดยเข้าสู่ระบบบัญชีของคุณ หากคุณมีคำถามหรือข้อสงสัย
                    </p>
                    <p style="font-size:13px; line-height:18px;color:#505050; margin:0;">หากท่านมีคำถามหรือข้อสงสัยใดๆ สามารถติดต่อเราได้ที่ <a href="mailto:{{config path='trans_email/ident_support/email'}}" style="color:#28438c; text-decoration:none;">{{config path='trans_email/ident_support/email'}}</a> หรือที่หมายเลขโทรศัพท์ {{config path='general/store_information/phone'}}</p>
                </td>
            </tr>
            <tr>
                <td style="padding:0 30px 15px 30px;">
                    <span style="font-size:12px; font-weight:normal; margin:0;color:#505050;">ข้อมูลการจัดส่งสินค้า  สำหรับใบสั่งซื้อ #{{var order.increment_id}}</span>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table cellspacing="0" cellpadding="0" border="0" width="590" style="border:3px solid #e1e1e1;">
                        <thead>
                        <tr>
                            <th align="left" width="290" style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;color:#505050;">ข้อมูลในการออกใบเสร็จ:</th>
                                <th align="left" width="290" style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;color:#505050;">วิธีการชำระเงิน:</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr><td colspan="2" height="3" bgcolor="#e1e1e1"></td></tr>
                        <tr>
                            <td valign="top" style="font-size:12px; padding:7px 9px 9px 9px;color:#505050;">
                                {{var order.billing_address.format('html')}}
                            </td>
                            <td valign="top" style="font-size:12px; padding:7px 9px 9px 9px; color:#505050;">
                                {{var payment_html}}
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <br/>
                    <table cellspacing="0" cellpadding="0" border="0" width="590" style="border:3px solid #e1e1e1;">
                        <thead>
                        <tr>
                            <th align="left" width="290"  style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;color:#505050;">ข้อมูลการจัดส่งสินค้า:</th>
                            <th align="left" width="290"  style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;color:#505050;">วิธีการจัดส่ง:</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr><td colspan="2" height="3" bgcolor="#e1e1e1"></td></tr>
                        <tr>
                            <td valign="top" style="font-size:12px; padding:7px 9px 9px 9px;color:#505050;">
                                {{var order.shipping_address.format('html')}}
                                &nbsp;
                            </td>
                            <td valign="top" style="font-size:12px; padding:7px 9px 9px 9px;color:#505050;">
                                {{var order.shipping_description}}
                                &nbsp;
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <br/>
                    {{layout handle="sales_email_order_shipment_items" shipment=$shipment order=$order}}
                    {{block type='core/template' area='frontend' template='email/order/shipment/track.phtml' shipment=$shipment order=$order}}
                    <p style="font-size:12px; margin:0 10px 10px 0">{{var comment}}</p>
                </td>
            </tr>
            <tr>
                <td height="30"><img src="{{skin url='images/email-line.gif'}}"  style="margin-top:20px;" border="0"/></td>
            </tr>
            <tr>
                <td style="padding:20px;">
                    <span style="font:normal 11px 'Arial';color:#505050; line-height:13px;">
                    พบข้อสงสัยในการใช้บริการ
ติดต่อแผนกลูกค้าสัมพันธ์ที่หมายเลข {{config path='general/store_information/phone'}} ในเวลาทำการ จันทร์ – ศุกร์ 10.00 – 18.00 น. <br/>นอกเหนือเวลาดังกล่าวท่านสามารถติดต่อผ่านได้ที่อีเมล์  <a href="mailto:{{config path='trans_email/ident_support/email'}}" style="color:#28438c; text-decoration:none;">{{config path='trans_email/ident_support/email'}}</a> </span>
<br/><br/>
                    <span style="font:normal 11px 'Arial';color:#505050;line-height:13px;">
                    กรุณาบันทึกอีเมล์ฉบับนี้ในสมุดที่อยู่ของคุณเพื่อจะทำให้แน่ใจว่าคุณได้รับอีเมล์ทุกฉบับจากเรา </span><br/><br/>
                    <table cellpadding="0" cellspacing="0" width="590">
                        <tr>
                            <td width="450">
                                <span style="font:normal 11px 'Arial';color:#505050;line-height:13px;">
                                อีเมล์ฉบับนี้ถูกส่งโดย Whatsnew Co., Ltd. <br/>อาคารเศรษฐีวรรณ ชั้น 17<br/>139 ถนนปั้น แขวงสีลม เขตบางรัก กรุงเทพฯ ประเทศไทย 10500</span><br/><br/>
                                <span style="font:normal 11px 'Arial';color:#505050;line-height:13px;">
                                <a href="{{store url="privacy-policy"}}" style="color:#505050; text-decoration:none;">ความเป็นส่วนตัว</a> | <a href="{{store url="contacts"}}" style="color:#505050; text-decoration:none;">ติดต่อเรา</a></span>
                            </td>
                            <td valign="bottom" align="right">
								<a href="https://www.facebook.com/moxyst" target="_blank"><img src="{{skin url='images/email-facebook.gif'}}"  style="margin-right:10px;" border="0"/></a>
								<a href="https://twitter.com/moxyst" target="_blank"><img src="{{skin url='images/email-twitter.gif'}}"  style="margin-right:10px;" border="0"/></a>
								<a href="https://plus.google.com/+Moxyst" target="_blank"><img src="{{skin url='images/email-google.gif'}}" border="0"/></a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </td>
</tr>
</table>
</div>
</body>
HTML;

/** @var Mage_Core_Model_Email_Template $template */
$template = Mage::getModel('core/email_template');
$template->loadByCode('New Shipment (Moxy Thai)')
    ->setTemplateCode('New Shipment (Moxy Thai)')
    ->setTemplateText($template_text)
    ->setTemplateSubject('{{var store.getFrontendName()}}: Shipment for Order # {{var order.increment_id}}')
    ->setTemplateType(Mage_Core_Model_Email_Template::TYPE_HTML);

try {
    $template->save();
} catch (Mage_Exception $e) {
    throw $e;
}

/*new Crediit Memo Moxy en*/
$template_text = <<<HTML
<!--@subject {{var store.getFrontendName()}}: Credit Memo for Order # {{var order.increment_id}} @-->
<!--@vars
{"store url=\"\"":"Store Url",
"var logo_url":"Email Logo Image Url",
"var logo_alt":"Email Logo Image Alt",
"htmlescape var=$order.getCustomerName()":"Customer Name",
"var store.getFrontendName()":"Store Name",
"store url=\"customer/account/\"":"Customer Account Url",
"var creditmemo.increment_id":"Credit Memo Id",
"var order.increment_id":"Order Id",
"var order.billing_address.format('html')":"Billing Address",
"payment_html":"Payment Details",
"var order.shipping_address.format('html')":"Shipping Address",
"var order.shipping_description":"Shipping Description",
"layout handle=\"sales_email_order_creditmemo_items\" creditmemo=$creditmemo order=$order":"Credit Memo Items Grid",
"var comment":"Credit Memo Comment"}
@-->
<!--@styles
body,td { color:#2f2f2f; font:11px/1.35em Verdana, Arial, Helvetica, sans-serif; }
@-->

<body style="background:#e3e3e3; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; margin:0; padding:0;">
<div style="background:#e3e3e3; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; margin:0; padding:0;">
<table cellspacing="0" cellpadding="0" border="0" width="100%">
<tr>
    <td align="center" valign="top" style="padding:20px 0 20px 0">
        <table bgcolor="#FFFFFF" cellspacing="0" cellpadding="0" border="0" width="590">
            <!-- [ header starts here] -->
            <tr>
                <td valign="middle" style="padding: 0 25px;" height="80"><a href="{{store url=""}}"><img src="{{skin url='images/logo_email.gif'}}" alt="{{var logo_alt}}" border="0"/></a></td>
            </tr>
            <tr>
                <td height="10"><img src="{{skin url='images/email-line.gif'}}"  style="margin-bottom:10px;" border="0"/></td>
            </tr>
            <!-- [ middle starts here] -->
            <tr>
                <td valign="top" style="padding:15px 30px;">
                    <h1 style="font-size:24px; font-weight:bold; line-height:24px; margin:0 0 11px 0;color:#28448d;">Hello, {{htmlescape var=$order.getCustomerName()}}</h1>
                    <p style="font-size:13px; line-height:18px; margin:0;color:#505050;">
                        Thank you for your order from {{var store.getFrontendName()}}.
                        You can check the status of your order by <a href="{{store url="customer/account/"}}" style="color:#28438c; text-decoration:none;">logging into your account</a>.
                        If you have any questions about your order please contact us at <a href="mailto:{{config path='trans_email/ident_support/email'}}" style="color:#28438c; text-decoration:none;">{{config path='trans_email/ident_support/email'}}</a> or call us at <span class="nobr">{{config path='general/store_information/phone'}}</span>.
                    </p>
                </td>
            </tr>
            <tr>
                <td style="padding:15px 30px;">
                    <h2 style="font-size:12px; font-weight:normal; margin:0;color:#505050;">Your Credit Memo for Order #{{var order.increment_id}}</h2>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table cellspacing="0" cellpadding="0" border="0" width="590" style="border:3px solid #e1e1e1;">
                        <thead>
                        <tr>
                            <th align="left" width="290" style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;color:#505050;">Billing Information:</th>
                            <th align="left" width="290" style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;color:#505050;">Payment Method:</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr><td colspan="2" height="3" bgcolor="#e1e1e1"></td></tr>
                        <tr>
                            <td valign="top" style="font-size:12px; padding:7px 9px 9px 9px;color:#505050;">
                                {{var order.billing_address.format('html')}}
                            </td>
                            <td valign="top" style="font-size:12px; padding:7px 9px 9px 9px; color:#505050;">
                                {{var payment_html}}
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <br/>
                    {{depend order.getIsNotVirtual()}}
                    <table cellspacing="0" cellpadding="0" border="0" width="590" style="border:3px solid #e1e1e1;">
                        <thead>
                        <tr>
                            <th align="left" width="290"  style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;color:#505050;">Shipping Information:</th>
                            <th align="left" width="290"  style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;color:#505050;">Shipping Method:</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr><td colspan="2" height="3" bgcolor="#e1e1e1"></td></tr>
                        <tr>
                            <td valign="top" style="font-size:12px; padding:7px 9px 9px 9px;color:#505050;">
                                {{var order.shipping_address.format('html')}}
                                &nbsp;
                            </td>
                            <td valign="top" style="font-size:12px; padding:7px 9px 9px 9px;color:#505050;">
                                {{var order.shipping_description}}
                                &nbsp;
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <br/>
                    {{/depend}}
                    {{layout handle="sales_email_order_creditmemo_items" creditmemo=$creditmemo order=$order}}
                    <p style="font-size:12px; margin:0 0 10px 0">{{var comment}}</p>
                </td>
            </tr>
            <tr>
                <td height="30"><img src="{{skin url='images/email-line.gif'}}"  style="margin-top:20px;" border="0"/></td>
            </tr>
            <tr>
                <td style="padding:20px;">
                    <span style="font:normal 11px 'Arial';color:#505050;">For Customer Service inquiries, please email <a href="mailto:{{config path='trans_email/ident_support/email'}}" style="color:#28438c; text-decoration:none;">{{config path='trans_email/ident_support/email'}}</a>.
                    Have a suggestion, comment, or general feedback? Please send an email to <a href="mailto:{{config path='trans_email/ident_support/email'}}" style="color:#28438c; text-decoration:none;">{{config path='trans_email/ident_support/email'}}</a> to let us know.</span><br/><br/>
                    <span style="font:normal 11px 'Arial';color:#505050;">
                    This email was sent to you by Moxyst.com. To ensure delivery to your inbox (not bulk or junk folders), you can add <a href="mailto:{{config path='trans_email/ident_support/email'}}" style="color:#28438c; text-decoration:none;">{{config path='trans_email/ident_support/email'}}</a> to your address book or safe list.</span><br/><br/>
                    <table cellpadding="0" cellspacing="0" width="590">
                        <tr>
                            <td width="450">
                                <span style="font:normal 11px 'Arial';color:#505050;line-height:16px;">
                                &copy; 2013 Whatsnew Co.,Ltd. All Rights Reserved.<br/>
                                Sethiwan Tower - 17th Floor, <br/>139 Pan Road, Silom, Bangrak, Bangkok, 10500 Thailand</span><br/><br/>
                                <span style="font:normal 11px 'Arial';color:#505050;line-height:16px;">
                                <a href="{{store url="privacy-policy"}}" style="color:#505050; text-decoration:none;">Privacy Policy</a> | <a href="{{store url="contacts"}}" style="color:#505050; text-decoration:none;">Contact us</a></span>
                            </td>
                            <td valign="bottom" align="right">
								<a href="https://www.facebook.com/moxyst" target="_blank"><img src="{{skin url='images/email-facebook.gif'}}"  style="margin-right:10px;" border="0"/></a>
								<a href="https://twitter.com/moxyst" target="_blank"><img src="{{skin url='images/email-twitter.gif'}}"  style="margin-right:10px;" border="0"/></a>
								<a href="https://plus.google.com/+Moxyst" target="_blank"><img src="{{skin url='images/email-google.gif'}}" border="0"/></a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </td>
</tr>
</table>
</div>
</body>
HTML;

/** @var Mage_Core_Model_Email_Template $template */
$template = Mage::getModel('core/email_template');
$template->loadByCode('New Crediit Memo (Moxy English)')
    ->setTemplateCode('New Crediit Memo (Moxy English)')
    ->setTemplateText($template_text)
    ->setTemplateSubject('{{var store.getFrontendName()}}: Credit Memo for Order # {{var order.increment_id}}')
    ->setTemplateType(Mage_Core_Model_Email_Template::TYPE_HTML);

try {
    $template->save();
} catch (Mage_Exception $e) {
    throw $e;
}

/*new Crediit Memo Moxy th*/
$template_text = <<<HTML
<!--@subject {{var store.getFrontendName()}}: Credit Memo # {{var creditmemo.increment_id}} for Order # {{var order.increment_id}} @-->
<!--@vars
{"store url=\"\"":"Store Url",
"var logo_url":"Email Logo Image Url",
"var logo_alt":"Email Logo Image Alt",
"htmlescape var=$order.getCustomerName()":"Customer Name",
"var store.getFrontendName()":"Store Name",
"store url=\"customer/account/\"":"Customer Account Url",
"var creditmemo.increment_id":"Credit Memo Id",
"var order.increment_id":"Order Id",
"var order.billing_address.format('html')":"Billing Address",
"payment_html":"Payment Details",
"var order.shipping_address.format('html')":"Shipping Address",
"var order.shipping_description":"Shipping Description",
"layout handle=\"sales_email_order_creditmemo_items\" creditmemo=$creditmemo order=$order":"Credit Memo Items Grid",
"var comment":"Credit Memo Comment"}
@-->
<!--@styles
body,td { color:#2f2f2f; font:11px/1.35em Verdana, Arial, Helvetica, sans-serif; }
@-->

<body style="background:#e3e3e3; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; margin:0; padding:0;">
<div style="background:#e3e3e3; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; margin:0; padding:0;">
<table cellspacing="0" cellpadding="0" border="0" width="100%">
<tr>
    <td align="center" valign="top" style="padding:20px 0 20px 0">
        <table bgcolor="#FFFFFF" cellspacing="0" cellpadding="0" border="0" width="590">
            <!-- [ header starts here] -->
            <tr>
                <td valign="middle" style="padding: 0 25px;" height="80"><a href="{{store url=""}}"><img src="{{skin url='images/logo_email.gif'}}" alt="{{var logo_alt}}" border="0"/></a></td>
            </tr>
            <tr>
                <td height="10"><img src="{{skin url='images/email-line.gif'}}"  style="margin-bottom:10px;" border="0"/></td>
            </tr>
            <!-- [ middle starts here] -->
            <tr>
                <td valign="top" style="padding:15px 30px;">
                    <h1 style="font-size:24px; font-weight:bold; line-height:24px; margin:0 0 11px 0;color:#28448d;">สวัสดีค่ะ, {{htmlescape var=$order.getCustomerName()}}</h1>
                    <p style="font-size:13px; line-height:18px; margin:0;color:#505050;">
                        ขอบคุณค่ะ สำหรับการสั่งซื้อจาก {{var store.getFrontendName()}}.
                        คุณสามารถตรวจสอบสถานะการสั่งซื้อโดย <a href="{{store url="customer/account/"}}" style="color:#28438c; text-decoration:none;">เข้าสู่ระบบ</a>
                        หากท่านมีคำถามหรือข้อสงสัยใดๆ สามารถติดต่อเราได้ที่  <a href="mailto:{{config path='trans_email/ident_support/email'}}" style="color:#28438c; text-decoration:none;">{{config path='trans_email/ident_support/email'}}</a> หรือที่หมายเลขโทรศัพท์ {{config path='general/store_information/phone'}}
                    </p>
                </td>
            </tr>
            <tr>
                <td valign="top" style="padding:15px 30px;">
                    <h2 style="font-size:12px; font-weight:normal; margin:0;color:#505050;">ใบสั่งซื้อ #{{var order.increment_id}}</h2>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table cellspacing="0" cellpadding="0" border="0" width="590" style="border:3px solid #e1e1e1;">
                        <thead>
                        <tr>
                            <th align="left" width="290" style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;color:#505050;">ข้อมูลในการออกใบเสร็จ:</th>
                            <th align="left" width="290" style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;color:#505050;">วิธีการชำระเงิน:</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr><td colspan="2" height="3" bgcolor="#e1e1e1"></td></tr>
                        <tr>
                            <td valign="top" style="font-size:12px; padding:7px 9px 9px 9px;color:#505050;">
                                {{var order.billing_address.format('html')}}
                            </td>
                            <td valign="top" style="font-size:12px; padding:7px 9px 9px 9px; color:#505050;">
                                {{var payment_html}}
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <br/>
                    {{depend order.getIsNotVirtual()}}
                    <table cellspacing="0" cellpadding="0" border="0" width="590" style="border:3px solid #e1e1e1;">
                        <thead>
                        <tr>
                            <th align="left" width="290"  style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;color:#505050;">ข้อมูลการจัดส่งสินค้า:</th>
                            <th align="left" width="290"  style="font-size:13px; padding:5px 9px 6px 9px; line-height:1em;color:#505050;">วิธีการจัดส่ง:</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr><td colspan="2" height="3" bgcolor="#e1e1e1"></td></tr>
                        <tr>
                            <td valign="top" style="font-size:12px; padding:7px 9px 9px 9px;color:#505050;">
                                {{var order.shipping_address.format('html')}}
                                &nbsp;
                            </td>
                            <td valign="top" style="font-size:12px; padding:7px 9px 9px 9px;color:#505050;">
                                {{var order.shipping_description}}
                                &nbsp;
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <br/>
                    {{/depend}}
                    {{layout handle="sales_email_order_creditmemo_items" creditmemo=$creditmemo order=$order}}
                    <p style="font-size:12px; margin:0 0 10px 0">{{var comment}}</p>
                </td>
            </tr>
            <tr>
                <td height="30"><img src="{{skin url='images/email-line.gif'}}"  style="margin-top:20px;" border="0"/></td>
            </tr>
            <tr>
                <td style="padding:20px;">
                    <span style="font:normal 11px 'Arial';color:#505050; line-height:13px;">
                    พบข้อสงสัยในการใช้บริการ
ติดต่อแผนกลูกค้าสัมพันธ์ที่หมายเลข {{config path='general/store_information/phone'}} ในเวลาทำการ จันทร์ – ศุกร์ 10.00 – 19.00 น. <br/>นอกเหนือเวลาดังกล่าวท่านสามารถติดต่อผ่านได้ที่อีเมล์  <a href="mailto:{{config path='trans_email/ident_support/email'}}" style="color:#28438c; text-decoration:none;">{{config path='trans_email/ident_support/email'}}</a> </span>
<br/><br/>
                    <span style="font:normal 11px 'Arial';color:#505050;line-height:13px;">
                    กรุณาบันทึกอีเมล์ฉบับนี้ในสมุดที่อยู่ของคุณเพื่อจะทำให้แน่ใจว่าคุณได้รับอีเมล์ทุกฉบับจากเรา </span><br/><br/>
                    <table cellpadding="0" cellspacing="0" width="590">
                        <tr>
                            <td width="450">
                                <span style="font:normal 11px 'Arial';color:#505050;line-height:13px;">
                                อีเมล์ฉบับนี้ถูกส่งโดย Whatsnew Co., Ltd. <br/>อาคารเศรษฐีวรรณ ชั้น 17<br/>139 ถนนปั้น แขวงสีลม เขตบางรัก กรุงเทพฯ ประเทศไทย 10500</span><br/><br/>
                                <span style="font:normal 11px 'Arial';color:#505050;line-height:13px;">
                                <a href="{{store url="privacy-policy"}}" style="color:#505050; text-decoration:none;">ความเป็นส่วนตัว</a> | <a href="{{store url="contacts"}}" style="color:#505050; text-decoration:none;">ติดต่อเรา</a></span>
                            </td>
                            <td valign="bottom" align="right">
								<a href="https://www.facebook.com/moxyst" target="_blank"><img src="{{skin url='images/email-facebook.gif'}}"  style="margin-right:10px;" border="0"/></a>
								<a href="https://twitter.com/moxyst" target="_blank"><img src="{{skin url='images/email-twitter.gif'}}"  style="margin-right:10px;" border="0"/></a>
								<a href="https://plus.google.com/+Moxyst" target="_blank"><img src="{{skin url='images/email-google.gif'}}" border="0"/></a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </td>
</tr>
</table>
</div>
</body>
HTML;

/** @var Mage_Core_Model_Email_Template $template */
$template = Mage::getModel('core/email_template');
$template->loadByCode('New Crediit Memo (Moxy Thai)')
    ->setTemplateCode('New Crediit Memo (Moxy Thai)')
    ->setTemplateText($template_text)
    ->setTemplateSubject('{{var store.getFrontendName()}}: Credit Memo for Order # {{var order.increment_id}}')
    ->setTemplateType(Mage_Core_Model_Email_Template::TYPE_HTML);

try {
    $template->save();
} catch (Mage_Exception $e) {
    throw $e;
}