<?php
// Init Magento
Mage::app();
// Set current store
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
// Get config of OneStepCheckout
$value = Mage::getStoreConfig('onestepcheckout/onestepconfig/apply', Mage_Core_Model_App::ADMIN_STORE_ID);
// Append value
$value .= ',default/moxy,default/moxy_mobile';
// Save config
$configurator = new Mage_Core_Model_Config();
$configurator->saveConfig('onestepcheckout/onestepconfig/apply', $value, Mage_Core_Model_App::DISTRO_STORE_CODE, Mage_Core_Model_App::ADMIN_STORE_ID);