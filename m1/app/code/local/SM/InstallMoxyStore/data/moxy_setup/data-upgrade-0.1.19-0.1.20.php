<?php

// Base-name
$name           = 'moxy';
$engStoreName   = 'Moxy English';
$engStoreCode   = 'moxyen';
$thStoreName    = 'Moxy Thai';
$thStoreCode    = 'moxyth';
$storeIdMoxyEn  = Mage::getModel('core/store')->load($engStoreCode, 'code')->getId();
$storeIdMoxyTh  = Mage::getModel('core/store')->load($thStoreCode, 'code')->getId();
$newProductsCateId = 0;
$bestSellersCateId = 0;
try {
    $installer = $this;
    $installer->startSetup();

    //==========================================================================
    // Help en Page
    //==========================================================================
    $pageTitle = "Help";
    $pageIdentifier = "help";
    $pageStores = array($storeIdMoxyEn);
    $pageIsActive = 1;
    $pageUnderVersionControl = 0;
    $pageContentHeading = "Help";
    $pageContent = <<<EOD
<div>
<ul id="helpMenu">
<li><a href="javascript:void(0)" data-id="faq">FAQ</a></li>
<li><a href="javascript:void(0)" data-id="howtoorder">How To Order</a></li>
<li><a href="javascript:void(0)" data-id="payments">Payments</a></li>
<li><a href="javascript:void(0)" data-id="shipping">Shipping</a></li>
<li><a href="javascript:void(0)" data-id="rewardpoints">Reward Points</a></li>
<li><a href="javascript:void(0)" data-id="returns">Cancellation and Returns</a></li>
</ul>
</div>
<div class="helpContent">
<div id="faq" class="subcontent">
<h2>FAQ</h2>
<ol class="accordion">
<li>
<div class="accord-header">Who is Moxy?</div>
<div class="accord-content">Moxy is a modern women’s online companion in South East Asia curated for women to find inspiration, explore new ideas and discover unique products through a social shopping experience. We understand and deliver her a smarter shopping experience, tailored to her needs and interests. Because women want it all. With the best selection of lifestyle products, we guarantee seamless ordering and fast shipping to your doorstep 24 hours a day.</div>
</li>
<li>
<div class="accord-header">How did Moxy come about?</div>
<div class="accord-content">Moxy realizes that women have a diverse and expansive lifestyle. At Moxy, we curate a variety of products, from basics to every retail therapy for women providing exclusive lifestyle products at good prices. It’s the beauty of the Moxy experience: women will find what they need and discover what they want. Moxy empowers women. </div>
</li>
<li>
<div class="accord-header">Where is Moxy based?</div>
<div class="accord-content">We are based at<br /> Sethiwan Tower - 19th Floor<br /> 139 Pan Road, Silom, Bangrak,<br /> Bangkok, 10500 Thailand<br /> Tel: 02-106-8222 <br /> Email <a href="mailto:support@moxyst.com">support@moxyst.com</a></div>
</li>
<li>
<div class="accord-header">If I were interested in partnering with Moxy, how would I go about it?</div>
<div class="accord-content">The company welcomes any and all business ventures and ideas that you might have that will ultimately add value to our members. To discuss this further, please contact us on <a href="mailto:support@moxyst.com">support@moxyst.com</a></div>
</li>
<li>
<div class="accord-header">Free Shipping Promotion</div>
<div class="accord-content">Free Delivery anywhere in Bangkok with the minimun purchase of 500 baht for each order
<ul style="margin-left: 30px; list-style: square !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">฿100 flat fee for shipping nationwide exclude Bangkok</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Flat shipping fee of ฿100 will be applied to the order less than ฿500 in Bangkok and ฿200 for nationwide exclude Bangkok</li>
</ul>
</div>
</li>
<li>
<div class="accord-header">I forgot my password</div>
<div class="accord-content">If you have forgotten your password, don&rsquo;t worry. We have systems in place to help you recover them. <a href="{{store url='forgot-password'}}" target="_blank">Click here</a> for more information.</div>
</li>
<li>
<div class="accord-header">The item I want is out of stock. What now?</div>
<div class="accord-content">Although we try to maintain a stock level that matches expected demand, highly popular items may sometimes be out of stock. Please contact our Customer Service team, who will quickly find a solution for you. Happy and healthy customers are important to us.</div>
</li>
<li>
<div class="accord-header">Can I place an item on hold for purchase at a later date?</div>
<div class="accord-content">Sorry, but we do not offer this option. Our goal is to ensure all customers can immediately purchase our great products.</div>
</li>
<li>
<div class="accord-header">How do I know if you carry a certain brand?</div>
<div class="accord-content">You can use the search function, which is available on the top right of the page or go through our brands list buy clicking on either dog or cat.</div>
</li>
<li>
<div class="accord-header">When can I expect new products to be listed on your website?</div>
<div class="accord-content">We constantly update our products with new lines and new brands. Please visit our website to see the latest items we have in stock.</div>
</li>
<li class="last">
<div class="accord-header">Is there somewhere I can go to view the product prior to purchasing?</div>
<div class="accord-content">Unfortunately, we do not have a retail store and for safety reasons we cannot allow customers in the warehouse. If you have any questions regarding the products, please do not hesitate to contact our Customer Service team.</div>
</li>
</ol></div>
<div id="howtoorder" class="subcontent">
<h2>How To Order</h2>
<ol class="accordion">
<li>
<div class="accord-header">How do I order?</div>
<div class="accord-content">Ordering at Moxyst.com is very easy. The steps can be found <a href="{{store url='how-to-order'}}" target="_blank">here</a>.</div>
</li>
<li>
<div class="accord-header">How do I register a new account?</div>
<div class="accord-content">Please click on 'Register', which is located on the top right hand side of the page. You will then be prompted to a new page where you will find 'New Customer' on the left hand side. Fill in your details as requested before clicking 'submit'. Registration should now be complete and you should receive a confirmation e-mail to the address you registered with.</div>
</li>
<li>
<div class="accord-header">Is there any registration fee for Moxy?</div>
<div class="accord-content">Registration at Moxy is completely free and easy! Any form of charge will only come from purchasing of Moxy products.</div>
</li>
<li>
<div class="accord-header">I need personal assistance with my order. Who can I contact?</div>
<div class="accord-content">Our Customer Service team will be happy to assist you. Please e-mail us at <a href="mailto:support@moxyst.com">support@moxyst.com</a> or call us at 02-106-8222.</div>
</li>
<li class="last">
<div class="accord-header">Do you take orders over the phone?</div>
<div class="accord-content">Yes. We do take orders over phone. The payment mode possible for these orders is Cash On Delivery only. Click here for more details.</div>
</li>
</ol></div>
<div id="payments" class="subcontent">
<h2>Payments</h2>
<ol class="accordion">
<li>
<div class="accord-header">Is my credit card information safe at Moxy?</div>
<div class="accord-content">The company will not store any credit card information on our servers. At Moxy, we use systems set in place by banks that have the security protocols set in place to handle safe credit card transactions.</div>
</li>
<li>
<div class="accord-header">My credit card details are not being accepted. What's wrong?</div>
<div class="accord-content">Please check with your bank or financial institution to rule out errors on their behalf. If problems continue to persist, please contact Customer Service who will notify the IT department of technical difficulties.</div>
</li>
<li>
<div class="accord-header">My computer froze while processing payment. How will I know that my payment went through successfully?</div>
<div class="accord-content">All successful transactions will receive a confirmation email that contains an order tracking number. If you have not received confirmation via email, please try placing your order again. Alternatively, please contact our Customer Service team to confirm the placement of your order.</div>
</li>
<li>
<div class="accord-header">How many payment methods does Moxy have?</div>
<div class="accord-content">At Moxy, we provide customers the ability to pay using the following facilities:
<ul style="margin-left: 30px; list-style: square !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Credit card and Debit card</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">PayPal</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Cash on delivery (pay when we deliver the product)</li>
</ul>
<!-- <strong>Payment Fee</strong>
				<ul style="margin-left: 20px; list-style: square !important; margin-top: 7px;">
					<li style="border-bottom: 0; padding: 5px; font-weight: normal;"><strong>
						Counter Service</strong>
						<br />
						Payments can be done through counter services at Seven Eleven. The conditions are as follows; Purchases that exceed 500 baht will be charged with a 15 baht fee. As for orders below 500 baht, 25 baht will be collected for this service. After the payment is completed, we will send you a confirmation email for your orders.
					</li>
					<li style="border-bottom: 0; padding: 5px; font-weight: normal;">
						<strong>Tesco Lotus Check Out Counter</strong>
						<br />
						Payment can be done through Tesco Lotus Check Out Counters in all Tesco Lotus stores. A payment form is be printed out and then used for payment at any check out counters of Tesco Lotus with a fee of 7 baht. After the payment is completed, we will send you a confirmation email for your orders.
					</li>
					<li style="border-bottom: 0; padding: 5px; font-weight: normal;">
						<strong>Just pay</strong>
						<br />
						Payment can be completed at TOT service centers that displays a Just pay system that allows you to make payments with an additional fee of 15 baht. A payment form can be printed and presented at the cashier in every TOT service counters throughtout Thailand. After the payment is completed, we will send you a confirmation email for your orders.
					</li>
					<li style="border-bottom: 0; padding: 5px; font-weight: normal;">
						<strong>True Money Express</strong>
						<br />
						Payment can be purchased at every branch of True Money Express. A service fee of 10 baht will be acquired in order complete your payment. Print your payment form and the form is given to the cashier to complete the payment at any True Money Express throughout Thailand. After the payment is completed, we will send you a confirmation email for your orders.
					</li>
					<li style="border-bottom: 0; padding: 5px; font-weight: normal;">
						<strong>Pay@post</strong>
						<br />
						Payment can be done at every post office in Thailand. To use this service, an additional fee of 10 baht will be charged. You can present your payment form at any Post office cashier available in your area. After the payment is completed, we will send you a confirmation email for your orders.
					</li>
					<li style="border-bottom: 0; padding: 5px; font-weight: normal;">
						<strong>Bangkok Bank</strong>
						<br />
						You can complete your payment through an ATM, Bualuang i-banking service, by charging 10 baht for areas in and close to Bangkok. For other provices, a 20 baht fee will be charged. If you pay at the bank's counter service, the fee will be 15 baht for areas in and close to Bankok, and 30 baht for places outside Bangkok.
					</li>
					<li style="border-bottom: 0; padding: 5px; font-weight: normal;">
						<strong>Kasikorn Bank</strong>
						<br />
						You can pay via ATM, K-cyber Banking, and bank counter services. A fee of 10 baht will be charged in areas within Bangkok and 20 baht for areas outside of Bangkok.
					</li>
					<li style="border-bottom: 0; padding: 5px; font-weight: normal;">
						<strong>Siam Commercial Bank</strong>
						<br />
						Payments can be done through an ATM, SCB Easy Net, and bank counters. An additional fee of 15 baht is required for all areas of delivery.
					</li>
					<li style="border-bottom: 0; padding: 5px; font-weight: normal;">
						<strong>TMB</strong>
						<br />
						Payment can be completed through an ATM account,
					</li>
					<li style="border-bottom: 0; padding: 5px; font-weight: normal;">
						<strong>Krungthai Bank</strong>
						<br />
						You can pay via ATM, KTB Online, and at Bank counters. The bank will charge 10 baht extra for areas in and near Bangkok, and 15 baht extra for areas outside Bangkok.
					</li>
					<li style="border-bottom: 0; padding: 5px; font-weight: normal;">
						<strong>Krungsri Bank</strong>
						<br />
						Payments can be done through an ATM, Krungsri online, and bank counters. An additional fee of 15 baht is requred for areas in and near Bangkok, as for other provinces 30 baht is required.
					</li>
					<li style="border-bottom: 0; padding: 5px; font-weight: normal;">
						<strong>UOB</strong>
						<br />
						Payments can be completed through an ATM, UOB Cybe Banking, and bank counters. An extra 15 baht will be charged for purchases in all areas over Thailand.
					</li>
				</ul>
				* Please pay within 48 hours after recieving your order confirmation email, your order will automatically expire if not paid within the specified period.
				<br />
				* Fees are subjected to change by the terms and conditions of the service provider of each payment type. --></div>
</li>
<li>
<div class="accord-header">Are your prices in Thai Baht (THB)?</div>
<div class="accord-content">All pricing is in Thai Baht.</div>
</li>
<li class="last">
<div class="accord-header">Does your prices include taxes?</div>
<div class="accord-content">All prices are inclusive of taxes.</div>
</li>
</ol></div>
<div id="shipping" class="subcontent">
<h2>Shipping</h2>
<ol class="accordion">
<li>
<div class="accord-header">When will my order ship?</div>
<div class="accord-content">All items are shipped within 24 hours after the purchase is made and most of the time even sooner (weekdays only). All Moxy deliveries are sent via Premium Courier. The deliveries are made between 9:00 am - 7:00 pm. Currently, we deliver from Monday to Saturday.</div>
</li>
<li>
<div class="accord-header">How long will it take until I receive my order?</div>
<div class="accord-content">We ship via our own delivery fleet and several other premium carriers. If you order by 4 pm on a business day, you will receive it in 2-3 business days. We do not yet guaranteed overnight shipping, although many areas of the country will receive their orders the next day. <br /><br /> * For Sangkhlaburi district, islands and provinces including Pattani, Yala, Naradhiwas, Mae Hong Son and Chiang Rai, delivery may take 1-2 days longer due to difficult routes. We&rsquo;ll try our best to deliver the product to you as soon as possible.</div>
</li>
<li>
<div class="accord-header">How do I track my order?</div>
<div class="accord-content">When your order ships, you will receive an email with the shipping and tracking information. You can use your tracking number on our website to trace your order. You can also find your tracking number with your order details; Click the "Order Status" button under the "My Account" menu after you have logged on. <br /><br /> Please check with your neighbors or building manager if you have not received your product even though the tracking says that it has been delivered. If the tracking information doesn't show up or shows that the item has been returned to us, please contact us by email or phone. It is possible that we may have the wrong shipping address, the driver may have unsuccessfully attempted to deliver, or the shipping carrier may have misplaced the product.</div>
</li>
<li>
<div class="accord-header">What happens if I am not at home when my order is being delivered?</div>
<div class="accord-content">In the process of delivering the product to you, our driver will contact you on the number provided to ensure that you are at the address that you have given to us. If you are not home at the time, do inform someone else in your household to beaware of our delivery so they can sign for the product on your behalf. IIf we are still unable to deliver the product to you despite our best efforts, the money paid for the product will be returned to you, but not the shipping fee (For more detail please check our &ldquo;<a href="{{store url='return-policy'}}" target="_blank">refund policy</a>&rdquo;). Undeliverable orders will not be resent and you must place a new order.</div>
</li>
<li>
<div class="accord-header">Can I have my order shipped to my office?</div>
<div class="accord-content">We are happy to delivery to your office. If you would like us to do that, please input your office address at checkout. Your office delivery will be made from the 9am to 7pm on weekdays and 9am to 12 noon on weekends.</div>
</li>
<li>
<div class="accord-header">How much is the shipping fee?</div>
<div class="accord-content">Free Delivery anywhere in Bangkok with the minimun purchase of 500 baht for each order
<ul style="margin-left: 30px; list-style: square !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">฿100 flat fee for shipping nationwide exclude Bangkok</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Flat shipping fee of ฿100 will be applied to the order less than ฿500 in Bangkok and ฿200 for nationwide exclude Bangkok</li>
</ul>
</div>
</li>
<li>
<div class="accord-header">Do you ship outside of Thailand?</div>
<div class="accord-content">Sorry, we currently do not ship outside of Thailand.</div>
</li>
<li class="last">
<div class="accord-header">Why is my shipment delayed?</div>
<div class="accord-content">You must order by 4 p.m. for 1-2-day shipping to apply. We try our best to make sure that you receive your order as soon as possible, however, there are some situations beyond our control that can delay your shipment.<br />The following orders may result in delayed shipping:
<ul style="margin-left: 30px; list-style: square !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Incorrect shipping address</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Incorrect Mobile Phone number</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Unable to reach you at provided contact number</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Being absent after courier made an appointment</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Shipping address does not match billing address</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Payment delay or issue</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Courier failure to deliver (e.g. severe weather conditions)</li>
</ul>
</div>
</li>
</ol></div>
<div id="rewardpoints" class="subcontent">
<h2>Reward Points</h2>
<ol class="accordion">
<li>
<div class="accord-header">1% Cash-back policy</div>
<div class="accord-content">A 1% cash back means that for every purchase made, you will receive 1% cash-back, which will be stored in your Moxy account. You can then use this credit as a discount on your next purchase.</div>
</li>
<li>
<div class="accord-header">How do I get points for referring a friend?</div>
<div class="accord-content">There are many ways where you can get ฿100 by <a href="{{store url='rewardpoints/index/referral'}}" target="_blank">referring a friend</a>at Moxyst.com
<ul style="margin-left: 30px; list-style: square !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">You can share your unique link which can be found in My Accounts straight to your Facebook or Twitter page. You can also email this link straight to your friends.</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Once your friends have clicked on the link and have made a purchase on Moxy, you will automatically be credited with ฿100 in your account in the form of credits.</li>
</ul>
</div>
</li>
<li class="last">
<div class="accord-header">Reward Points Terms &amp; Conditions</div>
<div class="accord-content">Each time you shop on our website, you will earn Reward Points on your purchases. The Reward Points will be accumulated each time you purchase the products on our website and it will be expired within 6 months from your purchasing date. <br /><br /> For example, if you earn 20 points on January from your first purchase and another 30 points on March, your Reward Point balance is 50 points. On July, your balance will be deducted to 30 points because 20 points on January are already expired. In case you have not used your point within September, you point balance will become 0 as your points earned previously on March are expired. <br /><br /> For your own interest, please kindly redeem your Reward Points in the available period of time.</div>
</li>
</ol></div>
<div id="returns" class="subcontent">
		<h2>Cancellation and Returns</h2>
		<ol class="accordion">
			<li>
			<div class="accord-header"><span></span>How do I return my purchase?</div>
			<div class="accord-content">
				<strong>Returns are FREE, fast and easy!</strong><br/>
				If you wish to return an item, please contact our customer care and request a return of the product(s) via phone 02-106-8222 or Email <a href="mailto:support@moxyst.com">support@moxyst.com</a>.  If the conditions of the returned product aligned within our return policy, please fill in the Returns Slip you received with your shipment, enclose the invoice in the package then send it back to us in its original packaging. Please send the product back to Moxyst.com at any Thailand Post location nearby your premise under domestic registered mail service.
				 <br/><br/>
				NOTE: Sorry for any inconveniences may cause you, return shipments are not eligible for scheduled pick-ups by Moxyst.com.
				 <br/><br/>

				Our address is:<br/>
				Acommerce Co.,Ltd<br/>
8/2 Rama3 Rd, Soi 53, <br/>Bang Phong Pang, Yarn-Nava, <br/>Bangkok 10120<br/><br/>


				Moxyst.com will absorb the incurred shipping costs in this registered returns back to you on the refund process. We ask that you keep the receipt/slip of this shipping cost issued by the post office as an evidence. And please send your receipt/slip back to Customer Care by submitting a photo file to the email <a href="mailto:support@moxyst.com">support@moxyst.com</a>

			</div>
			</li>
			<li>
			<div class="accord-header"><span></span>Can I cancel my order?</div>
			<div class="accord-content">
				If you wish to cancel your order, get in touch with customer service as soon as possible with your order number.
If your order has been dispatched, please do not accept delivery and contact customer service. In case of Prepaid orders Refunds will be processed as per our Return policies. Any cashback credits used in the purchase of the order will be credited back to your account.
			</div>
			</li>
			<li>
			<div class="accord-header"><span></span>Packing your return</div>
			<div class="accord-content">
				We suggest that you use the boxes that the products arrived in. Simply apply the shipping label acquired through the online return process or from our Customer Service team. Please put the return label to your box/package and drop it off at any Thailand Post location. NOTE: Return shipments are not eligible for scheduled pick-ups by Moxy. Please contact us with any questions or concerns at 02-106-8222 or send an e-mail to <a href="mailto:support@moxyst.com">support@moxyst.com</a>
			</div>
			</li>
			<li>
			<div class="accord-header"><span></span>How long will it take for my return to be processed?</div>
			<div class="accord-content">
				Once we receive the item(s), we will process your request within 14 business days.
			</div>
			</li>
			<li>
			<div class="accord-header"><span></span>When do I receive my refund?</div>
			<div class="accord-content">
				Once we receive your item(s) and have checked that it is in original condition, a refund will be initiated immediately. Please allow up to 7 business days to receive your refund from the time we receive your return. Moxy will refund your money through Bank Transfer or in store credit, if preferred.
			</div>
			</li>
			<li>
			<div class="accord-header"><span></span>What are the shipping charges if I return my purchased items?</div>
			<div class="accord-content">
				The shipping costs will vary according to your location and method of postage you choose. Please make sure to ship back to us via Thailand post by registered mail and we will refund the shipping fees after receiving the product.
			</div>
			</li>
			<li class="last">
			<div class="accord-header"><span></span>What if my item has a defect?</div>
			<div class="accord-content">
				Our Quality Control department tries to ensure that all products are of a high quality when they leave the warehouse. In the rare circumstance that your item has a defect, you may send it back to us with a completed Returns Slip attached within 30 days upon receiving the item(s), a full refund will be issued to you.
			</div>
			</li>
		</ol>
	</div>
</div>
<script type="text/javascript">// <![CDATA[
 jQuery(document).ready(function()
 {
  var tmpUrl = "";
  var h=window.location.hash.substring(0);
  if(h == ""){
   //alert('Hash not found!!!');
   h = "#faq";
   //alert(heart);
   jQuery( "#helpMenu li:first-child" ).addClass( "helpactive");
   jQuery('.subcontent[id='+h.substring(1)+']').fadeIn();
  }else{
   jQuery('#helpMenu li a[data-id='+h.substring(1)+']').parent().addClass( "helpactive");
  jQuery('.subcontent[id='+h.substring(1)+']').fadeIn();
  }

  //////////////////////////////
  jQuery('#keyMess li').on('click','a',function()  {
   tmpUrl=jQuery(this).attr('href');
   h=tmpUrl.substring(tmpUrl.indexOf("#")+1);

   jQuery('.subcontent:visible').fadeOut(0);
   jQuery('.subcontent[id='+h+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
  });
  jQuery('#zyncNav li').on('click','a',function()
  {
   tmpUrl=jQuery(this).attr('href');
   h=tmpUrl.substring(tmpUrl.indexOf("#")+1);
   jQuery('.subcontent:visible').fadeOut(0);
   jQuery('.subcontent[id='+h+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
  });
  jQuery('#footerBanner div').on('click','a',function()
  {
   tmpUrl=jQuery(this).attr('href');
   h=tmpUrl.substring(tmpUrl.indexOf("#")+1);
   jQuery('.subcontent:visible').fadeOut(0);
   jQuery('.subcontent[id='+h+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
  });
  jQuery('#topMklink').on('click','a',function()
  {
   tmpUrl=jQuery(this).attr('href');
   h=tmpUrl.substring(tmpUrl.indexOf("#")+1);
   jQuery('.subcontent:visible').fadeOut(0);
   jQuery('.subcontent[id='+h+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
  });
  jQuery('#footer_help li').on('click','a',function()
  {
   tmpUrl=jQuery(this).attr('href');
   h=tmpUrl.substring(tmpUrl.indexOf("#")+1);
   jQuery('.subcontent:visible').fadeOut(0);
   jQuery('.subcontent[id='+h+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
  });
 ///////////////////////////////

     jQuery('#helpMenu').on('click','a',function()
     {
         jQuery('.subcontent:visible').fadeOut(0);
         jQuery('.subcontent[id='+jQuery(this).attr('data-id')+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery(this).parent().addClass( "helpactive");
     });

///////////////////////////////

     jQuery(".accordion li .accord-header").mouseenter(function() {
	    	jQuery(this).css("cursor","pointer");
	    });
	    jQuery(".accordion li .accord-header").click(function() {
	    	jQuery(this).find("span").removeClass('active');
	      if(jQuery(this).next("div").is(":visible")){
	        jQuery(this).next("div").slideUp(300);
	      } else {
	        jQuery(".accordion li .accord-content").slideUp(400);
	        jQuery(this).next("div").slideToggle(400);
	        jQuery(".accordion li .accord-header").find("span").removeClass('active');
	        jQuery(this).find("span").addClass('active');
	      }
	    });
	  });
// ]]></script>
EOD;

    $pageRootTemplate = 'one_column';
    $pageLayoutUpdateXml = <<<EOD
EOD;


    $pageCustomLayoutUpdateXML = <<<EOD
EOD;

    $pageMetaKeywords = "";
    $pageMetaDescription = "";

    $page = Mage::getModel('cms/page')->getCollection()
        ->addStoreFilter(array($storeIdMoxyEn), $withAdmin = true)
        ->addFieldToFilter('identifier', $pageIdentifier)
        ->getFirstItem();
    if ($page->getId() == 0) {
        $page = Mage::getModel('cms/page');
    }
    else
    {
        // if exists then repair
        $page = Mage::getModel('cms/page')->load($page->getId());
    }

    $page->setTitle($pageTitle);
    $page->setIdentifier($pageIdentifier);
    $page->setStores($pageStores);
    $page->setIsActive($pageIsActive);
    $page->setUnderVersionControl($pageUnderVersionControl);
    $page->setContentHeading($pageContentHeading);
    $page->setContent($pageContent);
    $page->setRootTemplate($pageRootTemplate);
    $page->setLayoutUpdateXml($pageLayoutUpdateXml);
    $page->setCustomLayoutUpdateXml($pageCustomLayoutUpdateXML);
    $page->setMetaKeywords($pageMetaKeywords);
    $page->setMetaDescription($pageMetaDescription);
    $page->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================

    //==========================================================================
    // Help th Page
    //==========================================================================
    $pageTitle = "วิธีการสั่งซื้อ และคำถามที่พบบ่อย";
    $pageIdentifier = "help";
    $pageStores = array($storeIdMoxyTh);
    $pageIsActive = 1;
    $pageUnderVersionControl = 0;
    $pageContentHeading = "ช่วยเหลือ";
    $pageContent = <<<EOD
<div>
<ul id="helpMenu">
<li><a href="javascript:void(0)" data-id="faq">คำถามที่พบบ่อย</a></li>
<li><a href="javascript:void(0)" data-id="howtoorder">วิธีการการสั่งซื้อ</a></li>
<li><a href="javascript:void(0)" data-id="payments">การชำระเงิน</a></li>
<li><a href="javascript:void(0)" data-id="shipping">การจัดส่งสินค้า</a></li>
<li><a href="javascript:void(0)" data-id="rewardpoints">คะแนนสะสม</a></li>
<li><a href="javascript:void(0)" data-id="returns">การคืนสินค้าและขอคืนเงิน</a></li></ul>
</div>
<div class="helpContent">
<div id="faq" class="subcontent">
<h2>FAQ</h2>
<ol class="accordion">
<li>
<div class="accord-header">Moxy คืออะไร</div>
<div class="accord-content">Moxy  เป็น  Women’s e-Commerce หนึ่งเดียวในภูมิภาคเอเชียตะวันออกเฉียงใต้ ที่ช่วยเพิ่มความสะดวกสบายในการช็อปปิ้งให้มากขึ้น  โดยเฉพาะอย่างยิ่งสำหรับกลุ่มคนที่ไม่มีเวลา  <br />ช็อปปิ้งออนไลน์จึงเป็นอีกทางเลือกหนึ่งที่ถูกออกแบบมาเพื่อตอบสนองความต้องการของผู้บริโภคในปัจจุบัน <br />นอกจากนี้เรายังให้ความสำคัญกับผลิตภัณฑ์สินค้าที่แบ่งออกตามความแตกต่างและไลฟสไตล์ของผู้หญิงในแบบต่างๆ <br/>ทางเราได้เลือกสรรค์สินค้าที่ผู้หญิงต้องการไม่ว่าจะเป็นในแบบรายเดือนหรือชีวิตประจำวันที่มีความเป็นเอกลักษณ์เฉพาะตัวจากทั่วทุกมุมโลกและส่งตรงถึงมือลูกค้า</div>
</li>
<li>
<div class="accord-header">Moxy มีที่มาอย่างไร</div>
<div class="accord-content">จุดมุ่งหมายหลักของ Moxy คือให้ผู้หญิงได้เข้ามาค้นหา ความเป็นตัวเองในม็อกซี่ และได้รับแรงบัลดาลใจและอัพเดทสิ่งต่างๆที่เกี่ยวกับผู้หญิง ไม่เพียงแค่ในประเทศไทยเท่านั้น แต่เรายังเปน หนึ่งเดียวในภูมิภาคเอเชียตะวันออกเฉียงใต้ที่รวมเอาเรื่องราวต่างๆที่เกี่ยวกับผู้หญิงและผลิตภัณฑ์สินค้าต่างๆอีกมากมาย ที่คัดสรรเป็นอย่างดีจากทั่วทุกมุมโลก ผ่านโลกอินเตอร์เนต ที่จะช่วยคุณประหยัดเวลาและลดความยุ่งยากโดยการจัดหาสิ่งที่คุณต้องการมาให้ พร้อมกับให้ข้อมูลเพื่อช่วยให้คุณตัดสินใจได้ว่าอะไรเหมาะกับคุณที่สุด เรามีสินค้าจากแบรนด์ที่เป็นที่รู้จักจากทั้งในและต่างประเทศ ทั้งยังสามารถส่งตรงถึงมือคุณได้ภายใน 1-2 วัน</div>
</li>
<li>
<div class="accord-header">Moxy ตั้งอยู่ที่ไหน</div>
<div class="accord-content">บริษัทตั้งอยู่ที่ อาคารเศรษฐีวรรณ ชั้น 19 <br /> 139 ถนนปั้น แขวงสีลม เขตบางรัก <br /> กรุงเทพฯ ประเทศไทย 10500 <br /> โทรศัพท์: 02-106-8222<br /> อีเมล์ <a href="mailto:support@moxyst.com">support@moxyst.com</a></div>
</li>
<li>
<div class="accord-header">สนใจอยากร่วมธุรกิจกับ Moxy ทำอย่างไร</div>
<div class="accord-content">บริษัทยินดีต้อนรับผู้ที่สนใจเข้าเป็นส่วนหนึ่งกับเราได้ โดยท่านที่สนใจนำเสนอสินค้าเพื่อสุขภาพ คุณภาพสูงผ่านทางหน้าเว็บไซต์ Moxy สามารถติดต่อได้ที่อีเมล <a href="mailto:support@moxyst.com">support@moxyst.com</a></div>
</li>
<li>
<div class="accord-header">โปรโมชั่นจัดส่งสินค้าฟรี</div>
<div class="accord-content">จัดส่งฟรีเมื่อซื้อสินค้าขั้นต่ำ 500 บาทต่อการสั่งซื้อ1ครั้ง เฉพาะเขตกรุงเทพ ในส่วนของเขตปริมณฑล และต่างจังหวัด คิดค่าบริการในการจัดส่ง 100 บาท ในกรณีซื้อสินค้าต่ำกว่า 500 บาทเสียค่าจัดส่ง 100 บาทในเขตกรุงเทพ และ 200 บาทในเขตปริมณฑลและต่างจังหวัด</div>
</li>
<li>
<div class="accord-header">วิธีขอรหัสผ่านใหม่</div>
<div class="accord-content">ถ้าคุณลืมรหัสผ่านของคุณ คุณสามารถขอรหัสผ่านใหม่ได้ <a href="{{store url='forgot-password'}}" target="_blank">คลิกที่นี่</a>เพื่อดูรายละเอียดเพิ่มเติม</div>
</li>
<li>
<div class="accord-header">ถ้าสินค้าที่ต้องการหมด จะทำอย่างไร</div>
<div class="accord-content">ถึงแม้ว่าทางเราจะพยายามจัดหาสินค้าให้มีจำนวนเหมาะสมกับระดับความต้องการของลูกค้า แต่สินค้าที่ได้รับความนิยมอย่างมากก็อาจหมดลงได้อย่างรวดเร็ว แต่เราพยายามไม่ให้ปัญหานี้เกิดขึ้น หรือถ้าเกิดขึ้นก็จะพยายามหาทางแก้ไข และหาทางออกให้กับลูกค้าอย่างแน่นอน ถ้ามีข้อสงสัย หรือปัญหาเกี่ยวกับสินค้าสามารถติดต่อได้ที่ฝ่ายลูกค้าสัมพันธ์ได้ในเวลาทำการ</div>
</li>
<li>
<div class="accord-header">ฉันสามารถจองสินค้าไว้ก่อนเพื่อทำการสั่งซื้อในวันถัดไปได้หรือไม่</div>
<div class="accord-content">ต้องขออภัย ทางเราไม่มีนโยบายการจองสินค้า</div>
</li>
<li>
<div class="accord-header">ฉันจะทราบได้อย่างไรว่ามีแบรนด์ไหนที่ขายอยู่บนเว็บไซต์ Moxy บ้าง</div>
<div class="accord-content">คุณสามารถค้นหาสินค้าที่ต้องการได้ โดยกดปุ่มค้นหาที่หน้าเว็บไซต์ Moxy ซึ่งอยู่ด้านบนของหน้าเว็บไซต์ หรือคุณสามารถเข้าไปคลิกเลือกแบรนด์ต่าง ๆ ได้ บนแถบเมนู</div>
</li>
<li>
<div class="accord-header">จะทราบได้อย่างไร ว่ามีสินค้าใหม่เข้ามาในเวบไซต์</div>
<div class="accord-content">ทางเราจะทำการอัพเดทเวบไซต์ใหม่ทันที ที่มีสินค้าใหม่เข้ามาในคลังสินค้า ท่านสามารถตรวจสอบสินค้าใหม่ได้ที่เวบไซต์ของเรา</div>
</li>
<li class="last">
<div class="accord-header">ฉันสามารถแสดงความคิดเห็น หรือให้ข้อแนะนำเกี่ยวกับสินค้าได้อย่างไร</div>
<div class="accord-content">เรายินดีที่รับฟังความคิดเห็น และข้อเสนอแนะเกี่ยวกับผลิตภัณฑ์ของเรา คุณสามารถแสดงความคิดเห็นของคุณมาผ่านมางทางอีเมล <a href="mailto:support@moxyst.com">support@moxyst.com</a></div>
</li>
</ol></div>
<div id="howtoorder" class="subcontent">
<h2>วิธีการการสั่งซื้อ</h2>
<ol class="accordion">
<li>
<div class="accord-header">สั่งซื้อสินค้าผ่าน Moxy อย่างไร?</div>
<div class="accord-content">คุณสามารถสั่งซื้อสินค้ากับ Moxyst.com ด้วยวิธีง่ายๆ <a href="{{store url='how-to-order'}}" target="_blank">คลิกที่นี่</a>เพื่อดูขั้นตอนการสั่งซื้อ</div>
</li>
<li>
<div class="accord-header">สมัครเพื่อลงทะเบียนบัญชีผู้ใช้ใหม่อย่างไร?</div>
<div class="accord-content">เพียงคลิกไปที่ &ldquo;ลงทะเบียน&rdquo; ซึ่งอยู่ด้านบนขวามือของเว็บไซต์ หลังจากนั้นจะเปลี่ยนไปหน้าสร้างบัญชีผู้ใช้ใหม่ซึ่งจะอยู่ทางด้านซ้ายมือ หลังจากกรอกรายละเอียดข้อมูลของท่านเสร็จเรียบร้อยแล้วคลิกที่ &ldquo;ยืนยัน&rdquo; เพื่อเสร็จสิ้นการสมัครสมาชิก หลังจากนั้นคุณจะได้รับอีเมลยืนยันการลงทะเบียนผู้ใช้ใหม่กับทาง Moxy เพียงเท่านี้คุณก็สามารถซื้อสินค้าผ่านทางเราได้อย่างสะดวกสบาย</div>
</li>
<li>
<div class="accord-header">มีค่าธรรมเนียมในการสมัครสมาชิกกับ Moxy หรือไม่</div>
<div class="accord-content">การสมัครสมาชิกไม่มีค่าธรรมเนียมใด ๆ ทั้งสิ้น และจะเกิดค่าใช้จ่ายในกรณีที่มีการสั่งซื้อสินค้าเท่านั้น</div>
</li>
<li>
<div class="accord-header">ถ้าฉันต้องการที่ปรึกษาส่วนตัวสำหรับการสั่งซื้อสามารถติดต่อได้ทางใด</div>
<div class="accord-content">แผนกลูกค้าสัมพันธ์ของเรายินดีเป็นอย่างยิ่งที่จะช่วยเหลือท่านผ่านทางอีเมล <a href="mailto:support@moxyst.com">support@moxyst.com</a> หรือทางหมายเลยโทรศัพท์ 02-106-8222</div>
</li>
<li class="last">
<div class="accord-header">ฉันสามารถสั่งซื้อสินค้าผ่านทางโทรศัพท์ได้หรือไม่?</div>
<div class="accord-content">คุณสามารถสั่งซื้อสินค้าผ่านทางโทรศัพท์ได้ค่ะ เพียงโทรเข้าไปที่แผนกลูกค้าสัมพันธ์ตามหมายเลขที่แจ้งไว้ และต้องชำระค่าสินค้าด้วยวิธีเก็บเงินปลายทางเท่านั้น คลิกที่นี่เพื่อดูรายละเอียดเพิ่มเติม</div>
</li>
</ol></div>
<div id="payments" class="subcontent">
<h2>การชำระเงิน</h2>
<ol class="accordion">
<li>
<div class="accord-header">การใช้บัตรเครดิตกับทาง Moxy ปลอดภัยหรือไม่</div>
<div class="accord-content">บริษัทดูแลข้อมูลของสมาชิก Moxy เป็นอย่างดี ข้อมูลทุกอย่างจะถูกเก็บไว้เป็นความลับและดูแลรักษาอย่างปลอดภัย ด้านข้อมูลเกี่ยวกับบัตรเครดิตทั้งหมดจะถูกส่งผ่านระบบ SSL (Secure Socket Layer) ซึ่งเป็นระบบรักษาความปลอดภัยมาตรฐานในการส่งข้อมูลทางอินเตอร์เน็ตตรงไปยังระบบเซฟอิเล็กทรอนิกส์ของธนาคาร โดยจะไม่มีการเก็บข้อมูลเกี่ยวกับบัตรเครดิตไว้ที่ระบบเซิร์ฟเวอร์ของเว็บไซต์แต่อย่างใด</div>
</li>
<li>
<div class="accord-header">ข้อมูลบัตรเครดิตไม่สามารถใช้ได้ เกิดจากอะไร</div>
<div class="accord-content">กรุณาตรวจสอบสถานะการเงินของท่านจากทางธนาคารหากยังไม่สามารถใช้ได้กรุณาติดต่อแผนกลูกค้าสัมพันธ์ของธนาคารเพื่อแจ้งกับแผนกเทคโนโลยีสารสนเทศเพื่อแก้ไขปัญหาด้านเทคนิค</div>
</li>
<li>
<div class="accord-header">หากเครื่องคอมพิวเตอร์ขัดข้องระหว่างขั้นตอนการชำระเงิน จะทราบได้อย่างไรว่าการชำระเงินเสร็จสิ้นเรียบร้อยแล้ว</div>
<div class="accord-content">เมื่อท่านชำระเงินเรียบร้อยแล้วคำยืนยันการสั่งซื้อจะถูกส่งไปยังอีเมลของท่านหากท่านไม่ได้รับการยืนยันการสั่งซื้อทางอีเมลกรุณาลองทำการสั่งซื้ออีกครั้ง หากท่านมีข้อสงสัยกรุณาติดต่อแผนกลูกค้าสัมพันธ์เพื่อสอบถามสถานะการสั่งซื้อของท่านที่ อีเมล <a href="mailto:support@moxyst.com">support@moxyst.com</a> หรือทางโทรศัพท์หมายเลข 02-106-8222</div>
</li>
<li>
<div class="accord-header">วิธีการชำระเงินมีกี่ช่องทาง</div>
<div class="accord-content">บริษัทมีช่องทางให้ลูกค้าเลือกชำระเงินหลายช่องทาง ตามด้านล่างนี้
<ul style="margin-left: 30px; list-style: square !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">บัตรเครดิต วีซ่า มาสเตอร์ ซึ่งผ่าน่ระบบ Payment ของบริษัท 2C2P</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">PayPal ช่วยให้คุณสามารถชำระค่าสินค้าของคุณหรือชำระด้วยบัตรเครดิตชั้นนำต่างๆ ผ่านทางหน้าเว็บไซด์ได้ทันที โดยจะทำรายการผ่านระบบของ PayPal ทำให้การชำระเงินเป็นไปได้อย่างสะดวก รวดเร็วและปลอดภัย หากท่านยังไม่มีบัญชีกับ Paypal สามารถ คลิกที่นี่เพื่อสมัครบริการ Paypal</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Cash on delivery เป็นการเก็บเงินปลายทางจากผู้รับสินค้า โดยจะมีค่าธรรมเนียมในการเก็บเงินปลายทาง 50 บาท ต่อการสั่งซื้อต่อครั้ง ค่าธรรมเนียมนี้ไม่รวมค่าสินค้า และค่าจัดส่งสินค้า</li>
</ul>
<!-- <strong>อัตราค่าธรรมเนียมแต่ละช่องทาง</strong>
<ul style="margin-left: 20px; list-style: square !important; margin-top: 7px;">
	<li style="border-bottom: 0; padding: 5px; font-weight: normal;">
		<strong>เคาท์เตอร์เซอร์วิส</strong>
		<br />
		ช่องทางการชำระเงินผ่านเคาท์เตอร์เซอร์วิสที่เซเว่นอีเลเว่น มีค่าธรรมเนียมดังนี้ สั่งซื้อสินค้า 500 บาทขึ้นไป เสียค่าธรรมเนียม 15 บาท สั่งซื้อสินค้าไม่ถึง 500 บาท เสียค่าธรรมเนียม 25 บาท หลังจากชำระเงินเรียบร้อยแล้ว ทางเราจะส่งใบยืนยันการสั่งซื้อไปยังอีเมล์ของคุณ
	</li>
	<li style="border-bottom: 0; padding: 5px; font-weight: normal;">
		<strong>จุดชำระเงินเทสโก้โลตัส</strong>
		<br />
		สามารถชำระเงินผ่านจุดชำระ เทสโก้โลตัส ทุกสาขาทั่วประเทศ ในอัตราค่าบริการเพียง 7 บาท* ท่านสามารถพิมพ์แบบฟอร์มการชำระเงินเพื่อนำไปชำระที่จุดชำระเงิน เทสโก้ โลตัส ทุกสาขาทั่วประเทศ หลังจากชำระเรียบร้อยแล้ว เราจะส่งใบยืนยันการสั่งซื้อไปยังอีเมล์ของท่าน
	</li>
	<li style="border-bottom: 0; padding: 5px; font-weight: normal;">
		<strong>Just pay</strong>
		<br />
		สามารถชำระที่ศูนย์บริการลูกค้า ทีโอที ที่มีสัญลักษณ์ Just pay เพื่อชำระค่าสินค้าโดยมีอัตราค่าบริการ 15 บาท* ท่านสามารถพิมพ์แบบฟอร์มการชำระเงินเพื่อนำไปชำระที่จุดชำระเงิน ทุกสาขาทั่วประเทศ หลังจากชำระเรียบร้อยแล้ว เราจะส่งใบยืนยันการสั่งซื้อไปยังอีเมล์ของท่าน
	</li>
	<li style="border-bottom: 0; padding: 5px; font-weight: normal;">
		<strong>True Money Express</strong>
		<br />
		สามารถชำระได้ที่ True Money Express ทุกสาขา เพื่อชำระค่าสินค้าโดยมีอัตราค่าบริการ 10 บาท* ท่านสามารถพิมพ์แบบฟอร์มการชำระเงินเพื่อนำไปชำระที่จุดชำระเงิน ทุกสาขาทั่วประเทศ หลังจากชำระเรียบร้อยแล้ว เราจะส่งใบยืนยันการสั่งซื้อไปยังอีเมล์ของท่าน
	</li>
	<li style="border-bottom: 0; padding: 5px; font-weight: normal;">
		<strong>Pay@post</strong>
		<br />
		สามารถชำระได้ที่ศูนย์บริการไปรษณีย์ไทยทุกสาขา เพื่อชำระค่าสินค้าโดยมีอัตราค่าบริการ 10 บาท* ท่านสามารถพิมพ์แบบฟอร์มการชำระเงินเพื่อนำไปชำระที่จุดชำระเงิน ทุกสาขาทั่วประเทศ หลังจากชำระเรียบร้อยแล้ว เราจะทำการส่งใบยืนยันการสั่งซื้อไปยังอีเมล์ของท่าน
	</li>
	<li style="border-bottom: 0; padding: 5px; font-weight: normal;">
		<strong>ธนาคารกรุงเทพ</strong>
		<br />
		ท่านสามารถชำระค่าสินค้าผ่าน ATM, บัวหลวง i-banking,โดยธนาคารจะเรียกเก็บค่าธรรมเนียมในพื้นที่ กรุงเทพ-ปริมณฑล 10 บาท พื้นที่ต่างจังหวัด 20 บาท* เคาท์เตอร์ธนาคาร มีค่าธรรมเนียมเรียกเก็บในพื้นที่กรุงเทพ-ปริมณฑล 15 บาท พื้นที่ต่างจังหวัด 30 บาท*
	</li>
	<li style="border-bottom: 0; padding: 5px; font-weight: normal;">
		<strong>ธนาคารกสิกร</strong>
		<br />
		ท่านสามารถชำระค่าสินค้าผ่าน ATM, K-cyber Banking, เคาท์เตอร์ธนาคาร โดยธนาคารจะเรียกเก็บค่าธรรมเนียมในพื้นที่ กรุงเทพ-ปริมณฑล 10 บาท พื้นที่ต่างจังหวัด 20 บาท*
	</li>
	<li style="border-bottom: 0; padding: 5px; font-weight: normal;">
		<strong>ธนาคารไทยพาณิชย์</strong>
		<br />
		ท่านสามารถชำระค่าสินค้าผ่าน ATM, SCB Easy Net, เคาท์เตอร์ธนาคาร โดยธนาคารจะเรียกเก็บค่าธรรมเนียมในพื้นที่ กรุงเทพ-ปริมณฑล 15 บาท พื้นที่ต่างจังหวัด 30 บาท*
	</li>
	<li style="border-bottom: 0; padding: 5px; font-weight: normal;">
		<strong>ธนาคารทหารไทย (TMB)</strong>
		<br />
		ท่านสามารถชำระค่าสินค้าผ่าน ATM, TMB Internet banking, เคาท์เตอร์ธนาคาร โดยธนาคารจะเรียกเก็บค่าธรรมเนียม 15 บาท ทุกพื้นที่*
	</li>
	<li style="border-bottom: 0; padding: 5px; font-weight: normal;">
		<strong>ธนาคารกรุงไทย</strong>
		<br />
		ท่านสามารถชำระค่าสินค้าผ่าน ATM, KTB online, เคาท์เตอร์ธนาคาร โดยธนาคารจะเรียกเก็บค่าธรรมเนียมในพื้นที่ กรุงเทพ-ปริมณฑล 10 บาท พื้นที่ต่างจังหวัด 15 บาท*
	</li>
	<li style="border-bottom: 0; padding: 5px; font-weight: normal;">
		<strong>ธนาคารกรุงศรีอยูธยา</strong>
		<br />
		ท่านสามารถชำระค่าสินค้าผ่าน ATM, Krungsri online, เคาท์เตอร์ธนาคาร โดยธนาคารจะเรียกเก็บค่าธรรมเนียมในพื้นที่ กรุงเทพ-ปริมณฑล 15 บาท พื้นที่ต่างจังหวัด 30 บาท*
	</li>
	<li style="border-bottom: 0; padding: 5px; font-weight: normal;">
		<strong>ธนาคารยูโอบี</strong>
		<br />
		ท่านสามารถชำระค่าสินค้าผ่าน ATM, UOB Cybe banking, เคาท์เตอร์ธนาคาร โดยธนาคารจะเรียกเก็บค่าธรรมเนียม 15 บาท ทุกพื้นที่*
	</li>
</ul>
* กรุณาชำระเงินภายใน 48 ชั่วโมงหลังจากที่ท่านได้รับอีเมล์ยืนยันคำสั่งซื้อคำสั่งซื้อของท่านจะหมดอายุลงอัตโนมัติหากท่านไม่ได้ชำระเงินภายในระยะเวลาที่กำหนด
<br />
* ค่าบริการอาจมีการเปลี่ยนแปลงตามข้อกำหนดและเงื่อนไขของผู้ให้บริการการชำระเงินแต่ละราย --></div>
</li>
<li>
<div class="accord-header">ราคาสินค้าใช้สกุลเงินไทยหรือไม่</div>
<div class="accord-content">ราคาสินค้าทุกชิ้นของเราใช้หน่วยเงินบาทไทย</div>
</li>
<li class="last">
<div class="accord-header">ราคาสินค้ารวมภาษีมูลค่าเพิ่มหรือยังค่ะ</div>
<div class="accord-content">ราคาสินค้าของเราได้รวมภาษีมูลค่าเพิ่มแล้ว</div>
</li>
</ol></div>
<div id="shipping" class="subcontent">
<h2>การจัดส่งสินค้า</h2>
<ol class="accordion">
<li>
<div class="accord-header">บริษัททำการจัดส่งสินค้าเมื่อไหร่</div>
<div class="accord-content">หากสินค้าที่คุณสั่งซื้อมีพร้อมในคลังของเรา บริษัทจะทำการจัดส่งสินค้าออกจากคลังสินค้าภายใน 24 ชั่วโมงหลังจากคำสั่งซื้อสินค้าสมบูรณ์เรียบร้อยแล้ว หรือในเวลาที่ไม่ช้าหรือเร็วไปกว่านั้น (เฉพาะในวันทำการ) สินค้าของ Moxy ทุกรายการจัดส่งโดยระบบที่มีประสิทธิภาพสูง (Premium Courier) ซึ่งจะดำเนินการจัดส่งสินค้าในช่วงเวลา 9.00 น. &ndash; 19.00 น. และในปัจจุบันเราให้บริการจัดส่งสินค้าเฉพาะวันจันทร์ &ndash; วันเสาร์เท่านั้น</div>
</li>
<li>
<div class="accord-header">ระยะเวลาการจัดส่งสินค้านานเท่าไหร่</div>
<div class="accord-content">การจัดส่งสินค้าดำเนินการด้วยระบบของบริษัท ร่วมกับผู้ให้บริการจัดส่งสินค้า อื่น ๆ ซึ่งมีประสิทธิภาพสูง (Premium Courier) เพื่อให้เราแน่ใจได้ว่าสินค้าจะ จัดส่งถึงมือคุณอย่างรวดเร็วที่สุด หากคุณสั่งซื้อสินค้าภายในเวลา 16.00 น. สินค้าถึงมือคุณไม่เกิน 2-3 วันทำการเฉพาะในเขตกรุงเทพ และ 3-5 วันทำการ ในเขต ปริมณฑล และต่างจังหวัด ทั้งนี้บริษัทไม่อาจรับประกันได้ว่าหลังจากคุณสั่งซื้อ สินค้าแล้วจะได้รับสินค้าภายในวันถัดไปเลยทันที แม้ว่าการจัดส่งสินค้าไปยังหลาย พื้นที่ลูกค้าจะสามารถรับสินค้าได้ในวันรุ่งขึ้นก็ตาม และในบางพื้นที่อาจจะใช้ ระยะเวลาการจัดส่งที่นานกว่านั้นก็เป็นได้ <br /><br /> *เฉพาะ ปัตตานี ยะลา นราธิวาส แม่ฮ่องสอน เชียงราย อำเภอสังขระบุรี และบางพื้นที่ที่เป็นเกาะ อาจใช้เวลาในการจัดส่งมากกว่าปกติ 1-2 วันทำการ อย่างไรก็ตามทีมงานของเราจะจัดส่งถึงมือคุณอย่างเร็วที่สุด</div>
</li>
<li>
<div class="accord-header">วิธีการตรวจสอบและติดตามสถานะการจัดส่งสินค้า</div>
<div class="accord-content">เมื่อเราทำการจัดส่งสินค้า คุณจะได้รับอีเมลเพื่อยืนยันการจัดส่งสินค้าและแจ้งหมายเลขการจัดส่งสินค้า คุณสามารถใช้หมายเลขการจัดส่งสินค้าเพื่อตรวจสอบสถานะการจัดส่งได้ที่เว็บไซต์ของเรา (ตรวจสอบสถานะการจัดส่ง) หรือสามารถล็อกอินเพื่อดูสถานะการจัดส่งสินค้าได้ที่หน้าใบสั่งซื้อของคุณ โดยคลิกที่ปุ่ม &ldquo;สถานะสินค้า&rdquo; ในกรณีที่สถานะการจัดส่งสินค้าขึ้นว่าได้ &ldquo;ส่งมอบสินค้าแล้ว&rdquo; และคุณยังไม่ได้รับสินค้ากรุณาตรวจสอบกับเพื่อนบ้านหรือผู้ดูแลอาคารก่อนเบื้องต้น และหากสถานะการจัดส่งสินค้าขึ้นว่า &ldquo;ส่งคืนบริษัท&rdquo; กรุณาติดต่อเราทางอีเมลหรือโทรศัพท์ทันที เพราะอาจจะเกิดกรณีที่อยู่จัดส่งสินค้าไม่ถูกต้อง, เกิดเหตุสุดวิสัยที่ทำให้พนักงานไม่สามารถจัดส่งสินค้าได้ หรือสินค้าสูญหายระหว่างการจัดส่ง</div>
</li>
<li>
<div class="accord-header">กรณีที่พนักงานไปจัดส่งสินค้าแล้วไม่มีผู้รอรับสินค้าที่บ้าน</div>
<div class="accord-content">การจัดส่งสินค้าเมื่อไม่มีผู้รอรับสินค้าอยู่ที่บ้าน ขึ้นอยู่กับสถานการณ์และดุลยพินิจของพนักงานจัดส่งสินค้าว่าสามารถจะทำการวางสินค้าไว้ที่หน้าบ้านได้หรือไม่ ในกรณีที่พนักงานจัดส่งสินค้าเห็นว่าจำเป็นจะต้องมีผู้ลงชื่อรับสินค้า พนักงานจัดส่งสินค้าจะฝากสินค้าไว้ที่ผู้จัดการอาคารหรือพนักงานเฝ้าประตู โดยจะพิจารณาเลือกวิธีการที่ดีที่สุดอย่างใดอย่างหนึ่ง แต่หากไม่สามารถทำการจัดส่งสินค้าได้ สินค้าจะถูกตีคืนมาที่บริษัทซึ่งจะขึ้นสถานะการจัดส่งสินค้าว่า &ldquo;ไม่สามารถจัดส่ง&rdquo; คุณสามารถทำเรื่องขอคืนเงินค่าสินค้าเต็มจำนวน แต่ไม่รวมค่าจัดส่งสินค้าได้ (อ่านรายละเอียดได้ที่ <a href="{{store url='return-policy'}}" target="_blank">นโยบายการคืนสินค้า</a> ) ในกรณีที่คำสั่งซื้อถูกปฏิเสธอย่างไม่ถูกต้อง ทางบริษัทจะไม่ทำการจัดส่งสินค้ากลับไปอีกครั้ง คุณจะต้องดำเนินการสั่งซื้อใหม่เพื่อที่จะได้รับสินค้าที่คุณต้องการ</div>
</li>
<li>
<div class="accord-header">สามารถแจ้งส่งสินค้าไปยังที่สถานที่อื่นๆ ของคุณได้หรือไม่</div>
<div class="accord-content">แน่นอน เราทำการจัดส่งสินค้าในวันจันทร์- ศุกร์ เวลา 09.00 น. &ndash; 19.00 น. และวันเสาร์ เวลา 09.00 น. &ndash; 12.00 น. หากต้องการให้เราจัดส่งสินค้าไปยังที่ทำงานของคุณ สามารถกรอกที่อยู่ที่ต้องการให้เราจัดส่งในช่องสถานที่จัดส่งสินค้า หากเกิดเหตุสุดวิสัยที่คุณไม่สามารถอยู่รอรับสินค้าได้ด้วยตนเอง กรุณาตรวจสอบสินค้ากับทางพนักงานต้อนรับ, ผู้จัดการอาคาร, พนักงานเฝ้าประตูก่อนเบื้องต้น และเพื่อป้องกันความผิดพลาดในการนัดหมายรับสินค้า กรุณารอรับการติดต่อทางโทรศัพท์จากพนักงานของเราด้วย</div>
</li>
<li>
<div class="accord-header">ค่าบริการจัดส่งสินค้า</div>
<div class="accord-content">จัดส่งฟรีเมื่อซื้อสินค้าขั้นต่ำ 500 บาทต่อการสั่งซื้อ1ครั้ง เฉพาะเขตกรุงเทพ ในส่วนของเขตปริมณฑล และต่างจังหวัด คิดค่าบริการในการจัดส่ง 100 บาท ในกรณีซื้อสินค้าต่ำกว่า 500 บาทเสียค่าจัดส่ง 100 บาทในเขตกรุงเทพ และ 200 บาทในเขตปริมณฑลและต่างจังหวัด</div>
</li>
<li>
<div class="accord-header">การจัดส่งสินค้าไปยังต่างประเทศ</div>
<div class="accord-content">ขออภัย...ณ ตอนนี้บริษัทยังไม่มีบริการจัดส่งสินค้าไปยังต่างประเทศ แต่ในอนาคตเรามีแพลนที่จะเปิดร้านค้าออนไลน์ที่ขายอาหารสัตว์ทั่วภาคพื้นเอเชียตะวันออกเฉียงใต้</div>
</li>
<li class="last">
<div class="accord-header">ทำไมการจัดส่งสินค้าถึงล่าช้า</div>
<div class="accord-content">หากทำการสั่งซื้อสินค้าภายในเวลา 16.00 น. ของวันทำการปกติ คุณจะได้รับสินค้าหลังจากคำสั่งซื้อสมบูรณ์แล้วภายใน 2-3 วันทำการ ซึ่งเราจะพยายามจัดส่งสินค้าอย่างเร็วที่สุดเท่าที่จะเป็นไปได้ แต่ก็อาจมีบางกรณีที่จะทำให้คุณได้รับสินค้าล่าช้ากว่ากำหนด<br />สาเหตุที่อาจจะทำให้การจัดส่งสินค้าล่าช้า :
<ul style="margin-left: 30px; list-style: square !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">ข้อมูลสถานที่จัดส่งสินค้าไม่ถูกต้อง</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">ข้อมูลเบอร์โทรศัพท์ของลูกค้าไม่ถูกต้อง</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">ไม่สามารถติดต่อลูกค้าตามเบอร์โทรศัพท์ที่ให้ไว้ได้</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">ไม่มีคนอยู่รับสินค้าตามสถานที่ได้นัดหมายเอาไว้</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">แจ้งสถานที่จัดส่งสินค้าไม่เหมือนกับที่อยู่ตามใบสั่งซื้อ</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">ลูกค้าชำระเงินล่าช้าหรือมีข้อผิดพลาดในขั้นตอนการชำระเงิน</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">คำสั่งซื้อที่จำเป็นต้องหยุดชะงักหรือล่าช้า อันเนื่องมาจากการสั่งซื้อสินค้าในรายการใดรายการหนึ่งเป็นจำนวนมากทำให้สินค้าในคลังของเราไม่เพียงพอต่อการจำหน่าย (เป็นกรณีที่เกิดขึ้นไม่บ่อยนัก)</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">เหตุสุดวิสัยที่ทำให้การจัดส่งสินค้าไม่เป็นไปตามกำหนด (เช่น ภัยธรรมชาติหรือสภาพอากาศที่ผิดปกติ เป็นต้น)</li>
</ul>
</div>
</li>
</ol></div>
<div id="rewardpoints" class="subcontent">
<h2>คะแนนสะสม</h2>
<ol class="accordion">
<li>
<div class="accord-header">รับเงินคืน 1% เมื่อช็อปปิ้งกับ Moxy</div>
<div class="accord-content">เมื่อท่านได้สั่งซื้อของกับทาง Moxy ท่านจะได้รับเงินคืนทันที 1 % โดยเงินที่ได้จะถูกเก็บไว้ที่บัญชี Moxy ของท่าน ท่านสามารถนำมาใช้ได้ในครั้งต่อไปเมื่อท่านสั่งซื้อของผ่านทาง Moxy โดยสามารถใช้แทนเงินสด ทุกครั้งที่ท่านต้องการใช้ ท่านสามารถกรอกจำนวนเงินในช่อง ที่หน้าสั่งซื้อสินค้า ก่อนขั้นตอนสุดท้าย</div>
</li>
<li>
<div class="accord-header">คุณจะได้รับแต้มจากการแนะนำเว็บไซต์ให้กับเพื่อนๆ ได้อย่างไร</div>
<div class="accord-content">มีหลากหลายช่องทางที่คุณจะได้รับเครดิต 100 บาท จากการแนะนำเว็บไซต์ Moxyst.com ให้กับเพื่อนๆ <a href="{{store url='rewardpoints/index/referral'}}" target="_blank">อ่านรายละเอียดเพิ่มเติมที่นี่</a>
<ul style="margin-left: 30px; list-style: square !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">คุณสามารถนำลิงค์จากในหน้าบัญชีผู้ใช้ของคุณในเว็บไซต์ Moxyst.com แชร์ผ่านเฟสบุ๊ค ทวีทเตอร์ หรือส่งลิงค์ให้กับเพื่อนๆ ผ่านทางอีเมล</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">เมื่อเพื่อนของคุณคลิกลิงค์ดังกล่าวและซื้อสินค้าบนเว็บไซต์ Moxyst.com คุณจะได้รับทันทีเครดิต 100 บาทในบัญชีของคุณ เครดิตดังกล่าวสามารถใช้แทนเงินสดหรือใช้เป็นส่วนลดในการซื้อสินค้าครั้งต่อๆ ไป</li>
</ul>
</div>
</li>
<li class="last">
<div class="accord-header">อายุการใช้งานของคะแนนสะสม (Reward Points)</div>
<div class="accord-content">เมื่อคุณซื้อสินค้ากับทางเว็บไซต์ของเราคุณจะได้รับคะแนนสะสม (Reward Points) โดยคะแนนสะสมที่คุณได้รับจากการซื้อสินค้าแต่ละครั้งนั้นจะนำมารวมกันเมื่อคุณซื้อสินค้าในครั้งต่อไปเพิ่มขึ้นเรื่อยๆ แต่ถ้าคุณเก็บคะแนนสะสมไว้เป็นเวลานาน คะแนนบางส่วนของคุณอาจหายไปได้ เนื่องจากอายุการใช้งานของคะแนนสะสมจะมีระยะเวลาไม่เท่ากัน กล่าวคือ คะแนนที่คุณได้รับหลังจากการซื้อสินค้าแต่ละครั้งจะมีอายุการใช้งานนาน 6 เดือน <br /><br /> เช่น คุณซื้อสินค้าและได้คะแนนสะสมครั้งแรกในเดือนมกราคม จำนวน 20 คะแนน ต่อมาคุณซื้อสินค้าและได้คะแนนสะสมอีกครั้งในเดือนมีนาคม จำนวน 30 คะแนน คุณจะมีคะแนนสะสมรวมเป็น 50 คะแนน แต่เมื่อถึงเดือนกรกฎาคม คะแนนของคุณจะเหลือเพียง 30 คะแนน (เนื่องจาก 20 คะแนนที่ได้จากเดือนมกราคมหมดอายุในเดือนกรกฎาคม) และถ้าคุณยังไม่ได้ใช้คะแนนที่เหลืออีกภายในเดือนกันยายนคะแนนสะสมของคุณจะกลายเป็น 0 ทันที (เนื่องจาก 30 คะแนนที่ได้จากเดือนมีนาคมหมดอายุในเดือนกันยายน) <br /><br /> ดังนั้นเพื่อเป็นการรักษาสิทธิและผลประโยชน์ของคุณ จึงควรนำคะแนนสะสมมาใช้ภายในระยะเวลาที่กำหนด</div>
</li>
</ol></div>
<div id="returns" class="subcontent">
		<h2>การคืนสินค้าและขอคืนเงิน</h2>
		<ol class="accordion">
			<li>
			<div class="accord-header"><span></span>สามารถขอคืนสินค้าได้อย่างไร</div>
			<div class="accord-content">
				<strong>บริการคืนสินค้าอย่างง่ายดาย รวดเร็ว และไม่มีค่าใช้จ่าย!</strong><br/>
				กรณีที่คุณต้องการคืนสินค้า กรุณาติดต่อแผนกลูกค้าสัมพันธ์และแจ้งความประสงค์ในการขอคืนสินค้าผ่านทางโทรศัพท์ 02-106-8222 หรือ อีเมล์ <a href="mailto:support@moxyst.com">support@moxyst.com</a> หากเงื่อนไขการขอคืนสินค้าของคุณเป็นไปตามนโยบายการขอคืนสินค้าแล้ว กรุณากรอกรายละเอียดลงในแบบฟอร์มการขอคืนสินค้าที่คุณได้รับไปพร้อมกับสินค้าเมื่อตอนจัดส่ง และแนบใบกำกับภาษี (invoice) ใส่ลงในกล่องพัสดุของ Moxyst.com กลับมาพร้อมกันด้วย โดยจัดส่งสินค้ากลับมาให้ทางเรา ในแบบพัสดุลงทะเบียนผ่านทางที่ทำการไปรษณีย์ไทยใกล้บ้านคุณ
				 <br/><br/>
				กรุณาจัดส่งสินค้ามาที่ :<br/>
				บริษัท เอคอมเมิร์ซ จำกัด<br/>
				8/2 ถนนพระราม3 ซอย53<br/> แขวงบางโพงพาง เขตยานนาวา <br/>กรุงเทพฯ 10120

				 <br/><br/>

				ทาง Moxyst.com จะชดเชยค่าจัดส่งแบบลงทะเบียนพร้อมกับการคืนเงินค่าสินค้า โดยทางเราขอให้คุณเก็บใบเสร็จรับเงิน/slip ค่าจัดส่งที่ได้รับจากที่ทำการไปรษณีย์เพื่อเป็นหลักฐานในการขอคืนเงินค่าส่งสินค้ากลับไว้ด้วย กรุณาส่งใบเสร็จรับเงิน/slip นี้กลับมาทางแผนกลูกค้าสัมพันธ์ โดยส่งเป็นไฟล์รูปภาพกลับมาทางอีเมล์ <a href="mailto:support@moxyst.com">support@moxyst.com</a>
<br/><br/>
หมายเหตุ : ทาง Moxyst.com ต้องขออภัยในความไม่สะดวกที่เรายังไม่มีบริการในการเข้าไปรับสินค้าที่ลูกค้าต้องการส่งคืน

			</div>
			</li>
			<li>
			<div class="accord-header"><span></span>การแพ็คสินค้าเพื่อส่งคืนบริษัท</div>
			<div class="accord-content">
				เราแนะนำให้คุณทำการแพ็คสินค้าคืนด้วยกล่องหรือบรรจุภัณฑ์ลักษณะเดียวกับที่เราทำการจัดส่งไปให้ เว้นแต่ว่ามันไม่ได้อยู่ในสภาพที่ดีพอสำหรับการนำมาใช้ใหม่ อย่าลืม! ที่จะแนบป้าย/ฉลากขอคืนสินค้า (return label) กลับมาด้วย จากนั้นใช้ป้ายชื่อ/ที่อยู่ในการจัดส่งสินค้า (Shipping label)
				ที่ได้รับจากพนักงานหรือจากเว็บไซต์ของเราเพื่อทำการจัดส่งสินค้าคืนบริษัทได้ที่ทำการไปรษณีย์ทุกแห่งทั่วประเทศไทย
				<br/><br/>
หมายเหตุ : การจัดส่งสินค้ากลับคืนนั้น Moxy ไม่ได้เป็นผู้กำหนดระยะเวลาในการจัดส่งสินค้า จึงไม่สามารถบอกได้ว่าสินค้าที่ส่งคืนนั้นจะมาถึงบริษัทเมื่อไหร่
<br/><br/>
หากมีข้อสงสัยกรุณาติดต่อสอบถามเราได้ที่ 02-106-8222 หรือ อีเมล์ <a href="mailto:support@moxyst.com">support@moxyst.com</a>
			</div>
			</li>
			<li>
			<div class="accord-header"><span></span>ใช้ระยะเวลาดำเนินการคืนสินค้านานเท่าไหร่</div>
			<div class="accord-content">
				เราจะใช้เวลาดำเนินการภายใน 14 วันทำการ นับตั้งแต่วันที่ได้รับสินค้าคืนมาเรียบร้อยแล้ว
			</div>
			</li>
			<li>
			<div class="accord-header"><span></span>เมื่อไหร่ถึงจะได้รับเงินค่าสินค้าคืน</div>
			<div class="accord-content">
				เมื่อบริษัทได้รับสินค้าที่ส่งคืนเรียบร้อยแล้ว ขั้นตอนการคืนเงินจะเกิดขึ้นโดยทันที เราจะดำเนินการคืนเงินค่าสินค้ากลับไปยังบัญชีเงินฝากของคุณ กรุณารอและตรวจสอบยอดเงินหลังจากที่เราได้รับสินค้าคืนจากคุณภายใน  7 วันทำการ
			</div>
			</li>
			<li>
			<div class="accord-header"><span></span>ค่าใช้จ่ายในการจัดส่งสินค้าคืนบริษัท</div>
			<div class="accord-content">
				ค่าใช้จ่ายในการจัดส่งสินค้าจะแตกต่างกันไปตามสถานที่และวิธีการจัดส่งที่คุณเลือก กรุณาตรวจสอบให้แน่ใจว่าคุณได้ใช้ระบบจัดส่งแบบลงทะเบียนซึ่งดำเนินการโดยไปรษณีย์ไทย และเราจะคืนเงินค่าจัดส่งหลังจากที่ได้รับสินค้าคืนและยืนยันว่าได้รับสินค้าคืนเรียบร้อยแล้ว
			</div>
			</li>
			<li class="last">
			<div class="accord-header"><span></span>กรณีที่การจัดส่งสินค้าเกิดข้อผิดพลาด</div>
			<div class="accord-content">
				แผนกควบคุมคุณภาพของเราทำงานอย่างหนักเพื่อให้สินค้าทุกชิ้นอยู่ในสภาพที่ดีและมีคุณภาพสูงสุดเมื่อทำการจัดส่งออกจากคลังสินค้า แต่ก็มีในบางกรณีซึ่งเป็นไปได้ยากที่สินค้าของคุณจะเกิดตำหนิ,เสียหาย หรือมีข้อผิดพลาด ดังนั้นหากเกิดกรณีดังกล่าวแล้วคุณสามารถดำเนินการขอคืนสินค้าและเงินค่าสินค้าเต็มจำนวน ภายในระยะเวลา 30 วันหลังจากที่คุณได้รับสินค้าเรียบร้อยแล้ว
			</div>
			</li>
		</ol>
	</div>
	</div>
<script type="text/javascript">// <![CDATA[
 jQuery(document).ready(function()
 {
  var tmpUrl = "";
  var h=window.location.hash.substring(0);
  if(h == ""){
   //alert('Hash not found!!!');
   h = "#faq";
   //alert(heart);
   jQuery( "#helpMenu li:first-child" ).addClass( "helpactive");
   jQuery('.subcontent[id='+h.substring(1)+']').fadeIn();
  }else{
   jQuery('#helpMenu li a[data-id='+h.substring(1)+']').parent().addClass( "helpactive");
  jQuery('.subcontent[id='+h.substring(1)+']').fadeIn();
  }

  //////////////////////////////
  jQuery('#keyMess li').on('click','a',function()  {
   tmpUrl=jQuery(this).attr('href');
   h=tmpUrl.substring(tmpUrl.indexOf("#")+1);

   jQuery('.subcontent:visible').fadeOut(0);
   jQuery('.subcontent[id='+h+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
  });
  jQuery('#zyncNav li').on('click','a',function()
  {
   tmpUrl=jQuery(this).attr('href');
   h=tmpUrl.substring(tmpUrl.indexOf("#")+1);
   jQuery('.subcontent:visible').fadeOut(0);
   jQuery('.subcontent[id='+h+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
  });
  jQuery('#footerBanner div').on('click','a',function()
  {
   tmpUrl=jQuery(this).attr('href');
   h=tmpUrl.substring(tmpUrl.indexOf("#")+1);
   jQuery('.subcontent:visible').fadeOut(0);
   jQuery('.subcontent[id='+h+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
  });
  jQuery('#topMklink').on('click','a',function()
  {
   tmpUrl=jQuery(this).attr('href');
   h=tmpUrl.substring(tmpUrl.indexOf("#")+1);
   jQuery('.subcontent:visible').fadeOut(0);
   jQuery('.subcontent[id='+h+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
  });
  jQuery('#footer_help li').on('click','a',function()
  {
   tmpUrl=jQuery(this).attr('href');
   h=tmpUrl.substring(tmpUrl.indexOf("#")+1);
   jQuery('.subcontent:visible').fadeOut(0);
   jQuery('.subcontent[id='+h+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
  });
 ///////////////////////////////

     jQuery('#helpMenu').on('click','a',function()
     {
         jQuery('.subcontent:visible').fadeOut(0);
         jQuery('.subcontent[id='+jQuery(this).attr('data-id')+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery(this).parent().addClass( "helpactive");
     });

///////////////////////////////

     jQuery(".accordion li .accord-header").mouseenter(function() {
	    	jQuery(this).css("cursor","pointer");
	    });
	    jQuery(".accordion li .accord-header").click(function() {
	    	jQuery(this).find("span").removeClass('active');
	      if(jQuery(this).next("div").is(":visible")){
	        jQuery(this).next("div").slideUp(300);
	      } else {
	        jQuery(".accordion li .accord-content").slideUp(400);
	        jQuery(this).next("div").slideToggle(400);
	        jQuery(".accordion li .accord-header").find("span").removeClass('active');
	        jQuery(this).find("span").addClass('active');
	      }
	    });
	  });
// ]]></script>
EOD;

    $pageRootTemplate = 'one_column';
    $pageLayoutUpdateXml = <<<EOD
EOD;


    $pageCustomLayoutUpdateXML = <<<EOD
EOD;

    $pageMetaKeywords = "";
    $pageMetaDescription = "คำถามที่พบบ่อย เกี่ยวกับวิธีการการสั่งซื้อ การชำระเงิน และการจัดส่งสินค้า ตลอดจนนโยบายของ Moxyst.com ในการให้บริการลูกค้า";

    $page = Mage::getModel('cms/page')->getCollection()
        ->addStoreFilter(array($storeIdMoxyTh), $withAdmin = true)
        ->addFieldToFilter('identifier', $pageIdentifier)
        ->getFirstItem();
    if ($page->getId() == 0) {
        $page = Mage::getModel('cms/page');
    }
    else
    {
        // if exists then repair
        $page = Mage::getModel('cms/page')->load($page->getId());
    }

    $page->setTitle($pageTitle);
    $page->setIdentifier($pageIdentifier);
    $page->setStores($pageStores);
    $page->setIsActive($pageIsActive);
    $page->setUnderVersionControl($pageUnderVersionControl);
    $page->setContentHeading($pageContentHeading);
    $page->setContent($pageContent);
    $page->setRootTemplate($pageRootTemplate);
    $page->setLayoutUpdateXml($pageLayoutUpdateXml);
    $page->setCustomLayoutUpdateXml($pageCustomLayoutUpdateXML);
    $page->setMetaKeywords($pageMetaKeywords);
    $page->setMetaDescription($pageMetaDescription);
    $page->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================



    $installer->endSetup();
} catch (Excpetion $e) {
    Mage::logException($e);
    Mage::log("ERROR IN SETUP " . $e->getMessage());
}
