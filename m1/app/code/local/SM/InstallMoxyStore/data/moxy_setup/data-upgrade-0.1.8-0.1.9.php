<?php

// Base-name
$name           = 'moxy';
$engStoreName   = 'Moxy English';
$engStoreCode   = 'moxyen';
$thStoreName    = 'Moxy Thai';
$thStoreCode    = 'moxyth';
$storeIdMoxyEn  = Mage::getModel('core/store')->load($engStoreCode, 'code')->getId();
$storeIdMoxyTh  = Mage::getModel('core/store')->load($thStoreCode, 'code')->getId();
$newProductsCateId = 0;
$bestSellersCateId = 0;
try {
    $installer = $this;
    $installer->startSetup();

    // Force the store to be admin
    Mage::app()->setUpdateMode(false);
    Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
    if (!Mage::registry('isSecureArea'))
        Mage::register('isSecureArea', 1);

    //==========================================================================
    // guarantee_moxy EN block
    //==========================================================================
    $blockTitle = "guarantee_moxy EN";
    $blockIdentifier = "guarantee_moxy";
    $blockStores = array($storeIdMoxyEn);
    $blockIsActive = 1;
    $blockContent = <<<EOD
<div class="guarantee_moxy"><span class="guarantee_icon">Free Shipping</span> <span class="guarantee_icon">Pay Cash on Delivery</span> <span class="guarantee_icon">30 Day Free Returns</span> <span class="guarantee_icon">100% Secured Checkout</span> <span class="free-shipping-icon">Free Delivery</span><span class="shipping-list"><img src="{{media url="wysiwyg/Moxy/shipping_list.png"}}" alt="" /></span></div>
EOD;
    $block = Mage::getModel('cms/block')->getCollection()
        ->addStoreFilter(array($storeIdMoxyEn), $withAdmin = false)
        ->addFieldToFilter('identifier', $blockIdentifier)
        ->getFirstItem();
    if ($block->getId() == 0) {
        $block = Mage::getModel('cms/block');
    } else { // if exists then delete
        $block->delete();
        $block = Mage::getModel('cms/block');
    }
    $block->setTitle($blockTitle);
    $block->setIdentifier($blockIdentifier);
    $block->setStores($blockStores);
    $block->setIsActive($blockIsActive);
    $block->setContent($blockContent);
    $block->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================

    //==========================================================================
    // guarantee_moxy th block
    //==========================================================================
    $blockTitle = "guarantee_moxy EN";
    $blockIdentifier = "guarantee_moxy";
    $blockStores = array($storeIdMoxyTh);
    $blockIsActive = 1;
    $blockContent = <<<EOD
<div class="guarantee_moxy"><span class="guarantee_icon">ฟรีค่าจัดส่ง</span> <span class="guarantee_icon">ชำระเงินเมื่อได้รับสินค้า</span> <span class="guarantee_icon">รับคืนฟรี ภายใน 30 วัน</span> <span class="guarantee_icon">ระบบชำระเงินปลอดภัย 100%</span> <span class="free-shipping-icon">จัดส่งฟรี</span><span class="shipping-list"><img src="{{media url="wysiwyg/Moxy/shipping_list.png"}}" alt="" /></span></div>
EOD;
    $block = Mage::getModel('cms/block')->getCollection()
        ->addStoreFilter(array($storeIdMoxyTh), $withAdmin = false)
        ->addFieldToFilter('identifier', $blockIdentifier)
        ->getFirstItem();
    if ($block->getId() == 0) {
        $block = Mage::getModel('cms/block');
    } else { // if exists then delete
        $block->delete();
        $block = Mage::getModel('cms/block');
    }
    $block->setTitle($blockTitle);
    $block->setIdentifier($blockIdentifier);
    $block->setStores($blockStores);
    $block->setIsActive($blockIsActive);
    $block->setContent($blockContent);
    $block->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================


    $installer->endSetup();
} catch (Excpetion $e) {
    Mage::logException($e);
    Mage::log("ERROR IN SETUP " . $e->getMessage());
}
