<?php

// Base-name
$name           = 'moxy';
$engStoreName   = 'Moxy English';
$engStoreCode   = 'moxyen';
$thStoreName    = 'Moxy Thai';
$thStoreCode    = 'moxyth';
$storeIdMoxyEn  = Mage::getModel('core/store')->load($engStoreCode, 'code')->getId();
$storeIdMoxyTh  = Mage::getModel('core/store')->load($thStoreCode, 'code')->getId();
$newProductsCateId = 0;
$bestSellersCateId = 0;
try {
    $installer = $this;
    $installer->startSetup();

    // Force the store to be admin
    Mage::app()->setUpdateMode(false);
    Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
    if (!Mage::registry('isSecureArea'))
        Mage::register('isSecureArea', 1);

    //==========================================================================
    // easy-reorder-landing EN Page
    //==========================================================================
    $pageTitle = "Easy Reorder Landing";
    $pageIdentifier = "easy-reorder-landing";
    $pageStores = array('13','1','10','5',"$storeIdMoxyEn");
    $pageIsActive = 1;
    $pageUnderVersionControl = 0;
    $pageContentHeading = "";
    $pageContent = <<<EOD
<p><a href="/customer/account/login/"><img src="{{media url="wysiwyg/FastReorder_Page_Accounts_950.jpg"}}" alt="" /></a></p>
EOD;

    $pageRootTemplate = 'one_column';
    $pageLayoutUpdateXml = <<<EOD
EOD;


    $pageCustomLayoutUpdateXML = <<<EOD
EOD;

    $pageMetaKeywords = "";
    $pageMetaDescription = "";

    $page = Mage::getModel('cms/page')->getCollection()
        ->addStoreFilter(array('13','1','10','5',"$storeIdMoxyEn"), $withAdmin = true)
        ->addFieldToFilter('identifier', $pageIdentifier)
        ->getFirstItem();
    if ($page->getId() == 0) {
        $page = Mage::getModel('cms/page');
    }
    else
    {
        // if exists then repair
        $page = Mage::getModel('cms/page')->load($page->getId());
    }

    $page->setTitle($pageTitle);
    $page->setIdentifier($pageIdentifier);
    $page->setStores($pageStores);
    $page->setIsActive($pageIsActive);
    $page->setUnderVersionControl($pageUnderVersionControl);
    $page->setContentHeading($pageContentHeading);
    $page->setContent($pageContent);
    $page->setRootTemplate($pageRootTemplate);
    $page->setLayoutUpdateXml($pageLayoutUpdateXml);
    $page->setCustomLayoutUpdateXml($pageCustomLayoutUpdateXML);
    $page->setMetaKeywords($pageMetaKeywords);
    $page->setMetaDescription($pageMetaDescription);
    $page->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================

    //==========================================================================
    // easy-reorder-landing TH Page
    //==========================================================================
    $pageTitle = "Easy Reorder Landing";
    $pageIdentifier = "easy-reorder-landing";
    $pageStores = array('15','2','11','4',"$storeIdMoxyTh");
    $pageIsActive = 1;
    $pageUnderVersionControl = 0;
    $pageContentHeading = "";
    $pageContent = <<<EOD
<p><a href="/customer/account/login/"><img src="{{media url="wysiwyg/FastReorder_Page_Accounts_TH_950.jpg"}}" alt="" /></a></p>
EOD;

    $pageRootTemplate = 'one_column';
    $pageLayoutUpdateXml = <<<EOD
EOD;


    $pageCustomLayoutUpdateXML = <<<EOD
EOD;

    $pageMetaKeywords = "";
    $pageMetaDescription = "";

    $page = Mage::getModel('cms/page')->getCollection()
        ->addStoreFilter(array('15','2','11','4',"$storeIdMoxyTh"), $withAdmin = true)
        ->addFieldToFilter('identifier', $pageIdentifier)
        ->getFirstItem();
    if ($page->getId() == 0) {
        $page = Mage::getModel('cms/page');
    }
    else
    {
        // if exists then repair
        $page = Mage::getModel('cms/page')->load($page->getId());
    }

    $page->setTitle($pageTitle);
    $page->setIdentifier($pageIdentifier);
    $page->setStores($pageStores);
    $page->setIsActive($pageIsActive);
    $page->setUnderVersionControl($pageUnderVersionControl);
    $page->setContentHeading($pageContentHeading);
    $page->setContent($pageContent);
    $page->setRootTemplate($pageRootTemplate);
    $page->setLayoutUpdateXml($pageLayoutUpdateXml);
    $page->setCustomLayoutUpdateXml($pageCustomLayoutUpdateXML);
    $page->setMetaKeywords($pageMetaKeywords);
    $page->setMetaDescription($pageMetaDescription);
    $page->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================

    //==========================================================================
    // home moxy EN Page
    //==========================================================================
    $pageTitle = "MOXY - Online Lifestyle Shopping";
    $pageIdentifier = "home";
    $pageStores = array($storeIdMoxyEn);
    $pageIsActive = 1;
    $pageUnderVersionControl = 0;
    $pageContentHeading = "";
    $pageContent = <<<EOD
<p>{{block type="core/template" name="subscription" as="subscription" template="subscription/subscribe.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="$newProductsCateId" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="$bestSellersCateId" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="cms/block" block_id="all-brands-slider"}}</p>
<h2>&nbsp;</h2>
<div style="text-align: center;">
<a title="Shop at Sanoga" onclick="_gaq.push(['_trackEvent', 'Sanoga Banner', 'Click', 'Sanoga Home Banner - Bottom',1,true])" href="http://www.sanoga.com/">
<img src="{{media url="wysiwyg/BottomBannersWeb/Bottombanners_sanoga.jpg"}}" alt="Shop at Sanoga" />
</a>
<a title="Shop at PetLoft" onclick="_gaq.push(['_trackEvent', 'PetLoft Banner', 'Click', 'PetLoft Home Banner - Bottom',1,true])" href="http://www.petloft.com/">
<img src="{{media url="wysiwyg/BottomBannersWeb/Bottombanners_petloft.jpg"}}" style="padding: 0 5px;" alt="Shop pet products at PetLoft" />
</a>
<a title="Shop at Lafema" onclick="_gaq.push(['_trackEvent', 'Lafema Banner', 'Click', 'Lafema Home Banner - Bottom',1,true])" href="http://www.lafema.com/">
<img src="{{media url="wysiwyg/BottomBannersWeb/Bottombanners_lafema.jpg"}}" style="padding: 0 5px;"  alt="Shop cosmetics at Lafema" />
</a>
<a title="Shop at Venbi" onclick="_gaq.push(['_trackEvent', 'Venbi Banner', 'Click', 'Venbi Home Banner - Bottom',1,true])" href="http://www.venbi.com/">
<img src="{{media url="wysiwyg/BottomBannersWeb/Bottombanners_venbi.jpg"}}" alt="Shop baby and mom products at Venbi" />
</a>
</div>
<h2>&nbsp;</h2>
<h2>กล้า มีฝัน มุ่งมั่น</h2>
<p class="end_content_info">แล้วคุณมองเห็น ความเป็น Moxy หรือยังถ้าคุณคิดแบบ Moxy ก็แปลว่า คุณมีความคิดและความมุ่งมั่นว่าคุณจะต้องเป็นคนสำคัญ</p>
<p class="end_content_info">Moxy เป็นแบรนด์ที่ชัดเจนแต่เรียบง่าย และ บ่งบอกถึงแรงพลังที่จะสร้างสไตล์ให้กับตัวเอง นโยบายหมายเลขหนึ่งของ Moxy คือ การอยู่ร่วมโลกทุกวันนี้ เป็นโลกของการอยู่ร่วม</p>
<p class="end_content_info">ไม่ว่าจะกับเพื่อน ครอบครัว คนรู้จัก เมืองที่คุณอยู่ และจนถึงวัฒนธรรมร่วม ทิศทางที่จะเป็นประโยชน์สูงสุดแก่การอยู่ร่วม ไม่ใช่จะเป็นผลดีต่อแบรนด์เท่านั้น</p>
<p class="end_content_info">แต่มันจะกระทบทุกคนไปในทางบวก Moxy ไม่ได้มุ่งแต่กำไร แต่ยังตั้งใจที่จะให้ บริการที่ดีจริง และ ต้องการจะร่วมงานกับนักออกแบบและผู้ผลิตที่เป็นคนไทย บวกกับสินค้าที่ดีต่อสังคม เพราะ Moxy รู้ถึงประโยชน์ของการตอบแทน</p>
EOD;

    $pageRootTemplate = 'one_column';
    $pageLayoutUpdateXml = <<<EOD
    <reference name="header">
    <block type="cms/block" name="slider_1">
        <!--
        The content of this block is taken from the database by its block_id.
        You can manage it in admin CMS -> Static Blocks
        -->
        <action method="setBlockId"><block_id>slider_1</block_id></action>
    </block>
    <block type="cms/block" name="banners_home">
        <!--
        The content of this block is taken from the database by its block_id.
        You can manage it in admin CMS -> Static Blocks
        -->
        <action method="setBlockId"><block_id>banners_home</block_id></action>
    </block>
</reference>
<reference name="head">
    <block type="core/text" name="homepage.metadata" after="-">
        <action method="addText"><text><![CDATA[<meta name="google-site-verification" content="_4n5alA2_c2rhcm4BQTl-KqLMwVGw_gyHr20g1fCalA" />]]></text></action>
    </block>
</reference>
EOD;


    $pageCustomLayoutUpdateXML = <<<EOD
EOD;

    $pageMetaKeywords = "";
    $pageMetaDescription = "";

    $page = Mage::getModel('cms/page')->getCollection()
        ->addStoreFilter(array($storeIdMoxyEn), $withAdmin = true)
        ->addFieldToFilter('identifier', $pageIdentifier)
        ->getFirstItem();
    if ($page->getId() == 0) {
        $page = Mage::getModel('cms/page');
    }
    else
    {
        // if exists then repair
        $page = Mage::getModel('cms/page')->load($page->getId());
    }

    $page->setTitle($pageTitle);
    $page->setIdentifier($pageIdentifier);
    $page->setStores($pageStores);
    $page->setIsActive($pageIsActive);
    $page->setUnderVersionControl($pageUnderVersionControl);
    $page->setContentHeading($pageContentHeading);
    $page->setContent($pageContent);
    $page->setRootTemplate($pageRootTemplate);
    $page->setLayoutUpdateXml($pageLayoutUpdateXml);
    $page->setCustomLayoutUpdateXml($pageCustomLayoutUpdateXML);
    $page->setMetaKeywords($pageMetaKeywords);
    $page->setMetaDescription($pageMetaDescription);
    $page->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================

    //==========================================================================
    // home moxy TH Page
    //==========================================================================
    $pageTitle = "MOXY - Online Lifestyle Shopping";
    $pageIdentifier = "home";
    $pageStores = array($storeIdMoxyTh);
    $pageIsActive = 1;
    $pageUnderVersionControl = 0;
    $pageContentHeading = "";
    $pageContent = <<<EOD
<p>{{block type="core/template" name="subscription" as="subscription" template="subscription/subscribe.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="$newProductsCateId" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="$bestSellersCateId" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="cms/block" block_id="all-brands-slider"}}</p>
<h2>&nbsp;</h2>
<div style="text-align: center;">
<a title="Shop at Sanoga" onclick="_gaq.push(['_trackEvent', 'Sanoga Banner', 'Click', 'Sanoga Home Banner - Bottom',1,true])" href="http://www.sanoga.com/">
<img src="{{media url="wysiwyg/BottomBannersWeb/Bottombanners_sanoga.jpg"}}" alt="Shop at Sanoga" />
</a>
<a title="Shop at PetLoft" onclick="_gaq.push(['_trackEvent', 'PetLoft Banner', 'Click', 'PetLoft Home Banner - Bottom',1,true])" href="http://www.petloft.com/">
<img src="{{media url="wysiwyg/BottomBannersWeb/Bottombanners_petloft.jpg"}}" style="padding: 0 5px;" alt="Shop pet products at PetLoft" />
</a>
<a title="Shop at Lafema" onclick="_gaq.push(['_trackEvent', 'Lafema Banner', 'Click', 'Lafema Home Banner - Bottom',1,true])" href="http://www.lafema.com/">
<img src="{{media url="wysiwyg/BottomBannersWeb/Bottombanners_lafema.jpg"}}" style="padding: 0 5px;"  alt="Shop cosmetics at Lafema" />
</a>
<a title="Shop at Venbi" onclick="_gaq.push(['_trackEvent', 'Venbi Banner', 'Click', 'Venbi Home Banner - Bottom',1,true])" href="http://www.venbi.com/">
<img src="{{media url="wysiwyg/BottomBannersWeb/Bottombanners_venbi.jpg"}}" alt="Shop baby and mom products at Venbi" />
</a>
</div>
<h2>&nbsp;</h2>
<h2>กล้า มีฝัน มุ่งมั่น</h2>
<p class="end_content_info">แล้วคุณมองเห็น ความเป็น Moxy หรือยังถ้าคุณคิดแบบ Moxy ก็แปลว่า คุณมีความคิดและความมุ่งมั่นว่าคุณจะต้องเป็นคนสำคัญ</p>
<p class="end_content_info">Moxy เป็นแบรนด์ที่ชัดเจนแต่เรียบง่าย และ บ่งบอกถึงแรงพลังที่จะสร้างสไตล์ให้กับตัวเอง นโยบายหมายเลขหนึ่งของ Moxy คือ การอยู่ร่วมโลกทุกวันนี้ เป็นโลกของการอยู่ร่วม</p>
<p class="end_content_info">ไม่ว่าจะกับเพื่อน ครอบครัว คนรู้จัก เมืองที่คุณอยู่ และจนถึงวัฒนธรรมร่วม ทิศทางที่จะเป็นประโยชน์สูงสุดแก่การอยู่ร่วม ไม่ใช่จะเป็นผลดีต่อแบรนด์เท่านั้น</p>
<p class="end_content_info">แต่มันจะกระทบทุกคนไปในทางบวก Moxy ไม่ได้มุ่งแต่กำไร แต่ยังตั้งใจที่จะให้ บริการที่ดีจริง และ ต้องการจะร่วมงานกับนักออกแบบและผู้ผลิตที่เป็นคนไทย บวกกับสินค้าที่ดีต่อสังคม เพราะ Moxy รู้ถึงประโยชน์ของการตอบแทน</p>
EOD;

    $pageRootTemplate = 'one_column';
    $pageLayoutUpdateXml = <<<EOD
    <reference name="header">
    <block type="cms/block" name="slider_1">
        <!--
        The content of this block is taken from the database by its block_id.
        You can manage it in admin CMS -> Static Blocks
        -->
        <action method="setBlockId"><block_id>slider_1</block_id></action>
    </block>
    <block type="cms/block" name="banners_home">
        <!--
        The content of this block is taken from the database by its block_id.
        You can manage it in admin CMS -> Static Blocks
        -->
        <action method="setBlockId"><block_id>banners_home</block_id></action>
    </block>
</reference>
<reference name="head">
    <block type="core/text" name="homepage.metadata" after="-">
        <action method="addText"><text><![CDATA[<meta name="google-site-verification" content="_4n5alA2_c2rhcm4BQTl-KqLMwVGw_gyHr20g1fCalA" />]]></text></action>
    </block>
</reference>
EOD;


    $pageCustomLayoutUpdateXML = <<<EOD
EOD;

    $pageMetaKeywords = "";
    $pageMetaDescription = "";

    $page = Mage::getModel('cms/page')->getCollection()
        ->addStoreFilter(array($storeIdMoxyTh), $withAdmin = true)
        ->addFieldToFilter('identifier', $pageIdentifier)
        ->getFirstItem();
    if ($page->getId() == 0) {
        $page = Mage::getModel('cms/page');
    }
    else
    {
        // if exists then repair
        $page = Mage::getModel('cms/page')->load($page->getId());
    }

    $page->setTitle($pageTitle);
    $page->setIdentifier($pageIdentifier);
    $page->setStores($pageStores);
    $page->setIsActive($pageIsActive);
    $page->setUnderVersionControl($pageUnderVersionControl);
    $page->setContentHeading($pageContentHeading);
    $page->setContent($pageContent);
    $page->setRootTemplate($pageRootTemplate);
    $page->setLayoutUpdateXml($pageLayoutUpdateXml);
    $page->setCustomLayoutUpdateXml($pageCustomLayoutUpdateXML);
    $page->setMetaKeywords($pageMetaKeywords);
    $page->setMetaDescription($pageMetaDescription);
    $page->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================
    $installer->endSetup();
} catch (Excpetion $e) {
    Mage::logException($e);
    Mage::log("ERROR IN SETUP " . $e->getMessage());
}
