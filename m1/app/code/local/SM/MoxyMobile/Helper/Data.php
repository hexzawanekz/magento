<?php

class SM_MoxyMobile_Helper_Data extends Mage_Core_Helper_Abstract
{
    // url keys of Moxy categories
    protected $_fashion = array();
    protected $_living = array();
    protected $_electronics = array();

    public function __construct()
    {
        /** @var SM_MoxyMobile_Model_Navigation $model */
        $model = Mage::getModel('sm_moxymobile/navigation');

        $this->_fashion     = $model->getFashionUrlKeys();
        $this->_living      = $model->getLivingUrlKeys();
        $this->_electronics = $model->getElectronicUrlKeys();
    }

    public function getKey($key){

        if($key == null){
            $key = $this->getParentCategoriesWithLevel($this->getCurrentCategory(), 2)->getUrlKey();
        }
        if(in_array($key, $this->_fashion)){
            $key = SM_MoxyMobile_Model_Navigation::KEY_FASHION;
        }
        elseif(in_array($key, $this->_living)){
            $key = SM_MoxyMobile_Model_Navigation::KEY_HOMELIVING;
        }
        elseif(in_array($key, $this->_electronics)){
            $key = SM_MoxyMobile_Model_Navigation::KEY_ELECTRONICS;
        }
        return $key;
    }

    public function getParentCategoriesWithLevel($category, $level)
    {
        $pathIds = array_reverse(explode(',', $category->getPathInStore()));
        $categories = Mage::getResourceModel('catalog/category_collection')
            ->setStore(Mage::app()->getStore())
            ->addAttributeToSelect('name')
            ->addAttributeToSelect('url_key')
            ->addFieldToFilter('entity_id', array('in' => $pathIds))
            ->addFieldToFilter('is_active', 1)
            ->addFieldToFilter('level', $level)
            ->load()
            ->getFirstItem();
        return $categories;
    }

    public function getCurrentCategory()
    {
        if (Mage::getSingleton('catalog/layer')) {
            return Mage::getSingleton('catalog/layer')->getCurrentCategory();
        }
        return false;
    }

    public function checkCssClass($object = null)
    {
        $currentStoreCode = Mage::app()->getStore()->getCode();

        $global_links = $this->getGlobalLinks();
        if(count($global_links) > 0){
            $url_keys = array();
            foreach($global_links as $key => $link){
                if(isset($link['label']) && isset($link['url_key']) && isset($link['css_class'])){
                    $url_keys[$key] = $link['url_key'];
                }
            }

            // apply for only Moxy site
            if($object instanceof Mage_Catalog_Model_Category){
                if(!in_array($currentStoreCode, array('moxyen','moxyth'))) return false;

                if(in_array($object->getData('url_key'), $url_keys)){
                    return $global_links[array_search($object->getData('url_key'), $url_keys)]['css_class'];
                }

                if ($parentIds = $object->getParentIds()){
                    $parentIds[] = $object->getId();
                    foreach ($parentIds as $cateId){
                        if ($cateObj = Mage::getModel('catalog/category')->load($cateId)){
                            $urlKey = $cateObj->getUrlKey();
                            if (in_array($urlKey, $url_keys)){
                                return $global_links[array_search($urlKey, $url_keys)]['css_class'];
                            }
                        }
                    }
                }
            }
            if($object instanceof Mage_Catalog_Model_Product){
                if(!in_array($currentStoreCode, array('moxyen','moxyth'))) return false;
                //
                $currentUrl = Mage::helper('core/url')->getCurrentUrl();
                $url = Mage::getSingleton('core/url')->parseUrl($currentUrl);
                $path = $url->getPath();
                $pathArr = explode('/', $path);
                // round 1 - check url path
                foreach($pathArr as $str){
                    if (in_array($str, $url_keys)){
                        return $global_links[array_search($str, $url_keys)]['css_class'];
                    }
                }
                // round 2 - check category
                if($categoryIds = $object->getCategoryIds()){
                    /** @var Mage_Catalog_Model_Category $modelObj */
                    $modelObj = Mage::getModel('catalog/category');
                    foreach ($categoryIds as $id){
                        if ($modelObj->checkId($id)){
                            $cateObj = Mage::getModel('catalog/category')->load($id);
                            if ($parentIds = $cateObj->getParentIds()){
                                $parentIds[] = $id;
                                foreach ($parentIds as $parentId){
                                    if ($parentCateObj = Mage::getModel('catalog/category')->load($parentId)){
                                        $urlKey = $parentCateObj->getUrlKey();
                                        if (in_array($urlKey, $url_keys)){
                                            return $global_links[array_search($urlKey, $url_keys)]['css_class'];
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    public function getMoxyCategoriesByKey($key)
    {
        if($key == SM_MoxyMobile_Model_Navigation::KEY_FASHION){
            return explode(',', Mage::getStoreConfig('petloftcatalog/moxy_vertical/fashion_site'));
        }elseif($key == SM_MoxyMobile_Model_Navigation::KEY_HOMELIVING){
            return explode(',', Mage::getStoreConfig('petloftcatalog/moxy_vertical/living_site'));
        }elseif($key == SM_MoxyMobile_Model_Navigation::KEY_ELECTRONICS){
            return explode(',', Mage::getStoreConfig('petloftcatalog/moxy_vertical/electronics_site'));
        }
        return false;
    }

    /**
     * Get global links in Sys Config
     *
     * @return bool|mixed
     */
    public function getGlobalLinks()
    {
        $config = Mage::getStoreConfig('petloftcatalog/moxy_vertical/global_links', Mage_Core_Model_App::ADMIN_STORE_ID);
        return $config ? @unserialize($config) : false;
    }

    /**
     * Get link array from config
     * @return array|bool
     */
    public function getLinkArray(){
        $arrayLinks = array();
        if($rawData = $this->getGlobalLinks()) {
            foreach($rawData as $_params) {
                array_push($arrayLinks, $_params['url_key']);
            }
        }

        return (count($arrayLinks) == 0) ? false : $arrayLinks;
    }

    public function getCssPath($uri){
        $globalLinkArray = $this->getGlobalLinks();

        if($rawData = $this->getGlobalLinks()) {
            foreach($rawData as $_params) {
                if($_params['url_key'] == $uri)
                    return $_params['css_class'];
            }
        }

        return "";
    }
}