<?php

class SM_MoxyMobile_Model_Source_Category
{
    public function toOptionArray()
    {
        $result = array();
        /** @var Mage_Catalog_Model_Category $moxyCate */
        $moxyCate = Mage::getModel('sm_moxymobile/navigation')->getRootCategory();

        if($moxyCate->getId()){
            $cates = Mage::getModel('catalog/category')->getCollection()
                ->addAttributeToSelect('entity_id')
                ->addAttributeToSelect('name')
                ->addAttributeToSelect('url_key')
                ->addAttributeToFilter('parent_id',array('eq' => $moxyCate->getId()));

            if($cates->count() > 0){
                foreach($cates as $cate){
                    $result[] = array(
                        'value' => $cate->getData('entity_id'),
                        'label' => $cate->getData('name'),
                    );
                }
            }
        }

        return $result;
    }
}