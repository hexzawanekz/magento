<?php

class SM_MoxyMobile_Model_Navigation extends Mage_Core_Model_Abstract
{
    const FASHION_URL       = 'moxy/fashion';
    const HOMELIVING_URL    = 'moxy/homeliving';
    const ELECTRONICS_URL   = 'moxy/electronics';
    const PETLOFT_URL   = 'petloft.html';
    const SANOGA_URL   = 'sanoga.html';
    const LAFEMA_URL   = 'lafema.html';
    const VENBI_URL   = 'venbi.html';

    // url keys of Moxy categories
    protected $_fashion     = array();
    protected $_living      = array();
    protected $_electronics = array();

    // accepted keys
    const KEY_FASHION       = 'fashion';
    const KEY_HOMELIVING    = 'homeliving';
    const KEY_ELECTRONICS   = 'electronics';
    protected $_keys        = array('fashion','homeliving','electronics');
    protected $_fakeroot        = array('petloft','sanoga','venbi','lafema');

    public function _construct()
    {
        $fashion        = Mage::getStoreConfig('petloftcatalog/moxy_vertical/fashion_site', Mage_Core_Model_App::ADMIN_STORE_ID);
        $living         = Mage::getStoreConfig('petloftcatalog/moxy_vertical/living_site', Mage_Core_Model_App::ADMIN_STORE_ID);
        $electronics    = Mage::getStoreConfig('petloftcatalog/moxy_vertical/electronics_site', Mage_Core_Model_App::ADMIN_STORE_ID);

        /** @var Mage_Catalog_Model_Category $cateObj */
        $cateObj = Mage::getModel('catalog/category');

        if($fashion){
            $fashion = explode(',', $fashion);
            foreach($fashion as $id){
                try{
                    $cateObj->load($id);
                    if($cateObj->getId()) $this->_fashion[] = $cateObj->getUrlKey();
                }catch (Exception $e){
                    continue;
                }
            }
        }

        if($living){
            $living = explode(',', $living);
            foreach($living as $id){
                try{
                    $cateObj->load($id);
                    if($cateObj->getId()) $this->_living[] = $cateObj->getUrlKey();
                }catch (Exception $e){
                    continue;
                }
            }
        }

        if($electronics){
            $electronics = explode(',', $electronics);
            foreach($electronics as $id){
                try{
                    $cateObj->load($id);
                    if($cateObj->getId()) $this->_electronics[] = $cateObj->getUrlKey();
                }catch (Exception $e){
                    continue;
                }
            }
        }

        unset($cateObj);
    }

    public function checkKey($key)
    {
        return in_array($key, $this->_keys);
    }

    public function checkRoot($key)
    {
        return in_array($key, $this->_fakeroot);
    }

    public function getCategoryIdsByKey($key)
    {
        $result = array();
        /** @var Mage_Catalog_Model_Category $moxyCate */
        $moxyCate = $this->getRootCategory();

        if($moxyCate->getId()){
            $cates = Mage::getModel('catalog/category')->getCollection()
                ->addAttributeToSelect('entity_id')
                ->addAttributeToSelect('url_key')
                ->addAttributeToFilter('url_key', array(
                    'in' => $key == $this->_keys[0] ? $this->_fashion : ($key == $this->_keys[1] ? $this->_living : $this->_electronics)
                ))
                ->addAttributeToFilter('parent_id',array('eq' => $moxyCate->getId()));

            if($cates->count() > 0){
                foreach($cates as $cate){
                    $result[] = $cate->getData('entity_id');
                }
            }
        }

        return $result;
    }

    public function getPageByKey($key)
    {
        if($key == self::KEY_FASHION) return Mage::getBaseUrl().$this::FASHION_URL;
        if($key == 'living') return Mage::getBaseUrl().$this::HOMELIVING_URL;
        if($key == self::KEY_ELECTRONICS) return Mage::getBaseUrl().$this::ELECTRONICS_URL;
        return Mage::getBaseUrl();
    }

    public function getFashionUrlKeys()
    {
        return $this->_fashion;
    }

    public function getLivingUrlKeys()
    {
        return $this->_living;
    }

    public function getElectronicUrlKeys()
    {
        return $this->_electronics;
    }

    public function getRootCategory()
    {
        if($cateId = Mage::app()->getStore('moxyen')->getRootCategoryId()){//moxyth
            return Mage::getModel('catalog/category')->load($cateId);
        }
        return false;
    }
}