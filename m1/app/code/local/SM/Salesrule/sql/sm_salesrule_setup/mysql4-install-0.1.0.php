<?php

$installer = $this;
$installer->startSetup();

$installer->getConnection()
    ->addColumn($installer->getTable('salesrule'),
        'maximum_discount',
        array(
            'type' => Varien_Db_Ddl_Table::TYPE_DECIMAL,
            'length' => 12,
            'nullable' => true,
            'default' => null,
            'comment' => 'Maximum discount'
        )
    );

$installer->endSetup();