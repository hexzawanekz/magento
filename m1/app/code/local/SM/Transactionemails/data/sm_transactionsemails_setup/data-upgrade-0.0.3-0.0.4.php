<?php


/** @var Mage_Core_Model_Resource_Setup $installer */
$installer = $this;

$installer->startSetup();

/** Order Cancellation Mail Template Moxy */
/** @var Mage_Newsletter_Model_Template $mailTemplateModel */

$mailTemplateModel = Mage::getModel('newsletter/template');
$mailTemplateModel->loadByCode('HOW TO ORDER MOXY');
$mailTemplateModel->setTemplateCode('HOW TO ORDER MOXY');
$mailTemplateModel->setTemplateSubject('How to order!');
$mailTemplateModel->setTemplateType(Mage_Core_Model_Email_Template::TYPE_HTML);
$mailTemplateModel->setTemplateSenderName('Moxy Customer Service');
$mailTemplateModel->setTemplateSenderEmail('support@moxyst.com');

$mailContent = <<<HTML
<meta charset="utf-8"/>
<div class="mail-container" style="width:600px; padding:0px; font-family: Tahoma; font-size: 14px; color: #3c3d41; margin: 0px;">
    <div class="mail-header" style="width:95%; min-height: 110px; max-height: 110px; position: relative; margin:0px auto;">
        <div class="mail-logo" style="width:220px; min-height: 110px; max-height: 110px; margin:0px auto;">
            <a href="http://moxy.co.th/"><img src="{{skin _area="frontend"  url="images/email/moxy-orami-logo.png"}}" style="width:220px; max-height: 110px; min-height: 110px;" /></a>
        </div>
    </div>

    <img style="width: 600px; margin-top:20px;" alt="How To Order" src="{{skin _area="frontend"  url="images/email/how_to_order_heading.png"}}" />

    <img style="width: 600px; margin-top:20px;" alt="Order Steps" src="{{skin _area="frontend"  url="images/email/order_step_moxy_new.png"}}"/>

    <img style="width: 600px; margin:20px 0px;" alt="Order Steps" src="{{skin _area="frontend"  url="images/email/delivery_moxy.png"}}"/>

    <div class="tryit" style="text-align: center; margin-bottom: 20px; font-size: 24px; text-transform: uppercase;">Try It Now!</div>

    <div class="product-list">
        <div class="product-item" style="width: 32%; border: solid 1px #bbb; padding-bottom: 10px; margin: 5px 2px; float: left;">
            <a href="http://www.moxy.co.th/th/pets/kat-to-cat-litter-apple-10lt.html"><div class="product-image"><img width="100%" style="border: none;" src="{{skin _area="frontend"  url="images/email/kat-to.jpg"}}"/></div></a>

            <div class="product-name" style="text-align: center; white-space: nowrap; width: 95%; overflow: hidden; text-overflow: ellipsis; margin: 5px auto;">
                <a style="color: #3c3d41; text-decoration: none;" href="http://www.moxy.co.th/th/pets/kat-to-cat-litter-apple-10lt.html">KAT-TO Cat Litter Apple (10lt)</a>
            </div>
            <div class="product-more" style="margin-top: 10px; padding: 0px 5px;">
                <div class="product-price" style="float:left;">Price <strong>฿139</strong></div>
                <div class="product-discount" style="color: #0e80c0; float:right; color: #ee5191;">-23%</div>
                <div style="clear: both"></div>
            </div>
        </div>

        <div class="product-item" style="width: 32%; border: solid 1px #bbb; padding-bottom: 10px; margin: 5px 2px; float: left;">
            <a href="http://www.moxy.co.th/th/pets/kat-to-cat-litter-apple-10lt.html"><div class="product-image"><img width="100%" style="border: none;" src="{{skin _area="frontend"  url="images/email/kat-to.jpg"}}"/></div></a>

            <div class="product-name" style="text-align: center; white-space: nowrap; width: 95%; overflow: hidden; text-overflow: ellipsis; margin: 5px auto;">
                <a style="color: #3c3d41; text-decoration: none;" href="http://www.moxy.co.th/th/pets/kat-to-cat-litter-apple-10lt.html">KAT-TO Cat Litter Apple (10lt)</a>
            </div>
            <div class="product-more" style="margin-top: 10px; padding: 0px 5px;">
                <div class="product-price" style="float:left;">Price <strong>฿139</strong></div>
                <div class="product-discount" style="color: #0e80c0; float:right; color: #ee5191;">-23%</div>
                <div style="clear: both"></div>
            </div>
        </div>

        <div class="product-item" style="width: 32%; border: solid 1px #bbb; padding-bottom: 10px; margin: 5px 2px; float: left;">
            <a href="http://www.moxy.co.th/th/pets/kat-to-cat-litter-apple-10lt.html"><div class="product-image"><img width="100%" style="border: none;" src="{{skin _area="frontend"  url="images/email/kat-to.jpg"}}"/></div></a>

            <div class="product-name" style="text-align: center; white-space: nowrap; width: 95%; overflow: hidden; text-overflow: ellipsis; margin: 5px auto;">
                <a style="color: #3c3d41; text-decoration: none;" href="http://www.moxy.co.th/th/pets/kat-to-cat-litter-apple-10lt.html">KAT-TO Cat Litter Apple (10lt)</a>
            </div>
            <div class="product-more" style="margin-top: 10px; padding: 0px 5px;">
                <div class="product-price" style="float:left;">Price <strong>฿139</strong></div>
                <div class="product-discount" style="color: #0e80c0; float:right; color: #ee5191;">-23%</div>
                <div style="clear: both"></div>
            </div>
        </div>

        <div style="clear: both;"></div>
    </div>

    <!-- Footer -->
    <div class="navigation-th" style="margin-top: 24px; overflow:hidden;">
        <div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #ee5191; border-bottom: 2px solid #ee5191; text-align:center;">
            <span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 12px; padding-right: 5px;"><a href="http://www.moxy.co.th/th/beauty.html" style="color: #3c3d41; text-decoration: none;">ความงาม</a></span>
            <span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/fashion.html" style="color: #3c3d41; text-decoration: none;">แฟชั่น</a></span>
            <span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/home-living.html" style="color: #3c3d41; text-decoration: none;">ของแต่งบ้าน</a></span>
            <span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/home-appliances.html" style="color: #3c3d41; text-decoration: none;">เครื่องใช้ไฟฟ้าภายในบ้าน</a></span>
            <span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/electronics.html" style="color: #3c3d41; text-decoration: none;">อิเล็กทรอนิกส์</a></span>
            <span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/baby.html" style="color: #3c3d41; text-decoration: none;">แม่และเด็ก</a></span>
            <span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/health.html" style="color: #3c3d41; text-decoration: none;">สุขภาพ</a></span>
            <span class="nav-item" style=" line-height: 33px; font-size: 12px; padding-left: 5px;"><a href="http://www.moxy.co.th/th/pets.html" style="color: #3c3d41; text-decoration: none;">สัตว์เลี้ยง</a></span>
        </div>
    </div>

    <div class="email-footer" style="min-height: 82px; max-heigh: 82px; background: #E7E7E7; margin-top: 8px; color: #3c3d41; padding: 0 15px 5px 15px;">
        <div class="footer-col" style="float:left; padding: 17px 24px; width: 141px; color: #3c3d41;">
            <p style="font-size: 15px; margin: 0px 0px 0px"><img src="{{skin _area='frontend'  url='images/email/mail.png'}}" style="width:20px; float:left; margin-right:5px; margin-left:-2px;"/> ติดต่อเรา</p><div style="clear: both;"></div>
            <p style="font-size: 10px; margin: 0;">02-106-8222</p>
            <p style="margin: 0; font-size: 10px;"><a href="mailto:support@moxy.co.th" style="color: #3C3D41; text-decoration: none;">support@moxy.co.th</a></p>
        </div>

        <div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
            <p style="font-size: 15px; margin: 0px 0px 0px">ติดตามที่</p>
            <div class="follow-icons" style="width: 100%">
                <a href="https://www.facebook.com/moxyst"><img src="{{skin _area="frontend"  url="images/email/follow-button-fb.png"}}" style="width: 25px;" alt="Facebook fanpage"></a>
                <a href="https://twitter.com/moxyst"><img src="{{skin _area="frontend"  url="images/email/follow-button-twitter.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
                <a href="https://www.instagram.com/moxy_th/"><img src="{{skin _area="frontend"  url="images/email/follow-button-instagram.png"}}" style="width: 25px;" alt="Instagram fanpage"></a>
                <a href="https://plus.google.com/+Moxyst"><img src="{{skin _area="frontend"  url="images/email/follow-button-googleplus.png"}}" style="width: 25px;" alt="Google fanpage"></a>
                <a href="https://www.pinterest.com/MoxySEA1/"><img src="{{skin _area="frontend"  url="images/email/follow-button-pinterest.png"}}" style="width: 25px;" alt="Pinterest fanpage"></a>
            </div>
        </div>

        <div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
            <p style="font-size: 15px;margin: 0px 0px 0px">ช่องทางการชำระเงิน</p>
            <img src="{{skin _area="frontend"  url="images/email/payment_new.png"}}" alt="ช่องทางการชำระเงิน" style="margin-top: 3px;">
            <div class="cod" style="font-size: 12px;">
                <img src="{{skin _area='frontend'  url='images/email/COD.png'}}" style="float: left;">
                <p style="float: left; margin: 3px 0 0; font-size: 11px;">ช่องทางการชำระเงิน</p>
            </div>
        </div>
    </div>

    <div class="footer" style="padding: 0px 33px; margin-top: 18px;">
        <div class="copyr" style="float: left; font-size: 12px;">&copy;2016 Whatsnew Co.,Ltd. All Rights Reserved.</div>
        <div class="links" style="float: right; font-size: 12px;">
            <a href="http://www.moxy.co.th/th/about-us/" style="color: #3c3d41;"><span>เกี่ยวกับเรา</span></a> |
            <a href="http://www.moxy.co.th/th/terms-and-conditions/" style="color: #3c3d41;"><span>ข้อกำหนดและเงื่อนไข</span></a> |
            <a href="http://www.moxy.co.th/th/privacy-policy/" style="color: #3c3d41;"><span>ความเป็นส่วนตัว</span></a>
        </div>
    </div>
    <div style="clear:both"></div>
    <!-- End Footer -->
</div>
HTML;

$mailTemplateModel->setTemplateText($mailContent);
$mailTemplateModel->save();

$installer->endSetup();