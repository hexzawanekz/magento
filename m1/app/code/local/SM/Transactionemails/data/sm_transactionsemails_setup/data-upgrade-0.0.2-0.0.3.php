<?php


/** @var Mage_Core_Model_Resource_Setup $installer */
$installer = $this;

$installer->startSetup();

/** Order Cancellation Mail Template Moxy */
/** @var Mage_Adminhtml_Model_Email_Template $mailTemplateModel */
$mailTemplateModel = Mage::getModel('adminhtml/email_template');

$mailTemplateModel->loadByCode('Back in Stock Moxy');
$mailTemplateModel->setTemplateCode('Back in Stock Moxy');
$mailTemplateModel->setTemplateSubject('{{var product}} is back in stock!');
$mailTemplateModel->setTemplateType(2);

$mailContent = <<<HTML
<meta charset="utf-8"/>
<div class="mail-container" style="width:600px; padding:0px 33px; font-family:  'Trebuchet MS', Tahoma; font-size: 14px; color: #3c3d41; margin: 0px;">
    <div class="mail-header" style="width:95%; min-height: 110px; max-height: 110px; position: relative; margin:0px auto;">
        <div class="mail-logo" style="width:220px; min-height: 110px; max-height: 110px; margin:0px auto;">
            <a href="http://moxy.co.th/"><img src="{{skin url="images/email/moxy-orami-logo.png"}}" style="width:220px; max-height: 110px; min-height: 110px;" /></a>
        </div>

        <div class="english-below" style="min-height: 16px; max-height: 16px; position: absolute; bottom: 0px; line-height: 16px;">
            <span><img src="{{skin url="images/email/flag-en.png"}}" height="16px;"/> Scroll down for English version</span>
        </div>
    </div>

    <img style="width: 100%; margin-top:40px;" src="{{skin url="images/email/its_backinstock_thai.png"}}" alt="Reset Password." />
    <div class="product-info" style="width: 100%; margin:5px auto; padding:10px; border: 1px solid #e7e7e7;">
        <div class="product-image" style="width: 36%; padding: 5px; border: 1px solid #e7e7e7; float: left;">
            <a href="{{var product_url}}?utm_source=edm&amp;utm_medium=reminder&amp;utm_campaign=moxy_outofstock_reminder"><img src="{{var product_image}}" alt="{{var product}}" style="border: none; width: 95%;"/></a>
        </div>

        <div class="product-detail" style="float: right; width: 60%; text-align: center; margin-top: 15px;">
            <p><strong>{{var product}}</strong></p>

            <p><strong>{{var product_price}}</strong></p>

            <a href="{{var product_url}}?utm_source=edm&amp;utm_medium=reminder&amp;utm_campaign=moxy_outofstock_reminder" style=" background: #ee5191;color: #fff;padding: 10px;text-decoration: none;" >สั่งซื้อตอนนี้เลย</a>
        </div>

        <div style="clear: both;"></div>
    </div>
    <div class="mail-body" style="margin:50px auto; width: 95%;">
        <p>เรียน คุณ {{var customer.name}},</p>
        <br/>

        <p>คุณได้ทำการเพิ่มสินค้า {{var product}} เมื่อวันที่ {{var date}} เข้าไปยังตะกร้าสินค้าก่อนหน้านี้ </p>
        <p>เรามีความยินดีจะแจ้งว่าสินค้าตอนนี้ได้กลับเข้าสู่สต็อกสินค้าแล้ว</p>
        <br>

        <p>Warm Regards,</p>
        <p>Moxy Team</p>
    </div>

    <!-- Footer -->
    <div class="navigation-th" style="margin-top: 24px; overflow:hidden;">
        <div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #ee5191; border-bottom: 2px solid #ee5191; text-align:center;">
            <span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 12px; padding-right: 5px;"><a href="http://www.moxy.co.th/th/beauty.html" style="color: #3c3d41; text-decoration: none;">ความงาม</a></span>
            <span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/fashion.html" style="color: #3c3d41; text-decoration: none;">แฟชั่น</a></span>
            <span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/home-living.html" style="color: #3c3d41; text-decoration: none;">ของแต่งบ้าน</a></span>
            <span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/home-appliances.html" style="color: #3c3d41; text-decoration: none;">เครื่องใช้ไฟฟ้าภายในบ้าน</a></span>
            <span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/electronics.html" style="color: #3c3d41; text-decoration: none;">อิเล็กทรอนิกส์</a></span>
            <span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/baby.html" style="color: #3c3d41; text-decoration: none;">แม่และเด็ก</a></span>
            <span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/health.html" style="color: #3c3d41; text-decoration: none;">สุขภาพ</a></span>
            <span class="nav-item" style=" line-height: 33px; font-size: 12px; padding-left: 5px;"><a href="http://www.moxy.co.th/th/pets.html" style="color: #3c3d41; text-decoration: none;">สัตว์เลี้ยง</a></span>
        </div>
    </div>

    <div class="email-footer" style="min-height: 82px; max-heigh: 82px; background: #E7E7E7; margin-top: 8px; color: #3c3d41; padding: 0 15px 5px 15px;">
        <div class="footer-col" style="float:left; padding: 17px 24px; width: 141px; color: #3c3d41;">
            <p style="font-size: 15px; margin: 0px 0px 0px"><img src="{{skin _area='frontend'  url='images/email/mail.png'}}" style="width:20px; float:left; margin-right:5px; margin-left:-2px;"/> ติดต่อเรา</p><div style="clear: both;"></div>
            <p style="font-size: 10px; margin: 0;">02-106-8222</p>
            <p style="margin: 0; font-size: 10px;"><a href="mailto:support@moxy.co.th" style="color: #3C3D41; text-decoration: none;">support@moxy.co.th</a></p>
        </div>

        <div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
            <p style="font-size: 15px; margin: 0px 0px 0px">ติดตามที่</p>
            <div class="follow-icons" style="width: 100%">
                <a href="https://www.facebook.com/moxyst"><img src="{{skin _area="frontend"  url="images/email/follow-button-fb.png"}}" style="width: 25px;" alt="Facebook fanpage"></a>
                <a href="https://twitter.com/moxyst"><img src="{{skin _area="frontend"  url="images/email/follow-button-twitter.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
                <a href="https://www.instagram.com/moxy_th/"><img src="{{skin _area="frontend"  url="images/email/follow-button-instagram.png"}}" style="width: 25px;" alt="Instagram fanpage"></a>
                <a href="https://plus.google.com/+Moxyst"><img src="{{skin _area="frontend"  url="images/email/follow-button-googleplus.png"}}" style="width: 25px;" alt="Google fanpage"></a>
                <a href="https://www.pinterest.com/MoxySEA1/"><img src="{{skin _area="frontend"  url="images/email/follow-button-pinterest.png"}}" style="width: 25px;" alt="Pinterest fanpage"></a>
            </div>
        </div>

        <div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
            <p style="font-size: 15px;margin: 0px 0px 0px">ช่องทางการชำระเงิน</p>
            <img src="{{skin _area="frontend"  url="images/email/payment_new.png"}}" alt="ช่องทางการชำระเงิน" style="margin-top: 3px;">
            <div class="cod" style="font-size: 12px;">
                <img src="{{skin _area='frontend'  url='images/email/COD.png'}}" style="float: left;">
                <p style="float: left; margin: 3px 0 0; font-size: 11px;">ช่องทางการชำระเงิน</p>
            </div>
        </div>
    </div>

    <div class="footer" style="padding: 0px 33px; margin-top: 18px;">
        <div class="copyr" style="float: left; font-size: 12px;">&copy;2016 Whatsnew Co.,Ltd. All Rights Reserved.</div>
        <div class="links" style="float: right; font-size: 12px;">
            <a href="http://www.moxy.co.th/th/about-us/" style="color: #3c3d41;"><span>เกี่ยวกับเรา</span></a> |
            <a href="http://www.moxy.co.th/th/terms-and-conditions/" style="color: #3c3d41;"><span>ข้อกำหนดและเงื่อนไข</span></a> |
            <a href="http://www.moxy.co.th/th/privacy-policy/" style="color: #3c3d41;"><span>ความเป็นส่วนตัว</span></a>
        </div>
    </div>
    <div style="clear:both"></div>
    <!-- End Footer -->

    <hr style="margin: 40px auto; width: 95%;">

    <!--English Version-->
    <img style="width: 100%; margin-bottom:40px;" src="{{skin url="images/email/its_backinstock_en.png"}}" alt="Reset Password." />
    <div class="product-info" style="width: 100%; margin:5px auto; padding:10px; border: 1px solid #e7e7e7;">
        <div class="product-image" style="width: 36%; padding: 5px; border: 1px solid #e7e7e7; float: left;">
            <a href="{{var product_url}}?utm_source=edm&amp;utm_medium=reminder&amp;utm_campaign=moxy_outofstock_reminder"><img src="{{var product_image}}" alt="{{var product}}" style="border: none; width: 95%;"/></a>
        </div>

        <div class="product-detail" style="float: right; width: 60%; text-align: center; margin-top: 15px;">
            <p><strong>{{var product}}</strong></p>
            <p><strong>{{var product_price}}</strong></p>
            <a href="{{var product_url}}?utm_source=edm&amp;utm_medium=reminder&amp;utm_campaign=moxy_outofstock_reminder" style=" background: #ee5191;color: #fff;padding: 10px;text-decoration: none;" >BUY IT NOW</a>
        </div>

        <div style="clear: both;"></div>
    </div>
    <div class="mail-body" style="margin:50px auto; width: 95%;">
        <p>Dear {{var customer.name}},</p>
        <br/>

        <p>On {{var date}} , you tried to purchase {{var product}}. </p>
        <p>We have a good news for you, it is back in stock.</p>
        <br>
        <p>Get your hands on it now while stock lasts.</p>

        <p>Warm Regards,</p>
        <p>Moxy Team</p>
    </div>

    <!-- Footer -->
    <div class="navigation-th" style="margin-top: 24px; overflow:hidden;">
        <div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #ee5191; border-bottom: 2px solid #ee5191; text-align:center;">
            <span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 12px; padding-right: 5px;"><a href="http://www.moxy.co.th/th/beauty.html" style="color: #3c3d41; text-decoration: none;">Beauty</a></span>
            <span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/fashion.html" style="color: #3c3d41; text-decoration: none;">Fashion</a></span>
            <span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/home-living.html" style="color: #3c3d41; text-decoration: none;">Home Decor</a></span>
            <span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/home-appliances.html" style="color: #3c3d41; text-decoration: none;">Home Appliances</a></span>
            <span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/electronics.html" style="color: #3c3d41; text-decoration: none;">Electronics</a></span>
            <span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/baby.html" style="color: #3c3d41; text-decoration: none;">Baby & Mom</a></span>
            <span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/health.html" style="color: #3c3d41; text-decoration: none;">Health & Sports</a></span>
            <span class="nav-item" style=" line-height: 33px; font-size: 12px; padding-left: 5px;"><a href="http://www.moxy.co.th/th/pets.html" style="color: #3c3d41; text-decoration: none;">Pets</a></span>
        </div>
    </div>

    <div class="email-footer" style="min-height: 82px; max-heigh: 82px; background: #E7E7E7; margin-top: 8px; color: #3c3d41;padding: 0 15px 5px 15px;">
        <div class="footer-col" style="float:left; padding: 17px 24px; width: 141px; color: #3c3d41;">
            <p style="font-size: 15px; margin: 0px 0px 0px"><img src="{{skin _area='frontend'  url='images/email/mail.png'}}" style="width:20px; float:left; margin-right:5px; margin-left: -2px;"/> Contact Us</p><div style="clear: both;"></div>
            <p style="font-size: 10px; margin: 0;">02-106-8222</p>
            <p style="margin: 0; font-size: 10px;"><a href="mailto:support@moxy.co.th" style="color: #3C3D41; text-decoration: none;">support@moxy.co.th</a></p>
        </div>

        <div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
            <p style="font-size: 15px; margin: 0px 0px 0px">Follow Us</p>
            <div class="follow-icons" style="width: 100%">
                <a href="https://www.facebook.com/moxyst"><img src="{{skin _area="frontend"  url="images/email/follow-button-fb.png"}}" style="width: 25px;" alt="Facebook fanpage"></a>
                <a href="https://twitter.com/moxyst"><img src="{{skin _area="frontend"  url="images/email/follow-button-twitter.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
                <a href="https://www.instagram.com/moxy_th/"><img src="{{skin _area="frontend"  url="images/email/follow-button-instagram.png"}}" style="width: 25px;" alt="Instagram fanpage"></a>
                <a href="https://plus.google.com/+Moxyst"><img src="{{skin _area="frontend"  url="images/email/follow-button-googleplus.png"}}" style="width: 25px;" alt="Google fanpage"></a>
                <a href="https://www.pinterest.com/MoxySEA1/"><img src="{{skin _area="frontend"  url="images/email/follow-button-pinterest.png"}}" style="width: 25px;" alt="Pinterest fanpage"></a>
            </div>
        </div>

        <div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
            <p style="font-size: 15px;margin: 0px 0px 0px">Payment Options</p>
            <img src="{{skin _area="frontend"  url="images/email/payment_new.png"}}" alt="Payment Options" style="margin-top: 3px;">
            <div class="cod" style="font-size: 12px;">
                <img src="{{skin _area='frontend'  url='images/email/COD.png'}}" style="float: left;">
                <p style="float: left; margin: 3px 0 0; font-size: 11px;">Cash on Delivery</p>
            </div>
        </div>
    </div>

    <div class="footer" style="padding: 0px 33px; margin-top: 18px;">
        <div class="copyr" style="float: left; font-size: 12px;">&copy;2016 Whatsnew Co.,Ltd. All Rights Reserved.</div>
        <div class="links" style="float: right; font-size: 12px;">
            <a href="http://www.moxy.co.th/en/about-us/" style="color: #3c3d41;"><span>About Us</span></a> |
            <a href="http://www.moxy.co.th/en/terms-and-conditions/" style="color: #3c3d41;"><span>Term & Conditions</span></a> |
            <a href="http://www.moxy.co.th/en/privacy-policy/" style="color: #3c3d41;"><span>Privacy Policy</span></a>
        </div>
    </div>
    <div style="clear:both"></div>
    <!-- End Footer -->
</div>
HTML;

$mailTemplateModel->setTemplateText($mailContent);
$mailTemplateModel->save();

$installer->endSetup();