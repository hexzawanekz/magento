<?php

/** @var Mage_Core_Model_Resource_Setup $installer */
$installer = $this;

$installer->startSetup();


$moxyen = Mage::getModel('core/store')->load('moxyen', 'code')->getId();
$moxyth = Mage::getModel('core/store')->load('moxyth', 'code')->getId();

//==========================================================================
// Promotion Banners Welcome 2 EN
//==========================================================================
/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block')->getCollection()
    ->addFieldToFilter('identifier', 'promotion-banners-welcome-2')
    ->addStoreFilter(array($moxyen), $withAdmin = false)
    ->getFirstItem();

if ($block->getId()) $block->delete();// if exists then delete
$content = <<<EOD
<div class="banner">
<div class="left"><a href="#" target="_self"> <img title="" src="{{media url="wysiwyg/welcomePage/460x200.png"}}" alt="" /></a></div>
<div class="right"><a href="#" target="_self"> <img title="" src="{{media url="wysiwyg/welcomePage/460x200.png"}}" alt="" /></a></div>
</div>
EOD;
/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block');
$block->setTitle('Promotion Banners Welcome 2 EN');
$block->setIdentifier('promotion-banners-welcome-2');
$block->setStores(array($moxyen));
$block->setIsActive(0);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================


//==========================================================================
// Promotion Banners Welcome 2 TH
//==========================================================================
/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block')->getCollection()
    ->addFieldToFilter('identifier', 'promotion-banners-welcome-2')
    ->addStoreFilter(array($moxyth), $withAdmin = false)
    ->getFirstItem();

if ($block->getId()) $block->delete();// if exists then delete
$content = <<<EOD
<div class="banner">
<div class="left"><a href="#" target="_self"> <img title="" src="{{media url="wysiwyg/welcomePage/460x200.png"}}" alt="" /></a></div>
<div class="right"><a href="#" target="_self"> <img title="" src="{{media url="wysiwyg/welcomePage/460x200.png"}}" alt="" /></a></div>
</div>
EOD;
/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block');
$block->setTitle('Promotion Banners Welcome 2 TH');
$block->setIdentifier('promotion-banners-welcome-2');
$block->setStores(array($moxyth));
$block->setIsActive(0);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================


//==========================================================================
// Promotion Banners Welcome 3 EN
//==========================================================================
/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block')->getCollection()
    ->addFieldToFilter('identifier', 'promotion-banners-welcome-3')
    ->addStoreFilter(array($moxyen), $withAdmin = false)
    ->getFirstItem();

if ($block->getId()) $block->delete();// if exists then delete
$content = <<<EOD
<div class="banner">
<div class="left"><a href="#" target="_self"> <img title="" src="{{media url="wysiwyg/welcomePage/460x200.png"}}" alt="" /></a></div>
<div class="right"><a href="#" target="_self"> <img title="" src="{{media url="wysiwyg/welcomePage/460x200.png"}}" alt="" /></a></div>
</div>
EOD;
/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block');
$block->setTitle('Promotion Banners Welcome 3 EN');
$block->setIdentifier('promotion-banners-welcome-3');
$block->setStores(array($moxyen));
$block->setIsActive(0);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================


//==========================================================================
// Promotion Banners Welcome 3 TH
//==========================================================================
/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block')->getCollection()
    ->addFieldToFilter('identifier', 'promotion-banners-welcome-3')
    ->addStoreFilter(array($moxyth), $withAdmin = false)
    ->getFirstItem();

if ($block->getId()) $block->delete();// if exists then delete
$content = <<<EOD
<div class="banner">
<div class="left"><a href="#" target="_self"> <img title="" src="{{media url="wysiwyg/welcomePage/460x200.png"}}" alt="" /></a></div>
<div class="right"><a href="#" target="_self"> <img title="" src="{{media url="wysiwyg/welcomePage/460x200.png"}}" alt="" /></a></div>
</div>
EOD;
/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block');
$block->setTitle('Promotion Banners Welcome 3 TH');
$block->setIdentifier('promotion-banners-welcome-3');
$block->setStores(array($moxyth));
$block->setIsActive(0);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================


//==========================================================================
// Cms page: Home ENG
//==========================================================================
$title = 'ORAMI by Moxy The Online Shopping Destination for Women';
$identifier = "home";
$stores = array($moxyen);
$status = 1;
$content = <<<EOD
{{block type="core/template" name="subscription" as="subscription" template="subscription/subscribe.phtml"}}
EOD;
$rootTemplate = "one_column";
$layoutXML = <<<EOD
<reference name="head">
    <block type="core/text" name="homepage.metadata" after="-">
        <action method="addText"><text><![CDATA[<meta name="google-site-verification" content="IDwIJ1YbPmxWd60Ej1zWtvkggB95TZAyX5lIPOFn5s4"/>]]></text></action>
    </block>
</reference>
<reference name="content">
    <block type="cms/block" name="home.sliders">
        <action method="setBlockId"><block_id>welcome-shortcut</block_id></action>
    </block>

    <block type="petloftcatalog/product_homegrid" name="home.product.grid1" as="homegrid1" template="petloft/catalog/product/homegrid.phtml">
        <action method="setCategoryId"><id>2655</id></action>
    </block>

    <block type="cms/block" name="home.promotion.banner">
        <action method="setBlockId"><block_id>welcome-promotion-banner</block_id></action>
    </block>

     <block type="petloftcatalog/product_homegrid" name="home.product.grid2" as="homegrid2" template="petloft/catalog/product/homegrid.phtml">
        <action method="setCategoryId"><id>2655</id></action>
    </block>

    <block type="cms/block" name="promotion.banner-welcome-2">
        <action method="setBlockId"><block_id>promotion-banners-welcome-2</block_id></action>
    </block>

    <block type="petloftcatalog/product_homegrid" name="home.product.grid3" as="homegrid3" template="petloft/catalog/product/homegrid.phtml">
        <action method="setCategoryId"><id>2656</id></action>
    </block>

    <block type="cms/block" name="welcome.content.banner">
        <action method="setBlockId"><block_id>welcome-content-banner</block_id></action>
    </block>

    <block type="cms/block" name="welcome.bottom.banner">
        <action method="setBlockId"><block_id>welcome-bottom-banner</block_id></action>
    </block>

    <block type="cms/block" name="promotion.banner-welcome-3">
        <action method="setBlockId"><block_id>promotion-banners-welcome-3</block_id></action>
    </block>

    <block type="cms/block" name="welcome.brand.logo">
        <action method="setBlockId"><block_id>welcome-brand-logo</block_id></action>
    </block>
</reference>
EOD;
$metaDesc = 'Beauty, Fashion, Home Decor, Gadgets & Electronics, Health & Sports, Mom & Babies, Pet Products - shipped directly to you!';

/** @var Mage_Cms_Model_Page $page */
$page = Mage::getModel('cms/page')->getCollection()
    ->addStoreFilter($stores, $withAdmin = false)
    ->addFieldToFilter('identifier', $identifier)
    ->getFirstItem();

if($page->getId()) $page->delete();
$page = Mage::getModel('cms/page');
$page->setTitle($title);
$page->setIdentifier($identifier);
$page->setIsActive($status);
$page->setContent($content);
$page->setRootTemplate($rootTemplate);
$page->setLayoutUpdateXml($layoutXML);
$page->setMetaDescription($metaDesc);
$page->setStores($stores);
$page->save();
//==========================================================================
//==========================================================================
//==========================================================================


//==========================================================================
// Cms page:Home THAI
//==========================================================================
$title = 'ORAMI by Moxy เว็บช้อปปิ้งออนไลน์ สำหรับสาวนักช้อปทุกคน';
$identifier = "home";
$stores = array($moxyth);
$status = 1;
$content = <<<EOD
{{block type="core/template" name="subscription" as="subscription" template="subscription/subscribe.phtml"}}
EOD;
$rootTemplate = "one_column";
$layoutXML = <<<EOD
<reference name="head">
    <block type="core/text" name="homepage.metadata" after="-">
        <action method="addText"><text><![CDATA[<meta name="google-site-verification" content="IDwIJ1YbPmxWd60Ej1zWtvkggB95TZAyX5lIPOFn5s4"/>]]></text></action>
    </block>
</reference>
<reference name="content">
    <block type="cms/block" name="home.sliders">
        <action method="setBlockId"><block_id>welcome-shortcut</block_id></action>
    </block>

    <block type="petloftcatalog/product_homegrid" name="home.product.grid1" as="homegrid1" template="petloft/catalog/product/homegrid.phtml">
        <action method="setCategoryId"><id>2655</id></action>
    </block>

    <block type="cms/block" name="home.promotion.banner">
        <action method="setBlockId"><block_id>welcome-promotion-banner</block_id></action>
    </block>

     <block type="petloftcatalog/product_homegrid" name="home.product.grid2" as="homegrid2" template="petloft/catalog/product/homegrid.phtml">
        <action method="setCategoryId"><id>2655</id></action>
    </block>

    <block type="cms/block" name="promotion.banner-welcome-2">
        <action method="setBlockId"><block_id>promotion-banners-welcome-2</block_id></action>
    </block>

    <block type="petloftcatalog/product_homegrid" name="home.product.grid3" as="homegrid3" template="petloft/catalog/product/homegrid.phtml">
        <action method="setCategoryId"><id>2656</id></action>
    </block>

    <block type="cms/block" name="welcome.content.banner">
        <action method="setBlockId"><block_id>welcome-content-banner</block_id></action>
    </block>

    <block type="cms/block" name="welcome.bottom.banner">
        <action method="setBlockId"><block_id>welcome-bottom-banner</block_id></action>
    </block>

    <block type="cms/block" name="promotion.banner-welcome-3">
        <action method="setBlockId"><block_id>promotion-banners-welcome-3</block_id></action>
    </block>

    <block type="cms/block" name="welcome.brand.logo">
        <action method="setBlockId"><block_id>welcome-brand-logo</block_id></action>
    </block>
</reference>
EOD;
$metaDesc = 'เครื่องสำอาง แฟชั่น ของแต่งบ้าน แก็ตเจ็ต อีเล็คทรอนิกส์ สินค้าเพื่อสุขภาพ สินค้าสำหรับแม่และเด็ก และสินค้าสำหรับสัตว์เลี้ยง พร้อมส่งตรงถึงหน้าบ้านคุณ!';

/** @var Mage_Cms_Model_Page $page */
$page = Mage::getModel('cms/page')->getCollection()
    ->addStoreFilter($stores, $withAdmin = false)
    ->addFieldToFilter('identifier', $identifier)
    ->getFirstItem();

if($page->getId()) $page->delete();
$page = Mage::getModel('cms/page');
$page->setTitle($title);
$page->setIdentifier($identifier);
$page->setIsActive($status);
$page->setContent($content);
$page->setRootTemplate($rootTemplate);
$page->setLayoutUpdateXml($layoutXML);
$page->setMetaDescription($metaDesc);
$page->setStores($stores);
$page->save();
//==========================================================================
//==========================================================================
//==========================================================================


/** @var Mage_Cms_Model_Resource_Block_Collection $allBlocks */
$allBlocks = Mage::getModel('cms/block')->getCollection()
    ->addFieldToFilter('identifier', 'welcome-shortcut');
if($allBlocks->count() > 0) {
    /** @var Mage_Cms_Model_Block $item */
    foreach ($allBlocks as $item) {
        if ($item->getId()) $item->delete(); // delete old blocks
    }
}
//==========================================================================
// Welcome splash shortcut link (English)
//==========================================================================
$blockTitle = "Welcome splash shortcut link";
$blockIdentifier = "welcome-shortcut";
$blockStores = array($moxyen);
$blockIsActive = 1;
$blockContent = <<<EOD
<div class="grid-wall">
    <div class="row">
        <div class="col-sm-6">
            <div class="row">
                <div class="col-sm-12">
                    <div class="img-banner">
                        <div id="homepage_first_slider" class="homepage-main" style="float: left; width: 100%;"><!-- Open tab -->
                            <div id="container" style="width: 100%;">
                                <div id="slideshow" style="height: 100%; width: 100%; overflow: hidden;">
                                    <ul id="slide-nav" style="display: none; visibility: hidden;">
                                        <li id="prev"><a class="control-s pre-s" href="#">Previous</a></li>
                                        <li id="next"><a class="control-s nxt-s" href="#">Next</a></li>
                                    </ul>
                                    <ul id="home-slides" style="position: relative; width: 100%; height: 100%;">
                                        <li style="position: absolute; top: 0; left: 0; display: none; z-index: 1; opacity: 1;"><a style="width: 100%;" href="#"><img src="{{media url="wysiwyg/welcomePage/placehold/550x550_1.png"}}" alt="" /></a></li>
                                        <li style="position: absolute; top: 0; left: 0; display: none; z-index: 1; opacity: 1;"><a style="width: 100%;" href="#"><img src="{{media url="wysiwyg/welcomePage/placehold/550x550_2.png"}}" alt="" /></a></li>
                                        <li style="position: absolute; top: 0; left: 0; display: none; z-index: 1; opacity: 1;"><a style="width: 100%;" href="#"><img src="{{media url="wysiwyg/welcomePage/placehold/550x550_3.png"}}" alt="" /></a></li>
                                        <li style="position: absolute; top: 0; left: 0; display: none; z-index: 1; opacity: 1;"><a style="width: 100%;" href="#"><img src="{{media url="wysiwyg/welcomePage/placehold/550x550_4.png"}}" alt="" /></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div><!-- Close tab -->
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="row">
                <div class="col-xs-6 up-row">
                    <div class="img-banner"><a href="http://www.orami.co.th/deal/fashionable-deals/rainy-collection.html" target="_self"><img title="When it rains" src="{{media url="wysiwyg/09-Welcome-Page-Vertical-banner-When-It-rains-110716.jpg"}}" alt="When it rains" /></a></div>
                </div>
                <div class="col-xs-6 up-row">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="img-banner"><a href="http://www.orami.co.th/home-appliances/brands/brother.html" target="_self"><img title="Home Appliance" src="{{media url="wysiwyg/05-Welcome-Page-Square-banner-Home-App-SEWSAW-110716.jpg"}}" alt="Home Appliance" /></a></div>
                        </div>
                        <div class="col-sm-12 item-last">
                            <div class="img-banner"><a href="http://www.orami.co.th/th/deal/beautiful-deals/skincare-bible.html" target="_self"><img title="Get It Beauty" src="{{media url="wysiwyg/beauty-welcome-page-banner-GET-YOUR-BEAUTY.jpg"}}" alt="Get It Beauty" /></a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="row">
                <div class="col-xs-6">
                    <div class="img-banner"><a href="http://www.orami.co.th/pets/brands/ciao.html"><img title="CIAO" src="{{media url="wysiwyg/07-Welcome-Page-Square-banner-ciao-110716.jpg"}}" alt="CIAO" /></a></div>
                </div>
                <div class="col-xs-6">
                    <div class="img-banner"><a href="http://www.orami.co.th/deal/for-your-home-deals/bedding-flash-sales.html"><img title="Home Bedding" src="{{media url="wysiwyg/08-welcome-page-Square_banner_Home_Decor-SLEEPING-110716.jpg"}}" alt="Home Bedding" /></a></div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6">
                    <div class="img-banner"><a href="http://www.orami.co.th/health/brands/hi-balanz.html"><img title="Hi Balanz" src="{{media url="wysiwyg/02-Welcome-Page-2-small-banner-hi-balanz-1107162.jpg"}}" alt="Hi Balanz" /></a></div>
                </div>
                <div class="col-xs-6">
                    <div class="img-banner"><a href="http://www.orami.co.th/th/deal/mom-kids-deals.html"><img title="Babylove" src="{{media url="wysiwyg/babylovecertainty30_july_270.jpg"}}" alt="Babylove" /></a></div>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="row">
                <div class="col-sm-12">
                    <div class="img-banner"><a href="http://www.orami.co.th/deal/for-your-home-deals/music-lovers.html"><img title="Music Lover Clearance" src="{{media url="wysiwyg/01-Welcome-Page-Hero-banner-Electronics-musiclife-110716.jpg"}}" alt="Music Lover Clearance" /></a></div>
                </div>
            </div>
        </div>
    </div>
</div>
EOD;
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================


//==========================================================================
// Welcome splash shortcut link (Thai)
//==========================================================================
$blockTitle = "Welcome splash shortcut link";
$blockIdentifier = "welcome-shortcut";
$blockStores = array($moxyth);
$blockIsActive = 1;
$blockContent = <<<EOD
<div class="grid-wall">
    <div class="row">
        <div class="col-sm-6">
            <div class="row">
                <div class="col-sm-12">
                    <div class="img-banner">
                        <div id="homepage_first_slider" class="homepage-main" style="float: left; width: 100%;"><!-- Open tab -->
                            <div id="container" style="width: 100%;">
                                <div id="slideshow" style="height: 100%; width: 100%; overflow: hidden;">
                                    <ul id="slide-nav" style="display: none; visibility: hidden;">
                                        <li id="prev"><a class="control-s pre-s" href="#">Previous</a></li>
                                        <li id="next"><a class="control-s nxt-s" href="#">Next</a></li>
                                    </ul>
                                    <ul id="home-slides" style="position: relative; width: 100%; height: 100%;">
                                        <li style="position: absolute; top: 0; left: 0; display: none; z-index: 1; opacity: 1;"><a style="width: 100%;" href="#"><img src="{{media url="wysiwyg/welcomePage/placehold/550x550_1.png"}}" alt="" /></a></li>
                                        <li style="position: absolute; top: 0; left: 0; display: none; z-index: 1; opacity: 1;"><a style="width: 100%;" href="#"><img src="{{media url="wysiwyg/welcomePage/placehold/550x550_2.png"}}" alt="" /></a></li>
                                        <li style="position: absolute; top: 0; left: 0; display: none; z-index: 1; opacity: 1;"><a style="width: 100%;" href="#"><img src="{{media url="wysiwyg/welcomePage/placehold/550x550_3.png"}}" alt="" /></a></li>
                                        <li style="position: absolute; top: 0; left: 0; display: none; z-index: 1; opacity: 1;"><a style="width: 100%;" href="#"><img src="{{media url="wysiwyg/welcomePage/placehold/550x550_4.png"}}" alt="" /></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div><!-- Close tab -->
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="row">
                <div class="col-xs-6 up-row">
                    <div class="img-banner"><a href="http://www.orami.co.th/deal/fashionable-deals/rainy-collection.html" target="_self"><img title="When it rains" src="{{media url="wysiwyg/09-Welcome-Page-Vertical-banner-When-It-rains-110716.jpg"}}" alt="When it rains" /></a></div>
                </div>
                <div class="col-xs-6 up-row">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="img-banner"><a href="http://www.orami.co.th/home-appliances/brands/brother.html" target="_self"><img title="Home Appliance" src="{{media url="wysiwyg/05-Welcome-Page-Square-banner-Home-App-SEWSAW-110716.jpg"}}" alt="Home Appliance" /></a></div>
                        </div>
                        <div class="col-sm-12 item-last">
                            <div class="img-banner"><a href="http://www.orami.co.th/th/deal/beautiful-deals/skincare-bible.html" target="_self"><img title="Get It Beauty" src="{{media url="wysiwyg/beauty-welcome-page-banner-GET-YOUR-BEAUTY.jpg"}}" alt="Get It Beauty" /></a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="row">
                <div class="col-xs-6">
                    <div class="img-banner"><a href="http://www.orami.co.th/pets/brands/ciao.html"><img title="CIAO" src="{{media url="wysiwyg/07-Welcome-Page-Square-banner-ciao-110716.jpg"}}" alt="CIAO" /></a></div>
                </div>
                <div class="col-xs-6">
                    <div class="img-banner"><a href="http://www.orami.co.th/deal/for-your-home-deals/bedding-flash-sales.html"><img title="Home Bedding" src="{{media url="wysiwyg/08-welcome-page-Square_banner_Home_Decor-SLEEPING-110716.jpg"}}" alt="Home Bedding" /></a></div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6">
                    <div class="img-banner"><a href="http://www.orami.co.th/health/brands/hi-balanz.html"><img title="Hi Balanz" src="{{media url="wysiwyg/02-Welcome-Page-2-small-banner-hi-balanz-1107162.jpg"}}" alt="Hi Balanz" /></a></div>
                </div>
                <div class="col-xs-6">
                    <div class="img-banner"><a href="http://www.orami.co.th/th/deal/mom-kids-deals.html"><img title="Babylove" src="{{media url="wysiwyg/babylovecertainty30_july_270.jpg"}}" alt="Babylove" /></a></div>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="row">
                <div class="col-sm-12">
                    <div class="img-banner"><a href="http://www.orami.co.th/deal/for-your-home-deals/music-lovers.html"><img title="Music Lover Clearance" src="{{media url="wysiwyg/01-Welcome-Page-Hero-banner-Electronics-musiclife-110716.jpg"}}" alt="Music Lover Clearance" /></a></div>
                </div>
            </div>
        </div>
    </div>
</div>
EOD;
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================


$installer->endSetup();