<?php

/** @var Mage_Core_Model_Resource_Setup $installer */
$installer = $this;

$installer->startSetup();


$moxyen = Mage::getModel('core/store')->load('moxyen', 'code')->getId();
$moxyth = Mage::getModel('core/store')->load('moxyth', 'code')->getId();

//==========================================================================
// Welcome Mega Banner Home (EN)
//==========================================================================
$blockTitle = "Welcome Mega Banner Home En";
$blockIdentifier = "welcome-mega-banner-home";
$blockStores = array($moxyen);
$blockIsActive = 1;
$blockContent = <<<EOD
<div class="welcome-mega-banner-home">
    <a href="#"><img src="{{media url='wysiwyg/welcomePage/945x400.png'}}" alt="" /></a>
</div>
EOD;
/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================


//==========================================================================
// Welcome Mega Banner Home (TH)
//==========================================================================
$blockTitle = "Welcome Mega Banner Home TH";
$blockIdentifier = "welcome-mega-banner-home";
$blockStores = array($moxyth);
$blockIsActive = 1;
$blockContent = <<<EOD
<div class="welcome-mega-banner-home">
    <a href="#"><img src="{{media url='wysiwyg/welcomePage/945x400.png'}}" alt="" /></a>
</div>
EOD;
/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================


//==========================================================================
// Cms page: Home EN
//==========================================================================
$title = 'ORAMI by Moxy The Online Shopping Destination for Women';
$identifier = "home";
$stores = array($moxyen);
$status = 1;
$content = <<<EOD
{{block type="core/template" name="subscription" as="subscription" template="subscription/subscribe.phtml"}}
EOD;
$rootTemplate = "one_column";
$layoutXML = <<<EOD
<reference name="head">
    <block type="core/text" name="homepage.metadata" after="-">
        <action method="addText"><text><![CDATA[<meta name="google-site-verification" content="IDwIJ1YbPmxWd60Ej1zWtvkggB95TZAyX5lIPOFn5s4"/>]]></text></action>
    </block>
</reference>

<reference name="content">
    <block type="cms/block" name="welcome.mega.banner.home">
        <action method="setBlockId"><block_id>welcome-mega-banner-home</block_id></action>
    </block>

    <block type="cms/block" name="home.sliders">
        <action method="setBlockId"><block_id>welcome-shortcut</block_id></action>
    </block>

    <block type="petloftcatalog/product_homegrid" name="home.product.grid1" as="homegrid1" template="petloft/catalog/product/homegrid.phtml">
        <action method="setCategoryId"><id>2655</id></action>
    </block>

    <block type="cms/block" name="home.promotion.banner">
        <action method="setBlockId"><block_id>welcome-promotion-banner</block_id></action>
    </block>

    <block type="petloftcatalog/product_homegrid" name="home.product.grid2" as="homegrid2" template="petloft/catalog/product/homegrid.phtml">
        <action method="setCategoryId"><id>2656</id></action>
    </block>

    <block type="cms/block" name="welcome.content.banner">
        <action method="setBlockId"><block_id>welcome-content-banner</block_id></action>
    </block>

    <block type="cms/block" name="welcome.bottom.banner">
        <action method="setBlockId"><block_id>welcome-bottom-banner</block_id></action>
    </block>

    <block type="cms/block" name="welcome.brand.logo">
        <action method="setBlockId"><block_id>welcome-brand-logo</block_id></action>
    </block>
</reference>
EOD;
$metaDesc = 'Beauty, Fashion, Home Decor, Gadgets & Electronics, Health & Sports, Mom & Babies, Pet Products - shipped directly to you!';

/** @var Mage_Cms_Model_Page $page */
$page = Mage::getModel('cms/page')->getCollection()
    ->addStoreFilter($stores, $withAdmin = false)
    ->addFieldToFilter('identifier', $identifier)
    ->getFirstItem();
if($page->getId()) $page->delete();

$page = Mage::getModel('cms/page');
$page->setTitle($title);
$page->setIdentifier($identifier);
$page->setIsActive($status);
$page->setContent($content);
$page->setRootTemplate($rootTemplate);
$page->setLayoutUpdateXml($layoutXML);
$page->setMetaDescription($metaDesc);
$page->setStores($stores);
$page->save();
//==========================================================================
//==========================================================================
//==========================================================================


//==========================================================================
// Cms page:Home TH
//==========================================================================
$title = 'ORAMI by Moxy เว็บช้อปปิ้งออนไลน์ สำหรับสาวนักช้อปทุกคน';
$identifier = "home";
$stores = array($moxyth);
$status = 1;
$content = <<<EOD
{{block type="core/template" name="subscription" as="subscription" template="subscription/subscribe.phtml"}}
EOD;
$rootTemplate = "one_column";
$layoutXML = <<<EOD
<reference name="head">
    <block type="core/text" name="homepage.metadata" after="-">
        <action method="addText"><text><![CDATA[<meta name="google-site-verification" content="IDwIJ1YbPmxWd60Ej1zWtvkggB95TZAyX5lIPOFn5s4"/>]]></text></action>
    </block>
</reference>

<reference name="content">
    <block type="cms/block" name="welcome.mega.banner.home">
        <action method="setBlockId"><block_id>welcome-mega-banner-home</block_id></action>
    </block>

    <block type="cms/block" name="home.sliders">
        <action method="setBlockId"><block_id>welcome-shortcut</block_id></action>
    </block>

    <block type="petloftcatalog/product_homegrid" name="home.product.grid1" as="homegrid1" template="petloft/catalog/product/homegrid.phtml">
        <action method="setCategoryId"><id>2655</id></action>
    </block>

    <block type="cms/block" name="home.promotion.banner">
        <action method="setBlockId"><block_id>welcome-promotion-banner</block_id></action>
    </block>

    <block type="petloftcatalog/product_homegrid" name="home.product.grid2" as="homegrid1" template="petloft/catalog/product/homegrid.phtml">
        <action method="setCategoryId"><id>2656</id></action>
    </block>

    <block type="cms/block" name="welcome.content.banner">
        <action method="setBlockId"><block_id>welcome-content-banner</block_id></action>
    </block>

    <block type="cms/block" name="welcome.bottom.banner">
        <action method="setBlockId"><block_id>welcome-bottom-banner</block_id></action>
    </block>

    <block type="cms/block" name="welcome.brand.logo">
        <action method="setBlockId"><block_id>welcome-brand-logo</block_id></action>
    </block>
</reference>
EOD;
$metaDesc = 'เครื่องสำอาง แฟชั่น ของแต่งบ้าน แก็ตเจ็ต อีเล็คทรอนิกส์ สินค้าเพื่อสุขภาพ สินค้าสำหรับแม่และเด็ก และสินค้าสำหรับสัตว์เลี้ยง พร้อมส่งตรงถึงหน้าบ้านคุณ!';

/** @var Mage_Cms_Model_Page $page */
$page = Mage::getModel('cms/page')->getCollection()
    ->addStoreFilter($stores, $withAdmin = false)
    ->addFieldToFilter('identifier', $identifier)
    ->getFirstItem();
if($page->getId()) $page->delete();

$page = Mage::getModel('cms/page');
$page->setTitle($title);
$page->setIdentifier($identifier);
$page->setIsActive($status);
$page->setContent($content);
$page->setRootTemplate($rootTemplate);
$page->setLayoutUpdateXml($layoutXML);
$page->setMetaDescription($metaDesc);
$page->setStores($stores);
$page->save();
//==========================================================================
//==========================================================================
//==========================================================================

$installer->endSetup();