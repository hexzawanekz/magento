<?php

class SM_Transactionemails_Block_Adminhtml_Satisfaction extends Mage_Adminhtml_Block_Widget_Grid_Container{
    protected $_blockGroup = 'sm_transactionemails';

    public function __construct(){
        $this->_controller = 'adminhtml_satisfaction';
        $this->_blockGroup = 'sm_transactionemails';
        $this->_headerText = Mage::helper('sm_transactionemails')->__('Newsletter Satisfaction');

        parent::__construct();

        $this->_removeButton('add');
    }
}