<?php

class SM_Transactionemails_Adminhtml_SatisfactionController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction() {
        $this->loadLayout()
            ->_setActiveMenu('newsletter/satisfaction')
            ->_addBreadcrumb(Mage::helper('adminhtml')->__('Newsletter/Newsletter Satisfaction'), Mage::helper('adminhtml')->__('Newsletter/Newsletter Satisfaction'));

        return $this;
    }

    public function indexAction() {
        $this->_initAction()
            ->renderLayout();
    }

    public function exportCsvAction()
    {
        $fileName   = 'Facebook-User-'.date('d-m-Y').'.csv';
        $content    = $this->getLayout()->createBlock('sm_transactionemails/adminhtml_satisfaction_grid')
            ->getCsv();

        $this->_sendUploadResponse($fileName, $content);
    }

    public function exportXmlAction()
    {
        $fileName   = 'Facebook-User-'.date('d-m-Y').'.xml';
        $content    = $this->getLayout()->createBlock('sm_transactionemails/adminhtml_satisfaction_grid')
            ->getXml();

        $this->_sendUploadResponse($fileName, $content);
    }

    protected function _sendUploadResponse($fileName, $content, $contentType='application/octet-stream')
    {
        $response = $this->getResponse();
        $response->setHeader('HTTP/1.1 200 OK','');
        $response->setHeader('Pragma', 'public', true);
        $response->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true);
        $response->setHeader('Content-Disposition', 'attachment; filename='.$fileName);
        $response->setHeader('Last-Modified', date('r'));
        $response->setHeader('Accept-Ranges', 'bytes');
        $response->setHeader('Content-Length', strlen($content));
        $response->setHeader('Content-type', $contentType);
        $response->setBody($content);
        $response->sendResponse();
        die;
    }
}
