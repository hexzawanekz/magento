<?php
/**
 * Created by PhpStorm.
 * User: smartosc
 * Date: 3/11/2016
 * Time: 4:30 PM
 */

class SM_Transactionemails_Model_Queue extends Mage_Core_Model_Abstract{
    protected function _construct()
    {
        $this->_init('sm_transactionemails/queue');
    }
}