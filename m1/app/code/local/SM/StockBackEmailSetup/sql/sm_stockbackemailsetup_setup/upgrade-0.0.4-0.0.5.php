<?php
/**
 * Smartosc Ha Noi
 * ductm1@smartosc.com
 * Back in stock (LAFEMA ENGLISH)
 */

$template = Mage::getModel('core/email_template');

$template_text = <<<HTML
<div style="width:650px;" >
    <h1 style="font-size:150%;text-align:center">{{var product}}</h1>
    <a href="{{var product_url}}?utm_source=edm&utm_medium=reminder&utm_campaign=lafema_outofstock_reminder" target="_blank"><img style="width:650px; border:none;" src="{{skin url="images/backinstock/lf_header.png"}}" alt="lafema-email-header" /></a>
    <div style="display: block;float: left;width: 100%;padding: 0 11px;" >
        <p style="font-size:110%">Dear {{var customer}},</h3>
        <p>On {{var date}} you asked us to email you when {{var product}} is back in stock.</p>
        <p>Lafema is pleased to inform you that it has arrived!</p>

        <p>Get your hands on it now while stocks last.</p>
    </div>

    <div style=" float: left; clear: both; padding: 10px; width: 618px;min-height: 200px;">
        <div style="width: 195px; text-align: center;float: left;" ><a href="{{var product_url}}?utm_source=edm&utm_medium=reminder&utm_campaign=lafema_outofstock_reminder" target="_blank"><img height="195" src="{{var product_image}}" alt="{{var product}}" style="width: auto; height: 195px; float: left;" /></a></div>
        <div style="width: 260px; text-align: center; margin-top:60px; float: left;min-height:140px;">{{var product}}</div>
        <div style="background: transparent none repeat scroll 0% 0%; float: left; min-height: 180px; font-size: 20px; text-align: center; font-family: tahoma; width: 138px; color: rgb(73, 69, 70); border-left: 1px solid rgb(204, 204, 204); padding: 10px 0px 10px 17px;">
            AVAILABLE FOR A LIMITED TIME ONLY!

            <div style="width:120px;padding:10px; background: #494546; margin-top:28px;"><a href="{{var product_url}}?utm_source=edm&utm_medium=reminder&utm_campaign=lafema_outofstock_reminder" style="color: #fff; text-decoration: none">BUY NOW</a></div>
        </div>
    </div>
    <div style="clear:both;"></div>
    <a href="{{var product_url}}?utm_source=edm&utm_medium=reminder&utm_campaign=lafema_outofstock_reminder" target="_blank"><img style="width:650px; border:none;" src="{{skin url="images/backinstock/lf_footer.png"}}" alt="lafema-email-footer" /></a>
</div>
HTML;

/** @var Mage_Core_Model_Email_Template $model */
$model = Mage::getModel('core/email_template');
$model->loadByCode('Back in stock (LAFEMA ENGLISH)')
    ->setTemplateCode('Back in stock (LAFEMA ENGLISH)')
    ->setTemplateText($template_text)
    ->setTemplateSubject('{{var product}} is back in stock!')
    ->setTemplateType(Mage_Core_Model_Email_Template::TYPE_HTML);

try {
    $model->save();
} catch (Mage_Exception $e) {
    throw $e;
}