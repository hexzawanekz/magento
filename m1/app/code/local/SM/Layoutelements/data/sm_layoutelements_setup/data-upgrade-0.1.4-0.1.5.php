<?php

$coreConfigModel = Mage::getModel('core/config');

$coreConfigModel->saveConfig('customer/address/dob_show', 'req');
$coreConfigModel->saveConfig('customer/address/gender_show', 'req');
$coreConfigModel->saveConfig('onestepcheckout/onestepconfig/active', 0);
$coreConfigModel->saveConfig('checkout/options/customer_must_be_logged', 0);