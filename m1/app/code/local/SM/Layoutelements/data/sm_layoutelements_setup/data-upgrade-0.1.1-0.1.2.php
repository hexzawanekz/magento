<?php


// init Magento
Mage::app();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
$config = new Mage_Core_Model_Config();
// get store ids
$en     = Mage::getModel('core/store')->load('en', 'code')->getId();// Petloft
$th     = Mage::getModel('core/store')->load('th', 'code')->getId();
$len    = Mage::getModel('core/store')->load('len', 'code')->getId();// Lafema
$lth    = Mage::getModel('core/store')->load('lth', 'code')->getId();
$ven    = Mage::getModel('core/store')->load('ven', 'code')->getId();// Venbi
$vth    = Mage::getModel('core/store')->load('vth', 'code')->getId();
$sen    = Mage::getModel('core/store')->load('sen', 'code')->getId();// Sanoga
$sth    = Mage::getModel('core/store')->load('sth', 'code')->getId();
$moxyen = Mage::getModel('core/store')->load('moxyen', 'code')->getId();// Moxy
$moxyth = Mage::getModel('core/store')->load('moxyth', 'code')->getId();


$template = Mage::getModel('core/email_template');

//==========================================================================
// CART REMINDER PETLOFT
//==========================================================================

$template_text =
    <<<HTML
<meta charset="utf-8"/>
<div class="mail-container" style="width:600px; padding:0px 33px; font-family:  'Trebuchet MS', Tahoma; font-size: 14px; color: #3c3d41; margin: 0px;">
	<div class="mail-header" style="width:95%; min-height: 110px; max-height: 110px; position: relative; margin:0px auto;">
		<div class="mail-logo" style="width:220px; min-height: 110px; max-height: 110px; margin:0px auto;">
			<a href="http://petloft.com/"><img src="{{skin _area="frontend"  url="images/email/petloft-logo.png"}}" style="width:220px; max-height: 110px; min-height: 110px;" /></a>
		</div>

		<div class="english-below" style="min-height: 16px; max-height: 16px; position: absolute; bottom: 0px; line-height: 16px;">
			<span><img src="{{skin _area="frontend"  url="images/email/flag-en.png"}}" height="16px;"/> Scroll down for English version</span>
		</div>
	</div>

	<img style="width: 100%; margin-top:40px;" src="{{skin _area="frontend"  url="images/email/feel_like_shopping_today_thai.png"}}" alt="Thank for your order" />

	<div class="mail-body" style="margin:50px auto; width: 95%;">
		<div class="left-main-body" style="float: left; width: 57%; padding: 0px 5px; border-right: 1px solid #e7e7e7;">
			<p>เรียน คุณ {{var customer_name}},</p>
			<br/>

			<p>มีสินค้าบางอย่างหลงเหลืออยู่ในตะกร้าสินค้าของคุณ</p>
			<p>คุณต้องการจะสั่งซื้อวันนี้ไหม ?</p>
			<br>

			<p>คุณสามารถสั่งซื้อตอนนี้และชำระเงินในภายหลัง เพื่อความสะดวกสบายที่ยิ่งขึ้น</p>
			<p>เลือกดูวิธีการชำระเงินอื่นๆได้<a href="http://www.petloft.com/th/help/#payments" style="color: #3c3d41;">ที่น</a></p>
			<br>

			<p>Have a beautiful day!</p>
			<br>

			<p>Warm Regards,</p>
			<p>Moxy Team</p>
		</div>

		<div class="right-main-body" style="width: 38%; float: right; padding:0px 5px; text-align: center;">
		    {{var products}}

            <a href="{{store url="checkout/cart/"}}"
               style="display: block; width: 50%; color: #fff; text-decoration: none; margin:10px auto; background: #0e80c0;padding: 10px;clear:both;">สั่งซื้อ</a>
            <p>สั่งซื้อตอนนี้ก่อนที่ของจะหมด</p>
		</div>

		<div style="clear: both;"></div>
	</div>



	<div class="navigation-th" style="margin-top: 24px; overflow:hidden;">
		<div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #006ab4; border-bottom: 2px solid #006ab4; padding: 0px 17px; overflow: hidden;">
			<span class="nav-item" style="border-right: 2px solid #006ab4; line-height: 33px; font-size: 18px; padding: 0px 28px;"><a href="http://petloft.com/th/dog-food.html" style="color: #3c3d41; text-decoration: none;">สุนัข</a></span>
			<span class="nav-item" style="border-right: 2px solid #006ab4; line-height: 33px; font-size: 18px; padding: 0px 28px;"><a href="http://petloft.com/th/cat-food.html" style="color: #3c3d41; text-decoration: none;">แมว</a></span>
			<span class="nav-item" style="border-right: 2px solid #006ab4; line-height: 33px; font-size: 18px; padding: 0px 28px;"><a href="http://petloft.com/th/fish-products.html" style="color: #3c3d41; text-decoration: none;">ปลา</a></span>
			<span class="nav-item" style="border-right: 2px solid #006ab4; line-height: 33px; font-size: 18px; padding: 0px 28px;"><a href="http://www.petloft.com/th/bird.html" style="color: #3c3d41; text-decoration: none;">นก</a></span>
			<span class="nav-item" style="line-height: 33px; font-size: 18px; padding: 0px 28px;"><a href="http://petloft.com/th/small-pets-products.html" style="color: #3c3d41; text-decoration: none;">สัตว์เลี้ยงขนาดเล็ก</a></span>
		</div>
	</div>

    <div class="email-footer" style="background: #bbb; margin-top: 8px; color: #fff; padding: 0px 15px;">
        <div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
            <p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px"><img src="{{skin _area="frontend"  url="images/email/mailbox.png"}}" style="width:18px;"/> ติดต่อเรา</p>
            <p style="font-size: 10px; margin: 0;">02-106-8222</p>
            <p style="margin: 0; font-size: 10px;">support@petloft.com</p>
        </div>

        <div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
            <p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">ติดตามที่</p>
            <div class="follow-icons" style="width: 100%">
    <a href="https://www.facebook.com/petloftthai"><img src="{{skin _area="frontend"  url="images/email/facebook_follow.png"}}" style="width: 25px;" alt="Facebook fanpage"></a>
                <a href="https://twitter.com/PetLoft"><img src="{{skin _area="frontend"  url="images/email/twitter_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
                <a href="https://instagram.com/petloft"><img src="{{skin _area="frontend"  url="images/email/instagram_follow.png"}}" style="width: 25px;" alt="Instagram fanpage"></a>
                <a href="https://www.pinterest.com/petloft/"><img src="{{skin _area="frontend"  url="images/email/pinterest_follow.png"}}" style="width: 25px;" alt="Pinterest fanpage"></a>
                <a href="https://plus.google.com/110158894140498579470"><img src="{{skin _area="frontend"  url="images/email/google_follow.png"}}" style="width: 25px;" alt="Google fanpage"></a>
            </div>
        </div>

        <div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
            <p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">ช่องทางการชำระเงิน</p>
            <img src="{{skin _area="frontend"  url="images/email/payment-th.png"}}" alt="ช่องทางการชำระเงิน" style="width: 109px;">
        </div>

        <div class="clear" style="clear:both;"></div>
    </div>

    <div class="footer" style="padding: 0px 33px; margin-top: 18px;">
        <div class="copyr" style="float: left; font-size: 12px;">&copy;2015 Whatsnew Co.,Ltd. All Rights Reserved</div>
        <div class="links" style="float: right; font-size: 12px;">
            <a href="http://www.petloft.com/th/about-us/" style="color: #3c3d41;"><span>เกี่ยวกับเรา</span></a> |
            <a href="http://www.petloft.com/th/terms-and-conditions/" style="color: #3c3d41;"><span>ข้อกำหนดและเงื่อนไข</span></a> |
            <a href="http://www.petloft.com/th/privacy-policy/" style="color: #3c3d41;"><span>ความเป็นส่วนตัว</span></a>
        </div>
    </div>
	<div style="clear:both"></div>
	<hr style="margin: 40px auto; width: 95%;">

	<!--English Version-->
	<img style="width: 100%; margin-top:0px;" src="{{skin _area="frontend"  url="images/email/feel_like_shopping_today_en.png"}}" alt="Feel like shopping today ?" />

	<div class="mail-body" style="margin:50px auto; width: 95%;">
		<div class="left-main-body" style="float: left; width: 57%; padding: 0px 5px; border-right: 1px solid #e7e7e7;">
			<p>Dear {{var customer_name}},</p>

			<br/>
			<p>Looks like you have something left in your shopping cart. Do you feel like purchasing it today? </p>
			<br>

			<p>For your convenience, You can shop now and pay later with cash-on-delivery. </p>
			<p>Check our conditions of payment <a href="http://www.petloft.com/en/help/#payments" style="color: #3c3d41;">here</a>.</p>
			<br>

			<p>Have a beautiful day!</p>
			<br>

			<p>Warm Regards,</p>
			<p>Moxy Team</p>
		</div>
		<div class="right-main-body" style="width: 38%; float: right; padding:0px 5px; text-align: center;">
			 {{var products}}
   <a href="{{store url="checkout/cart/"}}"
               style="display: block; width: 50%; color: #fff; text-decoration: none; margin:10px auto; background: #0e80c0;padding: 10px;clear:both;">BUY IT NOW</a>
            <p>Buy now before stock last</p>
		</div>

		<div style="clear: both;"></div>
	</div>



 	<div class="navigation-th" style="margin-top: 24px; overflow:hidden;">
		<div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #006ab4; border-bottom: 2px solid #006ab4; padding: 0px 30px; overflow: hidden;">
			<span class="nav-item" style="border-right: 2px solid #006ab4; line-height: 33px; font-size: 18px; padding: 0px 29px;"><a href="http://petloft.com/en/dog-food.html" style="color: #3c3d41; text-decoration: none;">Dogs</a></span>
			<span class="nav-item" style="border-right: 2px solid #006ab4; line-height: 33px; font-size: 18px; padding: 0px 29px;"><a href="http://petloft.com/en/cat-food.html" style="color: #3c3d41; text-decoration: none;">Cats</a></span>
			<span class="nav-item" style="border-right: 2px solid #006ab4; line-height: 33px; font-size: 18px; padding: 0px 29px;"><a href="http://petloft.com/en/fish-products.html" style="color: #3c3d41; text-decoration: none;">Fish</a></span>
			<span class="nav-item" style="border-right: 2px solid #006ab4; line-height: 33px; font-size: 18px; padding: 0px 29px;"><a href="http://www.petloft.com/en/bird.html" style="color: #3c3d41; text-decoration: none;">Bird</a></span>
			<span class="nav-item" style="line-height: 33px; font-size: 18px; padding: 0px 29px;"><a href="http://petloft.com/en/small-pets-products.html" style="color: #3c3d41; text-decoration: none;">Small Pets</a></span>
		</div>
	</div>

    <div class="email-footer" style="background: #bbb; margin-top: 8px; color: #fff; padding: 0px 15px;">
        <div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
            <p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px"><img src="{{skin _area="frontend"  url="images/email/mailbox.png"}}" style="width:18px;"/> Contact Us</p>
            <p style="font-size: 10px; margin: 0;">02-106-8222</p>
            <p style="margin: 0; font-size: 10px;">support@petloft.com</p>
        </div>

        <div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
            <p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">Follow Us</p>
            <div class="follow-icons" style="width: 100%">
    <a href="https://www.facebook.com/petloftthai"><img src="{{skin _area="frontend"  url="images/email/facebook_follow.png"}}" style="width: 25px;" alt="Facebook fanpage"></a>
                <a href="https://twitter.com/PetLoft"><img src="{{skin _area="frontend"  url="images/email/twitter_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
                <a href="https://instagram.com/petloft"><img src="{{skin _area="frontend"  url="images/email/instagram_follow.png"}}" style="width: 25px;" alt="Instagram fanpage"></a>
                <a href="https://www.pinterest.com/petloft/"><img src="{{skin _area="frontend"  url="images/email/pinterest_follow.png"}}" style="width: 25px;" alt="Pinterest fanpage"></a>
                <a href="https://plus.google.com/110158894140498579470"><img src="{{skin _area="frontend"  url="images/email/google_follow.png"}}" style="width: 25px;" alt="Google fanpage"></a>
            </div>
        </div>

        <div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
            <p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">Payment Options</p>
            <img src="{{skin _area="frontend"  url="images/email/payment.png"}}" alt="Payment Options" style="width: 109px;">
        </div>

        <div class="clear" style="clear:both;"></div>
    </div>

    <div class="footer" style="padding: 0px 33px; margin-top: 18px;">
        <div class="copyr" style="float: left; font-size: 12px;">&copy;2015 Whatsnew Co.,Ltd. All Rights Reserved</div>
        <div class="links" style="float: right; font-size: 12px;">
            <a href="http://www.petloft.com/en/about-us/" style="color: #3c3d41;"><span>About Us</span></a> |
            <a href="http://www.petloft.com/en/terms-and-conditions/" style="color: #3c3d41;"><span>Term & Conditions</span></a> |
            <a href="http://www.petloft.com/en/privacy-policy/" style="color: #3c3d41;"><span>Privacy Policy</span></a>
        </div>
    </div>
	<div style="clear:both"></div>
</div>
HTML;

/** @var Mage_Core_Model_Email_Template $model */
$model = Mage::getModel('core/email_template');
$model->loadByCode('Cart Reminder Petloft')
    ->setTemplateCode('Cart Reminder Petloft')
    ->setTemplateText($template_text)
    ->setTemplateSubject('Don\'t miss out on these great offers now! Here are the items that you hand picked before. Save time and buy them now!')
    ->setTemplateType(Mage_Core_Model_Email_Template::TYPE_HTML);

try {
    $model->save();
    $config->saveConfig('catalog/adjcartalert/template', $model->getId() , 'stores', $en);
    $config->saveConfig('catalog/adjcartalert/template', $model->getId() , 'stores', $th);
    $config->saveConfig('catalog/adjcartalert/template2', $model->getId() , 'stores', $en);
    $config->saveConfig('catalog/adjcartalert/template2', $model->getId() , 'stores', $th);
    $config->saveConfig('catalog/adjcartalert/template3', $model->getId() , 'stores', $en);
    $config->saveConfig('catalog/adjcartalert/template3', $model->getId() , 'stores', $th);
} catch (Mage_Exception $e) {
    throw $e;
}
//==========================================================================
//==========================================================================

//==========================================================================
// CART REMINDER MOXY
//==========================================================================

$template_text =
    <<<HTML
<meta charset="utf-8"/>
<div class="mail-container" style="width:600px; padding:0px 33px; font-family:  'Trebuchet MS', Tahoma; font-size: 14px; color: #3c3d41; margin: 0px;">
	<div class="mail-header" style="width:95%; min-height: 110px; max-height: 110px; position: relative; margin:0px auto;">
		<div class="mail-logo" style="width:220px; min-height: 110px; max-height: 110px; margin:0px auto;">
			<a href="http://moxy.co.th/"><img src="{{skin _area="frontend"  url="images/email/moxy-logo.png"}}" style="width:220px; max-height: 110px; min-height: 110px;" /></a>
		</div>

		<div class="english-below" style="min-height: 16px; max-height: 16px; position: absolute; bottom: 0px; line-height: 16px;">
			<span><img src="{{skin _area="frontend"  url="images/email/flag-en.png"}}" height="16px;"/> Scroll down for English version</span>
		</div>
	</div>

	<img style="width: 100%; margin-top:40px;" src="{{skin _area="frontend"  url="images/email/feel_like_shopping_today_thai.png"}}" alt="Thank for your order" />

	<div class="mail-body" style="margin:50px auto; width: 95%;">
		<div class="left-main-body" style="float: left; width: 57%; padding: 0px 5px; border-right: 1px solid #e7e7e7;">
			<p>เรียน คุณ {{var customer_name}},</p>
			<br/>

			<p>มีสินค้าบางอย่างหลงเหลืออยู่ในตะกร้าสินค้าของคุณ</p>
			<p>คุณต้องการจะสั่งซื้อวันนี้ไหม ?</p>
			<br>

			<p>คุณสามารถสั่งซื้อตอนนี้และชำระเงินในภายหลัง เพื่อความสะดวกสบายที่ยิ่งขึ้น</p>
			<p>เลือกดูวิธีการชำระเงินอื่นๆได้<a href="http://www.moxy.co.th/th/help/#payments" style="color: #3c3d41;">ที่น</a></p>
			<br>

			<p>Have a beautiful day!</p>
			<br>

			<p>Warm Regards,</p>
			<p>Moxy Team</p>
		</div>

		<div class="right-main-body" style="width: 38%; float: right; padding:0px 5px; text-align: center;">
			                {{var products}}
                    <a href="{{store url="checkout/cart/"}}" style="display: block; width: 50%; color: #fff; text-decoration: none; margin:10px auto; background: #ed5192;padding: 10px;clear:both;">สั่งซื้อ</a>
                    <p>สั่งซื้อตอนนี้ก่อนที่ของจะหมด</p>
		</div>

		<div style="clear: both;"></div>
	</div>



 <div class="navigation-th" style="margin-top: 24px; overflow: hidden; text-align: center;">
		<div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #ee5191; border-bottom: 2px solid #ee5191; padding: 0px 11px;">
			<span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 17; padding: 0px 4px;"><a href="http://moxy.co.th/th/moxy/fashion" style="color: #3c3d41; text-decoration: none;">แฟชั่น</a></span>
			<span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 17; padding: 0px 4px;"><a href="http://moxy.co.th/th/moxy/homeliving" style="color: #3c3d41; text-decoration: none;">ของแต่งบ้าน</a></span>
			<span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 17; padding: 0px 4px;"><a href="http://moxy.co.th/th/moxy/electronics/" style="color: #3c3d41; text-decoration: none;">อิเล็กทรอนิกส์</a></span>
			<span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 17; padding: 0px 4px;"><a href="http://lafema.com/th/" style="color: #3c3d41; text-decoration: none;">ความงาม</a></span>
			<span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 17; padding: 0px 4px;"><a href="http://venbi.com/th/" style="color: #3c3d41; text-decoration: none;">แม่และเด็ก</a></span>
			<span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 17; padding: 0px 4px;"><a href="http://sanoga.com/th/" style="color: #3c3d41; text-decoration: none;">สุขภาพ</a></span>
			<span class="nav-item" style="line-height: 33px; font-size: 17; padding: 0px 4px;"><a href="http://petloft.com/th/" style="color: #3c3d41; text-decoration: none;">สัตว์เลี้ยง</a></span>
		</div>
	</div>

    <div class="email-footer" style="background: #bbb; margin-top: 8px; color: #fff; padding: 0px 15px;">
        <div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
            <p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px"><img src="{{skin _area="frontend"  url="images/email/mailbox.png"}}" style="width:18px;"/> ติดต่อเรา</p>
            <p style="font-size: 10px; margin: 0;">02-106-8222</p>
            <p style="margin: 0; font-size: 10px;">support@moxyst.com</p>
        </div>

        		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">ติดตามที่</p>
			<div class="follow-icons" style="width: 100%">
				<a href="https://www.facebook.com/moxyst"><img src="{{skin _area="frontend"  url="images/email/facebook_follow.png"}}" style="width: 25px;" alt="Facebook fanpage"></a>
				<a href="https://twitter.com/moxyst"><img src="{{skin _area="frontend"  url="images/email/twitter_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
				<a href="https://instagram.com/moxy_th/"><img src="{{skin _area="frontend"  url="images/email/instagram_follow.png"}}" style="width: 25px;" alt="Instagram fanpage"></a>
				<a href="https://www.pinterest.com/MoxySEA1/"><img src="{{skin _area="frontend"  url="images/email/pinterest_follow.png"}}" style="width: 25px;" alt="Pinterest fanpage"></a>
				<a href="https://plus.google.com/+Moxyst"><img src="{{skin _area="frontend"  url="images/email/google_follow.png"}}" style="width: 25px;" alt="Google fanpage"></a>
			</div>
		</div>

        <div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
            <p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">ช่องทางการชำระเงิน</p>
            <img src="{{skin _area="frontend"  url="images/email/payment-th.png"}}" alt="ช่องทางการชำระเงิน" style="width: 109px;">
        </div>

        <div class="clear" style="clear:both;"></div>
    </div>

    <div class="footer" style="padding: 0px 33px; margin-top: 18px;">
        <div class="copyr" style="float: left; font-size: 12px;">&copy;2015 Whatsnew Co.,Ltd. All Rights Reserved</div>
        <div class="links" style="float: right; font-size: 12px;">
            <a href="http://www.moxy.co.th/th/about-us/" style="color: #3c3d41;"><span>เกี่ยวกับเรา</span></a> |
            <a href="http://www.moxy.co.th/th/terms-and-conditions/" style="color: #3c3d41;"><span>ข้อกำหนดและเงื่อนไข</span></a> |
            <a href="http://www.moxy.co.th/th/privacy-policy/" style="color: #3c3d41;"><span>ความเป็นส่วนตัว</span></a>
        </div>
    </div>
	<div style="clear:both"></div>
	<hr style="margin: 40px auto; width: 95%;">

	<!--English Version-->
	<img style="width: 100%; margin-top:0px;" src="{{skin _area="frontend"  url="images/email/feel_like_shopping_today_en.png"}}" alt="Feel like shopping today ?" />

	<div class="mail-body" style="margin:50px auto; width: 95%;">
		<div class="left-main-body" style="float: left; width: 57%; padding: 0px 5px; border-right: 1px solid #e7e7e7;">
			<p>Dear {{var customer_name}},</p>

			<br/>
			<p>Looks like you have something left in your shopping cart. Do you feel like purchasing it today? </p>
			<br>

			<p>For your convenience, You can shop now and pay later with cash-on-delivery. </p>
			<p>Check our conditions of payment <a href="http://www.moxy.co.th/en/help/#payments" style="color: #3c3d41;">here</a>.</p>
			<br>

			<p>Have a beautiful day!</p>
			<br>

			<p>Warm Regards,</p>
			<p>Moxy Team</p>
		</div>
				<div class="right-main-body" style="width: 38%; float: right; padding:0px 5px; text-align: center;">
			 {{var products}}
   <a href="{{store url="checkout/cart/"}}"
               style="display: block; width: 50%; color: #fff; text-decoration: none; margin:10px auto; background: #ed5192;padding: 10px;clear:both;">BUY IT NOW</a>
            <p>Buy now before stock last</p>
		</div>

		<div style="clear: both;"></div>
	</div>



 <div class="navigation-th" style="margin-top: 24px; overflow: hidden; text-align: center;">
		<div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #ee5191; border-bottom: 2px solid #ee5191; padding: 0px 11px;">
			<span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 18; padding: 0px 7px;"><a href="http://moxy.co.th/en/moxy/fashion" style="color: #3c3d41; text-decoration: none;">Fashion</a></span>
			<span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 18; padding: 0px 7px;"><a href="http://moxy.co.th/en/moxy/homeliving" style="color: #3c3d41; text-decoration: none;">Living</a></span>
			<span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 18; padding: 0px 7px;"><a href="http://moxy.co.th/en/moxy/electronics/" style="color: #3c3d41; text-decoration: none;">Electronics</a></span>
			<span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 18; padding: 0px 7px;"><a href="http://lafema.com/en/" style="color: #3c3d41; text-decoration: none;">Beauty</a></span>
			<span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 18; padding: 0px 7px;"><a href="http://venbi.com/en/" style="color: #3c3d41; text-decoration: none;">Baby & Mom</a></span>
			<span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 18; padding: 0px 7px;"><a href="http://sanoga.com/en/" style="color: #3c3d41; text-decoration: none;">Health</a></span>
			<span class="nav-item" style="line-height: 33px; font-size: 18; padding: 0px 7px;"><a href="http://petloft.com/en/" style="color: #3c3d41; text-decoration: none;">Pets</a></span>
		</div>
	</div>

    <div class="email-footer" style="background: #bbb; margin-top: 8px; color: #fff; padding: 0px 15px;">
        <div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
            <p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px"><img src="{{skin _area="frontend"  url="images/email/mailbox.png"}}" style="width:18px;"/> Contact Us</p>
            <p style="font-size: 10px; margin: 0;">02-106-8222</p>
            <p style="margin: 0; font-size: 10px;">support@moxyst.com</p>
        </div>

       		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">Follow Us</p>
			<div class="follow-icons" style="width: 100%">
				<a href="https://www.facebook.com/moxyst"><img src="{{skin _area="frontend"  url="images/email/facebook_follow.png"}}" style="width: 25px;" alt="Facebook fanpage"></a>
				<a href="https://twitter.com/moxyst"><img src="{{skin _area="frontend"  url="images/email/twitter_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
				<a href="https://instagram.com/moxy_th/"><img src="{{skin _area="frontend"  url="images/email/instagram_follow.png"}}" style="width: 25px;" alt="Instagram fanpage"></a>
				<a href="https://www.pinterest.com/MoxySEA1/"><img src="{{skin _area="frontend"  url="images/email/pinterest_follow.png"}}" style="width: 25px;" alt="Pinterest fanpage"></a>
				<a href="https://plus.google.com/+Moxyst"><img src="{{skin _area="frontend"  url="images/email/google_follow.png"}}" style="width: 25px;" alt="Google fanpage"></a>
			</div>
		</div>

        <div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
            <p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">Payment Options</p>
            <img src="{{skin _area="frontend"  url="images/email/payment.png"}}" alt="Payment Options" style="width: 109px;">
        </div>

        <div class="clear" style="clear:both;"></div>
    </div>

    <div class="footer" style="padding: 0px 33px; margin-top: 18px;">
        <div class="copyr" style="float: left; font-size: 12px;">&copy;2015 Whatsnew Co.,Ltd. All Rights Reserved</div>
        <div class="links" style="float: right; font-size: 12px;">
            <a href="http://www.moxy.co.th/en/about-us/" style="color: #3c3d41;"><span>About Us</span></a> |
            <a href="http://www.moxy.co.th/en/terms-and-conditions/" style="color: #3c3d41;"><span>Term & Conditions</span></a> |
            <a href="http://www.moxy.co.th/en/privacy-policy/" style="color: #3c3d41;"><span>Privacy Policy</span></a>
        </div>
    </div>
	<div style="clear:both"></div>
</div>
HTML;

/** @var Mage_Core_Model_Email_Template $model */
$model = Mage::getModel('core/email_template');
$model->loadByCode('Cart Reminder Moxy')
    ->setTemplateCode('Cart Reminder Moxy')
    ->setTemplateText($template_text)
    ->setTemplateSubject('Don\'t miss out on these great offers now! Here are the items that you hand picked before. Save time and buy them now!')
    ->setTemplateType(Mage_Core_Model_Email_Template::TYPE_HTML);

try {
    $model->save();
    $config->saveConfig('catalog/adjcartalert/template', $model->getId() , 'stores', $moxyen);
    $config->saveConfig('catalog/adjcartalert/template', $model->getId() , 'stores', $moxyth);
    $config->saveConfig('catalog/adjcartalert/template2', $model->getId() , 'stores', $moxyen);
    $config->saveConfig('catalog/adjcartalert/template2', $model->getId() , 'stores', $moxyth);
    $config->saveConfig('catalog/adjcartalert/template3', $model->getId() , 'stores', $moxyen);
    $config->saveConfig('catalog/adjcartalert/template3', $model->getId() , 'stores', $moxyth);
} catch (Mage_Exception $e) {
    throw $e;
}
//==========================================================================
//==========================================================================

//==========================================================================
// CART REMINDER SANOGA
//==========================================================================

$template_text =
    <<<HTML
<meta charset="utf-8"/>
<div class="mail-container" style="width:600px; padding:0px 33px; font-family:  'Trebuchet MS', Tahoma; font-size: 14px; color: #3c3d41; margin: 0px;">
	<div class="mail-header" style="width:95%; min-height: 110px; max-height: 110px; position: relative; margin:0px auto;">
		<div class="mail-logo" style="width:220px; min-height: 110px; max-height: 110px; margin:0px auto;">
			<a href="http://sanoga.com/"><img src="{{skin _area="frontend"  url="images/email/sanoga-logo.png"}}" style="width:220px; max-height: 110px; min-height: 110px;" /></a>
		</div>

		<div class="english-below" style="min-height: 16px; max-height: 16px; position: absolute; bottom: 0px; line-height: 16px;">
			<span><img src="{{skin _area="frontend"  url="images/email/flag-en.png"}}" height="16px;"/> Scroll down for English version</span>
		</div>
	</div>

	<img style="width: 100%; margin-top:40px;" src="{{skin _area="frontend"  url="images/email/feel_like_shopping_today_thai.png"}}" alt="Thank for your order" />

	<div class="mail-body" style="margin:50px auto; width: 95%;">
		<div class="left-main-body" style="float: left; width: 57%; padding: 0px 5px; border-right: 1px solid #e7e7e7;">
			<p>เรียน คุณ {{var customer_name}},</p>
			<br/>

			<p>มีสินค้าบางอย่างหลงเหลืออยู่ในตะกร้าสินค้าของคุณ</p>
			<p>คุณต้องการจะสั่งซื้อวันนี้ไหม ?</p>
			<br>

			<p>คุณสามารถสั่งซื้อตอนนี้และชำระเงินในภายหลัง เพื่อความสะดวกสบายที่ยิ่งขึ้น</p>
			<p>เลือกดูวิธีการชำระเงินอื่นๆได้<a href="http://www.sanoga.com/th/help/#payments" style="color: #3c3d41;">ที่น</a></p>
			<br>

			<p>Have a beautiful day!</p>
			<br>

			<p>Warm Regards,</p>
			<p>Moxy Team</p>
		</div>

			<div class="right-main-body" style="width: 38%; float: right; padding:0px 5px; text-align: center;">
		           {{var products}}
            <a href="{{store url="checkout/cart/"}}"
               style="display: block; width: 50%; color: #fff; text-decoration: none; margin:10px auto; background: #6ABC45;padding: 10px;clear:both;">สั่งซื้อ</a>
            <p>สั่งซื้อตอนนี้ก่อนที่ของจะหมด</p>
		</div>

		<div style="clear: both;"></div>
	</div>



<div class="navigation-th" style="margin-top: 24px; text-align:center;  overflow:hidden;">
		<div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #6abc45; border-bottom: 2px solid #6abc45; padding: 0px 0px; overflow:hidden;">
			<span class="nav-item" style="border-right: 2px solid #6abc45; line-height: 33px; font-size: 14; padding: 0px 12px;"><a href="http://www.sanoga.com/th/health-care.html" style="color: #3c3d41; text-decoration: none;">ดูแลสุขภาพ</a></span>
			<span class="nav-item" style="border-right: 2px solid #6abc45; line-height: 33px; font-size: 14; padding: 0px 12px;"><a href="http://www.sanoga.com/th/beauty.html" style="color: #3c3d41; text-decoration: none;">ความงาม</a></span>
			<span class="nav-item" style="border-right: 2px solid #6abc45; line-height: 33px; font-size: 14; padding: 0px 12px;"><a href="http://www.sanoga.com/th/workout-fitness.html" style="color: #3c3d41; text-decoration: none;">ออกกำลังกาย-ฟิตเนส</a></span>
			<span class="nav-item" style="border-right: 2px solid #6abc45; line-height: 33px; font-size: 14; padding: 0px 12px;"><a href="http://www.sanoga.com/th/weight-loss.html" style="color: #3c3d41; text-decoration: none;">อาหารเสริมลดน้ำหนัก</a></span>
			<span class="nav-item" style="line-height: 33px; font-size: 14; padding: 0px 12px;"><a href="http://www.sanoga.com/th/sexual-well-being.html" style="color: #3c3d41; text-decoration: none;">สุขภาพ-เซ็กส์</a></span>
		</div>
	</div>

	<div class="email-footer" style="min-height: 82px; max-heigh: 82px; background: #bbb; margin-top: 8px; color: #fff; padding: 0px 15px;">
		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px"><img src="{{skin _area="frontend"  url="images/email/mailbox.png"}}" style="width:18px;"/> ติดต่อเรา</p>
			<p style="font-size: 10px; margin: 0;">02-106-8222</p>
			<p style="margin: 0; font-size: 10px;">support@sanoga.com</p>
		</div>

		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">ติดตามที่</p>
			<div class="follow-icons" style="width: 100%">
				<a href="https://www.facebook.com/SanogaThailand"><img src="{{skin _area="frontend"  url="images/email/facebook_follow.png"}}" style="width: 25px;" alt="Facebook fanpage"></a>
				<a href="https://twitter.com/sanoga_thailand"><img src="{{skin _area="frontend"  url="images/email/twitter_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
				<a href="#"><img src="{{skin _area="frontend"  url="images/email/instagram_follow.png"}}" style="width: 25px;" alt="Instagram fanpage"></a>
				<a href="https://www.pinterest.com/sanoga/"><img src="{{skin _area="frontend"  url="images/email/pinterest_follow.png"}}" style="width: 25px;" alt="Pinterest fanpage"></a>
				<a href="https://plus.google.com/114390899173698565828/"><img src="{{skin _area="frontend"  url="images/email/google_follow.png"}}" style="width: 25px;" alt="Google fanpage"></a>
			</div>
		</div>

		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">ช่องทางการชำระเงิน</p>
			<img src="{{skin _area="frontend"  url="images/email/payment-th.png"}}" alt="ช่องทางการชำระเงิน" style="width: 109px;">
		</div>
	</div>

	<div class="footer" style="padding: 0px 33px; margin-top: 18px;">
		<div class="copyr" style="float: left; font-size: 12px;">&copy;2015 Whatsnew Co.,Ltd. All Rights Reserved</div>
		<div class="links" style="float: right; font-size: 12px;">
			<a href="http://www.sanoga.com/th/about-us/" style="color: #3c3d41;"><span>เกี่ยวกับเรา</span></a> |
			<a href="http://www.sanoga.com/th/terms-and-conditions/" style="color: #3c3d41;"><span>ข้อกำหนดและเงื่อนไข</span></a> |
			<a href="http://www.sanoga.com/th/privacy-policy/" style="color: #3c3d41;"><span>ความเป็นส่วนตัว</span></a>
		</div>
	</div>
	<div style="clear:both"></div>
	<hr style="margin: 40px auto; width: 95%;">

	<!--English Version-->
	<img style="width: 100%; margin-top:0px;" src="{{skin _area="frontend"  url="images/email/feel_like_shopping_today_en.png"}}" alt="Feel like shopping today ?" />

	<div class="mail-body" style="margin:50px auto; width: 95%;">
		<div class="left-main-body" style="float: left; width: 57%; padding: 0px 5px; border-right: 1px solid #e7e7e7;">
			<p>Dear {{var customer_name}},</p>

			<br/>
			<p>Looks like you have something left in your shopping cart. Do you feel like purchasing it today? </p>
			<br>

			<p>For your convenience, You can shop now and pay later with cash-on-delivery. </p>
			<p>Check our conditions of payment <a href="http://www.sanoga.com/en/help/#payments" style="color: #3c3d41;">here</a>.</p>
			<br>

			<p>Have a beautiful day!</p>
			<br>

			<p>Warm Regards,</p>
			<p>Moxy Team</p>
		</div>
		<div class="right-main-body" style="width: 38%; float: right; padding:0px 5px; text-align: center;">
			 {{var products}}
   <a href="{{store url="checkout/cart/"}}"
               style="display: block; width: 50%; color: #fff; text-decoration: none; margin:10px auto; background: #6ABC45;padding: 10px;clear:both;">BUY IT NOW</a>
            <p>Buy now before stock last</p>
		</div>

		<div style="clear: both;"></div>
	</div>



<div class="navigation-th" style="margin-top: 24px; text-align:center;  overflow:hidden;">
		<div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #6abc45; border-bottom: 2px solid #6abc45; padding: 0px 0px; overflow:hidden;">
			<span class="nav-item" style="border-right: 2px solid #6abc45; line-height: 33px; font-size: 14; padding: 0px 12px;"><a href="http://www.sanoga.com/en/health-care.html" style="color: #3c3d41; text-decoration: none;">Health Care</a></span>
			<span class="nav-item" style="border-right: 2px solid #6abc45; line-height: 33px; font-size: 14; padding: 0px 12px;"><a href="http://www.sanoga.com/en/beauty.html" style="color: #3c3d41; text-decoration: none;">Beauty</a></span>
			<span class="nav-item" style="border-right: 2px solid #6abc45; line-height: 33px; font-size: 14; padding: 0px 12px;"><a href="http://www.sanoga.com/en/workout-fitness.html" style="color: #3c3d41; text-decoration: none;">Workout & Fitness</a></span>
			<span class="nav-item" style="border-right: 2px solid #6abc45; line-height: 33px; font-size: 14; padding: 0px 12px;"><a href="http://www.sanoga.com/en/weight-loss.html" style="color: #3c3d41; text-decoration: none;">Weight Loss</a></span>
			<span class="nav-item" style="line-height: 33px; font-size: 14; padding: 0px 12px;"><a href="http://www.sanoga.com/en/sexual-well-being.html" style="color: #3c3d41; text-decoration: none;">Sexual Well-Being</a></span>
		</div>
	</div>

	<div class="email-footer" style="min-height: 82px; max-heigh: 82px; background: #bbb; margin-top: 8px; color: #fff; padding: 0px 15px;">
		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px"><img src="{{skin _area="frontend"  url="images/email/mailbox.png"}}" style="width:18px;"/> Contact Us</p>
			<p style="font-size: 10px; margin: 0;">02-106-8222</p>
			<p style="margin: 0; font-size: 10px;">support@sanoga.com</p>
		</div>

	<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">Follow Us</p>
			<div class="follow-icons" style="width: 100%">
				<a href="https://www.facebook.com/SanogaThailand"><img src="{{skin _area="frontend"  url="images/email/facebook_follow.png"}}" style="width: 25px;" alt="Facebook fanpage"></a>
				<a href="https://twitter.com/sanoga_thailand"><img src="{{skin _area="frontend"  url="images/email/twitter_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
				<a href="#"><img src="{{skin _area="frontend"  url="images/email/instagram_follow.png"}}" style="width: 25px;" alt="Instagram fanpage"></a>
				<a href="https://www.pinterest.com/sanoga/"><img src="{{skin _area="frontend"  url="images/email/pinterest_follow.png"}}" style="width: 25px;" alt="Pinterest fanpage"></a>
				<a href="https://plus.google.com/114390899173698565828/"><img src="{{skin _area="frontend"  url="images/email/google_follow.png"}}" style="width: 25px;" alt="Google fanpage"></a>
			</div>
		</div>

		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">Payment Options</p>
			<img src="{{skin _area="frontend"  url="images/email/payment.png"}}" alt="Payment Options" style="width: 109px;">
		</div>
	</div>

	<div class="footer" style="padding: 0px 33px; margin-top: 18px;">
		<div class="copyr" style="float: left; font-size: 12px;">&copy;2015 Whatsnew Co.,Ltd. All Rights Reserved</div>
		<div class="links" style="float: right; font-size: 12px;">
			<a href="http://www.sanoga.com/en/about-us/" style="color: #3c3d41;"><span>About Us</span></a> |
			<a href="http://www.sanoga.com/en/terms-and-conditions/" style="color: #3c3d41;"><span>Term & Conditions</span></a> |
			<a href="http://www.sanoga.com/en/privacy-policy/" style="color: #3c3d41;"><span>Privacy Policy</span></a>
		</div>
	</div>
	<div style="clear:both"></div>
</div>
HTML;

/** @var Mage_Core_Model_Email_Template $model */
$model = Mage::getModel('core/email_template');
$model->loadByCode('Cart Reminder Sanoga')
    ->setTemplateCode('Cart Reminder Sanoga')
    ->setTemplateText($template_text)
    ->setTemplateSubject('Don\'t miss out on these great offers now! Here are the items that you hand picked before. Save time and buy them now!')
    ->setTemplateType(Mage_Core_Model_Email_Template::TYPE_HTML);

try {
    $model->save();
    $config->saveConfig('catalog/adjcartalert/template', $model->getId() , 'stores', $sen);
    $config->saveConfig('catalog/adjcartalert/template', $model->getId() , 'stores', $sth);
    $config->saveConfig('catalog/adjcartalert/template2', $model->getId() , 'stores', $sen);
    $config->saveConfig('catalog/adjcartalert/template2', $model->getId() , 'stores', $sth);
    $config->saveConfig('catalog/adjcartalert/template3', $model->getId() , 'stores', $sen);
    $config->saveConfig('catalog/adjcartalert/template3', $model->getId() , 'stores', $sth);
} catch (Mage_Exception $e) {
    throw $e;
}
//==========================================================================
//==========================================================================

//==========================================================================
// CART REMINDER VENBI
//==========================================================================

$template_text =
    <<<HTML
    <meta charset="utf-8"/>
<div class="mail-container" style="width:600px; padding:0px 33px; font-family:  'Trebuchet MS', Tahoma; font-size: 14px; color: #3c3d41; margin: 0px;">
	<div class="mail-header" style="width:95%; min-height: 110px; max-height: 110px; position: relative; margin:0px auto;">
		<div class="mail-logo" style="width:220px; min-height: 110px; max-height: 110px; margin:0px auto;">
			<a href="http://venbi.com/"><img src="{{skin _area="frontend"  url="images/email/venbi-logo.png"}}" style="width:220px; max-height: 110px; min-height: 110px;" /></a>
		</div>

		<div class="english-below" style="min-height: 16px; max-height: 16px; position: absolute; bottom: 0px; line-height: 16px;">
			<span><img src="{{skin _area="frontend"  url="images/email/flag-en.png"}}" height="16px;"/> Scroll down for English version</span>
		</div>
	</div>

	<img style="width: 100%; margin-top:40px;" src="{{skin _area="frontend"  url="images/email/feel_like_shopping_today_thai.png"}}" alt="Thank for your order" />

	<div class="mail-body" style="margin:50px auto; width: 95%;">
		<div class="left-main-body" style="float: left; width: 57%; padding: 0px 5px; border-right: 1px solid #e7e7e7;">
			<p>เรียน คุณ {{var customer_name}},</p>
			<br/>

			<p>มีสินค้าบางอย่างหลงเหลืออยู่ในตะกร้าสินค้าของคุณ</p>
			<p>คุณต้องการจะสั่งซื้อวันนี้ไหม ?</p>
			<br>

			<p>คุณสามารถสั่งซื้อตอนนี้และชำระเงินในภายหลัง เพื่อความสะดวกสบายที่ยิ่งขึ้น</p>
			<p>เลือกดูวิธีการชำระเงินอื่นๆได้<a href="http://www.venbi.com/th/help/#payments" style="color: #3c3d41;">ที่น</a></p>
			<br>

			<p>Have a beautiful day!</p>
			<br>

			<p>Warm Regards,</p>
			<p>Moxy Team</p>
		</div>

		<div class="right-main-body" style="width: 38%; float: right; padding:0px 5px; text-align: center;">
		           {{var products}}
            <a href="{{store url="checkout/cart/"}}"
               style="display: block; width: 50%; color: #fff; text-decoration: none; margin:10px auto; background: #f5ea14;padding: 10px;clear:both;">สั่งซื้อ</a>
            <p>สั่งซื้อตอนนี้ก่อนที่ของจะหมด</p>
		</div>

		<div style="clear: both;"></div>
	</div>



<div class="navigation-th" style="margin-top: 24px; overflow:hidden;">
		<div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #f5ea14; border-bottom: 2px solid #f5ea14; padding: 0px 0px; overflow: hidden;">
			<span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 6px;"><a href="http://www.venbi.com/th/formula-nursing.html" style="color: #3c3d41; text-decoration: none;">นมและอุปกรณ์</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 5px;"><a href="http://www.venbi.com/th/diapers.html" style="color: #3c3d41; text-decoration: none;">ผ้าอ้อมสำเร็จรูป</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 5px;"><a href="http://www.venbi.com/th/bath-baby-care.html" style="color: #3c3d41; text-decoration: none;">ผลิตภัณฑ์อาบน้ำและเบบี้แคร์</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 5px;"><a href="http://www.venbi.com/th/toys.html" style="color: #3c3d41; text-decoration: none;">ของเล่น</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 5px;"><a href="http://www.venbi.com/th/clothing.html" style="color: #3c3d41; text-decoration: none;">เสื้อผ้า</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 5px;"><a href="http://www.venbi.com/th/gear.html" style="color: #3c3d41; text-decoration: none;">เตียงและรถเข็น</a></span>
			<span class="nav-item" style="line-height: 33px; font-size: 11; padding: 0px 6px;"><a href="http://www.venbi.com/th/mom-maternity.html" style="color: #3c3d41; text-decoration: none;">แม่-การตั้งครรภ์</a></span>
		</div>
	</div>

	<div class="email-footer" style="min-height: 82px; max-heigh: 82px; background: #bbb; margin-top: 8px; color: #fff; padding: 0px 15px;">
		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px"><img src="{{skin _area="frontend"  url="images/email/mailbox.png"}}" style="width:18px;"/> ติดต่อเรา</p>
			<p style="font-size: 10px; margin: 0;">02-106-8222</p>
			<p style="margin: 0; font-size: 10px;">support@venbi.com</p>
		</div>

		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">สั่งซื้อ</p>
				<div class="follow-icons" style="width: 100%">
				<a href="https://www.facebook.com/venbithai"><img src="{{skin _area="frontend"  url="images/email/facebook_follow.png"}}" style="width: 25px;" alt="Facebook fanpage"></a>
				<a href="https://twitter.com/venbithai"><img src="{{skin _area="frontend"  url="images/email/twitter_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
				<a href="http://webstagr.am/venbi_thailand"><img src="{{skin _area="frontend"  url="images/email/instagram_follow.png"}}" style="width: 25px;" alt="Instagram fanpage"></a>
				<a href="https://www.pinterest.com/VenbiThailand/"><img src="{{skin _area="frontend"  url="images/email/pinterest_follow.png"}}" style="width: 25px;" alt="Pinterest fanpage"></a>
				<a href="https://plus.google.com/+VenbiThai"><img src="{{skin _area="frontend"  url="images/email/google_follow.png"}}" style="width: 25px;" alt="Google fanpage"></a>
			</div>
		</div>

		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">ช่องทางการชำระเงิน</p>
			<img src="{{skin _area="frontend"  url="images/email/payment-th.png"}}" alt="ช่องทางการชำระเงิน" style="width: 109px;">
		</div>
	</div>

	<div class="footer" style="padding: 0px 33px; margin-top: 18px;">
		<div class="copyr" style="float: left; font-size: 12px;">&copy;2015 Whatsnew Co.,Ltd. All Rights Reserved</div>
		<div class="links" style="float: right; font-size: 12px;">
			<a href="http://www.venbi.com/th/about-us/" style="color: #3c3d41;"><span>เกี่ยวกับเรา</span></a> |
			<a href="http://www.venbi.com/th/terms-and-conditions/" style="color: #3c3d41;"><span>ข้อกำหนดและเงื่อนไข</span></a> |
			<a href="http://www.venbi.com/th/privacy-policy/" style="color: #3c3d41;"><span>ความเป็นส่วนตัว</span></a>
		</div>
	</div>
	<div style="clear:both"></div>
	<hr style="margin: 40px auto; width: 95%;">

	<!--English Version-->
	<img style="width: 100%; margin-top:0px;" src="{{skin _area="frontend"  url="images/email/feel_like_shopping_today_en.png"}}" alt="Feel like shopping today ?" />

	<div class="mail-body" style="margin:50px auto; width: 95%;">
		<div class="left-main-body" style="float: left; width: 57%; padding: 0px 5px; border-right: 1px solid #e7e7e7;">
			<p>Dear {{var customer_name}},</p>

			<br/>
			<p>Looks like you have something left in your shopping cart. Do you feel like purchasing it today? </p>
			<br>

			<p>For your convenience, You can shop now and pay later with cash-on-delivery. </p>
			<p>Check our conditions of payment <a href="http://www.venbi.com/en/help/#payments" style="color: #3c3d41;">here</a>.</p>
			<br>

			<p>Have a beautiful day!</p>
			<br>

			<p>Warm Regards,</p>
			<p>Moxy Team</p>
		</div>
			<div class="right-main-body" style="width: 38%; float: right; padding:0px 5px; text-align: center;">
			 {{var products}}
   <a href="{{store url="checkout/cart/"}}"
               style="display: block; width: 50%; color: #fff; text-decoration: none; margin:10px auto; background: #f5ea14;padding: 10px;clear:both;">BUY IT NOW</a>
            <p>Buy now before stock last</p>
		</div>


		<div style="clear: both;"></div>
	</div>



<div class="navigation-th" style="margin-top: 24px; overflow:hidden;">
		<div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #f5ea14; border-bottom: 2px solid #f5ea14; padding: 0px 0px; overflow: hidden;">
			<span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 5px;"><a href="http://www.venbi.com/en/formula-nursing.html" style="color: #3c3d41; text-decoration: none;">Formula & Nursing</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 5px;"><a href="http://www.venbi.com/en/diapers.html" style="color: #3c3d41; text-decoration: none;">Diapers</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 5px;"><a href="http://www.venbi.com/en/bath-baby-care.html" style="color: #3c3d41; text-decoration: none;">Bath & Baby Care</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 5px;"><a href="http://www.venbi.com/en/toys.html" style="color: #3c3d41; text-decoration: none;">Toys</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 5px;"><a href="http://www.venbi.com/en/clothing.html" style="color: #3c3d41; text-decoration: none;">Clothing</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 5px;"><a href="http://www.venbi.com/en/gear.html" style="color: #3c3d41; text-decoration: none;">Gear</a></span>
			<span class="nav-item" style="line-height: 33px; font-size: 11; padding: 0px 6px;"><a href="http://www.venbi.com/en/mom-maternity.html" style="color: #3c3d41; text-decoration: none;">Mom & Maternity</a></span>
		</div>
	</div>

	<div class="email-footer" style="min-height: 82px; max-heigh: 82px; background: #bbb; margin-top: 8px; color: #fff; padding: 0px 15px;">
		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px"><img src="{{skin _area="frontend"  url="images/email/mailbox.png"}}" style="width:18px;"/> Contact Us</p>
			<p style="font-size: 10px; margin: 0;">02-106-8222</p>
			<p style="margin: 0; font-size: 10px;">support@venbi.com</p>
		</div>

		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">Follow Us</p>
				<div class="follow-icons" style="width: 100%">
				<a href="https://www.facebook.com/venbithai"><img src="{{skin _area="frontend"  url="images/email/facebook_follow.png"}}" style="width: 25px;" alt="Facebook fanpage"></a>
				<a href="https://twitter.com/venbithai"><img src="{{skin _area="frontend"  url="images/email/twitter_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
				<a href="http://webstagr.am/venbi_thailand"><img src="{{skin _area="frontend"  url="images/email/instagram_follow.png"}}" style="width: 25px;" alt="Instagram fanpage"></a>
				<a href="https://www.pinterest.com/VenbiThailand/"><img src="{{skin _area="frontend"  url="images/email/pinterest_follow.png"}}" style="width: 25px;" alt="Pinterest fanpage"></a>
				<a href="https://plus.google.com/+VenbiThai"><img src="{{skin _area="frontend"  url="images/email/google_follow.png"}}" style="width: 25px;" alt="Google fanpage"></a>
			</div>
		</div>

		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">Payment Options</p>
			<img src="{{skin _area="frontend"  url="images/email/payment.png"}}" alt="Payment Options" style="width: 109px;">
		</div>
	</div>

	<div class="footer" style="padding: 0px 33px; margin-top: 18px;">
		<div class="copyr" style="float: left; font-size: 12px;">&copy;2015 Whatsnew Co.,Ltd. All Rights Reserved</div>
		<div class="links" style="float: right; font-size: 12px;">
			<a href="http://www.venbi.com/en/about-us/" style="color: #3c3d41;"><span>About Us</span></a> |
			<a href="http://www.venbi.com/en/terms-and-conditions/" style="color: #3c3d41;"><span>Term & Conditions</span></a> |
			<a href="http://www.venbi.com/en/privacy-policy/" style="color: #3c3d41;"><span>Privacy Policy</span></a>
		</div>
	</div>
	<div style="clear:both"></div>
</div>
HTML;

/** @var Mage_Core_Model_Email_Template $model */
$model = Mage::getModel('core/email_template');
$model->loadByCode('Cart Reminder Venbi')
    ->setTemplateCode('Cart Reminder Venbi')
    ->setTemplateText($template_text)
    ->setTemplateSubject('Don\'t miss out on these great offers now! Here are the items that you hand picked before. Save time and buy them now!')
    ->setTemplateType(Mage_Core_Model_Email_Template::TYPE_HTML);

try {
    $model->save();
    $config->saveConfig('catalog/adjcartalert/template', $model->getId() , 'stores', $ven);
    $config->saveConfig('catalog/adjcartalert/template', $model->getId() , 'stores', $vth);
    $config->saveConfig('catalog/adjcartalert/template2', $model->getId() , 'stores', $ven);
    $config->saveConfig('catalog/adjcartalert/template2', $model->getId() , 'stores', $vth);
    $config->saveConfig('catalog/adjcartalert/template3', $model->getId() , 'stores', $ven);
    $config->saveConfig('catalog/adjcartalert/template3', $model->getId() , 'stores', $vth);
} catch (Mage_Exception $e) {
    throw $e;
}
//==========================================================================
//==========================================================================

//==========================================================================
// CART REMINDER LAFEMA
//==========================================================================

$template_text =
    <<<HTML
    <meta charset="utf-8"/>
<div class="mail-container" style="width:600px; padding:0px 33px; font-family:  'Trebuchet MS', Tahoma; font-size: 14px; color: #3c3d41; margin: 0px;">
	<div class="mail-header" style="width:95%; min-height: 110px; max-height: 110px; position: relative; margin:0px auto;">
		<div class="mail-logo" style="width:220px; min-height: 110px; max-height: 110px; margin:0px auto;">
			<a href="http://lafema.com/"><img src="{{skin _area="frontend"  url="images/email/lafema-logo.png"}}" style="width:220px; max-height: 110px; min-height: 110px;" /></a>
		</div>

		<div class="english-below" style="min-height: 16px; max-height: 16px; position: absolute; bottom: 0px; line-height: 16px;">
			<span><img src="{{skin _area="frontend"  url="images/email/flag-en.png"}}" height="16px;"/> Scroll down for English version</span>
		</div>
	</div>

	<img style="width: 100%; margin-top:40px;" src="{{skin _area="frontend"  url="images/email/feel_like_shopping_today_thai.png"}}" alt="Thank for your order" />

	<div class="mail-body" style="margin:50px auto; width: 95%;">
		<div class="left-main-body" style="float: left; width: 57%; padding: 0px 5px; border-right: 1px solid #e7e7e7;">
			<p>เรียน คุณ {{var customer_name}},</p>
			<br/>

			<p>มีสินค้าบางอย่างหลงเหลืออยู่ในตะกร้าสินค้าของคุณ</p>
			<p>คุณต้องการจะสั่งซื้อวันนี้ไหม ?</p>
			<br>

			<p>คุณสามารถสั่งซื้อตอนนี้และชำระเงินในภายหลัง เพื่อความสะดวกสบายที่ยิ่งขึ้น</p>
			<p>เลือกดูวิธีการชำระเงินอื่นๆได้<a href="http://www.lafema.com/th/help/#payments" style="color: #3c3d41;">ที่น</a></p>
			<br>

			<p>Have a beautiful day!</p>
			<br>

			<p>Warm Regards,</p>
			<p>Moxy Team</p>
		</div>

				<div class="right-main-body" style="width: 38%; float: right; padding:0px 5px; text-align: center;">
		           {{var products}}
            <a href="{{store url="checkout/cart/"}}"
               style="display: block; width: 50%; color: #fff; text-decoration: none; margin:10px auto; background: #f5a2b2;padding: 10px;clear:both;">สั่งซื้อ</a>
            <p>สั่งซื้อตอนนี้ก่อนที่ของจะหมด</p>
		</div>

		<div style="clear: both;"></div>
	</div>



  <div class="navigation-th" style="margin-top: 24px; text-align:center; overflow:hidden;">
		<div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #f5a2b2; border-bottom: 2px solid #f5a2b2; padding: 0px 0px;">
			<span class="nav-item" style="border-right: 2px solid #f5a2b2; line-height: 33px; font-size: 16; padding: 0px 18px;"><a href="http://www.lafema.com/th/makeup.html" style="color: #3c3d41; text-decoration: none;">เมคอัพ</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5a2b2; line-height: 33px; font-size: 16; padding: 0px 18px;"><a href="http://www.lafema.com/th/skincare.html" style="color: #3c3d41; text-decoration: none;">ดูแลผิวหน้า</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5a2b2; line-height: 33px; font-size: 16; padding: 0px 18px;"><a href="http://www.lafema.com/th/bath-body-hair.html" style="color: #3c3d41; text-decoration: none;">ดูแลผิวกาย-ผม</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5a2b2; line-height: 33px; font-size: 16; padding: 0px 18px;"><a href="http://www.lafema.com/th/fragrance.html" style="color: #3c3d41; text-decoration: none;">น้ำหอม</a></span>
			<span class="nav-item" style="line-height: 33px; font-size: 16; padding: 0px 12px;"><a href="http://www.lafema.com/th/men.html" style="color: #3c3d41; text-decoration: none;">สำหรับผู้ชาย</a></span>
		</div>
	</div>

	<div class="email-footer" style="min-height: 82px; max-heigh: 82px; background: #bbb; margin-top: 8px; color: #fff; padding: 0px 15px;">
		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px"><img src="{{skin _area="frontend"  url="images/email/mailbox.png"}}" style="width:18px;"/> ติดต่อเรา</p>
			<p style="font-size: 10px; margin: 0;">02-106-8222</p>
			<p style="margin: 0; font-size: 10px;">support@lafema.com</p>
		</div>

		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">ติดตามที่</p>
			<div class="follow-icons" style="width: 100%">
				<a href="https://www.facebook.com/lafemathai"><img src="{{skin _area="frontend"  url="images/email/facebook_follow.png"}}" style="width: 25px;" alt="Facebook fanpage"></a>
				<a href="https://twitter.com/lafemath"><img src="{{skin _area="frontend"  url="images/email/twitter_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
				<a href="https://instagram.com/lafema.th/"><img src="{{skin _area="frontend"  url="images/email/instagram_follow.png"}}" style="width: 25px;" alt="Instagram fanpage"></a>
				<a href="https://www.pinterest.com/lafema_thai/"><img src="{{skin _area="frontend"  url="images/email/pinterest_follow.png"}}" style="width: 25px;" alt="Pinterest fanpage"></a>
				<a href="https://plus.google.com/103314178165403417268/"><img src="{{skin _area="frontend"  url="images/email/google_follow.png"}}" style="width: 25px;" alt="Google fanpage"></a>
			</div>
		</div>

		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">ช่องทางการชำระเงิน</p>
			<img src="{{skin _area="frontend"  url="images/email/payment-th.png"}}" alt="ช่องทางการชำระเงิน" style="width: 109px;">
		</div>
	</div>

	<div class="footer" style="padding: 0px 33px; margin-top: 18px;">
		<div class="copyr" style="float: left; font-size: 12px;">&copy;2015 Whatsnew Co.,Ltd. All Rights Reserved</div>
		<div class="links" style="float: right; font-size: 12px;">
			<a href="http://www.lafema.com/th/about-us/" style="color: #3c3d41;"><span>เกี่ยวกับเรา</span></a> |
			<a href="http://www.lafema.com/th/terms-and-conditions/" style="color: #3c3d41;"><span>ข้อกำหนดและเงื่อนไข</span></a> |
			<a href="http://www.lafema.com/th/privacy-policy/" style="color: #3c3d41;"><span>ความเป็นส่วนตัว</span></a>
		</div>
	</div>
	<div style="clear:both"></div>
	<hr style="margin: 40px auto; width: 95%;">

	<!--English Version-->
	<img style="width: 100%; margin-top:0px;" src="{{skin _area="frontend"  url="images/email/feel_like_shopping_today_en.png"}}" alt="Feel like shopping today ?" />

	<div class="mail-body" style="margin:50px auto; width: 95%;">
		<div class="left-main-body" style="float: left; width: 57%; padding: 0px 5px; border-right: 1px solid #e7e7e7;">
			<p>Dear {{var customer_name}},</p>

			<br/>
			<p>Looks like you have something left in your shopping cart. Do you feel like purchasing it today? </p>
			<br>

			<p>For your convenience, You can shop now and pay later with cash-on-delivery. </p>
			<p>Check our conditions of payment <a href="http://www.lafema.com/en/help/#payments" style="color: #3c3d41;">here</a>.</p>
			<br>

			<p>Have a beautiful day!</p>
			<br>

			<p>Warm Regards,</p>
			<p>Moxy Team</p>
		</div>
				<div class="right-main-body" style="width: 38%; float: right; padding:0px 5px; text-align: center;">
			 {{var products}}
   <a href="{{store url="checkout/cart/"}}"
               style="display: block; width: 50%; color: #fff; text-decoration: none; margin:10px auto; background: #f5a2b2;padding: 10px;clear:both;">BUY IT NOW</a>
            <p>Buy now before stock last</p>
		</div>

		<div style="clear: both;"></div>
	</div>



    <div class="navigation-th" style="margin-top: 24px; text-align:center; overflow:hidden;">
		<div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #f5a2b2; border-bottom: 2px solid #f5a2b2; padding: 0px 0px;">
			<span class="nav-item" style="border-right: 2px solid #f5a2b2; line-height: 33px; font-size: 16; padding: 0px 18px;"><a href="http://www.lafema.com/en/makeup.html" style="color: #3c3d41; text-decoration: none;">Makeup</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5a2b2; line-height: 33px; font-size: 16; padding: 0px 18px;"><a href="http://www.lafema.com/en/skincare.html" style="color: #3c3d41; text-decoration: none;">Skin Care</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5a2b2; line-height: 33px; font-size: 16; padding: 0px 18px;"><a href="http://www.lafema.com/en/bath-body-hair.html" style="color: #3c3d41; text-decoration: none;">Bath & Body-Hair</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5a2b2; line-height: 33px; font-size: 16; padding: 0px 18px;"><a href="http://www.lafema.com/en/fragrance.html" style="color: #3c3d41; text-decoration: none;">Fragrance</a></span>
			<span class="nav-item" style="line-height: 33px; font-size: 16; padding: 0px 18px;"><a href="http://www.lafema.com/en/men.html" style="color: #3c3d41; text-decoration: none;">Men</a></span>
		</div>
	</div>

	<div class="email-footer" style="min-height: 82px; max-heigh: 82px; background: #bbb; margin-top: 8px; color: #fff; padding: 0px 15px;">
		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px"><img src="{{skin _area="frontend"  url="images/email/mailbox.png"}}" style="width:18px;"/> Contact Us</p>
			<p style="font-size: 10px; margin: 0;">02-106-8222</p>
			<p style="margin: 0; font-size: 10px;">support@lafema.com</p>
		</div>

		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">Follow Us</p>
							<div class="follow-icons" style="width: 100%">
				<a href="https://www.facebook.com/lafemathai"><img src="{{skin _area="frontend"  url="images/email/facebook_follow.png"}}" style="width: 25px;" alt="Facebook fanpage"></a>
				<a href="https://twitter.com/lafemath"><img src="{{skin _area="frontend"  url="images/email/twitter_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
				<a href="https://instagram.com/lafema.th/"><img src="{{skin _area="frontend"  url="images/email/instagram_follow.png"}}" style="width: 25px;" alt="Instagram fanpage"></a>
				<a href="https://www.pinterest.com/lafema_thai/"><img src="{{skin _area="frontend"  url="images/email/pinterest_follow.png"}}" style="width: 25px;" alt="Pinterest fanpage"></a>
				<a href="https://plus.google.com/103314178165403417268/"><img src="{{skin _area="frontend"  url="images/email/google_follow.png"}}" style="width: 25px;" alt="Google fanpage"></a>
			</div>
		</div>

		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">Payment Options</p>
			<img src="{{skin _area="frontend"  url="images/email/payment.png"}}" alt="Payment Options" style="width: 109px;">
		</div>
	</div>

	<div class="footer" style="padding: 0px 33px; margin-top: 18px;">
		<div class="copyr" style="float: left; font-size: 12px;">&copy;2015 Whatsnew Co.,Ltd. All Rights Reserved</div>
		<div class="links" style="float: right; font-size: 12px;">
			<a href="http://www.lafema.com/en/about-us/" style="color: #3c3d41;"><span>About Us</span></a> |
			<a href="http://www.lafema.com/en/terms-and-conditions/" style="color: #3c3d41;"><span>Term & Conditions</span></a> |
			<a href="http://www.lafema.com/en/privacy-policy/" style="color: #3c3d41;"><span>Privacy Policy</span></a>
		</div>
	</div>
	<div style="clear:both"></div>
</div>
HTML;

/** @var Mage_Core_Model_Email_Template $model */
$model = Mage::getModel('core/email_template');
$model->loadByCode('Cart Reminder Lafema')
    ->setTemplateCode('Cart Reminder Lafema')
    ->setTemplateText($template_text)
    ->setTemplateSubject('Don\'t miss out on these great offers now! Here are the items that you hand picked before. Save time and buy them now!')
    ->setTemplateType(Mage_Core_Model_Email_Template::TYPE_HTML);

try {
    $model->save();
    $config->saveConfig('catalog/adjcartalert/template', $model->getId() , 'stores', $len);
    $config->saveConfig('catalog/adjcartalert/template', $model->getId() , 'stores', $lth);
    $config->saveConfig('catalog/adjcartalert/template2', $model->getId() , 'stores', $len);
    $config->saveConfig('catalog/adjcartalert/template2', $model->getId() , 'stores', $lth);
    $config->saveConfig('catalog/adjcartalert/template3', $model->getId() , 'stores', $len);
    $config->saveConfig('catalog/adjcartalert/template3', $model->getId() , 'stores', $lth);
} catch (Mage_Exception $e) {
    throw $e;
}
//==========================================================================
//==========================================================================

//==========================================================================
// BACK IN STOCK PETLOFT
//==========================================================================

$template_text =
    <<<HTML
    <div class="mail-container" style="width:600px; padding:0px 33px; font-family:  'Trebuchet MS', Tahoma; font-size: 14px; color: #3c3d41; margin: 0px;">
	<div class="mail-header" style="width:95%; min-height: 110px; max-height: 110px; position: relative; margin:0px auto;">
		<div class="mail-logo" style="width:220px; min-height: 110px; max-height: 110px; margin:0px auto;">
			<a href="http://petloft.com/"><img src="{{skin url="images/email/petloft-logo.png"}}" style="width:220px; max-height: 110px; min-height: 110px;" /></a>
		</div>

		<div class="english-below" style="min-height: 16px; max-height: 16px; position: absolute; bottom: 0px; line-height: 16px;">
			<span><img src="{{skin url="images/email/flag-en.png"}}" height="16px;"/> Scroll down for English version</span>
		</div>
	</div>

	<img style="width: 100%; margin-top:40px;" src="{{skin url="images/email/its_backinstock_thai.png"}}" alt="Reset Password." />
	<div class="product-info" style="width: 100%; margin:5px auto; padding:10px; border: 1px solid #e7e7e7;">
		<div class="product-image" style="width: 36%; padding: 5px; border: 1px solid #e7e7e7; float: left;">
			<a href="{{var product_url}}?utm_source=edm&amp;utm_medium=reminder&amp;utm_campaign=petloft_outofstock_reminder"><img src="{{var product_image}}" alt="{{var product}}" style="border: none; width: 95%;"/></a>
		</div>

		<div class="product-detail" style="float: right; width: 60%; text-align: center; margin-top: 15px;">
			<p><strong>{{var product}}</strong></p>
			<p><strong>{{var product_price}}</strong></p>
			<a href="{{var product_url}}?utm_source=edm&amp;utm_medium=reminder&amp;utm_campaign=petloft_outofstock_reminder" style=" background: #007FBC;color: #fff;padding: 10px;text-decoration: none;" >สั่งซื้อตอนนี้เลย</a>
		</div>

		<div style="clear: both;"></div>
	</div>
	<div class="mail-body" style="margin:50px auto; width: 95%;">
		<p>เรียน คุณ  {{var customer.name}},</p>
		<br/>

		<p>คุณได้ทำการเพิ่มสินค้า {{var product}} เมื่อวันที่  {{var date}}  เข้าไปยังตะกร้าสินค้าก่อนหน้านี้ </p>
		<p>เรามีความยินดีจะแจ้งว่าสินค้าตอนนี้ได้กลับเข้าสู่สต็อกสินค้าแล้ว</p>
		<br>

		<p>Warm Regards,</p>
		<p>Moxy Team</p>
	</div>

	<div class="navigation-th" style="margin-top: 24px; overflow:hidden;">
		<div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #006ab4; border-bottom: 2px solid #006ab4; padding: 0px 17px; overflow: hidden;">
			<span class="nav-item" style="border-right: 2px solid #006ab4; line-height: 33px; font-size: 18px; padding: 0px 28px;"><a href="http://petloft.com/th/dog-food.html" style="color: #3c3d41; text-decoration: none;">สุนัข</a></span>
			<span class="nav-item" style="border-right: 2px solid #006ab4; line-height: 33px; font-size: 18px; padding: 0px 28px;"><a href="http://petloft.com/th/cat-food.html" style="color: #3c3d41; text-decoration: none;">แมว</a></span>
			<span class="nav-item" style="border-right: 2px solid #006ab4; line-height: 33px; font-size: 18px; padding: 0px 28px;"><a href="http://petloft.com/th/fish-products.html" style="color: #3c3d41; text-decoration: none;">ปลา</a></span>
			<span class="nav-item" style="border-right: 2px solid #006ab4; line-height: 33px; font-size: 18px; padding: 0px 28px;"><a href="http://www.petloft.com/th/bird.html" style="color: #3c3d41; text-decoration: none;">นก</a></span>
			<span class="nav-item" style="line-height: 33px; font-size: 18px; padding: 0px 28px;"><a href="http://petloft.com/th/small-pets-products.html" style="color: #3c3d41; text-decoration: none;">สัตว์เลี้ยงขนาดเล็ก</a></span>
		</div>
	</div>

	<div class="email-footer" style="background: #bbb; margin-top: 8px; color: #fff; padding: 0px 15px;">
		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px"><img src="{{skin url="images/email/mailbox.png"}}" style="width:18px;"/> ติดต่อเรา</p>
			<p style="font-size: 10px; margin: 0;">02-106-8222</p>
			<p style="margin: 0; font-size: 10px;">support@petloft.com</p>
		</div>

		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">ติดตามที่</p>
			<div class="follow-icons" style="width: 100%">
				<a href="https://www.facebook.com/petloftthai"><img src="{{skin url="images/email/facebook_follow.png"}}" style="width: 25px;" alt="Facebook fanpage"></a>
				<a href="https://twitter.com/petloft"><img src="{{skin url="images/email/twitter_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
				<a href="https://instagram.com/petloft/"><img src="{{skin url="images/email/instagram_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
				<a href="https://www.pinterest.com/petloft/"><img src="{{skin url="images/email/pinterest_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
				<a href="https://plus.google.com/110158894140498579470"><img src="{{skin url="images/email/google_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
			</div>
		</div>

		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">ช่องทางการชำระเงิน</p>
			<img src="{{skin url="images/email/payment-th.png"}}" alt="ช่องทางการชำระเงิน" style="width: 109px;">
		</div>

		<div class="clear" style="clear:both;"></div>
	</div>

	<div class="footer" style="padding: 0px 33px; margin-top: 18px;">
		<div class="copyr" style="float: left; font-size: 12px;">&copy;2015 Whatsnew Co.,Ltd. All Rights Reserved</div>
		<div class="links" style="float: right; font-size: 12px;">
			<a href="http://www.petloft.com/th/about-us/" style="color: #3c3d41;"><span>เกี่ยวกับเรา</span></a> |
			<a href="http://www.petloft.com/th/terms-and-conditions/" style="color: #3c3d41;"><span>ข้อกำหนดและเงื่อนไข</span></a> |
			<a href="http://www.petloft.com/th/privacy-policy/" style="color: #3c3d41;"><span>ความเป็นส่วนตัว</span></a>
		</div>
	</div>
	<div style="clear:both"></div>
	<hr style="margin: 40px auto; width: 95%;">

	<!--English Version-->
	<img style="width: 100%; margin-bottom:40px;" src="{{skin url="images/email/its_backinstock_en.png"}}" alt="Reset Password." />
	<div class="product-info" style="width: 100%; margin:5px auto; padding:10px; border: 1px solid #e7e7e7;">
		<div class="product-image" style="width: 36%; padding: 5px; border: 1px solid #e7e7e7; float: left;">
			<a href="{{var product_url}}?utm_source=edm&amp;utm_medium=reminder&amp;utm_campaign=petloft_outofstock_reminder"><img src="{{var product_image}}" alt="{{var product}}" style="border: none; width: 95%;"/></a>
		</div>

		<div class="product-detail" style="float: right; width: 60%; text-align: center; margin-top: 15px;">
			<p><strong>{{var product}}</strong></p>
			<p><strong>{{var product_price}}</strong></p>
			<a href="{{var product_url}}?utm_source=edm&amp;utm_medium=reminder&amp;utm_campaign=petloft_outofstock_reminder" style=" background: #007FBC;color: #fff;padding: 10px;text-decoration: none;" >BUY IT NOW</a>
		</div>

		<div style="clear: both;"></div>
	</div>
	<div class="mail-body" style="margin:50px auto; width: 95%;">
		<p>Dear {{var customer.name}},</p>

		<br/>

		<p>On {{var date}} , you tried to purchase {{var product}}. </p>
		<p>We have a good news for you, it is back in stock.</p>
		<br>
		<p>Get your hands on it now while stock lasts.</p>

		<p>Warm Regards,</p>
		<p>Moxy Team</p>
	</div>

	<div class="navigation-th" style="margin-top: 24px; overflow:hidden;">
		<div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #006ab4; border-bottom: 2px solid #006ab4; padding: 0px 30px; overflow: hidden;">
			<span class="nav-item" style="border-right: 2px solid #006ab4; line-height: 33px; font-size: 18px; padding: 0px 29px;"><a href="http://petloft.com/en/dog-food.html" style="color: #3c3d41; text-decoration: none;">Dogs</a></span>
			<span class="nav-item" style="border-right: 2px solid #006ab4; line-height: 33px; font-size: 18px; padding: 0px 29px;"><a href="http://petloft.com/en/cat-food.html" style="color: #3c3d41; text-decoration: none;">Cats</a></span>
			<span class="nav-item" style="border-right: 2px solid #006ab4; line-height: 33px; font-size: 18px; padding: 0px 29px;"><a href="http://petloft.com/en/fish-products.html" style="color: #3c3d41; text-decoration: none;">Fish</a></span>
			<span class="nav-item" style="border-right: 2px solid #006ab4; line-height: 33px; font-size: 18px; padding: 0px 29px;"><a href="http://www.petloft.com/en/bird.html" style="color: #3c3d41; text-decoration: none;">Bird</a></span>
			<span class="nav-item" style="line-height: 33px; font-size: 18px; padding: 0px 29px;"><a href="http://petloft.com/en/small-pets-products.html" style="color: #3c3d41; text-decoration: none;">Small Pets</a></span>
		</div>
	</div>

	<div class="email-footer" style="background: #bbb; margin-top: 8px; color: #fff; padding: 0px 15px;">
		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px"><img src="{{skin url="images/email/mailbox.png"}}" style="width:18px;"/> Contact Us</p>
			<p style="font-size: 10px; margin: 0;">02-106-8222</p>
			<p style="margin: 0; font-size: 10px;">support@petloft.com</p>
		</div>

		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">Follow Us</p>
			<div class="follow-icons" style="width: 100%">
				<a href="#"><img src="{{skin url="images/email/facebook_follow.png"}}" style="width: 25px;" alt="Facebook fanpage"></a>
				<a href="#"><img src="{{skin url="images/email/twitter_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
				<a href="#"><img src="{{skin url="images/email/instagram_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
				<a href="#"><img src="{{skin url="images/email/pinterest_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
				<a href="#"><img src="{{skin url="images/email/google_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
			</div>
		</div>

		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">Payment Options</p>
			<img src="{{skin url="images/email/payment.png"}}" alt="Payment Options" style="width: 109px;">
		</div>

		<div class="clear" style="clear:both;"></div>
	</div>

	<div class="footer" style="padding: 0px 33px; margin-top: 18px;">
		<div class="copyr" style="float: left; font-size: 12px;">&copy;2015 Whatsnew Co.,Ltd. All Rights Reserved</div>
		<div class="links" style="float: right; font-size: 12px;">
			<a href="http://www.petloft.com/en/about-us/" style="color: #3c3d41;"><span>About Us</span></a> |
			<a href="http://www.petloft.com/en/terms-and-conditions/" style="color: #3c3d41;"><span>Term & Conditions</span></a> |
			<a href="http://www.petloft.com/en/privacy-policy/" style="color: #3c3d41;"><span>Privacy Policy</span></a>
		</div>
	</div>
	<div style="clear:both"></div>
</div>
HTML;

/** @var Mage_Core_Model_Email_Template $model */
$model = Mage::getModel('core/email_template');
$model->loadByCode('Back in Stock Petloft')
    ->setTemplateCode('Back in Stock Petloft')
    ->setTemplateText($template_text)
    ->setTemplateSubject('{{var product}} is back in stock!')
    ->setTemplateType(Mage_Core_Model_Email_Template::TYPE_HTML);

try {
    $model->save();
    $config->saveConfig('outofstocksubscription/mail/template', $model->getId() , 'stores', $en);
    $config->saveConfig('outofstocksubscription/mail/template', $model->getId() , 'stores', $th);
} catch (Mage_Exception $e) {
    throw $e;
}
//==========================================================================
//==========================================================================

//==========================================================================
// BACK IN STOCK MOXY
//==========================================================================

$template_text =
    <<<HTML
    <meta charset="utf-8"/>
<div class="mail-container" style="width:600px; padding:0px 33px; font-family:  'Trebuchet MS', Tahoma; font-size: 14px; color: #3c3d41; margin: 0px;">
	<div class="mail-header" style="width:95%; min-height: 110px; max-height: 110px; position: relative; margin:0px auto;">
		<div class="mail-logo" style="width:220px; min-height: 110px; max-height: 110px; margin:0px auto;">
			<a href="http://moxy.co.th/"><img src="{{skin url="images/email/moxy-logo.png"}}" style="width:220px; max-height: 110px; min-height: 110px;" /></a>
		</div>

		<div class="english-below" style="min-height: 16px; max-height: 16px; position: absolute; bottom: 0px; line-height: 16px;">
			<span><img src="{{skin url="images/email/flag-en.png"}}" height="16px;"/> Scroll down for English version</span>
		</div>
	</div>

	<img style="width: 100%; margin-top:40px;" src="{{skin url="images/email/its_backinstock_thai.png"}}" alt="Reset Password." />
	<div class="product-info" style="width: 100%; margin:5px auto; padding:10px; border: 1px solid #e7e7e7;">
		<div class="product-image" style="width: 36%; padding: 5px; border: 1px solid #e7e7e7; float: left;">
			<a href="{{var product_url}}?utm_source=edm&amp;utm_medium=reminder&amp;utm_campaign=moxy_outofstock_reminder"><img src="{{var product_image}}" alt="{{var product}}" style="border: none; width: 95%;"/></a>
		</div>

		<div class="product-detail" style="float: right; width: 60%; text-align: center; margin-top: 15px;">
			<p><strong>{{var product}}</strong></p>

			<p><strong>{{var product_price}}</strong></p>

			<a href="{{var product_url}}?utm_source=edm&amp;utm_medium=reminder&amp;utm_campaign=moxy_outofstock_reminder" style=" background: #ee5191;color: #fff;padding: 10px;text-decoration: none;" >สั่งซื้อตอนนี้เลย</a>
		</div>

		<div style="clear: both;"></div>
	</div>
	<div class="mail-body" style="margin:50px auto; width: 95%;">
		<p>เรียน คุณ {{var customer.name}},</p>
		<br/>

		<p>คุณได้ทำการเพิ่มสินค้า {{var product}} เมื่อวันที่ {{var date}} เข้าไปยังตะกร้าสินค้าก่อนหน้านี้ </p>
		<p>เรามีความยินดีจะแจ้งว่าสินค้าตอนนี้ได้กลับเข้าสู่สต็อกสินค้าแล้ว</p>
		<br>

		<p>Warm Regards,</p>
		<p>Moxy Team</p>
	</div>

	<div class="navigation-th" style="margin-top: 24px; overflow: hidden; text-align: center;">
		<div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #ee5191; border-bottom: 2px solid #ee5191; padding: 0px 11px;">
			<span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 17; padding: 0px 4px;"><a href="http://moxy.co.th/th/moxy/fashion" style="color: #3c3d41; text-decoration: none;">แฟชั่น</a></span>
			<span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 17; padding: 0px 4px;"><a href="http://moxy.co.th/th/moxy/homeliving" style="color: #3c3d41; text-decoration: none;">ของแต่งบ้าน</a></span>
			<span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 17; padding: 0px 4px;"><a href="http://moxy.co.th/th/moxy/electronics/" style="color: #3c3d41; text-decoration: none;">อิเล็กทรอนิกส์</a></span>
			<span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 17; padding: 0px 4px;"><a href="http://lafema.com/th/" style="color: #3c3d41; text-decoration: none;">ความงาม</a></span>
			<span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 17; padding: 0px 4px;"><a href="http://venbi.com/th/" style="color: #3c3d41; text-decoration: none;">แม่และเด็ก</a></span>
			<span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 17; padding: 0px 4px;"><a href="http://sanoga.com/th/" style="color: #3c3d41; text-decoration: none;">สุขภาพ</a></span>
			<span class="nav-item" style="line-height: 33px; font-size: 17; padding: 0px 4px;"><a href="http://petloft.com/th/" style="color: #3c3d41; text-decoration: none;">สัตว์เลี้ยง</a></span>
		</div>
	</div>

	<div class="email-footer" style="background: #bbb; margin-top: 8px; color: #fff; padding: 0px 15px;">
		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px"><img src="{{skin url="images/email/mailbox.png"}}" style="width:18px;"/> ติดต่อเรา</p>
			<p style="font-size: 10px; margin: 0;">02-106-8222</p>
			<p style="margin: 0; font-size: 10px;">support@moxyst.com</p>
		</div>

		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">ติดตามที่</p>
			<div class="follow-icons" style="width: 100%">
				<a href="https://www.facebook.com/moxyst"><img src="{{skin url="images/email/facebook_follow.png"}}" style="width: 25px;" alt="Facebook fanpage"></a>
				<a href="https://twitter.com/moxyst"><img src="{{skin url="images/email/twitter_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
				<a href="https://instagram.com/moxy_th/"><img src="{{skin url="images/email/instagram_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
				<a href="https://www.pinterest.com/MoxySEA1/"><img src="{{skin url="images/email/pinterest_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
				<a href="https://plus.google.com/+Moxyst/"><img src="{{skin url="images/email/google_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
			</div>
		</div>

		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">ช่องทางการชำระเงิน</p>
			<img src="{{skin url="images/email/payment-th.png"}}" alt="ช่องทางการชำระเงิน" style="width: 109px;">
		</div>
		<div style="clear:both"></div>
	</div>

	<div class="footer" style="padding: 0px 33px; margin-top: 18px;">
		<div class="copyr" style="float: left; font-size: 12px;">&copy;2015 Whatsnew Co.,Ltd. All Rights Reserved</div>
		<div class="links" style="float: right; font-size: 12px;">
			<a href="http://www.moxy.co.th/th/about-us/" style="color: #3c3d41;"><span>เกี่ยวกับเรา</span></a> |
			<a href="http://www.moxy.co.th/th/terms-and-conditions/" style="color: #3c3d41;"><span>ข้อกำหนดและเงื่อนไข</span></a> |
			<a href="http://www.moxy.co.th/th/privacy-policy/" style="color: #3c3d41;"><span>ความเป็นส่วนตัว</span></a>
		</div>
	</div>
	<div style="clear:both"></div>
	<hr style="margin: 40px auto; width: 95%;">

	<!--English Version-->
	<img style="width: 100%; margin-bottom:40px;" src="{{skin url="images/email/its_backinstock_en.png"}}" alt="Reset Password." />
	<div class="product-info" style="width: 100%; margin:5px auto; padding:10px; border: 1px solid #e7e7e7;">
		<div class="product-image" style="width: 36%; padding: 5px; border: 1px solid #e7e7e7; float: left;">
			<a href="{{var product_url}}?utm_source=edm&amp;utm_medium=reminder&amp;utm_campaign=moxy_outofstock_reminder"><img src="{{var product_image}}" alt="{{var product}}" style="border: none; width: 95%;"/></a>
		</div>

		<div class="product-detail" style="float: right; width: 60%; text-align: center; margin-top: 15px;">
			<p><strong>{{var product}}</strong></p>
			<p><strong>{{var product_price}}</strong></p>
			<a href="{{var product_url}}?utm_source=edm&amp;utm_medium=reminder&amp;utm_campaign=moxy_outofstock_reminder" style=" background: #ee5191;color: #fff;padding: 10px;text-decoration: none;" >BUY IT NOW</a>
		</div>

		<div style="clear: both;"></div>
	</div>
	<div class="mail-body" style="margin:50px auto; width: 95%;">
		<p>Dear {{var customer.name}},</p>
		<br/>

		<p>On {{var date}} , you tried to purchase {{var product}}. </p>
		<p>We have a good news for you, it is back in stock.</p>
		<br>
		<p>Get your hands on it now while stock lasts.</p>

		<p>Warm Regards,</p>
		<p>Moxy Team</p>
	</div>

	<div class="navigation-th" style="margin-top: 24px; overflow: hidden; text-align: center;">
		<div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #ee5191; border-bottom: 2px solid #ee5191; padding: 0px 11px;">
			<span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 18; padding: 0px 7px;"><a href="http://moxy.co.th/en/moxy/fashion" style="color: #3c3d41; text-decoration: none;">Fashion</a></span>
			<span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 18; padding: 0px 7px;"><a href="http://moxy.co.th/en/moxy/homeliving" style="color: #3c3d41; text-decoration: none;">Living</a></span>
			<span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 18; padding: 0px 7px;"><a href="http://moxy.co.th/en/moxy/electronics/" style="color: #3c3d41; text-decoration: none;">Electronics</a></span>
			<span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 18; padding: 0px 7px;"><a href="http://lafema.com/en/" style="color: #3c3d41; text-decoration: none;">Beauty</a></span>
			<span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 18; padding: 0px 7px;"><a href="http://venbi.com/en/" style="color: #3c3d41; text-decoration: none;">Baby & Mom</a></span>
			<span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 18; padding: 0px 7px;"><a href="http://sanoga.com/en/" style="color: #3c3d41; text-decoration: none;">Health</a></span>
			<span class="nav-item" style="line-height: 33px; font-size: 18; padding: 0px 7px;"><a href="http://petloft.com/en/" style="color: #3c3d41; text-decoration: none;">Pets</a></span>
		</div>
	</div>

	<div class="email-footer" style="background: #bbb; margin-top: 8px; color: #fff; padding: 0px 15px;">
		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px"><img src="{{skin url="images/email/mailbox.png"}}" style="width:18px;"/> Contact Us</p>
			<p style="font-size: 10px; margin: 0;">02-106-8222</p>
			<p style="margin: 0; font-size: 10px;">support@moxyst.com</p>
		</div>

		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">Follow Us</p>
			<div class="follow-icons" style="width: 100%">
				<a href="https://www.facebook.com/moxyst"><img src="{{skin url="images/email/facebook_follow.png"}}" style="width: 25px;" alt="Facebook fanpage"></a>
				<a href="https://twitter.com/moxyst"><img src="{{skin url="images/email/twitter_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
				<a href="https://instagram.com/moxy_th/"><img src="{{skin url="images/email/instagram_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
				<a href="https://www.pinterest.com/MoxySEA1/"><img src="{{skin url="images/email/pinterest_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
				<a href="https://plus.google.com/+Moxyst/"><img src="{{skin url="images/email/google_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
			</div>
		</div>

		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">Payment Options</p>
			<img src="{{skin url="images/email/payment.png"}}" alt="Payment Options" style="width: 109px;">
		</div>
		<div style="clear:both"></div>
	</div>

	<div class="footer" style="padding: 0px 33px; margin-top: 18px;">
		<div class="copyr" style="float: left; font-size: 12px;">&copy;2015 Whatsnew Co.,Ltd. All Rights Reserved</div>
		<div class="links" style="float: right; font-size: 12px;">
			<a href="http://www.moxy.co.th/en/about-us/" style="color: #3c3d41;"><span>About Us</span></a> |
			<a href="http://www.moxy.co.th/en/terms-and-conditions/" style="color: #3c3d41;"><span>Term & Conditions</span></a> |
			<a href="http://www.moxy.co.th/en/privacy-policy/" style="color: #3c3d41;"><span>Privacy Policy</span></a>
		</div>
	</div>
	<div style="clear:both"></div>
</div>
HTML;

/** @var Mage_Core_Model_Email_Template $model */
$model = Mage::getModel('core/email_template');
$model->loadByCode('Back in Stock Moxy')
    ->setTemplateCode('Back in Stock Moxy')
    ->setTemplateText($template_text)
    ->setTemplateSubject('{{var product}} is back in stock!')
    ->setTemplateType(Mage_Core_Model_Email_Template::TYPE_HTML);

try {
    $model->save();
    $config->saveConfig('outofstocksubscription/mail/template', $model->getId() , 'stores', $moxyen);
    $config->saveConfig('outofstocksubscription/mail/template', $model->getId() , 'stores', $moxyth);
} catch (Mage_Exception $e) {
    throw $e;
}
//==========================================================================
//==========================================================================

//==========================================================================
// BACK IN STOCK SANOGA
//==========================================================================

$template_text =
    <<<HTML
    <meta charset="utf-8"/>
<div class="mail-container" style="width:600px; padding:0px 33px; font-family:  'Trebuchet MS', Tahoma; font-size: 14px; color: #3c3d41; margin: 0px;">
	<div class="mail-header" style="width:95%; min-height: 110px; max-height: 110px; position: relative; margin:0px auto;">
		<div class="mail-logo" style="width:220px; min-height: 110px; max-height: 110px; margin:0px auto;">
			<a href="http://sanoga.com/"><img src="{{skin url="images/email/sanoga-logo.png"}}" style="width:220px; max-height: 110px; min-height: 110px;" /></a>
		</div>

		<div class="english-below" style="min-height: 16px; max-height: 16px; position: absolute; bottom: 0px; line-height: 16px;">
			<span><img src="{{skin url="images/email/flag-en.png"}}" height="16px;"/> Scroll down for English version</span>
		</div>
	</div>

	<img style="width: 100%; margin-top:40px;" src="{{skin url="images/email/its_backinstock_thai.png"}}" alt="Reset Password." />
	<div class="product-info" style="width: 100%; margin:5px auto; padding:10px; border: 1px solid #e7e7e7;">
		<div class="product-image" style="width: 36%; padding: 5px; border: 1px solid #e7e7e7; float: left;">
			<a href="{{var product_url}}?utm_source=edm&amp;utm_medium=reminder&amp;utm_campaign=sanoga_outofstock_reminder"><img src="{{var product_image}}" alt="{{var product}}" style="border: none; width: 95%;"/></a>
		</div>

		<div class="product-detail" style="float: right; width: 60%; text-align: center; margin-top: 15px;">
			<p><strong>{{var product}}</strong></p>
			<p><strong>{{var product_price}}</strong></p>
			<a href="{{var product_url}}?utm_source=edm&amp;utm_medium=reminder&amp;utm_campaign=sanoga_outofstock_reminder" style=" background: #5FBD54;color: #fff;padding: 10px;text-decoration: none;" >สั่งซื้อตอนนี้เลย</a>
		</div>

		<div style="clear: both;"></div>
	</div>
	<div class="mail-body" style="margin:50px auto; width: 95%;">
		<p>เรียน คุณ {{var customer.name}},</p>
		<br/>

		<p>คุณได้ทำการเพิ่มสินค้า {{var product}} เมื่อวันที่ {{var date}} เข้าไปยังตะกร้าสินค้าก่อนหน้านี้ </p>
		<p>เรามีความยินดีจะแจ้งว่าสินค้าตอนนี้ได้กลับเข้าสู่สต็อกสินค้าแล้ว</p>
		<br>

		<p>Warm Regards,</p>
		<p>Moxy Team</p>
	</div>

	<div class="navigation-th" style="margin-top: 24px; text-align:center; overflow: hidden;">
		<div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #6abc45; border-bottom: 2px solid #6abc45; padding: 0px 0px;">
			<span class="nav-item" style="border-right: 2px solid #6abc45; line-height: 33px; font-size: 14; padding: 0px 12px;"><a href="http://www.sanoga.com/th/health-care.html" style="color: #3c3d41; text-decoration: none;">ดูแลสุขภาพ</a></span>
			<span class="nav-item" style="border-right: 2px solid #6abc45; line-height: 33px; font-size: 14; padding: 0px 12px;"><a href="http://www.sanoga.com/th/beauty.html" style="color: #3c3d41; text-decoration: none;">ความงาม</a></span>
			<span class="nav-item" style="border-right: 2px solid #6abc45; line-height: 33px; font-size: 14; padding: 0px 12px;"><a href="http://www.sanoga.com/th/workout-fitness.html" style="color: #3c3d41; text-decoration: none;">ออกกำลังกาย-ฟิตเนส</a></span>
			<span class="nav-item" style="border-right: 2px solid #6abc45; line-height: 33px; font-size: 14; padding: 0px 12px;"><a href="http://www.sanoga.com/th/weight-loss.html" style="color: #3c3d41; text-decoration: none;">อาหารเสริมลดน้ำหนัก</a></span>
			<span class="nav-item" style="line-height: 33px; font-size: 14; padding: 0px 12px;"><a href="http://www.sanoga.com/th/sexual-well-being.html" style="color: #3c3d41; text-decoration: none;">สุขภาพ-เซ็กส์</a></span>
		</div>
	</div>

	<div class="email-footer" style="background: #bbb; margin-top: 8px; color: #fff; padding: 0px 15px;">
		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px"><img src="{{skin url="images/email/mailbox.png"}}" style="width:18px;"/> ติดต่อเรา</p>
			<p style="font-size: 10px; margin: 0;">02-106-8222</p>
			<p style="margin: 0; font-size: 10px;">support@sanoga.com</p>
		</div>

		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">ติดตามที่</p>
			<div class="follow-icons" style="width: 100%">
				<a href="https://www.facebook.com/SanogaThailand"><img src="{{skin url="images/email/facebook_follow.png"}}" style="width: 25px;" alt="Facebook fanpage"></a>
				<a href="https://twitter.com/sanoga_thailand"><img src="{{skin url="images/email/twitter_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
				<a href="#"><img src="{{skin url="images/email/instagram_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
				<a href="https://www.pinterest.com/sanoga/"><img src="{{skin url="images/email/pinterest_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
				<a href="https://plus.google.com/114390899173698565828/"><img src="{{skin url="images/email/google_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
			</div>
		</div>

		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">ช่องทางการชำระเงิน</p>
			<img src="{{skin url="images/email/payment-th.png"}}" alt="ช่องทางการชำระเงิน" style="width: 109px;">
		</div>

		<div style="clear:both"></div>
	</div>

	<div class="footer" style="padding: 0px 33px; margin-top: 18px;">
		<div class="copyr" style="float: left; font-size: 12px;">&copy;2015 Whatsnew Co.,Ltd. All Rights Reserved</div>
		<div class="links" style="float: right; font-size: 12px;">
			<a href="http://www.sanoga.com/th/about-us/" style="color: #3c3d41;"><span>เกี่ยวกับเรา</span></a> |
			<a href="http://www.sanoga.com/th/terms-and-conditions/" style="color: #3c3d41;"><span>ข้อกำหนดและเงื่อนไข</span></a> |
			<a href="http://www.sanoga.com/th/privacy-policy/" style="color: #3c3d41;"><span>ความเป็นส่วนตัว</span></a>
		</div>
	</div>
	<div style="clear:both"></div>
	<hr style="margin: 40px auto; width: 95%;">

	<!--English Version-->
	<img style="width: 100%; margin-bottom:40px;" src="{{skin url="images/email/its_backinstock_en.png"}}" alt="Reset Password." />
	<div class="product-info" style="width: 100%; margin:5px auto; padding:10px; border: 1px solid #e7e7e7;">
		<div class="product-image" style="width: 36%; padding: 5px; border: 1px solid #e7e7e7; float: left;">
			<a href="{{var product_url}}?utm_source=edm&amp;utm_medium=reminder&amp;utm_campaign=sanoga_outofstock_reminder"><img src="{{var product_image}}" alt="{{var product}}" style="border: none; width: 95%;"/></a>
		</div>

		<div class="product-detail" style="float: right; width: 60%; text-align: center; margin-top: 15px;">
			<p><strong>{{var product}}</strong></p>
			<p><strong>{{var product_price}}</strong></p>
			<a href="{{var product_url}}?utm_source=edm&amp;utm_medium=reminder&amp;utm_campaign=sanoga_outofstock_reminder" style=" background: #5FBD54;color: #fff;padding: 10px;text-decoration: none;" >BUY IT NOW</a>
		</div>

		<div style="clear: both;"></div>
	</div>
	<div class="mail-body" style="margin:50px auto; width: 95%;">
		<p>Dear {{var customer.name}},</p>
		<br/>

		<p>On {{var date}} , you tried to purchase {{var product}}. </p>
		<p>We have a good news for you, it is back in stock.</p>
		<br>
		<p>Get your hands on it now while stock lasts.</p>

		<p>Warm Regards,</p>
		<p>Moxy Team</p>
	</div>

	<div class="navigation-th" style="margin-top: 24px; text-align:center; overflow: hidden">
		<div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #6abc45; border-bottom: 2px solid #6abc45; padding: 0px 0px;">
			<span class="nav-item" style="border-right: 2px solid #6abc45; line-height: 33px; font-size: 14; padding: 0px 12px;"><a href="http://www.sanoga.com/en/health-care.html" style="color: #3c3d41; text-decoration: none;">Health Care</a></span>
			<span class="nav-item" style="border-right: 2px solid #6abc45; line-height: 33px; font-size: 14; padding: 0px 12px;"><a href="http://www.sanoga.com/en/beauty.html" style="color: #3c3d41; text-decoration: none;">Beauty</a></span>
			<span class="nav-item" style="border-right: 2px solid #6abc45; line-height: 33px; font-size: 14; padding: 0px 12px;"><a href="http://www.sanoga.com/en/workout-fitness.html" style="color: #3c3d41; text-decoration: none;">Workout & Fitness</a></span>
			<span class="nav-item" style="border-right: 2px solid #6abc45; line-height: 33px; font-size: 14; padding: 0px 12px;"><a href="http://www.sanoga.com/en/weight-loss.html" style="color: #3c3d41; text-decoration: none;">Weight Loss</a></span>
			<span class="nav-item" style="line-height: 33px; font-size: 14; padding: 0px 12px;"><a href="http://www.sanoga.com/en/sexual-well-being.html" style="color: #3c3d41; text-decoration: none;">Sexual Well-Beign</a></span>
		</div>
	</div>

	<div class="email-footer" style="background: #bbb; margin-top: 8px; color: #fff; padding: 0px 15px;">
		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px"><img src="{{skin url="images/email/mailbox.png"}}" style="width:18px;"/> Contact Us</p>
			<p style="font-size: 10px; margin: 0;">02-106-8222</p>
			<p style="margin: 0; font-size: 10px;">support@sanoga.com</p>
		</div>

		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">Follow Us</p>
			<div class="follow-icons" style="width: 100%">
				<a href="https://www.facebook.com/SanogaThailand"><img src="{{skin url="images/email/facebook_follow.png"}}" style="width: 25px;" alt="Facebook fanpage"></a>
				<a href="https://twitter.com/sanoga_thailand"><img src="{{skin url="images/email/twitter_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
				<a href="#"><img src="{{skin url="images/email/instagram_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
				<a href="https://www.pinterest.com/sanoga/"><img src="{{skin url="images/email/pinterest_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
				<a href="https://plus.google.com/114390899173698565828/"><img src="{{skin url="images/email/google_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
			</div>
		</div>

		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">Payment Options</p>
			<img src="{{skin url="images/email/payment.png"}}" alt="Payment Options" style="width: 109px;">
		</div>

		<div style="clear:both"></div>
	</div>

	<div class="footer" style="padding: 0px 33px; margin-top: 18px;">
		<div class="copyr" style="float: left; font-size: 12px;">&copy;2015 Whatsnew Co.,Ltd. All Rights Reserved</div>
		<div class="links" style="float: right; font-size: 12px;">
			<a href="http://www.sanoga.com/en/about-us/" style="color: #3c3d41;"><span>About Us</span></a> |
			<a href="http://www.sanoga.com/en/terms-and-conditions/" style="color: #3c3d41;"><span>Term & Conditions</span></a> |
			<a href="http://www.sanoga.com/en/privacy-policy/" style="color: #3c3d41;"><span>Privacy Policy</span></a>
		</div>
	</div>
	<div style="clear:both"></div>
</div>
HTML;

/** @var Mage_Core_Model_Email_Template $model */
$model = Mage::getModel('core/email_template');
$model->loadByCode('Back in Stock Sanoga')
    ->setTemplateCode('Back in Stock Sanoga')
    ->setTemplateText($template_text)
    ->setTemplateSubject('{{var product}} is back in stock!')
    ->setTemplateType(Mage_Core_Model_Email_Template::TYPE_HTML);

try {
    $model->save();
    $config->saveConfig('outofstocksubscription/mail/template', $model->getId() , 'stores', $sen);
    $config->saveConfig('outofstocksubscription/mail/template', $model->getId() , 'stores', $sth);
} catch (Mage_Exception $e) {
    throw $e;
}
//==========================================================================
//==========================================================================

//==========================================================================
// BACK IN STOCK VENBI
//==========================================================================

$template_text =
    <<<HTML
    <meta charset="utf-8"/>
<div class="mail-container" style="width:600px; padding:0px 33px; font-family:  'Trebuchet MS', Tahoma; font-size: 14px; color: #3c3d41; margin: 0px;">
	<div class="mail-header" style="width:95%; min-height: 110px; max-height: 110px; position: relative; margin:0px auto;">
		<div class="mail-logo" style="width:220px; min-height: 110px; max-height: 110px; margin:0px auto;">
			<a href="http://venbi.com/"><img src="{{skin url="images/email/venbi-logo.png"}}" style="width:220px; max-height: 110px; min-height: 110px;" /></a>
		</div>

		<div class="english-below" style="min-height: 16px; max-height: 16px; position: absolute; bottom: 0px; line-height: 16px;">
			<span><img src="{{skin url="images/email/flag-en.png"}}" height="16px;"/> Scroll down for English version</span>
		</div>
	</div>

	<img style="width: 100%; margin-top:40px;" src="{{skin url="images/email/its_backinstock_thai.png"}}" alt="Reset Password." />
	<div class="product-info" style="width: 100%; margin:5px auto; padding:10px; border: 1px solid #e7e7e7;">
		<div class="product-image" style="width: 36%; padding: 5px; border: 1px solid #e7e7e7; float: left;">
			<a href="{{var product_url}}"><img src="{{var product_image}}" alt="{{var product}}" style="border: none; width: 95%;"/></a>
		</div>

		<div class="product-detail" style="float: right; width: 60%; text-align: center; margin-top: 15px;">
			<p><strong>{{var product}}</strong></p>
			<p><strong>{{var product_price}}</strong></p>
			<a href="{{var product_url}}" style=" background: #F6EB47;color: #0e80c0;padding: 10px;text-decoration: none;" >สั่งซื้อตอนนี้เลย</a>
		</div>

		<div style="clear: both;"></div>
	</div>
	<div class="mail-body" style="margin:50px auto; width: 95%;">
		<p>เรียน คุณ {{var customer.name}},</p>
		<br/>

		<p>คุณได้ทำการเพิ่มสินค้า {{var product}} เมื่อวันที่ {{var date}} เข้าไปยังตะกร้าสินค้าก่อนหน้านี้ </p>
		<p>เรามีความยินดีจะแจ้งว่าสินค้าตอนนี้ได้กลับเข้าสู่สต็อกสินค้าแล้ว</p>
		<br>

		<p>Warm Regards,</p>
		<p>Moxy Team</p>
	</div>

	<div class="navigation-th" style="margin-top: 24px; overflow: hidden;">
		<div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #f5ea14; border-bottom: 2px solid #f5ea14; padding: 0px 0px;">
			<span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 6px;"><a href="http://www.venbi.com/th/formula-nursing.html" style="color: #3c3d41; text-decoration: none;">นมและอุปกรณ์</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 5px;"><a href="http://www.venbi.com/th/diapers.html" style="color: #3c3d41; text-decoration: none;">ผ้าอ้อมสำเร็จรูป</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 5px;"><a href="http://www.venbi.com/th/bath-baby-care.html" style="color: #3c3d41; text-decoration: none;">ผลิตภัณฑ์อาบน้ำและเบบี้แคร์</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 5px;"><a href="http://www.venbi.com/th/toys.html" style="color: #3c3d41; text-decoration: none;">ของเล่น</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 5px;"><a href="http://www.venbi.com/th/clothing.html" style="color: #3c3d41; text-decoration: none;">เสื้อผ้า</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 5px;"><a href="http://www.venbi.com/th/gear.html" style="color: #3c3d41; text-decoration: none;">เตียงและรถเข็น</a></span>
			<span class="nav-item" style="line-height: 33px; font-size: 11; padding: 0px 6px;"><a href="http://www.venbi.com/th/mom-maternity.html" style="color: #3c3d41; text-decoration: none;">แม่-การตั้งครรภ์</a></span>
		</div>
	</div>

	<div class="email-footer" style="background: #bbb; margin-top: 8px; color: #fff; padding: 0px 15px;">
		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px"><img src="{{skin url="images/email/mailbox.png"}}" style="width:18px;"/> ติดต่อเรา</p>
			<p style="font-size: 10px; margin: 0;">02-106-8222</p>
			<p style="margin: 0; font-size: 10px;">support@venbi.com</p>
		</div>

		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">ติดตามที่</p>
			<div class="follow-icons" style="width: 100%">
				<a href="https://www.facebook.com/venbithai"><img src="{{skin url="images/email/facebook_follow.png"}}" style="width: 25px;" alt="Facebook fanpage"></a>
				<a href="https://twitter.com/venbithai"><img src="{{skin url="images/email/twitter_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
				<a href="http://webstagr.am/venbi_thailand"><img src="{{skin url="images/email/instagram_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
				<a href="https://www.pinterest.com/VenbiThailand/"><img src="{{skin url="images/email/pinterest_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
				<a href="https://plus.google.com/+VenbiThai"><img src="{{skin url="images/email/google_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
			</div>
		</div>

		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">ช่องทางการชำระเงิน</p>
			<img src="{{skin url="images/email/payment-th.png"}}" alt="ช่องทางการชำระเงิน" style="width: 109px;">
		</div>

		<div style="clear:both"></div>
	</div>

	<div class="footer" style="padding: 0px 33px; margin-top: 18px;">
		<div class="copyr" style="float: left; font-size: 12px;">&copy;2015 Whatsnew Co.,Ltd. All Rights Reserved</div>
		<div class="links" style="float: right; font-size: 12px;">
			<a href="http://www.venbi.com/th/about-us/" style="color: #3c3d41;"><span>เกี่ยวกับเรา</span></a> |
			<a href="http://www.venbi.com/th/terms-and-conditions/" style="color: #3c3d41;"><span>ข้อกำหนดและเงื่อนไข</span></a> |
			<a href="http://www.venbi.com/th/privacy-policy/" style="color: #3c3d41;"><span>ความเป็นส่วนตัว</span></a>
		</div>
	</div>
	<div style="clear:both"></div>
	<hr style="margin: 40px auto; width: 95%;">

	<!--English Version-->
	<img style="width: 100%; margin-bottom:40px;" src="{{skin url="images/email/its_backinstock_en.png"}}" alt="Reset Password." />
	<div class="product-info" style="width: 100%; margin:5px auto; padding:10px; border: 1px solid #e7e7e7;">
		<div class="product-image" style="width: 36%; padding: 5px; border: 1px solid #e7e7e7; float: left;">
			<a href="{{var product_url}}"><img src="{{var product_image}}" alt="{{var product}}" style="border: none; width: 95%;"/></a>
		</div>

		<div class="product-detail" style="float: right; width: 60%; text-align: center; margin-top: 15px;">
			<p><strong>{{var product}}</strong></p>
			<p><strong>{{var product_price}}</strong></p>
			<a href="{{var product_url}}" style=" background: #F6EB47;color: #0e80c0;padding: 10px;text-decoration: none;" >BUY IT NOW</a>
		</div>

		<div style="clear: both;"></div>
	</div>
	<div class="mail-body" style="margin:50px auto; width: 95%;">
		<p>Dear {{var customer.name}},</p>
		<br/>

		<p>On {{var date}} , you tried to purchase {{var product}}. </p>
		<p>We have a good news for you, it is back in stock.</p>
		<br>
		<p>Get your hands on it now while stock lasts.</p>

		<p>Warm Regards,</p>
		<p>Moxy Team</p>
	</div>

	<div class="navigation-th" style="margin-top: 24px; overflow:hidden;">
		<div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #f5ea14; border-bottom: 2px solid #f5ea14; padding: 0px 0px; overflow: hidden;">
			<span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 5px;"><a href="http://www.venbi.com/en/formula-nursing.html" style="color: #3c3d41; text-decoration: none;">Formula & Nursing</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 5px;"><a href="http://www.venbi.com/en/diapers.html" style="color: #3c3d41; text-decoration: none;">Diapers</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 5px;"><a href="http://www.venbi.com/en/bath-baby-care.html" style="color: #3c3d41; text-decoration: none;">Bath & Baby Care</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 5px;"><a href="http://www.venbi.com/en/toys.html" style="color: #3c3d41; text-decoration: none;">Toys</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 5px;"><a href="http://www.venbi.com/en/clothing.html" style="color: #3c3d41; text-decoration: none;">Clothing</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 5px;"><a href="http://www.venbi.com/en/gear.html" style="color: #3c3d41; text-decoration: none;">Gear</a></span>
			<span class="nav-item" style="line-height: 33px; font-size: 11; padding: 0px 6px;"><a href="http://www.venbi.com/en/mom-maternity.html" style="color: #3c3d41; text-decoration: none;">Mom & Maternity</a></span>
		</div>
	</div>

	<div class="email-footer" style="background: #bbb; margin-top: 8px; color: #fff; padding: 0px 15px;">
		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px"><img src="{{skin url="images/email/mailbox.png"}}" style="width:18px;"/> Contact Us</p>
			<p style="font-size: 10px; margin: 0;">02-106-8222</p>
			<p style="margin: 0; font-size: 10px;">support@venbi.com</p>
		</div>

		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">Follow Us</p>
			<div class="follow-icons" style="width: 100%">
				<a href="https://www.facebook.com/venbithai"><img src="{{skin url="images/email/facebook_follow.png"}}" style="width: 25px;" alt="Facebook fanpage"></a>
				<a href="https://twitter.com/venbithai"><img src="{{skin url="images/email/twitter_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
				<a href="http://webstagr.am/venbi_thailand"><img src="{{skin url="images/email/instagram_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
				<a href="https://www.pinterest.com/VenbiThailand/"><img src="{{skin url="images/email/pinterest_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
				<a href="https://plus.google.com/+VenbiThai"><img src="{{skin url="images/email/google_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
			</div>
		</div>

		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">Payment Options</p>
			<img src="{{skin url="images/email/payment.png"}}" alt="Payment Options" style="width: 109px;">
		</div>

		<div style="clear:both"></div>
	</div>

	<div class="footer" style="padding: 0px 33px; margin-top: 18px;">
		<div class="copyr" style="float: left; font-size: 12px;">&copy;2015 Whatsnew Co.,Ltd. All Rights Reserved</div>
		<div class="links" style="float: right; font-size: 12px;">
			<a href="http://www.venbi.com/en/about-us/" style="color: #3c3d41;"><span>About Us</span></a> |
			<a href="http://www.venbi.com/en/terms-and-conditions/" style="color: #3c3d41;"><span>Term & Conditions</span></a> |
			<a href="http://www.venbi.com/en/privacy-policy/" style="color: #3c3d41;"><span>Privacy Policy</span></a>
		</div>
	</div>
	<div style="clear:both"></div>
</div>
HTML;

/** @var Mage_Core_Model_Email_Template $model */
$model = Mage::getModel('core/email_template');
$model->loadByCode('Back in Stock Venbi')
    ->setTemplateCode('Back in Stock Venbi')
    ->setTemplateText($template_text)
    ->setTemplateSubject('{{var product}} is back in stock!')
    ->setTemplateType(Mage_Core_Model_Email_Template::TYPE_HTML);

try {
    $model->save();
    $config->saveConfig('outofstocksubscription/mail/template', $model->getId() , 'stores', $ven);
    $config->saveConfig('outofstocksubscription/mail/template', $model->getId() , 'stores', $vth);
} catch (Mage_Exception $e) {
    throw $e;
}
//==========================================================================
//==========================================================================


//==========================================================================
// BACK IN STOCK SANOGA
//==========================================================================

$template_text =
    <<<HTML
    <meta charset="utf-8"/>
<div class="mail-container" style="width:600px; padding:0px 33px; font-family:  'Trebuchet MS', Tahoma; font-size: 14px; color: #3c3d41; margin: 0px;">
	<div class="mail-header" style="width:95%; min-height: 110px; max-height: 110px; position: relative; margin:0px auto;">
		<div class="mail-logo" style="width:220px; min-height: 110px; max-height: 110px; margin:0px auto;">
			<a href="http://sanoga.com/"><img src="{{skin url="images/email/sanoga-logo.png"}}" style="width:220px; max-height: 110px; min-height: 110px;" /></a>
		</div>

		<div class="english-below" style="min-height: 16px; max-height: 16px; position: absolute; bottom: 0px; line-height: 16px;">
			<span><img src="{{skin url="images/email/flag-en.png"}}" height="16px;"/> Scroll down for English version</span>
		</div>
	</div>

	<img style="width: 100%; margin-top:40px;" src="{{skin url="images/email/its_backinstock_thai.png"}}" alt="Reset Password." />
	<div class="product-info" style="width: 100%; margin:5px auto; padding:10px; border: 1px solid #e7e7e7;">
		<div class="product-image" style="width: 36%; padding: 5px; border: 1px solid #e7e7e7; float: left;">
			<a href="{{var product_url}}?utm_source=edm&amp;utm_medium=reminder&amp;utm_campaign=sanoga_outofstock_reminder"><img src="{{var product_image}}" alt="{{var product}}" style="border: none; width: 95%;"/></a>
		</div>

		<div class="product-detail" style="float: right; width: 60%; text-align: center; margin-top: 15px;">
			<p><strong>{{var product}}</strong></p>
			<p><strong>{{var product_price}}</strong></p>
			<a href="{{var product_url}}?utm_source=edm&amp;utm_medium=reminder&amp;utm_campaign=sanoga_outofstock_reminder" style=" background: #5FBD54;color: #fff;padding: 10px;text-decoration: none;" >สั่งซื้อตอนนี้เลย</a>
		</div>

		<div style="clear: both;"></div>
	</div>
	<div class="mail-body" style="margin:50px auto; width: 95%;">
		<p>เรียน คุณ {{var customer.name}},</p>
		<br/>

		<p>คุณได้ทำการเพิ่มสินค้า {{var product}} เมื่อวันที่ {{var date}} เข้าไปยังตะกร้าสินค้าก่อนหน้านี้ </p>
		<p>เรามีความยินดีจะแจ้งว่าสินค้าตอนนี้ได้กลับเข้าสู่สต็อกสินค้าแล้ว</p>
		<br>

		<p>Warm Regards,</p>
		<p>Moxy Team</p>
	</div>

	<div class="navigation-th" style="margin-top: 24px; text-align:center; overflow: hidden;">
		<div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #6abc45; border-bottom: 2px solid #6abc45; padding: 0px 0px;">
			<span class="nav-item" style="border-right: 2px solid #6abc45; line-height: 33px; font-size: 14; padding: 0px 12px;"><a href="http://www.sanoga.com/th/health-care.html" style="color: #3c3d41; text-decoration: none;">ดูแลสุขภาพ</a></span>
			<span class="nav-item" style="border-right: 2px solid #6abc45; line-height: 33px; font-size: 14; padding: 0px 12px;"><a href="http://www.sanoga.com/th/beauty.html" style="color: #3c3d41; text-decoration: none;">ความงาม</a></span>
			<span class="nav-item" style="border-right: 2px solid #6abc45; line-height: 33px; font-size: 14; padding: 0px 12px;"><a href="http://www.sanoga.com/th/workout-fitness.html" style="color: #3c3d41; text-decoration: none;">ออกกำลังกาย-ฟิตเนส</a></span>
			<span class="nav-item" style="border-right: 2px solid #6abc45; line-height: 33px; font-size: 14; padding: 0px 12px;"><a href="http://www.sanoga.com/th/weight-loss.html" style="color: #3c3d41; text-decoration: none;">อาหารเสริมลดน้ำหนัก</a></span>
			<span class="nav-item" style="line-height: 33px; font-size: 14; padding: 0px 12px;"><a href="http://www.sanoga.com/th/sexual-well-being.html" style="color: #3c3d41; text-decoration: none;">สุขภาพ-เซ็กส์</a></span>
		</div>
	</div>

	<div class="email-footer" style="background: #bbb; margin-top: 8px; color: #fff; padding: 0px 15px;">
		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px"><img src="{{skin url="images/email/mailbox.png"}}" style="width:18px;"/> ติดต่อเรา</p>
			<p style="font-size: 10px; margin: 0;">02-106-8222</p>
			<p style="margin: 0; font-size: 10px;">support@sanoga.com</p>
		</div>

		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">ติดตามที่</p>
			<div class="follow-icons" style="width: 100%">
				<a href="https://www.facebook.com/SanogaThailand"><img src="{{skin url="images/email/facebook_follow.png"}}" style="width: 25px;" alt="Facebook fanpage"></a>
				<a href="https://twitter.com/sanoga_thailand"><img src="{{skin url="images/email/twitter_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
				<a href="#"><img src="{{skin url="images/email/instagram_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
				<a href="https://www.pinterest.com/sanoga/"><img src="{{skin url="images/email/pinterest_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
				<a href="https://plus.google.com/114390899173698565828/"><img src="{{skin url="images/email/google_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
			</div>
		</div>

		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">ช่องทางการชำระเงิน</p>
			<img src="{{skin url="images/email/payment-th.png"}}" alt="ช่องทางการชำระเงิน" style="width: 109px;">
		</div>

		<div style="clear:both"></div>
	</div>

	<div class="footer" style="padding: 0px 33px; margin-top: 18px;">
		<div class="copyr" style="float: left; font-size: 12px;">&copy;2015 Whatsnew Co.,Ltd. All Rights Reserved</div>
		<div class="links" style="float: right; font-size: 12px;">
			<a href="http://www.sanoga.com/th/about-us/" style="color: #3c3d41;"><span>เกี่ยวกับเรา</span></a> |
			<a href="http://www.sanoga.com/th/terms-and-conditions/" style="color: #3c3d41;"><span>ข้อกำหนดและเงื่อนไข</span></a> |
			<a href="http://www.sanoga.com/th/privacy-policy/" style="color: #3c3d41;"><span>ความเป็นส่วนตัว</span></a>
		</div>
	</div>
	<div style="clear:both"></div>
	<hr style="margin: 40px auto; width: 95%;">

	<!--English Version-->
	<img style="width: 100%; margin-bottom:40px;" src="{{skin url="images/email/its_backinstock_en.png"}}" alt="Reset Password." />
	<div class="product-info" style="width: 100%; margin:5px auto; padding:10px; border: 1px solid #e7e7e7;">
		<div class="product-image" style="width: 36%; padding: 5px; border: 1px solid #e7e7e7; float: left;">
			<a href="{{var product_url}}?utm_source=edm&amp;utm_medium=reminder&amp;utm_campaign=sanoga_outofstock_reminder"><img src="{{var product_image}}" alt="{{var product}}" style="border: none; width: 95%;"/></a>
		</div>

		<div class="product-detail" style="float: right; width: 60%; text-align: center; margin-top: 15px;">
			<p><strong>{{var product}}</strong></p>
			<p><strong>{{var product_price}}</strong></p>
			<a href="{{var product_url}}?utm_source=edm&amp;utm_medium=reminder&amp;utm_campaign=sanoga_outofstock_reminder" style=" background: #5FBD54;color: #fff;padding: 10px;text-decoration: none;" >BUY IT NOW</a>
		</div>

		<div style="clear: both;"></div>
	</div>
	<div class="mail-body" style="margin:50px auto; width: 95%;">
		<p>Dear {{var customer.name}},</p>
		<br/>

		<p>On {{var date}} , you tried to purchase {{var product}}. </p>
		<p>We have a good news for you, it is back in stock.</p>
		<br>
		<p>Get your hands on it now while stock lasts.</p>

		<p>Warm Regards,</p>
		<p>Moxy Team</p>
	</div>

	<div class="navigation-th" style="margin-top: 24px; text-align:center; overflow: hidden">
		<div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #6abc45; border-bottom: 2px solid #6abc45; padding: 0px 0px;">
			<span class="nav-item" style="border-right: 2px solid #6abc45; line-height: 33px; font-size: 14; padding: 0px 12px;"><a href="http://www.sanoga.com/en/health-care.html" style="color: #3c3d41; text-decoration: none;">Health Care</a></span>
			<span class="nav-item" style="border-right: 2px solid #6abc45; line-height: 33px; font-size: 14; padding: 0px 12px;"><a href="http://www.sanoga.com/en/beauty.html" style="color: #3c3d41; text-decoration: none;">Beauty</a></span>
			<span class="nav-item" style="border-right: 2px solid #6abc45; line-height: 33px; font-size: 14; padding: 0px 12px;"><a href="http://www.sanoga.com/en/workout-fitness.html" style="color: #3c3d41; text-decoration: none;">Workout & Fitness</a></span>
			<span class="nav-item" style="border-right: 2px solid #6abc45; line-height: 33px; font-size: 14; padding: 0px 12px;"><a href="http://www.sanoga.com/en/weight-loss.html" style="color: #3c3d41; text-decoration: none;">Weight Loss</a></span>
			<span class="nav-item" style="line-height: 33px; font-size: 14; padding: 0px 12px;"><a href="http://www.sanoga.com/en/sexual-well-being.html" style="color: #3c3d41; text-decoration: none;">Sexual Well-Beign</a></span>
		</div>
	</div>

	<div class="email-footer" style="background: #bbb; margin-top: 8px; color: #fff; padding: 0px 15px;">
		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px"><img src="{{skin url="images/email/mailbox.png"}}" style="width:18px;"/> Contact Us</p>
			<p style="font-size: 10px; margin: 0;">02-106-8222</p>
			<p style="margin: 0; font-size: 10px;">support@sanoga.com</p>
		</div>

		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">Follow Us</p>
			<div class="follow-icons" style="width: 100%">
				<a href="https://www.facebook.com/SanogaThailand"><img src="{{skin url="images/email/facebook_follow.png"}}" style="width: 25px;" alt="Facebook fanpage"></a>
				<a href="https://twitter.com/sanoga_thailand"><img src="{{skin url="images/email/twitter_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
				<a href="#"><img src="{{skin url="images/email/instagram_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
				<a href="https://www.pinterest.com/sanoga/"><img src="{{skin url="images/email/pinterest_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
				<a href="https://plus.google.com/114390899173698565828/"><img src="{{skin url="images/email/google_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
			</div>
		</div>

		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">Payment Options</p>
			<img src="{{skin url="images/email/payment.png"}}" alt="Payment Options" style="width: 109px;">
		</div>

		<div style="clear:both"></div>
	</div>

	<div class="footer" style="padding: 0px 33px; margin-top: 18px;">
		<div class="copyr" style="float: left; font-size: 12px;">&copy;2015 Whatsnew Co.,Ltd. All Rights Reserved</div>
		<div class="links" style="float: right; font-size: 12px;">
			<a href="http://www.sanoga.com/en/about-us/" style="color: #3c3d41;"><span>About Us</span></a> |
			<a href="http://www.sanoga.com/en/terms-and-conditions/" style="color: #3c3d41;"><span>Term & Conditions</span></a> |
			<a href="http://www.sanoga.com/en/privacy-policy/" style="color: #3c3d41;"><span>Privacy Policy</span></a>
		</div>
	</div>
	<div style="clear:both"></div>
</div>
HTML;

/** @var Mage_Core_Model_Email_Template $model */
$model = Mage::getModel('core/email_template');
$model->loadByCode('Back in Stock Sanoga')
    ->setTemplateCode('Back in Stock Sanoga')
    ->setTemplateText($template_text)
    ->setTemplateSubject('{{var product}} is back in stock!')
    ->setTemplateType(Mage_Core_Model_Email_Template::TYPE_HTML);

try {
    $model->save();
    $config->saveConfig('outofstocksubscription/mail/template', $model->getId() , 'stores', $sen);
    $config->saveConfig('outofstocksubscription/mail/template', $model->getId() , 'stores', $sth);
} catch (Mage_Exception $e) {
    throw $e;
}
//==========================================================================
//==========================================================================

//==========================================================================
// BACK IN STOCK LAFEMA
//==========================================================================

$template_text =
    <<<HTML
    <meta charset="utf-8"/>
<div class="mail-container" style="width:600px; padding:0px 33px; font-family:  'Trebuchet MS', Tahoma; font-size: 14px; color: #3c3d41; margin: 0px;">
	<div class="mail-header" style="width:95%; min-height: 110px; max-height: 110px; position: relative; margin:0px auto;">
		<div class="mail-logo" style="width:220px; min-height: 110px; max-height: 110px; margin:0px auto;">
			<a href="http://lafema.com/"><img src="{{skin url="images/email/lafema-logo.png"}}" style="width:220px; max-height: 110px; min-height: 110px;" /></a>
		</div>

		<div class="english-below" style="min-height: 16px; max-height: 16px; position: absolute; bottom: 0px; line-height: 16px;">
			<span><img src="{{skin url="images/email/flag-en.png"}}" height="16px;"/> Scroll down for English version</span>
		</div>
	</div>

	<img style="width: 100%; margin-top:40px;" src="{{skin url="images/email/its_backinstock_thai.png"}}" alt="Reset Password." />
	<div class="product-info" style="width: 100%; margin:5px auto; padding:10px; border: 1px solid #e7e7e7;">
		<div class="product-image" style="width: 36%; padding: 5px; border: 1px solid #e7e7e7; float: left;">
			<a href="{{var product_url}}?utm_source=edm&amp;utm_medium=reminder&amp;utm_campaign=lafema_outofstock_reminder"><img src="{{var product_image}}" alt="{{var product}}" style="border: none; width: 95%;"/></a>
		</div>

		<div class="product-detail" style="float: right; width: 60%; text-align: center; margin-top: 15px;">
			<p><strong>{{var product}}</strong></p>
			<p><strong>{{var product_price}}</strong></p>
			<a href="{{var product_url}}?utm_source=edm&amp;utm_medium=reminder&amp;utm_campaign=lafema_outofstock_reminder" style=" background: #f5a2b2;color: #fff;padding: 10px;text-decoration: none;" >สั่งซื้อตอนนี้เลย</a>
		</div>

		<div style="clear: both;"></div>
	</div>
	<div class="mail-body" style="margin:50px auto; width: 95%;">
		<p>เรียน คุณ {{var customer.name}},</p>
		<br/>

		<p>คุณได้ทำการเพิ่มสินค้า {{var product}} เมื่อวันที่ {{var date}} เข้าไปยังตะกร้าสินค้าก่อนหน้านี้ </p>
		<p>เรามีความยินดีจะแจ้งว่าสินค้าตอนนี้ได้กลับเข้าสู่สต็อกสินค้าแล้ว</p>
		<br>

		<p>Warm Regards,</p>
		<p>Moxy Team</p>
	</div>

	<div class="navigation-th" style="margin-top: 24px; text-align:center; overflow:hidden;">
		<div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #f5a2b2; border-bottom: 2px solid #f5a2b2; padding: 0px 0px;">
			<span class="nav-item" style="border-right: 2px solid #f5a2b2; line-height: 33px; font-size: 16; padding: 0px 18px;"><a href="http://www.lafema.com/th/makeup.html" style="color: #3c3d41; text-decoration: none;">เมคอัพ</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5a2b2; line-height: 33px; font-size: 16; padding: 0px 18px;"><a href="http://www.lafema.com/th/skincare.html" style="color: #3c3d41; text-decoration: none;">ดูแลผิวหน้า</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5a2b2; line-height: 33px; font-size: 16; padding: 0px 18px;"><a href="http://www.lafema.com/th/bath-body-hair.html" style="color: #3c3d41; text-decoration: none;">ดูแลผิวกาย-ผม</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5a2b2; line-height: 33px; font-size: 16; padding: 0px 18px;"><a href="http://www.lafema.com/th/fragrance.html" style="color: #3c3d41; text-decoration: none;">น้ำหอม</a></span>
			<span class="nav-item" style="line-height: 33px; font-size: 16; padding: 0px 12px;"><a href="http://www.lafema.com/th/men.html" style="color: #3c3d41; text-decoration: none;">สำหรับผู้ชาย</a></span>
		</div>
	</div>

	<div class="email-footer" style="background: #bbb; margin-top: 8px; color: #fff; padding: 0px 15px;">
		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px"><img src="{{skin url="images/email/mailbox.png"}}" style="width:18px;"/> ติดต่อเรา</p>
			<p style="font-size: 10px; margin: 0;">02-106-8222</p>
			<p style="margin: 0; font-size: 10px;">support@lafema.com</p>
		</div>

		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">ติดตามที่</p>
			<div class="follow-icons" style="width: 100%">
				<a href="https://www.facebook.com/lafemathai"><img src="{{skin url="images/email/facebook_follow.png"}}" style="width: 25px;" alt="Facebook"></a>
				<a href="https://twitter.com/lafemath"><img src="{{skin url="images/email/twitter_follow.png"}}" style="width: 25px;" alt="Twitter"></a>
				<a href="https://instagram.com/lafema.th/"><img src="{{skin url="images/email/instagram_follow.png"}}" style="width: 25px;" alt="Instagram"></a>
				<a href="https://www.pinterest.com/lafema_thai/"><img src="{{skin url="images/email/pinterest_follow.png"}}" style="width: 25px;" alt="Pinterest"></a>
				<a href="https://plus.google.com/103314178165403417268/"><img src="{{skin url="images/email/google_follow.png"}}" style="width: 25px;" alt="G+"></a>
			</div>
		</div>

		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">ช่องทางการชำระเงิน</p>
			<img src="{{skin url="images/email/payment-th.png"}}" alt="ช่องทางการชำระเงิน" style="width: 109px;">
		</div>
		<div style="clear:both"></div>
	</div>

	<div class="footer" style="padding: 0px 33px; margin-top: 18px;">
		<div class="copyr" style="float: left; font-size: 12px;">&copy;2015 Whatsnew Co.,Ltd. All Rights Reserved</div>
		<div class="links" style="float: right; font-size: 12px;">
			<a href="http://www.lafema.com/th/about-us/" style="color: #3c3d41;"><span>เกี่ยวกับเรา</span></a> |
			<a href="http://www.lafema.com/th/terms-and-conditions/" style="color: #3c3d41;"><span>ข้อกำหนดและเงื่อนไข</span></a> |
			<a href="http://www.lafema.com/th/privacy-policy/" style="color: #3c3d41;"><span>ความเป็นส่วนตัว</span></a>
		</div>
	</div>
	<div style="clear:both"></div>
	<hr style="margin: 40px auto; width: 95%;">

	<!--English Version-->
	<img style="width: 100%; margin-bottom:40px;" src="{{skin url="images/email/its_backinstock_en.png"}}" alt="Reset Password." />
	<div class="product-info" style="width: 100%; margin:5px auto; padding:10px; border: 1px solid #e7e7e7;">
		<div class="product-image" style="width: 36%; padding: 5px; border: 1px solid #e7e7e7; float: left;">
			<a href="{{var product_url}}?utm_source=edm&amp;utm_medium=reminder&amp;utm_campaign=lafema_outofstock_reminder"><img src="{{var product_image}}" alt="{{var product}}" style="border: none; width: 95%;"/></a>
		</div>

		<div class="product-detail" style="float: right; width: 60%; text-align: center; margin-top: 15px;">
			<p><strong>{{var product}}</strong></p>
			<p><strong>{{var product_price}}</strong></p>
			<a href="{{var product_url}}?utm_source=edm&amp;utm_medium=reminder&amp;utm_campaign=lafema_outofstock_reminder" style=" background: #f5a2b2;color: #fff;padding: 10px;text-decoration: none;" >BUY IT NOW</a>
		</div>

		<div style="clear: both;"></div>
	</div>
	<div class="mail-body" style="margin:50px auto; width: 95%;">
		<p>Dear {{var customer.name}},</p>
		<br/>

		<p>On {{var date}} , you tried to purchase {{var product}}. </p>
		<p>We have a good news for you, it is back in stock.</p>
		<br>
		<p>Get your hands on it now while stock lasts.</p>

		<p>Warm Regards,</p>
		<p>Moxy Team</p>
	</div>

	<div class="navigation-th" style="margin-top: 24px; text-align:center; overflow:hidden;">
		<div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #f5a2b2; border-bottom: 2px solid #f5a2b2; padding: 0px 0px;">
			<span class="nav-item" style="border-right: 2px solid #f5a2b2; line-height: 33px; font-size: 16; padding: 0px 18px;"><a href="http://www.lafema.com/en/makeup.html" style="color: #3c3d41; text-decoration: none;">Makeup</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5a2b2; line-height: 33px; font-size: 16; padding: 0px 18px;"><a href="http://www.lafema.com/en/skincare.html" style="color: #3c3d41; text-decoration: none;">Skin Care</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5a2b2; line-height: 33px; font-size: 16; padding: 0px 18px;"><a href="http://www.lafema.com/en/bath-body-hair.html" style="color: #3c3d41; text-decoration: none;">Bath & Body-Hair</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5a2b2; line-height: 33px; font-size: 16; padding: 0px 18px;"><a href="http://www.lafema.com/en/fragrance.html" style="color: #3c3d41; text-decoration: none;">Fragrance</a></span>
			<span class="nav-item" style="line-height: 33px; font-size: 16; padding: 0px 18px;"><a href="http://www.lafema.com/en/men.html" style="color: #3c3d41; text-decoration: none;">Men</a></span>
		</div>
	</div>

	<div class="email-footer" style="min-height: 82px; max-heigh: 82px; background: #bbb; margin-top: 8px; color: #fff; padding: 0px 15px;">
		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px"><img src="{{skin url="images/email/mailbox.png"}}" style="width:18px;"/> Contact Us</p>
			<p style="font-size: 10px; margin: 0;">02-106-8222</p>
			<p style="margin: 0; font-size: 10px;">support@lafema.com</p>
		</div>

		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">Follow Us</p>
			<div class="follow-icons" style="width: 100%">
				<a href="https://www.facebook.com/lafemathai"><img src="{{skin url="images/email/facebook_follow.png"}}" style="width: 25px;" alt="Facebook"></a>
				<a href="https://twitter.com/lafemath"><img src="{{skin url="images/email/twitter_follow.png"}}" style="width: 25px;" alt="Twitter"></a>
				<a href="https://instagram.com/lafema.th/"><img src="{{skin url="images/email/instagram_follow.png"}}" style="width: 25px;" alt="Instagram"></a>
				<a href="https://www.pinterest.com/lafema_thai/"><img src="{{skin url="images/email/pinterest_follow.png"}}" style="width: 25px;" alt="Pinterest"></a>
				<a href="https://plus.google.com/103314178165403417268/"><img src="{{skin url="images/email/google_follow.png"}}" style="width: 25px;" alt="G+"></a>
			</div>
		</div>

		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">Payment Options</p>
			<img src="{{skin url="images/email/payment.png"}}" alt="Payment Options" style="width: 109px;">
		</div>
		<div style="clear:both"></div>
	</div>

	<div class="footer" style="padding: 0px 33px; margin-top: 18px;">
		<div class="copyr" style="float: left; font-size: 12px;">&copy;2015 Whatsnew Co.,Ltd. All Rights Reserved</div>
		<div class="links" style="float: right; font-size: 12px;">
			<a href="http://www.lafema.com/en/about-us/" style="color: #3c3d41;"><span>About Us</span></a> |
			<a href="http://www.lafema.com/en/terms-and-conditions/" style="color: #3c3d41;"><span>Term & Conditions</span></a> |
			<a href="http://www.lafema.com/en/privacy-policy/" style="color: #3c3d41;"><span>Privacy Policy</span></a>
		</div>
	</div>
	<div style="clear:both"></div>
</div>
HTML;

/** @var Mage_Core_Model_Email_Template $model */
$model = Mage::getModel('core/email_template');
$model->loadByCode('Back in Stock Lafema')
    ->setTemplateCode('Back in Stock Lafema')
    ->setTemplateText($template_text)
    ->setTemplateSubject('{{var product}} is back in stock!')
    ->setTemplateType(Mage_Core_Model_Email_Template::TYPE_HTML);

try {
    $model->save();
    $config->saveConfig('outofstocksubscription/mail/template', $model->getId() , 'stores', $len);
    $config->saveConfig('outofstocksubscription/mail/template', $model->getId() , 'stores', $lth);
} catch (Mage_Exception $e) {
    throw $e;
}
//==========================================================================
//==========================================================================

