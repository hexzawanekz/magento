<?php

//==========================================================================
// How to Order Petloft
//==========================================================================

$template_text =  <<<HTML
<div class="mail-container" style="width: 600px; padding: 0px; font-family: Tahoma; font-size: 14px; color: #3c3d41; margin: 0px;">
<div class="mail-header" style="width: 95%; min-height: 110px; max-height: 110px; position: relative; margin: 0px auto;">
<div class="mail-logo" style="width: 220px; min-height: 110px; max-height: 110px; margin: 0px auto;"><a href="http://petloft.com/"><img style="width: 220px; max-height: 110px; min-height: 110px;" src="{{skin _area="frontend"  url="images/email/petloft-logo.png"}}" alt="" /></a></div>
</div>
<img style="width: 600px; margin-top: 20px;" src="{{skin _area="frontend"  url="images/email/how_to_order_heading.png"}}" alt="How To Order" /> <img style="width: 600px; margin-top: 20px;" src="{{skin _area="frontend"  url="images/email/order_step.png"}}" alt="Order Steps" /> <img style="width: 600px; margin: 2px 0px;" src="{{skin _area="frontend"  url="images/email/delivery.png"}}" alt="Order Steps" /> <img style="width: 600px;" src="{{skin _area="frontend"  url="images/email/try_it_now.png"}}" alt="TRY IT NOW!" />{{layout handle="newsletter_sample_products" sku="WhPed0000673,lemon fragrance,WhTin0304695" store="en" color="#0e80c0" }}
<div class="navigation-th" style="margin-top: 15px; overflow: hidden;">
<div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #006ab4; border-bottom: 2px solid #006ab4; padding: 0px 17px; overflow: hidden;"><span class="nav-item" style="border-right: 2px solid #006ab4; line-height: 33px; font-size: 18px; padding: 0px 28px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://petloft.com/th/dog-food.html">สุนัข</a></span> <span class="nav-item" style="border-right: 2px solid #006ab4; line-height: 33px; font-size: 18px; padding: 0px 28px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://petloft.com/th/cat-food.html">แมว</a></span> <span class="nav-item" style="border-right: 2px solid #006ab4; line-height: 33px; font-size: 18px; padding: 0px 28px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://petloft.com/th/fish-products.html">ปลา</a></span> <span class="nav-item" style="border-right: 2px solid #006ab4; line-height: 33px; font-size: 18px; padding: 0px 28px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://www.petloft.com/th/bird.html">นก</a></span> <span class="nav-item" style="line-height: 33px; font-size: 18px; padding: 0px 28px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://petloft.com/th/small-pets-products.html">สัตว์เลี้ยงขนาดเล็ก</a></span></div>
</div>
<div class="email-footer" style="background: #bbb; margin-top: 8px; color: #fff; padding: 0px 15px;">
<div class="footer-col" style="float: left; padding: 17px 24px; width: 141px;">
<p style="font-size: 14px; margin: 0px 0px 10px 0px;"><img style="width: 18px;" src="{{skin _area="frontend"  url="images/email/mailbox.png"}}" alt="" /> ติดต่อเรา</p>
<p style="font-size: 12px; margin: 0;">02-106-8222</p>
<p style="margin: 0; font-size: 12px;"><a style="font-size: 12px !important; color: #fff; text-decoration: none; font-weight: normal !important;" href="mailto:support@petloft.com" target="_blank">support@petloft.com</a></p>
</div>
<div class="footer-col" style="float: left; padding: 17px 10px; width: 164px;">
<p style="font-size: 14px; margin: 0px 0px 10px 0px;">ติดตามที่</p>
<div class="follow-icons" style="width: 100%;"><a href="https://www.facebook.com/petloftthai"><img style="width: 25px;" src="{{skin _area="frontend"  url="images/email/facebook_follow.png"}}" alt="Facebook fanpage" /></a> <a href="https://twitter.com/PetLoft"><img style="width: 25px;" src="{{skin _area="frontend"  url="images/email/twitter_follow.png"}}" alt="Twitter fanpage" /></a> <a href="https://instagram.com/petloft"><img style="width: 25px;" src="{{skin _area="frontend"  url="images/email/instagram_follow.png"}}" alt="Instagram fanpage" /></a> <a href="https://www.pinterest.com/petloft/"><img style="width: 25px;" src="{{skin _area="frontend"  url="images/email/pinterest_follow.png"}}" alt="Pinterest fanpage" /></a> <a href="https://plus.google.com/110158894140498579470"><img style="width: 25px;" src="{{skin _area="frontend"  url="images/email/google_follow.png"}}" alt="Google fanpage" /></a></div>
</div>
<div class="footer-col" style="float: left; padding: 17px 24px; width: 141px;">
<p style="font-size: 14px; margin: 0px 0px 5px 0px;">ช่องทางการชำระเงิน</p>
<img style="width: 109px;" src="{{skin _area="frontend"  url="images/email/payment-th.png"}}" alt="ช่องทางการชำระเงิน" /></div>
<div class="clear" style="clear: both; max-height: 0px;">&nbsp;</div>
</div>
<div class="footer" style="padding: 0px 33px; margin-top: 18px;">
<div class="copyr" style="float: left; font-size: 11px;">&copy;2015 Whatsnew Co.,Ltd. All Rights Reserved</div>
<div class="links" style="float: right; font-size: 11px;"><a style="color: #3c3d41; text-decoration: none;" href="http://www.petloft.com/th/about-us/"><span>เกี่ยวกับเรา</span></a> | <a style="color: #3c3d41; text-decoration: none;" href="http://www.petloft.com/th/terms-and-conditions/"><span>ข้อกำหนดและเงื่อนไข</span></a> | <a style="color: #3c3d41; text-decoration: none;" href="http://www.petloft.com/th/privacy-policy/"><span>ความเป็นส่วนตัว</span></a></div>
</div>
</div>
HTML;



$model = Mage::getModel('newsletter/template');
$model->loadByCode('HOW TO ORDER PETLOFT')
    ->setTemplateCode('How to Order Petloft')
    ->setTemplateText($template_text)
    ->setTemplateSubject('How to Order!')
    ->setTemplateType(Mage_Core_Model_Email_Template::TYPE_HTML)
    ->setTemplateSenderName('Petloft Customer Service')
    ->setTemplateSenderEmail('support@petloft.com');

try {
    $model->save();
} catch (Mage_Exception $e) {
    throw $e;
}
//==========================================================================
//==========================================================================

//==========================================================================
// How to Order Moxy
//==========================================================================

$template_text =  <<<HTML
<div class="mail-container" style="width: 600px; padding: 0px; font-family: Tahoma; font-size: 14px; color: #3c3d41; margin: 0px;">
<div class="mail-header" style="width: 95%; min-height: 110px; max-height: 110px; position: relative; margin: 0px auto;">
<div class="mail-logo" style="width: 220px; min-height: 110px; max-height: 110px; margin: 0px auto;"><a href="http://moxy.co.th/"><img style="width: 220px; max-height: 110px; min-height: 110px;" src="{{skin _area="frontend"  url="images/email/moxy-logo.png"}}" alt="" /></a></div>
</div>
<img style="width: 600px; margin-top: 20px;" src="{{skin _area="frontend"  url="images/email/how_to_order_heading.png"}}" alt="How To Order" /> <img style="width: 600px; margin-top: 20px;" src="{{skin _area="frontend"  url="images/email/order_step_moxy.png"}}" alt="Order Steps" /> <img style="width: 600px; margin: 2px 0px;" src="{{skin _area="frontend"  url="images/email/delivery_moxy.png"}}" alt="Order Steps" /> <img style="width: 600px;" src="{{skin _area="frontend"  url="images/email/try_it_now.png"}}" alt="TRY IT NOW!" />{{layout handle="newsletter_sample_products" sku="WhPed0000673,lemon fragrance,WhTin0304695" store="moxyen" color="#ee5191" }}
<div class="navigation-th" style="margin-top: 15px; overflow: hidden; text-align: center;">
<div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #ee5191; border-bottom: 2px solid #ee5191; padding: 0px 11px;"><span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 16; padding: 0px 19px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://www.moxy.co.th/th/fashion.html">แฟชั่น</a></span> <span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 16; padding: 0px 19px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://www.moxy.co.th/th/gadgets-electronics.html">แก็ดเจ็ต</a></span> <span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 16; padding: 0px 19px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://www.moxy.co.th/th/accessories.html">เครื่องประดับ</a></span> <span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 16; padding: 0px 19px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://www.moxy.co.th/en/moxy/homeliving">ของแต่งบ้าน</a></span> <span class="nav-item" style="line-height: 33px; font-size: 16; padding: 0px 19px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://www.moxy.co.th/th/lifestyle.html">ไลฟ์สไตล์</a></span></div>
</div>
<div class="email-footer" style="background: #bbb; margin-top: 8px; color: #fff; padding: 0px 15px;">
<div class="footer-col" style="float: left; padding: 17px 24px; width: 141px;">
<p style="font-size: 14px; margin: 0px 0px 10px 0px;"><img style="width: 18px;" src="{{skin _area="frontend"  url="images/email/mailbox.png"}}" alt="" /> ติดต่อเรา</p>
<p style="font-size: 12px; margin: 0;">02-106-8222</p>
<p style="margin: 0; font-size: 12px;"><a style="font-size: 12px !important; color: #fff; text-decoration: none; font-weight: normal !important;" href="mailto:support@moxyst.com" target="_blank">support@moxyst.com</a></p>
</div>
<div class="footer-col" style="float: left; padding: 17px 10px; width: 164px;">
<p style="font-size: 14px; margin: 0px 0px 10px 0px;">ติดตามที่</p>
<div class="follow-icons" style="width: 100%;"><a href="https://www.facebook.com/moxyst"><img style="width: 25px;" src="{{skin _area="frontend"  url="images/email/facebook_follow.png"}}" alt="Facebook fanpage" /></a> <a href="https://twitter.com/moxyst"><img style="width: 25px;" src="{{skin _area="frontend"  url="images/email/twitter_follow.png"}}" alt="Twitter fanpage" /></a> <a href="https://instagram.com/moxy_th/"><img style="width: 25px;" src="{{skin _area="frontend"  url="images/email/instagram_follow.png"}}" alt="Instagram fanpage" /></a> <a href="https://www.pinterest.com/MoxySEA1/"><img style="width: 25px;" src="{{skin _area="frontend"  url="images/email/pinterest_follow.png"}}" alt="Pinterest fanpage" /></a> <a href="https://plus.google.com/+Moxyst/"><img style="width: 25px;" src="{{skin _area="frontend"  url="images/email/google_follow.png"}}" alt="Google fanpage" /></a></div>
</div>
<div class="footer-col" style="float: left; padding: 17px 24px; width: 141px;">
<p style="font-size: 14px; margin: 0px 0px 5px 0px;">ช่องทางการชำระเงิน</p>
<img style="width: 109px;" src="{{skin _area="frontend"  url="images/email/payment-th.png"}}" alt="ช่องทางการชำระเงิน" /></div>
<div class="clear" style="clear: both; max-height: 0px;">&nbsp;</div>
</div>
<div class="footer" style="padding: 0px 33px; margin-top: 18px;">
<div class="copyr" style="float: left; font-size: 11px;">&copy;2015 Whatsnew Co.,Ltd. All Rights Reserved</div>
<div class="links" style="float: right; font-size: 11px;"><a style="color: #3c3d41; text-decoration: none;" href="http://www.moxy.co.th/th/about-us/"><span>เกี่ยวกับเรา</span></a> | <a style="color: #3c3d41; text-decoration: none;" href="http://www.moxy.co.th/th/terms-and-conditions/"><span>ข้อกำหนดและเงื่อนไข</span></a> | <a style="color: #3c3d41; text-decoration: none;" href="http://www.moxy.co.th/th/privacy-policy/"><span>ความเป็นส่วนตัว</span></a></div>
</div>
</div>
HTML;



$model = Mage::getModel('newsletter/template');
$model->loadByCode('HOW TO ORDER MOXY')
    ->setTemplateCode('How to Order Moxy')
    ->setTemplateText($template_text)
    ->setTemplateSubject('How to Order!')
    ->setTemplateType(Mage_Core_Model_Email_Template::TYPE_HTML)
    ->setTemplateSenderName('Moxy Customer Service')
    ->setTemplateSenderEmail('support@moxyst.com');

try {
    $model->save();
} catch (Mage_Exception $e) {
    throw $e;
}
//==========================================================================
//==========================================================================

//==========================================================================
// How to Order Sanoga
//==========================================================================

$template_text =  <<<HTML
<div class="mail-container" style="width: 600px; padding: 0px; font-family: Tahoma; font-size: 14px; color: #3c3d41; margin: 0px;">
<div class="mail-header" style="width: 95%; min-height: 110px; max-height: 110px; position: relative; margin: 0px auto;">
<div class="mail-logo" style="width: 220px; min-height: 110px; max-height: 110px; margin: 0px auto;"><a href="http://sanoga.com/"><img style="width: 220px; max-height: 110px; min-height: 110px;" src="{{skin _area="frontend"  url="images/email/sanoga-logo.png"}}" alt="" /></a></div>
</div>
<img style="width: 600px; margin-top: 20px;" src="{{skin _area="frontend"  url="images/email/how_to_order_heading.png"}}" alt="How To Order" /> <img style="width: 600px; margin-top: 20px;" src="{{skin _area="frontend"  url="images/email/order_step_sanoga.png"}}" alt="Order Steps" /> <img style="width: 600px; margin: 2px 0px;" src="{{skin _area="frontend"  url="images/email/delivery_sanoga.png"}}" alt="Order Steps" /> <img style="width: 600px;" src="{{skin _area="frontend"  url="images/email/try_it_now.png"}}" alt="TRY IT NOW!" />{{layout handle="newsletter_sample_products" sku="WhPed0000673,lemon fragrance,WhTin0304695" store="sen" color="#6abc45" }}
<div class="navigation-th" style="margin-top: 15px; text-align: center; overflow: hidden;">
<div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #6abc45; border-bottom: 2px solid #6abc45; padding: 0px 0px; overflow: hidden;"><span class="nav-item" style="border-right: 2px solid #6abc45; line-height: 33px; font-size: 14; padding: 0px 12px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://www.sanoga.com/th/health-care.html">ดูแลสุขภาพ</a></span> <span class="nav-item" style="border-right: 2px solid #6abc45; line-height: 33px; font-size: 14; padding: 0px 12px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://www.sanoga.com/th/beauty.html">ความงาม</a></span> <span class="nav-item" style="border-right: 2px solid #6abc45; line-height: 33px; font-size: 14; padding: 0px 12px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://www.sanoga.com/th/workout-fitness.html">ออกกำลังกาย-ฟิตเนส</a></span> <span class="nav-item" style="border-right: 2px solid #6abc45; line-height: 33px; font-size: 14; padding: 0px 12px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://www.sanoga.com/th/weight-loss.html">อาหารเสริมลดน้ำหนัก</a></span> <span class="nav-item" style="line-height: 33px; font-size: 14; padding: 0px 12px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://www.sanoga.com/th/sexual-well-being.html">สุขภาพ-เซ็กส์</a></span></div>
</div>
<div class="email-footer" style="background: #bbb; margin-top: 8px; color: #fff; padding: 0px 15px;">
<div class="footer-col" style="float: left; padding: 17px 24px; width: 141px;">
<p style="font-size: 14px; margin: 0px 0px 10px 0px;"><img style="width: 18px;" src="{{skin _area="frontend"  url="images/email/mailbox.png"}}" alt="" /> ติดต่อเรา</p>
<p style="font-size: 12px; margin: 0;">02-106-8222</p>
<p style="margin: 0; font-size: 12px;"><a style="font-size: 12px !important; color: #fff; font-weight: normal !important;" href="mailto:support@sanoga.com" target="_blank">support@sanoga.com</a></p>
</div>
<div class="footer-col" style="float: left; padding: 17px 24px; width: 141px;">
<p style="font-size: 14px; margin: 0px 0px 10px 0px;">ติดตามที่</p>
<div class="follow-icons" style="width: 100%;"><a href="https://twitter.com/sanoga_thailand"><img style="width: 25px;" src="{{skin _area="frontend"  url="images/email/facebook_follow.png"}}" alt="Facebook fanpage" /></a> <a href="https://twitter.com/sanoga_thailand"><img style="width: 25px;" src="{{skin _area="frontend"  url="images/email/twitter_follow.png"}}" alt="Twitter fanpage" /></a> <a href="#"><img style="width: 25px;" src="{{skin _area="frontend"  url="images/email/instagram_follow.png"}}" alt="Instagram fanpage" /></a> <a href="https://www.pinterest.com/sanoga/"><img style="width: 25px;" src="{{skin _area="frontend"  url="images/email/pinterest_follow.png"}}" alt="Pinterest fanpage" /></a> <a href="https://plus.google.com/114390899173698565828/"><img style="width: 25px;" src="{{skin _area="frontend"  url="images/email/google_follow.png"}}" alt="Google fanpage" /></a></div>
</div>
<div class="footer-col" style="float: left; padding: 17px 24px; width: 141px;">
<p style="font-size: 14px; margin: 0px 0px 5px 0px;">ช่องทางการชำระเงิน</p>
<img style="width: 109px;" src="{{skin _area="frontend"  url="images/email/payment-th.png"}}" alt="ช่องทางการชำระเงิน" /></div>
<div class="clear" style="clear: both; max-height: 0px;">&nbsp;</div>
</div>
<div class="footer" style="padding: 0px 33px; margin-top: 18px; color: #3c3d41;">
<div class="copyr" style="float: left; font-size: 11px;">&copy;2015 Whatsnew Co.,Ltd. All Rights Reserved</div>
<div class="links" style="float: right; font-size: 11px;"><a style="color: #3c3d41; text-decoration: none;" href="http://www.sanoga.com/th/about-us/"><span>เกี่ยวกับเรา</span></a> | <a style="color: #3c3d41; text-decoration: none;" href="http://www.sanoga.com/th/terms-and-conditions/"><span>ข้อกำหนดและเงื่อนไข</span></a> | <a style="color: #3c3d41; text-decoration: none;" href="http://www.sanoga.com/th/privacy-policy/"><span>ความเป็นส่วนตัว</span></a></div>
</div>
</div>
HTML;



$model = Mage::getModel('newsletter/template');
$model->loadByCode('HOW TO ORDER SANOGA')
    ->setTemplateCode('How to Order Sanoga')
    ->setTemplateText($template_text)
    ->setTemplateSubject('How to Order!')
    ->setTemplateType(Mage_Core_Model_Email_Template::TYPE_HTML)
    ->setTemplateSenderName('Sanoga Customer Service')
    ->setTemplateSenderEmail('support@sanoga.com');

try {
    $model->save();
} catch (Mage_Exception $e) {
    throw $e;
}
//==========================================================================
//==========================================================================

//==========================================================================
// How to Order Lafema
//==========================================================================

$template_text =  <<<HTML
<div class="mail-container" style="width: 600px; padding: 0px; font-family: Tahoma; font-size: 14px; color: #3c3d41; margin: 0px;">
<div class="mail-header" style="width: 95%; min-height: 110px; max-height: 110px; position: relative; margin: 0px auto;">
<div class="mail-logo" style="width: 220px; min-height: 110px; max-height: 110px; margin: 0px auto;"><a href="http://lafema.com/"><img style="width: 220px; max-height: 110px; min-height: 110px;" src="{{skin _area="frontend"  url="images/email/lafema-logo.png"}}" alt="" /></a></div>
</div>
<img style="width: 600px; margin-top: 20px;" src="{{skin _area="frontend"  url="images/email/how_to_order_heading.png"}}" alt="How To Order" /> <img style="width: 600px; margin-top: 20px;" src="{{skin _area="frontend"  url="images/email/order_step_lafema.png"}}" alt="Order Steps" /> <img style="width: 600px; margin: 2px 0px;" src="{{skin _area="frontend"  url="images/email/delivery_lafema.png"}}" alt="Order Steps" /> <img style="width: 600px;" src="{{skin _area="frontend"  url="images/email/try_it_now.png"}}" alt="TRY IT NOW!" />{{layout handle="newsletter_sample_products" sku="WhPed0000673,lemon fragrance,WhTin0304695" store="en" color="#f5a2b2" }}
<div class="navigation-th" style="margin-top: 15px; text-align: center; overflow: hidden;">
<div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #f5a2b2; border-bottom: 2px solid #f5a2b2; padding: 0px 0px;"><span class="nav-item" style="border-right: 2px solid #f5a2b2; line-height: 33px; font-size: 16; padding: 0px 18px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://www.lafema.com/th/makeup.html">เมคอัพ</a></span> <span class="nav-item" style="border-right: 2px solid #f5a2b2; line-height: 33px; font-size: 16; padding: 0px 18px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://www.lafema.com/th/skincare.html">ดูแลผิวหน้า</a></span> <span class="nav-item" style="border-right: 2px solid #f5a2b2; line-height: 33px; font-size: 16; padding: 0px 18px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://www.lafema.com/th/bath-body-hair.html">ดูแลผิวกาย-ผม</a></span> <span class="nav-item" style="border-right: 2px solid #f5a2b2; line-height: 33px; font-size: 16; padding: 0px 18px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://www.lafema.com/th/fragrance.html">น้ำหอม</a></span> <span class="nav-item" style="line-height: 33px; font-size: 16; padding: 0px 12px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://www.lafema.com/th/men.html">สำหรับผู้ชาย</a></span></div>
</div>
<div class="email-footer" style="background: #bbb; margin-top: 8px; color: #fff; padding: 0px 15px;">
<div class="footer-col" style="float: left; padding: 17px 24px; width: 141px;">
<p style="font-size: 14px; margin: 0px 0px 10px 0px;"><img style="width: 18px;" src="{{skin _area="frontend"  url="images/email/mailbox.png"}}" alt="" /> ติดต่อเรา</p>
<p style="font-size: 12px; margin: 0;">02-106-8222</p>
<p style="margin: 0; font-size: 12px;"><a style="font-size: 12px !important; color: #fff; font-weight: normal !important;" href="mailto:support@lafema.com" target="_blank">support@lafema.com</a></p>
</div>
<div class="footer-col" style="float: left; padding: 17px 24px; width: 141px;">
<p style="font-size: 14px; margin: 0px 0px 10px 0px;">ติดตามที่</p>
<div class="follow-icons" style="width: 100%;"><a href="https://www.facebook.com/lafemathai"><img style="width: 25px;" src="{{skin _area="frontend"  url="images/email/facebook_follow.png"}}" alt="Facebook fanpage" /></a> <a href="https://twitter.com/lafemath"><img style="width: 25px;" src="{{skin _area="frontend"  url="images/email/twitter_follow.png"}}" alt="Twitter fanpage" /></a> <a href="https://instagram.com/lafema.th/"><img style="width: 25px;" src="{{skin _area="frontend"  url="images/email/instagram_follow.png"}}" alt="Instagram fanpage" /></a> <a href="https://www.pinterest.com/lafema_thai/"><img style="width: 25px;" src="{{skin _area="frontend"  url="images/email/pinterest_follow.png"}}" alt="Pinterest fanpage" /></a> <a href="https://plus.google.com/103314178165403417268/"><img style="width: 25px;" src="{{skin _area="frontend"  url="images/email/google_follow.png"}}" alt="Google fanpage" /></a></div>
</div>
<div class="footer-col" style="float: left; padding: 17px 24px; width: 141px;">
<p style="font-size: 14px; margin: 0px 0px 5px 0px;">ช่องทางการชำระเงิน</p>
<img style="width: 109px;" src="{{skin _area="frontend"  url="images/email/payment-th.png"}}" alt="ช่องทางการชำระเงิน" /></div>
<div class="clear" style="clear: both; max-height: 0px;">&nbsp;</div>
</div>
<div class="footer" style="padding: 0px 33px; margin-top: 18px; color: #3c3d41;">
<div class="copyr" style="float: left; font-size: 11px;">&copy;2015 Whatsnew Co.,Ltd. All Rights Reserved</div>
<div class="links" style="float: right; font-size: 11px;"><a style="color: #3c3d41; text-decoration: none;" href="http://www.lafema.com/th/about-us/"><span>เกี่ยวกับเรา</span></a> | <a style="color: #3c3d41; text-decoration: none;" href="http://www.lafema.com/th/terms-and-conditions/"><span>ข้อกำหนดและเงื่อนไข</span></a> | <a style="color: #3c3d41; text-decoration: none;" href="http://www.lafema.com/th/privacy-policy/"><span>ความเป็นส่วนตัว</span></a></div>
</div>
</div>
HTML;



$model = Mage::getModel('newsletter/template');
$model->loadByCode('HOW TO ORDER LAFEMA')
    ->setTemplateCode('How to Order Lafema')
    ->setTemplateText($template_text)
    ->setTemplateSubject('How to Order!')
    ->setTemplateType(Mage_Core_Model_Email_Template::TYPE_HTML)
    ->setTemplateSenderName('Lafema Customer Service')
    ->setTemplateSenderEmail('support@lafema.com');

try {
    $model->save();
} catch (Mage_Exception $e) {
    throw $e;
}
//==========================================================================
//==========================================================================

//==========================================================================
// How to Order Venbi
//==========================================================================

$template_text =  <<<HTML
<div class="mail-container" style="width: 600px; padding: 0px; font-family: Tahoma; font-size: 14px; color: #3c3d41; margin: 0px;">
<div class="mail-header" style="width: 95%; min-height: 110px; max-height: 110px; position: relative; margin: 0px auto;">
<div class="mail-logo" style="width: 220px; min-height: 110px; max-height: 110px; margin: 0px auto;"><a href="http://venbi.com/"><img style="width: 220px; max-height: 110px; min-height: 110px;" src="{{skin _area="frontend"  url="images/email/venbi-logo.png"}}" alt="" /></a></div>
</div>
<img style="width: 600px; margin-top: 20px;" src="{{skin _area="frontend"  url="images/email/how_to_order_heading.png"}}" alt="How To Order" /> <img style="width: 600px; margin-top: 20px;" src="{{skin _area="frontend"  url="images/email/order_step_venbi.png"}}" alt="Order Steps" /> <img style="width: 600px; margin: 2px 0px;" src="{{skin _area="frontend"  url="images/email/delivery_venbi.png"}}" alt="Order Steps" /> <img style="width: 600px;" src="{{skin _area="frontend"  url="images/email/try_it_now.png"}}" alt="TRY IT NOW!" />{{layout handle="newsletter_sample_products" sku="WhPed0000673,lemon fragrance,WhTin0304695" store="en" color="#f5ea14" }}
<div class="navigation-th" style="margin-top: 15px; overflow: hidden;">
<div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #f5ea14; border-bottom: 2px solid #f5ea14; padding: 0px 0px; overflow: hidden;"><span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 6px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://www.venbi.com/th/formula-nursing.html">นมและอุปกรณ์</a></span> <span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 5px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://www.venbi.com/th/diapers.html">ผ้าอ้อมสำเร็จรูป</a></span> <span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 5px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://www.venbi.com/th/bath-baby-care.html">ผลิตภัณฑ์อาบน้ำและเบบี้แคร์</a></span> <span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 5px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://www.venbi.com/th/toys.html">ของเล่น</a></span> <span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 5px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://www.venbi.com/th/clothing.html">เสื้อผ้า</a></span> <span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 5px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://www.venbi.com/th/gear.html">เตียงและรถเข็น</a></span> <span class="nav-item" style="line-height: 33px; font-size: 11; padding: 0px 6px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://www.venbi.com/th/mom-maternity.html">แม่-การตั้งครรภ์</a></span></div>
</div>
<div class="email-footer" style="background: #bbb; margin-top: 8px; color: #fff; padding: 0px 15px;">
<div class="footer-col" style="float: left; padding: 17px 24px; width: 141px;">
<p style="font-size: 14px; margin: 0px 0px 10px 0px;"><img style="width: 18px;" src="{{skin _area="frontend"  url="images/email/mailbox.png"}}" alt="" /> ติดต่อเรา</p>
<p style="font-size: 12px; margin: 0;">02-106-8222</p>
<p style="margin: 0; font-size: 12px;"><a style="font-size: 12px !important; color: #fff; font-weight: normal !important;" href="mailto:support@venbi.com" target="_blank">support@venbi.com</a></p>
</div>
<div class="footer-col" style="float: left; padding: 17px 24px; width: 141px;">
<p style="font-size: 14px; margin: 0px 0px 10px 0px;">ติดตามที่</p>
<div class="follow-icons" style="width: 100%;"><a href="https://www.facebook.com/venbithai"><img style="width: 25px;" src="{{skin _area="frontend"  url="images/email/facebook_follow.png"}}" alt="Facebook fanpage" /></a> <a href="https://twitter.com/venbithai"><img style="width: 25px;" src="{{skin _area="frontend"  url="images/email/twitter_follow.png"}}" alt="Twitter fanpage" /></a> <a href="http://webstagr.am/venbi_thailand"><img style="width: 25px;" src="{{skin _area="frontend"  url="images/email/instagram_follow.png"}}" alt="Instagram fanpage" /></a> <a href="https://www.pinterest.com/VenbiThailand/"><img style="width: 25px;" src="{{skin _area="frontend"  url="images/email/pinterest_follow.png"}}" alt="Pinterest fanpage" /></a> <a href="https://plus.google.com/+VenbiThai"><img style="width: 25px;" src="{{skin _area="frontend"  url="images/email/google_follow.png"}}" alt="Google fanpage" /></a></div>
</div>
<div class="footer-col" style="float: left; padding: 17px 24px; width: 141px;">
<p style="font-size: 14px; margin: 0px 0px 5px 0px;">ช่องทางการชำระเงิน</p>
<img style="width: 109px;" src="{{skin _area="frontend"  url="images/email/payment-th.png"}}" alt="ช่องทางการชำระเงิน" /></div>
<div class="clear" style="clear: both; max-height: 0px;">&nbsp;</div>
</div>
<div class="footer" style="padding: 0px 33px; margin-top: 18px; color: #3c3d41;">
<div class="copyr" style="float: left; font-size: 11px;">&copy;2015 Whatsnew Co.,Ltd. All Rights Reserved</div>
<div class="links" style="float: right; font-size: 11px;"><a style="color: #3c3d41; text-decoration: none;" href="http://www.venbi.com/th/about-us/"><span>เกี่ยวกับเรา</span></a> | <a style="color: #3c3d41; text-decoration: none;" href="http://www.venbi.com/th/terms-and-conditions/"><span>ข้อกำหนดและเงื่อนไข</span></a> | <a style="color: #3c3d41; text-decoration: none;" href="http://www.venbi.com/th/privacy-policy/"><span>ความเป็นส่วนตัว</span></a></div>
</div>
</div>
HTML;



$model = Mage::getModel('newsletter/template');
$model->loadByCode('HOW TO ORDER VENBI')
    ->setTemplateCode('How to Order Venbi')
    ->setTemplateText($template_text)
    ->setTemplateSubject('How to Order!')
    ->setTemplateType(Mage_Core_Model_Email_Template::TYPE_HTML)
    ->setTemplateSenderName('Venbi Customer Service')
    ->setTemplateSenderEmail('support@veni.com');

try {
    $model->save();
} catch (Mage_Exception $e) {
    throw $e;
}
//==========================================================================
//==========================================================================

