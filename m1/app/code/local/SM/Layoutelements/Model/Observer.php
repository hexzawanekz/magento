<?php

class SM_Layoutelements_Model_Observer
{

    public function addEmailToQueue($observer)
    {
        $currentSubscriber = $observer->getEvent()->getSubscriber();

        if (!$currentSubscriber->getCustomerId() || !$currentSubscriber->getIsStatusChanged()) {
            return;
        }
        if(!$currentSubscriber->isSubscribed()){
            return;
        }
        $en     = Mage::getModel('core/store')->load('en', 'code')->getId();// Petloft
        $th     = Mage::getModel('core/store')->load('th', 'code')->getId();
        $len    = Mage::getModel('core/store')->load('len', 'code')->getId();// Lafema
        $lth    = Mage::getModel('core/store')->load('lth', 'code')->getId();
        $ven    = Mage::getModel('core/store')->load('ven', 'code')->getId();// Venbi
        $vth    = Mage::getModel('core/store')->load('vth', 'code')->getId();
        $sen    = Mage::getModel('core/store')->load('sen', 'code')->getId();// Sanoga
        $sth    = Mage::getModel('core/store')->load('sth', 'code')->getId();
        $moxyen = Mage::getModel('core/store')->load('moxyen', 'code')->getId();// Moxy
        $moxyth = Mage::getModel('core/store')->load('moxyth', 'code')->getId();
        $customer = Mage::getModel('customer/customer')->load($currentSubscriber->getCustomerId());
        $store = $customer->getStoreId();
        if(!$store){
            $store = Mage::app()->getStore()->getStoreId();
        }
        $ceoTemplateCode= '';
        $howToOrderTemplateCode = '';
        if($store == $en || $store == $th){
            $ceoTemplateCode = 'MESSAGE FROM THE CEO PETLOFT';
            $howToOrderTemplateCode = 'HOW TO ORDER PETLOFT';
        }else if($store == $len || $store == $lth){
            $ceoTemplateCode = 'MESSAGE FROM THE CEO LAFEMA';
            $howToOrderTemplateCode = 'HOW TO ORDER LAFEMA';
        }else if($store == $sen || $store == $sth){
            $ceoTemplateCode = 'MESSAGE FROM THE CEO SANOGA';
            $howToOrderTemplateCode = 'HOW TO ORDER SANOGA';
        }else if($store == $ven || $store == $vth){
            $ceoTemplateCode = 'MESSAGE FROM THE CEO VENBI';
            $howToOrderTemplateCode = 'HOW TO ORDER VENBI';
        }else if($store == $moxyen || $store == $moxyth){
            $ceoTemplateCode = 'MESSAGE FROM THE CEO ORAMI';
            $howToOrderTemplateCode = 'HOW TO ORDER ORAMI';
        }

        // This email isn't used now
//        $this->addEmail($currentSubscriber,$ceoTemplateCode,'1');
        $this->addEmail($currentSubscriber,$howToOrderTemplateCode,'2');
        //  $this->addHowToOrderToQueue($currentSubscriber);
    }

    public function addEmail($currentSubscriber,$templateCode,$day){
        $queue = Mage::getModel('newsletter/queue');

        $template = Mage::getModel('newsletter/template')->getCollection()
            ->addFieldToFilter('template_code', $templateCode)
            ->getFirstItem();
        $collection = $queue->getCollection();
        $collection->addFieldToFilter( 'template_id', $template->getId() )
            ->addFieldToFilter('receiver',$currentSubscriber->getSubscriberEmail())
            ->getFirstItem();
        ;
        if($collection->getSize() > 0){
            return;
        }
        if(!$template->getData()){
            return;
        }
        $queue->setTemplateId($template->getId())
            ->setQueueStatus(Mage_Newsletter_Model_Queue::STATUS_NEVER);

        if ($queue->getQueueStatus() == Mage_Newsletter_Model_Queue::STATUS_NEVER) {
            $date = new Zend_Date(Mage::getModel('core/date')->gmtDate(null, time()));
            $date->addDay($day);
            $queue->setQueueStartAt($date->toString());
        }
        $queue->setNewsletterSubject($template->getTemplateSubject())
            ->setNewsletterSenderName($template->getTemplateSenderName())
            ->setNewsletterSenderEmail($template->getTemplateSenderEmail())
            ->setNewsletterText($template->getTemplateText())
            ->setNewsletterStyles($template->getTemplateStyle())
            ->setData('receiver',$currentSubscriber->getSubscriberEmail());
        $queue->save();
        Mage::getResourceModel('newsletter/queue')->addSubscribersToQueue($queue,array($currentSubscriber->getId()));
    }

}