<?php

class SM_Filter_Block_Catalog_Product_List_Toolbar extends Mage_Catalog_Block_Product_List_Toolbar
{
    public function setCollection($collection)
    {
        parent::setCollection($collection);

        $config = Mage::getStoreConfig('cataloginventory/options/sort_out_of_stock');
        if ($config) {

            //skip search results
            $isSearch = in_array(Mage::app()->getRequest()->getModuleName(), array('sqli_singlesearchresult', 'catalogsearch'));
            if ($isSearch && 2 == $config) return $this;

            $select = $collection->getSelect();

            if (!strpos($select->__toString(), 'cataloginventory_stock_status')) {
                Mage::getResourceModel('cataloginventory/stock_status')
                    ->addStockStatusToSelect($select, Mage::app()->getWebsite());
            }

            $field = 'salable DESC';
            $select->order($field);

            // move to the first position
            $orders = $select->getPart(Zend_Db_Select::ORDER);
            if (count($orders) > 1) {
                $last = array_pop($orders);
                array_unshift($orders, $last);
                $select->setPart(Zend_Db_Select::ORDER, $orders);
            }
        }

        return $this;
    }

    /**
     * Get grit products sort order field
     *
     * @return string
     */
    public function getCurrentOrder()
    {
        //$order = $this->_getData('_current_grid_order');
        //if ($order) {
        //    return $order;
        //}

        $orders = $this->getAvailableOrders();
        $this->_orderField  = Mage::getStoreConfig(
            Mage_Catalog_Model_Config::XML_PATH_LIST_DEFAULT_SORT_BY
        );
        $defaultOrder = $this->_orderField;
        if (!isset($orders[$defaultOrder])) {
            $keys = array_keys($orders);
            $defaultOrder = $keys[0];
        }
        $order = $this->getRequest()->getParam($this->getOrderVarName());
        /*if ($order && isset($orders[$order])) {
            if ($order == $defaultOrder) {
                Mage::getSingleton('catalog/session')->unsSortOrder();
            } else {
                $this->_memorizeParam('sort_order', $order);
            }
        } else {
            $order = Mage::getSingleton('catalog/session')->getSortOrder();
        }*/
        // validate session value
        if (!$order || !isset($orders[$order])) {
            $order = $defaultOrder;
        }
        //$this->setData('_current_grid_order', $order);
        return $order;
    }

    public function getCurrentDirection()
    {
        // always sort Best Seller by DESC
        if($this->getCurrentOrder() == 'revenue' ||$this->getCurrentOrder()== 'revenue_last_month'  ){
            return 'desc';
        }

        return parent::getCurrentDirection();
    }
}