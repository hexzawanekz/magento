<?php

class SM_Filter_Block_Catalog_Layer_Filter_Abstract extends Mage_Catalog_Block_Layer_Filter_Abstract
{
    protected $_minPrice;
    protected $_maxPrice;

    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('catalog/layer/filter.phtml');
    }

    public function isCategoryFilter()
    {
        return $this->getFilterModelName() == 'catalog/layer_filter_category' ? true : false;
    }

    public function getFilterModelName()
    {
        return $this->_filterModelName;
    }

    /**
     * Get current attribute id
     */
    public function getAttributeId()
    {
        if ($this->isCategoryFilter() === false)
            return $this->_filter->getAttributeModel()->getAttributeId();
        else
            return false;
    }

    /**
     * Get current attribute code
     */
    public function getAttributeCode()
    {
        if ($this->isCategoryFilter() === false)
            return $this->_filter->getAttributeModel()->getAttributeCode();
        else
            return 'cat';
    }

    /**
     * Check current attribute which have 'price' frontend input type or not
     */
    public function isPriceAttribute()
    {
        if ($this->isCategoryFilter() === false)
            return $this->_filter->getAttributeModel()->getAttributeCode() == 'price' ? true : false;
        else
            return false;
    }

    /**
     *
     */
    public function initMinAndMaxPrice()
    {
        $hasFilter = false;
        $priceRangeStr = $this->getRequest()->getParam('price');
        if(!is_null($priceRangeStr)){
            $hasFilter = true;
            $priceRangeArr = explode('-', $priceRangeStr);
            $minFilterPrice = $priceRangeArr[0];
            $maxFilterPrice = $priceRangeArr[1];
        }

        $collection = $this->getLayer()->getProductCollection();
        $resultSet = $collection->getData();
        $resultSize = sizeof($resultSet);
        usort($resultSet, function($a, $b){
            return $a['final_price'] - $b['final_price'];
        });
        if($hasFilter){
            $this->_minPrice = $minFilterPrice;
            $this->_maxPrice = $maxFilterPrice;
        }else{
            $this->_minPrice = $resultSet[0]['final_price'];
            $this->_maxPrice = $resultSet[$resultSize - 1]['final_price'];
        }
        return $this;
    }

    /**
     * Get maximum price from layer products set
     */
    public function getMaxPrice($flag = false)
    {
        $req = $this->getRequest();
        if($req->getRouteName() == 'catalogsearch'){
            if(is_null($this->_maxPrice)){
                $this->initMinAndMaxPrice();
            }
            return $this->_maxPrice;
//            return Mage::getModel($this->_filterModelName)->getSearchMaxPrice($req->getParam('q'));
        }
        $currentCate = Mage::registry('current_category');
        if ($flag && $currentCate instanceof Mage_Catalog_Model_Category) {
            $result = Mage::getModel($this->_filterModelName)->getMaximumPrice($currentCate);
            if($result !== false)
                return $result;
        }
        return Mage::getModel($this->_filterModelName)->getMaxPriceInt();
    }

    /**
     * Get minimum price from layer products set
     */
    public function getMinPrice($flag = false)
    {
        $req = $this->getRequest();
        if($req->getRouteName() == 'catalogsearch'){
            if(is_null($this->_minPrice)){
                $this->initMinAndMaxPrice();
            }
            return $this->_minPrice;
//            return Mage::getModel($this->_filterModelName)->getSearchMinPrice($req->getParam('q'));
        }
        $currentCate = Mage::registry('current_category');
        if ($flag && $currentCate instanceof Mage_Catalog_Model_Category){
            $result = Mage::getModel($this->_filterModelName)->getMinimumPrice($currentCate);
            if($result !== false)
                return $result;
        }
        return Mage::getModel($this->_filterModelName)->getMinPriceInt();
    }
}