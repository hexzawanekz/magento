<?php

class SM_Improvement_Model_Observer{

    /**
     * Create cookie for login successfully customer
     * @param Varien_Event_Observer $observer
     */
    public function createCustomerCookie(Varien_Event_Observer $observer){
        try{
            $customer = $observer->getEvent()->getCustomer();
            $customerId = $customer->getId();
            $customerEmail = $customer->getEmail();

            $helper = Mage::helper('improvement');
            $code = $helper->encodeCustomerId($customerId,$customerEmail);
            setcookie('customer', $code, time() + 260000, "/");
        }catch(Exception $e){
            Mage::log($e->getMessage(),null,'customer_cookie_error.log',true);
        }
    }

    /**
     * Remove encoded cookie when customer logout
     * @param Varien_Event_Observer $observer
     */
    public function removeCustomerCookie(Varien_Event_Observer $observer){
        try{
           if(isset($_COOKIE['customer'])){
               unset($_COOKIE['customer']);
               setcookie('customer', null, -1, "/");
           }
        }catch(Exception $e){
            Mage::log($e->getMessage(),null,'customer_cookie_error.log',true);
        }
    }

}