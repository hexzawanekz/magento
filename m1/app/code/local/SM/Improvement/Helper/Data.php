<?php
class SM_Improvement_Helper_Data extends Mage_Core_Helper_Abstract{

    /**
     * This function will create a encode string base on customer Id and customer Email
     * @param $customerId
     * @param $customerEmail
     * @return array|string
     */
    public function encodeCustomerId($customerId,$customerEmail){
        $customerId = base64_encode($customerId);
        $customerId = rawurlencode($customerId);

        $customerEmail = base64_encode($customerEmail);
        $customerEmail = rawurlencode($customerEmail);

        $customerEncode = "osc_customer_cookie";
        $customerEncode = base64_encode($customerEncode);

        $code = $customerId.$customerEncode.$customerEmail;

        $code = base64_encode($code);
        $code = rawurlencode($code);

        return $code;
    }
}

?>