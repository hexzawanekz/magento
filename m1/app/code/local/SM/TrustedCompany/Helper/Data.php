<?php
class SM_TrustedCompany_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * @param string $_page (list || detail)
     * @param int $_store
     * @return mixed|string script code the trusted company
     */
    public function getTrustedCompany($_page = 'list', $_store = 0) {
        if($_page === 'list') {
            if (Mage::getStoreConfig('trustedcompany/product_list/is_displayed', $_store)) {
                if(Mage::getStoreConfig('trustedcompany/product_list/content', $_store)) {
                    return Mage::getStoreConfig('trustedcompany/product_list/content', $_store);
                }
            }
        }
        elseif($_page === 'detail') {
            if (Mage::getStoreConfig('trustedcompany/product_detail/is_displayed', $_store)) {
                if(Mage::getStoreConfig('trustedcompany/product_detail/content', $_store)) {
                    return Mage::getStoreConfig('trustedcompany/product_detail/content', $_store);
                }
            }
        }
        return '';
    }
    
    public function getTrustedCompanyInFooter($_store = 0) {
        if (Mage::getStoreConfig('trustedcompany/footer/is_displayed', $_store)) {
            if (Mage::getStoreConfig('trustedcompany/footer/content', $_store)) {
               return  Mage::getStoreConfig('trustedcompany/footer/content', $_store);
            }
        }
        return '';
    }

}
	 