<?php

class SM_GWM_Helper_Data extends Mage_Core_Helper_Abstract
{
    const USER_STATUS_NEW       = 1;
    const USER_STATUS_REPEATED  = 2;

    const XML_PATH_MODULE_ENABLED       = 'sm_gwm/general/enabled';
    const XML_PATH_OWNER_ID             = 'sm_gwm/general/owner_id';
    const XML_PATH_SCRIPT_TAG_URL       = 'sm_gwm/general/script_link';
    const XML_PATH_CATEGORY_MAPPING     = 'sm_gwm/general/category_maps';
    const XML_PATH_EVENT_NO             = 'sm_gwm/general/event_no';

    public function isEnabled()
    {
        return Mage::getStoreConfig(self::XML_PATH_MODULE_ENABLED);
    }

    public function getOwnerId()
    {
        return Mage::getStoreConfig(self::XML_PATH_OWNER_ID);
    }

    public function getScriptTagUrl()
    {
        return Mage::getStoreConfig(self::XML_PATH_SCRIPT_TAG_URL);
    }

    public function getCategoryMapping()
    {
        $data = @unserialize(Mage::getStoreConfig(self::XML_PATH_CATEGORY_MAPPING));
        $result = array();
        foreach($data as $item){
            $result[$item['fieldname']] = $item['fieldvalue'];
        }
        return $result;
    }

    public function getEventNo(){
        return Mage::getStoreConfig(self::XML_PATH_EVENT_NO);
    }
}