<?php

class SM_GWM_Block_Adminhtml_System_Config_Form_Field_Categories extends Mage_Adminhtml_Block_System_Config_Form_Field_Array_Abstract
{
    public function __construct()
    {
        $this->addColumn('fieldname', array(
            'label' => Mage::helper('sm_gwm')->__('GWM Label'),
            'style' => 'width:120px',
        ));
        $this->addColumn('fieldvalue', array(
            'label' => Mage::helper('sm_gwm')->__('Category URL Key'),
            'style' => 'width:120px',
        ));
        $this->_addAfter = false;
        $this->_addButtonLabel = Mage::helper('sm_gwm')->__('Add More');
        parent::__construct();
    }
}