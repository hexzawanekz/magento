<?php

class SM_GWM_Block_Checkout_Onepage_Success extends Mage_Core_Block_Template
{
    protected $_helper = null;

    /**
     * @return SM_GWM_Helper_Data
     */
    public function getModuleHelper()
    {
        if(is_null($this->_helper)){
            $this->_helper = Mage::helper('sm_gwm');
        }
        return $this->_helper;
    }

    public function getOrderInformation()
    {
        $result = array();

        // check if module is enabled
        if($this->getModuleHelper()->isEnabled()){

            /** @var Mage_Checkout_Model_Session $session */
            $session = Mage::getSingleton('checkout/session');

            if($lastOrderId = $session->getLastOrderId()){
                /** @var Mage_Sales_Model_Order $order */
                $order = Mage::getModel('sales/order')->load($lastOrderId);

                if($order->getId()){
                    // set TRANSACTION_ID
                    $result['transaction_id'] = $order->getIncrementId();

                    if($customerId = $order->getCustomerId()){
                        /** @var Mage_Customer_Model_Customer $customer */
                        $customer = Mage::getModel('customer/customer')->load($customerId);
                        /** @var Mage_Core_Model_Date $date */
                        $date = Mage::getModel('core/date');

                        $registerDate = $customer->getCreatedAtTimestamp();
                        $currentTime = strtotime($date->date('Y-m-d H:i:s'));

                        // set USER_STATUS
                        if($currentTime <= $registerDate + 86400){
                            $result['user_status'] = SM_GWM_Helper_Data::USER_STATUS_NEW;
                        }else{
                            $result['user_status'] = SM_GWM_Helper_Data::USER_STATUS_REPEATED;
                        }
                    }

                    $tempValues = $this->_buildCategoryStringPath($order);

                    // set CATEGORY
                    $result['categories'] = $tempValues['categories'];

                    // set TRANSACTION_AMT_THB
                    $result['transaction_amt_thb'] = (float) $order->getGrandTotal() - $tempValues['excluded_amount'];

                    $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
                    $sql        = "SELECT * FROM sales_flat_order_grid WHERE entity_id = '".$order->getId()."'";
                    $row        = $connection->fetchRow($sql);
                    if($row['utm_click'] != NULL)
                        $result['utm_click'] = $row['utm_click'];
                    if($row['utm_subid'] != NULL)
                        $result['utm_subid'] = $row['utm_subid'];
                }
            }
        }

        if(!empty($result)){
            return $result;
        }
        return false;
    }

    protected function _buildCategoryStringPath(Mage_Sales_Model_Order $order)
    {
        // get list all category from config mapping
        $tempCategories = array();
        $mappedCategories = $this->getModuleHelper()->getCategoryMapping();

        /**
         * Exclude Electronic items
         */
        $excludedCategoryUrlKey = 'electronics';
        /** @var Mage_Catalog_Model_Category $excludedCategoryObj */
        $excludedCategoryObj = Mage::getModel('catalog/category')
            ->getCollection()
            ->addAttributeToSelect('entity_id')
            ->addAttributeToFilter('url_key', $excludedCategoryUrlKey)
            ->getFirstItem();
        $excludedCategoryId = $excludedCategoryObj->getId();
        $excludedAmount = (float) 0;

        /** @var Mage_Catalog_Model_Resource_Category_Collection $categories */
        $categories = Mage::getModel('catalog/category')
            ->getCollection()
            ->addAttributeToSelect('entity_id')
            ->addAttributeToSelect('url_key')
            ->addAttributeToFilter('url_key', array('in' => array_values($mappedCategories)));

        if($categories->count() > 0){
            $categoryIds = $categories->getAllIds();

            $items = $order->getAllVisibleItems();
            foreach($items as $item){
                /** @var Mage_Sales_Model_Order_Item $item */

                /** @var Mage_Catalog_Model_Product $product */
                $product = Mage::getModel('catalog/product')->load($item->getProductId());
                foreach($product->getCategoryIds() as $cId){
                    if(in_array($cId, $categoryIds) && !in_array($cId, $tempCategories)){
                        $tempCategories[] = $cId;
                    }elseif($excludedCategoryId && $cId == $excludedCategoryId){
                        $excludedAmount += (float) $item->getBasePriceInclTax() * (float) $item->getQtyOrdered() - (float) $item->getDiscountAmount();
                    }
                }
                unset($product);
            }
        }

        $result = array();
        foreach($tempCategories as $cId){
            foreach($categories as $cate){
                /** @var Mage_Catalog_Model_Category $cate */
                if($cId == $cate->getId()){
                    $result[] = array_search($cate->getData('url_key'), $mappedCategories);
                }
            }
        }

        return array(
            'categories'        => implode('_', $result),
            'excluded_amount'   => $excludedAmount,
        );
    }
}