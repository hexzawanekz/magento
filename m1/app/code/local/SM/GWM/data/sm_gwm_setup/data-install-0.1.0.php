<?php

/** @var $this Mage_Core_Model_Resource_Setup */
$this->startSetup();

/** @var Mage_Core_Model_Config $config */
$config = Mage::getModel('core/config');

$config->saveConfig(SM_GWM_Helper_Data::XML_PATH_OWNER_ID, '41963', 'default', Mage_Core_Model_App::ADMIN_STORE_ID);

$config->saveConfig(SM_GWM_Helper_Data::XML_PATH_SCRIPT_TAG_URL, 'http://pfgbc.com/p.ashx', 'default', Mage_Core_Model_App::ADMIN_STORE_ID);

$data = array(
    array(
        'fieldname'     => 'Beauty',
        'fieldvalue'    => 'beauty'
    ),
    array(
        'fieldname'     => 'Fashion',
        'fieldvalue'    => 'fashion'
    ),
    array(
        'fieldname'     => 'Home Decor',
        'fieldvalue'    => 'home-living'
    ),
    array(
        'fieldname'     => 'Home Appliances',
        'fieldvalue'    => 'home-appliances'
    ),
//    array(
//        'fieldname'     => 'Electronics',
//        'fieldvalue'    => 'electronics'
//    ),
    array(
        'fieldname'     => 'Mom and Baby',
        'fieldvalue'    => 'baby'
    ),
    array(
        'fieldname'     => 'Health and Sports',
        'fieldvalue'    => 'health'
    ),
    array(
        'fieldname'     => 'Pets',
        'fieldvalue'    => 'pets'
    ),
);
$categories = serialize($data);

$config->saveConfig(SM_GWM_Helper_Data::XML_PATH_CATEGORY_MAPPING, $categories, 'default', Mage_Core_Model_App::ADMIN_STORE_ID);

$this->endSetup();