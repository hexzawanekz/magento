<?php
class SM_Emarsys_Block_Checkout_Onepage_Success extends Mage_Core_Block_Template
{
    public function getOrderInfo(){
        $count = 0;
        $lastOrderId = Mage::getSingleton('checkout/session')->getLastRealOrderId();
        $order = Mage::getModel('sales/order')->loadByIncrementId($lastOrderId);
        if($order->getId()){
            $incrementId = $order->getIncrementId();
            $_orderInfo = "";
            $itemCount = count($order->getAllVisibleItems());
            foreach ($order->getAllVisibleItems() as $_item) {
                $_itemSku = $_item->getSku();
                $_itemQty = $_item->getQtyOrdered();
                $_itemPrice = ($_item->getProduct()->getPrice())*$_itemQty;
                $_orderInfo .= "{item: \"".addslashes($_itemSku)."\", price: " . $_itemPrice . ", quantity: " . $_itemQty . "}";
                if ($count < $itemCount - 1) {
                    $_orderInfo .= ",";
                    $count++;
                }
            }
            return "ScarabQueue.push(['purchase', {orderId: {$incrementId}, items: [".$_orderInfo."]}]);";
        }
        return false;
    }
}