<?php

class SM_Emarsys_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * return email of currently user
     * @return string email of user
     */
    public function getCustomerEmail()
    {
        return Mage::getModel('customer/session')->getCustomer()->getEmail();
    }

    public function getQuoteInfo()
    {
        $_cartInfo = '';
        /** @var Mage_Sales_Model_Quote $_cart */
        $_cart = Mage::helper('checkout/cart')->getQuote();
        if ($_cart instanceof Mage_Sales_Model_Quote) {
            $_totalQty = $_cart->getItemsCount();
            if ($_totalQty) {
                $itemCount = count($_cart->getAllVisibleItems());
                $count = 0;
                /** @var Mage_Sales_Model_Quote_Item $_item */
                foreach ($_cart->getAllVisibleItems() as $_item) {
                    $_itemSku = $_item->getProduct()->getSku();
                    $_itemQty = $_item->getQty();
                    $_itemPrice = ($_item->getProduct()->getPrice())*$_itemQty;
                    $_cartInfo .= "{item: \"" . addslashes($_itemSku) . "\", price: " . $_itemPrice . ", quantity: " . $_itemQty . "}";
                    if ($count < $itemCount - 1) {
                        $_cartInfo .= ",";
                        $count++;
                    }
                }
            }
        }
        return $_cartInfo;
    }

    /**
     * return purchase detail for emarsys
     * @var Mage_Sales_Model_Order $_order
     * @return string purchase detail
     */
    public function getOrderInfo($_order)
    {
        /** @var Mage_Sales_Model_Order_Item $_item */
        if ($_order instanceof Mage_Sales_Model_Order) {
            $count = 0;
            $_incrementId = $_order->getIncrementId();
            $_orderInfo = "orderId: '$_incrementId', items: [";
            $itemCount = count($_order->getAllVisibleItems());
            foreach ($_order->getAllVisibleItems() as $_item) {
                $_itemSku = $_item->getSku();
                $_itemQty = $_item->getQtyOrdered();
                $_itemPrice = ($_item->getProduct()->getPrice())*$_itemQty;
                $_orderInfo .= "{item: \"".addslashes($_itemSku)."\", price: " . $_itemPrice . ", quantity: " . $_itemQty . "}";
                if ($count < $itemCount - 1) {
                    $_orderInfo .= ",";
                    $count++;
                }
            }
            $_orderInfo .= ']';
            return $_orderInfo;
        }
    }

    /**
     * @param Mage_Catalog_Model_Category $category
     * @return string
     */
    public function getCategoryPathInfo(Mage_Catalog_Model_Category $category)
    {
        $category = Mage::getModel('catalog/category')->load($category->getId());
        $categoryPath = $category->getPath();

        $categoryArr = explode('/', $categoryPath);
        $categoryCount = count($categoryArr);
        $categoryInfo = '';
        for($i = 2; $i < $categoryCount; $i++){
            $categoryInfo.= Mage::getModel('catalog/category')->load($categoryArr[$i])->getName();

            if($i < $categoryCount-1){
                $categoryInfo .= ' > ';
            }
        }

        return addslashes($categoryInfo);
    }

    /**
     * Check isSaleableProduct
     * @param $productId
     * @return mixed
     */
    public function checkProductStatus($productSku){
        $outputData = array();

        /** @var Mage_Catalog_Model_Product $product */
        $outputData['id'] = Mage::getModel('catalog/product')->getIdBySku($productSku);
        $product = Mage::getModel('catalog/product')->load($outputData['id']);

        // This is fake for test
//        $product = Mage::getModel('catalog/product')->load(14292);

        // Get rating summary
        $storeId = Mage::app()->getStore()->getId();
        $summaryData = Mage::getModel('review/review_summary')
            ->setStoreId($storeId)
            ->load($product->getId());

        // Get product brand
        $brand = $product->getData('all_brands');

        $brand ? $outputData['brand'] = $brand : $outputData['brand'] = "";
        $outputData['rating'] = $summaryData['rating_summary'];

        if($product->isConfigurable() || $product->isGrouped()){
            $outputData['available'] = false;
        } else {
            $outputData['available'] = $product->isSalable();
        }

        return $outputData;
    }

    public function getLink(){
        $link = Mage::getStoreConfig('emarsys/emarsys_register_form/register_link');
        return $link;
    }

    public function getField(){
        $field = Mage::getStoreConfig('emarsys/emarsys_register_form/field_value');
        return unserialize($field);
    }
}
	 