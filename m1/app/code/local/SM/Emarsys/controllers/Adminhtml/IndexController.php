<?php

class SM_Emarsys_Adminhtml_IndexController extends Mage_Adminhtml_Controller_Action
{

    public function downloadAction() {
        $folder = 'media'.DS.'EmarsysExportedFile'.DS;
        $fileName = 'Orami.csv';
        if($file = file_get_contents($folder.$fileName, true)){
            header("Content-type: text/csv");
            header("Content-Disposition: attachment; filename=\"".$fileName."\"");
            header("Pragma: no-cache");
            header("Expires: 0");
            echo $file;
        }
    }

    protected function _isAllowed()
    {
        return true;
    }
}