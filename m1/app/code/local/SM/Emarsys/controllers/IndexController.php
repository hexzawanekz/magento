<?php

class SM_Emarsys_IndexController extends Mage_Core_Controller_Front_Action {
    public function indexAction() {
        $this->getResponse()->setHeader('Access-Control-Allow-Origin', 'http://magazine.orami.co.th');
        if($this->getRequest()->getParam('url')) {
            $url = $this->getRequest()->getParam('url');

            try {

                $result = file_get_contents($url);

                //Mage::log($result, null, 'Emarsys.log');
                //Mage::log('URL : '.$url, null, 'Emarsys.log');

            } catch (Exception $e) {
                throw new Exception("Cannot access to the link");
            }
        }
    }

    public function downloadAction() {
        Mage::register('download_emarsys_csv',true);
        ob_clean();
        ob_end_clean();
        $fileName = 'Orami.csv';
        $link = Mage::getBaseDir('media');
        $fullpath = rtrim($link, '/').DS.'EmarsysExportedFile'.DS.$fileName;

        if(headers_sent()){
            header_remove();
        }

        if(file_exists($fullpath)){
            header("Content-type: text/csv");
            header("Content-Disposition: attachment; filename=\"".basename($fullpath)."\"");
            readfile($fullpath = rtrim($link, '/').DS.'EmarsysExportedFile'.DS.$fileName);
        }
    }

    public function checkstatusAction(){

        $scarabData = $this->getRequest()->getParam('data');

        foreach($scarabData as $k => $_item){
            if(isset($_item['id'])){
                if($checked = Mage::helper('emarsys')->checkProductStatus($_item['id'])){
                    $scarabData[$k]['available'] = $checked['available'];
                    $scarabData[$k]['productId'] = $checked['id'];
                    $scarabData[$k]['msrp'] = (isset($scarabData[$k]['msrp'])) ? $scarabData[$k]['msrp'] : 0;
                    $scarabData[$k]['brand'] = $checked['brand'];
                    $scarabData[$k]['rating'] = $checked['rating'];
                }
            }
        }

        for($i = 0; $i < count($scarabData); $i++){
            for($j = $i + 1; $j < count($scarabData); $j++){
                if($scarabData[$i]['available'] < $scarabData[$j]['available']){

                    $temp = $scarabData[$i];
                    $scarabData[$i] = $scarabData[$j];
                    $scarabData[$j] = $temp;

                }
            }
        }

	$this->getResponse()->setBody(json_encode($scarabData));
    }
}
?>