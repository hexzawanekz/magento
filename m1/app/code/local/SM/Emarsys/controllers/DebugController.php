<?php
/**
 * Created by PhpStorm.
 * User: SMART
 * Date: 7/15/2015
 * Time: 2:51 PM
 */

class SM_Emarsys_DebugController extends Mage_Core_Controller_Front_Action {

    public function indexAction() {

        ini_set('memory_limit', -1);
        set_time_limit(0);

        Mage::getModel('sm_emarsys/observer')->emarsysExportCsv();
    }

    public function vailonAction(){
        Mage::app()->setCurrentStore('len');
        echo Mage::getBaseUrl('media');
    }

    public function testAction() {
        $emarsys = new SM_Emarsys_Model_Observer();
        print_r($emarsys->_getIgnoreCategoryList());
    }

}