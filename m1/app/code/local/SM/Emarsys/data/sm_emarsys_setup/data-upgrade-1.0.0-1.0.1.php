<?php
$installer = $this;
 
$installer->startSetup();

$setup = Mage::getModel('core/config');
 
$setup->saveConfig('emarsys/emarsys_register_form/register_link', 'http://click.orami.co.th/u/register.php', 'default', 0);

$data = array(
	array('fieldname' => 'CID','fieldvalue' => 530127770),
	array('fieldname' => 'f','fieldvalue' => 2621),
	array('fieldname' => 'p','fieldvalue' => 2),
	array('fieldname' => 'a','fieldvalue' => 'r'),
	array('fieldname' => 'SID','fieldvalue' => ''),
	array('fieldname' => 'el','fieldvalue' => ''),
	array('fieldname' => 'llid','fieldvalue' => ''),
	array('fieldname' => 'counted','fieldvalue' => ''),
	array('fieldname' => 'c','fieldvalue' => ''),
	array('fieldname' => 'optin','fieldvalue' => 'y'),
	array('fieldname' => 'interest[]','fieldvalue' => '')
	);
$data = serialize($data);
$setup->saveConfig('emarsys/emarsys_register_form/field_value',$data,'default',0);
$installer->endSetup();
?>