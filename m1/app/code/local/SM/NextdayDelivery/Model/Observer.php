<?php

class SM_NextdayDelivery_Model_Observer
{
    public function updateOrderId(Varien_Event_Observer $observer)
    {
        $order = $observer->getEvent()->getOrder();
        $shippingMethod = $order->getShippingMethod();
        if (strcmp($shippingMethod, 'bkk_nextday_bkk_nextday') == 0) { //BKK Next Day Delivery
            $newIncrementId = 'EXP' . $order->getIncrementId();
            $order->setIncrementId($newIncrementId);
            $order->save();
        }
    }
}