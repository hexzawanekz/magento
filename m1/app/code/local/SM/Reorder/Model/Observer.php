<?php

class SM_Reorder_Model_Observer
{
    public function getProductPrice(Varien_Event_Observer $observer)
    {
        $newOrderItem = $observer->getEvent()->getQuoteItem();
        if(Mage::registry('subscription_pay')){
            $data = Mage::registry('subscription_pay');
            Mage::unregister('subscription_pay');
            $subId = $data['sub_id'];
            $seqId = $data['seq_id'];
            $termId = $data['term_id'];

            $sequences = Mage::getModel('recurringandrentalpayments/sequence')
                ->getCollection()
                ->addFieldToFilter('subscription_id',array('eq' => $subId));

            $count = 1;
            foreach($sequences as $seq){
                if($seq->getId() != $seqId) $count++;
                else break;
            }

            $term = Mage::getModel('recurringandrentalpayments/terms')->load($termId);
            $price = $term->getPrice();
            $discountTerm = $term->getDiscountterm();
            $recurringDiscount = 1 - ($discountTerm / 100);
            $recurringDiscount = pow($recurringDiscount,($count - 1));
            $price = $price * $recurringDiscount;

            if($term->getPriceCalculationType() == 1)
            {
                $product = $newOrderItem->getProduct();
                $price = ($product->getPrice()  * $term->getPrice()/100);
                $recurringDiscount = 1 - ($discountTerm / 100);
                $recurringDiscount = pow($recurringDiscount,($count - 1));
                $price = $price * $recurringDiscount;
            }
            $newOrderItem->setOriginalCustomPrice($price)
                ->setCustomPrice($price)
                ->setIsSuperMode(true)
                ->save();
        }else{
            $oldOrderItem = $observer->getEvent()->getOrderItem();
            $oldDiscount = $oldOrderItem->getDiscountAmount();
            $oldPrice = $oldOrderItem->getPrice();
            $quantity = $oldOrderItem->getData('qty_ordered');
            $discount = $oldDiscount / $quantity;
            $oldPrice = $oldPrice - $discount;

            if ($oldOrderItem->getData('product_type') != 'configurable') {
                $newOrderItem->setOriginalCustomPrice($oldPrice)
                    ->setCustomPrice($oldPrice)
                    ->setIsSuperMode(true)
                    ->save();
            } else{
                $product = Mage::getModel('catalog/product')->load($newOrderItem->getProductId());
                Mage::getSingleton('adminhtml/session')->addWarning($product->getSku() . " is a configurable item, remember to check price for it");
            }
        }

    }

    public function removePaymentInformation(Varien_Event_Observer $observer){
        $order = $observer->getEvent()->getOrder();
        $quote = $observer->getEvent()->getQuote();

        $array = $order->getPayment()->getData();
        $array['additional_information'] = "";
        $quote->getPayment()->addData($array);
        $quote->save();
    }

}