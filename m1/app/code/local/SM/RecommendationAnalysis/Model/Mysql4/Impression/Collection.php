<?php

class SM_RecommendationAnalysis_Model_Mysql4_Impression_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        $this->_init("sm_recommendationanalysis/impression");
    }
}