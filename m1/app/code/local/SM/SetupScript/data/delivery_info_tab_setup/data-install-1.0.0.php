<?php

/** @var Mage_Core_Model_Resource_Setup $installer */
$installer = $this;

$installer->startSetup();

$storeCollection=Mage::getModel('core/store')->getCollection();
/** @var Mage_Core_Model_Store $store */
foreach($storeCollection as $store)
{
    /** @var Mage_Cms_Model_Block $staticBlockModel */
    $staticBlockModel = Mage::getModel('cms/block');

    //$staticBlockModel->load('delivery_tab', 'identifier');
    $staticBlockModel->setIdentifier('delivery_tab');
    $staticBlockModel->setTitle('Delivery tab of Product Page - '.$store->getName());

    $htmlContent = <<<HTML
<div class="nav-detail-item product-delivery-items">
    <!--<h2>Delivery Info</h2>-->

    <div class="std">
        <strong>Cash On Delivery is enabled for this item for all post codes in Thailand</strong>
        <p>You can enjoy free shipping if you buy products sold by Moxy so that they together add up to 500 THB or more.</p>
    </div>
</div>
HTML;


    $staticBlockModel->setContent($htmlContent);
    $staticBlockModel->setIsActive(1);
    $staticBlockModel->setStores($store->getId());
    $staticBlockModel->save();
}

$installer->endSetup();