<?php
try {
    /** @var Mage_Core_Model_Resource_Setup $this */
    $installer = $this;
    $installer->startSetup();

    // Force the store to be admin
    Mage::app()->setUpdateMode(false);
    Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
    if (!Mage::registry('isSecureArea'))
        Mage::register('isSecureArea', 1);


    /** @var Mage_Core_Model_Config $coreConfigModel */
    $coreConfigModel = Mage::getModel('core/config');
    $coreConfigModel->saveConfig('facebookall/like/enabled', 1, 'default', Mage_Core_Model_App::ADMIN_STORE_ID);


    $installer->endSetup();
} catch (Exception $e) {
    throw $e;
}