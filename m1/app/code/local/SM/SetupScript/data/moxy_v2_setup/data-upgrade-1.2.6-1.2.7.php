<?php

/** @var Mage_Core_Model_Resource_Setup $this */
$installer = $this;
$installer->startSetup();

// Force the store to be admin
Mage::app()->setUpdateMode(false);
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
if (!Mage::registry('isSecureArea'))
    Mage::register('isSecureArea', 1);

$moxyen = Mage::getModel('core/store')->load('moxyen', 'code')->getId();
$moxyth = Mage::getModel('core/store')->load('moxyth', 'code')->getId();


//==========================================================================
// Contact Us Moxy EN
//==========================================================================
$blockTitle     = 'Contact Us Moxy EN';
$blockId        = 'contact-us';
$blockStores    = array($moxyen);

/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockId)
    ->getFirstItem();

if ($block->getId()) $block->delete();// if exists then delete
$content = <<<EOD
<div id="contact-info">
<div id="contact-info-details">
<p>&nbsp;</p>
<p>Customer service :&nbsp;<br /> 8/2 Rama 3 Road, Soi 53<br /> Bang Phongpang, Yannawa<br /> Bangkok, 10120 Thailand<br /> Tel: 02-106-8222&nbsp;<br /> Facebook:&nbsp;<br /> <a href="https://www.facebook.com/moxyst" target="_blank">https://www.facebook.com/moxyst</a><br /> Email:&nbsp;<br /> <a title="send email to Moxy" href="mailto:support@moxy.co.th" target="_self">support@moxy.co.th</a></p>
<p>&nbsp;</p>
<p>Office:<br /> Whatsnew Co., Ltd.<br /> Sethiwan Tower - 19th Floor<br /> 139 Pan Road, Silom, Bangrak,<br /> Bangkok, 10500 Thailand<br /> Tel: <span>02-106-8222</span>&nbsp;<br /> Facebook:&nbsp;<br /><a href="https://www.facebook.com/moxyst" target="_blank">https://www.facebook.com/moxyst</a><br /> Email:&nbsp;<br /><a title="send email to Moxy" href="mailto:support@moxy.co.th" target="_self">support@moxy.co.th</a></p>
</div>
<div id="contact-info-map"><iframe style="border: 0;" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1937.973472047649!2d100.5241348!3d13.721662!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e298cdd9e5bd05%3A0x1e206241cb063178!2sSethiwan+Tower!5e0!3m2!1sen!2sth!4v1397117387281" frameborder="0" width="350" height="300"></iframe><br /><small>View <a style="color: #0000ff; text-align: left;" href="https://maps.google.com/maps/ms?msa=0&amp;msid=214150831025562562519.0004daafa9c87d88cd667&amp;hl=en&amp;ie=UTF8&amp;t=m&amp;source=embed&amp;ll=13.728265,100.537412&amp;spn=0,0">Moxy.co.th</a> in a larger map</small></div>
</div>
EOD;

$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockId);
$block->setStores($blockStores);
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// ติดต่อเรา Moxy TH
//==========================================================================
$blockTitle     = 'ติดต่อเรา Moxy TH';
$blockId        = 'contact-us';
$blockStores    = array($moxyth);

/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockId)
    ->getFirstItem();

if ($block->getId()) $block->delete();// if exists then delete
$content = <<<EOD
<div id="contact-info">
<div id="contact-info-details">
<p><br /> บริการลูกค้า:<br /> 8/2 ถนนพระราม 3 ซอย 53<br /> แขวงบางโพงพาง เขตสาทร<br /> กรุงเทพฯ ประเทศไทย 10120<br /> โทรศัพท์: 02-106-8222&nbsp;<br /> Facebook:&nbsp;<br /> <a href="https://www.facebook.com/moxyst" target="_blank">https://www.facebook.com/moxyst</a><br /> Email:&nbsp;<br /> <a title="send email to Moxy" href="mailto:support@moxy.co.th" target="_self">support@moxy.co.th</a><strong></strong></p>
<p>&nbsp;</p>
<p>ออฟฟิศ:<br />อาคารเศรษฐีวรรณ ชั้น 19<br />139 ถนนปั้น แขวงสีลม เขตบางรัก<br /> กรุงเทพฯ ประเทศไทย 10500<br /> โทรศัพท์: <span>02-106-8222</span>&nbsp;<br /> Facebook:&nbsp;<br /><a href="https://www.facebook.com/moxyst" target="_blank">https://www.facebook.com/moxyst</a><br /> Email:&nbsp;<br /><a title="send email to Moxy" href="mailto:support@moxy.co.th" target="_self">support@moxy.co.th</a></p>
</div>
<div id="contact-info-map"><iframe style="border: 0;" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1937.973472047649!2d100.5241348!3d13.721662!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e298cdd9e5bd05%3A0x1e206241cb063178!2sSethiwan+Tower!5e0!3m2!1sen!2sth!4v1397117387281" frameborder="0" width="350" height="300"></iframe><br /><small>View <a style="color: #0000ff; text-align: left;" href="https://maps.google.com/maps/ms?msa=0&amp;msid=214150831025562562519.0004daafa9c87d88cd667&amp;hl=en&amp;ie=UTF8&amp;t=m&amp;source=embed&amp;ll=13.728265,100.537412&amp;spn=0,0">Moxy.co.th</a> in a larger map</small></div>
</div>
EOD;

$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockId);
$block->setStores($blockStores);
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================
$installer->endSetup();