<?php

/** @var Mage_Core_Model_Resource_Setup $this */
$installer = $this;
$installer->startSetup();

// Force the store to be admin
Mage::app()->setUpdateMode(false);
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
if (!Mage::registry('isSecureArea'))
    Mage::register('isSecureArea', 1);

$moxyen = Mage::getModel('core/store')->load('moxyen', 'code')->getId();
$moxyth = Mage::getModel('core/store')->load('moxyth', 'code')->getId();



//==========================================================================
// Footer Subscription Text EN
//==========================================================================
$blockTitle = 'Footer Subscription Text ENG';
$blockId = 'footer-subscription-text';
$blockStores = array($moxyen);

/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockId)
    ->getFirstItem();

if ($block->getId()) $block->delete();// if exists then delete
$content = <<<EOD
<p class="title">GET A VOUCHER WORTH 150 THB</p>
<p class="content">Subscribe and get the latest products, fashion news, trends, exclusive offers and more from moxy.co.th - straight to your box</p>
EOD;

$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockId);
$block->setStores($blockStores);
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// Footer Subscription Text TH
//==========================================================================
$blockTitle = 'Footer Subscription Text THAI';
$blockId = 'footer-subscription-text';
$blockStores = array($moxyth);

/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockId)
    ->getFirstItem();

if ($block->getId()) $block->delete();// if exists then delete
$content = <<<EOD
<p class="title">ลงทะเบี่ยนวันนี้ รับส่วนลด 150 บาท ทันที่</p>
<p class="content">ติดตามเรา รับข่าวสารล่าสุดและอัพเดทเทรนด์แฟชั่นใหม่ พร้อมโปรโมชั่นพิเศษที่ยิ่งกว่าจาก Moxy.co.th</p>
EOD;

$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockId);
$block->setStores($blockStores);
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// Footer Share Links
//==========================================================================
$blockTitle = 'Footer Share Links';
$blockId = 'banks_footer';
$blockStores = array(0);

/** @var Mage_Cms_Model_Block $block */
$blocks = Mage::getModel('cms/block')->getCollection()
    ->addFieldToFilter('identifier', $blockId);

foreach($blocks as $block){
    if ($block->getId()) $block->delete();// if exists then delete
}
$content = <<<EOD
<a href="https://www.facebook.com/moxyst" target="_blank">
    <span class="fa-stack facebook-footer">
        <i class="fa fa-circle fa-stack-2x"></i>
        <i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
    </span>
</a>
<a  href="https://twitter.com/moxyst" target="_blank">
    <span class="fa-stack twitter-footer">
        <i class="fa fa-circle fa-stack-2x"></i>
        <i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
    </span>
</a>
<a href="https://www.instagram.com/moxy_th/" target="_blank">
    <span class="fa-stack instagram-footer">
        <i class="fa fa-circle fa-stack-2x"></i>
        <i class="fa fa-instagram fa-stack-1x fa-inverse"></i>
    </span>
</a>
<a href="https://www.youtube.com/" target="_blank">
    <span class="fa-stack youtube-footer">
        <i class="fa fa-circle fa-stack-2x"></i>
        <i class="fa fa-youtube fa-stack-1x fa-inverse"></i>
    </span>
</a>
<a href="https://plus.google.com/+Moxyst" target="_blank">
    <span class="fa-stack google-footer">
        <i class="fa fa-circle fa-stack-2x"></i>
        <i class="fa fa-google-plus fa-stack-1x fa-inverse"></i>
    </span>
</a>
<a href="https://www.pinterest.com/MoxySEA1/" target="_blank">
    <span class="fa-stack pinterest-footer">
        <i class="fa fa-circle fa-stack-2x"></i>
        <i class="fa fa-pinterest-p fa-stack-1x fa-inverse"></i>
    </span>
</a>
EOD;

$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockId);
$block->setStores($blockStores);
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// Site Footer Links
//==========================================================================
$blockId = 'footer_links_1';

/** @var Mage_Cms_Model_Block $block */
$blocks = Mage::getModel('cms/block')->getCollection()
    ->addFieldToFilter('identifier', $blockId);

foreach($blocks as $block){
    if ($block->getId()) $block->delete();// if exists then delete
}

// English
$blockTitle = 'Site Footer Links EN';
$blockId = 'footer_links_1';
$blockStores = array($moxyen);

$content = <<<EOD
<div class="col-link">
    <h3 class="title col1">Information</h3>
    <ul class="footer_links col1">
        <li><a href="{{store url='about-us'}}">About Us</a></li>
        <li><a href="http://www.whatsnew.asia/">Corporate</a></li>
        <li><a href="{{store url='contacts'}}">Contact Us</a></li>
    </ul>
</div>
<div class="col-link">
    <h3 class="title col2">Security &amp; Privacy</h3>
    <ul class="footer_links col2">
        <li><a href="{{store url='terms-and-conditions'}}">Terms &amp; Conditions</a></li>
        <li><a href="{{store url='privacy-policy'}}">Privacy Policy</a></li>
    </ul>
</div>
<div class="col-link">
    <h3 class="title col3">Services &amp; Support</h3>
    <ul class="footer_links col3">
        <li><a href="{{store url='help'}}">Help &amp; FAQ</a></li>
        <li><a onclick="location.reload();" href="{{store url='help'}}#howtoorder">How To Order</a></li>
        <li><a onclick="location.reload();" href="{{store url='help'}}#payments">Payments</a></li>
        <li><a onclick="location.reload();" href="{{store url='help'}}#rewardpoints">Reward Points</a></li>
    </ul>
</div>
<div class="col-link">
    <ul class="footer_links col4">
        <li><a onclick="location.reload();" href="{{store url='help'}}#shipping">Shipping &amp; Delivery</a></li>
        <li><a onclick="location.reload();" href="{{store url='help'}}#returns">Cancellation &amp; Returns</a></li>
        <li><a href="{{store url='return-policy'}}">Return Policy</a></li>
    </ul>
</div>
<script type="text/javascript">// <![CDATA[
    jQuery(document).ready(function () {

        var checking_size = 690;

        jQuery(window).resize(function () {
            var maxwidth = jQuery(window).width();
            if(maxwidth <= checking_size){
                jQuery('ul.footer_links').hide();
                jQuery('h3.title').css('cursor', 'pointer');
            }else{
                jQuery('ul.footer_links').show();
                jQuery('h3.title').css('cursor', 'inherit');
            }
        });
        jQuery('h3.title.col1').click(function () {
            if(jQuery(window).width() <= checking_size){
                jQuery('ul.footer_links.col1').toggle(200);
            }
        });
        jQuery('h3.title.col2').click(function () {
            if(jQuery(window).width() <= checking_size){
                jQuery('ul.footer_links.col2').toggle(200);
            }
        });
        jQuery('h3.title.col3').click(function () {
            if(jQuery(window).width() <= checking_size){
                jQuery('ul.footer_links.col3').toggle(200);
                jQuery('ul.footer_links.col4').toggle(200);
            }
        });
    });
// ]]></script>
EOD;

$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockId);
$block->setStores($blockStores);
$block->setIsActive(1);
$block->setContent($content);
$block->save();


// Thailand
$blockTitle = 'Site Footer Links TH';
$blockId = 'footer_links_1';
$blockStores = array($moxyth);

$content = <<<EOD
<div class="col-link">
    <h3 class="title col1">ข้อมูล</h3>
    <ul class="footer_links col1">
        <li><a href="{{store url='about-us'}}">เกี่ยวกับเรา</a></li>
        <li><a href="http://www.whatsnew.asia/">องค์กร</a></li>
        <li><a href="{{store url='contacts'}}">ติดต่อเรา</a></li>
    </ul>
</div>
<div class="col-link">
    <h3 class="title col2">นโยบายความเป็นส่วนตัว</h3>
    <ul class="footer_links col2">
        <li><a href="{{store url='terms-and-conditions'}}">ข้อกำหนดและเงื่อนไข</a></li>
        <li><a href="{{store url='privacy-policy'}}">ความเป็นส่วนตัว</a></li>
    </ul>
</div>
<div class="col-link">
    <h3 class="title col3">ช่วยเหลือ</h3>
    <ul class="footer_links col3">
        <li><a href="{{store url='help'}}">คำถามที่พบบ่อย</a></li>
        <li><a onclick="location.reload();" href="{{store url='help'}}#howtoorder">วิธีการสั่งซื้อ</a></li>
        <li><a onclick="location.reload();" href="{{store url='help'}}#payments">การชำระเงิน</a></li>
        <li><a onclick="location.reload();" href="{{store url='help'}}#rewardpoints">คะแนนสะสม</a></li>
    </ul>
</div>
<div class="col-link">
    <ul class="footer_links col4">
        <li><a onclick="location.reload();" href="{{store url='help'}}#shipping">การจัดส่งสินค้า</a></li>
        <li><a onclick="location.reload();" href="{{store url='help'}}#returns">การคืนสินค้าและขอคืนเงิน</a></li>
        <li><a href="{{store url='return-policy'}}">นโยบายการคืนสินค้า</a></li>
    </ul>
</div>
<script type="text/javascript">// <![CDATA[
    jQuery(document).ready(function () {

        var checking_size = 690;

        jQuery(window).resize(function () {
            var maxwidth = jQuery(window).width();
            if(maxwidth <= checking_size){
                jQuery('ul.footer_links').hide();
                jQuery('h3.title').css('cursor', 'pointer');
            }else{
                jQuery('ul.footer_links').show();
                jQuery('h3.title').css('cursor', 'inherit');
            }
        });
        jQuery('h3.title.col1').click(function () {
            if(jQuery(window).width() <= checking_size){
                jQuery('ul.footer_links.col1').toggle(200);
            }
        });
        jQuery('h3.title.col2').click(function () {
            if(jQuery(window).width() <= checking_size){
                jQuery('ul.footer_links.col2').toggle(200);
            }
        });
        jQuery('h3.title.col3').click(function () {
            if(jQuery(window).width() <= checking_size){
                jQuery('ul.footer_links.col3').toggle(200);
                jQuery('ul.footer_links.col4').toggle(200);
            }
        });
    });
// ]]></script>
EOD;

$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockId);
$block->setStores($blockStores);
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// Bank Image Footer
//==========================================================================
$blockTitle = 'Bank Image Footer';
$blockId = 'bank_image_footer';
$blockStores = array(0);

/** @var Mage_Cms_Model_Block $block */
$blocks = Mage::getModel('cms/block')->getCollection()
    ->addFieldToFilter('identifier', $blockId);

foreach($blocks as $block){
    if ($block->getId()) $block->delete();// if exists then delete
}
$content = <<<EOD
<div class="bank-list-images">
    <p class="payment_partners_footer">Payment Partners</p>
	<img src="{{skin url="images/bn_payment.jpg"}}" alt="" />
</div>
<div class="acommerce-logo-footer">
    <p class="shipping_partners_footer">Shipping Partners</p>
    <img src="{{skin url="images/bn_shipping.jpg"}}" alt="" />
</div>
EOD;

$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockId);
$block->setStores($blockStores);
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// Easy Reorder Site Banner
//==========================================================================
$blockTitle = 'Easy Reorder Site Banner';
$blockId = 'site_banner_top_right_second';
$blockStores = array(0);

/** @var Mage_Cms_Model_Block $block */
$blocks = Mage::getModel('cms/block')->getCollection()
    ->addFieldToFilter('identifier', $blockId);

foreach($blocks as $block){
    if ($block->getId()) $block->delete();// if exists then delete
}
$content = <<<EOD
<div class="img_top_right_en">EASY reorder <i class="fa fa-chevron-right"></i></div>
EOD;

$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockId);
$block->setStores($blockStores);
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// Key Message
//==========================================================================
$blockId = 'keymessage';

/** @var Mage_Cms_Model_Block $block */
$blocks = Mage::getModel('cms/block')->getCollection()
    ->addFieldToFilter('identifier', $blockId);

foreach ($blocks as $block) {
    if ($block->getId()) $block->delete(); // if exists then delete
}

$blockTitle = 'Key Message ENG';
$blockStores = array($moxyen);
$content = <<<EOD
<div class="keyMessBlock">
<ul id="keyMess">
<li class="key-ico keyMenu"><a class="tt" href="{{store url='help'}}#returns"> <span class="return-ico"><i class="fa fa-undo"></i><span class="tx-for-top-ico">30 Days Return Policy</span></span>
<div class="tooltip">
<div class="top">&nbsp;</div>
<div class="middle">In the rare circumstance that your item has a defect, you may send it back to us with a completed Returns Slip attached within 30 days upon receiving the item(s), a full refund will be issued to you.</div>
</div></a>
</li>
<li class="key-ico keyMenu"><a class="tt"> <span class="callcenter-ico"><i class="fa fa-phone"></i><span class="tx-for-top-ico">Customer Service: 02-106-8222</span></span>
<div class="tooltip">
<div class="top">&nbsp;</div>
<div class="middle">If you have any problems, you can call us at 02-106-8222 (10am to 7pm) and we will do our best to help you out.</div>
</div></a>
</li>
<li class="key-ico keyMenu"><a class="tt" href="{{store url='help'}}#payments"> <span class="cod-ico"><i class="fa fa-bold"></i><span class="tx-for-top-ico">Cash on Delivery</span> </span>
<div class="tooltip">
<div class="top">&nbsp;</div>
<div class="middle">Cash On delivery: With cash on delivery, we offer you the choice to pay once you have received the product from our messenger. This option carries a small charge of 50 baht/ order.</div>
</div></a>
</li>
</ul>
</div>
EOD;

$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockId);
$block->setStores($blockStores);
$block->setIsActive(1);
$block->setContent($content);
$block->save();



$blockTitle = 'Key Message THAI';
$blockStores = array($moxyth);
$content = <<<EOD
<div class="keyMessBlock">
<ul id="keyMess">
<li class="key-ico keyMenu"><a class="tt" href="{{store url='help'}}#returns"> <span class="return-ico"><i class="fa fa-undo"></i><span class="tx-for-top-ico">รับคืนฟรี ภายใน 30วัน</span></span>
<div class="tooltip">
<div class="top">&nbsp;</div>
<div class="middle">ในบางกรณีซึ่งเป็นไปได้ยากที่สินค้าของคุณจะเกิดตำหนิ,เสียหาย หรือมีข้อผิดพลาด ดังนั้นหากเกิดกรณีดังกล่าวแล้วคุณสามารถดำเนินการขอคืนสินค้าและเงินค่าสินค้าเต็มจำนวน <strong>ภายในระยะเวลา 30 วัน</strong>หลังจากที่คุณได้รับสินค้าเรียบร้อยแล้ว</div>
</div></a>
</li>
<li class="key-ico keyMenu"><a class="tt"> <span class="callcenter-ico"><i class="fa fa-phone"></i><span class="tx-for-top-ico">โทรสั่งซื้อ: 02-106-8222</span></span>
<div class="tooltip">
<div class="top">&nbsp;</div>
<div class="middle">ท่านสามารถติดต่อศูนย์บริการลูกค้าได้ที่ <strong>02-106-8222</strong><br />(จ-ส 10.00-19.00)<br />เรามีทีมงานที่ยินดีและพร้อมที่จะให้คำปรึกษากับท่าน กรุณาติดต่อเราหากท่านมีข้อสงสัยหรือได้รับความไม่สะดวกสบาย</div>
</div></a>
</li>
<li class="key-ico keyMenu"><a class="tt" href="{{store url='help'}}#payments"> <span class="cod-ico"><i class="fa fa-bold"></i><span class="tx-for-top-ico">ชำระเงินปลายทาง</span> </span>
<div class="tooltip">
<div class="top">&nbsp;</div>
<div class="middle">เป็นการเก็บเงินปลายทางจากผู้รับสินค้า โดยจะมีค่าธรรมเนียมในการเก็บเงินปลายทาง 50 บาท ต่อการสั่งซื้อต่อครั้ง ค่าธรรมเนียมนี้ไม่รวมค่าสินค้า และค่าจัดส่งสินค้า</div>
</div></a>
</li>
</ul>
</div>
EOD;

$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockId);
$block->setStores($blockStores);
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// static-sidebar-1 ENG
//==========================================================================
$blockTitle     = 'Static Sidebar (INFORMATION)';
$blockId        = 'static-sidebar-1';
$blockStores    = array($moxyen);

/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockId)
    ->getFirstItem();

if ($block->getId()) $block->delete();// if exists then delete
$content = <<<EOD
<div class="col-left sidebar">
<div class="menu-static">
<h3>Information</h3>
<ul>
<li><a href="{{store url='about-us'}}">About Us</a></li>
<li><a href="http://www.whatsnew.asia/">Corporate</a></li>
<li><a href="{{store url='contacts'}}">Contact Us</a></li>
</ul>
</div>
</div>
<script type="text/javascript">// <![CDATA[
    jQuery(document).ready(function () {
        jQuery('div.menu-static').find('ul li a').each(function () {
            var element_url = jQuery(this).attr('href');
            var current_url = window.location.href;
            if(element_url == current_url){
                if(!jQuery(this).hasClass('active')){
                    jQuery(this).addClass('active');
                }
            }
        });
    });
// ]]></script>
EOD;

$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockId);
$block->setStores($blockStores);
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// static-sidebar-1 THAI
//==========================================================================
$blockTitle     = 'Static Sidebar (ข้อมูล)';
$blockId        = 'static-sidebar-1';
$blockStores    = array($moxyth);

/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockId)
    ->getFirstItem();

if ($block->getId()) $block->delete();// if exists then delete
$content = <<<EOD
<div class="col-left sidebar">
<div class="menu-static">
<h3>ข้อมูล</h3>
<ul>
<li><a href="{{store url='about-us'}}">เกี่ยวกับเรา</a></li>
<li><a href="http://www.whatsnew.asia/">องค์กร</a></li>
<li><a href="{{store url='contacts'}}">ติดต่อเรา</a></li>
</ul>
</div>
</div>
<script type="text/javascript">// <![CDATA[
    jQuery(document).ready(function () {
        jQuery('div.menu-static').find('ul li a').each(function () {
            var element_url = jQuery(this).attr('href');
            var current_url = window.location.href;
            if(element_url == current_url){
                if(!jQuery(this).hasClass('active')){
                    jQuery(this).addClass('active');
                }
            }
        });
    });
// ]]></script>
EOD;

$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockId);
$block->setStores($blockStores);
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// static-sidebar-2 THAI
//==========================================================================
$blockTitle     = 'Static Sidebar (นโยบายความเป็นส่วนตัว)';
$blockId        = 'static-sidebar-2';
$blockStores    = array($moxyth);

/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockId)
    ->getFirstItem();

if ($block->getId()) $block->delete();// if exists then delete
$content = <<<EOD
<div class="col-left sidebar">
<div class="menu-static">
<h3>นโยบายความเป็นส่วนตัว</h3>
<ul>
<li><a href="{{store url='terms-and-conditions'}}">ข้อกำหนดและเงื่อนไข</a></li>
<li><a href="{{store url='privacy-policy'}}">ความเป็นส่วนตัว</a></li>
</ul>
</div>
</div>
<script type="text/javascript">// <![CDATA[
    jQuery(document).ready(function () {
        jQuery('div.menu-static').find('ul li a').each(function () {
            var element_url = jQuery(this).attr('href');
            var current_url = window.location.href;
            if(element_url == current_url){
                if(!jQuery(this).hasClass('active')){
                    jQuery(this).addClass('active');
                }
            }
        });
    });
// ]]></script>
EOD;

$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockId);
$block->setStores($blockStores);
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// static-sidebar-2 ENG
//==========================================================================
$blockTitle     = 'Static Sidebar (Security and Privacy)';
$blockId        = 'static-sidebar-2';
$blockStores    = array($moxyen);

/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockId)
    ->getFirstItem();

if ($block->getId()) $block->delete();// if exists then delete
$content = <<<EOD
<div class="col-left sidebar">
<div class="menu-static">
<h3>Security and Privacy</h3>
<ul>
<li><a href="{{store url='terms-and-conditions'}}">Terms &amp; Conditions</a></li>
<li><a href="{{store url='privacy-policy'}}">Privacy Policy</a></li>
</ul>
</div>
</div>
<script type="text/javascript">// <![CDATA[
    jQuery(document).ready(function () {
        jQuery('div.menu-static').find('ul li a').each(function () {
            var element_url = jQuery(this).attr('href');
            var current_url = window.location.href;
            if(element_url == current_url){
                if(!jQuery(this).hasClass('active')){
                    jQuery(this).addClass('active');
                }
            }
        });
    });
// ]]></script>
EOD;

$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockId);
$block->setStores($blockStores);
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// static-sidebar-3 ENG
//==========================================================================
$blockTitle     = 'Static Sidebar (Services and Support)';
$blockId        = 'static-sidebar-3';
$blockStores    = array($moxyen);

/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockId)
    ->getFirstItem();

if ($block->getId()) $block->delete();// if exists then delete
$content = <<<EOD
<div class="col-left sidebar">
<div class="menu-static">
<h3>Services and Support</h3>
<ul>
<li><a href="{{store url='help'}}">Help and FAQ</a></li>
<li><a onclick="location.reload();" href="{{store url='help'}}#howtoorder">How To Order</a></li>
<li><a onclick="location.reload();" href="{{store url='help'}}#payments">Payments</a></li>
<li><a onclick="location.reload();" href="{{store url='help'}}#rewardpoints">Reward Points</a></li>
<li><a onclick="location.reload();" href="{{store url='help'}}#shipping">Shipping &amp; Delivery</a></li>
<li><a onclick="location.reload();" href="{{store url='help'}}#returns">Cancellation &amp; Returns</a></li>
<li><a href="{{store url='return-policy'}}">Privacy Policy</a></li>
</ul>
</div>
</div>
<script type="text/javascript">// <![CDATA[
    jQuery(document).ready(function () {
        jQuery('div.menu-static').find('ul li a').each(function () {
            var element_url = jQuery(this).attr('href');
            var current_url = window.location.href;
            if(element_url == current_url){
                if(!jQuery(this).hasClass('active')){
                    jQuery(this).addClass('active');
                }
            }
        });
    });
// ]]></script>
EOD;

$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockId);
$block->setStores($blockStores);
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// static-sidebar-3 THAI
//==========================================================================
$blockTitle     = 'Static Sidebar (ช่วยเหลือ)';
$blockId        = 'static-sidebar-3';
$blockStores    = array($moxyth);

/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockId)
    ->getFirstItem();

if ($block->getId()) $block->delete();// if exists then delete
$content = <<<EOD
<div class="col-left sidebar">
<div class="menu-static">
<h3>ช่วยเหลือ</h3>
<ul>
<li><a href="{{store url='help'}}">คำถามที่พบบ่อย</a></li>
<li><a onclick="location.reload();" href="{{store url='help'}}#howtoorder">วิธีการสั่งซื้อ</a></li>
<li><a onclick="location.reload();" href="{{store url='help'}}#payments">การชำระเงิน</a></li>
<li><a onclick="location.reload();" href="{{store url='help'}}#rewardpoints">คะแนนสะสม</a></li>
<li><a onclick="location.reload();" href="{{store url='help'}}#shipping">การจัดส่งสินค้า</a></li>
<li><a onclick="location.reload();" href="{{store url='help'}}#returns">การคืนสินค้าและขอคืนเงิน</a></li>
<li><a href="{{store url='return-policy'}}">นโยบายการคืนสินค้า</a></li>
</ul>
</div>
</div>
<script type="text/javascript">// <![CDATA[
    jQuery(document).ready(function () {
        jQuery('div.menu-static').find('ul li a').each(function () {
            var element_url = jQuery(this).attr('href');
            var current_url = window.location.href;
            if(element_url == current_url){
                if(!jQuery(this).hasClass('active')){
                    jQuery(this).addClass('active');
                }
            }
        });
    });
// ]]></script>
EOD;

$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockId);
$block->setStores($blockStores);
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// About Us ENG
//==========================================================================
$stores = array($moxyen);
$identifier = "about-us";
$rootTemplate = "two_columns_left";
$layoutXML = <<<EOD
<reference name="left">
    <block type="cms/block" name="block-name">
        <action method="setBlockId">
            <block_id>static-sidebar-1</block_id>
        </action>
    </block>
</reference>
EOD;
/** @var Mage_Cms_Model_Page $page */
$page = Mage::getModel('cms/page')->getCollection()
    ->addStoreFilter($stores, $withAdmin = false)
    ->addFieldToFilter('identifier', $identifier)
    ->getFirstItem();
$page->setRootTemplate($rootTemplate);
$page->setLayoutUpdateXml($layoutXML);
$page->setStores($stores);
$page->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// About Us THAI
//==========================================================================
$stores = array($moxyth);
$identifier = "about-us";
$rootTemplate = "two_columns_left";
$layoutXML = <<<EOD
<reference name="left">
    <block type="cms/block" name="block-name">
        <action method="setBlockId">
            <block_id>static-sidebar-1</block_id>
        </action>
    </block>
</reference>
EOD;
/** @var Mage_Cms_Model_Page $page */
$page = Mage::getModel('cms/page')->getCollection()
    ->addStoreFilter($stores, $withAdmin = false)
    ->addFieldToFilter('identifier', $identifier)
    ->getFirstItem();
$page->setRootTemplate($rootTemplate);
$page->setLayoutUpdateXml($layoutXML);
$page->setStores($stores);
$page->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// Terms and Conditions ENG
//==========================================================================
$stores = array($moxyen);
$identifier = "terms-and-conditions";
$rootTemplate = "two_columns_left";
$layoutXML = <<<EOD
<reference name="left">
    <block type="cms/block" name="block-name">
        <action method="setBlockId">
            <block_id>static-sidebar-2</block_id>
        </action>
    </block>
</reference>
EOD;
/** @var Mage_Cms_Model_Page $page */
$page = Mage::getModel('cms/page')->getCollection()
    ->addStoreFilter($stores, $withAdmin = false)
    ->addFieldToFilter('identifier', $identifier)
    ->getFirstItem();
$page->setRootTemplate($rootTemplate);
$page->setLayoutUpdateXml($layoutXML);
$page->setStores($stores);
$page->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// Terms and Conditions THAI
//==========================================================================
$stores = array($moxyth);
$identifier = "terms-and-conditions";
$rootTemplate = "two_columns_left";
$layoutXML = <<<EOD
<reference name="left">
    <block type="cms/block" name="block-name">
        <action method="setBlockId">
            <block_id>static-sidebar-2</block_id>
        </action>
    </block>
</reference>
EOD;
/** @var Mage_Cms_Model_Page $page */
$page = Mage::getModel('cms/page')->getCollection()
    ->addStoreFilter($stores, $withAdmin = false)
    ->addFieldToFilter('identifier', $identifier)
    ->getFirstItem();
$page->setRootTemplate($rootTemplate);
$page->setLayoutUpdateXml($layoutXML);
$page->setStores($stores);
$page->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// Privacy Policy ENG
//==========================================================================
$stores = array($moxyen);
$identifier = "privacy-policy";
$rootTemplate = "two_columns_left";
$layoutXML = <<<EOD
<reference name="left">
    <block type="cms/block" name="block-name">
        <action method="setBlockId">
            <block_id>static-sidebar-2</block_id>
        </action>
    </block>
</reference>
EOD;
/** @var Mage_Cms_Model_Page $page */
$page = Mage::getModel('cms/page')->getCollection()
    ->addStoreFilter($stores, $withAdmin = false)
    ->addFieldToFilter('identifier', $identifier)
    ->getFirstItem();
$page->setRootTemplate($rootTemplate);
$page->setLayoutUpdateXml($layoutXML);
$page->setStores($stores);
$page->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// Privacy Policy THAI
//==========================================================================
$stores = array($moxyth);
$identifier = "privacy-policy";
$rootTemplate = "two_columns_left";
$layoutXML = <<<EOD
<reference name="left">
    <block type="cms/block" name="block-name">
        <action method="setBlockId">
            <block_id>static-sidebar-2</block_id>
        </action>
    </block>
</reference>
EOD;
/** @var Mage_Cms_Model_Page $page */
$page = Mage::getModel('cms/page')->getCollection()
    ->addStoreFilter($stores, $withAdmin = false)
    ->addFieldToFilter('identifier', $identifier)
    ->getFirstItem();
$page->setRootTemplate($rootTemplate);
$page->setLayoutUpdateXml($layoutXML);
$page->setStores($stores);
$page->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// Return Policy ENG
//==========================================================================
$stores = array($moxyen);
$identifier = "return-policy";
$rootTemplate = "two_columns_left";
$layoutXML = <<<EOD
<reference name="left">
    <block type="cms/block" name="block-name">
        <action method="setBlockId">
            <block_id>static-sidebar-3</block_id>
        </action>
    </block>
</reference>
EOD;
/** @var Mage_Cms_Model_Page $page */
$page = Mage::getModel('cms/page')->getCollection()
    ->addStoreFilter($stores, $withAdmin = false)
    ->addFieldToFilter('identifier', $identifier)
    ->getFirstItem();
$page->setRootTemplate($rootTemplate);
$page->setLayoutUpdateXml($layoutXML);
$page->setStores($stores);
$page->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// Return Policy THAI
//==========================================================================
$stores = array($moxyth);
$identifier = "return-policy";
$rootTemplate = "two_columns_left";
$layoutXML = <<<EOD
<reference name="left">
    <block type="cms/block" name="block-name">
        <action method="setBlockId">
            <block_id>static-sidebar-3</block_id>
        </action>
    </block>
</reference>
EOD;
/** @var Mage_Cms_Model_Page $page */
$page = Mage::getModel('cms/page')->getCollection()
    ->addStoreFilter($stores, $withAdmin = false)
    ->addFieldToFilter('identifier', $identifier)
    ->getFirstItem();
$page->setRootTemplate($rootTemplate);
$page->setLayoutUpdateXml($layoutXML);
$page->setStores($stores);
$page->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// Help ENG
//==========================================================================
$stores = array($moxyen);
$identifier = "help";
$rootTemplate = "two_columns_left";
$layoutXML = <<<EOD
<reference name="left">
    <block type="cms/block" name="block-name">
        <action method="setBlockId">
            <block_id>static-sidebar-3</block_id>
        </action>
    </block>
</reference>
EOD;
/** @var Mage_Cms_Model_Page $page */
$page = Mage::getModel('cms/page')->getCollection()
    ->addStoreFilter($stores, $withAdmin = false)
    ->addFieldToFilter('identifier', $identifier)
    ->getFirstItem();
$page->setRootTemplate($rootTemplate);
$page->setLayoutUpdateXml($layoutXML);
$page->setStores($stores);
$page->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// Help THAI
//==========================================================================
$stores = array($moxyth);
$identifier = "help";
$rootTemplate = "two_columns_left";
$layoutXML = <<<EOD
<reference name="left">
    <block type="cms/block" name="block-name">
        <action method="setBlockId">
            <block_id>static-sidebar-3</block_id>
        </action>
    </block>
</reference>
EOD;
/** @var Mage_Cms_Model_Page $page */
$page = Mage::getModel('cms/page')->getCollection()
    ->addStoreFilter($stores, $withAdmin = false)
    ->addFieldToFilter('identifier', $identifier)
    ->getFirstItem();
$page->setRootTemplate($rootTemplate);
$page->setLayoutUpdateXml($layoutXML);
$page->setStores($stores);
$page->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// Contact Us Moxy EN
//==========================================================================
$blockTitle     = 'Contact Us Moxy EN';
$blockId        = 'contact-us';
$blockStores    = array($moxyen);

/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockId)
    ->getFirstItem();

if ($block->getId()) $block->delete();// if exists then delete
$content = <<<EOD
<div id="contact-info">
<div id="contact-info-details">
<p>&nbsp;</p>
<p>Customer service :&nbsp;<br /> 8/2 Rama 3 Road, Soi 53<br /> Bang Phongpang, Yannawa<br /> Bangkok, 10120 Thailand<br /> Tel: 02-106-8222&nbsp;<br /> Facebook:&nbsp;<a href="https://www.facebook.com/moxyst" target="_blank">https://www.facebook.com/moxyst</a><br /> Email:&nbsp;<a title="send email to Moxy" href="mailto:support@moxy.co.th" target="_self">support@moxy.co.th</a></p>
<p>&nbsp;</p>
<p>Office:<br /> Whatsnew Co., Ltd.<br /> Sethiwan Tower - 19th Floor<br /> 139 Pan Road, Silom, Bangrak,<br /> Bangkok, 10500 Thailand<br /> Tel: <span>02-106-8222</span>&nbsp;<br /> Facebook:&nbsp;<a href="https://www.facebook.com/moxyst" target="_blank">https://www.facebook.com/moxyst</a><br /> Email:&nbsp;<a title="send email to Moxy" href="mailto:support@moxy.co.th" target="_self">support@moxy.co.th</a></p>
</div>
<div id="contact-info-map"><iframe style="border: 0;" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1937.973472047649!2d100.5241348!3d13.721662!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e298cdd9e5bd05%3A0x1e206241cb063178!2sSethiwan+Tower!5e0!3m2!1sen!2sth!4v1397117387281" frameborder="0" width="350" height="300"></iframe><br /><small>View <a style="color: #0000ff; text-align: left;" href="https://maps.google.com/maps/ms?msa=0&amp;msid=214150831025562562519.0004daafa9c87d88cd667&amp;hl=en&amp;ie=UTF8&amp;t=m&amp;source=embed&amp;ll=13.728265,100.537412&amp;spn=0,0">Moxy.co.th</a> in a larger map</small></div>
</div>
EOD;

$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockId);
$block->setStores($blockStores);
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// ติดต่อเรา Moxy TH
//==========================================================================
$blockTitle     = 'ติดต่อเรา Moxy TH';
$blockId        = 'contact-us';
$blockStores    = array($moxyth);

/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockId)
    ->getFirstItem();

if ($block->getId()) $block->delete();// if exists then delete
$content = <<<EOD
<div id="contact-info">
<div id="contact-info-details">
<p><br /> บริการลูกค้า:<br /> 8/2 ถนนพระราม 3 ซอย 53<br /> แขวงบางโพงพาง เขตสาทร<br /> กรุงเทพฯ ประเทศไทย 10120<br /> โทรศัพท์: 02-106-8222&nbsp;<br /> Facebook:&nbsp;<a href="https://www.facebook.com/moxyst" target="_blank">https://www.facebook.com/moxyst</a><br /> Email:&nbsp;<a title="send email to Moxy" href="mailto:support@moxy.co.th" target="_self">support@moxy.co.th</a><strong></strong></p>
<p>&nbsp;</p>
<p>ออฟฟิศ:<br />อาคารเศรษฐีวรรณ ชั้น 19<br />139 ถนนปั้น แขวงสีลม เขตบางรัก<br /> กรุงเทพฯ ประเทศไทย 10500<br /> โทรศัพท์: <span>02-106-8222</span>&nbsp;<br /> Facebook:&nbsp;<a href="https://www.facebook.com/moxyst" target="_blank">https://www.facebook.com/moxyst</a><br /> Email:&nbsp;<a title="send email to Moxy" href="mailto:support@moxy.co.th" target="_self">support@moxy.co.th</a></p>
</div>
<div id="contact-info-map"><iframe style="border: 0;" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1937.973472047649!2d100.5241348!3d13.721662!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e298cdd9e5bd05%3A0x1e206241cb063178!2sSethiwan+Tower!5e0!3m2!1sen!2sth!4v1397117387281" frameborder="0" width="350" height="300"></iframe><br /><small>View <a style="color: #0000ff; text-align: left;" href="https://maps.google.com/maps/ms?msa=0&amp;msid=214150831025562562519.0004daafa9c87d88cd667&amp;hl=en&amp;ie=UTF8&amp;t=m&amp;source=embed&amp;ll=13.728265,100.537412&amp;spn=0,0">Moxy.co.th</a> in a larger map</small></div>
</div>
EOD;

$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockId);
$block->setStores($blockStores);
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// Reorder
//==========================================================================
$blockTitle     = 'Reorder';
$blockId        = 'reorder';
$blockStores    = array(0);

/** @var Mage_Cms_Model_Block $block */
$blocks = Mage::getModel('cms/block')->getCollection()
    ->addFieldToFilter('identifier', $blockId);

foreach($blocks as $block){
    if ($block->getId()) $block->delete();// if exists then delete
}
$content = <<<EOD
<img style="width: 100%;" src="{{media url="wysiwyg/FastReorder_22-02-2016.jpg"}}" alt="" />
EOD;

$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockId);
$block->setStores($blockStores);
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// Home ENG
//==========================================================================
$title = 'MOXY The Online Shopping Destination for Women';
$identifier = "home";
$stores = array($moxyen);
$status = 1;
$content = <<<EOD
{{block type="core/template" name="subscription" as="subscription" template="subscription/subscribe.phtml"}}
EOD;
$rootTemplate = "one_column";
$layoutXML = <<<EOD
<reference name="head">
    <block type="core/text" name="homepage.metadata" after="-">
        <action method="addText"><text><![CDATA[<meta name="google-site-verification" content="_4n5alA2_c2rhcm4BQTl-KqLMwVGw_gyHr20g1fCalA" />]]></text></action>
    </block>
</reference>

<reference name="content">
    <block type="cms/block" name="home.sliders">
        <action method="setBlockId"><block_id>welcome-shortcut</block_id></action>
    </block>

    <block type="petloftcatalog/product_homegrid" name="home.product.grid1" as="homegrid1" template="petloft/catalog/product/homegrid.phtml">
        <action method="setCategoryId"><id>2655</id></action>
    </block>

    <block type="cms/block" name="home.promotion.banner">
        <action method="setBlockId"><block_id>welcome-promotion-banner</block_id></action>
    </block>

    <block type="petloftcatalog/product_homegrid" name="home.product.grid2" as="homegrid2" template="petloft/catalog/product/homegrid.phtml">
        <action method="setCategoryId"><id>2656</id></action>
    </block>
</reference>
EOD;
$metaDesc = 'Beauty, Fashion, Home Decor, Gadgets & Electronics, Health & Sports, Mom & Babies, Pet Products - shipped directly to you!';

/** @var Mage_Cms_Model_Page $page */
$page = Mage::getModel('cms/page')->getCollection()
    ->addStoreFilter($stores, $withAdmin = false)
    ->addFieldToFilter('identifier', $identifier)
    ->getFirstItem();

if($page->getId()) $page->delete();
$page = Mage::getModel('cms/page');
$page->setTitle($title);
$page->setIdentifier($identifier);
$page->setIsActive($status);
$page->setContent($content);
$page->setRootTemplate($rootTemplate);
$page->setLayoutUpdateXml($layoutXML);
$page->setMetaDescription($metaDesc);
$page->setStores($stores);
$page->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// Home THAI
//==========================================================================
$title = 'MOXY เว็บช้อปปิ้งออนไลน์ สำหรับสาวนักช้อปทุกคน';
$identifier = "home";
$stores = array($moxyth);
$status = 1;
$content = <<<EOD
{{block type="core/template" name="subscription" as="subscription" template="subscription/subscribe.phtml"}}
EOD;
$rootTemplate = "one_column";
$layoutXML = <<<EOD
<reference name="head">
    <block type="core/text" name="homepage.metadata" after="-">
        <action method="addText"><text><![CDATA[<meta name="google-site-verification" content="_4n5alA2_c2rhcm4BQTl-KqLMwVGw_gyHr20g1fCalA" />]]></text></action>
    </block>
</reference>

<reference name="content">
    <block type="cms/block" name="home.sliders">
        <action method="setBlockId"><block_id>welcome-shortcut</block_id></action>
    </block>

    <block type="petloftcatalog/product_homegrid" name="home.product.grid1" as="homegrid1" template="petloft/catalog/product/homegrid.phtml">
        <action method="setCategoryId"><id>2655</id></action>
    </block>

    <block type="cms/block" name="home.promotion.banner">
        <action method="setBlockId"><block_id>welcome-promotion-banner</block_id></action>
    </block>

    <block type="petloftcatalog/product_homegrid" name="home.product.grid2" as="homegrid1" template="petloft/catalog/product/homegrid.phtml">
        <action method="setCategoryId"><id>2656</id></action>
    </block>
</reference>
EOD;
$metaDesc = 'เครื่องสำอาง แฟชั่น ของแต่งบ้าน แก็ตเจ็ต อีเล็คทรอนิกส์ สินค้าเพื่อสุขภาพ สินค้าสำหรับแม่และเด็ก และสินค้าสำหรับสัตว์เลี้ยง พร้อมส่งตรงถึงหน้าบ้านคุณ!';

/** @var Mage_Cms_Model_Page $page */
$page = Mage::getModel('cms/page')->getCollection()
    ->addStoreFilter($stores, $withAdmin = false)
    ->addFieldToFilter('identifier', $identifier)
    ->getFirstItem();

if($page->getId()) $page->delete();
$page = Mage::getModel('cms/page');
$page->setTitle($title);
$page->setIdentifier($identifier);
$page->setIsActive($status);
$page->setContent($content);
$page->setRootTemplate($rootTemplate);
$page->setLayoutUpdateXml($layoutXML);
$page->setMetaDescription($metaDesc);
$page->setStores($stores);
$page->save();
//==========================================================================
//==========================================================================
//==========================================================================


//==========================================================================
// Easy Reorder Landing
//==========================================================================
$title = 'Easy Reorder Landing';
$identifier = "easy-reorder-landing";
$stores = array(0);
$status = 1;
$content = <<<EOD
<a href="{{store url="customer/account/login"}}">
    <img style="width: 100%;" src="{{media url="wysiwyg/FastReorder_22-02-2016.jpg"}}" alt="" />
</a>
EOD;
$rootTemplate = "one_column";

/** @var Mage_Cms_Model_Page $page */
$pages = Mage::getModel('cms/page')->getCollection()
    ->addFieldToFilter('identifier', $identifier);

foreach($pages as $page){
    if($page->getId()) $page->delete();
}
$page = Mage::getModel('cms/page');
$page->setTitle($title);
$page->setIdentifier($identifier);
$page->setIsActive($status);
$page->setContent($content);
$page->setRootTemplate($rootTemplate);
$page->setStores($stores);
$page->save();
//==========================================================================
//==========================================================================
//==========================================================================
$installer->endSetup();