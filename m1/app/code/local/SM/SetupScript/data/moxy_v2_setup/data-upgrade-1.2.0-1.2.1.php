<?php
/** @var Mage_Core_Model_Resource_Setup $this */
$installer = $this;
$installer->startSetup();

// Force the store to be admin
Mage::app()->setUpdateMode(false);
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
if (!Mage::registry('isSecureArea'))
    Mage::register('isSecureArea', 1);

$moxyen = Mage::getModel('core/store')->load('moxyen', 'code')->getId();
$moxyth = Mage::getModel('core/store')->load('moxyth', 'code')->getId();



//==========================================================================
// Site Footer Links
//==========================================================================
$blockId = 'footer_links_1';

/** @var Mage_Cms_Model_Block $block */
$blocks = Mage::getModel('cms/block')->getCollection()
    ->addFieldToFilter('identifier', $blockId);

foreach($blocks as $block){
    if ($block->getId()) $block->delete();// if exists then delete
}

// English
$blockTitle = 'Site Footer Links EN';
$blockId = 'footer_links_1';
$blockStores = array($moxyen);

$content = <<<EOD
<div class="col-link">
    <h3 class="title col1">Information</h3>
    <ul class="footer_links col1">
        <li><a href="{{store url='about-us'}}">About Us</a></li>
        <li><a href="{{store url='newsroom'}}">Newsroom</a></li>
        <!--<li><a href="http://www.whatsnew.asia/">Corporate</a></li>-->
        <li><a href="{{store url='contacts'}}">Contact Us</a></li>
    </ul>
</div>
<div class="col-link">
    <h3 class="title col2">Security &amp; Privacy</h3>
    <ul class="footer_links col2">
        <li><a href="{{store url='terms-and-conditions'}}">Terms &amp; Conditions</a></li>
        <li><a href="{{store url='privacy-policy'}}">Privacy Policy</a></li>
    </ul>
</div>
<div class="col-link">
    <h3 class="title col3">Services &amp; Support</h3>
    <ul class="footer_links col3">
        <li><a href="{{store url='help'}}">Help &amp; FAQ</a></li>
        <li><a onclick="location.reload();" href="{{store url='help'}}#howtoorder">How To Order</a></li>
        <li><a onclick="location.reload();" href="{{store url='help'}}#payments">Payments</a></li>
        <li><a onclick="location.reload();" href="{{store url='help'}}#rewardpoints">Reward Points</a></li>
    </ul>
</div>
<div class="col-link">
    <ul class="footer_links col4">
        <li><a onclick="location.reload();" href="{{store url='help'}}#shipping">Shipping &amp; Delivery</a></li>
        <li><a onclick="location.reload();" href="{{store url='help'}}#returns">Cancellation &amp; Returns</a></li>
        <li><a href="{{store url='return-policy'}}">Return Policy</a></li>
    </ul>
</div>
<script type="text/javascript">// <![CDATA[
    jQuery(document).ready(function () {

        var checking_size = 690;

        jQuery(window).resize(function () {
            var maxwidth = jQuery(window).width();
            if(maxwidth <= checking_size){
                jQuery('ul.footer_links').hide();
                jQuery('h3.title').css('cursor', 'pointer');
            }else{
                jQuery('ul.footer_links').show();
                jQuery('h3.title').css('cursor', 'inherit');
            }
        });
        jQuery('h3.title.col1').click(function () {
            if(jQuery(window).width() <= checking_size){
                jQuery('ul.footer_links.col1').toggle(200);
            }
        });
        jQuery('h3.title.col2').click(function () {
            if(jQuery(window).width() <= checking_size){
                jQuery('ul.footer_links.col2').toggle(200);
            }
        });
        jQuery('h3.title.col3').click(function () {
            if(jQuery(window).width() <= checking_size){
                jQuery('ul.footer_links.col3').toggle(200);
                jQuery('ul.footer_links.col4').toggle(200);
            }
        });
    });
// ]]></script>
EOD;

$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockId);
$block->setStores($blockStores);
$block->setIsActive(1);
$block->setContent($content);
$block->save();


// Thailand
$blockTitle = 'Site Footer Links TH';
$blockId = 'footer_links_1';
$blockStores = array($moxyth);

$content = <<<EOD
<div class="col-link">
    <h3 class="title col1">ข้อมูล</h3>
    <ul class="footer_links col1">
        <li><a href="{{store url='about-us'}}">เกี่ยวกับเรา</a></li>
        <li><a href="{{store url='newsroom'}}">ห้องข่าว</a></li>
        <!--<li><a href="http://www.whatsnew.asia/">องค์กร</a></li>-->
        <li><a href="{{store url='contacts'}}">ติดต่อเรา</a></li>
    </ul>
</div>
<div class="col-link">
    <h3 class="title col2">นโยบายความเป็นส่วนตัว</h3>
    <ul class="footer_links col2">
        <li><a href="{{store url='terms-and-conditions'}}">ข้อกำหนดและเงื่อนไข</a></li>
        <li><a href="{{store url='privacy-policy'}}">ความเป็นส่วนตัว</a></li>
    </ul>
</div>
<div class="col-link">
    <h3 class="title col3">ช่วยเหลือ</h3>
    <ul class="footer_links col3">
        <li><a href="{{store url='help'}}">คำถามที่พบบ่อย</a></li>
        <li><a onclick="location.reload();" href="{{store url='help'}}#howtoorder">วิธีการสั่งซื้อ</a></li>
        <li><a onclick="location.reload();" href="{{store url='help'}}#payments">การชำระเงิน</a></li>
        <li><a onclick="location.reload();" href="{{store url='help'}}#rewardpoints">คะแนนสะสม</a></li>
    </ul>
</div>
<div class="col-link">
    <ul class="footer_links col4">
        <li><a onclick="location.reload();" href="{{store url='help'}}#shipping">การจัดส่งสินค้า</a></li>
        <li><a onclick="location.reload();" href="{{store url='help'}}#returns">การคืนสินค้าและขอคืนเงิน</a></li>
        <li><a href="{{store url='return-policy'}}">นโยบายการคืนสินค้า</a></li>
    </ul>
</div>
<script type="text/javascript">// <![CDATA[
    jQuery(document).ready(function () {

        var checking_size = 690;

        jQuery(window).resize(function () {
            var maxwidth = jQuery(window).width();
            if(maxwidth <= checking_size){
                jQuery('ul.footer_links').hide();
                jQuery('h3.title').css('cursor', 'pointer');
            }else{
                jQuery('ul.footer_links').show();
                jQuery('h3.title').css('cursor', 'inherit');
            }
        });
        jQuery('h3.title.col1').click(function () {
            if(jQuery(window).width() <= checking_size){
                jQuery('ul.footer_links.col1').toggle(200);
            }
        });
        jQuery('h3.title.col2').click(function () {
            if(jQuery(window).width() <= checking_size){
                jQuery('ul.footer_links.col2').toggle(200);
            }
        });
        jQuery('h3.title.col3').click(function () {
            if(jQuery(window).width() <= checking_size){
                jQuery('ul.footer_links.col3').toggle(200);
                jQuery('ul.footer_links.col4').toggle(200);
            }
        });
    });
// ]]></script>
EOD;

$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockId);
$block->setStores($blockStores);
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================


//==========================================================================
// static-sidebar-1 ENG
//==========================================================================
$blockTitle     = 'Static Sidebar (INFORMATION)';
$blockId        = 'static-sidebar-1';
$blockStores    = array($moxyen);

/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockId)
    ->getFirstItem();

if ($block->getId()) $block->delete();// if exists then delete
$content = <<<EOD
<div class="col-left sidebar">
<div class="menu-static">
<h3>Information</h3>
<ul>
<li><a href="{{store url='about-us'}}">About Us</a></li>
<li><a href="{{store url='newsroom'}}">Newsroom</a></li>
<!--<li><a href="http://www.whatsnew.asia/">Corporate</a></li>-->
<li><a href="{{store url='contacts'}}">Contact Us</a></li>
</ul>
</div>
</div>
<script type="text/javascript">// <![CDATA[
    jQuery(document).ready(function () {
        jQuery('div.menu-static').find('ul li a').each(function () {
            var element_url = jQuery(this).attr('href');
            var current_url = window.location.href;
            if(element_url == current_url){
                if(!jQuery(this).hasClass('active')){
                    jQuery(this).addClass('active');
                }
            }
        });
    });
// ]]></script>
EOD;

$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockId);
$block->setStores($blockStores);
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// static-sidebar-1 THAI
//==========================================================================
$blockTitle     = 'Static Sidebar (ข้อมูล)';
$blockId        = 'static-sidebar-1';
$blockStores    = array($moxyth);

/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockId)
    ->getFirstItem();

if ($block->getId()) $block->delete();// if exists then delete
$content = <<<EOD
<div class="col-left sidebar">
<div class="menu-static">
<h3>ข้อมูล</h3>
<ul>
<li><a href="{{store url='about-us'}}">เกี่ยวกับเรา</a></li>
<li><a href="{{store url='newsroom'}}">ห้องข่าว</a></li>
<!--<li><a href="http://www.whatsnew.asia/">องค์กร</a></li>-->
<li><a href="{{store url='contacts'}}">ติดต่อเรา</a></li>
</ul>
</div>
</div>
<script type="text/javascript">// <![CDATA[
    jQuery(document).ready(function () {
        jQuery('div.menu-static').find('ul li a').each(function () {
            var element_url = jQuery(this).attr('href');
            var current_url = window.location.href;
            if(element_url == current_url){
                if(!jQuery(this).hasClass('active')){
                    jQuery(this).addClass('active');
                }
            }
        });
    });
// ]]></script>
EOD;

$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockId);
$block->setStores($blockStores);
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// Newsroom
//==========================================================================
$stores = array(0);
$identifier = "newsroom";
$rootTemplate = "one_column";
$layoutXML = '';
$pageTitle = 'Newsroom';
$pageStatus = 1;
$contentHead = 'Newsroom';
$content = <<<EOD
<div class="press-coverage">
    <ul>
        <li>
            <a href="http://www.bloomberg.com/news/articles/2016-02-24/facebook-co-founder-saverin-among-investors-in-indonesia-s-orami" title="Facebook Co-Founder Saverin Among Investors in Indonesia's Orami">
                <img src="{{media url="wysiwyg/press_coverage/1.jpg"}}" alt=""/>
            </a>
        </li>
        <li>
            <a href="http://techcrunch.com/2016/02/23/newly-merged-moxybilna-lands-15m-for-female-focused-e-commerce-in-southeast-asia/" title="Newly Merged MoxyBilna Lands $15M For Female-Focused E-Commerce In Southeast Asia">
                <img src="{{media url="wysiwyg/press_coverage/2.jpg"}}" alt=""/>
            </a>
        </li>
        <li>
            <a href="https://www.techinasia.com/moxybilna-raises-15-million-is-now-called-orami" title="Company formerly known as MoxyBilna raises $15m, relaunches as Orami">
                <img src="{{media url="wysiwyg/press_coverage/3.jpg"}}" alt=""/>
            </a>
        </li>
        <li>
            <a href="https://e27.co/moxybilna-call-them-orami-and-they-have-secured-a-us15m-funding-20160224/" title="MoxyBilna? Call them Orami, and they have secured a US$15M funding">
                <img src="{{media url="wysiwyg/press_coverage/4.jpg"}}" alt=""/>
            </a>
        </li>
        <li>
            <a href="http://www.businesstimes.com.sg/technology/facebook-co-founder-saverin-among-investors-in-indonesias-orami" title="Facebook co-founder Saverin among investors in Indonesia's Orami">
                <img src="{{media url="wysiwyg/press_coverage/5.jpg"}}" alt=""/>
            </a>
        </li>
    </ul>
</div>
EOD;

/** @var Mage_Cms_Model_Page $page */
$page = Mage::getModel('cms/page')->getCollection()
    ->addStoreFilter($stores, $withAdmin = false)
    ->addFieldToFilter('identifier', $identifier)
    ->getFirstItem();

if($page->getId()) $page->delete();
$page = Mage::getModel('cms/page');
$page->setTitle($pageTitle);
$page->setIdentifier($identifier);
$page->setIsActive($pageStatus);
$page->setContentHeading($contentHead);
$page->setContent($content);
$page->setRootTemplate($rootTemplate);
$page->setLayoutUpdateXml($layoutXML);
$page->setStores($stores);
$page->save();
//==========================================================================
//==========================================================================
//==========================================================================
$installer->endSetup();