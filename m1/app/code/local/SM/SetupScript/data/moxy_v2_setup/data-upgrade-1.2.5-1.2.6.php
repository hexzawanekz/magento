<?php

/** @var Mage_Core_Model_Resource_Setup $this */
$installer = $this;
$installer->startSetup();

// Force the store to be admin
Mage::app()->setUpdateMode(false);
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
if (!Mage::registry('isSecureArea'))
    Mage::register('isSecureArea', 1);

$moxyen = Mage::getModel('core/store')->load('moxyen', 'code')->getId();
$moxyth = Mage::getModel('core/store')->load('moxyth', 'code')->getId();

//==========================================================================
// Key Message ENG
//==========================================================================
$blockId = 'keymessage';

/** @var Mage_Cms_Model_Block $block */
$blocks = Mage::getModel('cms/block')->getCollection()
    ->addFieldToFilter('identifier', $blockId);

foreach ($blocks as $block) {
    if ($block->getId()) $block->delete(); // if exists then delete
}

$blockTitle = 'Key Message ENG';
$blockStores = array($moxyen);
$content = <<<EOD
<div class="keyMessBlock">
<ul id="keyMess">
<li class="key-ico keyMenu"><a class="tt" href="{{store url='help'}}#returns"> <span class="return-ico"><i class="fa fa-undo"></i><span class="tx-for-top-ico">30 Days Return Policy</span></span>
<div class="tooltip">
<div class="top">&nbsp;</div>
<div class="middle">In the rare circumstance that your item has a defect, you may send it back to us with a completed Returns Slip attached within 30 days upon receiving the item(s), a full refund will be issued to you.</div>
</div></a>
</li>
<li class="key-ico keyMenu"><a class="tt"> <span class="callcenter-ico"><i class="fa fa-phone"></i><span class="tx-for-top-ico">Customer Service: 02-106-8222</span></span>
<div class="tooltip">
<div class="top">&nbsp;</div>
<div class="middle">If you have any problems, you can call us at 02-106-8222 (10am to 7pm) and we will do our best to help you out.</div>
</div></a>
</li>
</ul>
</div>
EOD;

$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockId);
$block->setStores($blockStores);
$block->setIsActive(1);
$block->setContent($content);
$block->save();


//==========================================================================
// Key Message TH
//==========================================================================
$blockTitle = 'Key Message THAI';
$blockStores = array($moxyth);
$content = <<<EOD
<div class="keyMessBlock">
<ul id="keyMess">
<li class="key-ico keyMenu"><a class="tt" href="{{store url='help'}}#returns"> <span class="return-ico"><i class="fa fa-undo"></i><span class="tx-for-top-ico">รับคืนฟรี ภายใน 30วัน</span></span>
<div class="tooltip">
<div class="top">&nbsp;</div>
<div class="middle">ในบางกรณีซึ่งเป็นไปได้ยากที่สินค้าของคุณจะเกิดตำหนิ,เสียหาย หรือมีข้อผิดพลาด ดังนั้นหากเกิดกรณีดังกล่าวแล้วคุณสามารถดำเนินการขอคืนสินค้าและเงินค่าสินค้าเต็มจำนวน <strong>ภายในระยะเวลา 30 วัน</strong>หลังจากที่คุณได้รับสินค้าเรียบร้อยแล้ว</div>
</div></a>
</li>
<li class="key-ico keyMenu"><a class="tt"> <span class="callcenter-ico"><i class="fa fa-phone"></i><span class="tx-for-top-ico">โทรสั่งซื้อ: 02-106-8222</span></span>
<div class="tooltip">
<div class="top">&nbsp;</div>
<div class="middle">ท่านสามารถติดต่อศูนย์บริการลูกค้าได้ที่ <strong>02-106-8222</strong><br />(จ-ส 10.00-19.00)<br />เรามีทีมงานที่ยินดีและพร้อมที่จะให้คำปรึกษากับท่าน กรุณาติดต่อเราหากท่านมีข้อสงสัยหรือได้รับความไม่สะดวกสบาย</div>
</div></a>
</li>
</ul>
</div>
EOD;

$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockId);
$block->setStores($blockStores);
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================
$installer->endSetup();