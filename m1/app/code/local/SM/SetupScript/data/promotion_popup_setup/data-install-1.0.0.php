<?php
/**WT-240**/

// init Magento
Mage::app();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

// get store ids
$en     = Mage::getModel('core/store')->load('en', 'code')->getId();// Petloft
$th     = Mage::getModel('core/store')->load('th', 'code')->getId();
$len    = Mage::getModel('core/store')->load('len', 'code')->getId();// Lafema
$lth    = Mage::getModel('core/store')->load('lth', 'code')->getId();
$ven    = Mage::getModel('core/store')->load('ven', 'code')->getId();// Venbi
$vth    = Mage::getModel('core/store')->load('vth', 'code')->getId();
$sen    = Mage::getModel('core/store')->load('sen', 'code')->getId();// Sanoga
$sth    = Mage::getModel('core/store')->load('sth', 'code')->getId();
$moxyen = Mage::getModel('core/store')->load('moxyen', 'code')->getId();// Moxy
$moxyth = Mage::getModel('core/store')->load('moxyth', 'code')->getId();

// Petloft EN
//==========================================================================
//==========================================================================
$blockTitle = "Promotion Banner EN";
$blockIdentifier = "promo_banner";
$blockStores = $en;
$blockIsActive = 1;

// delete old block if have
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');

$content = <<<EOD
<div class="promo_banner"><img src="{{media url="wysiwyg/915x90.jpg"}}" alt="Promotion Popup" /> <button id="cboxClose" type="button">close</button></div>
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$enBlock = Mage::getModel('cms/block');
$enBlock->setTitle($blockTitle);
$enBlock->setIdentifier($blockIdentifier);
$enBlock->setStores($blockStores);
$enBlock->setIsActive(0);
$enBlock->setContent($content);
$enBlock->save();
//==========================================================================
//==========================================================================

// Petloft TH
//==========================================================================
//==========================================================================
$blockTitle = "Promotion Banner TH";
$blockIdentifier = "promo_banner";
$blockStores = $th;
$blockIsActive = 1;

// delete old block if have
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');

$content = <<<EOD
<div class="promo_banner"><img src="{{media url="wysiwyg/915x90.jpg"}}" alt="Promotion Popup" /> <button id="cboxClose" type="button">close</button></div>
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$enBlock = Mage::getModel('cms/block');
$enBlock->setTitle($blockTitle);
$enBlock->setIdentifier($blockIdentifier);
$enBlock->setStores($blockStores);
$enBlock->setIsActive(0);
$enBlock->setContent($content);
$enBlock->save();
//==========================================================================
//==========================================================================

// VEN
//==========================================================================
//==========================================================================
$blockTitle = "Promotion Banner VEN";
$blockIdentifier = "promo_banner";
$blockStores = $ven;
$blockIsActive = 1;

// delete old block if have
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');

$content = <<<EOD
<div class="promo_banner"><img src="{{media url="wysiwyg/915x90.jpg"}}" alt="Promotion Popup" /> <button id="cboxClose" type="button">close</button></div>
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$enBlock = Mage::getModel('cms/block');
$enBlock->setTitle($blockTitle);
$enBlock->setIdentifier($blockIdentifier);
$enBlock->setStores($blockStores);
$enBlock->setIsActive(0);
$enBlock->setContent($content);
$enBlock->save();
//==========================================================================
//==========================================================================

// VTH
//==========================================================================
//==========================================================================
$blockTitle = "Promotion Banner VTH";
$blockIdentifier = "promo_banner";
$blockStores = $vth;
$blockIsActive = 1;

// delete old block if have
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');

$content = <<<EOD
<div class="promo_banner"><img src="{{media url="wysiwyg/915x90.jpg"}}" alt="Promotion Popup" /> <button id="cboxClose" type="button">close</button></div>
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$enBlock = Mage::getModel('cms/block');
$enBlock->setTitle($blockTitle);
$enBlock->setIdentifier($blockIdentifier);
$enBlock->setStores($blockStores);
$enBlock->setIsActive(0);
$enBlock->setContent($content);
$enBlock->save();
//==========================================================================
//==========================================================================

// Sanoga EN
//==========================================================================
//==========================================================================
$blockTitle = "Promotion Banner Sanoga EN";
$blockIdentifier = "promo_banner";
$blockStores = $sen;
$blockIsActive = 1;

// delete old block if have
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');

$content = <<<EOD
<div class="promo_banner"><img src="{{media url="wysiwyg/915x90.jpg"}}" alt="Promotion Popup" /> <button id="cboxClose" type="button">close</button></div>
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$enBlock = Mage::getModel('cms/block');
$enBlock->setTitle($blockTitle);
$enBlock->setIdentifier($blockIdentifier);
$enBlock->setStores($blockStores);
$enBlock->setIsActive(0);
$enBlock->setContent($content);
$enBlock->save();
//==========================================================================
//==========================================================================

// Sanoga TH
//==========================================================================
//==========================================================================
$blockTitle = "Promotion Banner Sanoga TH";
$blockIdentifier = "promo_banner";
$blockStores = $sth;
$blockIsActive = 1;

// delete old block if have
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');

$content = <<<EOD
<div class="promo_banner"><img src="{{media url="wysiwyg/915x90.jpg"}}" alt="Promotion Popup" /> <button id="cboxClose" type="button">close</button></div>
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$enBlock = Mage::getModel('cms/block');
$enBlock->setTitle($blockTitle);
$enBlock->setIdentifier($blockIdentifier);
$enBlock->setStores($blockStores);
$enBlock->setIsActive(0);
$enBlock->setContent($content);
$enBlock->save();
//==========================================================================
//==========================================================================

// LAFEMA EN
//==========================================================================
//==========================================================================
$blockTitle = "Promotion Banner LAFEMA EN";
$blockIdentifier = "promo_banner";
$blockStores = $len;
$blockIsActive = 1;

// delete old block if have
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');

$content = <<<EOD
<div class="promo_banner"><img src="{{media url="wysiwyg/915x90.jpg"}}" alt="Promotion Popup" /> <button id="cboxClose" type="button">close</button></div>
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$enBlock = Mage::getModel('cms/block');
$enBlock->setTitle($blockTitle);
$enBlock->setIdentifier($blockIdentifier);
$enBlock->setStores($blockStores);
$enBlock->setIsActive(0);
$enBlock->setContent($content);
$enBlock->save();
//==========================================================================
//==========================================================================

// Lafema TH
//==========================================================================
//==========================================================================
$blockTitle = "Promotion Banner Lafema TH";
$blockIdentifier = "promo_banner";
$blockStores = $lth;
$blockIsActive = 1;

// delete old block if have
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');

$content = <<<EOD
<div class="promo_banner"><img src="{{media url="wysiwyg/915x90.jpg"}}" alt="Promotion Popup" /> <button id="cboxClose" type="button">close</button></div>
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$enBlock = Mage::getModel('cms/block');
$enBlock->setTitle($blockTitle);
$enBlock->setIdentifier($blockIdentifier);
$enBlock->setStores($blockStores);
$enBlock->setIsActive(0);
$enBlock->setContent($content);
$enBlock->save();
//==========================================================================
//==========================================================================

// Moxy EN
//==========================================================================
//==========================================================================
$blockTitle = "Promotion Banner Moxy EN";
$blockIdentifier = "promo_banner";
$blockStores = $moxyen;
$blockIsActive = 1;

// delete old block if have
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');

$content = <<<EOD
<div class="promo_banner"><img src="{{media url="wysiwyg/915x90.jpg"}}" alt="Promotion Popup" /> <button id="cboxClose" type="button">close</button></div>
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$enBlock = Mage::getModel('cms/block');
$enBlock->setTitle($blockTitle);
$enBlock->setIdentifier($blockIdentifier);
$enBlock->setStores($blockStores);
$enBlock->setIsActive(0);
$enBlock->setContent($content);
$enBlock->save();
//==========================================================================
//==========================================================================

// Moxy TH
//==========================================================================
//==========================================================================
$blockTitle = "Promotion Banner Moxy TH";
$blockIdentifier = "promo_banner";
$blockStores = $moxyth;
$blockIsActive = 1;

// delete old block if have
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');

$content = <<<EOD
<div class="promo_banner"><img src="{{media url="wysiwyg/915x90.jpg"}}" alt="Promotion Popup" /> <button id="cboxClose" type="button">close</button></div>
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$enBlock = Mage::getModel('cms/block');
$enBlock->setTitle($blockTitle);
$enBlock->setIdentifier($blockIdentifier);
$enBlock->setStores($blockStores);
$enBlock->setIsActive(0);
$enBlock->setContent($content);
$enBlock->save();
//==========================================================================
//==========================================================================