<?php
/** @var Mage_Core_Model_Resource_Setup $install */
$install = $this;


$install->startSetup();
/** LAFEMA EN & TH */
/** @var Mage_Core_Model_Store $storeModel */
$storeModel = Mage::getModel('core/store')->load('len', 'code');
$storeId = $storeModel->getId();

/** @var Mage_Cms_Model_Block $model */
$model = Mage::getModel('cms/block');

$block = $model->getCollection()->addStoreFilter($storeId)->addFieldToFilter('identifier', 'slider_1')->getFirstItem();

/** @var Mage_Cms_Model_Block $block */
$block = $model->load($block->getId());

$slider = <<<HTML
<div class="homepage-main">
	<div id="container">
		<div id="slideshow">
			<ul id="slide-nav">
				<li id="prev"><a class="control-s pre-s" href="#">Previous</a></li>
				<li id="next"><a class="control-s nxt-s" href="#">Next</a></li>
			</ul>
			<ul id="home-slides">
				<li><a href="http://www.lafema.com/th/makeup/tools-brushes.html"><img src="{{media url="wysiwyg/EDM-Lafema-Mon-18-08-14-HP-Banner.jpg"}}" alt="Makeu tools up tp 40%" /></a>
					<div class="caption">Makeup brushes and tools Save up to 40%</div>
				</li>
				<li><a href="http://www.lafema.com/th/skincare/face-care/anti-aging.html"><img src="{{media url="wysiwyg/EDM-Lafema-Wed-13-08-14-HP-Banners.jpg"}}" alt="Anti-Aging Skin Care" /></a>
					<div class="caption">Anti-Aging Skin Care save up to 71%</div>
				</li>
				<li><a href="http://www.lafema.com/th/makeup/eyes.html?cat=%E0%B8%84%E0%B8%B4%E0%B9%89%E0%B8%A7"> <img src="{{media url="wysiwyg/Eyebrows-lafema-homepage-banner.jpg"}}" alt="eyebrows" /></a>
					<div class="caption">Eyebrows</div>
				</li>
				<li><a href="{{store url='makeup.html'}}"> <img src="{{media url="wysiwyg/Lafema/Lafema-free200-HP-Banner.jpg"}}" alt="" /> </a>
					<div class="caption">Sign up 200</div>
				</li>
			</ul>
		</div>
	</div>
</div>
<div class="homepage-bannerl1"><a href="http://www.lafema.com/?popup=acommerce?popup=acommerce"> <img src="{{media url="wysiwyg/Lafema/Lafema-Side-Banner-SIGNUP-200.jpg"}}" alt="Sign up 200" /> </a></div>
<div class="homepage-bannerl1"><a href="http://www.lafema.com/th/help/#shipping"><img src="{{media url="wysiwyg/Lafema/Lafema-Side-Banner-Free-Delivery_1.jpg"}}" alt="free shipping free cod" /></a></div>
<div class="homepage-bannerl2"><a href="http://www.lafema.com/en/urban-decay-naked-3-eyeshadow-palette.html"> <img src="{{media url="wysiwyg/Lafema/Lafema-Side-Banner-Urban-Decay.jpg"}}" alt="urban decay naked 3" /> </a></div>
HTML;


$block->setData('content', $slider);
$block->save();

/** End LAFEMA EN & TH */

$install->endSetup();