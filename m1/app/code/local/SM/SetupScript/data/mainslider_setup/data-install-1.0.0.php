<?php
/**
 * Created by PhpStorm.
 * User: SMART
 * Date: 6/5/2015
 * Time: 10:24 AM
 */

/** @var Mage_Core_Model_Resource_Setup $install */
$install = $this;


$install->startSetup();
/** Petloft EN */
/** @var Mage_Core_Model_Store $storeModel */
$storeModel = Mage::getModel('core/store')->load('EN', 'code');
$storeId = $storeModel->getId();

/** @var Mage_Cms_Model_Block $model */
$model = Mage::getModel('cms/block');

$block = $model->getCollection()->addStoreFilter($storeId)->addFieldToFilter('identifier', 'slider_1')->getFirstItem();

/** @var Mage_Cms_Model_Block $block */
$block = $model->load($block->getId());

$slider = <<<HTML
<div class="homepage-main" style="float: left; width: 610px; margin: 0px 30px;">
	<div id="container" style="width: 610px;">
		<div id="slideshow" style="margin: 0px 15px 25px 15px; height: 400px; width: 610px;">
			<ul id="slide-nav">
				<li id="prev">
					<a class="control-s pre-s" href="#">Previous</a>
				</li>
				<li id="next" style="left: 500px;">
					<a class="control-s nxt-s" href="#">Next</a>
				</li>
			</ul>
			<ul id="home-slides">
				<li>
					<a style="width: 610px;" href="http://www.petloft.com/en/"><img src="{{media url="wysiwyg/petloft-big_pack-homepage-banner.jpg"}}" alt="Big Pack Sale" /></a>
					<div class="caption">Discount for Big Pack Pet food free delivery</div>
				</li>
				<li>
					<a style="width: 610px;" href="http://www.petloft.com/en/catalogsearch/result/index/?pet_food_brands=1539&amp;q=husse"><img src="{{media url="wysiwyg/petloft-husse-pet-food-homepage-banner.jpg"}}" alt="Husse Pet Food" /></a>
					<div class="caption">Husse Pet Food start from 199</div>
				</li>
				<li>
					<a style="width: 610px;" href="http://www.petloft.com/en/catalogsearch/result/index/?pet_food_brands=1940&amp;q=bozzi"><img src="{{media url="wysiwyg/petloft_bozzi_homepage-banner.jpg"}}" alt="Bozzi Shampoo" /></a>
					<div class="caption">Bozzi shampoo sale up to 23%</div>
				</li>
				<li>
					<a style="width: 610px;" href="http://www.petloft.com/en/cat-food/cat-food-brands/toro-toro.html"><img src="{{media url="wysiwyg/petloft-torotoro-cat-food-homepage-banner.jpg"}}" alt="Toro Toro cat snacks" /></a>
					<div class="caption">Toro Toro Sale</div>
				</li>
				<li>
					<a style="width: 610px;" href="http://www.petloft.com/th/clearance-sale.html"><img src="{{media url="wysiwyg/clearance-petloft-homepage-banner_1.jpg"}}" alt="clearance pet" /></a>
					<div class="caption">clearance pet</div>
				</li>
				<li>
					<a style="width: 610px;" href="{{store url=''}}"><img src="{{media url="wysiwyg/ktc-petloft-promotion-homepage-banner-TH.jpg"}}" alt="ktc" /></a>
					<div class="caption">KTC 0%</div>
				</li>
			</ul>
		</div>
	</div>
</div>
<div class="homepage-bannerl1" style="margin-bottom: 15px;">
	<a href="http://www.petloft.com/?popup=acommerce?popup=acommerce"> <img src="{{media url="wysiwyg/petloft-sign-up-free-200-small-banner_copy_2-2.jpg"}}" alt="Sign up, Free 200 Baht" /> </a>
</div>
<!-- start small banner 1 -->
<div class="homepage-bannerl1">
	<a href="http://www.petloft.com/th/help/#shipping"> <img src="{{media url="wysiwyg/petloft-free-shipping-free-cod-small-banner_copy-2.jpg"}}" alt="Free Shipping" /> </a>
</div>
<!-- end small banner 1 -->
<p>&nbsp;</p>
<!-- start small banner 2 -->
<div class="homepage-bannerl2">
	<a href="http://www.petloft.com/th/catalogsearch/result/?q=Royal+Canin"> <img src="{{media url="wysiwyg/petloft-royal-canin-small-banner-2.jpg"}}" alt="Royal Canin Pet Food" /> </a>
</div>
<!-- end small banner 2 -->
<p>&nbsp;</p>
HTML;


$block->setData('content', $slider);
$block->save();

/** End Petloft EN */

/** Petloft Thai */
/** @var Mage_Core_Model_Store $storeModel */
$storeModel = Mage::getModel('core/store')->load('TH', 'code');
$storeId = $storeModel->getId();

/** @var Mage_Cms_Model_Block $model */
$model = Mage::getModel('cms/block');

$block = $model->getCollection()->addStoreFilter($storeId)->addFieldToFilter('identifier', 'slider_1')->getFirstItem();

/** @var Mage_Cms_Model_Block $block */
$block = $model->load($block->getId());

$slider = <<<HTML
<div class="homepage-main" style="float: left; width: 610px; margin: 0px 30px;">
	<div id="container" style="width: 610px;">
		<div id="slideshow" style="margin: 0px 15px 25px 15px; height: 400px; width: 610px;">
			<ul id="slide-nav">
				<li id="prev">
					<a class="control-s pre-s" href="#">Previous</a>
				</li>
				<li id="next" style="left: 500px;">
					<a class="control-s nxt-s" href="#">Next</a>
				</li>
			</ul>
			<ul id="home-slides">
				<li>
					<a style="width: 610px;" href="http://www.petloft.com/th/"><img src="{{media url="wysiwyg/petloft-big_pack-homepage-banner.jpg"}}" alt="Big Pack Sale" /></a>
					<div class="caption">Discount for Big Pack Pet food free delivery</div>
				</li>
				<li>
					<a style="width: 610px;" href="http://www.petloft.com/th/catalogsearch/result/index/?pet_food_brands=1539&amp;q=husse"><img src="{{media url="wysiwyg/petloft-husse-pet-food-homepage-banner.jpg"}}" alt="Husse Pet Food" /></a>
					<div class="caption">Husse Pet Food start from 199</div>
				</li>
				<li>
					<a style="width: 610px;" href="http://www.petloft.com/th/catalogsearch/result/index/?pet_food_brands=1940&amp;q=bozzi"><img src="{{media url="wysiwyg/petloft_bozzi_homepage-banner.jpg"}}" alt="Bozzi Shampoo" /></a>
					<div class="caption">Bozzi shampoo sale up to 23%</div>
				</li>
				<li>
					<a style="width: 610px;" href="http://www.petloft.com/th/cat-food/cat-food-brands/toro-toro.html"><img src="{{media url="wysiwyg/petloft-torotoro-cat-food-homepage-banner.jpg"}}" alt="Toro Toro cat snacks" /></a>
					<div class="caption">Toro Toro Sale</div>
				</li>
				<li>
					<a style="width: 610px;" href="http://www.petloft.com/th/clearance-sale.html"><img src="{{media url="wysiwyg/clearance-petloft-homepage-banner_1.jpg"}}" alt="clearance pet" /></a>
					<div class="caption">clearance pet</div>
				</li>
				<li>
					<a style="width: 610px;" href="{{store url=''}}"><img src="{{media url="wysiwyg/ktc-petloft-promotion-homepage-banner-TH.jpg"}}" alt="ktc" /></a>
					<div class="caption">KTC 0%</div>
				</li>
			</ul>
		</div>
	</div>
</div>
<div class="homepage-bannerl1" style="margin-bottom: 15px;">
	<a href="http://www.petloft.com/?popup=acommerce?popup=acommerce"> <img src="{{media url="wysiwyg/petloft-sign-up-free-200-small-banner_copy_2-2.jpg"}}" alt="Sign up, Free 200 Baht" /> </a>
</div>
<!-- start small banner 1 -->
<div class="homepage-bannerl1">
	<a href="http://www.petloft.com/th/help/#shipping"> <img src="{{media url="wysiwyg/petloft-free-shipping-free-cod-small-banner_copy-2.jpg"}}" alt="Free Shipping" /> </a>
</div>
<!-- end small banner 1 -->
<p>&nbsp;</p>
<!-- start small banner 2 -->
<div class="homepage-bannerl2">
	<a href="http://www.petloft.com/th/catalogsearch/result/?q=Royal+Canin"> <img src="{{media url="wysiwyg/petloft-royal-canin-small-banner-2.jpg"}}" alt="Royal Canin Pet Food" /> </a>
</div>
<!-- end small banner 2 -->
<p>&nbsp;</p>
HTML;


$block->setContent($slider);
$block->save();
/** End Petloft Thai */

/** VENBI EN */
/** @var Mage_Core_Model_Store $storeModel */
$storeModel = Mage::getModel('core/store')->load('ven', 'code');
$storeId = $storeModel->getId();

/** @var Mage_Cms_Model_Block $model */
$model = Mage::getModel('cms/block');

$block = $model->getCollection()->addStoreFilter($storeId)->addFieldToFilter('identifier', 'slider_1')->getFirstItem();

/** @var Mage_Cms_Model_Block $block */
$block = $model->load($block->getId());

$slider = <<<HTML
<div class="homepage-main" style="float: left; width: 610px; margin: 0px 30px;">
	<div id="container" style="width: 610px;">
		<div id="slideshow" style="margin: 0px 15px 25px 15px; height: 400px; width: 610px;">
			<ul id="slide-nav" style="top: 190px;">
				<li id="prev">
					<a class="control-s pre-s" href="#">Previous</a>
				</li>
				<li id="next" style="left: 500px;">
					<a class="control-s nxt-s" href="#">Next</a>
				</li>
			</ul>
			<ul id="home-slides">
				<li>
					<a style="width: 610px;" href="http://www.venbi.com/en/diapers.html?baby_brands=mamypoko"><img src="{{media url="wysiwyg/mamypoko-homepage-banner.jpg"}}" alt="3 Mamypoko free storybook" /></a>
					<div class="caption">Free Storybook for 3 packs of Mamypoko</div>
				</li>
				<li>
					<a style="width: 610px;" href="{{store url='mom-maternity.html'}}"><img src="{{media url="wysiwyg/mother_day-NOMMAE-homepage-banner.jpg"}}" alt="Coupon Code NOMMAE" /></a>
					<div class="caption">fill NOMMAE get more discount</div>
				</li>
				<li>
					<a style="width: 610px;" href="{{store url='toys/type/early-development-toys.html'}}"><img src="{{media url="wysiwyg/funtime-homepage-banner.jpg"}}" alt="Fun Time" /></a>
					<div class="caption">Toys for Children Development</div>
				</li>
				<li>
					<a style="width: 610px;" href="http://www.venbi.com/en/toys/type/cartoon-dvd.html"><img src="{{media url="wysiwyg/disney-DVD-homepage-banner.jpg"}}" alt="Introducing Cartoon DVD" /></a>
					<div class="caption">Cartoon DVD</div>
				</li>
				<li>
					<a style="width: 610px;" href="{{store url='diapers/popular-brands/baby-love.html'}}"><img src="{{media url="wysiwyg/Venbi/baby_love-homepage-banner.jpg"}}" alt="BabyLove freebies" /></a>
					<div class="caption">BabyLove Freebies Diapers</div>
				</li>
				<li>
					<a style="width: 610px;" href="{{store url='deals.html'}}"><img src="{{media url="wysiwyg/clearance-sale-venbi-homepage-banner.jpg"}}" alt="clearance venbi" /></a>
					<div class="caption">clearance venbi</div>
				</li>
				<li>
					<a style="width: 610px;" href="{{store url='gear.html'}}"><img src="{{media url="wysiwyg/ktc-venbi-promotion-homepage-banner-TH.jpg"}}" alt="KTC 0%" /></a>
					<div class="caption">KTC 0%</div>
				</li>
			</ul>
		</div>
	</div>
</div>
<div class="homepage-bannerl1" style="margin-bottom: 15px; display: block;">
	<a href="http://www.venbi.com/th/help/#shipping"> <img src="{{media url="wysiwyg/free-shipping-COD-homepage-banner-venbi.jpg"}}" alt="" /> </a>
</div>
<!-- start small banner 1 -->
<div class="homepage-bannerl1">
	<a href="http://www.venbi.com/th/customer/account/create/"> <img src="{{media url="wysiwyg/signup-homepage-banner-venbi.jpg"}}" alt="Sign up, Get Free 200 Baht" /> </a>
</div>
<!-- end small banner 1 -->
<p>&nbsp;</p>
<!-- start small banner 2 -->
<div class="homepage-bannerl2">
	<a href="http://www.venbi.com/th/diapers/popular-brands/mamy-poko.html"><img src="{{media url="wysiwyg/bestseller-homepage-banner-venbi.jpg"}}" alt="MamyPoko Diapers" /></a>
</div>
<!-- end small banner 2 -->
<p>&nbsp;</p>
HTML;


$block->setContent($slider);
$block->save();
/** End VENBI EN */

/** VENBI TH */
/** @var Mage_Core_Model_Store $storeModel */
$storeModel = Mage::getModel('core/store')->load('vth', 'code');
$storeId = $storeModel->getId();

/** @var Mage_Cms_Model_Block $model */
$model = Mage::getModel('cms/block');

$block = $model->getCollection()->addStoreFilter($storeId)->addFieldToFilter('identifier', 'slider_1')->getFirstItem();

/** @var Mage_Cms_Model_Block $block */
$block = $model->load($block->getId());

$slider = <<<HTML
<div class="homepage-main" style="float: left; width: 610px; margin: 0px 30px;">
	<div id="container" style="width: 610px;">
		<div id="slideshow" style="margin: 0px 15px 25px 15px; height: 400px; width: 610px;">
			<ul id="slide-nav" style="top: 190px;">
				<li id="prev">
					<a class="control-s pre-s" href="#">Previous</a>
				</li>
				<li id="next" style="left: 500px;">
					<a class="control-s nxt-s" href="#">Next</a>
				</li>
			</ul>
			<ul id="home-slides">
				<li>
					<a style="width: 610px;" href="http://www.venbi.com/th/diapers.html?baby_brands=mamypoko"><img src="{{media url="wysiwyg/mamypoko-homepage-banner.jpg"}}" alt="3 Mamypoko free storybook" /></a>
					<div class="caption">Free Storybook for 3 packs of Mamypoko</div>
				</li>
				<li>
					<a style="width: 610px;" href="{{store url='mom-maternity.html'}}"><img src="{{media url="wysiwyg/mother_day-NOMMAE-homepage-banner.jpg"}}" alt="Coupon Code NOMMAE" /></a>
					<div class="caption">fill NOMMAE get more discount</div>
				</li>
				<li>
					<a style="width: 610px;" href="{{store url='toys/type/early-development-toys.html'}}"><img src="{{media url="wysiwyg/funtime-homepage-banner.jpg"}}" alt="Fun Time" /></a>
					<div class="caption">Toys for Children Development</div>
				</li>
				<li>
					<a style="width: 610px;" href="http://www.venbi.com/th/toys/type/cartoon-dvd.html"><img src="{{media url="wysiwyg/disney-DVD-homepage-banner.jpg"}}" alt="Introducing Cartoon DVD" /></a>
					<div class="caption">Cartoon DVD</div>
				</li>
				<li>
					<a style="width: 610px;" href="{{store url='diapers/popular-brands/baby-love.html'}}"><img src="{{media url="wysiwyg/Venbi/baby_love-homepage-banner.jpg"}}" alt="BabyLove freebies" /></a>
					<div class="caption">BabyLove Freebies Diapers</div>
				</li>
				<li>
					<a style="width: 610px;" href="{{store url='deals.html'}}"><img src="{{media url="wysiwyg/clearance-sale-venbi-homepage-banner.jpg"}}" alt="clearance venbi" /></a>
					<div class="caption">clearance venbi</div>
				</li>
				<li>
					<a style="width: 610px;" href="{{store url='gear.html'}}"><img src="{{media url="wysiwyg/ktc-venbi-promotion-homepage-banner-TH.jpg"}}" alt="KTC 0%" /></a>
					<div class="caption">KTC 0%</div>
				</li>
			</ul>
		</div>
	</div>
</div>
<div class="homepage-bannerl1" style="margin-bottom: 15px; display: block;">
	<a href="http://www.venbi.com/th/help/#shipping"> <img src="{{media url="wysiwyg/free-shipping-COD-homepage-banner-venbi.jpg"}}" alt="" /> </a>
</div>
<!-- start small banner 1 -->
<div class="homepage-bannerl1">
	<a href="http://www.venbi.com/?popup=acommerce?popup=acommerce"> <img src="{{media url="wysiwyg/signup-homepage-banner-venbi.jpg"}}" alt="Sign up, Get Free 200 Baht" /> </a>
</div>
<!-- end small banner 1 -->
<p>&nbsp;</p>
<!-- start small banner 2 -->
<div class="homepage-bannerl2">
	<a href="http://www.venbi.com/th/diapers/popular-brands/mamy-poko.html"><img src="{{media url="wysiwyg/bestseller-homepage-banner-venbi.jpg"}}" alt="MamyPoko Diapers" /></a>
</div>
<!-- end small banner 2 -->
<p>&nbsp;</p>
HTML;


$block->setContent($slider);
$block->save();
/** End VENBI TH */

/** Sanoga ENG */

/** @var Mage_Core_Model_Store $storeModel */
$storeModel = Mage::getModel('core/store')->load('sen', 'code');
$storeId = $storeModel->getId();

/** @var Mage_Cms_Model_Block $model */
$model = Mage::getModel('cms/block');

$block = $model->getCollection()->addStoreFilter($storeId)->addFieldToFilter('identifier', 'slider_1')->getFirstItem();

/** @var Mage_Cms_Model_Block $block */
$block = $model->load($block->getId());

$slider = <<<HTML
<div class="homepage-main">
	<div id="container">
		<div id="slideshow">
			<ul id="slide-nav">
				<li id="prev">
					<a class="control-s pre-s" href="#">Previous</a>
				</li>
				<li id="next">
					<a class="control-s nxt-s" href="#">Next</a>
				</li>
			</ul>
			<ul id="home-slides">
				<li>
					<a href="http://www.sanoga.com/en/workout-fitness/protein/whey-proteins.html"><img src="{{media url="wysiwyg/Sanoga-whey150-homepage-banner.jpg"}}" alt="Whey Protein code 150" /></a>
					<div class="caption">Whey Protein discount 150 bht when purchase 1500 bht</div>
				</li>
				<li>
					<a href="http://www.sanoga.com/en/health-care/medical-equipment/health-monitors/maxxlife-major-ii-strip-2.html"><img src="{{media url="wysiwyg/health-sanoga-homepage-banner.jpg"}}" alt="MaxxLife for health" /></a>
					<div class="caption">Health Monitor</div>
				</li>
				<li>
					<a href="http://www.sanoga.com/th/beauty/skin-supplements/whitening.html"><img src="{{media url="wysiwyg/sanoga-skin-care-homepage-banner.jpg"}}" alt="Skin Care Supplement Sale" /></a>
					<div class="caption">Skin Care Supplement up to 46%</div>
				</li>
				<li>
					<a href="http://www.sanoga.com/th/catalog/category/view/s/a-a-a-a-a-a-sa-a-a/id/974/"><img src="{{media url="wysiwyg/clearance-sale-sanoga-homepage-banner.jpg"}}" alt="clearance sale sanoga" /></a>
					<div class="caption">CLEARANCE SALE SANOGA</div>
				</li>
				<li>
					<a><img src="{{media url="wysiwyg/ktc-sanoga-promotion-homepage-banner-TH.jpg"}}" alt="" /></a>
					<div class="caption">KTC 0%</div>
				</li>
			</ul>
		</div>
	</div>
</div>
<div class="homepage-bannerl1">
	<a href="http://www.sanoga.com/th/help/#shipping"><img src="{{media url="wysiwyg/sanoga-free-delivery-free-COD-small-banner.jpg"}}" alt="Free Shipping" /> </a>
</div>
<!-- start small banner 1 -->
<div class="homepage-bannerl1">
	<a href="https://www.sanoga.com/th/customer/account/create/"> <img src="{{media url="wysiwyg/sanoga-sign-up-get-200-small-banner.jpg"}}" alt="Sign up, Get Free 200 Baht" /></a>
</div>
<!-- end small banner 1 -->
<p>&nbsp;</p>
<!-- start small banner 2 -->
<div class="homepage-bannerl2">
	<a href="http://amado.popsho.ps/sanoga/"> <img src="{{media url="wysiwyg/Sanoga/Amado_banner.png"}}" alt="amado" /></a>
</div>
<!-- end small banner 2 -->
<p>&nbsp;</p>
HTML;


$block->setContent($slider);
$block->save();

/** END Sanoga ENG */

/** SANOGA THAI */
/** @var Mage_Core_Model_Store $storeModel */
$storeModel = Mage::getModel('core/store')->load('sth', 'code');
$storeId = $storeModel->getId();

/** @var Mage_Cms_Model_Block $model */
$model = Mage::getModel('cms/block');

$block = $model->getCollection()->addStoreFilter($storeId)->addFieldToFilter('identifier', 'slider_1')->getFirstItem();

/** @var Mage_Cms_Model_Block $block */
$block = $model->load($block->getId());

$slider = <<<HTML
<div class="homepage-main">
	<div id="container">
		<div id="slideshow">
			<ul id="slide-nav">
				<li id="prev">
					<a class="control-s pre-s" href="#">Previous</a>
				</li>
				<li id="next">
					<a class="control-s nxt-s" href="#">Next</a>
				</li>
			</ul>
			<ul id="home-slides">
				<li>
					<a href="http://www.sanoga.com/th/workout-fitness/protein/whey-proteins.html"><img src="{{media url="wysiwyg/Sanoga-whey150-homepage-banner.jpg"}}" alt="Whey Protein code 150" /></a>
					<div class="caption">Whey Protein discount 150 bht when purchase 1500 bht</div>
				</li>
				<li>
					<a href="http://www.sanoga.com/th/health-care/medical-equipment/health-monitors/maxxlife-major-ii-strip-2.html"><img src="{{media url="wysiwyg/health-sanoga-homepage-banner.jpg"}}" alt="MaxxLife for health" /></a>
					<div class="caption">Health Monitor</div>
				</li>
				<li>
					<a href="http://www.sanoga.com/th/catalogsearch/result/?q=eazy2diet"><img src="{{media url="wysiwyg/sanoga-eazy2diet-homepage-banner.jpg"}}" alt="eazy2diet sales" /></a>
					<div class="caption">Eazy2Diet up to 24%</div>
				</li>
				<li>
					<a href="http://www.sanoga.com/th/beauty/skin-supplements/whitening.html"><img src="{{media url="wysiwyg/sanoga-skin-care-homepage-banner.jpg"}}" alt="Skin Care Supplement Sale" /></a>
					<div class="caption">Skin Care Supplement up to 46%</div>
				</li>
				<li>
					<a href="http://www.sanoga.com/th/beauty.html"><img src="{{media url="wysiwyg/sanoga-friday-happy10-homepage-banner2.jpg"}}" alt="" /></a>
					<div class="caption">Coupon 10% off</div>
				</li>
				<li>
					<a href="http://www.sanoga.com/th/catalog/category/view/s/a-a-a-a-a-a-sa-a-a/id/974/"><img src="{{media url="wysiwyg/clearance-sale-sanoga-homepage-banner.jpg"}}" alt="clearance sale sanoga" /></a>
					<div class="caption">CLEARANCE SALE SANOGA</div>
				</li>
				<li>
					<a><img src="{{media url="wysiwyg/ktc-sanoga-promotion-homepage-banner-TH.jpg"}}" alt="" /></a>
					<div class="caption">KTC 0%</div>
				</li>
			</ul>
		</div>
	</div>
</div>
<div class="homepage-bannerl1">
	<a href="http://www.sanoga.com/th/help/#shipping"><img src="{{media url="wysiwyg/sanoga-free-delivery-free-COD-small-banner.jpg"}}" alt="Free Shipping" /> </a>
</div>
<!-- start small banner 1 -->
<div class="homepage-bannerl1">
	<a href="http://www.sanoga.com/?popup=acommerce?popup=acommerce"> <img src="{{media url="wysiwyg/sanoga-sign-up-get-200-small-banner.jpg"}}" alt="Sign up, Get Free 200 Baht" /></a>
</div>
<!-- end small banner 1 -->
<p>&nbsp;</p>
<!-- start small banner 2 -->
<div class="homepage-bannerl2">
	<a href="http://amado.popsho.ps/sanoga/"><img src="{{media url="wysiwyg/Sanoga/Amado_banner.png"}}" alt="amado" /></a>
</div>
<!-- end small banner 2 -->
<p>&nbsp;</p>
HTML;


$block->setContent($slider);
$block->save();
/** END SANOGA THAI */

/** MOXY ENG */
/** @var Mage_Core_Model_Store $storeModel */
$storeModel = Mage::getModel('core/store')->load('moxyen', 'code');
$storeId = $storeModel->getId();

/** @var Mage_Cms_Model_Block $model */
$model = Mage::getModel('cms/block');

$block = $model->getCollection()->addStoreFilter($storeId)->addFieldToFilter('identifier', 'slider_1')->getFirstItem();

/** @var Mage_Cms_Model_Block $block */
$block = $model->load($block->getId());

$slider = <<<HTML
<div class="homepage-main">
	<div id="container">
		<div id="slideshow">
			<ul id="slide-nav">
				<li id="prev">
					<a class="control-s pre-s" href="#">Previous</a>
				</li>
				<li id="next">
					<a class="control-s nxt-s" href="#">Next</a>
				</li>
			</ul>
			<ul id="home-slides">
				<li>
					<a href="http://www.sanoga.com/en/workout-fitness/protein/whey-proteins.html"><img src="{{media url="wysiwyg/moxy-whey150-homepage-banner.jpg"}}" alt="Whey Protein code 150" /></a>
					<div class="caption">Whey Protein discount 150 bht when purchase 1500 bht</div>
				</li>
				<li>
					<a href="http://www.sanoga.com/en/health-care/medical-equipment/health-monitors/maxxlife-major-ii-strip-2.html"><img src="{{media url="wysiwyg/health-moxy-homepage-banner.jpg"}}" alt="MaxxLife for health" /></a>
					<div class="caption">Health Monitor</div>
				</li>
				<li>
					<a href="http://www.sanoga.com/th/beauty/skin-supplements/whitening.html"><img src="{{media url="wysiwyg/moxy-skin-care-homepage-banner.jpg"}}" alt="Skin Care Supplement Sale" /></a>
					<div class="caption">Skin Care Supplement up to 46%</div>
				</li>
				<li>
					<a href="http://www.sanoga.com/th/catalog/category/view/s/a-a-a-a-a-a-sa-a-a/id/974/"><img src="{{media url="wysiwyg/clearance-sale-moxy-homepage-banner.jpg"}}" alt="clearance sale sanoga" /></a>
					<div class="caption">CLEARANCE SALE SANOGA</div>
				</li>
				<li>
					<a><img src="{{media url="wysiwyg/ktc-moxy-promotion-homepage-banner-TH.jpg"}}" alt="" /></a>
					<div class="caption">KTC 0%</div>
				</li>
			</ul>
		</div>
	</div>
</div>
<div class="homepage-bannerl1">
	<a href="http://www.moxyst.com/th/help/#shipping"><img src="{{media url="wysiwyg/moxy-free-delivery-free-COD-small-banner.jpg"}}" alt="Free Shipping" /> </a>
</div>
<!-- start small banner 1 -->
<div class="homepage-bannerl1">
	<a href="https://www.moxyst.com/th/customer/account/create/"> <img src="{{media url="wysiwyg/moxy-sign-up-get-200-small-banner.jpg"}}" alt="Sign up, Get Free 200 Baht" /></a>
</div>
<!-- end small banner 1 -->
<p>&nbsp;</p>
<!-- start small banner 2 -->
<div class="homepage-bannerl2">
	<a href="http://amado.popsho.ps/moxyst/"> <img src="{{media url="wysiwyg/Moxy/Amado_banner.png"}}" alt="amado" /></a>
</div>
<!-- end small banner 2 -->
<p>&nbsp;</p>
HTML;


$block->setContent($slider);
$block->save();
/**END MOXY ENG*/

/** MOXY THAI */
/** @var Mage_Core_Model_Store $storeModel */
$storeModel = Mage::getModel('core/store')->load('moxyth', 'code');
$storeId = $storeModel->getId();

/** @var Mage_Cms_Model_Block $model */
$model = Mage::getModel('cms/block');

$block = $model->getCollection()->addStoreFilter($storeId)->addFieldToFilter('identifier', 'slider_1')->getFirstItem();

/** @var Mage_Cms_Model_Block $block */
$block = $model->load($block->getId());

$slider = <<<HTML
<div class="homepage-main">
	<div id="container">
		<div id="slideshow">
			<ul id="slide-nav">
				<li id="prev">
					<a class="control-s pre-s" href="#">Previous</a>
				</li>
				<li id="next">
					<a class="control-s nxt-s" href="#">Next</a>
				</li>
			</ul>
			<ul id="home-slides">
				<li>
					<a href="http://www.sanoga.com/th/workout-fitness/protein/whey-proteins.html"><img src="{{media url="wysiwyg/moxy-whey150-homepage-banner.jpg"}}" alt="Whey Protein code 150" /></a>
					<div class="caption">Whey Protein discount 150 bht when purchase 1500 bht</div>
				</li>
				<li>
					<a href="http://www.sanoga.com/th/health-care/medical-equipment/health-monitors/maxxlife-major-ii-strip-2.html"><img src="{{media url="wysiwyg/health-moxy-homepage-banner.jpg"}}" alt="MaxxLife for health" /></a>
					<div class="caption">Health Monitor</div>
				</li>
				<li>
					<a href="http://www.sanoga.com/th/catalogsearch/result/?q=eazy2diet"><img src="{{media url="wysiwyg/moxy-eazy2diet-homepage-banner.jpg"}}" alt="eazy2diet sales" /></a>
					<div class="caption">Eazy2Diet up to 24%</div>
				</li>
				<li>
					<a href="http://www.sanoga.com/th/beauty/skin-supplements/whitening.html"><img src="{{media url="wysiwyg/moxy-skin-care-homepage-banner.jpg"}}" alt="Skin Care Supplement Sale" /></a>
					<div class="caption">Skin Care Supplement up to 46%</div>
				</li>
				<li>
					<a href="http://www.sanoga.com/th/beauty.html"><img src="{{media url="wysiwyg/moxy-friday-happy10-homepage-banner2.jpg"}}" alt="" /></a>
					<div class="caption">Coupon 10% off</div>
				</li>
				<li>
					<a href="http://www.sanoga.com/th/catalog/category/view/s/a-a-a-a-a-a-sa-a-a/id/974/"><img src="{{media url="wysiwyg/clearance-sale-moxy-homepage-banner.jpg"}}" alt="clearance sale sanoga" /></a>
					<div class="caption">CLEARANCE SALE SANOGA</div>
				</li>
				<li>
					<a><img src="{{media url="wysiwyg/ktc-moxy-promotion-homepage-banner-TH.jpg"}}" alt="" /></a>
					<div class="caption">KTC 0%</div>
				</li>
			</ul>
		</div>
	</div>
</div>
<div class="homepage-bannerl1">
	<a href="http://www.moxyst.com/th/help/#shipping"><img src="{{media url="wysiwyg/moxy-free-delivery-free-COD-small-banner.jpg"}}" alt="Free Shipping" /> </a>
</div>
<!-- start small banner 1 -->
<div class="homepage-bannerl1">
	<a href="http://www.moxyst.com/?popup=acommerce?popup=acommerce"> <img src="{{media url="wysiwyg/moxy-sign-up-get-200-small-banner.jpg"}}" alt="Sign up, Get Free 200 Baht" /></a>
</div>
<!-- end small banner 1 -->
<p>&nbsp;</p>
<!-- start small banner 2 -->
<div class="homepage-bannerl2">
	<a href="http://amado.popsho.ps/moxyst/"><img src="{{media url="wysiwyg/Moxy/Amado_banner.png"}}" alt="amado" /></a>
</div>
<!-- end small banner 2 -->
<p>&nbsp;</p>
HTML;


$block->setData('content', $slider);
$block->save();
/**END MOXY THAI*/

$install->endSetup();