<?php
try {
    $installer = $this;
    $installer->startSetup();

    // Force the store to be admin
    Mage::app()->setUpdateMode(false);
    Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
    if (!Mage::registry('isSecureArea'))
        Mage::register('isSecureArea', 1);

    // get store view ids
    $petloftEn = Mage::app()->getStore('en')->getId();
    $petloftMobi = Mage::app()->getStore('pm')->getId();
    $petloftTh = Mage::app()->getStore('th')->getId();
    //
    $venbiEn = Mage::app()->getStore('ven')->getId();
    $venbiMobi = Mage::app()->getStore('vm')->getId();
    $venbiTh = Mage::app()->getStore('vth')->getId();
    //
    $sanogaEn = Mage::app()->getStore('sen')->getId();
    $sanogaMobi = Mage::app()->getStore('sm')->getId();
    $sanogaTh = Mage::app()->getStore('sth')->getId();
    //
    $lafemaEn = Mage::app()->getStore('len')->getId();
    $lafemaMobi = Mage::app()->getStore('lm')->getId();
    $lafemaTh = Mage::app()->getStore('lth')->getId();

    //==========================================================================
    // Petloft Mobile Menu Footer Information Links
    //==========================================================================
    $petloftContent = <<<EOD
    <ul class="level0">
        <li class="level1"><a title="Return Policy" href="{{store url}}return-policy/"> <span>Return Policy</span> </a></li>
        <li class="level1"><a title="About Us" href="{{store url}}about-us/"> <span>About Us</span> </a></li>
        <li class="level1"><a title="Terms &amp; Conditions" href="{{store url}}terms-and-conditions/"> <span>Terms &amp; Conditions</span> </a></li>
        <li class="level1"><a title="Privacy Policy" href="{{store url}}privacy-policy/"> <span>Privacy Policy</span> </a></li>
    </ul>
EOD;

    // check exists
    $blockPetloft = Mage::getModel('cms/block')->getCollection()
        ->addStoreFilter(array($petloftEn, $petloftMobi, $petloftTh), $withAdmin = false)
        ->addFieldToFilter('identifier', "menu_footer_info_links")
        ->getFirstItem();
    if ($blockPetloft->getId()) {
        // save new content
        $blockPetloft->setContent($petloftContent);
        $blockPetloft->setStores(array($petloftEn, $petloftMobi, $petloftTh));
        $blockPetloft->save();
    }
    //==========================================================================
    //==========================================================================
    //==========================================================================


    //==========================================================================
    // Venbi Mobile Menu Footer Information Links
    //==========================================================================
    $venbiContent = <<<EOD
    <ul class="level0">
        <li class="level1"><a title="Return Policy" href="{{store url}}return-policy/"> <span>Return Policy</span> </a></li>
        <li class="level1"><a title="About Us" href="{{store url}}about-us/"> <span>About Us</span> </a></li>
        <li class="level1"><a title="Terms &amp; Conditions" href="{{store url}}terms-and-conditions/"> <span>Terms &amp; Conditions</span> </a></li>
        <li class="level1"><a title="Privacy Policy" href="{{store url}}privacy-policy/"> <span>Privacy Policy</span> </a></li>
    </ul>
EOD;

    $venbiData = array(
        'title' => "Venbi Mobile Menu Footer Information Links",
        'identifier' => "menu_footer_info_links",
        'stores' => array($venbiEn, $venbiMobi, $venbiTh),
        'is_active' => 1,
        'content' => $venbiContent,
    );

    // check exists
    $blockVenbi = Mage::getModel('cms/block')->getCollection()
        ->addStoreFilter(array($venbiEn, $venbiMobi, $venbiTh), $withAdmin = false)
        ->addFieldToFilter('identifier', "menu_footer_info_links")
        ->getFirstItem();
    if ($blockVenbi->getId() == null) {
        // create block
        $blockVenbi = Mage::getModel('cms/block');
        $blockVenbi->setData($venbiData);
        $blockVenbi->save();
    }
    //==========================================================================
    //==========================================================================
    //==========================================================================


    //==========================================================================
    // Sanoga Mobile Menu Footer Information Links
    //==========================================================================
    $sanogaContent = <<<EOD
    <ul class="level0">
        <li class="level1"><a title="Return Policy" href="{{store url}}return-policy/"> <span>Return Policy</span> </a></li>
        <li class="level1"><a title="About Us" href="{{store url}}about-us/"> <span>About Us</span> </a></li>
        <li class="level1"><a title="Terms &amp; Conditions" href="{{store url}}terms-and-conditions/"> <span>Terms &amp; Conditions</span> </a></li>
        <li class="level1"><a title="Privacy Policy" href="{{store url}}privacy-policy/"> <span>Privacy Policy</span> </a></li>
    </ul>
EOD;

    $sanogaData = array(
        'title' => "Sanoga Mobile Menu Footer Information Links",
        'identifier' => "menu_footer_info_links",
        'stores' => array($sanogaEn, $sanogaMobi, $sanogaTh),
        'is_active' => 1,
        'content' => $sanogaContent,
    );

    // check exists
    $blockSanoga = Mage::getModel('cms/block')->getCollection()
        ->addStoreFilter(array($sanogaEn, $sanogaMobi, $sanogaTh), $withAdmin = false)
        ->addFieldToFilter('identifier', "menu_footer_info_links")
        ->getFirstItem();
    if ($blockSanoga->getId() == null) {
        // create block
        $blockSanoga = Mage::getModel('cms/block');
        $blockSanoga->setData($sanogaData);
        $blockSanoga->save();
    }
    //==========================================================================
    //==========================================================================
    //==========================================================================


    //==========================================================================
    // Lafema Mobile Menu Footer Information Links
    //==========================================================================
    $lafemaContent = <<<EOD
    <ul class="level0">
        <li class="level1"><a title="Return Policy" href="{{store url}}return-policy/"> <span>Return Policy</span> </a></li>
        <li class="level1"><a title="About Us" href="{{store url}}about-us/"> <span>About Us</span> </a></li>
        <li class="level1"><a title="Terms &amp; Conditions" href="{{store url}}terms-and-conditions/"> <span>Terms &amp; Conditions</span> </a></li>
        <li class="level1"><a title="Privacy Policy" href="{{store url}}privacy-policy/"> <span>Privacy Policy</span> </a></li>
    </ul>
EOD;

    $lafemaData = array(
        'title' => "Lafema Mobile Menu Footer Information Links",
        'identifier' => "menu_footer_info_links",
        'stores' => array($lafemaEn, $lafemaMobi, $lafemaTh),
        'is_active' => 1,
        'content' => $lafemaContent,
    );

    // check exists
    $blockLafema = Mage::getModel('cms/block')->getCollection()
        ->addStoreFilter(array($lafemaEn, $lafemaMobi, $lafemaTh), $withAdmin = false)
        ->addFieldToFilter('identifier', "menu_footer_info_links")
        ->getFirstItem();
    if ($blockLafema->getId() == null) {
        // create block
        $blockLafema = Mage::getModel('cms/block');
        $blockLafema->setData($lafemaData);
        $blockLafema->save();
    }
    //==========================================================================
    //==========================================================================
    //==========================================================================

    $installer->endSetup();
} catch (Excpetion $e) {
    Mage::logException($e);
    Mage::log("ERROR IN SETUP " . $e->getMessage());
}