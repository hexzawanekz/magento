<?php

// Force the store to be admin
Mage::app()->setUpdateMode(false);
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
if (!Mage::registry('isSecureArea'))
    Mage::register('isSecureArea', 1);

// get store view ids
$moxyEn = Mage::app()->getStore('moxyen')->getId();
$moxyTh = Mage::app()->getStore('moxyth')->getId();

// save config
$configurator = new Mage_Core_Model_Config();
$configurator->saveConfig('design/head/default_title', 'Moxyst.com', 'stores', $moxyEn);
$configurator->saveConfig('design/head/default_title', 'Moxyst.com', 'stores', $moxyTh);