<?php
try {
    $installer = $this;
    $installer->startSetup();

    // Force the store to be admin
    Mage::app()->setUpdateMode(false);
    Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
    if (!Mage::registry('isSecureArea'))
        Mage::register('isSecureArea', 1);

    //==========================================================================
    // Mobile Footer block
    //==========================================================================
    $blockTitle = "Mobile Footer";
    $blockIdentifier = "mobile_footer";
    $blockStores = array('0');
    $blockIsActive = 1;
    $blockContent = <<<EOD
<footer>
<div class="logo-footer-mobile"><img style="width: 133px;" src="{{media url="wysiwyg/WHATSNEWlogo_white-01.png"}}" alt="" /></div>
<div>&copy; 2014, Whatsnew Co.,Ltd. All Rights Reserved</div>
</footer>
<div class="banks-footer-mobile"><img style="width: 200px;" src="{{media url="wysiwyg/logo-footer-mobile.png"}}" alt="" /></div>
EOD;
    $block = Mage::getModel('cms/block')->getCollection()
        ->addStoreFilter(array('0'), $withAdmin = false)
        ->addFieldToFilter('identifier', $blockIdentifier)
        ->getFirstItem();
    if ($block->getId() == 0) {
        $block = Mage::getModel('cms/block');
    } else { // if exists then delete
        $block->delete();
        $block = Mage::getModel('cms/block');
    }
    $block->setTitle($blockTitle);
    $block->setIdentifier($blockIdentifier);
    $block->setStores($blockStores);
    $block->setIsActive($blockIsActive);
    $block->setContent($blockContent);
    $block->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================

    $installer->endSetup();
} catch (Excpetion $e) {
    Mage::logException($e);
    Mage::log("ERROR IN SETUP " . $e->getMessage());
}
	 