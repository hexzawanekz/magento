<?php

$moxyen = Mage::getModel('core/store')->load('moxyen', 'code')->getId();
$moxyth = Mage::getModel('core/store')->load('moxyth', 'code')->getId();

//==========================================================================
// Auction Welcome EN
//==========================================================================
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($moxyen, $withAdmin = false)
    ->addFieldToFilter('identifier', 'auction_welcome')
    ->getFirstItem();

if ($block->getId()) $block->delete();// if exists then delete
$content = <<<EOD
<div id="auction_slider" class="homepage-main home-banner">
    <div id="container">
        <div id="slideshow" style="margin: 0 !important;">
        <ul id="slide-nav" style="display: none; visibility: hidden;">
            <li id="prev"><a class="control-s pre-s" href="#">Previous</a></li>
            <li id="next" style="left: 500px;"><a class="control-s nxt-s" href="#">Next</a></li>
        </ul>

        <ul id="home-slides">
            <li><img src="{{media url="wysiwyg/Auction/Auction_homepage_.jpg"}}" alt="Auction" /a>
            <div class="caption">Auction</div>
            </li>
            <li><img src="{{media url="wysiwyg/Auction/QSCBC_homepagebanners_all.jpg"}}" alt="Auction" />
            <div class="caption">Auction</div>
            </li>
            <li><img src="{{media url="wysiwyg/Auction/QSCBC_homepagebanners_pinkpark.jpg"}}" alt="Auction" />
            <div class="caption">Auction</div>
            </li>
            <li><img src="{{media url="wysiwyg/Auction/QSCBC_homepagebanners_queen.jpg"}}" alt="Auction" />
            <div class="caption">Auction</div>
            </li>
            <li><img src="{{media url="wysiwyg/Auction/QSCBC_homepagebanners_queensirikit.jpg"}}" alt="Auction" />
        <div class="caption">Auction</div>
        </li>
        </ul>
        </div>
    </div>
</div>
EOD;
/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block');
$block->setTitle('Auction Welcome EN');
$block->setIdentifier('auction_welcome');
$block->setStores($moxyen);
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================

//==========================================================================
// Auction Welcome TH
//==========================================================================
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($moxyth, $withAdmin = false)
    ->addFieldToFilter('identifier', 'auction_welcome')
    ->getFirstItem();

if ($block->getId()) $block->delete();// if exists then delete
$content = <<<EOD
<div id="auction_slider" class="homepage-main home-banner">
    <div id="container">
        <div id="slideshow" style="margin: 0 !important;">
        <ul id="slide-nav" style="display: none; visibility: hidden;">
            <li id="prev"><a class="control-s pre-s" href="#">Previous</a></li>
            <li id="next" style="left: 500px;"><a class="control-s nxt-s" href="#">Next</a></li>
        </ul>

        <ul id="home-slides">
            <li><img src="{{media url="wysiwyg/Auction/Auction_homepage_.jpg"}}" alt="Auction" /a>
            <div class="caption">Auction</div>
            </li>
            <li><img src="{{media url="wysiwyg/Auction/QSCBC_homepagebanners_all.jpg"}}" alt="Auction" />
            <div class="caption">Auction</div>
            </li>
            <li><img src="{{media url="wysiwyg/Auction/QSCBC_homepagebanners_pinkpark.jpg"}}" alt="Auction" />
            <div class="caption">Auction</div>
            </li>
            <li><img src="{{media url="wysiwyg/Auction/QSCBC_homepagebanners_queen.jpg"}}" alt="Auction" />
            <div class="caption">Auction</div>
            </li>
            <li><img src="{{media url="wysiwyg/Auction/QSCBC_homepagebanners_queensirikit.jpg"}}" alt="Auction" />
        <div class="caption">Auction</div>
        </li>
        </ul>
        </div>
    </div>
</div>
EOD;
/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block');
$block->setTitle('Auction Welcome TH');
$block->setIdentifier('auction_welcome');
$block->setStores($moxyth);
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================