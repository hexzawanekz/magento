<?php

class SM_Sales_Helper_Data extends Mage_Sales_Helper_Data
{
    var $_saveCart = true;

    /*check exist child of grouped products with option in cart and return false if exist*/
    public function getOptionProductCart($proChildId = null, $GroupedParentSku = null)
    {
        $items = Mage::getSingleton('checkout/session')->getQuote()->getAllItems();
        $return = true;
        if(count($items) > 0) {
            foreach ($items as $item) {
                $proCartChildId = $item->getProductId();
                //if child exist in cart
//                if($proChildId == $proCartChildId) {
                    //check option
                    foreach ($item->getOptions() as $option) {
                        if (strtolower($option->getCode()) == 'additional_options') {
                            $unserialized = unserialize($option->getValue());
                            $labelOpt = strtolower($unserialized['options']['label']);
                            $valueOpt = $unserialized['options']['value'];
                            if($labelOpt == 'groupedsku' && $valueOpt == $GroupedParentSku) {
//                                $oldCartQty = $item->getQty();
//                                $item->setQty($qty);
                                $return = false;
                            }
                            break;
                        }
                    }
//                    break;
//                }
            }
        }
        return $return;
    }

    /*update qty child grouped products*/
    public function setQtyGroupedProductCart($GroupedParentSku = null, $qtyAdd = null) {
        $items = Mage::getSingleton('checkout/session')->getQuote();
        $return = array();
        foreach ($items->getAllItems() as $item) {
            foreach ($item->getOptions() as $option) {
                if (strtolower($option->getCode()) == 'additional_options' && $item->getId() != null) {
                    $unserialized = unserialize($option->getValue());
                    $labelOpt = strtolower($unserialized['options']['label']);
                    $valueOpt = $unserialized['options']['value'];
                    if($labelOpt == 'groupedsku' && $valueOpt == $GroupedParentSku) {
                        $oldQty = $item->getQty();
                        $item->setQty($qtyAdd + $oldQty);
                        $item->save();
                        $return[$item->getId()] = $qtyAdd .'+'. $oldQty;
                    }
                    break;
                }
            }
        }
        $items->setTotalsCollectedFlag(false)->collectTotals()->save();
        return $return;
    }

    public function accessGroupedSaveCart($value = null) {
        if($value == 2) {
            $this->_saveCart = false;
        }
        if($value == 1) {
            $this->_saveCart = true;
        }
        return $this->_saveCart;
    }

    /*check if any item of group product is outstock then this group product is outstock*/
    public function checkInstockGroupCustom($groupId = null) {
        $instock = true;
        if($groupId) {
            $groupPro   = Mage::getModel('catalog/product')->load($groupId);
            if($groupPro->isSaleable()) {
                $_associatedProducts = $groupPro->getTypeInstance(true)->getAssociatedProducts($groupPro);
                foreach ($_associatedProducts as $item) {
                    if (!$item->isSaleable()) {
                        $instock = false;
                        break;
                    }
                }
            } else {
                $instock = false;
            }

        }
        return $instock;
    }
    /*end check if any item of group product is outstock then this group product is outstock*/

}
	 