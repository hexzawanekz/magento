<?php
class SM_ChangeInfoByCsv_Model_ChangeInfo extends Mage_Core_Model_Abstract {

    //Resource connection
    /** @var $_resource Mage_Core_Model_Resource */
    protected $_resource = null;
    /** @var $_readConnection Varien_Db_Adapter_Pdo_Mysql */
    protected $_readConnection = null;
    /** @var $_writeConnection Varien_Db_Adapter_Pdo_Mysql */
    protected $_writeConnection = null;

    protected $_productTable = '';
    protected $_eavAttributeTable = '';

    protected $_filePath        = '';
    protected $_errors          = 0;
    protected $_errorsMessage   = '';
    protected $_validAttribute  = null;

    protected function _initConnection(){
        $this->_resource        = Mage::getSingleton('core/resource');
        $this->_readConnection  = $this->_resource->getConnection('core_read');
        $this->_writeConnection = $this->_resource->getConnection('core_write');

        $this->_productTable    = $this->_resource->getTableName('catalog_product_entity');
        $this->_eavAttributeTable = $this->_resource->getTableName('eav_attribute');
    }

    protected function _getValidFieldList(){
        //Update info for Product
        $productEntityTypeId = 10;

        $sqlQuery = "SELECT attribute_id, attribute_code, backend_type FROM `{$this->_eavAttributeTable}` WHERE `entity_type_id` = ".$productEntityTypeId;
        $result = $this->_readConnection->fetchAll($sqlQuery);

        //$result[] = array('attribute_code' => 'entity_id', 'backend_type' => 'static');

        return $result;
    }

    protected function _checkValidCsvFile($headers){
        $result = null;
        $tmpHeader = $headers;
        $validFieldList = $this->_getValidFieldList();

        foreach($tmpHeader as $rawHeader) {
            foreach($validFieldList as $_validItem) {
                if(trim($rawHeader) == trim($_validItem['attribute_code'])) {
                    $result[] = $_validItem;
                } elseif ($rawHeader === 'category_id'){
                    $result[] = array(
                        'attribute_id'  => null,
                        'attribute_code'=> 'category_id',
                        'backend_type'  => null
                    );
                    break;
                }
            }
        }

        if(count($result) != count($tmpHeader)) {
            return false;
        }

        return $result;
    }

    public function getEntityIdBySku($sku){
        return Mage::getModel('catalog/product')->getIdBySku($sku);
    }

    /**
     * @param string $attribute_code
     * @param array $pattern
     * @return mixed
     */
    protected function _getHeaderInfo($attribute_code, $pattern = null){
        if($pattern != null) {
            $productEntityTypeId = 10;

            $sqlQuery = "SELECT attribute_id, attribute_code, backend_type FROM `{$this->_eavAttributeTable}` WHERE `attribute_code` = '{$attribute_code}' AND `entity_type_id` = ".$productEntityTypeId;
            $result = $this->_readConnection->fetchRow($sqlQuery);

            return $result;
        } else {
            foreach($pattern as $_item) {
                if($_item['attribute_code'] === $attribute_code) {
                    return $_item;
                }
            }
        }
    }

    public function changeInfoData() {
        $start = microtime(true);
        $this->_initConnection();

        $csvHandle = new Varien_File_Csv();
        $csvParser = new SM_ChangeInfoByCsv_Model_CsvParser($this->getFilePath());

        $csvContent = $csvParser->getArrayData();

        $headerArray = $csvParser->getCsvHeader();
        $headerArray = $this->_checkValidCsvFile($headerArray);
        $configArr = array();

        if($headerArray === false) {
            Mage::getSingleton('adminhtml/session')->addError('Invalid CSV File');
            Mage::getSingleton('core/session')->addError('Invalid CSV File');
        } else {

            try {
                $headerCount = count($headerArray);
                $csvContentCount = count($csvContent);

                for($contentIndex = 0; $contentIndex < $csvContentCount; $contentIndex++){
                    $entityId = $this->getEntityIdBySku($csvContent[$contentIndex]['sku']);

                    for($headerIndex = 1; $headerIndex < $headerCount; $headerIndex++){
                        if(trim($csvContent[$contentIndex][$headerArray[$headerIndex]['attribute_code']]) !== '') {
                            //Formating datetime fields//
                            if($headerArray[$headerIndex]['backend_type'] == 'datetime'){
                                $string = $csvContent[$contentIndex][$headerArray[$headerIndex]['attribute_code']];

                                $time = DateTime::createFromFormat('d/m/Y H:i:s', $string.' 00:00:00');

                                $csvContent[$contentIndex][$headerArray[$headerIndex]['attribute_code']] = date('Y-m-d H:i:s', $time->getTimestamp());

                            } else if($headerArray[$headerIndex]['backend_type'] == 'int' || $headerArray[$headerIndex]['backend_type'] == 'decimal' || $headerArray[$headerIndex]['backend_type'] == 'group_price' || $headerArray[$headerIndex]['backend_type'] == 'tier_price'){
                                $csvContent[$contentIndex][$headerArray[$headerIndex]['attribute_code']] = (int)$csvContent[$contentIndex][$headerArray[$headerIndex]['attribute_code']];
                            }

                            if ($headerArray[$headerIndex]['backend_type'] == 'static') {
                                $sqlQuery = "UPDATE `{$this->_productTable}`
                                                SET `{$headerArray[$headerIndex]['attribute_code']}` = '{$csvContent[$contentIndex][$headerArray[$headerIndex]['attribute_code']]}'
                                                WHERE `entity_id` = '{$entityId}'";
                                $this->_writeConnection->query($sqlQuery);
                            } elseif($headerArray[$headerIndex]['attribute_code'] === 'category_id'){

                                $categoryIds = explode(':', $csvContent[$contentIndex][$headerArray[$headerIndex]['attribute_code']]);

                                foreach($categoryIds as $_categoryId){
                                    $queryCheckingCategoryId = "SELECT count(entity_id) as `num_rows` FROM `{$this->_resource->getTableName('catalog_category_entity')}` WHERE `entity_id` = '{$_categoryId}'";
                                    $result = $this->_readConnection->fetchOne($queryCheckingCategoryId);

                                    /** Category Not found */
                                    if(($result['num_rows']) == 1) {
//                                        Mage::getSingleton('catalog/category_api')
//                                            ->assignProduct($_categoryId, $entityId);
                                        $product = Mage::getModel('catalog/product')->load($entityId);
                                        $categories = $product->getCategoryIds();
                                        $categories[] = $_categoryId;
                                        $product->setCategoryIds($categories);
                                        $product->save();
                                    } else {
                                        Mage::getSingleton('core/session')->addError('Catagory ID : '.$_categoryId.' Not found');
                                        Mage::getSingleton('adminhtml/session')->addError('Catagory ID : '.$_categoryId.' Not found');
                                        continue;
                                    }
                                }
                            } else {
                                $sqlQuery = "UPDATE `{$this->_productTable}_{$headerArray[$headerIndex]['backend_type']}`
                                                SET `value` = '{$csvContent[$contentIndex][$headerArray[$headerIndex]['attribute_code']]}'
                                                WHERE `entity_id` = '{$entityId}' AND `attribute_id` = '{$headerArray[$headerIndex]['attribute_id']}'";
                                $this->_writeConnection->query($sqlQuery);
                            }
                        }
                    }

                    $parentIds = Mage::getResourceSingleton('catalog/product_type_configurable')
                        ->getParentIdsByChild($entityId);
                    $configArr = array_merge($configArr, $parentIds);

                }

                $configArr = array_unique($configArr);
                $time_elapsed_secs = number_format(microtime(true) - $start, 2);
                Mage::getSingleton('core/session')->addSuccess("It takes ".$time_elapsed_secs."(s) to import {$csvContentCount} record(s).");
                Mage::getSingleton('admin/session')->addSuccess("It takes ".$time_elapsed_secs."(s) to import {$csvContentCount} record(s).");

            } catch (Mage_Exception $e){
                Mage::log($e->getMessage(), null,'ChangeInfoByCSV.log');
            }
        }

        // Update configurable product
        $startUpdateConfig = microtime(true);
        Mage::log('============================================', null, 'super_price.log', true);
        Mage::log('Change configurable product price start at ' . date('d/m/Y H:i:s'), null, 'super_price.log', true);
        Mage::log("Setting...", null, 'super_price.log', true);

        $count = 0;
        $pModel =  Mage::getModel('catalog/product');
        foreach ($configArr as $confgId) {
            $configurableProduct = $pModel->load($confgId);
            /** @var Mage_Catalog_Model_Product $configurableProduct */
            try {
                $this->setPriceByBestValue($configurableProduct);
                $this->resetSupperPrice($configurableProduct);
                $count++;
            } catch (Exception $e) {
                Mage::log($configurableProduct->getId(), null, 'super-price.log', true);
                Mage::log($e->getMessage(), null, 'super_price.log', true);
            }
        }
        $time_update_config = number_format(microtime(true) - $startUpdateConfig, 2);
        Mage::getSingleton('core/session')->addSuccess("It takes ".$time_update_config."(s) to update {$count} configurable products.");
        Mage::getSingleton('admin/session')->addSuccess("It takes ".$time_update_config."(s) to update {$count} configurable products.");
        Mage::log("Total: " . $count . ' configurable products', null, 'super_price.log', true);
        Mage::log("Finish at " . date('d/m/Y H:i:s'), null, 'super_price.log', true);
        Mage::log("It takes ".$time_update_config."(s) to update ".$count." configurable products.", null, 'super_price.log', true);
        Mage::log('============================================', null, 'super_price.log', true);

        // Reindex second time
        $startReindex = microtime(true);
        // $process = Mage::getSingleton('index/indexer')->getProcessByCode("catalog_product_price");
        // $process->reindexAll();
        $process = Mage::getSingleton('index/indexer');
        $process->getProcessByCode('catalog_product_price')
            ->changeStatus(Mage_Index_Model_Process::STATUS_REQUIRE_REINDEX);
        $time_reindex = number_format(microtime(true) - $startReindex, 2);
        Mage::getSingleton('core/session')->addSuccess("It takes ".$time_reindex."(s) to reindex.");
        Mage::getSingleton('admin/session')->addSuccess("It takes ".$time_reindex."(s) to reindex.");
    }

    //============================================================
    // Set price by best prodcut
    //============================================================
    public function setPriceByBestValue($product)
    {
        /** @var Mage_Catalog_Model_Product $product */
        /** @var Mage_Catalog_Model_Product $priceOfBestValue */
        try {
            $bestProduct = Mage::helper('petloftcatalog')->getBestProduct($product);
            if ($bestProduct) {
                $product->setPrice($bestProduct->getFinalPrice());
                $product->setMsrp($bestProduct->getMsrp());
                $product->save();
            } else {
                $product->setStatus(2);
                $product->setPrice(null);
                $product->setMsrp(null);
                $product->save();
            }
        } catch (Exception $e) {
            Mage::log($product->getId(), null, 'super-price.log', true);
            Mage::log($e->getMessage(), null, 'super_price.log', true);
        }
    }

    //============================================================
    // Reset supper price
    //============================================================
    public function resetSupperPrice($product)
    {
        try {
            $configAttributes = $this->getSupperPrice($product);
            if ($configAttributes) {
                /** @var Mage_Catalog_Model_Product $product */
                $product->setConfigurableAttributesData($configAttributes)->save();
            }
        } catch (Exception $e) {
            Mage::log($product->getId(), null, 'super-price.log', true);
            Mage::log($e->getMessage(), null, 'super_price.log', true);
        }
    }

    //========================================================================
    // get supper price by best value price minus price of each simple product
    //========================================================================
    public function getSupperPrice($parent)
    {
        try {
            // Attribute Array
            $configAttributes = $parent->getTypeInstance(true)->getConfigurableAttributesAsArray($parent);

            $configurable = Mage::getModel('catalog/product_type_configurable')->setProduct($parent);
            $simpleCollection = $configurable->getUsedProductCollection()
                ->addAttributeToSelect('*')
                ->addFilterByRequiredOptions();

            // Child array
            $configChild = array();
            foreach ($simpleCollection as $aProduct) {
                /** @var Mage_Catalog_Model_Product $aProduct - */
                $key = $aProduct->getEntityId();
                $value = array();
                foreach ($configAttributes as $attr) {
                    foreach ($attr['values'] as $option) {
                        if ($option['value_index'] == $aProduct->getData($attr['attribute_code'])) {
                            $data['attribute_id'] = $attr['attribute_id'];
                            $data['label'] = $option['label'];
                            $data['value_index'] = $option['value_index'];
                            $value[] = $data;
                        }
                    }
                }
                $configChild[$key] = $value;
            }

            $bestProduct = Mage::helper('petloftcatalog')->getBestProduct($parent);
            if ($bestProduct) {
                $parentPrice = $bestProduct->getFinalPrice();
            } else {
                $parentPrice = $parent->getFinalPrice();
            }

            $count = 0;
            $pos = 0;

            foreach ($configAttributes as $attribute) {
                if ($attribute['position'] == 0) {
                    $pos = $count;
                } else {
                    $count++;
                }
            }

            // pricing value array
            $children = array();
            foreach ($configChild as $id => $data) {
                if (!isset($data[$pos]['value_index'])) {
                    continue;
                }
                $children[$data[$pos]['value_index']] = Mage::getModel('catalog/product')->load($id)->getFinalPrice();
            }

            if (empty($children)) {
                return null;
            }
            $count = 0;
            foreach ($configAttributes as &$attribute) {
                if (isset($attribute['values']) && is_array($attribute['values'])) {
                    if ($attribute['position'] == 0 && $count == 0) {
                        $count++;
                        foreach ($attribute['values'] as &$attributeValue) {
                            $valueIndx = isset($attributeValue['value_index']) ? $attributeValue['value_index'] : '';
                            foreach ($children as $indx => $finalPrice) {
                                if ($indx == $valueIndx) {
                                    $attributeValue['pricing_value'] = $finalPrice - $parentPrice;
                                    $attributeValue['is_percent'] = 0;
                                    $attributeValue['can_edit_price'] = 0;
                                    $attributeValue['can_read_price'] = 1;
                                }
                            }
                        }
                    } else {
                        foreach ($attribute['values'] as &$attributeValue) {
                            $attributeValue['pricing_value'] = 0;
                            $attributeValue['is_percent'] = 0;
                            $attributeValue['can_edit_price'] = 0;
                            $attributeValue['can_read_price'] = 1;
                        }
                    }
                }
            }
            return $configAttributes;
        } catch (Exception $e) {
            Mage::log($parent->getId(), null, 'super-price.log', true);
            Mage::log($e->getMessage(), null, 'super_price.log', true);
        }
    }
}