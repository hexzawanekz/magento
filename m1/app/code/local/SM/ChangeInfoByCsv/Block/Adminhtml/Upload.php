<?php
class SM_ChangeInfoByCsv_Block_Adminhtml_Upload extends Mage_Adminhtml_Block_Widget_Form_Container{
    protected $_blockGroup  = 'sm_changeinfobycsv';
    protected $_mode        = 'edit';
    protected $_controller  = 'adminhtml_upload';
    protected $_headerText  = 'Change Price by Csv';

}