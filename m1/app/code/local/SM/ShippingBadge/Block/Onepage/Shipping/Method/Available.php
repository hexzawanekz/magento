<?php

class SM_ShippingBadge_Block_Onepage_Shipping_Method_Available extends Mage_Checkout_Block_Onepage_Shipping_Method_Available
{
    public function checkDropshipProduct()
    {
        /** @var Mage_Sales_Model_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        /** @var Mage_Sales_Model_Quote_Item $items */
        $items = $quote->getAllVisibleItems();
        // Product ids array: $pIds
        $pIds = array();
        foreach ($items as $item) {
            /** @var Mage_Sales_Model_Quote_Item $item */
            $pIds[] = $item->getProductId();
        }

        $totalItems = $quote->getItemsCount();
        if ($totalItems > 0) {
            $count = 0;
            $collection = Mage::getResourceModel('catalog/product_collection')
                ->addFieldToFilter('entity_id', $pIds)
                ->addAttributeToSelect('shipping_duration');
            foreach ($collection as $product) {
                /** @var Mage_Catalog_Model_Product $product */
                if ($product->getAttributeText('shipping_duration') == 'Dropship Product') {
                    $count++;
                }
            }
            if ($count == $totalItems) {
                return true;
            }
        }
        return false;
    }
}