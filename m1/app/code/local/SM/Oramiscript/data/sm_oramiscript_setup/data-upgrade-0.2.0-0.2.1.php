<?php

// init Magento
Mage::app();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

$moxyen = Mage::getModel('core/store')->load('moxyen', 'code')->getId();
$moxyth = Mage::getModel('core/store')->load('moxyth', 'code')->getId();

// Not separate welcome-brand-logo anymore
/** @var Mage_Cms_Model_Resource_Block_Collection $allBlocks */
$allBlocks = Mage::getModel('cms/block')->getCollection()
    ->addFieldToFilter('identifier', 'welcome-brand-logo');
if($allBlocks->count() > 0) {
    /** @var Mage_Cms_Model_Block $item */
    foreach ($allBlocks as $item) {
        if ($item->getId()) $item->delete(); // delete old blocks
    }
}

//==========================================================================
// Homepage Brand Logo Icons
//==========================================================================
$blockTitle = "Welcome Brand Logo Icons";
$blockIdentifier = "welcome-brand-logo";
$blockStores = 0;
$blockIsActive = 1;
$blockContent = <<<EOD
<div class="welcome-brand-logo">
	<h3 class="line-thought desktop">Our Popular Brands</h3>
	<h3 class="line-thought mobile">Our Popular Brands</h3>
	<div class="brand-items">
		<ul>
			<li><a href="http://www.orami.co.th/en/baby/brands/mamy-poko.html"><img src="{{media url='wysiwyg/welcomePage/Brands/brands_mamypoko.jpg'}}" alt="" /></a></li>
			<li><a href="http://www.orami.co.th/en/electronics/brands/apple.html"><img src="{{media url='wysiwyg/welcomePage/Brands/brands_apple.jpg'}}" alt="" /></a></li>
			<li><a href="http://www.orami.co.th/en/pets/brands/royal-canin.html"><img src="{{media url='wysiwyg/welcomePage/Brands/brands_royalcanin.jpg'}}" alt="" /></a></li>
			<li><a href="http://www.orami.co.th/en/health/brands/vistra.html"><img src="{{media url='wysiwyg/welcomePage/Brands/brands_vistra.jpg'}}" alt="" /></a></li>
			<li><a href="http://www.orami.co.th/en/fashion/brands/casio.html"><img src="{{media url='wysiwyg/welcomePage/Brands/brands_casio.jpg'}}" alt="" /></a></li>
			<li><a href="http://www.orami.co.th/en/beauty/brands/nyx.html"><img src="{{media url='wysiwyg/welcomePage/Brands/brands_nyx.jpg'}}" alt="" /></a></li>
			<li><a href="http://www.orami.co.th/en/beauty/brands/urban-decay.html"><img src="{{media url='wysiwyg/welcomePage/Brands/brands_urban.jpg'}}" alt="" /></a></li>
			<li><a href="http://www.orami.co.th/en/home-appliances/brands/electrolux.html"><img src="{{media url='wysiwyg/welcomePage/Brands/brands_electrolux.jpg'}}" alt="" /></a></li>
			<li><a href="http://www.orami.co.th/en/health/brands/brand-s.html"><img src="{{media url='wysiwyg/welcomePage/Brands/brands_brands.jpg'}}" alt="" /></a></li>
			<li><a href="http://www.orami.co.th/en/baby/brands/fisher-price.html"><img src="{{media url='wysiwyg/welcomePage/Brands/brands_price.jpg'}}" alt="" /></a></li>
			<li><a href="http://www.orami.co.th/en/health/brands/360-ongsa.html"><img src="{{media url='wysiwyg/welcomePage/Brands/brands_360.jpg'}}" alt="" /></a></li>
			<li><a href="http://www.orami.co.th/en/pets/brands/friskies.html"><img src="{{media url='wysiwyg/welcomePage/Brands/brands_friskies.jpg'}}" alt="" /></a></li>
			<li><a href="http://www.orami.co.th/en/electronics/brands/samsung.html"><img src="{{media url='wysiwyg/welcomePage/Brands/brands_samsung.jpg'}}" alt="" /></a></li>
			<li><a href="http://www.orami.co.th/en/baby/diapers/popular-brands/huggies.html"><img src="{{media url='wysiwyg/welcomePage/Brands/brands_huggies.jpg'}}" alt="" /></a></li>
			<li><a href="http://www.orami.co.th/fashion/brands/ray-ban.html"><img src="{{media url="wysiwyg/welcomePage/brands_rayban.jpg"}}" alt="rayban" /></a></li>
			<li><a href="http://www.orami.co.th/en/beauty/brands/l-oreal.html"><img src="{{media url='wysiwyg/welcomePage/Brands/brands_loreal.jpg'}}" alt="" /></a></li>
		</ul>
		<div style="clear: both;">&nbsp;</div>
	</div>
</div>
EOD;
/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block')->getCollection()
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================


//==========================================================================
// Homepage Bottom Banner (English)
//==========================================================================
$blockTitle = "Welcome Bottom Banner";
$blockIdentifier = "welcome-bottom-banner";
$blockStores = array($moxyen);
$blockIsActive = 0;
$blockContent = <<<EOD
<div id="welcome-bottom-banner">
    <div class="welcome-bottom-banner-a">
        <a href="#">
            <img title="" src='{{media url="wysiwyg/welcomePage/650x270.jpg"}}' alt="" />
        </a>
    </div>
    <div class="welcome-bottom-banner-b">
        <a href="#">
            <img title="" src='{{media url="wysiwyg/welcomePage/270x270.jpg"}}' alt="" />
        </a>
    </div>
    <div class="close-bottom-banner"></div>
</div>
EOD;
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================

//==========================================================================
// Welcome Bottom Banner (Thai)
//==========================================================================
$blockTitle = "Welcome Bottom Banner";
$blockIdentifier = "welcome-bottom-banner";
$blockStores = array($moxyth);
$blockIsActive = 0;
$blockContent = <<<EOD
<div id="welcome-bottom-banner">
    <div class="welcome-bottom-banner-a">
        <a href="#">
            <img title="" src='{{media url="wysiwyg/welcomePage/650x270.jpg"}}' alt="" />
        </a>
    </div>
    <div class="welcome-bottom-banner-b">
        <a href="#">
            <img title="" src='{{media url="wysiwyg/welcomePage/270x270.jpg"}}' alt="" />
        </a>
    </div>
    <div class="close-bottom-banner"></div>
</div>
EOD;
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================

//==========================================================================
// csm page: Home ENG
//==========================================================================
$title = 'ORAMI by Moxy The Online Shopping Destination for Women';
$identifier = "home";
$stores = array($moxyen);
$status = 1;
$content = <<<EOD
{{block type="core/template" name="subscription" as="subscription" template="subscription/subscribe.phtml"}}
EOD;
$rootTemplate = "one_column";
$layoutXML = <<<EOD
<reference name="head">
    <block type="core/text" name="homepage.metadata" after="-">
        <action method="addText"><text><![CDATA[<meta name="google-site-verification" content="IDwIJ1YbPmxWd60Ej1zWtvkggB95TZAyX5lIPOFn5s4"/>]]></text></action>
    </block>
</reference>

<reference name="content">
    <block type="cms/block" name="home.sliders">
        <action method="setBlockId"><block_id>welcome-shortcut</block_id></action>
    </block>

    <block type="petloftcatalog/product_homegrid" name="home.product.grid1" as="homegrid1" template="petloft/catalog/product/homegrid.phtml">
        <action method="setCategoryId"><id>2655</id></action>
    </block>

    <block type="cms/block" name="home.promotion.banner">
        <action method="setBlockId"><block_id>welcome-promotion-banner</block_id></action>
    </block>

    <block type="petloftcatalog/product_homegrid" name="home.product.grid2" as="homegrid2" template="petloft/catalog/product/homegrid.phtml">
        <action method="setCategoryId"><id>2656</id></action>
    </block>

    <block type="cms/block" name="welcome.content.banner">
        <action method="setBlockId"><block_id>welcome-content-banner</block_id></action>
    </block>

    <block type="cms/block" name="welcome.bottom.banner">
        <action method="setBlockId"><block_id>welcome-bottom-banner</block_id></action>
    </block>

    <block type="cms/block" name="welcome.brand.logo">
        <action method="setBlockId"><block_id>welcome-brand-logo</block_id></action>
    </block>
</reference>
EOD;
$metaDesc = 'Beauty, Fashion, Home Decor, Gadgets & Electronics, Health & Sports, Mom & Babies, Pet Products - shipped directly to you!';

/** @var Mage_Cms_Model_Page $page */
$page = Mage::getModel('cms/page')->getCollection()
    ->addStoreFilter($stores, $withAdmin = false)
    ->addFieldToFilter('identifier', $identifier)
    ->getFirstItem();

if($page->getId()) $page->delete();
$page = Mage::getModel('cms/page');
$page->setTitle($title);
$page->setIdentifier($identifier);
$page->setIsActive($status);
$page->setContent($content);
$page->setRootTemplate($rootTemplate);
$page->setLayoutUpdateXml($layoutXML);
$page->setMetaDescription($metaDesc);
$page->setStores($stores);
$page->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// cms page:Home THAI
//==========================================================================
$title = 'ORAMI by Moxy เว็บช้อปปิ้งออนไลน์ สำหรับสาวนักช้อปทุกคน';
$identifier = "home";
$stores = array($moxyth);
$status = 1;
$content = <<<EOD
{{block type="core/template" name="subscription" as="subscription" template="subscription/subscribe.phtml"}}
EOD;
$rootTemplate = "one_column";
$layoutXML = <<<EOD
<reference name="head">
    <block type="core/text" name="homepage.metadata" after="-">
        <action method="addText"><text><![CDATA[<meta name="google-site-verification" content="IDwIJ1YbPmxWd60Ej1zWtvkggB95TZAyX5lIPOFn5s4"/>]]></text></action>
    </block>
</reference>

<reference name="content">
    <block type="cms/block" name="home.sliders">
        <action method="setBlockId"><block_id>welcome-shortcut</block_id></action>
    </block>

    <block type="petloftcatalog/product_homegrid" name="home.product.grid1" as="homegrid1" template="petloft/catalog/product/homegrid.phtml">
        <action method="setCategoryId"><id>2655</id></action>
    </block>

    <block type="cms/block" name="home.promotion.banner">
        <action method="setBlockId"><block_id>welcome-promotion-banner</block_id></action>
    </block>

    <block type="petloftcatalog/product_homegrid" name="home.product.grid2" as="homegrid1" template="petloft/catalog/product/homegrid.phtml">
        <action method="setCategoryId"><id>2656</id></action>
    </block>

    <block type="cms/block" name="welcome.content.banner">
        <action method="setBlockId"><block_id>welcome-content-banner</block_id></action>
    </block>

    <block type="cms/block" name="welcome.bottom.banner">
        <action method="setBlockId"><block_id>welcome-bottom-banner</block_id></action>
    </block>

    <block type="cms/block" name="welcome.brand.logo">
        <action method="setBlockId"><block_id>welcome-brand-logo</block_id></action>
    </block>
</reference>
EOD;
$metaDesc = 'เครื่องสำอาง แฟชั่น ของแต่งบ้าน แก็ตเจ็ต อีเล็คทรอนิกส์ สินค้าเพื่อสุขภาพ สินค้าสำหรับแม่และเด็ก และสินค้าสำหรับสัตว์เลี้ยง พร้อมส่งตรงถึงหน้าบ้านคุณ!';

/** @var Mage_Cms_Model_Page $page */
$page = Mage::getModel('cms/page')->getCollection()
    ->addStoreFilter($stores, $withAdmin = false)
    ->addFieldToFilter('identifier', $identifier)
    ->getFirstItem();

if($page->getId()) $page->delete();
$page = Mage::getModel('cms/page');
$page->setTitle($title);
$page->setIdentifier($identifier);
$page->setIsActive($status);
$page->setContent($content);
$page->setRootTemplate($rootTemplate);
$page->setLayoutUpdateXml($layoutXML);
$page->setMetaDescription($metaDesc);
$page->setStores($stores);
$page->save();
//==========================================================================
//==========================================================================
//==========================================================================