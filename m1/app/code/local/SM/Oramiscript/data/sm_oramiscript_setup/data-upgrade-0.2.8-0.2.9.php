<?php

$moxyen = Mage::getModel('core/store')->load('moxyen', 'code')->getId();// Moxy
$moxyth = Mage::getModel('core/store')->load('moxyth', 'code')->getId();

//==========================================================================
// Security Cart EN
//==========================================================================
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($moxyen, $withAdmin = false)
    ->addFieldToFilter('identifier', 'cart_security')
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$content = <<<EOD
<img  src="{{skin url='images/cart_security.png'}}" alt="" />
EOD;
/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block');
$block->setTitle('cart_security EN');
$block->setIdentifier('cart_security');
$block->setStores($moxyen);
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================

//==========================================================================
// Security Cart TH
//==========================================================================
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($moxyth, $withAdmin = false)
    ->addFieldToFilter('identifier', 'cart_security')
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$content = <<<EOD
<img  src="{{skin url='images/cart_security.png'}}" alt="" />
EOD;
/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block');
$block->setTitle('cart_security TH');
$block->setIdentifier('cart_security');
$block->setStores($moxyth);
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================

//==========================================================================
// Homepage Brand Logo Icons
//==========================================================================
$blockTitle = "Welcome Brand Logo Icons";
$blockIdentifier = "welcome-brand-logo";
$blockStores = 0;
$blockIsActive = 1;
$blockContent = <<<EOD
<div class="welcome-brand-logo">
	<h3 class="line-thought desktop">Our Popular Brands     <a href="http://www.orami.co.th/all-brands">( View All )</a></h3>
	<h3 class="line-thought mobile">Our Popular Brands     <a href="http://www.orami.co.th/all-brands">( View All )</a></h3>
	<div class="brand-items">
		<ul>
			<li><a href="http://www.orami.co.th/en/baby/brands/mamy-poko.html"><img src="{{media url='wysiwyg/welcomePage/Brands/brands_mamypoko.jpg'}}" alt="" /></a></li>
			<li><a href="http://www.orami.co.th/en/electronics/brands/apple.html"><img src="{{media url='wysiwyg/welcomePage/Brands/brands_apple.jpg'}}" alt="" /></a></li>
			<li><a href="http://www.orami.co.th/en/pets/brands/royal-canin.html"><img src="{{media url='wysiwyg/welcomePage/Brands/brands_royalcanin.jpg'}}" alt="" /></a></li>
			<li><a href="http://www.orami.co.th/en/health/brands/vistra.html"><img src="{{media url='wysiwyg/welcomePage/Brands/brands_vistra.jpg'}}" alt="" /></a></li>
			<li><a href="http://www.orami.co.th/en/fashion/brands/casio.html"><img src="{{media url='wysiwyg/welcomePage/Brands/brands_casio.jpg'}}" alt="" /></a></li>
			<li><a href="http://www.orami.co.th/en/beauty/brands/nyx.html"><img src="{{media url='wysiwyg/welcomePage/Brands/brands_nyx.jpg'}}" alt="" /></a></li>
			<li><a href="http://www.orami.co.th/en/beauty/brands/urban-decay.html"><img src="{{media url='wysiwyg/welcomePage/Brands/brands_urban.jpg'}}" alt="" /></a></li>
			<li><a href="http://www.orami.co.th/en/home-appliances/brands/electrolux.html"><img src="{{media url='wysiwyg/welcomePage/Brands/brands_electrolux.jpg'}}" alt="" /></a></li>
			<li><a href="http://www.orami.co.th/en/health/brands/brand-s.html"><img src="{{media url='wysiwyg/welcomePage/Brands/brands_brands.jpg'}}" alt="" /></a></li>
			<li><a href="http://www.orami.co.th/en/baby/brands/fisher-price.html"><img src="{{media url='wysiwyg/welcomePage/Brands/brands_price.jpg'}}" alt="" /></a></li>
			<li><a href="http://www.orami.co.th/en/health/brands/360-ongsa.html"><img src="{{media url='wysiwyg/welcomePage/Brands/brands_360.jpg'}}" alt="" /></a></li>
			<li><a href="http://www.orami.co.th/en/pets/brands/friskies.html"><img src="{{media url='wysiwyg/welcomePage/Brands/brands_friskies.jpg'}}" alt="" /></a></li>
			<li><a href="http://www.orami.co.th/en/electronics/brands/samsung.html"><img src="{{media url='wysiwyg/welcomePage/Brands/brands_samsung.jpg'}}" alt="" /></a></li>
			<li><a href="http://www.orami.co.th/en/baby/diapers/popular-brands/huggies.html"><img src="{{media url='wysiwyg/welcomePage/Brands/brands_huggies.jpg'}}" alt="" /></a></li>
			<li><a href="http://www.orami.co.th/fashion/brands/ray-ban.html"><img src="{{media url="wysiwyg/welcomePage/brands_rayban.jpg"}}" alt="rayban" /></a></li>
			<li><a href="http://www.orami.co.th/en/beauty/brands/l-oreal.html"><img src="{{media url='wysiwyg/welcomePage/Brands/brands_loreal.jpg'}}" alt="" /></a></li>
		</ul>
		<div style="clear: both;">&nbsp;</div>
	</div>
</div>
EOD;
/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block')->getCollection()
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================