<?php
// init Magento
Mage::app();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

$moxyen = Mage::getModel('core/store')->load('moxyen', 'code')->getId();
$moxyth = Mage::getModel('core/store')->load('moxyth', 'code')->getId();

/** @var Mage_Cms_Model_Resource_Block_Collection $allBlocks */
$allBlocks = Mage::getModel('cms/block')->getCollection()
    ->addFieldToFilter('identifier', 'home-middle-banner');
if($allBlocks->count() > 0) {
    /** @var Mage_Cms_Model_Block $item */
    foreach ($allBlocks as $item) {
        if ($item->getId()) $item->delete(); // delete old blocks
    }
}

//==========================================================================
// Home middle Banner (English)
//==========================================================================
$blockTitle = "Homepage Middle Banner";
$blockIdentifier = "home-middle-banner";
$blockStores = array($moxyen);
$blockIsActive = 1;
$blockContent = <<<EOD
<div id="home-middle-banner">
<div id="home-middle-banner-container">
<h3 class="line-thought">Inspiration For Day | <span style="color: #ff6c6c;">Orami Magazine</span></h3>
<p id="middle-banner-text">A collection of inspirational articles around fashion, mother and child to love and lifestyle</p>
<div class="row">
<div class="col-sm-6"><img title="" src="{{media url="wysiwyg/welcomePage/magazine-banner.jpg"}}" alt="" /></div>
<div class="col-sm-6"><img title="" src="{{media url="wysiwyg/welcomePage/magazine-banner.jpg"}}" alt="" /></div>
</div>
</div>
</div>
EOD;
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================


//==========================================================================
// Home middle Banner (Thai)
//==========================================================================
$blockTitle = "Homepage Middle Banner";
$blockIdentifier = "home-middle-banner";
$blockStores = array($moxyth);
$blockIsActive = 1;
$blockContent = <<<EOD
<div id="home-middle-banner">
<div id="home-middle-banner-container">
<h3 class="line-thought">Inspiration For Day | <span style="color: #ff6c6c;">Orami Magazine</span></h3>
<p id="middle-banner-text">A collection of inspirational articles around fashion, mother and child to love and lifestyle</p>
<div class="row">
<div class="col-sm-6"><img title="" src="{{media url="wysiwyg/welcomePage/magazine-banner.jpg"}}" alt="" /></div>
<div class="col-sm-6"><img title="" src="{{media url="wysiwyg/welcomePage/magazine-banner.jpg"}}" alt="" /></div>
</div>
</div>
</div>
EOD;
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================
