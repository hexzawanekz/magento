<?php

//==========================================================================
// Set Thai text for Shipping method:table rate
//==========================================================================
$config = new Mage_Core_Model_Config();
$shippingDescription = <<<EOD
<div style="line-height:20px; font-weight: bold;">
    จัดส่งสินค้าฟรีเมื่อมียอดซื้อสินค้าขั้นต่ำ 500 บาทต่อการสั่งซื้อ1ครั้ง เฉพาะเขตกรุงเทพ
</div>
<div style="line-height:20px;font-weight: 100;">
    การจัดส่งในเขตกรุงเทพ ปริมณฑล และต่างจังหวัด ทางเราจัดส่งโดยบริษัทที่เชี่ยวชาญทางด้านการจัดส่งสินค้า คุณจะได้รับสินค้าถึงมือภายใน 2-3 วันทำการ.
</div>
EOD;


$config->saveConfig('carriers/tablerate/title', $shippingDescription, 'stores', 17);
//==========================================================================
//==========================================================================
//==========================================================================

//==========================================================================
// Set Thai text for COD payment method
//==========================================================================
$config = new Mage_Core_Model_Config();
$codDescription = <<<EOD
<div style="line-height:20px;">
    วิธีการชำระเงินด้วยวิธีเก็บเงินปลายทาง คุณสามารถชำระเงินปลายทาง เมื่อคุณได้รับสินค้าตามที่อยู่ที่แจ้งไว้กับพนักงานส่งสินค้า พร้อมกับใบเสร็จรับเงิน
</div>
<style>
    #payment_form_cashondelivery{width:208px !important;}
    #payment_form_one23tescolotus{border: 1px solid rgb(234, 233, 233); background-color: rgb(252, 252, 252); float: left; padding: 10px; width: 86%; margin-right: 6%;}
    #payment_form_one23tescolotus li{border-bottom: 1px dotted rgb(213, 213, 213);}
    #payment_form_one23tescolotus li:first-child{border-bottom:none;}
    #payment_form_one23tescolotus li:last-child{border-bottom:none;}
    #payment_form_one23tescolotus label.required{font-weight:bold;}
</style>
EOD;


$config->saveConfig('payment/cashondelivery/customtext', $codDescription, 'stores', 17);
//==========================================================================
//==========================================================================
//==========================================================================