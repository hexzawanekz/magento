<?php
/** @var Mage_Core_Model_Resource_Setup $installer */
$installer = $this;

$installer->startSetup();
/** Order Confirmation Mail Template Moxy */
/** @var Mage_Adminhtml_Model_Email_Template $orderConfirmation */
$orderConfirmation = Mage::getModel('adminhtml/email_template');
$orderConfirmation->loadByCode('Order Confirmation Orami');
$mailContent = <<<HTML
<meta charset="utf-8"/>
<div class="mail-container" style="width:600px; min-width:600px; padding:0px 33px; font-family:  'Trebuchet MS', Tahoma; font-size: 14px; color: #3c3d41; margin: 0px;">
    <div class="mail-header" style="width:95%; min-height: 110px; max-height: 110px; position: relative; margin:0px auto;">
        <div class="mail-logo" style="width:220px; min-height: 110px; max-height: 110px; margin:0px auto; text-align:center;">
            <a href="http://orami.co.th/"><img src="{{skin _area="frontend"  url="images/email/moxy-orami-logo.png"}}" style="width:220px; max-height: 110px; min-height: 110px;" /></a>
        </div>

        <div class="english-below" style="min-height: 16px; max-height: 16px; position: absolute; bottom: 0px; line-height: 16px;">
            <span><img src="{{skin _area="frontend"  url="images/email/flag-en.png"}}" height="16px;"/> Scroll down for English version</span>
        </div>
    </div>

    <div class="header-image" style="width: 100%; position: relative; margin:30px auto; text-align: center">
        <img src="{{skin _area="frontend"  url="images/email/thank_for_your_order_moxy.png"}}" style="width:100%; height: auto;" alt="Thank for your order" />
    </div>

    <div class="mail-body" style="margin:50px auto; width: 95%;">
        <p>เรียน คุณ {{var order.getCustomerName()}},</p>
        <br/>

        <p>ขอขอบคุณสำหรับการสั่งซื้อจาก Orami</p>
        <p>เลขที่ใบสั่งซื้อของคุณคือ  #{{var order.increment_id}}</p>
        <br>

        <p>{{var delivery_th}}</p>
        <br>

        <p>คุณสามารถเช็คสถานะการสั่งซื้อของคุณที่ : <a href="{{store url="customer/account/"}}" style="color:#28438c; text-decoration:none;">{{store url="customer/account/"}}</a></p>
        <p>หากคุณมีคำถามหรือข้อสงสัยใดๆสามารถติดต่อเราได้ที่ 02-106-8222</p>
        <br>

        <p>Warm Regards,</p>
        <p>Love, Orami Team</p>
    </div>

    <!--Billing-->
    <div class="order-detail" style="width: 95%; margin:0 auto; font-size: 12px;">
        <div class="billing-info" style="float: left; width: 49%;">
            <div class="heading" style="background: #ff6c6c; font-size: 12; color: #fff; padding: 3px 0px; float: left; width: 100%;"><div style="margin: 0px 5px; text-transform: uppercase;">ข้อมูลในการออกใบเสร็จ</div></div>
            <div class="billing-content" style="float: left; width: 100%; background: #e7e7e7; min-height: 175px;">
                <div style="margin: 0px 5px;">
                    {{var order.getBillingAddress().format('html')}}
                </div>
            </div>
        </div>

        <div class="payment-method" style="float: right; width: 49%; font-size: 12px;">
            <div class="heading" style="background: #ff6c6c; font-size: 12; color: #fff; padding: 3px 0px; float: left; width: 100%;"><div style="margin: 0px 5px; text-transform: uppercase;">วิธีการชำระเงิน</div></div>
            <div class="billing-content" style="float: left; width: 100%; background: #e7e7e7; min-height: 175px;">
                <div style="margin: 0px 5px;">
                    {{var payment_html}}
                </div>
            </div>
        </div>

        <div style="clear: both;"></div>
    </div>

    {{depend order.getIsNotVirtual()}}
    <!--Shipping-->
    <div class="order-detail" style="width: 95%; margin:15px auto; font-size: 12px;">
        <div class="billing-info" style="float: left; width: 49%;">
            <div class="heading" style="background: #ff6c6c; font-size: 12; color: #fff; padding: 3px 0px; float: left; width: 100%;"><div style="margin: 0px 5px; text-transform: uppercase;">ข้อมูลในการจัดส่งสินค้า</div></div>
            <div class="billing-content" style="float: left; width: 100%; background: #e7e7e7; min-height: 175px;">
                <div style="margin: 0px 5px;">
                    {{var order.getShippingAddress().format('html')}}
                </div>
            </div>
        </div>

        <div class="payment-method" style="float: right; width: 49%; font-size: 12px;">
            <div class="heading" style="background: #ff6c6c; font-size: 12; color: #fff; padding: 3px 0px; float: left; width: 100%;"><div style="margin: 0px 5px; text-transform: uppercase;">วิธีการส่งสินค้า</div></div>
            <div class="billing-content" style="float: left; width: 100%; background: #e7e7e7; min-height: 175px;">
                <div style="margin: 0px 5px;">
                    {{var shippingDescription}}
                </div>
            </div>
        </div>

        <div style="clear: both;"></div>
    </div>
    {{/depend}}

    <!--Order Item-->
    {{layout handle="sales_email_order_items" order=\$order language="th"}}
    <p style="font-size:12px; margin:0 0 10px 0">{{var order.getEmailCustomerNote()}}</p>
    <!--/End OrderItem-->
    <div class="banner" style="max-height: 120px; position: relative; margin:45px auto;">
        {{block type="cms/block" block_id="email_banner_en" }}
    </div>

    <!-- Footer -->
    <div class="navigation-th" style="margin-top: 24px; overflow:hidden;">
        <div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #ff6c6c; border-bottom: 2px solid #ff6c6c; text-align:center;">
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding-right: 5px;"><a href="http://www.orami.co.th/th/beauty.html" style="color: #3c3d41; text-decoration: none;">ความงาม</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.orami.co.th/th/fashion.html" style="color: #3c3d41; text-decoration: none;">แฟชั่น</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.orami.co.th/th/home-living.html" style="color: #3c3d41; text-decoration: none;">ของแต่งบ้าน</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.orami.co.th/th/home-appliances.html" style="color: #3c3d41; text-decoration: none;">เครื่องใช้ไฟฟ้าภายในบ้าน</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.orami.co.th/th/electronics.html" style="color: #3c3d41; text-decoration: none;">อิเล็กทรอนิกส์</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.orami.co.th/th/baby.html" style="color: #3c3d41; text-decoration: none;">แม่และเด็ก</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.orami.co.th/th/health.html" style="color: #3c3d41; text-decoration: none;">สุขภาพ</a></span>
            <span class="nav-item" style=" line-height: 33px; font-size: 12px; padding-left: 5px;"><a href="http://www.orami.co.th/th/pets.html" style="color: #3c3d41; text-decoration: none;">สัตว์เลี้ยง</a></span>
        </div>
    </div>

    <div class="email-footer" style="min-height: 85px; max-heigh: 85px; background: #E7E7E7; margin-top: 8px; color: #3c3d41; padding: 0 15px 5px 15px;">
        <div class="footer-col" style="float:left; padding: 17px 1% 17px 2%; width: 27%; color: #3c3d41;">
            <p style="font-size: 13px; margin: 0px 0px 0px"><img src="{{skin _area='frontend'  url='images/email/mail.png'}}" style="width:20px; float:left; margin-right:5px; margin-left:-2px;"/> ติดต่อเรา</p><div style="clear: both;"></div>
            <p style="font-size: 10px; margin: 0;">02-106-8222</p>
            <p style="margin: 0; font-size: 10px;"><a href="mailto:support@orami.co.th" style="color: #3C3D41; text-decoration: none;">support@orami.co.th</a></p>
        </div>

        <div class="footer-col" style="float:left; padding: 17px 1% 17px 0; width: 32%;">
            <p style="font-size: 13px; margin: 0px 0px 0px">ติดตามที่</p>
            <div class="follow-icons" style="width: 100%">
                <a href="https://www.facebook.com/oramithailand" style="float: left; width: 16%; padding-right: 2%;"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-fb.png"}}" alt="Facebook fanpage"></a>
                <a href="https://twitter.com/ORAMI_TH" style="float: left; width: 16%; padding-right: 2%;"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-twitter.png"}}" alt="Twitter fanpage"></a>
                <a href="https://www.instagram.com/orami_thailand/" style="float: left; width: 16%; padding-right: 2%;"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-instagram.png"}}" alt="Instagram fanpage"></a>
                <a href="https://www.pinterest.com/OramiTH/" style="float: left; width: 16%; padding-right: 2%;"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-pinterest.png"}}" alt="Pinterest fanpage"></a>
            </div>
        </div>

        <div class="footer-col" style="float:left; padding: 17px 0 17px 3%; width: 34%;">
            <p style="font-size: 13px;margin: 0px 0px 0px">ช่องทางการชำระเงิน</p>
            <img src="{{skin _area="frontend"  url="images/email/payment_new.png"}}" alt="ช่องทางการชำระเงิน" style="margin-top: 3px;">
            <div class="cod" style="font-size: 10px; margin-top: -3px;">
                <img src="{{skin _area='frontend'  url='images/email/COD.png'}}" style="float: left; margin-right: 2px;">
                <p style="float: left; margin: 3px 0 0; font-size: 10px;"> ช่องทางการชำระเงิน</p>
            </div>
        </div>
    </div>

    <div class="footer" style="padding: 0; margin-top: 18px;">
        <div class="copyr" style="text-align: center; font-size: 13px;">Copyright &copy; 2016 Orami. All rights reserved</div>
        <div class="links" style="text-align: center; font-size: 13px; margin-top: 5px;">
            <a href="http://www.orami.co.th/th/about-us/" style="color: #3c3d41;"><span>เกี่ยวกับเรา</span></a> |
            <a href="http://www.orami.co.th/th/terms-and-conditions/" style="color: #3c3d41;"><span>ข้อกำหนดและเงื่อนไข</span></a> |
            <a href="http://www.orami.co.th/th/privacy-policy/" style="color: #3c3d41;"><span>ความเป็นส่วนตัว</span></a>
        </div>
    </div>
    <div style="clear:both"></div>
    <!-- End Footer -->

    <hr style="margin: 40px auto; width: 95%;">

    <!--English Version-->
    <div class="mail-body" style="margin:50px auto; width: 95%;">
        <p>Dear {{var order.getCustomerName()}},</p>

        <br/>
        <p>Thank you for your purchasing at Orami.</p>
        <p>Your order number is #{{var order.increment_id}}</p>
        <br>

        <p>{{var delivery_en}}</p>
        <br>

        <p>Track the status of your order here : <a href="{{store url="customer/account/"}}" style="color:#28438c; text-decoration:none;">{{store url="customer/account/"}}</a></p>
        <p> You can also reach our customer service at +662-106-8222</p>
        <br>

        <p>Warm Regards,</p>
        <p>Love, Orami Team</p>
    </div>

    <!--Billing-->
    <div class="order-detail" style="width: 95%; margin:0 auto; font-size: 12px;">
        <div class="billing-info" style="float: left; width: 49%;">
            <div class="heading" style="background: #ff6c6c; font-size: 12; color: #fff; padding: 3px 0px; float: left; width: 100%;"><div style="margin: 0px 5px; text-transform: uppercase;">Billing Information</div></div>
            <div class="billing-content" style="float: left; width: 100%; background: #e7e7e7; min-height: 175px;">
                <div style="margin: 0px 5px;">
                    {{var order.getBillingAddress().format('html')}}
                </div>
            </div>
        </div>

        <div class="payment-method" style="float: right; width: 49%; font-size: 12px;">
            <div class="heading" style="background: #ff6c6c; font-size: 12; color: #fff; padding: 3px 0px; float: left; width: 100%;"><div style="margin: 0px 5px; text-transform: uppercase;">Payment Method</div></div>
            <div class="billing-content" style="float: left; width: 100%; background: #e7e7e7; min-height: 175px;">
                <div style="margin: 0px 5px;">
                    {{var payment_html}}
                </div>
            </div>
        </div>

        <div style="clear: both;"></div>
    </div>
    {{depend order.getIsNotVirtual()}}
    <!--Shipping-->
    <div class="order-detail" style="width: 95%; margin:15px auto; font-size: 12px;">
        <div class="billing-info" style="float: left; width: 49%;">
            <div class="heading" style="background: #ff6c6c; font-size: 12; color: #fff; padding: 3px 0px; float: left; width: 100%;"><div style="margin: 0px 5px; text-transform: uppercase;">Shipping Information</div></div>
            <div class="billing-content" style="float: left; width: 100%; background: #e7e7e7; min-height: 175px;">
                <div style="margin: 0px 5px;">
                    {{var order.getShippingAddress().format('html')}}
                </div>
            </div>
        </div>

        <div class="payment-method" style="float: right; width: 49%; font-size: 12px;">
            <div class="heading" style="background: #ff6c6c; font-size: 12; color: #fff; padding: 3px 0px; float: left; width: 100%;"><div style="margin: 0px 5px; text-transform: uppercase;">Shipping Method</div></div>
            <div class="billing-content" style="float: left; width: 100%; background: #e7e7e7; min-height: 175px;">
                <div style="margin: 0px 5px;">
                    {{var shippingDescription}}
                </div>
            </div>
        </div>

        <div style="clear: both;"></div>
    </div>
    {{/depend}}
    <!--Order Item-->
    {{layout handle="sales_email_order_items" order=\$order language="en"}}
    <p style="font-size:12px; margin:0 0 10px 0">{{var order.getEmailCustomerNote()}}</p>
    <!--/End OrderItem-->
    <div class="banner" style="max-height: 120px; position: relative; margin:45px auto;">
        {{block type="cms/block" block_id="email_banner_en" }}
    </div>

    <!-- Footer -->
    <div class="navigation-th" style="margin-top: 24px; overflow:hidden;">
        <div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #ff6c6c; border-bottom: 2px solid #ff6c6c; text-align:center;">
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding-right: 5px;"><a href="http://www.orami.co.th/th/beauty.html" style="color: #3c3d41; text-decoration: none;">Beauty</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.orami.co.th/th/fashion.html" style="color: #3c3d41; text-decoration: none;">Fashion</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.orami.co.th/th/home-living.html" style="color: #3c3d41; text-decoration: none;">Home Decor</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.orami.co.th/th/home-appliances.html" style="color: #3c3d41; text-decoration: none;">Home Appliances</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.orami.co.th/th/electronics.html" style="color: #3c3d41; text-decoration: none;">Electronics</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.orami.co.th/th/baby.html" style="color: #3c3d41; text-decoration: none;">Baby & Mom</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.orami.co.th/th/health.html" style="color: #3c3d41; text-decoration: none;">Health & Sports</a></span>
            <span class="nav-item" style=" line-height: 33px; font-size: 12px; padding-left: 5px;"><a href="http://www.orami.co.th/th/pets.html" style="color: #3c3d41; text-decoration: none;">Pets</a></span>
        </div>
    </div>

    <div class="email-footer" style="min-height: 85px; max-heigh: 85px; background: #E7E7E7; margin-top: 8px; color: #3c3d41;padding: 0 15px 5px 15px;">
        <div class="footer-col" style="float:left; padding: 17px 1% 17px 2%; width: 27%; color: #3c3d41;">
            <p style="font-size: 13px; margin: 0px 0px 0px"><img src="{{skin _area='frontend'  url='images/email/mail.png'}}" style="width:20px; float:left; margin-right:5px; margin-left: -2px;"/> Contact Us</p><div style="clear: both;"></div>
            <p style="font-size: 10px; margin: 0;">02-106-8222</p>
            <p style="margin: 0; font-size: 10px;"><a href="mailto:support@orami.co.th" style="color: #3C3D41; text-decoration: none;">support@orami.co.th</a></p>
        </div>

        <div class="footer-col" style="float:left; padding: 17px 1% 17px 0; width: 32%;">
            <p style="font-size: 13px; margin: 0px 0px 0px">Follow Us</p>
            <div class="follow-icons" style="width: 100%">
                <a href="https://www.facebook.com/oramithailand" style="float: left; width: 16%; padding-right: 2%;"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-fb.png"}}" alt="Facebook fanpage"></a>
                <a href="https://twitter.com/ORAMI_TH" style="float: left; width: 16%; padding-right: 2%;"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-twitter.png"}}" alt="Twitter fanpage"></a>
                <a href="https://www.instagram.com/orami_thailand/" style="float: left; width: 16%; padding-right: 2%;"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-instagram.png"}}" alt="Instagram fanpage"></a>
                <a href="https://www.pinterest.com/OramiTH/" style="float: left; width: 16%; padding-right: 2%;"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-pinterest.png"}}" alt="Pinterest fanpage"></a>
            </div>
        </div>

        <div class="footer-col" style="float:left; padding: 17px 0 17px 3%; width: 34%;">
            <p style="font-size: 13px;margin: 0px 0px 0px">Payment Options</p>
            <img src="{{skin _area="frontend"  url="images/email/payment_new.png"}}" alt="Payment Options" style="margin-top: 3px;">
            <div class="cod" style="font-size: 10px; margin-top: -3px;">
                <img src="{{skin _area='frontend'  url='images/email/COD.png'}}" style="float: left; margin-right: 2px;">
                <p style="float: left; margin: 3px 0 0; font-size: 10px;"> Cash on Delivery</p>
            </div>
        </div>
    </div>

    <div class="footer" style="padding: 0; margin-top: 18px;">
        <div class="copyr" style="text-align: center; font-size: 13px;">Copyright &copy; 2016 Orami. All rights reserved</div>
        <div class="links" style="text-align: center; font-size: 13px; margin-top: 5px;">
            <a href="http://www.orami.co.th/en/about-us/" style="color: #3c3d41;"><span>About Us</span></a> |
            <a href="http://www.orami.co.th/en/terms-and-conditions/" style="color: #3c3d41;"><span>Term & Conditions</span></a> |
            <a href="http://www.orami.co.th/en/privacy-policy/" style="color: #3c3d41;"><span>Privacy Policy</span></a>
        </div>
    </div>
    <div style="clear:both"></div>
    <!-- End Footer -->
</div>
HTML;
$orderConfirmation->setTemplateText($mailContent);
$orderConfirmation->save();
$installer->endSetup();