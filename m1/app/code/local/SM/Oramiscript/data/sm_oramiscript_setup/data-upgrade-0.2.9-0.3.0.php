<?php

$moxyen = Mage::getModel('core/store')->load('moxyen', 'code')->getId();// Moxy
$moxyth = Mage::getModel('core/store')->load('moxyth', 'code')->getId();

//==========================================================================
// New Header Banner EN
//==========================================================================
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($moxyen, $withAdmin = false)
    ->addFieldToFilter('identifier', 'new_header_banner')
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$content = <<<EOD
<img  src="{{skin url='images/free-shipping.jpg'}}" alt="" />
EOD;
/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block');
$block->setTitle('new_header_banner EN');
$block->setIdentifier('new_header_banner');
$block->setStores($moxyen);
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================

//==========================================================================
// New Header Banner TH
//==========================================================================
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($moxyth, $withAdmin = false)
    ->addFieldToFilter('identifier', 'new_header_banner')
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$content = <<<EOD
<img  src="{{skin url='images/free-shipping.jpg'}}" alt="" />
EOD;
/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block');
$block->setTitle('new_header_banner TH');
$block->setIdentifier('new_header_banner');
$block->setStores($moxyth);
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================

 ?>