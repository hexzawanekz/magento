<?php

// Attribute code
$arg_attribute = "shipping_duration";
/** @var Mage_Eav_Model_Entity_Attribute $attribute_model */
$attribute_model = Mage::getModel('eav/entity_attribute');
/** @var Mage_Eav_Model_Entity_Attribute_Source_Table $attribute_options_model */
$attribute_options_model = Mage::getModel('eav/entity_attribute_source_table');

$attribute = $attribute_model->loadByCode('catalog_product', $arg_attribute);
$attribute_table = $attribute_options_model->setAttribute($attribute);
$options = $attribute_options_model->getAllOptions(false);

// determine if this option exists
$value_exists = false;
$data = array();
if (count($options) > 0) {
    foreach ($options as &$opt) {
        if ($opt['label'] == 'Simple Product') {
            $opt['label'] = 'Consignment and Buy-in Product';
            $value_exists = true;
        }
        if ($opt['label'] == 'Dropship and Special Product') {
            $opt['label'] = 'Special Order Product';
            $value_exists = true;
        }

        $data[$opt['value']] = array($opt['label']);
    }
    $data['dropship_ship'] = array("Dropship Product");
}
if ($value_exists) {
    try {
        $dataOption['option']['value'] = $data;
        $attribute->addData($dataOption);
        $attribute->save();
    } catch (Exception $e) {
        Mage::log($e->getMessage(), null, 'shipping-detail.log', true);
    }
}