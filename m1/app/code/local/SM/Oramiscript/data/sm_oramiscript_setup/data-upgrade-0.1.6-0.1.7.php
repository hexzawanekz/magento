<?php


//==========================================================================
// Bank Image Footer
//==========================================================================

/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter(array(0), $withAdmin = false)
    ->addFieldToFilter('identifier', 'bank_image_footer')
    ->getFirstItem();

if ($block->getId()) $block->delete();// if exists then delete
$content = <<<EOD
<div class="bank-list-images">
    <p class="payment_partners_footer">Payment Partners</p>
    <img src="{{skin url="images/footer/payment_partner.png"}}" alt="" />
</div>
<div class="acommerce-logo-footer">
    <p class="shipping_partners_footer">Shipping Partners</p>
    <img src="{{skin url="images/footer/acommerce.png"}}" alt="" />
</div>
<div class="verfied-logo-footer">
    <p class="verified-partner-footer">Verified Services</p>
    <img src="{{skin url="images/footer/verified_service_without_dbd.png"}}" alt="" />
</div>
<div class="pay-over-footer">
    <p class="pay-over">Pay Over The Counter</p>
    <img src="{{skin url="images/footer/pay_over_the_counter.png"}}" alt="" />
</div>
EOD;

$block = Mage::getModel('cms/block');
$block->setTitle('Bank Image Footer');
$block->setIdentifier('bank_image_footer');
$block->setStores(array(0));
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================

//==========================================================================
// Footer Share Links
//==========================================================================
$blockTitle = 'Footer Share Links';
$blockId = 'banks_footer';
$blockStores = array(0);

/** @var Mage_Cms_Model_Block $block */
$blocks = Mage::getModel('cms/block')->getCollection()
    ->addFieldToFilter('identifier', $blockId);

foreach($blocks as $block){
    if ($block->getId()) $block->delete();// if exists then delete
}
$content = <<<EOD
<a href="https://www.facebook.com/oramithailand" target="_blank">
    <span class="fa-stack facebook-footer">
        <img src="{{skin url='images/footer/facebook.png'}}" />
    </span>
</a>
<a  href="https://twitter.com/ORAMI_TH" target="_blank">
    <span class="fa-stack twitter-footer">
        <img src="{{skin url='images/footer/twitter.png'}}" />
    </span>
</a>
<a href="https://www.instagram.com/orami_thailand/" target="_blank">
    <span class="fa-stack instagram-footer">
        <img src="{{skin url='images/footer/instagram.png'}}" />
    </span>
</a>
<a href="https://www.youtube.com/channel/UCMSbWSmzS3ZU12KE2ZfkuSQ" target="_blank">
    <span class="fa-stack youtube-footer">
        <img src="{{skin url='images/footer/youtube.png'}}" />
    </span>
</a>
<a href="https://www.pinterest.com/OramiTH/" target="_blank">
    <span class="fa-stack pinterest-footer">
        <img src="{{skin url='images/footer/pinterest.png'}}" />
    </span>
</a>
EOD;

$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockId);
$block->setStores($blockStores);
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================