<?php
Mage::app();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

$moxyen = Mage::getModel('core/store')->load('moxyen', 'code')->getId();
$moxyth = Mage::getModel('core/store')->load('moxyth', 'code')->getId();

/** @var Mage_Cms_Model_Resource_Block_Collection $allBlocks */
$allBlocks = Mage::getModel('cms/block')->getCollection()
    ->addFieldToFilter('identifier', 'welcome-shortcut');
if ($allBlocks->count() > 0) {
    /** @var Mage_Cms_Model_Block $item */
    foreach ($allBlocks as $item) {
        if ($item->getId()) $item->delete(); // delete old blocks
    }
}

//==========================================================================
// Welcome splash shortcut link (English)
//==========================================================================
$blockTitle = "Welcome splash shortcut link";
$blockIdentifier = "welcome-shortcut";
$blockStores = array($moxyen);
$blockIsActive = 1;
$blockContent = <<<EOD
<div class="grid-wall">
<div class="row">
<div class="col-sm-6">
<div class="row">
<div class="col-sm-12">
<div class="img-banner"><a href="http://www.orami.co.th/cheero-power-plus-10050mah-danboard-version-flower-series.html"><img title="Cheero" src="{{skin url="images/SamplePictures/472x236.png"}}" alt="Cheero" /></a></div>
</div>
</div>
<div class="row">
<div class="col-xs-6">
<div class="img-banner"><a href="http://www.orami.co.th/catalogsearch/result/index/?dir=asc&amp;order=price&amp;q=orijen"><img title="Orijen" src="{{skin url="images/SamplePictures/236x236_1.png"}}" alt="Orijen" /></a></div>
</div>
<div class="col-xs-6">
<div class="img-banner"><a href="http://www.orami.co.th/catalogsearch/result/?q=hospro"><img title="Hospro" src="{{skin url="images/SamplePictures/236x236_2.png"}}" alt="Hospro" /></a></div>
</div>
</div>
</div>
<div class="col-sm-6">
<div class="row">
<div class="col-sm-12">
<div class="img-banner"><a href="http://www.orami.co.th/deal/all-deals/all-about-cleaning.html"><img title="All about cleaning" src="{{skin url="images/SamplePictures/472x472.png"}}" alt="All about cleaning" /></a></div>
</div>
</div>
</div>
</div>
<div class="row">
<div class="col-sm-6">
<div class="row">
<div class="col-sm-12">
<div class="img-banner"><a href="http://www.orami.co.th/deal/all-deals/all-about-cleaning.html"><img title="All about cleaning" src="{{skin url="images/SamplePictures/472x472.png"}}" alt="All about cleaning" /></a></div>
</div>
</div>
</div>
<div class="col-sm-6">
<div class="row">
<div class="col-sm-12">
<div class="img-banner"><a href="http://www.orami.co.th/cheero-power-plus-10050mah-danboard-version-flower-series.html"><img title="Cheero" src="{{skin url="images/SamplePictures/472x236.png"}}" alt="Cheero" /></a></div>
</div>
</div>
<div class="row">
<div class="col-xs-6">
<div class="img-banner"><a href="http://www.orami.co.th/catalogsearch/result/index/?dir=asc&amp;order=price&amp;q=orijen"><img title="Orijen" src="{{skin url="images/SamplePictures/236x236_1.png"}}" alt="Orijen" /></a></div>
</div>
<div class="col-xs-6">
<div class="img-banner"><a href="http://www.orami.co.th/catalogsearch/result/?q=hospro"><img title="Hospro" src="{{skin url="images/SamplePictures/236x236_2.png"}}" alt="Hospro" /></a></div>
</div>
</div>
</div>
</div>
<div class="col-sm-6">
<div class="row">
<div class="col-sm-12">
<div class="img-banner"><a href="http://www.orami.co.th/cheero-power-plus-10050mah-danboard-version-flower-series.html"><img title="Cheero" src="{{skin url="images/SamplePictures/472x236.png"}}" alt="Cheero" /></a></div>
</div>
</div>
<div class="row">
<div class="col-xs-6">
<div class="img-banner"><a href="http://www.orami.co.th/catalogsearch/result/index/?dir=asc&amp;order=price&amp;q=orijen"><img title="Orijen" src="{{skin url="images/SamplePictures/236x236_1.png"}}" alt="Orijen" /></a></div>
</div>
<div class="col-xs-6">
<div class="img-banner"><a href="http://www.orami.co.th/catalogsearch/result/?q=hospro"><img title="Hospro" src="{{skin url="images/SamplePictures/236x236_2.png"}}" alt="Hospro" /></a></div>
</div>
</div>
</div>
<div class="row">
<div class="col-sm-6">
<div class="row">
<div class="col-sm-12">
<div class="img-banner"><a href="http://www.orami.co.th/deal/all-deals/all-about-cleaning.html"><img title="All about cleaning" src="{{skin url="images/SamplePictures/472x472.png"}}" alt="All about cleaning" /></a></div>
</div>
</div>
</div>
</div>
</div>
EOD;
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================


//==========================================================================
// Welcome splash shortcut link (Thai)
//==========================================================================
$blockTitle = "Welcome splash shortcut link";
$blockIdentifier = "welcome-shortcut";
$blockStores = array($moxyth);
$blockIsActive = 1;
$blockContent = <<<EOD
<div class="grid-wall">
<div class="row">
<div class="col-sm-6">
<div class="row">
<div class="col-sm-12">
<div class="img-banner"><a href="http://www.orami.co.th/cheero-power-plus-10050mah-danboard-version-flower-series.html"><img title="Cheero" src="{{skin url="images/SamplePictures/472x236.png"}}" alt="Cheero" /></a></div>
</div>
</div>
<div class="row">
<div class="col-xs-6">
<div class="img-banner"><a href="http://www.orami.co.th/catalogsearch/result/index/?dir=asc&amp;order=price&amp;q=orijen"><img title="Orijen" src="{{skin url="images/SamplePictures/236x236_1.png"}}" alt="Orijen" /></a></div>
</div>
<div class="col-xs-6">
<div class="img-banner"><a href="http://www.orami.co.th/catalogsearch/result/?q=hospro"><img title="Hospro" src="{{skin url="images/SamplePictures/236x236_2.png"}}" alt="Hospro" /></a></div>
</div>
</div>
</div>
<div class="col-sm-6">
<div class="row">
<div class="col-sm-12">
<div class="img-banner"><a href="http://www.orami.co.th/deal/all-deals/all-about-cleaning.html"><img title="All about cleaning" src="{{skin url="images/SamplePictures/472x472.png"}}" alt="All about cleaning" /></a></div>
</div>
</div>
</div>
</div>
<div class="row">
<div class="col-sm-6">
<div class="row">
<div class="col-sm-12">
<div class="img-banner"><a href="http://www.orami.co.th/deal/all-deals/all-about-cleaning.html"><img title="All about cleaning" src="{{skin url="images/SamplePictures/472x472.png"}}" alt="All about cleaning" /></a></div>
</div>
</div>
</div>
<div class="col-sm-6">
<div class="row">
<div class="col-sm-12">
<div class="img-banner"><a href="http://www.orami.co.th/cheero-power-plus-10050mah-danboard-version-flower-series.html"><img title="Cheero" src="{{skin url="images/SamplePictures/472x236.png"}}" alt="Cheero" /></a></div>
</div>
</div>
<div class="row">
<div class="col-xs-6">
<div class="img-banner"><a href="http://www.orami.co.th/catalogsearch/result/index/?dir=asc&amp;order=price&amp;q=orijen"><img title="Orijen" src="{{skin url="images/SamplePictures/236x236_1.png"}}" alt="Orijen" /></a></div>
</div>
<div class="col-xs-6">
<div class="img-banner"><a href="http://www.orami.co.th/catalogsearch/result/?q=hospro"><img title="Hospro" src="{{skin url="images/SamplePictures/236x236_2.png"}}" alt="Hospro" /></a></div>
</div>
</div>
</div>
</div>
<div class="col-sm-6">
<div class="row">
<div class="col-sm-12">
<div class="img-banner"><a href="http://www.orami.co.th/cheero-power-plus-10050mah-danboard-version-flower-series.html"><img title="Cheero" src="{{skin url="images/SamplePictures/472x236.png"}}" alt="Cheero" /></a></div>
</div>
</div>
<div class="row">
<div class="col-xs-6">
<div class="img-banner"><a href="http://www.orami.co.th/catalogsearch/result/index/?dir=asc&amp;order=price&amp;q=orijen"><img title="Orijen" src="{{skin url="images/SamplePictures/236x236_1.png"}}" alt="Orijen" /></a></div>
</div>
<div class="col-xs-6">
<div class="img-banner"><a href="http://www.orami.co.th/catalogsearch/result/?q=hospro"><img title="Hospro" src="{{skin url="images/SamplePictures/236x236_2.png"}}" alt="Hospro" /></a></div>
</div>
</div>
</div>
<div class="row">
<div class="col-sm-6">
<div class="row">
<div class="col-sm-12">
<div class="img-banner"><a href="http://www.orami.co.th/deal/all-deals/all-about-cleaning.html"><img title="All about cleaning" src="{{skin url="images/SamplePictures/472x472.png"}}" alt="All about cleaning" /></a></div>
</div>
</div>
</div>
</div>
</div>
EOD;
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================
