<?php
/**
 * Created by PhpStorm.
 * User: ducnt
 * Date: 8/18/15
 * Time: 2:54 PM
 */

class SM_Brand_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        $this->loadLayout();
        $this->getLayout()->getBlock('head')->setTitle($this->__('All Brands'));
        $this->renderLayout();
    }

}