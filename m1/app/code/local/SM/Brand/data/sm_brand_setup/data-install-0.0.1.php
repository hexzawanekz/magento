<?php


$urlMap = array(
        "all-brands-baby" => "all-brands/index/index/key/baby",
        "all-brands-pets" => "all-brands/index/index/key/pets",
        "all-brands-home-appliances" => "all-brands/index/index/key/home-appliances",
        "all-brands-home-living" => "all-brands/index/index/key/home-living",
        "all-brands-home-fashion" => "all-brands/index/index/key/home-fashion",
        "all-brands-home-beauty" => "all-brands/index/index/key/home-beauty",
        "all-brands-home-health" => "all-brands/index/index/key/home-health",
        "all-brands-home-electronics" => "all-brands/index/index/key/home-electronics",
);

try {

    foreach($urlMap as $old => $new) {
        Mage::getModel('core/url_rewrite')
            ->setIdPath($old)
            ->setRequestPath($old)
            ->setTargetPath($new)
            ->setIsSystem(0)
            ->setType(Mage_Core_Model_Url_Rewrite::TYPE_CUSTOM)
            ->setData('store_id',16)
            ->save();

        Mage::getModel('core/url_rewrite')
            ->setIdPath($old)
            ->setRequestPath($old)
            ->setTargetPath($new)
            ->setIsSystem(0)
            ->setType(Mage_Core_Model_Url_Rewrite::TYPE_CUSTOM)
            ->setData('store_id',17)
            ->save();
    }
} catch (Exception $e) {
  Zend_Debug::dump($e);die();
}