<?php
/**
 * Created by PhpStorm.
 * User: smartosc
 * Date: 4/8/2016
 * Time: 10:54 AM
 */

// Create state/province for Indonesia
/** @var Mage_Catalog_Model_Resource_Setup $installer */
$installer = $this;

$data = array(
//    array('ID', 'AC', 'Aceh'),
);

foreach ($data as $row) {
    $bind = array(
        'country_id' => $row[0],
        'code' => $row[1],
        'default_name' => $row[2],
    );
    //First add data into "directory_country_region" table
    $installer->getConnection()->insert($installer->getTable('directory/country_region'), $bind);

    //Then get the last inserted ID for the regionID
    $regionId = $installer->getConnection()->lastInsertId($installer->getTable('directory/country_region'));

    $bind1 = array(
        'locale' => 'en_US',
        'region_id' => $regionId,
        'name' => $row[2]
    );
    $bind2 = array(
        'locale' => 'th_TH',
        'region_id' => $regionId,
        'name' => $row[2]
    );
    //Insert data into "directory_country_region_name" table
    $installer->getConnection()->insert($installer->getTable('directory/country_region_name'), $bind1);
    $installer->getConnection()->insert($installer->getTable('directory/country_region_name'), $bind2);
}