<?php

class SM_Checkout_Helper_Data extends Mage_Checkout_Helper_Data
{
    var $_showItemGroupCart = array();

    public function showItemGroupCart($id = '', $val = '')
    {
        if($id != null && $val != null) {
            $this->_showItemGroupCart[$id] = $val;
        }
        if($id != null) {
            return $this->_showItemGroupCart[$id];
        }
        return $this->_showItemGroupCart;
    }

    /*return price grouped product*/
    public function getPriceGroupedProductCustom($groupId = null, $qty = null, $format = true) {
        $return = $this->getPriceGroupedProduct($groupId, $qty, $format);
        return $return['display_price'];
    }

    public function getSpecialPriceGroupedProduct($groupId = null, $qty = null, $format = true) {
        $return = $this->getPriceGroupedProduct($groupId, $qty, $format);
        return $return['old_price'];
    }

    public function getPriceGroupedProduct($groupId = null, $qty = null, $format = true) {
        $return = array();
        $priceDisplay  = $specialPrice = $msrpPrice = 0;
        if($groupId) {
            $groupPro    = Mage::getModel('catalog/product')->load($groupId);
            $_weeeHelper = Mage::helper('weee');
            $_taxHelper  = Mage::helper('tax');
            $_associatedProducts = $groupPro->getTypeInstance(true)->getAssociatedProducts($groupPro);
            foreach ($_associatedProducts as $item) {
                /*get old price*/
                $_price = ($item->getMsrp())?$item->getMsrp():$item->getPrice();
                $_price = ($_price>0)?$_price:1;
                $_simplePricesTax = ($_taxHelper->displayPriceIncludingTax() || $_taxHelper->displayBothPrices());
                $_regularPrice = $_taxHelper->getPrice($item, $_price, $_simplePricesTax);
                $_originalWeeeTaxAmount = $_weeeHelper->getOriginalAmount($item);
                $_oldPrice = $_regularPrice + $_originalWeeeTaxAmount;

                $priceDisplay += $item->getFinalPrice();
                $specialPrice += ($_oldPrice > 0) ? $_oldPrice : $item->getFinalPrice();
            }
            if($specialPrice == $priceDisplay) {$specialPrice = null;}
        }
        if($qty) {
            $priceDisplay *= $qty;
            $specialPrice *= $qty;
        }
        if($format) {
            $return['old_price']                = Mage::helper('checkout')->formatPrice($specialPrice);
            $return['display_price']            = Mage::helper('checkout')->formatPrice($priceDisplay);
        } else {
            $return['old_price']                = $specialPrice;
            $return['display_price']            = $priceDisplay;
        }
        return $return;
    }
    /*end return price grouped product*/

    /*return html for item review in Shopping Cart*/
    public function htmlItemGroupShoppingCart($groupPro = null, $qty = null, $storeName = null) {
        $html = $instock = '';
        if(!Mage::helper('sales')->checkInstockGroupCustom($groupPro->getId()) || !$groupPro->isSaleable()) {
            $instock = "<p class='item-msg error'>* ".$this->__('This product is currently out of stock.')."</p>";
        }
        if($groupPro && $qty) {
            $html = "
            <tr>
            <td>
                <a href='".$groupPro->getProductUrl()."' title='".$this->htmlEscape($groupPro->getName())."' class='product-image'>
                    <img src='".$groupPro->getThumbnailUrl()."' width='75' height='75' alt='".$this->htmlEscape($groupPro->getName())."' />
                </a>
            </td>
            <td>
                <h2 class='product-name'>
                    <a href='".$groupPro->getProductUrl()."'>".$this->htmlEscape($groupPro->getName())."</a>
                </h2>
                ".$instock."
                <!-- outofstock -->
                <span class='alertTxdelivery-arrow' style='display:none;'></span><span class='alertTxdelivery' style='display:none;'>Item will take longer than standard time for delivery.</span><br>
                <span class='alertTxdelivery-removetx' style='display:none;'>If you don’t need it. <a href=''>Please click here to remove it.</a></span>
                <!-- end outofstock -->
            </td>
            <td class='unit-price'>
                <span class='cart-price'><span class='price'>".$this->getPriceGroupedProductCustom($groupPro->getId())."</span></span>
            </td>
            <td class='cart-qty a-center'>
                <div class='numbers-row'>
                    <input type='text' maxlength='12' value='".$qty."' title='".$this->__('Qty')."' class='input-text qty grouped' idGroupPro='".$groupPro->getId()."' size='4'>
                </div>
            </td>
            <td class='subtotal-price total_group_pro'>
                <span class='cart-price'>
                    <span>".$this->getPriceGroupedProductCustom($groupPro->getId(), $qty)."</span>
                </span>
            </td>
            <td class='a-center remove-box last'><a title='".$this->__('Remove item')."' class='btn-remove btn-remove2 remove_grouped' href='".Mage::getBaseUrl().'checkout/cart/deletegroupedproduct/sku/'.$groupPro->getSku()."'>".$this->__('Remove item')."</a></td>
        </tr>";
        }
        return $html;
    }

    /*return html for item review in mini cart*/
    public function htmlItemGroupMiniCart($groupPro = null, $qty = null) {
        $html = '';
        if($groupPro && $qty) {
            $html = "
             <li class='item'>
            <a href='".$groupPro->getProductUrl()."' title='".$this->htmlEscape($groupPro->getName())."' class='product-image'>
                <img src='".$groupPro->getThumbnailUrl()."' width='75' height='75' alt='".$this->htmlEscape($groupPro->getName())."'>
            </a>
            <div class='product-details'>
                <a href='".Mage::getBaseUrl().'checkout/cart/deletegroupedproduct/sku/'.$groupPro->getSku()."' title='".$this->__('Remove This Item')."' onclick='return confirm(".$this->__('Are you sure you would like to remove this item from the shopping cart?').");'
                   class='btn-remove'>".$this->__('Remove This Item')."
                </a>
                <p class='product-name'>
                    <a href='".$groupPro->getProductUrl()."'>".$this->htmlEscape($groupPro->getName())."</a>
                </p>
                <span class='top-ajax-qty'>".$this->__('Quantity')."</span>
                <span class='top-ajax-qty-value'>".$qty."</span>
                <span class='top-ajax-price'>".$this->__('Price / Item')."</span>
                <span class='top-ajax-price-value'>
                    <span class='price'>
                        <span>".$this->getPriceGroupedProductCustom($groupPro->getId(), $qty)."</span>
                    </span>
                </span>
            </div>
        </li>";
        }
        return $html;
    }

    /*return html for item review in onestepcheckout*/
    public function htmlItemGroupReviewCheckout($groupPro = null, $qty = null) {
        $html = '';
        if($groupPro && $qty) {
            $html = "
        <tr>
            <td colspan='4' style='background:white;border-bottom:1px dotted #d5d5d5 !important;' class='last'>
                <h3 class='product-name'>".$this->htmlEscape($groupPro->getName())."</h3>
                <div class='checkout-review-item'>
                    <div class='w50item' style='width: 90px; text-align: left;'>
                        <span class='cart-price'><span style='font:normal 12px 'Arial' #000;'>Price&nbsp;</span>
                            <span class='price'>".$this->getPriceGroupedProductCustom($groupPro->getId())."</span>
                        </span>
                    </div>
                    <div class='w50itemmid' style='width: 80px; text-align: center;'>
                        <span class='cart-price'>
                            <span style='font:normal 12px 'Arial' #000;'>Qty.&nbsp;</span>
                            <span class='price'>".$qty."</span>
                        </span>
                    </div>
                    <div class='w50itemright' style='width: 110px; text-align: right;'>
                        <span class='cart-price'>
                            <span style='font:normal 12px 'Arial' #000;'>Subtotal&nbsp;</span>
                            <span class='price'>".$this->getPriceGroupedProductCustom($groupPro->getId(), $qty)."</span>
                        </span>
                    </div>
                </div>
            </td>
        </tr>";
        }
        return $html;
    }

    /*return html for item review in onestepcheckout inMobile*/
    public function htmlItemGroupReviewCheckoutMobile($groupPro = null, $qty = null) {
        $html = '';
        if($groupPro && $qty) {
            $html = "
        <tr>
            <td colspan='4' style='background:white;border-bottom:1px dotted #d5d5d5 !important;' class='last'>
                <h3 class='product-name'>".$this->htmlEscape($groupPro->getName())."</h3>
                <div class='checkout-review-item'>
                    <div class='w50item'>
                        <span class='cart-price'><span style='font:normal 12px 'Arial' #000;'>Price&nbsp;</span>
                            <span class='price'>".$this->getPriceGroupedProductCustom($groupPro->getId())."</span>
                        </span>
                    </div>
                    <div class='w50itemmid'>
                        <span class='cart-price'>
                            <span>Qty.&nbsp;</span>
                            <span class='price'>".$qty."</span>
                        </span>
                    </div>
                    <div class='w50itemright'>
                        <span class='cart-price'>
                            <span>Subtotal&nbsp;</span>
                            <span class='price'>".$this->getPriceGroupedProductCustom($groupPro->getId(), $qty)."</span>
                        </span>
                    </div>
                </div>
            </td>
        </tr>";
        }
        return $html;
    }

    /*return html for item review in Shopping Cart in Mobile*/
    public function htmlItemGroupShoppingCartMobile($groupPro = null, $qty = null, $storeName = null) {
        $html = $instock = '';
        if(!Mage::helper('sales')->checkInstockGroupCustom($groupPro->getId()) || !$groupPro->isSaleable()) {
            $instock = "<p class='item-msg error'>* ".$this->__('This product is currently out of stock.')."</p>";
        }
        if($groupPro && $qty) {
            $html = "
                <div class='shopping-cart-items grouped'>
                    <div class='shopping-cart-item'>
                        <div class='left-col'>
                            <a href='".$groupPro->getProductUrl()."'>
                                <img src='".$groupPro->getThumbnailUrl()."' alt='".$this->htmlEscape($groupPro->getName())."'>
                            </a>
                            <div class='action-delete'>
                                <a href='".Mage::getBaseUrl().'checkout/cart/deletegroupedproduct/sku/'.$groupPro->getSku()."' title='".$this->__('Delete item')."'>".$this->__('Delete item')."</a>
                            </div>
                        </div><!--end .left-col-->
                        <div class='right-col'>
                            <h2 class='product-name'>
                                <a href='".$groupPro->getProductUrl()."'>".$this->htmlEscape($groupPro->getName())."</a>
                            </h2><!--end .product-name-->
                            ".$instock."
                            <!--------------------------------------.product-price---------------------------------------------->
                            <div class='product-price'>
                                <span class='product-price-title'>".$this->__('Unit Price').": </span><!-- end .product-price-title-->

                                <span class='cart-price'><span class='price'>".$this->getPriceGroupedProductCustom($groupPro->getId())."</span></span>

                             </div><!--end .product-price-->
                            <!--------------------------------------end .product-price---------------------------------------------->
                            <div class='product-subtotal'>
                                <span class='product-subtotal-title'>".$this->__('Subtotal').": </span><!-- end .product-subtotal-title-->

                                <span class='cart-price'>
                                    <span class='price'>".$this->getPriceGroupedProductCustom($groupPro->getId(), $qty)."</span>
                                </span>

                            </div><!-- end .product-subtotal-->
                            <!--------------------------------------end .product-subtotal---------------------------------------------->
                            <div class='product-qty'>
                                <div class='numbers-row'>
                                    <div class='qty-button dec cart-qty-btn' id='dec-qty'>-</div>
                                    <input type='text' maxlength='12' value='".$qty."' title='".$this->__('Qty')."' class='input-text qty grouped' idGroupPro='".$groupPro->getId()."' size='4'>
                                    <div class='qty-button inc cart-qty-btn' id='inc-qty'>+</div>
                                <div class='qty-button inc' id='inc-qty'>+</div><div class='qty-button dec' id='dec-qty'>-</div></div>
                            </div><!--end .product-qty-->
                            <!--------------------------------------end .product-subtotal---------------------------------------------->
                        </div><!--end .right-col-->
                    </div><!--end .shopping-cart-item-->
                </div>
                ";
        }
        return $html;
    }
}
	 