<?php

class SM_Checkout_Block_Checkout_Cart extends Mage_Checkout_Block_Cart
{
    public function chooseTemplate()
    {
        $this->setTemplate('sm_checkout/cart.phtml');
    }

    public function checkIfHasItems()
    {
        $itemsCount = $this->getItemsCount() ? $this->getItemsCount() : $this->getQuote()->getItemsCount();
        return $itemsCount ? true : false;
    }

    public function checkDropshipOrSpecial()
    {
        /** @var Mage_Sales_Model_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        /** @var Mage_Sales_Model_Quote_Item $items */
        $items = $quote->getAllVisibleItems();
        // Product ids array: $pIds
        $pIds = array();
        foreach ($items as $item) {
            /** @var Mage_Sales_Model_Quote_Item $item */
            $pIds[] = $item->getProductId();
        }
        $totalItems = $quote->getItemsCount();
        if ($totalItems > 1) {
            $count = 0;
            $collection = Mage::getResourceModel('catalog/product_collection')
                ->addFieldToFilter('entity_id', $pIds)
                ->joinField('qty',
                    'cataloginventory/stock_item',
                    'qty',
                    'product_id=entity_id',
                    '{{table}}.stock_id=1',
                    'left')
                ->addAttributeToSelect('*');
            foreach ($collection as $product) {
                /** @var Mage_Catalog_Model_Product $product */
                if (($product->getAttributeText('shipping_duration') == 'Special Order Product' && $product->getData('qty') == 0) ||
                    ($product->getAttributeText('shipping_duration') == 'Dropship Product')) {
                    $count++;
                }
            }
            if ($count > 0 && $count <= $totalItems) {
                return true;
            }
        }
        return false;
    }

    /**
     * Check if a quote is wholesale
     *
     * @return boolean
     */
    public function isWholesale()
    {
        $quote = Mage::getSingleton('checkout/cart')->getQuote(); 
        if ($quote->getIsWholesale() == 1) {
            return true;
        }
        return false;
    }
}