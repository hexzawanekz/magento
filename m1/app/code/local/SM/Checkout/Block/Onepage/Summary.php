<?php
/**
 * @category   Werd
 * @package    Werd_Checkout
 */

/**
 * Onepage Checkout Payment Order Summary Block
 */
class SM_Checkout_Block_Onepage_Summary extends Mage_Sales_Block_Items_Abstract
{
    /**
     * Returns shopping cart items
     *
     * @return array
     */
    public function getItems()
    {
        return Mage::getSingleton('checkout/session')->getQuote()->getAllVisibleItems();
    }

    /**
     * Returns Cart Totals
     *
     * @return array
     */
    public function getTotals()
    {
        return Mage::getSingleton('checkout/session')->getQuote()->getTotals();
    }

    /**
     * Get shopping cart items qty based on configuration (summary qty or items qty)
     *
     * @return int | float
     */
    public function getSummaryCount()
    {
        if ($this->getData('summary_qty')) {
            return $this->getData('summary_qty');
        }

        return Mage::getSingleton('checkout/cart')->getSummaryQty();
    }
}
