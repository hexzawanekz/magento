<?php
require_once 'Mage/Checkout/controllers/CartController.php';

class SM_Checkout_CartController extends Mage_Checkout_CartController
{
    /**
     * Update shopping cart data action
     */
    public function updatePostAction()
    {
        if(Mage::helper('core')->isModuleEnabled('SM_NetsuiteInventorySync')){
            if($param = $this->getRequest()->getParam('cart')){
                if(count($param) == 1){
                    $quote_id = 0;
                    $qty_ordered = 0;
                    foreach($param as $qid => $qty){
                        $quote_id = $qid;
                        $qty_ordered = $qty['qty'];
                    }

                    /** @var Mage_Sales_Model_Quote_Item $quote_obj */
                    $quote_obj = Mage::getModel('sales/quote_item')->load($quote_id);
                    if($quote_obj->getId()){
                        /** @var Mage_Catalog_Model_Product $product */
                        $product = Mage::getModel('catalog/product')
                            ->setStoreId(Mage::app()->getStore()->getId())
                            ->load($quote_obj->getProductId());

                        if($product->getId()){
                            // check available stock for bundle here
                            $check_result = Mage::getSingleton('netsuiteinventorysync/stockBack')->checkAvailableStock($product, $qty_ordered, 'update');
                            if($check_result !== true){
                                $this->_getSession()->addError($this->__('Some of the requested products are not available in the desired quantity.'));
                                $this->_goBack();
                                return;
                            }
                        }
                    }
                }
            }
        }

        parent::updatePostAction();
    }

    /**
     * Delete Grouped product in shoping cart
     */
    public function deleteGroupedProductAction()
    {

        $skuGrouped = $this->getRequest()->getParam('sku');
        if ($skuGrouped) {
            try {
                $items = Mage::getSingleton('checkout/session')->getQuote();
                $cartHelper = Mage::helper('checkout/cart');
                foreach ($items->getAllItems() as $item) {
                    foreach ($item->getOptions() as $option) {
                        if (strtolower($option->getCode()) == 'additional_options' && $item->getId() != null) {
                            $unserialized = unserialize($option->getValue());
                            $labelOpt = strtolower($unserialized['options']['label']);
                            $valueOpt = $unserialized['options']['value'];
                            if($labelOpt == 'groupedsku' && $valueOpt == $skuGrouped) {
                                $itemId = $item->getItemId();
                                $cartHelper->getCart()->removeItem($itemId)->save();
                            }
                            break;
                        }
                    }
                }
                Mage::getModel('checkout/cart')->getQuote()->setTotalsCollectedFlag(false)->collectTotals()->save();
            } catch (Exception $e) {
                $this->_getSession()->addError($this->__('Cannot remove the item.'));
                Mage::logException($e);
            }
        }
        $this->_redirectReferer(Mage::getUrl('*/*'));
    }
}