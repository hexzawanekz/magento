<?php

require_once 'Mage/Checkout/controllers/OnepageController.php';

class SM_Checkout_OnepageController extends Mage_Checkout_OnepageController
{
    public function saveBillingAction()
    {
        if ($this->_expireAjax()) {
            return;
        }
        if ($this->getRequest()->isPost()) {
//            $postData = $this->getRequest()->getPost('billing', array());
//            $data = $this->_filterPostData($postData);
            $data = $this->getRequest()->getPost('billing', array());
            $customerAddressId = $this->getRequest()->getPost('billing_address_id', false);

            if (isset($data['email'])) {
                $data['email'] = trim($data['email']);
                $this->getOnepage()->saveCheckoutMethod('register');
            }
            $result = $this->getOnepage()->saveBilling($data, $customerAddressId);
            if (!isset($result['error'])) {
                /* check quote for virtual */
                if ($this->getOnepage()->getQuote()->isVirtual()) {
                    $result['goto_section'] = 'payment';
                    $result['update_section'] = array(
                        'name' => 'payment-method',
                        'html' => $this->_getPaymentMethodsHtml()
                    );
                } elseif (isset($data['use_for_shipping']) && $data['use_for_shipping'] == 1) {
                    $result['goto_section'] = 'payment';
                    $result['update_section'] = array(
                        'name' => 'payment-method',
                        'html' => $this->_getPaymentMethodsHtml()
                    );
                    $result['update_section2'] = array(
                        'name' => 'shipping-method',
                        'html' => $this->_getShippingMethodsHtml()
                    );

                    $result['allow_sections'] = array('payment');
                    $result['duplicateBillingInfo'] = 'true';
                    $result['summary'] = $this->_getSummaryHtml();
                } else {
                    $result['goto_section'] = 'payment';
                    $result['update_section'] = array(
                        'name' => 'payment-method',
                        'html' => $this->_getPaymentMethodsHtml()
                    );

                    $result['allow_sections'] = array('payment');
                    $result['duplicateBillingInfo'] = 'true';

                }
            }
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
        }
    }

    public function saveShippingAction()
    {
        if ($this->_expireAjax()) {
            return;
        }
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getPost('shipping', array());
            if($data['same_as_billing']){
                $customerAddressId = Mage::getSingleton('checkout/session')->getQuote()->getBillingAddress()->getData('customer_address_id');
            }else{
                $customerAddressId = $this->getRequest()->getPost('shipping_address_id', false);
            }
            $result = $this->getOnepage()->saveShipping($data, $customerAddressId);
            if (!isset($result['error'])) {
                $result['update_section2'] = array(
                    'name' => 'shipping-method',
                    'html' => $this->_getShippingMethodsHtml()
                );
                $result['summary'] = array(
                    'html' => $this->_getSummaryHtml()
                );
            }
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
        }
    }

    public function saveShippingMethodAction()
    {
        if ($this->_expireAjax()) {
            return;
        }
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getPost('shipping_method', '');
            $result = $this->getOnepage()->saveShippingMethod($data);
            /*
            $result will have erro data if shipping method is empty
            */
            if (!$result) {
                Mage::dispatchEvent('checkout_controller_onepage_save_shipping_method',
                    array('request' => $this->getRequest(),
                        'quote' => $this->getOnepage()->getQuote()));
                $this->getOnepage()->getQuote()->collectTotals();
                $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
            }
            $this->getOnepage()->getQuote()->collectTotals()->save();
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
        }
    }

    public function couponPostAction()
    {
        if ($this->_expireAjax()) {
            return;
        }
        $data = array();
        $cart = Mage::getSingleton('checkout/cart')->getQuote();
        foreach ($cart->getAllItems() as $item) {
            $buyInfo = $item->getBuyRequest();
            $custom = $buyInfo->getIndiesRecurringandrentalpaymentsSubscriptionType();
            if($custom && $custom > 0){
                $data['message'] = $this->__('Cannot use coupon with Autoship order.');
                $totalBlock = $this->getLayout()->createBlock('checkout/cart_totals')->setTemplate('checkout/onepage/summary/totals.phtml');
                $data['html'] = $totalBlock->toHtml();
                return $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($data));
            }
        }

        $couponCode = (string)$this->getRequest()->getParam('coupon_code');
        if ($this->getRequest()->getParam('remove') == 1) {
            $couponCode = '';
        }
        try {
            Mage::getSingleton('checkout/session')->getQuote()->getShippingAddress()->setCollectShippingRates(true);
            Mage::getSingleton('checkout/session')->getQuote()->setCouponCode(strlen($couponCode) ? $couponCode : '')
                ->collectTotals()
                ->save();

            if (strlen($couponCode)) {
                if ($couponCode == Mage::getSingleton('checkout/session')->getQuote()->getCouponCode()) {
                    $data['message'] = $this->__('Coupon code "%s" was applied.', Mage::helper('core')->htmlEscape($couponCode));
                } else {
                    /** @var Mage_SalesRule_Model_Coupon $oCoupon */
                    $oCoupon = Mage::getModel('salesrule/coupon')->load($couponCode, 'code');
                    /** @var Mage_SalesRule_Model_Rule $oRule */
                    $oRule = Mage::getModel('salesrule/rule')->load($oCoupon->getRuleId());
                    $desc = $oRule->getDescription();

                    /**
                     * Need to add "LINE Pay" into shopping cart rule description to validate
                     */
                    if(strpos('1'.$desc, 'LINE Pay') > 0){
                        $data['message'] = $this->__('Please apply this coupon after choosing correct payment method.');
                    }else{
                        $data['message'] = $this->__('Coupon code "%s" is not valid.', Mage::helper('core')->htmlEscape($couponCode));
                    }
                }
            } else {
                $data['message'] = $this->__('Coupon code was canceled.');
            }

        } catch (Mage_Core_Exception $e) {
            $data['error'] = $e->getMessage();
        } catch (Exception $e) {
            $data['error'] = $this->__('Cannot apply the coupon code.');
            Mage::logException($e);
        }
        $totalBlock = $this->getLayout()->createBlock('checkout/cart_totals')->setTemplate('checkout/onepage/summary/totals.phtml');
        $data['html'] = $totalBlock->toHtml();

        return $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($data));
    }


    protected function _getSummaryHtml()
    {
        $layout = $this->getLayout();
        $update = $layout->getUpdate();
        $update->setCacheId(uniqid("custom_checkout_onepage_summary"));
        $update->load('checkout_onepage_summary');
        $layout->generateXml();
        $layout->generateBlocks();
        $output = $layout->getOutput();
        return $output;
    }

    /**
     * Create order action
     */
    public function saveOrderAction()
    {
//        if (!$this->_validateFormKey()) {
//            return $this->_redirect('*/*');
//        }

        if ($this->_expireAjax()) {
            return;
        }

        $result = array();
        try {
            $requiredAgreements = Mage::helper('checkout')->getRequiredAgreementIds();
            if ($requiredAgreements) {
                $postedAgreements = array_keys($this->getRequest()->getPost('agreement', array()));
                $diff = array_diff($requiredAgreements, $postedAgreements);
                if ($diff) {
                    $result['success'] = false;
                    $result['error'] = true;
                    $result['error_messages'] = $this->__('Please agree to all the terms and conditions before placing the order.');
                    $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
                    return;
                }
            }
            $quote = Mage::getSingleton('checkout/cart')->getQuote();
            $paymentMethod = $quote->getPayment();
            $items = $quote->getAllVisibleItems();
            $isWholesale = false;
            foreach($items as $item){
                $product = $item->getProduct();
                $stock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product);
                $cartAllowed = $stock->getData('max_sale_qty');
                $orderQuantity = $item->getQty();
                if($cartAllowed != 0 && $orderQuantity > $cartAllowed && $paymentMethod->getMethod() == "cashondelivery"){
                    $isWholesale = true;
                    $quote->setIsWholesale(1);
                    $quote->save();
                    $result['success'] = false;
                    $result['error'] = true;
                    $result['error_messages'] = $this->__('Your order contain wholesales item so that you can not use Cash On Delivery');
                    $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
                    return;
                }
            }
            if($isWholesale == false){
                $quote->setIsWholesale(0);
                $quote->save();
            }
            if ($data = $this->getRequest()->getPost('payment', false)) {
                $this->getOnepage()->getQuote()->getPayment()->importData($data);
            }
            $this->getOnepage()->saveOrder();

            $redirectUrl = $this->getOnepage()->getCheckout()->getRedirectUrl();
            $result['success'] = true;
            $result['error']   = false;
        } catch (Mage_Payment_Model_Info_Exception $e) {
            $message = $e->getMessage();
            if ( !empty($message) ) {
                $result['error_messages'] = $message;
            }
            $result['goto_section'] = 'payment';
            $result['update_section'] = array(
                'name' => 'payment-method',
                'html' => $this->_getPaymentMethodsHtml()
            );
        } catch (Mage_Core_Exception $e) {
            Mage::logException($e);
            Mage::helper('checkout')->sendPaymentFailedEmail($this->getOnepage()->getQuote(), $e->getMessage());
            $result['success'] = false;
            $result['error'] = true;
            $result['error_messages'] = $e->getMessage();

            $gotoSection = $this->getOnepage()->getCheckout()->getGotoSection();
            if ($gotoSection) {
                $result['goto_section'] = $gotoSection;
                $this->getOnepage()->getCheckout()->setGotoSection(null);
            }
            $updateSection = $this->getOnepage()->getCheckout()->getUpdateSection();
            if ($updateSection) {
                if (isset($this->_sectionUpdateFunctions[$updateSection])) {
                    $updateSectionFunction = $this->_sectionUpdateFunctions[$updateSection];
                    $result['update_section'] = array(
                        'name' => $updateSection,
                        'html' => $this->$updateSectionFunction()
                    );
                }
                $this->getOnepage()->getCheckout()->setUpdateSection(null);
            }
        } catch (Exception $e) {
            Mage::logException($e);
            Mage::helper('checkout')->sendPaymentFailedEmail($this->getOnepage()->getQuote(), $e->getMessage());
            $result['success']  = false;
            $result['error']    = true;
            $result['error_messages'] = $this->__('There was an error processing your order. Please contact us or try again later.');
        }
        $this->getOnepage()->getQuote()->save();
        /**
         * when there is redirect to third party, we don't want to save order yet.
         * we will save the order in return action.
         */
        if (isset($redirectUrl)) {
            $result['redirect'] = $redirectUrl;
        }

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }
}
