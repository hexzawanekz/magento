<?php

/** @var Mage_Core_Model_Resource_Setup $installer */
$installer = $this;


$installer->startSetup();

$moxyCategoryId = Mage::getModel('catalog/category')
    ->getCollection()
    ->addAttributeToFilter('name', 'Moxy')
    ->getFirstItem()
    ->getId();

$moxyCategory = Mage::getModel('catalog/category')->load($moxyCategoryId);

//Create Welcome Splash Category
$welcomeCategory = Mage::getModel('catalog/category');
try {
    $welcomeCategory->setName("Welcome Splash");
    $welcomeCategory->setUrlKey('welcome-splash');
    $welcomeCategory->setIsActive(1);
    $welcomeCategory->setDisplayMode('PRODUCTS');
    $welcomeCategory->setIsAnchor(0);
    $welcomeCategory->setPath($moxyCategory->getPath());
    $welcomeCategory->save();
} catch ( Exception $e ) {
    Mage::log($e->getMessage(), null, 'welcomesplash.log');
}
$welcomeCategory = Mage::getModel('catalog/category')->load($welcomeCategory->getId());

try {
    //Adding new child categories
    $whatsHotCategory = Mage::getModel('catalog/category');
    $whatsHotCategory->setName("What's Hot");
    $whatsHotCategory->setUrlKey('whats-hot');
    $whatsHotCategory->setIsActive(1);
    $whatsHotCategory->setDisplayMode('PRODUCTS');
    $whatsHotCategory->setIsAnchor(0);
    $whatsHotCategory->setPath($welcomeCategory->getPath());

    $whatsHotCategory->save();
} catch ( Exception $e ) {
    Mage::log($e->getMessage(), null, 'welcomesplash.log');
}
Mage::getModel('core/config')->saveConfig('welcome_splash/product_list/what_hot_category_id', $whatsHotCategory->getId(), 'default', 0);

try {
    $newArrivals = Mage::getModel('catalog/category');
    $newArrivals->setName("New Arrivals");
    $newArrivals->setUrlKey('new-arrivals');
    $newArrivals->setIsActive(1);
    $newArrivals->setDisplayMode('PRODUCTS');
    $newArrivals->setIsAnchor(0);
    $newArrivals->setPath($welcomeCategory->getPath());

    $newArrivals->save();
} catch ( Exception $e ) {
    Mage::log($e->getMessage(), null, 'welcomesplash.log');
}
Mage::getModel('core/config')->saveConfig('welcome_splash/product_list/new_arrival_category_id', $newArrivals->getId(), 'default', 0);



$installer->endSetup();

