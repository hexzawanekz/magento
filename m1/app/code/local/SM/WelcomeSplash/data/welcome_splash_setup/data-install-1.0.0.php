<?php

/** @var Mage_Core_Model_Resource_Setup $install */
$install = $this;

$install->startSetup();

$engStores = array();
$thStores = array();
$allStores = array(0);

$coreStoreModel = Mage::getModel('core/store');

//Initial Store List
$engStores[] = $coreStoreModel->load('en', 'code')->getId();
$engStores[] = $coreStoreModel->load('len', 'code')->getId();
$engStores[] = $coreStoreModel->load('ven', 'code')->getId();
$engStores[] = $coreStoreModel->load('sen', 'code')->getId();
$engStores[] = $coreStoreModel->load('moxyen', 'code')->getId();

$thStores[] = $coreStoreModel->load('th', 'code')->getId();
$thStores[] = $coreStoreModel->load('lth', 'code')->getId();
$thStores[] = $coreStoreModel->load('vth', 'code')->getId();
$thStores[] = $coreStoreModel->load('sth', 'code')->getId();
$thStores[] = $coreStoreModel->load('moxyth', 'code')->getId();


//Welcome splash moxy logo
/** @var Mage_Cms_Model_Block $coreBlockModel */
$coreBlockModel = Mage::getModel('cms/block');
$coreStoreModel->setTitle("Welcome splash moxy logo");
$coreBlockModel->setIdentifier('welcome-main-logo');
$coreBlockModel->setStores($allStores);
$htmlContent = <<<HTML
<a href="{{config path="web/unsecure/base_url"}}"><p><img src="{{media url="wysiwyg/welcomePage/logo.png"}}" alt="Moxy" /></p></a>
HTML;

$coreBlockModel->setContent($htmlContent);
$coreBlockModel->save();


//Welcome splash navigation ENG
/** @var Mage_Cms_Model_Block $coreBlockModel */
$coreBlockModel = Mage::getModel('cms/block');
$coreStoreModel->setTitle('Welcome splash navigation ENG');
$coreBlockModel->setIdentifier('welcome-main-nav');
$coreBlockModel->setStores($engStores);
$htmlContent = <<<HTML
<ul class="nav">
<li class="active"><a>welcome</a></li>
<li class="beauty-nav-item"><a href="http://lafema.com/en/">beauty</a></li>
<li class="fashion-nav-item"><a href="http://moxy.co.th/en/moxy/fashion">fashion</a></li>
<li class="living-nav-item"><a href="http://moxy.co.th/en/moxy/homeliving">living</a></li>
<li class="elec-nav-item"><a href="http://moxy.co.th/en/moxy/electronics">electronics</a></li>
<li class="baby-nav-item"><a href="http://venbi.com/en/">mom &amp; baby</a></li>
<li class="heath-nav-item"><a href="http://sanoga.com/en/">health</a></li>
<li class="pet-nav-item"><a href="http://petloft.com/en/">pets</a></li>
</ul>
HTML;
$coreBlockModel->setContent($htmlContent);
$coreBlockModel->setIsActive(1);
$coreBlockModel->save();

//Welcome splash navigation THAI
/** @var Mage_Cms_Model_Block $coreBlockModel */
$coreBlockModel = Mage::getModel('cms/block');
$coreStoreModel->setTitle('Welcome splash navigation THAI');
$coreBlockModel->setIdentifier('welcome-main-nav');
$coreBlockModel->setStores($thStores);
$htmlContent = <<<HTML
<ul class="nav">
<li class="active"><a>หน้าแรก</a></li>
<li class="beauty-nav-item"><a href="http://lafema.com/th/">ความงาม</a></li>
<li class="fashion-nav-item"><a href="http://moxy.co.th/th/moxy/fashion">แฟชั่น</a></li>
<li class="living-nav-item"><a href="http://moxy.co.th/th/moxy/homeliving">ของแต่งบ้าน</a></li>
<li class="elec-nav-item"><a href="http://moxy.co.th/th/moxy/electronics">อิเล็กทรอนิกส์</a></li>
<li class="baby-nav-item"><a href="http://venbi.com/th/">แม่และเด็ก</a></li>
<li class="heath-nav-item"><a href="http://sanoga.com/th/">สุขภาพ</a></li>
<li class="pet-nav-item"><a href="http://petloft.com/th/">สัตว์เลี้ยง</a></li>
</ul>
HTML;
$coreBlockModel->setContent($htmlContent);
$coreBlockModel->setIsActive(1);
$coreBlockModel->save();

//Welcome splash shortcut link
/** @var Mage_Cms_Model_Block $coreBlockModel */
$coreBlockModel = Mage::getModel('cms/block');
$coreBlockModel->load('welcome-shortcut', 'identifier');
$coreStoreModel->setTitle('Welcome splash shortcut link');
$coreBlockModel->setIdentifier('welcome-shortcut');
$coreBlockModel->setStores($allStores);
$htmlContent = <<<HTML
<div class="col-sm-6">
<div class="row">
<div class="col-sm-12">
<div class="img-banner"><a href="http://stage.moxyst.com/en/moxy/electronics"><img onmouseover="this.src='{{media url="wysiwyg/welcomePage/electronics_hover_2.jpg"}}';" onmouseout="this.src='{{media url="wysiwyg/welcomePage/electronics_2.jpg"}}';" src="{{media url="wysiwyg/welcomePage/electronics_2.jpg"}}" alt="Electronics" /></a></div>
</div>
<div class="col-xs-6">
<div class="img-banner"><a href="http://stage.moxyst.com/en/moxy/fashion"><img onmouseover="this.src='{{media url="wysiwyg/welcomePage/fashion_hover_1.jpg"}}';" onmouseout="this.src='{{media url="wysiwyg/welcomePage/fashion_1.jpg"}}';" src="{{media url="wysiwyg/welcomePage/fashion_1.jpg"}}" alt="Fashion" /></a></div>
</div>
<div class="col-xs-6">
<div class="img-banner"><a href="http://stage.sanoga.com/en/"><img onmouseover="this.src='{{media url="wysiwyg/welcomePage/health_hover_1.jpg"}}';" onmouseout="this.src='{{media url="wysiwyg/welcomePage/health_2.jpg"}}';" src="{{media url="wysiwyg/welcomePage/health_2.jpg"}}" alt="HEALTH" /></a></div>
</div>
</div>
</div>
<div class="col-sm-6">
<div class="row">
<div class="col-xs-6">
<div class="img-banner"><a href="http://stage.lafema.com/"><img onmouseover="this.src='{{media url="wysiwyg/welcomePage/beauty_hover_1.jpg"}}';" onmouseout="this.src='{{media url="wysiwyg/welcomePage/beauty_1.jpg"}}';" src="{{media url="wysiwyg/welcomePage/beauty_1.jpg"}}" alt="BEAUTY" /></a></div>
</div>
<div class="col-xs-6">
<div class="img-banner"><a href="http://stage.moxyst.com/en/moxy/homeliving"><img onmouseover="this.src='{{media url="wysiwyg/welcomePage/living_hover_1.jpg"}}';" onmouseout="this.src='{{media url="wysiwyg/welcomePage/living_1.jpg"}}';" src="{{media url="wysiwyg/welcomePage/living_1.jpg"}}" alt="LIVING" /></a></div>
</div>
<div class="col-xs-6">
<div class="img-banner"><a href="http://stage.venbi.com/"><img onmouseover="this.src='{{media url="wysiwyg/welcomePage/mombaby_hover_1.jpg"}}';" onmouseout="this.src='{{media url="wysiwyg/welcomePage/mombaby_1.jpg"}}';" src="{{media url="wysiwyg/welcomePage/mombaby_1.jpg"}}" alt="MOM &amp; BABY" /></a></div>
</div>
<div class="col-xs-6">
<div class="img-banner"><a href="http://stage.petloft.com/"><img onmouseover="this.src='{{media url="wysiwyg/welcomePage/pets_hover_1.jpg"}}';" onmouseout="this.src='{{media url="wysiwyg/welcomePage/pets_1.jpg"}}';" src="{{media url="wysiwyg/welcomePage/pets_1.jpg"}}" alt="PETS" /></a></div>
</div>
</div>
</div>
HTML;
$coreBlockModel->setContent($htmlContent);
$coreBlockModel->save();

//Welcome splash promotion banner
/** @var Mage_Cms_Model_Block $coreBlockModel */
$coreBlockModel = Mage::getModel('cms/block');
$coreStoreModel->setTitle('Welcome splash promotion banner');
$coreBlockModel->setIdentifier('welcome-promotion-banner');
$coreBlockModel->setStores($allStores);
$htmlContent = <<<HTML
<div class="banner"><a href="http://www.moxy.co.th" target="_self"><img title="PROMOTION TITLE" onmouseover="this.src='{{media url="wysiwyg/welcomePage/SplashPage_moxy_Aug17_promobanner-celebrate_hover.jpg"}}';" onmouseout="this.src='{{media url="wysiwyg/welcomePage/SplashPage_moxy_Aug17_promobanner-celebrate.jpg"}}';" src="{{media url="wysiwyg/welcomePage/SplashPage_moxy_Aug17_promobanner-celebrate.jpg"}}" alt="PROMOTION TITLE" /></a></div>
HTML;
$coreBlockModel->setContent($htmlContent);
$coreBlockModel->save();

//Welcome splash footer THAI
/** @var Mage_Cms_Model_Block $coreBlockModel */
$coreBlockModel = Mage::getModel('cms/block');
$coreStoreModel->setData('title', 'Welcome splash footer THAI');
$coreBlockModel->setIdentifier('welcome-footer');
$coreBlockModel->setStores($thStores);
$htmlContent = <<<HTML
<div class="footer-mobile hidden-md hidden-lg"><a href="http://moxy.co.th/"><img src="{{media url="wysiwyg/welcomePage/logo-mobile.png"}}" alt="2015, Moxy Co.Ltd, All right reserved" /></a>
<div class="txt-reserved"><em class="fa fa-copyright"></em>2015, Moxy Co.Ltd, All right reserved</div>
</div>
<div class="container hidden-xs hidden-sm" style="padding: 0 5px 15px;">
<div class="footer-box">
<h3 class="footer-title">ข้อมูล</h3>
<ul class="footer_links">
<li><a href="{{config path="web/unsecure/base_url "}}about-us/">เกี่ยวกับเรา</a></li>
<li><a href="{{config path="web/unsecure/base_url "}}contacts/">ติดต่อเรา</a></li>
</ul>
</div>
<div class="footer-box">
<h3 class="footer-title">นโยบายความเป็นส่วนตัว</h3>
<ul class="footer_links">
<li><a href="{{config path="web/unsecure/base_url "}}terms-and-conditions/">ข้อกำหนดและเงื่อนไข</a></li>
<li><a href="{{config path="web/unsecure/base_url "}}privacy-policy/">ความเป็นส่วนตัว</a></li>
</ul>
</div>
<div class="footer-box" style="width: 300px;">
<h3 class="footer-title">ช่วยเหลือ</h3>
<div class="col-md-6">
<ul class="footer_links">
<li><a href="{{config path="web/unsecure/base_url "}}help/">คำถามที่พบบ่อย</a></li>
<li><a href="{{config path="web/unsecure/base_url "}}help/#howtoorder">วิธีการสั่งซื้อ</a></li>
<li><a href="{{config path="web/unsecure/base_url "}}help/#payments">การชำระเงิน</a></li>
<li><a href="{{config path="web/unsecure/base_url "}}help/#rewardpoints">คะแนนสะสม</a></li>
</ul>
</div>
<div class="col-md-6">
<ul class="footer_links">
<li><a href="{{config path="web/unsecure/base_url "}}help/#shipping">การจัดส่งสินค้า</a></li>
<li><a href="{{config path="web/unsecure/base_url "}}help/#returns">การคืนสินค้าและขอคืนเงิน</a></li>
<li><a href="{{config path="web/unsecure/base_url "}}return-policy/">นโยบายการคืนสินค้า</a></li>
</ul>
</div>
</div>
<div class="footer-box">
<h3 class="footer-title">LINE CHAT</h3>
<ul class="footer_links">
<li><img style="width: 100px; height: 100px;" src="{{media url="wysiwyg/PETLOFT_QR.jpg "}}" alt="" /></li>
</ul>
</div>
<div class="footer-box" style="margin: 0px;">
<h3 class="footer-title">Connect</h3>
<div class="item_shares">
<p><a href="https://plus.google.com/+Moxyst" target="_blank"> <span class="google-footer"><img src="{{media url="wysiwyg/footer_follow/google_follow.png"}}" alt="" width="32px" /></span> </a> <a href="https://pinterest.com/#" target="_blank"> <span class="pinterest-footer"><img src="{{media url="wysiwyg/footer_follow/pinterest_follow.png"}}" alt="" width="32px" /></span> </a> <a href="https://webstagra.ms/moxyst/" target="_blank"> <span class="instagram-footer"><img src="{{media url="wysiwyg/footer_follow/instagram_follow.png"}}" alt="" width="32px" /></span> </a> <a href="https://twitter.com/moxyst" target="_blank"> <span class="twitter-footer"><img src="{{media url="wysiwyg/footer_follow/twitter_follow.png"}}" alt="" width="32px" /></span> </a> <a href="https://www.facebook.com/moxyst" target="_blank"> <span class="facebook-footer"><img src="{{media url="wysiwyg/footer_follow/facebook_follow.png"}}" alt="" width="32px" /></span> </a></p>
</div>
</div>
<div class="col-md-6 row">
<h3 class="footer-title">Payment Partners</h3>
<img src="{{media url="wysiwyg/welcomePage/bank_images_footer.png "}}" alt="" width="450px;" /></div>
<div class="col-md-6 row">
<h3 class="footer-title">Shipping Partners</h3>
<img src="{{media url="wysiwyg/acommerce_footer.png "}}" alt="" width="185px;" /></div>
</div>
<div class="copyright_box  hidden-xs hidden-sm">
<div class="container" style="padding: 0px 5px;">
<p class="copyright">&copy; 2015 Whatsnew Co. Ltd.</p>
</div>
</div>
HTML;
$coreBlockModel->setContent($htmlContent);
$coreBlockModel->save();

//Welcome splash footer EN
/** @var Mage_Cms_Model_Block $coreBlockModel */
$coreBlockModel = Mage::getModel('cms/block');
$coreStoreModel->setData('title', 'Welcome splash footer ENG');
$coreBlockModel->setIdentifier('welcome-footer');
$coreBlockModel->setStores($engStores);
$htmlContent = <<<HTML
<div class="footer-mobile hidden-md hidden-lg"><a href="http://moxy.co.th/"><img src="{{media url="wysiwyg/welcomePage/logo-mobile.png"}}" alt="2015, Moxy Co.Ltd, All right reserved" /></a>
<div class="txt-reserved"><em class="fa fa-copyright"></em>2015, Moxy Co.Ltd, All right reserved</div>
</div>
<div class="container hidden-xs hidden-sm" style="padding: 0 5px 15px;">
<div class="footer-box">
<h3 class="footer-title">Information</h3>
<ul class="footer_links">
<li><a href="{{config path="web/unsecure/base_url "}}about-us/">About Us</a></li>
<li><a href="{{config path="web/unsecure/base_url "}}contacts/">Contact Us</a></li>
</ul>
</div>
<div class="footer-box">
<h3 class="footer-title">Security and Privacy</h3>
<ul class="footer_links">
<li><a href="{{config path="web/unsecure/base_url "}}terms-and-conditions/">Terms &amp; Conditions</a></li>
<li><a href="{{config path="web/unsecure/base_url "}}privacy-policy/">Privacy Policy</a></li>
</ul>
</div>
<div class="footer-box" style="width: 300px;">
<h3 class="footer-title">Services and Support</h3>
<div class="col-md-6">
<ul class="footer_links">
<li><a href="{{config path="web/unsecure/base_url "}}help/">Help and FAQ</a></li>
<li><a href="{{config path="web/unsecure/base_url "}}help/#howtoorder">How To Order</a></li>
<li><a href="{{config path="web/unsecure/base_url "}}help/#payments">Payments</a></li>
<li><a href="{{config path="web/unsecure/base_url "}}help/#rewardpoints">Reward Points</a></li>
</ul>
</div>
<div class="col-md-6">
<ul class="footer_links">
<li><a href="{{config path="web/unsecure/base_url "}}help/#shipping">Shipping &amp; Delivery</a></li>
<li><a href="{{config path="web/unsecure/base_url "}}help/#returns">Cancellation &amp; Returns</a></li>
<li><a href="{{config path="web/unsecure/base_url "}}return-policy/">Return Policy</a></li>
</ul>
</div>
</div>
<div class="footer-box">
<h3 class="footer-title">LINE CHAT</h3>
<ul class="footer_links">
<li><img style="width: 100px; height: 100px;" src="{{media url="wysiwyg/PETLOFT_QR.jpg "}}" alt="" /></li>
</ul>
</div>
<div class="footer-box" style="margin: 0px;">
<h3 class="footer-title">Connect</h3>
<div class="item_shares">
<p><a href="https://plus.google.com/+Moxyst" target="_blank"> <span class="google-footer"><img src="{{media url="wysiwyg/footer_follow/google_follow.png"}}" alt="" width="32px" /></span> </a> <a href="https://pinterest.com/#" target="_blank"> <span class="pinterest-footer"><img src="{{media url="wysiwyg/footer_follow/pinterest_follow.png"}}" alt="" width="32px" /></span> </a> <a href="https://webstagra.ms/moxyst/" target="_blank"> <span class="instagram-footer"><img src="{{media url="wysiwyg/footer_follow/instagram_follow.png"}}" alt="" width="32px" /></span> </a> <a href="https://twitter.com/moxyst" target="_blank"> <span class="twitter-footer"><img src="{{media url="wysiwyg/footer_follow/twitter_follow.png"}}" alt="" width="32px" /></span> </a> <a href="https://www.facebook.com/moxyst" target="_blank"> <span class="facebook-footer"><img src="{{media url="wysiwyg/footer_follow/facebook_follow.png"}}" alt="" width="32px" /></span> </a></p>
</div>
</div>
<div class="col-md-6 row">
<h3 class="footer-title">Payment Partners</h3>
<img src="{{media url="wysiwyg/welcomePage/bank_images_footer.png "}}" alt="" width="450px;" /></div>
<div class="col-md-6 row">
<h3 class="footer-title">Shipping Partners</h3>
<img src="{{media url="wysiwyg/acommerce_footer.png "}}" alt="" width="185px;" /></div>
</div>
<div class="copyright_box  hidden-xs hidden-sm">
<div class="container" style="padding: 0px 5px;">
<p class="copyright">&copy; 2015 Whatsnew Co. Ltd.</p>
</div>
</div>
HTML;
$coreBlockModel->setContent($htmlContent);
$coreBlockModel->save();

//Welcome splash menu mobile
/** @var Mage_Cms_Model_Block $coreBlockModel */
$coreBlockModel = Mage::getModel('cms/block');
$coreStoreModel->setData('title', 'Welcome splash menu mobile');
$coreBlockModel->setIdentifier('welcome-mobile-menu');
$coreBlockModel->setStores($allStores);
$htmlContent = <<<HTML
<ul class="menu-mobile">
    <li><a href="/welcome">Home</a></li>
<!--                    <li><a href="#">About</a></li>-->
    <li class="petloft">
        <a href="http://petloft.com/">
            <span class="icon-shop icon-petloft"></span>
            <div class="petlotf-txt">
                <p>Pest</p>
                <p>Petlotf.com</p>
            </div>
        </a>
    </li>
    <li class="venbi">
        <a href="http://venbi.com/">
            <span class="icon-shop icon-venbi"></span>
            <div class="venbi-txt">
                <p>Baby</p>
                <p>Venbi.com</p>
            </div>
        </a>
    </li>
    <li class="sanoga">
        <a href="http://sanoga.com/">
            <span class="icon-shop icon-sanoga"></span>
            <div class="sanoga-txt">
                <p>Health</p>
                <p>Sanoga.com</p>
            </div>
        </a>
    </li>
    <li class="lafema">
        <a href="lafema.com">
            <span class="icon-shop icon-lafema"></span>
            <div class="lafema-txt">
                <p>Beauty</p>
                <p>Lafema.com</p>
            </div>
        </a>
    </li>
    <!--<li><a href="#">Services</a></li>-->
</ul>
HTML;
$coreBlockModel->setContent($htmlContent);
$coreBlockModel->save();




$install->endSetup();