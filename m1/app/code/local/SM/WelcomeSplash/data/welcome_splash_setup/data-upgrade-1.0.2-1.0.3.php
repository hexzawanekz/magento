<?php

// init Magento
Mage::app();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

// get store ids
$en     = Mage::getModel('core/store')->load('en', 'code')->getId();// Petloft
$th     = Mage::getModel('core/store')->load('th', 'code')->getId();
$len    = Mage::getModel('core/store')->load('len', 'code')->getId();// Lafema
$lth    = Mage::getModel('core/store')->load('lth', 'code')->getId();
$ven    = Mage::getModel('core/store')->load('ven', 'code')->getId();// Venbi
$vth    = Mage::getModel('core/store')->load('vth', 'code')->getId();
$sen    = Mage::getModel('core/store')->load('sen', 'code')->getId();// Sanoga
$sth    = Mage::getModel('core/store')->load('sth', 'code')->getId();
$moxyen = Mage::getModel('core/store')->load('moxyen', 'code')->getId();// Moxy
$moxyth = Mage::getModel('core/store')->load('moxyth', 'code')->getId();


/** @var Mage_Cms_Model_Resource_Block_Collection $allBlocks */
$allBlocks = Mage::getModel('cms/block')->getCollection()
    ->addFieldToFilter('identifier', 'welcome-shortcut');
if($allBlocks->count() > 0){
    /** @var Mage_Cms_Model_Block $item */
    foreach($allBlocks as $item){
        if($item->getId()) $item->delete(); // delete old blocks
    }

    // add new ones


    //==========================================================================
    // Welcome splash shortcut link (English)
    //==========================================================================
    $blockTitle = "Welcome splash shortcut link";
    $blockIdentifier = "welcome-shortcut";
    $blockStores = array($en, $sen, $len, $ven, $moxyen);
    $blockIsActive = 1;
    $blockContent = <<<EOD
<div class="col-sm-6">
<div class="row">
<div class="col-sm-12">
<div class="img-banner"><a href="http://moxy.co.th/en/moxy/electronics"><img onmouseover="this.src='{{media url="wysiwyg/welcomePage/electronics_hover_2.jpg"}}';" onmouseout="this.src='{{media url="wysiwyg/welcomePage/electronics_2.jpg"}}';" src="{{media url="wysiwyg/welcomePage/electronics_2.jpg"}}" alt="Electronics" /></a></div>
</div>
<div class="col-xs-6">
<div class="img-banner"><a href="http://moxy.co.th/en/moxy/fashion"><img onmouseover="this.src='{{media url="wysiwyg/welcomePage/fashion_hover_1.jpg"}}';" onmouseout="this.src='{{media url="wysiwyg/welcomePage/fashion_1.jpg"}}';" src="{{media url="wysiwyg/welcomePage/fashion_1.jpg"}}" alt="Fashion" /></a></div>
</div>
<div class="col-xs-6">
<div class="img-banner"><a href="http://wwww.sanoga.com/en/"><img onmouseover="this.src='{{media url="wysiwyg/welcomePage/health_hover_1.jpg"}}';" onmouseout="this.src='{{media url="wysiwyg/welcomePage/health_2.jpg"}}';" src="{{media url="wysiwyg/welcomePage/health_2.jpg"}}" alt="HEALTH" /></a></div>
</div>
</div>
</div>
<div class="col-sm-6">
<div class="row">
<div class="col-xs-6">
<div class="img-banner"><a href="http://www.lafema.com/en/"><img onmouseover="this.src='{{media url="wysiwyg/welcomePage/beauty_hover_1.jpg"}}';" onmouseout="this.src='{{media url="wysiwyg/welcomePage/beauty_1.jpg"}}';" src="{{media url="wysiwyg/welcomePage/beauty_1.jpg"}}" alt="BEAUTY" /></a></div>
</div>
<div class="col-xs-6">
<div class="img-banner"><a href="http://moxy.co.th/en/moxy/homeliving"><img onmouseover="this.src='{{media url="wysiwyg/welcomePage/living_hover_1.jpg"}}';" onmouseout="this.src='{{media url="wysiwyg/welcomePage/living_1.jpg"}}';" src="{{media url="wysiwyg/welcomePage/living_1.jpg"}}" alt="LIVING" /></a></div>
</div>
<div class="col-xs-6">
<div class="img-banner"><a href="http://www.venbi.com/en/"><img onmouseover="this.src='{{media url="wysiwyg/welcomePage/mombaby_hover_1.jpg"}}';" onmouseout="this.src='{{media url="wysiwyg/welcomePage/mombaby_1.jpg"}}';" src="{{media url="wysiwyg/welcomePage/mombaby_1.jpg"}}" alt="MOM &amp; BABY" /></a></div>
</div>
<div class="col-xs-6">
<div class="img-banner"><a href="http://www.petloft.com/en/"><img onmouseover="this.src='{{media url="wysiwyg/welcomePage/pets_hover_1.jpg"}}';" onmouseout="this.src='{{media url="wysiwyg/welcomePage/pets_1.jpg"}}';" src="{{media url="wysiwyg/welcomePage/pets_1.jpg"}}" alt="PETS" /></a></div>
</div>
</div>
</div>
EOD;
    $block = Mage::getModel('cms/block')->getCollection()
        ->addStoreFilter($blockStores, $withAdmin = false)
        ->addFieldToFilter('identifier', $blockIdentifier)
        ->getFirstItem();
    if ($block->getId()) $block->delete();// if exists then delete
    $block = Mage::getModel('cms/block');
    $block->setTitle($blockTitle);
    $block->setIdentifier($blockIdentifier);
    $block->setStores($blockStores);
    $block->setIsActive($blockIsActive);
    $block->setContent($blockContent);
    $block->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================



    //==========================================================================
    // Welcome splash shortcut link (Thai)
    //==========================================================================
    $blockTitle = "Welcome splash shortcut link";
    $blockIdentifier = "welcome-shortcut";
    $blockStores = array($th, $sth, $lth, $vth, $moxyth);
    $blockIsActive = 1;
    $blockContent = <<<EOD
<div class="col-sm-6">
<div class="row">
<div class="col-sm-12">
<div class="img-banner"><a href="http://moxy.co.th/th/moxy/electronics"><img onmouseover="this.src='{{media url="wysiwyg/welcomePage/electronics_hover_2.jpg"}}';" onmouseout="this.src='{{media url="wysiwyg/welcomePage/electronics_2.jpg"}}';" src="{{media url="wysiwyg/welcomePage/electronics_2.jpg"}}" alt="Electronics" /></a></div>
</div>
<div class="col-xs-6">
<div class="img-banner"><a href="http://moxy.co.th/th/moxy/fashion"><img onmouseover="this.src='{{media url="wysiwyg/welcomePage/fashion_hover_1.jpg"}}';" onmouseout="this.src='{{media url="wysiwyg/welcomePage/fashion_1.jpg"}}';" src="{{media url="wysiwyg/welcomePage/fashion_1.jpg"}}" alt="Fashion" /></a></div>
</div>
<div class="col-xs-6">
<div class="img-banner"><a href="http://wwww.sanoga.com/th/"><img onmouseover="this.src='{{media url="wysiwyg/welcomePage/health_hover_1.jpg"}}';" onmouseout="this.src='{{media url="wysiwyg/welcomePage/health_2.jpg"}}';" src="{{media url="wysiwyg/welcomePage/health_2.jpg"}}" alt="HEALTH" /></a></div>
</div>
</div>
</div>
<div class="col-sm-6">
<div class="row">
<div class="col-xs-6">
<div class="img-banner"><a href="http://www.lafema.com/th/"><img onmouseover="this.src='{{media url="wysiwyg/welcomePage/beauty_hover_1.jpg"}}';" onmouseout="this.src='{{media url="wysiwyg/welcomePage/beauty_1.jpg"}}';" src="{{media url="wysiwyg/welcomePage/beauty_1.jpg"}}" alt="BEAUTY" /></a></div>
</div>
<div class="col-xs-6">
<div class="img-banner"><a href="http://moxy.co.th/th/moxy/homeliving"><img onmouseover="this.src='{{media url="wysiwyg/welcomePage/living_hover_1.jpg"}}';" onmouseout="this.src='{{media url="wysiwyg/welcomePage/living_1.jpg"}}';" src="{{media url="wysiwyg/welcomePage/living_1.jpg"}}" alt="LIVING" /></a></div>
</div>
<div class="col-xs-6">
<div class="img-banner"><a href="http://www.venbi.com/th/"><img onmouseover="this.src='{{media url="wysiwyg/welcomePage/mombaby_hover_1.jpg"}}';" onmouseout="this.src='{{media url="wysiwyg/welcomePage/mombaby_1.jpg"}}';" src="{{media url="wysiwyg/welcomePage/mombaby_1.jpg"}}" alt="MOM &amp; BABY" /></a></div>
</div>
<div class="col-xs-6">
<div class="img-banner"><a href="http://www.petloft.com/th/"><img onmouseover="this.src='{{media url="wysiwyg/welcomePage/pets_hover_1.jpg"}}';" onmouseout="this.src='{{media url="wysiwyg/welcomePage/pets_1.jpg"}}';" src="{{media url="wysiwyg/welcomePage/pets_1.jpg"}}" alt="PETS" /></a></div>
</div>
</div>
</div>
EOD;
    $block = Mage::getModel('cms/block')->getCollection()
        ->addStoreFilter($blockStores, $withAdmin = false)
        ->addFieldToFilter('identifier', $blockIdentifier)
        ->getFirstItem();
    if ($block->getId()) $block->delete();// if exists then delete
    $block = Mage::getModel('cms/block');
    $block->setTitle($blockTitle);
    $block->setIdentifier($blockIdentifier);
    $block->setStores($blockStores);
    $block->setIsActive($blockIsActive);
    $block->setContent($blockContent);
    $block->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================
}