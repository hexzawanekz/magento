<?php

class SM_WelcomeSplash_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction() {

        $currentUrl = Mage::helper('core/url')->getCurrentUrl();

        if(count(explode('moxy', $currentUrl)) != 2) {
            $this->_redirect('/');
        }

        if( !preg_match('/th/', $currentUrl) && !preg_match('/en/', $currentUrl)) {

            /** @var Mage_Core_Model_Store $store */
            $store = Mage::app()->getStore();

            if(substr($store->getCode(), strlen($store->getCode()) - 2) == 'th') {
                $baseUrl = Mage::getBaseUrl().'welcome?lang=th';
            } else {
                $baseUrl = Mage::getBaseUrl().'welcome?lang=en';
            }

            $this->_redirectUrl($baseUrl);
        }

        $this->loadLayout();
        $this->renderLayout();
    }
}