<?php

class SM_WelcomeSplash_Model_Adminhtml_Categorylist {
    public function toOptionArray() {

        $moxyCategoryId = Mage::getModel('catalog/category')
            ->getCollection()
            ->addAttributeToFilter('name', 'Moxy')
            ->getFirstItem()
            ->getId();

        $welcomeSplashCategoryId = Mage::getModel('catalog/category')
            ->getCollection()
            ->addAttributeToFilter('name', 'Welcome Splash')
            ->getFirstItem()
            ->getId();

        $categories = array();
        $collection = Mage::getModel('catalog/category')
            ->getCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('path', array('like' => '1/'.$moxyCategoryId.'/'.$welcomeSplashCategoryId.'/%'))
            ->setOrder('name', 'asc');

        foreach ($collection as $cat) {
            $categories[] = ( array(
                'label' => (string) $cat->getName(),
                'value' => $cat->getId()
            ));
        }
        return $categories;
    }
}