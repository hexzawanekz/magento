<?php

class SM_WelcomeSplash_Model_Adminhtml_Number extends Mage_Core_Model_Config_Data {
    protected function _beforeSave() {
        $value = $this->getValue();

        if(!is_numeric($value)) {
            $value = 8;

            $this->setValue($value);
        }
    }
}