<?php

class SM_RelatedProducts_Helper_Data extends Mage_Core_Helper_Abstract
{

    public function getRelatedBoughtProductsInfo($_obj)
    {
        $ids = array();
        $_similarItems = $_obj->psims->similarItems;
        foreach ($_similarItems as $_item) {
            if ($_item->itemPrice != 0) {
                $ids[] = $_item;
            }
        }
        return $ids;
    }

    public function getRelatedViewedProductsInfo($_obj)
    {
        $ids = array();
        $_similarItems = $_obj->vsims->similarItems;
        foreach ($_similarItems as $_item) {
            if ($_item->itemPrice != 0) {
                $ids[] = $_item;
            }
        }
        return $ids;
    }

    public function getRelatedProductsInfo($_obj)
    {
        $ids = array();
        $_similarItems = $_obj->vsims->similarItems;
        foreach ($_similarItems as $_item) {
            $ids[$_item->itemId] = $_item;
        }
        $_similarItems = $_obj->psims->similarItems;
        foreach ($_similarItems as $_item) {
            $ids[$_item->itemId] = $_item;
        }
        return $ids;
    }

}
