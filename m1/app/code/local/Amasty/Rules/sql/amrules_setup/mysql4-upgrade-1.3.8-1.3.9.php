<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

/** @var Mage_SalesRule_Model_Resource_Rule_Collection $rules */
$rules = Mage::getModel('salesrule/rule')->getCollection()->load();
$stores = Mage::getModel('core/store')->getCollection()->load();

$query = "SET FOREIGN_KEY_CHECKS=0;";

foreach($rules as $rule) {
    $id = $rule->getId();
    foreach($stores as $store) {
        $query .= "INSERT INTO `salesrule_store` (`rule_id`, `store_id`) VALUES ({$id}, {$store->getId()});";
    }
}

$query .= "SET FOREIGN_KEY_CHECKS=1;";

$installer->run($query);
$installer->endSetup();
