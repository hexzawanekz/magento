<?php
/**
* @author Amasty Team
* @copyright Copyright (c) 2013 Amasty (http://www.amasty.com)
* @package Amasty_Social
*/
class Amasty_Cart_Block_Config extends Mage_Core_Block_Template
{
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('amasty/amcart/config.phtml');
    }
    
    public function getSendUrl()
    {
        $url = $this->getUrl('amcart/ajax/index');
        if(strpos($url, '?') > 0) {
            $url = explode('?', $url);
            $url = $url[0];
        }
        if (isset($_SERVER['HTTPS']) && 'off' != $_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "")
        {
            $url = str_replace('http:', 'https:', $url);
        }
        return $url;
    }
    
    public function getUpdateUrl()
    {
        $url = $this->getUrl('checkout/cart/updatePost');
        if(strpos($url, '?') > 0) {
            $url = explode('?', $url);
            $url = $url[0];
        }
        if (isset($_SERVER['HTTPS']) && 'off' != $_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "")
        {
            $url = str_replace('http:', 'https:', $url);
        }
        return $url;
    }
}