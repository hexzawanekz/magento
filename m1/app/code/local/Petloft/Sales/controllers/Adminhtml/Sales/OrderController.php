<?php

require_once 'Mage/Adminhtml/controllers/Sales/OrderController.php';

class Petloft_Sales_Adminhtml_Sales_OrderController extends Mage_Adminhtml_Sales_OrderController
{
    public function sendPickupEmailAction()
    {
        if ($order = $this->_initOrder()) {
            try {
                $order->sendNewPickupEmail();
                $this->_getSession()->addSuccess($this->__('The pickup email has been sent.'));
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            } catch (Exception $e) {
                $this->_getSession()->addError($this->__('Failed to send the pickup email.'));
                Mage::logException($e);
            }
        }
        $this->_redirect('adminhtml/sales_order/view', ['order_id' => $order->getId()]);
    }
} 
