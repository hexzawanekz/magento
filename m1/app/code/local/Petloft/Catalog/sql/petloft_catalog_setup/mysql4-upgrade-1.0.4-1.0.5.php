<?php
/**
 * Ensogo extension for Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * 
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade
 * the Ensogo BgImage module to newer versions in the future.
 * If you wish to customize the Ensogo BgImage module for your needs
 * please refer to http://www.magentocommerce.com for more information.
 *
 * @category   Ensogo
 * @package    Ensogo_BgImage
 * @copyright  Copyright (coffee) 2011 Ensogo Web ltd (http://www.ensogo.com/)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

$installer = $this;

/* @var $installer Mage_Customer_Model_Entity_Setup */
$installer->startSetup();

$installer->addAttribute('catalog_product', 'display_final_price_only', array(
    'group'                         => 'Prices',
    'label'                         => 'Display Final Price Only',
    'type'                          => 'int',
    'input'                         => 'boolean',
    'source'                        => 'eav/entity_attribute_source_table',
    'global'                        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'                       => true,
    'required'                      => false,
    'user_defined'                  => true,
    'searchable'                    => false,
    'is_searchable'                 => true,
    'is_visible_in_advanced_search' => true,
    'filterable'                    => false,
    'comparable'                    => false,
    'visible_on_front'              => true,
    'is_visible_on_front'           => true,
    'used_in_product_listing'       => true,
    'visible_in_advanced_search'    => false,
    'unique'                        => false,
    'default'                       => false
));

$installer->endSetup();