<?php

$installer = $this;
$installer->startSetup();
$attrGroupName = 'Whatsnew Details';

$installer->addAttribute('catalog_product', 'packaging_label_header', array(
    'label'             => 'Product Packaging Label Header',
    'type'              => 'varchar',
    'input'             => 'select',
    'frontend'          => '',
    'source'            => '',
    'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'visible'           => true,
    'required'          => false,
    'user_defined'      => true,
    'searchable'        => false,
    'filterable'        => false,
    'comparable'        => false,
    'visible_on_front'  => true,
    'unique'            => false,
    'visible_in_advanced_search' => false,
    'used_in_product_listing' => true,
    'option' => array(
        'value' => array( 
            'Color'   => array('Color'),
            'Option'   => array('Option'),
            'Packaging' => array('Packaging'),
            'Qty' => array('Qty'),
            'Size' => array('Size'),
        )
    ),
    
    'default' =>  'Qty',
));

$installer->addAttributeToGroup('catalog_product', '*baby care', $attrGroupName, 'packaging_label_header');
$installer->addAttributeToGroup('catalog_product', '*baby clothes', $attrGroupName, 'packaging_label_header');
$installer->addAttributeToGroup('catalog_product', '*baby diapers', $attrGroupName, 'packaging_label_header');
$installer->addAttributeToGroup('catalog_product', '*baby food', $attrGroupName, 'packaging_label_header');
$installer->addAttributeToGroup('catalog_product', '*baby formula', $attrGroupName, 'packaging_label_header');
$installer->addAttributeToGroup('catalog_product', '*baby general products', $attrGroupName, 'packaging_label_header');
$installer->addAttributeToGroup('catalog_product', '*pet products', $attrGroupName, 'packaging_label_header');
$installer->addAttributeToGroup('catalog_product', '*petfood', $attrGroupName, 'packaging_label_header');

$installer->endSetup();