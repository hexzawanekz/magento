<?php

$this->startSetup();
$this->addAttribute(Mage_Catalog_Model_Category::ENTITY, 'navigation_css', array(
    'input'         => 'text',
    'type'          => 'varchar',
    'label'         => 'Navigation Css',
    'group'         => 'Custom Design',
    'backend'       => '',
    'visible'       => true,
    'required'      => false,
    'visible_on_front' => false,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
));

$this->endSetup();