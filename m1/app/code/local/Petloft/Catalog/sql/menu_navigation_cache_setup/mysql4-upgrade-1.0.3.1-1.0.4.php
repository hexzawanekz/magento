<?php

$installer = $this;
$installer->startSetup();

$installer->run("
    DROP TABLE IF EXISTS `petloftcatalog_menu_navigation_cache`;
");

$installer->run("
    CREATE TABLE `petloftcatalog_menu_navigation_cache` (
      `id` int(11) NOT NULL auto_increment,
      `path` text,
      `html_cache` text,
      PRIMARY KEY  (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");
$installer->endSetup();