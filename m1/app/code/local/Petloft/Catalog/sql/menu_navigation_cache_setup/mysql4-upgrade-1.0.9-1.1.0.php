<?php

$installer = $this;
$installer->startSetup();

$installer->run("
    ALTER TABLE `petloftcatalog_menu_navigation_cache`
    MODIFY COLUMN `html_cache` LONGTEXT
");

$installer->endSetup();