<?php

$installer = $this;
$installer->startSetup();

$installer->run("
    DROP TABLE IF EXISTS `petloftcatalog_menu_navigation_cache`;
");

$installer->run("
    CREATE TABLE `petloftcatalog_menu_navigation_cache` (
      `path` varchar(200) UNIQUE,
      `html_cache` text,
      PRIMARY KEY  (`path`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");
$installer->endSetup();