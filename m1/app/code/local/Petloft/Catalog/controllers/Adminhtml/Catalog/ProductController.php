<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @copyright   Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Catalog product controller
 *
 * @category   Mage
 * @package    Mage_Adminhtml
 * @author      Magento Core Team <core@magentocommerce.com>
 */

require_once 'Mage/Adminhtml/controllers/Catalog/ProductController.php';

class Petloft_Catalog_Adminhtml_Catalog_ProductController extends Mage_Adminhtml_Catalog_ProductController
{
    /**
     * Validate product
     *
     */
    public function validateAction()
    {
        $response = new Varien_Object();
        $response->setError(false);

        try {
            $productData = $this->getRequest()->getPost('product');

            if ($productData && !isset($productData['stock_data']['use_config_manage_stock'])) {
                $productData['stock_data']['use_config_manage_stock'] = 0;
            }
            /* @var $product Mage_Catalog_Model_Product */
            $product = Mage::getModel('catalog/product');
            $product->setData('_edit_mode', true);
            if ($storeId = $this->getRequest()->getParam('store')) {
                $product->setStoreId($storeId);
            }
            if ($setId = $this->getRequest()->getParam('set')) {
                $product->setAttributeSetId($setId);
            }
            if ($typeId = $this->getRequest()->getParam('type')) {
                $product->setTypeId($typeId);
            }
            if ($productId = $this->getRequest()->getParam('id')) {
                $product->load($productId);
            }

            $dateFields = array();
            $attributes = $product->getAttributes();
            foreach ($attributes as $attrKey => $attribute) {
                if ($attribute->getBackend()->getType() == 'datetime') {
                    if (array_key_exists($attrKey, $productData) && $productData[$attrKey] != ''){
                        $dateFields[] = $attrKey;
                    }
                }
            }
            $productData = $this->_filterDates($productData, $dateFields);

            $product->addData($productData);
            $product->validate();
            /**
             * @todo implement full validation process with errors returning which are ignoring now
             */
//            if (is_array($errors = $product->validate())) {
//                foreach ($errors as $code => $error) {
//                    if ($error === true) {
//                        Mage::throwException(Mage::helper('catalog')->__('Attribute "%s" is invalid.', $product->getResource()->getAttribute($code)->getFrontend()->getLabel()));
//                    }
//                    else {
//                        Mage::throwException($error);
//                    }
//                }
//            }

            $this->_validateBundleData($productData);
        }
        catch (Mage_Eav_Model_Entity_Attribute_Exception $e) {
            $response->setError(true);
            $response->setAttribute($e->getAttributeCode());
            $response->setMessage($e->getMessage());
        } catch (Mage_Core_Exception $e) {
            $response->setError(true);
            $response->setMessage($e->getMessage());
        } catch (Exception $e) {
            $this->_getSession()->addError($e->getMessage());
            $this->_initLayoutMessages('adminhtml/session');
            $response->setError(true);
            $response->setMessage($this->getLayout()->getMessagesBlock()->getGroupedHtml());
        }

        $this->getResponse()->setBody($response->toJson());
    }


    protected function _validateBundleData($productData) {

        if(isset($productData['bundle_enable']) && $productData['bundle_enable'] == 1) {

            $bundlePrice = $productData['bundle_price'];
            $bundleSku = $productData['bundle_sku'];
            $bundleQty = $productData['bundle_qty'];

            $price = $productData['price'];

            if(!$bundlePrice || trim($bundlePrice) == '') {
                Mage::throwException(Mage::helper('adminhtml')->__('Bundle price cannot be null or empty.'));
            } elseif(!$bundleSku || trim($bundleSku) == '') {
                Mage::throwException(Mage::helper('adminhtml')->__('Bundle SKU cannot be null or empty.'));
            } elseif(!$bundleQty || trim($bundleQty) == '') {
                Mage::throwException(Mage::helper('adminhtml')->__('Bundle qty cannot be null or empty.'));
            } else {
                $bundlePrices = explode('#', $bundlePrice);
                $bundleSkus = explode('#', $bundleSku);
                $bundleQtys = explode('#', $bundleQty);

                if(!((count($bundlePrices) == count($bundleSkus)) && (count($bundlePrices) == count($bundleQtys)))) {
                    Mage::throwException(Mage::helper('adminhtml')->__('Bundle data is incorrect.'));
                } else {

                    foreach($bundleQtys as $qty){
                        if(!is_numeric($qty)) {
                            Mage::throwException(Mage::helper('adminhtml')->__('Bundle qty is incorrect.'));
                        }
                    }

                    $totalPrice = 0;
                    foreach($bundlePrices as $key => $bundlePrice){
                        if(is_numeric($bundlePrice)) {
                            $totalPrice += ($bundlePrice * $bundleQtys[$key]);
                        } else {
                            Mage::throwException(Mage::helper('adminhtml')->__('Bundle price is incorrect.'));
                        }
                    }

                    if(round($totalPrice, 2) != $price) {
                        Mage::throwException(Mage::helper('adminhtml')->__('Sum Of bundle price is not equal price.'));
                    }

                    foreach($bundleSkus as $sku){
                        if(empty($sku)) {
                            Mage::throwException(Mage::helper('adminhtml')->__('Bundle sku is incorrect.'));
                        }
                    }
                }
            }
        }
    }
}
