<?php

class Petloft_Catalog_Adminhtml_Instagram_IndexController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
        /** @var Petloft_Catalog_Model_Instagram $igObj */
        $igObj = Mage::getSingleton('petloftcatalog/instagram');

        $result = $igObj->updateDataFromInstagram();
        if($result['status']){

            // refresh Block HTML cache
            Mage::app()->getCacheInstance()->cleanType(Mage_Core_Block_Abstract::CACHE_GROUP);

            Mage::getSingleton('core/session')->addSuccess($result['message']);
        }else{
            Mage::getSingleton('core/session')->addError($result['message']);
        }

        $this->_redirectReferer();
    }
}