<?php

class Petloft_Catalog_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function getCustomPriceConfigurable ($_product = null, $showOffText = false, $isSlider = false) {
        $priceHtml = null;
        if($_product != null) {
            if($_product->getTypeId() == "configurable") {
                $product_block = new Mage_Catalog_Block_Product;
                $_bestProduct = $this->getBestProduct($_product,$showOffText);

                $currentController = Mage::app()->getRequest()->getControllerName();
                $currentModule = Mage::app()->getRequest()->getModuleName();
                if ($currentController == 'category' && $currentModule == 'catalog') {
                    $priceHtml = $product_block->getPriceHtml($_bestProduct, true);
                } elseif (in_array($currentController, array('result', 'ajax')) && $currentModule == 'catalogsearch') {
                    $priceHtml = $product_block->getPriceHtml($_bestProduct, true);
                } elseif($isSlider) {
                    $priceHtml = $product_block->getPriceHtml($_bestProduct, true);
                } else {
                    $priceHtml = $product_block->setTemplate('catalog/product/price1.phtml')->setProduct($_bestProduct)->toHtml();
                }
                $priceHtml = str_replace('-'.$_bestProduct->getId(), '-'.$_product->getId(), $priceHtml);
                if($showOffText) {$priceHtml = $priceHtml."<span class='off_configurable'>&nbsp;off</span>";}

                unset($product_block);
            }
        }
        return $priceHtml;
    }

    public function getBestProduct ($_product = null, $showOffText = false){
        if($_product != null) {
            if($_product->getTypeId() == "configurable") {
                $_configurable = $_product->getTypeInstance()->getUsedProductIds();
                $_bestProduct = $_bestProductInstock = $_bestProductOutstock = false;
                foreach ($_configurable as $_config) {
                    $_simpleproduct = Mage::getModel('catalog/product')->load($_config);
                    $_productMeasurement = $_simpleproduct->getAdminAttributeText('pet_food_cost_per_type');

                    if (strtolower($_productMeasurement) == "kilo") {
                        $_simpleproduct->setUnit($_simpleproduct->getWeight() / 1000);
                    } else if (strtolower($_productMeasurement) == "litre") {
                        $_simpleproduct->setUnit($_simpleproduct->getWeight());
                    } else if ($_simpleproduct->getData('pet_food_packaging_count') != null) {
                        $_simpleproduct->setUnit($_simpleproduct->getData('pet_food_packaging_count'));
                    } else {
                        $_simpleproduct->setUnit(1);
                    }

                    $_simpleproduct->setPricePerUnit($_simpleproduct->getFinalPrice() / $_simpleproduct->getUnit());
                    if (!$_bestProduct) {
                        $_bestProduct = $_simpleproduct;
                        if ($_simpleproduct->isSaleable()) {
                            $_bestProductInstock = $_bestProduct;
                        } else {
//                            $_bestProductOutstock = $_bestProduct;
                        }
                    } else {
                        if (!$_bestProduct->getPricePerUnit() || ($_simpleproduct->getPricePerUnit() < $_bestProduct->getPricePerUnit())) {
                            $_bestProduct = $_simpleproduct;
                        }

                        if ($_simpleproduct->isSaleable()) {
                            if (!$_bestProductInstock) {
                                $_bestProductInstock = $_simpleproduct;
                            } else {
                                if (!$_bestProductInstock->getPricePerUnit() || ($_simpleproduct->getPricePerUnit() < $_bestProductInstock->getPricePerUnit())) {
                                    $_bestProductInstock = $_simpleproduct;
                                }
                            }
                        } else {
//                            $_bestProductOutstock = $_bestProduct;
                        }
                    }
                }
                if(!$_bestProductInstock) {
//                    $_bestProduct = $_bestProductOutstock;
                }
                else {$_bestProduct = $_bestProductInstock;}

                return $_bestProduct;
            }
        }
    }
    /**
     * Get delivery days by order id and region country id
     */
    public function getDeliveryDays($orderId, $regionId = null)
    {
        /** @var Mage_Sales_Model_Order $order */
        $order = Mage::getModel('sales/order')->getCollection()->addFieldToFilter('increment_id', $orderId)->getFirstItem();
        if($order->getId()){
            $ordered_items = $order->getAllItems();
            // Product ids array: $pIds
            $pIds = array();
            foreach ($ordered_items as $item) {
                /** @var Mage_Sales_Model_Order_Item $item */
                $pIds[] = $item->getProductId();
            }
            if(count($pIds) > 0){
                $flagDropship = $flagSpecial = false;
                $collection = Mage::getResourceModel('catalog/product_collection')
                    ->addFieldToFilter('entity_id', $pIds)
                    ->joinField('qty',
                        'cataloginventory/stock_item',
                        'qty',
                        'product_id=entity_id',
                        '{{table}}.stock_id=1',
                        'left')
                    ->addAttributeToSelect('*');
                $max = 0;
                foreach ($collection as $product) {
                    /** @var Mage_Catalog_Model_Product $product */
                    // Dropship product
                    if($product->getData('override_inventory') == 0 || $product->getStockItem()->getIsInStock() == 0) {
                        if ($product->getAttributeText('shipping_duration') == 'Dropship Product') {
                            $temp = 14;
                            if ($max < $temp) $max = $temp;
                        }
                        // Special product
                        if ($product->getAttributeText('shipping_duration') == 'Special Order Product') {
                            if($product->getStockItem()->getManageStock() == 1){
                                if($product->getStockItem()->getIsInStock() != 0){
                                    $temp = 10;
                                }
                            }else{
                                if($product->getStockItem()->getIsInStock() == 0){
                                    $temp = 10;
                                }else{
                                    $temp = 4;
                                }
                            }

                            if ($max < $temp) $max = $temp;
                        }
                        if ($product->getAttributeText('shipping_duration') == 'Consignment and Buy-in Product' && $product->getStockItem()->getIsInStock() == 1 ) {
                            $temp = 4;
                            if ($max < $temp) $max = $temp;
                        }
                    }else{
                        $temp = 10;
                        if ($max < $temp) $max = $temp;
                    }
                }
                if($max == 14) return '7-14';
                if($max == 10) return '7-10';
                if($max == 4) return '1-4';
            }
        }
        return '1-4';
    }

    public function getAllDealsCategories()
    {
        /** @var Mage_Catalog_Model_Category $cateObj */
        $cateObj = Mage::getModel('catalog/category')->getCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('url_key', array('eq' => 'all-deals'))
            ->addAttributeToFilter('level', array('eq' => 3))
            ->getFirstItem();

        return $cateObj->getId() ? $cateObj->getChildrenCategories() : array();
    }
}