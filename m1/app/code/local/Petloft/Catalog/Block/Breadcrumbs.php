<?php

class Petloft_Catalog_Block_Breadcrumbs extends Mage_Catalog_Block_Breadcrumbs
{
   
    /**
     * Preparing layout
     *
     * @return Petloft_Catalog_Block_Breadcrumbs
     */
    protected function _prepareLayout()
    {
        if ($breadcrumbsBlock = $this->getLayout()->getBlock('breadcrumbs')) {
            $firstLabelAndUrl = $this->getFirstLabelAndUrl();
            $breadcrumbsBlock->addCrumb('home', array(
                'label' => Mage::helper('catalog')->__($firstLabelAndUrl['label']),
                'title' => Mage::helper('catalog')->__('Go to Home Page'),
                'link'  => $firstLabelAndUrl['url'],
            ));

            $title = array();
            $orgTitle = array();
            $path  = Mage::helper('catalog')->getBreadcrumbPath();
			$view = false;
			$deal = false;
			$storeCode = Mage::app()->getStore()->getCode();
            foreach ($path as $name => $breadcrumb) {
                $breadcrumbsBlock->addCrumb($name, $breadcrumb);
				if($name == 'product') {
					$view = true;
					$title[] = $breadcrumb['label'];
				}
				if(strtolower($breadcrumb['label']) == 'deals') {
					if($storeCode!='th') {
						$title[] = 'Deal';
					}
					$deal = true;
				}
				$orgTitle[] = $breadcrumb['label'];
            }
			if(!$view) {
				$title = $orgTitle;
			}
			if($deal && $storeCode == 'th') {
				$title[] = Mage::helper('catalog')->__('Deal');
			}
			// array_unshift($title,$this->getDefaultTitle());
            if ($headBlock = $this->getLayout()->getBlock('head')) {
                $headBlock->setTitle(join($this->getTitleSeparator(), array_reverse($title)));
            }
        }
        return Mage_Core_Block_Template::_prepareLayout();
    }

	public function getDefaultTitle()
    {
        return Mage::getStoreConfig('design/head/default_title');
    }

    public function getFirstLabelAndUrl()
    {
        return array('label' => 'Home', 'url' => Mage::getBaseUrl());
    }
}
