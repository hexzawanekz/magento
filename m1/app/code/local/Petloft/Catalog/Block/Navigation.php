<?php

/**
 * Catalog navigation
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Petloft_Catalog_Block_Navigation extends Mage_Catalog_Block_Navigation
{
    protected $_config = null;

    public function _construct()
    {
        parent::_construct();

        if($config = Mage::getStoreConfig('petloftcatalog/menu_cache/enabled')) $this->_config = $config;

        // disable Magento cache for all sites
//        $this->unsetData('cache_lifetime');
//        $this->unsetData('cache_tags');
    }

    public function loadMenuNav()
    {
        /** @var Petloft_Catalog_Model_Navigation_Renderer $renderer */
        $renderer = Mage::getModel('petloftcatalog/navigation_renderer');
        /** @var Petloft_Catalog_Model_Navigation_Html $htmlModel */
        $htmlModel = Mage::getModel('petloftcatalog/navigation_html');

        if($this->_config == true){
            // get store code
            $store_code = Mage::app()->getStore()->getCode();

            return $renderer->toHtml($store_code . '/' . Petloft_Catalog_Model_Navigation_Renderer::DESKTOP_FILENAME);
        }else{
            $cacheId = 'des_navigation';
            $cacheTag = 'des_navigation_cache';
            if (($data_to_be_cached = Mage::app()->getCache()->load($cacheId))) {
                return unserialize($data_to_be_cached);
            } else {
                $data_to_be_cached = $htmlModel->getMenuHtmlByKey();
                Mage::app()->getCache()->save(serialize($data_to_be_cached), $cacheId, array($cacheTag));
                return unserialize(Mage::app()->getCache()->load($cacheId));
            }
        }
    }

    public function loadMobileMenuNav()
    {
        /** @var Petloft_Catalog_Model_Navigation_Renderer $renderer */
        $renderer = Mage::getModel('petloftcatalog/navigation_renderer');

        /** @var Petloft_Catalog_Model_Navigation_Html $htmlModel */
        $htmlModel = Mage::getModel('petloftcatalog/navigation_html');
        if($this->_config == true){
            $store_code = Mage::app()->getStore()->getCode();
            return $renderer->toHtml($store_code . '/' . Petloft_Catalog_Model_Navigation_Renderer::MOBILE_FILENAME);
        }else{
            $cacheId = 'mob_navigation';
            $cacheTag = 'mob_navigation_cache';
            if (($data_to_be_cached = Mage::app()->getCache()->load($cacheId))) {
                return unserialize($data_to_be_cached);
            } else {
                $data_to_be_cached = $htmlModel->cacheMobileRender();
                Mage::app()->getCache()->save(serialize($data_to_be_cached), $cacheId, array($cacheTag));
                return unserialize(Mage::app()->getCache()->load($cacheId));
            }
        }
    }
}
