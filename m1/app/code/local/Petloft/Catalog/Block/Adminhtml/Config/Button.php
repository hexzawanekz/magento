<?php

class Petloft_Catalog_Block_Adminhtml_Config_Button extends Mage_Adminhtml_Block_System_Config_Form_Field
{
    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        $this->setElement($element);
        $url = Mage::helper('adminhtml')->getUrl('petloftcatalog_menucache/index/index');

        $html = $this->getLayout()->createBlock('adminhtml/widget_button')
            ->setType('button')
            ->setClass('scalable')
            ->setLabel('Clear All')
            ->setOnClick("setLocation('{$url}')")
            ->toHtml();

        return $html;
    }
}