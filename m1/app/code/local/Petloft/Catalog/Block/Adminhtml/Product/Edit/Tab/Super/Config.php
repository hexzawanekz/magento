<?php
class Petloft_Catalog_Block_Adminhtml_Product_Edit_Tab_Super_Config extends Mage_Adminhtml_Block_Catalog_Product_Edit_Tab_Super_Config
{
    public function __construct()
    {
        parent::__construct();
		 $this->setTemplate('petloftcatalog/product/edit/super/config.phtml');
    }
}
