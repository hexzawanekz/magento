<?php

class Petloft_Catalog_Block_Product_Homegrid extends Mage_Catalog_Block_Product_Abstract
{
    protected $_category_id;
    protected $_object;

    public function setCategoryId($id)
    {
        $this->_category_id = $id;
    }

    public function getCategoryObject($id)
    {
        if(!is_object($this->_object)){
            $this->_object = Mage::getModel('catalog/category')->load($id);
        }
        return $this->_object;
    }

    public function getCategoryName()
    {
        return $this->_category_id ? $this->getCategoryObject($this->_category_id)->getName() : null;
    }

    public function getProducts()
    {
        /** @var Mage_Catalog_Model_Resource_Product_Collection $productCollection */
        $productCollection = $this->getCategoryObject($this->_category_id)->getProductCollection()
            ->addAttributeToSelect('*')
            ->setPageSize(4)
            ->addAttributeToFilter('status', 1)
            ->setOrder('position', 'ASC');

        return $productCollection;
    }

    public function getConfigurableUrl($pid)
    {
        $p = Mage::getModel('catalog/product')->load($pid);
        $link = $this->getProductUrl($p);
        return $link;
    }
}