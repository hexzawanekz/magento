<?php

/**
 * Product list
 *
 * @category   Petloft
 * @package    Petloft_Catalog
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Petloft_Catalog_Block_Product_Bestseller extends Petloft_Catalog_Block_Product_List
{
    /**
     * Get catalog layer model
     *
     * @return Petloft_Catalog_Model_Recommend
     */
    public function getLayer()
    {
        $layer = Mage::registry('current_layer');
        if ($layer) {
            return $layer;
        }
        return Mage::getSingleton('petloftcatalog/bestseller');
    }

    public function getBestsellerAllowAttributes($parent)
    {
        return $parent->getTypeInstance(true)
            ->getConfigurableAttributes($parent);
    }

    /**
     * Get Allowed Products
     *
     * @return array
     */
    public function getBestsellerAllowProducts($parent)
    {
        // if (!$this->getData('allow_products')) {
            $products = array();
            $skipSaleableCheck = Mage::helper('catalog/product')->getSkipSaleableCheck();
            $allProducts = $parent->getTypeInstance(true)
                ->getUsedProducts(null, $parent);
            foreach ($allProducts as $product) {
                if ($product->isSaleable() || $skipSaleableCheck) {
                    $products[] = $product;
                }
            }
            $this->setAllowProducts($products);
        // }
        return $this->getData('allow_products');
    }

    public function getBestsellerOptionAttribute($parent, $childId)
    {
        $allowProducts = $this->getBestsellerAllowProducts($parent);
        foreach($allowProducts as $product)
        {
            $productId  = $product->getId();
            $allowAttributes = $this->getBestsellerAllowAttributes($parent);
            foreach ($allowAttributes as $attribute)
            {
                $productAttribute   = $attribute->getProductAttribute();
                $productAttributeId = $productAttribute->getId();
                $attributeValue     = $product->getData($productAttribute->getAttributeCode());
                if (!isset($options[$productAttributeId])) {
                    $options[$productAttributeId] = array();
                }

                if (!isset($options[$productAttributeId][$attributeValue])) {
                    $options[$productAttributeId][$attributeValue] = array();
                }
                $options[$productAttributeId][$attributeValue][] = $productId;
            }
        }

        $e = array();
        $allowAttributes = $this->getBestsellerAllowAttributes($parent);
        foreach ($allowAttributes as $attribute)
        {
            $productAttribute = $attribute->getProductAttribute();
            $attributeId = $productAttribute->getId();

            $prices = $attribute->getPrices();
            foreach ($prices as $value)
            {
                $productsIndex = $options[$attributeId][$value['value_index']];
                if(in_array($childId, $productsIndex))
                {                            
                    $e['attributeId'] = $attributeId;
                    $e['attributeValue'] = $value['value_index'];
                    return $e;
                }
            }
        }
        return false;
    }
}