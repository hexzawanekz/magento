<?php

/**
 * Catalog super product configurable part block
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Petloft_Catalog_Block_Product_View_Type_Configurable extends Mage_Catalog_Block_Product_View_Type_Configurable
{
    
    /**
     * Composes configuration for js
     *
     * @return string
     */
//    public function getJsonConfig()
//    {
//        $attributes = array();
//        $options    = array();
//        $store      = $this->getCurrentStore();
//        $taxHelper  = Mage::helper('tax');
//        $currentProduct = $this->getProduct();
//
//        $optionsProd    = array();
//
//        $preconfiguredFlag = $currentProduct->hasPreconfiguredValues();
//        if ($preconfiguredFlag) {
//            $preconfiguredValues = $currentProduct->getPreconfiguredValues();
//            $defaultValues       = array();
//        }
//        $allowedProducts = $this->getAllowProducts();
//
//        if(sizeof($allowedProducts)<=0) {
//			Mage::helper('catalog/product')->setSkipSaleableCheck(true);
//			$allowedProducts = $this->setAllowProducts(null)->getAllowProducts();
//		}
//        foreach ($allowedProducts as $product) {
//            $productId  = $product->getId();
//            // *************
//            $prodPrice = ($product->getMsrp()>0)?$product->getMsrp():$product->getPrice();
//            $prodFinalPrice = $product->getFinalPrice();
//            // $prodWeight = $product->getWeight();
//            $pWeight = $product->getWeight();
//            $prodPackagingCount = $product->getResource()->getAttribute('pet_food_packaging_count')->getFrontend()->getValue($product);
//            $prodpackagingTypeDisplay = $product->getResource()->getAttribute('pet_food_packaging_type')->getFrontend()->getValue($product);
//            $pMeasurement = $product->getAdminAttributeText('pet_food_cost_per_type');
//            if($pMeasurement) {
//                $prodPackagingMeasurement = strtolower($pMeasurement);
//            } else {
//                $prodPackagingMeasurement = '';
//            }
//            $prodWeight = $pWeight;
//
//
//            // *************
//
//            foreach ($this->getAllowAttributes() as $attribute) {
//                $productAttribute   = $attribute->getProductAttribute();
//                $productAttributeId = $productAttribute->getId();
//                $attributeValue     = $product->getData($productAttribute->getAttributeCode());
//
//                if (!isset($options[$productAttributeId])) {
//                    $options[$productAttributeId] = array();
//                }
//
//                if (!isset($options[$productAttributeId][$attributeValue])) {
//                    $options[$productAttributeId][$attributeValue] = array();
//                }
//                $options[$productAttributeId][$attributeValue][] = $productId;
//
//                // *************
//                if (!isset($optionsProd[$productAttributeId])) {
//                    $optionsProd[$productAttributeId] = array();
//                }
//                if (!isset($optionsProd[$productAttributeId][$attributeValue])) {
//                    $optionsProd[$productAttributeId][$attributeValue] = array();
//                }
//                $optionsProd[$productAttributeId][$attributeValue]['label'] = $prodPackagingCount.' '.$prodpackagingTypeDisplay;
//                $optionsProd[$productAttributeId][$attributeValue]['regularPrice'] = $prodPrice;
//                $optionsProd[$productAttributeId][$attributeValue]['specialPrice'] = $prodFinalPrice;
//                $optionsProd[$productAttributeId][$attributeValue]['weight'] = $prodWeight;
//                $optionsProd[$productAttributeId][$attributeValue]['packagingCount'] = $prodPackagingCount;
//                $optionsProd[$productAttributeId][$attributeValue]['packagingMeasurement'] = $prodPackagingMeasurement;
//                // *************
//            }
//        }
//
//        foreach ($this->getAllowAttributes() as $attribute) {
//            $productAttribute = $attribute->getProductAttribute();
//            $attributeId = $productAttribute->getId();
//            $info = array(
//               'id'        => $productAttribute->getId(),
//               'code'      => $productAttribute->getAttributeCode(),
//               'label'     => $attribute->getLabel(),
//               'options'   => array()
//            );
//
//            $optionPrices = array();
//            $prices = $attribute->getPrices();
//            if (is_array($prices)) {
//                foreach ($prices as $value) {
//                    if(!$this->_validateAttributeValue($attributeId, $value, $options)) {
//                        continue;
//                    }
//                    $currentProduct->setConfigurablePrice(
//                        $this->_preparePrice($value['pricing_value'], $value['is_percent'])
//                    );
//                    $currentProduct->setParentId(true);
//                    Mage::dispatchEvent(
//                        'catalog_product_type_configurable_price',
//                        array('product' => $currentProduct)
//                    );
//                    $configurablePrice = $currentProduct->getConfigurablePrice();
//
//                    // *************
//                    $price = $optionsProd[$productAttributeId][$value['value_index']]['specialPrice'];
//                    $oldPrice = $optionsProd[$productAttributeId][$value['value_index']]['regularPrice'];
//                    // *************
//
//                    if (isset($options[$attributeId][$value['value_index']])) {
//                        $productsIndex = $options[$attributeId][$value['value_index']];
//                    } else {
//                        $productsIndex = array();
//                    }
//                    if($optionsProd[$productAttributeId][$value['value_index']]['packagingMeasurement']=='litre'){
//                        $tmp = explode(" ", $value['label']);
//                        $optionsProd[$productAttributeId][$value['value_index']]['weight'] = $tmp[0];
//                    }
//
//                    $info['options'][] = array(
//                        'id'        => $value['value_index'],
//                        // 'label'     => $value['label'],
//                        'label'     => $optionsProd[$productAttributeId][$value['value_index']]['label'],
//                        'packagingCount'        => $optionsProd[$productAttributeId][$value['value_index']]['packagingCount'],
//                        'packagingMeasurement'  => $optionsProd[$productAttributeId][$value['value_index']]['packagingMeasurement'],
//                        // *************
//                        'price'     => $configurablePrice,
//                        /*'oldPrice'  => $this->_prepareOldPrice($value['pricing_value'], $value['is_percent']),
//                        'specialPrice' => $this->_preparePrice($optionsProd[$productAttributeId][$value['value_index']]['specialPrice'], false),
//                        'regularPrice' => $this->_preparePrice($optionsProd[$productAttributeId][$value['value_index']]['regularPrice'], false),*/
//                        // 'price'     => $this->_preparePrice($price, false),
//                        'oldPrice'  => $this->_preparePrice($oldPrice, false),
//                        // 'ourPrice' => $this->_preparePrice($optionsProd[$productAttributeId][$value['value_index']]['specialPrice'], false),
//                        'ourPrice'  => $configurablePrice,
//                        'weight'    => $optionsProd[$productAttributeId][$value['value_index']]['weight'],
//                        // *************
//                        'products'  => $productsIndex,
//                    );
//                    $optionPrices[] = $configurablePrice;
//                }
//            }
//            /**
//             * Prepare formated values for options choose
//             */
//            foreach ($optionPrices as $optionPrice) {
//                foreach ($optionPrices as $additional) {
//                    $this->_preparePrice(abs($additional-$optionPrice));
//                }
//            }
//            if($this->_validateAttributeInfo($info)) {
//               $attributes[$attributeId] = $info;
//            }
//
//            // Add attribute default value (if set)
//            if ($preconfiguredFlag) {
//                $configValue = $preconfiguredValues->getData('super_attribute/' . $attributeId);
//                if ($configValue) {
//                    $defaultValues[$attributeId] = $configValue;
//                }
//            }
//        }
//
//        $taxCalculation = Mage::getSingleton('tax/calculation');
//        if (!$taxCalculation->getCustomer() && Mage::registry('current_customer')) {
//            $taxCalculation->setCustomer(Mage::registry('current_customer'));
//        }
//
//        $_request = $taxCalculation->getRateRequest(false, false, false);
//        $_request->setProductClassId($currentProduct->getTaxClassId());
//        $defaultTax = $taxCalculation->getRate($_request);
//
//        $_request = $taxCalculation->getRateRequest();
//        $_request->setProductClassId($currentProduct->getTaxClassId());
//        $currentTax = $taxCalculation->getRate($_request);
//
//        $taxConfig = array(
//            'includeTax'        => $taxHelper->priceIncludesTax(),
//            'showIncludeTax'    => $taxHelper->displayPriceIncludingTax(),
//            'showBothPrices'    => $taxHelper->displayBothPrices(),
//            'defaultTax'        => $defaultTax,
//            'currentTax'        => $currentTax,
//            'inclTaxTitle'      => Mage::helper('catalog')->__('Incl. Tax')
//        );
//
//        $config = array(
//            'attributes'        => $attributes,
//            'template'          => str_replace('%s', '#{price}', $store->getCurrentCurrency()->getOutputFormat()),
//            'basePrice'         => 0,//$this->_registerJsPrice($this->_convertPrice($currentProduct->getFinalPrice())),
//            'oldPrice'          => 0,//$this->_registerJsPrice($this->_convertPrice($currentProduct->getPrice())),
//            'productId'         => $currentProduct->getId(),
//            'chooseText'        => Mage::helper('catalog')->__('Choose an Option...'),
//            'taxConfig'         => $taxConfig,
//            'priceFormat'       => Mage::app()->getLocale()->getJsPriceFormat()
//        );
//
//        if ($preconfiguredFlag && !empty($defaultValues)) {
//            $config['defaultValues'] = $defaultValues;
//        }
//
//        $config = array_merge($config, $this->_getAdditionalConfig());
//        if(Mage::helper('catalog/product')->getSkipSaleableCheck()) {
//			Mage::helper('catalog/product')->setSkipSaleableCheck(false);
//        }
//        return Mage::helper('core')->jsonEncode($config);
//    }

    /**
     *  Recustom Json Config
     *
     * @return string
     */
    public function getJsonConfig()
    {
        $attributes = array();
        $options    = array();
        $store      = $this->getCurrentStore();
        $taxHelper  = Mage::helper('tax');
        $currentProduct = $this->getProduct();

        $preconfiguredFlag = $currentProduct->hasPreconfiguredValues();
        if ($preconfiguredFlag) {
            $preconfiguredValues = $currentProduct->getPreconfiguredValues();
            $defaultValues       = array();
        }

        foreach ($this->getAllowProducts() as $product) {
            $productId  = $product->getId();
            // *************
            $prodPrice = ($product->getMsrp()>0)?$product->getMsrp():$product->getPrice();
            $prodFinalPrice = $product->getFinalPrice();
            $prodWeight = $product->getWeight();
            // $prodWeight = $product->getWeight();

            $count = 0;
            foreach ($this->getAllowAttributes() as $attribute) {
                $productAttribute   = $attribute->getProductAttribute();
                $productAttributeId = $productAttribute->getId();
                $attributeValue     = $product->getData($productAttribute->getAttributeCode());
                if (!isset($options[$productAttributeId])) {
                    $options[$productAttributeId] = array();
                }

                if (!isset($options[$productAttributeId][$attributeValue])) {
                    $options[$productAttributeId][$attributeValue] = array();
                }
                $options[$productAttributeId][$attributeValue][] = $productId;
                // *************
                if (!isset($optionsProd[$productAttributeId])) {
                    $optionsProd[$productAttributeId] = array();
                }
                if (!isset($optionsProd[$productAttributeId][$attributeValue])) {
                    $optionsProd[$productAttributeId][$attributeValue] = array();
                }
                if($count == 0){
                    $optionsProd[$productAttributeId][$attributeValue]['regularPrice'] = $prodPrice;
                }else{
                    $optionsProd[$productAttributeId][$attributeValue]['regularPrice'] = 0;
                }
                $optionsProd[$productAttributeId][$attributeValue]['specialPrice'] = $prodFinalPrice;
                $optionsProd[$productAttributeId][$attributeValue]['weight'] = $prodWeight;
                $count++;
                // *************
            }
        }

        $this->_resPrices = array(
            $this->_preparePrice($currentProduct->getFinalPrice())
        );

        foreach ($this->getAllowAttributes() as $attribute) {
            $productAttribute = $attribute->getProductAttribute();
            $attributeId = $productAttribute->getId();
            $info = array(
                'id'        => $productAttribute->getId(),
                'code'      => $productAttribute->getAttributeCode(),
                'label'     => $attribute->getLabel(),
                'options'   => array()
            );

            $optionPrices = array();
            $prices = $attribute->getPrices();

            if (is_array($prices)) {
                foreach ($prices as $value) {
                    if(!$this->_validateAttributeValue($attributeId, $value, $options)) {
                        continue;
                    }
                    $currentProduct->setConfigurablePrice(
                        $this->_preparePrice($value['pricing_value'], $value['is_percent'])
                    );
                    $currentProduct->setParentId(true);
                    Mage::dispatchEvent(
                        'catalog_product_type_configurable_price',
                        array('product' => $currentProduct)
                    );
                    $configurablePrice = $currentProduct->getConfigurablePrice();

                    // *************
                    $price = $optionsProd[$attributeId][$value['value_index']]['specialPrice'];
                    $oldPrice = $optionsProd[$attributeId][$value['value_index']]['regularPrice'];
                    // *************

                    if (isset($options[$attributeId][$value['value_index']])) {
                        $productsIndex = $options[$attributeId][$value['value_index']];
                    } else {
                        $productsIndex = array();
                    }

                    $info['options'][] = array(
                        'id'        => $value['value_index'],
                        'label'     => $value['label'],
                        'price'     => $configurablePrice,
//                        'oldPrice'  => $this->_prepareOldPrice($value['pricing_value'], $value['is_percent']),
                        // *************
                        'oldPrice'  => $this->_preparePrice($oldPrice, false),
                        'weight'    => $optionsProd[$productAttributeId][$value['value_index']]['weight'],
                        'products'  => $productsIndex,
                        // *************
                    );
                    $optionPrices[] = $configurablePrice;
                }
            }

            /**
             * Prepare formated values for options choose
             */
            foreach ($optionPrices as $optionPrice) {
                foreach ($optionPrices as $additional) {
                    $this->_preparePrice(abs($additional-$optionPrice));
                }
            }
            if($this->_validateAttributeInfo($info)) {
                $attributes[$attributeId] = $info;
            }

            // Add attribute default value (if set)
            if ($preconfiguredFlag) {
                $configValue = $preconfiguredValues->getData('super_attribute/' . $attributeId);
                if ($configValue) {
                    $defaultValues[$attributeId] = $configValue;
                }
            }
        }

        $taxCalculation = Mage::getSingleton('tax/calculation');
        if (!$taxCalculation->getCustomer() && Mage::registry('current_customer')) {
            $taxCalculation->setCustomer(Mage::registry('current_customer'));
        }

        $_request = $taxCalculation->getRateRequest(false, false, false);
        $_request->setProductClassId($currentProduct->getTaxClassId());
        $defaultTax = $taxCalculation->getRate($_request);

        $_request = $taxCalculation->getRateRequest();
        $_request->setProductClassId($currentProduct->getTaxClassId());
        $currentTax = $taxCalculation->getRate($_request);

        // ************************
        // Get associated products list
        $associatedProductsList = array();
        if($currentProduct->getTypeId() == 'configurable'){
            $associatedProducts = $currentProduct->getTypeInstance()->getUsedProducts();
            foreach($associatedProducts as $simpleProduct){
                /** @var Mage_Catalog_Model_Product $simpleProduct */
                $associatedProductsList[$simpleProduct->getId()] = array(
                    'id' => $simpleProduct->getId(),
                    'final_price'=> $this->_registerJsPrice($this->_convertPrice($simpleProduct->getFinalPrice())),
                    'msrp' => $this->_registerJsPrice($this->_convertPrice($simpleProduct->getMsrp())),
                    'name' => $simpleProduct->getTitle() != null ? $simpleProduct->getTitle() : $simpleProduct->getName(),
                    'reward_points' => Mage::helper('rewardpoints/data')->getProductPoints($simpleProduct, true)
                );

                foreach ($currentProduct->getTypeInstance(true)->getConfigurableAttributesAsArray($currentProduct) as $attribute){
                    $associatedProductsList[$simpleProduct->getId()]['attr'][] = array(
                        $attribute['attribute_code'] => $simpleProduct->getData($attribute['attribute_code']),
                    );
                }
            }
        }
        // ************************

        $taxConfig = array(
            'includeTax'        => $taxHelper->priceIncludesTax(),
            'showIncludeTax'    => $taxHelper->displayPriceIncludingTax(),
            'showBothPrices'    => $taxHelper->displayBothPrices(),
            'defaultTax'        => $defaultTax,
            'currentTax'        => $currentTax,
            'inclTaxTitle'      => Mage::helper('catalog')->__('Incl. Tax')
        );

        $config = array(
            'attributes'        => $attributes,
            'configurable'      => $associatedProductsList,
            'template'          => str_replace('%s', '#{price}', $store->getCurrentCurrency()->getOutputFormat()),
            'basePrice'         => $this->_registerJsPrice($this->_convertPrice($currentProduct->getFinalPrice())),
            'oldPrice'          => $this->_registerJsPrice($this->_convertPrice($currentProduct->getMsrp())),
            'productId'         => $currentProduct->getId(),
            'chooseText'        => Mage::helper('catalog')->__('Choose an Option...'),
            'taxConfig'         => $taxConfig
        );

        if ($preconfiguredFlag && !empty($defaultValues)) {
            $config['defaultValues'] = $defaultValues;
        }

        $config = array_merge($config, $this->_getAdditionalConfig());

        return Mage::helper('core')->jsonEncode($config);
    }

    public function getJsPoints($_product)
    {
        $attributes = array();
        $attribute_credit = array();

        // if ($_product->isConfigurable()){
            /*$allProducts = $_product->getTypeInstance(true)
                            ->getUsedProducts(null, $product);
            $allowAttributes = $_product->getTypeInstance(true)
                        ->getConfigurableAttributes($product);*/
            $allProducts = $this->getAllowProducts();
            $allowAttributes = $this->getAllowAttributes();
            
            
            foreach ($allProducts as $product) {
                if ($product->isSaleable()) {
                    $attr_values = array();
                    foreach ($allowAttributes as $attribute) {
                        $productAttribute = $attribute->getProductAttribute();
                        $attributeId = $productAttribute->getId();
                        $attributeValue = $product->getData($productAttribute->getAttributeCode());
                        $attr_values[] = $attributeValue;
                    }
                    $return_val[implode("|",$attr_values)] = Mage::helper('rewardpoints/data')->getProductPoints($product, false, false);
                }
            }

            return Mage::helper('core')->jsonEncode($return_val);
            
            // end of modifications
            
            
            foreach ($allProducts as $product) {
                if ($product->isSaleable()) {
                    foreach ($allowAttributes as $attribute) {
                        $productAttribute = $attribute->getProductAttribute();
                        $attributeId = $productAttribute->getId();
                        $attributeValue = $product->getData($productAttribute->getAttributeCode());
                        
                        if (!isset($options[$productAttribute->getId()])) {
                            $options[$productAttribute->getId()] = array();
                        }

                        if (!isset($options[$productAttribute->getId()][$attributeValue])) {
                            $options[$productAttribute->getId()][$attributeValue] = array();
                        }
                        
                        
                        $attribute_credit[$attributeValue] = Mage::helper('rewardpoints/data')->getProductPoints($product, false, false);
                        
                        $prices = $attribute->getPrices();
                        if (is_array($prices)) {
                            $attr_list = array();
                            foreach ($prices as $value) {
                                if(!isset($options[$attributeId][$value['value_index']])) {
                                    continue;
                                }
                                $price = $value['pricing_value'];
                                $isPercent = $value['is_percent'];
                                if ($isPercent && !empty($price)) {
                                    $price = $_product->getFinalPrice()*$price/100;
                                }
                                
                                if (!isset($attribute_credit[$attributeValue])){
                                    $attribute_credit[$attributeValue] = array();
                                }
                                $attr_list[] = $value['value_index'];
                            }
                        }
                    }
                }
            }
        // }

        return Mage::helper('core')->jsonEncode($attribute_credit);
    }
}
