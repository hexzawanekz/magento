<?php

class Petloft_Catalog_Model_Cache_Resource_Cache extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('petloftcatalog_menucache/cache', 'id');
    }
}