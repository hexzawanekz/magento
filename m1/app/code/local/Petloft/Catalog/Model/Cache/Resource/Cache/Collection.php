<?php

class Petloft_Catalog_Model_Cache_Resource_Cache_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    protected function _construct()
    {
        parent::_construct();
        $this->_init('petloftcatalog_menucache/cache', 'petloftcatalog_menucache/cache');
    }
}