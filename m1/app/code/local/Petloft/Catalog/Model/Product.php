<?php

class Petloft_Catalog_Model_Product extends Mage_Catalog_Model_Product
{
	public function getAdminAttributeText($attributeCode)
	{
		$_attribute= $this->getResource()->getAttribute($attributeCode);
		$_options= $_attribute->getSource()->getAllOptions(true, true);
		foreach($_options as $option) {
			if ($option['value'] == $this->getData($attributeCode))
			{
				return $option['label'];
			}
		}
		return false;
	}

	public function getAttributePair($attributeCode){
    	$arr_front_options = $this->getResource()->getAttribute($attributeCode)->getSource()->getAllOptions();
		$_attribute= $this->getResource()->getAttribute($attributeCode);
  		$arr_admin_options = $_attribute->getSource()->getAllOptions(true, true);
		$attribute_ids = $this->getData($attributeCode);
		$arr_attribute_ids = explode(',', $attribute_ids);

		$arr_attribute_set = array();
		foreach ($arr_attribute_ids as $key => $value) {
			foreach ($arr_admin_options as $value2) {
				if ($value == $value2['value']) {
					$arr_attribute_set[$key]['value'] = $value2['label'];
				}
			}
			foreach ($arr_front_options as $value3) {
				if ($value == $value3['value']) {
					$arr_attribute_set[$key]['label'] = $value3['label'];
				}
			}
		}

		return $arr_attribute_set;
    }

}