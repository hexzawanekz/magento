<?php

class Petloft_Catalog_Model_Navigation_Cache extends Mage_Core_Model_Abstract
{
    protected $_tableName = 'petloftcatalog_menu_navigation_cache';

    // resource
    protected $_resource = null;
    protected $_readConnection = null;
    protected $_writeConnection = null;

    public function _construct()
    {
        // init resource
        $this->_resource = Mage::getSingleton('core/resource');
        $this->_readConnection = $this->_resource->getConnection('core_read');
        $this->_writeConnection = $this->_resource->getConnection('core_write');
    }

    public function deleteCache($path)
    {
        try {
            $sql = "DELETE FROM `{$this->_tableName}` WHERE `path` LIKE '{$path}%';";
            $this->_writeConnection->query($sql);
        } catch (Exception $e) {
            Mage::log($e->getMessage() . ' - File: ' . $e->getFile() . ' - Line: ' . $e->getLine(), null, 'menu_caching.log');
        }
    }

    public function deleteAllCache()
    {
        try {
            $sql = "DELETE FROM `{$this->_tableName}`;";
            $this->_writeConnection->query($sql);
        } catch (Exception $e) {
            Mage::log($e->getMessage() . ' - File: ' . $e->getFile() . ' - Line: ' . $e->getLine(), null, 'menu_caching.log');
        }
    }

    public function checkCache($path)
    {
        try {
            $sql = "SELECT `path` FROM `{$this->_tableName}` WHERE `path` = '{$path}';";
            $result = $this->_readConnection->fetchAll($sql);
            return is_array($result) && count($result) > 0 ? true : false;
        } catch (Exception $e) {
            Mage::log($e->getMessage() . ' - File: ' . $e->getFile() . ' - Line: ' . $e->getLine(), null, 'menu_caching.log');
            return false;
        }
    }

    public function createCache($path, $content)
    {
        try {
            /** @var Petloft_Catalog_Model_Cache_Cache $obj */
            $obj = Mage::getModel('petloftcatalog_menucache/cache');
            $obj->setData(array(
                'path' => $path,
                'html_cache' => htmlspecialchars($content),
            ));
            $obj->save();
        } catch (Exception $e) {
            Mage::log($e->getMessage() . ' - File: ' . $e->getFile() . ' - Line: ' . $e->getLine(), null, 'menu_caching.log');
        }
    }

    public function getCache($path)
    {
        $cache = Mage::app()->getCache();
        $navCache = $cache->load('nav_cache_'.$path);

        if ($navCache) return $navCache;

        try {
            /** @var Petloft_Catalog_Model_Cache_Cache $obj */
            $obj = Mage::getModel('petloftcatalog_menucache/cache')
                ->getCollection()
                ->addFieldToSelect('*')
                ->addFieldToFilter('path', $path)
                ->getLastItem();
            if ($obj->getData('id')) {
                $data = htmlspecialchars_decode($obj->getData('html_cache'));
                $cache->save($data, 'nav_cache_'.$path, array('nav_cache'), 86400);
                return $data;
            } else {
                return '';
            }
        } catch (Exception $e) {
            Mage::log($e->getMessage() . ' - File: ' . $e->getFile() . ' - Line: ' . $e->getLine(), null, 'menu_caching.log');
            return '';
        }
    }
}
