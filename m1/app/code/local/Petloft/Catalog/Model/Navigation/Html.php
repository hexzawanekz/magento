<?php

class Petloft_Catalog_Model_Navigation_Html extends Mage_Core_Model_Abstract
{
    protected $_enStoreCode = 'moxyen';
    protected $_thStoreCode = 'moxyth';
    protected $_config = null;
    protected $_mobileCategoryInfo = array();
    protected $_passedCategories = array();
    protected $_activatedStores = null;

    const XML_ENABLE_MENU_CACHE         = 'petloftcatalog/menu_cache/enabled';
    const XML_ENABLE_MENU_LAST_LINK     = 'petloftcatalog/menu_cache/enable_link_at_last';
    const XML_MENU_LAST_LINK_LABEL      = 'petloftcatalog/menu_cache/label_link_at_last';
    const XML_MENU_LAST_LINK_ADDRESS    = 'petloftcatalog/menu_cache/link_at_last';

    public function _construct()
    {
        if ($config = Mage::getStoreConfig(self::XML_ENABLE_MENU_CACHE)){$this->_config = $config;}
    }

    protected function _getActivatedStores()
    {
        if(!is_array($this->_activatedStores)){
            /** @var Mage_Core_Model_Resource_Store_Collection $collection */
            $collection = Mage::getModel('core/store')->getCollection()
                ->addFieldToSelect('*')
                ->addFieldToFilter('store_id', array('neq' => 0)) // avoid admin store id
                ->addFieldToFilter('is_active', array('eq' => 1)); // filter by activated stores

            if($collection->count() > 0){
                foreach($collection as $store){
                    /** @var Mage_Core_Model_Store $store */
                    if($rootCateId = $store->getRootCategoryId()){
                        $this->_activatedStores[$store->getData('code')] = $rootCateId;
                    }
                }
            }else{
                $this->_activatedStores = array();
            }
        }
        return $this->_activatedStores;
    }

    /**
     * @param $key (as category ID or url-key)
     * @return bool|string
     */
    public function getMenuHtmlByKey()
    {
        try{
            if($cateRootId = Mage::app()->getStore()->getRootCategoryId()){
                /** @var Mage_Catalog_Model_Category $cateObj */
                $cateObj = Mage::getModel('catalog/category');
                ///////////////////////////////////////////////////////////
                $activeCategories = $this->_getActiveCategoriesByKey($cateRootId);
                $all_categories = count($activeCategories) > 0 ? $this->_getCategories($activeCategories) : array();
                $html = count($all_categories) > 0 ? $this->_renderHtml($all_categories, $cateObj, Mage::app()->getStore()->getId()) : '';
                return $html ? $html : false;
            }
            return false;
        }catch (Exception $e){
            Mage::log($e->getMessage() . ' - File: ' . $e->getFile() . ' - Line: ' . $e->getLine(), null, 'menu_caching.log');
            return false;
        }
    }

    /*For Mobile*/
    public function cacheMobileRender(){
        try {
            $storeCode = Mage::app()->getStore()->getCode();
            $rootCateId = Mage::app()->getStore()->getRootCategoryId();
            if($storeCode && $rootCateId){
                // set current store
                $curStoreId = Mage::app()->getStore($storeCode)->getId();

                /**
                 * Get child categories from root
                 */

                /** @var Mage_Catalog_Model_Resource_Category_Collection $categoryCollections */
                /** @var Mage_Catalog_Model_Category $rootCategory */
                $rootCategory = Mage::getModel('catalog/category')->getCollection()
                    ->setStoreId($curStoreId)
                    ->addAttributeToSelect(array('url_key', 'name'))
                    ->addFieldToFilter('is_active', 1)
                    ->addAttributeToFilter('entity_id', $rootCateId)
                    //->addAttributeToFilter('level', 2)
                    //->addAttributeToFilter('parent_id', array('eq' => $this->_getRootCategory()->getId()))
                    ->getFirstItem();

                $rootPath = $rootCategory->getPath();

                $this->_mobileCategoryInfo[$rootCategory->getId()] = array(
                    'id' => $rootCategory->getId(),
                    'name' => $rootCategory->getName(),
                    'url_key' => $rootCategory->getUrlKey(),
                    'url' => $rootCategory->getUrl(),
                    'root' => true,
                    'children' => array()
                );

                $subCategoryCollections = Mage::getModel('catalog/category')->getCollection()
                    ->setStoreId($curStoreId)
                    ->addAttributeToSelect(array('name', 'url_key'))
                    ->addAttributeToFilter('path', array('like' => $rootPath . '%'))
                    ->addAttributeToFilter('level', array('or' => array(2, 3, 4, 5)))
                    ->addFieldToFilter('is_active', 1)
                    ->addAttributeToFilter('include_in_menu', 1)
                    ->setOrder('level')
                    ->setOrder('position');

                /** @var Mage_Catalog_Model_Category $_subCategory */
                foreach ($subCategoryCollections as $_subCategory) {
                    $categoryPath = $_subCategory->getPathIds();
                    $this->_mobileCategoryInfo[$_subCategory->getId()] = array(
                        'id' => $_subCategory->getId(),
                        'name' => $_subCategory->getName(),
                        'url_key' => $_subCategory->getUrlKey(),
                        'url' => $_subCategory->getUrl(),
                        'children' => array()
                    );

                    $this->_mobileCategoryInfo[$categoryPath[count($categoryPath) - 2]]['children'][] = $_subCategory->getId();
                }

                foreach ($this->_mobileCategoryInfo as $_category) {
                    if (isset($_category['root']) && $_category['root'] === true) {
                        $html = '';

                        foreach ($_category['children'] as $_childCategory) {
                            $html .= $this->renderMobileMenu(0, $_childCategory, $storeCode);
                        }

                        // add Magazine link
                        if(Mage::getStoreConfig(self::XML_ENABLE_MENU_LAST_LINK)){
                            $magazineLbl = Mage::getStoreConfig(self::XML_MENU_LAST_LINK_LABEL);
                            $magazineLink = Mage::getStoreConfig(self::XML_MENU_LAST_LINK_ADDRESS);
                            $html .= '<li class="level0 level-top"><a class="level-top" href="'. $magazineLink .'"><span></span><span>'. $magazineLbl .'</span></a></li>';
                        }
                    }
                }
            }
            return $html;
        } catch (Exception $e) {
            return 'Error: '. $e->getMessage();
        }
    }

    public function updateMobile(){
        try {
            ini_set('max_execution_time', 0);
            ini_set('memory_limit', '-1');

            foreach ($this->_getActivatedStores() as $storeCode => $rootCateId) {
                // set current store
                $curStoreId = Mage::app()->getStore($storeCode)->getId();
                Mage::app()->setCurrentStore($curStoreId);

                /**
                 * Get child categories from root
                 */

                /** @var Mage_Catalog_Model_Resource_Category_Collection $categoryCollections */
                /** @var Mage_Catalog_Model_Category $rootCategory */
                $rootCategory = Mage::getModel('catalog/category')->getCollection()
                    ->setStoreId($curStoreId)
                    ->addAttributeToSelect(array('url_key', 'name'))
                    ->addFieldToFilter('is_active', 1)
                    ->addAttributeToFilter('entity_id', $rootCateId)
                    //->addAttributeToFilter('level', 2)
                    //->addAttributeToFilter('parent_id', array('eq' => $this->_getRootCategory()->getId()))
                    ->getFirstItem();

                $rootPath = $rootCategory->getPath();

                $this->_mobileCategoryInfo[$rootCategory->getId()] = array(
                    'id' => $rootCategory->getId(),
                    'name' => $rootCategory->getName(),
                    'url_key' => $rootCategory->getUrlKey(),
                    'url' => $rootCategory->getUrl(),
                    'root' => true,
                    'children' => array()
                );

                $subCategoryCollections = Mage::getModel('catalog/category')->getCollection()
                    ->setStoreId($curStoreId)
                    ->addAttributeToSelect(array('name', 'url_key'))
                    ->addAttributeToFilter('path', array('like' => $rootPath . '%'))
                    ->addAttributeToFilter('level', array('or' => array(2, 3, 4, 5)))
                    ->addFieldToFilter('is_active', 1)
                    ->addAttributeToFilter('include_in_menu', 1)
                    ->setOrder('level')
                    ->setOrder('position');

                /** @var Mage_Catalog_Model_Category $_subCategory */
                foreach ($subCategoryCollections as $_subCategory) {
                    $categoryPath = $_subCategory->getPathIds();
                    $this->_mobileCategoryInfo[$_subCategory->getId()] = array(
                        'id' => $_subCategory->getId(),
                        'name' => $_subCategory->getName(),
                        'url_key' => $_subCategory->getUrlKey(),
                        'url' => $_subCategory->getUrl(),
                        'children' => array()
                    );

                    $this->_mobileCategoryInfo[$categoryPath[count($categoryPath) - 2]]['children'][] = $_subCategory->getId();
                }

                /**
                 * End to get child categories
                 */

                //Save to Database
                /** @var Petloft_Catalog_Model_Navigation_Renderer $renderer */
                $renderer = Mage::getModel('petloftcatalog/navigation_renderer');

                foreach ($this->_mobileCategoryInfo as $_category) {
                    if (isset($_category['root']) && $_category['root'] === true) {
                        $html = '';

                        foreach ($_category['children'] as $_childCategory) {
                            $html .= $this->renderMobileMenu(0, $_childCategory, $storeCode);
                        }

                        // add Magazine link
                        if(Mage::getStoreConfig(self::XML_ENABLE_MENU_LAST_LINK)){
                            $magazineLbl = Mage::getStoreConfig(self::XML_MENU_LAST_LINK_LABEL);
                            $magazineLink = Mage::getStoreConfig(self::XML_MENU_LAST_LINK_ADDRESS);
                            $html .= '<li class="level0 level-top"><a class="level-top" href="'. $magazineLink .'"><span></span><span>'. $magazineLbl .'</span></a></li>';
                        }

                        $renderer->renderMenuNavigationHtml($html, $storeCode . "/" . Petloft_Catalog_Model_Navigation_Renderer::MOBILE_FILENAME);
                    }
                }

                $this->_mobileCategoryInfo = null;

                Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
            }

            return true;
        } catch (Exception $e) {
            return 'Error: '. $e->getMessage();
        }
    }

    public function renderMobileMenu($level, $categoryId, $storeCode){
        $html = "";

        if($storeCode == $this->_enStoreCode){
            $this->_mobileCategoryInfo[$categoryId]['url'] = str_replace('/th/', '/en/', $this->_mobileCategoryInfo[$categoryId]['url']);
        }elseif($storeCode == $this->_thStoreCode){
            $this->_mobileCategoryInfo[$categoryId]['url'] = str_replace('/en/', '/th/', $this->_mobileCategoryInfo[$categoryId]['url']);
        }

        //Modifying link of brands category
        $tmpBrandUrl = null;
        if (strpos($this->_mobileCategoryInfo[$categoryId]['url'], '/brands.html')) {
            $a = explode("/", $this->_mobileCategoryInfo[$categoryId]['url']);
            $a = $a[sizeof($a) - 2];
            $baseUrl = Mage::app()->getStore($storeCode)->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_LINK);
            $tmpBrandUrl = $baseUrl . 'all-brands-' . $a;
            $this->_mobileCategoryInfo[$categoryId]['children'] = array();
        }

        if (strpos($this->_mobileCategoryInfo[$categoryId]['url'], '/deal.html')) {
            $html .= "<li class=\"level$level". (($level == 0) ? " level-top" : "") ."\"><a style='color:#FF6C6C' class=\"".(($level == 0) ? "level-top {$this->_mobileCategoryInfo[$categoryId]['url_key']}" : "")."\" href=\"".($tmpBrandUrl ? $tmpBrandUrl : $this->_mobileCategoryInfo[$categoryId]['url'])."\"><span></span><span>{$this->_mobileCategoryInfo[$categoryId]['name']}</span></a>";

        }else{
            $html .= "<li class=\"level$level". (($level == 0) ? " level-top" : "") ."\"><a class=\"".(($level == 0) ? "level-top {$this->_mobileCategoryInfo[$categoryId]['url_key']}" : "")."\" href=\"".($tmpBrandUrl ? $tmpBrandUrl : $this->_mobileCategoryInfo[$categoryId]['url'])."\"><span></span><span>{$this->_mobileCategoryInfo[$categoryId]['name']}</span></a>";

        }

        if(count($this->_mobileCategoryInfo[$categoryId]['children']) > 0){
            $html .= "<ul class=\"level$level\" style=\"display: none;\">";

            /*
             * Pass by "Shop By" category, and display their subs
             */
            $firstItemId = $this->_mobileCategoryInfo[$categoryId]['children'][0];
            // first condition
            $checkTotal = count($this->_mobileCategoryInfo[$categoryId]['children']) == 1 ? true : false;
            // second condition
            $checkFirstItem = ($this->_mobileCategoryInfo[$firstItemId]['url_key'] == 'shop-by') ? true : false;
            // check both conditions to pass this case
            if($checkTotal && $checkFirstItem){
                foreach($this->_mobileCategoryInfo[$firstItemId]['children'] as $_subCategoryId){
                    $html .= $this->renderMobileMenu($level+1, $_subCategoryId, $storeCode);
                }
            }else{
                foreach($this->_mobileCategoryInfo[$categoryId]['children'] as $_subCategoryId){
                    $html .= $this->renderMobileMenu($level+1, $_subCategoryId, $storeCode);
                }
            }


            $html .= "</ul>";
        }


        $html .= "</li>";
        return $html;
    }
    /*End For Mobile*/

    public function update()
    {
        // set max_execution_time
        ini_set('max_execution_time', 0);

        // set memory_limit
        ini_set('memory_limit', '-1');

        try{
            $result = array();
            if(count($this->_getActivatedStores()) > 0){
                foreach($this->_getActivatedStores() as $storeCode => $rootCateId){
                    $result[$storeCode] = $this->process($storeCode, $rootCateId);
                }
            }
            return count($result) > 0 ? $result : false;
        }catch (Exception $e){
            Mage::log($e->getMessage() . ' - File: ' . $e->getFile() . ' - Line: ' . $e->getLine(), null, 'menu_caching.log');
            return $e->getMessage();
        }
    }

    /**
     * @param $storeCode
     * @param $rootCateId
     * @return bool|string
     */
    protected function process($storeCode, $rootCateId)
    {
        try{
            /** @var Petloft_Catalog_Model_Category $cateObj */
            $cateObj = Mage::getModel('catalog/category');
            if (is_numeric($rootCateId)) {
                $cateObj->load($rootCateId);
            }

            if ($id = $cateObj->getId()) {

                /** @var Petloft_Catalog_Model_Navigation_Renderer $renderer */
                $renderer = Mage::getModel('petloftcatalog/navigation_renderer');

                // set current store
                $curStoreId = Mage::app()->getStore($storeCode)->getId();
                Mage::app()->setCurrentStore($curStoreId);

                if ($this->_config == true) {

                    ///////////////////////////////////////////////////////////
                    $activeCategories = $this->_getActiveCategoriesByKey($id);
                    $all_categories = count($activeCategories) > 0 ? $this->_getCategories($activeCategories) : array();
                    $html = count($all_categories) > 0 ? $this->_renderHtml($all_categories, $cateObj, $curStoreId) : '';
                    ///////////////////////////////////////////////////////////

                    // render menu to html content
                    if ($this->_config == true && $html != '') {
                        $renderer->renderMenuNavigationHtml($html, $storeCode . '/' . Petloft_Catalog_Model_Navigation_Renderer::DESKTOP_FILENAME);
                    }
                }

                // set current admin store
                Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
            }

            return true;
        }catch (Exception $e){
            Mage::log($e->getMessage() . ' - File: ' . $e->getFile() . ' - Line: ' . $e->getLine(), null, 'menu_caching.log');
            return false;
        }
    }

    protected function _renderHtml($arr, $currentCate, $store)
    {
        /** @var Mage_Catalog_Model_Category $currentCate */

        $baseUrl = Mage::app()->getStore($store)->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_LINK);
        $html = '';
        foreach ($arr as $keyCat => $category) {
            $urlRightMenu = $category['url'];

            if(Mage::app()->getStore($store)->getCode() == $this->_enStoreCode){
                $category['url'] = str_replace('/th/', '/en/', $category['url']);
            }elseif(Mage::app()->getStore($store)->getCode() == $this->_thStoreCode){
                $category['url'] = str_replace('/en/', '/th/', $category['url']);
            }

            //Modifying link of brands category
            $isBrandsCategory = false;
            if (strpos($category['url'], '/brands.html')) {
                $a = explode("/", $category['url']);
                $a = $a[sizeof($a) - 2];
                $category['url'] = $baseUrl . 'all-brands-' . $a;
                $category['child'] = array();
                $isBrandsCategory = true;
            }

            if (in_array($category['id'], $currentCate->getPathIds())) {
                $html .= '<li class="main-nav-item ';
                if (count($category['child'])) {
                    $html .= ' has-child';
                }
                $html .= '" contextmenu="category_' . $category['id'] . '">';
            } else {
                if ($isBrandsCategory) {
                    $html .= '<li class="main-nav-item all_brands ';
                    if (count($category['child'])) {
                        $html .= ' has-child';
                    }
                    $html .= '" contextmenu="category_' . $category['id'] . '">';
                } else {
                    $html .= '<li class="main-nav-item ';
                    if (count($category['child'])) {
                        $html .= ' has-child';
                    }
                    $html .= '" contextmenu="category_' . $category['id'] . '">';
                }
            }
            $total_child = count($category['child']);
            if (strpos($category['url'], '/deal.html')) {
                $html .= '<a style="color:#FF6C6C" href="' . $category['url'] . '" class="main-nav-tab"><span>' . $category['name'] . '</span>' . '</a>';
            }else{
                $html .= '<a href="' . $category['url'] . '" class="main-nav-tab"><span>' . $category['name'] . '</span>' . '</a>';
            }
            if ($total_child != 0) {
                $arrColumns = array();
                $setColumnAuto = 1;
                foreach ($category['child'] as $keyChild1 => $child1) {

                    if(Mage::app()->getStore($store)->getCode() == $this->_enStoreCode){
                        $child1['url'] = str_replace('/th/', '/en/', $child1['url']);
                    }elseif(Mage::app()->getStore($store)->getCode() == $this->_thStoreCode){
                        $child1['url'] = str_replace('/en/', '/th/', $child1['url']);
                    }

                    //Modifying link of brands category
                    if (strpos($child1['url'], '/brands.html')) {
                        $a = explode("/", $child1['url']);
                        $a = $a[sizeof($a) - 2];
                        $child1['url'] = $baseUrl . 'all-brands-' . $a;
                        $child1['child'] = array();
                    }

                    $subHtml = '';
                    //////////
                    $subHtml .= '<div><h3><a href="' . $child1['url'] . '" title="' . $child1['name'] . '">' . $child1['name'] . '</a></h3>';
                    if (in_array($child1['id'], $currentCate->getPathIds())) {
                        $subHtml .= '<ul class="active">';
                    } else {
                        $subHtml .= '<ul>';
                    }

                    foreach ($child1['child'] as $key => $newChild) {

                        if(Mage::app()->getStore($store)->getCode() == $this->_enStoreCode){
                            $newChild['url'] = str_replace('/th/', '/en/', $newChild['url']);
                        }elseif(Mage::app()->getStore($store)->getCode() == $this->_thStoreCode){
                            $newChild['url'] = str_replace('/en/', '/th/', $newChild['url']);
                        }

                        if (in_array($newChild['id'], $currentCate->getPathIds())) {
                            $subHtml .= '<li class="active">';
                        } else {
                            $subHtml .= '<li>';
                        }
                        $subHtml .= '<a href="' . $newChild['url'] . '" title="' . $newChild['name'] . '">' . $newChild['name'] . '</a></li>';
                    }

                    $subHtml .= '</ul></div>';

                    //$columnThisCategory = $categoryColumns[$child1['id']];
                    $columnThisCategory = null;
                    if ($columnThisCategory == null) {
                        $columnThisCategory = $setColumnAuto;
                        $setColumnAuto += 1;
                    }
                    $arrColumns[$columnThisCategory] .= $subHtml;
                    if ($setColumnAuto == 6) {
                        $setColumnAuto = 1;
                    }
                }

                //get html menu
                $i = 1;
                ksort($arrColumns);
                $totalColumns = count($arrColumns);

                $html .= '<div class="dropdown-outter-wrapper">';
                $html .= '<div class="dropdown-wrapper column_' . $totalColumns . '">';

                foreach ($arrColumns as $column) {
                    $html .= '<div class="column">';
                    $html .= $column;
                    $html .= '</div>';
                    $i++;
                }

                $html .= '</div>';
                $html .= '</div>';
            }
            $html .= '</li>';
        }

        // add Magazine link
        if(Mage::getStoreConfig(self::XML_ENABLE_MENU_LAST_LINK)){
            $magazineLbl = Mage::getStoreConfig(self::XML_MENU_LAST_LINK_LABEL);
            $magazineLink = Mage::getStoreConfig(self::XML_MENU_LAST_LINK_ADDRESS);
            $html .= '<li class="main-nav-item"><a href="'. $magazineLink .'" class="main-nav-tab"><span>'. $magazineLbl .'</span></a></li>';
        }

        return $html;
    }

    protected function _getActiveCategoriesByKey($key = null)
    {
        $activeCategories = array();

        if ($key) {
            $activeCategories = Mage::getModel('catalog/category')->getCategories($key, 0, 'position', false, true);
        } else {
            foreach (Mage::helper('catalog/category')->getStoreCategories('position') as $child) {
                if ($child->getIsActive()) {
                    $activeCategories[] = $child;
                }
            }
        }

        return $activeCategories;
    }

    protected function _getCategories($activeCategories)
    {
        $all_categories = array();

        $activeCategoriesCount = count($activeCategories);
        $hasActiveCategoriesCount = ($activeCategoriesCount > 0);

        if (!$hasActiveCategoriesCount) {
            return '';
        }

        foreach ($activeCategories as $idx => $category) {
            /** @var Mage_Catalog_Model_Category $category */
            $all_categories[$idx]['id'] = $category->getId();
            $all_categories[$idx]['name'] = $this->_escapeHtml($category->getName());
            $all_categories[$idx]['url'] = $this->_getCategoryUrl($category);
            $all_categories[$idx]['productCount'] = 0;
            $all_categories[$idx]['child'] = array();
            $children1 = $this->_getChildren($category);
            $children = array();
            $nav_no = 1;
            foreach ($children1 as $indx1 => $child1) {
                /** @var Mage_Catalog_Model_Category $child1 */
                $children[$nav_no]['id'] = $child1->getId();
                $children[$nav_no]['name'] = $this->_escapeHtml($child1->getName());
                $children[$nav_no]['url'] = $this->_getCategoryUrl($child1);
                $children[$nav_no]['productCount'] = 0;

                // Child of child
                $children[$nav_no]['child'] = array();
                $newChilds = $this->_getChildren($child1);
                $new_no = 1;
                foreach ($newChilds as $newChild) {
                    /** @var Mage_Catalog_Model_Category $newChild */
                    $children[$nav_no]['child'][$new_no]['id'] = $newChild->getId();
                    $children[$nav_no]['child'][$new_no]['name'] = $this->_escapeHtml($newChild->getName());
                    $children[$nav_no]['child'][$new_no]['url'] = $this->_getCategoryUrl($newChild);
                    $children[$nav_no]['child'][$new_no]['productCount'] = 0;
                    $new_no++;
                }
                $nav_no++;
            }
            // ksort($children);
            $all_categories[$idx]['child'] = $children;
        }

        return $all_categories;
    }

    /**
     * @return bool|Mage_Catalog_Model_Category
     */
    protected function _getRootCategory()
    {
        if ($cateId = Mage::app()->getStore($this->_enStoreCode)->getRootCategoryId()) {
            return Mage::getModel('catalog/category')->load($cateId);
        }
        return false;
    }

    protected function _escapeHtml($data, $allowedTags = null)
    {
        return Mage::helper('core')->escapeHtml($data, $allowedTags);
    }

    protected function _getCategoryUrl($category)
    {
        /** @var Mage_Catalog_Model_Category $category */
        if ($category instanceof Mage_Catalog_Model_Category) {
            $url = $category->getUrl();
        } else {
            $url = Mage::getModel('catalog/category')
                ->setData($category->getData())
                ->getUrl();
        }

        return $url;
    }

    protected function _getChildren($category)
    {
        if (Mage::helper('catalog/category_flat')->isEnabled()) {
            $children = (array)$category->getChildrenNodes();
        } else {
            $children = $category->getChildren();
        }
        if (!count($children)) $children = array();
        return $children;
    }

    protected function _getThumbnailUrl($category_id)
    {
        return Mage::getModel('catalog/category')->load($category_id)->getThumbnailUrl();
    }
}