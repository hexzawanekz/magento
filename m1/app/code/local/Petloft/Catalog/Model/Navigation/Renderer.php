<?php

class Petloft_Catalog_Model_Navigation_Renderer extends Mage_Core_Model_Abstract
{

    const DESKTOP_FILENAME  = 'desktop';
    const MOBILE_FILENAME   = 'mobile';

    /** @var Petloft_Catalog_Model_Navigation_Cache $_cacheObj */
    protected $_cacheObj;

    public function _construct()
    {
        $this->_cacheObj = Mage::getModel('petloftcatalog/navigation_cache');
    }

    /**
     * Event: remove navigation's html file by store id
     *
     * @param Varien_Event_Observer $observer
     */
    public function removeMenuNavigationHtml(Varien_Event_Observer $observer)
    {
        $category = $observer->getEvent()->getCategory();

        $rootCategoryIds = $this->_getCategoryIds();

        if (count($rootCategoryIds) > 0 && count($category->getParentIds()) > 0) {
            foreach($rootCategoryIds as $store_code => $root_id){
                if (in_array($root_id, $category->getParentIds())) {
                    $this->_cacheObj->deleteCache($store_code);
                }
            }
        }
    }

    /**
     * Return list of root category ids
     *
     * @return array
     */
    protected function _getCategoryIds()
    {
        $stores = Mage::getModel('core/store')
            ->getCollection()
            ->addFieldToSelect('code')
            ->addFieldToFilter('store_id', array('neq' => 0))
            ->addFieldToFilter('is_active', 1)
            ->join(
                array('group' => 'core/store_group'),
                'main_table.group_id = group.group_id',
                array('root_category_id' => 'group.root_category_id')
            );

        $rootCategoryIds = array();

        foreach($stores as $store){
            if($store->getData('root_category_id') && $store->getData('code'))
                $rootCategoryIds[$store->getData('code')] = $store->getData('root_category_id');
        }

        unset($stores);

        return $rootCategoryIds;
    }

    /**
     * Generate menu html to file
     *
     * @param $html
     * @param $store_code
     */
    public function renderMenuNavigationHtml($html, $store_code)
    {
        $this->_cacheObj->createCache($store_code, $html);
    }

    /**
     * Get menu html from static file content
     *
     * @param $store_code
     * @return string
     */
    public function toHtml($store_code)
    {
        return $this->_cacheObj->getCache($store_code);
    }

    /**
     * Delete all cache
     */
    public function deleteAll()
    {
        $this->_cacheObj->deleteAllCache();
    }

    /**
     * Delete cache by root category id
     */
    public function deleteCache($cate_id)
    {
        $rootCategoryIds = $this->_getCategoryIds();
        if (count($rootCategoryIds) > 0) {
            foreach($rootCategoryIds as $store_code => $root_id){
                if ($cate_id == $root_id) {
                    $this->_cacheObj->deleteCache($store_code);
                }
            }
        }
    }

    /**
     * Check cache
     *
     * @param $store_code
     * @return bool
     */
    public function checkExists($store_code)
    {
        return $this->_cacheObj->checkCache($store_code);
    }
}