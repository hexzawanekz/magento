<?php

class Petloft_Catalog_Model_Category extends Mage_Catalog_Model_Category
{
	public function getThumbnailUrl()
	{
		$url = false;
		if ($image = $this->getThumbnail()) {
			$url = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA).'catalog/category/'.$image;
		}
		return $url;
	}

}