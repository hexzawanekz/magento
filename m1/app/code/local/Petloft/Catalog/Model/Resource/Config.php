<?php
class Petloft_Catalog_Model_Resource_Config extends Mage_Catalog_Model_Resource_Config
{
    /**
     * Retrieve Used Product Attributes for Catalog Product Listing Sort By
     *
     * @return array
     */
	
    public function getAttributesUsedForSortBy()
    {
        $adapter = $this->_getReadAdapter();
        $storeLabelExpr = $adapter->getCheckSql('al.value IS NULL', 'main_table.frontend_label','al.value');
        $select = $adapter->select()
            ->from(array('main_table' => $this->getTable('eav/attribute')))
            ->join(
                array('additional_table' => $this->getTable('catalog/eav_attribute')),
                'main_table.attribute_id = additional_table.attribute_id',
                array()
            )
            ->joinLeft(
                array('al' => $this->getTable('eav/attribute_label')),
                'al.attribute_id = main_table.attribute_id AND al.store_id = ' . (int)$this->getStoreId(),
                array('store_label' => $storeLabelExpr)
            )
            ->where('main_table.entity_type_id = ?', (int)$this->getEntityTypeId())
            ->where('additional_table.used_for_sort_by = ?', 1);		
		if($setIds = $this->_getSetIds()) {
			$select->join(
					array('entity_attribute' => $this->getTable('eav/entity_attribute')),
					'entity_attribute.attribute_id = main_table.attribute_id',
					'attribute_id'
				)
				->where('entity_attribute.attribute_set_id in (?)', $setIds);
		}
		
        return $adapter->fetchAll($select);
    }
	protected function _getSetIds() {
		$setIds = $this->_getSetIdFromCollection();
		
		if(is_array($setIds)) {
			$setIds = implode(',',$setIds);
		}
		elseif(!is_numeric($setIds)) {
			$setIds = null;
		}
		/* $useCache   = Mage::app()->useCache('config');
		if ($useCache) {
            $cacheId    = 'PELOFT_CATALOG_SET_ID_' . Mage::app()->getStore()->getCode();
            $cacheTags  = array('config');
            $setIds = Mage::app()->loadCache($cacheId);
			if(!$setIds) {
				$setIds = $this->_getSetIdFromCollection();
				Mage::app()->saveCache($setIds, $cacheId, $cacheTags);
			}
        }
		else {
			$setIds = $this->_getSetIdFromCollection();
		} */
		return $setIds;
	}
	protected function _getSetIdFromCollection() {
		$setIds = null;
		try {				
			$productCollection = Mage::getSingleton('catalog/layer')->getProductCollection();			
			if($productCollection) {
				$setIds = $productCollection->getSetIds();
			}
		}
		catch(Exception $e) {
			Mage::log($e->getMessage());
		}
		return $setIds;
	}
}
