<?php

class Petloft_Catalog_Model_Instagram extends Mage_Core_Model_Abstract
{
    protected $_tableName   = 'instagram_media_recents';
    protected $_logFilename = 'instagram_caching.log';

    // resource
    protected $_resource = null;
    protected $_readConnection = null;
    protected $_writeConnection = null;

    public function _construct()
    {
        // init resource
        $this->_resource = Mage::getSingleton('core/resource');
        $this->_readConnection = $this->_resource->getConnection('core_read');
        $this->_writeConnection = $this->_resource->getConnection('core_write');
    }

    public function deleteByStore($store_id)
    {
        try {
            $sql = "DELETE FROM `{$this->_tableName}` WHERE `store_id` = {$store_id};";
            $this->_writeConnection->query($sql);
            return null;
        } catch (Exception $e) {
            Mage::log($e->getMessage() . ' - File: ' . $e->getFile() . ' - Line: ' . $e->getLine(), null, $this->_logFilename);
            return $e->getMessage();
        }
    }

    public function deleteAll()
    {
        try {
            $sql = "DELETE FROM `{$this->_tableName}`;";
            $this->_writeConnection->query($sql);
            return null;
        } catch (Exception $e) {
            Mage::log($e->getMessage() . ' - File: ' . $e->getFile() . ' - Line: ' . $e->getLine(), null, $this->_logFilename);
            return $e->getMessage();
        }
    }

    public function addLink($store_id, $link, $thumb, $caption = null)
    {
        try {
            // leave Caption from request
            $caption = null;

            $sql = "INSERT INTO `{$this->_tableName}`(`store_id`,`link`,`thumb`,`caption`) VALUES({$store_id},'{$link}','{$thumb}','{$caption}');";
            $this->_writeConnection->query($sql);
            return null;
        } catch (Exception $e) {
            Mage::log($e->getMessage() . ' - File: ' . $e->getFile() . ' - Line: ' . $e->getLine(), null, $this->_logFilename);
            return $e->getMessage();
        }
    }

    public function getMediaRecent($store_id = null)
    {
        try {
            $sql = "SELECT `link`,`thumb`,`caption` FROM `{$this->_tableName}` WHERE `store_id` = " . ($store_id ? $store_id : Mage_Core_Model_App::ADMIN_STORE_ID) . ";";
            return $this->_readConnection->fetchAll($sql);
        } catch (Exception $e) {
            Mage::log($e->getMessage() . ' - File: ' . $e->getFile() . ' - Line: ' . $e->getLine(), null, $this->_logFilename);
            return null;//$e->getMessage();
        }
    }

    public function updateDataFromInstagram($store_id = null)
    {
        $token = Mage::getStoreConfig('petloftcatalog/instagram_widget/access_token', $store_id);

        if($token){
            $userRequest = "https://api.instagram.com/v1/users/self/?access_token={$token}";

            $result = @file_get_contents($userRequest);

            if($result){
                $obj = @json_decode($result);

                if($userId = $obj->data->id){

                    $mediaRequest = "https://api.instagram.com/v1/users/{$userId}/media/recent/?access_token={$token}";

                    $data = @file_get_contents($mediaRequest);

                    if($data){
                        if($data = @json_decode($data)){
                            if($data = $data->data){
                                $final = array();
                                foreach($data as $obj){
                                    if($link = $obj->link){
                                        $thumb = '';
                                        if($thumb == '' && $obj->images->low_resolution->url){
                                            $thumb = $obj->images->low_resolution->url;
                                        }elseif($thumb == '' && $obj->images->standard_resolution->url){
                                            $thumb = $obj->images->standard_resolution->url;
                                        }elseif($thumb == '' && $obj->images->thumbnail->url){
                                            $thumb = $obj->images->thumbnail->url;
                                        }

                                        $final[] = array(
                                            'link'      => $link,
                                            'thumb'     => $thumb,
                                            'caption'   => $obj->caption->text ? $obj->caption->text : '',
                                        );
                                    }
                                }

                                // save to database, and return message
                                return $this->_saveToDatabase($final, $store_id);
                            }
                        }
                    }else{
                        return array(
                            'message'   => "Cannot get media recent from Instagram's API by account id #{$userId}",
                            'status'    => false,
                        );
                    }
                }
            }else{
                return array(
                    'message'   => "Cannot get Instagram's account information by token.",
                    'status'    => false,
                );
            }
        }
        return array(
            'message'   => "Instagram's token key still be empty, cannot process without it.",
            'status'    => false,
        );
    }

    protected function _saveToDatabase($final, $store_id = null)
    {
        // update database
        if(count($final) > 0){

            // delete old data
            $tempRe = $this->deleteByStore($store_id ? $store_id : Mage_Core_Model_App::ADMIN_STORE_ID);
            if(!is_null($tempRe)){
                return array('message'=>$tempRe,'status'=>false);
            }

            // add new rows
            foreach($final as $row){
                $rowResult = $this->addLink(
                    $store_id ? $store_id : Mage_Core_Model_App::ADMIN_STORE_ID,
                    $row['link'],
                    $row['thumb'],
                    $row['caption'] ? $row['caption'] : null
                );

                if(!is_null($rowResult)){
                    return array('message'=>$rowResult,'status'=>false);
                }
            }
        }

        return array('message'=>'Media recent links from Instagram have been saved successfully.','status'=>true);
    }
}