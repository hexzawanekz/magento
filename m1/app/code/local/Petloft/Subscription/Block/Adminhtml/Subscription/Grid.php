<?php
/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Subscription to newer
 * versions in the future.
 *
 * @category    Petloft
 * @package     Petloft_Subscription
 * @copyright   Copyright (c) 2012 aCommerce (http://www.acommerce.asia)
 * @license     http://www.acommerce.asia/LICENSE-E.txt
*/

class Petloft_Subscription_Block_Adminhtml_Subscription_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
  public function __construct()
  {
      parent::__construct();
      $this->setId('subscriptionGrid');
      $this->setDefaultSort('subscription_id');
      $this->setDefaultDir('ASC');
      $this->setSaveParametersInSession(true);
  }

  protected function _prepareCollection()
  {
      $collection = Mage::getModel('subscription/subscription')->getCollection();
      $this->setCollection($collection);
      return parent::_prepareCollection();
  }

  protected function _prepareColumns()
  {
      $helper = Mage::helper('subscription');
      $this->addColumn('subscription_id', array(
          'header'    => $helper->__('ID'),
          'align'     =>'right',
          'width'     => '50px',
          'index'     => 'subscription_id',
      ));

      
      $this->addColumn('first_name', array(
          'header'    => $helper->__('Subscriber Firstname'),
          'align'     =>'left',
          'index'     => 'first_name',
      ));

      $this->addColumn('last_name', array(
          'header'    => $helper->__('Subscriber Lastname'),
          'align'     =>'left',
          'index'     => 'last_name',
      ));

      $this->addColumn('email', array(
          'header'    => $helper->__('Subscriber Email'),
          'align'     =>'left',
          'index'     => 'email',
      ));

      $this->addColumn('baby_name', array(
          'header'    => $helper->__('Baby Name'),
          'align'     =>'left',
          'index'     => 'baby_name',
      ));

      $this->addColumn('baby_birthday', array(
          'header'    => $helper->__('Baby Birthday'),
          'align'     =>'left',
          'index'     => 'baby_birthday',
          'type'      => 'date',
      ));

      $this->addColumn('trying_to_conceive', array(
          'header'    => $helper->__('Trying to Conceive'),
          'align'     =>'left',
          'index'     => 'trying_to_conceive',
          'type'      => 'options',
            'options'   => array(
                1 => 'Yes',
                0 => 'No',
            ),
      ));

      $this->addColumn('created_time', array(
        'header'    => $helper->__('Subscribed At'),
        'index'     => 'created_time',
        'type'      => 'datetime',
      ));
	  
      // $this->addColumn('action',
      //     array(
      //         'header'    =>  $helper->__('Action'),
      //         'width'     => '100',
      //         'type'      => 'action',
      //         'getter'    => 'getId',
      //         'actions'   => array(
      //             array(
      //                 'caption'   => $helper->__('Edit'),
      //                 'url'       => array('base'=> '*/*/edit'),
      //                 'field'     => 'id'
      //             )
      //         ),
      //         'filter'    => false,
      //         'sortable'  => false,
      //         'index'     => 'stores',
      //         'is_system' => true,
      // ));
		
		$this->addExportType('*/*/exportCsv', $helper->__('CSV'));
		// $this->addExportType('*/*/exportXml', Mage::helper('subscription')->__('XML'));
	  
      return parent::_prepareColumns();
  }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('subscription_id');
        $this->getMassactionBlock()->setFormFieldName('subscription');

        $this->getMassactionBlock()->addItem('delete', array(
             'label'    => Mage::helper('subscription')->__('Delete'),
             'url'      => $this->getUrl('*/*/massDelete'),
             'confirm'  => Mage::helper('subscription')->__('Are you sure?')
        ));

        // $statuses = Mage::getSingleton('subscription/status')->getOptionArray();

        // array_unshift($statuses, array('label'=>'', 'value'=>''));
        // $this->getMassactionBlock()->addItem('status', array(
        //      'label'=> Mage::helper('subscription')->__('Change status'),
        //      'url'  => $this->getUrl('*/*/massStatus', array('_current'=>true)),
        //      'additional' => array(
        //             'visibility' => array(
        //                  'name' => 'status',
        //                  'type' => 'select',
        //                  'class' => 'required-entry',
        //                  'label' => Mage::helper('subscription')->__('Status'),
        //                  'values' => $statuses
        //              )
        //      )
        // ));
        return $this;
    }

  public function getRowUrl($row)
  {
      return $this->getUrl('*/*/edit', array('id' => $row->getId()));
  }

}