<?php
/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Subscription to newer
 * versions in the future.
 *
 * @category    Petloft
 * @package     Petloft_Subscription
 * @copyright   Copyright (c) 2012 aCommerce (http://www.acommerce.asia)
 * @license     http://www.acommerce.asia/LICENSE-E.txt
*/

class Petloft_Subscription_Block_Adminhtml_Subscription_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $helper = Mage::helper('subscription');
      $form = new Varien_Data_Form(array(
            'id' => 'edit_form',
            'action' => $this->getUrl('*/*/save', array('id' => $this->getRequest()->getParam('id'))),
            'method' => 'post')
      );

      $form->setUseContainer(true);
      $this->setForm($form);

      $fldInfo = $form->addFieldset('subscription_info', array('legend'=> $helper->__('Subscrioption Information')));
      $fldInfo->addField('subscription_id', 'hidden', array(
          'name' => 'id',
      ));

      $fldInfo->addField('first_name', 'text', array(
          'label'     => $helper->__('Firstname'),
          'name'      => 'first_name',
      ));

      $fldInfo->addField('last_name', 'text', array(
          'label'     => $helper->__('Lastname'),
          'name'      => 'last_name',
      ));


      $fldInfo->addField('email', 'text', array(
          'label'     => $helper->__('E-mail'),
          'name'      => 'email',
      ));

      
      $dateFormatIso = Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);
      $fldInfo->addField('baby_birthday', 'date', array(
          'label'     => $helper->__('Baby\'s Birthday'),
          'name'      => 'baby_birthday',
          'image'  => $this->getSkinUrl('images/grid-cal.gif'),
          'input_format' => Varien_Date::DATE_INTERNAL_FORMAT,
          'format'       => $dateFormatIso
      ));

      $fldInfo->addField('baby_name', 'text', array(
          'label'     => $helper->__('Baby\'s Name'),
          'name'      => 'baby_name',
      ));

      $fldInfo->addField('trying_to_conceive', 'select', array(
          'label'     => $helper->__('Trying to Conceive'),
          'name'      => 'trying_to_conceive',
          'options'   => array(
                NULL => $helper->__('Choose..'),
                '1' => $helper->__('Yes'),
                '0' => $helper->__('No'),
            ),
      ));

      if ( Mage::registry('subscription_data') ) {
          $form->setValues(Mage::registry('subscription_data')->getData());
      }

      return parent::_prepareForm();
  }
}