<?php
/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Subscription to newer
 * versions in the future.
 *
 * @category    Petloft
 * @package     Petloft_Subscription
 * @copyright   Copyright (c) 2012 aCommerce (http://www.acommerce.asia)
 * @license     http://www.acommerce.asia/LICENSE-E.txt
*/

$installer = $this;

$installer->startSetup();

$installer->run("
			 -- DROP TABLE IF EXISTS {$this->getTable('subscription')};
				CREATE TABLE {$this->getTable('subscription')} (
				  `subscription_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
				  `first_name` varchar(255) NOT NULL,
				  `last_name` varchar(255) NOT NULL,
				  `email` varchar(255) NOT NULL,				  
				  `baby_birthday` date DEFAULT NULL,
				  `trying_to_conceive` tinyint(1) NOT NULL DEFAULT '0',
				  `baby_name` varchar(255) DEFAULT NULL,
				  `created_time` DATETIME NULL,
				  `update_time` DATETIME NULL
				) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='subscription';
			");

$installer->endSetup(); 