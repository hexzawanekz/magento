<?php
/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Subscription to newer
 * versions in the future.
 *
 * @category    Petloft
 * @package     Petloft_Subscription
 * @copyright   Copyright (c) 2012 aCommerce (http://www.acommerce.asia)
 * @license     http://www.acommerce.asia/LICENSE-E.txt
*/

class Petloft_Subscription_Model_Subscription extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('subscription/subscription');
    }

    protected function _beforeSave()
    {
    	$id = $this->getId();
        $email = $this->getEmail();
        $firstname = $this->getFirstName();
        $lastname = $this->getLastName();
    	$subscribed = $this->getResource()->loadByEmail($email);

        if(($subscribed) && ($id != $subscribed['subscription_id'])){
    		Mage::throwException(Mage::helper('core')->__('This email already subscribed.'));
        }

        if(($firstname=="") || ($lastname=="")){
            Mage::throwException(Mage::helper('core')->__('Please fill in your firstname and lastname.'));
        }

        $babyBirthday = $this->getBabyBirthday();
        $tryingConceive = $this->getTryingToConceive();
        

        if(($babyBirthday=="") && ($tryingConceive=="")){
        	Mage::throwException(Mage::helper('core')->__("Please fill in your baby's birthday or tick in 'Trying to conceived' checkbox."));
        }
        else{
        	if (($babyBirthday!="") && ($tryingConceive!="")) {
        		$this->setTryingToConceive(0);
        	} elseif (($babyBirthday=="") && ($tryingConceive!="")) {        		
        		$this->setTryingToConceive(1);
        		$this->setBabyBirthday(NULL);
        	}
        }        

        return parent::_beforeSave();
    }

    protected function _afterSave()
    {
        $this->sendEmail();
    }

    public function sendEmail()
    {
        $store = Mage::app()->getStore();

        $translate = Mage::getSingleton('core/translate');
        /* @var $translate Mage_Core_Model_Translate */
        $translate->setTranslateInline(false);

        $oldStore = Mage::app()->getStore();
        Mage::app()->setCurrentStore($store);
        $storeId = $store->getId();

        $tpl = Mage::getModel('core/email_template');

        try {
            $url = $store->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_LINK);

            //added in 1.2.1
            $subscribedEmail = $this->getEmail();
            $customer = Mage::getModel("customer/customer");
            $customer->setWebsiteId(Mage::app()->getWebsite()->getId());
            $customer->loadByEmail($subscribedEmail); //load customer by email id

            $templateCode = 'subscription/settings/template';
            
            if ($customer->getId()){
                $cust = Mage::getModel('customer/customer')->load($customer->getId());
                $customerName = $cust->getFirstname() . ' ' . $cust->getLastname();
            } else {
                $customerName = $this->getFirstName() . ' ' . $this->getLastName();
            }

            // added in 0.2.2
            $couponCode = '';
            $couponCode = $this->_createCoupon($store);

            $templateId = Mage::getStoreConfig($templateCode, $storeId);
            $mailer = Mage::getModel('core/email_template_mailer');
            $emailInfo = Mage::getModel('core/email_info');
            $emailInfo->addTo($subscribedEmail, $customerName);
            $bccEmail = Mage::getStoreConfig('subscription/settings/bcc');
            if ($bccEmail) {
                $emailInfo->addBcc($bccEmail);
            }
            $mailer->addEmailInfo($emailInfo);
            

            // Set all required params and send emails
            $sender = Mage::getStoreConfig('subscription/settings/identity', $storeId);
            $mailer->setSender($sender);
            $mailer->setStoreId($storeId);
            $mailer->setTemplateId($templateId);
            $mailer->setTemplateParams(array(
                            'website_name' => $store->getWebsite()->getName(),
                            'store_name' => $store->getName(),
                            'store_url' => $url,
                            'deals_url' => $url.'deals.html',
                            'help_url'  => $url.'help',
                            'customer_name' => $customerName,
                            'coupon' => $couponCode
                        )
                    );
            $mailer->send();

        } catch (Exception $e) {
            //todo: remove coupon if any
            // $history->delete();
            Mage::log($e->getMessage(), null, 'subscription.log');
        }

        Mage::app()->setCurrentStore($oldStore);

        $translate->setTranslateInline(true);
    }

    public function _createCoupon($store = null) {
        try{            
            return Mage::getModel('subscription/coupons')->setStoreId($store->getId())->generateCouponCode();
        }
        catch(Exception $e) {
            Mage::logException($e);
        }
        return '';
    }
}