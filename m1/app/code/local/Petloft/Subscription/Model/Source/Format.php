<?php
class Petloft_Subscription_Model_Source_Format extends Varien_Object
{
    public function toOptionArray()
    {
        $vals = Mage::helper('salesrule/coupon')->getFormatsList();
        $options = array();
        foreach ($vals as $k => $v)
            $options[] = array(
                    'value' => $k,
                    'label' => $v
            );
        
        return $options;
    }
}  