<?php
/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Subscription to newer
 * versions in the future.
 *
 * @category    Petloft
 * @package     Petloft_Subscription
 * @copyright   Copyright (c) 2012 aCommerce (http://www.acommerce.asia)
 * @license     http://www.acommerce.asia/LICENSE-E.txt
*/

class Petloft_Subscription_Model_Resource_Subscription extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {    
        // Note that the subscription_id refers to the key field in your database table.
        $this->_init('subscription/subscription', 'subscription_id');
    }

    public function loadByEmail($email)
    {
    	$select = $this->_getReadAdapter()->select()->from($this->getTable('subscription/subscription'))
                ->where('email=?',$email );
        return $this->_getReadAdapter()->fetchRow($select);
    }
}