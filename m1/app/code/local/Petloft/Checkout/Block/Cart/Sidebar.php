<?php
/**
 * Wishlist sidebar block
 *
 * @category    Mage
 * @package     Mage_Checkout
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Petloft_Checkout_Block_Cart_Sidebar extends Mage_Checkout_Block_Cart_Sidebar
{
    /**
     * Class constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->addItemRender('default', 'checkout/cart_item_renderer', 'petloftcheckout/cartmodal/checkout/cart/sidebar/right.phtml');
    }


    public function getTotalTax()
    {
        $totals = $this->getTotals();
        if ( isset($totals['subtotal']) && isset($totals['tax']) ) {
            return $totals['tax']->getValue();
        }
        return '0';
    }

    public function getFreeShippingTitle()
    {
        return Mage::getStoreConfig('carriers/freeshipping/title');
    }

    public function getFreeShippingRate()
    {
        return Mage::getStoreConfig('carriers/freeshipping/free_shipping_subtotal');
    }
 }
