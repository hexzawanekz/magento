<?php

/**
 * Shopping ajax cart controller
 */

require_once 'Mage/Checkout/controllers/CartController.php';

class Petloft_Checkout_CartController extends Mage_Checkout_CartController
{ 

    public function addAction()
    {
        $this->ajax_addAction();
    }

    public function getcartmodalAction()
    {
        $this->ajax_getcartmodalAction();
    }

    public function getcartrightAction()
    {
        $this->ajax_getcartrightAction();
    }

    public function updateItemOptionsAction()
    {
        $this->ajax_updateItemOptionsAction();
    }

    public function deleteAction()
    {
        $this->ajax_deleteAction();        
    }

    protected function getItemsCountText()
    {
        $cart  = $this->_getCart();
        $count = $cart->getSummaryQty();

        if ($count == 1) {
            $text = $this->__('(%s item)', $count);
        } elseif ($count > 0) {
            $text = $this->__('(%s items)', $count);
        } else {
            $text = $this->__('');
        }
        return $text;
    }

    protected function getCardModalHtml()
    {
        Varien_Profiler::start(__METHOD__ . 'cart_display');
       
        /*$this
            ->loadLayout()
            ->_initLayoutMessages('checkout/session')
            ->_initLayoutMessages('catalog/session')
            ->getLayout()->getBlock('head')->setTitle($this->__('Shopping Cart'));
        $this->renderLayout();*/
      
        /* Cart modal */
        $layout = $this->loadLayout('checkout_cart_index')
                        ->_initLayoutMessages('checkout/session')
                        ->_initLayoutMessages('catalog/session')
                        ->getLayout();

        // $top_methods = $layout->getBlock('checkout.cart.top_methods');
        // $crosssell =  $layout->getBlock('checkout.cart.crosssell');
        // $coupon =  $layout->getBlock('checkout.cart.coupon');
        // $shipping =  $layout->getBlock('checkout.cart.shipping');
        // $totals =  $layout->getBlock('checkout.cart.totals');
        // $methods =  $layout->getBlock('checkout.cart.methods');

        // $block = $layout->getBlockSingleton('Mage_Checkout_Block_Cart')
        // $block = $layout->createBlock('checkout/cart')
        //                 ->setTemplate('productoptions/cartmodal/cartmodal.phtml')
        //                 ->setChild('top_methods', $top_methods)
        //                 ->setChild('crosssell', $crosssell)
        //                 ->setChild('coupon', $coupon)
        //                 ->setChild('shipping', $shipping)
        //                 ->setChild('totals', $totals)
        //                 ->setChild('methods', $methods);

        $block = $layout->getBlock('checkout.cartmodal');
        $block_cart_modal = $block->toHtml();

        Varien_Profiler::stop(__METHOD__ . 'cart_display');

        return $block_cart_modal;
    }

    protected function getCardRightHtml()
    {
        $cart  = $this->_getCart();
        $count = $cart->getSummaryQty();

        /* Right cart */
        $block_cart_right = '';
        if($cart->getSummaryQty() > 0)
        {
            $layout = $this->loadLayout('cart_right_ajax')
                            ->_initLayoutMessages('checkout/session')
                            ->_initLayoutMessages('catalog/session')
                            ->getLayout();

            $block = $layout->getBlock('cart_sidebar_ajax');
            $block_cart_right = $block->toHtml();
        }

        return $block_cart_right;
    }

    public function ajax_getcartrightAction()
    {
        $json['block_cart_right'] = $this->getCardRightHtml();
        $json['status'] = 1;
        AJAX::ResponseJSON($json);
    }

    public function ajax_getcartmodalAction()
    {
        $cart = $this->_getCart();
        if ($cart->getQuote()->getItemsCount()) {
            $cart->init();
            $cart->save();

            if (!$this->_getQuote()->validateMinimumAmount()) {
                $minimumAmount = Mage::app()->getLocale()->currency(Mage::app()->getStore()->getCurrentCurrencyCode())
                    ->toCurrency(Mage::getStoreConfig('sales/minimum_order/amount'));

                $warning = Mage::getStoreConfig('sales/minimum_order/description')
                    ? Mage::getStoreConfig('sales/minimum_order/description')
                    : Mage::helper('checkout')->__('Minimum order amount is %s', $minimumAmount);

                $cart->getCheckoutSession()->addNotice($warning);
            }
        }

        // Compose array of messages to add
        $messages = array();
        foreach ($cart->getQuote()->getMessages() as $message) {
            if ($message) {
                // Escape HTML entities in quote message to prevent XSS
                $message->setCode(Mage::helper('core')->escapeHtml($message->getCode()));
                $messages[] = $message;
            }
        }
        $cart->getCheckoutSession()->addUniqueMessages($messages);

        /**
         * if customer enteres shopping cart we should mark quote
         * as modified bc he can has checkout page in another window.
         */
        $this->_getSession()->setCartWasUpdated(true);
         
        $json = array();
        $json['status'] = 1;
        $json['html_cart_modal'] = $this->getCardModalHtml();
        $json['html_cart_right'] = $this->getCardRightHtml();
        AJAX::ResponseJSON($json);
    }

    public function ajax_addAction()
    {
        $cart   = $this->_getCart();
        $params = $this->getRequest()->getParams();
        
        $json = array();
        try {
            if (isset($params['qty'])) {
                $filter = new Zend_Filter_LocalizedToNormalized(
                    array('locale' => Mage::app()->getLocale()->getLocaleCode())
                );
                $params['qty'] = $filter->filter($params['qty']);
            }

            $product = $this->_initProduct();
            $related = $this->getRequest()->getParam('related_product');

            /**
             * Check product availability
             */
            if (!$product) {
                // $this->_goBack();
                // return;

                $json['status'] = 0;
                $json['message'] = "Product not availability !";
                AJAX::ResponseJSON($json);
                return;
            }

            $cart->addProduct($product, $params);
            if (!empty($related)) {
                $cart->addProductsByIds(explode(',', $related));
            }

            $cart->save();

            $this->_getSession()->setCartWasUpdated(true);

            /**
             * @todo remove wishlist observer processAddToCart
             */
            Mage::dispatchEvent('checkout_cart_add_product_complete',
                array('product' => $product, 'request' => $this->getRequest(), 'response' => $this->getResponse())
            );

            if (!$this->_getSession()->getNoCartRedirect(true)) {
                if (!$cart->getQuote()->getHasError()){
                    $message = $this->__('%s was added to your shopping cart.', Mage::helper('core')->escapeHtml($product->getName()));
                    $this->_getSession()->addSuccess($message);
                }
                // $this->_goBack();

                $json['status'] = 1;
                $json['items_count_text'] = $this->getItemsCountText();
                $json['html_cart_modal'] = $this->getCardModalHtml();
                $json['html_cart_right'] = $this->getCardRightHtml();
                $json['message'] = "OK";
                AJAX::ResponseJSON($json);
            }
        } catch (Mage_Core_Exception $e) {
            if ($this->_getSession()->getUseNotice(true)) {
                $this->_getSession()->addNotice(Mage::helper('core')->escapeHtml($e->getMessage()));
            } else {
                $messages = array_unique(explode("\n", $e->getMessage()));
                foreach ($messages as $message) {
                    $this->_getSession()->addError(Mage::helper('core')->escapeHtml($message));
                }
            }

            $url = $this->_getSession()->getRedirectUrl(true);
            if ($url) {
                // $this->getResponse()->setRedirect($url);
            } else {
                // $this->_redirectReferer(Mage::helper('checkout/cart')->getCartUrl());
            }

            $json['status'] = 0;
            $json['message'] = "ERROR";
            AJAX::ResponseJSON($json);

        } catch (Exception $e) {
            $this->_getSession()->addException($e, $this->__('Cannot add the item to shopping cart.'));
            Mage::logException($e);
            // $this->_goBack();


            $json['status'] = 0;
            $json['message'] = "ERROR";
            AJAX::ResponseJSON($json);
        }
    }

    /**
     * Update product configuration for a cart item
     */
    // Array
    // (
    //     [id] => 65           Shoping cart ID
    //     [product] => 174     Product ID
    //     [related_product] => 
    //     [qty] => 4
    // )
    // 
    // Array
    // (
    //     [id] => 74
    //     [product] => 209
    //     [related_product] => 
    //     [super_attribute] => Array
    //         (
    //             [979] => 335
    //         )
    //     [qty] => 2
    // )
    public function ajax_updateItemOptionsAction()
    {
        $cart   = $this->_getCart();
        $id = (int) $this->getRequest()->getParam('id');
        $params = $this->getRequest()->getParams();

        if (!isset($params['options'])) {
            $params['options'] = array();
        }

        $json = array();
        $json['status'] = 0;
        $block_message = '';
        try {
            if (isset($params['qty'])) {
                $filter = new Zend_Filter_LocalizedToNormalized(
                    array('locale' => Mage::app()->getLocale()->getLocaleCode())
                );
                $params['qty'] = $filter->filter($params['qty']);
            }

            $quoteItem = $cart->getQuote()->getItemById($id);
            if (!$quoteItem) {
                Mage::throwException($this->__('Quote item is not found.'));
            }

            $item = $cart->updateItem($id, new Varien_Object($params));
            if (is_string($item)) {
                Mage::throwException($item);
            }
            if ($item->getHasError()) {
                Mage::throwException($item->getMessage());
            }

            $related = $this->getRequest()->getParam('related_product');
            if (!empty($related)) {
                $cart->addProductsByIds(explode(',', $related));
            }

            $cart->save();

            $this->_getSession()->setCartWasUpdated(true);

            Mage::dispatchEvent('checkout_cart_update_item_complete',
                array('item' => $item, 'request' => $this->getRequest(), 'response' => $this->getResponse())
            );
            if (!$this->_getSession()->getNoCartRedirect(true)) {
                if (!$cart->getQuote()->getHasError()){
                    $message = $this->__('%s was updated in your shopping cart.', Mage::helper('core')->htmlEscape($item->getProduct()->getName()));
                    $this->_getSession()->addSuccess($message);
                }
                // $this->_goBack();
            }
            
            // $block_message = $this->loadLayout()
                            // ->_initLayoutMessages('checkout/session')
                            // ->_initLayoutMessages('catalog/session')
                            // ->getLayout()
                            // ->getMessagesBlock()
                            // ->getGroupedHtml();

            $json['status'] = 1;
            $json['items_count_text'] = $this->getItemsCountText();
            $json['html_cart_modal'] = $this->getCardModalHtml();
            $json['html_cart_right'] = $this->getCardRightHtml();
            $json['message'] = $block_message;
            AJAX::ResponseJSON($json);

        } catch (Mage_Core_Exception $e) {
            if ($this->_getSession()->getUseNotice(true)) {
                $this->_getSession()->addNotice($e->getMessage());
            } else {
                $messages = array_unique(explode("\n", $e->getMessage()));
                foreach ($messages as $message) {
                    $this->_getSession()->addError($message);
                }
            }

            $url = $this->_getSession()->getRedirectUrl(true);
            if ($url) {
                // $this->getResponse()->setRedirect($url);
            } else {
                // $this->_redirectReferer(Mage::helper('checkout/cart')->getCartUrl());
            }

            // $block_message = $this->loadLayout()
            //                 ->_initLayoutMessages('checkout/session')
            //                 ->_initLayoutMessages('catalog/session')
            //                 ->getLayout()
            //                 ->getMessagesBlock()
            //                 ->getGroupedHtml();

            $json['status'] = 0;
            $json['message'] = $block_message;
            AJAX::ResponseJSON($json);            
        } catch (Exception $e) {
            $this->_getSession()->addException($e, $this->__('Cannot update the item.'));
            Mage::logException($e);
            // $this->_goBack();

            // $block_message = $this->loadLayout()
            //                 ->_initLayoutMessages('checkout/session')
            //                 ->_initLayoutMessages('catalog/session')
            //                 ->getLayout()
            //                 ->getMessagesBlock()
            //                 ->getGroupedHtml();

            $json['status'] = 0;
            $json['message'] = $block_message;
            AJAX::ResponseJSON($json);
        }
        // $this->_redirect('*/*');
    }

    /**
     * Delete shoping cart item action
     */
    public function ajax_deleteAction()
    {
        $cart   = $this->_getCart();
        $id = (int) $this->getRequest()->getParam('id');
        
        $json = array();
        $json['status'] = 0;
        $block_message = '';
        if ($id) {
            try {
                $this->_getCart()->removeItem($id)->save();                
                // $this->_getSession()->addSuccess($this->__('Removed the item.'));
                $json['status'] = 1;
            } catch (Exception $e) {
                // $this->_getSession()->addError($this->__('Cannot remove the item.'));
                Mage::logException($e);
                $json['status'] = 0;
            }
        }
        // $this->_redirectReferer(Mage::getUrl('*/*'));
        // $block_message = $this->loadLayout()
                    // ->_initLayoutMessages('checkout/session')
                    // ->_initLayoutMessages('catalog/session')
                    // ->getLayout()
                    // ->getMessagesBlock()
                    // ->getGroupedHtml();
        $json['message'] = $block_message;
        $json['items_count_text'] = $this->getItemsCountText();
        $json['html_cart_modal'] = $this->getCardModalHtml();
        $json['html_cart_right'] = $this->getCardRightHtml();
        AJAX::ResponseJSON($json);     
    }

}
