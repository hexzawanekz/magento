<?php

/**
 * Created by Orami Development Team <development@orami.co.id>
 * Date: 5/26/17
 * Time: 2:59 PM
 */
class Petloft_Wholesale_Model_Pickup extends Mage_Shipping_Model_Carrier_Abstract implements Mage_Shipping_Model_Carrier_Interface
{
    protected $_code = 'petloft_wholesalepickup';

    public function getAllowedMethods()
    {
        return array(
            'petloft_wholesalepickup' => $this->getConfigData('name')
        );
    }

    public function collectRates(Mage_Shipping_Model_Rate_Request $request)
    {
        $quote = false;

        /** @var Mage_Shipping_Model_Rate_Result $result */
        $result = Mage::getModel('shipping/rate_result');

        // Get Quote
        foreach ($request->getAllItems() as $item){
            $quote = $item->getQuote();
            break;
        }

        if ($quote) {
            if ($quote->getIsWholesale() == 1) {
                $result->append($this->_getPickupRate());
            }
        }

        return $result;
    }

    /**
     * Get rate for 'Pickup' option
     *
     * @return Mage_Shipping_Model_Rate_Result_Method
     */
    protected function _getPickupRate()
    {
        /** @var Mage_Shipping_Model_Rate_Result_Method $rate */
        $rate = Mage::getModel('shipping/rate_result_method');

        $rate->setCarrier($this->_code);
        $rate->setCarrierTitle($this->getConfigData('title'));
        $rate->setMethod($this->_code);
        $rate->setMethodTitle($this->getConfigData('name'));

        $rate->setPrice(0);
        $rate->setCost(0);

        return $rate;
    }
}