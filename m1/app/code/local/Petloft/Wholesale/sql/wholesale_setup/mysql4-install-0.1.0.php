<?php
/** @var $installer Mage_Eav_Model_Entity_Setup */
$installer = $this;

try {
    // Get old index name
    $oldIndexName = $installer->getIdxName(
        'shipping/tablerate',
        array('website_id', 'dest_country_id', 'dest_region_id', 'dest_zip', 'condition_name', 'condition_value'),
        Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
    );

    // Get new index name
    $newIndexName = $installer->getIdxName(
        'shipping/tablerate',
        array('website_id', 'dest_country_id', 'dest_region_id', 'dest_zip', 'condition_name', 'condition_value', 'order_type'),
        Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
    );

    // Add new field to 'shipping/tablerate'
    $installer->getConnection()
        ->addColumn(
            $installer->getTable('shipping/tablerate'),
            'order_type',
            array(
                'TYPE' => Varien_Db_Ddl_Table::TYPE_TEXT,
                'LENGTH' => 25,
                'NULLABLE' => false,
                'DEFAULT' => 'retail',
                'COMMENT' => 'retail or wholesale'
            )
        );

    // Drop old index
    $installer->run("ALTER TABLE `{$this->getTable('shipping/tablerate')}` DROP INDEX `{$oldIndexName}`");

    // Create new index
    $installer->run("
        ALTER TABLE `{$this->getTable('shipping/tablerate')}` 
        ADD CONSTRAINT {$newIndexName} 
        UNIQUE (`website_id`,`dest_country_id`,`dest_region_id`,`dest_zip`,`condition_name`,`condition_value`, `order_type`)"
    );

} catch (Exception $ex) {
    Mage::logException($ex);
}

$installer->endSetup();
