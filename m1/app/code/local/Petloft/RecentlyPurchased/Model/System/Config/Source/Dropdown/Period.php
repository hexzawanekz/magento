<?php

class Petloft_RecentlyPurchased_Model_System_Config_Source_Dropdown_Period
{
    public function toOptionArray()
    {
        return array(
            array(
                'value' => '-3 day',
                'label' => 'Last 3 days',
            ),
            array(
                'value' => '-7 day',
                'label' => 'Last 7 days',
            ),
            array(
                'value' => '-1 month',
                'label' => 'Last 1 month',
            ),
            array(
                'value' => 'all',
                'label' => 'All',
            ),
        );
    }
}