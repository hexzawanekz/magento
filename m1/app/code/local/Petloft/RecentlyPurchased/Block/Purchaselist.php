<?php

class Petloft_RecentlyPurchased_Block_Purchaselist extends Mage_Core_Block_Template
{
	protected $model;
    public function __construct()
    {
        $this->model_orderitems_collection = Mage::getModel('sales/order_item')->getCollection();
	}

	public function getOrderItemList()
    {
        $limit = Mage::getStoreConfig('recentlypurchased/settings/maxdisplay');
        $period = Mage::getStoreConfig('recentlypurchased/settings/period');

        if ($period != 'all') {
            $to = date("Y-m-d H:i:s", Mage::getModel('core/date')->timestamp(time()));
            $from = strtotime($to);
            $from = strtotime($period, $from);
            $from = date("Y-m-d H:i:s", $from);
        }
            

        if ($period == 'all') {
            $this->model_orderitems_collection        
                ->addAttributeToFilter('created_at', array(
                                                            'from'  => $from,
                                                            'to'    => $to,                    
                                                        ));
        }

        $this->model_orderitems_collection
        	->addAttributeToSort('created_at', 'desc')
        	// ->setPageSize($limit)
            ->getSelect()->group('name');


        $order_items = $this->model_orderitems_collection->getData();
        if (! empty($order_items))
        {
            $item_list = array();
            $id_list = array();
            $i=0;
            foreach ($order_items as $key=>$list)
            {                
                $product_id = $list['product_id'];
                $parent_id = Mage::getResourceSingleton('catalog/product_type_configurable')
                                ->getParentIdsByChild($product_id);

                if (! empty($parent_id))
                {
                    $product_id = $parent_id[0];
                }
                array_push($id_list, $product_id);
            }

            $item_ids = array_values(array_unique($id_list));

            if(! empty($item_ids))
            {
                foreach ($item_ids as $key=>$value)
                {
                    $product_id = $value;
                    $product = Mage::getModel('catalog/product')->load($product_id);
                    $product_detail = $product->getData();

                    $product_detail['url_product'] = $product->getProductUrl();
                    $product_detail['product_thumbnail'] = $product->getThumbnailUrl();

                    $item_detail = $product->getData();
                    /*$item_detail['product_detail'] = $product_detail;
                    $item_list[] = $item_detail;*/

                    
                    if($i<$limit)
                    {
                        if($product_detail['visibility']!=1)
                        {
                            $item_list[$i]['name'] = $product_detail['name'];
                            $item_list[$i]['url_product'] = $product_detail['url_product'];
                            $item_list[$i]['product_thumbnail'] = $product_detail['product_thumbnail'];
                            $i++;
                        }
                    }
                    else
                    {
                        break;
                    }
                }
            }
        }
		// debug($item_list);
        return $item_list;
    }
}