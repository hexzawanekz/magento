<?php

/**
 * Customer Data Helper
 *
 * @category   Petloft
 * @package    Petloft_Customer
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Petloft_UTMTracking_Helper_Data extends Mage_Core_Helper_Abstract {

    function getUtm() {
        $utm = array();
        if (!empty($_COOKIE['__utmz'])) {
            $pattern = "/(utmcsr=([^\|]*)[\|]?)|(utmccn=([^\|]*)[\|]?)|(utmcmd=([^\|]*)[\|]?)|(utmctr=([^\|]*)[\|]?)|(utmcct=([^\|]*)[\|]?)/i";
            preg_match_all($pattern, $_COOKIE['__utmz'], $matches);
            if (!empty($matches[0])) {
                foreach ($matches[0] as $match) {
                    $pair = null;
                    $match = trim($match, "|");
                    list($k, $v) = explode("=", $match);
                    $utm[$k] = $v;
                }
            }
        }
        return $utm;
    }

    /**
     * created for MAGENTO-487
     * Get product type (for cat or dog)
     * Return cat, dog or generic
     */
    function getProductType($_product_id) {
        $_product = Mage::getModel('catalog/product')->load($_product_id);       
        $cates_id = $_product->getCategoryIds();
        foreach ($cates_id as $cate_id) {
            $_cate_name = trim(strtolower(Mage::getModel('catalog/category')->load($cate_id)->getName()));
            if(strpos($_cate_name,'cat') === 0 ||  strpos($_cate_name,'แมว') === 0) {
                return 'cat';
            }
            else if(strpos($_cate_name,'สุนัข') === 0 || strpos($_cate_name,'dog') === 0) {
                return 'dog';
            }
        }
        return 'generic';
    }
}
