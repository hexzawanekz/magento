<?php
/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade TableRate to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_TableRate
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
*/

$installer = $this;;
$setup = new Mage_Eav_Model_Entity_Setup('core_setup');

$installer->startSetup();
$setup->addAttribute('customer', 'utm_source', array(
    'label'			=> 'UTM Source',
    'input'			=> 'text',
 	'type'			=> 'static',
    'user_defined'	=> 1,
    'visible'   	=> false,
    'is_system' 	=> false,
    'required'		=> false
));
$this->_conn->addColumn($this->getTable('customer/entity'), 'utm_source', 'varchar(50)');

$setup->addAttribute('customer', 'utm_medium', array(
    'label'			=> 'UTM Medium',
    'input'			=> 'text',
 	'type'			=> 'static',
    'user_defined'	=> 1,
    'visible'   	=> false,
    'is_system' 	=> false,
    'required'		=> false
));
$this->_conn->addColumn($this->getTable('customer/entity'), 'utm_medium', 'varchar(50)');

$setup->addAttribute('customer', 'utm_term', array(
    'label'			=> 'UTM Term',
    'input'			=> 'text',
 	'type'			=> 'static',
    'user_defined'	=> 1,
    'visible'   	=> false,
    'is_system' 	=> false,
    'required'		=> false
));
$this->_conn->addColumn($this->getTable('customer/entity'), 'utm_term', 'varchar(50)');

$setup->addAttribute('customer', 'utm_content', array(
    'label'			=> 'UTM Content',
    'input'			=> 'text',
 	'type'			=> 'static',
    'user_defined'	=> 1,
    'visible'   	=> false,
    'is_system' 	=> false,
    'required'		=> false
));
$this->_conn->addColumn($this->getTable('customer/entity'), 'utm_content', 'varchar(50)');

$setup->addAttribute('customer', 'utm_campaign', array(
    'label'			=> 'UTM Campaign',
    'input'			=> 'text',
 	'type'			=> 'static',
    'user_defined'	=> 1,
    'visible'   	=> false,
    'is_system' 	=> false,
    'required'		=> false
));
$this->_conn->addColumn($this->getTable('customer/entity'), 'utm_campaign', 'varchar(50)');

// $this->_conn->addColumn($this->getTable('sales_flat_order'), 'utm_source', 	'varchar(255) default NULL');
// $this->_conn->addColumn($this->getTable('sales_flat_order'), 'utm_medium', 	'varchar(255) default NULL');
// $this->_conn->addColumn($this->getTable('sales_flat_order'), 'utm_term', 	'varchar(255) default NULL');
// $this->_conn->addColumn($this->getTable('sales_flat_order'), 'utm_content', 'varchar(255) default NULL');
// $this->_conn->addColumn($this->getTable('sales_flat_order'), 'utm_campaign','varchar(255) default NULL');

$installer->endSetup();