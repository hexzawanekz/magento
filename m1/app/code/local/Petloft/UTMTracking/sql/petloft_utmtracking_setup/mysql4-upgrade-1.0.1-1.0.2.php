<?php

$installer = $this;
$installer->startSetup(); // start setup




// sales entity tables
$gridTable = $installer->getTable('sales/order_grid');
$salesTable = $installer->getTable('sales/order');

// $salesSetup = new Mage_Sales_Model_Resource_Setup('core_setup');// sales eav entity setup

$conn = $installer->getConnection(); // connection object

// UTM name and comment
$utms = array(
    'utm_click'	=> 'UTM Reqid ID',
    'utm_subid'	=> 'UTM Affiliate ID',
);

//creating attributes and columns for UTM tracking
foreach($utms as $utm => $comm) {

    if($conn->tableColumnExists($salesTable,$utm)) {
        $conn->dropColumn($salesTable,$utm);
    }
    $conn->addColumn($gridTable, $utm, array(
        'TYPE'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'LENGTH'    => 255,
        'NULLABLE'  => true,
        'COMMENT'   => $comm
    ));
}

$installer->endSetup();
