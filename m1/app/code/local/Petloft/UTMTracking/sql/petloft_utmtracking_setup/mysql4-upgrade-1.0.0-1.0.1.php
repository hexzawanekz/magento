<?php

$installer = $this;
$installer->startSetup(); // start setup


$customerTable = $installer->getTable('customer/entity');// customer entity table


// sales entity tables
$gridTable = $installer->getTable('sales/order_grid');
$salesTable = $installer->getTable('sales/order');

// $salesSetup = new Mage_Sales_Model_Resource_Setup('core_setup');// sales eav entity setup 

$conn = $installer->getConnection(); // connection object

// UTM name and comment
$utms = array(
	'utm_source' 	=> 'UTM Source',
	'utm_medium' 	=> 'UTM Medium',
	'utm_term' 		=> 'UTM Term',
	'utm_content'	=> 'UTM Content',
	'utm_campaign'	=> 'UTM Campaign',
);

//creating attributes and columns for UTM tracking
foreach($utms as $utm => $comm) {	
	// check and add if attributes, columns of customer entity are missed from version 0.1.0 
	$conn->addColumn($customerTable, $utm, array(
        'TYPE'      => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'LENGTH'    => 255,
        'NULLABLE'  => true,
        'COMMENT'   => $comm
    ));
	// add customer attributes
	$installer->addAttribute('customer', $utm, array(
		'type'     => 'static',
		'label'    => $comm,
		'user_defined'	=> 1,
		'visible'  => false,
		'required' => false,
		'input'    => 'text',
	));
	
	
	// $installer->addAttribute('order', $utm, array('type'=>'static'));
	
	// fix to remove columns of sales_flat_order and add to sales_flat_order_grid
	if($conn->tableColumnExists($salesTable,$utm)) {
		$conn->dropColumn($salesTable,$utm);
	}
    $conn->addColumn($gridTable, $utm, array(
        'TYPE'      => Varien_Db_Ddl_Table::TYPE_VARCHAR,
        'LENGTH'    => 255,
        'NULLABLE'  => true,
        'COMMENT'   => $comm
    ));
}

$installer->endSetup();
