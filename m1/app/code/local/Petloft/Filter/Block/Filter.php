<?php
class Petloft_Filter_Block_Filter extends Mage_Catalog_Block_Product_List
{
	
	protected $_layer = null;	
	protected $_filterModelName = 'petloftfilter/catalog_layer_filter_attribute';
	protected $_defaultToolbarBlock = 'petloftfilter/filter_toolbar';
	
	const CACHE_KEY = '_filterable_attributes';
	protected function _prepareLayout()
    {
		$this->filter();				
        parent::_prepareLayout();
		return $this;
    }
	
	public function filter() {
		$filters = $this->getAttributeActiveFilters();
		
		foreach($filters as $filter) {
			$attribute = $this->getAttributeByCode($filter);
			if(!$this->getParam($filter,false)) {
				continue;
			}	
			if($attribute) {
				
				Mage::getModel($this->_filterModelName)
					->setLayer($this->getLayer())
					->setAttributeModel($attribute)->filter($this->getParam($filter));
			}
		}
		return $this->getLayer()->apply();
	}
	public function getAttributeActiveFilters() {
		return Mage::helper('petloftfilter')->getActiveFilterAttributes();
	}
	public function getRemoveUrl($code=null) {
		if($code) {
			return $this->getFilterValue($code,null);
		}
		$all = array();
		foreach($this->getActiveFilters() as $filter) {
			$code = $filter->getFilter()->getAttributeModel()->getAttributeCode();
			$all[$code] = null;
		}
        return $this->getFilterUrl($all);
	}
	public function getFilterValue($name,$value) {		
        return $this->getFilterUrl(array($name=>$value));
	}
	protected function getFilterUrl($data) {
		$urlParams = array();
        $urlParams['_current']  = true;
        $urlParams['_escape']   = true;
        $urlParams['_use_rewrite']   = true;		
        $urlParams['_query'] = $data;		
        return $this->getUrl('*/*/*', $urlParams); 
	}
	public function getActiveFilters() {
        $filters = $this->getLayer()->getState()->getFilters();
        if (!is_array($filters)) {
            $filters = array();
        }
        return $filters;
    }
	public function getParam($paramName, $default=null) {
		// $session = Mage::getSingleton('core/session');
        if ($this->getRequest()->has($paramName)) {
            $param = $this->getRequest()->getParam($paramName);
            // $session->setData($paramName, $param);
            return $param;
        }
        return $default;
	}
	public function getAttributeByCode($code) {
		$attributes = $this->getAttributeOptions();
		foreach($attributes as $attribute) {
			if($attribute->getAttributeCode() == $code) {
				return $attribute;
			}
		}
		return null;
	}
	public function getAttributeOptions() {
		$attributes = $this->getData('_filterable_attributes');
        if (is_null($attributes)) {
            $attributes = $this->getLayer()->getFilterableAttributes();			
            $this->setData('_filterable_attributes', $attributes);
        }
        return $attributes;		
	}
	
	
}