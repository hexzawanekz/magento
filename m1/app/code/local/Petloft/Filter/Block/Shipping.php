<?php
class Petloft_Filter_Block_Shipping extends Mage_Checkout_Block_Cart_Sidebar
{	
	public function getFreeShipping() {
		return Mage::getStoreConfig('carriers/freeshipping/free_shipping_subtotal');
	}
	public function calculate() {
		$total = $this->getSubtotal();
		$freeship = $this->getFreeShipping();
		$per = $total/$freeship;
		if($per>1) {
			$per = 1;
		}
		return $per;
	}
}