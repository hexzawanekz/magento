<?php

class Petloft_Filter_AjaxController extends Mage_Core_Controller_Front_Action
{ 
	protected $_update = false;
	
	public function reloadAction() {				
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($this->_reload()));
	}
	protected function _reloadProduct() {
		// Mage::register('current_layer',$layer);	
		$layout = $this->getLayout();
        $layout->getUpdate()->addHandle('product_list_ajax_load')->merge('product_list_ajax_load');
        $layout->generateXml()->generateBlocks();
        return $layout->getOutput(); 		
	}
	protected function _reload() {
		$return = array();		
		//$shippingWidget = $this->getLayout()->createBlock('petloftfilter/shipping')->setTemplate('petloftfilter/category/shipping.widget.phtml');
		$return['topHtml'] = $this->getLayout()->createBlock('petloftfilter/filter')
								//->setChild('cart_sidebar',$shippingWidget)
								->setTemplate('petloftfilter/category/filter.phtml')
								->toHtml();
		$return['html'] = $this->_reloadProduct();
        return $return;
	}
	public function removeAction() {
		$rem = $this->getRequest()->getParam('attr');		
		if(strtolower($rem) == 'all' || !$rem) {
			Mage::helper('petloftfilter')->unsetFilter();
		}
		else {
			Mage::helper('petloftfilter')->unsetFilter($rem);
		}
		$this->_update = true;
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($this->_reload()));
	}
}
