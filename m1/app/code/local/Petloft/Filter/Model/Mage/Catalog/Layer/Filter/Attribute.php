<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright   Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Layer attribute filter
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Petloft_Filter_Model_Mage_Catalog_Layer_Filter_Attribute extends Mage_Catalog_Model_Layer_Filter_Attribute
{

    /**
     * Apply attribute option filter to product collection
     *
     * @param   Zend_Controller_Request_Abstract $request
     * @param   Varien_Object $filterBlock
     * @return  Mage_Catalog_Model_Layer_Filter_Attribute
     */
    public function apply(Zend_Controller_Request_Abstract $request, $filterBlock)
    {
        $filter = $request->getParam($this->_requestVar);
        if (is_array($filter)) {
            return $this;
        }

        $text = '';
        if (ctype_digit(strval($filter))) {
            $text = $this->_getOptionText($filter);
        } else {
            $option = $this->_getOptionByText($filter);
            if ($option) {
                $text = $option['label'];
                $filter = $option['value'];
            }
        }

        if ($filter && strlen($text)) {
            $this->_getResource()->applyFilterToCollection($this, $filter);
            $this->getLayer()->getState()->addFilter($this->_createItem($text, $filter));
            $this->_items = array();
        }
        return $this;
    }

    /**
     * Get option value from frontend model by option text
     *
     * @param   string $optionText
     * @return  string|bool
     */
    protected function _getOptionByText($optionText)
    {
        $isMultiple = false;
        if (strpos($optionText, ',')) {
            $isMultiple = true;
            $optionText = explode(',', strtolower($optionText));
        }

        $options = $this->getAttributeModel()->getFrontend()->getAttribute()->getSource()->getAllOptions(false);

        if ($isMultiple) {
            $items = array();
            foreach ($options as $item) {
                if (in_array($item['label'], $optionText)) {
                    $items[] = $item;
                }
            }
            return $items;
        }

        foreach ($options as $item) {
            if (strtolower($item['label']) == strtolower($optionText)) {
                return $item;
            }
        }
        return false;
    }
}
