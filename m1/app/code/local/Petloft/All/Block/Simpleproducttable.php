<?php
/**
 * Wishlist sidebar block
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Petloft_All_Block_Simpleproducttable extends Mage_Catalog_Block_Product_View
{
    /**
     * Class constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('petloftall/simpleproducttable.phtml');
    }
 }
