<?php

require_once(Mage::getBaseDir('lib') . '/Facebook/FacebookApi.php');

class Petloft_FacebookApi_Helper_Api extends Mage_Core_Helper_Abstract
{    
    public function initFacebook()
    {
    	return new FacebookApi( $this->getAppId(), $this->getAppSecret(), $this->getAppScope());
    }

    public function getAppId()
    {
        return Mage::getStoreConfig('facebookapi/settings/appid');
    }

    public function getAppSecret()
    {
        return Mage::getStoreConfig('facebookapi/settings/appsecret');
    }

    public function getAppScope()
    {
        return Mage::getStoreConfig('facebookapi/settings/appscope');
    }
}