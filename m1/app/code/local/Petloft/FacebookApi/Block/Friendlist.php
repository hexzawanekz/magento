<?php

class Petloft_FacebookApi_Block_Friendlist extends Mage_Core_Block_Template {
 	
    protected $facebook;
    protected $model;

	public function __construct(){
        $this->facebook = Mage::helper('facebookapi/api')->initFacebook();
        $this->model_facebookuser_collection = Mage::getModel('facebookapi/facebookuser')->getCollection();
        // debug($this->model_facebookuser_collection->getData());exit();
        // $this->model_facebookuser_collection = Mage::getResourceModel('facebookapi/facebookuser_collection');
        // debug($this->model_facebookuser_collection->getData());exit();
	}

 	public function getFacebookFriendBuy()
    {
        /**
         * Get Facebook Freidns User List
         * 
         * */
        $facebook = $this->facebook;        
        $fb_data_tmp = $this->facebook->getUserFriendList('',1000);
        $fb_data_tmp = $fb_data_tmp['data'];

        /**
         * Get User list that register via Facebook plugin
         * */
        // $this->model_facebookuser_collection->addCountFacebookUser();
        $local_data_tmp = $this->model_facebookuser_collection->getData();
  
        /**
         * Prepare Data
         * 
         * */
        $fb_data = array();
        foreach ($fb_data_tmp as $key => $value) {
            $fb_data[$value['id']] = $value;
        }

        $local_data = array();
        foreach ($local_data_tmp as $key => $value) {
            $local_data[$value['fb_id']] = $value;
        }

        // debug( $fb_data );
        // debug( $local_data );
        // debug( array_intersect($local_data, $fb_data) );

        /**
         * Unset current login user data
         * 
         * */
        unset($local_data[$this->facebook->getUser()]);
        $friend_intersect = array_intersect($local_data, $fb_data);
        // debug($friend_intersect);


        $friend_user_buy = array();
        foreach ($friend_intersect as $fb_id => $tmp_value)
        {
            $customer_id = $local_data[$fb_id]['customer_id'];
            $name = $fb_data[$fb_id]['name'];

            /**
             * Get user order totals collection
             * 
             * */
            $model_custommertotals_collection = Mage::getResourceModel('facebookapi/customer_totals_collection');
            $model_custommertotals_collection
                ->joinCustomerName()
                ->addBillingData()
                // ->addTotalOrderAmount()
                ->addCustomerData()
                // ->addFacebookUserData()
                ->addFieldToFilter('customer_id', $customer_id)
                ->orderByCreatedAt();
                // ->orderByTotalAmount();

            if( ! empty($model_custommertotals_collection) )
            {
                foreach ($model_custommertotals_collection as $entity_id => $totals_collection)
                {
                    /**
                     *  Get user order collection
                     * 
                     * */
                    $model_orders_collection = Mage::getResourceModel('sales/order_collection');
                    $model_orders_collection
                    ->addFieldToSelect('*')
                    ->addFieldToFilter('entity_id', $entity_id)
                    ->addFieldToFilter('customer_id', $customer_id);

                    /**
                     * Get user order items list
                     * 
                     * */
                    $order_items_list = array();
                    foreach($model_orders_collection as $order_id => $order)
                    {
                        $items = $order->getAllVisibleItems();
                        foreach($items as $item)
                        {
                            $my_product = Mage::getModel('catalog/product')->load($item->getProductId());
                            $product_detail = $my_product->getData();
                            $product_detail['url_product'] = $my_product->getProductUrl();

                            $item_detail = $item->getData();
                            $item_detail['product_detail'] = $product_detail;
                            $order_items_list[] = $item_detail;
                        }
                    }

                    /**
                     * Put order totals and order items list together
                     * 
                     * */
                    $totals = $totals_collection->getData();
                    $totals['items'] = $order_items_list;
                    $fb_order_list[$fb_id][] = $totals;
                }          
            }

            /**
             * Grouping data
             * 
             * */
            if( ! empty($fb_order_list[$fb_id]) )
            {
                $friend_user_buy[] = array(
                                    'customer_id' => $customer_id,
                                    'fb_id' => $fb_id,
                                    'name' => $name,
                                    'orders' => $fb_order_list[$fb_id]
                                    );
            }
        }

        // debug( $friend_user_buy );

        return $friend_user_buy;
    }

}