<?php

class Petloft_FacebookApi_Block_Adminhtml_System_Config_Time extends Mage_Adminhtml_Block_System_Config_Form_Field
{
    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        $time = new Varien_Data_Form_Element_Time;
        $format = Mage::app()->getLocale()->getTimeFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);

        $data = array(
            'name'      => $element->getName(),
            'html_id'   => $element->getId()
        );
        $time->setData($data);
        $time->setValue($element->getValue(), $format);
        $time->setFormat(Mage::app()->getLocale()->getTimeFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT));
        $time->setForm($element->getForm());

        return $time->getElementHtml();
    }
}