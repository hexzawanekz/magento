<?php

class Petloft_FacebookApi_Model_Resource_Facebookuser_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    /**
     * Define resource model
     *
     */
    protected function _construct()
    {
        $this->_init('facebookapi/facebookuser');
    }

    public function addCountFacebookUser()
    {
        $this->getSelect()
            ->columns(array('count_fb_user' => 'COUNT(main_table.fb_id)'));

        return $this;
    }
}
