<?php

/**
 * Petloft extension for Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade
 * the Petloft Inventory module to newer versions in the future.
 * If you wish to customize the Petloft Inventory module for your needs
 * please refer to http://www.magentocommerce.com for more information.
 *
 * @category   Petloft
 * @package    Petloft_Inventory
 * @copyright  Copyright (C) 2011 Petloft Web ltd (http://Petloftweb.com/)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
$installer = $this;
$installer->startSetup();

$installer->addAttribute('catalog_product', 'is_kit', array(
    'group' => 'general',
    'type' => 'int',
    'input' => 'text',
    'label' => Mage::helper('catalog/product')->__('Is Kit'),
    'global' => 1,
    'visible' => 0,
    'required' => 0,
    'user_defined' => 1,
    'default_value' => null,
    'visible_on_front' => 0,
));

$installer->updateAttribute('catalog_product', 'is_kit', 'is_visible', '0');

$installer->endSetup();
