<?php

/**
 * Petloft extension for Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade
 * the Petloft Inventory module to newer versions in the future.
 * If you wish to customize the Petloft Inventory module for your needs
 * please refer to http://www.magentocommerce.com for more information.
 *
 * @category   Petloft
 * @package    Petloft_Inventory
 * @copyright  Copyright (C) 2013
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Stock inventory observer model
 *
 * @category   Petloft
 * @package    Petloft_Inventory
 * @subpackage Model
 * @author
 */
class Petloft_Inventory_Model_Observer
{
    const REPLACE_THAI_WHOLESALE_ORDER_NUMBER_PATH = 'wholesale_settings/wholesale/convert_thai_order_number';
    const REPLACE_ENG_WHOLESALE_ORDER_NUMBER_PATH = 'wholesale_settings/wholesale/convert_eng_order_number';

    /**
     * After update cart items remove sold out and expired deals
     *
     * @param Varien_Event_Observer $observer observer
     *
     * @return void
     */
    public function cartUpdateItemsAfter($observer)
    {
        /* @var $cart Mage_Checkout_Model_Cart */
        $cart = $observer->getCart();
        $quote = $cart->getQuote();
        Mage::getModel('inventory/validator')->validateQuote($quote);
    }

    /**
     * Check person limit before add product to cart
     *
     * @param Varien_Event_Observer $observer observer
     *
     * @return void
     */
    public function checkPersonLimit(Varien_Event_Observer $observer)
    {
        Mage::getModel('inventory/validator')->validateBeforeAddProduct($observer);
    }

    /**
     * Remove max_deal_qty and person_limit from mass update attribute
     *
     * @param Varien_Event_Observer $observer observer
     *
     * @return void
     */
    public function removeInventoryAttributes(Varien_Event_Observer $observer)
    {
        $block = $observer->getObject();

        $attributesArray = $block->getFormExcludedFieldList();
        array_push($attributesArray, 'person_limit');
        $block->setFormExcludedFieldList($attributesArray);
    }

    protected function _updateStockBalance($skus)
    {
        $kits = Mage::getModel('inventory/kit')->getCollection()
            ->addFieldToFilter('item_external_id', array('in' => $skus));

        foreach ($kits as $kit) {
            $sku = $kit->getExternalId();
            $kitProductId = Mage::getModel('catalog/product')->getIdBySku($sku);
            if ($kitProductId) {
                $kitStockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($kitProductId);
                $qty = $kitStockItem->getQty();
                $kitItems = Mage::getModel('inventory/kit')->getCollection()
                    ->addFieldToFilter('external_id', array('eq' => $sku));
                $itemsQty = array();
                foreach ($kitItems as $item) {
                    $productId = Mage::getModel('catalog/product')->getIdBySku($item->getItemExternalId());
                    $stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($productId);
                    //Mage::log($qty.':'.$stockItem->getQty().':'.((int) ($stockItem->getQty() / $item->getQuantity())), null, 'test.log');
                    $itemsQty[] = (int)($stockItem->getQty() / $item->getQuantity());
                }

                if ($qty > min($itemsQty)) {
                    $kitStockItem->subtractQty(($qty - min($itemsQty)));
                    $kitStockItem->save();
                } elseif ($qty < min($itemsQty)) {
                    Mage::getSingleton('cataloginventory/stock')->backItemQty($kitProductId, (min($itemsQty) - $qty));
                }
            }
        }
    }

    /*protected function _updateStockBalance($sku, $itemSku = null)
    {
        $kits = Mage::getModel('inventory/kit')->getCollection()
                ->addFieldToFilter('item_external_id', array('eq' => $sku));

        if (!is_null($itemSku)) {
            $kits->addFieldToFilter('external_id', array('neq' => $itemSku));
        }        

        foreach ($kits as $kit) {
            $sku = $kit->getExternalId();
            $kitProductId = Mage::getModel('catalog/product')->getIdBySku($sku);
            if ($kitProductId) {
                $kitStockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($kitProductId);
                $qty = $kitStockItem->getQty();                
                $kitItems = Mage::getModel('inventory/kit')->getCollection()
                        ->addFieldToFilter('external_id', array('eq' => $sku));
                $itemsQty = array();
                foreach ($kitItems as $item) {                                        
                    $productId = Mage::getModel('catalog/product')->getIdBySku($item->getItemExternalId());
                    $stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($productId);
                    //Mage::log($qty.':'.$stockItem->getQty().':'.((int) ($stockItem->getQty() / $item->getQuantity())), null, 'test.log');
                    $itemsQty[] = (int) ($stockItem->getQty() / $item->getQuantity());
                }

                if ($qty > min($itemsQty)) {
                    $kitStockItem->subtractQty(($qty - min($itemsQty)));
                    $kitStockItem->save();
                } elseif ($qty < min($itemsQty)) {
                    Mage::getSingleton('cataloginventory/stock')->backItemQty($kitProductId, (min($itemsQty) - $qty));
                }
            }
        }
    }*/

    /**
     * Increase limit record
     *
     * @param Varien_Event_Observer $observer observer
     *
     * @return void
     */
    public function addToLimit($observer)
    {
        $quote = $observer->getEvent()->getQuote();
        $customerId = $quote->getCustomerId();
        if(!$customerId){
            $customerId = Mage::helper('customer')->getCurrentCustomer()->getId();
        }

        foreach ($quote->getAllItems() as $item) {
            $product = $item->getProduct();
            $stockItem = $product->getStockItem();
            if ($stockItem->getPersonLimit()) {
                $limit = Mage::getModel('inventory/person_limit')
                    ->loadLimit($product->getId(), $customerId);
                if (!$limit->getId()) {
                    $limit->setData('qty', $item->getQty());
                    $limit->save();
                } else {
                    $limit->setData('qty', $limit->getData('qty') + $item->getQty());
                    $limit->save();
                }
            }
            //Update revenue
        }
        return $this;
    }

    public function cancelOrderItem($observer)
    {
        $item = $observer->getEvent()->getItem();

        if ($item->getIsWholesale()) {
            return $this;
        }

        $children = $item->getChildrenItems();
        $qty = $item->getQtyOrdered() - max($item->getQtyShipped(), $item->getQtyInvoiced()) - $item->getQtyCanceled();

        if ($item->getId() && ($productId = $item->getProductId()) && empty($children) && $qty) {
            Mage::getSingleton('cataloginventory/stock')->backItemQty($productId, $qty);
        }

        return $this;
    }

    public function observerSalesOrderItemCancelAction($observer)
    {
        $this->reduceLimitCancel($observer);
        $this->cancelOrderItem($observer);

        return $this;
    }

    /**
     * Reduce limit record
     *
     * @param Varien_Event_Observer $observer observer
     *
     * @return void
     */
    public function reduceLimit($observer)
    {
        $creditmemo = $observer->getEvent()->getCreditmemo();
        foreach ($creditmemo->getAllItems() as $item) {
            if ($item->getParentItemId()) {
                continue;
            }
            $orderItem = $item->getOrderItem();
            $product = $orderItem->getProduct();
            $stockItem = $product->getStockItem();
            if ($stockItem->getPersonLimit()) {
                $limit = Mage::getModel('inventory/person_limit')
                    ->loadLimit($product->getId(), $creditmemo->getData('customer_id'));
                if ($limit->getId()) {
                    $count = $limit->getData('qty') - $item->getQty();
                    if ($count >= 0) {
                        $limit->setData('qty', $limit->getData('qty') - $item->getQty());
                        $limit->save();
                    }else{
                        $limit->setData('qty', 0);
                        $limit->save();
                    }
                }
            }
        }
        return $this;
    }

    public function reduceLimitCancel($observer)
    {
        $item = $observer->getEvent()->getItem();
        $order = Mage::getModel('sales/order')->load($item->getData('order_id'));
        $customerId = $order->getCustomerId();
        $product = $item->getProduct();
        $stockItem = $product->getStockItem();

        if ($stockItem->getPersonLimit()) {
            $limit = Mage::getModel('inventory/person_limit')
                ->loadLimit($product->getId(), $customerId);
            if ($limit->getId()) {
                $count = $limit->getData('qty') - $item->getData('qty_ordered');
                if ($count >= 0) {
                    $limit->setData('qty', $limit->getData('qty') - $item->getData('qty_ordered'));
                    $limit->save();
                }else{
                    $limit->setData('qty', 0);
                    $limit->save();
                }
            }
        }

        return $this;
    }

    public function subtractKitItem($observer)
    {
        $quote = $observer->getEvent()->getQuote();
        foreach ($quote->getAllItems() as $item) {
            if ($item->getParentItemId()) {
                continue;
            }

            $kitSku = $item->getSku();
            $productId = Mage::getModel('catalog/product')->getIdBySku($kitSku);
            $isKit = $this->_getAttributeValue($productId, 'is_kit');
            if ($isKit) {
                $kits = Mage::getModel('inventory/kit')->getCollection()
                    ->addFieldToFilter('external_id', array('eq' => $kitSku));
                if ($kits) {
                    $skus = array();
                    foreach ($kits as $kit) {
                        $sku = trim($kit->getItemExternalId());
                        if ($sku != '') {
                            $productId = Mage::getModel('catalog/product')->getIdBySku($sku);
                            if ($productId) {
                                $stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($productId);
                                $stockItem->subtractQty(($kit->getQuantity() * $item->getQty()));
                                $stockItem->save();
                                $skus[] = $sku;
                            }
                        }
                    }
                    $this->_updateStockBalance($skus);
                }
            } else {
                $this->_updateStockBalance(array($kitSku));
            }
        }
        return $this;
    }

    /**
     * Cancel order item
     *
     * @param   Varien_Event_Observer $observer
     * @return  Mage_CatalogInventory_Model_Observer
     */
    public function returnKitItem($observer)
    {
        $order = $observer->getEvent()->getOrder();

        foreach ($order->getAllItems() as $item) {
            if ($item->getParentItemId()) {
                continue;
            }
            $kitSku = $item->getSku();
            $productId = Mage::getModel('catalog/product')->getIdBySku($kitSku);
            $isKit = $this->_getAttributeValue($productId, 'is_kit');
            if ($isKit) {
                $kits = Mage::getModel('inventory/kit')->getCollection()
                    ->addFieldToFilter('external_id', array('eq' => $kitSku));
                if ($kits) {
                    $skus = array();
                    foreach ($kits as $kit) {
                        $sku = trim($kit->getItemExternalId());
                        if ($sku != '') {
                            $productId = Mage::getModel('catalog/product')->getIdBySku($sku);
                            if ($productId) {
                                Mage::getSingleton('cataloginventory/stock')->backItemQty($productId,
                                    ($kit->getQuantity() * $item->getQtyOrdered()));
                                $skus[] = $sku;
                            }
                        }
                    }
                    $this->_updateStockBalance($skus);
                }
            } else {
                $this->_updateStockBalance(array($kitSku));
            }
        }
        return $this;
    }

    public function refundKitItem($observer)
    {
        /* @var $creditmemo Mage_Sales_Model_Order_Creditmemo */
        $creditmemo = $observer->getEvent()->getCreditmemo();
        $items = array();
        foreach ($creditmemo->getAllItems() as $item) {
            $orderItem = $item->getOrderItem();
            if ($orderItem->getParentItemId()) {
                continue;
            }

            $kitSku = $item->getSku();
            $productId = Mage::getModel('catalog/product')->getIdBySku($kitSku);
            $isKit = $this->_getAttributeValue($productId, 'is_kit');
            $datas = array();
            if ($isKit) {
                $kits = Mage::getModel('inventory/kit')->getCollection()
                    ->addFieldToFilter('external_id', array('eq' => $kitSku));
                if ($kits) {
                    $skus = array();
                    foreach ($kits as $kit) {
                        $sku = trim($kit->getItemExternalId());
                        if ($sku != '') {
                            $productId = Mage::getModel('catalog/product')->getIdBySku($sku);
                            if ($productId) {
                                Mage::getSingleton('cataloginventory/stock')->backItemQty($productId,
                                    ($kit->getQuantity() * $item->getQty()));
                                $skus[] = $sku;
                            }
                        }
                    }
                    $this->_updateStockBalance($skus);
                }
            } else {
                $this->_updateStockBalance(array($kitSku));
            }
        }
        return $this;
    }

    public function checkKitItemQty($observer)
    {
        $quoteItem = $observer->getEvent()->getItem();
        if (!$quoteItem || !$quoteItem->getProductId() || !$quoteItem->getQuote()
            || $quoteItem->getQuote()->getIsSuperMode()
        ) {
            return $this;
        }
        //Mage::log($quoteItem->getProductId().':'.$quoteItem->getSku().':'. $quoteItem->getQty(), null, 'test.log');
        //  Mage::throwException(Mage::helper('cataloginventory')
        //                            ->__('xxx.'));
    }

    public function wholesaleConvertQuoteToOrder(Varien_Event_Observer $observer)
    {
        $quote = $observer->getEvent()->getQuote();
        $order = $observer->getEvent()->getOrder();
        $order->setIsWholesale($quote->getIsWholesale());

        if ($quote->getIsWholesale()) {
            $oldIncrementId = $order->getIncrementId();
            if ($order->getStoreId() == 16) { // EN Store
                $replacement = Mage::getStoreConfig(self::REPLACE_ENG_WHOLESALE_ORDER_NUMBER_PATH);
                $newIncrementId = substr_replace($oldIncrementId, $replacement, 0, 2);
            }

            if ($order->getStoreId() == 17) { // TH Store
                $replacement = Mage::getStoreConfig(self::REPLACE_THAI_WHOLESALE_ORDER_NUMBER_PATH);
                $newIncrementId = substr_replace($oldIncrementId, $replacement, 0, 2);
            }

            $order->setIncrementId($newIncrementId);
        }

        return $this;
    }

    public function wholesaleConvertQuoteItemToOrderItem(Varien_Event_Observer $observer)
    {
        $orderItem = $observer->getEvent()->getOrderItem();
        $quoteItem = $observer->getEvent()->getItem();
        $orderItem->setIsWholesale($quoteItem->getIsWholesale());

        return $this;
    }

    public function wholesaleQuoteAddItem(Varient_Event_Observer $observer)
    {
        $item = $observer->getEvent()->getQuoteItem();
        $stockItem = $item->getProduct()->getStockItem();

        if ($stockItem->isWholesaleQty($item->getProduct()->getQty())) {
            $item->setIsWholesale(1);
            $item->getQuote()->setIsWholesale(1);
        }

        return $this;
    }

    public function wholesaleQuoteRemoveItem(Varient_Event_Observer $observer)
    {
        $item = $observer->getEvent()->getQuoteItem();
        $quoteItemCollection = Mage::getModel('sales/quote_item')->getCollection()->addFieldToFilter('is_wholesale', 1)->addFieldToFilter('quote_id', $item->getQuoteId());

        if ($item->getIsWholesale() == 1 && $item->getQuote()->getIsWholesale() == 1 && count($quoteItemCollection->getData()) == 1) {
            $item->getQuote()->setIsWholesale(0);
        }

        return $this;
    }

    /**
     * Get Raw attribute value
     *
     * @param int $productId product id
     * @param string $attributeCode code of attribute
     *
     * @return mixed
     */
    protected function _getAttributeValue($productId, $attributeCode)
    {
        return Mage::getResourceModel('catalog/product')
            ->getAttributeRawValue($productId, $attributeCode, Mage_Core_Model_App::ADMIN_STORE_ID);
    }
}
