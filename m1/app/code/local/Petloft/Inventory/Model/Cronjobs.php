<?php

/**
 * Petloft Web extension for Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade
 * the Petloft Inventory module to newer versions in the future.
 * If you wish to customize the Petloft Inventory module for your needs
 * please refer to http://www.magentocommerce.com for more information.
 *
 * @category   Petloft
 * @package    Petloft_Inventory
 * @copyright  Copyright (C) 2013
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Inventory validator model
 *
 * @category   Petloft
 * @package    Petloft_Inventory
 * @subpackage Model
 * @author
 */
class Petloft_Inventory_Model_Cronjobs
{

    /**
     * Retrieve information from payment configuration
     *
     * @param string $field
     * @param int|string|null|Mage_Core_Model_Store $storeId
     *
     * @return mixed
     */
    public function getConfigData($field, $storeId = null)
    {
        $path = 'netsuiteexport/netsuiteapi/' . $field;
        return Mage::getStoreConfig($path, $storeId);
    }

    public function updateInventory($item) {

        if (trim($item->externalId) != '') {
            $productId = Mage::getModel('catalog/product')->getIdBySku($item->externalId);
            if ($productId) {
                Mage::getSingleton('catalog/product_action')
                    ->updateAttributes(array($productId), array('is_kit' => $item->isKit), Mage_Core_Model_App::ADMIN_STORE_ID);

                $stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($productId);
                if($stockItem->getId()) {
                    $stockItem->setQty($item->quantityAvailable);
                    $flag = false;
                    if ($stockItem->getQty() > $stockItem->getMinQty()) {
                        $flag = true;
                    }
                    $stockItem->setIsInStock($flag)
                            ->setStockStatusChangedAutomaticallyFlag(true);
                    $stockItem->save();
                }
            }
            sleep(5);
        }
    }

    public function updateKitAttribute($item) {
       if (trim($item->externalId) != '') {
            $productId = Mage::getModel('catalog/product')->getIdBySku($item->externalId);
            if ($productId) {
                Mage::getSingleton('catalog/product_action')
                    ->updateAttributes(array($productId), array('is_kit' => $item->isKit), Mage_Core_Model_App::ADMIN_STORE_ID);

            }
        }
    }

    public function updateStockWithCommand() {
        $con = Mage::getSingleton('core/resource')->getConnection('core_write');

        $sql = "INSERT INTO `cataloginventory_stock_item`
                (
                   `product_id`,
                   `stock_id`,
                   `qty`,
                   `min_qty`,
                   `use_config_min_qty`,
                   `is_qty_decimal`,
                   `backorders`,
                   `use_config_backorders`,
                   `min_sale_qty`,
                   `use_config_min_sale_qty`,
                   `max_sale_qty`,
                   `use_config_max_sale_qty`,
                   `is_in_stock`,
                   `low_stock_date`,
                   `notify_stock_qty`,
                   `use_config_notify_stock_qty`,
                   `manage_stock`,
                   `use_config_manage_stock`,
                   `stock_status_changed_auto`,
                   `use_config_qty_increments`,
                   `qty_increments`,
                   `use_config_enable_qty_inc`,
                   `enable_qty_increments`,
                   `is_decimal_divided`
                )
                SELECT p.entity_id,
                        1 AS stock_id,
                        ns.quantity_available AS total_qty,
                        0 AS min_qty, 1 AS use_config_min_qty,
                        0 AS is_qty_decimal,
                        0 AS backorders,
                        1 AS use_config_backorders,
                        1 AS min_sale_qty,
                        1 AS use_config_min_sale_qty,
                        0 AS max_sale_qty,
                        1 AS use_config_max_sale_qty,
                        IF(ns.quantity_available > 0, 1, 0) AS is_in_stock,
                        NULL AS low_stock_date,
                        NULL AS notify_stock_qty,
                        1 AS use_config_notify_stock_qty,
                        0 AS manage_stock,
                        1 AS use_config_manage_stock,
                        1 AS stock_status_changed_auto,
                        1 AS use_config_qty_increments,
                        0 AS qty_increments,
                        1 AS use_config_enable_qty_inc,
                        0 AS enable_qty_increments,
                        0 AS is_decimal_divided
                FROM netsuite_inventory_item AS ns
                INNER JOIN catalog_product_entity AS p ON ns.external_id =  p.sku
                ON DUPLICATE KEY UPDATE qty = ns.quantity_available,  is_in_stock  = IF(ns.quantity_available > 0, 1, 0);";

        $con->query($sql);
    }

    public function updateNetsuiteItem($item) {

        $netsuiteItem = Mage::getModel('inventory/netsuite')->load($item->internalId);
        $netsuiteItem->setId($item->internalId);
        $netsuiteItem->setItemName($item->itemName);
        $netsuiteItem->setExternalId($item->externalId);
        $netsuiteItem->setSku($item->sku);
        $netsuiteItem->setQuantityAvailable($item->quantityAvailable);
        $netsuiteItem->setIsKit($item->isKit);
        $netsuiteItem->save();
    }

    public function updateKitItem($kitItem) {

        $netsuitekit = Mage::getModel('inventory/kit')->loadKitItem($kitItem->internalId, $kitItem->itemInternalId);

        $netsuitekit->setInternalId($kitItem->internalId);
        $netsuitekit->setExternalId($kitItem->externalId);
        $netsuitekit->setItemInternalId($kitItem->itemInternalId);
        $netsuitekit->setItemExternalId($kitItem->itemExternalId);
        $netsuitekit->setQuantity($kitItem->quantity);
        $netsuitekit->save();
    }

    public function syncInventory() {

        $startDate = Mage::app()->getLocale()->storeDate(null, null, true);
        $startDate = $startDate->toString('Y-m-d H:i:s', 'php');

        $helper = Mage::helper('inventory');
        $host = addslashes($this->getConfigData('host'));
        $email = addslashes($this->getConfigData('email'));
        $password = addslashes($this->getConfigData('password'));
        $role = $this->getConfigData('role');
        $account = $this->getConfigData('account');
        $endpoint = $this->getConfigData('endpoint');

        $saveSearch1 = $this->getConfigData('searchid1');
        $saveSearch2 = $this->getConfigData('searchid2');

        //$host = "https://webservices.netsuite.com";
        //$email = "norawich@acommerce.asia";
        //$password = "apple123";
        //$role = "3";
        //$account = "3686224";
        //$endpoint = "2013_1";


        $helper->setConfiguration($account, $email, $password, $host, $role, $endpoint);
        $records = $helper->getSaveSearchData($saveSearch1);

        foreach($records as $record) {
            $item = new stdClass();

            $item->internalId = $record->basic->internalId[0]->searchValue->internalId;
            $item->itemName = $record->basic->itemId[0]->searchValue;
            //$item->externalId = (!is_null($record->basic->externalId))?$record->basic->externalId[0]->searchValue->internalId:'';
            $item->externalId = '';

            $item->sku = '';
            if(!is_null($record->basic->customFieldList)) {
                foreach($record->basic->customFieldList->customField as $customField) {
                    //echo 'Item ::'.$customField->internalId."\n";
                    if($customField->internalId == 'custitem11') {
                       $item->sku = $customField->searchValue;
                    } elseif($customField->internalId == 'custitem23') {
                        $item->externalId = $customField->searchValue;
                    }
                }
            }

            $item->quantityAvailable = (!is_null($record->basic->quantityAvailable))?$record->basic->quantityAvailable[0]->searchValue:0;
            $item->isKit = 0;

            $this->updateNetsuiteItem($item);
            //$this->updateInventory($item);
            $this->updateKitAttribute($item);

        }


        $records = $helper->getSaveSearchData($saveSearch2);
        $items = array();
        $kitItems = array();

        foreach($records as $record) {
            if(!is_null($record->memberItemJoin)) {
                $item = new stdClass();
                $kitItem = new stdClass();

                $item->internalId = $record->basic->internalId[0]->searchValue->internalId;
                $item->itemName = $record->basic->itemId[0]->searchValue;
                //$item->externalId = (!is_null($record->basic->externalId))?$record->basic->externalId[0]->searchValue->internalId:'';
                $item->externalId = '';
                $item->sku = '';
                $kitItem->externalId = '';

                if(!is_null($record->basic->customFieldList)) {
                    foreach($record->basic->customFieldList->customField as $customField) {
                        //echo 'Kit ::'.$customField->internalId."\n";
                        if($customField->internalId == 'custitem11') {
                           $item->sku = $customField->searchValue;
                        } elseif($customField->internalId == 'custitem23') {
                            $item->externalId = $customField->searchValue;
                            $kitItem->externalId = $customField->searchValue;
                        }
                    }
                }

                //echo $item->externalId."\n";
                $quantityAvailable = (!is_null($record->memberItemJoin->quantityAvailable))?$record->memberItemJoin->quantityAvailable[0]->searchValue:0;

                $kitItem->internalId = $record->basic->internalId[0]->searchValue->internalId;
                //$kitItem->externalId = (!is_null($record->basic->externalId))?$record->basic->externalId[0]->searchValue->internalId:'';

                $kitItem->itemInternalId = $record->basic->memberItem[0]->searchValue->internalId;
                //$kitItem->itemExternalId = $record->memberItemJoin->externalId[0]->searchValue->internalId;
                $kitItem->itemExternalId = '';
                if(!is_null($record->memberItemJoin->customFieldList)) {
                    foreach($record->memberItemJoin->customFieldList->customField as $customField) {
                        if($customField->internalId == 'memberitem_custitem23') {
                            $kitItem->itemExternalId = $customField->searchValue;
                        }
                    }
                }


                $kitItem->quantity = $record->basic->memberQuantity[0]->searchValue;
                $kitItems[] = $kitItem;

                $quantityAvailable = (int)($quantityAvailable / $kitItem->quantity);

                if(isset($items[$item->internalId])) {
                    $item = $items[$item->internalId];
                    $item->quantityAvailable = ($item->quantityAvailable < $quantityAvailable)? $item->quantityAvailable : $quantityAvailable;
                    $items[$item->internalId] = $item;
                } else {
                    $item->quantityAvailable = $quantityAvailable;
                    $item->isKit = 1;
                    $items[$item->internalId] = $item;
                }
            }
        }

        foreach($items as $item) {
            $this->updateNetsuiteItem($item);
            //$this->updateInventory($item);
            $this->updateKitAttribute($item);
        }

        //$pCollection = Mage::getSingleton('index/indexer')->getProcessesCollection();
        //foreach ($pCollection as $process) {
        //$process->setMode(Mage_Index_Model_Process::MODE_MANUAL)->save();
        //  $process->setMode(Mage_Index_Model_Process::MODE_REAL_TIME)->save();
        //}

        foreach($kitItems as $kitItem) {
            $this->updateKitItem($kitItem);
        }

        $this->updateStockWithCommand();
        // $process = Mage::getSingleton('index/indexer')->getProcessByCode("cataloginventory_stock");
        // $process->reindexAll();
        $process = Mage::getSingleton('index/indexer');
        $process->getProcessByCode('cataloginventory_stock')
            ->changeStatus(Mage_Index_Model_Process::STATUS_REQUIRE_REINDEX);
        $endDate = Mage::app()->getLocale()->storeDate(null, null, true);
        $endDate = $endDate->toString('Y-m-d H:i:s', 'php');

        $mail = new Zend_Mail('utf-8');
        $mail->addTo('nattawut.m@acommerce.asia');
        $mail->addCc('ranai@acommerce.asia');

        $mail->setFrom('support@acommerce.asia');
        $mail->setSubject("Inventory Sync");

        $content = "Start Date: ".$startDate."\n".
                   "End Date: ".$endDate."\n".
                   "Status: Completed\n";
        $mail->setBodyHtml($content, 'UTF-8');
        $mail->send();


        $base_path = Mage::getBaseDir('lib');
        $cmd = sprintf("rm -f %s/PHPToolkit/nslog/*", $base_path);
        exec($cmd, $result);
    }

    public function SyncInventoryItem1()
    {
        $host = addslashes($this->getConfigData('host'));
        $email = addslashes($this->getConfigData('email'));
        $password = addslashes($this->getConfigData('password'));
        $role = $this->getConfigData('role');
        $account = $this->getConfigData('account');
        $endpoint = $this->getConfigData('endpoint');
        $businessIds = $this->getConfigData('businessids');
        $businessIds = str_replace(' ', '', $businessIds);

        //$subsidiary = Mage::getStoreConfig('netsuiteexport/salesexport/subsidiary');
        //$subsidiary = explode(':', $subsidiary);
        //$subsidiary = trim($subsidiary[(count($subsidiary) - 1)]);
        //$subsidiary = addslashes($subsidiary);
        $base_path = Mage::getBaseDir('lib');

        if ($host != '' && $email != '' && $password != '' && $role != '' && $account != '' && $endpoint != '' && $businessIds != '') {
            $cmd = sprintf("cd %s/PHPToolkit; php Inventory.php %s %s %s %d %s %s %s", $base_path, $host, $email,
                           $password, $role, $account, $endpoint, $businessIds);
            //echo $cmd."\n";
            exec($cmd, $result);

            $collection = Mage::getModel('inventory/netsuite')->getCollection();
            foreach ($collection as $item) {
                if (trim($item->getExternalId()) != '') {
                    $productId = Mage::getModel('catalog/product')->getIdBySku($item->getExternalId());
                    if ($productId) {
                        echo $productId.':'.$item->getIsKit()."\n";
                        Mage::getSingleton('catalog/product_action')
                            ->updateAttributes(array($productId), array('is_kit' => $item->getIsKit()), Mage_Core_Model_App::ADMIN_STORE_ID);

                        $stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($productId);
                        $stockItem->setQty($item->getQuantityAvailable());
                        $flag = false;
                        if ($stockItem->getQty() > $stockItem->getMinQty()) {
                            $flag = true;
                        }
                        $stockItem->setIsInStock($flag)
                                ->setStockStatusChangedAutomaticallyFlag(true);
                        $stockItem->save();
                    }
                }
            }
            $cmd = sprintf("rm -f %s/PHPToolkit/nslog/*", $base_path);
            exec($cmd, $result);
        }
    }
}
