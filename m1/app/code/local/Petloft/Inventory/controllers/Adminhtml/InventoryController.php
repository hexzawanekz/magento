<?php

/**
 * Petloft extension for Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade
 * the Petloft Inventory module to newer versions in the future.
 * If you wish to customize the Petloft Inventory module for your needs
 * please refer to http://www.magentocommerce.com for more information.
 *
 * @category   Petloft
 * @package    Petloft_Inventory
 * @copyright  Copyright (C) 2013
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Inventory Controller
 *
 * @category   Petloft
 * @package    Petloft_Inventory
 * @subpackage Controller
 * @author
 */
class Petloft_Inventory_Adminhtml_InventoryController extends Mage_Adminhtml_Controller_Action
{
    /**
     * Initialize action
     *
     * @return Petloft_Inventory_Adminhtml_InventoryController
     */
    protected function _initAction()
    {
        $this->loadLayout()
                ->_setActiveMenu('catalog/inventory')
                ->_addBreadcrumb(Mage::helper('adminhtml')->__('Inventory'),
                                                               Mage::helper('adminhtml')->__('Manage Inventory'));
        return $this;
    }

    /**
     * Index Action
     *
     * @return void
     */
    public function indexAction()
    {
        $this->_initAction();
        $this->_addContent($this->getLayout()->createBlock('inventory/adminhtml_inventory'));
        $this->renderLayout();
    }

    /**
     * Rander Grid Layout
     *
     * @return void
     */
    public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
                $this->getLayout()->createBlock('inventory/adminhtml_inventory_grid')->toHtml()
        );
    }

    /**
     * Import CSV File
     *
     * @return void
     */
    public function importAction()
    {
        $rowcount = 0;
        $errorcount = 0;
        $orders = array();

        if (isset($_FILES['csvfile']['name']) and (file_exists($_FILES['csvfile']['tmp_name']))) {
            $file = $_FILES['csvfile']['tmp_name'];
            $data = $this->_parseCSV($file);

            if (count($data) < 1) {
                $this->_getSession()->addError($this->__('The file cannot be parsed.'));
                return $this->_redirect('*/*/');
            }
            foreach ($data as $row) {
                $productName = $row['product_name'];
                $product = Mage::getModel('catalog/product')->getCollection()
                        ->addAttributeToSelect('entity_id')
                        ->addAttributeToSelect('name')
                        ->addAttributeToFilter('netsuite_item_name', array('eq' => $productName))
                        ->getFirstItem();

                if ($product->getId()) {
                    $rowcount++;
                    $temp = Mage::getModel('inventory/temp');
                    $temp->setProductId($product->getId());
                    $temp->setProductName($product->getName());
                    $temp->setNetsuiteName($product->getNetsuiteItemName());
                    $temp->setQty($row['qty']);
                    $temp->save();
                } else {
                    $errorcount++;
                }
            }
            if ($errorcount > 0) {
                $ordersString = implode('<br/>', $orders);
                $this->_getSession()->addError($this->__('Total of %d row(s) has been Failed. <br/>%s', $errorcount,
                                                         $ordersString));
            }

            if ($rowcount > 0) {
                $this->_getSession()->addSuccess($this->__('Total of %d row(s) has been imported.', $rowcount));
            }

            $this->_redirect('*/*/');
        }
    }

    /**
     * Parse CSV File to Get Codes
     *
     * @param string $file file to parse
     *
     * @return array
     */
    protected function _parseCSV($file)
    {
        $mageCsv = new Varien_File_Csv();
        $data = array();
        try {
            $data = $mageCsv->getData($file);
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            return $data;
        }

        $keys = array('product_name', 'qty');
        foreach ($data as &$value) {
            if (count($value) == count($keys)) {
                $keys = array_slice($keys, 0, count($value));
                $value = array_combine($keys, $value);
            }
        }
        return $data;
    }

    /**
     * Delete temp data
     *
     * @return void
     */
    public function massDeleteAction()
    {
        $itemIds = $this->getRequest()->getParam('item_ids');
        foreach ($itemIds as $id) {
            $temp = Mage::getModel('inventory/temp')->load($id);
            if ($temp->getId()) {
                $temp->delete();
            }
        }
        $this->_getSession()->addSuccess($this->__('The seleted items have been successfully deleted.'));
        $this->_redirect('*/*/');
    }

    /**
     * Update Inventories to stock
     *
     * @return void
     */
    public function massAddAction()
    {
        $itemIds = $this->getRequest()->getParam('item_ids');
        foreach ($itemIds as $id) {
            $temp = Mage::getModel('inventory/temp')->load($id);
            if ($temp->getId()) {
                Mage::getSingleton('cataloginventory/stock')->backItemQty($temp->getProductId(), $temp->getQty());
                $temp->delete();
            }
        }
        $this->_getSession()->addSuccess($this->__('The inventories have been successfully added to stock.'));
        $this->_redirect('*/*/');
    }

    /**
     * Subtract inventory from stock
     *
     * @return void
     */
    public function massSubtractAction()
    {
        $itemIds = $this->getRequest()->getParam('item_ids');
        foreach ($itemIds as $id) {
            $temp = Mage::getModel('inventory/temp')->load($id);
            if ($temp->getId()) {
                $stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($temp->getProductId());
                $stockItem->subtractQty($temp->getQty());
                $stockItem->save();
                $temp->delete();
            }
        }
        $this->_getSession()->addSuccess($this->__('The inventories have been successfully subtracted from stock.'));
        $this->_redirect('*/*/');
    }

    /**
     * Update Data In Stock
     *
     * @return void
     */
    public function massUpdateAction()
    {
        $itemIds = $this->getRequest()->getParam('item_ids');
        foreach ($itemIds as $id) {
            $temp = Mage::getModel('inventory/temp')->load($id);
            if ($temp->getId()) {
                $stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($temp->getProductId());
                $stockItem->setQty($temp->getQty());
                if ($stockItem->getCanBackInStock() && $stockItem->getQty() > $stockItem->getMinQty()) {
                    $stockItem->setIsInStock(true)
                            ->setStockStatusChangedAutomaticallyFlag(true);
                }
                $stockItem->save();
                $temp->delete();
            }
        }
        $this->_getSession()->addSuccess($this->__('The inventories have been successfully updated in stock.'));
        $this->_redirect('*/*/');
    }
}