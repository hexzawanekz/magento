<?php

class IWD_OnepageCheckout_Helper_Data extends Mage_Core_Helper_Abstract
{
    protected $_agree = null;

    public function isOnepageCheckoutEnabled()
    {
        return (bool)Mage::getStoreConfig('onepagecheckout/general/enabled');
    }

    public function isGuestCheckoutAllowed()
    {
        return Mage::getStoreConfig('onepagecheckout/general/guest_checkout');
    }

    public function isShippingAddressAllowed()
    {
    	return Mage::getStoreConfig('onepagecheckout/general/shipping_address');
    }

    public function getAgreeIds()
    {
        if (is_null($this->_agree))
        {
            if (Mage::getStoreConfigFlag('onepagecheckout/agreements/enabled'))
            {
                $this->_agree = Mage::getModel('checkout/agreement')->getCollection()
                    												->addStoreFilter(Mage::app()->getStore()->getId())
                    												->addFieldToFilter('is_active', 1)
                    												->getAllIds();
            }
            else
            	$this->_agree = array();
        }
        return $this->_agree;
    }
    
    public function isSubscribeNewAllowed()
    {
        if (!Mage::getStoreConfig('onepagecheckout/general/newsletter_checkbox'))
            return false;

        $cust_sess = Mage::getSingleton('customer/session');
        if (!$cust_sess->isLoggedIn() && !Mage::getStoreConfig('newsletter/subscription/allow_guest_subscribe'))
            return false;

		$subscribed	= $this->getIsSubscribed();
		if($subscribed)
			return false;
		else
			return true;
    }
    
    public function getIsSubscribed()
    {
        $cust_sess = Mage::getSingleton('customer/session');
        if (!$cust_sess->isLoggedIn())
            return false;

        return Mage::getModel('newsletter/subscriber')->getCollection()
            										->useOnlySubscribed()
            										->addStoreFilter(Mage::app()->getStore()->getId())
            										->addFieldToFilter('subscriber_email', $cust_sess->getCustomer()->getEmail())
            										->getAllIds();
    }
    
    public function getOPCVersion()
    {
    	return (string) Mage::getConfig()->getNode()->modules->IWD_OnepageCheckout->version;
    }
    
    public function getMagentoVersion()
    {
		$ver_info = Mage::getVersionInfo();
		$mag_version	= "{$ver_info['major']}.{$ver_info['minor']}.{$ver_info['revision']}.{$ver_info['patch']}";
		
		return $mag_version;
    }  

    public function getNoItemsText()
    {
		$url	= 'http://www.interiorwebdesign.com/magento/opclicense/nocartitemstext.php';

		$text	= file_get_contents($url);
		if(empty($text))
			$text = 'jZNBj9owEIXv+ytGUauAtEnUaxuCdilakMouAlYVp8hxhtjCsS3b2Yj++joECgdKe4qUvJn3zczLOEtL/gFUEGtHgSYVRo47gUGWsi9ZOtZMA1Km4JNj3EZZng/CzWwK69nbcjl/fYHJ02oD8zVM3ler6evmxxami+VmGw5hnKVJ1yPxBtnDtQ0lxkVYa3cIblhU6BZorUexz0LR/WB4fPdiVKOxnLlaDI7NbxVOGBe9JKQM6V41Lu/c8qNb3vLSi8LhN1//kOq/zLdVjQHLlNZcVtCVA7dAG2NQOnGAY69H0AKJRUgJMIO7UfDZBhlV0nHZ4J/yNCFZHD5eI54k65Pi3fh5+m3pO1Ab1jGcZgJ7sA5raImFouHCQXGA+c/vQEAbtfPb40oSAeHtRXX9rrHBEePJRkFeCCL3QdZiASVaXkmgqtZEHro5wM8RMuf01yRp2zbm0qHhynh1L469OLnjaTVSTgT/1e2VS7jLgBNV12gowoXm/ymQnqojfxLbCL/yKuq/+8cHCqVrf807sEBkCRcIp5SwYBvKwC/9LvnCR1c6Bcu3NayPhzqn4MztT+U/dqDh5fL/yPM5UXEXyNiRQmBMdn72Ps79b5aOfwM=';
		
		return $text;    	
    }
}