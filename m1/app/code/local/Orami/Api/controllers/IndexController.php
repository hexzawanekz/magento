<?php

/**
 * Class Orami_Api_IndexController
 */
class Orami_Api_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        $cmd = ($this->getRequest()->getParam('cmd')) ? ($this->getRequest()->getParam('cmd')) : 'daily_sale';
        switch ($cmd) {
            case 'menu' : // OK
                $obj = Mage::getModel('petloftcatalog_menucache/cache');
                $collection = $obj->getCollection()->addFieldtoFilter('path', 'moxyen/api');
                foreach ($collection as $menu) {
                    $path = $menu->getPath();
                    if ($path = 'moxyen/api') {
                        $en = $menu->getHtmlCache();
                    }
                }
                $data = stripslashes(json_encode($en));
                $data = str_replace('"[{', '[{', $data);
                $data = str_replace('}]"', '}]', $data);
                echo '{"code":0,"msg":null,"model":' . $data . '}';
                break;
            case 'catalog' :
                Mage::app()->setCurrentStore(16);
                $category_id = $this->getRequest()->getParam('category_id');
                $page = ($this->getRequest()->getParam('page')) ? ($this->getRequest()->getParam('page')) : 1;
                $limit = ($this->getRequest()->getParam('limit')) ? ($this->getRequest()->getParam('limit')) : 10;
                $order = ($this->getRequest()->getParam('order')) ? ($this->getRequest()->getParam('order')) : 'entity_id';
                $dir = ($this->getRequest()->getParam('dir')) ? ($this->getRequest()->getParam('dir')) : 'desc';
                $filterValue = $this->getRequest()->getParam('filter');
                $filterName = $this->getRequest()->getParam('attribute_name');
                $month_ini = new DateTime("first day of last year");
                $category = Mage::getModel('catalog/category')->load($category_id);
                $cat_name = $category->getName();

                $filter = Mage::getModel('mobile/filter');
                $filterData = $filter->getFilterData($category,$filterValue,$filterName);
                if($filter != "" && $filter != NULL && $filterName != "" && $filterName != NULL ){
                    if ($order == 'bestseller') {
                        $collection = $category->getProductCollection()->addAttributeToFilter('status', 1)->addAttributeToFilter('visibility', array('neq' => 1))->addAttributeToFilter($filterName,$filterValue)->addAttributeToSort('revenue_last_month', $dir);
                        Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($collection);
                        $pages = $collection->setPageSize($limit)->getLastPageNumber();
                        if ($page <= $pages) {
                            $collection->setPage($page, $limit);
                            $product_list = $this->getProductList($collection, 'catalog');
                        } else {
                            $product_list = array();
                        }
                    } else {
                        $collection = $category->getProductCollection()->addAttributeToFilter('status', 1)->addAttributeToFilter('visibility', array('neq' => 1))->addAttributeToFilter($filterName,$filterValue)->addAttributeToSort($order, $dir);
                        Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($collection);
                        $pages = $collection->setPageSize($limit)->getLastPageNumber();
                        if ($page <= $pages) {
                            $collection->setPage($page, $limit);
                            $product_list = $this->getProductList($collection, 'catalog');
                        } else {
                            $product_list = array();
                        }
                    }
                }else{
                    if ($order == 'bestseller') {
                        $collection = $category->getProductCollection()->addAttributeToFilter('status', 1)->addAttributeToFilter('visibility', array('neq' => 1))->addAttributeToSort('revenue_last_month', $dir);
                        Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($collection);
                        $pages = $collection->setPageSize($limit)->getLastPageNumber();
                        if ($page <= $pages) {
                            $collection->setPage($page, $limit);
                            $product_list = $this->getProductList($collection, 'catalog');
                        } else {
                            $product_list = array();
                        }
                    } else {
                        $collection = $category->getProductCollection()->addAttributeToFilter('status', 1)->addAttributeToFilter('visibility', array('neq' => 1))->addAttributeToSort($order, $dir);
                        Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($collection);
                        $pages = $collection->setPageSize($limit)->getLastPageNumber();
                        if ($page <= $pages) {
                            $collection->setPage($page, $limit);
                            $product_list = $this->getProductList($collection, 'catalog');
                        } else {
                            $product_list = array();
                        }
                    }
                    $data['product'] = $product_list;
                }

                $data["filter"] = $filterData;
                echo json_encode(array('code' => 0, 'msg' => 'get ' . count($product_list) . ' product success!', 'category name' => $cat_name, 'model' => $data));
                break;
            case 'coming_soon' :

                $page = ($this->getRequest()->getParam('page')) ? ($this->getRequest()->getParam('page')) : 1;
                $limit = ($this->getRequest()->getParam('limit')) ? ($this->getRequest()->getParam('limit')) : 5;
                // $todayDate = Mage::app ()->getLocale ()->date ()->toString ( Varien_Date::DATETIME_INTERNAL_FORMAT );
                $tomorrow = mktime(0, 0, 0, date('m'), date('d') + 1, date('y'));
                $dateTomorrow = date('m/d/y', $tomorrow);
                $tdatomorrow = mktime(0, 0, 0, date('m'), date('d') + 3, date('y'));
                $tdaTomorrow = date('m/d/y', $tdatomorrow);
                $_productCollection = Mage::getModel('catalog/product')->getCollection();
                $_productCollection->addAttributeToSelect('*')->addAttributeToFilter('visibility', array(
                    'neq' => 1
                ))->addAttributeToFilter('status', 1)->addAttributeToFilter('special_price', array(
                    'neq' => 0
                ))->addAttributeToFilter('special_from_date', array(
                    'date' => true,
                    'to' => $dateTomorrow
                ))->addAttributeToFilter(array(
                    array(
                        'attribute' => 'special_to_date',
                        'date' => true,
                        'from' => $tdaTomorrow
                    ),
                    array(
                        'attribute' => 'special_to_date',
                        'null' => 1
                    )
                ))/* ->setPage ( $page, $limit ) */
                ;
                $pages = $_productCollection->setPageSize($limit)->getLastPageNumber();
                // $count=$collection->getSize();
                if ($page <= $pages) {
                    $_productCollection->setPage($page, $limit);
                    $products = $_productCollection->getItems();
                    $productlist = $this->getProductList($products);
                }
                echo json_encode(array('code' => 0, 'msg' => null, 'model' => $productlist));

                break;
            case 'best_seller' : // OK

                $page = ($this->getRequest()->getParam('page')) ? ($this->getRequest()->getParam('page')) : 1;
                $limit = ($this->getRequest()->getParam('limit')) ? ($this->getRequest()->getParam('limit')) : 5;
                $todayDate = Mage::app()->getLocale()->date()->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);
                $fromDate = ($todayDate) - 7;
                $_products = Mage::getResourceModel('catalog/product_collection')
                    ->addAttributeToSelect(Mage::getSingleton('catalog/config')->getProductAttributes())
                    ->addStoreFilter()
                    ->addPriceData()
                    ->addTaxPercents()
                    ->addUrlRewrite()
                    ->setPageSize(6);

                $_products->getSelect()
                    ->joinLeft(
                        array('aggregation' => $_products->getResource()->getTable('sales/bestsellers_aggregated_monthly')),
                        "e.entity_id = aggregation.product_id AND aggregation.store_id='17' AND aggregation.period BETWEEN '{$fromDate}' AND '{$todayDate}'",
                        array('SUM(aggregation.qty_ordered) AS sold_quantity')
                    )
                    ->group('e.entity_id')
                    ->order(array('sold_quantity DESC', 'e.created_at'));


                $pages = $_products->setPageSize($limit)->getLastPageNumber();
                // $count=$collection->getSize();
                if ($page <= $pages) {
                    $_products->setPage($page, $limit);
                    $products = $_products->getItems();
                    $product_list = $this->getProductList($products);
                } else {
                    $product_list = array();
                }
                echo json_encode(array('code' => 0, 'msg' => null, 'model' => $product_list));

                break;
            case 'daily_sale' :

                $page = ($this->getRequest()->getParam('page')) ? ($this->getRequest()->getParam('page')) : 1;
                $limit = ($this->getRequest()->getParam('limit')) ? ($this->getRequest()->getParam('limit')) : 5;
                $todayDate = Mage::app()->getLocale()->date()->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);
                $tomorrow = mktime(0, 0, 0, date('m'), date('d') + 1, date('y'));
                $dateTomorrow = date('m/d/y', $tomorrow);
                // $collection = Mage::getResourceModel ( 'catalog/product_collection' );
                $collection = Mage::getModel('catalog/product')->getCollection();
                $collection->/* addStoreFilter ()-> */
                addAttributeToSelect('*')->addAttributeToFilter('special_price', array(
                    'neq' => "0"
                ))->addAttributeToFilter('special_from_date', array(
                    'date' => true,
                    'to' => $todayDate
                ))->addAttributeToFilter(array(
                    array(
                        'attribute' => 'special_to_date',
                        'date' => true,
                        'from' => $dateTomorrow
                    ),
                    array(
                        'attribute' => 'special_to_date',
                        'null' => 1
                    )
                ));
                $pages = $collection->setPageSize($limit)->getLastPageNumber();
                // $count=$collection->getSize();
                if ($page <= $pages) {
                    $collection->setPage($page, $limit);
                    $products = $collection->getItems();
                    $productlist = $this->getProductList($products);
                }
                echo json_encode(array('code' => 0, 'msg' => null, 'model' => $productlist));
                // echo $count;


                break;
            case 'new_products' :

                $page = ($this->getRequest()->getParam('page')) ? ($this->getRequest()->getParam('page')) : 1;
                $limit = ($this->getRequest()->getParam('limit')) ? ($this->getRequest()->getParam('limit')) : 5;
                $todayDate = Mage::app()->getLocale()->date()->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);
//				$tomorrow = mktime ( 0, 0, 0, date ( 'm' ), date ( 'd' ) + 1, date ( 'y' ) );
//				$dateTomorrow = date ( 'm/d/y', $tomorrow );
                // $collection = Mage::getResourceModel ( 'catalog/product_collection' );
                $collection = Mage::getModel('catalog/product')->getCollection();
                $collection->/* addStoreFilter ()-> */
                addAttributeToSelect('*')->addAttributeToSort('created_at', 'desc');
                $pages = $collection->setPageSize($limit)->getLastPageNumber();
                // $count=$collection->getSize();
                if ($page <= $pages) {
                    $collection->setPage($page, $limit);
                    $products = $collection->getItems();
                    $productlist = $this->getProductList($products);
                }
                echo json_encode(array('code' => 0, 'msg' => null, 'model' => $productlist));
                // echo $count;
                break;
            case 'home' :
                // ----- slider -----
                $slide = Mage::getModel('cms/block')->load('home-slider-banner')->getContent();
                if ($slide != '') {
                    $result = Mage::helper('cms')->getBlockTemplateProcessor()->filter($slide);
                    //preg_match_all('/<img[^>]+>/i',$pageTitle, $result);
                    $doc = new DOMDocument();
                    $doc->loadHTML($result);
                    $linkTags = $doc->getElementsByTagName('a');
                    $link = array();
                    foreach ($linkTags as $tag) {
                        $link = $tag->getAttribute('href');
                        $img = $tag->getElementsByTagName('img');
                        foreach ($img as $tag2) {
                            $tagsrc = $tag2->getAttribute('src');
                        }
                        if (strpos($link, 'catalog/category')) {
                            $type = 'category';
                            $link = strstr($link, 'id/');
                            $link = substr($link, 3, 5);
                        } elseif (strpos($link, 'catalog/product')) {
                            $type = 'product';
                            $link = strstr($link, 'id/');
                            $link = substr($link, 3, 5);
                        } else {
                            $type = 'static';
                        }
                        $src = $tagsrc;
                        $slider[] = array('image' => $src, 'type' => $type, 'id' => $link);
                    }
                }
                $main1 = Mage::getModel('cms/block')->load('welcome-shortcut-1')->getContent();
                $status1 = Mage::getModel('cms/block')->load('welcome-shortcut-1')->getIsActive();
                if ($status1) {
                    if ($main1 != '') {
                        $result1 = Mage::helper('cms')->getBlockTemplateProcessor()->filter($main1);
                        $doc = new DOMDocument();
                        $doc->loadHTML($result1);
                        $mainTag = $doc->getElementsByTagName('a');
                        $maintags = array();
                        foreach ($mainTag as $tag) {
                            $mainlink1 = $tag->getAttribute('href');
                            $mainImage = $tag->getElementsByTagName('img');
                            foreach ($mainImage as $img) {
                                $mainimages1 = $img->getAttribute('src');
                            }
                            if (strpos($mainlink1, 'catalog/category')) {
                                $type = 'category';
                                $mainlink1 = strstr($mainlink1, 'id/');
                                $mainlink1 = substr($mainlink1, 3, 5);
                            } elseif (strpos($mainlink1, 'catalog/product')) {
                                $type = 'product';
                                $mainlink1 = strstr($mainlink1, 'id/');
                                $mainlink1 = substr($mainlink1, 3, 5);
                            } else {
                                $type = 'static';
                            }
                            $src = $mainimages1;
                            $mainTag1[] = array('image' => $src, 'type' => $type, 'id' => $mainlink1);
                        }
                    }
                }
                $main2 = Mage::getModel('cms/block')->load('welcome-shortcut-2')->getContent();
                $status2 = Mage::getModel('cms/block')->load('welcome-shortcut-2')->getIsActive();
                if ($status2) {
                    if ($main2 != '') {
                        $result2 = Mage::helper('cms')->getBlockTemplateProcessor()->filter($main2);
                        $doc = new DOMDocument();
                        $doc->loadHTML($result2);
                        $mainTag = $doc->getElementsByTagName('a');
                        $maintags = array();
                        foreach ($mainTag as $tag) {
                            $mainlink2 = $tag->getAttribute('href');
                            $mainImage = $tag->getElementsByTagName('img');
                            foreach ($mainImage as $img) {
                                $mainimages2 = $img->getAttribute('src');
                            }

                            if (strpos($mainlink2, 'catalog/category')) {
                                $type = 'category';
                                $mainlink2 = strstr($mainlink2, 'id/');
                                $mainlink2 = substr($mainlink2, 3, 5);
                            } elseif (strpos($mainlink2, 'catalog/product')) {
                                $type = 'product';
                                $mainlink2 = strstr($mainlink2, 'id/');
                                $mainlink2 = substr($mainlink2, 3, 5);
                            } else {
                                $type = 'static';
                            }
                            $src = $mainimages2;
                            $mainTag2[] = array('image' => $src, 'type' => $type, 'id' => $mainlink2);
                        }
                    }
                }
                $main3 = Mage::getModel('cms/block')->load('welcome-shortcut-3')->getContent();
                $status3 = Mage::getModel('cms/block')->load('welcome-shortcut-3')->getIsActive();
                if ($status3) {
                    if ($main3 != '') {
                        $result3 = Mage::helper('cms')->getBlockTemplateProcessor()->filter($main3);
                        $doc = new DOMDocument();
                        $doc->loadHTML($result3);
                        $mainTag = $doc->getElementsByTagName('a');
                        $maintags = array();
                        foreach ($mainTag as $tag) {
                            $mainlink3 = $tag->getAttribute('href');
                            $mainImage = $tag->getElementsByTagName('img');
                            foreach ($mainImage as $img) {
                                $mainimages3 = $img->getAttribute('src');
                            }
                            if (strpos($mainlink3, 'catalog/category')) {
                                $type = 'category';
                                $mainlink3 = strstr($mainlink3, 'id/');
                                $mainlink3 = substr($mainlink3, 3, 5);
                            } elseif (strpos($mainlink3, 'catalog/product')) {
                                $type = 'product';
                                $mainlink3 = strstr($mainlink3, 'id/');
                                $mainlink3 = substr($mainlink3, 3, 5);
                            } else {
                                $type = 'static';
                            }
                            $src = $mainimages3;
                            $mainTag3[] = array('image' => $src, 'type' => $type, 'id' => $mainlink3);
                        }
                    }
                }
                $main4 = Mage::getModel('cms/block')->load('welcome-shortcut-4')->getContent();
                $status4 = Mage::getModel('cms/block')->load('welcome-shortcut-4')->getIsActive();
                if ($status4) {
                    if ($main4 != '') {
                        $result4 = Mage::helper('cms')->getBlockTemplateProcessor()->filter($main4);
                        $doc = new DOMDocument();
                        $doc->loadHTML($result4);
                        $mainTag = $doc->getElementsByTagName('a');
                        $maintags = array();
                        foreach ($mainTag as $tag) {
                            $mainlink4 = $tag->getAttribute('href');
                            $mainImage = $tag->getElementsByTagName('img');
                            foreach ($mainImage as $img) {
                                $mainimages4 = $img->getAttribute('src');
                            }
                            if (strpos($mainlink4, 'catalog/category')) {
                                $type = 'category';
                                $mainlink4 = strstr($mainlink4, 'id/');
                                $mainlink4 = substr($mainlink4, 3, 5);
                            } elseif (strpos($mainlink4, 'catalog/product')) {
                                $type = 'product';
                                $mainlink4 = strstr($mainlink4, 'id/');
                                $mainlink4 = substr($mainlink4, 3, 5);
                            } else {
                                $type = 'static';
                            }
                            $src = $mainimages4;
                            $mainTag4[] = array('image' => $src, 'type' => $type, 'id' => $mainlink4);
                        }
                    }
                }

                $main5 = Mage::getModel('cms/block')->load('welcome-shortcut-5')->getContent();
                $status5 = Mage::getModel('cms/block')->load('welcome-shortcut-5')->getIsActive();
                if ($status5) {
                    if ($main5 != '') {
                        $result5 = Mage::helper('cms')->getBlockTemplateProcessor()->filter($main5);
                        $doc = new DOMDocument();
                        $doc->loadHTML($result5);
                        $mainTag = $doc->getElementsByTagName('a');
                        $maintags = array();
                        foreach ($mainTag as $tag) {
                            $mainlink5 = $tag->getAttribute('href');
                            $mainImage = $tag->getElementsByTagName('img');
                            foreach ($mainImage as $img) {
                                $mainimages5 = $img->getAttribute('src');
                            }
                            if (strpos($mainlink5, 'catalog/category')) {
                                $type = 'category';
                                $mainlink5 = strstr($mainlink5, 'id/');
                                $mainlink5 = substr($mainlink5, 3, 5);
                            } elseif (strpos($mainlink5, 'catalog/product')) {
                                $type = 'product';
                                $mainlink5 = strstr($mainlink5, 'id/');
                                $mainlink5 = substr($mainlink5, 3, 5);
                            } else {
                                $type = 'static';
                            }
                            $src = $mainimages5;
                            $mainTag5[] = array('image' => $src, 'type' => $type, 'id' => $mainlink5);
                        }
                    }
                }
                $status6 = Mage::getModel('cms/block')->load('welcome-shortcut-6')->getIsActive();
                if ($status6) {
                    $main6 = Mage::getModel('cms/block')->load('welcome-shortcut-6')->getContent();
                    if ($main6 != '') {
                        $result6 = Mage::helper('cms')->getBlockTemplateProcessor()->filter($main6);
                        $doc = new DOMDocument();
                        $doc->loadHTML($result6);
                        $mainTag = $doc->getElementsByTagName('a');
                        $maintags = array();
                        foreach ($mainTag as $tag) {
                            $mainlink6 = $tag->getAttribute('href');
                            $mainImage = $tag->getElementsByTagName('img');
                            foreach ($mainImage as $img) {
                                $mainimages6 = $img->getAttribute('src');
                            }
                            if (strpos($mainlink6, 'catalog/category')) {
                                $type = 'category';
                                $mainlink6 = strstr($mainlink6, 'id/');
                                $mainlink6 = substr($mainlink6, 3, 5);
                            } elseif (strpos($mainlink5, 'catalog/product')) {
                                $type = 'product';
                                $mainlink6 = strstr($mainlink6, 'id/');
                                $mainlink6 = substr($mainlink6, 3, 5);
                            } else {
                                $type = 'static';
                            }
                            $src = $mainimages6;
                            $mainTag6[] = array('image' => $src, 'type' => $type, 'id' => $mainlink6);
                        }
                    }
                }
                // -------------------- home catalog --------------------
                $category_id = '2656';
                $page = '1';
                $limit = '8';
                $order = 'entity_id';
                $dir = 'desc';
                $category = Mage::getModel('catalog/category')->load($category_id);
                $collection = $category->getProductCollection()->addAttributeToFilter('status', 1)->addAttributeToFilter('visibility', array('neq' => 1))->addAttributeToSort($order, $dir);
                Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($collection);
                $pages = $collection->setPageSize($limit)->getLastPageNumber();
                if ($page <= $pages) {
                    $collection->setPage($page, $limit);
                    $product_list = $this->getProductList($collection, 'catalog');
                } else {
                    $product_list = array();
                }
                // -------------------- home catalog --------------------

                $category_id = '4173';
                $page = '1';
                $limit = '8';
                $order = 'entity_id';
                $dir = 'desc';
                $category = Mage::getModel('catalog/category')->load($category_id);
                $collection = $category->getProductCollection()->addAttributeToFilter('status', 1)->addAttributeToFilter('visibility', array('neq' => 1))->addAttributeToSort($order, $dir);
                Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($collection);
                $pages = $collection->setPageSize($limit)->getLastPageNumber();
                if ($page <= $pages) {
                    $collection->setPage($page, $limit);
                    $product_list2 = $this->getProductList($collection, 'catalog');
                } else {
                    $product_list2 = array();
                }
                if ($main6 == "") {
                    $data = array($mainTag1, $mainTag2, $mainTag3, $mainTag4, $mainTag5);
                } else {
                    $data = array($mainTag1, $mainTag2, $mainTag3, $mainTag4, $mainTag5, $mainTag6);
                }
                /*echo json_encode ( array('code'=>0, 'msg'=>null, 'slider'=> $slider,'main1'=>$mainTag1,'main2'=>$mainTag2, 'main3'=>$mainTag3 ,
                'main4'=>$mainTag4,'main5'=>$mainTag5,'main6'=>$mainTag6,'orami pick'=>$product_list , 'hot product'=>$product_list2  ));*/
                echo json_encode(array('code' => 0, 'msg' => null, 'slider' => $slider, 'data' => $data, 'orami pick' => $product_list, 'hot product' => $product_list2));

                break;


            default :
                echo json_encode(array('code' => 1, 'msg' => 'Your request was wrong.', 'model' => array()));
                // echo $currency_code = Mage::app()->getStore()->getCurrentCurrencyCode();
                // echo Mage::app()->getLocale()->currency(Mage::app()->getStore()->getCurrentCurrencyCode())->getSymbol();
                break;
        }
    }

    /**
     * Get navigation data action
     */
    public function navigationAction()
    {
        /** @var Petloft_Catalog_Model_Navigation_Renderer $renderer */
        $renderer = Mage::getModel('petloftcatalog/navigation_renderer');
        $data = array();
        $thaiCode = "moxyth";
        $enCode = "moxyen";
        $data['thaiNavigationDesktop'] = $renderer->toHtml($thaiCode . '/' . Petloft_Catalog_Model_Navigation_Renderer::DESKTOP_FILENAME);
        $data['thaiNavigationMobile'] = $renderer->toHtml($thaiCode . '/' . Petloft_Catalog_Model_Navigation_Renderer::MOBILE_FILENAME);
        $data['enNavigationDesktop'] = $renderer->toHtml($enCode . '/' . Petloft_Catalog_Model_Navigation_Renderer::DESKTOP_FILENAME);
        $data['enNavigationMobile'] = $renderer->toHtml($enCode . '/' . Petloft_Catalog_Model_Navigation_Renderer::MOBILE_FILENAME);
        if (empty($data)) {
            echo json_encode(array('code' => 1, 'msg' => 'Fail to get navigation data', 'model' => array()));
        } else {
            echo json_encode(array('code' => 0, 'msg' => 'Get navigation bar cache successfully.', 'model' => $data));
        }
    }

    /**
     * @param $products
     * @param string $mod
     * @return array
     *
     *
     */
    public function getProductList($products, $mod = 'product')
    {
        $baseCurrency = Mage::app()->getStore()->getBaseCurrency()->getCode();
        $currentCurrency = Mage::app()->getStore()->getCurrentCurrencyCode();
        $store_id = Mage::app()->getStore()->getId();
        $product_list = array();
        $main = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB);
        foreach ($products as $product) {
            if ($mod == 'catalog') {
                $product = Mage::getModel('catalog/product')->load($product ['entity_id']);
            }
            $summaryData = Mage::getModel('review/review_summary')->setStoreId($store_id)->load($product->getId());
            $price = ($product->getSpecialPrice()) == null ? ($product->getPrice()) : ($product->getSpecialPrice());
            $regular_price_with_tax = number_format(Mage::helper('directory')->currencyConvert($product->getPrice(), $baseCurrency, $currentCurrency), 2, '.', '');
            $final_price_with_tax = number_format(Mage::helper('directory')->currencyConvert($product->getSpecialPrice(), $baseCurrency, $currentCurrency), 2, '.', '');
            $temp_product = array(
                'entity_id' => $product->getId(),
                'sku' => $product->getSku(),
                'name' => $product->getName(),
                'rating_summary' => $summaryData->getRatingSummary(),
                'reviews_count' => $summaryData->getReviewsCount(),
                'news_from_date' => $product->getNewsFromDate(),
                'news_to_date' => $product->getNewsToDate(),
                'special_from_date' => $product->getSpecialFromDate(),
                'special_to_date' => $product->getSpecialToDate(),
                'image_url' => $product->getImageUrl(),
                'url_key' => $product->getProductUrl(),
                'price' => number_format(Mage::getModel('mobile/currency')->getCurrencyPrice($regular_price_with_tax), 2, '.', ''),
                //'regular_price_with_tax' =>  number_format(Mage::getModel('mobile/currency')->getCurrencyPrice($regular_price_with_tax),2,'.',''),
                //'special_price' =>  number_format(Mage::getModel('mobile/currency')->getCurrencyPrice($final_price_with_tax),2,'.',''),
                'msrp' => $product->getMsrp(),
                'symbol' => Mage::app()->getLocale()->currency(Mage::app()->getStore()->getCurrentCurrencyCode())->getSymbol(),
                'stock_level' => (int)Mage::getModel('cataloginventory/stock_item')->loadByProduct($product)->getQty()
            );
            array_push($product_list, $temp_product);
        }
        return $product_list;
    }
}
