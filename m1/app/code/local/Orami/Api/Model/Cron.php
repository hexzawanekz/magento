<?php
set_time_limit(0);
class Orami_Api_Model_Cron {
    public function runCronMenu() {
            Mage::app()->setCurrentStore('17');
            $_helper = Mage::helper ( 'catalog/category' );
            $_categories = $_helper->getStoreCategories ();
            $_categorylist = array ();
            $key = "menu";
            if (count ( $_categories ) > 0) {
                    foreach ( $_categories as $_category ) {
                            if(Mage::getModel('mobile/menu')->_hasProducts($_category->getId())) {
                                    $_helper->getCategoryUrl($_category);
                                    $childMenu = Mage::getModel('catalog/category')->load($_category->getId())->getAllChildren();
                                    $childMenu = explode(',', $childMenu);
                                    array_shift($childMenu);
                                    $child = array();
                                    $main_id = $_category->getId();
                                foreach ($childMenu as $childSec) {
                                    if (Mage::getModel('mobile/menu')->_hasProducts($childSec)) {
                                            if((Mage::getModel('catalog/category')->load($childSec)->getLevel()) == '3') {
                                                $id = $childSec;
                                                $child[$childSec] = array('id' => $id,
                                                'main_cate_name' => Mage::getModel('catalog/category')->load($childSec)->getName(),
                                                'is_active' => Mage::getModel('catalog/category')->load($childSec)->getIsActive(),
                                                //'level' => Mage::getModel('catalog/category')->load($childSec)->getLevel(),
                                                'include_in_menu' => Mage::getModel('catalog/category')->load($childSec)->getIncludeInMenu()
                                                );
                                            }
                                            if((Mage::getModel('catalog/category')->load($childSec)->getLevel()) == '4'){
                                                $child[$id]['child'][] = array(
                                                'id' => Mage::getModel('catalog/category')->load($childSec)->getId(),
                                                'name' => Mage::getModel('catalog/category')->load($childSec)->getName(),
                                                'is_active' => Mage::getModel('catalog/category')->load($childSec)->getIsActive(),
                                                'level ' => Mage::getModel('catalog/category')->load($childSec)->getLevel(),
                                                'include_in_menu' => Mage::getModel('catalog/category')->load($childSec)->getIncludeInMenu(),
                                                'parent_id' => Mage::getModel('catalog/category')->load($childSec)->getParentId()
                                                );
                                            }
                                    }
                                }
                                //$child[] = (object)$child;
                                $child = array_values($child);
                                $_categorylist [] = array(
                                        'category_id' => $_category->getId(),
                                        'name' => $_category->getName(),
                                        'is_active' => $_category->getIsActive(),
                                        'include_in_menu' => $_category->getIncludeInMenu(),
                                        'child' => $child
                                );
                            }
                    }
                    $data = $_categorylist;
                    Mage::app()->getCache()->save(serialize($data), $key);
        }
    }
}
