<?php

$installer = $this;
$installer->startSetup(); // start setup
$transTable = $installer->getTable('one23cash/one23_order_transaction');
$conn = $installer->getConnection(); // connection object
if(!$conn->tableColumnExists($transTable,'sent_sms')) {
	$conn->addColumn($transTable, 'sent_sms', array(
			'TYPE'      => Varien_Db_Ddl_Table::TYPE_INTEGER,
			'LENGTH'    => 2,
			'NULLABLE'  => false,
			//'DEFAULT'  => 0,
			'COMMENT'   => 'Send SMS Status'
		));
}
if(!$conn->tableColumnExists($transTable,'notify_sms')) {
	$conn->addColumn($transTable, 'notify_sms', array(
			'TYPE'      => Varien_Db_Ddl_Table::TYPE_INTEGER,
			'LENGTH'    => 2,
			'NULLABLE'  => false,
			//'DEFAULT'  => 0,
			'COMMENT'   => 'Notify SMS Status'
		));
}
$installer->endSetup();
