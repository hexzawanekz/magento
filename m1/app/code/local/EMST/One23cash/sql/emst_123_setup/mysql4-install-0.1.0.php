<?php
/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade TableRate to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_One23Cash
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
*/

$installer = $this;

$installer->startSetup();
	$conn = $installer->getConnection();
	$tranTbl = $installer->getTable('one23cash/one23_order_transaction');
	if(!$conn->isTableExists($tranTbl)) {
		$tranTbl = $conn->newTable($tranTbl)
			->addColumn('order_increment',Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
				'unsigned'	=> true,
				'nullable'	=> false,
				'primary'	=> true,
			),'Order Increment ID')
			->addColumn('transaction_id',Varien_Db_Ddl_Table::TYPE_TEXT, 20, array(
				'nullable'  => true,
				'default'   => null,
			),'Transaction ID')
			->addColumn('payer_email',Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
				'nullable'  => true,
				'default'   => null,
			),'Payer Email')
			->addColumn('paid_amount',Varien_Db_Ddl_Table::TYPE_DECIMAL, '12,4', array(
				'nullable'  => true,
				'default'   => null,
			),'Paid Amount')
			->addColumn('agent_code',Varien_Db_Ddl_Table::TYPE_TEXT, 30, array(
				'nullable'  => true,
				'default'   => null,
			),'Agent Code')
			->addColumn('channel_code',Varien_Db_Ddl_Table::TYPE_TEXT, 30, array(
				'nullable'  => true,
				'default'   => null,
			),'Channel Code')
			->addColumn('created_at',Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
				'nullable'  => false,
				'default'   => '0000-00-00 00:00:00',
			),'Channel Code')			
			->addColumn('status',Varien_Db_Ddl_Table::TYPE_TEXT, 3, array(
				'nullable'  => true,
				'default'   => null,
			),'Transaction Response Code')
			->addColumn('additional_info',Varien_Db_Ddl_Table::TYPE_BLOB, null, array(
				'nullable'  => true,
				'default'   => null,
			),'Other Information')						
			->setComment('123Cash Gateway Payment Transaction Table');
		$conn->createTable($tranTbl);
	}
$installer->endSetup();