<?php
/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade SalesExport to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_Sales
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
*/

class EMST_Sales_Adminhtml_ChannelController extends Mage_Adminhtml_Controller_Action
{
	protected function _initAction() {
		$this->loadLayout()
			->_setActiveMenu('sales/channel')
			->_addBreadcrumb(Mage::helper('adminhtml')->__('Channels Manager'), Mage::helper('adminhtml')->__('Channels Manager'));
		
		return $this;
	}   
 
	public function indexAction() {
		$this->_initAction()
			->_addContent($this->getLayout()->createBlock('emst_sales/adminhtml_channel','emst_sales_order_channel'))
			->renderLayout();
	}

	public function gridAction() {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('netsuiteexport/adminhtml_channel_grid')->toHtml()
        );
    }
	
	public function editAction() {
		$id     = $this->getRequest()->getParam('id');
		$model  = Mage::getModel('emst_sales/channel')->load($id);

		if ($model->getId() || $id == 0) {
			$data = $this->_getSession()->getFormData(true);
			if (!empty($data)) {
				$model->setData($data);
			}

			Mage::register('emst_sales_channel_data', $model);

			$this->loadLayout();
			$this->_setActiveMenu('sales/channel');

			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Channel Manager'), Mage::helper('adminhtml')->__('Channel Manager'));
			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Channel News'), Mage::helper('adminhtml')->__('Channel News'));

			$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

			$this->_addContent($this->getLayout()->createBlock('emst_sales/adminhtml_channel_edit'))
				->_addLeft($this->getLayout()->createBlock('emst_sales/adminhtml_channel_edit_tabs'));

			$this->renderLayout();
		} else {
			$this->_getSession()->addError(Mage::helper('emst_sales')->__('Channel does not exist'));
			$this->_redirect('*/*/');
		}
	}
 
	public function newAction() {
		$this->_forward('edit');
	}
 
	public function saveAction() {
		if ($data = $this->getRequest()->getPost()) {
			$model = Mage::getModel('emst_sales/channel');		
			$model->setData($data)
				->setId($this->getRequest()->getParam('id'));
			
			try {				
				$model->save();
				$this->_getSession()->addSuccess(Mage::helper('emst_sales')->__('Channel was successfully saved'));
				$this->_getSession()->setFormData(false);

				if ($this->getRequest()->getParam('back')) {
					$this->_redirect('*/*/edit', array('id' => $model->getId()));
					return;
				}
				$this->_redirect('*/*/');
				return;
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
                $this->_getSession()->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        $this->_getSession()->addError(Mage::helper('emst_sales')->__('Unable to find Channel to save'));
        $this->_redirect('*/*/');
	}
 
	public function deleteAction() {
		if( $this->getRequest()->getParam('id') > 0 ) {
			try {
				$model = Mage::getModel('emst_sales/channel');
				 
				$model->setId($this->getRequest()->getParam('id'))
					->delete();
					 
				$this->_getSession()->addSuccess(Mage::helper('adminhtml')->__('Channel was successfully deleted'));
				$this->_redirect('*/*/');
			} catch (Exception $e) {
				$this->_getSession()->addError($e->getMessage());
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
			}
		}
		$this->_redirect('*/*/');
	}

    public function massDeleteAction() {
        $channelIds = $this->getRequest()->getParam('emst_sales_channel');
        if(!is_array($channelIds)) {
			$this->_getSession()->addError(Mage::helper('adminhtml')->__('Please select Channel(s)'));
        } else {
            try {
                foreach ($channelIds as $channelId) {
                    $channel = Mage::getModel('emst_sales/channel')->load($channelId);
                    $channel->delete();
                }
                $this->_getSession()->addSuccess(
                    Mage::helper('adminhtml')->__(
                        'Total of %d record(s) were successfully deleted', count($channelIds)
                    )
                );
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }
	
    public function massTypesAction() {		
        $channelIds = $this->getRequest()->getParam('emst_sales_channel');
        if(!is_array($channelIds)) {
            $this->_getSession()->addError($this->__('Please select Channel(s)'));
        } else {
            try {
                foreach ($channelIds as $channelId) {
                    $channel = Mage::getSingleton('emst_sales/channel')
                        ->load($channelId)
                        ->setType($this->getRequest()->getParam('type'))                        
                        ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d record(s) were successfully updated', count($channelIds))
                );
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }
  
    public function exportCsvAction()
    {
        $fileName   = 'emst_sales.csv';
        $content    = $this->getLayout()->createBlock('emst_sales/adminhtml_channel_grid')
            ->getCsv();

        $this->_sendUploadResponse($fileName, $content);
    }

    public function exportXmlAction()
    {
        $fileName   = 'emst_sales.xml';
        $content    = $this->getLayout()->createBlock('emst_sales/adminhtml_channel_grid')
            ->getXml();

        $this->_sendUploadResponse($fileName, $content);
    }

    protected function _sendUploadResponse($fileName, $content, $contentType='application/octet-stream')
    {
        $response = $this->getResponse();
        $response->setHeader('HTTP/1.1 200 OK','');
        $response->setHeader('Pragma', 'public', true);
        $response->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true);
        $response->setHeader('Content-Disposition', 'attachment; filename='.$fileName);
        $response->setHeader('Last-Modified', date('r'));
        $response->setHeader('Accept-Ranges', 'bytes');
        $response->setHeader('Content-Length', strlen($content));
        $response->setHeader('Content-type', $contentType);
        $response->setBody($content);
        $response->sendResponse();
        die;
    }
}