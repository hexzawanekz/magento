<?php
/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade SalesExport to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_SalesExport
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
*/

class EMST_Sales_Adminhtml_ImportController extends Mage_Adminhtml_Controller_Action
{
	protected $_helper = '';
	public function invoiceAction() {
		$content = $this->_upload('invoice');
		$invalid = array();
		$failed = array();
		if($content!==false) {
			$count = 0;
			foreach($content as $row) {
				$order = Mage::getModel('sales/order')->loadByIncrementId($row[0]);
				if($order->getId()) {
					if($order->canInvoice()) {
						$invoice = Mage::getModel('sales/service_order', $order)->prepareInvoice();
						$invoice->register();
						$invoice->setEmailSent(true);
						$invoice->getOrder()->setCustomerNoteNotify(true);
						$invoice->getOrder()->setIsInProcess(true);
						$invoice->getOrder()->addStatusHistoryComment($this->getHelper()->__('Updated(Payment method: %s, at:%s)',$row[1],$row[2]));
						$transactionSave = Mage::getModel('core/resource_transaction')
							->addObject($invoice)
							->addObject($invoice->getOrder())->save();
						$invoice->sendEmail(true);		
						$count++;
					}
					else {
						$failed[] = $row[0];
					}
				}
				else {
					$invalid[] = $row[0];
				}				
			}
			if(!empty($invalid)) {
				$this->_getSession()->addError($this->__('Invalid order numbers: ').implode(',',$invalid));
			}
			if(!empty($failed)) {
				$this->_getSession()->addError($this->__('Failed to invoice: ').implode(',',$invalid));
			}
			if($count>0) {
				$this->_getSession()->addSuccess($this->__('%s orders successfully updated(invoiced)',$count));
			}
		}
		$this->_redirect("adminhtml/sales_order/index");
	}
	public function shippingAction() {
		$content = $this->_upload('shipping');		
		$invalid = array();
		if($content!==false) {
			$count = 0;
			foreach($content as $row) {
				$order = Mage::getModel('sales/order')->loadByIncrementId($row[0]);
				if($order->getId() && $order->canShip()) {
					$shipment = $order->prepareShipment()->register();	
					$shipment->getOrder()->setIsInProcess(true);
					$shipment->getOrder()->setActionFlag(Mage_Sales_Model_Order::ACTION_FLAG_SHIP, true);                    
					Mage::getModel('core/resource_transaction')
							->addObject($shipment->getOrder())
							->addObject($shipment)->save();
					if($shipment) {
						$shipment->sendEmail(true, '');
						$track = Mage::getModel('sales/order_shipment_track')
							->setNumber($row[1])
							->setCarrierCode('custom')
							->setTitle($row[2]);
						$shipment->addTrack($track)
						->save();
					}
					$count++;
				}
				else {
					$invalid[] = $row[0];
				}
			}
			if(!empty($invalid)) {
				$this->_getSession()->addError($this->__('Invalid order numbers: ').implode(',',$invalid));
			}
			if($count>0) {
				$this->_getSession()->addSuccess($this->__('%s orders successfully udpated(shipped)',$count));
			}
		}		
		$this->_redirect("adminhtml/sales_order/index");
	}
	protected function _upload($type)
    {
        try {
            $path = $this->getHelper()->uploadCSV($type);
            if ($path !== false) {
                $content = $this->getHelper()->readCSV($path);                
				if($content !== false) {
					if(isset($content['invalid']) && 
					is_array($content['invalid']) && 
					sizeof($content['invalid'])) {
						$this->_getSession()->addError($this->__('Please correct the CSV file at: ').implode(',',$content['invalid']));
					}
					elseif(isset($content['valid']) && sizeof($content['valid'])) {
						return $content['valid'];
					}
				}
				else {
					$this->_getSession()->addError($this->__('No uploaded file was found'));
				}				
            }
            else {
                $this->_getSession()->addError($this->__('Unable find item to save'));
            }
			return false;
        } catch (Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        }        
    }

    protected function getHelper()
    {
        if (!$this->_helper) $this->_helper = Mage::helper('emst_sales');
        return $this->_helper;
    }
}