<?php
/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade SalesExport to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_Sales
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
*/

class EMST_Sales_Block_Adminhtml_Channel_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'emst_sales';
        $this->_controller = 'adminhtml_channel';
        
        $this->_updateButton('save', 'label', Mage::helper('emst_sales')->__('Save Channel'));
        $this->_updateButton('delete', 'label', Mage::helper('emst_sales')->__('Delete Channel'));
		
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('emst_sales_channel_comment') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'emst_sales_channel_comment');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'emst_sales_channel_comment');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    public function getHeaderText()
    {
        if( Mage::registry('emst_sales_channel_data') && Mage::registry('emst_sales_channel_data')->getId() ) {
            return Mage::helper('emst_sales')->__("Edit Channel '%s'", $this->htmlEscape(Mage::registry('emst_sales_channel_data')->getTitle()));
        } else {
            return Mage::helper('emst_sales')->__('Add Channel');
        }
    }
}