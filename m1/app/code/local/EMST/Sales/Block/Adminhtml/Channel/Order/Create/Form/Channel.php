<?php
/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade SalesExport to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_Sales
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
*/

class EMST_Sales_Block_Adminhtml_Channel_Order_Create_Form_Channel extends Mage_Adminhtml_Block_Sales_Order_Create_Form_Abstract
{
    
    /**
     * Return header text
     *
     * @return string
     */
    public function getHeaderText()
    {
        return Mage::helper('emst_sales')->__('Channel Information');
    }
    /**
     * Prepare Form and add elements to form
     *
     * @return Mage_Adminhtml_Block_Sales_Order_Create_Form_Account
     */
    protected function _prepareForm()
    {
		
        $fieldset = $this->_form->addFieldset('main',array());
		$helper = Mage::helper('emst_sales');
		foreach($helper->getChannelFields() as $field=>$label) {
			$fieldset->addField($field, 'select', array(
                    'name'      => $field,
                    'label'     => $this->__($label),
                    'values'    => $helper->getChannelOptionByType($field),
                    'required'  => false,
                ));
		}
		$this->_form->addFieldNameSuffix('order[channel]');
        return $this;
    }

}
