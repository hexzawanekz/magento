<?php
/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade SalesExport to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_Sales
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
*/

class EMST_Sales_Block_Adminhtml_Channel_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('emst_sales_form', array('legend'=>Mage::helper('emst_sales')->__('Channel information')));
	  $types = Mage::getModel('emst_sales/source_types')->toOptionArray();	  
      $fieldset->addField('name', 'text', array(
          'label'     => Mage::helper('emst_sales')->__('Name'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'name',
      ));
      $fieldset->addField('type', 'select', array(
          'label'     => Mage::helper('emst_sales')->__('Type'),
          'name'      => 'type',
		  'required'  => true,
          'values'    => $types,
      ));
     
      $fieldset->addField('comment', 'editor', array(
          'name'      => 'comment',
          'label'     => Mage::helper('emst_sales')->__('Comment'),
          'title'     => Mage::helper('emst_sales')->__('Comment'),
          'style'     => 'width:500px; height:200px;',
          'wysiwyg'   => false,
          'required'  => false,
      ));
     
      if ( Mage::getSingleton('adminhtml/session')->getEmstSalesChannelData() ) {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getEmstSalesChannelData());
          Mage::getSingleton('adminhtml/session')->setEmstSalesChannelData(null);
      } elseif ( Mage::registry('emst_sales_channel_data') ) {
          $form->setValues(Mage::registry('emst_sales_channel_data')->getData());
      }
      return parent::_prepareForm();
  }
}