<?php

$installer = $this;
$installer->startSetup();
$conn = $installer->getConnection();
$channelTbl = $installer->getTable('emst_sales/orders_channels');
$salesTable = $installer->getTable('sales/order');

if(!$conn->isTableExists($channelTbl)) {
	$table = $conn
			->newTable($channelTbl)
			->addColumn('channel_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
				'identity'  => true,
				'unsigned'  => true,
				'nullable'  => false,
				'primary'   => true,
				), 'Entity Id')
			->addColumn('name', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
				'nullable'  => false,
				), 'Channel Name')
			->addColumn('type', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
				'nullable'  => false,
				), 'Channel Type')
			->addColumn('comment', Varien_Db_Ddl_Table::TYPE_TEXT, '64k', array(
				'nullable'  => false,
				), 'Channel Comment')
			/* ->addColumn('store_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
				'nullable'  => false,
				), 'Channel Type') */
			->setComment('Sales Channel');
	$conn->createTable($table);
}

$channelColumns = array(
	'main_channel'=>'Main Channel',
	'sub_channel'=>'Sub Channel',
	'channel_source'=>'Channel Source',
);
foreach($channelColumns as $col=>$lb) {
	$conn->addColumn($salesTable, $col, array(
        'TYPE'      => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'LENGTH'    => 11,
        'NULLABLE'  => true,
        'COMMENT'   => $lb
    ));
}
$installer->endSetup();
