<?php
/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Sales to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_Sales
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
*/

class EMST_Sales_Model_Cronjobs
{	protected $_helper = '';

	/**
	 * @return EMST_Sales_Helper_Data
	 */
	protected function _getHelper() {
		if(!$this->_helper) {
			$this->_helper = Mage::helper('emst_sales');
		}
		return $this->_helper;
	}
	
	public function autoCancelOrders() {
		try{
			if(!$this->_getHelper()->isCronActive()) {
				return ;
			}
			$config = $this->_getHelper()->getConfig();

			/** @var EMST_Sales_Model_Resource_Order_Grid_Collection $collection */
			$collection = Mage::getResourceModel('emst_sales/order_grid_collection')
				->addPaymentToSelect()
				->addTimeFilter($config['instant_payment'],$config['cod_payment'])
				->addPendingOrders();
				foreach($collection as $order) {
					/** @var Mage_Sales_Model_Order $order */
					if($order->canCancel()) {
						$order->cancel()->save();
					}
				}
		}
		catch(Exception $e) {
			Mage::log($e->getMessage(),null,'CancelOrders.log');
		}
	}
	public function checkInstantOrders() {
		try{
			if(!$this->_getHelper()->isInstantCronActive()) {
				return ;
			}
			$time = (int)Mage::getStoreConfig('emst_sales/instantconfig/time');
			$methods = explode(',',Mage::getStoreConfig('emst_sales/instantconfig/apply_for'));
			if($time>0) {
				/** @var EMST_Sales_Model_Resource_Order_Grid_Collection $collection */
				$collection = Mage::getResourceModel('emst_sales/order_grid_collection')->setInstantMethods($methods)
					->addPaymentToSelect()					
					->addTimeFilter($time)
					->addPendingOrders();				
				foreach($collection as $order) {
					/** @var Mage_Sales_Model_Order $order */

					$sequence = Mage::getModel('recurringandrentalpayments/sequence')
						->getCollection()
						->addFieldToFilter('order_id',array('eq' => $order->getId()))->getFirstItem();
					
					if($sequence->getId()){
						continue;
					}

					// avoid transaction paid via 123 services and use method 'normal2c2p'
					if(Mage::helper('core')->isModuleEnabled('EMST_Normal2c2p')
						&& $order->getData('payment_method') == 'normal2c2p'){

						$pay_info = $order->getPayment()->getAdditionalInformation();

						if(isset($pay_info[EMST_Normal2c2p_Model_Normal2c2p::PAY_CHANNEL])
							&& $pay_info[EMST_Normal2c2p_Model_Normal2c2p::PAY_CHANNEL] == EMST_Normal2c2p_Model_PaymentChannel::CASH_PAYMENT_CHANNEL){
							// ignore this order
							continue;
						}
					}

					// cancel order and set it as invalid
					if($order->canCancel()) {
						$order->cancel();
					}
					$order
					->setState(EMST_Sales_Model_Order::STATE_INVALID)
					->setStatus(EMST_Sales_Model_Order::STATE_INVALID)					
					->save();

					if(Mage::helper('core')->isModuleEnabled('SM_NetsuiteInventorySync')){
						// return stock back to product in order
						Mage::getSingleton('netsuiteinventorysync/stockBack')->returnStockBackToProduct($order);
						// update stock for related products
						Mage::getSingleton('netsuiteinventorysync/stockBack')->updateStock($order, false);
					}
				}
			}
		}
		catch(Exception $e) {
			Mage::log($e->getMessage(),null,'changeStatus.log');
		}
	}
} 