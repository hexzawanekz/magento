<?php
/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Sales to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_Sales
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
*/

class EMST_Sales_Model_Observer
{
	protected $_actionTypes = array('shipping','invoice');
	
	public function addChannelData($observer) {
		$request = $observer->getEvent()->getRequestModel();
		if($request->getRequestedActionName() == 'save') {			
			$data = $request->getPost('order');
			if(isset($data['channel'])) {				
				$observer->getEvent()->getSession()->getQuote()->addData($data['channel']);
			}
		}
	}
	
	public function addImportAction($observer){
		if($observer->getEvent()->getBlock() instanceof Mage_Adminhtml_Block_Sales_Order_Grid) {
			$this->addActions($observer,array());
        }
	}
	protected function getActions() {
		return $this->_actionTypes;
	}
	public function addActions($observer,$param){
		if(Mage::app()->getStore()->isCurrentlySecure()) {
			$param= array_merge((array)$param,array('_secure'=>1));
		}
		$grid = $observer->getEvent()->getBlock();
		$massaction = $grid->getMassactionBlock();
		$setFormType = '<script type="text/javascript">$("'.$massaction->getHtmlId().'-form").enctype="multipart/form-data"</script>';
		foreach($this->getActions() as $type) {
			$massaction->addItem('sales_'.$type.'_import_action', array(
				 'label'=> Mage::helper('emst_sales')->__(ucfirst($type).' Import'),
				 'url'  => Mage::getUrl('*/import/'.$type, array('_current'=>true)),
				 'additional' => array(
						'sales_'.$type.'_import_action' => array(
							 'name' => $type,
							 'type' => 'file',
							 'class' => 'required-entry',
							 'label' => Mage::helper('emst_sales')->__(ucfirst($type).' CSV'),
							 'after_element_html' => $setFormType
						 )
				 )
			));
		}
		$this->extendJs($grid,$massaction);
	}
	public function extendJs($grid,$massaction) {
		$js = $grid->getAdditionalJavaScript();
		$actions = array();
		foreach($this->getActions() as $type) {
			$actions[] = 'sales_'.$type.'_import_action';
		}
		$actions = implode(',',$actions);
		$js .= "
			Object.extend({$massaction->getJsObjectName()}, {
				apply: function() {
					var actions = '{$actions}';
					actions = actions.split(',');
					var item = this.getSelectedItem();
					if(!item) {
						this.validator.validate();
						return;
					}
					var me = this;
					actions.each(function(e){
						if(item.id == e) {
							if(!\$(e).value) {
								alert('Please select a CSV file.');
								return;
							}
							me.form.action = item.url;
							me.form.submit();
							return ;
						}
					});
					return this.applyOrigin();
				},
				applyOrigin: function() {
					if(varienStringArray.count(this.checkedString) == 0) {
							alert(this.errorText);
							return;
						}

					var item = this.getSelectedItem();
					if(!item) {
						this.validator.validate();
						return;
					}
					this.currentItem = item;
					var fieldName = (item.field ? item.field : this.formFieldName);
					var fieldsHtml = '';

					if(this.currentItem.confirm && !window.confirm(this.currentItem.confirm)) {
						return;
					}

					this.formHiddens.update('');
					new Insertion.Bottom(this.formHiddens, this.fieldTemplate.evaluate({name: fieldName, value: this.checkedString}));
					new Insertion.Bottom(this.formHiddens, this.fieldTemplate.evaluate({name: 'massaction_prepare_key', value: fieldName}));

					if(!this.validator.validate()) {
						return;
					}

					if(this.useAjax && item.url) {
						new Ajax.Request(item.url, {
							'method': 'post',
							'parameters': this.form.serialize(true),
							'onComplete': this.onMassactionComplete.bind(this)
						});
					} else if(item.url) {
						this.form.action = item.url;
						this.form.submit();
					}
				}
			});
		";
		$grid->setAdditionalJavaScript($js);
	}
	
	public function invalidOrderMail(Varien_Event_Observer $observer)
	{
		try{
			/** @var Mage_Sales_Model_Order $order */
			$order = $observer->getEvent()->getOrder();
			$status = $order->getStatus();

			// check config
			if(Mage::getStoreConfig(EMST_Sales_Model_Order::XML_PATH_INVALID_EMAIL_ENABLED) &&
				Mage::getStoreConfig(EMST_Sales_Model_Order::XML_PATH_INVALID_EMAIL_SENDER_NAME) &&
				Mage::getStoreConfig(EMST_Sales_Model_Order::XML_PATH_INVALID_EMAIL_SENDER_EMAIL)) {

				// check order status
				if ($status == EMST_Sales_Model_Order::STATE_INVALID) {
					// store
					$store = $order->getStore();
					$storeId = $order->getStoreId();

					// email info
					$emailTemplateId = Mage::getStoreConfig(EMST_Sales_Model_Order::XML_PATH_INVALID_EMAIL_TEMPLATE);
					/** @var Mage_Core_Model_Email_Template $mailTemplateModel */
					$mailTemplateModel = Mage::getModel('core/email_template')->load($emailTemplateId);
					$customerEmail = $order->getCustomerEmail();
					$customer = Mage::getModel('customer/customer')->load($order->getCustomerID());

					$sender = array(
						'name' 	=> Mage::getStoreConfig(EMST_Sales_Model_Order::XML_PATH_INVALID_EMAIL_SENDER_NAME),
						'email' => Mage::getStoreConfig(EMST_Sales_Model_Order::XML_PATH_INVALID_EMAIL_SENDER_EMAIL),
					);

					/** @var Mage_Core_Model_Translate $translate */
					$translate = Mage::getSingleton('core/translate');

					$translate->setTranslateInline(false);

					$mailTemplateModel->setDesignConfig(array('area' => 'frontend', 'store' => $storeId))
						->sendTransactional(
							$mailTemplateModel,
							$sender,
							$customerEmail,
							'',
							array(
								'store' => $store,
								'order' => $order,
								'customer' => $customer,
							));
					$translate->setTranslateInline(true);
				}
			}
		}catch(Exception $e){
			Mage::log($e, null, 'emst_invalid_email.log');
		}
	}
}