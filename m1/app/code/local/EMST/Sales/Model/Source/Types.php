<?php
/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Sales to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_Sales
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
*/

class EMST_Sales_Model_Source_Types extends Varien_Object
{
	const MAIN_CHANNEL             	= '1';
	const MAIN_CHANNEL_FIELD        = 'main_channel';
	const MAIN_CHANNEL_LABEL        = 'Main Channel';
	
	const SUB_CHANNEL             	= '2';
	const SUB_CHANNEL_FIELD         = 'sub_channel';
	const SUB_CHANNEL_LABEL         = 'Sub Channel';
	
	const CHANNEL_SOURCE            = '3';
	const CHANNEL_SOURCE_FIELD      = 'channel_source';
	const CHANNEL_SOURCE_LABEL      = 'Channel Source';
	
	public function toOptionArray() {        
        return array(
			array('value'=>self::MAIN_CHANNEL,'label'=>self::MAIN_CHANNEL_LABEL),
			array('value'=>self::SUB_CHANNEL,'label'=>self::SUB_CHANNEL_LABEL),
			array('value'=>self::CHANNEL_SOURCE,'label'=>self::CHANNEL_SOURCE_LABEL),
		);
    }
	static public function getOptionArray() {
        return array(
            self::MAIN_CHANNEL    => Mage::helper('emst_sales')->__(self::MAIN_CHANNEL_LABEL),
            self::SUB_CHANNEL   => Mage::helper('emst_sales')->__(self::SUB_CHANNEL_LABEL),
            self::CHANNEL_SOURCE   => Mage::helper('emst_sales')->__(self::CHANNEL_SOURCE_LABEL),
        );
    }
	public function getTypeByFieldName($field) {
		switch($field) {
			case self::MAIN_CHANNEL_FIELD:
				return self::MAIN_CHANNEL;
				break;
			case self::SUB_CHANNEL_FIELD:
				return self::SUB_CHANNEL;
				break;
			case self::CHANNEL_SOURCE_FIELD:
				return self::CHANNEL_SOURCE;
				break;
		}
		return $field;
	}
	public function getChannelFields() {
		return array(
			self::MAIN_CHANNEL_FIELD	=> self::MAIN_CHANNEL_LABEL,
			self::SUB_CHANNEL_FIELD		=> self::SUB_CHANNEL_LABEL,
			self::CHANNEL_SOURCE_FIELD	=> self::CHANNEL_SOURCE_LABEL,
		);
	}
}