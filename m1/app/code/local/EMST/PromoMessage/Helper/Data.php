<?php
/**
 * Developed by Acommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.Acommerce.asia/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PromoMessage to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_PromoMessage
 * @copyright   Copyright (c) 2012 Acommerce (http://www.Acommerce.asia)
 * @license     http://www.Acommerce.asia/LICENSE-E.txt
*/

class EMST_PromoMessage_Helper_Data extends Mage_Core_Helper_Abstract
{	
	public function addMessage($msg) {		
		$this->_msgStorage = Mage::getSingleton('checkout/session');		
		if(!$this->_msgStorage->getCouponMessage()) {
			$this->_msgStorage->setCouponMessage($msg);
		}
		return $this;
	}
}