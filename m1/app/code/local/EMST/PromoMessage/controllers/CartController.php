<?php
require_once Mage::getModuleDir('controllers', 'Mage_Checkout') . DS . 'CartController.php';
class EMST_PromoMessage_CartController extends Mage_Checkout_CartController
{
	/**
     * Initialize coupon
     */
    public function couponPostAction()
    {
        /**
         * No reason continue with empty shopping cart
         */
        if (!$this->_getCart()->getQuote()->getItemsCount()) {
            $this->_goBack();
            return;
        }
//        $cart = Mage::getSingleton('checkout/cart')->getQuote();
//        foreach ($cart->getAllItems() as $item) {
//            $buyInfo = $item->getBuyRequest();
//            $custom = $buyInfo->getIndiesRecurringandrentalpaymentsSubscriptionType();
//            if($custom && $custom > 0){
//                $session = Mage::getSingleton('core/session');
//                $session->addError($this->__('Coupon code and reward points cannot be used with autoship order.'));
//                $this->_goBack();
//                return;
//            }
//        }

        $rewardPoint = Mage::getSingleton('checkout/cart')->getQuote()->getData('rewardpoints_quantity');
        if($rewardPoint > 1){
            $session = Mage::getSingleton('core/session');
            $session->addError($this->__('Coupon code and reward points cannot be used at the same time.'));
            $this->_goBack();
            return;
        }
        $couponCode = (string) $this->getRequest()->getParam('coupon_code');
        if ($this->getRequest()->getParam('remove') == 1) {
            $couponCode = '';
        }
        $oldCouponCode = $this->_getQuote()->getCouponCode();

        if (!strlen($couponCode) && !strlen($oldCouponCode)) {
            $this->_goBack();
            return;
        }

        try {
            $this->_getQuote()->getShippingAddress()->setCollectShippingRates(true);
            $this->_getQuote()->setCouponCode(strlen($couponCode) ? $couponCode : '')
                ->collectTotals()
                ->save();

            if (strlen($couponCode)) {
                if ($this->_getQuote()->getCouponCode()) {
                    $this->_getSession()->addSuccess(
                        $this->__('Coupon code "%s" was applied.', Mage::helper('core')->htmlEscape($couponCode))
                    );
                }
                elseif($this->_getSession()->getCouponMessage()) {					
					$this->_getSession()->addError(
                        $this->__($this->_getSession()->getCouponMessage(), Mage::helper('core')->htmlEscape($couponCode))
                    );
					$this->_getSession()->unsCouponMessage();
				}
				else {
					$coupon = Mage::getModel('salesrule/coupon')->loadByCode($couponCode);
					$dateModel = Mage::getModel('core/date');
                    if($expDate = $coupon->getExpirationDate()) {
                        $expirationDate = $dateModel->timestamp($coupon->getExpirationDate());
                        $currDate = $dateModel->timestamp();
                        if($coupon->getId() && $expirationDate <= $currDate) {
                            $this->_getSession()->addError(
                                $this->__('Coupon code "%s" has expired.', Mage::helper('core')->htmlEscape($couponCode))
                            );
                        }
                        else {
                            $this->_getSession()->addError(
                                $this->__('Coupon code "%s" is not valid.', Mage::helper('core')->htmlEscape($couponCode))
                            );
                        }
                    } else {
                        $this->_getSession()->addError(
                            $this->__('Coupon code "%s" is not valid.', Mage::helper('core')->htmlEscape($couponCode))
                        );
                    }
                }
            } else {
                $this->_getSession()->addSuccess($this->__('Coupon code was canceled.'));
            }

        } catch (Mage_Core_Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        } catch (Exception $e) {
            $this->_getSession()->addError($this->__('Cannot apply the coupon code.'));
            Mage::logException($e);
        }

        $this->_goBack();
    }
}
