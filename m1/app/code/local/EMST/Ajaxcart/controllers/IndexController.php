<?php
/**
 * Developed by Acommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.Acommerce.asia/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Ajaxcart to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_Ajaxcart
 * @copyright   Copyright (c) 2012 Acommerce (http://www.Acommerce.asia)
 * @license     http://www.Acommerce.asia/LICENSE-E.txt
*/
require_once Mage::getModuleDir('controllers', 'Mage_Checkout') . DS . 'CartController.php';
class EMST_Ajaxcart_IndexController extends Mage_Checkout_CartController
{
	protected $_oriUrl = '';
	public function preDispatch() {
        parent::preDispatch();
        $this->_oriUrl = Mage::getSingleton('core/session')->getLastUrl();
        return $this;
    }
	public function postDispatch() {
        parent::postDispatch();
        Mage::getSingleton('core/session')->setLastUrl($this->_oriUrl);
        return $this;
    }
    public function indexAction()
    {
		try{
			if(!$this->_checkAjaxRequest()) {
				return ;
			}
			// $product = Mage::getModel('catalog/product')->load(7457);
			// Mage::register('product', $product);
			// Mage::register('current_product', $product);			
			$this->_addHandleMessageBlock();
			$pId = $this->getRequest()->getParam('product',false);
			if(!$this->_validate($pId)) {
				$this->_noPidError();
				$this->getResponse()->setBody('<br/><br/><br/><br/>'.$this->getLayout()->getMessagesBlock()->getGroupedHtml()); 				
				return;
			}
			$this->getLayout()->getUpdate()->addHandle('catalog_product_view');
			Mage::helper('catalog/product_view')->prepareAndRender($pId, $this);
			//$this->loadLayout('catalog_product_view');			
			$addTo = $this->getLayout()->getBlock('product.info')->setTemplate('emst_ajaxcart/product.info.phtml');;			
			/* if(strpos($addTo->getTemplate(),'view-simple.phtml')!== false) {				
				$addTo->setTemplate('emst_ajaxcart/view-simple.phtml');
			} else {
				$addTo->setTemplate('emst_ajaxcart/product.info.phtml');
			}	 */		
			$this->getResponse()->setBody($addTo->toHtml());    	
		}
		catch(Exception $e) {
			Mage::logException($e);
		}
		return;
    }
	protected function _checkAjaxRequest() {
		return $this->getRequest()->isAjax();
	}
	protected function _noPidError() {
		Mage::getSingleton('catalog/session')->addError($this->__('The requested product is not available or the session was expired, please try again later.'));
	}
	protected function _addHandleMessageBlock()
    {
		$this->_initLayoutMessages('checkout/session');
		$this->_initLayoutMessages('catalog/session');
    }
	protected function _validate($pId = null) {
		$active = Mage::helper('emst_ajaxcart')->isEnabled();
		if(!$active || !$pId)  {			
			return false;
		}
		return true;
	}
	protected function _responseMessage() {
		$this->_addHandleMessageBlock();
		$return =  array('error'=>$this->getLayout()->getMessagesBlock()->getGroupedHtml());
		$this->getResponse()->setBody(Mage::helper('core')->jsonEncode($return));
	}
	public function addToCartAction() {
		if(!$this->_checkAjaxRequest()) {
			return ;
		}
		if(!$this->_validate($this->getRequest()->getParam('product',false))) {
			$this->_noPidError();
			 $this->_responseMessage();
			return;
		}
		$cart   = $this->_getCart();
        $params = $this->getRequest()->getParams();
        try {
            if (isset($params['qty'])) {
                $filter = new Zend_Filter_LocalizedToNormalized(
                    array('locale' => Mage::app()->getLocale()->getLocaleCode())
                );
                $params['qty'] = $filter->filter($params['qty']);
            }

            $product = $this->_initProduct();
            $related = $this->getRequest()->getParam('related_product');

            /**
             * Check product availability
             */
            if (!$product) {				
				$this->_getSession()->addError($this->__('The requested product is not available'));
                $this->_responseMessage();
                return;
            }			
            $cart->addProduct($product, $params);
            if (!empty($related)) {
                $cart->addProductsByIds(explode(',', $related));
            }

            $cart->save();

            $this->_getSession()->setCartWasUpdated(true);

            /**
             * @todo remove wishlist observer processAddToCart
             */
            Mage::dispatchEvent('checkout_cart_add_product_complete',
                array('product' => $product, 'request' => $this->getRequest(), 'response' => $this->getResponse())
            );

            if (!$this->_getSession()->getNoCartRedirect(true)) {
                if (!$cart->getQuote()->getHasError()){
                    $message = $this->__('%s was added to your shopping cart.', Mage::helper('core')->escapeHtml($product->getName()));
                    $this->_getSession()->addSuccess($message);
					$this->result($product);
					return;
                }
                
            }
			$this->_getSession()->addError($this->__('There is an error in your shopping cart items, please %sgo to your shopping cart%s to correct it.',
				'<a href="'.Mage::getUrl('checkout/cart').'">','</a>'));
        } catch (Mage_Core_Exception $e) {
            if ($this->_getSession()->getUseNotice(true)) {
                $this->_getSession()->addNotice(Mage::helper('core')->escapeHtml($e->getMessage()));
            } else {
                $messages = array_unique(explode("\n", $e->getMessage()));
                foreach ($messages as $message) {
                    $this->_getSession()->addError(Mage::helper('core')->escapeHtml($message));
                }
            }
        } catch (Exception $e) {
            $this->_getSession()->addException($e, $this->__('Cannot add the item to shopping cart.'));
            Mage::logException($e);            
        }
		 $this->_responseMessage();
	}
	protected function result($product) {
		try{
			if(!$product) {
				return;
			}
			$this->loadLayout();		
			$return = array();
			$this->_initLayoutMessages('checkout/session');
			if(Mage::helper('core')->isModuleEnabled('Itc_Cdlogin')) {
				$return['headerajax'] = true;
			}
			else {
				$globalLinks = $this->getLayout()->getBlock('global.header.links')->toHtml();
				$ori = $this->getRequest()->getParam('oriReq');
				// $oriUrl = $this->getRequest()->getParam('currUrl');
				if($ori) {
					$curr = $this->getRequest()->getRequestString();
					$globalLinks = str_replace($curr,Mage::helper('core')->urlDecode($ori),$globalLinks);
				}
				// if($oriUrl) {
					// $globalLinks = str_replace(Mage::helper('core/url')->getEncodedUrl(),$oriUrl,$globalLinks);
				// }
				$return['globallinks'] = $globalLinks;
			}
			// $topLinks = $this->getLayout()->getBlock('top.links')->toHtml();			
			$resultHtml = $this->getLayout()->createBlock('emst_ajaxcart/result')->setProduct($product)->setTemplate('emst_ajaxcart/result.phtml')->toHtml();
			$return['result'] = $resultHtml;
		}
		catch(Exception $e) {
			Mage::logException($e);
			if(isset($return['result'])) {
				unset($return['result']);
			}
			$return['error'] = Mage::getUrl('checkout/cart/index');
		}
		$this->getResponse()->setBody(Mage::helper('core')->jsonEncode($return));
		return;
	}
}