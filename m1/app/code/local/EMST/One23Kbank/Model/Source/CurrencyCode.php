<?php
class EMST_One23Kbank_Model_Source_CurrencyCode
{
	public function getCurrencyCode() {
		return array(
			"THB" => "THB"
		);
	}
	public function toOptionArray()
    {
		$options = array();
		foreach($this->getCurrencyCode() as $lb=>$vl) {
			$options[] = array('value' => $vl, 'label' => $lb);
		}
        return $options;
    }
}