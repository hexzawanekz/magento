<?php

// Insert data to table shipping_tablerateindo
// Add 2 conditions
//   + Shipping fee is 200 if subtotal less than 500 Bath
//   + Shipping fee is 0 if subtotal greater than 500 Bath

/** @var Mage_Core_Model_Resource $installer */
$installer = Mage::getSingleton('core/resource');
$writer = $installer->getConnection('core_write');

$tableName = $installer->getTableName('emst_tablerate/tablerateindo');
$data = array(
    array('1', 'ID', '0', '*', 'package_value', 0, 0, 0),
);

foreach ($data as $row) {
    $bind = array(
        'website_id' => $row[0],
        'dest_country_id' => $row[1],
        'dest_region_id' => $row[2],
        'dest_zip' => $row[3],
        'condition_name' => $row[4],
        'condition_value' => $row[5],
        'price' => $row[6],
        'cost' => $row[7],
    );
    $writer->insert($tableName, $bind);
}