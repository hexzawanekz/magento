<?php 
class EMST_One23BBL_Model_Source_ChannelAgents
{
	const CHANNELS 	= array(
						'OVERTHECOUNTER' 	=> '',
						'ATM' 				=> '',
						'BANKCOUNTER' 		=> '',
						'IBANKING' 			=> '',
						'WEBPAY' 			=> '',
						);
	const AGENTS 	= array(
						'7ELEVEN'	=>'7ELEVEN',
						'TRUEMONEY'	=>'TRUEMONEY',
						'TESCO'		=>'TESCO',
						'TOT'		=>'TOT',
						'PAYATPOST'	=>'PAYATPOST',
						'SCB'		=>'SCB',
						'KTB'		=>'KTB',
						'TMB'		=>'TMB',
						'UOB'		=>'UOB',
						'BAY'		=>'BAY',
						'BBL'		=>'BBL',
						'KBANK'		=>'KBANK',
						);
}
  