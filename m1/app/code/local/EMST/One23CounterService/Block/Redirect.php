<?php


class EMST_One23CounterService_Block_Redirect extends Mage_Core_Block_Abstract
{
	protected function _toHtml()
	{
		$one23CounterService = $this->getModel();
        $form = new Varien_Data_Form();
        $form->setAction($one23CounterService->getGatewayUrl())
            ->setId('123counterservice_payment_checkout')
            ->setName('123counterservice_payment_checkout')
            ->setMethod('POST')
            ->setUseContainer(true);
        foreach ($one23CounterService->getStandardCheckoutFormFields() as $field => $value) {
            $form->addField($field, 'hidden', array('name' => $field, 'value' => $value));
        }

        $formHTML = $form->toHtml();

        $html = '<html>';
		$html .= '<head><meta content="text/html; charset=utf-8" http-equiv="Content-Type"></head>';
		$html .= '<body>';
        $html.= $this->__('You will be redirected to OneTwoThree Gateway in a few seconds.');
        $html.= $formHTML;
        $html.= '<script type="text/javascript">document.getElementById("123counterservice_payment_checkout").submit();</script>';
        $html.= '</body></html>';


        return $html;
    }

}
