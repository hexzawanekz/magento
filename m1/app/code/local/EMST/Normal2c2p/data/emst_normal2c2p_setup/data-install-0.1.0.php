<?php

/** @var Mage_Core_Model_Resource_Setup $this */
$this->startSetup();

$agents = array(
    array(
        'bank' => 'BAY',
        'name' => 'Bank of Ayutthaya',
    ),
    array(
        'bank' => 'BBL',
        'name' => 'Bangkok Bank',
    ),
    array(
        'bank' => 'BIGC',
        'name' => 'Big C Supercenter',
    ),
    array(
        'bank' => 'KBANK',
        'name' => 'Kasikorn Bank',
    ),
    array(
        'bank' => 'KTB',
        'name' => 'Krung Thai Bank',
    ),
    array(
        'bank' => 'MPAY',
        'name' => 'mPay Station by AIS',
    ),
    array(
        'bank' => 'PAYATPOST',
        'name' => 'Pay@Post by Thailand post',
    ),
    array(
        'bank' => 'SCB',
        'name' => 'Siam Commercial Bank',
    ),
    array(
        'bank' => 'TBANK',
        'name' => 'Thanachart Bank Public Company Ltd.',
    ),
    array(
        'bank' => 'TESCO',
        'name' => 'Tesco Lotus Counter Service',
    ),
    array(
        'bank' => 'TMB',
        'name' => 'TMB Bank Public Company Limited',
    ),
    array(
        'bank' => 'TOT',
        'name' => 'Just Pay by TOT public Company Ltd.',
    ),
    array(
        'bank' => 'TRUEMONEY',
        'name' => 'True Money Shop',
    ),
    array(
        'bank' => 'UOB',
        'name' => 'United Overseas Bank',
    ),
    array(
        'bank' => 'INDOMARET',
        'name' => 'Indo Maret',
    ),
    array(
        'bank' => 'BIIVA',
        'name' => 'BII Bank',
    ),
    array(
        'bank' => 'BANK_BCA',
        'name' => 'BCA Bank',
    ),
    array(
        'bank' => 'BANK_MANDIRI',
        'name' => 'Mandiri Bank',
    ),
    array(
        'bank' => '1STOP',
        'name' => '1-Stop',
    ),
);

/** @var Mage_Core_Model_Config $config */
$config = Mage::getModel('core/config');
$config->saveConfig(EMST_Normal2c2p_Model_PaymentChannel::XML_CASH_AGENT_CHANNELS, serialize($agents));

$this->endSetup();