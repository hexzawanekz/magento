<?php


class EMST_Normal2c2p_Block_Info extends Mage_Payment_Block_Info
{
    protected function _prepareSpecificInformation($transport = null)
    {
        $transport = parent::_prepareSpecificInformation($transport);
        if (!Mage::app()->getStore()->isAdmin()) {
            return $transport;
        }
        $payment = $this->getInfo();
        $info = $payment->getAdditionalInformation();

        $transport->addData($info);

        //$transport->addData(array('Status' => $payment->getCcApproval()));
        //$transport->addData(array('Credit card transaction id' => $payment->getCcTransId()));
        return $transport;
    }

}
