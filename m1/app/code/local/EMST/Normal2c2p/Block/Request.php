<?php


class EMST_Normal2c2p_Block_Request extends Mage_Core_Block_Abstract
{
    protected function _toHtml()
    {
        /** @var Mage_Checkout_Model_Session $session */
        $session = Mage::getSingleton('checkout/session');
        $pay_channel = !empty($session->getNormalPaymentChannel()) ? $session->getNormalPaymentChannel() : '';

        /** @var EMST_Normal2c2p_Model_Normal2c2p $model */
        $model = $this->getModel();

        /** @var Mage_Sales_Model_Order $order */
        $order = $model->getOrder();
        $autoship = Mage::helper('recurringandrentalpayments')->getSubscriptionPaymentData($order, $model);

        $html = '<html>';
        $html .= '<head><meta content="text/html; charset=utf-8" http-equiv="Content-Type"></head>';
        $html .= '<body>';
        $html.= $this->__('You will be redirected to Credit card Storage Gateway in a few seconds.');
        $html.= '<form style="display:none;"id="normal2c2p_payment_checkout" method="post" action="'.$model->getGatewayRedirectUrl().'">';
        $html.= '<input type="text" id="version" name="version" value="'.$model->getVersion().'"/>';
        $html.= '<input type="text" id="merchant_id" name="merchant_id" value="'.$model->getMerchantId().'"/>';
        $html.= '<input type="text" id="payment_description" name="payment_description" value="'.$model->getPaymentDescription().'" />';
        $html.= '<input type="text" id="order_id" name="order_id" value="'.$order->getIncrementId().'" />';
        $html.= '<input id="invoice_no" name="invoice_no" type="text" value="'.$order->getId().'" />';
        $html.= '<input id="currency" name="currency" type="text" value="'.$model->getCurrencyCode().'"/>';
        $html.= '<input id="amount" name="amount" type="text" value="'.$model->getAmount($order->getData('base_grand_total')).'"/>
                <input type="text" id="customer_email" name="customer_email" value="'.Mage::getSingleton('customer/session')->getCustomer()->getEmail().'"/>
                <input type="text" id="pay_category_id" name="pay_category_id" value="'.$model->getPayCategoryID().'"/>
                <input type="text" id="promotion" name="promotion" value="'.$model->getPromotion().'"/>
                <input type="text" id="user_defined_1" name="user_defined_1" value="'.$model->getUserDefined(1).'"/>
                <input type="text" id="user_defined_2" name="user_defined_2" value="'.$model->getUserDefined(2).'"/>
                <input type="text" id="user_defined_3" name="user_defined_3" value="'.$model->getUserDefined(3).'"/>
                <input type="text" id="user_defined_4" name="user_defined_4" value="'.$model->getUserDefined(4).'"/>
                <input type="text" id="user_defined_5" name="user_defined_5" value="'.$model->getUserDefined(5).'"/>
                <input type="text" id="result_url_1" name="result_url_1" value=""/>
                <input type="text" id="result_url_2" name="result_url_2" value=""/>';
        if ($pay_channel == EMST_Normal2c2p_Model_PaymentChannel::CASH_PAYMENT_CHANNEL) {
            $html .= '<input id="payment_option" name="payment_option" type="text" value="' . EMST_Normal2c2p_Model_PaymentChannel::PAY_OP_ONE_TWO_THREE . '"/>';
        } elseif ($pay_channel == EMST_Normal2c2p_Model_PaymentChannel::CREDIT_DEBIT_CARDS) {
            $html .= '<input id="payment_option" name="payment_option" type="text" value="' . EMST_Normal2c2p_Model_PaymentChannel::PAY_OP_CREDIT_CARD . '"/>';
        } else {
            $html .= '<input id="payment_option" name="payment_option" type="text" value="' . EMST_Normal2c2p_Model_PaymentChannel::PAY_OP_ALL . '"/>';
        }

        if ($autoship && $pay_channel != EMST_Normal2c2p_Model_PaymentChannel::CASH_PAYMENT_CHANNEL) {
            $html .= '
                <input type="text" id="recurring" name="recurring" value="'. $autoship['recurring'].'"/>
                <input type="text" id="order_prefix" name="order_prefix" value="'. $autoship['order_prefix'].'"/>
                <input type="text" id="recurring_amount" name="recurring_amount" value="'. $autoship['recurring_amount'].'"/>
                <input type="text" id="allow_accumulate" name="allow_accumulate" value="'. $autoship['allow_accumulate'].'"/>
                <input type="text" id="max_accumulate_amount" name="max_accumulate_amount" value="'. $autoship['max_accumulate_amount'].'"/>
                <input type="text" id="recurring_count" name="recurring_count" value="'. $autoship['recurring_count'].'"/>
                <input type="text" id="charge_next_date" name="charge_next_date" value="'. $autoship['charge_next_date'].'"/>
                <input type="text" id="charge_on_date" name="charge_on_date" value="'. $autoship['charge_on_date'].'"/>
                ';
        }
        $html.= '<input type="text" id="payment_expiry" name="payment_expiry" value="'. $model->getPaymentExpiryTime() .'"/>
                <input type="hidden" id="hash_value" name="hash_value" value="'.$model->getHash($pay_channel).'"/>
                <input type="Submit" name="btnSubmit" value="Submit" id="btnSubmit" />
            </form>';
        $html.= '<script type="text/javascript">document.getElementById("normal2c2p_payment_checkout").submit();  document.getElementById("normal2c2p_payment_checkout").innerHTML =""</script>';
        $html.= '</body></html>';

        return $html;
    }



}
