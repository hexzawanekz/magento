<?php

class EMST_Normal2c2p_Model_PaymentChannel extends Mage_Core_Model_Abstract
{
    const CREDIT_DEBIT_CARDS    = '001';// Credit card
    const CASH_PAYMENT_CHANNEL  = '002';// 123 Payment service
    const DIRECT_DEBIT_CARD     = '003';
    const OTHER_CHANNELS        = '004';
    const IPP_TRANSACTION       = '005';// Installment Channel

    const XML_CASH_AGENT_CHANNELS   = 'payment/normal2c2p/cash_payment_channel/agent';
    const XML_BANK_CHANNELS         = 'payment/normal2c2p/bank';
    const XML_PAYMENT_EXPIRY_TIME   = 'payment/normal2c2p/cash_payment_channel/payment_expiry';

    const PAY_STATUS_SUCCESS        = '000';
    const PAY_STATUS_PENDING        = '001';
    const PAY_STATUS_REJECTED       = '002';
    const PAY_STATUS_USER_CANCEL    = '003';
    const PAY_STATUS_ERROR          = '999';

    const PAY_OP_FULL_AMOUNT    = 'F';
    const PAY_OP_INSTALLMENT    = 'I';
    const PAY_OP_CREDIT_CARD    = 'C';
    const PAY_OP_ONE_TWO_THREE  = '1';
    const PAY_OP_ALL            = 'A';

    /**
     * Get/check payment status by word
     *
     * @param $responseCode
     * @return mixed|string
     */
    public function getPaymentStatusByText($responseCode)
    {
        $list = array(
            'Success'                           => $this::PAY_STATUS_SUCCESS,
            'Pending (waiting customer to pay)' => $this::PAY_STATUS_PENDING,
            'Rejected'                          => $this::PAY_STATUS_REJECTED,
            'User cancel'                       => $this::PAY_STATUS_USER_CANCEL,
            'Error'                             => $this::PAY_STATUS_ERROR,
        );

        $result = array_search($responseCode, $list);

        return $result ? $result . " ({$responseCode})" : $responseCode;
    }

    /**
     * Get paid channel for Cash Payment Channel only
     *
     * @param $paymentChannelCode
     * @param $responseCode
     * @return bool|string
     */
    public function getPaidChannelByText($paymentChannelCode, $responseCode)
    {
        if($paymentChannelCode == $this::CASH_PAYMENT_CHANNEL){
            $list = array(
                'ATM Machine'       => 'ATM',
                'Bank Counter'      => 'BANKCOUNTER',
                'Internet Banking'  => 'IBANKING',
                'Over the counter'  => 'OVERTHECOUNTER',
                'Web Payment'       => 'WEBPAY',
            );

            $result = array_search($responseCode, $list);

            return $result ? $result : $responseCode;
        }
        return false;
    }

    /**
     * Get Cash agent channel name
     *
     * @param $paymentChannelCode
     * @param $bankCode
     * @return bool|null|string
     */
    public function getCashAgentChannel($paymentChannelCode, $bankCode)
    {
        if($paymentChannelCode == $this::CASH_PAYMENT_CHANNEL){
            $agents = unserialize(Mage::getStoreConfig($this::XML_CASH_AGENT_CHANNELS));
            $result = null;
            foreach($agents as $item){
                if($item['bank'] == $bankCode){
                    $result = $item['name'];
                    break;
                }
            }
            return $result ? $result . " ({$bankCode})" : $bankCode;
        }
        return false;
    }

    /**
     * Get bank name by code
     *
     * @param $bankCode
     * @return null|string
     */
    public function getBank($bankCode)
    {
        $agents = unserialize(Mage::getStoreConfig($this::XML_BANK_CHANNELS));
        $result = null;
        foreach($agents as $item){
            if($item['bank'] == $bankCode){
                $result = $item['name'];
                break;
            }
        }
        return $result ? $result . " ({$bankCode})" : $bankCode;
    }

    public function getCashPaymentDesc($responseCode)
    {
        $list = array(
            '000' => 'SUCCESS(PAID)',
            '001' => 'SUCCESS(PENDING)',
            '002' => 'TIMEOUT',
            '003' => 'INVALID MESSAGE',
            '004' => 'INVALID PROFILE(MERCHANT) ID',
            '005' => 'DUPLICATED INVOICE',
            '006' => 'INVALID AMOUNT',
            '007' => 'INSUFFICIENT BALANCE',
            '008' => 'INVALID CURRENCY CODE',
            '009' => 'PAYMENT EXPIRED',
            '010' => 'PAYMENT CANCELED BY PAYER',
            '011' => 'INVALID PAYEE ID',
            '012' => 'INVALID CUSTOMER ID',
            '013' => 'ACCOUNT DOES NOT EXIST',
            '014' => 'AUTHENTICATION FAILED',
            '015' => 'SUCCESS (PAID MORE MISMATCHED), customer paid more than transaction amount',
            '016' => 'SUCCESS (PAID LESS MISMATCHED), customer paid less than transaction amount',
            '017' => 'SUCCESS (PAID EXPIRED), customer paid expired transaction',
            '998' => 'INTERNAL ERROR FROM MERCHANT SITE',
            '999' => 'SYSTEM ERROR',
        );

        if(array_key_exists($responseCode, $list)){
            return ucfirst(strtolower($list[$responseCode]));
        }else{
            return '';
        }
    }
}