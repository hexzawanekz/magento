<?php
class EMST_Normal2c2p_Model_Cronjobs {
	protected $_paymodel = null;
	public function checkTransaction() {
		$normal2c2p = $this->getPayModel();
		if($normal2c2p->isCronEnable() && $normal2c2p->getInquiryUrl()) {
			
			$ids = Mage::getResourceModel('normal2c2p/order_grid_collection')
							->addFieldToFilter('main_table.status', array('nin' => $this->_getExcludedStates()))
							->addFieldToFilter('created_at',array('gt'=>"DATE_SUB(".Varien_Date::now().", INTERVAL 30 MINUTE)"))
							->getAllIds();
			
			Mage::helper('normal2c2p')->transactionInquiry($ids,$normal2c2p->cronInvoice());
		}
	}
	 protected function _getExcludedStates() {
        return array(
            Mage_Sales_Model_Order::STATE_CLOSED,
            Mage_Sales_Model_Order::STATE_CANCELED,
            Mage_Sales_Model_Order::STATE_HOLDED,
            Mage_Sales_Model_Order::STATE_PROCESSING,
            Mage_Sales_Model_Order::STATE_COMPLETE,
        );
    }
	public function getPayModel() {
		if(!$this->_paymodel) {
			$this->_paymodel = Mage::getModel('normal2c2p/normal2c2p');
		}
		return $this->_paymodel;
	}
}
