<?php
/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Onestepcheckout to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_Onestepcheckout
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
*/

class EMST_Cc2p_Block_Adminhtml_System_Config_Form_Field_Bank extends Mage_Adminhtml_Block_System_Config_Form_Field_Array_Abstract
{
    public function __construct()
    {
        $this->addColumn('bank', array(
            'label' => Mage::helper('adminhtml')->__('Bank Code'),
            'style' => 'width:120px',
        ));
        $this->addColumn('name', array(
            'label' => Mage::helper('adminhtml')->__('Name'),
            'style' => 'width:420px',
        ));
        $this->_addAfter = true;
        $this->_addButtonLabel = Mage::helper('adminhtml')->__('Add');
        parent::__construct();
        $this->setTemplate('cc2p/system/config/form/field/array.phtml');
    }
}
