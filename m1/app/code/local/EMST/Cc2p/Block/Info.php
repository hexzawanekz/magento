<?php


class EMST_Cc2p_Block_Info extends Mage_Payment_Block_Info
{
	protected function _prepareSpecificInformation($transport = null)
  {
		$transport = parent::_prepareSpecificInformation($transport);
		if(!Mage::app()->getStore()->isAdmin()) {
			return $transport;
		}
    $payment = $this->getInfo();
    $info = $payment->getAdditionalInformation();
    $masked_cc_number = '';
    $approval_code = '';
    $eci = '';
    $fail_reason = '';
    $import = '';
    $reason = '';

    if(isset($info['masked_cc_number'])){
      $masked_cc_number = $info['masked_cc_number'];
    }

    if(isset($info['approval_code'])){
      $approval_code = $info['approval_code'];
    }

    if(isset($info['eci'])){
      $eci = $info['eci'];
    }

    if(isset($info['fail_reason'])){
      $fail_reason = $info['fail_reason'];
    }
	if(isset($info['Import To AEON Point Stystem'])){
      $import = $info['Import To AEON Point Stystem'];
    }
	if(isset($info['Reason'])){
      $reason = $info['Reason'];
    }

    $transport->addData(
							array('masked_cc_number' => $masked_cc_number,
                              'approval_code' =>  $approval_code,
                              'eci' =>  $eci,
                              'fail_reason' =>  $fail_reason,
                              'Import To AEON Point Stystem' =>  $import,
                              'Reason' =>  $reason,
							  )							  
						);

    $transport->addData(array('Status'=>$payment->getCcApproval()));
    $transport->addData(array('Credit card transaction id'=>$payment->getCcTransId()));
		return $transport;
  }

}
