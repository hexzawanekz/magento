<?php
class EMST_Cc2p_Model_Source_Banks
{
	public function toOptionArray() {
		$_banks = array();
		foreach(Mage::getModel('cc2p/cc2p')->getBanks() as $code => $name) {
			$_banks[]  = array('value'=>$code,'label'=>$name);
		}
		return $_banks;
    }
}
