<?php

class EMST_Cc2p_Model_Card extends Mage_Core_Model_Abstract
{
    public function _construct(){
        parent::_construct();
        $this->_init('cc2p/card');
    }	
	protected function _beforeSave()
    {
        $date = Mage::getModel('core/date')->gmtDate();        
		$this->setAddedAt($date);
        return parent::_beforeSave();
    }
}