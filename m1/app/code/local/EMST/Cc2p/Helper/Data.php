<?php
class EMST_Cc2p_Helper_Data extends Mage_Core_Helper_Abstract {
	protected function _validData($data) {
		if(!is_array($data)) {
			$data = array($data);
		}
		return array_merge(array(
					'Version'=>'',
					'timeStamp'=>'',
					'respCode'=>'',
					'pan'=>'',
					'amt'=>'',
					'invoiceNo'=>'',
					'tranRef'=>'',
					'approvalCode'=>'',
					'eci'=>'',
					'dateTime'=>'',
					'status'=>'',
					'failReason'=>'',
					'recurringActive'=>'',
					'hashValue'=>'',
				),$data);
	}
	public function transactionInquiry($orderIds,$invoice = false) {
		if(!is_array($orderIds)) {
			$orderIds = array($orderIds);
		}

		$query = 0;
		$inv = 0;
		$messages = array();
		$payModel = Mage::getModel('cc2p/cc2p');
		if(!$payModel->getInquiryUrl()) {
			throw Mage::exception('Mage_Payment',$this->__('Inquiry URL was not set correctly'));
		}
		foreach($orderIds as $id) {
			$order = Mage::getModel('sales/order')->loadByIncrementId($id);
			if($order->getId() && !in_array($order->getState(),array(Mage_Sales_Model_Order::STATE_COMPLETE,Mage_Sales_Model_Order::STATE_CLOSED))) {
				try {
					$result = $this->_validData($payModel->inquiry($order));
					if($result) {
						$resHash = isset($result['HashValue'])?$result['HashValue']:'';
						++$query;
						$payModel->setParsedData($result);
						
						if($result['respCode'] == '00' && $result['hashValue'] == $payModel->getInquiryResponseHash($result) && $result['approvalCode']=='A') {
							//$hash = $payModel->hashData($payModel->getMerchantId().$id.$transaction->getTransactionId());
							if($invoice && $order->canInvoice()) {
									$payModel->saveInvoice();
									++$inv;
							}
							else {
								$payModel->saveFailReason();
							}
						}
						else {
							//$messages[$id] = Mage::helper('cc2p')->__('The result was saved to order');
							$payModel->saveFailReason();
						}
					}
					else {
						//$messages[$id] = Mage::helper('cc2p')->__('Can not query the transaction from gateway');;
					}
				}
				catch(Exception $e) {
					$messages[$id] = $e->getMessage();
				}
			}
			elseif(!$order->getId()) {
				$messages[$id] = Mage::helper('cc2p')->__('Order doesn\'t exist.');
			}
			else {
				$messages[$id] = Mage::helper('cc2p')->__('Orders with status "complete","closed" won\'t be queried.');;
			}
		}
		return array(array($query,$inv),$messages);
	}
}
