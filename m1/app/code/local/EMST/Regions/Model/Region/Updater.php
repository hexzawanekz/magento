<?php
/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Regions to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_Regions
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
*/
class EMST_Regions_Model_Region_Updater extends Varien_Object
{
    protected $_country;
    
    /**
     * Add Regions
     * 
     * @param array $regions array
     * @return int $success Number of returned entries
     */
    public function addRegions($regions)
    {
        $success = 0;
        $connection = $this->_getConncection();
        $default_locale = $this->getDefaultLocale();
        $count_region = $connection->prepare("SELECT region_id FROM `directory_country_region` WHERE `code` = ? " .
            "AND `country_id` = '{$this->getCountryId()}'");
		$count_locale = $connection->prepare("SELECT COUNT(*) FROM `directory_country_region_name` WHERE `region_id` = ? " .
            "AND `locale` = ?");
        $insert = $connection->prepare("INSERT INTO `directory_country_region` (`region_id`,`country_id`," 
            ."`code`,`default_name`) VALUES (NULL,'{$this->getCountryId()}',?,?)");
        $insertLocale = $connection->prepare("INSERT INTO `directory_country_region_name` " 
            ."(`locale`,`region_id`,`name`) VALUES (?,?,?)");		
        foreach ($regions as $region) {                        
            $count_region->execute(array(substr($region['code'], 0, 32)));
			$exist_regions = $count_region->fetch();			
            if(!$exist_regions) {
				$insert->execute(array($region['code'], $region['name']));  
				$regionId = $connection->lastInsertId();
            }
			else {				
				$regionId = $exist_regions['region_id'];
				if ($region['locale'] == $default_locale) {
					$data = array(
						'id' 			=> $regionId,
						'country_id' 	=> $this->getCountryId(),
						'code' 			=> $region['code'],
						'default_name' 	=> $region['name'],
						'locale' 		=> $region['locale'],
					);
					$this->updateRegion($data);
				}
			} 
			$count_locale->execute(array($regionId,$region['locale']));
			if($count_locale->fetchColumn(0)==0) {
				$insertLocale->execute(array($region['locale'], $regionId, $region['name']));
			}
			else {
				continue;
			}
            
            $success++;
        }
        return $success;
    }
    
	public function updateRegion($data) {	
		if(!isset($data['id'])) {
			return;
		}
		$this->setId($data['id']);
		$connection = $this->_getConncection();
		if(isset($data['code']) || isset($data['default_name']) || isset($data['country_id'])) {
			$sql = "UPDATE `directory_country_region` SET";
			if(isset($data['country_id'])) {
				$sql .= " country_id = '".$data['country_id']."'";
			}
			if(isset($data['code'])) {
				$sql .= ", code = '".$data['code']."'";
			}
			if(isset($data['default_name'])) {
				$sql .= ", default_name = '".$data['default_name']."'";
			}
			$sql .= " WHERE `region_id` = ".$data['id'];
			
			$result = $connection->prepare($sql)->execute();			
			
		}
		if(isset($data['name']) && $data['locale']) {
			$sql = "UPDATE `directory_country_region_name` SET";	
			if(isset($data['name'])) {
				$sql .= " `name` = '".$data['name']."'";
			}
			$sql .= " WHERE `region_id` = ".$data['id']." AND `locale` = '".$data['locale']."'";
			$connection->prepare($sql)->execute();
		}
	}
    /**
     * Delete Region
     * 
     * @param int $regionId region id
     * @return void
     */
    public function deleteRegion($regionId)
    {
        return $this->_getConncection()->delete("directory_country_region",
            $this->_getConncection()->quoteInto('`region_id` = ?', $regionId));
    }


    /**
     * Get DB connect
     * 
     * @return resource 
     */
    protected function _getConncection()
    {
        return Mage::getSingleton('core/resource')->getConnection('core_write');
    }
    
    /**
     * Set country id
     * 
     * @param int $countryId country id
     * @return Oggetto_Regions_Model_Region_Updater 
     */
    public function setCountryId($countryId)
    {
        $this->_country = $countryId;
        return $this;
    }
    
    /**
     * Set country id
     *
     * @return string|null
     */
    public function getCountryId()
    {
        if ($this->_country) {
            return $this->_country;
        } else {
            Mage::throwException(Mage::helper('emst_regions')->__('You should specify country id before use this method!'));
            return null;
        }
    }
}
