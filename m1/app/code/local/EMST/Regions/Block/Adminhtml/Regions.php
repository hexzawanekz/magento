<?php
/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Regions to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_Regions
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
*/

class EMST_Regions_Block_Adminhtml_Regions extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_regions';
    $this->_blockGroup = 'emst_regions';
    $this->_headerText = Mage::helper('emst_regions')->__('Regions Manager');
    $this->_addButtonLabel = Mage::helper('emst_regions')->__('Add Region');
	$this->_addButton('import', array(
            'label'     => Mage::helper('emst_regions')->__('Import Regions'),
            'onclick'   => 'setLocation(\'' . $this->getImportUrl() .'\')',
            'class'     => 'add',
        ));
    parent::__construct();
  }
  public function getImportUrl() {
	return $this->getUrl('*/*/import');
  }
}