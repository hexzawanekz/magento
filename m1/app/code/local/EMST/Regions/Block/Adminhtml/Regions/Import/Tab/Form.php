<?php
/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Regions to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_Regions
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
*/

class EMST_Regions_Block_Adminhtml_Regions_Import_Tab_Form extends Mage_Adminhtml_Block_Widget_Form 

{
  protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset('upload_file', array('legend' => Mage::helper('emst_regions')->__('Upload CSV')));

        $fieldset->addField('country_id', 'select', array(
            'name'      => 'country_id',
            'label'     => Mage::helper('emst_regions')->__('Country'),
            'title'     => Mage::helper('emst_regions')->__('Country'),
            'values'    => Mage::getResourceModel('directory/country_collection')->toOptionArray(),
			'value'		=> Mage::helper('core')->getDefaultCountry(),
            'required'  => true,
        ));
        
         $fieldset->addField('csvfile', 'file', array(
            'label'     => Mage::helper('emst_regions')->__('Select a CSV File'),
            'title'     => Mage::helper('emst_regions')->__('Select a CSV File'),
            'name'      => 'csvfile',
            'required'  => true,
        ));
        $insertVariableButton = $this->getLayout()
            ->createBlock('adminhtml/widget_button', '', array(
                'type'    => 'button',
                'label'   => Mage::helper('emst_regions')->__('Upload'),
                'onclick' => 'import_form.submit()',
				'class'	  => 'save'
            ));
        $fieldset->addField('upload', 'note', array(
            'text' => $insertVariableButton->toHtml()
        ));
        
       
        return parent::_prepareForm();
    }
    
}