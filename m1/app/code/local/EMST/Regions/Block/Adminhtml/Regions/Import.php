<?php
/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Regions to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_Regions
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
*/

class EMST_Regions_Block_Adminhtml_Regions_Import extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
        $this->_mode = 'import';
        $this->_objectId = 'id';
        $this->_blockGroup = 'emst_regions';
        $this->_controller = 'adminhtml_regions';        
        $this->_updateButton('save', 'label', Mage::helper('emst_regions')->__('Upload'));
    }
}