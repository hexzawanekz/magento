<?php
/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Regions to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_Regions
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
*/

class EMST_Regions_Block_Adminhtml_Regions_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

  public function __construct()
  {
      parent::__construct();
      $this->setId('regions_tabs');
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('emst_regions')->__('Region Information'));
  }

  protected function _beforeToHtml()
  {
      $this->addTab('form_section', array(
          'label'     => Mage::helper('emst_regions')->__('Region Information'),
          'title'     => Mage::helper('emst_regions')->__('Region Information'),
          'content'   => $this->getLayout()->createBlock('emst_regions/adminhtml_regions_edit_tab_form')->toHtml(),
      ));
     
      return parent::_beforeToHtml();
  }
}