<?php
/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Regions to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_Regions
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
*/

class EMST_Regions_Block_Adminhtml_Regions_Locale_Switcher extends Mage_Adminhtml_Block_Template
{
	protected $_localeVarName = 'locale';
	
	public function __construct()
    {
        parent::__construct();
        $this->setTemplate('regions/switcher.phtml');
        $this->setUseConfirm(true);
        $this->setUseAjax(true);
    }
	
    public function getLocales()
    {
        return Mage::app()->getLocale()->getOptionLocales();
    }
	public function getDefaultLocale() {
		return Mage::getModel('core/locale')->getDefaultLocale();
	}
	public function getLocaleCode() {
		$locale = Mage::registry('locale_code');
		if(!$locale) {
			return $this->getDefaultLocale();
		}
		return $locale;
	}
	public function getSwitchUrl()
    {
        if ($url = $this->getData('switch_url')) {
            return $url;
        }
        return $this->getUrl('*/*/*', array('_current' => true, $this->_localeVarName => null));
    }
}
