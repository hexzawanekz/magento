<?php
/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Regions to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_Regions
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
*/

class EMST_Regions_Block_Adminhtml_Regions_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('regions_form', array('legend'=>Mage::helper('emst_regions')->__('Region information')));
	  
      $locale = (Mage::registry('locale_code') && Mage::getModel('core/locale')->getDefaultLocale() != Mage::registry('locale_code'))?true:false;
	  $fieldset->addField('country_id', 'select', array(
            'name'      => 'country_id',
            'label'     => Mage::helper('emst_regions')->__('Country'),
            'title'     => Mage::helper('emst_regions')->__('Country'),
            'values'    => Mage::getResourceModel('directory/country_collection')->toOptionArray(),
			'value'		=> Mage::helper('core')->getDefaultCountry(),
            'required'  => true,
			'disabled'  => $locale,
        ));
        
      $fieldset->addField('code', 'text', array(
          'label'     => Mage::helper('emst_regions')->__('Region Code'),
          'class'     => 'required-entry',
          'required'  => true,
          'disabled'  => $locale,
          'name'      => 'code',
      ));
	  $fieldset->addField('name', 'text', array(
          'label'     => Mage::helper('emst_regions')->__('Region Name'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'name',
      ));
      if ( Mage::getSingleton('adminhtml/session')->getRegionsData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getRegionsData());
          Mage::getSingleton('adminhtml/session')->setRegionsData(null);
      } elseif ( Mage::registry('directory_region_data') ) {
          $form->setValues(Mage::registry('directory_region_data')->getData());
      }
      return parent::_prepareForm();
  }
}