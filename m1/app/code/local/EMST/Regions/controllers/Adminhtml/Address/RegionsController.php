<?php
/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Regions to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_Regions
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
*/

class EMST_Regions_Adminhtml_Address_RegionsController extends Mage_Adminhtml_Controller_Action
{
	protected $_csv_format = array('locale','code','name');
	/**
     * Set active menu and breadcrumbs
     * 
     * @return Oggetto_Regions_controllers_Adminhtml_Address_RegionsController
     */
    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('customer/regions')
            ->_addBreadcrumb(Mage::helper('emst_regions')->__('Regions'), Mage::helper('emst_regions')->__('Manage Regions'));
        return $this;
    }

    /**
     * Index action with grid
     * 
     * @return void
     */
    public function indexAction()
    {
        $this->_initAction();
        $this->renderLayout();
    }

    public function editAction() {
		$id     = $this->getRequest()->getParam('id');
		$locale     = $this->getRequest()->getParam('locale');
		$model  = Mage::getModel('emst_regions/region')->setLocale($locale)->load($id);
		$store = $this->getRequest()->getParam('store');
		if ($model->getId() || $id == 0) {
			$data = $this->_getSession()->getFormData(true);
			if (!empty($data)) {
				$model->setData($data);
			}

			Mage::register('directory_region_data', $model);
			Mage::register('locale_code', $locale);

			$this->loadLayout();
			$this->_setActiveMenu('customer/regions');

			$this->_addBreadcrumb(Mage::helper('emst_regions')->__('Region Manager'), Mage::helper('adminhtml')->__('Region Manager'));
			$this->_addBreadcrumb(Mage::helper('emst_regions')->__('Region News'), Mage::helper('adminhtml')->__('Region News'));

			$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

			$this->_addContent($this->getLayout()->createBlock('emst_regions/adminhtml_regions_edit'))
				->_addLeft($this->getLayout()->createBlock('emst_regions/adminhtml_regions_locale_switcher'))
				->_addLeft($this->getLayout()->createBlock('emst_regions/adminhtml_regions_edit_tabs'))
				;

			$this->renderLayout();
		} else {
			$this->_getSession()->addError(Mage::helper('emst_regions')->__('Region does not exist'));
			$this->_redirect('*/*/index',array('store'=>$store));
		}
	}
 
	public function newAction() {
		$this->_forward('edit');
	}
 
	public function saveNewRegionAction() {
		$data = $this->getRequest()->getParams();
		$store = $this->getRequest()->getParam('store');
		if ($data) {
			try {
				$model = Mage::getModel('emst_regions/region_updater');				
				if(isset($data['id'])) {
					if(!isset($data['locale'])) {
						$data['default_name'] = $data['name'];
						$data['locale'] = Mage::getModel('core/locale')->getDefaultLocale();
					}
					$model->updateRegion($data);
				}			
				else {
					$model->addRegions($data);
				}				
				$this->_getSession()->addSuccess(Mage::helper('emst_regions')->__('Region was successfully saved'));
				$this->_getSession()->setFormData(false);

				if ($this->getRequest()->getParam('back')) {
					$this->_redirect('*/*/edit', array('id' => $model->getId(),'store'=>$store,'locale'=>$data['locale']));
					return;
				}
				$this->_redirect('*/*/index',array('store'=>$store));
				return;
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
                $this->_getSession()->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id'),'store'=>$store,'locale'=>$data['locale']));
                return;
            }
        }
        $this->_getSession()->addError(Mage::helper('emst_regions')->__('Unable to find item to save'));
        $this->_redirect('*/*/');
	}

	
	public function importAction() {		
		$this->loadLayout();
		$this->_setActiveMenu('customer/regions');

		$this->_addBreadcrumb(Mage::helper('emst_regions')->__('Region Manager'), Mage::helper('adminhtml')->__('Import Regions'));

		$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
		$this->_addContent($this->getLayout()->createBlock('emst_regions/adminhtml_regions_import'))
			->_addLeft($this->getLayout()->createBlock('emst_regions/adminhtml_regions_import_tabs'))
			;

		$this->renderLayout();
	}
    /**
     * Upload File and Parse regions
     * 
     * @return void
     */
    public function uploadAction()
    {
		
        try {
            $countryId = $this->getRequest()->getParam('country_id', null);
            if (!$countryId) {
                Mage::throwException($this->__('Please, specify country'));
            }
            // if loaded csv file
            if (isset($_FILES['csvfile']['name']) and (file_exists($_FILES['csvfile']['tmp_name']))) {
                $file = $_FILES['csvfile']['tmp_name'];
                if ($data = $this->_parseCSV($file)) {
                    $regions = Mage::getModel('emst_regions/region_updater')
                        ->setCountryId($countryId)
                        ->addRegions($data);
                    if ($regions) {
                        Mage::getSingleton('adminhtml/session')
                            ->addSuccess($this->__('Total of %d regions have been imported', $regions));
                    } else {
                        Mage::getSingleton('adminhtml/session')->addNotice($this->__('No regions has been imported'));
                    }
                    $this->_redirect('*/*/index');
                    return;
                } else {
                    Mage::throwException($this->__("Can't parse the file!"));
                }
                
            }
        } catch (Mage_Core_Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            $this->_redirect('*/*/import');
            return;
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')
                ->addError($this->__('Something goes wrong! Please, try again later.'));
            Mage::logException($e);
            $this->_redirect('*/*/import');
            return;
        }
    }

        
    /**
     * Delete Region Action
     * 
     * @return void
     */
    public function deleteAction()
    {
        if ($regionId = $this->getRequest()->getParam('id')) {
            try {
                
                Mage::getModel('emst_regions/region_updater')->deleteRegion($regionId);
                Mage::getSingleton('adminhtml/session')
                    ->addSuccess(Mage::helper('adminhtml')->__('The region has been deleted.'));
            } catch (Exception $e){
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        } else {
            Mage::getSingleton('adminhtml/session')
                ->addError(Mage::helper('adminhtml')->__("Region Id is not specified."));
        }
        $this->_redirect('*/*/index');
    }


    /**
     * Product grid for AJAX request.
     * Sort and filter result for example.
     * 
     * @return void
     */
    public function gridAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }


    /**
     * Mass-delete action
     * 
     * @return void
     */
    public function massDeleteAction()
    {
        $regionIds = $this->getRequest()->getParam('region');
        if (!is_array($regionIds)) {
            $this->_getSession()->addError($this->__('Please select region(s).'));
        } else {
            try {
                foreach ($regionIds as $regionId) {
                    Mage::getModel('emst_regions/region_updater')->deleteRegion($regionId);
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d region(s) have been deleted.', count($regionIds))
                );
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

   
    
    /**
     * Parse CSV File to regions
     * 
     * @param string $file file name
     * @return array 
     */
    protected function _parseCSV($file)
    {
        //fix thai symbols csv parsing
        setlocale (LC_ALL, 'en_US.UTF-8');
        $mageCsv = new Varien_File_Csv();
        try {
            $data = $mageCsv->getData($file);
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            return false;
        }
        // get locale codes from first string of file
        array_shift($data);
		
        foreach ($data as $id => $region) {
            if(!is_array($region) || sizeof($region)<3) {
				 Mage::getSingleton('adminhtml/session')->addError($this->__('CSV file is missing some data at line %s.',$id+1));
				return false;
			}
			foreach($region as $indx=>$value) {
				$region[$indx] = trim($value);
			}
			$data[$id] = array_combine($this->_csv_format,$region);
			if (!Zend_Locale::isLocale($data[$id]['locale'])) {
                Mage::getSingleton('adminhtml/session')
                    ->addError($this->__('Invalid locale code: at line %s',$id+1));
                return false;
            }
        }		
        return $data;
    }
	
}