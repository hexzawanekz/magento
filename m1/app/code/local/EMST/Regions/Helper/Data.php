<?php
/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Regions to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_Regions
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
*/

class EMST_Regions_Helper_Data extends Mage_Core_Helper_Abstract
{
	public function getCountiesOptions()
    {
        $items = array();
        $collection = Mage::getResourceModel('directory/country_collection');
        foreach ($collection as $country) {
            $items[$country->getId()] = $country->getName();
        }
        return $items;
    }
	public function getStoreLocale($store) {
		return Mage::getStoreConfig(Mage_Core_Model_Locale::XML_PATH_DEFAULT_LOCALE, $store);
	}
	
}