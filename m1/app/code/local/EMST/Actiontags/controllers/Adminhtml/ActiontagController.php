<?php
class EMST_Actiontags_Adminhtml_ActiontagController extends Mage_Adminhtml_Controller_Action
{
    /**
     * Index Action
     * 
     * @return void
     */
    public function indexAction()
    {
        $this->_title($this->__('System'))->_title($this->__('Action Tags'));

        $this->loadLayout();
        $this->_setActiveMenu('system/actiontags');
		$storeId = $this->getRequest()->getParam('store', 0);
        $actionModel = Mage::getModel('actiontags/action')->getCollection()
						->addFieldToFilter('store_id',$storeId);
        if ($actionModel->count()) {
            $action = $actionModel->getFirstItem();
            Mage::register('actiontag', $action);
        }
        $this->_addContent($this->getLayout()->createBlock('adminhtml/store_switcher'));
        $this->_addContent($this->getLayout()->createBlock('actiontags/adminhtml_system_actions_edit'));
        $this->renderLayout();
    }
    
    /**
     * Save action tags
     * 
     * @return void
     */
    public function saveAction()
    {
        $postData = $this->getRequest()->getPost();
        try {
		
            $actionModel = Mage::getModel('actiontags/action');
			$storeId = $this->getRequest()->getParam('store', 0);			
            if ($actionId = $this->getRequest()->getParam('action_id', 0)) {
                $actionModel->load($actionId);
            } else {
                unset($postData['action_id']);
				$actionModel->load($storeId,'store_id');
            }
			if($actionModel->getStoreId()!=$storeId) {
				unset($postData['action_id']);
				$actionModel->setId(null);
			}
			$postData['store_id'] = $storeId;
            $actionModel->setData($postData)
                        ->save();
            Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Actions have been successfully saved.'));
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            Mage::getSingleton('adminhtml/session')->setRuleData($this->getRequest()->getPost());
            $this->_redirect('*/*/', array('id' => $this->getRequest()->getParam('id'),'store'=>$storeId));
            return;
        }
        $this->_redirect('*/*/',array('id' => $this->getRequest()->getParam('id'),'store'=>$storeId));
    }

}
