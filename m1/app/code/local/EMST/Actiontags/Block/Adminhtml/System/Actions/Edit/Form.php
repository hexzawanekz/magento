<?php
class EMST_Actiontags_Block_Adminhtml_System_Actions_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * Set form fields
     *
     * @return Mage_Adminhtml_Block_Widget_Form
     */
    protected function _prepareForm()
    {

        $form = new Varien_Data_Form();

        $helper = Mage::helper('actiontags');

        $fieldset = $form->addFieldset('base_fieldset',
                                       array(
            'legend' => $helper->__('Subscription')
                ));

        $fieldset->addField('action_id', 'hidden', array(
            'name' => 'action_id',
        ));

        $fieldset->addField('subscription', 'textarea',
                            array(
            'name' => 'subscription',
            'label' => $helper->__('Newsletter Subscriptions'),
            'title' => $helper->__('Newsletter Subscriptions'),
            'rows' => 2,
            'style' => 'height:auto;',
            'after_element_html' => $this->_getNote('{{var customer_email}}, {{var category}}'),
                )
        );

        $fieldset = $form->addFieldset('registration_fieldset',
                                       array(
            'legend' => $helper->__('Registrations')
                ));

        $fieldset->addField('registration', 'textarea',
                            array(
            'name' => 'registration',
            'label' => $helper->__('Execute this tag when anyone registers on the website'),
            'title' => $helper->__('Execute this tag when anyone registers on the website'),
            'rows' => 2,
            'style' => 'height:auto;',
            'after_element_html' => $this->_getNote('{{var customer_id}}, {{var first_name}}, {{var last_name}},' .
                    ' {{var subscribe_status}}, {{var current_date}}, {{var category}}'),
                )
        );

        $fieldset = $form->addFieldset('profileupdate_fieldset', array('legend' => $helper->__('Profile Update')));
        $fieldset->addField('profileupdate', 'textarea',
                            array(
            'name' => 'profileupdate',
            'label' => $helper->__('Execute when user updates their profile'),
            'title' => $helper->__('Execute when user updates their profile'),
            'style' => 'height:auto;',
            'after_element_html' => $this->_getNote('{{var customer_id}}, {{var first_name}}, {{var last_name}},' .
                    ' {{var subscribe_status}}, {{var current_date}}, {{var category}}'),
                )
        );

		$fieldset = $form->addFieldset('addtocart_fieldset', array('legend' => $helper->__('Add to cart(AW module only)')));
        $fieldset->addField('addtocart', 'textarea',
                            array(
            'name' => 'addtocart',
            'label' => $helper->__('Execute when user adds a product to their shopping cart'),
            'title' => $helper->__('Execute when user adds a product to their shopping cart'),
            'style' => 'height:auto;',
            'after_element_html' => $this->_getNote('{{var product_id}}'),
                )
        );


		$fieldset = $form->addFieldset('removefromcart_fieldset', array('legend' => $helper->__('Remove product from cart(AW module only)')));
        $fieldset->addField('removefromcart', 'textarea',
                            array(
            'name' => 'removefromcart',
            'label' => $helper->__('Execute when user remove a product from their shopping cart'),
            'title' => $helper->__('Execute when user remove a product from their shopping cart'),
            'style' => 'height:auto;',
            'after_element_html' => $this->_getNote('{{var product_id}}'),
                )
        );

		$fieldset = $form->addFieldset('cart_fieldset', array('legend' => $helper->__('Shopping Cart')));
        $fieldset->addField('cart', 'textarea',
                            array(
            'name' => 'cart',
            'label' => $helper->__('Execute this at shopping cart page'),
            'title' => $helper->__('Execute this at shopping cart page'),
            'style' => 'height:auto;',
                )
        );

        $fieldset = $form->addFieldset('order_fieldset', array('legend' => $helper->__('Order')));
		$fieldset->addField('order', 'textarea',
                            array(
            'name' => 'order',
            'label' => $helper->__('Execute this when order is submitted'),
            'title' => $helper->__('Execute this when order is submitted'),
            'style' => 'height:auto;',
            'after_element_html' => $this->_getNote('{{var transation_id}}, {{var customer_id}}, {{var store_id}},' .
                    ' {{var gross_billing}}, {{var tax}},{{var current_date}},{{shipping}},{{var shipping_tax}},'.
					"{{var shipping_discount}},{{var payment_method}}, {{var items}} || Items format: { 'sku': 'DD44', 'product_name': 'T-Shirt',qty:'1', 'price': 11.99, 'discount_amount': 1,row_total:1} "),
                )
        );

        $fieldset = $form->addFieldset('checkout_fieldser', array('legend' => $helper->__('Onepage Checkout')));
        $fieldset->addField('checkout', 'textarea',
                            array(
            'name' => 'checkout',
            'label' => $helper->__('Execute this on onepage checkout page'),
            'title' => $helper->__('Execute this on onepage checkout page'),
            'style' => 'height:auto;',
            'after_element_html' => $this->_getNote('{{var transation_id}}, {{var customer_id}}, {{var store_id}},' .
                    ' {{var gross_billing}}, {{var tax}},{{var current_date}},{{var shipping}},{{var shipping_tax}},'.
					"{{var shipping_discount}},{{var payment_method}}, {{var items}} || Items format: { 'sku': 'DD44', 'name': 'T-Shirt', 'category': 'Apparel', 'price': 11.99, 'quantity': 1} "),
                )
        );

        $fieldset = $form->addFieldset('checkout_success_head_fieldset', array('legend' => $helper->__('Onepage Checkout Success (before &lt;head&gt; tag)')));
        $fieldset->addField('checkout_success_head', 'textarea',
            array(
                'name' => 'checkout_success_head',
                'label' => $helper->__('Execute this on onepage checkout success page, in <head> tag'),
                'title' => $helper->__('Execute this on onepage checkout success page, in <head> tag'),
                'style' => 'height:auto;',
            )
        );

        $fieldset = $form->addFieldset('checkout_success_fieldset', array('legend' => $helper->__('Onepage Checkout Success (after &lt;body&gt; tag)')));
        $fieldset->addField('checkout_success', 'textarea',
            array(
                'name' => 'checkout_success',
                'label' => $helper->__('Execute this on onepage checkout success page, after <body> tag starts'),
                'title' => $helper->__('Execute this on onepage checkout success page, after <body> tag starts'),
                'style' => 'height:auto;',
            )
        );

        $fieldset = $form->addFieldset('criteo_id_fieldset', array('legend' => $helper->__('Criteo Account ID')));
        $fieldset->addField('criteo_id', 'text',
                            array(
            'name' => 'criteo_id',
            'label' => $helper->__('Account ID for handling Criteo tracking Script'),
            'title' => $helper->__('Account ID for handling Criteo tracking Script')
			)
		);

		$fieldset = $form->addFieldset('header_fieldset', array('legend' => $helper->__('Header(after &lt;body&gt; tag)')));
        $fieldset->addField('header', 'textarea',
                            array(
            'name' => 'header',
            'label' => $helper->__('Execute this tag every time header is loaded'),
            'title' => $helper->__('Execute this tag every time header is loaded'),
            'rows' => 2,
            'style' => 'height:auto;',
            'after_element_html' => $this->_getNote('{{var customer_id}}, {{var customer_email}},' .
                    ' {{var first_name}}, {{var last_name}}, {{var subscribe_status}}, ' .
                    '{{var order_id}}, {{var store_id}}, {{var current_date}}, {{var category}}'),
                )
        );

        $fieldset = $form->addFieldset('footer_fieldset', array('legend' => $helper->__('Footer(before &lt;/body&gt; tag)')));
        $fieldset->addField('footer', 'textarea',
                            array(
            'name' => 'footer',
            'label' => $helper->__('Execute this tag everytime footer is loaded'),
            'title' => $helper->__('Execute this tag everytime footer is loaded'),
            'rows' => 2,
            'style' => 'height:auto;',
            'after_element_html' => $this->_getNote('{{var customer_id}}, {{var customer_email}},' .
                    ' {{var first_name}}, {{var last_name}}, {{var subscribe_status}},' .
                    ' {{var order_id}}, {{var store_id}}, {{var current_date}}, {{var category}}'),
                )
        );
        if (Mage::registry('actiontag')) {
            $form->setValues(Mage::registry('actiontag'));
            $form->getElement('action_id')->setValue(Mage::registry('actiontag')->getId());
        }
        $form->setAction($this->getUrl('*/*/save',array('id' => $this->getRequest()->getParam('id'),
		'store'=>$this->getRequest()->getParam('store'))));
        $form->setMethod('post');
        $form->setUseContainer(true);
        $form->setId('edit_form');

        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Note text prepare
     *
     * @param string $text text
     * @return string
     */
    protected function _getNote($text)
    {
        return '<p class="note"><span>' . $this->__('Available params:') . htmlentities($text) . '</span></p>';
    }
}
