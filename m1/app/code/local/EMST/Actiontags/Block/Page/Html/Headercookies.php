<?php
class EMST_Actiontags_Block_Page_Html_Headercookies extends Mage_Core_Block_Template
{

    /**
     * Adding header script
     *
     * @param string $html block html
     * @return string
     */
    protected function _afterToHtml($html)
    {
        $html .= $this->_getActionTags();
        return $html;
    }

    /**
     * Get action tags html
     *
     * @return <string>
     */
    protected function _getActionTags()
    {
        $result = '';
        $actions = Mage::getModel('actiontags/action')->getConfiguration();
        if ($actions) {
            foreach (array_keys($actions->getData()) as $actionName) {
                if ($content = $this->_getCookie()->get($actionName)) {
                    if ($content == '1') {
                        $content = null;
                    }
                    $result .= Mage::helper('actiontags')->getHtmlForAction($actionName, urldecode($content));
                    $this->_getCookie()->delete($actionName);
                }
            }
        }
        return $result;
    }

    /**
     * Get cookie model
     *
     * @return <Mage_Core_Model_Cookie>
     */
    protected function _getCookie()
    {
        return Mage::getModel('core/cookie');
    }

    /**
     * Cache for header block
     *
     * @return bool
     */
    protected function _loadCache()
    {
        return false;
    }

}
