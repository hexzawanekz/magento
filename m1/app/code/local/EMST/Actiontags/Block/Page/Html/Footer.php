<?php
class EMST_Actiontags_Block_Page_Html_Footer extends Mage_Page_Block_Html_Footer
{
    /**
     * Constructor
     *
     * @return void
     */
    protected function _construct()
    {
        $this->addData(array(
            'cache_lifetime' => false,
            'cache_tags'     => array(Mage_Core_Model_Store::CACHE_TAG, Mage_Cms_Model_Block::CACHE_TAG),
            'cache_key' => 'BLOCK_TPL' .
                Mage::app()->getStore()->getCode() .
                $this->getTemplateFile() .
                $this->getRequest()->isSecure() .
                (isset($_COOKIE['nomobile'])
            )
        ));
    }

    /**
     * Adding footer script
     *
     * @param string $html block html
     *
     * @return string
     */
    protected function _afterToHtml($html)
    {
        $script = Mage::helper('actiontags')->getHtmlForAction('footer');
        $html .= $script;
        return $html;
    }
    
}
