<?php
class EMST_Actiontags_Block_Page_Html_Header extends Mage_Core_Block_Template
{

    /**
     * Adding header script
     *
     * @param string $html block html 
     * @return string
     */
    protected function _afterToHtml($html)
    {
        $html .= Mage::helper('actiontags')->getHtmlForAction('header');
        return $html;
    }

    /**
     * Cache for header block
     *
     * @return bool
     */
    protected function _loadCache()
    {
        return false;
    }

}
