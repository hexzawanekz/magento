<?php

/** @var Mage_Core_Model_Resource_Setup $this */
$this->startSetup();
/**Default Store**/

/** @var Mage_Core_Model_Store $coreStoreModel */
$coreStoreModel = Mage::getModel('core/store');
$storeId = 0;

/** @var EMST_Actiontags_Model_Mysql4_Action_Collection $actionTagModel */
$actionTagModel = Mage::getModel('actiontags/action')->getCollection();

$actionItem = $actionTagModel->addFieldToFilter('store_id', $storeId)->getFirstItem();

$actionItem = Mage::getModel('actiontags/action')->load($actionItem->getId());

$code = <<<HTML
<!--NeverBlue-->
<script type="text/javascript">
function addNeverBlue() {
	var generated = 1;
	var order_id = '{{var order_id}}';
	var neverblueScript = "http://cjsab.com/p.ashx?o=36693&e=696&f=js&p=SALES_AMT&t="+order_id;

    if(generated == 1) {
        var s = document.createElement("script");
        s.type = "text/javascript";
        s.src = neverblueScript;
        jQuery("head").append(s);

        generated = 0;
    }
}
</script>
<!--End NeverBlue-->
<!-- AccessTrade tracking code-->
<script type="text/javascript">
function addAccesstrade() {
	var generated = 1;
	var transaction = '{{var order_id}}';
	var accessTradeItems = '{{var accessTradeItems}}';
	var accesstrade = "https://cv.accesstrade.in.th/cv.php?identifier=" + transaction + "&mcn=d645920e395fedad7bbbed0eca3fe2e0&result_id=3" + accessTradeItems;
		if(generated == 1) {
			jQuery('<img/>', {
				src: accesstrade,
				width: 1,
				height: 1,
			}).appendTo('body');
			generated = 0;
		}
}
</script>
<!-- End AccessTrade tracking code-->
<!-- GA E-Commerce Tracking (via GTM) -->
<script>
    dataLayer = [{
        'transactionId': '{{var transation_id}}',
        'transactionAffiliation': 'Petloft EN Store',
        'transactionTotal': {{var gross_billing}},
        'transactionTax': {{var tax}},
        'transactionShipping': {{var shipping}},
        'transactionProducts': {{var items}}
    }];
</script>
<!-- End GA E-Commerce Tracking (via GTM) -->

<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-T3ZMK9"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-T3ZMK9');</script>
<!-- End Google Tag Manager -->

<script type="text/javascript">
	document.observe('dom:loaded',function() {
		var btn = $$('#checkout-review-submit button').first();
		var utm_medium = "{{var utm_medium}}";
		if(!btn) {
			btn = $$('button[onclick ^= "checkout.save"]').first();
		}
		if(!btn) {
			return;
		}
		btn.setAttribute('onclick',"checkout.save();dataLayer.push({'event': 'placeOrder'});addCriteoCode();");

		if( utm_medium == 'neverblue' ) {
            btn.setAttribute('onclick',"checkout.save();dataLayer.push({'event': 'placeOrder'});addNeverBlue();addCriteoCode();");
        } else if(utm_medium == 'interspace') {
            btn.setAttribute('onclick',"checkout.save();dataLayer.push({'event': 'placeOrder'});addAccesstrade();addCriteoCode();");
        }
	});
</script>
<!-- Conversion Tracking -->
<script type="text/javascript">
dataLayer.push({'conversionValue':{{var gross_billing}}});
</script>
<!-- End Conversion Tracking -->

HTML;
$actionItem->setCheckout($code);
$actionItem->save();
/*********************************END DEFAULT STORE***********************************/

/*********************************PETLOFT EN***********************************/
/** @var Mage_Core_Model_Store $coreStoreModel */
$coreStoreModel = Mage::getModel('core/store');
$storeId = $coreStoreModel->load('en')->getId();

/** @var EMST_Actiontags_Model_Mysql4_Action_Collection $actionTagModel */
$actionTagModel = Mage::getModel('actiontags/action')->getCollection();

$actionItem = $actionTagModel->addFieldToFilter('store_id', $storeId)->getFirstItem();

$actionItem = Mage::getModel('actiontags/action')->load($actionItem->getId());

$code = <<<HTML
<!--NeverBlue-->
<script type="text/javascript">
function addNeverBlue() {
	var generated = 1;
	var order_id = '{{var order_id}}';
	var neverblueScript = "http://cjsab.com/p.ashx?o=36693&e=696&f=js&p=SALES_AMT&t="+order_id;

    if(generated == 1) {
        var s = document.createElement("script");
        s.type = "text/javascript";
        s.src = neverblueScript;
        jQuery("head").append(s);

        generated = 0;
    }
}
</script>
<!--End NeverBlue-->
<!-- AccessTrade tracking code-->
<script type="text/javascript">
function addAccesstrade() {
	var generated = 1;
	var transaction = '{{var order_id}}';
	var accessTradeItems = '{{var accessTradeItems}}';
	var accesstrade = "https://cv.accesstrade.in.th/cv.php?identifier=" + transaction + "&mcn=d645920e395fedad7bbbed0eca3fe2e0&result_id=3" + accessTradeItems;
		if(generated == 1) {
			jQuery('<img/>', {
				src: accesstrade,
				width: 1,
				height: 1,
			}).appendTo('body');
			generated = 0;
		}
}
</script>
<!-- End AccessTrade tracking code-->
<!-- GA E-Commerce Tracking (via GTM) -->
<script>
    dataLayer = [{
        'transactionId': '{{var transation_id}}',
        'transactionAffiliation': 'Petloft EN Store',
        'transactionTotal': {{var gross_billing}},
        'transactionTax': {{var tax}},
        'transactionShipping': {{var shipping}},
        'transactionProducts': {{var items}}
    }];
</script>
<!-- End GA E-Commerce Tracking (via GTM) -->

<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-T3ZMK9"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-T3ZMK9');</script>
<!-- End Google Tag Manager -->

<script type="text/javascript">
	document.observe('dom:loaded',function() {
		var btn = $$('#checkout-review-submit button').first();
		var utm_medium = "{{var utm_medium}}";
		if(!btn) {
			btn = $$('button[onclick ^= "checkout.save"]').first();
		}
		if(!btn) {
			return;
		}
		btn.setAttribute('onclick',"checkout.save();dataLayer.push({'event': 'placeOrder'});addCriteoCode();");

		//Fire code Neverblue
		if(utm_medium == 'neverblue') {
			btn.setAttribute('onclick',"checkout.save();dataLayer.push({'event': 'placeOrder'});addNeverBlue();addCriteoCode();");
		} else if(utm_medium == 'interspace') {
            btn.setAttribute('onclick',"checkout.save();dataLayer.push({'event': 'placeOrder'});addAccesstrade();addCriteoCode();");
		}
	});
</script>
<!-- Conversion Tracking -->
<script type="text/javascript">
dataLayer.push({'conversionValue':{{var gross_billing}}});
</script>
<!-- End Conversion Tracking -->
HTML;
$actionItem->setCheckout($code);
$actionItem->save();
/*********************************END PETLOFT EN***********************************/

/*********************************PETLOFT TH***********************************/
/** @var Mage_Core_Model_Store $coreStoreModel */
$coreStoreModel = Mage::getModel('core/store');
$storeId = $coreStoreModel->load('th')->getId();

/** @var EMST_Actiontags_Model_Mysql4_Action_Collection $actionTagModel */
$actionTagModel = Mage::getModel('actiontags/action')->getCollection();

$actionItem = $actionTagModel->addFieldToFilter('store_id', $storeId)->getFirstItem();

$actionItem = Mage::getModel('actiontags/action')->load($actionItem->getId());

$code = <<<HTML
<!--NeverBlue-->
<script type="text/javascript">
function addNeverBlue() {
	var generated = 1;
	var order_id = '{{var order_id}}';
	var neverblueScript = "http://cjsab.com/p.ashx?o=36693&e=696&f=js&p=SALES_AMT&t="+order_id;

    if(generated == 1) {
        var s = document.createElement("script");
        s.type = "text/javascript";
        s.src = neverblueScript;
        jQuery("head").append(s);

        generated = 0;
    }
}
</script>
<!--End NeverBlue-->
<!-- AccessTrade tracking code-->
<script type="text/javascript">
function addAccesstrade() {
	var generated = 1;
	var transaction = '{{var order_id}}';
	var accessTradeItems = '{{var accessTradeItems}}';
	var accesstrade = "https://cv.accesstrade.in.th/cv.php?identifier=" + transaction + "&mcn=d645920e395fedad7bbbed0eca3fe2e0&result_id=3" + accessTradeItems;
		if(generated == 1) {
			jQuery('<img/>', {
				src: accesstrade,
				width: 1,
				height: 1,
			}).appendTo('body');
			generated = 0;
		}
}
</script>
<!-- End AccessTrade tracking code-->
<!-- GA E-Commerce Tracking (via GTM) -->
<script>
    dataLayer = [{
        'transactionId': '{{var transation_id}}',
        'transactionAffiliation': 'Petloft TH Store',
        'transactionTotal': {{var gross_billing}},
        'transactionTax': {{var tax}},
        'transactionShipping': {{var shipping}},
        'transactionProducts': {{var items}}
    }];
</script>
<!-- End GA E-Commerce Tracking (via GTM) -->

<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-T3ZMK9"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-T3ZMK9');</script>
<!-- End Google Tag Manager -->

<script type="text/javascript">
	document.observe('dom:loaded',function() {
		var btn = $$('#checkout-review-submit button').first();
		var utm_medium = "{{var utm_medium}}";
		if(!btn) {
			btn = $$('button[onclick ^= "checkout.save"]').first();
		}
		if(!btn) {
			return;
		}
		btn.setAttribute('onclick',"checkout.save();dataLayer.push({'event': 'placeOrder'});addCriteoCode();");

		//Fire code Neverblue
		if(utm_medium == 'neverblue') {
			btn.setAttribute('onclick',"checkout.save();dataLayer.push({'event': 'placeOrder'});addNeverBlue();addCriteoCode();");
		} else if(utm_medium == 'interspace') {
            btn.setAttribute('onclick',"checkout.save();dataLayer.push({'event': 'placeOrder'});addAccesstrade();addCriteoCode();");
		}
	});
</script>
<!-- Conversion Tracking -->
<script type="text/javascript">
dataLayer.push({'conversionValue':{{var gross_billing}}});
</script>
<!-- End Conversion Tracking -->

HTML;
$actionItem->setCheckout($code);
$actionItem->save();
/*********************************END PETLOFT TH***********************************/

/*********************************PETLOFT TH***********************************/
/** @var Mage_Core_Model_Store $coreStoreModel */
$coreStoreModel = Mage::getModel('core/store');
$storeId = $coreStoreModel->load('pm')->getId();

/** @var EMST_Actiontags_Model_Mysql4_Action_Collection $actionTagModel */
$actionTagModel = Mage::getModel('actiontags/action')->getCollection();

$actionItem = $actionTagModel->addFieldToFilter('store_id', $storeId)->getFirstItem();

$actionItem = Mage::getModel('actiontags/action')->load($actionItem->getId());

$code = <<<HTML
<!--NeverBlue-->
<script type="text/javascript">
function addNeverBlue() {
	var generated = 1;
	var order_id = '{{var order_id}}';
	var neverblueScript = "http://cjsab.com/p.ashx?o=36693&e=696&f=js&p=SALES_AMT&t="+order_id;

    if(generated == 1) {
        var s = document.createElement("script");
        s.type = "text/javascript";
        s.src = neverblueScript;
        jQuery("head").append(s);

        generated = 0;
    }
}
</script>
<!--End NeverBlue-->
<!-- AccessTrade tracking code-->
<script type="text/javascript">
function addAccesstrade() {
	var generated = 1;
	var transaction = '{{var order_id}}';
	var accessTradeItems = '{{var accessTradeItems}}';
	var accesstrade = "https://cv.accesstrade.in.th/cv.php?identifier=" + transaction + "&mcn=d645920e395fedad7bbbed0eca3fe2e0&result_id=3" + accessTradeItems;
		if(generated == 1) {
			jQuery('<img/>', {
				src: accesstrade,
				width: 1,
				height: 1,
			}).appendTo('body');
			generated = 0;
		}
}
</script>
<!-- End AccessTrade tracking code-->
<!-- GA E-Commerce Tracking (via GTM) -->
<script>
    dataLayer = [{
        'transactionId': '{{var transation_id}}',
        'transactionAffiliation': 'Petloft Mobile Store',
        'transactionTotal': {{var gross_billing}},
        'transactionTax': {{var tax}},
        'transactionShipping': {{var shipping}},
        'transactionProducts': {{var items}}
    }];
</script>
<!-- End GA E-Commerce Tracking (via GTM) -->

<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-T3ZMK9"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-T3ZMK9');</script>
<!-- End Google Tag Manager -->

<script type="text/javascript">
	document.observe('dom:loaded',function() {
		var btn = $$('#checkout-review-submit button').first();
		var utm_medium = "{{var utm_medium}}";
		if(!btn) {
			btn = $$('button[onclick ^= "checkout.save"]').first();
		}
		if(!btn) {
			return;
		}
		btn.setAttribute('onclick',"checkout.save();dataLayer.push({'event': 'placeOrder'});addCriteoCode();");

		//Fire code Neverblue
		if(utm_medium == 'neverblue') {
			btn.setAttribute('onclick',"checkout.save();dataLayer.push({'event': 'placeOrder'});addNeverBlue();addCriteoCode();");
		} else if(utm_medium == 'interspace') {
		    btn.setAttribute('onclick',"checkout.save();dataLayer.push({'event': 'placeOrder'});addAccesstrade();addCriteoCode();");
		}
	});
</script>
<!-- Conversion Tracking -->
<script type="text/javascript">
dataLayer.push({'conversionValue':{{var gross_billing}}});
</script>
<!-- End Conversion Tracking -->

HTML;
$actionItem->setCheckout($code);
$actionItem->save();
/*********************************END PETLOFT TH***********************************/

/*********************************LEFEMA EN***********************************/
/** @var Mage_Core_Model_Store $coreStoreModel */
$coreStoreModel = Mage::getModel('core/store');
$storeId = $coreStoreModel->load('len')->getId();

/** @var EMST_Actiontags_Model_Mysql4_Action_Collection $actionTagModel */
$actionTagModel = Mage::getModel('actiontags/action')->getCollection();

$actionItem = $actionTagModel->addFieldToFilter('store_id', $storeId)->getFirstItem();

$actionItem = Mage::getModel('actiontags/action')->load($actionItem->getId());

$code = <<<HTML
<!--NeverBlue-->
<script type="text/javascript">
function addNeverBlue() {
	var generated = 1;
	var order_id = '{{var order_id}}';
	var neverblueScript = "http://cjsab.com/p.ashx?o=36696&e=696&f=js&p=SALES_AMT&t="+order_id;

    if(generated == 1) {
        var s = document.createElement("script");
        s.type = "text/javascript";
        s.src = neverblueScript;
        jQuery("head").append(s);

        generated = 0;
    }
}
</script>
<!--End NeverBlue-->

<!-- AccessTrade tracking code-->
<script type="text/javascript">
function addAccesstrade() {
	var generated = 1;
	var transaction = '{{var transation_id}}';
	var accessTradeItems = '{{var accessTradeItems}}';
	var accesstrade = "https://cv.accesstrade.in.th/cv.php?identifier=" + transaction + "&mcn=d645920e395fedad7bbbed0eca3fe2e0&result_id=3" + accessTradeItems;
		if(generated == 1) {
			jQuery('<img/>', {
				src: accesstrade,
				width: 1,
				height: 1,
			}).appendTo('body');
			generated = 0;
		}
}
</script>
<!-- End AccessTrade tracking code-->

<!-- GA E-Commerce Tracking (via GTM) -->
<script>
    dataLayer = [{
        'transactionId': '{{var transation_id}}',
        'transactionAffiliation': 'Lafema EN Store',
        'transactionTotal': {{var gross_billing}},
        'transactionTax': {{var tax}},
        'transactionShipping': {{var shipping}},
        'transactionProducts': {{var items}}
    }];
</script>
<!-- End GA E-Commerce Tracking (via GTM) -->

<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-T3ZMK9"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-T3ZMK9');</script>
<!-- End Google Tag Manager -->

<script type="text/javascript">
	document.observe('dom:loaded',function() {
		var btn = $$('#checkout-review-submit button').first();
		var utm_medium = "{{var utm_medium}}";
		if(!btn) {
			btn = $$('button[onclick ^= "checkout.save"]').first();
		}
		if(!btn) {
			return;
		}
		btn.setAttribute('onclick',"checkout.save();dataLayer.push({'event': 'placeOrder'});addCriteoCode()");

		if(utm_medium == 'neverblue') {
			btn.setAttribute('onclick',"checkout.save();dataLayer.push({'event': 'placeOrder'});addNeverBlue();addCriteoCode()");
		} else if(utm_medium == 'interspace') {
		    btn.setAttribute('onclick',"checkout.save();dataLayer.push({'event': 'placeOrder'});addAccesstrade();addCriteoCode()");
		}
	});
</script>

<!-- Conversion Tracking -->
<script type="text/javascript">
dataLayer.push({'conversionValue':{{var gross_billing}}});
</script>
<!-- End Conversion Tracking -->
HTML;
$actionItem->setCheckout($code);
$actionItem->save();
/*********************************END LAFEMA EN***********************************/

/*********************************LEFEMA TH***********************************/
/** @var Mage_Core_Model_Store $coreStoreModel */
$coreStoreModel = Mage::getModel('core/store');
$storeId = $coreStoreModel->load('lth')->getId();

/** @var EMST_Actiontags_Model_Mysql4_Action_Collection $actionTagModel */
$actionTagModel = Mage::getModel('actiontags/action')->getCollection();

$actionItem = $actionTagModel->addFieldToFilter('store_id', $storeId)->getFirstItem();

$actionItem = Mage::getModel('actiontags/action')->load($actionItem->getId());

$code = <<<HTML
<!--NeverBlue-->
<script type="text/javascript">
function addNeverBlue() {
	var generated = 1;
	var order_id = '{{var order_id}}';
	var neverblueScript = "http://cjsab.com/p.ashx?o=36693&e=696&f=js&p=SALES_AMT&t="+order_id;

    if(generated == 1) {
        var s = document.createElement("script");
        s.type = "text/javascript";
        s.src = neverblueScript;
        jQuery("head").append(s);

        generated = 0;
    }
}
</script>
<!--End NeverBlue-->
<!-- AccessTrade tracking code-->
<script type="text/javascript">
function addAccesstrade() {
	var generated = 1;
	var transaction = '{{var transation_id}}';
	var accessTradeItems = '{{var accessTradeItems}}';
	var accesstrade = "https://cv.accesstrade.in.th/cv.php?identifier=" + transaction + "&mcn=d645920e395fedad7bbbed0eca3fe2e0&result_id=3" + accessTradeItems;
		if(generated == 1) {
			jQuery('<img/>', {
				src: accesstrade,
				width: 1,
				height: 1,
			}).appendTo('body');
			generated = 0;
		}
}
</script>
<!-- End AccessTrade tracking code-->

<!-- GA E-Commerce Tracking (via GTM) -->
<script>
    dataLayer = [{
        'transactionId': '{{var transation_id}}',
        'transactionAffiliation': 'Lafema TH Store',
        'transactionTotal': {{var gross_billing}},
        'transactionTax': {{var tax}},
        'transactionShipping': {{var shipping}},
        'transactionProducts': {{var items}}
    }];
</script>
<!-- End GA E-Commerce Tracking (via GTM) -->

<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-T3ZMK9"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-T3ZMK9');</script>
<!-- End Google Tag Manager -->

<script type="text/javascript">
	document.observe('dom:loaded',function() {
		var btn = $$('#checkout-review-submit button').first();
		var utm_medium = "{{var utm_medium}}";
		if(!btn) {
			btn = $$('button[onclick ^= "checkout.save"]').first();
		}
		if(!btn) {
			return;
		}
		btn.setAttribute('onclick',"checkout.save();dataLayer.push({'event': 'placeOrder'});addCriteoCode()");
		if(utm_medium == 'neverblue') {
			btn.setAttribute('onclick',"checkout.save();dataLayer.push({'event': 'placeOrder'});addNeverBlue();addCriteoCode()");
		} else if (utm_medium == 'interspace') {
		    btn.setAttribute('onclick',"checkout.save();dataLayer.push({'event': 'placeOrder'});addAccesstrade();addCriteoCode()");
		}
	});
</script>

<!-- Conversion Tracking -->
<script type="text/javascript">
dataLayer.push({'conversionValue':{{var gross_billing}}});
</script>
<!-- End Conversion Tracking -->
HTML;
$actionItem->setCheckout($code);
$actionItem->save();
/*********************************END LAFEMA TH***********************************/

/*********************************LEFEMA MOBILE***********************************/
/** @var Mage_Core_Model_Store $coreStoreModel */
$coreStoreModel = Mage::getModel('core/store');
$storeId = $coreStoreModel->load('lm')->getId();

/** @var EMST_Actiontags_Model_Mysql4_Action_Collection $actionTagModel */
$actionTagModel = Mage::getModel('actiontags/action')->getCollection();

$actionItem = $actionTagModel->addFieldToFilter('store_id', $storeId)->getFirstItem();

$actionItem = Mage::getModel('actiontags/action')->load($actionItem->getId());

$code = <<<HTML
<!--NeverBlue-->
<script type="text/javascript">
function addNeverBlue() {
	var generated = 1;
	var order_id = '{{var order_id}}';
	var neverblueScript = "http://cjsab.com/p.ashx?o=36693&e=696&f=js&p=SALES_AMT&t="+order_id;

    if(generated == 1) {
        var s = document.createElement("script");
        s.type = "text/javascript";
        s.src = neverblueScript;
        jQuery("head").append(s);

        generated = 0;
    }
}
</script>
<!--End NeverBlue-->
<!-- AccessTrade tracking code-->
<script type="text/javascript">
function addAccesstrade() {
	var generated = 1;
	var transaction = '{{var transation_id}}';
	var accessTradeItems = '{{var accessTradeItems}}';
	var accesstrade = "https://cv.accesstrade.in.th/cv.php?identifier=" + transaction + "&mcn=17e62166fc8586dfa4d1bc0e1742c08b&result_id=3" + accessTradeItems;
		if(generated == 1) {
			jQuery('<img/>', {
				src: accesstrade,
				width: 1,
				height: 1,
			}).appendTo('body');
			generated = 0;
		}
}
</script>
<!-- End AccessTrade tracking code-->

<!-- GA E-Commerce Tracking (via GTM) -->
<script>
    dataLayer = [{
        'transactionId': '{{var transation_id}}',
        'transactionAffiliation': 'Lafema Mobile Store',
        'transactionTotal': {{var gross_billing}},
        'transactionTax': {{var tax}},
        'transactionShipping': {{var shipping}},
        'transactionProducts': {{var items}}
    }];
</script>
<!-- End GA E-Commerce Tracking (via GTM) -->

<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-T3ZMK9"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-T3ZMK9');</script>
<!-- End Google Tag Manager -->

<script type="text/javascript">
	document.observe('dom:loaded',function() {
		var btn = $$('#checkout-review-submit button').first();
		var utm_medium = "{{var utm_medium}}";
		if(!btn) {
			btn = $$('button[onclick ^= "checkout.save"]').first();
		}
		if(!btn) {
			return;
		}
		btn.setAttribute('onclick',"checkout.save();dataLayer.push({'event': 'placeOrder'});");

		if(utm_medium == 'neverblue') {
			btn.setAttribute('onclick',"checkout.save();dataLayer.push({'event': 'placeOrder'});addNeverBlue()");
		} else if (utm_medium == 'interspace') {
		    btn.setAttribute('onclick',"checkout.save();dataLayer.push({'event': 'placeOrder'});addAccesstrade()");
		}
	});
</script>

<!-- Conversion Tracking -->
<script type="text/javascript">
dataLayer.push({'conversionValue':{{var gross_billing}}});
</script>
<!-- End Conversion Tracking -->
HTML;
$actionItem->setCheckout($code);
$actionItem->save();
/*********************************END LAFEMA MOBILE***********************************/

/*********************************Moxy EN***********************************/
/** @var Mage_Core_Model_Store $coreStoreModel */
$coreStoreModel = Mage::getModel('core/store');
$storeId = $coreStoreModel->load('moxyen')->getId();

/** @var EMST_Actiontags_Model_Mysql4_Action_Collection $actionTagModel */
$actionTagModel = Mage::getModel('actiontags/action')->getCollection();

$actionItem = $actionTagModel->addFieldToFilter('store_id', $storeId)->getFirstItem();

$actionItem = Mage::getModel('actiontags/action')->load($actionItem->getId());

$code = <<<HTML
<!--NeverBlue-->
<script type="text/javascript">
function addNeverBlue() {
	var generated = 1;
	var order_id = '{{var order_id}}';
	var neverblueScript = "http://cjsab.com/p.ashx?o=36693&e=696&f=js&p=SALES_AMT&t="+order_id;

    if(generated == 1) {
        var s = document.createElement("script");
        s.type = "text/javascript";
        s.src = neverblueScript;
        jQuery("head").append(s);

        generated = 0;
    }
}
</script>
<!--End NeverBlue-->
<!-- AccessTrade tracking code-->
<script type="text/javascript">
function addAccesstrade() {
	var generated = 1;
	var transaction = '{{var transation_id}}';
	var accessTradeItems = '{{var accessTradeItems}}';
	var accesstrade = "https://cv.accesstrade.in.th/cv.php?identifier=" + transaction + "&mcn=d645920e395fedad7bbbed0eca3fe2e0&result_id=3" + accessTradeItems;
		if(generated == 1) {
			jQuery('<img/>', {
				src: accesstrade,
				width: 1,
				height: 1,
			}).appendTo('body');
			generated = 0;
		}
}
</script>
<!-- End AccessTrade tracking code-->

<!-- GA E-Commerce Tracking (via GTM) -->
<script>
    dataLayer = [{
        'transactionId': '{{var transation_id}}',
        'transactionAffiliation': 'Moxy EN Store',
        'transactionTotal': {{var gross_billing}},
        'transactionTax': {{var tax}},
        'transactionShipping': {{var shipping}},
        'transactionProducts': {{var items}}
    }];
</script>
<!-- End GA E-Commerce Tracking (via GTM) -->

<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-T3ZMK9"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-T3ZMK9');</script>
<!-- End Google Tag Manager -->

<script type="text/javascript">
	document.observe('dom:loaded',function() {
		var btn = $$('#checkout-review-submit button').first();
		var utm_medium = "{{var utm_medium}}";
		if(!btn) {
			btn = $$('button[onclick ^= "checkout.save"]').first();
		}
		if(!btn) {
			return;
		}
		btn.setAttribute('onclick',"checkout.save();dataLayer.push({'event': 'placeOrder'});addCriteoCode()");
		if(utm_medium == 'neverblue') {
			btn.setAttribute('onclick',"checkout.save();dataLayer.push({'event': 'placeOrder'});addNeverBlue();addCriteoCode()");
		} else if(utm_medium == 'interspace') {
		    btn.setAttribute('onclick',"checkout.save();dataLayer.push({'event': 'placeOrder'});addAccesstrade();addCriteoCode()");
		}
	});
</script>

<!-- Conversion Tracking -->
<script type="text/javascript">
dataLayer.push({'conversionValue':{{var gross_billing}}});
</script>
<!-- End Conversion Tracking -->
HTML;
$actionItem->setCheckout($code);
$actionItem->setCriteoId(21453);
$actionItem->save();
/*********************************END MOXY EN***********************************/

/*********************************Moxy TH***********************************/
/** @var Mage_Core_Model_Store $coreStoreModel */
$coreStoreModel = Mage::getModel('core/store');
$storeId = $coreStoreModel->load('moxyth')->getId();

/** @var EMST_Actiontags_Model_Mysql4_Action_Collection $actionTagModel */
$actionTagModel = Mage::getModel('actiontags/action')->getCollection();

$actionItem = $actionTagModel->addFieldToFilter('store_id', $storeId)->getFirstItem();

$actionItem = Mage::getModel('actiontags/action')->load($actionItem->getId());

$code = <<<HTML
<!--NeverBlue-->
<script type="text/javascript">
function addNeverBlue() {
	var generated = 1;
	var order_id = '{{var order_id}}';
	var neverblueScript = "http://cjsab.com/p.ashx?o=36693&e=696&f=js&p=SALES_AMT&t="+order_id;

    if(generated == 1) {
        var s = document.createElement("script");
        s.type = "text/javascript";
        s.src = neverblueScript;
        jQuery("head").append(s);

        generated = 0;
    }
}
</script>
<!--End NeverBlue-->
<!-- AccessTrade tracking code-->
<script type="text/javascript">
function addAccesstrade() {
	var generated = 1;
	var transaction = '{{var transation_id}}';
	var accessTradeItems = '{{var accessTradeItems}}';
	var accesstrade = "https://cv.accesstrade.in.th/cv.php?identifier=" + transaction + "&mcn=d645920e395fedad7bbbed0eca3fe2e0&result_id=3" + accessTradeItems;
		if(generated == 1) {
			jQuery('<img/>', {
				src: accesstrade,
				width: 1,
				height: 1,
			}).appendTo('body');
			generated = 0;
		}
}
</script>
<!-- End AccessTrade tracking code-->

<!-- GA E-Commerce Tracking (via GTM) -->
<script>
    dataLayer = [{
        'transactionId': '{{var transation_id}}',
        'transactionAffiliation': 'Moxy TH Store',
        'transactionTotal': {{var gross_billing}},
        'transactionTax': {{var tax}},
        'transactionShipping': {{var shipping}},
        'transactionProducts': {{var items}}
    }];
</script>
<!-- End GA E-Commerce Tracking (via GTM) -->

<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-T3ZMK9"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-T3ZMK9');</script>
<!-- End Google Tag Manager -->

<script type="text/javascript">
	document.observe('dom:loaded',function() {
		var btn = $$('#checkout-review-submit button').first();
		var utm_medium = "{{var utm_medium}}";
		if(!btn) {
			btn = $$('button[onclick ^= "checkout.save"]').first();
		}
		if(!btn) {
			return;
		}
		btn.setAttribute('onclick',"checkout.save();dataLayer.push({'event': 'placeOrder'});addCriteoCode()");
		if(utm_medium == 'neverblue') {
			btn.setAttribute('onclick',"checkout.save();dataLayer.push({'event': 'placeOrder'});addNeverBlue();addCriteoCode()");
		} else if (utm_medium == 'interspace') {
		    btn.setAttribute('onclick',"checkout.save();dataLayer.push({'event': 'placeOrder'});addAccesstrade();addCriteoCode()");
		}
	});
</script>

<!-- Conversion Tracking -->
<script type="text/javascript">
dataLayer.push({'conversionValue':{{var gross_billing}}});
</script>
<!-- End Conversion Tracking -->
HTML;
$actionItem->setCheckout($code);
$actionItem->setCriteoId(21453);
$actionItem->save();
/*********************************END MOXY TH***********************************/

/*********************************SANOGA EN***********************************/
/** @var Mage_Core_Model_Store $coreStoreModel */
$coreStoreModel = Mage::getModel('core/store');
$storeId = $coreStoreModel->load('sen')->getId();

/** @var EMST_Actiontags_Model_Mysql4_Action_Collection $actionTagModel */
$actionTagModel = Mage::getModel('actiontags/action')->getCollection();

$actionItem = $actionTagModel->addFieldToFilter('store_id', $storeId)->getFirstItem();

$actionItem = Mage::getModel('actiontags/action')->load($actionItem->getId());

$code = <<<HTML
<!--NeverBlue-->
<script type="text/javascript">
function addNeverBlue() {
	var generated = 1;
	var order_id = '{{var order_id}}';
	var neverblueScript = "http://cjsab.com/p.ashx?o=36695&e=696&f=js&p=SALES_AMT&t="+order_id;

    if(generated == 1) {
        var s = document.createElement("script");
        s.type = "text/javascript";
        s.src = neverblueScript;
        jQuery("head").append(s);

        generated = 0;
    }
}
</script>
<!--End NeverBlue-->
<!-- AccessTrade tracking code-->
<script type="text/javascript">
function addAccesstrade() {
	var generated = 1;
	var transaction = '{{var transation_id}}';
	var accessTradeItems = '{{var accessTradeItems}}';
	var accesstrade = "https://cv.accesstrade.in.th/cv.php?identifier=" + transaction + "&mcn=d645920e395fedad7bbbed0eca3fe2e0&result_id=3" + accessTradeItems;
		if(generated == 1) {
			jQuery('<img/>', {
				src: accesstrade,
				width: 1,
				height: 1,
			}).appendTo('body');
			generated = 0;
		}
}
</script>
<!-- End AccessTrade tracking code-->
<!-- GA E-Commerce Tracking (via GTM) -->
<script>
    dataLayer = [{
        'transactionId': '{{var transation_id}}',
        'transactionAffiliation': 'Sanoga EN Store',
       'transactionTotal': {{var gross_billing}},
        'transactionTax': {{var tax}},
        'transactionShipping': {{var shipping}},
        'transactionProducts': {{var items}}
    }];
</script>
<!-- End GA E-Commerce Tracking (via GTM) -->

<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-T3ZMK9"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-T3ZMK9');</script>
<!-- End Google Tag Manager -->

<script type="text/javascript">
	document.observe('dom:loaded',function() {
		var btn = $$('#checkout-review-submit button').first();
		var utm_medium = "{{var utm_medium}}";
		if(!btn) {
			btn = $$('button[onclick ^= "checkout.save"]').first();
		}
		if(!btn) {
			return;
		}
		btn.setAttribute('onclick',"checkout.save();dataLayer.push({'event': 'placeOrder'});addCriteoCode();");

		//Fire code Neverblue
		if(utm_medium == 'neverblue') {
			btn.setAttribute('onclick',"checkout.save();dataLayer.push({'event': 'placeOrder'});addNeverBlue();addCriteoCode();");
		} else if(utm_medium == 'interspace') {
		    btn.setAttribute('onclick',"checkout.save();dataLayer.push({'event': 'placeOrder'});addAccesstrade();addCriteoCode();");
		}
	});
</script>

<!-- Conversion Tracking -->
<script type="text/javascript">
dataLayer.push({'conversionValue':{{var gross_billing}}});
</script>
<!-- End Conversion Tracking -->
HTML;
$actionItem->setCheckout($code);
$actionItem->save();
/*********************************END SANOGA EN***********************************/

/*********************************SANOGA MOBILE***********************************/
/** @var Mage_Core_Model_Store $coreStoreModel */
$coreStoreModel = Mage::getModel('core/store');
$storeId = $coreStoreModel->load('sm')->getId();

/** @var EMST_Actiontags_Model_Mysql4_Action_Collection $actionTagModel */
$actionTagModel = Mage::getModel('actiontags/action')->getCollection();

$actionItem = $actionTagModel->addFieldToFilter('store_id', $storeId)->getFirstItem();

$actionItem = Mage::getModel('actiontags/action')->load($actionItem->getId());

$code = <<<HTML
<!--NeverBlue-->
<script type="text/javascript">
function addNeverBlue() {
	var generated = 1;
	var order_id = '{{var order_id}}';
	var neverblueScript = "http://cjsab.com/p.ashx?o=36695&e=696&f=js&p=SALES_AMT&t="+order_id;

    if(generated == 1) {
        var s = document.createElement("script");
        s.type = "text/javascript";
        s.src = neverblueScript;
        jQuery("head").append(s);

        generated = 0;
    }
}
</script>
<!--End NeverBlue-->
<!-- AccessTrade tracking code-->
<script type="text/javascript">
function addAccesstrade() {
	var generated = 1;
	var transaction = '{{var transation_id}}';
	var accessTradeItems = '{{var accessTradeItems}}';
	var accesstrade = "https://cv.accesstrade.in.th/cv.php?identifier=" + transaction + "&mcn=a1d0c6e83f027327d8461063f4ac58a6b&result_id=3" + accessTradeItems;
		if(generated == 1) {
			jQuery('<img/>', {
				src: accesstrade,
				width: 1,
				height: 1,
			}).appendTo('body');
			generated = 0;
		}
}
</script>
<!-- End AccessTrade tracking code-->
<!-- GA E-Commerce Tracking (via GTM) -->
<script>
    dataLayer = [{
        'transactionId': '{{var transation_id}}',
        'transactionAffiliation': 'Sanoga Mobile Store',
       'transactionTotal': {{var gross_billing}},
        'transactionTax': {{var tax}},
        'transactionShipping': {{var shipping}},
        'transactionProducts': {{var items}}
    }];
</script>
<!-- End GA E-Commerce Tracking (via GTM) -->

<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-T3ZMK9"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-T3ZMK9');</script>
<!-- End Google Tag Manager -->

<script type="text/javascript">
	document.observe('dom:loaded',function() {
		var btn = $$('#checkout-review-submit button').first();
		var utm_medium = "{{var utm_medium}}";
		if(!btn) {
			btn = $$('button[onclick ^= "checkout.save"]').first();
		}
		if(!btn) {
			return;
		}
		btn.setAttribute('onclick',"checkout.save();dataLayer.push({'event': 'placeOrder'});");
		//Fire code Neverblue
		if(utm_medium == 'neverblue') {
			btn.setAttribute('onclick',"checkout.save();dataLayer.push({'event': 'placeOrder'});addNeverBlue();");
		} else if(utm_medium == 'interspace') {
		    btn.setAttribute('onclick',"checkout.save();dataLayer.push({'event': 'placeOrder'});addAccesstrade();");
		}
	});
</script>

<!-- Conversion Tracking -->
<script type="text/javascript">
dataLayer.push({'conversionValue':{{var gross_billing}}});
</script>
<!-- End Conversion Tracking -->
HTML;
$actionItem->setCheckout($code);
$actionItem->save();
/*********************************END SANOGA MOBILE***********************************/

/*********************************SANOGA TH***********************************/
/** @var Mage_Core_Model_Store $coreStoreModel */
$coreStoreModel = Mage::getModel('core/store');
$storeId = $coreStoreModel->load('sth')->getId();

/** @var EMST_Actiontags_Model_Mysql4_Action_Collection $actionTagModel */
$actionTagModel = Mage::getModel('actiontags/action')->getCollection();

$actionItem = $actionTagModel->addFieldToFilter('store_id', $storeId)->getFirstItem();

$actionItem = Mage::getModel('actiontags/action')->load($actionItem->getId());

$code = <<<HTML
<!--NeverBlue-->
<script type="text/javascript">
function addNeverBlue() {
	var generated = 1;
	var order_id = '{{var order_id}}';
	var neverblueScript = "http://cjsab.com/p.ashx?o=36695&e=696&f=js&p=SALES_AMT&t="+order_id;

    if(generated == 1) {
        var s = document.createElement("script");
        s.type = "text/javascript";
        s.src = neverblueScript;
        jQuery("head").append(s);

        generated = 0;
    }
}
</script>
<!--End NeverBlue-->
<!-- AccessTrade tracking code-->
<script type="text/javascript">
function addAccesstrade() {
	var generated = 1;
	var transaction = '{{var transation_id}}';
	var accessTradeItems = '{{var accessTradeItems}}';
	var accesstrade = "https://cv.accesstrade.in.th/cv.php?identifier=" + transaction + "&mcn=d645920e395fedad7bbbed0eca3fe2e0&result_id=3" + accessTradeItems;
		if(generated == 1) {
			jQuery('<img/>', {
				src: accesstrade,
				width: 1,
				height: 1,
			}).appendTo('body');
			generated = 0;
		}
}
</script>
<!-- End AccessTrade tracking code-->
<!-- GA E-Commerce Tracking (via GTM) -->
<script>
    dataLayer = [{
        'transactionId': '{{var transation_id}}',
        'transactionAffiliation': 'Sanoga TH Store',
       'transactionTotal': {{var gross_billing}},
        'transactionTax': {{var tax}},
        'transactionShipping': {{var shipping}},
        'transactionProducts': {{var items}}
    }];
</script>
<!-- End GA E-Commerce Tracking (via GTM) -->

<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-T3ZMK9"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-T3ZMK9');</script>
<!-- End Google Tag Manager -->

<script type="text/javascript">
	document.observe('dom:loaded',function() {
		var btn = $$('#checkout-review-submit button').first();
		var utm_medium = "{{var utm_medium}}";
		if(!btn) {
			btn = $$('button[onclick ^= "checkout.save"]').first();
		}
		if(!btn) {
			return;
		}
		btn.setAttribute('onclick',"checkout.save();dataLayer.push({'event': 'placeOrder'});addCriteoCode();");
		//Fire code Neverblue
		if(utm_medium == 'neverblue') {
			btn.setAttribute('onclick',"checkout.save();dataLayer.push({'event': 'placeOrder'});addNeverBlue();addCriteoCode();");
		} else if (utm_medium == 'interspace') {
		    btn.setAttribute('onclick',"checkout.save();dataLayer.push({'event': 'placeOrder'});addAccesstrade();addCriteoCode();");
		}
	});
</script>

<!-- Conversion Tracking -->
<script type="text/javascript">
dataLayer.push({'conversionValue':{{var gross_billing}}});
</script>
<!-- End Conversion Tracking -->
HTML;
$actionItem->setCheckout($code);
$actionItem->save();
/*********************************END SANOGA TH***********************************/

/*********************************VENBI EN***********************************/
/** @var Mage_Core_Model_Store $coreStoreModel */
$coreStoreModel = Mage::getModel('core/store');
$storeId = $coreStoreModel->load('ven')->getId();

/** @var EMST_Actiontags_Model_Mysql4_Action_Collection $actionTagModel */
$actionTagModel = Mage::getModel('actiontags/action')->getCollection();

$actionItem = $actionTagModel->addFieldToFilter('store_id', $storeId)->getFirstItem();

$actionItem = Mage::getModel('actiontags/action')->load($actionItem->getId());

$code = <<<HTML
<!--NeverBlue-->
<script type="text/javascript">
function addNeverBlue() {
	var generated = 1;
	var order_id = '{{var order_id}}';
	var neverblueScript = "http://cjsab.com/p.ashx?o=36694&e=696&f=js&p=SALES_AMT&t="+order_id;

    if(generated == 1) {
        var s = document.createElement("script");
        s.type = "text/javascript";
        s.src = neverblueScript;
        jQuery("head").append(s);

        generated = 0;
    }
}
</script>
<!--End NeverBlue-->
<!-- AccessTrade tracking code-->
<script type="text/javascript">
function addAccesstrade() {
	var generated = 1;
	var transaction = '{{var transation_id}}';
	var accessTradeItems = '{{var accessTradeItems}}';
	var accesstrade = "https://cv.accesstrade.in.th/cv.php?identifier=" + transaction + "&mcn=d645920e395fedad7bbbed0eca3fe2e0&result_id=3" + accessTradeItems;
		if(generated == 1) {
			jQuery('<img/>', {
				src: accesstrade,
				width: 1,
				height: 1,
			}).appendTo('body');
			generated = 0;
		}
}
</script>
<!-- End AccessTrade tracking code--><!-- GA E-Commerce Tracking (via GTM) -->
<script>
    dataLayer = [{
        'transactionId': '{{var transation_id}}',
        'transactionAffiliation': 'Venbi EN Store',
        'transactionTotal': {{var gross_billing}},
        'transactionTax': {{var tax}},
        'transactionShipping': {{var shipping}},
        'transactionProducts': {{var items}}
    }];
</script>
<!-- End GA E-Commerce Tracking (via GTM) -->

<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-T3ZMK9"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-T3ZMK9');</script>
<!-- End Google Tag Manager -->

<script type="text/javascript">
	document.observe('dom:loaded',function() {
		var btn = $$('#checkout-review-submit button').first();
		var utm_medium = "{{var utm_medium}}";

		if(!btn) {
			btn = $$('button[onclick ^= "checkout.save"]').first();
		}
		if(!btn) {
			return;
		}
		btn.setAttribute('onclick',"checkout.save();dataLayer.push({'event': 'placeOrder'});addCriteoCode();");
		if(utm_medium == 'neverblue') {
			btn.setAttribute('onclick',"checkout.save();dataLayer.push({'event': 'placeOrder'});addNeverBlue();addCriteoCode();");
		} else if (utm_medium == 'interspace') {
		    btn.setAttribute('onclick',"checkout.save();dataLayer.push({'event': 'placeOrder'});addAccesstrade();addCriteoCode();");
		}
	});
</script>

<!-- Conversion Tracking -->
<script type="text/javascript">
dataLayer.push({'conversionValue':{{var gross_billing}}});
</script>
<!-- End Conversion Tracking -->
HTML;
$actionItem->setCheckout($code);
$actionItem->save();
/*********************************END VENBI EN***********************************/

/*********************************VENBI MOBILE***********************************/
/** @var Mage_Core_Model_Store $coreStoreModel */
$coreStoreModel = Mage::getModel('core/store');
$storeId = $coreStoreModel->load('vm')->getId();

/** @var EMST_Actiontags_Model_Mysql4_Action_Collection $actionTagModel */
$actionTagModel = Mage::getModel('actiontags/action')->getCollection();

$actionItem = $actionTagModel->addFieldToFilter('store_id', $storeId)->getFirstItem();

$actionItem = Mage::getModel('actiontags/action')->load($actionItem->getId());

$code = <<<HTML
<!--NeverBlue-->
<script type="text/javascript">
function addNeverBlue() {
	var generated = 1;
	var order_id = '{{var order_id}}';
	var neverblueScript = "http://cjsab.com/p.ashx?o=36694&e=696&f=js&p=SALES_AMT&t="+order_id;

    if(generated == 1) {
        var s = document.createElement("script");
        s.type = "text/javascript";
        s.src = neverblueScript;
        jQuery("head").append(s);

        generated = 0;
    }
}
</script>
<!--End NeverBlue-->
<!-- AccessTrade tracking code-->
<script type="text/javascript">
function addAccesstrade() {
	var generated = 1;
	var transaction = '{{var transation_id}}';
	var accessTradeItems = '{{var accessTradeItems}}';
	var accesstrade = "https://cv.accesstrade.in.th/cv.php?identifier=" + transaction + "&mcn=3416a75f4cea9109507cacd8e2f2aefc&result_id=3" + accessTradeItems;
		if(generated == 1) {
			jQuery('<img/>', {
				src: accesstrade,
				width: 1,
				height: 1,
			}).appendTo('body');
			generated = 0;
		}
}
</script>
<!-- End AccessTrade tracking code--><!-- GA E-Commerce Tracking (via GTM) -->
<script>
    dataLayer = [{
        'transactionId': '{{var transation_id}}',
        'transactionAffiliation': 'Venbi Mobile Store',
        'transactionTotal': {{var gross_billing}},
        'transactionTax': {{var tax}},
        'transactionShipping': {{var shipping}},
        'transactionProducts': {{var items}}
    }];
</script>
<!-- End GA E-Commerce Tracking (via GTM) -->

<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-T3ZMK9"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-T3ZMK9');</script>
<!-- End Google Tag Manager -->

<script type="text/javascript">
	document.observe('dom:loaded',function() {
		var btn = $$('#checkout-review-submit button').first();
		var utm_medium = "{{var utm_medium}}";
		if(!btn) {
			btn = $$('button[onclick ^= "checkout.save"]').first();
		}
		if(!btn) {
			return;
		}
		btn.setAttribute('onclick',"checkout.save();dataLayer.push({'event': 'placeOrder'});");

		if(utm_medium == 'neverblue') {
			btn.setAttribute('onclick',"checkout.save();dataLayer.push({'event': 'placeOrder'});addNeverBlue();");
		} else if (utm_medium == 'interspace') {
		    btn.setAttribute('onclick',"checkout.save();dataLayer.push({'event': 'placeOrder'});addAccesstrade();");
		}
	});
</script>

<!-- Conversion Tracking -->
<script type="text/javascript">
dataLayer.push({'conversionValue':{{var gross_billing}}});
</script>
<!-- End Conversion Tracking -->
HTML;
$actionItem->setCheckout($code);
$actionItem->save();
/*********************************END VENBI MOBILE***********************************/

/*********************************VENBI TH***********************************/
/** @var Mage_Core_Model_Store $coreStoreModel */
$coreStoreModel = Mage::getModel('core/store');
$storeId = $coreStoreModel->load('vth')->getId();

/** @var EMST_Actiontags_Model_Mysql4_Action_Collection $actionTagModel */
$actionTagModel = Mage::getModel('actiontags/action')->getCollection();

$actionItem = $actionTagModel->addFieldToFilter('store_id', $storeId)->getFirstItem();

$actionItem = Mage::getModel('actiontags/action')->load($actionItem->getId());

$code = <<<HTML
<!--NeverBlue-->
<script type="text/javascript">
function addNeverBlue() {
	var generated = 1;
	var order_id = '{{var order_id}}';
	var neverblueScript = "http://cjsab.com/p.ashx?o=36694&e=696&f=js&p=SALES_AMT&t="+order_id;

    if(generated == 1) {
        var s = document.createElement("script");
        s.type = "text/javascript";
        s.src = neverblueScript;
        jQuery("head").append(s);

        generated = 0;
    }
}
</script>
<!--End NeverBlue-->
<!-- AccessTrade tracking code-->
<script type="text/javascript">
function addAccesstrade() {
	var generated = 1;
	var transaction = '{{var transation_id}}';
	var accessTradeItems = '{{var accessTradeItems}}';
	var accesstrade = "https://cv.accesstrade.in.th/cv.php?identifier=" + transaction + "&mcn=d645920e395fedad7bbbed0eca3fe2e0&result_id=3" + accessTradeItems;
		if(generated == 1) {
			jQuery('<img/>', {
				src: accesstrade,
				width: 1,
				height: 1,
			}).appendTo('body');
			generated = 0;
		}
}
</script>
<!-- End AccessTrade tracking code--><!-- GA E-Commerce Tracking (via GTM) -->
<script>
    dataLayer = [{
        'transactionId': '{{var transation_id}}',
        'transactionAffiliation': 'Venbi TH Store',
        'transactionTotal': {{var gross_billing}},
        'transactionTax': {{var tax}},
        'transactionShipping': {{var shipping}},
        'transactionProducts': {{var items}}
    }];
</script>
<!-- End GA E-Commerce Tracking (via GTM) -->

<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-T3ZMK9"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-T3ZMK9');</script>
<!-- End Google Tag Manager -->

<script type="text/javascript">
	document.observe('dom:loaded',function() {
		var btn = $$('#checkout-review-submit button').first();
		var utm_medium = "{{var utm_medium}}";

		if(!btn) {
			btn = $$('button[onclick ^= "checkout.save"]').first();
		}
		if(!btn) {
			return;
		}
		btn.setAttribute('onclick',"checkout.save();dataLayer.push({'event': 'placeOrder'});addCriteoCode();");
		if(utm_medium == 'neverblue') {
			btn.setAttribute('onclick',"checkout.save();dataLayer.push({'event': 'placeOrder'});addNeverBlue();addCriteoCode();");
		} else if(utm_medium == 'interspace') {
		    btn.setAttribute('onclick',"checkout.save();dataLayer.push({'event': 'placeOrder'});addAccesstrade();addCriteoCode();");
		}
	});
</script>

<!-- Conversion Tracking -->
<script type="text/javascript">
dataLayer.push({'conversionValue':{{var gross_billing}}});
</script>
<!-- End Conversion Tracking -->
HTML;
$actionItem->setCheckout($code);
$actionItem->save();
/*********************************END VENBI TH***********************************/

$this->endSetup();