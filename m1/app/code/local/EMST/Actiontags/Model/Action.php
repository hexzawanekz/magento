<?php
class EMST_Actiontags_Model_Action extends Mage_Core_Model_Abstract
{

    /**
     * Initialize model
     * 
     * @return void
     */
    protected function _construct()
    {
        $this->_init('actiontags/action');
    }

    /**
     * Get Action tags
     * 
     * @param string $param configuration param name
     * 
     * @return string|EMST_Actiontags_Model_Action|bool
     */
    public function getConfiguration($param = NULL)
    {
		$storeId = (int) Mage::app()->getStore()->getId();
        $collection = $this->getCollection()
						->addFieldToFilter('store_id',array('in'=>array(0,$storeId)));
        if ($count = $collection->count()) {
			$result = null;
			if($count>1) {
				$default = null;
				foreach($collection as $it) {
					if($it->getStoreId() == $storeId) {
						$result = $it;
					}
					if($it->getStoreId() == 0) {
						$default = $it;
					}
				}
				if($param) {
					if($result && !$result->getData($param)) {
						$result = $default;
					}
				}
			}			
			if(!$result) {
				$result = $collection->getFirstItem();
			}
			
            if ($param) {
                return $result->getData($param);
            }
            return $result;
        } else {
            return false;
        }
    }

}
