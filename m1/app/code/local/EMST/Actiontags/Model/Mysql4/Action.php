<?php
class EMST_Actiontags_Model_Mysql4_Action extends Mage_Core_Model_Mysql4_Abstract
{
    /**
     * Initialize resource model
     * 
     * @return void
     */
    protected function _construct()
    {
        $this->_init('actiontags/action', 'action_id');
    }
}
