<?php
$installer = $this;
$installer->startSetup();

$installer->run("
 CREATE TABLE {$this->getTable('actiontags_actions')} (
    `action_id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
    `subscription` TEXT NOT NULL ,
    `registration` TEXT NOT NULL ,
    `profileupdate` TEXT NOT NULL ,
    `order` TEXT NOT NULL ,
    `header` TEXT NOT NULL ,
    `footer` TEXT NOT NULL,
	`checkout` TEXT NULL
) ENGINE = InnoDB CHARACTER SET utf8;
");

$installer->endSetup();
