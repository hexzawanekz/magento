<?php

$installer = $this;
$installer->startSetup();
$installer->getConnection()
	->addColumn($installer->getTable('actiontags/action'), 'cart', 
			array(
				'TYPE'      => Varien_Db_Ddl_Table::TYPE_TEXT,
				'NULLABLE'  => true,
				'COMMENT'   => 'cart'
			));
$installer->getConnection()
	->addColumn($installer->getTable('actiontags/action'), 'store_id', 
			array(
				'TYPE'      => Varien_Db_Ddl_Table::TYPE_SMALLINT,
				'NULLABLE'  => false,
				'DEFAULT'  	=> 0,
				'COMMENT'   => 'store ID'
			));
$installer->endSetup();
