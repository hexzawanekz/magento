<?php
/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Reports to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_Reports
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
*/

class EMST_Reports_Block_Adminhtml_Report_Product_Lowstock_Grid extends Mage_Adminhtml_Block_Report_Product_Lowstock_Grid
{
	protected $_category = null;
	public function __construct() {
        parent::__construct();
		$this->_saveParametersInSession = true;
        $this->setUseAjax(true);
    }
	protected function _getStoreId() {
		$storeId = Mage::registry('store_id');	
		$this->_category = (Mage::registry('category'))?Mage::registry('category'):Mage::registry('current_category');
		if (!$this->_category && $this->getParam('id')) {
            $this->_category = Mage::getModel('catalog/category')->load($this->getParam('id'));
        }		
		if($this->_category) {
			$cStoreIds = $this->_category->getStoreIds();
			if(!in_array($storeId,$cStoreIds)) {						
				$this->_category = null;
			}
		}
		if(!$this->_category && $storeId!=$this->helper('emst_reports')->getDefaultStoreId()) {
			$rootCategoryId = Mage::app()->getStore($storeId)->getRootCategoryId();				
			$this->_category = Mage::getModel('catalog/category')->load($rootCategoryId);
		}		
		return $storeId;
	}
	protected function _prepareCollection() {
		$storeId = $this->_getStoreId();
        

        /** @var $collection Mage_Reports_Model_Resource_Product_Lowstock_Collection  */
        $collection = Mage::getResourceModel('reports/product_lowstock_collection')
            ->addAttributeToSelect('*')
            ->filterByIsQtyProductTypes()
            ->joinInventoryItem('qty')
            ->useManageStockFilter($storeId)
            ->useNotifyStockQtyFilter($storeId)
            ->setOrder('qty', Varien_Data_Collection::SORT_ORDER_ASC);

		if( $this->_category && $this->_category->getId()) {			
            $collection->addCategoryFilter($this->_category);
        }
		elseif($this->getRequest()->getParam('website')) {
			$collection->addWebsiteFilter($this->getRequest()->getParam('website'));
		}
		/* if( $storeId ) {
            $collection->addStoreFilter($storeId);
        } */
        $this->setCollection($collection);
        return Mage_Adminhtml_Block_Widget_Grid::_prepareCollection();
    }
	public function getGridUrl() {
		return $this->getUrl('*/*/lowstockgrid',array('_current'=>true));
	}
}