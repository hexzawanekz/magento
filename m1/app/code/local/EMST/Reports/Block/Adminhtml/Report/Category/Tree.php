<?php
class EMST_Reports_Block_Adminhtml_Report_Category_Tree extends Mage_Adminhtml_Block_Catalog_Category_Tree
{

    public function __construct()
    {
        parent::__construct();
        $this->_withProductCount = false;
    }
	protected function _getDefaultStoreId() {
        return $this->helper('emst_reports')->getDefaultStoreId();
    }
    protected function _prepareLayout() {
        return Mage_Adminhtml_Block_Catalog_Category_Abstract::_prepareLayout();
    }
    public function getAddRootButtonHtml()
    {
        return null;
    }

    public function getAddSubButtonHtml() {
        return null;
    }
    public function getStoreSwitcherHtml() {
        return null;
    }

    public function getSwitchTreeUrl() {
        return $this->getUrl("*/report_product/tree", array('_current'=>true));
    }
	public function getContentUrl() {
        return $this->getUrl("*/report_product/lowstock", array('_current'=>true));
    }
    protected function _isCategoryMoveable($node) {
        return false;
    }
	public function getGroupRequest() {
		return $this->getRequest()->getParam('group');
	}
	public function getRoot($parentNodeCategory=null, $recursionLevel=3) {
        if (!is_null($parentNodeCategory) && $parentNodeCategory->getId()) {
            return $this->getNode($parentNodeCategory, $recursionLevel);
        }
        $root = Mage::registry('root');
        if (is_null($root)) {
            $storeId = $this->getStore()->getId();
            if ($storeId) {                
                $rootId = $this->getStore()->getRootCategoryId();
            }
            else {
                $rootId = Mage_Catalog_Model_Category::TREE_ROOT_ID;
            }

            $tree = Mage::getResourceSingleton('catalog/category_tree')
                ->load(null, $recursionLevel);

            if ($this->getCategory()) {
                $tree->loadEnsuredNodes($this->getCategory(), $tree->getNodeById($rootId));
            }

            $tree->addCollectionData($this->getCategoryCollection(), false, array(), true, true);

            $root = $tree->getNodeById($rootId);

            if ($root && $rootId != Mage_Catalog_Model_Category::TREE_ROOT_ID) {
                $root->setIsVisible(true);
            }
            elseif($root && $root->getId() == Mage_Catalog_Model_Category::TREE_ROOT_ID) {
                $root->setName(Mage::helper('catalog')->__('Root'));
            }

            Mage::register('root', $root);
        }

        return $root;
    }
	public function getStore() {
        $storeId = (int) Mage::registry('store_id');
        return Mage::app()->getStore($storeId);
    }
	public function getCategory() {
        $category = Mage::registry('category');
		if(!$category) {
			$rootId = $this->getStore()->getRootCategoryId();
			$category = Mage::getModel('catalog/category')->load($rootId);
		}
		return $category;
    }
	public function getCategoryId() {
        if ($this->getCategory()) {
            return $this->getCategory()->getId();
        }
		if(!$this->getRequest()->getParam('store') && $this->getRequest()->getParam('group')) {
			return Mage::app()->getGroup($this->getRequest()->getParam('group'))->getRootCategoryId();
		}
		return Mage_Catalog_Model_Category::TREE_ROOT_ID;
    }
	public function getNode($parentNodeCategory, $recursionLevel=2) {
        $tree = Mage::getResourceModel('catalog/category_tree');

        $nodeId     = $parentNodeCategory->getId();
        $parentId   = $parentNodeCategory->getParentId();

        $node = $tree->loadNode($nodeId);
        $node->loadChildren($recursionLevel);

        if ($node && $nodeId != Mage_Catalog_Model_Category::TREE_ROOT_ID) {
            $node->setIsVisible(true);
        } elseif($node && $node->getId() == Mage_Catalog_Model_Category::TREE_ROOT_ID) {
            $node->setName(Mage::helper('catalog')->__('Root'));
        }

        $tree->addCollectionData($this->getCategoryCollection(), false, array(), true, true);

        return $node;
    }
}
