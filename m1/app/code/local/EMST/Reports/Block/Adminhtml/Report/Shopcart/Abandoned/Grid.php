<?php
/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Reports to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_Reports
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
*/

class EMST_Reports_Block_Adminhtml_Report_Shopcart_Abandoned_Grid extends Mage_Adminhtml_Block_Report_Grid_Shopcart
{
	
	public function __construct()
    {
        parent::__construct();
		$this->setDefaultSort('updated_at');
		$this->setDefaultDir('DESC');
        $this->setId('gridAbandoned');
    }
	protected function _prepareCollection()
    {
        /** @var $collection Mage_Reports_Model_Resource_Quote_Collection */
        $collection = Mage::getResourceModel('emst_reports/quote_collection');

        $filter = $this->getParam($this->getVarNameFilter(), array());
        if ($filter) {
            $filter = base64_decode($filter);
            parse_str(urldecode($filter), $data);
        }

        if (!empty($data)) {
            $collection->prepareForAbandonedReport($this->_storeIds, $data);
        } else {
            $collection->prepareForAbandonedReport($this->_storeIds);
        }
		$collection->addProductItems()
					->addExpiredTime();				
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }
    protected function _prepareColumns()
    {
		$this->addColumn('customer_email', array(
            'header'    =>Mage::helper('emst_reports')->__('Customer Email'),
            'index'     =>'customer_email',
        ));
		
		$this->addColumn('product_id', array(
            'header'    =>Mage::helper('emst_reports')->__('Product ID'),
            'index'     =>'product_id',
        ));
		
		$this->addColumn('created_at', array(
            'header'    =>Mage::helper('emst_reports')->__('Created At'),
            'width'     =>'170px',
            'type'      =>'datetime',
            'index'     =>'created_at',
            'filter_index'=>'main_table.created_at',
        ));
		
		$this->addColumn('qty', array(
            'header'    =>Mage::helper('emst_reports')->__('QTY'),
            'width'     =>'80px',
            'align'     =>'right',
            'index'     =>'qty',
            'type'      =>'number'
        ));
		
		$this->addColumn('send_email', array(
            'header'    =>Mage::helper('emst_reports')->__('Send Email'),
            'index'     =>'send_email',			
        ));
		
        $this->addExportType('*/*/exportAbandonedCsv', Mage::helper('emst_reports')->__('CSV'));
        $this->addExportType('*/*/exportAbandonedExcel', Mage::helper('emst_reports')->__('Excel XML'));

        return parent::_prepareColumns();
    }
	
	public function getRowUrl($row)
    {
        return $this->getUrl('*/customer/edit', array('id'=>$row->getCustomerId(), 'active_tab'=>'cart'));
    }
}