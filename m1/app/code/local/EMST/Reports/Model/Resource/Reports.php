<?php
/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Regions to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_Reports
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
*/
class EMST_Reports_Model_Resource_Reports extends Mage_Core_Model_Resource_Db_Abstract
{
	public function _construct()
    { 
        $this->_init('emst_reports/viewed_product', 'id');
    }
	protected function _beforeSave(Mage_Core_Model_Abstract $object)
    {
		$storeId = Mage::app()->getStore()->getId();
		$object->setUpdatedAt(Varien_Date::now())
				->setStoreId($storeId);
        return $this;
    }
}
