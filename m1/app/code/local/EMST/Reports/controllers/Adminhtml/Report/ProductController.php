<?php
/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Reports to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_Reports
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
*/
require_once Mage::getModuleDir('controllers', 'Mage_Adminhtml') . DS . 'Report' . DS . 'ProductController.php';
class EMST_Reports_Adminhtml_Report_ProductController extends Mage_Adminhtml_Report_ProductController
{

	public function preDispatch() {
		parent::preDispatch();
		$this->_initStore();
	}
    /**
     * Low stock action
     *
     */
    public function lowstockAction() {
        $this->_title($this->__('Reports'))
             ->_title($this->__('Products'))
             ->_title($this->__('Low Stock'));
		// $this->_initCategory();
		
		if ($this->getRequest()->getQuery('isAjax')) {  			
			if($catId = $this->getRequest()->getParam('id')) {
				$this->_getSession()->setLastCategory($catId);
			}
            $this->loadLayout();
            $eventResponse = new Varien_Object(array(
                'content' => $this->getLayout()->createBlock('emst_reports/adminhtml_report_product_lowstock_grid')->toHtml(),
                'messages' => $this->getLayout()->getMessagesBlock()->getGroupedHtml(),
            ));
            $this->getResponse()->setBody(
                Mage::helper('core')->jsonEncode($eventResponse->getData())
            );
            return;
        }		
		if($_prevCatId = $this->_getSession()->getLastCategory()) {			
			$prevStoreIds = Mage::getModel('catalog/category')->load($_prevCatId)->getStoreIds();			
			if(in_array(Mage::registry('store_id'),$prevStoreIds)) {
				$this->getRequest()->setParam('id',$_prevCatId);
				$this->_initCategory();	
			}
		}
        $this->_initAction();
		$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
        $this->_setActiveMenu('report/product/lowstock')
            ->_addBreadcrumb(Mage::helper('reports')->__('Low Stock'), Mage::helper('reports')->__('Low Stock'))
            ->renderLayout();
    }
	protected function _initStore() {
		if ($this->getRequest()->getParam('group')) {
			$storeId = Mage::app()->getGroup($this->getRequest()->getParam('group'))->getDefaultStoreId();
			// $storeId = array_pop($storeIds);
		} else if ($this->getRequest()->getParam('store')) {
			$storeId = (int)$this->getRequest()->getParam('store');
		} else {
		   $storeId = Mage::helper('emst_reports')->getDefaultStoreId();
		}
		Mage::register('store_id',$storeId);
		return $storeId;
	}
	protected function _initCategory($getRootInstead = false) {
        $categoryId = (int) $this->getRequest()->getParam('id',false);
        $storeId    = (int) Mage::registry('store_id');
        $category = Mage::getModel('catalog/category');
        $category->setStoreId($storeId);
		
        if ($categoryId) {
            $category->load($categoryId);
            if ($storeId) {
                $rootId = Mage::app()->getStore($storeId)->getRootCategoryId();
                if (!in_array($rootId, $category->getPathIds())) {
                    // load root category instead wrong one
                    if ($getRootInstead) {
                        $category->load($rootId);
                    }
                    else {
                        $this->_redirect('*/*/', array('_current'=>true, 'id'=>null));
                        return false;
                    }
                }
            }
        }
        Mage::register('category', $category);
        Mage::register('current_category', $category);
        return $category;
    }
	public function lowstockgridAction() {
		$this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('emst_reports/adminhtml_report_product_lowstock_grid')->toHtml()
        );
	}
	public function treeAction() {
        // $storeId = (int) $this->getRequest()->getParam('store');
        $storeId = (int) Mage::registry('store_id');
        $categoryId = (int) $this->getRequest()->getParam('id');

        if ($storeId) {
            if (!$categoryId) {
                $store = Mage::app()->getStore($storeId);
                $rootId = $store->getRootCategoryId();
                $this->getRequest()->setParam('id', $rootId);
            }
        }

        $category = $this->_initCategory(true);

        $block = $this->getLayout()->createBlock('emst_reports/adminhtml_report_category_tree');
        $root  = $block->getRoot();
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode(array(
            'data' => $block->getTree(),
            'parameters' => array(
                'text'        => $block->buildNodeName($root),
                'draggable'   => false,
                'allowDrop'   => false,
                'id'          => (int) $root->getId(),
                'expanded'    => (int) $block->getIsWasExpanded(),
                'store_id'    => (int) $block->getStore()->getId(),
                'category_id' => (int) $category->getId(),
                'root_visible'=> (int) $root->getIsVisible()
        ))));
    }
	public function categoriesJsonAction() {
        if ($this->getRequest()->getParam('expand_all')) {
            Mage::getSingleton('admin/session')->setIsTreeWasExpanded(true);
        } else {
            Mage::getSingleton('admin/session')->setIsTreeWasExpanded(false);
        }
        if ($categoryId = (int) $this->getRequest()->getPost('id')) {
            $this->getRequest()->setParam('id', $categoryId);

            if (!$category = $this->_initCategory()) {
                return;
            }
            $this->getResponse()->setBody(
                $this->getLayout()->createBlock('emst_reports/adminhtml_report_category_tree')
                    ->getTreeJson($category)
            );
        }
    }
    /**
     * Export low stock products report to CSV format
     *
     */
    public function exportLowstockCsvAction() {
        $fileName   = 'products_lowstock.csv';
        $content    = $this->getLayout()->createBlock('emst_reports/adminhtml_report_product_lowstock_grid')
            ->setSaveParametersInSession(true)
            ->getCsv();

        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * Export low stock products report to XML format
     *
     */
    public function exportLowstockExcelAction() {
        $fileName   = 'products_lowstock.xml';
        $content    = $this->getLayout()->createBlock('emst_reports/adminhtml_report_product_lowstock_grid')
            ->setSaveParametersInSession(true)
            ->getExcel($fileName);

        $this->_prepareDownloadResponse($fileName, $content);
    }

	protected function _isEnableCategoryFilter() {
		return Mage::getStoreConfig('');
	}
}
