<?php
class EMST_Onestepcheckout_Model_Customer_Form extends Mage_Customer_Model_Form
{
    public function extractData(Zend_Controller_Request_Http $request, $scope = null, $scopeOnly = true)
    {
		$moduleEnabled 		= Mage::getStoreConfig(EMST_Onestepcheckout_Helper_Data::XML_PATH_ENABLE_ONESTEP);
		$appliedPackages 	= Mage::getStoreConfig(EMST_Onestepcheckout_Helper_Data::XML_PATH_APPLY);
		$currentTheme 		= Mage::getDesign()->getPackageName().'/'.Mage::getDesign()->getTheme('frontend');
		$currentStoreId 	= Mage::app()->getStore()->getId();
		$adminStoreId 		= Mage_Core_Model_App::ADMIN_STORE_ID;
		if($moduleEnabled && in_array($currentTheme, explode(',', $appliedPackages)) && $currentStoreId != $adminStoreId){
			$data = array();
			foreach ($this->getAttributes() as $attribute) {
				if ($this->_isAttributeOmitted($attribute)) {
					continue;
				}
				$dataModel = $this->_getAttributeDataModel($attribute);
				$dataModel->setRequestScope($scope);
				$dataModel->setRequestScopeOnly($scopeOnly);
				$data[$attribute->getAttributeCode()] = $dataModel->extractValue($request);
			}
			$district = '';
			$subDistrict = '';
			if($request->getParam('district')) {
				$district .= $request->getParam('district');
			}
			if($request->getParam('sub_district')) {
				$subDistrict .= $request->getParam('sub_district');
			}
			if($district) {
				if(is_array($data['street'])) {
					$data['street'][] = $district;
				}
				else {
					$data['street'] = array($district);
				}
			}
			if($subDistrict) {
				if(is_array($data['street'])) {
					$data['street'][] = $subDistrict;
				}
				else {
					$data['street'] = array($subDistrict);
				}
			}

			return $data;
		}else{
			return parent::extractData($request, $scope, $scopeOnly);
		}
    }
}
