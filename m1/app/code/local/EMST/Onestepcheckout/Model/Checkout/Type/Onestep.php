<?php

/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Onestepcheckout to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_Onestepcheckout
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
 */
class EMST_Onestepcheckout_Model_Checkout_Type_Onestep extends Mage_Checkout_Model_Type_Onepage
{
	protected $_defaultPaymentData = array();

    public function __construct()
    {
        return parent::__construct();
    }

    public function setBilling($data, $customerAddressId, $save = false)
    {
        if (empty($data)) {
            return array('error' => -1, 'message' => Mage::helper('checkout')->__('Invalid data.'));
        }

        $address = $this->getQuote()->getBillingAddress();
        /* @var $addressForm Mage_Customer_Model_Form */
        $addressForm = Mage::getModel('customer/form');
        $addressForm->setFormCode('customer_address_edit')
                ->setEntityType('customer_address')
                ->setIsAjaxRequest(Mage::app()->getRequest()->isAjax());

        if (!empty($customerAddressId)) {

            $customerAddress = Mage::getModel('customer/address')->load($customerAddressId);
            if ($customerAddress->getId()) {

                if ($customerAddress->getCustomerId() != $this->getQuote()->getCustomerId()) {
                    return array('error' => 1,
                        'message' => Mage::helper('checkout')->__('Customer Address is not valid.')
                    );
                }

                $address->importCustomerAddress($customerAddress)->setSaveInAddressBook(0);
                $addressForm->setEntity($address);
                if ($address->getRegion()) {
                    $address->setCity($address->getRegion());
                }
                $addressErrors = $addressForm->validateData($address->getData());
                if ($addressErrors !== true) {
                    return array('error' => 1, 'message' => $addressErrors);
                }
            }
        } else {

            $addressForm->setEntity($address);
            // emulate request object
            $addressData = $addressForm->extractData($addressForm->prepareRequest($data));
            $addressErrors = $addressForm->validateData($addressData);
            if ($addressErrors !== true) {
                return array('error' => 1, 'message' => array_values($addressErrors));
            }
            $addressForm->compactData($addressData);
            //unset billing address attributes which were not shown in form
            foreach ($addressForm->getAttributes() as $attribute) {
                if (!isset($data[$attribute->getAttributeCode()])) {
                    $address->setData($attribute->getAttributeCode(), NULL);
                }
            }
            $address->setCustomerAddressId(null);
            // Additional form data, not fetched by extractData (as it fetches only attributes)
            $address->setSaveInAddressBook(1);
        }

        if ($address->getRegion()) {
            $address->setCity($address->getRegion());
        }
        // validate billing address
        if (($validateRes = $address->validate()) !== true) {
            return array('error' => 1, 'message' => $validateRes);
        }

        $address->implodeStreetAddress();

        if (true !== ($result = $this->_validateCustomerData($data))) {
            return $result;
        }

        if (!$this->getQuote()->getCustomerId() && self::METHOD_REGISTER == $this->getQuote()->getCheckoutMethod()) {
            if ($this->_customerEmailExists($address->getEmail(), Mage::app()->getWebsite()->getId())) {
                return array('error' => 1, 'message' => $this->_customerEmailExistsMessage);
            }
        }

        if (!$this->getQuote()->isVirtual()) {
            /**
             * Billing address using otions
             */
            $usingCase = isset($data['use_for_shipping']) ? (int) $data['use_for_shipping'] : 0;

            switch ($usingCase) {
                case 0:
                    $shipping = $this->getQuote()->getShippingAddress();
                    $shipping->setSameAsBilling(0);
                    break;
                case 1:
                    $billing = clone $address;
                    $billing->unsAddressId()->unsAddressType();
                    $shipping = $this->getQuote()->getShippingAddress();
                    $shippingMethod = $shipping->getShippingMethod();
                    // Billing address properties that must be always copied to shipping address
                    $requiredBillingAttributes = array('customer_address_id');

                    // don't reset original shipping data, if it was not changed by customer
                    foreach ($shipping->getData() as $shippingKey => $shippingValue) {
                        if (!is_null($shippingValue) && !is_null($billing->getData($shippingKey)) && !isset($data[$shippingKey]) && !in_array($shippingKey, $requiredBillingAttributes)
                        ) {
                            $billing->unsetData($shippingKey);
                        }
                    }
                    $shipping->addData($billing->getData())
                            ->setSameAsBilling(1)
                            ->setSaveInAddressBook(0)
                            ->setShippingMethod($shippingMethod)
                            ->setCollectShippingRates(true);

                    break;
            }
        }
        $this->getQuote()->getShippingAddress()->setCollectShippingRates(true);
        $customer = Mage::getSingleton('customer/session')->getCustomer();
        $this->setShippingmethod();
        $this->getQuote()->collectTotals();
        if ($save) {
            $this->getQuote()->save();
        }

        return array();
    }

    public function setShipping($data, $customerAddressId, $save = false)
    {
        if (empty($data)) {
            return array('error' => -1, 'message' => Mage::helper('checkout')->__('Invalid data.'));
        }
        $address = $this->getQuote()->getShippingAddress();

        /* @var $addressForm Mage_Customer_Model_Form */
        $addressForm = Mage::getModel('customer/form');
        $addressForm->setFormCode('customer_address_edit')
                ->setEntityType('customer_address')
                ->setIsAjaxRequest(Mage::app()->getRequest()->isAjax());

        if (!empty($customerAddressId)) {
            $customerAddress = Mage::getModel('customer/address')->load($customerAddressId);
            if ($customerAddress->getId()) {
                if ($customerAddress->getCustomerId() != $this->getQuote()->getCustomerId()) {
                    return array('error' => 1,
                        'message' => Mage::helper('checkout')->__('Customer Address is not valid.')
                    );
                }

                $address->importCustomerAddress($customerAddress)->setSaveInAddressBook(0);
                $addressForm->setEntity($address);
                if ($address->getRegion()) {
                    $address->setCity($address->getRegion());
                }
                $addressErrors = $addressForm->validateData($address->getData());
                if ($addressErrors !== true) {
                    return array('error' => 1, 'message' => $addressErrors);
                }
            }
        } else {
            $addressForm->setEntity($address);
            // emulate request object
            $addressData = $addressForm->extractData($addressForm->prepareRequest($data));
            $addressErrors = $addressForm->validateData($addressData);
            if ($addressErrors !== true) {
                return array('error' => 1, 'message' => $addressErrors);
            }
            $addressForm->compactData($addressData);
            // unset shipping address attributes which were not shown in form
            foreach ($addressForm->getAttributes() as $attribute) {
                if (!isset($data[$attribute->getAttributeCode()])) {
                    $address->setData($attribute->getAttributeCode(), NULL);
                }
            }

            $address->setCustomerAddressId(null);
            // Additional form data, not fetched by extractData (as it fetches only attributes)
            $address->setSaveInAddressBook(1);
            $address->setSameAsBilling(empty($data['same_as_billing']) ? 0 : 1);
        }
        if ($address->getRegion()) {
            $address->setCity($address->getRegion());
        }

        $address->implodeStreetAddress();
        $address->setCollectShippingRates(true)->collectTotals();
        $this->setShippingmethod();
        if (($validateRes = $address->validate()) !== true) {
            return array('error' => 1, 'message' => $validateRes);
        }
        if ($save) {
            $this->getQuote()->collectTotals()->save();
        }
        return array();
    }
	public function setDefaultPaymentMethod($method) {
		$this->_defaultPaymentData['method']=$method;
		return $this;
	}
	public function getDefaultPaymentData() {
		return $this->_defaultPaymentData;
	}
    public function initCheckout()
    {
        $customerSession = $this->getCustomerSession();
        /**
         * Reset multishipping flag before any manipulations with quote address
         * addAddress method for quote object related on this flag
         */
        if ($this->getQuote()->getIsMultiShipping()) {
            $this->getQuote()->setIsMultiShipping(false);
            $this->getQuote()->save();
        }

        /*
         * want to load the correct customer information by assigning to address
         * instead of just loading from sales/quote_address
         */
        $customer = $customerSession->getCustomer();
        if ($customer) {
            $this->getQuote()->assignCustomer($customer);
        }

		$this->getQuote()->removePayment();
        $this->setShippingmethod();
		$defaultPaymentData = $this->getDefaultPaymentData();
		if(!empty($defaultPaymentData)) {
			$this->getQuote()->getPayment()->importData($defaultPaymentData);
		}
        $this->getQuote()->collectTotals()->save();
        return $this;
    }

    public function setShippingmethod()
    {
         /* $shippingMethod = $this->getShippingMethod($this->getQuote()->getShippingAddress());
        $this->getQuote()->getShippingAddress()->setShippingMethod($shippingMethod); */
        
          $groups = $this->getQuote()->getShippingAddress()->collectShippingRates()->getGroupedAllShippingRates();
          if(sizeof($groups)>1) {
			return;
          }
          foreach($groups as $rates) {
			  if(sizeof($rates)>1) {
				continue;
			  }
			  foreach($rates as $rate) {
				  $this->getQuote()->getShippingAddress()->setShippingMethod($rate->getCode());
				  // $this->getQuote()->collectTotals()->save();
			  }
          }
    }


    public function saveShippingMethod($shippingMethod)
    {
        if (empty($shippingMethod)) {
            return array('error' => -1, 'message' => Mage::helper('checkout')->__('Invalid shipping method.'));
        }
        $rate = $this->getQuote()->getShippingAddress()->getShippingRateByCode($shippingMethod);
        if (!$rate) {
            return array('error' => -1, 'message' => Mage::helper('checkout')->__('Invalid shipping method.'));
        }
        $this->getQuote()->getShippingAddress()
                ->setShippingMethod($shippingMethod);
		// $this->getQuote()->getShippingAddress()->setCollectShippingRates(true)->collectTotals();
        return array();
    }

    public function getShippingMethod(Mage_Sales_Model_Quote_Address $address)
    {
        $carriers = Mage::getModel('shipping/config')->getActiveCarriers();
        foreach ($carriers as $carrier) {
            /** @var Mage_Shipping_Model_Rate_Request $request */
            $request = Mage::getModel('shipping/rate_request');
            $request->setStoreId($address->getQuote()->getStore()->getId());
            $request->setWebsiteId($address->getQuote()->getStore()->getWebsiteId());

            $request->setDestCountryId($address->getCountryId());
            $request->setDestRegionCode($address->getRegionCode());
            $request->setDestRegionId($address->getRegionId());
            $request->setDestPostcode($address->getPostcode());
            // $request->setConditionName('package_weight');
            $request->setConditionName('package_value');
            // $request->setPackageWeight($address->getPostcode());
            $request->setPackageValue(Mage::helper('checkout/cart')->getQuote()->getSubtotal());
            $minimumSpend =  Mage::app()->getWebsite($address->getQuote()->getStore()->getWebsiteId())->getConfig('carriers/'.$carrier->getId().'/minimum_spend');
            $request->setMinimumSpend($minimumSpend);

            $result = $carrier->getRate($request);
            if (is_array($result)) {
                $allowedMethods = $carrier->getAllowedMethods();
                if(is_array($allowedMethods) && count($allowedMethods) > 0){
                    $firstMethods = array_keys($allowedMethods)[0];
                    return $carrier->getId().'_'.$firstMethods;
                }
            }
        }
        return null;
    }
}