<?php
/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Onestepcheckout to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_Onestepcheckout
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
*/

class EMST_Onestepcheckout_Model_Observer
{
	public function checkRedirect($observer) {
		
		$checkoutController = $observer->getEvent()->getControllerAction();
		if(!Mage::helper('onestepcheckout')->validateCandidateDesign()) {
			return;
		}
		if($checkoutController->getRequest()->getRequestedControllerName()!= 'onepage' 
			|| !Mage::helper('onestepcheckout')->isActive()) {
			return;
		}	
		$actionName = $checkoutController->getRequest()->getRequestedActionName();
		if($actionName != 'index') {
			$checkoutController->setRedirectWithCookieCheck(Mage::helper('onestepcheckout')->getOnestepUrlPath($actionName));
		}
		else {
			$checkoutController->setRedirectWithCookieCheck(Mage::helper('onestepcheckout')->getOnestepUrlPath());
		}
        $checkoutController->setFlag('', $checkoutController::FLAG_NO_DISPATCH, true);
	}
	public function afterLoadAddress($observer) {
		$obj = $observer->getEvent()->getObject();			
		if($obj instanceof Mage_Customer_Model_Address_Abstract) {
			$arr = $obj->getStreet();
			$size = sizeof($arr);
			if(!is_array($arr) || $size <3) {
				return;
			}			
			$subDistrict = $arr[$size-1];
			$district = $arr[$size-2];
			$obj->setDistrict($district);
			$obj->setSubDistrict($subDistrict);
		}
	}
}