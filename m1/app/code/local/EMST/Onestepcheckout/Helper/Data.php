<?php

/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Onestepcheckout to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_Onestepcheckout
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
 */
class EMST_Onestepcheckout_Helper_Data extends Mage_Core_Helper_Abstract
{
    const XML_PATH_ENABLE_ONESTEP = 'onestepcheckout/onestepconfig/active';
    const XML_PATH_REQUIRE_LOGGING = 'onestepcheckout/onestepconfig/require_logging_in';
    const XML_PATH_SHOW_SHIPPING_ADDRESS = 'onestepcheckout/onestepconfig/show_shipping_address';
    const XML_PATH_APPLY = 'onestepcheckout/onestepconfig/apply';
    const CHECKOUT_URL_PATH = 'onestepcheckout/onestep/';
    const DEFAULT_CHECKOUT_URL_PATH = 'checkout/onepage/index';

    public function getDefaultCheckoutUrlPath()
    {
        return self::DEFAULT_CHECKOUT_URL_PATH;
    }

    public function getThemes()
    {
        return explode(',', Mage::getStoreConfig(self::XML_PATH_APPLY));
    }

    public function validateCandidateDesign()
    {
        $package = Mage::getDesign()->getPackageName();
        $theme = Mage::getDesign()->getTheme('template');
        $currentDesign = $package . '/' . $theme;
        $config = $this->getThemes();
        if (in_array($currentDesign, $config) || empty($config) || $theme == Mage_Core_Model_Design_Package::DEFAULT_THEME) {
            return true;
        }
        return false;
    }

    public function getOnestepUrlPath($action = '')
    {
        if ($action) {
            return self::CHECKOUT_URL_PATH . $action;
        }
        return self::CHECKOUT_URL_PATH . 'index';
    }

    public function isActive($store = null)
    {
        $isActive = (bool) (int) Mage::getStoreConfig(self::XML_PATH_ENABLE_ONESTEP, $store);
        return $isActive;
    }

    public function hideShippingAddress($store = null)
    {
        $isShown = (bool) (int) Mage::getStoreConfig(self::XML_PATH_SHOW_SHIPPING_ADDRESS, $store);
        return !$isShown;
    }

    public function requireLoggingIn($store = null)
    {
        $require = (bool) (int) Mage::getStoreConfig(self::XML_PATH_REQUIRE_LOGGING, $store);
        return $require;
    }

    public function isAllowedGuestCheckout(Mage_Sales_Model_Quote $quote)
    {
        return Mage::helper('checkout')->isAllowedGuestCheckout($quote);
    }

    public function isCustomerMustBeLogged()
    {
        return Mage::helper('checkout')->isCustomerMustBeLogged();
    }

    public function setCityAsRegion($data)
    {
        if (!isset($data['city']) || !$data['city']) {
            $data['city'] = (isset($data['region']) && $data['region'] != '') ? $data['region'] : $data['region_id'];
        }
        return $data;
    }

    public function getDistrictData($obj, $line = 1)
    {
        $arr = $obj->getStreet();
        $size = sizeof($arr);
        if (!is_array($arr) || $size < 3) {
            return;
        }
        return $arr[$size - $line];
    }

    public function validateFreeMethod($quote, $method=null)
    {
        $total = $quote->getGrandTotal();
		$cod = $quote->getBaseCodFee();
        if (is_object($method)) {
            $methodCode = $method->getCode();
        } elseif (is_array($method)) {
            $methodCode = isset($method['method']) ? $method['method'] : '';
        } else {
            $methodCode = '';
        }
        if ($total - $cod <= 0.001 && $methodCode != 'free') {
			$payment['method'] = 'free';
			$quote->removePayment()->setTotalsCollectedFlag(false)->collectTotals();				
			$quote->getPayment()->importData($payment);
            return false;
        }
        return true;
    }
}