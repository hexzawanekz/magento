<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Checkout
 * @copyright   Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * One page checkout status
 *
 * @category   Mage
 * @category   Mage
 * @package    Mage_Checkout
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class EMST_Onestepcheckout_Block_Onepage_Shipping_Method_Available extends Mage_Checkout_Block_Onepage_Shipping_Method_Available
{

    public function getThirdPartyScript($carrierCode)
    {
        if ($name = Mage::getStoreConfig('carriers/' . $carrierCode . '/script_field')) {
            return $name;
        }
        return '';
    }

    public function saveShippingMethod()
    {
        return $this->getUrl('*/*/saveShippingMethod');
    }

    public function getDescription($carrierCode)
    {
        return Mage::getStoreConfig('carriers/' . $carrierCode . '/description');
    }

    public function getShippingRates()
    {        
        if (empty($this->_rates)) {
            $this->getAddress()->collectShippingRates()->save();
            $groups = $this->getAddress()->getGroupedAllShippingRates();
            $this->setShippingmethod($groups);
            return $this->_rates = $groups;
        }

        return $this->_rates;
    }

    public function setShippingmethod($groups)
    {
        if (sizeof($groups) > 1) {
            return;
        }
        foreach ($groups as $rates) {
            if (sizeof($rates) > 1) {
                continue;
            }
            foreach ($rates as $rate) {
                $method = $this->getAddress()->getShippingMethod();
                $this->getAddress()->setShippingMethod($rate->getCode());
                //if($this->hasAmpromoRule()) {
                //    $this->getQuote()->setTotalsCollectedFlag(false);
                //}
                $this->getQuote()->collectTotals();
            }
        }
    }

    /**
     * Check Am promo Rule
     * 
     * @return boolean
     */
    public function hasAmpromoRule()
    {
        foreach ($this->getQuote()->getAllItems() as $item) {
            foreach ($item->getOptions() as $optionItem) {
                if ($optionItem->getCode() == 'ampromo_rule') {
                    return true;
                }
            }
        }
        return false;
    }

    //Customize for MAGENTO-519: Next Day delivery option
    public function getCarrierName($carrierCode)
    {
        if (strcmp($carrierCode, 'bkk_nextday') == 0 &&
            strlen(trim(Mage::getStoreConfig('carriers/'.$carrierCode.'/special_shipping_fee'))) &&
            Mage::getStoreConfig('carriers/'.$carrierCode.'/minimum_spend') >= 0 &&
            $this->getAddress()->getData('base_subtotal_incl_tax') >= Mage::getStoreConfig('carriers/'.$carrierCode.'/minimum_spend') &&
            Mage::getStoreConfig('carriers/'.$carrierCode.'/special_shipping_fee') >= 0) {
            $name = Mage::getStoreConfig('carriers/'.$carrierCode.'/title');
            if (Mage::getStoreConfig('carriers/'.$carrierCode.'/special_shipping_fee') > 0) {
                $surfix = Mage::getStoreConfig('carriers/'.$carrierCode.'/special_shipping_fee') . " " . $this->__('Baht');
            } else {
                $surfix = " " . $this->__('FREE');
            }
            $fullName = '<span style="text-decoration: line-through">' . $name . '</span> ' . $surfix;
            return $fullName;
        }

        return parent::getCarrierName($carrierCode);
    }
}
