<?php
class EMST_One23BAY_Block_Form extends Mage_Payment_Block_Form
{
    protected function _construct() {
		parent::_construct();
        $this->setTemplate('one23BAY/form.phtml');        
    }
	public function getDescription() {
		return $this->getMethod()->getConfigData('description');
	}
}
