<?php

$installer = $this;
$installer->startSetup();
$installer->run(" 
CREATE TABLE {$this->getTable('netsuite_sales_order')} (
	`order_increment_id` INT(11) UNSIGNED NOT NULL,
	`created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`status` TINYINT(1) NOT NULL DEFAULT '0',
	PRIMARY KEY (`order_increment_id`),
	CONSTRAINT netsuite_sale_order_unique_index UNIQUE INDEX (`order_increment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Netsuite Sales Order Export By CronJob' ; ");
$installer->endSetup();
