<?php

$installer = $this;
$installer->startSetup();
$installer->run("
CREATE TABLE {$this->getTable('sales_flat_order_item_bundles')} (
	`item_id` INT(11) NOT NULL AUTO_INCREMENT,
	`order_id` INT NOT NULL,
    `store_id` INT NULL,
	`created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`product_id` INT NULL,
    `bundle_sku` TEXT NULL,
    `bundle_qty` TEXT NULL,
    `bundle_price` TEXT NULL,
	PRIMARY KEY (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales order items for bundle products';");
$installer->endSetup();