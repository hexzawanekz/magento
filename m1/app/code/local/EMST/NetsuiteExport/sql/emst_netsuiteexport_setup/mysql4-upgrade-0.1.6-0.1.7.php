<?php

$installer = $this;
$installer->startSetup();
$installer->_conn->addColumn($this->getTable('sales_flat_order'), 'error_message', 'TEXT NULL');
$installer->_conn->addColumn($this->getTable('sales_flat_order_grid'), 'error_message', 'TEXT NULL');
$installer->endSetup();