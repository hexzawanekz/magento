<?php
/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade TableRate to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_TableRate
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
*/

$installer = $this;

$installer->startSetup();
$this->_conn->addColumn($this->getTable('sales/quote_item'), 'netsuite_item_name', 'varchar(250)');
$this->_conn->addColumn($this->getTable('sales/order_item'), 'netsuite_item_name', 'varchar(250)');
$this->_conn->addColumn($this->getTable('sales/invoice_item'), 'netsuite_item_name', 'varchar(250)');
$this->_conn->addColumn($this->getTable('sales/shipment_item'), 'netsuite_item_name', 'varchar(250)');
$installer->endSetup();