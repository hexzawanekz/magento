<?php

/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Sales to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_Sales
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
 */
class EMST_NetsuiteExport_Model_Export1
{
    protected $_helper = null;
    protected $_offlineState = EMST_Sales_Model_Order::STATE_NEW;
    protected $_onlineState = EMST_Sales_Model_Order::STATE_INVOICED;
    protected $_offlineMethod = 'cashondelivery';
    protected $_countryNames = false;
    protected $_timeComfig = false;

    protected function _getHelper()
    {
        if (!$this->_helper) {
            $this->_helper = Mage::helper('netsuiteexport');
        }
        return $this->_helper;
    }

    protected function _getTimeConfig() {
        if(!$this->_timeComfig) {
            $this->_timeComfig = Mage::getStoreConfig('netsuiteexport/cronconfig/timefilter');
        }
        return $this->_timeComfig;
    }

    protected function _getOffLineSalesOrder()
    {
        $salesOrder = Mage::getModel('sales/order')->getCollection()
                ->join(
                        array('payment' => 'sales/order_payment'), 'main_table.entity_id = payment.parent_id',
                        array('payment_method' => 'payment.method'));


        $salesOrder->addFieldToFilter('main_table.state', array('eq' => $this->_offlineState))
                   ->addFieldToFilter('main_table.status', array('neq' => 'holded'))
                   ->addFieldToFilter('main_table.is_exported', array('eq' => 0))
                   ->addFieldToFilter('payment.method', array('eq' => $this->_offlineMethod))
                   ->addFieldToFilter('main_table.created_at', array('gteq' => new Zend_Db_Expr('DATE_SUB(\''.Varien_Date::now(true).'\', INTERVAL ' . intval($this->_getTimeConfig()) . ' DAY)')));


        return $salesOrder;
    }

    protected function _getOnLineSalesOrder()
    {
        $salesOrder = Mage::getModel('sales/order')->getCollection()
                ->join(
                        array('payment' => 'sales/order_payment'), 'main_table.entity_id = payment.parent_id',
                        array('payment_method' => 'payment.method'));

        $salesOrder->addFieldToFilter('main_table.state', array('eq' => $this->_onlineState))
                   ->addFieldToFilter('main_table.status', array('neq' => 'holded'))
                   ->addFieldToFilter('main_table.is_exported', array('eq' => 0))
                   ->addFieldToFilter('payment.method', array('nin' => array($this->_offlineMethod)))
                   ->addFieldToFilter('main_table.created_at', array('gteq' => new Zend_Db_Expr('DATE_SUB(\''.Varien_Date::now(true).'\', INTERVAL ' . intval($this->_getTimeConfig()) . ' DAY)')));;


        return $salesOrder;
    }

    protected function _getSalesOrderByIds($orderIds)
    {
        $salesOrder = Mage::getModel('sales/order')->getCollection()
                ->join(
                        array('payment' => 'sales/order_payment'), 'main_table.entity_id = payment.parent_id',
                        array('payment_method' => 'payment.method'));

        $salesOrder->addFieldToFilter('main_table.entity_id', array('in' => $orderIds));
        return $salesOrder;
    }

    protected function _getCountryNames() {
        if(!$this->_countryNames) {
            $countrieNames = array();
            $countries = Mage::getModel('directory/country')
                    ->getResourceCollection()
                    ->toOptionArray();

            foreach ($countries as $country) {
                $countrieNames[$country['value']] = $country['label'];
            }
            $this->_countryNames = $countrieNames;
        }
        return $this->_countryNames;
    }

    protected function _convertOrderToArray($orders) {
        $datas = array();
        foreach ($orders as $order) {
            $address = $order->getShippingAddress();
            $payment = $order->getPayment()->getMethod();
            $orderDate = $order->getCreatedAtStoreDate()->toString('d/m/Y', 'php');
            $invoiceDate = false;
            $streets = $address->getStreet();

            if($order->hasInvoice()) {
                foreach($order->getAllInvoices as $invoice) {
                    $invoiceDate = $invoice->getCreatedAtStoreDate()->toString('d/m/Y', 'php');
                    break;
                }
            }

            foreach($order->getAllItems() as $item) {
                if($item->getProductType() == 'simple') {

                    $bundleEnable = $this->_getAttributeValue($item->getProductId(), 'bundle_enable');
                    $bundleSku = $item->getSku();
                    $bundleQty = "1";

                    if($parentItem = $item->getParentItem()) {
                        $bundlePrice = $parentItem->getPriceInclTax();
                    } else {
                        $bundlePrice = $item->getPriceInclTax();
                    }


                    if($bundleEnable) {
                        $bundleSku = $this->_getAttributeValue($item->getProductId(), 'bundle_sku');
                        $bundleQty = $this->_getAttributeValue($item->getProductId(), 'bundle_qty');
                        $bundlePrice = $this->_getAttributeValue($item->getProductId(), 'bundle_price');

                        if(!$bundleSku) {
                            $bundleSku = $item->getSku();
                        }

                        if(!$bundleQty) {
                            $bundleQty = "1";
                        }

                        if(!$bundlePrice) {
                            if($parentItem = $item->getParentItem()) {
                                $bundlePrice = $parentItem->getPriceInclTax();
                            } else {
                                $bundlePrice = $item->getPriceInclTax();
                            }
                        }
                    }

                    $bundleSkus = explode('#', $bundleSku);
                    $bundleQtys = explode('#', $bundleQty);
                    $bundlePrices = explode('#', $bundlePrice);

                    Mage::log($bundleSkus, null, 'bundle.log');
                    Mage::log($bundleQtys, null, 'bundle.log');
                    Mage::log($bundlePrices, null, 'bundle.log');

                    foreach($bundleSkus as $key => $value) {
                        $temp = array();
                        $temp['order_id'] = $this->_setCsvFormat($order->getIncrementId());
                        $temp['order_date'] = $this->_setCsvFormat($orderDate);
                        $temp['invoice_date'] = $this->_setCsvFormat((($invoiceDate)?$invoiceDate:$orderDate));
                        $temp['email'] = $this->_setCsvFormat($order->getCustomerEmail());
                        $temp['customer_id'] = $this->_setCsvFormat($order->getCustomerId());
                        //$temp['external_customer_id'] = $temp['customer_id'];
                        $temp['customer_firstname'] = $this->_setCsvFormat($order->getCustomerFirstname());
                        $temp['customer_lastname'] = $this->_setCsvFormat($order->getCustomerLastname());
                        $temp['shipping_addressee'] = $this->_setCsvFormat((($address->getFirstname())? $address->getFirstname():'').' '.(($address->getLastname())? $address->getLastname():''));

                        $temp['shipping_address_line_1'] = $this->_setCsvFormat($this->_getAddress($streets, 1));
                        $temp['shipping_address_line_2'] = $this->_setCsvFormat($this->_getAddress($streets, 2));

                        $temp['shipping_address_city'] = $this->_setCsvFormat($address->getCity());
                        $temp['shipping_address_state'] = $this->_setCsvFormat($address->getRegion());
                        $temp['shipping_address_country'] = $this->_setCsvFormat($this->_getCountryNames()[$address->getCountryId()]);
                        $temp['shipping_address_postal_code'] = $this->_setCsvFormat($address->getPostcode());
                        $temp['shipping_address_postal_phone'] = $this->_setCsvFormat($address->getTelephone());

                        //$temp['item_id'] = $this->_setCsvFormat($item->getSku());
                        //$temp['item_qty'] = $this->_setCsvFormat($item->getQtyOrdered());

                        $qty = $bundleQtys[$key] * $item->getQtyOrdered();
                        $temp['item_id'] = $this->_setCsvFormat($value);
                        $temp['item_qty'] = $this->_setCsvFormat($qty);


                        //$temp['rate'] = $this->_setCsvFormat($bundlePrices[$key]);
                        $temp['gross_total'] = $this->_setCsvFormat($bundlePrices[$key] * $qty);

                        //if($bundleEnable) {
                        //    $temp['rate'] = $this->_setCsvFormat(0);
                        //}

                        $temp['payment_type'] = $this->_setCsvFormat($this->_getHelper()->getPaymentMethod($order->getPaymentMethod()));
                        $temp['shipping_type'] = $this->_setCsvFormat($this->_getHelper()->getShippingMethod($order->getShippingMethod()));

                        $temp['main_sales_channel'] = $this->_setCsvFormat($this->_getHelper()->getChannel($order->getMainChannel()));
                        $temp['sub_sales_channel'] = $this->_setCsvFormat($this->_getHelper()->getChannel($order->getSubChannel()));
                        $temp['sales_channel_source'] = $this->_setCsvFormat($this->_getHelper()->getChannel($order->getChannelSource()));
                        $datas[] = implode(',', $temp);
                    }
                }
            }

            if($order->getCodFee()) {
                $datas[] = $this->_createOptionData($order, EMST_NetsuiteExport_Helper_Data::ITEM_TYPE_COD_FEE);
            }

            if($order->getRewardpoints() && (float)$order->getRewardpoints() > 0.001) {
               $datas[] = $this->_createOptionData($order, EMST_NetsuiteExport_Helper_Data::ITEM_TYPE_REWARD_POINTS);
            }

            if($order->getShippingInclTax()) {
               $datas[] = $this->_createOptionData($order, EMST_NetsuiteExport_Helper_Data::ITEM_TYPE_SHIPPING_AMOUNT);
            }

            $discount = (abs($order->getDiscountAmount()) - abs($order->getRewardpoints()));
            if((float)$discount> 0.001){
                $datas[] = $this->_createOptionData($order, EMST_NetsuiteExport_Helper_Data::ITEM_TYPE_DISCOUNT_RULES);
            }

            //$order->setIsExported(1);
            //$order->save();
        }

        return $datas;
    }

    protected function _getAddress($streets, $row) {
        $streets = (is_array($streets)) ? $streets : explode("\n", $streets);
        $itemCount =  floor(count($streets) / 2);
        $itemCount = ($itemCount < 1) ? 1 : $itemCount;

        if($row == 1) {
            return preg_replace('~\r\n?~', ' ', implode("\r\n", array_slice($streets, 0, $itemCount)));
        } else {
            return preg_replace('~\r\n?~', ' ', implode("\r\n", array_slice($streets, $itemCount)));
        }
    }

    protected function _createOptionData($order, $type) {
        $address = $order->getShippingAddress();
        $payment = $order->getPayment()->getMethod();
        $orderDate = $order->getCreatedAtStoreDate()->toString('d/m/Y', 'php');
        $invoiceDate = false;
        $streets = $address->getStreet();

        if($order->hasInvoice()) {
            foreach($order->getAllInvoices as $invoice) {
                $invoiceDate = $invoice->getCreatedAtStoreDate()->toString('d/m/Y', 'php');
                break;
            }
        }

        $temp = array();
        $temp['order_id'] = $this->_setCsvFormat($order->getIncrementId());
        $temp['order_date'] = $this->_setCsvFormat($orderDate);
        $temp['invoice_date'] = $this->_setCsvFormat((($invoiceDate)?$invoiceDate:$orderDate));
        $temp['email'] = $this->_setCsvFormat($order->getCustomerEmail());
        $temp['customer_id'] = $this->_setCsvFormat($order->getCustomerId());
        //$temp['external_customer_id'] = $temp['customer_id'];
        $temp['customer_firstname'] = $this->_setCsvFormat($order->getCustomerFirstname());
        $temp['customer_lastname'] = $this->_setCsvFormat($order->getCustomerLastname());
        $temp['shipping_addressee'] = $this->_setCsvFormat((($address->getFirstname())? $address->getFirstname():'').' '.(($address->getLastname())? $address->getLastname():''));

        $temp['shipping_address_line_1'] = $this->_setCsvFormat($this->_getAddress($streets, 1));
        $temp['shipping_address_line_2'] = $this->_setCsvFormat($this->_getAddress($streets, 2));

        $temp['shipping_address_city'] = $this->_setCsvFormat($address->getCity());
        $temp['shipping_address_state'] = $this->_setCsvFormat($address->getRegion());
        $temp['shipping_address_country'] = $this->_setCsvFormat($this->_getCountryNames()[$address->getCountryId()]);
        $temp['shipping_address_postal_code'] = $this->_setCsvFormat($address->getPostcode());
        $temp['shipping_address_postal_phone'] = $this->_setCsvFormat($address->getTelephone());

        $itemId = '';
        $rate = 0;
        if($type == EMST_NetsuiteExport_Helper_Data::ITEM_TYPE_COD_FEE) {
            //$itemId = Mage::getStoreConfig('netsuiteexport/salesexport/codfee_name');
            //if(!$itemId) {
                $itemId = "COD Charges-WHS";
            //}
            $rate = $order->getCodFee() + $order->getCodTaxAmount();
        } elseif($type == EMST_NetsuiteExport_Helper_Data::ITEM_TYPE_SHIPPING_AMOUNT) {
            //$itemId = Mage::getStoreConfig('netsuiteexport/salesexport/shipping_name');
            //if(!$itemId) {
                $itemId = "All Shipping Charges-WHS";
            //}
            $rate = $order->getShippingInclTax();
        } elseif($type == EMST_NetsuiteExport_Helper_Data::ITEM_TYPE_REWARD_POINTS) {
            $itemId = Mage::getStoreConfig('netsuiteexport/salesexport/reward_name');
            if(!$itemId) {
                $itemId = "Reward Points";
            }
            $rate = - $order->getRewardpoints();
        } elseif($type == EMST_NetsuiteExport_Helper_Data::ITEM_TYPE_DISCOUNT_RULES) {
            $ruleIds = $order->getAppliedRuleIds();
            $ruleName = $order->getCouponRuleName();
            $itemId = 'ALL WHS Discount';

            /*if($ruleName) {
                $itemId = $ruleName;
            } elseif((int)$ruleIds) {
                $rule = Mage::getModel('salesrule/rule')->load($ruleIds);
                if($rule->getName()) {
                    $itemId = $rule->getName();
                }
            } else {
                $itemId = $otherDiscount;
            }*/

            $rate = - (abs($order->getDiscountAmount()) - abs($order->getRewardpoints()));
        }

        $temp['item_id'] = $this->_setCsvFormat($itemId);
        $temp['item_qty'] = $this->_setCsvFormat(1);
        //$temp['rate'] = $this->_setCsvFormat($rate);
        $temp['gross_total'] = $this->_setCsvFormat($rate);

        $temp['payment_type'] = $this->_setCsvFormat($this->_getHelper()->getPaymentMethod($order->getPaymentMethod()));
        $temp['shipping_type'] = $this->_setCsvFormat($this->_getHelper()->getShippingMethod($order->getShippingMethod()));

        $temp['main_sales_channel'] = $this->_setCsvFormat($this->_getHelper()->getChannel($order->getMainChannel()));
        $temp['sub_sales_channel'] = $this->_setCsvFormat($this->_getHelper()->getChannel($order->getSubChannel()));
        $temp['sales_channel_source'] = $this->_setCsvFormat($this->_getHelper()->getChannel($order->getChannelSource()));

        return implode(',', $temp);
    }

    protected function _setCsvFormat($data) {
        return '"' . str_replace(array('"', '\\'), array('""', '\\\\'), $data) . '"';
    }

    protected function _getHeaders() {
        return array('Order #', 'Order Date', 'Invoice Date', 'Email', 'Customer ID', 'Customer First Name', 'Customer Last Name', 'Shipping Addressee', 'Shipping Address Line 1', 'Shipping Address Line 2', 'Shipping Address City', 'Shipping Address State/Province', 'Shipping Address Country', 'Shipping Address Postal Code', 'Shipping Address Phone', 'Item  ID', 'Item Qty', 'Gross Total', 'Payment Type', 'Shipping Type', 'Main Sales Channel(Optional)', 'Sub Sales Channel(Optional)', 'Sales Channel Source(Optional)');
        //return array('Order #', 'Order Date', 'Invoice Date', 'Email', 'Customer ID', 'External Customer ID', 'Customer First Name', 'Customer Last Name', 'Shipping Addressee', 'Shipping Address Line 1', 'Shipping Address Line 2', 'Shipping Address City', 'Shipping Address State/Province', 'Shipping Address Country', 'Shipping Address Postal Code', 'Shipping Address Phone', 'Item ID', 'Item Qty', 'Rate', 'Gross Total', 'Payment Type', 'Shipping Type', 'Main Sales Channel(Optional)', 'Sub Sales Channel(Optional)', 'Sales Channel Source(Optional)');
    }

    public function ExportCsvData()
    {
        //if (!$this->_getHelper()->isCronActive()) {
        //    return;
        //}

        $onlineOrders = $this->_getOnLineSalesOrder();
        $offlineOrders = $this->_getOffLineSalesOrder();

        if($onlineOrders->getSize() > 0 || $offlineOrders->getSize() > 0) {
            $header[] = implode(',', $this->_getHeaders());
            $onlineOrders = $this->_convertOrderToArray($onlineOrders);
            $offlineOrders = $this->_convertOrderToArray($offlineOrders);
            $currentDate = Varien_Date::now();
            //$currentDate = substr($currentDate, 0, strlen($currentDate) - 2);
            $currentDate = str_replace(array('-', ':', ' '), array('', '', '-'), $currentDate);

            $io = $this->getFile();
            $path = $this->getOutputFolder();
            $io->write($path.'4SO-'.$currentDate.'.csv', implode(PHP_EOL, array_merge($header, $onlineOrders, $offlineOrders)));
            $io->close();
            $this->_ftpFile($path.'4SO-'.$currentDate.'.csv');
        }
    }

    public function ManuallyExport($orderIds) {

        $orders = $this->_getSalesOrderByIds($orderIds);

        if($orders->getSize() > 0) {
            $header[] = implode(',', $this->_getHeaders());
            $manuallyOrders = $this->_convertOrderToArray($orders);
            $currentDate = Varien_Date::now();
            //$currentDate = substr($currentDate, 0, strlen($currentDate) - 2);
            $currentDate = str_replace(array('-', ':', ' '), array('', '', '-'), $currentDate);

            $io = $this->getFile();
            $path = $this->getOutputFolder();
            $io->write($path.'6SO-'.$currentDate.'.csv', implode(PHP_EOL, array_merge($header, $manuallyOrders)));
            $io->close();
            $this->_ftpFile($path.'6SO-'.$currentDate.'.csv');
            return true;
        }
        return false;
    }

    protected function getOutputFolder()
    {
        return Mage::getBaseDir('var') . DS . 'export' . DS . 'sales_order'. DS;
    }

    protected function getFile() {
        $io = new Varien_Io_File();
        $io->setAllowCreateFolders(true);
        $io->open(array('path' => $this->getOutputFolder()));
        return $io;
    }


    protected function _ftpFile($path) {
        try {
            /*$host = Mage::getStoreConfig('netsuiteexport/ftpexport/host');
            $usrname = Mage::getStoreConfig('netsuiteexport/ftpexport/usrname');
            $password = Mage::getStoreConfig('netsuiteexport/ftpexport/password');
            $cd = Mage::getStoreConfig('netsuiteexport/ftpexport/path');
            */

            $host = '54.254.137.655';
            $usrname = 'whatsnew_ftp';
            $password = 'Ube2jzny5D';
            $cd = '/incoming/Import/TempSO/';

            $sftp = new Varien_Io_Sftp();
            $sftp->open(
                    array(
                            'host'      => $host,
                            'username'  => $usrname,
                            'password'  => $password,
                        )
                    );

            $pos = strrpos($path, DS);
            if($pos !== false) {
                $fileName = substr($path, $pos + 1, strlen($path) - ($pos + 1));
            }

            $content = @file_get_contents($path);

            if($cd) {
                $sftp->cd($cd);
            }

            $sftp->write($fileName, $content);
            $sftp->close();
        } catch (Exception $e) {
          Mage::log($e->getMessage(), null, 'sales_order_export.log');
        }
    }

    /**
     * Get Raw attribute value
     *
     * @param int    $productId     product id
     * @param string $attributeCode code of attribute
     *
     * @return mixed
     */
    protected function _getAttributeValue($productId, $attributeCode)
    {
        return Mage::getResourceModel('catalog/product')
                        ->getAttributeRawValue($productId, $attributeCode, Mage_Core_Model_App::ADMIN_STORE_ID);
    }

}

