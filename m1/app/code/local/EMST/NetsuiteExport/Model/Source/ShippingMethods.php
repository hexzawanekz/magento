<?php
class EMST_NetsuiteExport_Model_Source_ShippingMethods
{
	protected $_shippingMethods = null;
	
	public function toOptionArray() {
        if (!$this->_shippingMethods) {
            $this->_shippingMethods = Mage::helper('netsuiteexport')->getStoreShippingMethods();
        }
        $options = array();
        $options[] = array(
               'value' => '',
               'label' => Mage::helper('adminhtml')->__('-- Please Select --')
            );
        foreach ($this->_shippingMethods as $code=>$label) {
            $options[] = array(
               'value' => $code,
               'label' => $label
            );
        }
        return $options;
    }
}
