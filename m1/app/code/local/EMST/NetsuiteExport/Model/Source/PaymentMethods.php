<?php
class EMST_NetsuiteExport_Model_Source_PaymentMethods
{
	protected $_paymentMethods = null;
	
	public function toOptionArray() {
        if (!$this->_paymentMethods) {
            $this->_paymentMethods = Mage::helper('netsuiteexport')->getStorePaymentMethods();
        }
        $options = array();
        $options[] = array(
               'value' => '',
               'label' => Mage::helper('adminhtml')->__('-- Please Select --')
            );
        foreach ($this->_paymentMethods as $code=>$label) {
            $options[] = array(
               'value' => $code,
               'label' => $label
            );
        }
        return $options;
    }
}
