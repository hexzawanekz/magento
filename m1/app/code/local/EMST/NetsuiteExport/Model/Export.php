<?php

/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Sales to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_Sales
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
 */

require_once(Mage::getBaseDir().DS.'xlsxwriter.class.php');

class EMST_NetsuiteExport_Model_Export
{
    protected $_helper = null;
    protected $_offlineState = EMST_Sales_Model_Order::STATE_NEW;
    protected $_onlineState = EMST_Sales_Model_Order::STATE_INVOICED;
    protected $_offlineMethod = 'cashondelivery';
    protected $_countryNames = false;
    protected $_timeComfig = false;

    // Temporary data
    protected $_tempShippingDesc = array();
    protected $_tempPaymentMethod = array();
    protected $_tempNewCustomer = array();

    protected $_processingOrderIds1 = array();
    protected $_processingOrderIds2 = array();
    protected $_successOrderIds = array();

    // Resource
    protected $_resource = null;
    protected $_writeConnection = null;
    protected $_productResource = null;

    // Table names
    protected $_tblOrder = '';
    protected $_tblOrderGrid = '';

    /**
     * Get order table name
     */
    protected function _getOrderTableName()
    {
        if($this->_tblOrder == '')
            $this->_tblOrder = $this->_getResource()->getTableName('sales/order');
        return $this->_tblOrder;
    }

    /**
     * Get order grid table name
     */
    protected function _getOrderGridTableName()
    {
        if($this->_tblOrderGrid == '')
            $this->_tblOrderGrid = $this->_getResource()->getTableName('sales/order_grid');
        return $this->_tblOrderGrid;
    }

    /**
     * Get resource
     */
    protected function _getResource()
    {
        if(is_null($this->_resource))
            $this->_resource = Mage::getSingleton('core/resource');
        return $this->_resource;
    }

    /**
     * Get write connection
     */
    protected function _getWriteConnection()
    {
        if(is_null($this->_writeConnection))
            $this->_writeConnection = $this->_getResource()->getConnection('core_write');
        return $this->_writeConnection;
    }

    /**
     * Save order
     */
    protected function _saveOrder($increment_id, $error_message = null, $is_exported = -1)
    {
        try{
            if(!is_null($error_message) || $is_exported != -1){
                if(!is_null($error_message)){
                    $error_message = addslashes($error_message);
                }
                $queryStr1 = 'UPDATE '.$this->_getOrderTableName().' SET '.(!is_null($error_message)?"`error_message` = '".$error_message."'".($is_exported!=-1?", ":""):"").($is_exported!=-1?"`is_exported` = ".$is_exported:"")." WHERE `increment_id` = ".$increment_id;
                $queryStr2 = 'UPDATE '.$this->_getOrderGridTableName().' SET '.(!is_null($error_message)?"`error_message` = '".$error_message."'".($is_exported!=-1?", ":""):"").($is_exported!=-1?"`is_exported` = ".$is_exported:"")." WHERE `increment_id` = ".$increment_id;
                $this->_getWriteConnection()->query($queryStr1);
                $this->_getWriteConnection()->query($queryStr2);
            }
            return true;
        }catch (Exception $e){
            Mage::log($e->getMessage(), null, 'sales_order_export.log');
            return false;
        }
    }

    /**
     * Get product resource model
     */
    protected function _getProductResource()
    {
        if (!$this->_productResource)
            $this->_productResource = Mage::getResourceModel('catalog/product');
        return $this->_productResource;
    }

    /**
     * Get helper model
     */
    protected function _getHelper()
    {
        if (!$this->_helper) {
            $this->_helper = Mage::helper('netsuiteexport');
        }
        return $this->_helper;
    }

    /**
     * Get time config
     */
    protected function _getTimeConfig()
    {
        if (!$this->_timeComfig) {
            $this->_timeComfig = Mage::getStoreConfig('netsuiteexport/cronconfig/timefilter', Mage_Core_Model_App::ADMIN_STORE_ID);
        }
        return $this->_timeComfig;
    }

    /**
     * Get offline sales order
     */
    protected function _getOffLineSalesOrder()
    {
        /** @var Mage_Sales_Model_Resource_Order_Collection $salesOrder */
        $salesOrder = Mage::getModel('sales/order')->getCollection()
            ->join(
                array('payment' => 'sales/order_payment'), 'main_table.entity_id = payment.parent_id',
                array('payment_method' => 'payment.method'));


        $salesOrder->addFieldToFilter('main_table.state', array('eq' => $this->_offlineState))
            ->addFieldToFilter('main_table.status', array('nlike' => 'holded'))
            ->addFieldToFilter('main_table.status', array('nlike' => 'invalid'))
            ->addFieldToFilter('main_table.status', array('nlike' => 'canceled'))
            ->addFieldToFilter('main_table.status', array('nlike' => 'closed'))
            ->addFieldToFilter('main_table.is_exported', array('eq' => 0))
            ->addFieldToFilter('payment.method', array('in' => array($this->_offlineMethod, 'nocost')))
            ->addFieldToFilter('main_table.created_at', array('gteq' => new Zend_Db_Expr('DATE_SUB(\'' . Varien_Date::now(true) . '\', INTERVAL ' . intval($this->_getTimeConfig()) . ' DAY)')))
            ->addFieldToFilter('main_table.error_message', array(
                array('eq' => ''),
                array('null' => true),
            ));

        if ($salesOrder->count() > 0) {
            // save processing orders into temp array
            $this->_processingOrderIds1 = $salesOrder->getAllIds();
            // mark orders as processing
            $this->_markAsProcessing($this->_processingOrderIds1);
        }

        return $salesOrder;
    }

    /**
     * Get online sales order
     */
    protected function _getOnLineSalesOrder()
    {
        /** @var Mage_Sales_Model_Resource_Order_Collection $salesOrder */
        $salesOrder = Mage::getModel('sales/order')->getCollection()
            ->join(
                array('payment' => 'sales/order_payment'), 'main_table.entity_id = payment.parent_id',
                array('payment_method' => 'payment.method'));

        $salesOrder->addFieldToFilter('main_table.state', array('eq' => $this->_onlineState))
            ->addFieldToFilter('main_table.status', array('nlike' => 'holded'))
            ->addFieldToFilter('main_table.status', array('nlike' => 'invalid'))
            ->addFieldToFilter('main_table.status', array('nlike' => 'canceled'))
            ->addFieldToFilter('main_table.status', array('nlike' => 'closed'))
            ->addFieldToFilter('main_table.status', array('nlike' => 'pending'))
            ->addFieldToFilter('main_table.status', array('nlike' => 'pending_payment'))
            ->addFieldToFilter('main_table.is_exported', array('eq' => 0))
            ->addFieldToFilter('payment.method', array('nin' => array($this->_offlineMethod)))
            ->addFieldToFilter('main_table.created_at', array('gteq' => new Zend_Db_Expr('DATE_SUB(\'' . Varien_Date::now(true) . '\', INTERVAL ' . intval($this->_getTimeConfig()) . ' DAY)')))
            ->addFieldToFilter('main_table.error_message', array(
                array('eq' => ''),
                array('null' => true),
            ));

        if ($salesOrder->count() > 0) {
            // save processing orders into temp array
            $this->_processingOrderIds2 = $salesOrder->getAllIds();
            // mark orders as processing
            $this->_markAsProcessing($this->_processingOrderIds2);
        }

        return $salesOrder;
    }

    /**
     * Get sales order by ids
     */
    protected function _getSalesOrderByIds($orderIds)
    {
        /** @var Mage_Sales_Model_Resource_Order_Collection $salesOrder */
        $salesOrder = Mage::getModel('sales/order')->getCollection()
            ->join(
                array('payment' => 'sales/order_payment'), 'main_table.entity_id = payment.parent_id',
                array('payment_method' => 'payment.method'));

        $salesOrder->addFieldToFilter('main_table.entity_id', array('in' => $orderIds))
            ->addFieldToFilter('main_table.status', array('nlike' => 'holded'))
            ->addFieldToFilter('main_table.status', array('nlike' => 'invalid'))
            ->addFieldToFilter('main_table.status', array('nlike' => 'canceled'))
            ->addFieldToFilter('main_table.status', array('nlike' => 'closed'));
            //->addFieldToFilter('main_table.status', array('nlike' => 'pending'))
            //->addFieldToFilter('main_table.status', array('nlike' => 'pending_payment'));

        $select = $salesOrder->getSelect();
        $select->where("IF(`main_table`.`state` = '{$this->_onlineState}' AND `payment`.`method` NOT IN('{$this->_offlineMethod}'), `main_table`.`status`, FALSE) NOT IN('pending','pending_payment')");

        // save processing orders into temp array
        $this->_processingOrderIds1 = $salesOrder->getAllIds();

        return $salesOrder;
    }

    /**
     * Get country name
     */
    protected function _getCountryNames()
    {
        if (!$this->_countryNames) {
            $countrieNames = array();
            $countries = Mage::getModel('directory/country')
                ->getResourceCollection()
                ->toOptionArray();

            foreach ($countries as $country) {
                $countrieNames[$country['value']] = $country['label'];
            }
            $this->_countryNames = $countrieNames;
        }
        return $this->_countryNames;
    }

    /**
     * Convert orders to array, and send to NetSuite
     */
    protected function _convertOrderToArray($orders)
    {
        // don't do anything from 1:30 AM to after 3:30 AM
        date_default_timezone_set('Asia/Bangkok');
        $current_time = explode(':', date("H:i", time()));
        $current_time = intval($current_time[0] . $current_time[1]);
        if ($current_time > 130 && $current_time < 330) {
            Mage::log('Now is limited time, Cannot send SO during this moment', null, 'data.log');
            return array();
        }
        // never send SO exceeds this limit number
        $limit = 50; //default
        $configLimit = Mage::getStoreConfig('netsuiteexport/cronconfig/netsuitelimit', Mage_Core_Model_App::ADMIN_STORE_ID); // config limit
        // if there config limit, then not use default anymore
        if (is_int(trim($configLimit))) {
            $limit = $configLimit;
        }
        $sent = 1;
        // return all order details
        $allExportedOrders = array();
        // loop orders
        foreach ($orders as $order) {
            /** @var Mage_Sales_Model_Order $order */

            // stop in exceeds limit
            if ($sent == $limit) break;
            $sent++;
            $netsuiteorder = null;
            unset($netsuiteorder);
            $address = is_object($order->getShippingAddress()) ? $order->getShippingAddress() : $order->getBillingAddress();
            $shippingMethod = $address->getShippingMethod();
            $payment = $order->getPayment()->getMethod();
            $orderDate = $order->getCreatedAtStoreDate()->toString('d/m/Y', 'php');
            $invoiceDate = false;
            $streets = $address->getStreet();
            $netsuiteorder['increment_id'] = $order->getIncrementId();
            Mage::log('----Start sending progress - order id = ' . $netsuiteorder['increment_id'] . '----', null, 'data.log');
            // create new customer for Netsuite if doesn't exist
            $netsuiteorder['customer_id'] = $order->getData('customer_id');
            $resultFlag = true;
            unset($result);
            if ($this->_verifyCustomer($netsuiteorder['customer_id']) === 'false') {
                $result = $this->_sendCustomerDataToNetSuite($netsuiteorder['customer_id']);
                // if we got Error word inside the return string -> skip sending this order
                if (is_null($result) || $result == ''){
                    $this->_saveOrder($order->getIncrementId(), 'Customer Error : Unknown');
                    Mage::log("Fail in inserting new customer " . $netsuiteorder['customer_id'] . "of order " . $netsuiteorder['increment_id'] . " --" . $result, null, 'data.log');
                    continue;
                } elseif (strpos($result, 'Error')) {
                    $this->_saveOrder($order->getIncrementId(), 'Customer '. str_replace('"', '', $result));
                    Mage::log("Fail in inserting new customer " . $netsuiteorder['customer_id'] . "of order " . $netsuiteorder['increment_id'] . " --" . $result, null, 'data.log');
                    continue;
                }
            }
            $netsuiteorder['recordtype'] = 'salesorder'; // add required data from Netsuite
            $netsuiteorder['order_date'] = $orderDate; // add required data from Netsuite
            $netsuiteorder['invoice_date'] = ($invoiceDate) ? $invoiceDate : $orderDate;
            $netsuiteorder['email'] = $order->getCustomerEmail();
            $netsuiteorder['customer_firstname'] = $order->getCustomerFirstname();
            $netsuiteorder['customer_lastname'] = $order->getCustomerLastname();
            $shipping_addressee = (($address->getFirstname()) ? $address->getFirstname() : '') . ' ' . (($address->getLastname()) ? $address->getLastname() : '');
            $netsuiteorder['shipping_addressee'] = $shipping_addressee;
            $netsuiteorder['shipping_address_line_1'] = $this->_getAddress($streets, 1);
            $netsuiteorder['shipping_address_line_2'] = $this->_getAddress($streets, 2);
            $netsuiteorder['shipping_address_city'] = $address->getCity();
            $netsuiteorder['shipping_address_state'] = $address->getRegion();
            $netsuiteorder['region'] = $this->_getCountryNames()[$address->getCountryId()];
            $netsuiteorder['postcode'] = $address->getPostcode();
            $netsuiteorder['telephone'] = $address->getTelephone();
            $netsuiteorder['main_sales_channel'] = $this->_getHelper()->getChannel($order->getMainChannel());
            $netsuiteorder['sub_sales_channel'] = $this->_getHelper()->getChannel($order->getSubChannel());
            $netsuiteorder['sales_channel_source'] = $this->_getHelper()->getChannel($order->getChannelSource());
            $netsuiteorder['state'] = $order->getState();
            $netsuiteorder['status'] = $order->getStatus();
            $netsuiteorder['discount_amount'] = $order->getDiscountAmount();
            $netsuiteorder['total_qty_ordered'] = $order->getTotalQtyOrdered();
            $netsuiteorder['shipping_amount'] = $order->getData('shipping_amount');
            $netsuiteorder['subtotal'] = $order->getSubtotal();
            $netsuiteorder['grandtotal'] = $order->getGrandTotal();
            $netsuiteorder['shipping_type'] = '';
            $netsuiteorder['is_wholesale'] = $order->getIsWholesale();
            // match shipment with coresponding number
            $this->_tempShippingDesc[$order->getIncrementId()] = $order->getData('shipping_description');
            switch (strtolower(trim($order->getData('shipping_description')))) {
                case '150 baht':
                    $netsuiteorder['shipping_type'] = '3';
                    break;
                case 'next day delivery':
                    $netsuiteorder['shipping_type'] = '3';
                    break;
                case 'free shipping':
                    $netsuiteorder['shipping_type'] = '10';
                    break;
                case 'bangkok shipping 2-3 business days':
                    $netsuiteorder['shipping_type'] = '9';
                    break;
                case 'nationwide delivery 3-5 days':
                    $netsuiteorder['shipping_type'] = '6';
                    break;
                case 'bangkok only next day shipping':
                    $netsuiteorder['shipping_type'] = '5';
                    break;
                case 'pick up':
                    $netsuiteorder['shipping_type'] = '22';
                    break;
                case 'international':
                    $netsuiteorder['shipping_type'] = '26';
                    break;
                case 'international via thaipost':
                    $netsuiteorder['shipping_type'] = '28';
                    break;
                default:
                    $netsuiteorder['shipping_type'] = '9';
                    break;
            }

            // match payment with coresponding number
            $netsuiteorder['payment_type'] = '';
            $this->_tempPaymentMethod[$order->getIncrementId()] = $order->getData('payment_method');
            switch (strtolower(trim($order->getData('payment_method')))) {
                case 'cashondelivery':
                    $netsuiteorder['payment_type'] = '2';
                    break;
                case 'paypal_standard':
                    $netsuiteorder['payment_type'] = '1';
                    break;
                case '2c2p':
                    $netsuiteorder['payment_type'] = '4';
                    break;
                case 'normal2c2p':
                    $netsuiteorder['payment_type'] = '4';
                    break;
                case 'nocost':
                    $netsuiteorder['payment_type'] = '13';
                    break;
                case 'linepay':
                    $netsuiteorder['payment_type'] = '47';
                    break;
                default:
                    $netsuiteorder['payment_type'] = '11';
                    break;
            }
            $_storeName = $order->getData('store_name');
            $netsuiteorder['store'] = $this->_getStoreNumber($_storeName);

            if ($order->hasInvoice()) {
                foreach ($order->getAllInvoices as $invoice) {
                    $invoiceDate = $invoice->getCreatedAtStoreDate()->toString('d/m/Y', 'php');
                    break;
                }
            }

            foreach ($order->getAllItems() as $item) {
                /** @var Mage_Sales_Model_Order_Item $item */

                if ($item->getProductType() == 'simple') {

                    $bundleEnable = $this->_getAttributeValue($item->getProductId(), 'bundle_enable');
                    $bundleSku = $item->getSku();
                    $bundleQty = "1";

                    if ($parentItem = $item->getParentItem()) {
                        $bundlePrice = $parentItem->getPriceInclTax();
                    } else {
                        $bundlePrice = $item->getPriceInclTax();
                    }

                    if ($bundleEnable) {
                        $product = Mage::getModel('catalog/product')->load($item->getProductId());
                        /** @var EMST_NetsuiteExport_Model_BundleItems $bundleObj */
                        $bundleObj = Mage::getSingleton('netsuiteexport/bundleItems');
                        if($item->getPrice() == $product->getPrice()){

                            if($bundleValues = $bundleObj->getBundleItemsInOrder($order->getId(), $item->getProductId(), $order->getStoreId())){
                                $bundleSku = $bundleValues['bundle_sku'];
                                $bundleQty = $bundleValues['bundle_qty'];
                                $bundlePrice = $bundleValues['bundle_price'];
                            }else{
                                $bundleSku = $this->_getAttributeValue($item->getProductId(), 'bundle_sku');
                                $bundleQty = $this->_getAttributeValue($item->getProductId(), 'bundle_qty');
                                $bundlePrice = $this->_getAttributeValue($item->getProductId(), 'bundle_price');
                            }

                        }else{
                            if(!empty($order->getRemoteIp())){
                                if($bundleValues = $bundleObj->getBundleItemsInOrder($order->getId(), $item->getProductId(), $order->getStoreId())){
                                    $bundleSku = $bundleValues['bundle_sku'];
                                    $bundleQty = $bundleValues['bundle_qty'];
                                    $bundlePrice = $bundleValues['bundle_price'];
                                }else{
                                    $bundleSku = $this->_getAttributeValue($item->getProductId(), 'bundle_sku');
                                    $bundleQty = $this->_getAttributeValue($item->getProductId(), 'bundle_qty');
                                    $bundlePrice = $this->_getAttributeValue($item->getProductId(), 'bundle_price');
                                }
                            }else{
                                
                                $bundleSku = $this->_getAttributeValue($item->getProductId(), 'bundle_sku');
                                $bundleQty = $this->_getAttributeValue($item->getProductId(), 'bundle_qty');
                                $bundleNewPrice = $this->_getAttributeValue($item->getProductId(), 'bundle_price');
                                $qty = explode('#', $bundleQty);
                                $bundleNewPrices = explode('#',$bundleNewPrice);
                                $count = 0;
                                for($i = 0;$i < count($bundleNewPrices);$i++){
                                    if($bundleNewPrices[$i] == 0 || $bundleNewPrices[$i] == '0'){
                                        $count++;
                                    }
                                }

                                $array = array();
                                if((count($qty) - $count) == 0){
                                    $oldBundlePrice = 0;
                                }else{
                                    $oldBundlePrice = $item->getPrice() / (count($qty) - $count);
                                }

                                for($i = 0;$i < count($qty);$i++){
                                    if($bundleNewPrices[$i] == 0 ||$bundleNewPrices[$i] == '0'){
                                        $array[] = 0;
                                    }else{
                                        $array[] = $oldBundlePrice/$qty[$i];
                                    }

                                }
                                $bundlePrice = implode('#',$array);
                            }
                        }


                        if (!$bundleSku) {
                            $bundleSku = $item->getSku();
                        }

                        if (!$bundleQty) {
                            $bundleQty = "1";
                        }

                        if (!$bundlePrice) {
                            if ($parentItem = $item->getParentItem()) {
                                $bundlePrice = $parentItem->getPriceInclTax();
                            } else {
                                $bundlePrice = $item->getPriceInclTax();
                            }
                        }
                    }
                    
                    $bundleSkus = explode('#', $bundleSku);
                    $bundleQtys = explode('#', $bundleQty);
                    $bundlePrices = explode('#', $bundlePrice);

                    Mage::log($bundleSkus, null, 'bundle.log');
                    Mage::log($bundleQtys, null, 'bundle.log');
                    Mage::log($bundlePrices, null, 'bundle.log');
                    // This is the snippet of code for exporting CSV data
                    // you shouldn't touch it
                    foreach ($bundleSkus as $key => $value) {
                        $qty = $bundleQtys[$key] * $item->getQtyOrdered();

                        /* extract some data for Netsuite send data to webservice */
                        $tmp['order_id'] = $order->getIncrementId();
                        $tmp['item_id'] = $value;
                        $tmp['item_qty'] = $qty;
                        $tmp['rate'] = $bundlePrices[$key];
                        $tmp['gross_total'] = $bundlePrices[$key] * $qty;

                        if ($netsuiteorder['is_wholesale'] =='1'){

                        $tmp['is_wholesale'] = $item->getIsWholesale();
                        }else {
                        $tmp['is_wholesale'] = '0';
                        }
                        
                        $netsuiteorder['items'][] = $tmp;
                    }
                }
            }
            // only set order is exported if successfully sent to Netsuite
            unset($sendOrderResult);


            /** @var Rewardpoints_Helper_Data $rpHelper */
            $rpHelper = Mage::helper('rewardpoints');
            /** @var Mage_Sales_Model_Order $order */
            $rewardPoints = $order->getData('rewardpoints');

            $rpDiscountTotal = 0;
            if (!is_null($rewardPoints) && $rewardPoints > 0)
                $rpDiscountTotal = (-1) * $rpHelper->convertPointsToMoneyEquivalence(floor($rewardPoints));


            //add discount name and coupon code into order (format like an item)
            $coupon_rule_name = $order->getData('coupon_rule_name');
            if (!empty($coupon_rule_name)) {
                // calc discount amount for coupon code
                $finalDiscountAmount = $order->getData('discount_amount');
                if (!is_null($rewardPoints) && $rewardPoints > 0)
                    $finalDiscountAmount = $order->getData('discount_amount') - $rpDiscountTotal;

                // build coupon item
                $discount['order_id'] = $order->getIncrementId();
                $discount['item_type'] = 'discount';
                $discount['item_id'] = $order->getData('coupon_rule_name');
                $discount['item_qty'] = 1;
                $discount['rate'] = $finalDiscountAmount;
                $discount['gross_total'] = floor($finalDiscountAmount);
                $netsuiteorder['items'][] = $discount;
                unset($discount);
            }


            /**
             * Begin WT-1 Fix
             * Make sure NetSuite receives reward point discount item
             */
            if (!is_null($rewardPoints) && $rewardPoints > 0) {
                // build reward points item
                $discount['order_id'] = $order->getIncrementId();
                $discount['item_type'] = 'discount';
                $discount['item_id'] = "Reward Points";
                $discount['item_qty'] = 1;
                $discount['rate'] = $rpDiscountTotal;
                $discount['gross_total'] = floor($rpDiscountTotal);
                $netsuiteorder['items'][] = $discount;
                unset($discount);
            }
            /**
             * End WT-1 Fix
             */

            $sendOrderResult = $this->_sendDataToNetsuite($netsuiteorder);
            Mage::log('data---' . $sendOrderResult, null, 'data.log');
            if (is_null($sendOrderResult) || $sendOrderResult == '') {
                // save error message
                $this->_saveOrder($order->getIncrementId(), 'SO Error : Unknown');
                Mage::log("Fail in send order " . $netsuiteorder['increment_id'] . "-- Unknown error", null, 'data.log');
            } elseif (is_numeric(str_replace('"', '', $sendOrderResult))) {
                // mark as exported and remove error message if have
                $this->_saveOrder($order->getIncrementId(), '', 1);
                // add successful order to return last array
                $allExportedOrders[] = $netsuiteorder;
                // add id of success order to array for last check
                $this->_successOrderIds[] = $order->getId();
                // record log
                Mage::log("Successfully send order " . $netsuiteorder['increment_id'], null, 'data.log');
            } else {
                // save error message
                $this->_saveOrder($order->getIncrementId(), 'SO '. str_replace('"', '', $sendOrderResult));
                Mage::log("Fail in send order " . $netsuiteorder['increment_id'] . "-- " . $sendOrderResult, null, 'data.log');
            }
            Mage::log('----End sending progress - order id = ' . $netsuiteorder['increment_id'] . '----', null, 'data.log');
        }
        return $allExportedOrders;
    }

    /**
     * Verify if customer exists in Netsuite
     * It will check if a customer in Magento exists in Netsuit or not
     * $id = customer id in Magento
     */
    protected function _verifyCustomer($_id)
    {
        if ($_id && intval($_id)) {
            $_data = Array('recordtype' => "customer_check", 'customer_id' => $_id);
            return $this->_sendDataToNetsuite($_data);
        }
        return null;
    }

    /**
     * Send customer info to NetSuite to create new
     */
    protected function _sendCustomerDataToNetSuite($_id)
    {
        $customer = null;
        if ($_id && intval($_id)) {
            $customer = Mage::getModel('customer/customer')->load($_id);
        }
        if ($customer) {
            $defaultBilling = $customer->getDefaultBilling();
            $_tmp = Mage::getModel('customer/address')->load($defaultBilling);
            $_customerData = Array(
                'recordtype' => 'customer',
                'customer_id' => $_id,
                'company_name' => $_tmp->getData('company') ? $_tmp->getData('company') : '',
                'email' => $customer->getEmail() ? $customer->getEmail() : '',
                'firstname' => $customer->getFirstname() ? $customer->getFirstname() : '',
                'lastname' => $customer->getLastname() ? $customer->getLastname() : '',
                'prefix' => $_tmp->getData('prefix') ? $_tmp->getData('prefix') : '',
                'telephone' => $_tmp->getData('telephone') ? $_tmp->getData('telephone') : '',
                'fax' => $_tmp->getData('fax') ? $_tmp->getData('fax') : ''
            );
            // send customer data to NS
            $result = $this->_sendDataToNetsuite($_customerData);
            // check submission result
            if (!is_null($result) || $result != '' || strpos($result, 'Error') == false)
                $this->_tempNewCustomer[] = $_customerData;
            return $result;
        }
        return null;
    }

    /**
     * Send all kinds of data to NetSuite
     */
    protected function _sendDataToNetsuite($_data)
    {
        Mage::log($_data, null, 'data.log');
        $url = Mage::getStoreConfig('netsuiteexport/cronconfig/netsuiteurl');
        $account = Mage::getStoreConfig('netsuiteexport/cronconfig/netsuiteaccount');
        $email = Mage::getStoreConfig('netsuiteexport/cronconfig/netsuiteemail');
        $password = Mage::getStoreConfig('netsuiteexport/cronconfig/netsuitepassword');
        $role = Mage::getStoreConfig('netsuiteexport/cronconfig/netsuiterole');

        $options = array(
            'http' => array(
                'header' => array(
                    'Authorization: NLAuth nlauth_account=' . $account . ', nlauth_email=' . $email . ', nlauth_signature=' . $password . ', nlauth_role=' . $role,
                    'Content-type: application/json',
                    'User-Agent-x: SuiteScript-Call'
                ),
                'method' => 'POST',
                'content' => json_encode($_data)
            )
        );
        $context = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        return $result;
    }

    /**
     * Get address
     */
    protected function _getAddress($streets, $row)
    {
        $streets = (is_array($streets)) ? $streets : explode("\n", $streets);
        $itemCount = floor(count($streets) / 2);
        $itemCount = ($itemCount < 1) ? 1 : $itemCount;
        if ($row == 1) {
            return preg_replace('~\r\n?~', ' ', implode("\r\n", array_slice($streets, 0, $itemCount)));
        } else {
            return preg_replace('~\r\n?~', ' ', implode("\r\n", array_slice($streets, $itemCount)));
        }
    }

    /**
     * Not used function
     */
    protected function _createOptionData($order, $type)
    {
        $address = $order->getShippingAddress();
        $payment = $order->getPayment()->getMethod();
        $orderDate = $order->getCreatedAtStoreDate()->toString('d/m/Y', 'php');
        $invoiceDate = false;
        $streets = $address->getStreet();

        if ($order->hasInvoice()) {
            foreach ($order->getAllInvoices as $invoice) {
                $invoiceDate = $invoice->getCreatedAtStoreDate()->toString('d/m/Y', 'php');
                break;
            }
        }

        $temp = array();
        $temp['order_id'] = $this->_setCsvFormat($order->getIncrementId());
        $temp['order_date'] = $this->_setCsvFormat($orderDate);
        $temp['invoice_date'] = $this->_setCsvFormat((($invoiceDate) ? $invoiceDate : $orderDate));
        $temp['email'] = $this->_setCsvFormat($order->getCustomerEmail());
        $temp['customer_id'] = $this->_setCsvFormat($order->getCustomerId());
        $temp['external_customer_id'] = $temp['customer_id'];
        $temp['customer_firstname'] = $this->_setCsvFormat($order->getCustomerFirstname());
        $temp['customer_lastname'] = $this->_setCsvFormat($order->getCustomerLastname());
        $temp['shipping_addressee'] = $this->_setCsvFormat((($address->getFirstname()) ? $address->getFirstname() : '') . ' ' . (($address->getLastname()) ? $address->getLastname() : ''));

        $temp['shipping_address_line_1'] = $this->_setCsvFormat($this->_getAddress($streets, 1));
        $temp['shipping_address_line_2'] = $this->_setCsvFormat($this->_getAddress($streets, 2));

        $temp['shipping_address_city'] = $this->_setCsvFormat($address->getCity());
        $temp['shipping_address_state'] = $this->_setCsvFormat($address->getRegion());
        $temp['shipping_address_country'] = $this->_setCsvFormat($this->_getCountryNames()[$address->getCountryId()]);
        $temp['shipping_address_postal_code'] = $this->_setCsvFormat($address->getPostcode());
        $temp['shipping_address_postal_phone'] = $this->_setCsvFormat($address->getTelephone());

        $itemId = '';
        $rate = 0;
        if ($type == EMST_NetsuiteExport_Helper_Data::ITEM_TYPE_COD_FEE) {
            $itemId = Mage::getStoreConfig('netsuiteexport/salesexport/codfee_name');
            if (!$itemId) {
                $itemId = "COD Charges";
            }
            $rate = $order->getCodFee() + $order->getCodTaxAmount();
        } elseif ($type == EMST_NetsuiteExport_Helper_Data::ITEM_TYPE_SHIPPING_AMOUNT) {
            $itemId = Mage::getStoreConfig('netsuiteexport/salesexport/shipping_name');
            if (!$itemId) {
                $itemId = "All Shipping Charges";
            }
            $rate = $order->getShippingInclTax();
        } elseif ($type == EMST_NetsuiteExport_Helper_Data::ITEM_TYPE_REWARD_POINTS) {
            $itemId = Mage::getStoreConfig('netsuiteexport/salesexport/reward_name');
            if (!$itemId) {
                $itemId = "Reward Points";
            }
            $rate = -$order->getRewardpoints();
        } elseif ($type == EMST_NetsuiteExport_Helper_Data::ITEM_TYPE_DISCOUNT_RULES) {
            $ruleIds = $order->getAppliedRuleIds();
            $ruleName = $order->getCouponRuleName();
            $otherDiscount = 'Discount';

            if ($ruleName) {
                $itemId = $ruleName;
            } elseif ((int)$ruleIds) {
                $rule = Mage::getModel('salesrule/rule')->load($ruleIds);
                if ($rule->getName()) {
                    $itemId = $rule->getName();
                }
            } else {
                $itemId = $otherDiscount;
            }
            $rate = -(abs($order->getDiscountAmount()) - abs($order->getRewardpoints()));
        }

        $temp['item_id'] = $this->_setCsvFormat($itemId);
        $temp['item_qty'] = $this->_setCsvFormat(1);
        $temp['rate'] = $this->_setCsvFormat($rate);
        $temp['gross_total'] = $this->_setCsvFormat($rate);

        $temp['payment_type'] = $this->_setCsvFormat($this->_getHelper()->getPaymentMethod($order->getPaymentMethod()));
        $temp['shipping_type'] = $this->_setCsvFormat($this->_getHelper()->getShippingMethod($order->getShippingMethod()));

        $temp['main_sales_channel'] = $this->_setCsvFormat($this->_getHelper()->getChannel($order->getMainChannel()));
        $temp['sub_sales_channel'] = $this->_setCsvFormat($this->_getHelper()->getChannel($order->getSubChannel()));
        $temp['sales_channel_source'] = $this->_setCsvFormat($this->_getHelper()->getChannel($order->getChannelSource()));

        return implode(',', $temp);
    }

    /**
     * Get representing number for a store
     * This number is defined by Netsuite team
     * @var String name of the store
     * @return int corresponding number of the store
     */
    protected function _getStoreNumber($_storeName = '')
    {
        $_storeName = trim(strtolower($_storeName));
        if (strpos($_storeName, 'bodyden') > 0) {
            return 1;
        }
        if (strpos($_storeName, 'commerce') > 0) {
            return 2;
        }
        if (strpos($_storeName, 'lafema') > 0) {
            return 3;
        }
        if (strpos($_storeName, 'petloft') > 0) {
            return 4;
        }
        if (strpos($_storeName, 'sanoga') > 0) {
            return 5;
        }
        if (strpos($_storeName, 'sumobuys') > 0) {
            return 6;
        }
        if (strpos($_storeName, 'venbi') > 0) {
            return 7;
        }
        if (strpos($_storeName, 'moxy') > 0) {
            return 8;
        }
        if (strpos($_storeName, 'orami') > 0) {
            return 8;
        }

        return 0;
    }

    /**
     * Not used function
     */
    protected function _setCsvFormat($data)
    {
        return '"' . str_replace(array('"', '\\'), array('""', '\\\\'), $data) . '"';
    }

    /**
     * Get header content of each file
     */
    protected function _getHeaders()
    {
        return array(
            'Order #'                           => 'string',
            'Order Date'                        => 'string',
            'Email'                             => 'string',
            'Customer ID'                       => 'string',
            'Customer First Name'               => 'string',
            'Customer Last Name'                => 'string',
            'Shipping Addressee'                => 'string',
            'Shipping Address Line 1'           => 'string',
            'Shipping Address Line 2'           => 'string',
            'Shipping Address City'             => 'string',
            'Shipping Address State/Province'   => 'string',
            'Shipping Address Country'          => 'string',
            'Shipping Address Postal Code'      => 'string',
            'Shipping Address Phone'            => 'string',
            'Items : Item'                      => 'string',
            'Item Qty'                          => 'string',
            'Gross Total'                       => 'string',
            'Payment Type'                      => 'string',
            'Shipping Type'                     => 'string',
            'Main Sales Channel(Optional)'      => 'string',
            'Sub Sales Channel(Optional)'       => 'string',
            'Sales Channel Source(Optional)'    => 'string',
            'Location (Item)'                   => 'string',
            'Location'                          => 'string',
            'Business Unit'                     => 'string',
        );
    }

    /**
     * Get customer header
     */
    protected function _getCustomerHeaders()
    {
        return array(
            'Customer ID' => 'string',
            'Customer First Name' => 'string',
            'Customer Last Name' => 'string',
            'Company name (if no first name and lastname, must provide companyname)' => 'string',
            'Email' => 'string',
            'Phone' => 'string',
            'fax (optional)' => 'string',
            'prefix (optional)' => 'string',
            'job title (optional)' => 'string',
        );
    }

    /**
     * Cron main function
     */
    public function ExportCsvData()
    {
        // check cron is activated
        if (!$this->_getHelper()->isCronActive()) {
            return;
        }

        ini_set("memory_limit", "-1");
        ini_set('max_execution_time', 0);

        // get order collection
        $onlineOrders = $this->_getOnLineSalesOrder();
        $offlineOrders = $this->_getOffLineSalesOrder();

        // count orders to check
        if ($onlineOrders->count() > 0 || $offlineOrders->count() > 0) {

            // send orders to NetSuite and get order data
            $onlineOrdersArr = $this->_convertOrderToArray($onlineOrders);
            $offlineOrdersArr = $this->_convertOrderToArray($offlineOrders);

            // get different between two array (temp orders & success orders) to un-mark processing status
            $diffOrderArr = array_diff(array_merge($this->_processingOrderIds1, $this->_processingOrderIds2), $this->_successOrderIds);
            $this->_markAsProcessing($diffOrderArr, false);

            // export online order
            if (count($onlineOrdersArr) > 0) {
                // prepare order data for export file
                $exportedOnlineOrders = $this->_buildOrdersData($onlineOrdersArr);
                if(count($exportedOnlineOrders) > 0){
                    // export order to file
                    $onResult = $this->_exportExcelFile($exportedOnlineOrders);
                    // move export file to data source folder
                    if (!is_null($onResult))
                        $this->_ftpFile($onResult);
                }
            }

            // export offline order
            if (count($offlineOrdersArr) > 0) {
                // prepare order data for export file
                $exportedOfflineOrders = $this->_buildOrdersData($offlineOrdersArr);
                if(count($exportedOfflineOrders) > 0){
                    // export order to file
                    $offResult = $this->_exportExcelFile($exportedOfflineOrders);
                    // move export file to data source folder
                    if (!is_null($offResult))
                        $this->_ftpFile($offResult);
                }
            }
        }
    }

    /**
     * Manually export function
     */
    public function ManuallyExport($orderIds)
    {
        ini_set("memory_limit", "-1");
        ini_set('max_execution_time', 0);

        // get collection of selected orders
        $orders = $this->_getSalesOrderByIds($orderIds);

        // prevent user to re-export SOs that already sent to NS and processing SOs.
        foreach($orders as $so){
            if($isExported = $so->getData('is_exported')){
                if($isExported == 1 || $isExported == 2){
                    $this->_markAsProcessing($this->_processingOrderIds1, false);
                    return array(
                        'message'   => "Please don't select exported SOs or processing.",
                        'status'    => false,
                    );
                }
            }
        }

        // mark orders as processing
        $this->_markAsProcessing($this->_processingOrderIds1);

        // count orders to check
        if ($orders->getSize() > 0) {
            // send orders to NetSuite and get order data
            $manuallyOrders = $this->_convertOrderToArray($orders);

            // get different between two array (temp orders & success orders) to un-mark processing status
            $diffOrderArr = array_diff($this->_processingOrderIds1, $this->_successOrderIds);
            $this->_markAsProcessing($diffOrderArr, false);

            if (count($manuallyOrders) > 0) {
                // prepare order data for export file
                $exportedOrders = $this->_buildOrdersData($manuallyOrders);
                if(count($exportedOrders) > 0){
                    // export order to file
                    $destinationPath = $this->_exportExcelFile($exportedOrders);
                    if (!is_null($destinationPath)){
                        // move export file to data source folder
                        $return = $this->_ftpFile($destinationPath);
                        if($return){
                            if (file_exists($destinationPath)) {
                                return array(
                                    'message'   => "Successfully export SOs to NetSuite.",
                                    'status'    => true,
                                    'file_path' => $destinationPath,
                                );
                            }
                        }
                        return array(
                            'message'   => "Fail to store export file into Data Source folder. Please check configuration.",
                            'status'    => false,
                        );


                        // available to download
                        /*if (file_exists($destinationPath)) {
                            header('Content-Disposition: attachment; filename="'.basename($destinationPath).'"');
                            header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                            header('Content-Transfer-Encoding: binary');
                            header('Content-Length: ' . filesize($destinationPath));
                            header('Cache-Control: must-revalidate');
                            header('Pragma: public');
                            ob_clean();
                            flush();
                            readfile($destinationPath);
                        }*/
                    }
                }
            }
        }
        return false;
    }

    /**
     * Build orders data for exporting file
     */
    protected function _buildOrdersData($orders)
    {
        $result = array();
        if(is_array($orders) && count($orders) > 0){
            foreach($orders as $order){
                if(isset($order['items'])){
                    foreach($order['items'] as $item){
                        $result[] = array(
                            $order['increment_id'],
                            $order['order_date'],
                            $order['email'],
                            $order['customer_id'],
                            $order['customer_firstname'],
                            $order['customer_lastname'],
                            $order['shipping_addressee'],
                            $order['shipping_address_line_1'],
                            $order['shipping_address_line_2'],
                            $order['shipping_address_city'],
                            $order['shipping_address_state'],
                            $order['region'],
                            $order['postcode'],
                            $order['telephone'],
                            $item['item_id'],
                            $item['item_qty'],
                            $item['gross_total'],
                            $this->_getPaymentTypeById($order['increment_id']),
                            $this->_getShippingTypeById($order['increment_id']),
                            $order['main_sales_channel'],
                            $order['sub_sales_channel'],
                            $order['sales_channel_source'],
                            'Acommerce',
                            'Acommerce',
                            'All',
                        );
                    }
                }
            }
        }
        return $result;
    }

    /**
     * Get shipping type title by id
     */
    protected function _getShippingTypeById($increment_id)
    {
        if(array_key_exists($increment_id, $this->_tempShippingDesc))
            return $this->_tempShippingDesc[$increment_id];
        return '';
    }

    /**
     * Get payment type title by id
     */
    protected function _getPaymentTypeById($increment_id)
    {
        if(array_key_exists($increment_id, $this->_tempPaymentMethod))
            if($result = Mage::getStoreConfig('payment/' . $this->_tempPaymentMethod[$increment_id] . '/title', Mage_Core_Model_App::ADMIN_STORE_ID))
                return $result;
        return '';
    }

    /**
     * Sort customer data for exporting file
     */
    protected function _sortCustomerData($key)
    {
        if(array_key_exists($key, $this->_tempNewCustomer)){
            return array(
                $this->_tempNewCustomer[$key]['customer_id'],
                $this->_tempNewCustomer[$key]['firstname'],
                $this->_tempNewCustomer[$key]['lastname'],
                $this->_tempNewCustomer[$key]['company_name'],
                $this->_tempNewCustomer[$key]['email'],
                $this->_tempNewCustomer[$key]['telephone'],
                $this->_tempNewCustomer[$key]['fax'],
                $this->_tempNewCustomer[$key]['prefix'],
                '',// job title
            );
        }
        return array('','','','','','','','','');
    }

    /**
     * Export orders to excel file format
     */
    protected function _exportExcelFile($orders)
    {
        try{
            // build new filename base on current time
            $currentDate = Varien_Date::now();
            $currentDate = str_replace(array('-', ':', ' '), array('', '', '-'), $currentDate);
            $filename = 'SO-'.$currentDate.'.xlsx';

            // header for excel file
            header('Content-Disposition: attachment; filename="'.XLSXWriter::sanitize_filename($filename).'"');
            header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            header('Content-Transfer-Encoding: binary');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');

            // get header of data table
            $header = $this->_getHeaders();
            $cusHeader = $this->_getCustomerHeaders();

            // create output folder
            $path = Mage::getBaseDir('var') . DS .'export' . DS . 'sales_order' . DS;
            $file = new Varien_Io_File();
            $file->mkdir($path, 0777);

            // build destination file path
            $destinationPath = $path . DS . $filename;

            // put order data into new file
            if(is_array($orders)){
                $writer = new XLSXWriter();
                $writer->setAuthor('Unknown');
                // write orders
                $writer->writeSheet($orders, 'SO', $header);
                // write new customers
                if(count($this->_tempNewCustomer) > 0){
                    $writer->writeSheetHeader('CUS', $cusHeader );
                    foreach($this->_tempNewCustomer as $key => $customer){
                        $writer->writeSheetRow('CUS', $this->_sortCustomerData($key));
                    }
                }
                // save to local file
                $writer->writeToFile($destinationPath);
            }
            return $destinationPath;
        }catch (Exception $e){
            Mage::log($e->getMessage(), null, 'sales_order_export.log');
            return null;
        }
    }

    /**
     * Import SO content in export file
     */
    protected function _ftpFile($path)
    {
        try {
            /** @var EMST_NetsuiteExport_Helper_Data $helper */
            $helper = Mage::helper('netsuiteexport');
            $sftp = $helper->open();

            if ($sftp instanceof Varien_Io_File || $sftp instanceof Varien_Io_Sftp) {
                $fileName = '';
                $pos = strrpos($path, DS);
                if ($pos !== false) {
                    $fileName = substr($path, $pos + 1, strlen($path) - ($pos + 1));
                }

                $content = @file_get_contents($path);

                if($fileName) $sftp->write($fileName, $content);
                $sftp->close();
            }

            return true;
        } catch (Exception $e) {
            Mage::log($e->getMessage(), null, 'sales_order_export.log');
            return false;
        }
    }

    /**
     * Get Raw attribute value
     *
     * @param int $productId product id
     * @param string $attributeCode code of attribute
     *
     * @return mixed
     */
    protected function _getAttributeValue($productId, $attributeCode)
    {
        return $this->_getProductResource()->getAttributeRawValue($productId, $attributeCode, Mage_Core_Model_App::ADMIN_STORE_ID);
    }

    /**
     * Mark SOs as processing and contrary
     */
    protected function _markAsProcessing($orderIds, $mark = true)
    {
        try{
            if(is_array($orderIds) && count($orderIds) > 0){
                $orderIds = implode(',', $orderIds);

                $queryStr1 = 'UPDATE '.$this->_getOrderTableName().' SET `is_exported` = '.($mark == true ? '2' : '0').' WHERE `entity_id` IN('.$orderIds.');';
                $queryStr2 = 'UPDATE '.$this->_getOrderGridTableName().' SET `is_exported` = '.($mark == true ? '2' : '0').' WHERE `entity_id` IN('.$orderIds.');';

                $this->_getWriteConnection()->query($queryStr1);
                $this->_getWriteConnection()->query($queryStr2);
            }
            return true;
        }catch (Exception $e){
            Mage::log($e->getMessage(), null, 'sales_order_export.log');
            return false;
        }
    }
}

