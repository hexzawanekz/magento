<?php

/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade SalesExport to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_SalesExport
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
 */
class EMST_NetsuiteExport_Model_Resource_Sales_Order_Grid_Collection extends Mage_Sales_Model_Resource_Order_Grid_Collection
{
    
    protected function _construct()
    {
        parent::_construct();
    }

    /**
     * join sales order collection to payment
     *
     * @return order collection
     */
    public function addPaymentToSelect() {
        $this->addFilterToMap('payment_method', 'payment.method');
        $this->join(
                array('payment' => 'sales/order_payment'), 'main_table.entity_id = payment.parent_id',
                array('payment_method' => 'payment.method')
        );
        return $this;
    }

    public function addEmailToSelect() {
        $this->addFilterToMap('customer_email', 'customer.email');
        $this->addFilterToMap('created_at', 'main_table.created_at');
        $this->addFilterToMap('store_id', 'main_table.store_id');
        $this->addFilterToMap('increment_id', 'main_table.increment_id');
        $this->join(
                array('customer' => 'customer/entity'), 'customer.entity_id = main_table.customer_id',
                array('customer_email' => 'customer.email')
        );
        return $this;
    }
	public function addNetsuiteStatus() {
		$this->addFilterToMap('export_status', 'IF(order_export.status!="",order_export.status,-1)');
		$this->addFilterToMap('status', 'main_table.status');
		$this->addFilterToMap('created_at', 'main_table.created_at');
		$joinTable = $this->getTable('netsuiteexport/netsuite_sales_order');
		$this->getSelect()->joinLeft(
                array('order_export' => $joinTable), 'order_export.order_increment_id = main_table.increment_id',
                array('export_status' => 'IF(order_export.status!="",order_export.status,-1)')
        );
		return $this;
	}
}
