<?php

/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade NetsuiteExport to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_NetsuiteExport
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
 */
class EMST_NetsuiteExport_Helper_Data extends Mage_Core_Helper_Abstract
{
    protected $_paymentMethods = array();
    protected $_shippingMethods = array();
    protected $_shipping = null;
    protected $_countryName = array();
    protected $_netsuiteSales = null;
    protected $_coreDate = null;
    protected $_netsuite_config = null;
    protected $_channels = null;

    CONST ITEM_TYPE_PRODUCT_PRICE = 'product_price';
    CONST ITEM_TYPE_SHIPPING_AMOUNT = 'shipping_amount';
    CONST ITEM_TYPE_COD_FEE = 'cod_fee';
    CONST ITEM_TYPE_REWARD_POINTS = 'reward_points';
    CONST ITEM_TYPE_DISCOUNT_RULES = 'discount_rules';

    const XML_PATH_ACOMMERCE_EXPORT_ACTIVE        = 'netsuiteexport/export_files/active';

    const XML_PATH_ACOMMERCE_EXPORT_CONNECT_TYPE  = 'netsuiteexport/export_files/type';

    const XML_PATH_ACOMMERCE_EXPORT_INTERNAL_PATH = 'netsuiteexport/export_files/internal_path';

    const XML_PATH_ACOMMERCE_EXPORT_SFTP_HOST     = 'netsuiteexport/export_files/host';
    const XML_PATH_ACOMMERCE_EXPORT_SFTP_USERNAME = 'netsuiteexport/export_files/user';
    const XML_PATH_ACOMMERCE_EXPORT_SFTP_PASSWORD = 'netsuiteexport/export_files/password';
    const XML_PATH_ACOMMERCE_EXPORT_SFTP_PATH     = 'netsuiteexport/export_files/path';


    // CONST ITEM_FIELD_NAME	= 'netsuite_item_name';
    CONST ITEM_FIELD_NAME = 'name';
    CONST TAX_RATE = 1.07;

    public function getChannel($id)
    {
        $name = '';
        if (is_null($this->_channels)) {
            $this->_channels = Mage::getSingleton('emst_sales/channel')->getCollection();
        }
        if ($this->_channels) {
            foreach ($this->_channels as $channel) {
                if ($id == $channel->getId()) {
                    return $channel->getName();
                }
            }
        }
        return $name;
    }

    public function isValidToUpdate($order)
    {
        $method = $order->getPayment()->getMethodInstance()->getCode();
        if ($method == 'cashondelivery') {
            if (!in_array($order->getState(),
                          array(EMST_Sales_Model_Order::STATE_NEW, EMST_Sales_Model_Order::STATE_CANCELED))) {
                return false;
            }
            return true;
        } elseif ($order->getState() == EMST_Sales_Model_Order::STATE_INVOICED) {
            return true;
        }
        return false;
    }

    public function getStorePaymentMethods()
    {
        if (empty($this->_paymentMethods)) {
            $methods = Mage::getSingleton('payment/config')->getActiveMethods();
            foreach ($methods as $method) {
                $this->_paymentMethods[$method->getCode()] = $method->getTitle();
            }
        }
        return $this->_paymentMethods;
    }

    public function getShipping()
    {
        if (!$this->_shipping) {
            $this->_shipping = Mage::getModel('shipping/shipping');
        }
        return $this->_shipping;
    }

    // match with magento shipping methods name in db
    public function matchMagentoShit($method)
    {
        if (!$method) {
            return null;
        }
        if (is_array($method)) {
            $matched = array();
            foreach ($method as $mt) {
                $matched[] = $this->matchMagentoShit($mt);
            }
            return $matched;
        }
        switch ($method) {
            case 'tablerate':
                return 'tablerate_bestway';
                break;
            case 'pickup':
                return 'pickup_store';
                break;
            case 'freeshipping':
                return 'freeshipping_freeshipping';
                break;
            case 'flatrate':
                return 'flatrate_flatrate';
                break;
            default:
                return $method . '_' . $method;
                break;
        }
    }

    public function getSalesShippingMethods()
    {
        $m = $this->getStoreShippingMethods();
        $matchedMethods = array();
        foreach ($m as $code => $name) {
            $matchedMethods[$this->matchMagentoShit($code)] = $name;
        }
        return $matchedMethods;
    }

    public function getStoreShippingMethods()
    {
        if (empty($this->_shippingMethods)) {
            $carriers = Mage::getStoreConfig('carriers');
            foreach ($carriers as $carrierCode => $carrierConfig) {
                $carrier = $this->getCarrierByCode($carrierCode, 0);
                if (!$carrier) {
                    continue;
                }
                // $title = ($carrier->getConfigData('title'))?$carrier->getConfigData('title'):$carrierCode;
                $name = ($carrier->getConfigData('name')) ? $carrier->getConfigData('name') : $carrierCode;
                $description = $carrier->getConfigData('description');
                // $this->_shippingMethods[$carrierCode] = array('title'=>$title,'name'=>$name,'description'=>$description);
                $this->_shippingMethods[$carrierCode] = $name;
            }
        }
        return $this->_shippingMethods;
    }

    public function getCarrierByCode($carrierCode, $storeId = null)
    {
        $className = Mage::getStoreConfig('carriers/' . $carrierCode . '/model', $storeId);
        if (!$className) {
            return false;
        }
        $obj = Mage::getModel($className);
        if (!is_null($storeId)) {
            $obj->setStore($storeId);
        }
        return $obj;
    }

    public function getCountryNames()
    {
        if (empty($this->_countryName)) {
            $col = Mage::getModel('directory/country')
                    ->getResourceCollection()
                    ->toOptionArray();
            foreach ($col as $data) {
                $this->_countryName[$data['value']] = $data['label'];
            }
        }
        return $this->_countryName;
    }

    public function getNetsuiteConfigFields($item = null)
    {
        // if(!$this->_netsuite_config) {
        $netsuite = array(
            'warehouse_location' => '',
            'business_unit' => '',
            'subsidiary' => '',
            'tax_code' => '',
            'discount_tax_code' => '',
            'product_price' => '',
            'shipping_amount' => '',
            'discount_amount' => '',
            'reward_points' => '',
            'discount_rules' => '',
            'cod_fee' => '',
            'netsuite_status' => '',
        );
        $getDefault = true;
        $product = new Varien_Object;
        $storeId = '';
        if ($item && $item instanceof Varien_Object) {
            $storeId = $item->getStoreId();
            if ($item->getType() == self::ITEM_TYPE_PRODUCT_PRICE) {
                $product = Mage::getModel('catalog/product')->load($item->getProductId());
                if ($product->getId()) {
                    $getDefault = false;
                }
            }
        }
        foreach ($netsuite as $k => $v) {
            if ($getDefault) {
                $netsuite[$k] = Mage::getStoreConfig('netsuiteexport/salesexport/' . $k, $storeId);
            } else {
                $netsuite[$k] = ($product->getData($k)) ? $product->getData($k) : Mage::getStoreConfig('netsuiteexport/salesexport/' . $k,
                                                                                                       $storeId);
            }
        }

        // $this->_netsuite_config = $netsuite;
        // }
        return $netsuite;
    }

    public function getPaymentMethod($method)
    {
        $methods = $this->getStorePaymentMethods();
        if (isset($methods[$method])) {
            return $methods[$method];
        }
        return $method;
    }

    public function getShippingMethod($method)
    {
        $salesMethods = $this->getSalesShippingMethods();
        if (isset($salesMethods[$method])) {
            return $salesMethods[$method];
        }
        return $method;
    }

    public function getNetsuiteSalesConfig($storeId = null)
    {
        if (!$this->_netsuiteSales) {
            $config = array(
                'statusfilter' => '',
                'paymentfilter' => '',
                'shippingfilter' => '',
                'timefilter' => '',
            );
            foreach ($config as $k => $v) {
                $config[$k] = Mage::getStoreConfig('netsuiteexport/cronconfig/' . $k, $storeId);
            }
            $this->_netsuiteSales = $config;
        }
        return $this->_netsuiteSales;
    }

    public function getSalesOuterTable()
    {
        return Mage::getStoreConfig('netsuiteexport/cronconfig/tablename');
    }

    public function isCronActive()
    {
        return (bool) Mage::getStoreConfig('netsuiteexport/cronconfig/enable');
    }

    /**
     * Convert datetime to current timezone
     * Input value must be in GMT time
     * @param string $value
     * @param string $format
     * @return string;
     */
    public function formatDateTime($value = null, $format = null)
    {
        if (!$this->_coreDate) {
            $this->_coreDate = Mage::getModel('core/date');
        }
        return $this->_coreDate->date($format, $value);
    }

    public function getBillingStreet($item, $line = 1)
    {
        return $this->getAddressline($item->getBillingStreet(), $line);
    }

    public function getShippingStreet($item, $line = 1)
    {
        return $this->getAddressline($item->getShippingStreet(), $line);
    }

    public function getAddressline($street, $line)
    {
        $arr = is_array($street) ? $street : explode("\n", $street);
        if ($line > 1) {
            return str_replace('\n', ' ', implode('\n', array_slice($arr, 1)));
        }
        return (isset($arr[$line - 1])) ? $arr[$line - 1] : '';
    }

    public function getItemType()
    {
        return array(
            self::ITEM_TYPE_PRODUCT_PRICE,
            self::ITEM_TYPE_SHIPPING_AMOUNT,
            self::ITEM_TYPE_COD_FEE,
            self::ITEM_TYPE_REWARD_POINTS,
            self::ITEM_TYPE_DISCOUNT_RULES,
        );
    }

    public function renderOtherConfig($index, $item)
    {
        $config = $this->getNetsuiteConfigFields($item);

        return isset($config[$index]) ? $config[$index] : '';
    }

    public function renderPriceLevel($item)
    {
        $config = $this->getNetsuiteConfigFields($item);
        return isset($config[$item->getType()]) ? $config[$item->getType()] : '';
    }

    public function renderTaxCode($item)
    {
        $config = $this->getNetsuiteConfigFields($item);
        if ($item->getType() == self::ITEM_TYPE_REWARD_POINTS || $item->getType() == self::ITEM_TYPE_DISCOUNT_RULES) {
            return $config['discount_tax_code'];
        }
        return $config['tax_code'];
    }

    public function rederQty($item)
    {
        if ($item->getType() != self::ITEM_TYPE_PRODUCT_PRICE) {
            return 1;
        }
        return $item->getItemQty();
    }

    public function renderCountry($countryCode)
    {
        $countryNames = $this->getCountryNames();
        return isset($countryNames[$countryCode]) ? $countryNames[$countryCode] : $countryCode;
    }

    public function renderItemName($item, $exterId = false)
    {
        if ($item->getType() == self::ITEM_TYPE_SHIPPING_AMOUNT) {
            return 'All Shipping Charges';
        } elseif ($item->getType() == self::ITEM_TYPE_COD_FEE) {
            return 'COD Charges';
        } elseif ($item->getType() == self::ITEM_TYPE_REWARD_POINTS) {
            return 'Reward Points';
        } elseif ($item->getType() == self::ITEM_TYPE_DISCOUNT_RULES) {
            $ruleIds = $item->getData('applied_rule_ids');
            $ruleName = $item->getData('coupon_rule_name');
            $otherDiscount = 'Discount';
            if ($ruleName) {
                return $ruleName;
            }
            if ((int) $ruleIds) {
                $rule = Mage::getModel('salesrule/rule')->load($ruleIds);
                if ($rule->getName()) {
                    return $rule->getName();
                }
            }
            return $otherDiscount;
        } elseif ($item->getType() == self::ITEM_TYPE_PRODUCT_PRICE) {
            return ($exterId) ? $item->getData('sku') : $item->getData(self::ITEM_FIELD_NAME);
        }
    }

    public function getItemFieldName()
    {
        return self::ITEM_FIELD_NAME;
    }

    public function renderItemPrice($item, $renderTax = true)
    {
        $amount = null;
        if ($item->getType() == self::ITEM_TYPE_SHIPPING_AMOUNT) {
            $amount = $item->getData('shipping_incl_tax');
        } elseif ($item->getType() == self::ITEM_TYPE_REWARD_POINTS) {
            $amount = - $item->getData('rewardpoints');
        } elseif ($item->getType() == self::ITEM_TYPE_DISCOUNT_RULES) {
            $amount = - (abs($item->getData('discount_amount')) - abs($item->getData('rewardpoints')));
        } elseif ($item->getType() == self::ITEM_TYPE_COD_FEE) {
            $cAmount = $item->getData('cod_fee');
            $cTax = $item->getData('cod_tax_amount');
            $amount = $cAmount + $cTax;
        } elseif ($item->getType() == self::ITEM_TYPE_PRODUCT_PRICE) {
            $amount = $item->getData('product_price');
        }
        if ($renderTax) {
            return $this->renderPrice($item->getData('order_currency_code'), $amount);
        }
        return $amount;
    }

    public function renderGrossTotal($item)
    {
        return $this->renderItemPrice($item, false) * $this->rederQty($item);
    }

    public function renderPrice($currency, $amount)
    {
        $amount = sprintf('%.03f', $amount / $this->getTaxRate()); // hard code for tax calculating
        return $amount; // no rendering
        if ($data = (string) $amount) {
            if (!$currency) {
                return $data;
            }
            $data = sprintf("%f", floatval($data));
            $data = Mage::app()->getLocale()->currency($currency)->toCurrency($data,
                                                                              array('display' => Zend_Currency::NO_SYMBOL));
            return $data;
        }
        return $amount;
    }

    public function getExternalId($item)
    {

    }

    public function getTaxRate()
    {
        return self::TAX_RATE;
    }

    public function renderPhone($phone)
    {
        if (!is_string($phone) && strlen($phone) <= 22) {
            return $phone;
        }
        return substr($phone, 0, 22);
    }

    /**
     * Open connection to directory folder
     *
     * @return bool|Varien_Io_File|Varien_Io_Sftp
     * @throws Exception
     */
    public function open()
    {
        try{
            // check if module is enabled
            if(Mage::getStoreConfig(self::XML_PATH_ACOMMERCE_EXPORT_ACTIVE)){

                // check if connect type is via SFTP folder
                if(Mage::getStoreConfig(self::XML_PATH_ACOMMERCE_EXPORT_CONNECT_TYPE)){
                    // get info to access
                    $host       = Mage::getStoreConfig(self::XML_PATH_ACOMMERCE_EXPORT_SFTP_HOST);
                    $username   = Mage::getStoreConfig(self::XML_PATH_ACOMMERCE_EXPORT_SFTP_USERNAME);
                    $password   = Mage::getStoreConfig(self::XML_PATH_ACOMMERCE_EXPORT_SFTP_PASSWORD);
                    $path       = Mage::getStoreConfig(self::XML_PATH_ACOMMERCE_EXPORT_SFTP_PATH);

                    if($host && $username && $password && $path){
                        // open accessing
                        $sftp = new Varien_Io_Sftp();
                        $sftp->open(
                            array(
                                'host'      => $host,
                                'username'  => $username,
                                'password'  => $password,
                            )
                        );

                        $cd = $path;

                        // go to folder
                        if (!empty($cd)){
                            $sftp->cd($cd);
                        }

                        return $sftp;
                    }
                }else{
                    // check if connect type is internal folder

                    if($directoryPath = Mage::getStoreConfig(self::XML_PATH_ACOMMERCE_EXPORT_INTERNAL_PATH)){
                        $file = new Varien_Io_File();
                        if($file->open(array('path' => $directoryPath))){
                            return $file;
                        }
                    }
                }
            }
            return false;
        }catch(Exception $e){
            Mage::log($e, null, 'sales_order_connect.log');
            throw $e;
        }
    }
}

