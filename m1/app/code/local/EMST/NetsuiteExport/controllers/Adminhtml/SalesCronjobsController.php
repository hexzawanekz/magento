<?php
/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade NetsuiteExport to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_NetsuiteExport
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
*/

class EMST_NetsuiteExport_Adminhtml_SalesExportController extends Mage_Adminhtml_Controller_Action
{

	protected function _initAction() {
		$this->loadLayout()
			->_setActiveMenu('sales/netsuiteexport')
			->_addBreadcrumb(Mage::helper('adminhtml')->__('Order Export By Cron Report'), Mage::helper('adminhtml')->__('Order Export By Cron Report'));
		
		return $this;
	}   
 
	public function indexAction() {			
		$this->_initAction();
		$this->_addContent($this->getLayout()->createBlock('netsuiteexport/adminhtml_salesExport_cronjobs','salesordercronjobs'));
		$this->renderLayout();
	}
	public function gridAction() {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('netsuiteexport/adminhtml_salesExport_grid')->toHtml()
        );
    }
}