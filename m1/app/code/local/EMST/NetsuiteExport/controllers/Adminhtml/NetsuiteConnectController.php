<?php
/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade NetsuiteExport to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_NetsuiteExport
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
*/

class EMST_NetsuiteExport_Adminhtml_NetsuiteConnectController extends Mage_Adminhtml_Controller_Action
{

	protected function _initAction() {
		$this->loadLayout()
			->_setActiveMenu('netsuite')
			->_addBreadcrumb(Mage::helper('adminhtml')->__('Netsuite Connect'), Mage::helper('adminhtml')->__('Netsuite Connect'));

		return $this;
	}

	public function indexAction() {
		$this->_initAction();
		$this->_addContent($this->getLayout()->createBlock('netsuiteexport/adminhtml_netsuiteconnect_order','netsuiteconnect'));
		$this->renderLayout();
	}
	public function gridAction() {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('netsuiteexport/adminhtml_netsuiteconnect_order_grid')->toHtml()
        );
    }
	public function exportAction() {
		$ids = $this->getRequest()->getParam('order_id');
		if($ids) {
			if(!is_array($ids)) {
				$ids = array($ids);
			}
			try {
				$result = Mage::getModel('netsuiteexport/export')->ManuallyExport($ids);
				if(is_array($result) && isset($result['status']) && isset($result['message'])) {
                    if ($result['status'] == false) {
                        $this->_getSession()->addError($this->__($result['message']));
                    } else {
                        $this->_getSession()->addSuccess($this->__($result['message']));
                        // prepare download file
//                        $file = $result['file_path'];
//                        if (file_exists($file)) {
//                            header('Content-Disposition: attachment; filename="'.basename($file).'"');
//                            header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
//                            header('Content-Transfer-Encoding: binary');
//                            header('Content-Length: ' . filesize($file));
//                            header('Cache-Control: must-revalidate');
//                            header('Pragma: public');
//                            ob_clean();
//                            flush();
//                            readfile($file);
//                        }
                    }
				} else {
					$this->_getSession()->addError($this->__('Failed to export, please check the time or payment method filter in configuration area and the order status should satisfy the condition: "pending" for offline methods(COD,atm....), "invoiced" for all other instant methods(paypal, kbank,123,cc2p....)'));
				}
			}
			catch(Exception $e) {
				$this->_getSession()->addError($e->getMessage());
			}
		}
		else {
			$this->_getSession()->addWarning($this->__('No order was selected'));
		}
		$this->_redirect('*/*/index');
	}

    protected function _isAllowed()
    {
        return true;
    }
}