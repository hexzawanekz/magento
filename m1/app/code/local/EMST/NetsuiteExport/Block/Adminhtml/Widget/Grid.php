<?php
/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade NetsuiteExport to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_NetsuiteExport
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
*/
class EMST_NetsuiteExport_Block_Adminhtml_Widget_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

	protected $_shippingMethods = null;
	protected $_addedIncrement = null;
	// Netsuite Fields
	protected $_netsuite = array();
	// protected $_itemTypes = array('product_price','shipping_amount','cod_fee');
	protected $_storeId = null;
	protected $_helper = null;
    /**
     * Retrieve file content from file container array
     *
     * @param array $fileData
     * @return string
     */
    protected function _getFileContainerContent(array $fileData)
    {
        $io = new Varien_Io_File();
        $path = $io->dirname($fileData['value']);
        $io->open(array('path' => $path));
        return $io->read($fileData['value']);
    }

	protected function getShippingMethods() {
		if(!$this->_shippingMethods) {
			$this->_shippingMethods = $this->helper()->getStoreShippingMethods();
		}
		return $this->_shippingMethods;
	}
	/**
     * Add additional columns for csv export file
     *
     * @return array
     */
	protected function _getExportColumns() {
		$itemFieldName = $this->helper()->getItemFieldName();
		$columns = array(
			'invoice_created_at'=>'Timestamp',
			'increment_id'		=>'Order #',
			'customer_id'		=>'Customer',			
			'order_date'		=>'Order Date',			
			'order_date_time'	=>'Order Date With Time',			
			'customer_email'	=>'Email',
			'payment_method'	=>'Payment Type',
			'payment_received'	=>'Payment Received', 
			'shipping_method'	=>'Shipping Type',
			// 'shipping_type'		=>'Shipping Type',
			'billing_name'		=>'Billing Addressee',
			'billing_address1'	=>'Billing Address Line 1',
			'billing_address2'	=>'Billing Address Line 2',
			'billing_city'		=>'Billing Address City',
			'billing_region'	=>'Billing Address State/Province',
			'billing_country_id'=>'Billing Address Country',
			'billing_postcode'	=>'Billing Address Postal Code',
			'billing_phone'		=>'Billing Address Phone',
			'shipping_name'		=>'Shipping Addressee',
			'shipping_address1'	=>'Shipping Address Line 1',
			'shipping_address2'	=>'Shipping Address Line 2',
			'shipping_city'		=>'Shipping Address City',
			'shipping_region'	=>'Shipping Address State/Province',
			'shipping_country_id'=>'Shipping Address Country',
			'shipping_postcode'	=>'Shipping Address Postal Code',
			'shipping_phone'	=>'Shipping Address Phone',
			$itemFieldName		=>		'Item',
			'item_qty'			=>'Item Qty',
			'item_rate'			=>'Item Rate',
			// 'discount_amount'	=>'Discount Item', // remove discount column, make them to be line items
			'item_price_level'	=>'Item Price Level',
			'warehouse_location'=>'Location',			
			'business_unit'		=>'Business Unit',
			'tax_code'			=>'Tax Code',
			'tax'				=>'Tax',
			'amount'			=>'Amount',
			'gross_total'	  	=>'Gross Total',
			// 'subsidiary'		=>'Subsidiary',
		);
		return $columns;
	}
	protected function _getCurrencyCode($item) {
		return $item->getData('order_currency_code');
	}
	protected function _getColumnValue(Varien_Object $item,$index) {
		$type = $item->getType();
		$currencyCode = $this->_getCurrencyCode($item);
		// get shipping name as "Shipping Type"
		/* if($index=='shipping_type') {
			$methods = $this->getShippingMethods();
			return (isset($methods[$index]))?$methods[$index]['name']:'';
			return $this->helper()->getShippingMethod($item);
		} */
		if($index == 'billing_address1') {
			return $this->helper()->getBillingStreet($item);
		}
		if($index == 'billing_address2') {
			return $this->helper()->getBillingStreet($item,2);
		}
		if($index == 'shipping_address1') {
			return $this->helper()->getShippingStreet($item);
		}
		if($index == 'shipping_address2') {
			return $this->helper()->getShippingStreet($item,2);
		}
		if($index == 'item_price_level' && $type) {
			return $this->helper()->renderPriceLevel($item);
		}
		if($index == 'item_qty') {
			return $this->helper()->rederQty($item);
		}
		if($index == 'item_rate') {						
			return $this->helper()->renderItemPrice($item);
		}
		if($index == 'billing_phone') {						
			return $this->helper()->renderPhone($item->getData($index));
		}
		if($index == 'shipping_phone') {						
			return $this->helper()->renderPhone($item->getData($index));
		}
		/* if($index == 'discount_amount' && $type) {
			// if($type == 'shipping_amount') {
				// $sDiscount = $item->getData('shipping_discount_amount');
				// $sDiscount = $this->_renderPrice($currencyCode,$sDiscount);
				// return $sDiscount?$sDiscount:null;
			// }
			// elseif($type == 'product_price') {
				// $iDiscount = $item->getData('item_discount_amount');
				// $iDiscount = $this->_renderPrice($currencyCode,$iDiscount);
				// return $iDiscount?$iDiscount:null;
			// }
			// elseif($type == 'cod_fee') {
				// return null;
			// }
			return $item->getData('coupon_rule_name');
		} */
		if($index == $this->helper()->getItemFieldName()) {
			return $this->helper()->renderItemName($item);
		}
		
		if($index == 'invoice_created_at') {
			if($item->getData('invoice_created_at')) {
				return $this->helper()->formatDateTime($item->getData('invoice_created_at'),'d/m/Y  H:i:s');
			}
			return $this->helper()->formatDateTime($item->getData('order_created_at'),'d/m/Y  H:i:s');
		}
		if($index == 'order_date') {			
			return $this->helper()->formatDateTime($item->getData('order_created_at'),'d/m/Y');
		}		
		if($index == 'order_date_time') {			
			return $this->helper()->formatDateTime($item->getData('order_created_at'),'d/m/Y H:i:s');
		}
		if($index == 'shipping_country_id' || $index == 'billing_country_id') {
			return $this->helper()->renderCountry($item->getData($index));
		}
		if($index == 'tax_code') {
			return $this->helper()->renderTaxCode($item);
		}
		if($index == 'gross_total') {
			return $this->helper()->renderGrossTotal($item);
		}
		if(in_array($index,array('warehouse_location','business_unit','subsidiary'))) {
			return $this->helper()->renderOtherConfig($index,$item);
		}		
		if($column = $this->getColumn($index)) {
			return $column->getRowFieldExport($item);
		}		
		return $item->getData($index);
	}
	public function helper($name='netsuiteexport') {
		return parent::helper($name);
	}
     /**
     * Retrieve Grid data as CSV
     *
     * @return string
     */
    public function getCsv()
    {
        $csv = '';
        $this->_isExport = true;
        $this->_prepareGrid();
        $this->getCollection()->getSelect()->limit();
        $this->getCollection()->setPageSize(0);
        $this->getCollection()->load();
        $this->_afterLoadCollection();

        $data = array();
        foreach ($this->_getExportColumns() as $header) {            
            $data[] = '"'.$header.'"';
        }
        $csv.= implode(',', $data)."\n";

        foreach ($this->getCollection() as $item) {
			if($this->_addedIncrement != $item->getIncrementId()) {
				foreach($this->helper()->getItemType() as $type) {
					if(!$item->getData('cod_fee') && $type == EMST_NetsuiteExport_Helper_Data::ITEM_TYPE_COD_FEE) {
						continue;
					}
					if(!$item->getData('rewardpoints') && $type == EMST_NetsuiteExport_Helper_Data::ITEM_TYPE_REWARD_POINTS) {
						continue;
					}
					if(!$item->getData('shipping_incl_tax') && $type == EMST_NetsuiteExport_Helper_Data::ITEM_TYPE_SHIPPING_AMOUNT) {
						continue;
					}
					$discount = abs(abs($item->getData('discount_amount')) - abs($item->getData('rewardpoints')));
					if(((float)$discount<=0.001 || (int)$discount == 0) && $type == EMST_NetsuiteExport_Helper_Data::ITEM_TYPE_DISCOUNT_RULES) {
						continue;
					}
					$csv.= $this->getExportContent($item,$type);
				}
			}
			else {
				$csv.= $this->getExportContent($item);
			}
			$this->_addedIncrement = $item->getIncrementId();
        }

        return $csv;
    }
	protected function getExportContent($item,$type=EMST_NetsuiteExport_Helper_Data::ITEM_TYPE_PRODUCT_PRICE) {
		$data = array();
		foreach ($this->_getExportColumns() as $field => $header) {
			$item->setType($type);
			$data[] = '"' . str_replace(array('"', '\\'), array('""', '\\\\'),
				$this->_getColumnValue($item,$field)) . '"';
		}
		return implode(',', $data)."\n";
	}
	protected function _renderPrice($currency,$amount) {
		return $this->helper()->renderPrice($currency,$amount);
	}
}
