<?php
class EMST_NetsuiteExport_Block_Adminhtml_CustomerExport_Render_Address extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Text
{
    protected function getBillingStreet($street,$line = 1) {	
		if(!$street) {
			return $street;
		}
		return $this->getAddressline($street,$line);		
	}
	protected function getAddressline($street,$line) {
		$arr = is_array($street) ? $street : explode("\n", $street);
		if($line>1) {
			return str_replace('\n',' ' ,implode('\n',array_slice($arr,1)));
		}
		return (isset($arr[$line-1]))? $arr[$line-1]:'';
	}
}