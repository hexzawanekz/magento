<?php
/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade NetsuiteExport to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_NetsuiteExport
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
*/

class EMST_NetsuiteExport_Block_Adminhtml_CustomerExport_Grid extends EMST_NetsuiteExport_Block_Adminhtml_CustomerExport_Widget_Grid
{
	public function __construct()
	{
		parent::__construct();
		$this->setId('netsuitecustomerexportGrid');
		$this->setDefaultSort('netsuiteexport_id');
		$this->setDefaultDir('ASC');
		 $this->setUseAjax(true);
		$this->setSaveParametersInSession(true);
	}

    protected function _prepareCollection()
    {
       $collection = Mage::getResourceModel('customer/customer_collection')
            ->addAttributeToSelect('email')
            ->addAttributeToSelect('created_at')
            ->addAttributeToSelect('firstname')
            ->addAttributeToSelect('lastname')            
            ->addAttributeToSelect('group_id')
            ->joinAttribute('billing_postcode', 'customer_address/postcode', 'default_billing', null, 'left')
            ->joinAttribute('billing_city', 'customer_address/city', 'default_billing', null, 'left')
            ->joinAttribute('billing_telephone', 'customer_address/telephone', 'default_billing', null, 'left')
            ->joinAttribute('billing_region', 'customer_address/region', 'default_billing', null, 'left')
            ->joinAttribute('billing_street', 'customer_address/street', 'default_billing', null, 'left')
            ->joinAttribute('billing_country_id', 'customer_address/country_id', 'default_billing', null, 'left');
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

	protected function _prepareColumns()
	{
		$this->addColumn('entity_id', array(
            'header'    => Mage::helper('customer')->__('ID'),
            'index'     => 'entity_id',
            'type'  => 'number',
        ));
		$this->addColumn('external_id', array(
            'header'    => Mage::helper('customer')->__('External ID'),
            'index'     => 'entity_id',
			'filter' => false,
            'type'  => 'number',
        ));
        $this->addColumn('firstname', array(
            'header'    => Mage::helper('customer')->__('First Name'),
            'index'     => 'firstname'
        ));
        $this->addColumn('lastname', array(
            'header'    => Mage::helper('customer')->__('Last Name'),
            'index'     => 'lastname'
        ));
		
		$this->addColumn('telephone', array(
            'header'    => Mage::helper('customer')->__('Telephone'),
            'index'     => 'billing_telephone'
        ));
		
        $this->addColumn('email', array(
            'header'    => Mage::helper('customer')->__('Email'),
            'index'     => 'email'
        ));

        $this->addColumn('address_1', array(
            'header'    => Mage::helper('customer')->__('Address 1'),
			'renderer' => 'netsuiteexport/adminhtml_customerExport_render_address1',
            'index'     => 'billing_street',
        ));
		
		$this->addColumn('address_2', array(
            'header'    => Mage::helper('customer')->__('Address 2'),
			'renderer' => 'netsuiteexport/adminhtml_customerExport_render_address2',
            'index'     => 'billing_street',
        ));
		$this->addColumn('billing_city', array(
            'header'    => Mage::helper('customer')->__('City'),
            'index'     => 'billing_city',
        ));
		 $this->addColumn('billing_region', array(
            'header'    => Mage::helper('customer')->__('State/Province'),
            'index'     => 'billing_region',
        ));
        $this->addColumn('billing_country_id', array(
            'header'    => Mage::helper('customer')->__('Country'),
            'type'      => 'country',
            'index'     => 'billing_country_id',
        ));
		$this->addColumn('billing_postcode', array(
            'header'    => Mage::helper('customer')->__('Postal Code'),
            'index'     => 'billing_postcode',
        ));
		// $this->addColumn('netsuite_status', array(
            // 'header'    => Mage::helper('customer')->__('Status'),
            // 'index'     => 'netsuite_status',
        // ));
        // $this->addColumn('subsidiary', array(
            // 'header'    => Mage::helper('customer')->__('Subsidiary'),
            // 'index'     => 'subsidiary',
			// 'filter' => false,
        // ));
		// $this->addColumn('individual', array(
            // 'header'    => Mage::helper('customer')->__('Individual'),
            // 'index'     => 'individual',
			// 'filter' => false,
        // ));
		// $this->addColumn('net_default_billing', array(
            // 'header'    => Mage::helper('customer')->__('Default Billing'),
            // 'index'     => 'net_default_billing',
			// 'filter' => false,
        // ));
		// $this->addColumn('net_default_shipping', array(
            // 'header'    => Mage::helper('customer')->__('Default Shipping'),
            // 'index'     => 'net_default_shipping',
			// 'filter' => false,
        // ));		
		$this->addExportType('*/*/exportCsv', Mage::helper('netsuiteexport')->__('CSV'));	  
		return parent::_prepareColumns();
	}
	public function getRowUrl($row)
	{
		return null;
	}
	public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }
}