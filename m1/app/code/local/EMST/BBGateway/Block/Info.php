<?php


class EMST_BBGateway_Block_Info extends Mage_Payment_Block_Info
{
	protected function _prepareSpecificInformation($transport = null)
    {
        $transport = parent::_prepareSpecificInformation($transport);
        $payment = $this->getInfo();
		$data = $payment->getAdditionalInformation();
		$data = Mage::getModel('bbgateway/info')->filterDataForDisplaying($data);
        $transport->addData($data);
		return $transport;
    }

}
