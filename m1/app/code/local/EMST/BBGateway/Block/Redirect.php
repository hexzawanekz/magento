<?php


class EMST_BBGateway_Block_Redirect extends Mage_Core_Block_Abstract
{
	protected function _toHtml()
	{
		$bbgateway = $this->getModel();
        $form = new Varien_Data_Form();
        $form->setAction($bbgateway->getGatewayUrl())
            ->setId('bbgateway_payment_checkout')
            ->setName('bbgateway_payment_checkout')
            ->setMethod('POST')
            ->setUseContainer(true);
        foreach ($bbgateway->getStandardCheckoutFormFields() as $field => $value) {
            $form->addField($field, 'hidden', array('name' => $field, 'value' => $value));
        }

        $formHTML = $form->toHtml();

        $html = '<html>';
		$html .= '<head><meta content="text/html; charset=utf-8" http-equiv="Content-Type"></head>';
		$html .= '<body>';
        $html.= $this->__('You will be redirected to Bangkok Bank Gateway in a few seconds.');
        $html.= $formHTML;
        $html.= '<script type="text/javascript">document.getElementById("bbgateway_payment_checkout").submit();</script>';
        $html.= '</body></html>';


        return $html;
    }

}
