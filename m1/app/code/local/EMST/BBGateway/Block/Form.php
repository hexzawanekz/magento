<?php
class EMST_BBGateway_Block_Form extends Mage_Payment_Block_Form_Cc
{
	protected $_cards = array();
    protected function _construct() {
		parent::_construct();
        $this->setTemplate('bbgateway/form.phtml');        
    }
	public function getCcStored() {	
		if(sizeof($this->_cards)<=1) {
			$cards = Mage::getModel('bbgateway/card')
					->getCollection()
					->addCustomerIdFilter(Mage::getSingleton('customer/session')->getCustomer()->getId());
			foreach($cards as $card) {
				$this->_cards[$card->getCcType()] = $card->getCcNumber();
			}
			$this->_cards['new'] = $this->__('new');
		}
		return $this->_cards;
	}
	public function hasCards() {
		$cards = $this->getCcStored();		
		if(sizeof($cards)>1) {
			return true;
		}
		else {
			return false;
		}
	}
	public function getHeaderText() {
		return $this->getMethod()->getHeaderText();
	}
	public function getFooterText() {
		return $this->getMethod()->getFooterText();
	}
	public function getDes() {
		$items = Mage::getSingleton('checkout/session')->getQuote()->getAllItems();
		$pr_id = '';
		foreach ($items as $item) {
			$pr_id = $item->getProductId();
		}
		return Mage::getModel('catalog/product')->load($pr_id)->getUrlKey();
	}
	public function getAmount() {
		return Mage::getSingleton('checkout/session')->getQuote()->getGrandTotal();
	}
	public function getCurrency() {
		return $this->getMethod()->getCurrencyCode();
	}
	public function getUserDefined($num) {
		return $this->getMethod()->getUserDefined($num);
	}
	public function getPayCategoryID() {
		return $this->getMethod()->getPayCategoryID();
	}
	public function getCardHolderName() {
		if($this->getInfoData('cc_owner')) {
			return $this->getInfoData('cc_owner');
		}
		else {
			$customer = Mage::getSingleton('customer/session')->getCustomer();
			
			return $customer->getFirstname().' '.$customer->getLastname();
		}
	}
	 public function getCountryCollection()
    {
        $collection = $this->getData('country_collection');
        if (is_null($collection)) {
            $collection = Mage::getModel('directory/country')->getResourceCollection();
			if($this->getCountryFilter()) {
				$collection ->addCountryCodeFilter($this->getCountryFilter());
			}
            $this->setData('country_collection', $collection);
        }
		
        return $collection;
    }

    public function getCountryHtmlSelect($defValue=null, $name='payment[panCountry]', $id=null, $title='Country of Issuer Bank')
    {
        if (is_null($defValue)) {
            $defValue = $this->getCountryId();
        }
		if(!$id) {
			$id = $this->getMethodCode().'_cc_country';
		}
        $cacheKey = 'DIRECTORY_COUNTRY_SELECT_ALL_'.Mage::app()->getStore()->getCode();
        if (Mage::app()->useCache('config') && $cache = Mage::app()->loadCache($cacheKey)) {
            $options = unserialize($cache);
        } else {
            $options = $this->getCountryCollection()->toOptionArray(Mage::helper('directory')->__('--Please Select--'));
            if (Mage::app()->useCache('config')) {
                Mage::app()->saveCache(serialize($options), $cacheKey, array('config'));
            }
        }
        $html = $this->getLayout()->createBlock('core/html_select')
            ->setName($name)
            ->setId($id)
            ->setTitle(Mage::helper('directory')->__($title))
            ->setClass('validate-select')
            ->setValue($defValue)
            ->setOptions($options)
            ->getHtml();
        return $html;
    }
	public function getCountryId()
    {
        $countryId = $this->getData('country_id');
        if (is_null($countryId)) {
            $countryId = Mage::helper('core')->getDefaultCountry();
        }
        return $countryId;
    }
	public function getCcAvailableCountry() {		
		$selected = $this->getInfoData('panCountry')?$this->getInfoData('panCountry'):null;
		if($this->getMethod()->getConfigData('allowspecific')==1){
            $availableCountries = explode(',', $this->getMethod()->getConfigData('specificcountry'));
			$this->setCountryFilter($availableCountries);
        }
		return $this->getCountryHtmlSelect($selected);
	}
	
	public function getCustomerEmail() {
		$stored_email = $this->htmlEscape($this->getInfoData('cardholderEmail'));
		$customer_email = Mage::getSingleton('customer/session')->getCustomer()->getEmail();
		return $stored_email ? $stored_email: $customer_email;
	}
}
