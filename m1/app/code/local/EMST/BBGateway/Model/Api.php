<?php


/**
 * BBGateway payment model
 *
 */
class EMST_BBGateway_Model_Api extends Varien_Object
{
	protected $_payRef 		= null;
	protected $_order 		= null;
	protected $_payModel 	= null;
	
	CONST ACTION_QUERY 			= 'Query';
	CONST ACTION_VOID 			= 'Void';
	CONST ACTION_CAPTURE 		= 'Capture';
	CONST ACTION_REVERSALAUTH 	= 'ReversalAuth';
	
    protected function _getPayRef() {
		if(!$this->_payRef) {
			$this->_payRef = $this->getPayRef();
		}
		return $this->_payRef;
	}
	protected function _getOrder() {
		if(!$this->_order) {
			$this->_order = $this->getOrder();
		}
		return $this->_order;
	}
	protected function _getPayModel() {
		if(!$this->_payModel) {
			$this->_payModel = $this->getPayModel();
		}
		return $this->_payModel;
	}
	public function void() {
		if(!$this->_validate()) {
			return $this;
		}		
		$result = $this->setAction(self::ACTION_VOID)->_httpRequest();
		$return = $result->getResponse();
		if(isset($return['resultCode']) && (string)$return['resultCode']=='0') {
			$result->setValidatedStatus(true);
		}
		return $result;
	}
	public function query() {
		if(!$this->_validate()) {
			return $this;
		}
		$result = $this->setAction(self::ACTION_QUERY)->_httpRequest();		
		$returnedXml = $result->getResponse();		
		if($returnedXml && (string)$returnedXml->ResultCode=='0') {
			$result->setValidatedStatus(true);
		}
		return $result;
		
	}
	public function reversal() {
		if(!$this->_validate()) {
			return $this;
		}		
		$result = $this->setAction(self::ACTION_REVERSALAUTH)->_httpRequest();
		$return = $result->getResponse();
		if(isset($return['resultCode']) && (string)$return['resultCode']=='0') {
			$result->setValidatedStatus(true);
		}
		return $result;
	}
	public function capture() {
		if(!$this->_validate()) {
			return $this;
		}		
		$result = $this->setAction(self::ACTION_CAPTURE)->_httpRequest();
		$return = $result->getResponse();
		if(isset($return['resultCode']) && (string)$return['resultCode']=='0') {
			$result->setValidatedStatus(true);
		}
		return $result;
	}
	protected function _prepareData() {
		$payModel = $this->_getPayModel();
		$data = array(
			'merchantId'=>$payModel->getMerchantId(),
			'loginId'=>$payModel->getApiUsername(),
			'password'=>$payModel->getApiPassword(),
			'actionType'=>$this->getAction(),			
			'payRef'=>$this->_getPayRef(),			
		);
		switch($this->getAction()) {
			case self::ACTION_QUERY: 
				$data['orderRef'] = $this->_getOrder()->getIncrementId();
				break;
			case self::ACTION_CAPTURE:
				$data['amount'] = Mage::helper('bbgateway')->formatAmount($this->_getOrder()->getBaseGrandTotal());
				break;
			default:
				break;
		}
		return $data;
	}
	protected function _validate() {
		if(!$this->_getPayModel() || !$this->_getOrder() || !$this->_getPayRef()) {
			return false;
		}
		return true;
	}
	protected function _getResult() {
		return Mage::getModel('bbgateway/result');
	}
	protected function _httpRequest($xml = false) {	
		$request = $this->_prepareData();		
		$request = ($xml)?$this->_getPayModel()->getSimpleXml($request):http_build_query($request);
		$this->_getPayModel()->debugData(array('Request'=>$request),$this->getAction());
		$return = $this->_request($request);
		if(!$xml) {			
			$return = explode('&',$return);
		}	
		$this->_getPayModel()->debugData(array('Result'=>$return),$this->getAction());
		if($xml) {
			$return = Mage::getModel('core/config_base',$return)->getNode();
		}
		$result = $this->_getResult()
					->setResponse($return);
		return $result;
	}
	protected function _request($xml) {
		$http = new Varien_Http_Adapter_Curl();
		$http->write(Zend_Http_Client::POST,$this->_getPayModel()->getApiUrl(), '1.1', array(), $xml); // 'Content-Type: text/xml'
        $result=$http->read();
		if (Zend_Http_Response::extractCode($result) == 200) {
			$result = preg_split('/^\r?$/m', $result, 2);
			$result = trim($result[1]);
		}
		return $result;
	}
}
