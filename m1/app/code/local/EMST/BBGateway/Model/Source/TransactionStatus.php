<?php
class EMST_BBGateway_Model_Source_TransactionStatus
{
	public function getTransactionStatus() {
		return array(
			"A" => "Approved" ,
			"S" => "Settled" ,
			"V" => "Voided (Canceled)" ,
			"D" => "Declined by the issuer Host" ,
			"F" => "Failed" 
		);
	}
}
