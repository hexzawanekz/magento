<?php
class EMST_BBGateway_Model_Source_PaymentTypes
{
	public function getPaymentTypes() {
		return array(
			EMST_BBGateway_Model_Bbgateway::TYPE_SALES 	=> "Sales" ,
			EMST_BBGateway_Model_Bbgateway::TYPE_AUTH 	=> "Authorization" ,
		);
	}
	public function getPaymenTypeLable($type) {
		$types = $this->getPaymentTypes();
		if(isset($types[$type])) {
			return $types[$type];
		}
		return $type;
	}
	public function toOptionArray()
    {
		$options = array();
		foreach($this->getPaymentTypes() as $vl=>$lb) {
			$options[] = array('value' => $vl, 'label' => $lb);
		}
        return $options;
    }
}
