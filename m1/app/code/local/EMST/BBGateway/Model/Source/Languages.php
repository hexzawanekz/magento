<?php
class EMST_BBGateway_Model_Source_Languages
{
	public function getLanguages() {
		return array(
			"E" => "English",
			// "T" => "Thai",
		);
	}
	public function toOptionArray()
    {
		$options = array();
		foreach($this->getLanguages() as $vl=>$lb) {
			$options[] = array('value' => $vl, 'label' => $lb);
		}
        return $options;
    }
}
