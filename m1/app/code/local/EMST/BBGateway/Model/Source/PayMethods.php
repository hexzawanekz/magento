<?php
class EMST_BBGateway_Model_Source_PayMethods
{
	public function PayMethods() {
		return array(
			"ALL" 	=> " All available payment methods" ,
			"CC" 	=> "Credit Card Payment " ,
		);
	}
	public function toOptionArray()
    {
		$options = array();
		foreach($this->PayMethods() as $vl=>$lb) {
			$options[] = array('value' => $vl, 'label' => $lb);
		}
        return $options;
    }
}
