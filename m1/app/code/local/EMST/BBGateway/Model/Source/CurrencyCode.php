<?php
class EMST_BBGateway_Model_Source_CurrencyCode
{
	public function getCurrencyCode() {
		return array(
			"764" => "THB",
			"840" => "USD",
		);
	}
	public function toOptionArray()
    {
		$options = array();
		foreach($this->getCurrencyCode() as $vl=>$lb) {
			$options[] = array('value' => $vl, 'label' => $lb);
		}
        return $options;
    }
}