<?php
class EMST_BBGateway_Helper_Data extends Mage_Core_Helper_Abstract{
	
	public function formatAmount($amount) {
		return sprintf('%.2f',$amount);
	}
} 
