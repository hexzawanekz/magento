<?php
class EMST_BBGateway_PaymentController extends Mage_Core_Controller_Front_Action
{
	protected $_order = null;
	protected $_checkout_session = null;
    protected function getOrder() {
		if ($this->_order == null)
            $this->_order = Mage::getModel('sales/order')->loadByIncrementId($this->getSession()->getLastRealOrderId());
        return $this->_order;
    }
	public function setOrder(Mage_Sales_Model_Order $order) {
		$this->_order = $order;
	}
	protected function getSession() {
		if(!$this->_checkout_session)
			$this->_checkout_session=Mage::getSingleton('checkout/session');
		return $this->_checkout_session;
	}
	public function redirectAction() {
		try {
			$order = $this->getOrder();	
			$bbgateway = Mage::getModel('bbgateway/bbgateway')->setOrder($order);
			$session = $this->getSession();
			$allowState  = array(
					Mage_Sales_Model_Order::STATE_NEW
				);
			if(!$order->getId()||!in_array($order->getState(),$allowState) || $session->getRedirected() == $order->getId() || !$bbgateway->validateRedirect($order)) {				
				Mage::getSingleton('checkout/session')->clear();
				$this->_redirect('');
				return;
			}
			$order->addStatusHistoryComment(Mage::helper('bbgateway')->__('Customer was redirected to iPay'))->save();
			$this->getResponse()
			->setBody($this->getLayout()
			->createBlock('bbgateway/redirect')
			->setModel($bbgateway)
			->toHtml());
			$session->setRedirected($order->getId());
		}
		catch(Exception $e) {
			Mage::log($e->getMessage(),null, 'BBGateway.log');
			$this->_getCoreSession()->addError($this->__('There was an error, please try again'));
			$this->getSession()->clear();
			$this->_redirect('');
		}
	}
	public function cancelAction() {
		try {
			$orderRef = $this->getRequest()->getParam('Ref');
			$order = Mage::getModel('sales/order')->loadByIncrementId($orderRef);
			$customer = Mage::getSingleton('customer/session')->getCustomer();
			if($customer->getEmail() == $order->getCustomerEmail()) {
				$this->_getCoreSession()->addError(Mage::helper('bbgateway')->__('Your transaction was canceled'));				
			}
		}
		catch(Exception $e) {
			Mage::log($e->getMessage(),null, 'BBGateway.log');
			$this->_getCoreSession()->addError(Mage::helper('bbgateway')->__('There was an error occur while trying to verify your transaction'));
		}
		$this->_redirect('');
	}
	public function failAction() {
		try {
			$orderRef = $this->getRequest()->getParam('Ref');
			$order = Mage::getModel('sales/order')->loadByIncrementId($orderRef);
			$customer = Mage::getSingleton('customer/session')->getCustomer();
			if($customer->getEmail() == $order->getCustomerEmail()) {
				$this->_getCoreSession()->addError(Mage::helper('bbgateway')->__('The transaction was not go through'));				
			}
		}
		catch(Exception $e) {
			Mage::log($e->getMessage(),null, 'BBGateway.log');
			$this->_getCoreSession()->addError(Mage::helper('bbgateway')->__('There was an error occur while trying to verify your transaction'));
		}
		$this->_redirect('');
	}
	public function successAction() {
		try {
			$orderRef = $this->getRequest()->getParam('Ref');
			$order = Mage::getModel('sales/order')->loadByIncrementId($orderRef);
			$customer = Mage::getSingleton('customer/session')->getCustomer();
			if($customer->getEmail() == $order->getCustomerEmail()) {
				$this->_redirect('checkout/onepage/success');
				return;
			}
		}
		catch(Exception $e) {
			Mage::log($e->getMessage(),null, 'BBGateway.log');
			$this->_getCoreSession()->addError(Mage::helper('bbgateway')->__('There was an error occur while trying to verify your transaction'));
		}
		$this->_redirect('');
	}
	protected function _getResponse() {
		$params =  array(
			'src'			=>'',
			'prc'			=>'',
			'Ord'			=>'',
			'Holder'		=>'',
			'successcode'	=>'',
			'Ref'			=>'',
			'orderRef1'		=>'',
			'orderRef2'		=>'',
			'orderRef3'		=>'',
			'orderRef4'		=>'',
			'orderRef5'		=>'',
			'PayRef'		=>'',
			'Amt'			=>'',
			'Cur'			=>'',
			'remark'		=>'',
			'AuthId'		=>'',
			'eci'			=>'',
			'payerAuth'		=>'',
			'sourceIp'		=>'',
			'ipCountry'		=>'',
			'cc0104'		=>'',
			'cc1316'		=>'',
			'baseAmt'		=>'',
			'baseCur'		=>'',
			'exRate'		=>'',
		);
		$data = $this->getRequest()->getParams();
		return array_merge($params,$data);
	}
	public function iPayDFAction() {
		try{
			echo 'OK';
            $response = $this->_getResponse();	
		    $bbgateway = Mage::getModel('bbgateway/bbgateway');
		    $order = Mage::getModel('sales/order')->loadByIncrementId(trim($response['Ref']));
			if(!$order->getId()) {
				return;
			}
		    $bbgateway->setOrder($order)->setInfo($response);
			$msg = '';
		    if((string)$response['successcode'] == '0' && $order->getBaseGrandTotal() == $response['Amt']) {
				if($bbgateway->useApi()) {
					$return = $bbgateway->query();
					$result = $return->getValidatedStatus();
					if($result) {
						$invoiced = $bbgateway->saveInvoice();
					}
				}
				else {
					$invoiced = $bbgateway->saveInvoice();					
				}
				if(isset($invoiced) && !$invoiced) {
					$msg = Mage::helper('bbgateway')->__('Failed to invoice order');
				}
		    }
		    else {
				$result = false;
				if(isset($response['PayRef'])) {
					$bbgateway->logInfo();
				}
		    }
			if(!$result) {
				$msg = Mage::helper('bbgateway')->__('The payment was refused by the gateway');
			}
			if($msg) {
				$bbgateway->getOrder->addStatusHistoryComment($msg)->save();
			}			
			$bbgateway->debugData(array('Return'=>$request),'DataFeed');
		}
		catch(Exception $e) {			
			Mage::log($e->getMessage(),null, 'BBLGateway.log');
		}		
	}
	protected function _getCoreSession() {
		return Mage::getSingleton('core/session');
	}
}
