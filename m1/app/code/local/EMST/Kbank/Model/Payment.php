<?php

class EMST_Kbank_Model_Payment extends Mage_Payment_Model_Method_Abstract
{
    protected $_code = 'kbank_payment';
    protected $_formBlockType = 'kbank/form';
    // Payment configuration
    protected $_isGateway = false;
    protected $_canAuthorize = true;
    //protected $_canCapture              = true;
    protected $_canCapturePartial = false;
    protected $_canRefund = false;
    protected $_canVoid = false;
    protected $_canUseInternal = false;
    protected $_canUseCheckout = true;
    protected $_canUseForMultishipping = false;
    // Order instance
    protected $_order = null;

    /**
     * validate payment before redirect
     *
     * @param Mage_Sales_Model_Order $order order
     *
     * @return boolean
     */
    public function validateRedirect($order)
    {
        $method = $order->getPayment()->getMethodInstance();
        if ($method->getCode() == $this->getCode()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * validate payment on checkout page
     *
     * @param Mage_Sales_Model_Order_Quote $quote quote
     *
     * @return boolean
     */
    public function isAvailable($quote = null)
    {
        $active = parent::isAvailable($quote);
        if ($quote && $active) {
            if ($this->getConfigData('dev')) {
                $allowedEmails = explode(',', $this->getConfigData('email'));
                $customerEmail = Mage::getSingleton('customer/session')->getCustomer()->getEmail();
                if (in_array($customerEmail, $allowedEmails)) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return true;
            }
        }
        return $active;
    }

    /**
     * get kbank url
     *
     * @param int $storeId store id
     *
     * @return string
     */
    public function getKbankUrl($storeId = null)
    {
        $url = $this->getConfigData('gateway', $storeId);
        return $url;
    }

    /**
     * get customer text
     *
     * @param int $storeId store id
     *
     * @return string
     */
    public function getCustomText($storeId = null)
    {
        $customtext = $this->getConfigData('customtext', $storeId);
        return $customtext;
    }

    /**
     * get url 2
     *
     * @return string
     */
    protected function getURL2()
    {
        return Mage::getUrl('kbank/payment/return', array('_secure' => true));
    }

    /**
     * get res url
     *
     * @return string
     */
    protected function getResURL()
    {
        //return Mage::getUrl('', array('_secure' => true));
        return Mage::getUrl('kbank/payment/return', array('_secure' => true));
    }

    /**
     * get Success URL
     *
     * @return string
     */
    protected function getSuccessURL()
    {
        return Mage::getUrl('checkout/onepage/success', array('_secure' => true));
    }

    /**
     * get error url
     *
     * @return type
     */
    protected function getErrorURL()
    {
        return Mage::getUrl('kbank/payment/error', array('_secure' => true));
    }

    /**
     * Capture payment
     *
     * @param Varien_Object $payment payment
     * @param decimal       $amount  amount
     *
     * @return Mage_Payment_Model_Abstract
     */
    public function capture(Varien_Object $payment, $amount)
    {
        $payment->setStatus(self::STATUS_APPROVED)
                ->setLastTransId($this->getTransactionId());

        return $this;
    }

    /**
     *  Form block description
     *
     * @param string $name name
     *
     * @return	 object
     */
    public function createFormBlock($name)
    {
        $block = $this->getLayout()->createBlock('kbank/form_payment', $name);
        $block->setMethod($this->_code);
        $block->setPayment($this->getPayment());

        return $block;
    }

    /**
     * Return Order Place Redirect URL
     *
     * @return string
     */
    public function getOrderPlaceRedirectUrl()
    {
        return Mage::getUrl('kbank/payment/redirect');
    }

    /**
     * Return Standard Checkout Form Fields for request to Kbank
     *
     * @return	array
     */
    public function getStandardCheckoutFormFields()
    {
        $session = Mage::getSingleton('checkout/session');

        $order = $this->getOrder();
        if (!($order instanceof Mage_Sales_Model_Order)) {
            Mage::throwException($this->_getHelper()->__('Cannot retrieve order object'));
        }
        $detail2 = '';
        foreach ($order->getAllItems() as $item) {
            $detail2 = $item->getName();
            break;
        }
        $amount = $order->getBaseGrandTotal() * 100;
        $parameter = array('MERCHANT2' => $this->getConfigData('merchant'),
            'TERM2' => $this->getConfigData('term'),
            'AMOUNT2' => sprintf('%d', $amount),
            'URL2' => $this->getURL2(),
            'RESPURL' => $this->getResURL(),
            'IPCUST2' => $order->getRemoteIp(),
            'DETAIL2' => $detail2,
            'INVMERCHANT' => $order->getRealOrderId(),
            '_input_charset' => 'utf-8',
        );

        $parameter = $this->para_filter($parameter);
        $securityCode = $this->getConfigData('security_code');
        $signType = 'MD5';

        $sortArray = array();
        $arg = "";
        $sortArray = $this->arg_sort($parameter); //$parameter

        while (list ($key, $val) = each($sortArray)) {
            $arg .= $key . "=" . $val . "&";
        }

        $prestr = substr($arg, 0, count($arg) - 2);

        $mysign = $this->sign($prestr . $securityCode);

        $fields = array();
        $sortArray = array();
        $arg = "";
        $sortArray = $this->arg_sort($parameter); //$parameter
        while (list ($key, $val) = each($sortArray)) {
            $fields[$key] = $val;
        }
        $fields['sign'] = $mysign;
        $fields['sign_type'] = $signType;
        return $fields;
    }

    /**
     * sign
     *
     * @param string $prestr prestr
     *
     * @return string
     */
    public function sign($prestr)
    {
        $mysign = md5($prestr);
        return $mysign;
    }

    /**
     * para filter
     *
     * @param array $parameter parameter
     *
     * @return array
     */
    public function para_filter($parameter)
    {
        $para = array();
        while (list ($key, $val) = each($parameter)) {
            if ($key == "sign" || $key == "sign_type" || $val == "") {
                continue;
            } else {
                $para[$key] = $parameter[$key];
            }
        }
        return $para;
    }

    /**
     * arg sort
     *
     * @param array $array array
     *
     * @return array
     */
    public function arg_sort($array)
    {
        ksort($array);
        reset($array);
        return $array;
    }

    /**
     * charset encode
     *
     * @param string $input         input
     * @param string $outputCharset output charset
     * @param string $inputCharset  input charset
     *
     * @return string
     */
    public function charset_encode($input, $outputCharset, $inputCharset = "GBK")
    {
        $output = "";
        if ($inputCharset == $outputCharset || $input == null) {
            $output = $input;
        } elseif (function_exists("mb_convert_encoding")) {
            $output = mb_convert_encoding($input, $outputCharset, $inputCharset);
        } elseif (function_exists("iconv")) {
            $output = iconv($inputCharset, $outputCharset, $input);
        } else {
            die("sorry, you have no libs support for charset change.");
        }
        return $output;
    }

    /**
     * get Authorized Languages
     *
     * @return boolean
     */
    protected function _getAuthorizedLanguages()
    {
        $languages = array();

        foreach (Mage::getConfig()->getNode('global/payment/kbank_payment/languages')->asArray() as $data) {
            $languages[$data['code']] = $data['name'];
        }

        return $languages;
    }

    /**
     * get Language Code
     *
     * @return string
     */
    protected function _getLanguageCode()
    {
        // Store language
        $language = strtoupper(substr(Mage::getStoreConfig('general/locale/code'), 0, 2));

        // Authorized Languages
        $authorizedLanguages = $this->_getAuthorizedLanguages();

        if (count($authorizedLanguages) === 1) {
            $codes = array_keys($authorizedLanguages);
            return $codes[0];
        }

        if (array_key_exists($language, $authorizedLanguages)) {
            return $language;
        }

        // By default we use language selected in store admin
        return $this->getConfigData('language');
    }

    /**
     * Output failure response and stop the script
     *
     * @return void
     */
    public function generateErrorResponse()
    {
        die($this->getErrorResponse());
    }

    /**
     * get Success Response
     *
     * @return string
     */
    public function getSuccessResponse()
    {
        $response = array(
            'Pragma: no-cache',
            'Content-type : text/plain',
            'Version: 1',
            'OK'
        );
        return implode("\n", $response) . "\n";
    }

    /**
     * Return response for Kbank failure payment
     *
     * @return	string
     */
    public function getErrorResponse()
    {
        $response = array(
            'Pragma: no-cache',
            'Content-type : text/plain',
            'Version: 1',
            'Document falsifie'
        );
        return implode("\n", $response) . "\n";
    }

    /**
     * get new status
     *
     * @param int $storeId store id
     *
     * @return string
     */
    public function getNewStatus($storeId = null)
    {
        return $this->getConfigData('order_status', $storeId);
    }

    /**
     * get Refuse Status
     *
     * @param int $storeId store id
     *
     * @return string
     */
    public function getRefuseStatus($storeId = null)
    {
        return $this->getConfigData('order_status_payment_refused', $storeId);
    }

    /**
     * get Accept Status
     *
     * @param int $storeId store id
     *
     * @return string
     */
    public function getAcceptStatus($storeId = null)
    {
        return $this->getConfigData('order_status_payment_accepted', $storeId);
    }

    /**
     * initialize
     *
     * @param string $paymentAction payment Action
     * @param object $stateObject   state Object
     *
     * @return \EMST_Kbank_Model_Payment
     */
    public function initialize($paymentAction, $stateObject)
    {
        $state = $this->getNewStatus();
        $stateObject->addData(array('state' => $state, 'status' => $state));
        return $this;
    }

    /**
     * get transaction url
     *
     * @param int $storeId store id
     *
     * @return string
     */
    public function getTransactionUrl($storeId = null)
    {
        return $this->getConfigData('transaction_url', $storeId);
    }

    /**
     * get user name
     *
     * @param int $storeId store id
     *
     * @return string
     */
    public function getUserName($storeId = null)
    {
        return $this->getConfigData('user_name', $storeId);
    }

    /**
     * get merchant id
     *
     * @param int $storeId store id
     *
     * @return string
     */
    public function getMerchantId($storeId = null)
    {
        return $this->getConfigData('merchant', $storeId);
    }

    /**
     * get auto approve
     *
     * @param int $storeId store id
     *
     * @return boolean
     */
    public function getAutoApprove($storeId = null)
    {
        return $this->getConfigData('auto_approve', $storeId);
    }

    /**
     * check authentication ip
     *
     * @param int $storeId store id
     *
     * @return boolean
     */
    public function isAuthenticationIp($storeId = null)
    {
        $authentication = $this->getConfigData('authentication_ip', $storeId);
        if ($authentication) {
            return (count($this->getIpAddress()) > 0);
        }
        return false;
    }

    /**
     * get ip address
     *
     * @param int $storeId store id
     *
     * @return array
     */
    public function getIpAddress($storeId = null)
    {
        $ips = $this->getConfigData('ip_address', $storeId);
        $ipAddress = array();
        foreach (explode(',', $ips) as $ip) {
            $ipAddress[] = trim($ip);
        }
        return $ipAddress;
    }
}