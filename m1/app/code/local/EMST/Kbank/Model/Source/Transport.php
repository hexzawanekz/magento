<?php

class EMST_Kbank_Model_Source_Transport
{
    public function toOptionArray()
    {
        return array(
            array('value' => 'https', 'label' => Mage::helper('kbank')->__('https')),
            array('value' => 'http', 'label' => Mage::helper('kbank')->__('http')),
        );
    }
}