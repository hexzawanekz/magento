<?php

class EMST_Kbank_Model_Approval
{
    protected $_paymentModel = null;

    /**
     * get excluded states
     *
     * @return array
     */
    protected function _getExcludedStates()
    {
        return array(
            Mage_Sales_Model_Order::STATE_CLOSED,
            Mage_Sales_Model_Order::STATE_CANCELED,
            Mage_Sales_Model_Order::STATE_HOLDED,
            Mage_Sales_Model_Order::STATE_PROCESSING,
            Mage_Sales_Model_Order::STATE_COMPLETE,
        );
    }

    /**
     * get sales order by store
     *
     * @param int $storeId store id
     *
     * @return Mage_Sales_Order_Collection
     */
    public function getOrders($storeId)
    {
        $payment = $this->_getPaymentModel();
        if ($payment->getAutoApprove($storeId) && $payment->getTransactionUrl($storeId)) {
            $orders = Mage::getResourceModel('sales/order_collection')
                    ->addFieldToFilter('main_table.status', array('nin' => $this->_getExcludedStates()))
                    ->addFieldToFilter('main_table.store_id', array('eq' => $storeId))
                    ->join(
                            array('payment' => 'sales/order_payment'), 'main_table.entity_id = payment.parent_id',
                            array('payment_method' => 'payment.method')
                    )
                    ->setOrder('main_table.entity_id', Varien_Db_Select::SQL_DESC);
            $orders->getSelect()
                    ->where('main_table.created_at <= DATE_SUB(?, INTERVAL 30 MINUTE)', Varien_Date::now())
                    ->where('payment.method = ?', $payment->getCode())
                    ->limit(50);
            echo "retrive data from store : " . $storeId . "\n";
            return $orders;
        }
        return null;
    }

    /**
     * get kbank payment model
     *
     * @return EMST_Kbank_Model_Payment
     */
    protected function _getPaymentModel()
    {
        if (!$this->_paymentModel) {
            $this->_paymentModel = Mage::getModel('kbank/payment');
        }
        return $this->_paymentModel;
    }

    /**
     * set date format
     *
     * @param Zend_Date $date date
     *
     * @return string
     */
    protected function customFormatDate($date)
    {
        if (is_empty_date($date)) {
            return 'none';
        }
        $date = new Zend_Date($date);
        return $date->toString('dmY', 'php');
    }

    /**
     * check kbank authorized
     *
     * @param Mage_Sales_Order $order order
     *
     * @return boolean
     */
    protected function _checkAuthorized($order)
    {
        $payment = $this->_getPaymentModel();
        $userName = $payment->getUserName($order->getStoreId());
        $url = $payment->getTransactionUrl($order->getStoreId());
        $merchantId = $payment->getMerchantId($order->getStoreId());
        $agent = "Mozilla/5.0 (Windows; U; Windows NT 5.0; en-US; rv:1.4) Gecko/20030624 Netscape/7.1 (ax)";

        if ($url == "" || $url === false) {
            return false;
        }

        $orderId = sprintf("%012s", $order->getIncrementId());
        $status = "A";
        $date = $this->customFormatDate($order->getCreatedAtStoreDate());
        $amount = number_format($order->getBaseGrandTotal(), 2, '', '');
        $amount = sprintf("%012s", $amount);

        $dataString = "USERNAME=" . $userName . "&TMERCHANTID=" . $merchantId
                . "&TDATE=" . $date . "&TINVOICE=" . $orderId . "&TAMOUNT=" . $amount
                . "&TSTATUS=" . $status . "&cmdPost=POST";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERAGENT, $agent);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 15);
        $result = curl_exec($ch);

        //echo $result."\n";
        //get response server domain
        $realUrl = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
        $host = parse_url($realUrl, PHP_URL_HOST);
        $ip = gethostbyname($host);
        curl_close($ch);

        // check kbank ip
        $isip = ($ip == "203.146.18.94");
        $digit98 = substr($result, 98, 2); // if digit 98 99 is 00
        $isA = (substr($result, 0, 2) == "01");

        $noApprove = array("0X", "1X", "2X", "3X", "4X", "5X", "6X", "7X", "8X", "9X");
        if (in_array($digit98, $noApprove)) {
            $isA = false;
        }

        $approve = array("00", "02", "03", "04", "05", "06", "07", "09", "0T", "01", "08");
        if ($isip && $isA && in_array($digit98, $approve)) {
            $authorized = true;
        } else {
            $authorized = false;
        }
        return $authorized;
    }

    /**
     * auto approve order
     *
     * @return void
     */
    public function autoApprove()
    {
        try {
            $stores = Mage::getModel('core/store')->getCollection();
            foreach ($stores as $store) {
                $orders = $this->getOrders($store->getId());
                if (count($orders) > 0 && !is_null($orders)) {
                    foreach ($orders as $order) {
                        if ($order->hasInvoices()) {
                            continue;
                        }
                        if ($this->_checkAuthorized($order) === true) {
                            $this->_createInvoice($order);
                            Mage::log($order->getIncrementId(), null,
                                      $order->getPaymentMethod() . '_approve_' . date('dmY') . '.log');
                        } elseif ($order->canCancel()) {
                            $order->cancel()->save();
                            $order->addStatusHistoryComment('Canceled order from Kbank cronjob.')->save();
                            Mage::log($order->getIncrementId(), null,
                                      $order->getPaymentMethod() . '_reject_' . date('dmY') . '.log');
                        }
                    }
                }
            }
        } catch (Exception $e) {
            Mage::log($e->getMessage(), null, 'Kbank.log');
        }
    }

    /**
     * Create order Invoice
     *
     * @param Mage_Sales_Model_Order $order order
     *
     * @return void
     */
    protected function _createInvoice($order)
    {
        $payment = $order->getPayment();
        $payment->setCcTransId($order->getIncrementId())
                ->setCcApproval(true)
                ->setTransactionId($order->getIncrementId())
                ->registerCaptureNotification($order->getBaseGrandTotal());
        $order->save();
        $invoice = $payment->getCreatedInvoice();
        if ($invoice && !$order->getEmailSent()) {

            $order->sendNewOrderEmail()->addStatusHistoryComment(
                            Mage::helper('cc2p')->__('Notified customer about invoice #%s.', $invoice->getIncrementId())
                    )
                    ->setIsCustomerNotified(true)
                    ->save();
        }
    }
}