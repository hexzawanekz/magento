<?php

class EMST_Kbank_Block_Form extends Mage_Payment_Block_Form
{
    protected function _construct()
    {
        $this->setTemplate('kbank/form.phtml');
        parent::_construct();
    }

}