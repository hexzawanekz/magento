<?php

class EMST_Kbank_Block_Redirect extends Mage_Core_Block_Abstract
{

	protected function _toHtml()
	{
		$standard = Mage::getModel('kbank/payment');
        $form = new Varien_Data_Form();
        $form->setAction($standard->getKbankUrl())
            ->setId('kbank_payment_checkout')
            ->setName('kbank_payment_checkout')
            ->setMethod('POST')
            ->setUseContainer(true);
        foreach ($standard->setOrder($this->getOrder())->getStandardCheckoutFormFields() as $field => $value) {
            $form->addField($field, 'hidden', array('name' => $field, 'value' => $value));
        }

        $formHTML = $form->toHtml();

        $html = '<html>';
		$html .= '<head><meta content="text/html; charset=utf-8" http-equiv="Content-Type"></head>';
		$html .= '<body>';
        $html.= $this->__('You will be redirected to Kbank in a few seconds.');
        $html.= $formHTML;
        $html.= '<script type="text/javascript">document.getElementById("kbank_payment_checkout").submit();</script>';
        $html.= '</body></html>';


        return $html;
    }
}