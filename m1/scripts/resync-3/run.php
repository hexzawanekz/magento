<?php

ini_set('max_execution_time', 0);
ini_set('display_error', 1);
ini_set('memory_limit', -1);

include_once(__DIR__ . '/../../app/Mage.php');
Mage::getIsDeveloperMode(true);
Mage::app();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

$logFileName = 'resync_editing_log.log';


if (!file_exists('resync_order.csv')) {
    echo "Item file was not found. You should go to last sub-folder \"script/resync/\" from Magento's root. \nThen run php file in there.\n";
    exit();
}


// Get data from csv file
function strGetCsv($string)
{
    $data = array();
    $rows = str_getcsv($string, "\n");
    foreach ($rows as $row) {
        $data[] = str_getcsv($row);
    }
    unset($data[0]);// split header in CSV file
    return $data;
}

// Add product to category

$sqlFileBeauty = 'resync_order.csv';
$sqlString = file_get_contents($sqlFileBeauty);
$mainData = strGetCsv($sqlString);
$data = array();
foreach ($mainData as $item) {
    if(isset($item[0])) $data[] = $item[0];
}
$totalOrder = count($data);
echo "\n Total order from " . ' resync_order.csv '. ': ' . $totalOrder . "\n";
Mage::log("Total products from " . ' resync_order.csv ' . ': ' . $totalOrder, null, $logFileName, true);
try {
    $result = Mage::getModel('netsuiteexport/export')->ManuallyExport($data);
    if(is_array($result) && isset($result['status']) && isset($result['message'])) {
        if ($result['status'] == false) {
            echo $result['message'] .'\n';
            Mage::log($result['message'],null,$logFileName,true);
        } else {
            echo $result['message'] .'\n';
            Mage::log($result['message'],null,$logFileName,true);
        }
    } else {
        echo 'Some order fail to export. Check netsuite message.'. '\n';
        Mage::log('Some order fail to export. Check netsuite message.',null,$logFileName,true);
    }
}
catch(Exception $e) {
    echo $e->getMessage() .'\n';
    Mage::log($e->getMessage(),null,$logFileName,true);
}

echo "\n All done!";
Mage::log("All done!");
exit();