<?php

$logFileName = 'wt595_editing_logs.log';
$material = 'buy1000get1000_list.csv';
$attributeCode = 'promotion_flag';

if (!file_exists($material)) {
    echo "Item file was not found. You should go to last sub-folder \"script/wt595/\" from Magento's root. \nThen run php file in there.\n";
    exit();
}

ini_set('max_execution_time', 0);
ini_set('display_error', 1);
ini_set('memory_limit', -1);

include_once(__DIR__ . '/../../app/Mage.php');
Mage::getIsDeveloperMode(true);
Mage::app();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

$attributeId = Mage::getResourceModel('eav/entity_attribute')->getIdByCode('catalog_product', $attributeCode);
if (empty($attributeId)) {
    echo "Attribute \"{$attributeCode}\" was not found in database.\n";
    exit();
}

function strGetCsv($string)
{
    $data = array();
    $rows = str_getcsv($string, "\n");
    foreach ($rows as $row) {
        $data[] = str_getcsv($row);
    }
    unset($data[0]);// split header in CSV file
    return $data;
}

echo "Starting...\n\n";

$content = file_get_contents($material);
$tempData = strGetCsv($content);
$data = array();
foreach ($tempData as $item) {
    $data[] = $item[0];
}

if (count($data) > 0) {
    /** @var Mage_Core_Model_Resource $resource */
    /** @var Mage_Catalog_Model_Resource_Product $productResource */

    // init resource
    $resource = Mage::getSingleton('core/resource');
    $connection = $resource->getConnection('core_write');
    $productResource = Mage::getResourceModel('catalog/product');

    echo "Total Products: " . count($data) . PHP_EOL;

    $count = 1;
    foreach ($data as $sku) {

        // block this line on real time
        //if ($count == 2) break;

        Mage::log("--- Processing product {$sku}", null, $logFileName, true);

        if ($id = $productResource->getIdBySku($sku)) {

            try {

                $queryStr = "UPDATE `catalog_product_entity_int` SET `value` = 0 WHERE `attribute_id` = {$attributeId} and `entity_id` = {$id};";
                $connection->query($queryStr);
                echo $count . ". {$sku} - Switch off promotion flag successfully.\n";
                Mage::log("Switch off promotion flag successfully.", null, $logFileName, true);

            } catch (Exception $e) {
                echo $count . ". {$e->getMessage()}\n";
                Mage::log($e->getMessage(), null, $logFileName, true);
                continue;
            }

        } else {
            echo $count . ". Product {$sku} is not found..." . PHP_EOL;
            Mage::log("Product is not found...", null, $logFileName, true);
        }

        Mage::log("-----------------------------------------------------------", null, $logFileName, true);
        $count += 1;
    }
} else {
    echo "There is nothing to be run.\n";
}

echo "\nFinishing updates\n";

if (count($data) > 0) {
    echo "\nRun index \"Product Attributes\", it may take few minutes.\n\n";

    $indexCode = 'catalog_product_attribute';
    /** @var Mage_Index_Model_Indexer $process */
    $process = Mage::getModel('index/indexer');
    $process->getProcessByCode($indexCode)->reindexAll();
}

echo "Done\n";
exit();