<?php

ini_set('max_execution_time', 0);
ini_set('display_error', 1);
ini_set('memory_limit', -1);

include_once(__DIR__ . '/../../app/Mage.php');
Mage::getIsDeveloperMode(true);
Mage::app();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

$logFileName = 'wt663_editing_log.log';

if (!file_exists("orders.csv")) {
    echo "Item file was not found. You should go to last sub-folder \"script/wt663/\" from Magento's root. \nThen run php file in there.\n";
    exit();
}


// Get data from csv file
function strGetCsv($string)
{
    $data = array();
    $rows = str_getcsv($string, "\n");
    foreach ($rows as $row) {
        $data[] = str_getcsv($row);
    }
    unset($data[0]);// split header in CSV file
    return $data;
}


$sqlFileBeauty = 'orders.csv';
$sqlString = file_get_contents($sqlFileBeauty);
$mainData = strGetCsv($sqlString);
$data = array();
foreach ($mainData as $item) {
    if(isset($item[0])) $data[] = $item[0];
}
$totalOrder = count($data);
echo "\n Total orders from " . 'orders.csv' . ': ' . $totalOrder;
Mage::log("Total orders from " . 'orders.csv' . ': ' . $totalOrder, null, $logFileName, true);
echo "\n";
echo "Start cancel orders and make orders invalid";
Mage::log("Start cancel orders and make orders invalid", null, $logFileName, true);
echo "\n";
echo "\n ----------------------------------------------";
Mage::log("----------------------------------------------", null, $logFileName, true);
echo "\n";

$startTimeAdding = microtime(true);
$count = 0;
$countSuccess = 0;
$countFail = 0;
foreach ($data as $order) {
    try {
        $count++;
        echo $order . "; ";
        $orderReal = Mage::getModel('sales/order')->loadByIncrementId($order);
        if($orderReal->getId()){
            if($orderReal->canCancel()){
                $orderReal->cancel();
                $orderReal->setState(Mage_Sales_Model_Order::STATE_CANCELED, true, 'Cancel Transaction.');
                $orderReal->setStatus("canceled");
                $orderReal->save();
            }

            $orderReal->setState("invalid", true, 'Order Invalid.');
            $orderReal->setStatus("invalid");
            $orderReal->save();
            $countSuccess++;
        }else{
            $countFail++;
        }
    } catch (Exception $e) {
        echo "\n Problem with order #{$order}";
        Mage::log("\n Problem with order #{$order} ", null, $logFileName, true);
        echo "\n --- Error: " . $e->getMessage() . PHP_EOL;
        Mage::log("\n --- Error: " . $e->getMessage() . PHP_EOL, null, $logFileName, true);
        $countFail++;
    }

    if ($count % 200 == 0 || $count == $totalOrder) {
        echo "\n\n Total orders cancel and make invalid: " . $count . '/' . $totalOrder;
        echo "\n";
    }
}
echo "\n Success: " . $countSuccess . '/' . $totalOrder;
echo "\n Fail: " . $countFail . '/' . $totalOrder;
echo "\n Time for cancel order and make invalid: " . number_format(microtime(true) - $startTimeAdding, 2) . " s";
Mage::log("Time for cancel order and make invalid: " . number_format(microtime(true) - $startTimeAdding, 2) . " s", null, $logFileName, true);
echo "\n ----------------------------------------------";
Mage::log("----------------------------------------------", null, $logFileName, true);

echo "\n All done!";
Mage::log("All done!");
exit();