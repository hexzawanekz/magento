<?php

ini_set('max_execution_time', 0);
ini_set('display_error', 1);
ini_set('memory_limit', -1);

include_once(__DIR__ . '/../../app/Mage.php');
Mage::getIsDeveloperMode(true);
Mage::app();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
echo "\n Start Update Product Attributes";
$categoriesList = array(2185,2789,2792,2794);
foreach($categoriesList as $value){
    $category = Mage::getModel('catalog/category')->load($value);
    if($category->getId()){
        echo "\n--------------***----------------";
        echo "\n ********** Update all product in category: ". $category->getName() ." ***********";
        $products = Mage::getModel('catalog/product')
            ->getCollection()
            ->addAttributeToSelect('*')
            ->addCategoryFilter($category);
        foreach($products as $key => $product){
            try {
                echo "\n Update attributes: " . $product->getName();
                $product->setData('product_payment_methods', 'cashondelivery')
                    ->getResource()
                    ->saveAttribute($product, 'product_payment_methods');
                echo "\n---------------------";
            }catch(Exception $e){
                echo "\n $e->getMessage()";
                echo "\n---------------------";
            }
        }
    }else{
        echo "\n--------------***----------------";
        echo "\n Not Found Category";
    }
}
$process = Mage::getModel('index/indexer')->getProcessByCode('catalog_product_attribute');
$process->reindexAll();
echo "\n All done!";
Mage::log("All done!");
exit();