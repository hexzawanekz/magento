<?php

/**
 * This script was used to move current category "Home Appliances" under "Moxy".
 *
 * Related issue: WT-270
 * https://whatsnew.atlassian.net/browse/WT-270
 */

// init Magento
require_once '../app/Mage.php';
ini_set('max_execution_time', 0);
ini_set('display_error', 1);
ini_set('memory_limit', -1);

Mage::app();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

// get store ids
$moxyen = Mage::getModel('core/store')->load('moxyen', 'code')->getId();// Moxy
$moxyth = Mage::getModel('core/store')->load('moxyth', 'code')->getId();

// Move categories
$newRoot = Mage::app()->getStore($moxyen)->getRootCategoryId();

if($id = $argv[1]){
    /** @var Mage_Catalog_Model_Category $homeAppCate */
    $homeAppCate = Mage::getModel('catalog/category')->load($id);

    echo "\nThe category object is loaded by ID #".$homeAppCate->getId().PHP_EOL;
    echo "Processing...\n";
    echo "It may take several minutes because of index processes.\n";

    $homeAppCate->move($newRoot, null);
    $homeAppCate->setData('include_in_menu',false)
        ->setData('is_anchor',true)
        ->save();

    echo "Done !!! (re-run index processes if necessary)\n";
}else{
    echo "\nPlease put 'Home Appliances' category ID at the end of command.\n\n";
    echo "Eg: php -f categoryHomeAppliances.php <category_id>\n\n";
    echo "Then try again.\n";
}

exit();