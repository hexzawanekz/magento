<?php
/** Migrate All Newsletter_Subscriber to Moxy store */
require_once '../app/Mage.php';
ini_set('max_execution_time', 0);
ini_set('display_error', 1);
ini_set('memory_limit', -1);
// init Magento
Mage::app();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

// get store ids
$en     = Mage::getModel('core/store')->load('en', 'code')->getId();// Petloft
$th     = Mage::getModel('core/store')->load('th', 'code')->getId();
$len    = Mage::getModel('core/store')->load('len', 'code')->getId();// Lafema
$lth    = Mage::getModel('core/store')->load('lth', 'code')->getId();
$ven    = Mage::getModel('core/store')->load('ven', 'code')->getId();// Venbi
$vth    = Mage::getModel('core/store')->load('vth', 'code')->getId();
$sen    = Mage::getModel('core/store')->load('sen', 'code')->getId();// Sanoga
$sth    = Mage::getModel('core/store')->load('sth', 'code')->getId();
$moxyen = Mage::getModel('core/store')->load('moxyen', 'code')->getId();// Moxy
$moxyth = Mage::getModel('core/store')->load('moxyth', 'code')->getId();


$enStoresIds = array($en, $len, $ven, $sen);

$defaultEnStore = $moxyen;
$defaultThStore = $moxyth;

/** @var Mage_Newsletter_Model_Resource_Subscriber_Collection $newsletterSubcriber */
$newsletterSubscriber = Mage::getModel('newsletter/subscriber')->getCollection();

/** @var Mage_Newsletter_Model_Subscriber $_newsletter */
foreach($newsletterSubscriber as $_newsletter){
    if(in_array($_newsletter->getStoreId(), $enStoresIds)){
        $_newsletter->setStoreId($moxyen);
    } else {
        $_newsletter->setStoreId($moxyth);
    }

    $_newsletter->save();
}
