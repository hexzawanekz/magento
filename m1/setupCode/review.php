<?php
/** Migarate All Review To Moxy */

// init Magento
require_once '../app/Mage.php';
ini_set('max_execution_time', 0);
ini_set('display_error', 1);
ini_set('memory_limit', -1);

//Mage::getIsDeveloperMode(true);
Mage::app();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

// get store ids
$en     = Mage::getModel('core/store')->load('en', 'code')->getId();// Petloft
$th     = Mage::getModel('core/store')->load('th', 'code')->getId();
$len    = Mage::getModel('core/store')->load('len', 'code')->getId();// Lafema
$lth    = Mage::getModel('core/store')->load('lth', 'code')->getId();
$ven    = Mage::getModel('core/store')->load('ven', 'code')->getId();// Venbi
$vth    = Mage::getModel('core/store')->load('vth', 'code')->getId();
$sen    = Mage::getModel('core/store')->load('sen', 'code')->getId();// Sanoga
$sth    = Mage::getModel('core/store')->load('sth', 'code')->getId();
$moxyen = Mage::getModel('core/store')->load('moxyen', 'code')->getId();// Moxy
$moxyth = Mage::getModel('core/store')->load('moxyth', 'code')->getId();


$enStoresIds = array($en, $len, $ven, $sen);
$thStoresIds = array(0, $th, $lth, $vth, $sth);

$defaultEnStore = $moxyen;
$defaultThStore = $moxyth;

/** @var Mage_Review_Model_Resource_Review_Collection $reviewCollection */
$reviewCollection = Mage::getModel('review/review')->getCollection();

/** @var Mage_Review_Model_Review $_review */
foreach($reviewCollection as $_review) {
    /** @var Mage_Review_Model_Review $review */
    $review = (Mage::getModel('review/review')->load($_review->getId()));

    if (count(array_intersect($review->getStores(), $enStoresIds)) < count(array_intersect($review->getStores(), $thStoresIds))) {
        /** Review of THAI Stores*/
        $review->setData('stores', $thStoresIds);
    } else {
        /** Review of ENG Stores */
        $review->setData('stores', $enStoresIds);
    }

    $review->save();
}
