<?php
/** Migrate All Customer to Moxy store */
$start = microtime(true);
// init Magento
require_once '../app/Mage.php';
ini_set('max_execution_time', 0);
ini_set('display_error', 1);
ini_set('memory_limit', -1);

//Mage::getIsDeveloperMode(true);
Mage::app();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

$moxyTh = Mage::getModel('core/store')->load('moxyth', 'code');

/** @var Mage_Customer_Model_Customer $customerModel */
$customerModel = Mage::getModel('customer/customer');

/** @var Mage_Customer_Model_Resource_Customer_Collection $customerList */
$customerList = $customerModel->getCollection();

/** @var Mage_Customer_Model_Customer $_customer */
foreach($customerList as $_customer) {
    $_customer->setStore($moxyTh);
    $_customer->save();
}