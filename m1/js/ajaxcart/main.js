/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 /***************************************
 *         MAGENTO EDITION USAGE NOTICE *
 *****************************************/
 /* This package designed for Magento COMMUNITY edition
 * BelVG does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * BelVG does not provide extension support in case of
 * incorrect edition usage.
 /***************************************
 *         DISCLAIMER   *
 *****************************************/
 /* Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future.
 *****************************************************
 * @category   Belvg
 * @package    Belvg_Ajaxcart
 * @copyright  Copyright (c) 2010 - 2011 BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */
    

     function closeOptions(el){
        $(el).up().hide();
        $(el).up().update('');
        //$(el).up().innerHTML;

        $('fade').hide();

     }


     function showActionbox(id,error){
       $('actionbox').show();
         if (error == ''){
            $('actiontitle').removeClassName('error');
            $('actiontitle').update('Product '+$('proname_'+id).value+' was added to cart');
         }else{
            $('actiontitle').addClassName('error');
            $('actiontitle').update(''+error+'');
         }
         $('actiontitle').innerHTML;

     }

     function find_window(){
        var ScrollTop = document.body.scrollTop;
        if (ScrollTop == 0)
        {
            if (window.pageYOffset)
                ScrollTop = window.pageYOffset;
        }
        var offset = window.outerHeight/2+ScrollTop;
        $('ajax_cart_loading').setStyle({top: offset+'px'});
        $('actionbox').setStyle({top: (offset-200)+'px'});

     }

    

    function showPopup(id){
        $('popup'+id).show();
    }

    function hidePopup(id){
        $('popup'+id).hide();
    }


