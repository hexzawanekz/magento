var AW_AjaxCartProObserverObject = new AW_AjaxCartProObserver('clickOnAddToCartInCategoryList');
Object.extend(AW_AjaxCartProObserverObject, {

    uiBlocks: ['progress', 'options', 'add_confirmation'],

    _oldSetLocation: null,

    run: function() {
        this._oldSetLocation = setLocation;
        setLocation = this._observeFn.bind(this);
    },

    stop: function() {
        setLocation = this._oldSetLocation;
    },

    /* fireOriginal: function(url, parameters) {
        this._oldSetLocation(url);
    },
 */
    _observeFn: function(url) {		
		var selected_option = $$("form[action = '"+url+"'] input[type = radio][name ^= 'super_attribute']:checked").first();
		var parameters = {};
		if(typeof(selected_option) !== 'undefined') {
			parameters = {selected_opt : selected_option.value};
		}
		
        var mageVersion = AW_AjaxCartProConfig.data.mageVersion.split('.');
        var is14XAndLess = (mageVersion[0] < 2 && mageVersion[1] < 5);
        if (
            (url.indexOf('options=cart') !== -1) ||
            (url.indexOf('checkout/cart/add') !== -1) ||
            ((url.indexOf('wishlist/index/cart') !== -1) && !is14XAndLess)
        ) {
            this.fireCustom(url,parameters);
        } else {
            this.fireOriginal(url,parameters);
        }
    }
});
AW_AjaxCartPro.registerObserver(AW_AjaxCartProObserverObject);
delete AW_AjaxCartProObserverObject;