<?php

// * 2,6,9,11,15,19,21,23 * * *
// Possible times for cron

/**
 * Error reporting
 */
error_reporting(-1);

/**
 * Compilation includes configuration file
 */
define('MAGENTO_ROOT', getcwd());
$mageFilename = MAGENTO_ROOT . '/app/Mage.php';
require_once $mageFilename;
ini_set('display_errors', 1);
umask(0);
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

if (Mage::helper('core')->isModuleEnabled('SM_Emarsys')) {

    /** @var SM_Emarsys_Model_Observer $observer */
    $observer = Mage::getModel('sm_emarsys/observer');

    try {
        $observer->emarsysExportCsv();
    } catch (Exception $e) {
        Mage::log($e, null, 'emarsys_hourly_cron.log', true);
    }
}
exit(0);
