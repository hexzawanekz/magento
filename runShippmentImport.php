<?php

// * 2,6,9,11,15,19,21,23 * * *
// Possible times for cron

/**
 * Error reporting
 */
error_reporting(-1);

/**
 * Compilation includes configuration file
 */
define('MAGENTO_ROOT', getcwd());
$mageFilename = MAGENTO_ROOT . '/app/Mage.php';
require_once $mageFilename;
ini_set('display_errors', 1);
umask(0);
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

$helper = Mage::helper('shipmentimport');

if(Mage::getStoreConfig($helper::XML_PATH_SHIPMENT_IMPORT_ACTIVE)){

    $observer = Mage::getModel('shipmentimport/cronjobs');

    try {
        $observer->schedule();
    } catch (Exception $e) {
        Mage::log($e, null, 'shipment_import_manual.log', true);
    }
}

exit(0);
