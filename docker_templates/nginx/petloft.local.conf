server {
    listen 80;
    server_name petloft.local;
    return 301 https://petloft.local$request_uri;
}

server {
    listen 443 ssl;
    server_name petloft.local;
    ssl_certificate      /etc/ssl/server.crt;
    ssl_certificate_key  /etc/ssl/server.key;
    root /var/www/whatsnew-thailand;
    port_in_redirect off;
    merge_slashes on;
    real_ip_recursive on;

    open_file_cache          max=2000 inactive=20s;
    open_file_cache_valid    60s;
    open_file_cache_min_uses 5;
    open_file_cache_errors   off;

    location ~* \.(?:woff|woff2)$ {
        expires max;
        add_header Cache-Control public;
    }

    location ~* \.(?:ico|css|js|gif|jpe?g|png)$ {
	access_log off;
        expires 30d;
        add_header Pragma public;
        add_header Cache-Control public;
    }

    location ~ ^/media/sitemap/ {
	access_log off;
    }

    location ~ ^/media/xml-record/ {
        access_log off;
    }

    location ~ ^/media/ {
        access_log off;
        return 403;
    }

    location / {
        set $store_code petloftth;
        index index.html index.php evolved.php;
        try_files $uri $uri/ @handler;
        expires epoch;
    }

    location /th/ {
        set $store_code petloftth;
        try_files $uri $uri/ @handler_th;
        index index.html index.php evolved.php;
        # expires epoch;
    }

    location /en/ {
        set $store_code petloften;
        try_files $uri $uri/ @handler_en;
        index index.html index.php evolved.php;
        # expires epoch;
    }

    location /app/                { deny all; }
    location /includes/           { deny all; }
    location /lib/                { deny all; }
    location /media/downloadable/ { deny all; }
    location /pkginfo/            { deny all; }
    location /report/config.xml   { deny all; }
    location /var/                { deny all; }
    location /downloader/         { deny all; }
    location /rss/         	  { deny all; }
    location /index.phprss/         	  { deny all; }

    location /var/export/ {
        auth_basic           "Restricted";
        auth_basic_user_file htpasswd;
        autoindex            on;
    }
   location  /. {
        return 404;
    }

    location @handler { ## Magento uses a common front handler
        rewrite / /index.php;
    }

    location @handler_en {
        rewrite /en/ /en/index.php;
    }

    location @handler_th { ## Magento uses a common front handler
        rewrite /th/ /th/index.php;
    }

    location ~ .php/ {
        rewrite ^(.*.php)/ $1 last;
    }

    location ~ .php$ {
        if (!-e $request_filename) { rewrite / /index.php last; }
        expires        epoch;
        # fastcgi_pass   unix:/run/php/php5.5-fpm.sock;
        fastcgi_pass   hhvm:9000;
        fastcgi_param  SCRIPT_FILENAME  $document_root$fastcgi_script_name;
        fastcgi_param  MAGE_RUN_CODE $store_code;
        fastcgi_param  HTTPS on;
        fastcgi_read_timeout 300;
        client_max_body_size 32m;
        fastcgi_buffers 8 128k;
        fastcgi_buffer_size 128k;
        include        fastcgi_params;
    }

}
