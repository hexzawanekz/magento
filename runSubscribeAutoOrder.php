<?php
/**
 * Error reporting
 */
error_reporting(-1);

/**
 * Compilation includes configuration file
 */
define('MAGENTO_ROOT', getcwd());
$mageFilename = MAGENTO_ROOT . '/app/Mage.php';
require_once $mageFilename;
ini_set('display_errors', 1);
umask(0);
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

/** @var SM_Emarsys_Model_Observer $observer */
$observer = Mage::getModel('recurringandrentalpayments/recurringandrentalpaymentscron');

try {
    $observer->autoorder();
} catch (Exception $e) {
    Mage::log($e, null, 'subscription_error.log', true);
}
exit(0);
