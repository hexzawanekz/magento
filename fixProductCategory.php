<?php

// init Magento
ini_set('max_execution_time', 0);
ini_set('display_error', 1);
require_once 'app/Mage.php';
Mage::getIsDeveloperMode(true);
Mage::app();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

$storeId = isset($_REQUEST['store_id']) ? $_REQUEST['store_id'] : "1";
$categoryId = Mage::app()->getStore($storeId)->getRootCategoryId();

echo "StoreId: " . $storeId . " - CategoryId: " . $categoryId . "\n";

/** @var Mage_Catalog_Model_Resource_Product_Collection $collection */
$collection = Mage::getModel('catalog/product')->getCollection();
$collection->addAttributeToSelect('id')
    ->addAttributeToSelect('name')
    ->addAttributeToFilter('status', 1)
    ->addAttributeToFilter('visibility', 4)
    ->addCategoryIds()
    ->addStoreFilter($storeId)
    ->distinct(true);

/** @var Mage_Catalog_Model_Product $p */
foreach($collection as $p) {
    $categoryIds = $p->getCategoryIds();
    if(!empty($categoryIds) && !in_array($categoryId, $categoryIds)) {
        // Check if there exists child category of this root category
        $childCatId = $categoryIds[0];
        /** @var Mage_Catalog_Model_Category $childCat */
        $childCat = Mage::getModel('catalog/category')->load($childCatId);
        if(in_array($categoryId, $childCat->getParentIds())) {
            echo "Process: " . $p->getId() . " " . $p->getName() . "\n";
            Mage::log($p->getId() . " " . $p->getName(), null, 'fix_product_category.log');
            $catIds = $p->getCategoryIds();
            $catIds[] = $categoryId;
            $p->setCategoryIds($catIds);
            try {
                $p->save();
            } catch (Exception $e) {
                echo $e->getMessage() . "\n";
                Mage::log($e->getMessage(), null, 'fix_product_category.log');
            }
        }
    }
}

echo "Sucess";