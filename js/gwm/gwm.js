function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function getUTMTrackingId(){
    var affiliate = getParameterByName('utm_subid');
    var requid = getParameterByName('utm_click');
    if(affiliate != null && affiliate != "" && requid != null && requid != "")
        document.cookie = "__utmz2" + "=" + affiliate + "&" + requid + "; ";
}

jQuery(document).ready(function () {
    getUTMTrackingId();
});
