function registerFormObserver(objForm) {  			
	var targetObj = new VarienForm(objForm);
	if(!targetObj || !targetObj.form || ! targetObj.form.id) {
		return;
	}
	var AW_AjaxCartProObserverObject = new AW_AjaxCartProObserver('clickOnAddToCartInProductListPage'+targetObj.form.id);
	Object.extend(AW_AjaxCartProObserverObject, {

		uiBlocks: ['progress', 'options', 'add_confirmation'],

		_oldSubmitFn: null,

		run: function() {
			if (!targetObj) {
				return;
			}                   
			this._oldSubmitFn = targetObj.form.submit;
			targetObj.form.submit = this._observeFn.bind(this);
			//HACK for EE
			targetObj.form.select('button').each(function(btn){
				btn.removeAttribute('disabled')
			});
			return;
		},

		stop: function() {
			if (!targetObj) {
				return;
			}
			targetObj.form.submit = this._oldSubmitFn;
		},

		fireOriginal: function(url, parameters) {
			if (!targetObj) {
				return;
			}
			this.stop();
			targetObj.submit();
		},

		_observeFn: function() {					
			if (!targetObj) {
				return;
			}
			if (
				targetObj.form.action.indexOf('wishlist/index/add') !== -1 ||
				targetObj.form.action.indexOf('checkout/cart/updateItemOptions') !== -1
				) {
				this.stop()
				targetObj.form.submit();
				this.run();
				return;
			}
			var action = targetObj.form.readAttribute('action') || '';
			var params = targetObj.form.serialize(true);					
			this.fireCustom(action, params);
		},
	});
	AW_AjaxCartPro.registerObserver(AW_AjaxCartProObserverObject);
}