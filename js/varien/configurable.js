/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Varien
 * @package     js
 * @copyright   Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */
if (typeof Product == 'undefined') {
    var Product = {};
}

/**************************** CONFIGURABLE PRODUCT **************************/
Product.Config = Class.create();
Product.Config.prototype = {
    initialize: function(config){
        this.config     = config;
        this.taxConfig  = this.config.taxConfig;
        this.priceFormat = this.config.priceFormat;
        if (config.containerId) {
            this.settings   = $$('#' + config.containerId + ' ' + '.super-attribute-select_'+this.config.productId);
        } else {
            this.settings   = $$('.super-attribute-select_'+this.config.productId);
        }
        this.state      = new Hash();
        this.priceTemplate = new Template(this.config.template);
        this.prices     = config.prices;      
        
        // Set default values from config
        if (config.defaultValues) {
            this.values = config.defaultValues;
        }
        
        // Overwrite defaults by url
        var separatorIndex = window.location.href.indexOf('#');
        if (separatorIndex != -1) {
            var paramsStr = window.location.href.substr(separatorIndex+1);
            var urlValues = paramsStr.toQueryParams();
            if (!this.values) {
                this.values = {};
            }
            for (var i in urlValues) {
                this.values[i] = urlValues[i];
            }
        }
        
        // Overwrite defaults by inputs values if needed
        if (config.inputsInitialized) {
            this.values = {};
            this.settings.each(function(element) {
                if (element.value) {
                    // var attributeId = element.id.replace(/[a-z]*/, '');
                    var id = element.id.split("_");
                    var productId = id[0].replace(/[a-z]*/, '');                    
                    var attributeId = id[1].replace(/[a-z]*/, '');
                    this.values[attributeId] = element.value;
                }
            }.bind(this));
        }
            
        // Put events to check select reloads 
        this.settings.each(function(element){
            Event.observe(element, 'change', this.configure.bind(this))
        }.bind(this));

        // fill state
        this.settings.each(function(element){
            // var attributeId = element.id.replace(/[a-z]*/, '');
            var id = element.id.split("_");
            var productId = id[0].replace(/[a-z]*/, '');                    
            var attributeId = id[1].replace(/[a-z]*/, '');
            if(attributeId && this.config.attributes[attributeId]) {
                element.config = this.config.attributes[attributeId];
                element.attributeId = attributeId;
                this.state[attributeId] = false;
            }
        }.bind(this))

        // Init settings dropdown
        var childSettings = [];
        for(var i=this.settings.length-1;i>=0;i--){
            var prevSetting = this.settings[i-1] ? this.settings[i-1] : false;
            var nextSetting = this.settings[i+1] ? this.settings[i+1] : false;
            if (i == 0){
                this.fillSelect(this.settings[i])
            } else {
                this.settings[i].disabled = true;
            }
            $(this.settings[i]).childSettings = childSettings.clone();
            $(this.settings[i]).prevSetting   = prevSetting;
            $(this.settings[i]).nextSetting   = nextSetting;
            childSettings.push(this.settings[i]);
        }

        // Set values to inputs
        this.configureForValues();
        document.observe("dom:loaded", this.configureForValues.bind(this));
    },
    
    configureForValues: function () {
        if (this.values) {
            this.settings.each(function(element){
                var attributeId = element.attributeId;
                element.value = (typeof(this.values[attributeId]) == 'undefined')? '' : this.values[attributeId];
                this.configureElement(element);
            }.bind(this));
        }
    },

    configure: function(event){ 
        var element = Event.element(event);
        this.configureElement(element);
    },

    configureElement : function(element) {
        // this.reloadOptionLabels(element);        
        /*if(element.value){
            this.state[element.config.id] = element.value;
            if(element.nextSetting){
                element.nextSetting.disabled = false;
                this.fillSelect(element.nextSetting);
                // this.resetChildren(element.nextSetting);
            }
        }
        else {
            // this.resetChildren(element);
        }*/
        this.reloadPrice();
    },

    reloadOptionLabels: function(element){
        var selectedPrice;
        if(element.options[element.selectedIndex].config && !this.config.stablePrices){
            selectedPrice = parseFloat(element.options[element.selectedIndex].config.price)
        }
        else{
            selectedPrice = 0;
        }
        for(var i=0;i<element.options.length;i++){
            if(element.options[i].config){
                element.options[i].text = this.getOptionLabel(element.options[i].config, element.options[i].config.price-selectedPrice);
            }
        }
    },

    resetChildren : function(element){
        if(element.childSettings) {
            for(var i=0;i<element.childSettings.length;i++){
                element.childSettings[i].selectedIndex = 0;
                element.childSettings[i].disabled = true;
                if(element.config){
                    this.state[element.config.id] = false;
                }
            }
        }
    },

    fillSelect: function(element){
        // var attributeId = element.id.replace(/[a-z]*/, '');
        var id = element.id.split("_");
        var productId = id[0].replace(/[a-z]*/, '');                    
        var attributeId = id[1].replace(/[a-z]*/, '');
        var options = this.getAttributeOptions(attributeId);        
        /*this.clearSelect(element);
        element.options[0] = new Option(this.config.chooseText, '');

        var prevConfig = false;
        if(element.prevSetting){
            prevConfig = element.prevSetting.options[element.prevSetting.selectedIndex];
        }

        if(options) {
            var index = 1;
            for(var i=0;i<options.length;i++){
                var allowedProducts = [];
                if(prevConfig) {
                    for(var j=0;j<options[i].products.length;j++){
                        if(prevConfig.config.allowedProducts
                            && prevConfig.config.allowedProducts.indexOf(options[i].products[j])>-1){
                            allowedProducts.push(options[i].products[j]);
                        }
                    }
                } else {
                    allowedProducts = options[i].products.clone();
                }

                if(allowedProducts.size()>0){
                    options[i].allowedProducts = allowedProducts;
                    element.options[index] = new Option(this.getOptionLabel(options[i], options[i].price), options[i].id);
                    if (typeof options[i].price != 'undefined') {
                        element.options[index].setAttribute('price', options[i].price);
                    }
                    element.options[index].config = options[i];
                    index++;
                }
            }
        }*/

        
        var table = $$("[id ^= 'product"+productId+"']");
		var length = table.length;
		var tblLength = 0;
		if(length>1) {table = table.last();}else {table = table.first();tblLength = $$('#'+table.id+" tbody tr").length;}
		
        if(options && tblLength<options.length) {
            var valArr = [];
            for(var i=0;i<options.length;i++){
                var row = new Element("tr");

                var col1 = new Element("td");
                var col2 = new Element("td");
                var col3 = new Element("td");
                var col4 = new Element("td");

                var price = parseFloat(options[i].ourPrice);
                if (this.taxConfig.includeTax) {
                    var tax = price / (100 + this.taxConfig.defaultTax) * this.taxConfig.defaultTax;
                    var excl = price - tax;
                    var incl = excl*(1+(this.taxConfig.currentTax/100));                    
                } else {
                    var tax = price * (this.taxConfig.currentTax / 100);
                    var excl = price;
                    var incl = excl + tax;
                }

                if (this.taxConfig.showIncludeTax || this.taxConfig.showBothPrices) {
                    price = incl;
                } else {
                    price = excl;
                }

                // Qty
                var el1 = '<input type="radio" id="option_product'+productId+'_attribute'+attributeId+'_'+i+'" name="super_attribute['+attributeId+']" value="'+options[i].id+'" price="'+options[i].price+'" oldprice="'+options[i].oldPrice+'" productid="'+options[i].products[0]+'" /> '+this.getOptionLabel(options[i], options[i].price);
                // Size
                if (options[i].packagingMeasurement=='litre') {
                    var el2 = options[i].weight + ' ' + Translator.translate('L.');
                } else {
                    if (options[i].weight>=1000) {
                        var el2 = parseFloat(options[i].weight/1000) + ' ' + Translator.translate('kg.');
                    } else {
                        var el2 = parseFloat(options[i].weight) + ' ' + Translator.translate('g.');
                    }
                }
                                    
                // Price/Unit
                if (options[i].packagingMeasurement=='kilo') {
                    var unit = (options[i].weight/1000);
                } else if (options[i].packagingMeasurement=='litre') {
                    var unit = options[i].weight;
                } else {
                    var unit = options[i].packagingCount;
                }
                var perUnit = (price)/unit;
                // var el3 = this.priceTemplate.evaluate({price:perUnit.toFixed(2)});
                var el3 = this.formatPriceCurrency(perUnit);
                // Our Price
                var ourPrice = price;
                // var el4 = this.priceTemplate.evaluate({price:ourPrice});
                var el4 = this.formatPriceCurrency(ourPrice);

                col1.addClassName("qty");
                row.appendChild(col1.insert(el1));                
                row.appendChild(col2.insert(el2));
                row.appendChild(col3.insert(el3));
                row.appendChild(col4.insert(el4));

                table.down('tbody').insert(row);

                valArr[i] = perUnit;
            }            
            var bestValIdx = valArr.indexOf(Math.min.apply(null, valArr));
            $$("[id ^='option_product"+productId+"_attribute"+attributeId+"_"+bestValIdx+"']").each(function(el){el.checked = true;});
            // $('option_attribute'+attributeId+'_'+bestValIdx).down('qty').insert('<span class="best-value"></span>');			
            table.down('.qty', bestValIdx).insert(' <span class="best-value"></span>');			
            if(length==1) {
				this.reloadPrice();
			}
        }

    },

    getOptionLabel: function(option, price){
        var price = parseFloat(price);
        if (this.taxConfig.includeTax) {
            var tax = price / (100 + this.taxConfig.defaultTax) * this.taxConfig.defaultTax;
            var excl = price - tax;
            var incl = excl*(1+(this.taxConfig.currentTax/100));
        } else {
            var tax = price * (this.taxConfig.currentTax / 100);
            var excl = price;
            var incl = excl + tax;
        }

        if (this.taxConfig.showIncludeTax || this.taxConfig.showBothPrices) {
            price = incl;
        } else {
            price = excl;
        }

        var str = option.label;
        /*if(price){
            if (this.taxConfig.showBothPrices) {
                str+= ' ' + this.formatPrice(excl, true) + ' (' + this.formatPrice(price, true) + ' ' + this.taxConfig.inclTaxTitle + ')';
            } else {
                str+= ' ' + this.formatPrice(price, true);
            }
        }*/
        return str;
    },

    formatPrice: function(price, showSign){
        var str = '';
        price = parseFloat(price);
        if(showSign){
            if(price<0){
                str+= '-';
                price = -price;
            }
            else{
                str+= '+';
            }
        }

        var roundedPrice = (Math.round(price*100)/100).toString();

        if (this.prices && this.prices[roundedPrice]) {
            str+= this.prices[roundedPrice];
        }
        else {
            str+= this.priceTemplate.evaluate({price:price.toFixed(2)});
        }
        return str;
    },

    formatPriceCurrency: function(price){
        return formatCurrency(price, this.priceFormat);
    },

    clearSelect: function(element){
        for(var i=element.options.length-1;i>=0;i--){
            element.remove(i);
        }
    },
    
    getAttributeOptions: function(attributeId){
        if(this.config.attributes[attributeId]){
            return this.config.attributes[attributeId].options;
        }
    },

    reloadPrice: function(){
        if (this.config.disablePriceReload) {
            return;
        }
        var price    = 0;
        var oldPrice = 0;

        for(var i=this.settings.length-1;i>=0;i--){
            /*var selected = this.settings[i].options[this.settings[i].selectedIndex];
            if(selected.config){
                price    += parseFloat(selected.config.price);
                oldPrice += parseFloat(selected.config.oldPrice);
            }*/
            
            var id = this.settings[i].id.split("_");
            var productId = id[0].replace(/[a-z]*/, '');                    
            var attributeId = id[1].replace(/[a-z]*/, '');
            var elRef = 'list-'+productId;

            // var attributeId = this.settings[i].id.replace(/[a-z]*/, '');
            var radioGrpName = 'super_attribute['+attributeId+']';
            var frmName = 'product_addtocart_form_'+productId;
                        
            var radioGrp = document['forms'][''+frmName+''][''+radioGrpName+''];

            if(radioGrp.length){
                for(var j=0; j < radioGrp.length; j++){
                    if (radioGrp[j].checked == true) {                    
                        price = parseFloat(radioGrp[j].readAttribute('price'));
                        oldPrice = parseFloat(radioGrp[j].readAttribute('oldprice'));
                        var newRef = radioGrp[j].readAttribute('productid');
                        if($(elRef)) {
                            $(elRef).setAttribute('ref', newRef);
                        } else if($('ref')) {
                            $('ref').value = newRef;
                        }
                    }
                }
            } else {
                price = parseFloat(radioGrp.readAttribute('price'));
                oldPrice = parseFloat(radioGrp.readAttribute('oldprice'));
            }
                
        }

        optionsPrice[''+productId+''].changePrice('config', {'price': price, 'oldPrice': oldPrice});
        optionsPrice[''+productId+''].reload();

        return price;

        if($('product-price-'+this.config.productId)){
            $('product-price-'+this.config.productId).innerHTML = price;
        }
        this.reloadOldPrice();
    },

    reloadOldPrice: function(){
        if (this.config.disablePriceReload) {
            return;
        }
        if ($('old-price-'+this.config.productId)) {

            var price = parseFloat(this.config.oldPrice);
            for(var i=this.settings.length-1;i>=0;i--){
                var selected = this.settings[i].options[this.settings[i].selectedIndex];
                if(selected.config){
                    price+= parseFloat(selected.config.price);
                }
            }
            if (price < 0)
                price = 0;
            price = this.formatPrice(price);

            if($('old-price-'+this.config.productId)){
                $('old-price-'+this.config.productId).innerHTML = price;
            }

        }
    }
}
