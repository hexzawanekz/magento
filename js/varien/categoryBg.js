jQuery(function() {
	var theWindow = jQuery(window);
	var bg = jQuery("#bg");
	var aspectRatio = bg.width() / bg.height();
	    			    		
	function resizeBg() {
		
		if ((theWindow.width() / theWindow.height()) < aspectRatio ) {
		    bg
		    	.removeClass()
		    	.addClass('bgheight');
		} else {
		    bg
		    	.removeClass()
		    	.addClass('bgwidth');
		}
					
	}
	                   			
	theWindow.resize(function() {
		resizeBg();
	}).trigger("resize");
});