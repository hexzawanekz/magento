document.observe('dom:loaded', function() {
	var prForm = $$("form[id ^= 'product_addtocart_form']").first();
	var action = null;
	if(typeof(prForm) !== 'undefined') {
		var action = prForm.action;
	}
	if(action) {
		var pr = action.slice(action.indexOf('product/'));
		var baseUrl = action.slice(0,action.indexOf('checkout/'));	
		baseUrl = baseUrl.replace(/^http:|https:/, window.location.protocol);
		var prId = pr.split('/');
		if(prId[1]){
			new Ajax.Request(
					baseUrl + 'cdlogin/index/productView/id/' + prId[1],
					{
						method: 'get',
						parameters: ''
					}
				);
		}
	}
});
