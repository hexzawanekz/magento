/* 
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/LICENSE-M1.txt
 *
 * @category   AW
 * @package    AW_Mobile
 * @copyright  Copyright (c) 2010-2011 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/LICENSE-M1.txt
 */

/* [@pack@] */

$j(function(){

    /* Android orientation support */
    var supportsOrientationChange = "onorientationchange" in window,
    orientationEvent = supportsOrientationChange ? "orientationchange" : "resize";

    window.addEventListener(orientationEvent, function(){
        checkLand(window.orientation);
        $j(document).trigger('checkorientationchange');
    }, true);        
});

$j(document).ready(function(){
    checkLand(window.orientation);    
    $j(document).trigger('checkorientationchange');
    //****** Nav under page **********//
    //jQuery(document).ready(function(){

    var viewport;
    var page;
    var pageContent;
    var slidingMenu;
    var slidingMenuContent;
    var isMenuOpen = false;
    var visiblePageMargin = 55;
    var maximumMenuWith = 280;
    var preloader   = '<!-- center style="margin-top: 30px;"><img src="img/preloader.gif" alt="loading..." /></center -->';

    initMetrics();

    function initMetrics() {

        viewport = {
            width  : jQuery(window).width(),
            height : jQuery(window).height()
        };
        page = jQuery("#mobilePage");
        pageContent  = jQuery("#pageContent");
        menuStage  = jQuery("#underNav");
        slidingMenuContent = jQuery("#slidingMenuContent");
        itemContent = "";
        page.css("height",menuStage.height()+"px");
    }

    function openMenu() { 

        isMenuOpen = true;
        //Rem : Had to do this here because viewport.width value could have been updated since next open/close. If we rotate the device for example
        var menuWidth = viewport.width - visiblePageMargin;

        if(viewport.width > (maximumMenuWith+visiblePageMargin) ){
            menuWidth = maximumMenuWith;    
        } 

        //Rem : Unecessary except for windows phone7.5 where div with lower z-index are clickable....
        //menuStage.css("visibility","visible");
        //adjustHeight();        
        page.animate({
           left: menuWidth+"px"
        }, { duration: 300 });
    }
    

    function closeMenu() {

        isMenuOpen = false;

        page.animate(
            {   left: "0px" }, 
            {   duration: 180
                //For wp7 where div with lower z-index are clickable....
                //SetTimeout to hide the menu only after closing
                //complete: function() { menuStage.css("visibility","hidden");}
            }
        );
        /*.animate({
            height : "100%"
        }, { duration: 0 });*/
    }

    function adjustHeight() {

        var menuHeight = menuStage.height();
        var pageHeight = page.height();
        var MenuContentHeight = slidingMenuContent.height();
        //to avoid overflow block on Android < 2.3

        if(viewport.height > page.height){
            alert("menuHeight: "+menuHeight+"  viewport.height: "+viewport.height+"  page.height: "+page.height());
            //menuStage.css("height",100+"%");   
            //pageContent.css("height",menuHeight+"px");
            //page.css("height",menuHeight+"px");
            // page.css("height",MenuContentHeight+20+"px");
            //menuStage.css("height",pageHeight+"px");
            page.css("height",viewport.height+"px")
        }/*else{
            page.css("height",menuHeight+"px")
            //menuStage.css("height",pageHeight+"px");
        } */
    } 

    function loadPage(url) {

        closeMenu();
        pageContent.html(preloader);
        //Rem : Timeout is only necessary for demo purpose, to display the loader. Remove it for production.
        setTimeout( pageContent.load(url, function() {/* no callbacks */}), 1200);
    }

    function orientationChange() {

        //We must wait at least 500ms before recalculate metrics, 
        //If we don't, some old phones send the old metrics value instead of new orientation values
        window.setTimeout(function() {
            
            initMetrics();
            if(isMenuOpen) openMenu(); 
            else closeMenu();

        }, 400);
    }

    jQuery("#slidingMenuContent li a").click(function () {
        closeMenu();
    });
    //trigger the opening or closing action
    jQuery("span.show-menu-button").click(function () {
        
        var pagePosition = page.css('left');
        if(pagePosition == "0px") {
            openMenu();
            //alert(jQuery(document).height());
            page.css("height",jQuery(document).height()+"px");
        }
        else { 
            closeMenu(); 
        }
    });

    //Some windows phones (7.5) does'nt fired the "orientationchange" event, that's why we must use "resize" event
    window.addEventListener("resize", orientationChange, false);
    window.addEventListener("orientationchange", orientationChange, false);


    //detect hash change
    jQuery(window).bind('hashchange', function (e) { 
    
        var hash = location.hash;
        var url = "";

        //For windows phone 7.5, the view doesn't automatically scroll to top when the pageContent is load
        window.scrollTo(0, 1);
        
        if(hash == ''){
            url = "";
        } else{
            url = hash.split("#")[1];
        } 

        loadPage(url); 
    });
});

//});


var checkLand = function(orientation){
    switch (orientation){
        case -90:
        case 90:
            $j('body').addClass('wide'); //Landscape
            break;
        case 0:
            $j('body').removeClass('wide'); //Profile
            break;
    }
}

/**
 * Set left to given element
 * @return null
 */
var setLeft = function(element, left){
    if (element && element.attr('id')){
        $(element.attr('id')).style.left = left + 'px';        
    }
}

/**
 * Retrives with of screen
 * @return int
 */
var getScreenWidth = function(){
    var width = 320;
    if ($j(window).width()){
        return $j(window).width();
    }
    return width;
}

var isTouchDevice = function() {
    try {
        document.createEvent("TouchEvent");
        return true;
    } catch (e) {
        return false;
    }
}


var uagent = navigator.userAgent.toLowerCase();
$j.support.WebKitCSSMatrix = (typeof WebKitCSSMatrix == "object");
$j.support.touch = (typeof Touch == "object");
$j.support.WebKitAnimationEvent = (typeof WebKitTransitionEvent == "object");
$j.support.isAndroid = (uagent.search('android') > -1);
$j.support.isBlackBerry = (uagent.search('blackberry') > -1);

$j.support.isMobile = ($j.support.touch || $j.support.isAndroid || $j.support.isBlackBerry); 
 
/**
 * Mobile Safari missing labels fix
 */
var registerLabels = function(){
		$$('label').each(
            function(el){                
                if(el.readAttribute('for')){
                    var _for = $(el.readAttribute('for'));
                    if(_for instanceof Object){                        
                        el.onclick = function(el){return function(e){
                            if (['radio', 'checkbox'].indexOf(el.getAttribute('type')) != -1) {
                                el.setAttribute('selected', !el.getAttribute('selected'));
                            } else {
                                el.focus();
                            }
                        }}(_for)
                    }
                }
            }
        );
	}

document.observe('dom:loaded',	function () {
    registerLabels();
});

