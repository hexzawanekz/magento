<?php

/**
 * This script was used to change url-key of categories "Petloft", "Lafema", "Venbi", "Sanoga" and "Gadgets-Electronics" under "Moxy".
 *
 * Related issue: WT-277
 * https://whatsnew.atlassian.net/browse/WT-277
 */

// init Magento
require_once '../app/Mage.php';
ini_set('max_execution_time', 0);
ini_set('display_error', 1);
ini_set('memory_limit', -1);

echo "\nStarting...\n";
echo "It may take several minutes because of index processes.\n";

Mage::app();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

// get store ids
$moxyen = Mage::getModel('core/store')->load('moxyen', 'code')->getId();// Moxy
$moxyth = Mage::getModel('core/store')->load('moxyth', 'code')->getId();

// get root category id
$rootId = Mage::app()->getStore($moxyen)->getRootCategoryId();

// get category collection
$collection = Mage::getModel('catalog/category')->getCollection()
    ->addAttributeToSelect('*')
    ->addAttributeToFilter('parent_id', array('eq' => $rootId))
    ->addAttributeToFilter('url_key', array('in' => array('petloft','venbi','lafema','sanoga','gadgets-electronics')))
    ->load();

$list = array(
    'petloft'               => 'pets',
    'venbi'                 => 'baby',
    'lafema'                => 'beauty',
    'sanoga'                => 'health',
    'gadgets-electronics'   => 'electronics'
);

/** @var Mage_Catalog_Model_Resource_Category_Collection $collection */
foreach($collection as $category){
    /** @var Mage_Catalog_Model_Category $category */
    if(array_key_exists($category->getData('url_key'), $list)){
        try{
            echo "\nThe category object is loaded by ID #".$category->getId().PHP_EOL;
            echo "Processing...\n";

            /** @var Mage_Catalog_Model_Category $obj */
            $obj = Mage::getModel('catalog/category')->load($category->getId());
            $obj->setData('url_key', $list[$category->getData('url_key')]);
            $obj->setData('url_key_create_redirect', true);
            $obj->save();

            echo "Finish...#".$obj->getId().PHP_EOL;
        }catch (Exception $e){
            Mage::log($e->getMessage().' - on line '.$e->getLine(),null,'changeCategoryUrlKey.log',true);
        }
    }
}

echo "\nDone !!! (re-run index processes if necessary)\n";
exit();