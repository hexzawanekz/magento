<?php

// init Magento
require_once '../app/Mage.php';
ini_set('max_execution_time', 0);
ini_set('display_error', 1);
ini_set('memory_limit', -1);

Mage::app();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

// get store ids
$en     = Mage::getModel('core/store')->load('en', 'code')->getId();// Petloft
$th     = Mage::getModel('core/store')->load('th', 'code')->getId();
$len    = Mage::getModel('core/store')->load('len', 'code')->getId();// Lafema
$lth    = Mage::getModel('core/store')->load('lth', 'code')->getId();
$ven    = Mage::getModel('core/store')->load('ven', 'code')->getId();// Venbi
$vth    = Mage::getModel('core/store')->load('vth', 'code')->getId();
$sen    = Mage::getModel('core/store')->load('sen', 'code')->getId();// Sanoga
$sth    = Mage::getModel('core/store')->load('sth', 'code')->getId();
$moxyen = Mage::getModel('core/store')->load('moxyen', 'code')->getId();// Moxy
$moxyth = Mage::getModel('core/store')->load('moxyth', 'code')->getId();

// Disable all store views except moxy

Mage::getModel('core/store')->load('en', 'code')->setData('is_active',0)->save();
Mage::getModel('core/store')->load('th', 'code')->setData('is_active',0)->save();
Mage::getModel('core/store')->load('len', 'code')->setData('is_active',0)->save();
Mage::getModel('core/store')->load('lth', 'code')->setData('is_active',0)->save();
Mage::getModel('core/store')->load('ven', 'code')->setData('is_active',0)->save();
Mage::getModel('core/store')->load('vth', 'code')->setData('is_active',0)->save();
Mage::getModel('core/store')->load('sen', 'code')->setData('is_active',0)->save();
Mage::getModel('core/store')->load('sth', 'code')->setData('is_active',0)->save();

// Move categories
$newRoot = Mage::app()->getStore($moxyen)->getRootCategoryId();

$petRootId  = Mage::app()->getStore($en)->getRootCategoryId();
$petRoot = Mage::getModel('catalog/category')->load($petRootId);
$petRoot->move($newRoot, null);
$petRoot->setUrlKey('petloft')
    ->setData('include_in_menu',false)
    ->setData('is_anchor',true)
    ->save();

$lafemaRootId  = Mage::app()->getStore($len)->getRootCategoryId();
$lafemaRoot = Mage::getModel('catalog/category')->load($lafemaRootId);
$lafemaRoot->move($newRoot, null);
$lafemaRoot->setUrlKey('lafema')
    ->setData('include_in_menu',false)
    ->setData('is_anchor',true)
    ->save();

$sanogaRootId  = Mage::app()->getStore($sen)->getRootCategoryId();
$sanogaRoot = Mage::getModel('catalog/category')->load($sanogaRootId);
$sanogaRoot->move($newRoot, null);
$sanogaRoot->setUrlKey('sanoga')
    ->setData('include_in_menu',false)
    ->setData('is_anchor',true)
    ->save();

$venbiRootId  = Mage::app()->getStore($ven)->getRootCategoryId();
$venbiRoot = Mage::getModel('catalog/category')->load($venbiRootId);
$venbiRoot->move($newRoot, null);
$venbiRoot->setUrlKey('venbi')
    ->setData('include_in_menu',false)
    ->setData('is_anchor',true)
    ->save();

