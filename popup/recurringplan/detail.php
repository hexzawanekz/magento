<style>
    body{
        background: url(images/popup-autoship.png) no-repeat;
    }
    #sub-close-button{
        cursor: hand;
        border: 0;
        padding: 0;
        overflow: visible;
        width: 18px;
        height: 18px;
        position: absolute;
        top: 0;
        right: 0;
        margin-top: 6px;
        margin-right: -2px;
        background-color: transparent;
    }
</style>
<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
<body>
    <div id="sub-close-button"></div>
</body>
<script>
    jQuery('#sub-close-button').click(function(){
        parent.jQuery.colorbox.close();
        return false;
    });
</script>