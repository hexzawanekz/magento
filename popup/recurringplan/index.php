<?php
define('MAGENTO_ROOT', getcwd());
$mageFilename = MAGENTO_ROOT . '/../../app/Mage.php';
require_once $mageFilename;

$productId = Mage::app()->getRequest()->getParam('product_id');
$termNum = Mage::app()->getRequest()->getParam('term_num');
$planId = Mage::app()->getRequest()->getParam('plan_id');
$termFrequency = Mage::app()->getRequest()->getParam('term_frequency');
$dataFrequency = explode('-',$termFrequency);
$term = Mage::getModel('recurringandrentalpayments/terms')->getCollection()
    ->addFieldToSelect('*')
    ->addFieldToFilter('noofterms', array('eq' => $termNum))
    ->addFieldToFilter('plan_id', array('eq' => $planId))
    ->addFieldToFilter('repeateach', array('eq' => $dataFrequency[0]))
    ->addFieldToFilter('termsper', array('eq' => $dataFrequency[1]))
    ->getFirstItem();
if(!empty($term->getData())){
    $planPrice = Mage::getModel('recurringandrentalpayments/termsprice')->getCollection()
        ->addFieldToSelect('*')
        ->addFieldToFilter('product_id', array('eq' => $productId))
        ->addFieldToFilter('term_id', array('eq' => $term->getId()))
        ->addFieldToFilter('plan_id', array('eq' => $planId));
}else{
    $term = Mage::getModel('recurringandrentalpayments/terms')->getCollection()
        ->addFieldToSelect('*')
        ->addFieldToFilter('noofterms', array('eq' => $termNum))
        ->addFieldToFilter('plan_id', array('eq' => $planId))
        ->getFirstItem();

    $planPrice = Mage::getModel('recurringandrentalpayments/termsprice')->getCollection()
        ->addFieldToSelect('*')
        ->addFieldToFilter('product_id', array('eq' => $productId))
        ->addFieldToFilter('term_id', array('eq' => $term->getId()))
        ->addFieldToFilter('plan_id', array('eq' => $planId));
}



?>
<style>
    #cboxLoadedContent {
        background-color: white
    }
    #recurring_plan{
        color:#73767a;
        font-family: 'Open Sans', Arial;
    }
    #recurring_popup_header{
        margin-top: 10px;
        text-align: center;
        font-weight: bold;
    }
    #recurring_extra_header{
        text-align: center;
        font-weight: bold;
        font-size: 10px;
    }
    .table_center {
        margin-top: 10px;
        margin-left: auto;
        margin-right: auto;
    }
    .table_center tr th{
        text-align: center;
        width: 35%;
        border-bottom: 1px solid;
        padding: 5px;
        font-family: 'Open Sans', Arial;
        font-weight: bold;
    }
    .table_center tr td{
        text-align: center;
        padding: 5px;
        font-family: 'Open Sans', Arial;
    }
</style>
<body>
<?php
if ($termId == -1) {
    echo "Please select a plan";
} else {
    ?>
    <div id="recurring_plan">
        <p id="recurring_popup_header">SAVE MORE ON EVERY ORDER</p>
        <p id="recurring_extra_header">Below is the price of "<?php echo $term->getLabel() ?>" plan for this product</p>
        <table class="table_center">
            <tr>
                <th>Term</th>
                <th>Price</th>
            </tr>
            <?php foreach($planPrice as $price): ?>
                <tr>
                    <td><?php echo $price->getData('term_number')?></td>
                    <td>฿<?php echo FLOOR($price->getData('term_price')) ?></td>
                </tr>
            <?php endforeach; ?>
        </table>
    </div>
    <?php
}
?>
</body>
