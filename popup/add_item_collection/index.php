<?php
define('MAGENTO_ROOT', getcwd());
$mageFilename = MAGENTO_ROOT . '/../../app/Mage.php';
require_once $mageFilename;

$storeId      = Mage::app()->getRequest()->getParam('store_id');
Mage::app()->setCurrentStore($storeId);
$customerId   = Mage::app()->getRequest()->getParam('log_customer');
$productId    = Mage::app()->getRequest()->getParam('product_id');
$productName  = Mage::app()->getRequest()->getParam('product_name');
$productPrice = Mage::app()->getRequest()->getParam('product_price');
$productUrl   = Mage::app()->getRequest()->getParam('product_url');
$productImage = Mage::app()->getRequest()->getParam('product_image');
?>
<html>
<head>
    <style>
        .form-add-collection .wrap-button-choice .button-choice {
            background: #c9c9c9;
            border-radius: 5px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            display: inline-block;
            width: 100%;
            overflow: hidden;
            border: 1px solid #c9c9c9;
        }

        .form-add-collection .wrap-button-choice .button-choice label {
            padding: 10px 5px;
            float: left;
            display: block;
            cursor: pointer;
            width: 50%;
            text-align: center;
            min-width: 0;
            font-size: 10px;
        }

        .form-add-collection .wrap-button-choice .button-choice label .fa {
            color: #ff6c6c;
            display: block;
            font-size: 18px;
            clear: both;
        }

        .form-add-collection .wrap-button-choice .button-choice label.active-choice {
            background: #fff;
        }

        .form-add-collection .wrap-button-choice {
            border: 1px solid #e4e4e4;
            border-radius: 5px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            padding: 1%;
            display: inline-block;
            width: 100%;
        }

        .form-add-collection .wrap-button-choice .button-choice {
            background: #c9c9c9;
            border-radius: 5px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            display: inline-block;
            width: 100%;
            overflow: hidden;
            border: 1px solid #c9c9c9;
        }

        .form-add-collection .wrap-button-choice .button-choice label {
            padding: 10px 5px;
            float: left;
            display: block;
            cursor: pointer;
            width: 50%;
            text-align: center;
            min-width: 0;
            font-size: 10px;
        }

        .form-add-collection .wrap-button-choice .button-choice label .fa {
            color: #ff6c6c;
            display: block;
            font-size: 18px;
            clear: both;
        }

        .form-add-collection .wrap-button-choice .button-choice label.active-choice {
            background: #fff;
        }

        .product-wrap {
            width: 215px;
            float: left;
            padding-right: 10px;
            border-right: 2px dashed gray;
        }

        .add-to-collections .box-popup {
            max-width: 600px !important;
        }

        .add-to-collections .form-area {
            padding: 0 10px;
            border-left: 1px dashed #999;
            width: 300px;
            min-height: 200px;
            float: right;
        }

        .add-to-collections .form-area .choice-collection {
            border-bottom: 1px solid #f2f2f2;
            padding-bottom: 10px;
            margin-bottom: 10px;
            display: block;
        }

        .add-to-collections .form-area .choice-collection select {
            height: 30px;
            width: 100%;
            display: block;
        }

        .add-to-collections .login-collections {
            text-align: center;
        }

        .add-to-collections .login-collections a {
            margin: 0 auto;
        }

        .add-to-collections .login-collections p {
            display: block;
        }

        .add-to-collections .widget-product-area {
            width: 200px;
            float: left;
        }

        .add-to-collections .widget-product-area .prod-image a {
            margin: 0 auto;
            display: table;
            position: relative;
        }

        .add-to-collections .widget-product-area .prod-name a {
            display: block;
            text-align: center;
        }

        .add-to-collections .widget-product-area .widget-price {
            margin: 0 auto;
        }

        .add-to-collections .widget-product-area .widget-price .wrap-badges-widget {
            float: right;
            font-size: 12px;
            display: table;
        }

        .add-to-collections .widget-product-area .widget-price .wrap-badges-widget .discount {
            padding: 3px;
        }

        .add-to-collections .widget-product-area .widget-price .normal-price {
            text-align: center;
        }

        #cboxLoadedContent {
            background-color: white;
            padding: 15px;
        }

        .wrap-popup .header-popup {
            width: 110%;
            font-size: 20px;
            height: 32px;
            border-bottom: 2px solid #ff6c6c;
            margin-bottom: 10px;
            position: relative;
            line-height: 30px;
            font-weight: bold;
            padding-right: 30px;
        }

        .wrap-popup .box-popup.newstyle .header-popup {
            padding: 0;
            border-bottom: none;
        }

        .form-add-collection #title {
            width: 100%;
            border: 1px solid #ccc;
            padding: 5px;
            margin-top: 5px;
            margin-bottom: 5px;
        }

        .form-add-collection #desc {
            width: 100%;
            resize: none;
            margin-top: 5px;
            margin-bottom: 5px;
        }

        .form-add-collection #image-url {
            width: 100%;
            border: 1px solid #ccc;
            padding: 5px;
            margin-top: 5px;
            margin-bottom: 5px;
        }

        .row-popup .wrap-field-choice .field-choice.show-choice {
            display: block;
        }

        .row-popup .wrap-field-choice .image_upload {
            width: 150px;
            margin: 0 auto;
            text-align: center;
            padding: 10px;
            border: 1px dashed #999;
            position: relative;
        }

        .row-popup .wrap-field-choice .image_upload #cover-img {
            position: absolute;
            top: 0;
            left: 0;
        }

        .row-popup .wrap-field-choice .field-choice.show-choice img {
            min-height: 70px;
            display: block;
        }

        .row-popup .wrap-field-choice .image_upload img {
            max-width: 100%;
        }

        .row-popup .wrap-field-choice .image_upload .fa {
            font-size: 40px;
            margin-bottom: 10px;
        }

        .fa {
            display: inline-block;
            font: normal normal normal 14px/1 FontAwesome;
            font-size: inherit;
            text-rendering: auto;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
        }

        .row-popup .wrap-field-choice .image_upload span {
            font-size: 10px;
        }

        .row-popup .wrap-field-choice .image_upload label {
            background: #ff6c6c;
            display: table;
            min-width: 0;
            padding: 5px 30px;
            color: #fff;
            border-radius: 5px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            margin: 10px auto 0;
        }

        .row-popup .wrap-field-choice .image_upload .input_image_upload {
            display: none;
        }

        .row-popup .wrap-field-choice {
            padding: 10px 0;
        }

        .row-popup .wrap-field-choice .field-choice .show-choice {
            display: block;
        }



        .add-collection-btn {
            border: 1px;
            background-color: #ff6c6c;
            text-align: center;
            padding: 5px;
            border-radius: 5px;
            display: block;
            margin: 0;
            color: white;
        }
        .choice-form-collection{
            -webkit-appearance:text;
        }
    </style>

    <script>
        jQuery(document).ready(function () {
            jQuery("#wishlist_id-106140").on("change", function () {
                var thisValue = parseInt(jQuery(this).val());
                if (thisValue == 0) {
                    jQuery("#new-collection-form-106140").show();
                    jQuery("#new-collection-form-106140 #title").addClass('nempty');
                } else {
                    jQuery("#new-collection-form-106140").hide();
                    jQuery("#new-collection-form-106140 #title").removeClass();
                }
            });
        });

        jQuery('#weblink').click(function () {
            jQuery('#weblink').css("background-color", "#fff");
            jQuery('#locallink').css("background-color", "#c9c9c9");
            jQuery('#image-url').show();
            jQuery('#cover_image_upload_container').hide();
        });

        jQuery('#locallink').click(function () {
            jQuery('#locallink').css("background-color", "#fff");
            jQuery('#weblink ').css("background-color", "#c9c9c9");
            jQuery('#cover_image_upload_container').show();
            jQuery('#image-url').hide();
        });

        function readURL(input) {
            var maxSize = 1024*1024*1;
            jQuery('#image_replace').fadeOut('fast');
            jQuery('#blah').fadeIn('fast');
            if(input.files[0].size < maxSize) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        jQuery('#blah')
                            .attr('src', e.target.result)
                            .width(140)
                            .height(100);
                    };

                    reader.readAsDataURL(input.files[0]);
                }
            }else{
                alert('Your image size must be smaller than 1MB');
                return false;
            }
        }
    </script>
</head>
<body>
<div style="" class="wrap-popup  add-to-collections" id="add-collection-categori-106140">
    <div id="overlay-wishlist-106140" class="box-popup">
        <div class="header-popup">
            <p style="font-weight: bold">Add Item To Collection</p>
        </div>
        <div class="content-popup">
            <div class="widget-product-area">
                <div class="prod-image">
                    <a class="block-link-product-1" itemprop="url" href="<?php echo $productUrl ?>">

                        <img itemprop="image" class="lazy" src="<?php echo $productImage ?>"
                             data-src="<?php echo $productImage ?>" style="display: inline;">
                    </a>
                </div>

                <div class="prod-name">
                    <a style="font-weight: bold" itemprop="url"
                       href="<?php echo $productUrl ?>"><?php echo $productName ?></a>
                </div>

                <div class="widget-price" itemprop="offers" itemscope="" itemtype="http://schema.org/Offer">
                    <div class="price-box">
                            <div class="special-price in-block full-width">
                                <p>
                                    <span class="price size12">Price: <?php echo $productPrice; ?></span>
                                </p>
                            </div>
                    </div>
                </div>
            </div>
            <?php if($customerId != null && $customerId != ""):?>
            <div class="form-area">
                <form method="POST" id="form-add-collection-106140"
                      action="<?php echo Mage::getUrl('social/settings/additemtocollection') ?>"
                      enctype="multipart/form-data" class="form-add-collection">
                    <div class="rowpopup choice-collection">
                        <select class="choice-form-collection" id="wishlist_id-106140" name="selected_collection" id-prod-coll="add-collection-categori-106140">
                            <?php
                            $resource = Mage::getSingleton('core/resource');
                            $readConnection = $resource->getConnection('core_read');
                            $query = "SELECT * FROM wishlist WHERE customer_id='".$customerId."'";
                            $wishlistCollection = $readConnection->fetchAll($query);
                            ?>
                            <?php foreach($wishlistCollection as $wish): ?>
                                <option value="<?php echo $wish['wishlist_id'] ?>"><?php echo $wish['name'] ?></option>
                            <?php endforeach;?>
                            <option value="0">Add New Collection</option>
                        </select>
                        <div class="list-image-category"></div>
                    </div>
                    <input type="hidden" name="product_id" value="<?php echo $productId ?>">

                    <div id="new-collection-form-106140"
                         class="form-add-collection"
                         style="display:<?php if(count($wishlistCollection) < 1){echo 'block';}else{echo 'none';}?>;">
                        <div class="row-popup">
                            <label style="font-weight: bold"><span>Title:</span></label>
                            <input type="text" id="title" name="colname" size="20"
                                   placeholder="Enter your collection title" class="nempty">
                        </div>

                        <div class="row-popup">
                            <label style="font-weight: bold"><span>Description:</span></label>
                            <textarea id="desc" name="desc" rows="4" maxlength="250"
                                      placeholder="Enter your collection description"></textarea>
                        </div>

                        <div class="row-popup">

                            <label style="font-weight: bold"><span>Collection Cover:</span></label>
                            <div class="wrap-button-choice">
                                <div class="button-choice">
                                    <label id="weblink" name="image_link"><i class="fa fa-link"></i>Use Image From
                                        Web</label>
                                    <label id="locallink" name="image_upload" class="active-choice"><i
                                            class="fa fa-cloud-upload"></i>Upload An Image</label>
                                </div>
                            </div>
                            <input style="display: none" class="image_link" type="text" id="image-url" name="image_url"
                                   placeholder="http://" size="20">
                            <div id="cover_image_upload_container" class="wrap-field-choice">
                                <div class="image_upload field-choice show-choice">
                                    <img style="display:none;" id="blah" src="#" alt="your image"/>
                                    <div id="image_replace">
                                        <i class="fa fa-file-image-o"></i>
                                        <span class="clearfix">Recommend .jpg with size 400x400</span>
                                    </div>
                                    <label for="cover_image_upload">browse</label>
                                    <input onchange="readURL(this)" class="input_image_upload" type="file"
                                           title="Browse" name="cover" id="cover_image_upload"/>
                                    <input type="hidden" id="image-browse-preset-106140" name="image_browse_preset">
                                </div>
                            </div>
                            <div style="text-align: center" class="row-popup check_public">
                                <label><input type="checkbox" checked="checked" id="is-public" name="is_public"
                                              value="1">Make Collection Public</label>
                            </div>
                        </div>
                    </div>

                    <div style="margin-top: 10px" class="row-popup">
                        <button class="add-collection-btn" id="add-collection-106140">Add Item To Collection</button>
                    </div>
                </form>
            </div>
            <?php else:?>
                <h2 style="font-weight: bold;color: #ff6c6c;">You must login to use this feature</h2>
            <?php endif;?>
        </div>
    </div>
</div>

</body>
</html>