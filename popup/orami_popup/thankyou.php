<!DOCTYPE html>
<html>
<head>
    <title>Moxy Popup</title>
    <meta charset="utf-8" />

    <script src="js/jquery.min.js"></script>
    <script src="js/jquery.email-autocomplete.js"></script>
    <script src="js/jquery.cookie.js"></script>

    <style>
        @font-face {
            font-family: 'montserratbold';
            src: url('fonts/montserrat-bold-webfont.eot');
            src: url('fonts/montserrat-bold-webfont.eot?#iefix') format('embedded-opentype'),
            url('fonts/montserrat-bold-webfont.woff2') format('woff2'),
            url('fonts/montserrat-bold-webfont.woff') format('woff'),
            url('fonts/montserrat-bold-webfont.ttf') format('truetype'),
            url('fonts/montserrat-bold-webfont.svg#montserratbold') format('svg');
            font-weight: normal;
            font-style: normal;
        }

        @font-face {
            font-family: 'montserratlight';
            src: url('fonts/montserrat-light-webfont.eot');
            src: url('fonts/montserrat-light-webfont.eot?#iefix') format('embedded-opentype'),
            url('fonts/montserrat-light-webfont.woff2') format('woff2'),
            url('fonts/montserrat-light-webfont.woff') format('woff'),
            url('fonts/montserrat-light-webfont.ttf') format('truetype'),
            url('fonts/montserrat-light-webfont.svg#montserratlight') format('svg');
            font-weight: normal;
            font-style: normal;
        }

        @font-face{
            font-family: 'Helvethaica3';
            src: url('fonts/DB_Helvethaica_X_v3.2.ttf') format('truetype');
        }

        @font-face{
            font-family: 'Helvethaica';
            src: url('fonts/DB_Helvethaica_X.ttf') format('truetype');
        }

        @font-face{
            font-family: 'Luna';
            src: url('fonts/Luna.ttf') format('truetype');
        }

        *{margin: 0px; padding: 0px;}
        body, html{ height: 550px; width: 360px;}
        .popup-container{
            color: #3c3d41;
            height: 500px;
            width: 330px;
            /*background: url('./images/popup_background.png') 0 0 no-repeat;*/
            /*background-size: 100%;*/
            background-color: white;
            padding: 17px;
            margin: 0px auto;
            position: relative;
            font-size: 14px;
            font-family: Tahoma, 'Trebuchet MS', 'Lucida Sans Unicode', 'Lucida Grande', 'Lucida Sans', Arial, sans-serif;
        }
        .images-container{width: 100%;height: 135px;float: left;margin-top: 33px}
        .images-container .image{width: 100%;height: 100%;background: url('./images/text.png') 0 0 no-repeat;background-size: 100%;margin: 40px auto 15px;}
        .images-container .butter{width: 56px;height: 30px;background: url('./images/white_butter.png') 0 0 no-repeat;background-size: 100%;margin: auto ;}
        .form-container {width: 100%;float: left;height: 272px;padding-bottom: 20px;}
        .form-container .welcome-title {width: 326px;height: 160px;float: left;border: 2px solid #fff;
            /*-webkit-box-shadow: 0 5px 11px 1px rgba(0, 0, 0, 0.13);*/
            /*-moz-box-shadow: 0 5px 11px 1px rgba(0, 0, 0, 0.13);*/
            /*box-shadow: 0 5px 11px 1px rgba(0, 0, 0, 0.13);*/
            margin-bottom: 16px;
        }
        .form-container .welcome-title h2{font-family: 'Helvethaica3', 'montserratbold', Arial;color: #3c3d41;text-align: center;width: 100%;margin: 14px 0 -1px 0;font-size: 30px;line-height: 30px;}
        .form-container .welcome-title p.top{font-family: 'Helvethaica3', 'montserratbold', Arial;color: #3c3d41;width: 100%;text-align: center;font-size: 23px;line-height: 20px;}
        .form-container .welcome-title p.mid{font-family: 'Helvethaica3', 'montserratbold', Arial;color: #3c3d41;width: 100%;text-align: center;font-size: 23px;line-height: 20px;margin: 25px 0 0 0;}
        .form-container .welcome-title p.mid span{color: #f16b6e;font-size: 28px;font-weight: bold;}
        .form-container p.discount{color: #f16b6e;font-family: 'Helvethaica3', 'montserratbold', Arial;text-align: center;font-size: 20px;font-weight: bold;}
        .form-container p.discount-last{font-family: 'Helvethaica3', 'montserratbold', Arial;font-size: 17px;line-height: 20px;margin-top: 20px;}

        .input-email{margin-top:60px; border: 0;height: 31px;line-height: 31px;width: 94%;text-align:center;padding: 0px 10px;font-size: 20px;font-family: 'Helvethaica3', Arial;border-radius: 15px}
        .dogs, .cats {margin-top:10px;height: 31px;color: #fff;background: #91a7d0;border-color:#91a7d0 ;border-radius: 15px;font-size: 27px;padding: 0px 10px;cursor: pointer;width: 90px;font-family: 'Helvethaica3', Arial;width: 100%;text-align:center;}
        .form-container .category-choice{width: 90%;margin: auto;font-family: 'Helvethaica3', 'montserratbold', Arial;padding-top: 14px;height: 103px;font-size: 20px;}
        .form-container .category-choice label{width: 50%;  float: left;  font-size: 22px;  margin-bottom: -3px;}
        .form-container .category-choice .input-wrap{float: left;width: 50%;height: 23px; padding-bottom: 5px;}
        .form-container .category-choice .input-wrap:nth-child(8n+1){width: 37%;}
        .form-container .category-choice .input-wrap input{margin-right: 7px;}

        div.thank-you{color: white;height: 100%;margin-bottom: 15px;position: relative;text-align: center;width: 100%; display: none;}
        .thank-you h1 {
            display: block;
            position: absolute;
            top: 135px;
            width: 100%;
            font-family: 'Luna', 'montserratbold', Arial;
            font-size: 23px;
            font-weight: normal;
            line-height: 40px;
            letter-spacing: 8px;
        }
        .thank-you img {left: 0;margin: 0 auto;position: absolute;right: 0;top: 71px;}
        .thank-you .images{width: 100%;margin-top: 294px;float: left;}
        .thank-you .images .image{width: 105px;height: 105px;background: url('./images/image-1.jpg') 0 0 no-repeat;background-size: 100%;margin: 0px 7px 7px 0px;float: left;}
        .thank-you .images .image:nth-child(3n+0){margin-right: 0px;}
        .thank-you .images .image.item-2{background: url('./images/image-2.jpg') 0 0 no-repeat;}
        .thank-you .images .image.item-3{background: url('./images/image-3.jpg') 0 0 no-repeat;}
        .thank-you .images .image.item-4{background: url('./images/image-4.jpg') 0 0 no-repeat;}
        .thank-you .images .image.item-5{background: url('./images/image-5.jpg') 0 0 no-repeat;}
        .thank-you .images .image.item-6{background: url('./images/image-6.jpg') 0 0 no-repeat;}

        #new-sub-icon{width: 60px;height: 60px;background: url('./images/orami-butterfly-50.png') 0 0 no-repeat;float: left}
        #new-sub-header{font-weight: bold;font-size: 48px;}
        /*#close-popup{height: 17px;width: 17px;background: url('./images/close.png') 0 0 no-repeat;background-size: 100%;position: relative;left: 309px;cursor: pointer}*/
    </style>


</head>
<body>
<div class="popup-container">
    <div class="images-container">
        <div id="new-sub-icon"></div>
        <div id="new-sub-header">Newsletter</div>
        <div style="clear: both"></div>
        <span style="font-size: 15px;margin-left: 10px">ช้อปครั้งแรกกับเรา รับเลย!<span style="font-size: 18px;font-weight: bold"> 150 บาท*</span></span>
    </div>
    <div class="form-container">
<h1>Thanks for Subscribing!</h1>
    </div>
</div>


</body>

</html> 