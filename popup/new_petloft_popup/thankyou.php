<!DOCTYPE html>
<html>
<head>
    <title>Moxy Popup</title>
    <meta charset="utf-8" />

    <script src="js/jquery.min.js"></script>
    <script src="js/jquery.email-autocomplete.js"></script>
    <script src="js/jquery.cookie.js"></script>

    <style>
        @font-face{
            font-family: 'Luna';
            src: url('fonts/Luna.ttf') format('truetype');
        }

        @font-face {
            font-family: 'montserratbold';
            src: url('fonts/montserrat-bold-webfont.eot');
            src: url('fonts/montserrat-bold-webfont.eot?#iefix') format('embedded-opentype'),
            url('fonts/montserrat-bold-webfont.woff2') format('woff2'),
            url('fonts/montserrat-bold-webfont.woff') format('woff'),
            url('fonts/montserrat-bold-webfont.ttf') format('truetype'),
            url('fonts/montserrat-bold-webfont.svg#montserratbold') format('svg');
            font-weight: normal;
            font-style: normal;
        }

        *{margin: 0px; padding: 0px;}
        body, html{ height: 550px; width: 360px;}
        .popup-container{
            height: 560px;
            width: 330px;
            background: url('./images/popup_background.png') 0 0 no-repeat;
            background-size: 100%;
            padding: 17px;
            margin: 0px auto;
            position: relative;
            font-size: 14px;
            font-family: Tahoma, Arial, sans-serif;
        }
        .images-container{width: 100%;height: 135px;float: left;margin-top: 33px}
        .images-container .image{width: 100%;height: 100%;background: url('./images/text.png') 0 0 no-repeat;background-size: 100%;margin: 40px auto 15px;}
        .images-container .butter{width: 56px;height: 30px;background: url('./images/white_butter.png') 0 0 no-repeat;background-size: 100%;margin: auto ;}

        div.thank-you-show{color: white;height: 100%;margin-bottom: 15px;position: relative;text-align: center;width: 100%; display: block;}
        .thank-you-show h1 {
            display: block;
            position: absolute;
            top: 135px;
            width: 100%;
            font-family: 'Luna', 'montserratbold', Arial;
            font-size: 23px;
            font-weight: normal;
            line-height: 40px;
            letter-spacing: 8px;
        }
        .thank-you-show img {left: 0;margin: 0 auto;position: absolute;right: 0;top: 71px;}
        .thank-you-show .images{width: 100%;margin-top: 294px;float: left;}
        .thank-you-show .images .image{width: 105px;height: 105px;background: url('./images/image-1.jpg') 0 0 no-repeat;background-size: 100%;margin: 0px 7px 7px 0px;float: left;}
        .thank-you-show .images .image:nth-child(3n+0){margin-right: 0px;}
        .thank-you-show .images .image.item-2{background: url('./images/image-2.jpg') 0 0 no-repeat;}
        .thank-you-show .images .image.item-3{background: url('./images/image-3.jpg') 0 0 no-repeat;}
        .thank-you-show .images .image.item-4{background: url('./images/image-4.jpg') 0 0 no-repeat;}
        .thank-you-show .images .image.item-5{background: url('./images/image-5.jpg') 0 0 no-repeat;}
        .thank-you-show .images .image.item-6{background: url('./images/image-6.jpg') 0 0 no-repeat;}
    </style>


</head>

<body>
    <div class="popup-container">
        <div class="thank-you-show">
            <img src="./images/white_butter.png" alt="Moxy Have It All"/>
            <h1>Thanks for Subscribing!</h1>
            <div class="images">
                <div class="image item-1"></div>
                <div class="image item-2"></div>
                <div class="image item-3"></div>
                <div class="image item-4"></div>
                <div class="image item-5"></div>
                <div class="image item-6"></div>
            </div>
        </div>
        <div class="images-container">
            <div class="butter"></div>
            <div class="image"></div>
        </div>
    </div>
</body>
</html> 