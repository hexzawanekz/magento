<!DOCTYPE html>
<html>
<head>
    <title>Moxy Popup</title>
    <meta charset="utf-8" />

    <script src="js/jquery.min.js"></script>
    <script src="js/jquery.email-autocomplete.js"></script>
    <script src="js/jquery.cookie.js"></script>

    <style>
        @font-face {
            font-family: 'montserratbold';
            src: url('fonts/montserrat-bold-webfont.eot');
            src: url('fonts/montserrat-bold-webfont.eot?#iefix') format('embedded-opentype'),
            url('fonts/montserrat-bold-webfont.woff2') format('woff2'),
            url('fonts/montserrat-bold-webfont.woff') format('woff'),
            url('fonts/montserrat-bold-webfont.ttf') format('truetype'),
            url('fonts/montserrat-bold-webfont.svg#montserratbold') format('svg');
            font-weight: normal;
            font-style: normal;
        }

        @font-face {
            font-family: 'montserratlight';
            src: url('fonts/montserrat-light-webfont.eot');
            src: url('fonts/montserrat-light-webfont.eot?#iefix') format('embedded-opentype'),
            url('fonts/montserrat-light-webfont.woff2') format('woff2'),
            url('fonts/montserrat-light-webfont.woff') format('woff'),
            url('fonts/montserrat-light-webfont.ttf') format('truetype'),
            url('fonts/montserrat-light-webfont.svg#montserratlight') format('svg');
            font-weight: normal;
            font-style: normal;
        }

        @font-face{
            font-family: 'Helvethaica3';
            src: url('fonts/DB_Helvethaica_X_v3.2.ttf') format('truetype');
        }

        @font-face{
            font-family: 'Helvethaica';
            src: url('fonts/DB_Helvethaica_X.ttf') format('truetype');
        }

        @font-face{
            font-family: 'Luna';
            src: url('fonts/Luna.ttf') format('truetype');
        }

        *{margin: 0px; padding: 0px;}
        body, html{ height: 550px; width: 360px;}
        .popup-container{
            color: #3c3d41;
            height: 400px;
            width: 330px;
            /*background: url('./images/popup_background.png') 0 0 no-repeat;*/
            /*background-size: 100%;*/
            background-color: white;
            padding: 17px;
            margin: 0px auto;
            position: relative;
            font-size: 14px;
            font-family: Tahoma, 'Trebuchet MS', 'Lucida Sans Unicode', 'Lucida Grande', 'Lucida Sans', Arial, sans-serif;
        }
        .images-container{width: 100%;height: 135px;float: left;margin-top: 33px}
        .images-container .image{width: 100%;height: 100%;background: url('./images/text.png') 0 0 no-repeat;background-size: 100%;margin: 40px auto 15px;}
        .images-container .butter{width: 56px;height: 30px;background: url('./images/white_butter.png') 0 0 no-repeat;background-size: 100%;margin: auto ;}
        .form-container {width: 100%;float: left;height: 272px;padding-bottom: 20px;}
        .form-container .welcome-title {width: 326px;height: 160px;float: left;border: 2px solid #fff;
            /*-webkit-box-shadow: 0 5px 11px 1px rgba(0, 0, 0, 0.13);*/
            /*-moz-box-shadow: 0 5px 11px 1px rgba(0, 0, 0, 0.13);*/
            /*box-shadow: 0 5px 11px 1px rgba(0, 0, 0, 0.13);*/
            margin-bottom: 16px;
        }
        .form-container .welcome-title h2{font-family: 'Helvethaica3', 'montserratbold', Arial;color: #3c3d41;text-align: center;width: 100%;margin: 14px 0 -1px 0;font-size: 30px;line-height: 30px;}
        .form-container .welcome-title p.top{font-family: 'Helvethaica3', 'montserratbold', Arial;color: #3c3d41;width: 100%;text-align: center;font-size: 23px;line-height: 20px;}
        .form-container .welcome-title p.mid{font-family: 'Helvethaica3', 'montserratbold', Arial;color: #3c3d41;width: 100%;text-align: center;font-size: 23px;line-height: 20px;margin: 25px 0 0 0;}
        .form-container .welcome-title p.mid span{color: #f16b6e;font-size: 28px;font-weight: bold;}
        .form-container p.discount{color: #f16b6e;font-family: 'Helvethaica3', 'montserratbold', Arial;text-align: center;font-size: 20px;font-weight: bold;}
        .form-container p.discount-last{font-family: 'Helvethaica3', 'montserratbold', Arial;font-size: 17px;line-height: 20px;margin-top: 20px;}

        .input-email{margin-top:60px; border: 0;height: 31px;line-height: 31px;width: 94%;text-align:center;padding: 0px 10px;font-size: 20px;font-family: 'Helvethaica3', Arial;border-radius: 15px}
        .dogs, .cats {margin-top:10px;height: 31px;color: #fff;background: #91a7d0;border-color:#91a7d0 ;border-radius: 15px;font-size: 27px;padding: 0px 10px;cursor: pointer;width: 90px;font-family: 'Helvethaica3', Arial;width: 100%;text-align:center;}
        .form-container .category-choice{width: 90%;margin: auto;font-family: 'Helvethaica3', 'montserratbold', Arial;padding-top: 14px;height: 103px;font-size: 20px;}
        .form-container .category-choice label{width: 50%;  float: left;  font-size: 22px;  margin-bottom: -3px;}
        .form-container .category-choice .input-wrap{float: left;width: 50%;height: 23px; padding-bottom: 5px;}
        .form-container .category-choice .input-wrap:nth-child(8n+1){width: 37%;}
        .form-container .category-choice .input-wrap input{margin-right: 7px;}

        div.thank-you{color: white;height: 100%;margin-bottom: 15px;position: relative;text-align: center;width: 100%; display: none;}
        .thank-you h1 {
            display: block;
            position: absolute;
            top: 135px;
            width: 100%;
            font-family: 'Luna', 'montserratbold', Arial;
            font-size: 23px;
            font-weight: normal;
            line-height: 40px;
            letter-spacing: 8px;
        }
        .thank-you img {left: 0;margin: 0 auto;position: absolute;right: 0;top: 71px;}
        .thank-you .images{width: 100%;margin-top: 294px;float: left;}
        .thank-you .images .image{width: 105px;height: 105px;background: url('./images/image-1.jpg') 0 0 no-repeat;background-size: 100%;margin: 0px 7px 7px 0px;float: left;}
        .thank-you .images .image:nth-child(3n+0){margin-right: 0px;}
        .thank-you .images .image.item-2{background: url('./images/image-2.jpg') 0 0 no-repeat;}
        .thank-you .images .image.item-3{background: url('./images/image-3.jpg') 0 0 no-repeat;}
        .thank-you .images .image.item-4{background: url('./images/image-4.jpg') 0 0 no-repeat;}
        .thank-you .images .image.item-5{background: url('./images/image-5.jpg') 0 0 no-repeat;}
        .thank-you .images .image.item-6{background: url('./images/image-6.jpg') 0 0 no-repeat;}

        #new-sub-icon{width: 60px;height: 60px;background: url('./images/petloft_paw_50.png') 0 0 no-repeat;float: left}
        #new-sub-header{font-weight: bold;font-size: 48px;}
        #close-popup{height: 17px;width: 17px;background: url('./images/close.png') 0 0 no-repeat;background-size: 100%;position: relative;left: 309px;cursor: pointer}
    </style>


</head>

<body>

<?php
function getUtm(){
    $utm = array();
    if(!empty($_COOKIE['__utmz'])){
        $pattern = "/(utmcsr=([^\|]*)[\|]?)|(utmccn=([^\|]*)[\|]?)|(utmcmd=([^\|]*)[\|]?)|(utmctr=([^\|]*)[\|]?)|(utmcct=([^\|]*)[\|]?)/i";
        preg_match_all($pattern, $_COOKIE['__utmz'], $matches);
        if(!empty($matches[0])){
            foreach($matches[0] as $match){
                $pair = null;
                $match = trim($match, "|");
                list($k, $v) = explode("=", $match);
                $utm[$k] = $v;
            }
        }
    }
    return $utm;
}
?>
<?php
$utm_array = getUtm();

$utm_source = "";
$utm_medium = "";
$utm_campaign = "";
$utm_content = "";
$utm_term = "";

if (isset($_GET["utm_source"])) {
    $utm_source = $_GET["utm_source"];
} else {
    if (isset($utm_array["utmcsr"])) {
        $utm_source = $utm_array["utmcsr"];
    } else {
        $utm_source = "";
    }
}

if (isset($_GET["utm_medium"])) {
    $utm_medium = $_GET["utm_medium"];
} else {
    if (isset($utm_array["utmcmd"])) {
        $utm_medium = $utm_array["utmcmd"];
    } else {
        $utm_medium = "";
    }
}

if (isset($_GET["utm_campaign"])) {
    $utm_campaign = $_GET["utm_campaign"];
} else {
    if (isset($utm_array["utmccn"])) {
        $utm_campaign = $utm_array["utmccn"];
    } else {
        $utm_campaign = "";
    }
}

if (isset($_GET["utm_content"])) {
    $utm_content = $_GET["utm_content"];
} else {
    if (isset($utm_array["utmcct"])) {
        $utm_content = $utm_array["utmcct"];
    } else {
        $utm_content = "";
    }
}

if (isset($_GET["utm_term"])) {
    $utm_term = $_GET["utm_term"];
} else {
    if (isset($utm_array["utmctr"])) {
        $utm_term = $utm_array["utmctr"];
    } else {
        $utm_term = "";
    }
}

$uri_parts = explode('?', $_SERVER['REQUEST_URI'], 2);

require_once __DIR__.'/../../app/Mage.php';
Mage::app();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
?>
<div class="popup-container">
    <div class="thank-you">
        <img src="./images/white_butter.png" alt="Moxy Have It All"/>
        <h1>Thanks for Subscribing!</h1>
        <div class="images">
            <div class="image item-1"></div>
            <div class="image item-2"></div>
            <div class="image item-3"></div>
            <div class="image item-4"></div>
            <div class="image item-5"></div>
            <div class="image item-6"></div>
        </div>
    </div>
    <div class="images-container">
        <div id="new-sub-icon"></div>
        <div id="new-sub-header">Newsletter</div>
        <div style="clear: both"></div>
        <span style="font-size: 15px;margin-left: 10px">ช้อปครั้งแรกกับเรา รับเลย!<span style="font-size: 18px;font-weight: bold"> 150 บาท*</span></span>
    </div>
    <div class="form-container">

        <form name="moxy-popup" method="GET" action="<?php echo Mage::helper('emarsys')->getLink(); ?>">
            <!-- <form name="moxy-popup" method="POST" action="/display/index/send"> -->
            <div class="category-choice">
                <label for="category_select_all">คุณสนใจสินค้าประเภท</label>

                <div class="">
                    <input type="checkbox" id="category_select_all" value="" name="category_ids"/>เลือกทั้งหมด<!--All of them-->
                    <script type="text/javascript">
                        jQuery(document).ready(function(){
                            jQuery('#category_select_all').change(function () {
                                if(jQuery(this).is(':checked')){
                                    jQuery('.category_ids').prop('checked', true);
                                }else{
                                    jQuery('.category_ids').prop('checked', false);
                                }
                            });
                        });
                    </script>
                </div>

                <div class="input-wrap">
                    <input type="checkbox" class="category_ids" value="1" name="inp_39794"/>แมว<!--Cat-->
                </div>
                <div class="input-wrap">
                    <input type="checkbox" class="category_ids" value="1" name="inp_39793"/>หมา<!--Dog-->
                </div>
            </div>

            <input style="border-radius: 0;width: 55%;border-bottom: 2px solid #e6e6e6;margin-top: 0" id="fieldEmail" class="input-email email-field" type="email" name="inp_3" placeholder="กรอกอีเมล์ รับส่วนลดทันที" required x-autocomplete size="100" />
            <?php
            $fields = Mage::helper('emarsys')->getField();
            foreach ($fields as $key => $value):
                ?>
                <?php if ($value['fieldname'] == "f"):?>
                    <input type="hidden" name="<?php echo $value['fieldname']; ?>" value="4498" />
                <?php else: ?>
                    <input type="hidden" name="<?php echo $value['fieldname']; ?>" value="<?php echo $value['fieldvalue']; ?>" />
                <?php endif; ?>
            <?php endforeach; ?>

            <button style="width: 30%;position: relative;left: 210px;top: -42px;border-radius: 5px;border: none;background: #6ac8df;border-color: #6ac8df;" type="submit" name="submit-form" class="dogs email-button" id="dogs" title="ยืนยัน">ยืนยัน</button>
        </form>
        <p class="discount-last">*เมื่อมีการสั่งซื้อขั้นต่ำ 750 บาท</p>
    </div>
</div>

<script>
    jQuery('#close-popup').click(function(){
        parent.jQuery.fn.colorbox.close();
    });

    function validateEmail(sEmail) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(sEmail);
    }

    function validateForm(){
        return jQuery('.category_ids:checked').length;
    }

    jQuery(".form-container form").submit(function(e){
        e.preventDefault();

        var email = jQuery('#fieldEmail').val();
        if(!validateEmail(email)){return false;}
        if(!validateForm()){
            alert('กรุณาเลือกประเภทความสนใจของคุณ อย่างน้อย 1 ช่อง');
            return false;
        }
        //Emarsys
        try {
            var mox = "";
            if($(this).serialize().includes("24286") == true
                || $(this).serialize().includes("24287") == true
                || $(this).serialize().includes("24288")== true
                || $(this).serialize().includes("24289") == true){
                mox = "&inp_10828=1";
            }else{
                mox = "";
            }
            var emarsysUrl = jQuery(this).attr('action')+ '?' + $(this).serialize() + mox;
            var baseUrl = '<?php printf("%s://%s/emarsys/", isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http', $_SERVER['SERVER_NAME']);?>';
            jQuery.ajax({
                type: 'POST',
                url: baseUrl,
                data : {'url' : emarsysUrl},
                async : false,
                success : function () {
                    alert("Thanks for Subscribing!");
//                    jQuery('.thank-you').show();return;
                }
            });
        } catch ($e) {
            console.log($e);
        }
    });

    (function($){
        $(function() {
            $("#fieldEmail").emailautocomplete({
                domains: ["moxy.co.th"] //add your own domains
            });
        });
    }(jQuery));

</script>

<!--
    http://suite9.emarsys.net/u/register_bg.php?owner_id=420582093&key_id=3&f=1662&optin=y&inp_3=[*E-Mail]
-->
</body>

</html> 