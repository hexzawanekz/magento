<?php
/**
 * FaceBook Class  Class
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Facebook
 * @author		Somjet Suanthong
 * @link		-
 */


require_once "facebook.php";

abstract class FbAbstract Extends Facebook {
	
	protected $app_id;
	protected $app_secret;
	protected $app_scope;
	protected $user_permissions;
	
	
	public function __construct($app_id, $app_secret, $app_scope){

		$this->app_id = $app_id;
		$this->app_secret = $app_secret;
		$this->app_scope = $app_scope;
		
		/**
		* This will call Facebook::___construct();
		*
		*/
		parent::__construct(array(
				'appId' => $this->app_id,
				'secret' => $this->app_secret,
				'cookie' => true
		));
		/**
		* Facebook class not save your session,
		* until call function getUser(); WTF
		*
		*/
		parent::getUser(); // Get user to save fbid to session
	}

	/********************************
	* Login
	*
	*/
	public function login($lastUrl)
	{
		 try { 

            $accessToken = $this->getAccessToken();
            if ( ! $this->getUser()) {
                $this->destroySession();
                $this->installPermissions($lastUrl);
            }
            else
            {
                if( count(explode("|",$accessToken)) > 1 )
                {           
                    // 123047464514353|61177e04b9c424cb770f17aaa44fa8fe  
                    $this->destroySession();
                    $this->installPermissions($lastUrl);
                }

                // $_SESSION['fb_'.$this->getAppId().'_state'] = $_REQUEST['state'];
                // $_SESSION['fb_'.$this->getAppId().'_user_id'] = $this->getUser();
                // $_SESSION['fb_'.$this->getAppId().'_access_token'] = $this->getAccessToken();

                // $accessToken = $this->getAccessToken();
                // // Have this FB user in DB, then use access token from DB
                // $tmp_user = $user_table->getUserByFacebookId($this->getUser());
                // $data = array();
                // $data['facebook'] = $accessToken;
                // $where = $user_table->getAdapter()->quoteInto('facebook_id = ?', $tmp_user['id']);
                // $user_table->update($data, $where);
                // debug($accessToken);exit;
              

                $user_profile = $this->api("/" . $this->getUser());

            }
        } catch (Exception $e) {
            $this->installPermissions($lastUrl);            
        }
	}

	private function installPermissions($redirect_uri)
    {

        header('p3p: CP="NOI ADM DEV PSAi COM NAV OUR OTR STP IND DEM"');   //fix for IE

        $scope_game = Zend_Registry::get('facebook_game');
        $scope_game = $scope_game['scope'];
        $params = array(
                        'display' => 'page',
                        'scope' => $scope_game,
                        'redirect_uri' => $redirect_uri
                        );


        $loginUrl = $this->getLoginUrl($params);
       // $this->_redirect($loginUrl, array());
    }

	/**
	* To run facebook api by using graph
	*
	* @param array $param - Array of api parameters
	* @return array $response - Response data
	*
	*/
	protected function graph($param)
	{
		/*
			path 	The Graph API path for the request, e.g. "/me" for the logged in user's profile.
			method 	(optional) Specify the HTTP method for this request: 'GET', 'POST', or 'DELETE'.
			params 	(optional) Parameters specific to the particular Graph API method you are calling. Passed in as an associative array of 'name' => 'value' pairs.
		*/
		try
		{
			return $this->api($param['path'], $param['method'], $param['params']);
		}
		catch(FacebookApiException $e)
		{
		  $result = $e->getResult();
		  error_log(json_encode($result));
		  //$e_type = $e->getType();
		  //error_log('Got an ' . $e_type . ' while posting');
		}
	}

	/**
	* To run facebook api by using FQL
	*
	* @param array $fql - FQL command
	* @return array $response - Response data
	*
	*/
	protected function fql($param)
	{
		$fql  = array(
						 'query' => $param['query'],
						 'method' => 'fql.query',
						 'callback'  => $param['callback']
						);

		return $this->api($fql);
	}

	/**
	* To get file from URL
	*
	* @param str $url - URL
	* @return str $response - Response data
	*
	*/
	protected function _getContents($url, $follow=false)
	{
		$response = "";
		if($url == '') return $response;

		$ch = curl_init();
		curl_setopt_array($ch, array(
								CURLOPT_URL => $url,
								//CURLOPT_POSTFIELDS => $params,
								CURLOPT_FOLLOWLOCATION => $follow,  //follow any "Location: "
								CURLOPT_RETURNTRANSFER => true,
								CURLOPT_SSL_VERIFYPEER => false,
								CURLOPT_VERBOSE => true,
		));

		$response = curl_exec($ch); 
		curl_close($ch);

		return $response;
	}

	public function getFbId()
	{
		return $this->getUser();
	}
}

/*************************************************************************************************/

class FacebookApi Extends FbAbstract
{

	/**
	* To get user info from fbid
	*
	* @param int $fbid - FacebookID, If not input $fbid, it will be your fbid
	* @return array $ret - User info data
	*
	*/
	public function getUserInfo($fbid='')
	{
		$ret = 0;
		if($fbid == '') $fbid = $this->getUser();
		if($fbid == '') return $ret;
		
		$param = array	(
							'path' => "/{$fbid}",
							'method' => 'GET',
							'params' =>array()
						);
		$ret = $this->graph($param);

		return $ret;
	}

	/**
	* To get name from fbid
	*
	* @param int $fbid - FacebookID, If not input $fbid, it will be your fbid
	* @return str $ret - Name
	*
	*/
	public function getUserProfileName($fbid='')
	{
		$ret = 0;
		if($fbid == '') $fbid = $this->getUser();
		if($fbid == '') return $ret;
		
		$ret = $this->getProfile($fbid);

		return $ret['name'];
	}

	/**
	* To get user profile url 
	*
	* @param int $fbid - FacebookID, If not input $fbid, it will be your fbid
	* @return str $ret - Image URL
	*
	*/
	public function getUserProfileImageUrl($fbid='', $size='square')
	{
		$ret = 0;
		if($fbid == '') $fbid = $this->getUser();
		if($fbid == '') return $ret;

		// square (50x50)
		// small (50 pixels wide, variable height)
		// normal (100 pixels wide, variable height)
		// large (about 200 pixels wide, variable height)
		// picture?width=500&height=500
		$ret = parent::$DOMAIN_MAP['graph'] . "{$fbid}/picture?type=$size";
		
		return $ret;
	}

	public function getUserProfileImageUrlReal($fbid='', $pic_size='pic_square')
	{
		/*
		pic_small
			The URL to the small-sized profile picture for the user being queried. The image can have a maximum width of 50px and a maximum height of 150px. This URL may be blank.

		pic_big
			The URL to the largest-sized profile picture for the user being queried. The image can have a maximum width of 200px and a maximum height of 600px. This URL may be blank.

		pic_square
			The URL to the square profile picture for the user being queried. The image can have a maximum width and height of 50px. This URL may be blank.

		pic
			The URL to the medium-sized profile picture for the user being queried. The image can have a maximum width of 100px and a maximum height of 300px. This URL may be blank.
		*/
		$ret = 0;
		if($fbid == '') $fbid = $this->getUser();
		if($fbid == '') return $ret;

		$fql = "select {$pic_size} from user where uid={$fbid}";
		$param  =   array(
							'query'     => $fql,
							'callback'  => ''
						);
		$ret = $this->fql($param);

		return $ret[0]["$pic_size"];

	}

	/**
	* To get usr location from fbid
	*
	* @param int $fbid - FacebookID, If not input $fbid, it will be your fbid
	* @return str $ret - list['location']: latitude, longitude
	*
	*/
	public function getUserLocationList($fbid='',$limit='1')
	{
		$ret = 0;
		if($fbid=='') $fbid = $this->getUser();
		if($fbid == '') return $ret;
		
		$param = array	(
							'path' => "/{$fbid}/locations",
							'method' => 'GET',
							'params' =>array(
												'limit'=>$limit
											)
						);
		$ret = $this->graph($param);
		
		return $ret;
	}

	/**
	* To get user permissions
	*
	* @param int $fbid - FacebookID, If not input $fbid, it will be your fbid
	* @return str $ret - Permissions list
	*
	*/
	public function getUserPermissions($fbid='')
	{
		$ret = 0;
		if($fbid == '') $fbid = $this->getUser();
		if($fbid == '') return $ret;

		$param = array	(
							'path' => "/{$fbid}/permissions",
							'method' => 'GET',
							'params' =>array()
						);
		if( empty($this->user_permissions) ){
			$this->user_permissions = $this->graph($param);
		}
		
		return $this->user_permissions;
	}

	/**
	* To get friend list 
	*
	* @param int $fbid - FacebookID, If not input $fbid, it will be your fbid
	* @return str $ret - Friend list (name, fbid)
	*
	*/
	public function getUserFriendList($fbid='', $limit='100')
	{
		$ret = 0;
		if($fbid=='') $fbid = $this->getUser();
		if($fbid == '') return $ret;
		
		$param = array	(
							'path' => "/{$fbid}/friends",
							'method' => 'GET',
							'params' =>array(
												'limit'=>$limit
											)
						);
		$ret = $this->graph($param);
		
		return $ret;
	}
	

	/**
	* To get next from $fb_data['paging']['next']
	*
	* @param next url
	* @return str $ret - Friend list (name, fbid)
	*
	*/
	public function getNext($url_tmp)
	{
		$ret = 0;
		if($url_tmp == '') return $ret;
		$var = explode("https://graph.facebook.com/", $url_tmp);
		if( count($var) != 2) return $ret;

		$path = '/'.$var[1];
		$param = array	(
							'path' => $path,
							'method' => 'GET',
							'params' =>array()
						);
		$ret = $this->graph($param);
		
		return $ret;
	}

	/**
	* To get previous from $fb_data['paging']['previous']
	*
	* @param previous url
	* @return str $ret - Friend list (name, fbid)
	*
	*/
	public function getPrev($url_tmp)
	{
		$ret = 0;
		if($url_tmp == '') return $ret;

		$ret = $this->getNext($url_tmp);
		
		return $ret;
	}

	/**
	* To get album list 
	*
	* @param int $fbid - FacebookID, If not input $fbid, it will be your fbid
	* @return str $ret - Album list
	*
	*/
	public function getUserAlbumList($fbid='', $limit='100')
	{
		$ret = 0;
		if($fbid=='') $fbid = $this->getUser();
		if($fbid == '') return $ret;
		
		$param = array	(
							'path' => "/{$fbid}/albums",
							'method' => 'GET',
							'params' =>array(
												'limit'=>$limit
											)
						);
		$ret = $this->graph($param);
		
		return $ret;
	}
	
	/**
	* To get photo list 
	*
	* @param int $aid - Album id
	* @return str $ret - Photo list
	*
	*/
	public function getUserAlbumPhotoList($aid='', $limit='100')
	{
		$ret = 0;
		if($aid == '') return $ret;
		
		$param = array	(
							'path' => "/{$aid}/photos",
							'method' => 'GET',
							'params' =>array(
												'limit'=>$limit
											)
						);
		$ret = $this->graph($param);
		
		return $ret;
	}
	
	/**
	* To post message to wall
	*
	* @param	int $fbid - FacebookID, If not input $fbid, it will be your fbid
	*			array $attachment = array(
											'message' => $message,
											'name' => $name,
											'caption' => $caption,
											'link' => $link,
											'description' => $description,
											'picture' => $picture,
											'actions' => $actions
										);
	* @return	Array
					(
						[id] => 510926261_10151102068641262
					)
	*
	*/
	public function postToWall($fbid, $attachment)
	{
		$ret = 0;
		if($fbid=='') $fbid = $this->getUser();
		if($fbid == '') return $ret;
		if( empty($attachment) ) return $ret;
		
		$param = array	(
							'path' => "/{$fbid}/feed",
							'method' => 'POST',
							'params' => $attachment
						);
		$ret = $this->graph($param);

		return $ret;
	}

	/**
	* To get photo by photo id 
	*
	* @param	string $aname - Album name
	*			string $adesc - Album desc
	* @return	Array
					(
						[id] => 10151102064721262
					)
	*
	*/
	public function createAlbum($fbid, $aname, $adesc='')
	{
		$ret = 0;
		if($fbid=='') $fbid = $this->getUser();
		if($fbid == '') return $ret;
		if( empty($aname) ) return $ret;

		//Create an album
		$album_details = array(
				'name'=> $aname,
				'message'=> $adesc
		);
		$param = array	(
							'path' => "/{$fbid}/albums",
							'method' => 'POST',
							'params' =>$album_details
						);
		$ret = $this->graph($param);

		return $ret;
	}


	/**
	* To get photo by photo id 
	*
	* @param	string $aname - Album name
	*			string $adesc - Album desc
	* @return	Array
					(
						[id] => 10151102064721262
						[post_id] => 510926261_10151102064741262
					)
					Array
					(
					    [id] => photo_id (change)
					    [post_id] => fbid_10151102077741262 <= ? (not change)
									      10151102077251262 <= aid of this post
					)
	*
	*/
	public function postAlbumImage($album_id, $img, $msg)
	{
		$ret = 0;
		if( empty($album_id) ) return $ret;
		if( empty($img) ) return $ret;

		//Upload a photo to album of ID...
		$this->setFileUploadSupport(true);  
		$file = '@' . realpath($img);

		$photo_details = array(
			'message'=> $msg,
			'image'=> $file
		);
		$param = array	(
							'path' => "/{$album_id}/photos",
							'method' => 'POST',
							'params' =>$photo_details
						);
		$ret = $this->graph($param);
		
		return $ret;
	}
	
	/**
	* To get photo by photo id 
	*
	* @param	int $photo_id - Photo id
	* @return	Array
					(
						[0] => Array
							(
								[src] => http://sphotos-b.ak.fbcdn.net/hphotos-ak-ash4/s2048x2048/488392_10151102079996262_2087083343_n.jpg
								[width] => 2048
								[height] => 1456
							)
							...
					)

	*
	*/
	public function getImageFromPhotoId($photo_id)
	{
		$ret = 0;
		if($photo_id == '') return $ret;

		$fql = "SELECT src, width, height FROM photo_src WHERE photo_id='{$photo_id}'";

		$param  =   array(
							'query'     => $fql,
							'callback'  => ''
						);
		$ret   =   $this->fql($param);

		return $ret;
	}

	
	/**
	* To get Album id by album name
	*
	* @param	int $photo_id - Photo id
	* @return	Array
					(
						[0] => Array
							(
								[aid] => 2194411581662981947
								[owner] => 510926261
								[name] => name
								[object_id] => 10151102067591262 <= album id
							)
							...
					)

	*
	*/
	public function getAlbumId($album_name)
	{
		$fql =   'SELECT aid, owner, name, object_id FROM album WHERE owner=me() and name="'.$album_name.'"';

		$param  =   array(
							'query'     => $fql,
							'callback'  => ''
						);
		$ret   =   $this->fql($param);

		return $ret;
	}
	
	/**
	* To get likes list 
	*
	* @param 	int $fbid - FacebookID, If not input $fbid, it will be your fbid
	* @return	array $ret - Likes list
	*
	*/
	public function getUserLikes($fbid='', $limit='100')
	{
		$ret = 0;
		if($fbid=='') $fbid = $this->getUser();
		if($fbid == '') return $ret;
		
		$param = array	(
							'path' => "/{$fbid}/likes",
							'method' => 'GET',
							'params' =>array(
												'limit'=>$limit
											)
						);
		$ret = $this->graph($param);
		
		return $ret;
	}



	/**************************************************************************************************/
	
	/**
	* To get page profile from page id
	*
	* @param int $page_id - Fans page id
	* @return array $ret - Page info data
	*
	*/
	public function getPageInfo($page_id='')
	{
		$ret = 0;
		if($page_id == '') return $ret;
		
		$ret = $this->getUserInfo($page_id );

		return $ret;
	}

	/**
	* To get page wall post from page id
	*
	* @param int $page_id - Fans page id
	* @return array $ret - Page info data
	*
	*/
	public function getPageFeed($page_id='', $limit=10)
	{
		$ret = 0;
		if($page_id == '') return $ret;
		
		$param = array	(
							'path' => "/{$page_id}/feed",
							'method' => 'GET',
							'params' =>array(
												'limit'=>$limit
											)
						);
		$ret = $this->graph($param);

		return $ret;
	}

}


