<?php

set_time_limit(0);
date_default_timezone_set("Asia/Bangkok");
require_once 'NetSuiteService.php';

class InventorySync
{
    protected $_inventoryItems = array();
    protected $_kitItems = array();
    protected $_subsidiarySearch = null;
    protected $_host;
    protected $_username;
    protected $_password;
    protected $_dbname;
    protected $_connection;

    /**
     * Class constructor
     *
     * @return void
     */
    public function __construct()
    {
        $doc = new DOMDocument();
        $xml = simplexml_load_file('../../app/etc/local.xml');
        $config = $xml->global->resources->default_setup->connection;        
        $this->_host = $config->host;
        $this->_username = $config->username;
        $this->_password = $config->password;
        $this->_dbname = $config->dbname;
        $this->_createInventoryItemTable();
        $this->_createKeyInventoryKitTable();
    }

    /**
     * Return Mysql Connection
     *
     * @return resource
     */
    public function connect()
    {
        if ($this->_connection) {
            return $this->_connection;
        } else {
            $this->_connection = new mysqli($this->_host, $this->_username, $this->_password, $this->_dbname);
            return $this->connect();
        }
    }

    protected function _createInventoryItemTable()
    {
        $sql = "CREATE TABLE IF NOT EXISTS netsuite_inventory_item
                (
                    `internal_id`            INTEGER(11) NOT NULL UNIQUE,
                    `item_name`              VARCHAR(255),
                    `external_id`            VARCHAR(255),
                    `sku`                    VARCHAR(100),
                    `quantity_available`     INTEGER(11),
                    `is_kit`                 INTEGER(1),
                    PRIMARY KEY (`internal_id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
        $this->connect()->query($sql);
    }   
    
    protected function _createKeyInventoryKitTable()
    {
        $sql = "CREATE TABLE IF NOT EXISTS netsuite_kit_item
                (
                    `id`                 INTEGER(11) NOT NULL AUTO_INCREMENT,
                    `internal_id`        INTEGER(11) NOT NULL,
                    `external_id`        VARCHAR(255),
                    `item_internal_id`   INTEGER(11) NOT NULL,
                    `item_external_id`   VARCHAR(255),
                    `quantity`           VARCHAR(255),                    
                    UNIQUE KEY `netsuit_inventory_kit_unique` (`internal_id`, `item_internal_id`),
                    PRIMARY KEY (`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
        $this->connect()->query($sql);
    }

    public function _updateInventory($items)
    {
       foreach ($items as $item) {
            $values = sprintf("(%d, '%s', '%s', '%s', %d, %d)", $item->internalId,
                              addslashes($item->itemName), addslashes($item->externalId),
                                                                      addslashes($item->sku),
                                                                                 $item->quantityAvailable, $item->isKit);

            $sql = sprintf("INSERT INTO netsuite_inventory_item (internal_id, item_name, external_id, sku, quantity_available, is_kit)
                    VALUES %s ON DUPLICATE KEY UPDATE external_id = VALUES(external_id), quantity_available = VALUES(quantity_available);",
                           $values);

            $this->connect()->query($sql);
        }
    }

    public function _updateKitItem($items)
    {
        foreach ($items as $item) {

            $values = sprintf("(%d, '%s', %d, '%s', %d)", $item->internalId, addslashes($item->externalId), $item->itemInternalId, addslashes($item->itemExternalId), $item->quantity);
            $sql = sprintf("INSERT INTO netsuite_kit_item (internal_id, external_id, item_internal_id, item_external_id, quantity)
                    VALUES %s ON DUPLICATE KEY UPDATE external_id = VALUES(external_id), item_external_id = VALUES(item_external_id), quantity = VALUES(quantity);",
                           $values);

            $this->connect()->query($sql);
        }
    }

    public function getSubsidiarySearchValues($subsidiary)
    {
        //Ardent Universe : Ardent HK : Whatsnew HK : Whatsnew Company Limited
        if (is_null($this->_subsidiarySearch)) {
            $service = new NetSuiteService();
            $service->setSearchPreferences(false, 10);

            $subsidiarySearchBasic = new SubsidiarySearchBasic();
            $searchStringField = new SearchStringField();
            $searchStringField->operator = 'contains';
            $searchStringField->searchValue = $subsidiary;

            $subsidiarySearchBasic->name = $searchStringField;

            $request = new SearchRequest();
            $request->searchRecord = $subsidiarySearchBasic;
            $searchResponse = $service->search($request);

            if (!$searchResponse->searchResult->status->isSuccess) {
                $this->_subsidiarySearch = false;
            } else {
                $totalRecords = $searchResponse->searchResult->totalRecords;
                if ($totalRecords > 0) {
                    $records = $searchResponse->searchResult->recordList->record;
                    $searchValues = array();
                    foreach ($records as $record) {
                        //var_dump($record);
                        $searchValue = new RecordRef();
                        $searchValue->type = 'subsidiary';
                        $searchValue->internalId = $record->internalId;
                        $searchValues[] = $searchValue;
                        if (isset($record->parent)) {
                            $searchValue = new RecordRef();
                            $searchValue->type = 'subsidiary';
                            $searchValue->internalId = $record->parent->internalId;
                            $searchValues[] = $searchValue;
                        }
                    }
                    $this->_subsidiarySearch = $searchValues;
                } else {
                    $this->_subsidiarySearch = false;
                }
            }
        }
        return $this->_subsidiarySearch;
    }

    public function getItemList($bussIds)
    {
        $service = new NetSuiteService();
        $service->setSearchPreferences(false, 1000);

        /*$searchValues = $this->getSubsidiarySearchValues($subsidiary);*/
        
        $inventoryDetailSearchBasic = new ItemSearchBasic();
        /*if ($searchValues) {
            $searchMultiSelectField = new SearchMultiSelectField();
            $searchMultiSelectField->operator = 'anyOf';
            $searchMultiSelectField->searchValue = $searchValues;
        }
        $inventoryDetailSearchBasic->subsidiary = $searchMultiSelectField;
        */
        
        $businessUnits = explode(',', trim($bussIds));
        $businessValues = array();
        foreach($businessUnits as $businessUnit) {
            $businessValue = new RecordRef();
            $businessValue->type = 'classification';
            $businessValue->internalId = $businessUnit;
            $businessValues[] = $businessValue;            
        }
        $searchBusinessField = new SearchMultiSelectField();
        $searchBusinessField->operator = 'anyOf';
        $searchBusinessField->searchValue = $businessValues;
        
        $itemTypes = array('_inventoryItem', '_kit');
        foreach ($itemTypes as $itemType) {
            $searchValues = array($itemType);
            $searchEnumMultiSelectField = new SearchEnumMultiSelectField();
            $searchEnumMultiSelectField->operator = 'anyOf';
            $searchEnumMultiSelectField->searchValue = $searchValues;
            
            $inventoryDetailSearchBasic->class = $searchBusinessField;            
            $inventoryDetailSearchBasic->type = $searchEnumMultiSelectField;
             
            $request = new SearchRequest();
            $request->searchRecord = $inventoryDetailSearchBasic;
            $searchResponse = $service->search($request);

            if (!$searchResponse->searchResult->status->isSuccess) {
                echo "SEARCH ERROR\n";
            } else {
                echo "SEARCH SUCCESS, records found: " . $searchResponse->searchResult->totalRecords . "\n";
                $totalRecords = $searchResponse->searchResult->totalRecords;
                $records = $searchResponse->searchResult->recordList->record;
                foreach ($records as $record) {
                    $item = $this->_covertItemToObject($record);
                    $item->isKit = false;
                    if ($itemType == '_kit') {
                        $item->quantityAvailable = $this->_getQuantityAvailable($record->memberList->itemMember,
                                                                                $item);
                        $item->isKit = true;
                    }
                    //echo $item->internalId . "\n";
                    $this->_inventoryItems[$record->internalId] = $item;
                }

                while (($searchResponse->searchResult->pageIndex * 1000) < $totalRecords) {
                    $moreRequest = new SearchMoreWithIdRequest();
                    $moreRequest->pageIndex = $searchResponse->searchResult->pageIndex + 1;
                    $moreRequest->searchId = $searchResponse->searchResult->searchId;
                    $searchResponse = $service->searchMoreWithId($moreRequest);

                    if (!$searchResponse->searchResult->status->isSuccess) {
                        echo "SEARCH ERROR\n";
                    } else {
                        $records = $searchResponse->searchResult->recordList->record;
                        foreach ($records as $record) {
                            $item = $this->_covertItemToObject($record);
                            $item->isKit = false;
                            if ($itemType == '_kit') {
                                $item->quantityAvailable = $this->_getQuantityAvailable($record->memberList->itemMember,
                                                                                        $item);
                                $item->isKit = true;
                            }
                            //echo $item->internalId . "\n";
                            $this->_inventoryItems[$record->internalId] = $item;
                        }
                    }
                }
            }
        }
        return $this;
    }

    protected function _getQuantityAvailable($itemMembers, $item)
    {
        $quantities = array();
        foreach ($itemMembers as $itemMember) {
            $kitItem = new stdClass();
            $kitItem->internalId = $item->internalId;
            $kitItem->externalId = $item->externalId;
            $kitItem->itemInternalId = $itemMember->item->internalId;
            $kitItem->itemExternalId = $this->_inventoryItems[$itemMember->item->internalId]->externalId;
            $kitItem->quantity = $itemMember->quantity;
            $this->_kitItems[] = $kitItem;
            $quantities[] = (int) ($this->_inventoryItems[$itemMember->item->internalId]->quantityAvailable / $itemMember->quantity);
        }
        return min($quantities);
    }

    protected function _covertItemToObject($record)
    {
        $item = new stdClass();
        $item->internalId = $record->internalId;
        $item->itemName = $record->itemId;
        $item->externalId = $record->externalId;
        $item->sku = '';
        $customFields = $record->customFieldList->customField;
        foreach ($customFields as $customField) {
            if ($customField->internalId == 'custitem11') {
                $item->sku = $customField->value;
                break;
            }
        }
        $item->quantityAvailable = 0;
        if (isset($record->locationsList)) {
            $locations = $record->locationsList->locations;
            foreach ($locations as $location) {
                if (strpos($location->location, 'Defective') === false) {
                    $item->quantityAvailable += isset($location->quantityAvailable) ? (int) $location->quantityAvailable
                                : 0;
                }
            }
        }
        return $item;
    }

    public function getInventoryItems()
    {
        return $this->_inventoryItems;
    }

    public function syncData($subsidiary)
    {
        $this->getItemList($subsidiary);
        $this->_updateInventory($this->_inventoryItems);
        $this->_updateKitItem($this->_kitItems);
    }
}

//global $nshost, $nsendpoint;
//global $nsaccount, $nsemail, $nsrole, $nspassword;

$nshost = $argv[1];//"https://webservices.sandbox.netsuite.com";
$nsemail = $argv[2];//"ranai@acommerce.asia";
$nspassword = $argv[3];//"abc123";
$nsrole = $argv[4];//"3";
$nsaccount = $argv[5];//"3686224";
$nsendpoint = $argv[6];//"2013_1";
$bussIds = $argv[7];//"2013_1";

$inventory = new InventorySync();
$inventory->syncData($bussIds);