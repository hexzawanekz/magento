<?php
require_once dirname(__FILE__) . '/abstract.php';

class Update_Sales_Flat_Order_Grid extends Mage_Shell_Abstract
{
    public function run()
    {
        try {
            Mage::getModel('sales/order')->getResource()->updateGridRecords(Mage::getResourceModel('sales/order_collection')->getAllIds());
            echo 'Done!' . PHP_EOL;
        } catch (Exception $e) {
            echo $e->getMessage() . PHP_EOL;
        }
    }
}

$shell = new Update_Sales_Flat_Order_Grid();
$shell->run();
