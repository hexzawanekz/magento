<?php

require_once 'abstract.php';
error_reporting(-1);
set_time_limit(0);
ini_set('max_execution_time', 0);
ini_set('memory_limit', '-1');

class Resave_Products extends Mage_Shell_Abstract
{
    public function run()
    {
        try {
            if(Mage::registry('outstock_sub')){
                Mage::unregister('outstock_sub');
            }
            Mage::register('outstock_sub', true);
            $collection = Mage::getResourceModel('catalog/product_collection')->addAttributeToFilter('type_id', array('eq' => 'configurable'));

            foreach ($collection as $product) {
                $product = Mage::getModel('catalog/product')->load($product->getId());
                $product->setIsChanged(true);
                $product->save();
            }
        }catch(Exception $e){
            Mage::log($e->getMessage(),null,'resaveProduct.log',true);
        }
    }
}

$shell = new Resave_Products();
$shell->run();