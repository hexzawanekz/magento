<?php
/**
 * This function is used to mark old orders before May 1st, 2015 as exported.
 * Find out to log file 'checkListOfOrdersExported.log' to see the details of work.
 */

echo "\nThis function is used to mark old orders before May 1st, 2015 as exported.";
echo "\nFind out to log file 'checkListOfOrdersExported.log' to see the details of work.";

// init Magento
ini_set('max_execution_time', 0);
ini_set('display_error', 1);
require_once 'app/Mage.php';
Mage::getIsDeveloperMode(true);
Mage::app();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

try {
    // temp
    $formatStr = 'Y-m-d H:i:s';
    $dateObj = Mage::getSingleton('core/date');

    // get connection
    $resource = Mage::getSingleton('core/resource');
    $readConnection = $resource->getConnection('core_read');
    $writeConnection = $resource->getConnection('core_write');

    // build query
    $table = $resource->getTableName("sales/order");
    $timeToSort = '2015-04-30 23:59:59';
    $query = "SELECT `entity_id`,`created_at` FROM {$table} WHERE `is_exported` = 0 AND `created_at` <= '{$timeToSort}'";

    // query old orders before May
    $orders = $readConnection->fetchAll($query);

    if (count($orders) > 0) {
        Mage::log("---------- START PROCESSING", null, 'checkListOfOrdersExported.log', true);
        foreach ($orders as $order) {
            $logStr = "Order ID: {$order['entity_id']} - created at: {$order['created_at']}";
            Mage::log($logStr, null, 'checkListOfOrdersExported.log', true);
            // build sub query
            $subQuery = "UPDATE {$table} SET `is_exported` = 1 WHERE `entity_id` = {$order['entity_id']}";
            // execute query to save
            $writeConnection->query($subQuery);
            $gridQuery = "UPDATE `sales_flat_order_grid` SET `is_exported` = 1 WHERE `entity_id` = {$order['entity_id']}";
            $writeConnection->query($gridQuery);
        }
        Mage::log('---------- END PROCESSING', null, 'checkListOfOrdersExported.log', true);
        Mage::log('', null, 'checkListOfOrdersExported.log', true);
    }
    echo "\n\nSuccess!!!";
} catch (Exception $e) {
    echo "\n\n" . $e->getMessage();
}