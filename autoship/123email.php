<?php
	error_reporting(-1);
	define('MAGENTO_ROOT', getcwd());
	$mageFilename = MAGENTO_ROOT . '/../app/Mage.php';
	require_once $mageFilename;
	ini_set('display_errors', 1);
	umask(0);
	Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
	$sequences = Mage::getModel('recurringandrentalpayments/sequence')->getCollection();
	foreach ($sequences as $sequence) {
					$subscription = Mage::getModel('recurringandrentalpayments/subscription')->load($sequence->getSubscriptionId());
					if ($subscription->getId() != $sequence_id) {
						$sequence_id = $subscription->getId();
						$order_id = $subscription->getParentOrderId();
						$order = Mage::getModel('sales/order')->loadByIncrementId($order_id);
						$payment = $order->getPayment();
						$was = $payment->getAdditionalInformation();
						$event = Mage::getModel('recurringandrentalpayments/alert_event');
						if ($was['Paid Channel'] == 'Over the counter'){
							if ($sequence->getMailsent() != 1 && (Mage::getStoreConfig(Indies_Recurringandrentalpayments_Helper_Config::XML_PATH_NEXT_PAYMNET_REMINDER) == '1') && $subscription->getStatus() == 1) {
							/// email for OTC ////
									$event->send($subscription,
									Mage::getStoreConfig(Indies_Recurringandrentalpayments_Helper_Config::XML_PATH_NEXT_123_PAYMNET_REMINDER_TEMPLATE),
									0,
									Mage::getStoreConfig(Indies_Recurringandrentalpayments_Helper_Config::XML_PATH_NEXT_PAYMNET_REMINDER_SENDER),
									Mage::getStoreConfig(Indies_Recurringandrentalpayments_Helper_Config::XML_PATH_NEXT_PAYMNET_REMINDER_CC_TO));
								$sequence->setMailsent(1)->save();	
							}
						}else{
							if ($sequence->getMailsent() != 1 && (Mage::getStoreConfig(Indies_Recurringandrentalpayments_Helper_Config::XML_PATH_NEXT_PAYMNET_REMINDER) == '1') && $subscription->getStatus() == 1) {
							/// email for CC card ////
									$event->send($subscription,
									Mage::getStoreConfig(Indies_Recurringandrentalpayments_Helper_Config::XML_PATH_NEXT_PAYMNET_REMINDER_TEMPLATE),
									0,
									Mage::getStoreConfig(Indies_Recurringandrentalpayments_Helper_Config::XML_PATH_NEXT_PAYMNET_REMINDER_SENDER),
									Mage::getStoreConfig(Indies_Recurringandrentalpayments_Helper_Config::XML_PATH_NEXT_PAYMNET_REMINDER_CC_TO));
								$sequence->setMailsent(1)->save();	
							}
					}
				}
			}

?>