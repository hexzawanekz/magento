<?php
        error_reporting(-1);
        define('MAGENTO_ROOT', getcwd());
        $mageFilename = MAGENTO_ROOT . '/../app/Mage.php';
        require_once $mageFilename;
        ini_set('display_errors', 1);
        umask(0);
        Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
        $payment = Mage::getModel('normal2c2p/normal2c2p');
        $merchant_id = $payment->getConfigData('merchant_id');
        $param = $_GET['order_id'];
        $param = Mage::getModel('sales/order')->loadByIncrementId($param);
        $param = $param->getId();
        $sequences = Mage::getModel('recurringandrentalpayments/sequence')->getCollection()
                                        ->setOrder('date', 'ASC')
                                        ->addFieldToFilter('order_id', $param);
                                foreach ($sequences as $sequence) {
                                        $subscription = Mage::getModel('recurringandrentalpayments/subscription')->load($sequence->getSubscriptionId());
                                        //if ($subscription->getId() != $sequence_id) {
                                                $sequence_id = $subscription->getId();
                                                $order_id = $subscription->getParentOrderId();
                                                $order = Mage::getModel('recurringandrentalpayments/sequence')
                                             ->getCollection()
                                             ->addFieldToFilter('subscription_id',$sequence_id)
                                             ->addFieldToFilter('status','pending_payment')
                                             ->addFieldToFilter('order_id',array('notnull' => true))
                                             ->setOrder('date', 'asc')
                                             ->setPageSize(1);
                                        foreach ($order as $o){
                                                $order2 = $o->getOrderId();
                                                $oid = Mage::getModel('sales/order')->load($order2);
                                                $order2 = $oid->getIncrementId();
                                        }
    $version = $payment->getVersion();
    $merchant_id = $payment->getMerchantId();
    $payment_description = $payment->getPaymentDescription();
    $main_id =  Mage::getModel('sales/order')->loadByIncrementId($order2);
    $main_id = $main_id->getId();
    $order_id = $main_id;
    $invoice_no = $order2;
    $currency = $payment->getCurrencyCode();
        $base = $oid->getBaseGrandTotal();
        $ship = $oid->getShippingAmount();
        $total = $base - $ship;
    $amount = $payment->getAmount($total);
    $customer_email = $oid->getCustomerEmail();
    $payment_option = '1';
    $pay_category_id = $payment->getPayCategoryID();
    $promotion = $payment->getPromotion();
    $user_defined_1 = $payment->getUserDefined(1);
    $user_defined_2 = $payment->getUserDefined(2);
    $user_defined_3 = $payment->getUserDefined(3);
    $user_defined_4 = $payment->getUserDefined(4);
    $user_defined_5 = $payment->getUserDefined(5);
    $result_url_1 = '';
    $result_url_2 = '';
	$secret_key = $payment->getSecretKey();
        $strSignatureString =$version . $merchant_id . $payment_description . $invoice_no
            . $order_id . $currency . $amount . $customer_email . $payment_option . $pay_category_id .
            $promotion . $user_defined_1 . $user_defined_2 . $user_defined_3 .
            $user_defined_4 . $user_defined_5 . $result_url_1 . $result_url_2 ;
        $HashValue = hash_hmac('sha1', $strSignatureString, $secret_key, false);
                }

?>
<form action='<?php echo $payment->getGatewayRedirectUrl() ;?>' method='POST' name='authForm'>
<input type="hidden" id="version" name="version" value="<?php echo $version;?>"/>
<input type="hidden" id="merchant_id" name="merchant_id" value="<?php echo $merchant_id;?>">
<input type="hidden" id="payment_description" name="payment_description" value="<?php echo $payment_description;?>" />
<input type="hidden" id="order_id" name="order_id" value="<?php echo $invoice_no;?>" />
<input type="hidden" id="invoice_no" name="invoice_no"  value="<?php echo $order_id;?>" />
<input type="hidden" id="currency" name="currency"  value="<?php echo $currency; ?>"/>
<input type="hidden" id="amount" name="amount"  value="<?php echo $amount;?>"/>
<input type="hidden" id="customer_email" name="customer_email" value="<?php echo $customer_email;?>"/>
<input type="hidden" id="payment_option" name="payment_option" value="<?php echo $payment_option;?>"/>
<input type="hidden" id="pay_category_id" name="pay_category_id" value="<?php echo $pay_category_id;?>"/>
<input type="hidden" id="promotion" name="promotion" value="<?php echo $promotion;?>"/>
<input type="hidden" id="user_defined_1" name="user_defined_1" value="<?php echo $user_defined_1;?>"/>
<input type="hidden" id="user_defined_2" name="user_defined_2" value="<?php echo $user_defined_2;?>"/>
<input type="hidden" id="user_defined_3" name="user_defined_3" value="<?php echo $user_defined_3;?>"/>
<input type="hidden" id="user_defined_4" name="user_defined_4" value="<?php echo $user_defined_4;?>"/>
<input type="hidden" id="user_defined_5" name="user_defined_5" value="<?php echo $user_defined_5;?>"/>
<input type="hidden" id="result_url_1" name="result_url_1" value="<?php echo $result_url_1;?>"/>
<input type="hidden" id="result_url_2" name="result_url_2" value="<?php echo $result_url_2;?>"/>
<input type="hidden" id="hash_value" name="hash_value" value="<?php echo $HashValue;?>"/>
</form>

<script language="JavaScript">
        document.authForm.submit();
</script>

