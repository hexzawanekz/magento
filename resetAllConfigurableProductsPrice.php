<?php

// init Magento
ini_set('max_execution_time', 0);
ini_set('display_error', 1);
require_once 'app/Mage.php';
Mage::getIsDeveloperMode(true);
Mage::app();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

echo "Setting... \n";
Mage::log("Start setting...", null, 'super_price.log', true);

// Get all configurable products
$collectionConfigurable = Mage::getResourceModel('catalog/product_collection')
    ->addAttributeToFilter('type_id', array('eq' => 'configurable'));
$configurableSize = count($collectionConfigurable);
$count = 1;

foreach ($collectionConfigurable as $configurableProduct) {
    /** @var Mage_Catalog_Model_Product $configurableProduct */
    $current = $count . '/' . $configurableSize . '(pid:' . $configurableProduct->getId() . ')' . "\n";
    echo $current;
    Mage::log($current, null, 'super_price.log', true);

    try {
        setPriceByBestValue($configurableProduct);
        resetSupperPrice($configurableProduct);
        $count++;
    } catch (Exception $e) {
        Mage::log($configurableProduct->getId(), null, 'super-price.log', true);
        Mage::log($e->getMessage(), null, 'super_price.log', true);
    }
}

echo "Done";
Mage::log("Done", null, 'super_price.log', true);

//============================================================
// Set price by best prodcut
//============================================================
function setPriceByBestValue($product)
{
    /** @var Mage_Catalog_Model_Product $product */
    /** @var Mage_Catalog_Model_Product $priceOfBestValue */
    try {
        $bestProduct = Mage::helper('petloftcatalog')->getBestProduct($product);
        if ($bestProduct) {
            $product->setPrice($bestProduct->getFinalPrice());
            $product->setMsrp($bestProduct->getMsrp());
            $product->save();
        } else {
            $product->setStatus(2);
            $changeStatue = "pid:" . $product->getId() . "(This product was setted disabled)" . "\n";
            $product->save();
            echo $changeStatue;
            Mage::log($changeStatue, null, 'super_price.log', true);
        }
    } catch (Exception $e) {
        Mage::log($product->getId(), null, 'super-price.log', true);
        Mage::log($e->getMessage(), null, 'super_price.log', true);
    }
}

//============================================================
// Reset supper price
//============================================================
function resetSupperPrice($product)
{
    try {
        $configAttributes = getSupperPrice($product);
        if ($configAttributes) {
            /** @var Mage_Catalog_Model_Product $product */
            $product->setConfigurableAttributesData($configAttributes)->save();
        }
    } catch (Exception $e) {
        Mage::log($product->getId(), null, 'super-price.log', true);
        Mage::log($e->getMessage(), null, 'super_price.log', true);
    }
}

//========================================================================
// get supper price by best value price minus price of each simple product
//========================================================================
function getSupperPrice($parent)
{
    try {
        // Attribute Array
        $configAttributes = $parent->getTypeInstance(true)->getConfigurableAttributesAsArray($parent);

        $configurable = Mage::getModel('catalog/product_type_configurable')->setProduct($parent);
        $simpleCollection = $configurable->getUsedProductCollection()
            ->addAttributeToSelect('*')
            ->addFilterByRequiredOptions();

        // Child array
        $configChild = array();
        foreach ($simpleCollection as $aProduct) {
            /** @var Mage_Catalog_Model_Product $aProduct - */
            $key = $aProduct->getEntityId();
            $value = array();
            foreach ($configAttributes as $attr) {
                foreach ($attr['values'] as $option) {
                    if ($option['value_index'] == $aProduct->getData($attr['attribute_code'])) {
                        $data['attribute_id'] = $attr['attribute_id'];
                        $data['label'] = $option['label'];
                        $data['value_index'] = $option['value_index'];
                        $value[] = $data;
                    }
                }
            }
            $configChild[$key] = $value;
        }

        $bestProduct = Mage::helper('petloftcatalog')->getBestProduct($parent);
        if ($bestProduct) {
            $parentPrice = $bestProduct->getFinalPrice();
        } else {
            $parentPrice = $parent->getFinalPrice();
        }

        $count = 0;
        $pos = 0;

        foreach ($configAttributes as $attribute) {
            if ($attribute['position'] == 0) {
                $pos = $count;
            } else {
                $count++;
            }
        }

        // pricing value array
        $children = array();
        foreach ($configChild as $id => $data) {
            if (!isset($data[$pos]['value_index'])) {
                continue;
            }
            $children[$data[$pos]['value_index']] = Mage::getModel('catalog/product')->load($id)->getFinalPrice();
        }

        if (empty($children)) {
            return null;
        }
        $count = 0;
        foreach ($configAttributes as &$attribute) {
            if (isset($attribute['values']) && is_array($attribute['values'])) {
                if ($attribute['position'] == 0 && $count == 0) {
                    $count++;
                    foreach ($attribute['values'] as &$attributeValue) {
                        $valueIndx = isset($attributeValue['value_index']) ? $attributeValue['value_index'] : '';
                        foreach ($children as $indx => $finalPrice) {
                            if ($indx == $valueIndx) {
                                $attributeValue['pricing_value'] = $finalPrice - $parentPrice;
                                $attributeValue['is_percent'] = 0;
                                $attributeValue['can_edit_price'] = 0;
                                $attributeValue['can_read_price'] = 1;
                            }
                        }
                    }
                } else {
                    foreach ($attribute['values'] as &$attributeValue) {
                        $attributeValue['pricing_value'] = 0;
                        $attributeValue['is_percent'] = 0;
                        $attributeValue['can_edit_price'] = 0;
                        $attributeValue['can_read_price'] = 1;
                    }
                }
            }
        }
        return $configAttributes;
    } catch (Exception $e) {
        Mage::log($parent->getId(), null, 'super-price.log', true);
        Mage::log($e->getMessage(), null, 'super_price.log', true);
    }
}