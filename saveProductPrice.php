<?php

// * 2,6,9,11,15,19,21,23 * * *
// Possible times for cron

/**
 * Error reporting
 */
error_reporting(-1);

/**
 * Compilation includes configuration file
 */
define('MAGENTO_ROOT', getcwd());
$mageFilename = MAGENTO_ROOT . '/app/Mage.php';
require_once $mageFilename;
ini_set('display_errors', 1);
umask(0);
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

try {
    $products = Mage::getModel('catalog/product')->getCollection();
    echo "Saving product.....Please wait \n";
    foreach($products as $product){
        if($product->getTypeId() == 'simple'){
            $configurable_product_model = Mage::getModel('catalog/product_type_configurable');
            $parentIdArray= $configurable_product_model->getParentIdsByChild($product->getId());
            if($parentIdArray){
                $product->save();
            }

        }
    }
} catch (Exception $e) {
    Mage::log($e->getMessage(), null, 'product_save_price_error.log', true);
}

exit(0);
