<?php
/**
 * Error reporting
 */
error_reporting(-1);

/**
 * Compilation includes configuration file
 */
define('MAGENTO_ROOT', getcwd());
$mageFilename = MAGENTO_ROOT . '/app/Mage.php';
require_once $mageFilename;
ini_set('display_errors', 0);
umask(0);
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
$collectionConfigurable = Mage::getResourceModel('catalog/product_collection')->addAttributeToFilter('type_id', array('eq' => 'configurable'));

foreach($collectionConfigurable as $_configurableproduct) {

    /**
     * Load Product you want to get Super attributes of
     */
    $product = Mage::getModel('catalog/product')->load($_configurableproduct->getId());
    $product->save();
//    $productType = $product->getTypeId();
//    $configurablePrice = $product->getPrice();
//    $associatedProducts = $product->getTypeInstance()->getUsedProducts();
//    $stack = array();
//    for ($j = 0; $j < sizeof($associatedProducts); $j++) {
//        array_push($stack, Array("color" => $associatedProducts[$j]['color'], "size" => $associatedProducts[$j]['size'], "price" => $associatedProducts[$j]['price']));
//    }
//
//    if ($data = $product->getTypeInstance(true)->getConfigurableAttributesAsArray(($product))) {
//
//        foreach ($data as $attributeData) {
//            $id = isset($attributeData['id']) ? $attributeData['id'] : null;
//            $size = sizeof($attributeData['values']);
//            for ($j = 0; $j < $size; $j++) {
//                multiArrayValueSearch($stack, $attributeData['values'][$j]['value_index'], $match);
//                reset($match); // make sure array pointer is at first element
//                $firstKey = key($match);
//                Zend_Debug::dump($firstKey);die;
//                $match = array();
//                $attributeData['values'][$j]['pricing_value'] = $stack[$firstKey]['price'] - $configurablePrice;
//            }
//
//            if ($id == 7) {   // Check your $id value
//                $attribute = Mage::getModel('catalog/product_type_configurable_attribute')
//                    ->setData($attributeData)
//                    ->setId($id)
//                    ->setStoreId($product->getStoreId())
//                    ->setProductId($productid)
//                    ->save();
//            }
//        }
//    }

}

function multiArrayValueSearch($haystack, $needle, &$result, &$aryPath=NULL, $currentKey='') {
    if (is_array($haystack)) {
        $count = count($haystack);
        $iterator = 0;
        foreach($haystack as $location => $straw) {
            $iterator++;
            $next = ($iterator == $count)?false:true;
            if (is_array($straw)) $aryPath[$location] = $location;
            multiArrayValueSearch($straw,$needle,$result,$aryPath,$location);
            if (!$next) {
                unset($aryPath[$currentKey]);
            }
        }
    } else {
        $straw = $haystack;
        if ($straw == $needle) {
            if (!isset($aryPath)) {
                $strPath = "\$result[$currentKey] = \$needle;";
            } else {
                $strPath = "\$result['".join("']['",$aryPath)."'][$currentKey] = \$needle;";
            }
            eval($strPath);
        }
    }
}
exit(0);
?>
