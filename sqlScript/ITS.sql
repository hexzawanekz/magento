select cpe.entity_id AS "product_id", 
cpe.sku AS "sku", 
cpev2.value AS "product_name", 
cpev5.value AS "product_description",
FORMAT(cped.value,0) AS "price",
FORMAT(LEAST(IFNULL(IF(now()>=IFNULL(cped2.value, ADDDATE(now(), INTERVAL -99999 DAY)) && now()<= IFNULL(cped3.value, ADDDATE(now(), INTERVAL 999999 DAY)), cped4.value, cped5.value),cped5.value), cped5.value),0)  AS "discounted_price",
/*FORMAT(cped.value - LEAST(IFNULL(IF(now()>=IFNULL(cped2.value, ADDDATE(now(), INTERVAL -99999 DAY)) && now()<= IFNULL(cped3.value, ADDDATE(now(), INTERVAL 999999 DAY)), cped4.value, cped5.value),cped5.value), cped5.value),0) AS "discount_amount",
FORMAT(100*(1 - LEAST(IFNULL(IF(now()>=IFNULL(cped2.value, ADDDATE(now(), INTERVAL -99999 DAY)) && now()<= IFNULL(cped3.value, ADDDATE(now(), INTERVAL 999999 DAY)), cped4.value, cped5.value),cped5.value), cped5.value)/cped.value),0) AS "percentage_savings",*/
CONCAT("https://www.orami.co.th/",cpev3.value) AS "product_url", 
IFNULL(CASE cpev4.value WHEN "no_selection" THEN null ELSE CONCAT("https://www.orami.co.th/media/catalog/product",REPLACE(CONCAT("/",cpev4.value),"//","/")) END, IFNULL(CASE cpemg.value WHEN "no_selection" THEN NULL ELSE CONCAT("https://www.orami.co.th/media/catalog/product",cpemg.value) END, CASE cpev4p.value when "no_selection" THEN CONCAT("https://www.orami.co.th/media/catalog/product",REPLACE(CONCAT("/",cpev4p.value),"//","/")) ELSE null END)) AS "image_url", 
categorySub2.categoryName AS "category_name_level2", 
categorySub3.categoryName AS "category_name_level3"
from catalog_product_entity cpe 
left join catalog_product_entity_int cpei ON cpe.entity_id = cpei.entity_id AND cpei.attribute_id = 273 and cpei.store_id = 0 -- enabled status 
left join catalog_product_entity_int cpei2 ON cpe.entity_id = cpei2.entity_id AND cpei2.attribute_id = 526 AND cpei2.store_id = 0 -- visibility 
left join catalog_product_entity_int cpei3 ON cpe.entity_id = cpei3.entity_id AND cpei3.attribute_id = 1281  AND cpei3.store_id = 0 -- brand ID
left join catalog_product_entity_int cpei5 ON cpe.entity_id = cpei5.entity_id and cpei5.attribute_id = 1098 AND cpei5.store_id = 0 -- is bundle ?
left join catalog_product_entity_varchar cpev2 ON cpe.entity_id = cpev2.entity_id AND cpev2.attribute_id = 96 AND cpev2.store_id = 0 -- product name
left join catalog_product_entity_varchar cpev3 ON cpe.entity_id = cpev3.entity_id AND cpev3.attribute_id = 570 AND cpev3.store_id = 0 -- product url
left join catalog_product_entity_varchar cpev4 ON cpe.entity_id = cpev4.entity_id AND cpev4.attribute_id = 106 AND cpev4.store_id = 0 -- image url
left join catalog_product_entity_varchar cpev5 ON cpe.entity_id = cpev5.entity_id AND cpev5.attribute_id = 1058 AND cpev5.store_id = 0 -- product title
left join catalog_product_entity_decimal cped ON cpe.entity_id = cped.entity_id AND cped.attribute_id = 943 AND cped.store_id = 0 -- msrp
left join catalog_product_entity_datetime cped2 ON cpe.entity_id = cped2.entity_id AND cped2.attribute_id = 568 AND cped2.store_id = 0  -- special price from date -- needed for website price
left join catalog_product_entity_datetime cped3 ON cpe.entity_id = cped3.entity_id AND cped3.attribute_id = 569 AND cped3.store_id = 0 -- special price to date -- needed for website price
left join catalog_product_entity_decimal cped4 ON cpe.entity_id = cped4.entity_id AND cped4.attribute_id = 567 AND cped4.store_id = 0 -- special price -- needed for website price
left join catalog_product_entity_decimal cped5 ON cpe.entity_id = cped5.entity_id AND cped5.attribute_id = 99 AND cped5.store_id = 0 -- price -- needed for website price
left join eav_attribute_option_value eaov ON cpei3.value = eaov.option_id AND eaov.store_id = 0 -- brand name
left join cataloginventory_stock_item csi ON cpe.entity_id = csi.product_id -- inventory
left join 
(select cpsl.product_id, cpsl.parent_id from catalog_product_super_link cpsl
group by  cpsl.product_id 
) cpslSub ON cpe.entity_id = cpslSub.product_id -- product links

left join catalog_product_entity cpe_parent ON cpslSub.parent_id = cpe_parent.entity_id -- parent info
left join catalog_product_entity_int cpei4 ON cpe_parent.entity_id = cpei4.entity_id AND cpei4.attribute_id = 526 AND cpei4.store_id = 0 -- visibility of parent 
left join catalog_product_entity_varchar cpev4p ON cpe_parent.entity_id = cpev4p.entity_id AND cpev4p.attribute_id = 106 AND cpev4p.store_id = 0 -- parent image url
left join   
(
select cpemgv.value_id, cpemgv.store_id, CAST(cpemgv.label AS decimal) AS label from catalog_product_entity_media_gallery_value cpemgv
where cpemgv.label is not null 
GROUP BY cpemgv.store_id, CAST(cpemgv.label AS decimal)
) cpemgvSub ON cpe.entity_id =  cpemgvSub.label AND cpemgvSub.store_id = 0
left join catalog_product_entity_media_gallery cpemg ON cpe_parent.entity_id = cpemg.entity_id AND cpemgvSub.value_id = cpemg.value_id 
-- level 2 category
left join 
(
select cpe.entity_id, ccp.category_id AS categoryID, ccev.value AS categoryName from catalog_product_entity cpe
left join catalog_category_product ccp ON  cpe.entity_id = ccp.product_id
left join catalog_category_entity cce ON ccp.category_id = cce.entity_id 
left join catalog_category_entity_varchar ccev ON ccp.category_id = ccev.entity_id AND ccev.store_id = 0 and ccev.attribute_id = 111
where cce.level = 2 AND ccev.entity_id NOT IN (2488, 2654, 3260)
group by cpe.entity_id
) categorySub2 ON cpe.entity_id = categorySub2.entity_id
-- level 3 category
left join 
(
select cpe.entity_id, ccp.category_id AS categoryID, ccev.value AS categoryName, cce.parent_id AS parentID from catalog_product_entity cpe
left join catalog_category_product ccp ON  cpe.entity_id = ccp.product_id
left join catalog_category_entity cce ON ccp.category_id = cce.entity_id 
left join catalog_category_entity_varchar ccev ON ccp.category_id = ccev.entity_id AND ccev.store_id = 0 and ccev.attribute_id = 111
where cce.level = 3 AND ccev.entity_id NOT IN (804, 803, 3411, 1580, 3379, 3470, 3414, 2678, 3380, 2782, 3412, 2673, 2413, 3392, 3413, 3406, 2877, 3378, 2739, 2668, 3407, 113, 146, 1573, 3409, 1117, 3490, 3410, 1576, 1030, 566, 1568, 3408, 2143)
group by cpe.entity_id, cce.parent_id
) categorySub3 ON cpe.entity_id = categorySub3.entity_id AND categorySub2.categoryID = categorySub3.parentID
left join 
(
select cpe.entity_id, ccp.category_id AS categoryID, ccev.value AS categoryName, cce.parent_id AS parentID from catalog_product_entity cpe
left join catalog_category_product ccp ON  cpe.entity_id = ccp.product_id
left join catalog_category_entity cce ON ccp.category_id = cce.entity_id 
left join catalog_category_entity_varchar ccev ON ccp.category_id = ccev.entity_id AND ccev.store_id = 0 and ccev.attribute_id = 111
where cce.level = 4 AND ccev.entity_id NOT IN (233, 200, 209, 221, 253, 454, 1951, 1326, 1825, 1913, 2872, 2894, 2898, 2904, 2907, 645, 80, 441, 449, 490, 685, 1892)
group by cpe.entity_id, cce.parent_id
) categorySub4 ON cpe.entity_id = categorySub4.entity_id AND categorySub3.categoryID = categorySub4.parentID
left join
(
select cpe.entity_id,ccp.category_id AS categoryID, ccev.value AS categoryName, cce.parent_id AS parentID from catalog_product_entity cpe
left join catalog_category_product ccp ON  cpe.entity_id = ccp.product_id
left join catalog_category_entity cce ON ccp.category_id = cce.entity_id 
left join catalog_category_entity_varchar ccev ON ccp.category_id = ccev.entity_id AND ccev.store_id = 0 and ccev.attribute_id = 111
where cce.level = 5
)categorySub5 on cpe.entity_id = categorySub5.entity_id AND categorySub4.categoryID = categorySub5.parentID
-- custom category id
LEFT JOIN criteo_custom_category_mapping cccm ON categorySub2.categoryID = cccm.custom_vertical_id AND categorySub3.categoryID = cccm.subcategory
LEFT JOIN criteo_custom_url_mapping ccum ON cpe.sku = ccum.sku
LEFT JOIN criteo_custom_product_category_mapping ccpcm ON cpe.sku = ccpcm.sku
/*
left join 
( select distinct ccp.product_id from catalog_category_product ccp
where ccp.category_id = 2138 ) electronicsSub  ON cpe.entity_id = electronicsSub.product_id */
where cpei.value = 1 AND (cpei2.value = 2 OR cpei2.value = 4  OR (cpe_parent.type_id = 'configurable' AND (cpei4.value = 2 OR cpei4.value = 4))) AND cpe.type_id = "simple" AND IF(IF(csi.manage_stock+csi.use_config_manage_stock =0,0,1) = 0,1, IF(csi.qty = 0,0,1)) = 1
AND cpei5.value != 1
 -- only items that are enabled, visible OR children of visible configurable item, simple and in stock
 /*AND electronicsSub.product_id IS NULL and IFNULL(categorySub5.categoryID,0) NOT IN (5684,5685)  -- filter out electronics category*/