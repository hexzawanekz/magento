select cpe.entity_id AS "product_id", 
cpe.sku AS "sku", 
cpev2.value AS "product_name", 
cpev5.value AS "product_description",
FORMAT(cped.value,0) AS "price",
FORMAT(LEAST(IFNULL(IF(now()>=IFNULL(cped2.value, ADDDATE(now(), INTERVAL -99999 DAY)) && now()<= IFNULL(cped3.value, ADDDATE(now(), INTERVAL 999999 DAY)), cped4.value, cped5.value),cped5.value), cped5.value),0)  AS "discounted_price",
FORMAT(cped.value - LEAST(IFNULL(IF(now()>=IFNULL(cped2.value, ADDDATE(now(), INTERVAL -99999 DAY)) && now()<= IFNULL(cped3.value, ADDDATE(now(), INTERVAL 999999 DAY)), cped4.value, cped5.value),cped5.value), cped5.value),0) AS "discount_amount",
FORMAT(100*(1 - LEAST(IFNULL(IF(now()>=IFNULL(cped2.value, ADDDATE(now(), INTERVAL -99999 DAY)) && now()<= IFNULL(cped3.value, ADDDATE(now(), INTERVAL 999999 DAY)), cped4.value, cped5.value),cped5.value), cped5.value)/cped.value),0) AS "percentage_savings",
IFNULL(ccum.custom_url, CONCAT("http://www.orami.co.th/",cpev3.value)) AS "product_url", 
CONCAT("http://www.orami.co.th/media/catalog/product",REPLACE(CONCAT("/",cpev4.value),"//","/")) AS "image_url", 
cpei3.value AS "brand_id", 
categorySub2.categoryName AS "category_name_level2", 
IFNULL(cccm.custom_category_id, CASE categorySub2.categoryID WHEN 767 THEN 2 WHEN 2138 THEN 7 WHEN 2140 THEN 10 WHEN 582 THEN 12 WHEN 2355 THEN 14 WHEN 2139 THEN 25 WHEN 58 THEN 21 WHEN 3 THEN 19 END) AS "custom_category",
IFNULL(cccm.custom_category_name, CASE categorySub2.categoryID WHEN 767 THEN "Beauty_Men ; Rest" WHEN 2138 THEN "Electronics_Gadgets ; Rest" WHEN 2140 THEN "Fashion_Accessories ; Bags ; Watches ; Rest" WHEN 582 THEN "Health & Sports_Health Care ; Sports nutrition ; Weight loss ; Sports & Fitness ; Outdoors ; Rest" WHEN 2355 THEN "Home Appliances_ALL" WHEN 2139 THEN "Home Decor_Bathroom ; Decor Accessories ; Furniture ; Home Improve ; Outdoor&Garden ; Rest" WHEN 58 THEN "Mom & Baby_Maternity ; Rest" WHEN 3 THEN "Pets_Small Pets ; Rest" END) AS "custom_category_name",
eaov.value AS "brand_name"

from catalog_product_entity cpe 

left join catalog_product_entity_int cpei ON cpe.entity_id = cpei.entity_id AND cpei.attribute_id = 273 and cpei.store_id = 0 -- enabled status 
left join catalog_product_entity_int cpei2 ON cpe.entity_id = cpei2.entity_id AND cpei2.attribute_id = 526 AND cpei2.store_id = 0 -- visibility 
left join catalog_product_entity_int cpei3 ON cpe.entity_id = cpei3.entity_id AND cpei3.attribute_id = 1281  AND cpei3.store_id = 0 -- brand ID
left join catalog_product_entity_varchar cpev2 ON cpe.entity_id = cpev2.entity_id AND cpev2.attribute_id = 96 AND cpev2.store_id = 0 -- product name
left join catalog_product_entity_varchar cpev3 ON cpe.entity_id = cpev3.entity_id AND cpev3.attribute_id = 570 AND cpev3.store_id = 0 -- product url
left join catalog_product_entity_varchar cpev4 ON cpe.entity_id = cpev4.entity_id AND cpev4.attribute_id = 106 AND cpev4.store_id = 0 -- image url
left join catalog_product_entity_varchar cpev5 ON cpe.entity_id = cpev5.entity_id AND cpev5.attribute_id = 1058 AND cpev5.store_id = 0 -- product title
left join catalog_product_entity_decimal cped ON cpe.entity_id = cped.entity_id AND cped.attribute_id = 943 AND cped.store_id = 0 -- msrp
left join catalog_product_entity_datetime cped2 ON cpe.entity_id = cped2.entity_id AND cped2.attribute_id = 568 AND cped2.store_id = 0  -- special price from date -- needed for website price
left join catalog_product_entity_datetime cped3 ON cpe.entity_id = cped3.entity_id AND cped3.attribute_id = 569 AND cped3.store_id = 0 -- special price to date -- needed for website price
left join catalog_product_entity_decimal cped4 ON cpe.entity_id = cped4.entity_id AND cped4.attribute_id = 567 AND cped4.store_id = 0 -- special price -- needed for website price
left join catalog_product_entity_decimal cped5 ON cpe.entity_id = cped5.entity_id AND cped5.attribute_id = 99 AND cped5.store_id = 0 -- price -- needed for website price
left join eav_attribute_option_value eaov ON cpei3.value = eaov.option_id AND eaov.store_id = 0 -- brand name
left join cataloginventory_stock_item csi ON cpe.entity_id = csi.product_id -- inventory
left join catalog_product_super_link cpsl ON cpe.entity_id = cpsl.product_id -- product links
left join catalog_product_entity cpe_parent ON cpsl.parent_id = cpe_parent.entity_id -- parent info
left join catalog_product_entity_int cpei4 ON cpe_parent.entity_id = cpei4.entity_id AND cpei4.attribute_id = 526 AND cpei4.store_id = 0 -- visibility of parent 

-- level 2 category
left join 
(
select cpe.entity_id, ccp.category_id AS categoryID, ccev.value AS categoryName from catalog_product_entity cpe
left join catalog_category_product ccp ON  cpe.entity_id = ccp.product_id
left join catalog_category_entity cce ON ccp.category_id = cce.entity_id 
left join catalog_category_entity_varchar ccev ON ccp.category_id = ccev.entity_id AND ccev.store_id = 0 and ccev.attribute_id = 111
where cce.level = 2 AND ccev.value NOT IN ("Deal", "Welcome Splash", "Sanook Super Deal")
group by cpe.entity_id
) categorySub2 ON cpe.entity_id = categorySub2.entity_id

-- level 3 category
left join 
(
select cpe.entity_id, ccp.category_id AS categoryID, ccev.value AS categoryName, cce.parent_id AS parentID from catalog_product_entity cpe
left join catalog_category_product ccp ON  cpe.entity_id = ccp.product_id
left join catalog_category_entity cce ON ccp.category_id = cce.entity_id 
left join catalog_category_entity_varchar ccev ON ccp.category_id = ccev.entity_id AND ccev.store_id = 0 and ccev.attribute_id = 111
where cce.level = 3 AND ccev.value NOT IN ("All Deals", "Best Sellers", "Brands", "Deals", "New Arrivals", "New Products", "What's Hot")
group by cpe.entity_id
) categorySub3 ON cpe.entity_id = categorySub3.entity_id AND categorySub2.categoryID = categorySub3.parentID

-- custom category id

LEFT JOIN criteo_custom_category_mapping cccm ON categorySub2.categoryID = cccm.custom_vertical_id AND categorySub3.categoryID = cccm.subcategory
LEFT JOIN criteo_custom_url_mapping ccum ON cpe.sku = ccum.sku

where cpei.value = 1 AND (cpei2.value = 2 OR cpei2.value = 4  OR (cpe_parent.type_id = 'configurable' AND (cpei4.value = 2 OR cpei4.value = 4))) AND cpe.type_id = "simple" AND IF(IF(csi.manage_stock+csi.use_config_manage_stock =0,0,1) = 0,1, IF(csi.qty = 0,0,1)) = 1
AND IFNULL(cccm.custom_category_id, CASE categorySub2.categoryID WHEN 767 THEN 2 WHEN 2138 THEN 7 WHEN 2140 THEN 10 WHEN 582 THEN 12 WHEN 2355 THEN 14 WHEN 2139 THEN 25 WHEN 58 THEN 21 WHEN 3 THEN 19 END) IS NOT NULL
 -- only items that are enabled, visible OR children of visible configurable item, simple and in stock
