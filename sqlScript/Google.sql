select entity_id,url, count(if( (Appear_SKU )= 'Yes', appear_sku, null )) as "sku_available_count", case when count(if( (Appear_SKU )= 'Yes', appear_sku, null )) > 5 then 'Y' else 'N' end as "more_than_5"   from ( 
select entity_id,url,sku, case when type_of_product = 'simple' and status = 'Enabled' and visibility = 'Catalog, Search' and stock_availability_check = 1 then "Yes" else "No" end as "Appear_SKU" 
from (
select distinct
ccev4.entity_id,cpe.type_id AS "type_of_product",
CASE cpei.value WHEN 1 THEN "Enabled" WHEN 2 THEN "Disabled" ELSE null END AS "status",
CASE cpei2.value WHEN 1 THEN "Not Visible Individually" WHEN 2 THEN "Catalog" WHEN 3 THEN "Search" WHEN 4 THEN "Catalog, Search" ELSE null END  AS "visibility", 
cpe.sku,IF(IF(csi.manage_stock+csi.use_config_manage_stock =0,0,1) = 0,1, IF(csi.qty = 0,0,1)) AS "stock_availability_check",
CONCAT("https://www.orami.co.th/th/",ccev4.value)  AS "url"
from catalog_product_entity cpe 
left join catalog_category_product ccp ON  cpe.entity_id = ccp.product_id  
left join catalog_category_entity cce ON ccp.category_id = cce.entity_id 
left join catalog_category_entity_varchar ccev ON ccp.category_id = ccev.entity_id AND ccev.store_id = 0 and ccev.attribute_id = 111
left join catalog_category_product ccp2 ON  cpe.entity_id = ccp2.product_id  
left join catalog_category_entity cce2 ON ccp2.category_id = cce2.entity_id 
left join catalog_category_entity_varchar ccev2 ON ccp2.category_id = ccev2.entity_id AND ccev2.store_id = 0 and ccev2.attribute_id = 111
left join catalog_category_product ccp3 ON  cpe.entity_id = ccp3.product_id  
left join catalog_category_entity cce3 ON ccp3.category_id = cce3.entity_id 
left join catalog_category_entity_varchar ccev3 ON ccp3.category_id = ccev3.entity_id AND ccev3.store_id = 0 and ccev3.attribute_id = 533
left join catalog_category_product ccp4 ON  cpe.entity_id = ccp4.product_id  
left join catalog_category_entity cce4 ON ccp3.category_id = cce4.entity_id 
left join catalog_category_entity_varchar ccev4 ON ccp4.category_id = ccev4.entity_id AND ccev4.store_id = 0 and ccev4.attribute_id = 533
left join cataloginventory_stock_item csi ON cpe.entity_id = csi.product_id
left join catalog_product_entity_int cpei ON cpe.entity_id = cpei.entity_id AND cpei.attribute_id = 273 and cpei.store_id = 0 -- enabled status
left join catalog_product_entity_int cpei2 ON cpe.entity_id = cpei2.entity_id AND cpei2.attribute_id = 526 AND cpei2.store_id = 0 -- visibility  
where cce.level = 2 
and cce2.level = 3 
and cce3.level = 4
AND cce2.parent_id = cce.entity_id
AND cce3.parent_id = cce2.entity_id 
and cpe.sku is not null
order by cpe.sku)A order by entity_id desc)B where entity_id is not null group by entity_id,url;
