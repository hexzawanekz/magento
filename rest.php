<?php
/**
 * Copyright Magento 2012
 * Example of products list retrieve using Customer account via Magento
REST API. OAuth authorization is used
 */
$callbackUrl = "https://stage.orami.co.th/rest.php";
$temporaryCredentialsRequestUrl =
"https://stage.orami.co.th/oauth/initiate?oauth_callback=" .
urlencode($callbackUrl);
$adminAuthorizationUrl = 'https://stage.orami.co.th/admintools/oauth_authorize';
$accessTokenRequestUrl = 'https://stage.orami.co.th/oauth/token';
$apiUrl = 'https://stage.orami.co.th/api/rest';
$consumerKey = '6c5jtgky7hzzvt0fmb6h3k6xwpodnus4';
$consumerSecret = 'rbd456kgh0bupgr575zvjq66iidhj1r0';
session_start();
if (!isset($_GET['oauth_token']) && isset($_SESSION['state']) &&
$_SESSION['state'] == 1) {
   $_SESSION['state'] = 0;
}
try {
   $authType = ($_SESSION['state'] == 2) ? OAUTH_AUTH_TYPE_AUTHORIZATION
: OAUTH_AUTH_TYPE_URI;
   $oauthClient = new OAuth($consumerKey, $consumerSecret,
OAUTH_SIG_METHOD_HMACSHA1, $authType);
   $oauthClient->enableDebug();
   if (!isset($_GET['oauth_token']) && !$_SESSION['state']) {
       $requestToken =
$oauthClient->getRequestToken($temporaryCredentialsRequestUrl);
       $_SESSION['secret'] = $requestToken['oauth_token_secret'];
       $_SESSION['state'] = 1;
       header('Location: ' . $adminAuthorizationUrl . '?oauth_token=' .
$requestToken['oauth_token']);
       exit;
   } else if ($_SESSION['state'] == 1) {
       $oauthClient->setToken($_GET['oauth_token'], $_SESSION['secret']);
       $accessToken =
$oauthClient->getAccessToken($accessTokenRequestUrl);
       $_SESSION['state'] = 2;
       $_SESSION['token'] = $accessToken['oauth_token'];
       $_SESSION['secret'] = $accessToken['oauth_token_secret'];
       header('Location: ' . $callbackUrl);
       exit;
   } else {
       $oauthClient->setToken($_SESSION['token'], $_SESSION['secret']);
       $resourceUrl = "$apiUrl/products";
       $oauthClient->fetch($resourceUrl);
       $productsList = json_decode($oauthClient->getLastResponse());
       print_r($productsList);
   }
} catch (OAuthException $e) {
   print_r($e);
}
