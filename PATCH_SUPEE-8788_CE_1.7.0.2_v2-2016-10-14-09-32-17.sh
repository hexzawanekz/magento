#!/bin/bash
# Patch apllying tool template
# v0.1.2
# (c) Copyright 2013. Magento Inc.
#
# DO NOT CHANGE ANY LINE IN THIS FILE.

# 1. Check required system tools
_check_installed_tools() {
    local missed=""

    until [ -z "$1" ]; do
        type -t $1 >/dev/null 2>/dev/null
        if (( $? != 0 )); then
            missed="$missed $1"
        fi
        shift
    done

    echo $missed
}

REQUIRED_UTILS='sed patch'
MISSED_REQUIRED_TOOLS=`_check_installed_tools $REQUIRED_UTILS`
if (( `echo $MISSED_REQUIRED_TOOLS | wc -w` > 0 ));
then
    echo -e "Error! Some required system tools, that are utilized in this sh script, are not installed:\nTool(s) \"$MISSED_REQUIRED_TOOLS\" is(are) missed, please install it(them)."
    exit 1
fi

# 2. Determine bin path for system tools
CAT_BIN=`which cat`
PATCH_BIN=`which patch`
SED_BIN=`which sed`
PWD_BIN=`which pwd`
BASENAME_BIN=`which basename`

BASE_NAME=`$BASENAME_BIN "$0"`

# 3. Help menu
if [ "$1" = "-?" -o "$1" = "-h" -o "$1" = "--help" ]
then
    $CAT_BIN << EOFH
Usage: sh $BASE_NAME [--help] [-R|--revert] [--list]
Apply embedded patch.

-R, --revert    Revert previously applied embedded patch
--list          Show list of applied patches
--help          Show this help message
EOFH
    exit 0
fi

# 4. Get "revert" flag and "list applied patches" flag
REVERT_FLAG=
SHOW_APPLIED_LIST=0
if [ "$1" = "-R" -o "$1" = "--revert" ]
then
    REVERT_FLAG=-R
fi
if [ "$1" = "--list" ]
then
    SHOW_APPLIED_LIST=1
fi

# 5. File pathes
CURRENT_DIR=`$PWD_BIN`/
APP_ETC_DIR=`echo "$CURRENT_DIR""app/etc/"`
APPLIED_PATCHES_LIST_FILE=`echo "$APP_ETC_DIR""applied.patches.list"`

# 6. Show applied patches list if requested
if [ "$SHOW_APPLIED_LIST" -eq 1 ] ; then
    echo -e "Applied/reverted patches list:"
    if [ -e "$APPLIED_PATCHES_LIST_FILE" ]
    then
        if [ ! -r "$APPLIED_PATCHES_LIST_FILE" ]
        then
            echo "ERROR: \"$APPLIED_PATCHES_LIST_FILE\" must be readable so applied patches list can be shown."
            exit 1
        else
            $SED_BIN -n "/SUP-\|SUPEE-/p" $APPLIED_PATCHES_LIST_FILE
        fi
    else
        echo "<empty>"
    fi
    exit 0
fi

# 7. Check applied patches track file and its directory
_check_files() {
    if [ ! -e "$APP_ETC_DIR" ]
    then
        echo "ERROR: \"$APP_ETC_DIR\" must exist for proper tool work."
        exit 1
    fi

    if [ ! -w "$APP_ETC_DIR" ]
    then
        echo "ERROR: \"$APP_ETC_DIR\" must be writeable for proper tool work."
        exit 1
    fi

    if [ -e "$APPLIED_PATCHES_LIST_FILE" ]
    then
        if [ ! -w "$APPLIED_PATCHES_LIST_FILE" ]
        then
            echo "ERROR: \"$APPLIED_PATCHES_LIST_FILE\" must be writeable for proper tool work."
            exit 1
        fi
    fi
}

_check_files

# 8. Apply/revert patch
# Note: there is no need to check files permissions for files to be patched.
# "patch" tool will not modify any file if there is not enough permissions for all files to be modified.
# Get start points for additional information and patch data
SKIP_LINES=$((`$SED_BIN -n "/^__PATCHFILE_FOLLOWS__$/=" "$CURRENT_DIR""$BASE_NAME"` + 1))
ADDITIONAL_INFO_LINE=$(($SKIP_LINES - 3))p

_apply_revert_patch() {
    DRY_RUN_FLAG=
    if [ "$1" = "dry-run" ]
    then
        DRY_RUN_FLAG=" --dry-run"
        echo "Checking if patch can be applied/reverted successfully..."
    fi
    PATCH_APPLY_REVERT_RESULT=`$SED_BIN -e '1,/^__PATCHFILE_FOLLOWS__$/d' "$CURRENT_DIR""$BASE_NAME" | $PATCH_BIN $DRY_RUN_FLAG $REVERT_FLAG -p0`
    PATCH_APPLY_REVERT_STATUS=$?
    if [ $PATCH_APPLY_REVERT_STATUS -eq 1 ] ; then
        echo -e "ERROR: Patch can't be applied/reverted successfully.\n\n$PATCH_APPLY_REVERT_RESULT"
        exit 1
    fi
    if [ $PATCH_APPLY_REVERT_STATUS -eq 2 ] ; then
        echo -e "ERROR: Patch can't be applied/reverted successfully."
        exit 2
    fi
}

REVERTED_PATCH_MARK=
if [ -n "$REVERT_FLAG" ]
then
    REVERTED_PATCH_MARK=" | REVERTED"
fi

_apply_revert_patch dry-run
_apply_revert_patch

# 9. Track patch applying result
echo "Patch was applied/reverted successfully."
ADDITIONAL_INFO=`$SED_BIN -n ""$ADDITIONAL_INFO_LINE"" "$CURRENT_DIR""$BASE_NAME"`
APPLIED_REVERTED_ON_DATE=`date -u +"%F %T UTC"`
APPLIED_REVERTED_PATCH_INFO=`echo -n "$APPLIED_REVERTED_ON_DATE"" | ""$ADDITIONAL_INFO""$REVERTED_PATCH_MARK"`
echo -e "$APPLIED_REVERTED_PATCH_INFO\n$PATCH_APPLY_REVERT_RESULT\n\n" >> "$APPLIED_PATCHES_LIST_FILE"

exit 0


SUPEE-8788 | CE_1.7.0.2 | v2 | 914ffb3a5cd3b84cb7cdfaac53623dbc25e803be | Fri Oct 14 19:24:41 2016 +0300 | a63ab79dcd75012590a2b4a34ac9d1d54607daff

__PATCHFILE_FOLLOWS__
diff --git app/code/core/Mage/Adminhtml/Block/Catalog/Product/Helper/Form/Gallery/Content.php app/code/core/Mage/Adminhtml/Block/Catalog/Product/Helper/Form/Gallery/Content.php
index effc12e..19f15f4 100644
--- app/code/core/Mage/Adminhtml/Block/Catalog/Product/Helper/Form/Gallery/Content.php
+++ app/code/core/Mage/Adminhtml/Block/Catalog/Product/Helper/Form/Gallery/Content.php
@@ -34,6 +34,12 @@
  */
 class Mage_Adminhtml_Block_Catalog_Product_Helper_Form_Gallery_Content extends Mage_Adminhtml_Block_Widget
 {
+    /**
+     * Type of uploader block
+     *
+     * @var string
+     */
+    protected $_uploaderType = 'uploader/multiple';
 
     public function __construct()
     {
@@ -44,17 +50,17 @@ class Mage_Adminhtml_Block_Catalog_Product_Helper_Form_Gallery_Content extends M
     protected function _prepareLayout()
     {
         $this->setChild('uploader',
-            $this->getLayout()->createBlock('adminhtml/media_uploader')
+            $this->getLayout()->createBlock($this->_uploaderType)
         );
 
-        $this->getUploader()->getConfig()
-            ->setUrl(Mage::getModel('adminhtml/url')->addSessionParam()->getUrl('*/catalog_product_gallery/upload'))
-            ->setFileField('image')
-            ->setFilters(array(
-                'images' => array(
-                    'label' => Mage::helper('adminhtml')->__('Images (.gif, .jpg, .png)'),
-                    'files' => array('*.gif', '*.jpg','*.jpeg', '*.png')
-                )
+        $this->getUploader()->getUploaderConfig()
+            ->setFileParameterName('image')
+            ->setTarget(Mage::getModel('adminhtml/url')->addSessionParam()->getUrl('*/catalog_product_gallery/upload'));
+
+        $browseConfig = $this->getUploader()->getButtonConfig();
+        $browseConfig
+            ->setAttributes(array(
+                'accept' => $browseConfig->getMimeTypesByExtensions('gif, png, jpeg, jpg')
             ));
 
         Mage::dispatchEvent('catalog_product_gallery_prepare_layout', array('block' => $this));
@@ -65,7 +71,7 @@ class Mage_Adminhtml_Block_Catalog_Product_Helper_Form_Gallery_Content extends M
     /**
      * Retrive uploader block
      *
-     * @return Mage_Adminhtml_Block_Media_Uploader
+     * @return Mage_Uploader_Block_Multiple
      */
     public function getUploader()
     {
diff --git app/code/core/Mage/Adminhtml/Block/Cms/Wysiwyg/Images/Content/Uploader.php app/code/core/Mage/Adminhtml/Block/Cms/Wysiwyg/Images/Content/Uploader.php
index b9b7376..82a33c0 100644
--- app/code/core/Mage/Adminhtml/Block/Cms/Wysiwyg/Images/Content/Uploader.php
+++ app/code/core/Mage/Adminhtml/Block/Cms/Wysiwyg/Images/Content/Uploader.php
@@ -31,29 +31,24 @@
  * @package    Mage_Adminhtml
  * @author     Magento Core Team <core@magentocommerce.com>
 */
-class Mage_Adminhtml_Block_Cms_Wysiwyg_Images_Content_Uploader extends Mage_Adminhtml_Block_Media_Uploader
+class Mage_Adminhtml_Block_Cms_Wysiwyg_Images_Content_Uploader extends Mage_Uploader_Block_Multiple
 {
+    /**
+     * Uploader block constructor
+     */
     public function __construct()
     {
         parent::__construct();
-        $params = $this->getConfig()->getParams();
         $type = $this->_getMediaType();
         $allowed = Mage::getSingleton('cms/wysiwyg_images_storage')->getAllowedExtensions($type);
-        $labels = array();
-        $files = array();
-        foreach ($allowed as $ext) {
-            $labels[] = '.' . $ext;
-            $files[] = '*.' . $ext;
-        }
-        $this->getConfig()
-            ->setUrl(Mage::getModel('adminhtml/url')->addSessionParam()->getUrl('*/*/upload', array('type' => $type)))
-            ->setParams($params)
-            ->setFileField('image')
-            ->setFilters(array(
-                'images' => array(
-                    'label' => $this->helper('cms')->__('Images (%s)', implode(', ', $labels)),
-                    'files' => $files
-                )
+        $this->getUploaderConfig()
+            ->setFileParameterName('image')
+            ->setTarget(
+                Mage::getModel('adminhtml/url')->addSessionParam()->getUrl('*/*/upload', array('type' => $type))
+            );
+        $this->getButtonConfig()
+            ->setAttributes(array(
+                'accept' => $this->getButtonConfig()->getMimeTypesByExtensions($allowed)
             ));
     }
 
diff --git app/code/core/Mage/Adminhtml/Block/Dashboard/Graph.php app/code/core/Mage/Adminhtml/Block/Dashboard/Graph.php
index 3cc1121..454e0e2 100644
--- app/code/core/Mage/Adminhtml/Block/Dashboard/Graph.php
+++ app/code/core/Mage/Adminhtml/Block/Dashboard/Graph.php
@@ -444,7 +444,7 @@ class Mage_Adminhtml_Block_Dashboard_Graph extends Mage_Adminhtml_Block_Dashboar
             }
             return self::API_URL . '?' . implode('&', $p);
         } else {
-            $gaData = urlencode(base64_encode(serialize($params)));
+            $gaData = urlencode(base64_encode(json_encode($params)));
             $gaHash = Mage::helper('adminhtml/dashboard_data')->getChartDataHash($gaData);
             $params = array('ga' => $gaData, 'h' => $gaHash);
             return $this->getUrl('*/*/tunnel', array('_query' => $params));
diff --git app/code/core/Mage/Adminhtml/Block/Media/Uploader.php app/code/core/Mage/Adminhtml/Block/Media/Uploader.php
index 033ece1..1f1d0fc 100644
--- app/code/core/Mage/Adminhtml/Block/Media/Uploader.php
+++ app/code/core/Mage/Adminhtml/Block/Media/Uploader.php
@@ -31,189 +31,20 @@
  * @package    Mage_Adminhtml
  * @author      Magento Core Team <core@magentocommerce.com>
  */
-class Mage_Adminhtml_Block_Media_Uploader extends Mage_Adminhtml_Block_Widget
-{
-
-    protected $_config;
-
-    public function __construct()
-    {
-        parent::__construct();
-        $this->setId($this->getId() . '_Uploader');
-        $this->setTemplate('media/uploader.phtml');
-        $this->getConfig()->setUrl(Mage::getModel('adminhtml/url')->addSessionParam()->getUrl('*/*/upload'));
-        $this->getConfig()->setParams(array('form_key' => $this->getFormKey()));
-        $this->getConfig()->setFileField('file');
-        $this->getConfig()->setFilters(array(
-            'images' => array(
-                'label' => Mage::helper('adminhtml')->__('Images (.gif, .jpg, .png)'),
-                'files' => array('*.gif', '*.jpg', '*.png')
-            ),
-            'media' => array(
-                'label' => Mage::helper('adminhtml')->__('Media (.avi, .flv, .swf)'),
-                'files' => array('*.avi', '*.flv', '*.swf')
-            ),
-            'all'    => array(
-                'label' => Mage::helper('adminhtml')->__('All Files'),
-                'files' => array('*.*')
-            )
-        ));
-    }
-
-    protected function _prepareLayout()
-    {
-        $this->setChild(
-            'browse_button',
-            $this->getLayout()->createBlock('adminhtml/widget_button')
-                ->addData(array(
-                    'id'      => $this->_getButtonId('browse'),
-                    'label'   => Mage::helper('adminhtml')->__('Browse Files...'),
-                    'type'    => 'button',
-                    'onclick' => $this->getJsObjectName() . '.browse()'
-                ))
-        );
-
-        $this->setChild(
-            'upload_button',
-            $this->getLayout()->createBlock('adminhtml/widget_button')
-                ->addData(array(
-                    'id'      => $this->_getButtonId('upload'),
-                    'label'   => Mage::helper('adminhtml')->__('Upload Files'),
-                    'type'    => 'button',
-                    'onclick' => $this->getJsObjectName() . '.upload()'
-                ))
-        );
-
-        $this->setChild(
-            'delete_button',
-            $this->getLayout()->createBlock('adminhtml/widget_button')
-                ->addData(array(
-                    'id'      => '{{id}}-delete',
-                    'class'   => 'delete',
-                    'type'    => 'button',
-                    'label'   => Mage::helper('adminhtml')->__('Remove'),
-                    'onclick' => $this->getJsObjectName() . '.removeFile(\'{{fileId}}\')'
-                ))
-        );
-
-        return parent::_prepareLayout();
-    }
-
-    protected function _getButtonId($buttonName)
-    {
-        return $this->getHtmlId() . '-' . $buttonName;
-    }
-
-    public function getBrowseButtonHtml()
-    {
-        return $this->getChildHtml('browse_button');
-    }
-
-    public function getUploadButtonHtml()
-    {
-        return $this->getChildHtml('upload_button');
-    }
-
-    public function getDeleteButtonHtml()
-    {
-        return $this->getChildHtml('delete_button');
-    }
-
-    /**
-     * Retrive uploader js object name
-     *
-     * @return string
-     */
-    public function getJsObjectName()
-    {
-        return $this->getHtmlId() . 'JsObject';
-    }
-
-    /**
-     * Retrive config json
-     *
-     * @return string
-     */
-    public function getConfigJson()
-    {
-        return Mage::helper('core')->jsonEncode($this->getConfig()->getData());
-    }
-
-    /**
-     * Retrive config object
-     *
-     * @return Varien_Config
-     */
-    public function getConfig()
-    {
-        if(is_null($this->_config)) {
-            $this->_config = new Varien_Object();
-        }
-
-        return $this->_config;
-    }
-
-    public function getPostMaxSize()
-    {
-        return ini_get('post_max_size');
-    }
-
-    public function getUploadMaxSize()
-    {
-        return ini_get('upload_max_filesize');
-    }
-
-    public function getDataMaxSize()
-    {
-        return min($this->getPostMaxSize(), $this->getUploadMaxSize());
-    }
-
-    public function getDataMaxSizeInBytes()
-    {
-        $iniSize = $this->getDataMaxSize();
-        $size = substr($iniSize, 0, strlen($iniSize)-1);
-        $parsedSize = 0;
-        switch (strtolower(substr($iniSize, strlen($iniSize)-1))) {
-            case 't':
-                $parsedSize = $size*(1024*1024*1024*1024);
-                break;
-            case 'g':
-                $parsedSize = $size*(1024*1024*1024);
-                break;
-            case 'm':
-                $parsedSize = $size*(1024*1024);
-                break;
-            case 'k':
-                $parsedSize = $size*1024;
-                break;
-            case 'b':
-            default:
-                $parsedSize = $size;
-                break;
-        }
-        return $parsedSize;
-    }
 
+/**
+ * @deprecated
+ * Class Mage_Adminhtml_Block_Media_Uploader
+ */
+class Mage_Adminhtml_Block_Media_Uploader extends Mage_Uploader_Block_Multiple
+{
     /**
-     * Retrieve full uploader SWF's file URL
-     * Implemented to solve problem with cross domain SWFs
-     * Now uploader can be only in the same URL where backend located
-     *
-     * @param string $url url to uploader in current theme
-     *
-     * @return string full URL
+     * Constructor for uploader block
      */
-    public function getUploaderUrl($url)
+    public function __construct()
     {
-        if (!is_string($url)) {
-            $url = '';
-        }
-        $design = Mage::getDesign();
-        $theme = $design->getTheme('skin');
-        if (empty($url) || !$design->validateFile($url, array('_type' => 'skin', '_theme' => $theme))) {
-            $theme = $design->getDefaultTheme();
-        }
-        return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB) . 'skin/' .
-            $design->getArea() . '/' . $design->getPackageName() . '/' . $theme . '/' . $url;
+        parent::__construct();
+        $this->getUploaderConfig()->setTarget(Mage::getModel('adminhtml/url')->addSessionParam()->getUrl('*/*/upload'));
+        $this->getUploaderConfig()->setFileParameterName('file');
     }
 }
diff --git app/code/core/Mage/Adminhtml/Block/Urlrewrite/Category/Tree.php app/code/core/Mage/Adminhtml/Block/Urlrewrite/Category/Tree.php
index 8483bf6..5354be9 100644
--- app/code/core/Mage/Adminhtml/Block/Urlrewrite/Category/Tree.php
+++ app/code/core/Mage/Adminhtml/Block/Urlrewrite/Category/Tree.php
@@ -119,7 +119,7 @@ class Mage_Adminhtml_Block_Urlrewrite_Category_Tree extends Mage_Adminhtml_Block
             'parent_id'      => (int)$node->getParentId(),
             'children_count' => (int)$node->getChildrenCount(),
             'is_active'      => (bool)$node->getIsActive(),
-            'name'           => $node->getName(),
+            'name'           => $this->escapeHtml($node->getName()),
             'level'          => (int)$node->getLevel(),
             'product_count'  => (int)$node->getProductCount()
         );
diff --git app/code/core/Mage/Adminhtml/Model/System/Config/Backend/Serialized.php app/code/core/Mage/Adminhtml/Model/System/Config/Backend/Serialized.php
index 3355f17..8465a50 100644
--- app/code/core/Mage/Adminhtml/Model/System/Config/Backend/Serialized.php
+++ app/code/core/Mage/Adminhtml/Model/System/Config/Backend/Serialized.php
@@ -29,8 +29,17 @@ class Mage_Adminhtml_Model_System_Config_Backend_Serialized extends Mage_Core_Mo
     protected function _afterLoad()
     {
         if (!is_array($this->getValue())) {
-            $value = $this->getValue();
-            $this->setValue(empty($value) ? false : unserialize($value));
+            $serializedValue = $this->getValue();
+            $unserializedValue = false;
+            if (!empty($serializedValue)) {
+                try {
+                    $unserializedValue = Mage::helper('core/unserializeArray')
+                        ->unserialize($serializedValue);
+                } catch (Exception $e) {
+                    Mage::logException($e);
+                }
+            }
+            $this->setValue($unserializedValue);
         }
     }
 
diff --git app/code/core/Mage/Adminhtml/controllers/DashboardController.php app/code/core/Mage/Adminhtml/controllers/DashboardController.php
index 8afc56e..a67c452 100644
--- app/code/core/Mage/Adminhtml/controllers/DashboardController.php
+++ app/code/core/Mage/Adminhtml/controllers/DashboardController.php
@@ -91,8 +91,9 @@ class Mage_Adminhtml_DashboardController extends Mage_Adminhtml_Controller_Actio
         $gaHash = $this->getRequest()->getParam('h');
         if ($gaData && $gaHash) {
             $newHash = Mage::helper('adminhtml/dashboard_data')->getChartDataHash($gaData);
-            if ($newHash == $gaHash) {
-                if ($params = unserialize(base64_decode(urldecode($gaData)))) {
+            if (hash_equals($newHash, $gaHash)) {
+                $params = json_decode(base64_decode(urldecode($gaData)), true);
+                if ($params) {
                     $response = $httpClient->setUri(Mage_Adminhtml_Block_Dashboard_Graph::API_URL)
                             ->setParameterGet($params)
                             ->setConfig(array('timeout' => 5))
diff --git app/code/core/Mage/Adminhtml/controllers/IndexController.php app/code/core/Mage/Adminhtml/controllers/IndexController.php
index 724dd73..4565d93 100644
--- app/code/core/Mage/Adminhtml/controllers/IndexController.php
+++ app/code/core/Mage/Adminhtml/controllers/IndexController.php
@@ -392,7 +392,7 @@ class Mage_Adminhtml_IndexController extends Mage_Adminhtml_Controller_Action
         }
 
         $userToken = $user->getRpToken();
-        if (strcmp($userToken, $resetPasswordLinkToken) != 0 || $user->isResetPasswordLinkTokenExpired()) {
+        if (!hash_equals($userToken, $resetPasswordLinkToken) || $user->isResetPasswordLinkTokenExpired()) {
             throw Mage::exception('Mage_Core', Mage::helper('adminhtml')->__('Your password reset link has expired.'));
         }
     }
diff --git app/code/core/Mage/Adminhtml/controllers/Media/UploaderController.php app/code/core/Mage/Adminhtml/controllers/Media/UploaderController.php
index 940e97c..6dd6380 100644
--- app/code/core/Mage/Adminhtml/controllers/Media/UploaderController.php
+++ app/code/core/Mage/Adminhtml/controllers/Media/UploaderController.php
@@ -43,7 +43,7 @@ class Mage_Adminhtml_Media_UploaderController extends Mage_Adminhtml_Controller_
     {
         $this->loadLayout();
         $this->_addContent(
-            $this->getLayout()->createBlock('adminhtml/media_uploader')
+            $this->getLayout()->createBlock('uploader/multiple')
         );
         $this->renderLayout();
     }
diff --git app/code/core/Mage/Catalog/Block/Product/Abstract.php app/code/core/Mage/Catalog/Block/Product/Abstract.php
index a4728a5..7275a1e 100644
--- app/code/core/Mage/Catalog/Block/Product/Abstract.php
+++ app/code/core/Mage/Catalog/Block/Product/Abstract.php
@@ -34,6 +34,11 @@
  */
 abstract class Mage_Catalog_Block_Product_Abstract extends Mage_Core_Block_Template
 {
+    /**
+     * Price block array
+     *
+     * @var array
+     */
     protected $_priceBlock = array();
 
     /**
@@ -43,10 +48,25 @@ abstract class Mage_Catalog_Block_Product_Abstract extends Mage_Core_Block_Templ
      */
     protected $_block = 'catalog/product_price';
 
+    /**
+     * Price template
+     *
+     * @var string
+     */
     protected $_priceBlockDefaultTemplate = 'catalog/product/price.phtml';
 
+    /**
+     * Tier price template
+     *
+     * @var string
+     */
     protected $_tierPriceDefaultTemplate  = 'catalog/product/view/tierprices.phtml';
 
+    /**
+     * Price types
+     *
+     * @var array
+     */
     protected $_priceBlockTypes = array();
 
     /**
@@ -56,6 +76,11 @@ abstract class Mage_Catalog_Block_Product_Abstract extends Mage_Core_Block_Templ
      */
     protected $_useLinkForAsLowAs = true;
 
+    /**
+     * Review block instance
+     *
+     * @var null|Mage_Review_Block_Helper
+     */
     protected $_reviewsHelperBlock;
 
     /**
@@ -89,18 +114,33 @@ abstract class Mage_Catalog_Block_Product_Abstract extends Mage_Core_Block_Templ
      */
     public function getAddToCartUrl($product, $additional = array())
     {
-        if ($product->getTypeInstance(true)->hasRequiredOptions($product)) {
-            if (!isset($additional['_escape'])) {
-                $additional['_escape'] = true;
-            }
-            if (!isset($additional['_query'])) {
-                $additional['_query'] = array();
-            }
-            $additional['_query']['options'] = 'cart';
-
-            return $this->getProductUrl($product, $additional);
+        if (!$product->getTypeInstance(true)->hasRequiredOptions($product)) {
+            return $this->helper('checkout/cart')->getAddUrl($product, $additional);
         }
-        return $this->helper('checkout/cart')->getAddUrl($product, $additional);
+        $additional = array_merge(
+            $additional,
+            array(Mage_Core_Model_Url::FORM_KEY => $this->_getSingletonModel('core/session')->getFormKey())
+        );
+        if (!isset($additional['_escape'])) {
+            $additional['_escape'] = true;
+        }
+        if (!isset($additional['_query'])) {
+            $additional['_query'] = array();
+        }
+        $additional['_query']['options'] = 'cart';
+        return $this->getProductUrl($product, $additional);
+    }
+
+    /**
+     * Return model instance
+     *
+     * @param string $className
+     * @param array $arguments
+     * @return Mage_Core_Model_Abstract
+     */
+    protected function _getSingletonModel($className, $arguments = array())
+    {
+        return Mage::getSingleton($className, $arguments);
     }
 
     /**
@@ -126,7 +166,7 @@ abstract class Mage_Catalog_Block_Product_Abstract extends Mage_Core_Block_Templ
     }
 
     /**
-     * Enter description here...
+     * Return link to Add to Wishlist
      *
      * @param Mage_Catalog_Model_Product $product
      * @return string
@@ -155,6 +195,12 @@ abstract class Mage_Catalog_Block_Product_Abstract extends Mage_Core_Block_Templ
         return null;
     }
 
+    /**
+     * Return price block
+     *
+     * @param string $productTypeId
+     * @return mixed
+     */
     protected function _getPriceBlock($productTypeId)
     {
         if (!isset($this->_priceBlock[$productTypeId])) {
@@ -169,6 +215,12 @@ abstract class Mage_Catalog_Block_Product_Abstract extends Mage_Core_Block_Templ
         return $this->_priceBlock[$productTypeId];
     }
 
+    /**
+     * Return Block template
+     *
+     * @param string $productTypeId
+     * @return string
+     */
     protected function _getPriceBlockTemplate($productTypeId)
     {
         if (isset($this->_priceBlockTypes[$productTypeId])) {
@@ -304,6 +356,11 @@ abstract class Mage_Catalog_Block_Product_Abstract extends Mage_Core_Block_Templ
         return $this->getData('product');
     }
 
+    /**
+     * Return tier price template
+     *
+     * @return mixed|string
+     */
     public function getTierPriceTemplate()
     {
         if (!$this->hasData('tier_price_template')) {
@@ -419,13 +476,13 @@ abstract class Mage_Catalog_Block_Product_Abstract extends Mage_Core_Block_Templ
      *
      * @return string
      */
-    public function getImageLabel($product=null, $mediaAttributeCode='image')
+    public function getImageLabel($product = null, $mediaAttributeCode = 'image')
     {
         if (is_null($product)) {
             $product = $this->getProduct();
         }
 
-        $label = $product->getData($mediaAttributeCode.'_label');
+        $label = $product->getData($mediaAttributeCode . '_label');
         if (empty($label)) {
             $label = $product->getName();
         }
diff --git app/code/core/Mage/Catalog/Block/Product/View.php app/code/core/Mage/Catalog/Block/Product/View.php
index f641f24..bc81fd7 100644
--- app/code/core/Mage/Catalog/Block/Product/View.php
+++ app/code/core/Mage/Catalog/Block/Product/View.php
@@ -61,7 +61,7 @@ class Mage_Catalog_Block_Product_View extends Mage_Catalog_Block_Product_Abstrac
             $currentCategory = Mage::registry('current_category');
             if ($keyword) {
                 $headBlock->setKeywords($keyword);
-            } elseif($currentCategory) {
+            } elseif ($currentCategory) {
                 $headBlock->setKeywords($product->getName());
             }
             $description = $product->getMetaDescription();
@@ -71,7 +71,7 @@ class Mage_Catalog_Block_Product_View extends Mage_Catalog_Block_Product_Abstrac
                 $headBlock->setDescription(Mage::helper('core/string')->substr($product->getDescription(), 0, 255));
             }
             if ($this->helper('catalog/product')->canUseCanonicalTag()) {
-                $params = array('_ignore_category'=>true);
+                $params = array('_ignore_category' => true);
                 $headBlock->addLinkRel('canonical', $product->getUrlModel()->getUrl($product, $params));
             }
         }
@@ -117,7 +117,7 @@ class Mage_Catalog_Block_Product_View extends Mage_Catalog_Block_Product_Abstrac
             return $this->getCustomAddToCartUrl();
         }
 
-        if ($this->getRequest()->getParam('wishlist_next')){
+        if ($this->getRequest()->getParam('wishlist_next')) {
             $additional['wishlist_next'] = 1;
         }
 
@@ -191,9 +191,9 @@ class Mage_Catalog_Block_Product_View extends Mage_Catalog_Block_Product_Abstrac
         );
 
         $responseObject = new Varien_Object();
-        Mage::dispatchEvent('catalog_product_view_config', array('response_object'=>$responseObject));
+        Mage::dispatchEvent('catalog_product_view_config', array('response_object' => $responseObject));
         if (is_array($responseObject->getAdditionalOptions())) {
-            foreach ($responseObject->getAdditionalOptions() as $option=>$value) {
+            foreach ($responseObject->getAdditionalOptions() as $option => $value) {
                 $config[$option] = $value;
             }
         }
diff --git app/code/core/Mage/Catalog/Helper/Image.php app/code/core/Mage/Catalog/Helper/Image.php
index cafed95..02afd32 100644
--- app/code/core/Mage/Catalog/Helper/Image.php
+++ app/code/core/Mage/Catalog/Helper/Image.php
@@ -31,6 +31,8 @@
  */
 class Mage_Catalog_Helper_Image extends Mage_Core_Helper_Abstract
 {
+    const XML_NODE_PRODUCT_MAX_DIMENSION = 'catalog/product_image/max_dimension';
+
     /**
      * Current model
      *
@@ -631,10 +633,16 @@ class Mage_Catalog_Helper_Image extends Mage_Core_Helper_Abstract
      * @throws Mage_Core_Exception
      */
     public function validateUploadFile($filePath) {
-        if (!getimagesize($filePath)) {
+        $maxDimension = Mage::getStoreConfig(self::XML_NODE_PRODUCT_MAX_DIMENSION);
+        $imageInfo = getimagesize($filePath);
+        if (!$imageInfo) {
             Mage::throwException($this->__('Disallowed file type.'));
         }
 
+        if ($imageInfo[0] > $maxDimension || $imageInfo[1] > $maxDimension) {
+            Mage::throwException($this->__('Disalollowed file format.'));
+        }
+
         $_processor = new Varien_Image($filePath);
         return $_processor->getMimeType() !== null;
     }
diff --git app/code/core/Mage/Catalog/Helper/Product/Compare.php app/code/core/Mage/Catalog/Helper/Product/Compare.php
index d38d2ba..5860121 100644
--- app/code/core/Mage/Catalog/Helper/Product/Compare.php
+++ app/code/core/Mage/Catalog/Helper/Product/Compare.php
@@ -79,17 +79,17 @@ class Mage_Catalog_Helper_Product_Compare extends Mage_Core_Helper_Url
      */
     public function getListUrl()
     {
-         $itemIds = array();
-         foreach ($this->getItemCollection() as $item) {
-             $itemIds[] = $item->getId();
-         }
+        $itemIds = array();
+        foreach ($this->getItemCollection() as $item) {
+            $itemIds[] = $item->getId();
+        }
 
-         $params = array(
-            'items'=>implode(',', $itemIds),
+        $params = array(
+            'items' => implode(',', $itemIds),
             Mage_Core_Controller_Front_Action::PARAM_NAME_URL_ENCODED => $this->getEncodedUrl()
-         );
+        );
 
-         return $this->_getUrl('catalog/product_compare', $params);
+        return $this->_getUrl('catalog/product_compare', $params);
     }
 
     /**
@@ -102,7 +102,8 @@ class Mage_Catalog_Helper_Product_Compare extends Mage_Core_Helper_Url
     {
         return array(
             'product' => $product->getId(),
-            Mage_Core_Controller_Front_Action::PARAM_NAME_URL_ENCODED => $this->getEncodedUrl()
+            Mage_Core_Controller_Front_Action::PARAM_NAME_URL_ENCODED => $this->getEncodedUrl(),
+            Mage_Core_Model_Url::FORM_KEY => $this->_getSingletonModel('core/session')->getFormKey()
         );
     }
 
@@ -128,7 +129,8 @@ class Mage_Catalog_Helper_Product_Compare extends Mage_Core_Helper_Url
         $beforeCompareUrl = Mage::getSingleton('catalog/session')->getBeforeCompareUrl();
 
         $params = array(
-            'product'=>$product->getId(),
+            'product' => $product->getId(),
+            Mage_Core_Model_Url::FORM_KEY => $this->_getSingletonModel('core/session')->getFormKey(),
             Mage_Core_Controller_Front_Action::PARAM_NAME_URL_ENCODED => $this->getEncodedUrl($beforeCompareUrl)
         );
 
@@ -143,10 +145,11 @@ class Mage_Catalog_Helper_Product_Compare extends Mage_Core_Helper_Url
      */
     public function getAddToCartUrl($product)
     {
-        $beforeCompareUrl = Mage::getSingleton('catalog/session')->getBeforeCompareUrl();
+        $beforeCompareUrl = $this->_getSingletonModel('catalog/session')->getBeforeCompareUrl();
         $params = array(
-            'product'=>$product->getId(),
-            Mage_Core_Controller_Front_Action::PARAM_NAME_URL_ENCODED => $this->getEncodedUrl($beforeCompareUrl)
+            'product' => $product->getId(),
+            Mage_Core_Controller_Front_Action::PARAM_NAME_URL_ENCODED => $this->getEncodedUrl($beforeCompareUrl),
+            Mage_Core_Model_Url::FORM_KEY => $this->_getSingletonModel('core/session')->getFormKey()
         );
 
         return $this->_getUrl('checkout/cart/add', $params);
@@ -161,7 +164,7 @@ class Mage_Catalog_Helper_Product_Compare extends Mage_Core_Helper_Url
     public function getRemoveUrl($item)
     {
         $params = array(
-            'product'=>$item->getId(),
+            'product' => $item->getId(),
             Mage_Core_Controller_Front_Action::PARAM_NAME_URL_ENCODED => $this->getEncodedUrl()
         );
         return $this->_getUrl('catalog/product_compare/remove', $params);
diff --git app/code/core/Mage/Catalog/Model/Resource/Layer/Filter/Price.php app/code/core/Mage/Catalog/Model/Resource/Layer/Filter/Price.php
index 544c549..dca2307 100755
--- app/code/core/Mage/Catalog/Model/Resource/Layer/Filter/Price.php
+++ app/code/core/Mage/Catalog/Model/Resource/Layer/Filter/Price.php
@@ -269,7 +269,7 @@ class Mage_Catalog_Model_Resource_Layer_Filter_Price extends Mage_Core_Model_Res
             'range' => $rangeExpr,
             'count' => $countExpr
         ));
-        $select->group($rangeExpr)->order("$rangeExpr ASC");
+        $select->group('range')->order('range ' . Varien_Data_Collection::SORT_ORDER_ASC);
 
         return $this->_getReadAdapter()->fetchPairs($select);
     }
diff --git app/code/core/Mage/Catalog/controllers/Product/CompareController.php app/code/core/Mage/Catalog/controllers/Product/CompareController.php
index 5855daa..32f9b4b 100644
--- app/code/core/Mage/Catalog/controllers/Product/CompareController.php
+++ app/code/core/Mage/Catalog/controllers/Product/CompareController.php
@@ -74,6 +74,11 @@ class Mage_Catalog_Product_CompareController extends Mage_Core_Controller_Front_
      */
     public function addAction()
     {
+        if (!$this->_validateFormKey()) {
+            $this->_redirectReferer();
+            return;
+        }
+
         $productId = (int) $this->getRequest()->getParam('product');
         if ($productId
             && (Mage::getSingleton('log/visitor')->getId() || Mage::getSingleton('customer/session')->isLoggedIn())
diff --git app/code/core/Mage/Catalog/etc/config.xml app/code/core/Mage/Catalog/etc/config.xml
index 9f49002..b7728a8 100644
--- app/code/core/Mage/Catalog/etc/config.xml
+++ app/code/core/Mage/Catalog/etc/config.xml
@@ -771,6 +771,9 @@
             <product>
                 <default_tax_group>2</default_tax_group>
             </product>
+            <product_image>
+                <max_dimension>5000</max_dimension>
+            </product_image>
             <seo>
                 <product_url_suffix>.html</product_url_suffix>
                 <category_url_suffix>.html</category_url_suffix>
diff --git app/code/core/Mage/Catalog/etc/system.xml app/code/core/Mage/Catalog/etc/system.xml
index 40268ec..7324482 100644
--- app/code/core/Mage/Catalog/etc/system.xml
+++ app/code/core/Mage/Catalog/etc/system.xml
@@ -185,6 +185,24 @@
                         </lines_perpage>
                     </fields>
                 </sitemap>
+                <product_image translate="label">
+                    <label>Product Image</label>
+                    <sort_order>200</sort_order>
+                    <show_in_default>1</show_in_default>
+                    <show_in_website>1</show_in_website>
+                    <show_in_store>1</show_in_store>
+                    <fields>
+                        <max_dimension translate="label comment">
+                            <label>Maximum resolution for upload image</label>
+                            <comment>Maximum width and height resolutions for upload image</comment>
+                            <frontend_type>text</frontend_type>
+                            <sort_order>10</sort_order>
+                            <show_in_default>1</show_in_default>
+                            <show_in_website>1</show_in_website>
+                            <show_in_store>1</show_in_store>
+                        </max_dimension>
+                    </fields>
+                </product_image>
                 <placeholder translate="label">
                     <label>Product Image Placeholders</label>
                     <clone_fields>1</clone_fields>
diff --git app/code/core/Mage/Centinel/Model/Api.php app/code/core/Mage/Centinel/Model/Api.php
index 13b1b36..9ef06d9 100644
--- app/code/core/Mage/Centinel/Model/Api.php
+++ app/code/core/Mage/Centinel/Model/Api.php
@@ -25,11 +25,6 @@
  */
 
 /**
- * 3D Secure Validation Library for Payment
- */
-include_once '3Dsecure/CentinelClient.php';
-
-/**
  * 3D Secure Validation Api
  */
 class Mage_Centinel_Model_Api extends Varien_Object
@@ -73,19 +68,19 @@ class Mage_Centinel_Model_Api extends Varien_Object
     /**
      * Centinel validation client
      *
-     * @var CentinelClient
+     * @var Mage_Centinel_Model_Api_Client
      */
     protected $_clientInstance = null;
 
     /**
      * Return Centinel thin client object
      *
-     * @return CentinelClient
+     * @return Mage_Centinel_Model_Api_Client
      */
     protected function _getClientInstance()
     {
         if (empty($this->_clientInstance)) {
-            $this->_clientInstance = new CentinelClient();
+            $this->_clientInstance = new Mage_Centinel_Model_Api_Client();
         }
         return $this->_clientInstance;
     }
@@ -136,7 +131,7 @@ class Mage_Centinel_Model_Api extends Varien_Object
      * @param $method string
      * @param $data array
      *
-     * @return CentinelClient
+     * @return Mage_Centinel_Model_Api_Client
      */
     protected function _call($method, $data)
     {
diff --git app/code/core/Mage/Centinel/Model/Api/Client.php app/code/core/Mage/Centinel/Model/Api/Client.php
new file mode 100644
index 0000000..e91a482
--- /dev/null
+++ app/code/core/Mage/Centinel/Model/Api/Client.php
@@ -0,0 +1,79 @@
+<?php
+/**
+ * Magento
+ *
+ * NOTICE OF LICENSE
+ *
+ * This source file is subject to the Open Software License (OSL 3.0)
+ * that is bundled with this package in the file LICENSE.txt.
+ * It is also available through the world-wide-web at this URL:
+ * http://opensource.org/licenses/osl-3.0.php
+ * If you did not receive a copy of the license and are unable to
+ * obtain it through the world-wide-web, please send an email
+ * to license@magento.com so we can send you a copy immediately.
+ *
+ * DISCLAIMER
+ *
+ * Do not edit or add to this file if you wish to upgrade Magento to newer
+ * versions in the future. If you wish to customize Magento for your
+ * needs please refer to http://www.magento.com for more information.
+ *
+ * @category    Mage
+ * @package     Mage_Centinel
+ * @copyright Copyright (c) 2006-2014 X.commerce, Inc. (http://www.magento.com)
+ * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
+ */
+
+/**
+ * 3D Secure Validation Library for Payment
+ */
+include_once '3Dsecure/CentinelClient.php';
+
+/**
+ * 3D Secure Validation Api
+ */
+class Mage_Centinel_Model_Api_Client extends CentinelClient
+{
+    public function sendHttp($url, $connectTimeout = "", $timeout)
+    {
+        // verify that the URL uses a supported protocol.
+        if ((strpos($url, "http://") === 0) || (strpos($url, "https://") === 0)) {
+
+            //Construct the payload to POST to the url.
+            $data = $this->getRequestXml();
+
+            // create a new cURL resource
+            $ch = curl_init($url);
+
+            // set URL and other appropriate options
+            curl_setopt($ch, CURLOPT_POST ,1);
+            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
+            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
+            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
+            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
+            curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
+
+            // Execute the request.
+            $result = curl_exec($ch);
+            $succeeded = curl_errno($ch) == 0 ? true : false;
+
+            // close cURL resource, and free up system resources
+            curl_close($ch);
+
+            // If Communication was not successful set error result, otherwise
+            if (!$succeeded) {
+                $result = $this->setErrorResponse(CENTINEL_ERROR_CODE_8030, CENTINEL_ERROR_CODE_8030_DESC);
+            }
+
+            // Assert that we received an expected Centinel Message in reponse.
+            if (strpos($result, "<CardinalMPI>") === false) {
+                $result = $this->setErrorResponse(CENTINEL_ERROR_CODE_8010, CENTINEL_ERROR_CODE_8010_DESC);
+            }
+        } else {
+            $result = $this->setErrorResponse(CENTINEL_ERROR_CODE_8000, CENTINEL_ERROR_CODE_8000_DESC);
+        }
+        $parser = new XMLParser;
+        $parser->deserializeXml($result);
+        $this->response = $parser->deserializedResponse;
+    }
+}
diff --git app/code/core/Mage/Checkout/Helper/Cart.php app/code/core/Mage/Checkout/Helper/Cart.php
index 33ba781..38c333d 100644
--- app/code/core/Mage/Checkout/Helper/Cart.php
+++ app/code/core/Mage/Checkout/Helper/Cart.php
@@ -31,6 +31,9 @@
  */
 class Mage_Checkout_Helper_Cart extends Mage_Core_Helper_Url
 {
+    /**
+     * Redirect to Cart path
+     */
     const XML_PATH_REDIRECT_TO_CART         = 'checkout/cart/redirect_to_cart';
 
     /**
@@ -47,16 +50,16 @@ class Mage_Checkout_Helper_Cart extends Mage_Core_Helper_Url
      * Retrieve url for add product to cart
      *
      * @param   Mage_Catalog_Model_Product $product
+     * @param array $additional
      * @return  string
      */
     public function getAddUrl($product, $additional = array())
     {
-        $continueUrl    = Mage::helper('core')->urlEncode($this->getCurrentUrl());
-        $urlParamName   = Mage_Core_Controller_Front_Action::PARAM_NAME_URL_ENCODED;
-
         $routeParams = array(
-            $urlParamName   => $continueUrl,
-            'product'       => $product->getEntityId()
+            Mage_Core_Controller_Front_Action::PARAM_NAME_URL_ENCODED => $this->_getHelperInstance('core')
+                ->urlEncode($this->getCurrentUrl()),
+            'product' => $product->getEntityId(),
+            Mage_Core_Model_Url::FORM_KEY => $this->_getSingletonModel('core/session')->getFormKey()
         );
 
         if (!empty($additional)) {
@@ -77,6 +80,17 @@ class Mage_Checkout_Helper_Cart extends Mage_Core_Helper_Url
     }
 
     /**
+     * Return helper instance
+     *
+     * @param  string $helperName
+     * @return Mage_Core_Helper_Abstract
+     */
+    protected function _getHelperInstance($helperName)
+    {
+        return Mage::helper($helperName);
+    }
+
+    /**
      * Retrieve url for remove product from cart
      *
      * @param   Mage_Sales_Quote_Item $item
@@ -85,7 +99,7 @@ class Mage_Checkout_Helper_Cart extends Mage_Core_Helper_Url
     public function getRemoveUrl($item)
     {
         $params = array(
-            'id'=>$item->getId(),
+            'id' => $item->getId(),
             Mage_Core_Controller_Front_Action::PARAM_NAME_BASE64_URL => $this->getCurrentBase64Url()
         );
         return $this->_getUrl('checkout/cart/delete', $params);
diff --git app/code/core/Mage/Checkout/controllers/CartController.php app/code/core/Mage/Checkout/controllers/CartController.php
index 4e41521..db69872 100644
--- app/code/core/Mage/Checkout/controllers/CartController.php
+++ app/code/core/Mage/Checkout/controllers/CartController.php
@@ -70,6 +70,7 @@ class Mage_Checkout_CartController extends Mage_Core_Controller_Front_Action
      * Set back redirect url to response
      *
      * @return Mage_Checkout_CartController
+     * @throws Mage_Exception
      */
     protected function _goBack()
     {
@@ -166,9 +167,15 @@ class Mage_Checkout_CartController extends Mage_Core_Controller_Front_Action
 
     /**
      * Add product to shopping cart action
+     *
+     * @return void
      */
     public function addAction()
     {
+        if (!$this->_validateFormKey()) {
+            $this->_goBack();
+            return;
+        }
         $cart   = $this->_getCart();
         $params = $this->getRequest()->getParams();
         try {
@@ -207,7 +214,7 @@ class Mage_Checkout_CartController extends Mage_Core_Controller_Front_Action
             );
 
             if (!$this->_getSession()->getNoCartRedirect(true)) {
-                if (!$cart->getQuote()->getHasError()){
+                if (!$cart->getQuote()->getHasError()) {
                     $message = $this->__('%s was added to your shopping cart.', Mage::helper('core')->escapeHtml($product->getName()));
                     $this->_getSession()->addSuccess($message);
                 }
@@ -236,34 +243,41 @@ class Mage_Checkout_CartController extends Mage_Core_Controller_Front_Action
         }
     }
 
+    /**
+     * Add products in group to shopping cart action
+     */
     public function addgroupAction()
     {
         $orderItemIds = $this->getRequest()->getParam('order_items', array());
-        if (is_array($orderItemIds)) {
-            $itemsCollection = Mage::getModel('sales/order_item')
-                ->getCollection()
-                ->addIdFilter($orderItemIds)
-                ->load();
-            /* @var $itemsCollection Mage_Sales_Model_Mysql4_Order_Item_Collection */
-            $cart = $this->_getCart();
-            foreach ($itemsCollection as $item) {
-                try {
-                    $cart->addOrderItem($item, 1);
-                } catch (Mage_Core_Exception $e) {
-                    if ($this->_getSession()->getUseNotice(true)) {
-                        $this->_getSession()->addNotice($e->getMessage());
-                    } else {
-                        $this->_getSession()->addError($e->getMessage());
-                    }
-                } catch (Exception $e) {
-                    $this->_getSession()->addException($e, $this->__('Cannot add the item to shopping cart.'));
-                    Mage::logException($e);
-                    $this->_goBack();
+
+        if (!is_array($orderItemIds) || !$this->_validateFormKey()) {
+            $this->_goBack();
+            return;
+        }
+
+        $itemsCollection = Mage::getModel('sales/order_item')
+            ->getCollection()
+            ->addIdFilter($orderItemIds)
+            ->load();
+        /* @var $itemsCollection Mage_Sales_Model_Mysql4_Order_Item_Collection */
+        $cart = $this->_getCart();
+        foreach ($itemsCollection as $item) {
+            try {
+                $cart->addOrderItem($item, 1);
+            } catch (Mage_Core_Exception $e) {
+                if ($this->_getSession()->getUseNotice(true)) {
+                    $this->_getSession()->addNotice($e->getMessage());
+                } else {
+                    $this->_getSession()->addError($e->getMessage());
                 }
+            } catch (Exception $e) {
+                $this->_getSession()->addException($e, $this->__('Cannot add the item to shopping cart.'));
+                Mage::logException($e);
+                $this->_goBack();
             }
-            $cart->save();
-            $this->_getSession()->setCartWasUpdated(true);
         }
+        $cart->save();
+        $this->_getSession()->setCartWasUpdated(true);
         $this->_goBack();
     }
 
@@ -347,8 +361,8 @@ class Mage_Checkout_CartController extends Mage_Core_Controller_Front_Action
                 array('item' => $item, 'request' => $this->getRequest(), 'response' => $this->getResponse())
             );
             if (!$this->_getSession()->getNoCartRedirect(true)) {
-                if (!$cart->getQuote()->getHasError()){
-                    $message = $this->__('%s was updated in your shopping cart.', Mage::helper('core')->htmlEscape($item->getProduct()->getName()));
+                if (!$cart->getQuote()->getHasError()) {
+                    $message = $this->__('%s was updated in your shopping cart.', Mage::helper('core')->escapeHtml($item->getProduct()->getName()));
                     $this->_getSession()->addSuccess($message);
                 }
                 $this->_goBack();
@@ -382,6 +396,11 @@ class Mage_Checkout_CartController extends Mage_Core_Controller_Front_Action
      */
     public function updatePostAction()
     {
+        if (!$this->_validateFormKey()) {
+            $this->_redirect('*/*/');
+            return;
+        }
+
         $updateAction = (string)$this->getRequest()->getParam('update_cart_action');
 
         switch ($updateAction) {
@@ -492,6 +511,11 @@ class Mage_Checkout_CartController extends Mage_Core_Controller_Front_Action
         $this->_goBack();
     }
 
+    /**
+     * Estimate update action
+     *
+     * @return null
+     */
     public function estimateUpdatePostAction()
     {
         $code = (string) $this->getRequest()->getParam('estimate_method');
diff --git app/code/core/Mage/Checkout/controllers/OnepageController.php app/code/core/Mage/Checkout/controllers/OnepageController.php
index e62cb91..55d0833 100644
--- app/code/core/Mage/Checkout/controllers/OnepageController.php
+++ app/code/core/Mage/Checkout/controllers/OnepageController.php
@@ -24,16 +24,27 @@
  * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
  */
 
-
+/**
+ * Class Onepage controller
+ */
 class Mage_Checkout_OnepageController extends Mage_Checkout_Controller_Action
 {
+    /**
+     * Functions for concrete method
+     *
+     * @var array
+     */
     protected $_sectionUpdateFunctions = array(
         'payment-method'  => '_getPaymentMethodsHtml',
         'shipping-method' => '_getShippingMethodsHtml',
         'review'          => '_getReviewHtml',
     );
 
-    /** @var Mage_Sales_Model_Order */
+    /**
+     * Order instance
+     *
+     * @var Mage_Sales_Model_Order
+     */
     protected $_order;
 
     /**
@@ -50,7 +61,7 @@ class Mage_Checkout_OnepageController extends Mage_Checkout_Controller_Action
             $checkoutSessionQuote->removeAllAddresses();
         }
 
-        if(!$this->_canShowForUnregisteredUsers()){
+        if (!$this->_canShowForUnregisteredUsers()) {
             $this->norouteAction();
             $this->setFlag('',self::FLAG_NO_DISPATCH,true);
             return;
@@ -59,6 +70,11 @@ class Mage_Checkout_OnepageController extends Mage_Checkout_Controller_Action
         return $this;
     }
 
+    /**
+     * Send headers in case if session is expired
+     *
+     * @return Mage_Checkout_OnepageController
+     */
     protected function _ajaxRedirectResponse()
     {
         $this->getResponse()
@@ -123,6 +139,12 @@ class Mage_Checkout_OnepageController extends Mage_Checkout_Controller_Action
         return $output;
     }
 
+    /**
+     * Return block content from the 'checkout_onepage_additional'
+     * This is the additional content for shipping method
+     *
+     * @return string
+     */
     protected function _getAdditionalHtml()
     {
         $layout = $this->getLayout();
@@ -180,7 +202,7 @@ class Mage_Checkout_OnepageController extends Mage_Checkout_Controller_Action
             return;
         }
         Mage::getSingleton('checkout/session')->setCartWasUpdated(false);
-        Mage::getSingleton('customer/session')->setBeforeAuthUrl(Mage::getUrl('*/*/*', array('_secure'=>true)));
+        Mage::getSingleton('customer/session')->setBeforeAuthUrl(Mage::getUrl('*/*/*', array('_secure' => true)));
         $this->getOnepage()->initCheckout();
         $this->loadLayout();
         $this->_initLayoutMessages('customer/session');
@@ -200,6 +222,9 @@ class Mage_Checkout_OnepageController extends Mage_Checkout_Controller_Action
         $this->renderLayout();
     }
 
+    /**
+     * Shipping action
+     */
     public function shippingMethodAction()
     {
         if ($this->_expireAjax()) {
@@ -209,6 +234,9 @@ class Mage_Checkout_OnepageController extends Mage_Checkout_Controller_Action
         $this->renderLayout();
     }
 
+    /**
+     * Review action
+     */
     public function reviewAction()
     {
         if ($this->_expireAjax()) {
@@ -244,6 +272,9 @@ class Mage_Checkout_OnepageController extends Mage_Checkout_Controller_Action
         $this->renderLayout();
     }
 
+    /**
+     * Failure action
+     */
     public function failureAction()
     {
         $lastQuoteId = $this->getOnepage()->getCheckout()->getLastQuoteId();
@@ -259,6 +290,9 @@ class Mage_Checkout_OnepageController extends Mage_Checkout_Controller_Action
     }
 
 
+    /**
+     * Additional action
+     */
     public function getAdditionalAction()
     {
         $this->getResponse()->setBody($this->_getAdditionalHtml());
@@ -383,10 +417,10 @@ class Mage_Checkout_OnepageController extends Mage_Checkout_Controller_Action
             /*
             $result will have erro data if shipping method is empty
             */
-            if(!$result) {
+            if (!$result) {
                 Mage::dispatchEvent('checkout_controller_onepage_save_shipping_method',
-                        array('request'=>$this->getRequest(),
-                            'quote'=>$this->getOnepage()->getQuote()));
+                    array('request' => $this->getRequest(),
+                        'quote' => $this->getOnepage()->getQuote()));
                 $this->getOnepage()->getQuote()->collectTotals();
                 $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
 
@@ -452,7 +486,8 @@ class Mage_Checkout_OnepageController extends Mage_Checkout_Controller_Action
     /**
      * Get Order by quoteId
      *
-     * @return Mage_Sales_Model_Order
+     * @return Mage_Core_Model_Abstract|Mage_Sales_Model_Order
+     * @throws Mage_Payment_Model_Info_Exception
      */
     protected function _getOrder()
     {
@@ -489,15 +524,21 @@ class Mage_Checkout_OnepageController extends Mage_Checkout_Controller_Action
      */
     public function saveOrderAction()
     {
+        if (!$this->_validateFormKey()) {
+            return $this->_redirect('*/*');
+        }
+
         if ($this->_expireAjax()) {
             return;
         }
 
         $result = array();
         try {
-            if ($requiredAgreements = Mage::helper('checkout')->getRequiredAgreementIds()) {
+            $requiredAgreements = Mage::helper('checkout')->getRequiredAgreementIds();
+            if ($requiredAgreements) {
                 $postedAgreements = array_keys($this->getRequest()->getPost('agreement', array()));
-                if ($diff = array_diff($requiredAgreements, $postedAgreements)) {
+                $diff = array_diff($requiredAgreements, $postedAgreements);
+                if ($diff) {
                     $result['success'] = false;
                     $result['error'] = true;
                     $result['error_messages'] = $this->__('Please agree to all the terms and conditions before placing the order.');
@@ -515,7 +556,7 @@ class Mage_Checkout_OnepageController extends Mage_Checkout_Controller_Action
             $result['error']   = false;
         } catch (Mage_Payment_Model_Info_Exception $e) {
             $message = $e->getMessage();
-            if( !empty($message) ) {
+            if ( !empty($message) ) {
                 $result['error_messages'] = $message;
             }
             $result['goto_section'] = 'payment';
@@ -530,12 +571,13 @@ class Mage_Checkout_OnepageController extends Mage_Checkout_Controller_Action
             $result['error'] = true;
             $result['error_messages'] = $e->getMessage();
 
-            if ($gotoSection = $this->getOnepage()->getCheckout()->getGotoSection()) {
+            $gotoSection = $this->getOnepage()->getCheckout()->getGotoSection();
+            if ($gotoSection) {
                 $result['goto_section'] = $gotoSection;
                 $this->getOnepage()->getCheckout()->setGotoSection(null);
             }
-
-            if ($updateSection = $this->getOnepage()->getCheckout()->getUpdateSection()) {
+            $updateSection = $this->getOnepage()->getCheckout()->getUpdateSection();
+            if ($updateSection) {
                 if (isset($this->_sectionUpdateFunctions[$updateSection])) {
                     $updateSectionFunction = $this->_sectionUpdateFunctions[$updateSection];
                     $result['update_section'] = array(
diff --git app/code/core/Mage/Core/Block/Abstract.php app/code/core/Mage/Core/Block/Abstract.php
index 1e1659d..a58fc26 100644
--- app/code/core/Mage/Core/Block/Abstract.php
+++ app/code/core/Mage/Core/Block/Abstract.php
@@ -38,6 +38,10 @@
 abstract class Mage_Core_Block_Abstract extends Varien_Object
 {
     /**
+     * Prefix for cache key
+     */
+    const CACHE_KEY_PREFIX = 'BLOCK_';
+    /**
      * Cache group Tag
      */
     const CACHE_GROUP = 'block_html';
@@ -1233,7 +1237,13 @@ abstract class Mage_Core_Block_Abstract extends Varien_Object
     public function getCacheKey()
     {
         if ($this->hasData('cache_key')) {
-            return $this->getData('cache_key');
+            $cacheKey = $this->getData('cache_key');
+            if (strpos($cacheKey, self::CACHE_KEY_PREFIX) !== 0) {
+                $cacheKey = self::CACHE_KEY_PREFIX . $cacheKey;
+                $this->setData('cache_key', $cacheKey);
+            }
+
+            return $cacheKey;
         }
         /**
          * don't prevent recalculation by saving generated cache key
diff --git app/code/core/Mage/Core/Helper/Url.php app/code/core/Mage/Core/Helper/Url.php
index 2fd8608..a899975 100644
--- app/code/core/Mage/Core/Helper/Url.php
+++ app/code/core/Mage/Core/Helper/Url.php
@@ -51,7 +51,7 @@ class Mage_Core_Helper_Url extends Mage_Core_Helper_Abstract
             $port = (in_array($port, $defaultPorts)) ? '' : ':' . $port;
         }
         $url = $request->getScheme() . '://' . $request->getHttpHost() . $port . $request->getServer('REQUEST_URI');
-        return $url;
+        return $this->escapeUrl($url);
 //        return $this->_getUrl('*/*/*', array('_current' => true, '_use_rewrite' => true));
     }
 
@@ -65,7 +65,13 @@ class Mage_Core_Helper_Url extends Mage_Core_Helper_Abstract
         return $this->urlEncode($this->getCurrentUrl());
     }
 
-    public function getEncodedUrl($url=null)
+    /**
+     * Return encoded url
+     *
+     * @param null|string $url
+     * @return string
+     */
+    public function getEncodedUrl($url = null)
     {
         if (!$url) {
             $url = $this->getCurrentUrl();
@@ -83,6 +89,12 @@ class Mage_Core_Helper_Url extends Mage_Core_Helper_Abstract
         return Mage::getBaseUrl();
     }
 
+    /**
+     * Formatting string
+     *
+     * @param string $string
+     * @return string
+     */
     protected function _prepareString($string)
     {
         $string = preg_replace('#[^0-9a-z]+#i', '-', $string);
@@ -104,7 +116,7 @@ class Mage_Core_Helper_Url extends Mage_Core_Helper_Abstract
         $startDelimiter = (false === strpos($url,'?'))? '?' : '&';
 
         $arrQueryParams = array();
-        foreach($param as $key=>$value) {
+        foreach ($param as $key => $value) {
             if (is_numeric($key) || is_object($value)) {
                 continue;
             }
@@ -128,6 +140,7 @@ class Mage_Core_Helper_Url extends Mage_Core_Helper_Abstract
      *
      * @param string $url
      * @param string $paramKey
+     * @param boolean $caseSensitive
      * @return string
      */
     public function removeRequestParam($url, $paramKey, $caseSensitive = false)
@@ -143,4 +156,16 @@ class Mage_Core_Helper_Url extends Mage_Core_Helper_Abstract
         }
         return $url;
     }
+
+    /**
+     * Return singleton model instance
+     *
+     * @param string $name
+     * @param array $arguments
+     * @return Mage_Core_Model_Abstract
+     */
+    protected function _getSingletonModel($name, $arguments = array())
+    {
+        return Mage::getSingleton($name, $arguments);
+    }
 }
diff --git app/code/core/Mage/Core/Model/Encryption.php app/code/core/Mage/Core/Model/Encryption.php
index 4de64a2..cec49c8 100644
--- app/code/core/Mage/Core/Model/Encryption.php
+++ app/code/core/Mage/Core/Model/Encryption.php
@@ -98,9 +98,9 @@ class Mage_Core_Model_Encryption
         $hashArr = explode(':', $hash);
         switch (count($hashArr)) {
             case 1:
-                return $this->hash($password) === $hash;
+                return hash_equals($this->hash($password), $hash);
             case 2:
-                return $this->hash($hashArr[1] . $password) === $hashArr[0];
+                return hash_equals($this->hash($hashArr[1] . $password),  $hashArr[0]);
         }
         Mage::throwException('Invalid hash.');
     }
diff --git app/code/core/Mage/Core/Model/Input/Filter/MaliciousCode.php app/code/core/Mage/Core/Model/Input/Filter/MaliciousCode.php
index 55da2fb..0d4128e 100644
--- app/code/core/Mage/Core/Model/Input/Filter/MaliciousCode.php
+++ app/code/core/Mage/Core/Model/Input/Filter/MaliciousCode.php
@@ -65,7 +65,13 @@ class Mage_Core_Model_Input_Filter_MaliciousCode implements Zend_Filter_Interfac
      */
     public function filter($value)
     {
-        return preg_replace($this->_expressions, '', $value);
+        $result = false;
+        do {
+            $subject = $result ? $result : $value;
+            $result = preg_replace($this->_expressions, '', $subject, -1, $count);
+        } while ($count !== 0);
+
+        return $result;
     }
 
     /**
diff --git app/code/core/Mage/Core/Model/Url.php app/code/core/Mage/Core/Model/Url.php
index c87bf48..28086af 100644
--- app/code/core/Mage/Core/Model/Url.php
+++ app/code/core/Mage/Core/Model/Url.php
@@ -89,14 +89,31 @@ class Mage_Core_Model_Url extends Varien_Object
     const DEFAULT_ACTION_NAME       = 'index';
 
     /**
-     * Configuration paths
+     * XML base url path unsecure
      */
     const XML_PATH_UNSECURE_URL     = 'web/unsecure/base_url';
+
+    /**
+     * XML base url path secure
+     */
     const XML_PATH_SECURE_URL       = 'web/secure/base_url';
+
+    /**
+     * XML path for using in adminhtml
+     */
     const XML_PATH_SECURE_IN_ADMIN  = 'default/web/secure/use_in_adminhtml';
+
+    /**
+     * XML path for using in frontend
+     */
     const XML_PATH_SECURE_IN_FRONT  = 'web/secure/use_in_frontend';
 
     /**
+     * Param name for form key functionality
+     */
+    const FORM_KEY = 'form_key';
+
+    /**
      * Configuration data cache
      *
      * @var array
@@ -483,7 +500,7 @@ class Mage_Core_Model_Url extends Varien_Object
             }
             $routePath = $this->getActionPath();
             if ($this->getRouteParams()) {
-                foreach ($this->getRouteParams() as $key=>$value) {
+                foreach ($this->getRouteParams() as $key => $value) {
                     if (is_null($value) || false === $value || '' === $value || !is_scalar($value)) {
                         continue;
                     }
@@ -939,8 +956,8 @@ class Mage_Core_Model_Url extends Varien_Object
     /**
      * Build url by requested path and parameters
      *
-     * @param   string|null $routePath
-     * @param   array|null $routeParams
+     * @param string|null $routePath
+     * @param array|null $routeParams
      * @return  string
      */
     public function getUrl($routePath = null, $routeParams = null)
@@ -974,6 +991,7 @@ class Mage_Core_Model_Url extends Varien_Object
             $noSid = (bool)$routeParams['_nosid'];
             unset($routeParams['_nosid']);
         }
+
         $url = $this->getRouteUrl($routePath, $routeParams);
         /**
          * Apply query params, need call after getRouteUrl for rewrite _current values
@@ -1007,6 +1025,18 @@ class Mage_Core_Model_Url extends Varien_Object
     }
 
     /**
+     * Return singleton model instance
+     *
+     * @param string $name
+     * @param array $arguments
+     * @return Mage_Core_Model_Abstract
+     */
+    protected function _getSingletonModel($name, $arguments = array())
+    {
+        return Mage::getSingleton($name, $arguments);
+    }
+
+    /**
      * Check and add session id to URL
      *
      * @param string $url
diff --git app/code/core/Mage/Core/functions.php app/code/core/Mage/Core/functions.php
index fbd0acc..ac6cfc2 100644
--- app/code/core/Mage/Core/functions.php
+++ app/code/core/Mage/Core/functions.php
@@ -375,3 +375,38 @@ if ( !function_exists('sys_get_temp_dir') ) {
         }
     }
 }
+
+if (!function_exists('hash_equals')) {
+    /**
+     * Compares two strings using the same time whether they're equal or not.
+     * A difference in length will leak
+     *
+     * @param string $known_string
+     * @param string $user_string
+     * @return boolean Returns true when the two strings are equal, false otherwise.
+     */
+    function hash_equals($known_string, $user_string)
+    {
+        $result = 0;
+
+        if (!is_string($known_string)) {
+            trigger_error("hash_equals(): Expected known_string to be a string", E_USER_WARNING);
+            return false;
+        }
+
+        if (!is_string($user_string)) {
+            trigger_error("hash_equals(): Expected user_string to be a string", E_USER_WARNING);
+            return false;
+        }
+
+        if (strlen($known_string) != strlen($user_string)) {
+            return false;
+        }
+
+        for ($i = 0; $i < strlen($known_string); $i++) {
+            $result |= (ord($known_string[$i]) ^ ord($user_string[$i]));
+        }
+
+        return 0 === $result;
+    }
+}
diff --git app/code/core/Mage/Customer/Block/Address/Book.php app/code/core/Mage/Customer/Block/Address/Book.php
index 2b295c5..9bc7c69 100644
--- app/code/core/Mage/Customer/Block/Address/Book.php
+++ app/code/core/Mage/Customer/Block/Address/Book.php
@@ -56,7 +56,8 @@ class Mage_Customer_Block_Address_Book extends Mage_Core_Block_Template
 
     public function getDeleteUrl()
     {
-        return $this->getUrl('customer/address/delete');
+        return $this->getUrl('customer/address/delete',
+            array(Mage_Core_Model_Url::FORM_KEY => Mage::getSingleton('core/session')->getFormKey()));
     }
 
     public function getAddressEditUrl($address)
diff --git app/code/core/Mage/Customer/controllers/AccountController.php app/code/core/Mage/Customer/controllers/AccountController.php
index 60be0f7..7361168 100644
--- app/code/core/Mage/Customer/controllers/AccountController.php
+++ app/code/core/Mage/Customer/controllers/AccountController.php
@@ -140,6 +140,11 @@ class Mage_Customer_AccountController extends Mage_Core_Controller_Front_Action
      */
     public function loginPostAction()
     {
+        if (!$this->_validateFormKey()) {
+            $this->_redirect('*/*/');
+            return;
+        }
+
         if ($this->_getSession()->isLoggedIn()) {
             $this->_redirect('*/*/');
             return;
@@ -157,8 +162,8 @@ class Mage_Customer_AccountController extends Mage_Core_Controller_Front_Action
                 } catch (Mage_Core_Exception $e) {
                     switch ($e->getCode()) {
                         case Mage_Customer_Model_Customer::EXCEPTION_EMAIL_NOT_CONFIRMED:
-                            $value = Mage::helper('customer')->getEmailConfirmationUrl($login['username']);
-                            $message = Mage::helper('customer')->__('This account is not confirmed. <a href="%s">Click here</a> to resend confirmation email.', $value);
+                            $value = $this->_getHelper('customer')->getEmailConfirmationUrl($login['username']);
+                            $message = $this->_getHelper('customer')->__('This account is not confirmed. <a href="%s">Click here</a> to resend confirmation email.', $value);
                             break;
                         case Mage_Customer_Model_Customer::EXCEPTION_INVALID_EMAIL_OR_PASSWORD:
                             $message = $e->getMessage();
@@ -188,7 +193,7 @@ class Mage_Customer_AccountController extends Mage_Core_Controller_Front_Action
 
         if (!$session->getBeforeAuthUrl() || $session->getBeforeAuthUrl() == Mage::getBaseUrl()) {
             // Set default URL to redirect customer to
-            $session->setBeforeAuthUrl(Mage::helper('customer')->getAccountUrl());
+            $session->setBeforeAuthUrl($this->_getHelper('customer')->getAccountUrl());
             // Redirect customer to the last page visited after logging in
             if ($session->isLoggedIn()) {
                 if (!Mage::getStoreConfigFlag(
@@ -197,8 +202,8 @@ class Mage_Customer_AccountController extends Mage_Core_Controller_Front_Action
                     $referer = $this->getRequest()->getParam(Mage_Customer_Helper_Data::REFERER_QUERY_PARAM_NAME);
                     if ($referer) {
                         // Rebuild referer URL to handle the case when SID was changed
-                        $referer = Mage::getModel('core/url')
-                            ->getRebuiltUrl(Mage::helper('core')->urlDecode($referer));
+                        $referer = $this->_getModel('core/url')
+                            ->getRebuiltUrl($this->_getHelper('core')->urlDecode($referer));
                         if ($this->_isUrlInternal($referer)) {
                             $session->setBeforeAuthUrl($referer);
                         }
@@ -207,10 +212,10 @@ class Mage_Customer_AccountController extends Mage_Core_Controller_Front_Action
                     $session->setBeforeAuthUrl($session->getAfterAuthUrl(true));
                 }
             } else {
-                $session->setBeforeAuthUrl(Mage::helper('customer')->getLoginUrl());
+                $session->setBeforeAuthUrl($this->_getHelper('customer')->getLoginUrl());
             }
-        } else if ($session->getBeforeAuthUrl() == Mage::helper('customer')->getLogoutUrl()) {
-            $session->setBeforeAuthUrl(Mage::helper('customer')->getDashboardUrl());
+        } else if ($session->getBeforeAuthUrl() == $this->_getHelper('customer')->getLogoutUrl()) {
+            $session->setBeforeAuthUrl($this->_getHelper('customer')->getDashboardUrl());
         } else {
             if (!$session->getAfterAuthUrl()) {
                 $session->setAfterAuthUrl($session->getBeforeAuthUrl());
@@ -267,125 +272,254 @@ class Mage_Customer_AccountController extends Mage_Core_Controller_Front_Action
             return;
         }
 
+        /** @var $session Mage_Customer_Model_Session */
         $session = $this->_getSession();
         if ($session->isLoggedIn()) {
             $this->_redirect('*/*/');
             return;
         }
 
-        if ($this->getRequest()->isPost()) {
-            $errors = array();
+        if (!$this->getRequest()->isPost()) {
+            $errUrl = $this->_getUrl('*/*/create', array('_secure' => true));
+            $this->_redirectError($errUrl);
+            return;
+        }
 
-            if (!$customer = Mage::registry('current_customer')) {
-                $customer = Mage::getModel('customer/customer')->setId(null);
+        $customer = $this->_getCustomer();
+
+        try {
+            $errors = $this->_getCustomerErrors($customer);
+
+            if (empty($errors)) {
+                $customer->save();
+                $this->_dispatchRegisterSuccess($customer);
+                $this->_successProcessRegistration($customer);
+                return;
+            } else {
+                $this->_addSessionError($errors);
+            }
+        } catch (Mage_Core_Exception $e) {
+            $session->setCustomerFormData($this->getRequest()->getPost());
+            if ($e->getCode() === Mage_Customer_Model_Customer::EXCEPTION_EMAIL_EXISTS) {
+                $url = $this->_getUrl('customer/account/forgotpassword');
+                $message = $this->__('There is already an account with this email address. If you are sure that it is your email address, <a href="%s">click here</a> to get your password and access your account.', $url);
+            } else {
+                $message = Mage::helper('core')->escapeHtml($e->getMessage());
             }
+            $session->addError($message);
+        } catch (Exception $e) {
+            $session->setCustomerFormData($this->getRequest()->getPost())
+                ->addException($e, $this->__('Cannot save the customer.'));
+        }
+        $url = $this->_getUrl('*/*/create', array('_secure' => true));
+        $this->_redirectError($url);
+    }
 
-            /* @var $customerForm Mage_Customer_Model_Form */
-            $customerForm = Mage::getModel('customer/form');
-            $customerForm->setFormCode('customer_account_create')
-                ->setEntity($customer);
+    /**
+     * Success Registration
+     *
+     * @param Mage_Customer_Model_Customer $customer
+     * @return Mage_Customer_AccountController
+     */
+    protected function _successProcessRegistration(Mage_Customer_Model_Customer $customer)
+    {
+        $session = $this->_getSession();
+        if ($customer->isConfirmationRequired()) {
+            /** @var $app Mage_Core_Model_App */
+            $app = $this->_getApp();
+            /** @var $store  Mage_Core_Model_Store*/
+            $store = $app->getStore();
+            $customer->sendNewAccountEmail(
+                'confirmation',
+                $session->getBeforeAuthUrl(),
+                $store->getId()
+            );
+            $customerHelper = $this->_getHelper('customer');
+            $session->addSuccess($this->__('Account confirmation is required. Please, check your email for the confirmation link. To resend the confirmation email please <a href="%s">click here</a>.',
+                $customerHelper->getEmailConfirmationUrl($customer->getEmail())));
+            $url = $this->_getUrl('*/*/index', array('_secure' => true));
+        } else {
+            $session->setCustomerAsLoggedIn($customer);
+            $session->renewSession();
+            $url = $this->_welcomeCustomer($customer);
+        }
+        $this->_redirectSuccess($url);
+        return $this;
+    }
 
-            $customerData = $customerForm->extractData($this->getRequest());
+    /**
+     * Get Customer Model
+     *
+     * @return Mage_Customer_Model_Customer
+     */
+    protected function _getCustomer()
+    {
+        $customer = $this->_getFromRegistry('current_customer');
+        if (!$customer) {
+            $customer = $this->_getModel('customer/customer')->setId(null);
+        }
+        if ($this->getRequest()->getParam('is_subscribed', false)) {
+            $customer->setIsSubscribed(1);
+        }
+        /**
+         * Initialize customer group id
+         */
+        $customer->getGroupId();
+
+        return $customer;
+    }
 
-            if ($this->getRequest()->getParam('is_subscribed', false)) {
-                $customer->setIsSubscribed(1);
+    /**
+     * Add session error method
+     *
+     * @param string|array $errors
+     */
+    protected function _addSessionError($errors)
+    {
+        $session = $this->_getSession();
+        $session->setCustomerFormData($this->getRequest()->getPost());
+        if (is_array($errors)) {
+            foreach ($errors as $errorMessage) {
+                $session->addError(Mage::helper('core')->escapeHtml($errorMessage));
             }
+        } else {
+            $session->addError($this->__('Invalid customer data'));
+        }
+    }
 
-            /**
-             * Initialize customer group id
-             */
-            $customer->getGroupId();
-
-            if ($this->getRequest()->getPost('create_address')) {
-                /* @var $address Mage_Customer_Model_Address */
-                $address = Mage::getModel('customer/address');
-                /* @var $addressForm Mage_Customer_Model_Form */
-                $addressForm = Mage::getModel('customer/form');
-                $addressForm->setFormCode('customer_register_address')
-                    ->setEntity($address);
-
-                $addressData    = $addressForm->extractData($this->getRequest(), 'address', false);
-                $addressErrors  = $addressForm->validateData($addressData);
-                if ($addressErrors === true) {
-                    $address->setId(null)
-                        ->setIsDefaultBilling($this->getRequest()->getParam('default_billing', false))
-                        ->setIsDefaultShipping($this->getRequest()->getParam('default_shipping', false));
-                    $addressForm->compactData($addressData);
-                    $customer->addAddress($address);
-
-                    $addressErrors = $address->validate();
-                    if (is_array($addressErrors)) {
-                        $errors = array_merge($errors, $addressErrors);
-                    }
-                } else {
-                    $errors = array_merge($errors, $addressErrors);
-                }
+    /**
+     * Validate customer data and return errors if they are
+     *
+     * @param Mage_Customer_Model_Customer $customer
+     * @return array|string
+     */
+    protected function _getCustomerErrors($customer)
+    {
+        $errors = array();
+        $request = $this->getRequest();
+        if ($request->getPost('create_address')) {
+            $errors = $this->_getErrorsOnCustomerAddress($customer);
+        }
+        $customerForm = $this->_getCustomerForm($customer);
+        $customerData = $customerForm->extractData($request);
+        $customerErrors = $customerForm->validateData($customerData);
+        if ($customerErrors !== true) {
+            $errors = array_merge($customerErrors, $errors);
+        } else {
+            $customerForm->compactData($customerData);
+            $customer->setPassword($request->getPost('password'));
+            $customer->setConfirmation($request->getPost('confirmation'));
+            $customerErrors = $customer->validate();
+            if (is_array($customerErrors)) {
+                $errors = array_merge($customerErrors, $errors);
             }
+        }
+        return $errors;
+    }
 
-            try {
-                $customerErrors = $customerForm->validateData($customerData);
-                if ($customerErrors !== true) {
-                    $errors = array_merge($customerErrors, $errors);
-                } else {
-                    $customerForm->compactData($customerData);
-                    $customer->setPassword($this->getRequest()->getPost('password'));
-                    $customer->setConfirmation($this->getRequest()->getPost('confirmation'));
-                    $customerErrors = $customer->validate();
-                    if (is_array($customerErrors)) {
-                        $errors = array_merge($customerErrors, $errors);
-                    }
-                }
+    /**
+     * Get Customer Form Initalized Model
+     *
+     * @param Mage_Customer_Model_Customer $customer
+     * @return Mage_Customer_Model_Form
+     */
+    protected function _getCustomerForm($customer)
+    {
+        /* @var $customerForm Mage_Customer_Model_Form */
+        $customerForm = $this->_getModel('customer/form');
+        $customerForm->setFormCode('customer_account_create');
+        $customerForm->setEntity($customer);
+        return $customerForm;
+    }
 
-                $validationResult = count($errors) == 0;
+    /**
+     * Get Helper
+     *
+     * @param string $path
+     * @return Mage_Core_Helper_Abstract
+     */
+    protected function _getHelper($path)
+    {
+        return Mage::helper($path);
+    }
 
-                if (true === $validationResult) {
-                    $customer->save();
+    /**
+     * Get App
+     *
+     * @return Mage_Core_Model_App
+     */
+    protected function _getApp()
+    {
+        return Mage::app();
+    }
 
-                    Mage::dispatchEvent('customer_register_success',
-                        array('account_controller' => $this, 'customer' => $customer)
-                    );
-
-                    if ($customer->isConfirmationRequired()) {
-                        $customer->sendNewAccountEmail(
-                            'confirmation',
-                            $session->getBeforeAuthUrl(),
-                            Mage::app()->getStore()->getId()
-                        );
-                        $session->addSuccess($this->__('Account confirmation is required. Please, check your email for the confirmation link. To resend the confirmation email please <a href="%s">click here</a>.', Mage::helper('customer')->getEmailConfirmationUrl($customer->getEmail())));
-                        $this->_redirectSuccess(Mage::getUrl('*/*/index', array('_secure'=>true)));
-                        return;
-                    } else {
-                        $session->setCustomerAsLoggedIn($customer);
-                        $url = $this->_welcomeCustomer($customer);
-                        $this->_redirectSuccess($url);
-                        return;
-                    }
-                } else {
-                    $session->setCustomerFormData($this->getRequest()->getPost());
-                    if (is_array($errors)) {
-                        foreach ($errors as $errorMessage) {
-                            $session->addError(Mage::helper('core')->escapeHtml($errorMessage));
-                        }
-                    } else {
-                        $session->addError($this->__('Invalid customer data'));
-                    }
-                }
-            } catch (Mage_Core_Exception $e) {
-                $session->setCustomerFormData($this->getRequest()->getPost());
-                if ($e->getCode() === Mage_Customer_Model_Customer::EXCEPTION_EMAIL_EXISTS) {
-                    $url = Mage::getUrl('customer/account/forgotpassword');
-                    $message = $this->__('There is already an account with this email address. If you are sure that it is your email address, <a href="%s">click here</a> to get your password and access your account.', $url);
-                } else {
-                    $message = Mage::helper('core')->escapeHtml($e->getMessage());
-                }
-                $session->addError($message);
-            } catch (Exception $e) {
-                $session->setCustomerFormData($this->getRequest()->getPost())
-                    ->addException($e, $this->__('Cannot save the customer.'));
-            }
+    /**
+     * Dispatch Event
+     *
+     * @param Mage_Customer_Model_Customer $customer
+     */
+    protected function _dispatchRegisterSuccess($customer)
+    {
+        Mage::dispatchEvent('customer_register_success',
+            array('account_controller' => $this, 'customer' => $customer)
+        );
+    }
+
+    /**
+     * Get errors on provided customer address
+     *
+     * @param Mage_Customer_Model_Customer $customer
+     * @return array $errors
+     */
+    protected function _getErrorsOnCustomerAddress($customer)
+    {
+        $errors = array();
+        /* @var $address Mage_Customer_Model_Address */
+        $address = $this->_getModel('customer/address');
+        /* @var $addressForm Mage_Customer_Model_Form */
+        $addressForm = $this->_getModel('customer/form');
+        $addressForm->setFormCode('customer_register_address')
+            ->setEntity($address);
+
+        $addressData = $addressForm->extractData($this->getRequest(), 'address', false);
+        $addressErrors = $addressForm->validateData($addressData);
+        if (is_array($addressErrors)) {
+            $errors = $addressErrors;
         }
+        $address->setId(null)
+            ->setIsDefaultBilling($this->getRequest()->getParam('default_billing', false))
+            ->setIsDefaultShipping($this->getRequest()->getParam('default_shipping', false));
+        $addressForm->compactData($addressData);
+        $customer->addAddress($address);
+
+        $addressErrors = $address->validate();
+        if (is_array($addressErrors)) {
+            $errors = array_merge($errors, $addressErrors);
+        }
+        return $errors;
+    }
 
-        $this->_redirectError(Mage::getUrl('*/*/create', array('_secure' => true)));
+    /**
+     * Get model by path
+     *
+     * @param string $path
+     * @param array|null $arguments
+     * @return false|Mage_Core_Model_Abstract
+     */
+    public function _getModel($path, $arguments = array())
+    {
+        return Mage::getModel($path, $arguments);
+    }
+
+    /**
+     * Get model from registry by path
+     *
+     * @param string $path
+     * @return mixed
+     */
+    protected function _getFromRegistry($path)
+    {
+        return Mage::registry($path);
     }
 
     /**
@@ -403,14 +537,16 @@ class Mage_Customer_AccountController extends Mage_Core_Controller_Front_Action
         );
         if ($this->_isVatValidationEnabled()) {
             // Show corresponding VAT message to customer
-            $configAddressType = Mage::helper('customer/address')->getTaxCalculationAddressType();
+            $configAddressType = $this->_getHelper('customer/address')->getTaxCalculationAddressType();
             $userPrompt = '';
             switch ($configAddressType) {
                 case Mage_Customer_Model_Address_Abstract::TYPE_SHIPPING:
-                    $userPrompt = $this->__('If you are a registered VAT customer, please click <a href="%s">here</a> to enter you shipping address for proper VAT calculation', Mage::getUrl('customer/address/edit'));
+                    $userPrompt = $this->__('If you are a registered VAT customer, please click <a href="%s">here</a> to enter you shipping address for proper VAT calculation',
+                        $this->_getUrl('customer/address/edit'));
                     break;
                 default:
-                    $userPrompt = $this->__('If you are a registered VAT customer, please click <a href="%s">here</a> to enter you billing address for proper VAT calculation', Mage::getUrl('customer/address/edit'));
+                    $userPrompt = $this->__('If you are a registered VAT customer, please click <a href="%s">here</a> to enter you billing address for proper VAT calculation',
+                        $this->_getUrl('customer/address/edit'));
             }
             $this->_getSession()->addSuccess($userPrompt);
         }
@@ -421,7 +557,7 @@ class Mage_Customer_AccountController extends Mage_Core_Controller_Front_Action
             Mage::app()->getStore()->getId()
         );
 
-        $successUrl = Mage::getUrl('*/*/index', array('_secure'=>true));
+        $successUrl = $this->_getUrl('*/*/index', array('_secure' => true));
         if ($this->_getSession()->getBeforeAuthUrl()) {
             $successUrl = $this->_getSession()->getBeforeAuthUrl(true);
         }
@@ -433,7 +569,8 @@ class Mage_Customer_AccountController extends Mage_Core_Controller_Front_Action
      */
     public function confirmAction()
     {
-        if ($this->_getSession()->isLoggedIn()) {
+        $session = $this->_getSession();
+        if ($session->isLoggedIn()) {
             $this->_redirect('*/*/');
             return;
         }
@@ -447,7 +584,7 @@ class Mage_Customer_AccountController extends Mage_Core_Controller_Front_Action
 
             // load customer by id (try/catch in case if it throws exceptions)
             try {
-                $customer = Mage::getModel('customer/customer')->load($id);
+                $customer = $this->_getModel('customer/customer')->load($id);
                 if ((!$customer) || (!$customer->getId())) {
                     throw new Exception('Failed to load customer by id.');
                 }
@@ -471,21 +608,22 @@ class Mage_Customer_AccountController extends Mage_Core_Controller_Front_Action
                     throw new Exception($this->__('Failed to confirm customer account.'));
                 }
 
+                $session->renewSession();
                 // log in and send greeting email, then die happy
-                $this->_getSession()->setCustomerAsLoggedIn($customer);
+                $session->setCustomerAsLoggedIn($customer);
                 $successUrl = $this->_welcomeCustomer($customer, true);
                 $this->_redirectSuccess($backUrl ? $backUrl : $successUrl);
                 return;
             }
 
             // die happy
-            $this->_redirectSuccess(Mage::getUrl('*/*/index', array('_secure'=>true)));
+            $this->_redirectSuccess($this->_getUrl('*/*/index', array('_secure' => true)));
             return;
         }
         catch (Exception $e) {
             // die unhappy
             $this->_getSession()->addError($e->getMessage());
-            $this->_redirectError(Mage::getUrl('*/*/index', array('_secure'=>true)));
+            $this->_redirectError($this->_getUrl('*/*/index', array('_secure' => true)));
             return;
         }
     }
@@ -495,7 +633,7 @@ class Mage_Customer_AccountController extends Mage_Core_Controller_Front_Action
      */
     public function confirmationAction()
     {
-        $customer = Mage::getModel('customer/customer');
+        $customer = $this->_getModel('customer/customer');
         if ($this->_getSession()->isLoggedIn()) {
             $this->_redirect('*/*/');
             return;
@@ -516,10 +654,10 @@ class Mage_Customer_AccountController extends Mage_Core_Controller_Front_Action
                     $this->_getSession()->addSuccess($this->__('This email does not require confirmation.'));
                 }
                 $this->_getSession()->setUsername($email);
-                $this->_redirectSuccess(Mage::getUrl('*/*/index', array('_secure' => true)));
+                $this->_redirectSuccess($this->_getUrl('*/*/index', array('_secure' => true)));
             } catch (Exception $e) {
                 $this->_getSession()->addException($e, $this->__('Wrong email.'));
-                $this->_redirectError(Mage::getUrl('*/*/*', array('email' => $email, '_secure' => true)));
+                $this->_redirectError($this->_getUrl('*/*/*', array('email' => $email, '_secure' => true)));
             }
             return;
         }
@@ -535,6 +673,18 @@ class Mage_Customer_AccountController extends Mage_Core_Controller_Front_Action
     }
 
     /**
+     * Get Url method
+     *
+     * @param string $url
+     * @param array $params
+     * @return string
+     */
+    protected function _getUrl($url, $params = array())
+    {
+        return Mage::getUrl($url, $params);
+    }
+
+    /**
      * Forgot customer password page
      */
     public function forgotPasswordAction()
@@ -565,13 +715,13 @@ class Mage_Customer_AccountController extends Mage_Core_Controller_Front_Action
             }
 
             /** @var $customer Mage_Customer_Model_Customer */
-            $customer = Mage::getModel('customer/customer')
+            $customer = $this->_getModel('customer/customer')
                 ->setWebsiteId(Mage::app()->getStore()->getWebsiteId())
                 ->loadByEmail($email);
 
             if ($customer->getId()) {
                 try {
-                    $newResetPasswordLinkToken = Mage::helper('customer')->generateResetPasswordLinkToken();
+                    $newResetPasswordLinkToken = $this->_getHelper('customer')->generateResetPasswordLinkToken();
                     $customer->changeResetPasswordLinkToken($newResetPasswordLinkToken);
                     $customer->sendPasswordResetConfirmationEmail();
                 } catch (Exception $exception) {
@@ -581,7 +731,9 @@ class Mage_Customer_AccountController extends Mage_Core_Controller_Front_Action
                 }
             }
             $this->_getSession()
-                ->addSuccess(Mage::helper('customer')->__('If there is an account associated with %s you will receive an email with a link to reset your password.', Mage::helper('customer')->htmlEscape($email)));
+                ->addSuccess($this->_getHelper('customer')
+                    ->__('If there is an account associated with %s you will receive an email with a link to reset your password.',
+                        $this->_getHelper('customer')->escapeHtml($email)));
             $this->_redirect('*/*/');
             return;
         } else {
@@ -626,16 +778,14 @@ class Mage_Customer_AccountController extends Mage_Core_Controller_Front_Action
                 ->_redirect('*/*/changeforgotten');
 
         } catch (Exception $exception) {
-            $this->_getSession()->addError(Mage::helper('customer')->__('Your password reset link has expired.'));
+            $this->_getSession()->addError($this->_getHelper('customer')->__('Your password reset link has expired.'));
             $this->_redirect('*/*/forgotpassword');
         }
     }
 
     /**
      * Reset forgotten password
-     *
      * Used to handle data recieved from reset forgotten password form
-     *
      */
     public function resetPasswordPostAction()
     {
@@ -646,17 +796,17 @@ class Mage_Customer_AccountController extends Mage_Core_Controller_Front_Action
         try {
             $this->_validateResetPasswordLinkToken($customerId, $resetPasswordLinkToken);
         } catch (Exception $exception) {
-            $this->_getSession()->addError(Mage::helper('customer')->__('Your password reset link has expired.'));
+            $this->_getSession()->addError($this->_getHelper('customer')->__('Your password reset link has expired.'));
             $this->_redirect('*/*/');
             return;
         }
 
         $errorMessages = array();
         if (iconv_strlen($password) <= 0) {
-            array_push($errorMessages, Mage::helper('customer')->__('New password field cannot be empty.'));
+            array_push($errorMessages, $this->_getHelper('customer')->__('New password field cannot be empty.'));
         }
         /** @var $customer Mage_Customer_Model_Customer */
-        $customer = Mage::getModel('customer/customer')->load($customerId);
+        $customer = $this->_getModel('customer/customer')->load($customerId);
 
         $customer->setPassword($password);
         $customer->setConfirmation($passwordConfirmation);
@@ -684,7 +834,7 @@ class Mage_Customer_AccountController extends Mage_Core_Controller_Front_Action
             $this->_getSession()->unsetData(self::TOKEN_SESSION_NAME);
             $this->_getSession()->unsetData(self::CUSTOMER_ID_SESSION_NAME);
 
-            $this->_getSession()->addSuccess(Mage::helper('customer')->__('Your password has been updated.'));
+            $this->_getSession()->addSuccess($this->_getHelper('customer')->__('Your password has been updated.'));
             $this->_redirect('*/*/login');
         } catch (Exception $exception) {
             $this->_getSession()->addException($exception, $this->__('Cannot save a new password.'));
@@ -708,18 +858,18 @@ class Mage_Customer_AccountController extends Mage_Core_Controller_Front_Action
             || empty($customerId)
             || $customerId < 0
         ) {
-            throw Mage::exception('Mage_Core', Mage::helper('customer')->__('Invalid password reset token.'));
+            throw Mage::exception('Mage_Core', $this->_getHelper('customer')->__('Invalid password reset token.'));
         }
 
         /** @var $customer Mage_Customer_Model_Customer */
-        $customer = Mage::getModel('customer/customer')->load($customerId);
+        $customer = $this->_getModel('customer/customer')->load($customerId);
         if (!$customer || !$customer->getId()) {
-            throw Mage::exception('Mage_Core', Mage::helper('customer')->__('Wrong customer account specified.'));
+            throw Mage::exception('Mage_Core', $this->_getHelper('customer')->__('Wrong customer account specified.'));
         }
 
         $customerToken = $customer->getRpToken();
         if (strcmp($customerToken, $resetPasswordLinkToken) != 0 || $customer->isResetPasswordLinkTokenExpired()) {
-            throw Mage::exception('Mage_Core', Mage::helper('customer')->__('Your password reset link has expired.'));
+            throw Mage::exception('Mage_Core', $this->_getHelper('customer')->__('Your password reset link has expired.'));
         }
     }
 
@@ -741,7 +891,7 @@ class Mage_Customer_AccountController extends Mage_Core_Controller_Front_Action
         if (!empty($data)) {
             $customer->addData($data);
         }
-        if ($this->getRequest()->getParam('changepass')==1){
+        if ($this->getRequest()->getParam('changepass') == 1) {
             $customer->setChangePassword(1);
         }
 
@@ -764,7 +914,7 @@ class Mage_Customer_AccountController extends Mage_Core_Controller_Front_Action
             $customer = $this->_getSession()->getCustomer();
 
             /** @var $customerForm Mage_Customer_Model_Form */
-            $customerForm = Mage::getModel('customer/form');
+            $customerForm = $this->_getModel('customer/form');
             $customerForm->setFormCode('customer_account_edit')
                 ->setEntity($customer);
 
@@ -785,7 +935,7 @@ class Mage_Customer_AccountController extends Mage_Core_Controller_Front_Action
                     $confPass   = $this->getRequest()->getPost('confirmation');
 
                     $oldPass = $this->_getSession()->getCustomer()->getPasswordHash();
-                    if (Mage::helper('core/string')->strpos($oldPass, ':')) {
+                    if ($this->_getHelper('core/string')->strpos($oldPass, ':')) {
                         list($_salt, $salt) = explode(':', $oldPass);
                     } else {
                         $salt = false;
@@ -863,7 +1013,7 @@ class Mage_Customer_AccountController extends Mage_Core_Controller_Front_Action
      */
     protected function _isVatValidationEnabled($store = null)
     {
-        return Mage::helper('customer/address')->isVatValidationEnabled($store);
+        return $this->_getHelper('customer/address')->isVatValidationEnabled($store);
     }
 
     /**
diff --git app/code/core/Mage/Customer/controllers/AddressController.php app/code/core/Mage/Customer/controllers/AddressController.php
index 22dcc23..ab0916f 100644
--- app/code/core/Mage/Customer/controllers/AddressController.php
+++ app/code/core/Mage/Customer/controllers/AddressController.php
@@ -163,6 +163,9 @@ class Mage_Customer_AddressController extends Mage_Core_Controller_Front_Action
 
     public function deleteAction()
     {
+        if (!$this->_validateFormKey()) {
+            return $this->_redirect('*/*/');
+        }
         $addressId = $this->getRequest()->getParam('id', false);
 
         if ($addressId) {
diff --git app/code/core/Mage/Dataflow/Model/Profile.php app/code/core/Mage/Dataflow/Model/Profile.php
index b6062cd..20bdf0e 100644
--- app/code/core/Mage/Dataflow/Model/Profile.php
+++ app/code/core/Mage/Dataflow/Model/Profile.php
@@ -64,10 +64,14 @@ class Mage_Dataflow_Model_Profile extends Mage_Core_Model_Abstract
 
     protected function _afterLoad()
     {
+        $guiData = '';
         if (is_string($this->getGuiData())) {
-            $guiData = unserialize($this->getGuiData());
-        } else {
-            $guiData = '';
+            try {
+                $guiData = Mage::helper('core/unserializeArray')
+                    ->unserialize($this->getGuiData());
+            } catch (Exception $e) {
+                Mage::logException($e);
+            }
         }
         $this->setGuiData($guiData);
 
@@ -127,7 +131,13 @@ class Mage_Dataflow_Model_Profile extends Mage_Core_Model_Abstract
     protected function _afterSave()
     {
         if (is_string($this->getGuiData())) {
-            $this->setGuiData(unserialize($this->getGuiData()));
+            try {
+                $guiData = Mage::helper('core/unserializeArray')
+                    ->unserialize($this->getGuiData());
+                $this->setGuiData($guiData);
+            } catch (Exception $e) {
+                Mage::logException($e);
+            }
         }
 
         $profileHistory = Mage::getModel('dataflow/profile_history');
diff --git app/code/core/Mage/Downloadable/Block/Adminhtml/Catalog/Product/Edit/Tab/Downloadable/Links.php app/code/core/Mage/Downloadable/Block/Adminhtml/Catalog/Product/Edit/Tab/Downloadable/Links.php
index 6156e3b..d88c7fd 100644
--- app/code/core/Mage/Downloadable/Block/Adminhtml/Catalog/Product/Edit/Tab/Downloadable/Links.php
+++ app/code/core/Mage/Downloadable/Block/Adminhtml/Catalog/Product/Edit/Tab/Downloadable/Links.php
@@ -32,7 +32,7 @@
  * @author      Magento Core Team <core@magentocommerce.com>
  */
 class Mage_Downloadable_Block_Adminhtml_Catalog_Product_Edit_Tab_Downloadable_Links
-    extends Mage_Adminhtml_Block_Template
+    extends Mage_Uploader_Block_Single
 {
     /**
      * Purchased Separately Attribute cache
@@ -245,6 +245,7 @@ class Mage_Downloadable_Block_Adminhtml_Catalog_Product_Edit_Tab_Downloadable_Li
      */
      protected function _prepareLayout()
     {
+        parent::_prepareLayout();
         $this->setChild(
             'upload_button',
             $this->getLayout()->createBlock('adminhtml/widget_button')->addData(array(
@@ -254,6 +255,10 @@ class Mage_Downloadable_Block_Adminhtml_Catalog_Product_Edit_Tab_Downloadable_Li
                 'onclick' => 'Downloadable.massUploadByType(\'links\');Downloadable.massUploadByType(\'linkssample\')'
             ))
         );
+        $this->_addElementIdsMapping(array(
+            'container' => $this->getHtmlId() . '-new',
+            'delete'    => $this->getHtmlId() . '-delete'
+        ));
     }
 
     /**
@@ -273,33 +278,56 @@ class Mage_Downloadable_Block_Adminhtml_Catalog_Product_Edit_Tab_Downloadable_Li
      */
     public function getConfigJson($type='links')
     {
-        $this->getConfig()->setUrl(Mage::getModel('adminhtml/url')->addSessionParam()
-            ->getUrl('*/downloadable_file/upload', array('type' => $type, '_secure' => true)));
-        $this->getConfig()->setParams(array('form_key' => $this->getFormKey()));
-        $this->getConfig()->setFileField($type);
-        $this->getConfig()->setFilters(array(
-            'all'    => array(
-                'label' => Mage::helper('adminhtml')->__('All Files'),
-                'files' => array('*.*')
+
+        $this->getUploaderConfig()
+            ->setFileParameterName($type)
+            ->setTarget(
+                Mage::getModel('adminhtml/url')
+                    ->addSessionParam()
+                    ->getUrl('*/downloadable_file/upload', array('type' => $type, '_secure' => true))
+            );
+        $this->getMiscConfig()
+            ->setReplaceBrowseWithRemove(true)
+        ;
+        return Mage::helper('core')->jsonEncode(parent::getJsonConfig());
+    }
+
+    /**
+     * @return string
+     */
+    public function getBrowseButtonHtml($type = '')
+    {
+        return $this->getChild('browse_button')
+            // Workaround for IE9
+            ->setBeforeHtml(
+                '<div style="display:inline-block; " id="downloadable_link_{{id}}_' . $type . 'file-browse">'
             )
-        ));
-        $this->getConfig()->setReplaceBrowseWithRemove(true);
-        $this->getConfig()->setWidth('32');
-        $this->getConfig()->setHideUploadButton(true);
-        return Mage::helper('core')->jsonEncode($this->getConfig()->getData());
+            ->setAfterHtml('</div>')
+            ->setId('downloadable_link_{{id}}_' . $type . 'file-browse_button')
+            ->toHtml();
     }
 
+
     /**
-     * Retrive config object
+     * @return string
+     */
+    public function getDeleteButtonHtml($type = '')
+    {
+        return $this->getChild('delete_button')
+            ->setLabel('')
+            ->setId('downloadable_link_{{id}}_' . $type . 'file-delete')
+            ->setStyle('display:none; width:31px;')
+            ->toHtml();
+    }
+
+    /**
+     * Retrieve config object
      *
-     * @return Varien_Config
+     * @deprecated
+     * @return $this
      */
     public function getConfig()
     {
-        if(is_null($this->_config)) {
-            $this->_config = new Varien_Object();
-        }
-
-        return $this->_config;
+        return $this;
     }
 }
diff --git app/code/core/Mage/Downloadable/Block/Adminhtml/Catalog/Product/Edit/Tab/Downloadable/Samples.php app/code/core/Mage/Downloadable/Block/Adminhtml/Catalog/Product/Edit/Tab/Downloadable/Samples.php
index 94e9040..0d2b560 100644
--- app/code/core/Mage/Downloadable/Block/Adminhtml/Catalog/Product/Edit/Tab/Downloadable/Samples.php
+++ app/code/core/Mage/Downloadable/Block/Adminhtml/Catalog/Product/Edit/Tab/Downloadable/Samples.php
@@ -32,7 +32,7 @@
  * @author      Magento Core Team <core@magentocommerce.com>
  */
 class Mage_Downloadable_Block_Adminhtml_Catalog_Product_Edit_Tab_Downloadable_Samples
-    extends Mage_Adminhtml_Block_Widget
+    extends Mage_Uploader_Block_Single
 {
     /**
      * Class constructor
@@ -148,6 +148,7 @@ class Mage_Downloadable_Block_Adminhtml_Catalog_Product_Edit_Tab_Downloadable_Sa
      */
     protected function _prepareLayout()
     {
+        parent::_prepareLayout();
         $this->setChild(
             'upload_button',
             $this->getLayout()->createBlock('adminhtml/widget_button')
@@ -158,6 +159,11 @@ class Mage_Downloadable_Block_Adminhtml_Catalog_Product_Edit_Tab_Downloadable_Sa
                     'onclick' => 'Downloadable.massUploadByType(\'samples\')'
                 ))
         );
+
+        $this->_addElementIdsMapping(array(
+            'container' => $this->getHtmlId() . '-new',
+            'delete'    => $this->getHtmlId() . '-delete'
+        ));
     }
 
     /**
@@ -171,40 +177,59 @@ class Mage_Downloadable_Block_Adminhtml_Catalog_Product_Edit_Tab_Downloadable_Sa
     }
 
     /**
-     * Retrive config json
+     * Retrieve config json
      *
      * @return string
      */
     public function getConfigJson()
     {
-        $this->getConfig()->setUrl(Mage::getModel('adminhtml/url')
-            ->addSessionParam()
-            ->getUrl('*/downloadable_file/upload', array('type' => 'samples', '_secure' => true)));
-        $this->getConfig()->setParams(array('form_key' => $this->getFormKey()));
-        $this->getConfig()->setFileField('samples');
-        $this->getConfig()->setFilters(array(
-            'all'    => array(
-                'label' => Mage::helper('adminhtml')->__('All Files'),
-                'files' => array('*.*')
-            )
-        ));
-        $this->getConfig()->setReplaceBrowseWithRemove(true);
-        $this->getConfig()->setWidth('32');
-        $this->getConfig()->setHideUploadButton(true);
-        return Mage::helper('core')->jsonEncode($this->getConfig()->getData());
+        $this->getUploaderConfig()
+            ->setFileParameterName('samples')
+            ->setTarget(
+                Mage::getModel('adminhtml/url')
+                    ->addSessionParam()
+                    ->getUrl('*/downloadable_file/upload', array('type' => 'samples', '_secure' => true))
+            );
+        $this->getMiscConfig()
+            ->setReplaceBrowseWithRemove(true)
+        ;
+        return Mage::helper('core')->jsonEncode(parent::getJsonConfig());
     }
 
     /**
-     * Retrive config object
+     * @return string
+     */
+    public function getBrowseButtonHtml()
+    {
+        return $this->getChild('browse_button')
+            // Workaround for IE9
+            ->setBeforeHtml('<div style="display:inline-block; " id="downloadable_sample_{{id}}_file-browse">')
+            ->setAfterHtml('</div>')
+            ->setId('downloadable_sample_{{id}}_file-browse_button')
+            ->toHtml();
+    }
+
+
+    /**
+     * @return string
+     */
+    public function getDeleteButtonHtml()
+    {
+        return $this->getChild('delete_button')
+            ->setLabel('')
+            ->setId('downloadable_sample_{{id}}_file-delete')
+            ->setStyle('display:none; width:31px;')
+            ->toHtml();
+    }
+
+    /**
+     * Retrieve config object
      *
-     * @return Varien_Config
+     * @deprecated
+     * @return $this
      */
     public function getConfig()
     {
-        if(is_null($this->_config)) {
-            $this->_config = new Varien_Object();
-        }
-
-        return $this->_config;
+        return $this;
     }
 }
diff --git app/code/core/Mage/Downloadable/Helper/File.php app/code/core/Mage/Downloadable/Helper/File.php
index 8ec4dfb..6241fb9 100644
--- app/code/core/Mage/Downloadable/Helper/File.php
+++ app/code/core/Mage/Downloadable/Helper/File.php
@@ -33,15 +33,35 @@
  */
 class Mage_Downloadable_Helper_File extends Mage_Core_Helper_Abstract
 {
+    /**
+     * @see Mage_Uploader_Helper_File::getMimeTypes
+     * @var array
+     */
+    protected $_mimeTypes;
+
+    /**
+     * @var Mage_Uploader_Helper_File
+     */
+    protected $_fileHelper;
+
+    /**
+     * Populate self::_mimeTypes array with values that set in config or pre-defined
+     */
     public function __construct()
     {
-        $nodes = Mage::getConfig()->getNode('global/mime/types');
-        if ($nodes) {
-            $nodes = (array)$nodes;
-            foreach ($nodes as $key => $value) {
-                self::$_mimeTypes[$key] = $value;
-            }
+        $this->_mimeTypes = $this->_getFileHelper()->getMimeTypes();
+    }
+
+    /**
+     * @return Mage_Uploader_Helper_File
+     */
+    protected function _getFileHelper()
+    {
+        if (!$this->_fileHelper) {
+            $this->_fileHelper = Mage::helper('uploader/file');
         }
+
+        return $this->_fileHelper;
     }
 
     /**
@@ -152,628 +172,48 @@ class Mage_Downloadable_Helper_File extends Mage_Core_Helper_Abstract
         return $file;
     }
 
+    /**
+     * Get MIME type for $filePath
+     *
+     * @param $filePath
+     * @return string
+     */
     public function getFileType($filePath)
     {
         $ext = substr($filePath, strrpos($filePath, '.')+1);
         return $this->_getFileTypeByExt($ext);
     }
 
+    /**
+     * Get MIME type by file extension
+     *
+     * @param $ext
+     * @return string
+     * @deprecated
+     */
     protected function _getFileTypeByExt($ext)
     {
-        $type = 'x' . $ext;
-        if (isset(self::$_mimeTypes[$type])) {
-            return self::$_mimeTypes[$type];
-        }
-        return 'application/octet-stream';
+        return $this->_getFileHelper()->getMimeTypeByExtension($ext);
     }
 
+    /**
+     * Get all MIME types
+     *
+     * @return array
+     */
     public function getAllFileTypes()
     {
-        return array_values(self::getAllMineTypes());
+        return array_values($this->getAllMineTypes());
     }
 
+    /**
+     * Get list of all MIME types
+     *
+     * @return array
+     */
     public function getAllMineTypes()
     {
-        return self::$_mimeTypes;
+        return $this->_mimeTypes;
     }
 
-    protected static $_mimeTypes =
-        array(
-            'x123' => 'application/vnd.lotus-1-2-3',
-            'x3dml' => 'text/vnd.in3d.3dml',
-            'x3g2' => 'video/3gpp2',
-            'x3gp' => 'video/3gpp',
-            'xace' => 'application/x-ace-compressed',
-            'xacu' => 'application/vnd.acucobol',
-            'xaep' => 'application/vnd.audiograph',
-            'xai' => 'application/postscript',
-            'xaif' => 'audio/x-aiff',
-
-            'xaifc' => 'audio/x-aiff',
-            'xaiff' => 'audio/x-aiff',
-            'xami' => 'application/vnd.amiga.ami',
-            'xapr' => 'application/vnd.lotus-approach',
-            'xasf' => 'video/x-ms-asf',
-            'xaso' => 'application/vnd.accpac.simply.aso',
-            'xasx' => 'video/x-ms-asf',
-            'xatom' => 'application/atom+xml',
-            'xatomcat' => 'application/atomcat+xml',
-
-            'xatomsvc' => 'application/atomsvc+xml',
-            'xatx' => 'application/vnd.antix.game-component',
-            'xau' => 'audio/basic',
-            'xavi' => 'video/x-msvideo',
-            'xbat' => 'application/x-msdownload',
-            'xbcpio' => 'application/x-bcpio',
-            'xbdm' => 'application/vnd.syncml.dm+wbxml',
-            'xbh2' => 'application/vnd.fujitsu.oasysprs',
-            'xbmi' => 'application/vnd.bmi',
-
-            'xbmp' => 'image/bmp',
-            'xbox' => 'application/vnd.previewsystems.box',
-            'xboz' => 'application/x-bzip2',
-            'xbtif' => 'image/prs.btif',
-            'xbz' => 'application/x-bzip',
-            'xbz2' => 'application/x-bzip2',
-            'xcab' => 'application/vnd.ms-cab-compressed',
-            'xccxml' => 'application/ccxml+xml',
-            'xcdbcmsg' => 'application/vnd.contact.cmsg',
-
-            'xcdkey' => 'application/vnd.mediastation.cdkey',
-            'xcdx' => 'chemical/x-cdx',
-            'xcdxml' => 'application/vnd.chemdraw+xml',
-            'xcdy' => 'application/vnd.cinderella',
-            'xcer' => 'application/pkix-cert',
-            'xcgm' => 'image/cgm',
-            'xchat' => 'application/x-chat',
-            'xchm' => 'application/vnd.ms-htmlhelp',
-            'xchrt' => 'application/vnd.kde.kchart',
-
-            'xcif' => 'chemical/x-cif',
-            'xcii' => 'application/vnd.anser-web-certificate-issue-initiation',
-            'xcil' => 'application/vnd.ms-artgalry',
-            'xcla' => 'application/vnd.claymore',
-            'xclkk' => 'application/vnd.crick.clicker.keyboard',
-            'xclkp' => 'application/vnd.crick.clicker.palette',
-            'xclkt' => 'application/vnd.crick.clicker.template',
-            'xclkw' => 'application/vnd.crick.clicker.wordbank',
-            'xclkx' => 'application/vnd.crick.clicker',
-
-            'xclp' => 'application/x-msclip',
-            'xcmc' => 'application/vnd.cosmocaller',
-            'xcmdf' => 'chemical/x-cmdf',
-            'xcml' => 'chemical/x-cml',
-            'xcmp' => 'application/vnd.yellowriver-custom-menu',
-            'xcmx' => 'image/x-cmx',
-            'xcom' => 'application/x-msdownload',
-            'xconf' => 'text/plain',
-            'xcpio' => 'application/x-cpio',
-
-            'xcpt' => 'application/mac-compactpro',
-            'xcrd' => 'application/x-mscardfile',
-            'xcrl' => 'application/pkix-crl',
-            'xcrt' => 'application/x-x509-ca-cert',
-            'xcsh' => 'application/x-csh',
-            'xcsml' => 'chemical/x-csml',
-            'xcss' => 'text/css',
-            'xcsv' => 'text/csv',
-            'xcurl' => 'application/vnd.curl',
-
-            'xcww' => 'application/prs.cww',
-            'xdaf' => 'application/vnd.mobius.daf',
-            'xdavmount' => 'application/davmount+xml',
-            'xdd2' => 'application/vnd.oma.dd2+xml',
-            'xddd' => 'application/vnd.fujixerox.ddd',
-            'xdef' => 'text/plain',
-            'xder' => 'application/x-x509-ca-cert',
-            'xdfac' => 'application/vnd.dreamfactory',
-            'xdis' => 'application/vnd.mobius.dis',
-
-            'xdjv' => 'image/vnd.djvu',
-            'xdjvu' => 'image/vnd.djvu',
-            'xdll' => 'application/x-msdownload',
-            'xdna' => 'application/vnd.dna',
-            'xdoc' => 'application/msword',
-            'xdot' => 'application/msword',
-            'xdp' => 'application/vnd.osgi.dp',
-            'xdpg' => 'application/vnd.dpgraph',
-            'xdsc' => 'text/prs.lines.tag',
-
-            'xdtd' => 'application/xml-dtd',
-            'xdvi' => 'application/x-dvi',
-            'xdwf' => 'model/vnd.dwf',
-            'xdwg' => 'image/vnd.dwg',
-            'xdxf' => 'image/vnd.dxf',
-            'xdxp' => 'application/vnd.spotfire.dxp',
-            'xecelp4800' => 'audio/vnd.nuera.ecelp4800',
-            'xecelp7470' => 'audio/vnd.nuera.ecelp7470',
-            'xecelp9600' => 'audio/vnd.nuera.ecelp9600',
-
-            'xecma' => 'application/ecmascript',
-            'xedm' => 'application/vnd.novadigm.edm',
-            'xedx' => 'application/vnd.novadigm.edx',
-            'xefif' => 'application/vnd.picsel',
-            'xei6' => 'application/vnd.pg.osasli',
-            'xeml' => 'message/rfc822',
-            'xeol' => 'audio/vnd.digital-winds',
-            'xeot' => 'application/vnd.ms-fontobject',
-            'xeps' => 'application/postscript',
-
-            'xesf' => 'application/vnd.epson.esf',
-            'xetx' => 'text/x-setext',
-            'xexe' => 'application/x-msdownload',
-            'xext' => 'application/vnd.novadigm.ext',
-            'xez' => 'application/andrew-inset',
-            'xez2' => 'application/vnd.ezpix-album',
-            'xez3' => 'application/vnd.ezpix-package',
-            'xfbs' => 'image/vnd.fastbidsheet',
-            'xfdf' => 'application/vnd.fdf',
-
-            'xfe_launch' => 'application/vnd.denovo.fcselayout-link',
-            'xfg5' => 'application/vnd.fujitsu.oasysgp',
-            'xfli' => 'video/x-fli',
-            'xflo' => 'application/vnd.micrografx.flo',
-            'xflw' => 'application/vnd.kde.kivio',
-            'xflx' => 'text/vnd.fmi.flexstor',
-            'xfly' => 'text/vnd.fly',
-            'xfnc' => 'application/vnd.frogans.fnc',
-            'xfpx' => 'image/vnd.fpx',
-
-            'xfsc' => 'application/vnd.fsc.weblaunch',
-            'xfst' => 'image/vnd.fst',
-            'xftc' => 'application/vnd.fluxtime.clip',
-            'xfti' => 'application/vnd.anser-web-funds-transfer-initiation',
-            'xfvt' => 'video/vnd.fvt',
-            'xfzs' => 'application/vnd.fuzzysheet',
-            'xg3' => 'image/g3fax',
-            'xgac' => 'application/vnd.groove-account',
-            'xgdl' => 'model/vnd.gdl',
-
-            'xghf' => 'application/vnd.groove-help',
-            'xgif' => 'image/gif',
-            'xgim' => 'application/vnd.groove-identity-message',
-            'xgph' => 'application/vnd.flographit',
-            'xgram' => 'application/srgs',
-            'xgrv' => 'application/vnd.groove-injector',
-            'xgrxml' => 'application/srgs+xml',
-            'xgtar' => 'application/x-gtar',
-            'xgtm' => 'application/vnd.groove-tool-message',
-
-            'xgtw' => 'model/vnd.gtw',
-            'xh261' => 'video/h261',
-            'xh263' => 'video/h263',
-            'xh264' => 'video/h264',
-            'xhbci' => 'application/vnd.hbci',
-            'xhdf' => 'application/x-hdf',
-            'xhlp' => 'application/winhlp',
-            'xhpgl' => 'application/vnd.hp-hpgl',
-            'xhpid' => 'application/vnd.hp-hpid',
-
-            'xhps' => 'application/vnd.hp-hps',
-            'xhqx' => 'application/mac-binhex40',
-            'xhtke' => 'application/vnd.kenameaapp',
-            'xhtm' => 'text/html',
-            'xhtml' => 'text/html',
-            'xhvd' => 'application/vnd.yamaha.hv-dic',
-            'xhvp' => 'application/vnd.yamaha.hv-voice',
-            'xhvs' => 'application/vnd.yamaha.hv-script',
-            'xice' => '#x-conference/x-cooltalk',
-
-            'xico' => 'image/x-icon',
-            'xics' => 'text/calendar',
-            'xief' => 'image/ief',
-            'xifb' => 'text/calendar',
-            'xifm' => 'application/vnd.shana.informed.formdata',
-            'xigl' => 'application/vnd.igloader',
-            'xigx' => 'application/vnd.micrografx.igx',
-            'xiif' => 'application/vnd.shana.informed.interchange',
-            'ximp' => 'application/vnd.accpac.simply.imp',
-
-            'xims' => 'application/vnd.ms-ims',
-            'xin' => 'text/plain',
-            'xipk' => 'application/vnd.shana.informed.package',
-            'xirm' => 'application/vnd.ibm.rights-management',
-            'xirp' => 'application/vnd.irepository.package+xml',
-            'xitp' => 'application/vnd.shana.informed.formtemplate',
-            'xivp' => 'application/vnd.immervision-ivp',
-            'xivu' => 'application/vnd.immervision-ivu',
-            'xjad' => 'text/vnd.sun.j2me.app-descriptor',
-
-            'xjam' => 'application/vnd.jam',
-            'xjava' => 'text/x-java-source',
-            'xjisp' => 'application/vnd.jisp',
-            'xjlt' => 'application/vnd.hp-jlyt',
-            'xjoda' => 'application/vnd.joost.joda-archive',
-            'xjpe' => 'image/jpeg',
-            'xjpeg' => 'image/jpeg',
-            'xjpg' => 'image/jpeg',
-            'xjpgm' => 'video/jpm',
-
-            'xjpgv' => 'video/jpeg',
-            'xjpm' => 'video/jpm',
-            'xjs' => 'application/javascript',
-            'xjson' => 'application/json',
-            'xkar' => 'audio/midi',
-            'xkarbon' => 'application/vnd.kde.karbon',
-            'xkfo' => 'application/vnd.kde.kformula',
-            'xkia' => 'application/vnd.kidspiration',
-            'xkml' => 'application/vnd.google-earth.kml+xml',
-
-            'xkmz' => 'application/vnd.google-earth.kmz',
-            'xkon' => 'application/vnd.kde.kontour',
-            'xksp' => 'application/vnd.kde.kspread',
-            'xlatex' => 'application/x-latex',
-            'xlbd' => 'application/vnd.llamagraphics.life-balance.desktop',
-            'xlbe' => 'application/vnd.llamagraphics.life-balance.exchange+xml',
-            'xles' => 'application/vnd.hhe.lesson-player',
-            'xlist' => 'text/plain',
-            'xlog' => 'text/plain',
-
-            'xlrm' => 'application/vnd.ms-lrm',
-            'xltf' => 'application/vnd.frogans.ltf',
-            'xlvp' => 'audio/vnd.lucent.voice',
-            'xlwp' => 'application/vnd.lotus-wordpro',
-            'xm13' => 'application/x-msmediaview',
-            'xm14' => 'application/x-msmediaview',
-            'xm1v' => 'video/mpeg',
-            'xm2a' => 'audio/mpeg',
-            'xm3a' => 'audio/mpeg',
-
-            'xm3u' => 'audio/x-mpegurl',
-            'xm4u' => 'video/vnd.mpegurl',
-            'xmag' => 'application/vnd.ecowin.chart',
-            'xmathml' => 'application/mathml+xml',
-            'xmbk' => 'application/vnd.mobius.mbk',
-            'xmbox' => 'application/mbox',
-            'xmc1' => 'application/vnd.medcalcdata',
-            'xmcd' => 'application/vnd.mcd',
-            'xmdb' => 'application/x-msaccess',
-
-            'xmdi' => 'image/vnd.ms-modi',
-            'xmesh' => 'model/mesh',
-            'xmfm' => 'application/vnd.mfmp',
-            'xmgz' => 'application/vnd.proteus.magazine',
-            'xmid' => 'audio/midi',
-            'xmidi' => 'audio/midi',
-            'xmif' => 'application/vnd.mif',
-            'xmime' => 'message/rfc822',
-            'xmj2' => 'video/mj2',
-
-            'xmjp2' => 'video/mj2',
-            'xmlp' => 'application/vnd.dolby.mlp',
-            'xmmd' => 'application/vnd.chipnuts.karaoke-mmd',
-            'xmmf' => 'application/vnd.smaf',
-            'xmmr' => 'image/vnd.fujixerox.edmics-mmr',
-            'xmny' => 'application/x-msmoney',
-            'xmov' => 'video/quicktime',
-            'xmovie' => 'video/x-sgi-movie',
-            'xmp2' => 'audio/mpeg',
-
-            'xmp2a' => 'audio/mpeg',
-            'xmp3' => 'audio/mpeg',
-            'xmp4' => 'video/mp4',
-            'xmp4a' => 'audio/mp4',
-            'xmp4s' => 'application/mp4',
-            'xmp4v' => 'video/mp4',
-            'xmpc' => 'application/vnd.mophun.certificate',
-            'xmpe' => 'video/mpeg',
-            'xmpeg' => 'video/mpeg',
-
-            'xmpg' => 'video/mpeg',
-            'xmpg4' => 'video/mp4',
-            'xmpga' => 'audio/mpeg',
-            'xmpkg' => 'application/vnd.apple.installer+xml',
-            'xmpm' => 'application/vnd.blueice.multipass',
-            'xmpn' => 'application/vnd.mophun.application',
-            'xmpp' => 'application/vnd.ms-project',
-            'xmpt' => 'application/vnd.ms-project',
-            'xmpy' => 'application/vnd.ibm.minipay',
-
-            'xmqy' => 'application/vnd.mobius.mqy',
-            'xmrc' => 'application/marc',
-            'xmscml' => 'application/mediaservercontrol+xml',
-            'xmseq' => 'application/vnd.mseq',
-            'xmsf' => 'application/vnd.epson.msf',
-            'xmsh' => 'model/mesh',
-            'xmsi' => 'application/x-msdownload',
-            'xmsl' => 'application/vnd.mobius.msl',
-            'xmsty' => 'application/vnd.muvee.style',
-
-            'xmts' => 'model/vnd.mts',
-            'xmus' => 'application/vnd.musician',
-            'xmvb' => 'application/x-msmediaview',
-            'xmwf' => 'application/vnd.mfer',
-            'xmxf' => 'application/mxf',
-            'xmxl' => 'application/vnd.recordare.musicxml',
-            'xmxml' => 'application/xv+xml',
-            'xmxs' => 'application/vnd.triscape.mxs',
-            'xmxu' => 'video/vnd.mpegurl',
-
-            'xn-gage' => 'application/vnd.nokia.n-gage.symbian.install',
-            'xngdat' => 'application/vnd.nokia.n-gage.data',
-            'xnlu' => 'application/vnd.neurolanguage.nlu',
-            'xnml' => 'application/vnd.enliven',
-            'xnnd' => 'application/vnd.noblenet-directory',
-            'xnns' => 'application/vnd.noblenet-sealer',
-            'xnnw' => 'application/vnd.noblenet-web',
-            'xnpx' => 'image/vnd.net-fpx',
-            'xnsf' => 'application/vnd.lotus-notes',
-
-            'xoa2' => 'application/vnd.fujitsu.oasys2',
-            'xoa3' => 'application/vnd.fujitsu.oasys3',
-            'xoas' => 'application/vnd.fujitsu.oasys',
-            'xobd' => 'application/x-msbinder',
-            'xoda' => 'application/oda',
-            'xodc' => 'application/vnd.oasis.opendocument.chart',
-            'xodf' => 'application/vnd.oasis.opendocument.formula',
-            'xodg' => 'application/vnd.oasis.opendocument.graphics',
-            'xodi' => 'application/vnd.oasis.opendocument.image',
-
-            'xodp' => 'application/vnd.oasis.opendocument.presentation',
-            'xods' => 'application/vnd.oasis.opendocument.spreadsheet',
-            'xodt' => 'application/vnd.oasis.opendocument.text',
-            'xogg' => 'application/ogg',
-            'xoprc' => 'application/vnd.palm',
-            'xorg' => 'application/vnd.lotus-organizer',
-            'xotc' => 'application/vnd.oasis.opendocument.chart-template',
-            'xotf' => 'application/vnd.oasis.opendocument.formula-template',
-            'xotg' => 'application/vnd.oasis.opendocument.graphics-template',
-
-            'xoth' => 'application/vnd.oasis.opendocument.text-web',
-            'xoti' => 'application/vnd.oasis.opendocument.image-template',
-            'xotm' => 'application/vnd.oasis.opendocument.text-master',
-            'xots' => 'application/vnd.oasis.opendocument.spreadsheet-template',
-            'xott' => 'application/vnd.oasis.opendocument.text-template',
-            'xoxt' => 'application/vnd.openofficeorg.extension',
-            'xp10' => 'application/pkcs10',
-            'xp7r' => 'application/x-pkcs7-certreqresp',
-            'xp7s' => 'application/pkcs7-signature',
-
-            'xpbd' => 'application/vnd.powerbuilder6',
-            'xpbm' => 'image/x-portable-bitmap',
-            'xpcl' => 'application/vnd.hp-pcl',
-            'xpclxl' => 'application/vnd.hp-pclxl',
-            'xpct' => 'image/x-pict',
-            'xpcx' => 'image/x-pcx',
-            'xpdb' => 'chemical/x-pdb',
-            'xpdf' => 'application/pdf',
-            'xpfr' => 'application/font-tdpfr',
-
-            'xpgm' => 'image/x-portable-graymap',
-            'xpgn' => 'application/x-chess-pgn',
-            'xpgp' => 'application/pgp-encrypted',
-            'xpic' => 'image/x-pict',
-            'xpki' => 'application/pkixcmp',
-            'xpkipath' => 'application/pkix-pkipath',
-            'xplb' => 'application/vnd.3gpp.pic-bw-large',
-            'xplc' => 'application/vnd.mobius.plc',
-            'xplf' => 'application/vnd.pocketlearn',
-
-            'xpls' => 'application/pls+xml',
-            'xpml' => 'application/vnd.ctc-posml',
-            'xpng' => 'image/png',
-            'xpnm' => 'image/x-portable-anymap',
-            'xportpkg' => 'application/vnd.macports.portpkg',
-            'xpot' => 'application/vnd.ms-powerpoint',
-            'xppd' => 'application/vnd.cups-ppd',
-            'xppm' => 'image/x-portable-pixmap',
-            'xpps' => 'application/vnd.ms-powerpoint',
-
-            'xppt' => 'application/vnd.ms-powerpoint',
-            'xpqa' => 'application/vnd.palm',
-            'xprc' => 'application/vnd.palm',
-            'xpre' => 'application/vnd.lotus-freelance',
-            'xprf' => 'application/pics-rules',
-            'xps' => 'application/postscript',
-            'xpsb' => 'application/vnd.3gpp.pic-bw-small',
-            'xpsd' => 'image/vnd.adobe.photoshop',
-            'xptid' => 'application/vnd.pvi.ptid1',
-
-            'xpub' => 'application/x-mspublisher',
-            'xpvb' => 'application/vnd.3gpp.pic-bw-var',
-            'xpwn' => 'application/vnd.3m.post-it-notes',
-            'xqam' => 'application/vnd.epson.quickanime',
-            'xqbo' => 'application/vnd.intu.qbo',
-            'xqfx' => 'application/vnd.intu.qfx',
-            'xqps' => 'application/vnd.publishare-delta-tree',
-            'xqt' => 'video/quicktime',
-            'xra' => 'audio/x-pn-realaudio',
-
-            'xram' => 'audio/x-pn-realaudio',
-            'xrar' => 'application/x-rar-compressed',
-            'xras' => 'image/x-cmu-raster',
-            'xrcprofile' => 'application/vnd.ipunplugged.rcprofile',
-            'xrdf' => 'application/rdf+xml',
-            'xrdz' => 'application/vnd.data-vision.rdz',
-            'xrep' => 'application/vnd.businessobjects',
-            'xrgb' => 'image/x-rgb',
-            'xrif' => 'application/reginfo+xml',
-
-            'xrl' => 'application/resource-lists+xml',
-            'xrlc' => 'image/vnd.fujixerox.edmics-rlc',
-            'xrm' => 'application/vnd.rn-realmedia',
-            'xrmi' => 'audio/midi',
-            'xrmp' => 'audio/x-pn-realaudio-plugin',
-            'xrms' => 'application/vnd.jcp.javame.midlet-rms',
-            'xrnc' => 'application/relax-ng-compact-syntax',
-            'xrpss' => 'application/vnd.nokia.radio-presets',
-            'xrpst' => 'application/vnd.nokia.radio-preset',
-
-            'xrq' => 'application/sparql-query',
-            'xrs' => 'application/rls-services+xml',
-            'xrsd' => 'application/rsd+xml',
-            'xrss' => 'application/rss+xml',
-            'xrtf' => 'application/rtf',
-            'xrtx' => 'text/richtext',
-            'xsaf' => 'application/vnd.yamaha.smaf-audio',
-            'xsbml' => 'application/sbml+xml',
-            'xsc' => 'application/vnd.ibm.secure-container',
-
-            'xscd' => 'application/x-msschedule',
-            'xscm' => 'application/vnd.lotus-screencam',
-            'xscq' => 'application/scvp-cv-request',
-            'xscs' => 'application/scvp-cv-response',
-            'xsdp' => 'application/sdp',
-            'xsee' => 'application/vnd.seemail',
-            'xsema' => 'application/vnd.sema',
-            'xsemd' => 'application/vnd.semd',
-            'xsemf' => 'application/vnd.semf',
-
-            'xsetpay' => 'application/set-payment-initiation',
-            'xsetreg' => 'application/set-registration-initiation',
-            'xsfs' => 'application/vnd.spotfire.sfs',
-            'xsgm' => 'text/sgml',
-            'xsgml' => 'text/sgml',
-            'xsh' => 'application/x-sh',
-            'xshar' => 'application/x-shar',
-            'xshf' => 'application/shf+xml',
-            'xsilo' => 'model/mesh',
-
-            'xsit' => 'application/x-stuffit',
-            'xsitx' => 'application/x-stuffitx',
-            'xslt' => 'application/vnd.epson.salt',
-            'xsnd' => 'audio/basic',
-            'xspf' => 'application/vnd.yamaha.smaf-phrase',
-            'xspl' => 'application/x-futuresplash',
-            'xspot' => 'text/vnd.in3d.spot',
-            'xspp' => 'application/scvp-vp-response',
-            'xspq' => 'application/scvp-vp-request',
-
-            'xsrc' => 'application/x-wais-source',
-            'xsrx' => 'application/sparql-results+xml',
-            'xssf' => 'application/vnd.epson.ssf',
-            'xssml' => 'application/ssml+xml',
-            'xstf' => 'application/vnd.wt.stf',
-            'xstk' => 'application/hyperstudio',
-            'xstr' => 'application/vnd.pg.format',
-            'xsus' => 'application/vnd.sus-calendar',
-            'xsusp' => 'application/vnd.sus-calendar',
-
-            'xsv4cpio' => 'application/x-sv4cpio',
-            'xsv4crc' => 'application/x-sv4crc',
-            'xsvd' => 'application/vnd.svd',
-            'xswf' => 'application/x-shockwave-flash',
-            'xtao' => 'application/vnd.tao.intent-module-archive',
-            'xtar' => 'application/x-tar',
-            'xtcap' => 'application/vnd.3gpp2.tcap',
-            'xtcl' => 'application/x-tcl',
-            'xtex' => 'application/x-tex',
-
-            'xtext' => 'text/plain',
-            'xtif' => 'image/tiff',
-            'xtiff' => 'image/tiff',
-            'xtmo' => 'application/vnd.tmobile-livetv',
-            'xtorrent' => 'application/x-bittorrent',
-            'xtpl' => 'application/vnd.groove-tool-template',
-            'xtpt' => 'application/vnd.trid.tpt',
-            'xtra' => 'application/vnd.trueapp',
-            'xtrm' => 'application/x-msterminal',
-
-            'xtsv' => 'text/tab-separated-values',
-            'xtxd' => 'application/vnd.genomatix.tuxedo',
-            'xtxf' => 'application/vnd.mobius.txf',
-            'xtxt' => 'text/plain',
-            'xumj' => 'application/vnd.umajin',
-            'xunityweb' => 'application/vnd.unity',
-            'xuoml' => 'application/vnd.uoml+xml',
-            'xuri' => 'text/uri-list',
-            'xuris' => 'text/uri-list',
-
-            'xurls' => 'text/uri-list',
-            'xustar' => 'application/x-ustar',
-            'xutz' => 'application/vnd.uiq.theme',
-            'xuu' => 'text/x-uuencode',
-            'xvcd' => 'application/x-cdlink',
-            'xvcf' => 'text/x-vcard',
-            'xvcg' => 'application/vnd.groove-vcard',
-            'xvcs' => 'text/x-vcalendar',
-            'xvcx' => 'application/vnd.vcx',
-
-            'xvis' => 'application/vnd.visionary',
-            'xviv' => 'video/vnd.vivo',
-            'xvrml' => 'model/vrml',
-            'xvsd' => 'application/vnd.visio',
-            'xvsf' => 'application/vnd.vsf',
-            'xvss' => 'application/vnd.visio',
-            'xvst' => 'application/vnd.visio',
-            'xvsw' => 'application/vnd.visio',
-            'xvtu' => 'model/vnd.vtu',
-
-            'xvxml' => 'application/voicexml+xml',
-            'xwav' => 'audio/x-wav',
-            'xwax' => 'audio/x-ms-wax',
-            'xwbmp' => 'image/vnd.wap.wbmp',
-            'xwbs' => 'application/vnd.criticaltools.wbs+xml',
-            'xwbxml' => 'application/vnd.wap.wbxml',
-            'xwcm' => 'application/vnd.ms-works',
-            'xwdb' => 'application/vnd.ms-works',
-            'xwks' => 'application/vnd.ms-works',
-
-            'xwm' => 'video/x-ms-wm',
-            'xwma' => 'audio/x-ms-wma',
-            'xwmd' => 'application/x-ms-wmd',
-            'xwmf' => 'application/x-msmetafile',
-            'xwml' => 'text/vnd.wap.wml',
-            'xwmlc' => 'application/vnd.wap.wmlc',
-            'xwmls' => 'text/vnd.wap.wmlscript',
-            'xwmlsc' => 'application/vnd.wap.wmlscriptc',
-            'xwmv' => 'video/x-ms-wmv',
-
-            'xwmx' => 'video/x-ms-wmx',
-            'xwmz' => 'application/x-ms-wmz',
-            'xwpd' => 'application/vnd.wordperfect',
-            'xwpl' => 'application/vnd.ms-wpl',
-            'xwps' => 'application/vnd.ms-works',
-            'xwqd' => 'application/vnd.wqd',
-            'xwri' => 'application/x-mswrite',
-            'xwrl' => 'model/vrml',
-            'xwsdl' => 'application/wsdl+xml',
-
-            'xwspolicy' => 'application/wspolicy+xml',
-            'xwtb' => 'application/vnd.webturbo',
-            'xwvx' => 'video/x-ms-wvx',
-            'xx3d' => 'application/vnd.hzn-3d-crossword',
-            'xxar' => 'application/vnd.xara',
-            'xxbd' => 'application/vnd.fujixerox.docuworks.binder',
-            'xxbm' => 'image/x-xbitmap',
-            'xxdm' => 'application/vnd.syncml.dm+xml',
-            'xxdp' => 'application/vnd.adobe.xdp+xml',
-
-            'xxdw' => 'application/vnd.fujixerox.docuworks',
-            'xxenc' => 'application/xenc+xml',
-            'xxfdf' => 'application/vnd.adobe.xfdf',
-            'xxfdl' => 'application/vnd.xfdl',
-            'xxht' => 'application/xhtml+xml',
-            'xxhtml' => 'application/xhtml+xml',
-            'xxhvml' => 'application/xv+xml',
-            'xxif' => 'image/vnd.xiff',
-            'xxla' => 'application/vnd.ms-excel',
-
-            'xxlc' => 'application/vnd.ms-excel',
-            'xxlm' => 'application/vnd.ms-excel',
-            'xxls' => 'application/vnd.ms-excel',
-            'xxlt' => 'application/vnd.ms-excel',
-            'xxlw' => 'application/vnd.ms-excel',
-            'xxml' => 'application/xml',
-            'xxo' => 'application/vnd.olpc-sugar',
-            'xxop' => 'application/xop+xml',
-            'xxpm' => 'image/x-xpixmap',
-
-            'xxpr' => 'application/vnd.is-xpr',
-            'xxps' => 'application/vnd.ms-xpsdocument',
-            'xxsl' => 'application/xml',
-            'xxslt' => 'application/xslt+xml',
-            'xxsm' => 'application/vnd.syncml+xml',
-            'xxspf' => 'application/xspf+xml',
-            'xxul' => 'application/vnd.mozilla.xul+xml',
-            'xxvm' => 'application/xv+xml',
-            'xxvml' => 'application/xv+xml',
-
-            'xxwd' => 'image/x-xwindowdump',
-            'xxyz' => 'chemical/x-xyz',
-            'xzaz' => 'application/vnd.zzazz.deck+xml',
-            'xzip' => 'application/zip',
-            'xzmm' => 'application/vnd.handheld-entertainment+xml',
-            'xodt' => 'application/x-vnd.oasis.opendocument.spreadsheet'
-        );
 }
diff --git app/code/core/Mage/Oauth/Model/Server.php app/code/core/Mage/Oauth/Model/Server.php
index b5fdb2c..1aaaa9b 100644
--- app/code/core/Mage/Oauth/Model/Server.php
+++ app/code/core/Mage/Oauth/Model/Server.php
@@ -328,10 +328,10 @@ class Mage_Oauth_Model_Server
             if (self::REQUEST_TOKEN == $this->_requestType) {
                 $this->_validateVerifierParam();
 
-                if ($this->_token->getVerifier() != $this->_protocolParams['oauth_verifier']) {
+                if (!hash_equals($this->_token->getVerifier(), $this->_protocolParams['oauth_verifier'])) {
                     $this->_throwException('', self::ERR_VERIFIER_INVALID);
                 }
-                if ($this->_token->getConsumerId() != $this->_consumer->getId()) {
+                if (!hash_equals($this->_token->getConsumerId(), $this->_consumer->getId())) {
                     $this->_throwException('', self::ERR_TOKEN_REJECTED);
                 }
                 if (Mage_Oauth_Model_Token::TYPE_REQUEST != $this->_token->getType()) {
@@ -541,7 +541,7 @@ class Mage_Oauth_Model_Server
             $this->_request->getScheme() . '://' . $this->_request->getHttpHost() . $this->_request->getRequestUri()
         );
 
-        if ($calculatedSign != $this->_protocolParams['oauth_signature']) {
+        if (!hash_equals($calculatedSign, $this->_protocolParams['oauth_signature'])) {
             $this->_throwException($calculatedSign, self::ERR_SIGNATURE_INVALID);
         }
     }
diff --git app/code/core/Mage/Paygate/Model/Authorizenet.php app/code/core/Mage/Paygate/Model/Authorizenet.php
index 0ad8cf2..f947216 100644
--- app/code/core/Mage/Paygate/Model/Authorizenet.php
+++ app/code/core/Mage/Paygate/Model/Authorizenet.php
@@ -1261,8 +1261,10 @@ class Mage_Paygate_Model_Authorizenet extends Mage_Payment_Model_Method_Cc
         $uri = $this->getConfigData('cgi_url');
         $client->setUri($uri ? $uri : self::CGI_URL);
         $client->setConfig(array(
-            'maxredirects'=>0,
-            'timeout'=>30,
+            'maxredirects' => 0,
+            'timeout' => 30,
+            'verifyhost' => 2,
+            'verifypeer' => true,
             //'ssltransport' => 'tcp',
         ));
         foreach ($request->getData() as $key => $value) {
@@ -1529,8 +1531,13 @@ class Mage_Paygate_Model_Authorizenet extends Mage_Payment_Model_Method_Cc
 
         $client = new Varien_Http_Client();
         $uri = $this->getConfigData('cgi_url_td');
-        $client->setUri($uri ? $uri : self::CGI_URL_TD);
-        $client->setConfig(array('timeout'=>45));
+        $uri = $uri ? $uri : self::CGI_URL_TD;
+        $client->setUri($uri);
+        $client->setConfig(array(
+            'timeout' => 45,
+            'verifyhost' => 2,
+            'verifypeer' => true,
+        ));
         $client->setHeaders(array('Content-Type: text/xml'));
         $client->setMethod(Zend_Http_Client::POST);
         $client->setRawData($requestBody);
diff --git app/code/core/Mage/Payment/Block/Info/Checkmo.php app/code/core/Mage/Payment/Block/Info/Checkmo.php
index 57572c0..2bc2e59 100644
--- app/code/core/Mage/Payment/Block/Info/Checkmo.php
+++ app/code/core/Mage/Payment/Block/Info/Checkmo.php
@@ -70,7 +70,13 @@ class Mage_Payment_Block_Info_Checkmo extends Mage_Payment_Block_Info
      */
     protected function _convertAdditionalData()
     {
-        $details = @unserialize($this->getInfo()->getAdditionalData());
+        $details = false;
+        try {
+            $details = Mage::helper('core/unserializeArray')
+                ->unserialize($this->getInfo()->getAdditionalData());
+        } catch (Exception $e) {
+            Mage::logException($e);
+        }
         if (is_array($details)) {
             $this->_payableTo = isset($details['payable_to']) ? (string) $details['payable_to'] : '';
             $this->_mailingAddress = isset($details['mailing_address']) ? (string) $details['mailing_address'] : '';
@@ -80,7 +86,7 @@ class Mage_Payment_Block_Info_Checkmo extends Mage_Payment_Block_Info
         }
         return $this;
     }
-    
+
     public function toPdf()
     {
         $this->setTemplate('payment/info/pdf/checkmo.phtml');
diff --git app/code/core/Mage/Paypal/Model/Resource/Payment/Transaction.php app/code/core/Mage/Paypal/Model/Resource/Payment/Transaction.php
index af5bce2..c582558 100644
--- app/code/core/Mage/Paypal/Model/Resource/Payment/Transaction.php
+++ app/code/core/Mage/Paypal/Model/Resource/Payment/Transaction.php
@@ -53,6 +53,30 @@ class Mage_Paypal_Model_Resource_Payment_Transaction extends Mage_Core_Model_Res
     }
 
     /**
+     * Unserialize Varien_Object field in an object
+     *
+     * @param Mage_Core_Model_Abstract $object
+     * @param string $field
+     * @param mixed $defaultValue
+     */
+    protected function _unserializeField(Varien_Object $object, $field, $defaultValue = null)
+    {
+        $value = $object->getData($field);
+        if (empty($value)) {
+            $object->setData($field, $defaultValue);
+        } elseif (!is_array($value) && !is_object($value)) {
+            $unserializedValue = false;
+            try {
+                $unserializedValue = Mage::helper('core/unserializeArray')
+                    ->unserialize($value);
+            } catch (Exception $e) {
+                Mage::logException($e);
+            }
+            $object->setData($field, $unserializedValue);
+        }
+    }
+
+    /**
      * Load the transaction object by specified txn_id
      *
      * @param Mage_Paypal_Model_Payment_Transaction $transaction
diff --git app/code/core/Mage/Review/controllers/ProductController.php app/code/core/Mage/Review/controllers/ProductController.php
index d35c296..27c6ec5 100644
--- app/code/core/Mage/Review/controllers/ProductController.php
+++ app/code/core/Mage/Review/controllers/ProductController.php
@@ -155,6 +155,12 @@ class Mage_Review_ProductController extends Mage_Core_Controller_Front_Action
      */
     public function postAction()
     {
+        if (!$this->_validateFormKey()) {
+            // returns to the product item page
+            $this->_redirectReferer();
+            return;
+        }
+
         if ($data = Mage::getSingleton('review/session')->getFormData(true)) {
             $rating = array();
             if (isset($data['ratings']) && is_array($data['ratings'])) {
diff --git app/code/core/Mage/Sales/Model/Resource/Order/Payment.php app/code/core/Mage/Sales/Model/Resource/Order/Payment.php
index 830d7ab..d3d1f71 100755
--- app/code/core/Mage/Sales/Model/Resource/Order/Payment.php
+++ app/code/core/Mage/Sales/Model/Resource/Order/Payment.php
@@ -58,4 +58,28 @@ class Mage_Sales_Model_Resource_Order_Payment extends Mage_Sales_Model_Resource_
     {
         $this->_init('sales/order_payment', 'entity_id');
     }
+
+    /**
+     * Unserialize Varien_Object field in an object
+     *
+     * @param Mage_Core_Model_Abstract $object
+     * @param string $field
+     * @param mixed $defaultValue
+     */
+    protected function _unserializeField(Varien_Object $object, $field, $defaultValue = null)
+    {
+        $value = $object->getData($field);
+        if (empty($value)) {
+            $object->setData($field, $defaultValue);
+        } elseif (!is_array($value) && !is_object($value)) {
+            $unserializedValue = false;
+            try {
+                $unserializedValue = Mage::helper('core/unserializeArray')
+                ->unserialize($value);
+            } catch (Exception $e) {
+                Mage::logException($e);
+            }
+            $object->setData($field, $unserializedValue);
+        }
+    }
 }
diff --git app/code/core/Mage/Sales/Model/Resource/Order/Payment/Transaction.php app/code/core/Mage/Sales/Model/Resource/Order/Payment/Transaction.php
index 7447967..82c7b10 100755
--- app/code/core/Mage/Sales/Model/Resource/Order/Payment/Transaction.php
+++ app/code/core/Mage/Sales/Model/Resource/Order/Payment/Transaction.php
@@ -53,6 +53,30 @@ class Mage_Sales_Model_Resource_Order_Payment_Transaction extends Mage_Sales_Mod
     }
 
     /**
+     * Unserialize Varien_Object field in an object
+     *
+     * @param Mage_Core_Model_Abstract $object
+     * @param string $field
+     * @param mixed $defaultValue
+     */
+    protected function _unserializeField(Varien_Object $object, $field, $defaultValue = null)
+    {
+        $value = $object->getData($field);
+        if (empty($value)) {
+            $object->setData($field, $defaultValue);
+        } elseif (!is_array($value) && !is_object($value)) {
+            $unserializedValue = false;
+            try {
+                $unserializedValue = Mage::helper('core/unserializeArray')
+                ->unserialize($value);
+            } catch (Exception $e) {
+                Mage::logException($e);
+            }
+            $object->setData($field, $unserializedValue);
+        }
+    }
+
+    /**
      * Update transactions in database using provided transaction as parent for them
      * have to repeat the business logic to avoid accidental injection of wrong transactions
      *
diff --git app/code/core/Mage/Sales/Model/Resource/Quote/Payment.php app/code/core/Mage/Sales/Model/Resource/Quote/Payment.php
index 8f8f0d1..ac4b8fe 100755
--- app/code/core/Mage/Sales/Model/Resource/Quote/Payment.php
+++ app/code/core/Mage/Sales/Model/Resource/Quote/Payment.php
@@ -51,4 +51,28 @@ class Mage_Sales_Model_Resource_Quote_Payment extends Mage_Sales_Model_Resource_
     {
         $this->_init('sales/quote_payment', 'payment_id');
     }
+
+    /**
+     * Unserialize Varien_Object field in an object
+     *
+     * @param Mage_Core_Model_Abstract $object
+     * @param string $field
+     * @param mixed $defaultValue
+     */
+    protected function _unserializeField(Varien_Object $object, $field, $defaultValue = null)
+    {
+        $value = $object->getData($field);
+        if (empty($value)) {
+            $object->setData($field, $defaultValue);
+        } elseif (!is_array($value) && !is_object($value)) {
+            $unserializedValue = false;
+            try {
+                $unserializedValue = Mage::helper('core/unserializeArray')
+                    ->unserialize($value);
+            } catch (Exception $e) {
+                Mage::logException($e);
+            }
+            $object->setData($field, $unserializedValue);
+        }
+    }
 }
diff --git app/code/core/Mage/Sales/Model/Resource/Recurring/Profile.php app/code/core/Mage/Sales/Model/Resource/Recurring/Profile.php
index d637be3b..5ad9383 100755
--- app/code/core/Mage/Sales/Model/Resource/Recurring/Profile.php
+++ app/code/core/Mage/Sales/Model/Resource/Recurring/Profile.php
@@ -54,6 +54,33 @@ class Mage_Sales_Model_Resource_Recurring_Profile extends Mage_Sales_Model_Resou
     }
 
     /**
+     * Unserialize Varien_Object field in an object
+     *
+     * @param Mage_Core_Model_Abstract $object
+     * @param string $field
+     * @param mixed $defaultValue
+     */
+    protected function _unserializeField(Varien_Object $object, $field, $defaultValue = null)
+    {
+        if ($field != 'additional_info') {
+            return parent::_unserializeField($object, $field, $defaultValue);
+        }
+        $value = $object->getData($field);
+        if (empty($value)) {
+            $object->setData($field, $defaultValue);
+        } elseif (!is_array($value) && !is_object($value)) {
+            $unserializedValue = false;
+            try {
+                $unserializedValue = Mage::helper('core/unserializeArray')
+                ->unserialize($value);
+            } catch (Exception $e) {
+                Mage::logException($e);
+            }
+            $object->setData($field, $unserializedValue);
+        }
+    }
+
+    /**
      * Return recurring profile child Orders Ids
      *
      *
diff --git app/code/core/Mage/Uploader/Block/Abstract.php app/code/core/Mage/Uploader/Block/Abstract.php
new file mode 100644
index 0000000..a11c23a
--- /dev/null
+++ app/code/core/Mage/Uploader/Block/Abstract.php
@@ -0,0 +1,247 @@
+<?php
+/**
+ * Magento
+ *
+ * NOTICE OF LICENSE
+ *
+ * This source file is subject to the Open Software License (OSL 3.0)
+ * that is bundled with this package in the file LICENSE.txt.
+ * It is also available through the world-wide-web at this URL:
+ * http://opensource.org/licenses/osl-3.0.php
+ * If you did not receive a copy of the license and are unable to
+ * obtain it through the world-wide-web, please send an email
+ * to license@magento.com so we can send you a copy immediately.
+ *
+ * DISCLAIMER
+ *
+ * Do not edit or add to this file if you wish to upgrade Magento to newer
+ * versions in the future. If you wish to customize Magento for your
+ * needs please refer to http://www.magento.com for more information.
+ *
+ * @category    Mage
+ * @package     Mage_Uploader
+ * @copyright Copyright (c) 2006-2016 X.commerce, Inc. and affiliates (http://www.magento.com)
+ * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
+ */
+
+abstract class Mage_Uploader_Block_Abstract extends Mage_Adminhtml_Block_Widget
+{
+    /**
+     * Template used for uploader
+     *
+     * @var string
+     */
+    protected $_template = 'media/uploader.phtml';
+
+    /**
+     * @var Mage_Uploader_Model_Config_Misc
+     */
+    protected $_misc;
+
+    /**
+     * @var Mage_Uploader_Model_Config_Uploader
+     */
+    protected $_uploaderConfig;
+
+    /**
+     * @var Mage_Uploader_Model_Config_Browsebutton
+     */
+    protected $_browseButtonConfig;
+
+    /**
+     * @var Mage_Uploader_Model_Config_Misc
+     */
+    protected $_miscConfig;
+
+    /**
+     * @var array
+     */
+    protected $_idsMapping = array();
+
+    /**
+     * Default browse button ID suffix
+     */
+    const DEFAULT_BROWSE_BUTTON_ID_SUFFIX = 'browse';
+
+    /**
+     * Constructor for uploader block
+     *
+     * @see https://github.com/flowjs/flow.js/tree/v2.9.0#configuration
+     * @description Set unique id for block
+     */
+    public function __construct()
+    {
+        parent::__construct();
+        $this->setId($this->getId() . '_Uploader');
+    }
+
+    /**
+     * Helper for file manipulation
+     *
+     * @return Mage_Uploader_Helper_File
+     */
+    protected function _getHelper()
+    {
+        return Mage::helper('uploader/file');
+    }
+
+    /**
+     * @return string
+     */
+    public function getJsonConfig()
+    {
+        return $this->helper('core')->jsonEncode(array(
+            'uploaderConfig'    => $this->getUploaderConfig()->getData(),
+            'elementIds'        => $this->_getElementIdsMapping(),
+            'browseConfig'      => $this->getButtonConfig()->getData(),
+            'miscConfig'        => $this->getMiscConfig()->getData(),
+        ));
+    }
+
+    /**
+     * Get mapping of ids for front-end use
+     *
+     * @return array
+     */
+    protected function _getElementIdsMapping()
+    {
+        return $this->_idsMapping;
+    }
+
+    /**
+     * Add mapping ids for front-end use
+     *
+     * @param array $additionalButtons
+     * @return $this
+     */
+    protected function _addElementIdsMapping($additionalButtons = array())
+    {
+        $this->_idsMapping = array_merge($this->_idsMapping, $additionalButtons);
+
+        return $this;
+    }
+
+    /**
+     * Prepare layout, create buttons, set front-end elements ids
+     *
+     * @return Mage_Core_Block_Abstract
+     */
+    protected function _prepareLayout()
+    {
+        $this->setChild(
+            'browse_button',
+            $this->getLayout()->createBlock('adminhtml/widget_button')
+                ->addData(array(
+                    // Workaround for IE9
+                    'before_html'   => sprintf(
+                        '<div style="display:inline-block;" id="%s">',
+                        $this->getElementId(self::DEFAULT_BROWSE_BUTTON_ID_SUFFIX)
+                    ),
+                    'after_html'    => '</div>',
+                    'id'            => $this->getElementId(self::DEFAULT_BROWSE_BUTTON_ID_SUFFIX . '_button'),
+                    'label'         => Mage::helper('uploader')->__('Browse Files...'),
+                    'type'          => 'button',
+                ))
+        );
+
+        $this->setChild(
+            'delete_button',
+            $this->getLayout()->createBlock('adminhtml/widget_button')
+                ->addData(array(
+                    'id'      => '{{id}}',
+                    'class'   => 'delete',
+                    'type'    => 'button',
+                    'label'   => Mage::helper('uploader')->__('Remove')
+                ))
+        );
+
+        $this->_addElementIdsMapping(array(
+            'container'         => $this->getHtmlId(),
+            'templateFile'      => $this->getElementId('template'),
+            'browse'            => $this->_prepareElementsIds(array(self::DEFAULT_BROWSE_BUTTON_ID_SUFFIX))
+        ));
+
+        return parent::_prepareLayout();
+    }
+
+    /**
+     * Get browse button html
+     *
+     * @return string
+     */
+    public function getBrowseButtonHtml()
+    {
+        return $this->getChildHtml('browse_button');
+    }
+
+    /**
+     * Get delete button html
+     *
+     * @return string
+     */
+    public function getDeleteButtonHtml()
+    {
+        return $this->getChildHtml('delete_button');
+    }
+
+    /**
+     * Get uploader misc settings
+     *
+     * @return Mage_Uploader_Model_Config_Misc
+     */
+    public function getMiscConfig()
+    {
+        if (is_null($this->_miscConfig)) {
+            $this->_miscConfig = Mage::getModel('uploader/config_misc');
+        }
+        return $this->_miscConfig;
+    }
+
+    /**
+     * Get uploader general settings
+     *
+     * @return Mage_Uploader_Model_Config_Uploader
+     */
+    public function getUploaderConfig()
+    {
+        if (is_null($this->_uploaderConfig)) {
+            $this->_uploaderConfig = Mage::getModel('uploader/config_uploader');
+        }
+        return $this->_uploaderConfig;
+    }
+
+    /**
+     * Get browse button settings
+     *
+     * @return Mage_Uploader_Model_Config_Browsebutton
+     */
+    public function getButtonConfig()
+    {
+        if (is_null($this->_browseButtonConfig)) {
+            $this->_browseButtonConfig = Mage::getModel('uploader/config_browsebutton');
+        }
+        return $this->_browseButtonConfig;
+    }
+
+    /**
+     * Get button unique id
+     *
+     * @param string $suffix
+     * @return string
+     */
+    public function getElementId($suffix)
+    {
+        return $this->getHtmlId() . '-' . $suffix;
+    }
+
+    /**
+     * Prepare actual elements ids from suffixes
+     *
+     * @param array $targets $type => array($idsSuffixes)
+     * @return array $type => array($htmlIds)
+     */
+    protected function _prepareElementsIds($targets)
+    {
+        return array_map(array($this, 'getElementId'), array_unique(array_values($targets)));
+    }
+}
diff --git app/code/core/Mage/Uploader/Block/Multiple.php app/code/core/Mage/Uploader/Block/Multiple.php
new file mode 100644
index 0000000..abf47df
--- /dev/null
+++ app/code/core/Mage/Uploader/Block/Multiple.php
@@ -0,0 +1,71 @@
+<?php
+/**
+ * Magento
+ *
+ * NOTICE OF LICENSE
+ *
+ * This source file is subject to the Open Software License (OSL 3.0)
+ * that is bundled with this package in the file LICENSE.txt.
+ * It is also available through the world-wide-web at this URL:
+ * http://opensource.org/licenses/osl-3.0.php
+ * If you did not receive a copy of the license and are unable to
+ * obtain it through the world-wide-web, please send an email
+ * to license@magento.com so we can send you a copy immediately.
+ *
+ * DISCLAIMER
+ *
+ * Do not edit or add to this file if you wish to upgrade Magento to newer
+ * versions in the future. If you wish to customize Magento for your
+ * needs please refer to http://www.magento.com for more information.
+ *
+ * @category    Mage
+ * @package     Mage_Uploader
+ * @copyright Copyright (c) 2006-2016 X.commerce, Inc. and affiliates (http://www.magento.com)
+ * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
+ */
+
+class Mage_Uploader_Block_Multiple extends Mage_Uploader_Block_Abstract
+{
+    /**
+     *
+     * Default upload button ID suffix
+     */
+    const DEFAULT_UPLOAD_BUTTON_ID_SUFFIX = 'upload';
+
+
+    /**
+     * Prepare layout, create upload button
+     *
+     * @return Mage_Uploader_Block_Multiple
+     */
+    protected function _prepareLayout()
+    {
+        parent::_prepareLayout();
+
+        $this->setChild(
+            'upload_button',
+            $this->getLayout()->createBlock('adminhtml/widget_button')
+                ->addData(array(
+                    'id'      => $this->getElementId(self::DEFAULT_UPLOAD_BUTTON_ID_SUFFIX),
+                    'label'   => Mage::helper('uploader')->__('Upload Files'),
+                    'type'    => 'button',
+                ))
+        );
+
+        $this->_addElementIdsMapping(array(
+            'upload' => $this->_prepareElementsIds(array(self::DEFAULT_UPLOAD_BUTTON_ID_SUFFIX))
+        ));
+
+        return $this;
+    }
+
+    /**
+     * Get upload button html
+     *
+     * @return string
+     */
+    public function getUploadButtonHtml()
+    {
+        return $this->getChildHtml('upload_button');
+    }
+}
diff --git app/code/core/Mage/Uploader/Block/Single.php app/code/core/Mage/Uploader/Block/Single.php
new file mode 100644
index 0000000..ed298a0
--- /dev/null
+++ app/code/core/Mage/Uploader/Block/Single.php
@@ -0,0 +1,52 @@
+<?php
+/**
+ * Magento
+ *
+ * NOTICE OF LICENSE
+ *
+ * This source file is subject to the Open Software License (OSL 3.0)
+ * that is bundled with this package in the file LICENSE.txt.
+ * It is also available through the world-wide-web at this URL:
+ * http://opensource.org/licenses/osl-3.0.php
+ * If you did not receive a copy of the license and are unable to
+ * obtain it through the world-wide-web, please send an email
+ * to license@magento.com so we can send you a copy immediately.
+ *
+ * DISCLAIMER
+ *
+ * Do not edit or add to this file if you wish to upgrade Magento to newer
+ * versions in the future. If you wish to customize Magento for your
+ * needs please refer to http://www.magento.com for more information.
+ *
+ * @category    Mage
+ * @package     Mage_Uploader
+ * @copyright Copyright (c) 2006-2016 X.commerce, Inc. and affiliates (http://www.magento.com)
+ * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
+ */
+
+class Mage_Uploader_Block_Single extends Mage_Uploader_Block_Abstract
+{
+    /**
+     * Prepare layout, change button and set front-end element ids mapping
+     *
+     * @return Mage_Core_Block_Abstract
+     */
+    protected function _prepareLayout()
+    {
+        parent::_prepareLayout();
+        $this->getChild('browse_button')->setLabel(Mage::helper('uploader')->__('...'));
+
+        return $this;
+    }
+
+    /**
+     * Constructor for single uploader block
+     */
+    public function __construct()
+    {
+        parent::__construct();
+
+        $this->getUploaderConfig()->setSingleFile(true);
+        $this->getButtonConfig()->setSingleFile(true);
+    }
+}
diff --git app/code/core/Mage/Uploader/Helper/Data.php app/code/core/Mage/Uploader/Helper/Data.php
new file mode 100644
index 0000000..2650976
--- /dev/null
+++ app/code/core/Mage/Uploader/Helper/Data.php
@@ -0,0 +1,30 @@
+<?php
+/**
+ * Magento
+ *
+ * NOTICE OF LICENSE
+ *
+ * This source file is subject to the Open Software License (OSL 3.0)
+ * that is bundled with this package in the file LICENSE.txt.
+ * It is also available through the world-wide-web at this URL:
+ * http://opensource.org/licenses/osl-3.0.php
+ * If you did not receive a copy of the license and are unable to
+ * obtain it through the world-wide-web, please send an email
+ * to license@magento.com so we can send you a copy immediately.
+ *
+ * DISCLAIMER
+ *
+ * Do not edit or add to this file if you wish to upgrade Magento to newer
+ * versions in the future. If you wish to customize Magento for your
+ * needs please refer to http://www.magento.com for more information.
+ *
+ * @category    Mage
+ * @package     Mage_Uploader
+ * @copyright Copyright (c) 2006-2016 X.commerce, Inc. and affiliates (http://www.magento.com)
+ * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
+ */
+
+class Mage_Uploader_Helper_Data extends Mage_Core_Helper_Abstract
+{
+
+}
diff --git app/code/core/Mage/Uploader/Helper/File.php app/code/core/Mage/Uploader/Helper/File.php
new file mode 100644
index 0000000..b0f17cb
--- /dev/null
+++ app/code/core/Mage/Uploader/Helper/File.php
@@ -0,0 +1,750 @@
+<?php
+/**
+ * Magento
+ *
+ * NOTICE OF LICENSE
+ *
+ * This source file is subject to the Open Software License (OSL 3.0)
+ * that is bundled with this package in the file LICENSE.txt.
+ * It is also available through the world-wide-web at this URL:
+ * http://opensource.org/licenses/osl-3.0.php
+ * If you did not receive a copy of the license and are unable to
+ * obtain it through the world-wide-web, please send an email
+ * to license@magento.com so we can send you a copy immediately.
+ *
+ * DISCLAIMER
+ *
+ * Do not edit or add to this file if you wish to upgrade Magento to newer
+ * versions in the future. If you wish to customize Magento for your
+ * needs please refer to http://www.magento.com for more information.
+ *
+ * @category    Mage
+ * @package     Mage_Uploader
+ * @copyright Copyright (c) 2006-2016 X.commerce, Inc. and affiliates (http://www.magento.com)
+ * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
+ */
+
+class Mage_Uploader_Helper_File extends Mage_Core_Helper_Abstract
+{
+    /**
+     * List of pre-defined MIME types
+     *
+     * @var array
+     */
+    protected $_mimeTypes =
+        array(
+            'x123' => 'application/vnd.lotus-1-2-3',
+            'x3dml' => 'text/vnd.in3d.3dml',
+            'x3g2' => 'video/3gpp2',
+            'x3gp' => 'video/3gpp',
+            'xace' => 'application/x-ace-compressed',
+            'xacu' => 'application/vnd.acucobol',
+            'xaep' => 'application/vnd.audiograph',
+            'xai' => 'application/postscript',
+            'xaif' => 'audio/x-aiff',
+
+            'xaifc' => 'audio/x-aiff',
+            'xaiff' => 'audio/x-aiff',
+            'xami' => 'application/vnd.amiga.ami',
+            'xapr' => 'application/vnd.lotus-approach',
+            'xasf' => 'video/x-ms-asf',
+            'xaso' => 'application/vnd.accpac.simply.aso',
+            'xasx' => 'video/x-ms-asf',
+            'xatom' => 'application/atom+xml',
+            'xatomcat' => 'application/atomcat+xml',
+
+            'xatomsvc' => 'application/atomsvc+xml',
+            'xatx' => 'application/vnd.antix.game-component',
+            'xau' => 'audio/basic',
+            'xavi' => 'video/x-msvideo',
+            'xbat' => 'application/x-msdownload',
+            'xbcpio' => 'application/x-bcpio',
+            'xbdm' => 'application/vnd.syncml.dm+wbxml',
+            'xbh2' => 'application/vnd.fujitsu.oasysprs',
+            'xbmi' => 'application/vnd.bmi',
+
+            'xbmp' => 'image/bmp',
+            'xbox' => 'application/vnd.previewsystems.box',
+            'xboz' => 'application/x-bzip2',
+            'xbtif' => 'image/prs.btif',
+            'xbz' => 'application/x-bzip',
+            'xbz2' => 'application/x-bzip2',
+            'xcab' => 'application/vnd.ms-cab-compressed',
+            'xccxml' => 'application/ccxml+xml',
+            'xcdbcmsg' => 'application/vnd.contact.cmsg',
+
+            'xcdkey' => 'application/vnd.mediastation.cdkey',
+            'xcdx' => 'chemical/x-cdx',
+            'xcdxml' => 'application/vnd.chemdraw+xml',
+            'xcdy' => 'application/vnd.cinderella',
+            'xcer' => 'application/pkix-cert',
+            'xcgm' => 'image/cgm',
+            'xchat' => 'application/x-chat',
+            'xchm' => 'application/vnd.ms-htmlhelp',
+            'xchrt' => 'application/vnd.kde.kchart',
+
+            'xcif' => 'chemical/x-cif',
+            'xcii' => 'application/vnd.anser-web-certificate-issue-initiation',
+            'xcil' => 'application/vnd.ms-artgalry',
+            'xcla' => 'application/vnd.claymore',
+            'xclkk' => 'application/vnd.crick.clicker.keyboard',
+            'xclkp' => 'application/vnd.crick.clicker.palette',
+            'xclkt' => 'application/vnd.crick.clicker.template',
+            'xclkw' => 'application/vnd.crick.clicker.wordbank',
+            'xclkx' => 'application/vnd.crick.clicker',
+
+            'xclp' => 'application/x-msclip',
+            'xcmc' => 'application/vnd.cosmocaller',
+            'xcmdf' => 'chemical/x-cmdf',
+            'xcml' => 'chemical/x-cml',
+            'xcmp' => 'application/vnd.yellowriver-custom-menu',
+            'xcmx' => 'image/x-cmx',
+            'xcom' => 'application/x-msdownload',
+            'xconf' => 'text/plain',
+            'xcpio' => 'application/x-cpio',
+
+            'xcpt' => 'application/mac-compactpro',
+            'xcrd' => 'application/x-mscardfile',
+            'xcrl' => 'application/pkix-crl',
+            'xcrt' => 'application/x-x509-ca-cert',
+            'xcsh' => 'application/x-csh',
+            'xcsml' => 'chemical/x-csml',
+            'xcss' => 'text/css',
+            'xcsv' => 'text/csv',
+            'xcurl' => 'application/vnd.curl',
+
+            'xcww' => 'application/prs.cww',
+            'xdaf' => 'application/vnd.mobius.daf',
+            'xdavmount' => 'application/davmount+xml',
+            'xdd2' => 'application/vnd.oma.dd2+xml',
+            'xddd' => 'application/vnd.fujixerox.ddd',
+            'xdef' => 'text/plain',
+            'xder' => 'application/x-x509-ca-cert',
+            'xdfac' => 'application/vnd.dreamfactory',
+            'xdis' => 'application/vnd.mobius.dis',
+
+            'xdjv' => 'image/vnd.djvu',
+            'xdjvu' => 'image/vnd.djvu',
+            'xdll' => 'application/x-msdownload',
+            'xdna' => 'application/vnd.dna',
+            'xdoc' => 'application/msword',
+            'xdot' => 'application/msword',
+            'xdp' => 'application/vnd.osgi.dp',
+            'xdpg' => 'application/vnd.dpgraph',
+            'xdsc' => 'text/prs.lines.tag',
+
+            'xdtd' => 'application/xml-dtd',
+            'xdvi' => 'application/x-dvi',
+            'xdwf' => 'model/vnd.dwf',
+            'xdwg' => 'image/vnd.dwg',
+            'xdxf' => 'image/vnd.dxf',
+            'xdxp' => 'application/vnd.spotfire.dxp',
+            'xecelp4800' => 'audio/vnd.nuera.ecelp4800',
+            'xecelp7470' => 'audio/vnd.nuera.ecelp7470',
+            'xecelp9600' => 'audio/vnd.nuera.ecelp9600',
+
+            'xecma' => 'application/ecmascript',
+            'xedm' => 'application/vnd.novadigm.edm',
+            'xedx' => 'application/vnd.novadigm.edx',
+            'xefif' => 'application/vnd.picsel',
+            'xei6' => 'application/vnd.pg.osasli',
+            'xeml' => 'message/rfc822',
+            'xeol' => 'audio/vnd.digital-winds',
+            'xeot' => 'application/vnd.ms-fontobject',
+            'xeps' => 'application/postscript',
+
+            'xesf' => 'application/vnd.epson.esf',
+            'xetx' => 'text/x-setext',
+            'xexe' => 'application/x-msdownload',
+            'xext' => 'application/vnd.novadigm.ext',
+            'xez' => 'application/andrew-inset',
+            'xez2' => 'application/vnd.ezpix-album',
+            'xez3' => 'application/vnd.ezpix-package',
+            'xfbs' => 'image/vnd.fastbidsheet',
+            'xfdf' => 'application/vnd.fdf',
+
+            'xfe_launch' => 'application/vnd.denovo.fcselayout-link',
+            'xfg5' => 'application/vnd.fujitsu.oasysgp',
+            'xfli' => 'video/x-fli',
+            'xflo' => 'application/vnd.micrografx.flo',
+            'xflw' => 'application/vnd.kde.kivio',
+            'xflx' => 'text/vnd.fmi.flexstor',
+            'xfly' => 'text/vnd.fly',
+            'xfnc' => 'application/vnd.frogans.fnc',
+            'xfpx' => 'image/vnd.fpx',
+
+            'xfsc' => 'application/vnd.fsc.weblaunch',
+            'xfst' => 'image/vnd.fst',
+            'xftc' => 'application/vnd.fluxtime.clip',
+            'xfti' => 'application/vnd.anser-web-funds-transfer-initiation',
+            'xfvt' => 'video/vnd.fvt',
+            'xfzs' => 'application/vnd.fuzzysheet',
+            'xg3' => 'image/g3fax',
+            'xgac' => 'application/vnd.groove-account',
+            'xgdl' => 'model/vnd.gdl',
+
+            'xghf' => 'application/vnd.groove-help',
+            'xgif' => 'image/gif',
+            'xgim' => 'application/vnd.groove-identity-message',
+            'xgph' => 'application/vnd.flographit',
+            'xgram' => 'application/srgs',
+            'xgrv' => 'application/vnd.groove-injector',
+            'xgrxml' => 'application/srgs+xml',
+            'xgtar' => 'application/x-gtar',
+            'xgtm' => 'application/vnd.groove-tool-message',
+
+            'xsvg' => 'image/svg+xml',
+
+            'xgtw' => 'model/vnd.gtw',
+            'xh261' => 'video/h261',
+            'xh263' => 'video/h263',
+            'xh264' => 'video/h264',
+            'xhbci' => 'application/vnd.hbci',
+            'xhdf' => 'application/x-hdf',
+            'xhlp' => 'application/winhlp',
+            'xhpgl' => 'application/vnd.hp-hpgl',
+            'xhpid' => 'application/vnd.hp-hpid',
+
+            'xhps' => 'application/vnd.hp-hps',
+            'xhqx' => 'application/mac-binhex40',
+            'xhtke' => 'application/vnd.kenameaapp',
+            'xhtm' => 'text/html',
+            'xhtml' => 'text/html',
+            'xhvd' => 'application/vnd.yamaha.hv-dic',
+            'xhvp' => 'application/vnd.yamaha.hv-voice',
+            'xhvs' => 'application/vnd.yamaha.hv-script',
+            'xice' => '#x-conference/x-cooltalk',
+
+            'xico' => 'image/x-icon',
+            'xics' => 'text/calendar',
+            'xief' => 'image/ief',
+            'xifb' => 'text/calendar',
+            'xifm' => 'application/vnd.shana.informed.formdata',
+            'xigl' => 'application/vnd.igloader',
+            'xigx' => 'application/vnd.micrografx.igx',
+            'xiif' => 'application/vnd.shana.informed.interchange',
+            'ximp' => 'application/vnd.accpac.simply.imp',
+
+            'xims' => 'application/vnd.ms-ims',
+            'xin' => 'text/plain',
+            'xipk' => 'application/vnd.shana.informed.package',
+            'xirm' => 'application/vnd.ibm.rights-management',
+            'xirp' => 'application/vnd.irepository.package+xml',
+            'xitp' => 'application/vnd.shana.informed.formtemplate',
+            'xivp' => 'application/vnd.immervision-ivp',
+            'xivu' => 'application/vnd.immervision-ivu',
+            'xjad' => 'text/vnd.sun.j2me.app-descriptor',
+
+            'xjam' => 'application/vnd.jam',
+            'xjava' => 'text/x-java-source',
+            'xjisp' => 'application/vnd.jisp',
+            'xjlt' => 'application/vnd.hp-jlyt',
+            'xjoda' => 'application/vnd.joost.joda-archive',
+            'xjpe' => 'image/jpeg',
+            'xjpeg' => 'image/jpeg',
+            'xjpg' => 'image/jpeg',
+            'xjpgm' => 'video/jpm',
+
+            'xjpgv' => 'video/jpeg',
+            'xjpm' => 'video/jpm',
+            'xjs' => 'application/javascript',
+            'xjson' => 'application/json',
+            'xkar' => 'audio/midi',
+            'xkarbon' => 'application/vnd.kde.karbon',
+            'xkfo' => 'application/vnd.kde.kformula',
+            'xkia' => 'application/vnd.kidspiration',
+            'xkml' => 'application/vnd.google-earth.kml+xml',
+
+            'xkmz' => 'application/vnd.google-earth.kmz',
+            'xkon' => 'application/vnd.kde.kontour',
+            'xksp' => 'application/vnd.kde.kspread',
+            'xlatex' => 'application/x-latex',
+            'xlbd' => 'application/vnd.llamagraphics.life-balance.desktop',
+            'xlbe' => 'application/vnd.llamagraphics.life-balance.exchange+xml',
+            'xles' => 'application/vnd.hhe.lesson-player',
+            'xlist' => 'text/plain',
+            'xlog' => 'text/plain',
+
+            'xlrm' => 'application/vnd.ms-lrm',
+            'xltf' => 'application/vnd.frogans.ltf',
+            'xlvp' => 'audio/vnd.lucent.voice',
+            'xlwp' => 'application/vnd.lotus-wordpro',
+            'xm13' => 'application/x-msmediaview',
+            'xm14' => 'application/x-msmediaview',
+            'xm1v' => 'video/mpeg',
+            'xm2a' => 'audio/mpeg',
+            'xm3a' => 'audio/mpeg',
+
+            'xm3u' => 'audio/x-mpegurl',
+            'xm4u' => 'video/vnd.mpegurl',
+            'xmag' => 'application/vnd.ecowin.chart',
+            'xmathml' => 'application/mathml+xml',
+            'xmbk' => 'application/vnd.mobius.mbk',
+            'xmbox' => 'application/mbox',
+            'xmc1' => 'application/vnd.medcalcdata',
+            'xmcd' => 'application/vnd.mcd',
+            'xmdb' => 'application/x-msaccess',
+
+            'xmdi' => 'image/vnd.ms-modi',
+            'xmesh' => 'model/mesh',
+            'xmfm' => 'application/vnd.mfmp',
+            'xmgz' => 'application/vnd.proteus.magazine',
+            'xmid' => 'audio/midi',
+            'xmidi' => 'audio/midi',
+            'xmif' => 'application/vnd.mif',
+            'xmime' => 'message/rfc822',
+            'xmj2' => 'video/mj2',
+
+            'xmjp2' => 'video/mj2',
+            'xmlp' => 'application/vnd.dolby.mlp',
+            'xmmd' => 'application/vnd.chipnuts.karaoke-mmd',
+            'xmmf' => 'application/vnd.smaf',
+            'xmmr' => 'image/vnd.fujixerox.edmics-mmr',
+            'xmny' => 'application/x-msmoney',
+            'xmov' => 'video/quicktime',
+            'xmovie' => 'video/x-sgi-movie',
+            'xmp2' => 'audio/mpeg',
+
+            'xmp2a' => 'audio/mpeg',
+            'xmp3' => 'audio/mpeg',
+            'xmp4' => 'video/mp4',
+            'xmp4a' => 'audio/mp4',
+            'xmp4s' => 'application/mp4',
+            'xmp4v' => 'video/mp4',
+            'xmpc' => 'application/vnd.mophun.certificate',
+            'xmpe' => 'video/mpeg',
+            'xmpeg' => 'video/mpeg',
+
+            'xmpg' => 'video/mpeg',
+            'xmpg4' => 'video/mp4',
+            'xmpga' => 'audio/mpeg',
+            'xmpkg' => 'application/vnd.apple.installer+xml',
+            'xmpm' => 'application/vnd.blueice.multipass',
+            'xmpn' => 'application/vnd.mophun.application',
+            'xmpp' => 'application/vnd.ms-project',
+            'xmpt' => 'application/vnd.ms-project',
+            'xmpy' => 'application/vnd.ibm.minipay',
+
+            'xmqy' => 'application/vnd.mobius.mqy',
+            'xmrc' => 'application/marc',
+            'xmscml' => 'application/mediaservercontrol+xml',
+            'xmseq' => 'application/vnd.mseq',
+            'xmsf' => 'application/vnd.epson.msf',
+            'xmsh' => 'model/mesh',
+            'xmsi' => 'application/x-msdownload',
+            'xmsl' => 'application/vnd.mobius.msl',
+            'xmsty' => 'application/vnd.muvee.style',
+
+            'xmts' => 'model/vnd.mts',
+            'xmus' => 'application/vnd.musician',
+            'xmvb' => 'application/x-msmediaview',
+            'xmwf' => 'application/vnd.mfer',
+            'xmxf' => 'application/mxf',
+            'xmxl' => 'application/vnd.recordare.musicxml',
+            'xmxml' => 'application/xv+xml',
+            'xmxs' => 'application/vnd.triscape.mxs',
+            'xmxu' => 'video/vnd.mpegurl',
+
+            'xn-gage' => 'application/vnd.nokia.n-gage.symbian.install',
+            'xngdat' => 'application/vnd.nokia.n-gage.data',
+            'xnlu' => 'application/vnd.neurolanguage.nlu',
+            'xnml' => 'application/vnd.enliven',
+            'xnnd' => 'application/vnd.noblenet-directory',
+            'xnns' => 'application/vnd.noblenet-sealer',
+            'xnnw' => 'application/vnd.noblenet-web',
+            'xnpx' => 'image/vnd.net-fpx',
+            'xnsf' => 'application/vnd.lotus-notes',
+
+            'xoa2' => 'application/vnd.fujitsu.oasys2',
+            'xoa3' => 'application/vnd.fujitsu.oasys3',
+            'xoas' => 'application/vnd.fujitsu.oasys',
+            'xobd' => 'application/x-msbinder',
+            'xoda' => 'application/oda',
+            'xodc' => 'application/vnd.oasis.opendocument.chart',
+            'xodf' => 'application/vnd.oasis.opendocument.formula',
+            'xodg' => 'application/vnd.oasis.opendocument.graphics',
+            'xodi' => 'application/vnd.oasis.opendocument.image',
+
+            'xodp' => 'application/vnd.oasis.opendocument.presentation',
+            'xods' => 'application/vnd.oasis.opendocument.spreadsheet',
+            'xodt' => 'application/vnd.oasis.opendocument.text',
+            'xogg' => 'application/ogg',
+            'xoprc' => 'application/vnd.palm',
+            'xorg' => 'application/vnd.lotus-organizer',
+            'xotc' => 'application/vnd.oasis.opendocument.chart-template',
+            'xotf' => 'application/vnd.oasis.opendocument.formula-template',
+            'xotg' => 'application/vnd.oasis.opendocument.graphics-template',
+
+            'xoth' => 'application/vnd.oasis.opendocument.text-web',
+            'xoti' => 'application/vnd.oasis.opendocument.image-template',
+            'xotm' => 'application/vnd.oasis.opendocument.text-master',
+            'xots' => 'application/vnd.oasis.opendocument.spreadsheet-template',
+            'xott' => 'application/vnd.oasis.opendocument.text-template',
+            'xoxt' => 'application/vnd.openofficeorg.extension',
+            'xp10' => 'application/pkcs10',
+            'xp7r' => 'application/x-pkcs7-certreqresp',
+            'xp7s' => 'application/pkcs7-signature',
+
+            'xpbd' => 'application/vnd.powerbuilder6',
+            'xpbm' => 'image/x-portable-bitmap',
+            'xpcl' => 'application/vnd.hp-pcl',
+            'xpclxl' => 'application/vnd.hp-pclxl',
+            'xpct' => 'image/x-pict',
+            'xpcx' => 'image/x-pcx',
+            'xpdb' => 'chemical/x-pdb',
+            'xpdf' => 'application/pdf',
+            'xpfr' => 'application/font-tdpfr',
+
+            'xpgm' => 'image/x-portable-graymap',
+            'xpgn' => 'application/x-chess-pgn',
+            'xpgp' => 'application/pgp-encrypted',
+            'xpic' => 'image/x-pict',
+            'xpki' => 'application/pkixcmp',
+            'xpkipath' => 'application/pkix-pkipath',
+            'xplb' => 'application/vnd.3gpp.pic-bw-large',
+            'xplc' => 'application/vnd.mobius.plc',
+            'xplf' => 'application/vnd.pocketlearn',
+
+            'xpls' => 'application/pls+xml',
+            'xpml' => 'application/vnd.ctc-posml',
+            'xpng' => 'image/png',
+            'xpnm' => 'image/x-portable-anymap',
+            'xportpkg' => 'application/vnd.macports.portpkg',
+            'xpot' => 'application/vnd.ms-powerpoint',
+            'xppd' => 'application/vnd.cups-ppd',
+            'xppm' => 'image/x-portable-pixmap',
+            'xpps' => 'application/vnd.ms-powerpoint',
+
+            'xppt' => 'application/vnd.ms-powerpoint',
+            'xpqa' => 'application/vnd.palm',
+            'xprc' => 'application/vnd.palm',
+            'xpre' => 'application/vnd.lotus-freelance',
+            'xprf' => 'application/pics-rules',
+            'xps' => 'application/postscript',
+            'xpsb' => 'application/vnd.3gpp.pic-bw-small',
+            'xpsd' => 'image/vnd.adobe.photoshop',
+            'xptid' => 'application/vnd.pvi.ptid1',
+
+            'xpub' => 'application/x-mspublisher',
+            'xpvb' => 'application/vnd.3gpp.pic-bw-var',
+            'xpwn' => 'application/vnd.3m.post-it-notes',
+            'xqam' => 'application/vnd.epson.quickanime',
+            'xqbo' => 'application/vnd.intu.qbo',
+            'xqfx' => 'application/vnd.intu.qfx',
+            'xqps' => 'application/vnd.publishare-delta-tree',
+            'xqt' => 'video/quicktime',
+            'xra' => 'audio/x-pn-realaudio',
+
+            'xram' => 'audio/x-pn-realaudio',
+            'xrar' => 'application/x-rar-compressed',
+            'xras' => 'image/x-cmu-raster',
+            'xrcprofile' => 'application/vnd.ipunplugged.rcprofile',
+            'xrdf' => 'application/rdf+xml',
+            'xrdz' => 'application/vnd.data-vision.rdz',
+            'xrep' => 'application/vnd.businessobjects',
+            'xrgb' => 'image/x-rgb',
+            'xrif' => 'application/reginfo+xml',
+
+            'xrl' => 'application/resource-lists+xml',
+            'xrlc' => 'image/vnd.fujixerox.edmics-rlc',
+            'xrm' => 'application/vnd.rn-realmedia',
+            'xrmi' => 'audio/midi',
+            'xrmp' => 'audio/x-pn-realaudio-plugin',
+            'xrms' => 'application/vnd.jcp.javame.midlet-rms',
+            'xrnc' => 'application/relax-ng-compact-syntax',
+            'xrpss' => 'application/vnd.nokia.radio-presets',
+            'xrpst' => 'application/vnd.nokia.radio-preset',
+
+            'xrq' => 'application/sparql-query',
+            'xrs' => 'application/rls-services+xml',
+            'xrsd' => 'application/rsd+xml',
+            'xrss' => 'application/rss+xml',
+            'xrtf' => 'application/rtf',
+            'xrtx' => 'text/richtext',
+            'xsaf' => 'application/vnd.yamaha.smaf-audio',
+            'xsbml' => 'application/sbml+xml',
+            'xsc' => 'application/vnd.ibm.secure-container',
+
+            'xscd' => 'application/x-msschedule',
+            'xscm' => 'application/vnd.lotus-screencam',
+            'xscq' => 'application/scvp-cv-request',
+            'xscs' => 'application/scvp-cv-response',
+            'xsdp' => 'application/sdp',
+            'xsee' => 'application/vnd.seemail',
+            'xsema' => 'application/vnd.sema',
+            'xsemd' => 'application/vnd.semd',
+            'xsemf' => 'application/vnd.semf',
+
+            'xsetpay' => 'application/set-payment-initiation',
+            'xsetreg' => 'application/set-registration-initiation',
+            'xsfs' => 'application/vnd.spotfire.sfs',
+            'xsgm' => 'text/sgml',
+            'xsgml' => 'text/sgml',
+            'xsh' => 'application/x-sh',
+            'xshar' => 'application/x-shar',
+            'xshf' => 'application/shf+xml',
+            'xsilo' => 'model/mesh',
+
+            'xsit' => 'application/x-stuffit',
+            'xsitx' => 'application/x-stuffitx',
+            'xslt' => 'application/vnd.epson.salt',
+            'xsnd' => 'audio/basic',
+            'xspf' => 'application/vnd.yamaha.smaf-phrase',
+            'xspl' => 'application/x-futuresplash',
+            'xspot' => 'text/vnd.in3d.spot',
+            'xspp' => 'application/scvp-vp-response',
+            'xspq' => 'application/scvp-vp-request',
+
+            'xsrc' => 'application/x-wais-source',
+            'xsrx' => 'application/sparql-results+xml',
+            'xssf' => 'application/vnd.epson.ssf',
+            'xssml' => 'application/ssml+xml',
+            'xstf' => 'application/vnd.wt.stf',
+            'xstk' => 'application/hyperstudio',
+            'xstr' => 'application/vnd.pg.format',
+            'xsus' => 'application/vnd.sus-calendar',
+            'xsusp' => 'application/vnd.sus-calendar',
+
+            'xsv4cpio' => 'application/x-sv4cpio',
+            'xsv4crc' => 'application/x-sv4crc',
+            'xsvd' => 'application/vnd.svd',
+            'xswf' => 'application/x-shockwave-flash',
+            'xtao' => 'application/vnd.tao.intent-module-archive',
+            'xtar' => 'application/x-tar',
+            'xtcap' => 'application/vnd.3gpp2.tcap',
+            'xtcl' => 'application/x-tcl',
+            'xtex' => 'application/x-tex',
+
+            'xtext' => 'text/plain',
+            'xtif' => 'image/tiff',
+            'xtiff' => 'image/tiff',
+            'xtmo' => 'application/vnd.tmobile-livetv',
+            'xtorrent' => 'application/x-bittorrent',
+            'xtpl' => 'application/vnd.groove-tool-template',
+            'xtpt' => 'application/vnd.trid.tpt',
+            'xtra' => 'application/vnd.trueapp',
+            'xtrm' => 'application/x-msterminal',
+
+            'xtsv' => 'text/tab-separated-values',
+            'xtxd' => 'application/vnd.genomatix.tuxedo',
+            'xtxf' => 'application/vnd.mobius.txf',
+            'xtxt' => 'text/plain',
+            'xumj' => 'application/vnd.umajin',
+            'xunityweb' => 'application/vnd.unity',
+            'xuoml' => 'application/vnd.uoml+xml',
+            'xuri' => 'text/uri-list',
+            'xuris' => 'text/uri-list',
+
+            'xurls' => 'text/uri-list',
+            'xustar' => 'application/x-ustar',
+            'xutz' => 'application/vnd.uiq.theme',
+            'xuu' => 'text/x-uuencode',
+            'xvcd' => 'application/x-cdlink',
+            'xvcf' => 'text/x-vcard',
+            'xvcg' => 'application/vnd.groove-vcard',
+            'xvcs' => 'text/x-vcalendar',
+            'xvcx' => 'application/vnd.vcx',
+
+            'xvis' => 'application/vnd.visionary',
+            'xviv' => 'video/vnd.vivo',
+            'xvrml' => 'model/vrml',
+            'xvsd' => 'application/vnd.visio',
+            'xvsf' => 'application/vnd.vsf',
+            'xvss' => 'application/vnd.visio',
+            'xvst' => 'application/vnd.visio',
+            'xvsw' => 'application/vnd.visio',
+            'xvtu' => 'model/vnd.vtu',
+
+            'xvxml' => 'application/voicexml+xml',
+            'xwav' => 'audio/x-wav',
+            'xwax' => 'audio/x-ms-wax',
+            'xwbmp' => 'image/vnd.wap.wbmp',
+            'xwbs' => 'application/vnd.criticaltools.wbs+xml',
+            'xwbxml' => 'application/vnd.wap.wbxml',
+            'xwcm' => 'application/vnd.ms-works',
+            'xwdb' => 'application/vnd.ms-works',
+            'xwks' => 'application/vnd.ms-works',
+
+            'xwm' => 'video/x-ms-wm',
+            'xwma' => 'audio/x-ms-wma',
+            'xwmd' => 'application/x-ms-wmd',
+            'xwmf' => 'application/x-msmetafile',
+            'xwml' => 'text/vnd.wap.wml',
+            'xwmlc' => 'application/vnd.wap.wmlc',
+            'xwmls' => 'text/vnd.wap.wmlscript',
+            'xwmlsc' => 'application/vnd.wap.wmlscriptc',
+            'xwmv' => 'video/x-ms-wmv',
+
+            'xwmx' => 'video/x-ms-wmx',
+            'xwmz' => 'application/x-ms-wmz',
+            'xwpd' => 'application/vnd.wordperfect',
+            'xwpl' => 'application/vnd.ms-wpl',
+            'xwps' => 'application/vnd.ms-works',
+            'xwqd' => 'application/vnd.wqd',
+            'xwri' => 'application/x-mswrite',
+            'xwrl' => 'model/vrml',
+            'xwsdl' => 'application/wsdl+xml',
+
+            'xwspolicy' => 'application/wspolicy+xml',
+            'xwtb' => 'application/vnd.webturbo',
+            'xwvx' => 'video/x-ms-wvx',
+            'xx3d' => 'application/vnd.hzn-3d-crossword',
+            'xxar' => 'application/vnd.xara',
+            'xxbd' => 'application/vnd.fujixerox.docuworks.binder',
+            'xxbm' => 'image/x-xbitmap',
+            'xxdm' => 'application/vnd.syncml.dm+xml',
+            'xxdp' => 'application/vnd.adobe.xdp+xml',
+
+            'xxdw' => 'application/vnd.fujixerox.docuworks',
+            'xxenc' => 'application/xenc+xml',
+            'xxfdf' => 'application/vnd.adobe.xfdf',
+            'xxfdl' => 'application/vnd.xfdl',
+            'xxht' => 'application/xhtml+xml',
+            'xxhtml' => 'application/xhtml+xml',
+            'xxhvml' => 'application/xv+xml',
+            'xxif' => 'image/vnd.xiff',
+            'xxla' => 'application/vnd.ms-excel',
+
+            'xxlc' => 'application/vnd.ms-excel',
+            'xxlm' => 'application/vnd.ms-excel',
+            'xxls' => 'application/vnd.ms-excel',
+            'xxlt' => 'application/vnd.ms-excel',
+            'xxlw' => 'application/vnd.ms-excel',
+            'xxml' => 'application/xml',
+            'xxo' => 'application/vnd.olpc-sugar',
+            'xxop' => 'application/xop+xml',
+            'xxpm' => 'image/x-xpixmap',
+
+            'xxpr' => 'application/vnd.is-xpr',
+            'xxps' => 'application/vnd.ms-xpsdocument',
+            'xxsl' => 'application/xml',
+            'xxslt' => 'application/xslt+xml',
+            'xxsm' => 'application/vnd.syncml+xml',
+            'xxspf' => 'application/xspf+xml',
+            'xxul' => 'application/vnd.mozilla.xul+xml',
+            'xxvm' => 'application/xv+xml',
+            'xxvml' => 'application/xv+xml',
+
+            'xxwd' => 'image/x-xwindowdump',
+            'xxyz' => 'chemical/x-xyz',
+            'xzaz' => 'application/vnd.zzazz.deck+xml',
+            'xzip' => 'application/zip',
+            'xzmm' => 'application/vnd.handheld-entertainment+xml',
+        );
+
+    /**
+     * Extend list of MIME types if needed from config
+     */
+    public function __construct()
+    {
+        $nodes = Mage::getConfig()->getNode('global/mime/types');
+        if ($nodes) {
+            $nodes = (array)$nodes;
+            foreach ($nodes as $key => $value) {
+                $this->_mimeTypes[$key] = $value;
+            }
+        }
+    }
+
+    /**
+     * Get MIME type by file extension from list of pre-defined MIME types
+     *
+     * @param $ext
+     * @return string
+     */
+    public function getMimeTypeByExtension($ext)
+    {
+        $type = 'x' . $ext;
+        if (isset($this->_mimeTypes[$type])) {
+            return $this->_mimeTypes[$type];
+        }
+        return 'application/octet-stream';
+    }
+
+    /**
+     * Get all MIME Types
+     *
+     * @return array
+     */
+    public function getMimeTypes()
+    {
+        return $this->_mimeTypes;
+    }
+
+    /**
+     * Get array of MIME types associated with given file extension
+     *
+     * @param array|string $extensionsList
+     * @return array
+     */
+    public function getMimeTypeFromExtensionList($extensionsList)
+    {
+        if (is_string($extensionsList)) {
+            $extensionsList = array_map('trim', explode(',', $extensionsList));
+        }
+
+        return array_map(array($this, 'getMimeTypeByExtension'), $extensionsList);
+    }
+
+    /**
+     * Get post_max_size server setting
+     *
+     * @return string
+     */
+    public function getPostMaxSize()
+    {
+        return ini_get('post_max_size');
+    }
+
+    /**
+     * Get upload_max_filesize server setting
+     *
+     * @return string
+     */
+    public function getUploadMaxSize()
+    {
+        return ini_get('upload_max_filesize');
+    }
+
+    /**
+     * Get max upload size
+     *
+     * @return mixed
+     */
+    public function getDataMaxSize()
+    {
+        return min($this->getPostMaxSize(), $this->getUploadMaxSize());
+    }
+
+    /**
+     * Get maximum upload size in bytes
+     *
+     * @return int
+     */
+    public function getDataMaxSizeInBytes()
+    {
+        $iniSize = $this->getDataMaxSize();
+        $size = substr($iniSize, 0, strlen($iniSize)-1);
+        $parsedSize = 0;
+        switch (strtolower(substr($iniSize, strlen($iniSize)-1))) {
+            case 't':
+                $parsedSize = $size*(1024*1024*1024*1024);
+                break;
+            case 'g':
+                $parsedSize = $size*(1024*1024*1024);
+                break;
+            case 'm':
+                $parsedSize = $size*(1024*1024);
+                break;
+            case 'k':
+                $parsedSize = $size*1024;
+                break;
+            case 'b':
+            default:
+                $parsedSize = $size;
+                break;
+        }
+        return (int)$parsedSize;
+    }
+
+}
diff --git app/code/core/Mage/Uploader/Model/Config/Abstract.php app/code/core/Mage/Uploader/Model/Config/Abstract.php
new file mode 100644
index 0000000..b11f11e
--- /dev/null
+++ app/code/core/Mage/Uploader/Model/Config/Abstract.php
@@ -0,0 +1,69 @@
+<?php
+/**
+ * Magento
+ *
+ * NOTICE OF LICENSE
+ *
+ * This source file is subject to the Open Software License (OSL 3.0)
+ * that is bundled with this package in the file LICENSE.txt.
+ * It is also available through the world-wide-web at this URL:
+ * http://opensource.org/licenses/osl-3.0.php
+ * If you did not receive a copy of the license and are unable to
+ * obtain it through the world-wide-web, please send an email
+ * to license@magento.com so we can send you a copy immediately.
+ *
+ * DISCLAIMER
+ *
+ * Do not edit or add to this file if you wish to upgrade Magento to newer
+ * versions in the future. If you wish to customize Magento for your
+ * needs please refer to http://www.magento.com for more information.
+ *
+ * @category    Mage
+ * @package     Mage_Uploader
+ * @copyright Copyright (c) 2006-2016 X.commerce, Inc. and affiliates (http://www.magento.com)
+ * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
+ */
+
+abstract class Mage_Uploader_Model_Config_Abstract extends Varien_Object
+{
+    /**
+     * Get file helper
+     *
+     * @return Mage_Uploader_Helper_File
+     */
+    protected function _getHelper()
+    {
+        return Mage::helper('uploader/file');
+    }
+
+    /**
+     * Set/Get attribute wrapper
+     * Also set data in cameCase for config values
+     *
+     * @param string $method
+     * @param array $args
+     * @return bool|mixed|Varien_Object
+     * @throws Varien_Exception
+     */
+    public function __call($method, $args)
+    {
+        $key = lcfirst($this->_camelize(substr($method,3)));
+        switch (substr($method, 0, 3)) {
+            case 'get' :
+                $data = $this->getData($key, isset($args[0]) ? $args[0] : null);
+                return $data;
+
+            case 'set' :
+                $result = $this->setData($key, isset($args[0]) ? $args[0] : null);
+                return $result;
+
+            case 'uns' :
+                $result = $this->unsetData($key);
+                return $result;
+
+            case 'has' :
+                return isset($this->_data[$key]);
+        }
+        throw new Varien_Exception("Invalid method ".get_class($this)."::".$method."(".print_r($args,1).")");
+    }
+}
diff --git app/code/core/Mage/Uploader/Model/Config/Browsebutton.php app/code/core/Mage/Uploader/Model/Config/Browsebutton.php
new file mode 100644
index 0000000..442f254
--- /dev/null
+++ app/code/core/Mage/Uploader/Model/Config/Browsebutton.php
@@ -0,0 +1,63 @@
+<?php
+/**
+ * Magento
+ *
+ * NOTICE OF LICENSE
+ *
+ * This source file is subject to the Open Software License (OSL 3.0)
+ * that is bundled with this package in the file LICENSE.txt.
+ * It is also available through the world-wide-web at this URL:
+ * http://opensource.org/licenses/osl-3.0.php
+ * If you did not receive a copy of the license and are unable to
+ * obtain it through the world-wide-web, please send an email
+ * to license@magento.com so we can send you a copy immediately.
+ *
+ * DISCLAIMER
+ *
+ * Do not edit or add to this file if you wish to upgrade Magento to newer
+ * versions in the future. If you wish to customize Magento for your
+ * needs please refer to http://www.magento.com for more information.
+ *
+ * @category  Mage
+ * @package   Mage_Uploader
+ * @copyright Copyright (c) 2006-2016 X.commerce, Inc. and affiliates (http://www.magento.com)
+ * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
+
+ * @method Mage_Uploader_Model_Config_Browsebutton setDomNodes(array $domNodesIds)
+ *      Array of element browse buttons ids
+ * @method Mage_Uploader_Model_Config_Browsebutton setIsDirectory(bool $isDirectory)
+ *      Pass in true to allow directories to be selected (Google Chrome only)
+ * @method Mage_Uploader_Model_Config_Browsebutton setSingleFile(bool $isSingleFile)
+ *      To prevent multiple file uploads set this to true.
+ *      Also look at config parameter singleFile (Mage_Uploader_Model_Config_Uploader setSingleFile())
+ * @method Mage_Uploader_Model_Config_Browsebutton setAttributes(array $attributes)
+ *      Pass object of keys and values to set custom attributes on input fields.
+ *      @see http://www.w3.org/TR/html-markup/input.file.html#input.file-attributes
+ */
+
+class Mage_Uploader_Model_Config_Browsebutton extends Mage_Uploader_Model_Config_Abstract
+{
+    /**
+     * Set params for browse button
+     */
+    protected function _construct()
+    {
+        $this->setIsDirectory(false);
+    }
+
+    /**
+     * Get MIME types from files extensions
+     *
+     * @param string|array $exts
+     * @return string
+     */
+    public function getMimeTypesByExtensions($exts)
+    {
+        $mimes = array_unique($this->_getHelper()->getMimeTypeFromExtensionList($exts));
+
+        // Not include general file type
+        unset($mimes['application/octet-stream']);
+
+        return implode(',', $mimes);
+    }
+}
diff --git app/code/core/Mage/Uploader/Model/Config/Misc.php app/code/core/Mage/Uploader/Model/Config/Misc.php
new file mode 100644
index 0000000..8231844
--- /dev/null
+++ app/code/core/Mage/Uploader/Model/Config/Misc.php
@@ -0,0 +1,46 @@
+<?php
+/**
+ * Magento
+ *
+ * NOTICE OF LICENSE
+ *
+ * This source file is subject to the Open Software License (OSL 3.0)
+ * that is bundled with this package in the file LICENSE.txt.
+ * It is also available through the world-wide-web at this URL:
+ * http://opensource.org/licenses/osl-3.0.php
+ * If you did not receive a copy of the license and are unable to
+ * obtain it through the world-wide-web, please send an email
+ * to license@magento.com so we can send you a copy immediately.
+ *
+ * DISCLAIMER
+ *
+ * Do not edit or add to this file if you wish to upgrade Magento to newer
+ * versions in the future. If you wish to customize Magento for your
+ * needs please refer to http://www.magento.com for more information.
+ *
+ * @category   Mage
+ * @package    Mage_Uploader
+ * @copyright  Copyright (c) 2006-2016 X.commerce, Inc. and affiliates (http://www.magento.com)
+ * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
+ * 
+ * @method Mage_Uploader_Model_Config_Misc setMaxSizePlural (string $sizePlural) Set plural info about max upload size
+ * @method Mage_Uploader_Model_Config_Misc setMaxSizeInBytes (int $sizeInBytes) Set max upload size in bytes
+ * @method Mage_Uploader_Model_Config_Misc setReplaceBrowseWithRemove (bool $replaceBrowseWithRemove)
+ *      Replace browse button with remove
+ *
+ * Class Mage_Uploader_Model_Config_Misc
+ */
+
+class Mage_Uploader_Model_Config_Misc extends Mage_Uploader_Model_Config_Abstract
+{
+    /**
+     * Prepare misc params
+     */
+    protected function _construct()
+    {
+        $this
+            ->setMaxSizeInBytes($this->_getHelper()->getDataMaxSizeInBytes())
+            ->setMaxSizePlural($this->_getHelper()->getDataMaxSize())
+        ;
+    }
+}
diff --git app/code/core/Mage/Uploader/Model/Config/Uploader.php app/code/core/Mage/Uploader/Model/Config/Uploader.php
new file mode 100644
index 0000000..9e35570
--- /dev/null
+++ app/code/core/Mage/Uploader/Model/Config/Uploader.php
@@ -0,0 +1,122 @@
+<?php
+/**
+ * Magento
+ *
+ * NOTICE OF LICENSE
+ *
+ * This source file is subject to the Open Software License (OSL 3.0)
+ * that is bundled with this package in the file LICENSE.txt.
+ * It is also available through the world-wide-web at this URL:
+ * http://opensource.org/licenses/osl-3.0.php
+ * If you did not receive a copy of the license and are unable to
+ * obtain it through the world-wide-web, please send an email
+ * to license@magento.com so we can send you a copy immediately.
+ *
+ * DISCLAIMER
+ *
+ * Do not edit or add to this file if you wish to upgrade Magento to newer
+ * versions in the future. If you wish to customize Magento for your
+ * needs please refer to http://www.magento.com for more information.
+ *
+ * @category   Mage
+ * @package    Mage_Uploader
+ * @copyright  Copyright (c) 2006-2016 X.commerce, Inc. and affiliates (http://www.magento.com)
+ * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
+ */
+
+/**
+ * @method Mage_Uploader_Model_Config_Uploader setTarget(string $url)
+ *      The target URL for the multipart POST request.
+ * @method Mage_Uploader_Model_Config_Uploader setSingleFile(bool $isSingleFile)
+ *      Enable single file upload.
+ *      Once one file is uploaded, second file will overtake existing one, first one will be canceled.
+ * @method Mage_Uploader_Model_Config_Uploader setChunkSize(int $chunkSize) The size in bytes of each uploaded chunk of data.
+ * @method Mage_Uploader_Model_Config_Uploader setForceChunkSize(bool $forceChunkSize)
+ *      Force all chunks to be less or equal than chunkSize.
+ * @method Mage_Uploader_Model_Config_Uploader setSimultaneousUploads(int $amountOfSimultaneousUploads)
+ * @method Mage_Uploader_Model_Config_Uploader setFileParameterName(string $fileUploadParam)
+ * @method Mage_Uploader_Model_Config_Uploader setQuery(array $additionalQuery)
+ * @method Mage_Uploader_Model_Config_Uploader setHeaders(array $headers)
+ *      Extra headers to include in the multipart POST with data.
+ * @method Mage_Uploader_Model_Config_Uploader setWithCredentials(bool $isCORS)
+ *      Standard CORS requests do not send or set any cookies by default.
+ *      In order to include cookies as part of the request, you need to set the withCredentials property to true.
+ * @method Mage_Uploader_Model_Config_Uploader setMethod(string $sendMethod)
+ *       Method to use when POSTing chunks to the server. Defaults to "multipart"
+ * @method Mage_Uploader_Model_Config_Uploader setTestMethod(string $testMethod) Defaults to "GET"
+ * @method Mage_Uploader_Model_Config_Uploader setUploadMethod(string $uploadMethod) Defaults to "POST"
+ * @method Mage_Uploader_Model_Config_Uploader setAllowDuplicateUploads(bool $allowDuplicateUploads)
+ *      Once a file is uploaded, allow reupload of the same file. By default, if a file is already uploaded,
+ *      it will be skipped unless the file is removed from the existing Flow object.
+ * @method Mage_Uploader_Model_Config_Uploader setPrioritizeFirstAndLastChunk(bool $prioritizeFirstAndLastChunk)
+ *      This can be handy if you can determine if a file is valid for your service from only the first or last chunk.
+ * @method Mage_Uploader_Model_Config_Uploader setTestChunks(bool $prioritizeFirstAndLastChunk)
+ *      Make a GET request to the server for each chunks to see if it already exists.
+ * @method Mage_Uploader_Model_Config_Uploader setPreprocess(bool $prioritizeFirstAndLastChunk)
+ *      Optional function to process each chunk before testing & sending.
+ * @method Mage_Uploader_Model_Config_Uploader setInitFileFn(string $function)
+ *      Optional function to initialize the fileObject (js).
+ * @method Mage_Uploader_Model_Config_Uploader setReadFileFn(string $function)
+ *      Optional function wrapping reading operation from the original file.
+ * @method Mage_Uploader_Model_Config_Uploader setGenerateUniqueIdentifier(string $function)
+ *      Override the function that generates unique identifiers for each file. Defaults to "null"
+ * @method Mage_Uploader_Model_Config_Uploader setMaxChunkRetries(int $maxChunkRetries) Defaults to 0
+ * @method Mage_Uploader_Model_Config_Uploader setChunkRetryInterval(int $chunkRetryInterval) Defaults to "undefined"
+ * @method Mage_Uploader_Model_Config_Uploader setProgressCallbacksInterval(int $progressCallbacksInterval)
+ * @method Mage_Uploader_Model_Config_Uploader setSpeedSmoothingFactor(int $speedSmoothingFactor)
+ *      Used for calculating average upload speed. Number from 1 to 0.
+ *      Set to 1 and average upload speed wil be equal to current upload speed.
+ *      For longer file uploads it is better set this number to 0.02,
+ *      because time remaining estimation will be more accurate.
+ * @method Mage_Uploader_Model_Config_Uploader setSuccessStatuses(array $successStatuses)
+ *      Response is success if response status is in this list
+ * @method Mage_Uploader_Model_Config_Uploader setPermanentErrors(array $permanentErrors)
+ *      Response fails if response status is in this list
+ *
+ * Class Mage_Uploader_Model_Config_Uploader
+ */
+
+class Mage_Uploader_Model_Config_Uploader extends Mage_Uploader_Model_Config_Abstract
+{
+    /**
+     * Type of upload
+     */
+    const UPLOAD_TYPE = 'multipart';
+
+    /**
+     * Test chunks on resumable uploads
+     */
+    const TEST_CHUNKS = false;
+
+    /**
+     * Used for calculating average upload speed.
+     */
+    const SMOOTH_UPLOAD_FACTOR = 0.02;
+
+    /**
+     * Progress check interval
+     */
+    const PROGRESS_CALLBACK_INTERVAL = 0;
+
+    /**
+     * Set default values for uploader
+     */
+    protected function _construct()
+    {
+        $this
+            ->setChunkSize($this->_getHelper()->getDataMaxSizeInBytes())
+            ->setWithCredentials(false)
+            ->setForceChunkSize(false)
+            ->setQuery(array(
+                'form_key' => Mage::getSingleton('core/session')->getFormKey()
+            ))
+            ->setMethod(self::UPLOAD_TYPE)
+            ->setAllowDuplicateUploads(true)
+            ->setPrioritizeFirstAndLastChunk(false)
+            ->setTestChunks(self::TEST_CHUNKS)
+            ->setSpeedSmoothingFactor(self::SMOOTH_UPLOAD_FACTOR)
+            ->setProgressCallbacksInterval(self::PROGRESS_CALLBACK_INTERVAL)
+            ->setSuccessStatuses(array(200, 201, 202))
+            ->setPermanentErrors(array(404, 415, 500, 501));
+    }
+}
diff --git app/code/core/Mage/Uploader/etc/config.xml app/code/core/Mage/Uploader/etc/config.xml
new file mode 100644
index 0000000..d3fcd40
--- /dev/null
+++ app/code/core/Mage/Uploader/etc/config.xml
@@ -0,0 +1,51 @@
+<?xml version="1.0"?>
+<!--
+/**
+ * Magento
+ *
+ * NOTICE OF LICENSE
+ *
+ * This source file is subject to the Open Software License (OSL 3.0)
+ * that is bundled with this package in the file LICENSE.txt.
+ * It is also available through the world-wide-web at this URL:
+ * http://opensource.org/licenses/osl-3.0.php
+ * If you did not receive a copy of the license and are unable to
+ * obtain it through the world-wide-web, please send an email
+ * to license@magento.com so we can send you a copy immediately.
+ *
+ * DISCLAIMER
+ *
+ * Do not edit or add to this file if you wish to upgrade Magento to newer
+ * versions in the future. If you wish to customize Magento for your
+ * needs please refer to http://www.magento.com for more information.
+ *
+ * @category    Mage
+ * @package     Mage_Uploader
+ * @copyright   Copyright (c) 2006-2016 X.commerce, Inc. and affiliates (http://www.magento.com)
+ * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
+ */
+-->
+<config>
+    <modules>
+        <Mage_Uploader>
+            <version>0.1.0</version>
+        </Mage_Uploader>
+    </modules>
+    <global>
+        <blocks>
+            <uploader>
+                <class>Mage_Uploader_Block</class>
+            </uploader>
+        </blocks>
+        <helpers>
+            <uploader>
+                <class>Mage_Uploader_Helper</class>
+            </uploader>
+        </helpers>
+        <models>
+            <uploader>
+                <class>Mage_Uploader_Model</class>
+            </uploader>
+        </models>
+    </global>
+</config>
diff --git app/code/core/Mage/Uploader/etc/jstranslator.xml app/code/core/Mage/Uploader/etc/jstranslator.xml
new file mode 100644
index 0000000..4d7d405
--- /dev/null
+++ app/code/core/Mage/Uploader/etc/jstranslator.xml
@@ -0,0 +1,44 @@
+<?xml version="1.0"?>
+<!--
+/**
+ * Magento
+ *
+ * NOTICE OF LICENSE
+ *
+ * This source file is subject to the Open Software License (OSL 3.0)
+ * that is bundled with this package in the file LICENSE.txt.
+ * It is also available through the world-wide-web at this URL:
+ * http://opensource.org/licenses/osl-3.0.php
+ * If you did not receive a copy of the license and are unable to
+ * obtain it through the world-wide-web, please send an email
+ * to license@magento.com so we can send you a copy immediately.
+ *
+ * DISCLAIMER
+ *
+ * Do not edit or add to this file if you wish to upgrade Magento to newer
+ * versions in the future. If you wish to customize Magento for your
+ * needs please refer to http://www.magento.com for more information.
+ *
+ * @category   Mage
+ * @package    Mage_Uploader
+ * @copyright  Copyright (c) 2006-2016 X.commerce, Inc. and affiliates (http://www.magento.com)
+ * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
+ */
+-->
+<jstranslator>
+    <uploader-exceed_max-1 translate="message" module="uploader">
+        <message>Maximum allowed file size for upload is</message>
+    </uploader-exceed_max-1>
+    <uploader-exceed_max-2 translate="message" module="uploader">
+        <message>Please check your server PHP settings.</message>
+    </uploader-exceed_max-2>
+    <uploader-tab-change-event-confirm translate="message" module="uploader">
+        <message>There are files that were selected but not uploaded yet. After switching to another tab your selections will be lost. Do you wish to continue ?</message>
+    </uploader-tab-change-event-confirm>
+    <uploader-complete-event-text translate="message" module="uploader">
+        <message>Complete</message>
+    </uploader-complete-event-text>
+    <uploader-uploading-progress translate="message" module="uploader">
+        <message>Uploading...</message>
+    </uploader-uploading-progress>
+</jstranslator>
diff --git app/code/core/Mage/Usa/Model/Shipping/Carrier/Dhl.php app/code/core/Mage/Usa/Model/Shipping/Carrier/Dhl.php
index ed20e08..d89cfa5 100644
--- app/code/core/Mage/Usa/Model/Shipping/Carrier/Dhl.php
+++ app/code/core/Mage/Usa/Model/Shipping/Carrier/Dhl.php
@@ -566,8 +566,8 @@ class Mage_Usa_Model_Shipping_Carrier_Dhl
                 $ch = curl_init();
                 curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                 curl_setopt($ch, CURLOPT_URL, $url);
-                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
-                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
+                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $this->getConfigFlag('verify_peer'));
+                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
                 curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
                 $responseBody = curl_exec($ch);
                 curl_close($ch);
@@ -1070,8 +1070,8 @@ class Mage_Usa_Model_Shipping_Carrier_Dhl
             $ch = curl_init();
             curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
             curl_setopt($ch, CURLOPT_URL, $url);
-            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
-            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
+            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $this->getConfigFlag('verify_peer'));
+            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
             curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
             $responseBody = curl_exec($ch);
             $debugData['result'] = $responseBody;
diff --git app/code/core/Mage/Usa/Model/Shipping/Carrier/Dhl/International.php app/code/core/Mage/Usa/Model/Shipping/Carrier/Dhl/International.php
index 06bf2f5..5c564da 100644
--- app/code/core/Mage/Usa/Model/Shipping/Carrier/Dhl/International.php
+++ app/code/core/Mage/Usa/Model/Shipping/Carrier/Dhl/International.php
@@ -841,7 +841,12 @@ class Mage_Usa_Model_Shipping_Carrier_Dhl_International
             try {
                 $client = new Varien_Http_Client();
                 $client->setUri((string)$this->getConfigData('gateway_url'));
-                $client->setConfig(array('maxredirects' => 0, 'timeout' => 30));
+                $client->setConfig(array(
+                    'maxredirects' => 0,
+                    'timeout' => 30,
+                    'verifypeer' => $this->getConfigFlag('verify_peer'),
+                    'verifyhost' => 2,
+                ));
                 $client->setRawData($request);
                 $responseBody = $client->request(Varien_Http_Client::POST)->getBody();
                 $debugData['result'] = $responseBody;
@@ -1362,7 +1367,12 @@ class Mage_Usa_Model_Shipping_Carrier_Dhl_International
             try {
                 $client = new Varien_Http_Client();
                 $client->setUri((string)$this->getConfigData('gateway_url'));
-                $client->setConfig(array('maxredirects' => 0, 'timeout' => 30));
+                $client->setConfig(array(
+                    'maxredirects' => 0,
+                    'timeout' => 30,
+                    'verifypeer' => $this->getConfigFlag('verify_peer'),
+                    'verifyhost' => 2,
+                ));
                 $client->setRawData($request);
                 $responseBody = $client->request(Varien_Http_Client::POST)->getBody();
                 $debugData['result'] = $responseBody;
@@ -1554,7 +1564,12 @@ class Mage_Usa_Model_Shipping_Carrier_Dhl_International
             try {
                 $client = new Varien_Http_Client();
                 $client->setUri((string)$this->getConfigData('gateway_url'));
-                $client->setConfig(array('maxredirects' => 0, 'timeout' => 30));
+                $client->setConfig(array(
+                    'maxredirects' => 0,
+                    'timeout' => 30,
+                    'verifypeer' => $this->getConfigFlag('verify_peer'),
+                    'verifyhost' => 2,
+                ));
                 $client->setRawData($request);
                 $responseBody = $client->request(Varien_Http_Client::POST)->getBody();
                 $debugData['result'] = $responseBody;
diff --git app/code/core/Mage/Usa/Model/Shipping/Carrier/Fedex.php app/code/core/Mage/Usa/Model/Shipping/Carrier/Fedex.php
index 604f1fa..f04dc04 100644
--- app/code/core/Mage/Usa/Model/Shipping/Carrier/Fedex.php
+++ app/code/core/Mage/Usa/Model/Shipping/Carrier/Fedex.php
@@ -563,6 +563,7 @@ class Mage_Usa_Model_Shipping_Carrier_Fedex
     /**
      * Get xml quotes
      *
+     * @deprecated
      * @return Mage_Shipping_Model_Rate_Result
      */
     protected function _getXmlQuotes()
@@ -622,8 +623,8 @@ class Mage_Usa_Model_Shipping_Carrier_Fedex
                 $ch = curl_init();
                 curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                 curl_setopt($ch, CURLOPT_URL, $url);
-                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
-                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
+                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
+                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
                 curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
                 $responseBody = curl_exec($ch);
                 curl_close ($ch);
diff --git app/code/core/Mage/Usa/Model/Shipping/Carrier/Ups.php app/code/core/Mage/Usa/Model/Shipping/Carrier/Ups.php
index a026a7e..aaca20a 100644
--- app/code/core/Mage/Usa/Model/Shipping/Carrier/Ups.php
+++ app/code/core/Mage/Usa/Model/Shipping/Carrier/Ups.php
@@ -932,7 +932,7 @@ XMLRequest;
                 curl_setopt($ch, CURLOPT_POST, 1);
                 curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlRequest);
                 curl_setopt($ch, CURLOPT_TIMEOUT, 30);
-                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, (boolean)$this->getConfigFlag('mode_xml'));
+                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $this->getConfigFlag('verify_peer'));
                 $xmlResponse = curl_exec ($ch);
 
                 $debugData['result'] = $xmlResponse;
@@ -1567,7 +1567,7 @@ XMLAuth;
             curl_setopt($ch, CURLOPT_POST, 1);
             curl_setopt($ch, CURLOPT_POSTFIELDS, $this->_xmlAccessRequest . $xmlRequest->asXML());
             curl_setopt($ch, CURLOPT_TIMEOUT, 30);
-            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, (boolean)$this->getConfigFlag('mode_xml'));
+            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $this->getConfigFlag('verify_peer'));
             $xmlResponse = curl_exec ($ch);
 
             $debugData['result'] = $xmlResponse;
@@ -1625,7 +1625,7 @@ XMLAuth;
             curl_setopt($ch, CURLOPT_POST, 1);
             curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlRequest);
             curl_setopt($ch, CURLOPT_TIMEOUT, 30);
-            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, (boolean)$this->getConfigFlag('mode_xml'));
+            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $this->getConfigFlag('verify_peer'));
             $xmlResponse = curl_exec($ch);
             if ($xmlResponse === false) {
                 throw new Exception(curl_error($ch));
diff --git app/code/core/Mage/Usa/etc/config.xml app/code/core/Mage/Usa/etc/config.xml
index 6273178..e9b47a1 100644
--- app/code/core/Mage/Usa/etc/config.xml
+++ app/code/core/Mage/Usa/etc/config.xml
@@ -114,6 +114,7 @@
                 <dutypaymenttype>R</dutypaymenttype>
                 <free_method>G</free_method>
                 <gateway_url>https://eCommerce.airborne.com/ApiLandingTest.asp</gateway_url>
+                <verify_peer>0</verify_peer>
                 <id backend_model="adminhtml/system_config_backend_encrypted"/>
                 <model>usa/shipping_carrier_dhl</model>
                 <password backend_model="adminhtml/system_config_backend_encrypted"/>
@@ -181,6 +182,7 @@
                 <negotiated_active>0</negotiated_active>
                 <mode_xml>1</mode_xml>
                 <type>UPS</type>
+                <verify_peer>0</verify_peer>
             </ups>
             <usps>
                 <active>0</active>
@@ -216,6 +218,7 @@
                 <doc_methods>2,5,6,7,9,B,C,D,U,K,L,G,W,I,N,O,R,S,T,X</doc_methods>
                 <free_method>G</free_method>
                 <gateway_url>https://xmlpi-ea.dhl.com/XMLShippingServlet</gateway_url>
+                <verify_peer>0</verify_peer>
                 <id backend_model="adminhtml/system_config_backend_encrypted"/>
                 <password backend_model="adminhtml/system_config_backend_encrypted"/>
                 <shipment_type>N</shipment_type>
diff --git app/code/core/Mage/Usa/etc/system.xml app/code/core/Mage/Usa/etc/system.xml
index b6e7ba5..7a4de06 100644
--- app/code/core/Mage/Usa/etc/system.xml
+++ app/code/core/Mage/Usa/etc/system.xml
@@ -130,6 +130,15 @@
                             <show_in_website>1</show_in_website>
                             <show_in_store>0</show_in_store>
                         </gateway_url>
+                        <verify_peer translate="label">
+                            <label>Enable SSL Verification</label>
+                            <frontend_type>select</frontend_type>
+                            <source_model>adminhtml/system_config_source_yesno</source_model>
+                            <sort_order>30</sort_order>
+                            <show_in_default>1</show_in_default>
+                            <show_in_website>1</show_in_website>
+                            <show_in_store>0</show_in_store>
+                        </verify_peer>
                         <handling_type translate="label">
                             <label>Calculate Handling Fee</label>
                             <frontend_type>select</frontend_type>
@@ -735,6 +744,15 @@
                             <show_in_website>1</show_in_website>
                             <show_in_store>0</show_in_store>
                         </gateway_url>
+                        <verify_peer translate="label">
+                            <label>Enable SSL Verification</label>
+                            <frontend_type>select</frontend_type>
+                            <source_model>adminhtml/system_config_source_yesno</source_model>
+                            <sort_order>45</sort_order>
+                            <show_in_default>1</show_in_default>
+                            <show_in_website>1</show_in_website>
+                            <show_in_store>0</show_in_store>
+                        </verify_peer>
                         <gateway_xml_url translate="label">
                             <label>Gateway XML URL</label>
                             <frontend_type>text</frontend_type>
@@ -1239,6 +1257,15 @@
                             <show_in_website>1</show_in_website>
                             <show_in_store>0</show_in_store>
                         </gateway_url>
+                        <verify_peer translate="label">
+                            <label>Enable SSL Verification</label>
+                            <frontend_type>select</frontend_type>
+                            <source_model>adminhtml/system_config_source_yesno</source_model>
+                            <sort_order>30</sort_order>
+                            <show_in_default>1</show_in_default>
+                            <show_in_website>1</show_in_website>
+                            <show_in_store>0</show_in_store>
+                        </verify_peer>
                         <title translate="label">
                             <label>Title</label>
                             <frontend_type>text</frontend_type>
diff --git app/code/core/Mage/Wishlist/Controller/Abstract.php app/code/core/Mage/Wishlist/Controller/Abstract.php
index ee1d08b..b67ca4e 100644
--- app/code/core/Mage/Wishlist/Controller/Abstract.php
+++ app/code/core/Mage/Wishlist/Controller/Abstract.php
@@ -73,10 +73,15 @@ abstract class Mage_Wishlist_Controller_Abstract extends Mage_Core_Controller_Fr
      */
     public function allcartAction()
     {
+        if (!$this->_validateFormKey()) {
+            $this->_forward('noRoute');
+            return;
+        }
+
         $wishlist   = $this->_getWishlist();
         if (!$wishlist) {
             $this->_forward('noRoute');
-            return ;
+            return;
         }
         $isOwner    = $wishlist->isOwner(Mage::getSingleton('customer/session')->getCustomerId());
 
@@ -89,7 +94,9 @@ abstract class Mage_Wishlist_Controller_Abstract extends Mage_Core_Controller_Fr
         $collection = $wishlist->getItemCollection()
                 ->setVisibilityFilter();
 
-        $qtys = $this->getRequest()->getParam('qty');
+        $qtysString = $this->getRequest()->getParam('qty');
+        $qtys =  array_filter(json_decode($qtysString), 'strlen');
+
         foreach ($collection as $item) {
             /** @var Mage_Wishlist_Model_Item */
             try {
diff --git app/code/core/Mage/Wishlist/Helper/Data.php app/code/core/Mage/Wishlist/Helper/Data.php
index 93a1e7f..7f69583 100644
--- app/code/core/Mage/Wishlist/Helper/Data.php
+++ app/code/core/Mage/Wishlist/Helper/Data.php
@@ -135,11 +135,9 @@ class Mage_Wishlist_Helper_Data extends Mage_Core_Helper_Abstract
         if (is_null($this->_wishlist)) {
             if (Mage::registry('shared_wishlist')) {
                 $this->_wishlist = Mage::registry('shared_wishlist');
-            }
-            elseif (Mage::registry('wishlist')) {
+            } else if (Mage::registry('wishlist')) {
                 $this->_wishlist = Mage::registry('wishlist');
-            }
-            else {
+            } else {
                 $this->_wishlist = Mage::getModel('wishlist/wishlist');
                 if ($this->getCustomer()) {
                     $this->_wishlist->loadByCustomer($this->getCustomer());
@@ -260,8 +258,7 @@ class Mage_Wishlist_Helper_Data extends Mage_Core_Helper_Abstract
         if ($product) {
             if ($product->isVisibleInSiteVisibility()) {
                 $storeId = $product->getStoreId();
-            }
-            else if ($product->hasUrlDataObject()) {
+            } else if ($product->hasUrlDataObject()) {
                 $storeId = $product->getUrlDataObject()->getStoreId();
             }
         }
@@ -277,7 +274,10 @@ class Mage_Wishlist_Helper_Data extends Mage_Core_Helper_Abstract
     public function getRemoveUrl($item)
     {
         return $this->_getUrl('wishlist/index/remove',
-            array('item' => $item->getWishlistItemId())
+            array(
+                'item' => $item->getWishlistItemId(),
+                Mage_Core_Model_Url::FORM_KEY => $this->_getSingletonModel('core/session')->getFormKey()
+            )
         );
     }
 
@@ -360,40 +360,62 @@ class Mage_Wishlist_Helper_Data extends Mage_Core_Helper_Abstract
             $productId = $item->getProductId();
         }
 
-        if ($productId) {
-            $params['product'] = $productId;
-            return $this->_getUrlStore($item)->getUrl('wishlist/index/add', $params);
+        if (!$productId) {
+            return false;
         }
-
-        return false;
+        $params['product'] = $productId;
+        $params[Mage_Core_Model_Url::FORM_KEY] = $this->_getSingletonModel('core/session')->getFormKey();
+        return $this->_getUrlStore($item)->getUrl('wishlist/index/add', $params);
     }
 
     /**
-     * Retrieve URL for adding item to shoping cart
+     * Retrieve URL for adding item to shopping cart
      *
      * @param string|Mage_Catalog_Model_Product|Mage_Wishlist_Model_Item $item
      * @return  string
      */
     public function getAddToCartUrl($item)
     {
-        $urlParamName = Mage_Core_Controller_Front_Action::PARAM_NAME_URL_ENCODED;
-        $continueUrl  = Mage::helper('core')->urlEncode(
-            Mage::getUrl('*/*/*', array(
+        $continueUrl  = $this->_getHelperInstance('core')->urlEncode(
+            $this->_getUrl('*/*/*', array(
                 '_current'      => true,
                 '_use_rewrite'  => true,
                 '_store_to_url' => true,
             ))
         );
-
-        $urlParamName = Mage_Core_Controller_Front_Action::PARAM_NAME_URL_ENCODED;
         $params = array(
             'item' => is_string($item) ? $item : $item->getWishlistItemId(),
-            $urlParamName => $continueUrl
+            Mage_Core_Controller_Front_Action::PARAM_NAME_URL_ENCODED => $continueUrl,
+            Mage_Core_Model_Url::FORM_KEY => $this->_getSingletonModel('core/session')->getFormKey()
         );
+
         return $this->_getUrlStore($item)->getUrl('wishlist/index/cart', $params);
     }
 
     /**
+     * Return helper instance
+     *
+     * @param string $helperName
+     * @return Mage_Core_Helper_Abstract
+     */
+    protected function _getHelperInstance($helperName)
+    {
+        return Mage::helper($helperName);
+    }
+
+    /**
+     * Return model instance
+     *
+     * @param string $className
+     * @param array $arguments
+     * @return Mage_Core_Model_Abstract
+     */
+    protected function _getSingletonModel($className, $arguments = array())
+    {
+        return Mage::getSingleton($className, $arguments);
+    }
+
+    /**
      * Retrieve URL for adding item to shoping cart from shared wishlist
      *
      * @param string|Mage_Catalog_Model_Product|Mage_Wishlist_Model_Item $item
@@ -407,10 +429,10 @@ class Mage_Wishlist_Helper_Data extends Mage_Core_Helper_Abstract
             '_store_to_url' => true,
         )));
 
-        $urlParamName = Mage_Core_Controller_Front_Action::PARAM_NAME_URL_ENCODED;
         $params = array(
             'item' => is_string($item) ? $item : $item->getWishlistItemId(),
-            $urlParamName => $continueUrl
+            Mage_Core_Controller_Front_Action::PARAM_NAME_URL_ENCODED => $continueUrl,
+            Mage_Core_Model_Url::FORM_KEY => $this->_getSingletonModel('core/session')->getFormKey()
         );
         return $this->_getUrlStore($item)->getUrl('wishlist/shared/cart', $params);
     }
diff --git app/code/core/Mage/Wishlist/controllers/IndexController.php app/code/core/Mage/Wishlist/controllers/IndexController.php
index c750064..46c9355 100644
--- app/code/core/Mage/Wishlist/controllers/IndexController.php
+++ app/code/core/Mage/Wishlist/controllers/IndexController.php
@@ -48,6 +48,11 @@ class Mage_Wishlist_IndexController extends Mage_Wishlist_Controller_Abstract
      */
     protected $_skipAuthentication = false;
 
+    /**
+     * Extend preDispatch
+     *
+     * @return Mage_Core_Controller_Front_Action|void
+     */
     public function preDispatch()
     {
         parent::preDispatch();
@@ -152,9 +157,24 @@ class Mage_Wishlist_IndexController extends Mage_Wishlist_Controller_Abstract
 
     /**
      * Adding new item
+     *
+     * @return Mage_Core_Controller_Varien_Action|void
      */
     public function addAction()
     {
+        if (!$this->_validateFormKey()) {
+            return $this->_redirect('*/*');
+        }
+        $this->_addItemToWishList();
+    }
+
+    /**
+     * Add the item to wish list
+     *
+     * @return Mage_Core_Controller_Varien_Action|void
+     */
+    protected function _addItemToWishList()
+    {
         $wishlist = $this->_getWishlist();
         if (!$wishlist) {
             return $this->norouteAction();
@@ -162,7 +182,7 @@ class Mage_Wishlist_IndexController extends Mage_Wishlist_Controller_Abstract
 
         $session = Mage::getSingleton('customer/session');
 
-        $productId = (int) $this->getRequest()->getParam('product');
+        $productId = (int)$this->getRequest()->getParam('product');
         if (!$productId) {
             $this->_redirect('*/');
             return;
@@ -192,9 +212,9 @@ class Mage_Wishlist_IndexController extends Mage_Wishlist_Controller_Abstract
             Mage::dispatchEvent(
                 'wishlist_add_product',
                 array(
-                    'wishlist'  => $wishlist,
-                    'product'   => $product,
-                    'item'      => $result
+                    'wishlist' => $wishlist,
+                    'product' => $product,
+                    'item' => $result
                 )
             );
 
@@ -212,10 +232,10 @@ class Mage_Wishlist_IndexController extends Mage_Wishlist_Controller_Abstract
 
             Mage::helper('wishlist')->calculate();
 
-            $message = $this->__('%1$s has been added to your wishlist. Click <a href="%2$s">here</a> to continue shopping.', $product->getName(), Mage::helper('core')->escapeUrl($referer));
+            $message = $this->__('%1$s has been added to your wishlist. Click <a href="%2$s">here</a> to continue shopping.',
+                $product->getName(), Mage::helper('core')->escapeUrl($referer));
             $session->addSuccess($message);
-        }
-        catch (Mage_Core_Exception $e) {
+        } catch (Mage_Core_Exception $e) {
             $session->addError($this->__('An error occurred while adding item to wishlist: %s', $e->getMessage()));
         }
         catch (Exception $e) {
@@ -337,7 +357,7 @@ class Mage_Wishlist_IndexController extends Mage_Wishlist_Controller_Abstract
         }
 
         $post = $this->getRequest()->getPost();
-        if($post && isset($post['description']) && is_array($post['description'])) {
+        if ($post && isset($post['description']) && is_array($post['description'])) {
             $updatedItems = 0;
 
             foreach ($post['description'] as $itemId => $description) {
@@ -393,8 +413,7 @@ class Mage_Wishlist_IndexController extends Mage_Wishlist_Controller_Abstract
                 try {
                     $wishlist->save();
                     Mage::helper('wishlist')->calculate();
-                }
-                catch (Exception $e) {
+                } catch (Exception $e) {
                     Mage::getSingleton('customer/session')->addError($this->__('Can\'t update wishlist'));
                 }
             }
@@ -412,6 +431,9 @@ class Mage_Wishlist_IndexController extends Mage_Wishlist_Controller_Abstract
      */
     public function removeAction()
     {
+        if (!$this->_validateFormKey()) {
+            return $this->_redirect('*/*');
+        }
         $id = (int) $this->getRequest()->getParam('item');
         $item = Mage::getModel('wishlist/item')->load($id);
         if (!$item->getId()) {
@@ -428,7 +450,7 @@ class Mage_Wishlist_IndexController extends Mage_Wishlist_Controller_Abstract
             Mage::getSingleton('customer/session')->addError(
                 $this->__('An error occurred while deleting the item from wishlist: %s', $e->getMessage())
             );
-        } catch(Exception $e) {
+        } catch (Exception $e) {
             Mage::getSingleton('customer/session')->addError(
                 $this->__('An error occurred while deleting the item from wishlist.')
             );
@@ -447,6 +469,9 @@ class Mage_Wishlist_IndexController extends Mage_Wishlist_Controller_Abstract
      */
     public function cartAction()
     {
+        if (!$this->_validateFormKey()) {
+            return $this->_redirect('*/*');
+        }
         $itemId = (int) $this->getRequest()->getParam('item');
 
         /* @var $item Mage_Wishlist_Model_Item */
@@ -536,7 +561,7 @@ class Mage_Wishlist_IndexController extends Mage_Wishlist_Controller_Abstract
         $cart = Mage::getSingleton('checkout/cart');
         $session = Mage::getSingleton('checkout/session');
 
-        try{
+        try {
             $item = $cart->getQuote()->getItemById($itemId);
             if (!$item) {
                 Mage::throwException(
@@ -632,7 +657,7 @@ class Mage_Wishlist_IndexController extends Mage_Wishlist_Controller_Abstract
                     ->createBlock('wishlist/share_email_rss')
                     ->setWishlistId($wishlist->getId())
                     ->toHtml();
-                $message .=$rss_url;
+                $message .= $rss_url;
             }
             $wishlistBlock = $this->getLayout()->createBlock('wishlist/share_email_items')->toHtml();
 
@@ -641,19 +666,19 @@ class Mage_Wishlist_IndexController extends Mage_Wishlist_Controller_Abstract
             $emailModel = Mage::getModel('core/email_template');
 
             $sharingCode = $wishlist->getSharingCode();
-            foreach($emails as $email) {
+            foreach ($emails as $email) {
                 $emailModel->sendTransactional(
                     Mage::getStoreConfig('wishlist/email/email_template'),
                     Mage::getStoreConfig('wishlist/email/email_identity'),
                     $email,
                     null,
                     array(
-                        'customer'      => $customer,
-                        'salable'       => $wishlist->isSalable() ? 'yes' : '',
-                        'items'         => $wishlistBlock,
-                        'addAllLink'    => Mage::getUrl('*/shared/allcart', array('code' => $sharingCode)),
-                        'viewOnSiteLink'=> Mage::getUrl('*/shared/index', array('code' => $sharingCode)),
-                        'message'       => $message
+                        'customer'       => $customer,
+                        'salable'        => $wishlist->isSalable() ? 'yes' : '',
+                        'items'          => $wishlistBlock,
+                        'addAllLink'     => Mage::getUrl('*/shared/allcart', array('code' => $sharingCode)),
+                        'viewOnSiteLink' => Mage::getUrl('*/shared/index', array('code' => $sharingCode)),
+                        'message'        => $message
                     )
                 );
             }
@@ -663,7 +688,7 @@ class Mage_Wishlist_IndexController extends Mage_Wishlist_Controller_Abstract
 
             $translate->setTranslateInline(true);
 
-            Mage::dispatchEvent('wishlist_share', array('wishlist'=>$wishlist));
+            Mage::dispatchEvent('wishlist_share', array('wishlist' => $wishlist));
             Mage::getSingleton('customer/session')->addSuccess(
                 $this->__('Your Wishlist has been shared.')
             );
@@ -719,7 +744,7 @@ class Mage_Wishlist_IndexController extends Mage_Wishlist_Controller_Abstract
                 ));
             }
 
-        } catch(Exception $e) {
+        } catch (Exception $e) {
             $this->_forward('noRoute');
         }
         exit(0);
diff --git app/code/core/Mage/XmlConnect/Block/Adminhtml/Mobile/Edit/Tab/Design.php app/code/core/Mage/XmlConnect/Block/Adminhtml/Mobile/Edit/Tab/Design.php
index b6a249a..5e6bb9a 100644
--- app/code/core/Mage/XmlConnect/Block/Adminhtml/Mobile/Edit/Tab/Design.php
+++ app/code/core/Mage/XmlConnect/Block/Adminhtml/Mobile/Edit/Tab/Design.php
@@ -95,4 +95,21 @@ class Mage_XmlConnect_Block_Adminhtml_Mobile_Edit_Tab_Design
     {
         return true;
     }
+
+    /**
+     * Create browse button template
+     *
+     * @return string
+     */
+    public function getBrowseButtonHtml()
+    {
+        return $this->getLayout()->createBlock('adminhtml/widget_button')
+            ->addData(array(
+                'before_html'   => '<div style="display:inline-block; " id="{{file_field}}_{{id}}_file-browse">',
+                'after_html'    => '</div>',
+                'id'            => '{{file_field}}_{{id}}_file-browse_button',
+                'label'         => Mage::helper('uploader')->__('...'),
+                'type'          => 'button',
+            ))->toHtml();
+    }
 }
diff --git app/design/adminhtml/default/default/layout/cms.xml app/design/adminhtml/default/default/layout/cms.xml
index 67fbc20..cc9356e 100644
--- app/design/adminhtml/default/default/layout/cms.xml
+++ app/design/adminhtml/default/default/layout/cms.xml
@@ -82,7 +82,9 @@
         </reference>
         <reference name="content">
             <block name="wysiwyg_images.content"  type="adminhtml/cms_wysiwyg_images_content" template="cms/browser/content.phtml">
-                <block name="wysiwyg_images.uploader" type="adminhtml/cms_wysiwyg_images_content_uploader" template="cms/browser/content/uploader.phtml" />
+                <block name="wysiwyg_images.uploader" type="adminhtml/cms_wysiwyg_images_content_uploader" template="media/uploader.phtml">
+                    <block name="additional_scripts" type="core/template" template="cms/browser/content/uploader.phtml"/>
+                </block>
                 <block name="wysiwyg_images.newfolder" type="adminhtml/cms_wysiwyg_images_content_newfolder" template="cms/browser/content/newfolder.phtml" />
             </block>
         </reference>
diff --git app/design/adminhtml/default/default/layout/main.xml app/design/adminhtml/default/default/layout/main.xml
index f3ce2d9..99f2096 100644
--- app/design/adminhtml/default/default/layout/main.xml
+++ app/design/adminhtml/default/default/layout/main.xml
@@ -170,9 +170,10 @@ Layout for editor element
             <action method="setCanLoadExtJs"><flag>1</flag></action>
             <action method="addJs"><script>mage/adminhtml/variables.js</script></action>
             <action method="addJs"><script>mage/adminhtml/wysiwyg/widget.js</script></action>
-            <action method="addJs"><script>lib/flex.js</script></action>
-            <action method="addJs"><script>lib/FABridge.js</script></action>
-            <action method="addJs"><script>mage/adminhtml/flexuploader.js</script></action>
+            <action method="addJs"><name>lib/uploader/flow.min.js</name></action>
+            <action method="addJs"><name>lib/uploader/fusty-flow.js</name></action>
+            <action method="addJs"><name>lib/uploader/fusty-flow-factory.js</name></action>
+            <action method="addJs"><name>mage/adminhtml/uploader/instance.js</name></action>
             <action method="addJs"><script>mage/adminhtml/browser.js</script></action>
             <action method="addJs"><script>prototype/window.js</script></action>
             <action method="addItem"><type>js_css</type><name>prototype/windows/themes/default.css</name></action>
diff --git app/design/adminhtml/default/default/layout/xmlconnect.xml app/design/adminhtml/default/default/layout/xmlconnect.xml
index 9ae04dc..18063f9 100644
--- app/design/adminhtml/default/default/layout/xmlconnect.xml
+++ app/design/adminhtml/default/default/layout/xmlconnect.xml
@@ -74,9 +74,10 @@
             <action method="setCanLoadExtJs"><flag>1</flag></action>
             <action method="addJs"><script>mage/adminhtml/variables.js</script></action>
             <action method="addJs"><script>mage/adminhtml/wysiwyg/widget.js</script></action>
-            <action method="addJs"><script>lib/flex.js</script></action>
-            <action method="addJs"><script>lib/FABridge.js</script></action>
-            <action method="addJs"><script>mage/adminhtml/flexuploader.js</script></action>
+             <action method="addJs"><name>lib/uploader/flow.min.js</name></action>
+             <action method="addJs"><name>lib/uploader/fusty-flow.js</name></action>
+             <action method="addJs"><name>lib/uploader/fusty-flow-factory.js</name></action>
+             <action method="addJs"><name>mage/adminhtml/uploader/instance.js</name></action>
             <action method="addJs"><script>mage/adminhtml/browser.js</script></action>
             <action method="addJs"><script>prototype/window.js</script></action>
             <action method="addItem"><type>js_css</type><name>prototype/windows/themes/default.css</name></action>
diff --git app/design/adminhtml/default/default/template/catalog/product/helper/gallery.phtml app/design/adminhtml/default/default/template/catalog/product/helper/gallery.phtml
index a7640c8..ffe0c33 100644
--- app/design/adminhtml/default/default/template/catalog/product/helper/gallery.phtml
+++ app/design/adminhtml/default/default/template/catalog/product/helper/gallery.phtml
@@ -108,6 +108,7 @@ $_block = $this;
     <tfoot>
         <tr>
             <td colspan="100" class="last" style="padding:8px">
+                <?php echo Mage::helper('catalog')->__('Maximum width and height dimension for upload image is %s.', Mage::getStoreConfig(Mage_Catalog_Helper_Image::XML_NODE_PRODUCT_MAX_DIMENSION)); ?>
                 <?php echo $_block->getUploaderHtml() ?>
             </td>
         </tr>
@@ -120,6 +121,6 @@ $_block = $this;
 <input type="hidden" id="<?php echo $_block->getHtmlId() ?>_save_image" name="<?php echo $_block->getElement()->getName() ?>[values]" value="<?php echo $_block->htmlEscape($_block->getImagesValuesJson()) ?>" />
 <script type="text/javascript">
 //<![CDATA[
-var <?php echo $_block->getJsObjectName(); ?> = new Product.Gallery('<?php echo $_block->getHtmlId() ?>', <?php if ($_block->getElement()->getReadonly()):?>null<?php else:?><?php echo $_block->getUploader()->getJsObjectName() ?><?php endif;?>, <?php echo $_block->getImageTypesJson() ?>);
+var <?php echo $_block->getJsObjectName(); ?> = new Product.Gallery('<?php echo $_block->getHtmlId() ?>', <?php echo $_block->getImageTypesJson() ?>);
 //]]>
 </script>
diff --git app/design/adminhtml/default/default/template/cms/browser/content/uploader.phtml app/design/adminhtml/default/default/template/cms/browser/content/uploader.phtml
index d3b5a96..7480f752 100644
--- app/design/adminhtml/default/default/template/cms/browser/content/uploader.phtml
+++ app/design/adminhtml/default/default/template/cms/browser/content/uploader.phtml
@@ -24,48 +24,8 @@
  * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
  */
 ?>
-<?php
-/**
- * Uploader template for Wysiwyg Images
- *
- * @see Mage_Adminhtml_Block_Cms_Wysiwyg_Images_Content_Uploader
- */
-?>
-<div id="<?php echo $this->getHtmlId() ?>" class="uploader">
-    <div class="buttons">
-        <div id="<?php echo $this->getHtmlId() ?>-install-flash" style="display:none">
-            <?php echo Mage::helper('media')->__('This content requires last version of Adobe Flash Player. <a href="%s">Get Flash</a>', 'http://www.adobe.com/go/getflash/') ?>
-        </div>
-    </div>
-    <div class="clear"></div>
-    <div class="no-display" id="<?php echo $this->getHtmlId() ?>-template">
-        <div id="{{id}}" class="file-row">
-        <span class="file-info">{{name}} ({{size}})</span>
-        <span class="delete-button"><?php echo $this->getDeleteButtonHtml() ?></span>
-        <span class="progress-text"></span>
-        <div class="clear"></div>
-        </div>
-    </div>
-    <div class="no-display" id="<?php echo $this->getHtmlId() ?>-template-progress">
-        {{percent}}% {{uploaded}} / {{total}}
-    </div>
-</div>
-
 <script type="text/javascript">
 //<![CDATA[
-maxUploadFileSizeInBytes = <?php echo $this->getDataMaxSizeInBytes() ?>;
-maxUploadFileSize = '<?php echo $this->getDataMaxSize() ?>';
-
-<?php echo $this->getJsObjectName() ?> = new Flex.Uploader('<?php echo $this->getHtmlId() ?>', '<?php echo $this->getSkinUrl('media/uploader.swf') ?>', <?php echo $this->getConfigJson() ?>);
-<?php echo $this->getJsObjectName() ?>.onFilesComplete = function(completedFiles){
-    completedFiles.each(function(file){
-        <?php echo $this->getJsObjectName() ?>.removeFile(file.id);
-    });
-    MediabrowserInstance.handleUploadComplete();
-}
-// hide flash buttons
-if ($('<?php echo $this->getHtmlId() ?>-flash') != undefined) {
-    $('<?php echo $this->getHtmlId() ?>-flash').setStyle({float:'left'});
-}
+    document.on('uploader:success', MediabrowserInstance.handleUploadComplete.bind(MediabrowserInstance));
 //]]>
 </script>
diff --git app/design/adminhtml/default/default/template/downloadable/product/edit/downloadable.phtml app/design/adminhtml/default/default/template/downloadable/product/edit/downloadable.phtml
index e2f150b..20c3689 100644
--- app/design/adminhtml/default/default/template/downloadable/product/edit/downloadable.phtml
+++ app/design/adminhtml/default/default/template/downloadable/product/edit/downloadable.phtml
@@ -34,19 +34,16 @@
 //<![CDATA[>
 
 var uploaderTemplate = '<div class="no-display" id="[[idName]]-template">' +
-                            '<div id="{{id}}" class="file-row file-row-narrow">' +
+                            '<div id="{{id}}-container" class="file-row file-row-narrow">' +
                                 '<span class="file-info">' +
                                     '<span class="file-info-name">{{name}}</span>' +
                                     ' ' +
-                                    '<span class="file-info-size">({{size}})</span>' +
+                                    '<span class="file-info-size">{{size}}</span>' +
                                 '</span>' +
                                 '<span class="progress-text"></span>' +
                                 '<div class="clear"></div>' +
                             '</div>' +
-                        '</div>' +
-                            '<div class="no-display" id="[[idName]]-template-progress">' +
-                            '{{percent}}% {{uploaded}} / {{total}}' +
-                            '</div>';
+                        '</div>';
 
 var fileListTemplate = '<span class="file-info">' +
                             '<span class="file-info-name">{{name}}</span>' +
@@ -88,7 +85,7 @@ var Downloadable = {
     massUploadByType : function(type){
         try {
             this.uploaderObj.get(type).each(function(item){
-                container = item.value.container.up('tr');
+                var container = item.value.elements.container.up('tr');
                 if (container.visible() && !container.hasClassName('no-display')) {
                     item.value.upload();
                 } else {
@@ -141,10 +138,11 @@ Downloadable.FileUploader.prototype = {
                ? this.fileValue.toJSON()
                : Object.toJSON(this.fileValue);
         }
+        var uploaderConfig = (Object.isString(this.config) && this.config.evalJSON()) || this.config;
         Downloadable.setUploaderObj(
             this.type,
             this.key,
-            new Flex.Uploader(this.idName, '<?php echo $this->getSkinUrl('media/uploaderSingle.swf') ?>', this.config)
+            new Uploader(uploaderConfig)
         );
         if (varienGlobalEvents) {
             varienGlobalEvents.attachEventHandler('tabChangeBefore', Downloadable.getUploaderObj(type, key).onContainerHideBefore);
@@ -167,16 +165,48 @@ Downloadable.FileList.prototype = {
         this.containerId  = containerId,
         this.container = $(this.containerId);
         this.uploader = uploader;
-        this.uploader.onFilesComplete = this.handleUploadComplete.bind(this);
+        document.on('uploader:fileSuccess', function(event) {
+            var memo = event.memo;
+            if(this._checkCurrentContainer(memo.containerId)) {
+                this.handleUploadComplete([{response: memo.response}]);
+                this.handleButtonsSwap();
+            }
+        }.bind(this));
+        document.on('uploader:fileError', function(event) {
+            var memo = event.memo;
+            if(this._checkCurrentContainer(memo.containerId)) {
+                this.handleButtonsSwap();
+            }
+        }.bind(this));
+        document.on('upload:simulateDelete', this.handleFileRemoveAll.bind(this));
+        document.on('uploader:simulateNewUpload', this.handleFileNew.bind(this));
         this.file = this.getElement('save').value.evalJSON();
         this.listTemplate = new Template(this.fileListTemplate, this.templatePattern);
         this.updateFiles();
         this.uploader.onFileRemoveAll = this.handleFileRemoveAll.bind(this);
         this.uploader.onFileSelect = this.handleFileSelect.bind(this);
     },
-    handleFileRemoveAll: function(fileId) {
-        $(this.containerId+'-new').hide();
-        $(this.containerId+'-old').show();
+
+    _checkCurrentContainer: function (child) {
+        return $(this.containerId).down('#' + child);
+    },
+
+    handleFileRemoveAll: function(e) {
+        if(e.memo && this._checkCurrentContainer(e.memo.containerId)) {
+            $(this.containerId+'-new').hide();
+            $(this.containerId+'-old').show();
+            this.handleButtonsSwap();
+        }
+    },
+    handleFileNew: function (e) {
+        if(e.memo && this._checkCurrentContainer(e.memo.containerId)) {
+            $(this.containerId + '-new').show();
+            $(this.containerId + '-old').hide();
+            this.handleButtonsSwap();
+        }
+    },
+    handleButtonsSwap: function () {
+        $$(['#' + this.containerId+'-browse', '#'+this.containerId+'-delete']).invoke('toggle');
     },
     handleFileSelect: function() {
         $(this.containerId+'_type').checked = true;
@@ -204,7 +234,6 @@ Downloadable.FileList.prototype = {
            newFile.size = response.size;
            newFile.status = 'new';
            this.file[0] = newFile;
-           this.uploader.removeFile(item.id);
         }.bind(this));
         this.updateFiles();
     },
diff --git app/design/adminhtml/default/default/template/downloadable/product/edit/downloadable/links.phtml app/design/adminhtml/default/default/template/downloadable/product/edit/downloadable/links.phtml
index ab3238e..597fb7d 100644
--- app/design/adminhtml/default/default/template/downloadable/product/edit/downloadable/links.phtml
+++ app/design/adminhtml/default/default/template/downloadable/product/edit/downloadable/links.phtml
@@ -28,6 +28,7 @@
 
 /**
  * @see Mage_Downloadable_Block_Adminhtml_Catalog_Product_Edit_Tab_Downloadable_Links
+ * @var $this Mage_Downloadable_Block_Adminhtml_Catalog_Product_Edit_Tab_Downloadable_Links
  */
 ?>
 <?php $_product = $this->getProduct()?>
@@ -137,17 +138,14 @@ var linkTemplate = '<tr>'+
     '</td>'+
     '<td>'+
         '<div class="files">'+
-            '<div class="row">'+
-                '<label for="downloadable_link_{{id}}_sample_file_type"><input type="radio" class="radio" id="downloadable_link_{{id}}_sample_file_type" name="downloadable[link][{{id}}][sample][type]" value="file"{{sample_file_checked}} /> File:</label>'+
+            '<div class="row a-right">'+
+                '<label for="downloadable_link_{{id}}_sample_file_type" class="a-left"><input type="radio" class="radio" id="downloadable_link_{{id}}_sample_file_type" name="downloadable[link][{{id}}][sample][type]" value="file"{{sample_file_checked}} /> File:</label>'+
                 '<input type="hidden" id="downloadable_link_{{id}}_sample_file_save" name="downloadable[link][{{id}}][sample][file]" value="{{sample_file_save}}" />'+
-                '<div id="downloadable_link_{{id}}_sample_file" class="uploader">'+
+                '<?php echo $this->getBrowseButtonHtml('sample_'); ?>'+
+                '<?php echo $this->getDeleteButtonHtml('sample_'); ?>'+
+                '<div id="downloadable_link_{{id}}_sample_file" class="uploader a-left">'+
                     '<div id="downloadable_link_{{id}}_sample_file-old" class="file-row-info"></div>'+
                     '<div id="downloadable_link_{{id}}_sample_file-new" class="file-row-info"></div>'+
-                    '<div class="buttons">'+
-                        '<div id="downloadable_link_{{id}}_sample_file-install-flash" style="display:none">'+
-                            '<?php echo $this->jsQuoteEscape(Mage::helper('media')->__('This content requires last version of Adobe Flash Player. <a href="%s">Get Flash</a>', 'http://www.adobe.com/go/getflash/')) ?>'+
-                        '</div>'+
-                    '</div>'+
                     '<div class="clear"></div>'+
                 '</div>'+
             '</div>'+
@@ -161,17 +159,14 @@ var linkTemplate = '<tr>'+
     '</td>'+
     '<td>'+
         '<div class="files">'+
-            '<div class="row">'+
-                '<label for="downloadable_link_{{id}}_file_type"><input type="radio" class="radio validate-one-required-by-name" id="downloadable_link_{{id}}_file_type" name="downloadable[link][{{id}}][type]" value="file"{{file_checked}} /> File:</label>'+
+            '<div class="row a-right">'+
+                '<label for="downloadable_link_{{id}}_file_type" class="a-left"><input type="radio" class="radio validate-one-required-by-name" id="downloadable_link_{{id}}_file_type" name="downloadable[link][{{id}}][type]" value="file"{{file_checked}} /> File:</label>'+
             '<input type="hidden" class="validate-downloadable-file" id="downloadable_link_{{id}}_file_save" name="downloadable[link][{{id}}][file]" value="{{file_save}}" />'+
-                '<div id="downloadable_link_{{id}}_file" class="uploader">'+
+                '<?php echo $this->getBrowseButtonHtml(); ?>'+
+                '<?php echo $this->getDeleteButtonHtml(); ?>'+
+                '<div id="downloadable_link_{{id}}_file" class="uploader a-left">'+
                     '<div id="downloadable_link_{{id}}_file-old" class="file-row-info"></div>'+
                     '<div id="downloadable_link_{{id}}_file-new" class="file-row-info new-file"></div>'+
-                    '<div class="buttons">'+
-                        '<div id="downloadable_link_{{id}}_file-install-flash" style="display:none">'+
-                            '<?php echo $this->jsQuoteEscape(Mage::helper('media')->__('This content requires last version of Adobe Flash Player. <a href="%s">Get Flash</a>', 'http://www.adobe.com/go/getflash/')) ?>'+
-                        '</div>'+
-                    '</div>'+
                     '<div class="clear"></div>'+
                 '</div>'+
             '</div>'+
@@ -282,6 +277,9 @@ var linkItems = {
         if (!data.sample_file_save) {
             data.sample_file_save = [];
         }
+        var UploaderConfigLinkSamples = <?php echo $this->getConfigJson('link_samples') ?>.replace(
+            new RegExp('<?php echo $this->getId(); ?>', 'g'),
+            'downloadable_link_'+data.id+'_sample_file');
 
         // link sample file
         new Downloadable.FileUploader(
@@ -291,8 +289,12 @@ var linkItems = {
             'downloadable[link]['+data.id+'][sample]',
             data.sample_file_save,
             'downloadable_link_'+data.id+'_sample_file',
-            <?php echo $this->getConfigJson('link_samples') ?>
+            UploaderConfigLinkSamples
         );
+
+        var UploaderConfigLink = <?php echo $this->getConfigJson() ?>.replace(
+            new RegExp('<?php echo $this->getId(); ?>', 'g'),
+            'downloadable_link_'+data.id+'_file');
         // link file
         new Downloadable.FileUploader(
             'links',
@@ -301,7 +303,7 @@ var linkItems = {
             'downloadable[link]['+data.id+']',
             data.file_save,
             'downloadable_link_'+data.id+'_file',
-            <?php echo $this->getConfigJson() ?>
+            UploaderConfigLink
         );
 
         linkFile = $('downloadable_link_'+data.id+'_file_type');
diff --git app/design/adminhtml/default/default/template/downloadable/product/edit/downloadable/samples.phtml app/design/adminhtml/default/default/template/downloadable/product/edit/downloadable/samples.phtml
index b9cdcfa..0b6b890 100644
--- app/design/adminhtml/default/default/template/downloadable/product/edit/downloadable/samples.phtml
+++ app/design/adminhtml/default/default/template/downloadable/product/edit/downloadable/samples.phtml
@@ -27,6 +27,7 @@
 <?php
 /**
  * @see Mage_Downloadable_Block_Adminhtml_Catalog_Product_Edit_Tab_Downloadable_Samples
+ * @var $this Mage_Downloadable_Block_Adminhtml_Catalog_Product_Edit_Tab_Downloadable_Samples
  */
 ?>
 
@@ -89,17 +90,14 @@ var sampleTemplate = '<tr>'+
                         '</td>'+
                         '<td>'+
                             '<div class="files-wide">'+
-                                '<div class="row">'+
-                                    '<label for="downloadable_sample_{{id}}_file_type"><input type="radio" class="radio validate-one-required-by-name" id="downloadable_sample_{{id}}_file_type" name="downloadable[sample][{{id}}][type]" value="file"{{file_checked}} /> File:</label>'+
+                                '<div class="row a-right">'+
+                                    '<label for="downloadable_sample_{{id}}_file_type" class="a-left"><input type="radio" class="radio validate-one-required-by-name" id="downloadable_sample_{{id}}_file_type" name="downloadable[sample][{{id}}][type]" value="file"{{file_checked}} /> File:</label>'+
                                     '<input type="hidden" class="validate-downloadable-file" id="downloadable_sample_{{id}}_file_save" name="downloadable[sample][{{id}}][file]" value="{{file_save}}" />'+
-                                    '<div id="downloadable_sample_{{id}}_file" class="uploader">'+
+                                    '<?php echo $this->getBrowseButtonHtml(); ?>'+
+                                    '<?php echo $this->getDeleteButtonHtml(); ?>'+
+                                    '<div id="downloadable_sample_{{id}}_file" class="uploader a-left">' +
                                         '<div id="downloadable_sample_{{id}}_file-old" class="file-row-info"></div>'+
                                         '<div id="downloadable_sample_{{id}}_file-new" class="file-row-info new-file"></div>'+
-                                        '<div class="buttons">'+
-                                            '<div id="downloadable_sample_{{id}}_file-install-flash" style="display:none">'+
-                                                '<?php echo $this->jsQuoteEscape(Mage::helper('media')->__('This content requires last version of Adobe Flash Player. <a href="%s">Get Flash</a>', 'http://www.adobe.com/go/getflash/')) ?>'+
-                                            '</div>'+
-                                        '</div>'+
                                         '<div class="clear"></div>'+
                                     '</div>'+
                                 '</div>'+
@@ -161,6 +159,10 @@ var sampleItems = {
 
         sampleUrl = $('downloadable_sample_'+data.id+'_url_type');
 
+        var UploaderConfig = <?php echo $this->getConfigJson() ?>.replace(
+            new RegExp('<?php echo $this->getId(); ?>', 'g'),
+            'downloadable_sample_'+data.id+'_file');
+
         if (!data.file_save) {
             data.file_save = [];
         }
@@ -171,7 +173,7 @@ var sampleItems = {
             'downloadable[sample]['+data.id+']',
             data.file_save,
             'downloadable_sample_'+data.id+'_file',
-            <?php echo $this->getConfigJson() ?>
+            UploaderConfig
         );
         sampleUrl.advaiceContainer = 'downloadable_sample_'+data.id+'_container';
         sampleFile = $('downloadable_sample_'+data.id+'_file_type');
diff --git app/design/adminhtml/default/default/template/media/uploader.phtml app/design/adminhtml/default/default/template/media/uploader.phtml
index 923a220..8dba02f 100644
--- app/design/adminhtml/default/default/template/media/uploader.phtml
+++ app/design/adminhtml/default/default/template/media/uploader.phtml
@@ -26,48 +26,30 @@
 ?>
 <?php
 /**
- * @see Mage_Adminhtml_Block_Media_Uploader
+ * @var $this Mage_Uploader_Block_Multiple|Mage_Uploader_Block_Single
  */
 ?>
-
-<?php echo $this->helper('adminhtml/js')->includeScript('lib/flex.js') ?>
-<?php echo $this->helper('adminhtml/js')->includeScript('mage/adminhtml/flexuploader.js') ?>
-<?php echo $this->helper('adminhtml/js')->includeScript('lib/FABridge.js') ?>
-
 <div id="<?php echo $this->getHtmlId() ?>" class="uploader">
-    <div class="buttons">
-        <?php /* buttons included in flex object */ ?>
-        <?php  /*echo $this->getBrowseButtonHtml()*/  ?>
-        <?php  /*echo $this->getUploadButtonHtml()*/  ?>
-        <div id="<?php echo $this->getHtmlId() ?>-install-flash" style="display:none">
-            <?php echo Mage::helper('media')->__('This content requires last version of Adobe Flash Player. <a href="%s">Get Flash</a>', 'http://www.adobe.com/go/getflash/') ?>
-        </div>
+    <div class="buttons a-right">
+        <?php echo $this->getBrowseButtonHtml(); ?>
+        <?php echo $this->getUploadButtonHtml(); ?>
     </div>
-    <div class="clear"></div>
-    <div class="no-display" id="<?php echo $this->getHtmlId() ?>-template">
-        <div id="{{id}}" class="file-row">
-        <span class="file-info">{{name}} ({{size}})</span>
+</div>
+<div class="no-display" id="<?php echo $this->getElementId('template') ?>">
+    <div id="{{id}}-container" class="file-row">
+        <span class="file-info">{{name}} {{size}}</span>
         <span class="delete-button"><?php echo $this->getDeleteButtonHtml() ?></span>
         <span class="progress-text"></span>
         <div class="clear"></div>
-        </div>
-    </div>
-    <div class="no-display" id="<?php echo $this->getHtmlId() ?>-template-progress">
-        {{percent}}% {{uploaded}} / {{total}}
     </div>
 </div>
-
 <script type="text/javascript">
-//<![CDATA[
-
-var maxUploadFileSizeInBytes = <?php echo $this->getDataMaxSizeInBytes() ?>;
-var maxUploadFileSize = '<?php echo $this->getDataMaxSize() ?>';
-
-<?php echo $this->getJsObjectName() ?> = new Flex.Uploader('<?php echo $this->getHtmlId() ?>', '<?php echo $this->getUploaderUrl('media/uploader.swf') ?>', <?php echo $this->getConfigJson() ?>);
-
-if (varienGlobalEvents) {
-    varienGlobalEvents.attachEventHandler('tabChangeBefore', <?php echo $this->getJsObjectName() ?>.onContainerHideBefore);
-}
+    (function() {
+        var uploader = new Uploader(<?php echo $this->getJsonConfig(); ?>);
 
-//]]>
+        if (varienGlobalEvents) {
+            varienGlobalEvents.attachEventHandler('tabChangeBefore', uploader.onContainerHideBefore);
+        }
+    })();
 </script>
+<?php echo $this->getChildHtml('additional_scripts'); ?>
diff --git app/design/frontend/base/default/template/catalog/product/view.phtml app/design/frontend/base/default/template/catalog/product/view.phtml
index 5d9212f..fc34321 100644
--- app/design/frontend/base/default/template/catalog/product/view.phtml
+++ app/design/frontend/base/default/template/catalog/product/view.phtml
@@ -40,6 +40,7 @@
 <div class="product-view">
     <div class="product-essential">
     <form action="<?php echo $this->getSubmitUrl($_product) ?>" method="post" id="product_addtocart_form"<?php if($_product->getOptions()): ?> enctype="multipart/form-data"<?php endif; ?>>
+        <?php echo $this->getBlockHtml('formkey') ?>
         <div class="no-display">
             <input type="hidden" name="product" value="<?php echo $_product->getId() ?>" />
             <input type="hidden" name="related_product" id="related-products-field" value="" />
diff --git app/design/frontend/base/default/template/checkout/cart.phtml app/design/frontend/base/default/template/checkout/cart.phtml
index 7baafce..8cba100 100644
--- app/design/frontend/base/default/template/checkout/cart.phtml
+++ app/design/frontend/base/default/template/checkout/cart.phtml
@@ -47,6 +47,7 @@
     <?php echo $this->getMessagesBlock()->getGroupedHtml() ?>
     <?php echo $this->getChildHtml('form_before') ?>
     <form action="<?php echo $this->getUrl('checkout/cart/updatePost') ?>" method="post">
+        <?php echo $this->getBlockHtml('formkey'); ?>
         <fieldset>
             <table id="shopping-cart-table" class="data-table cart-table">
                 <col width="1" />
diff --git app/design/frontend/base/default/template/checkout/onepage/review/info.phtml app/design/frontend/base/default/template/checkout/onepage/review/info.phtml
index d98d00f..6336137 100644
--- app/design/frontend/base/default/template/checkout/onepage/review/info.phtml
+++ app/design/frontend/base/default/template/checkout/onepage/review/info.phtml
@@ -78,7 +78,7 @@
     </div>
     <script type="text/javascript">
     //<![CDATA[
-        review = new Review('<?php echo $this->getUrl('checkout/onepage/saveOrder') ?>', '<?php echo $this->getUrl('checkout/onepage/success') ?>', $('checkout-agreements'));
+        review = new Review('<?php echo $this->getUrl('checkout/onepage/saveOrder', array('form_key' => Mage::getSingleton('core/session')->getFormKey())) ?>', '<?php echo $this->getUrl('checkout/onepage/success') ?>', $('checkout-agreements'));
     //]]>
     </script>
 </div>
diff --git app/design/frontend/base/default/template/customer/form/login.phtml app/design/frontend/base/default/template/customer/form/login.phtml
index dfb8245..fd3ceb9 100644
--- app/design/frontend/base/default/template/customer/form/login.phtml
+++ app/design/frontend/base/default/template/customer/form/login.phtml
@@ -39,6 +39,7 @@
     <?php /* Extensions placeholder */ ?>
     <?php echo $this->getChildHtml('customer.form.login.extra')?>
     <form action="<?php echo $this->getPostActionUrl() ?>" method="post" id="login-form">
+        <?php echo $this->getBlockHtml('formkey'); ?>
         <div class="col2-set">
             <div class="col-1 new-users">
                 <div class="content">
diff --git app/design/frontend/base/default/template/persistent/customer/form/login.phtml app/design/frontend/base/default/template/persistent/customer/form/login.phtml
index 622620f..b178d4b 100644
--- app/design/frontend/base/default/template/persistent/customer/form/login.phtml
+++ app/design/frontend/base/default/template/persistent/customer/form/login.phtml
@@ -38,6 +38,7 @@
     </div>
     <?php echo $this->getMessagesBlock()->getGroupedHtml() ?>
     <form action="<?php echo $this->getPostActionUrl() ?>" method="post" id="login-form">
+        <?php echo $this->getBlockHtml('formkey'); ?>
         <div class="col2-set">
             <div class="col-1 new-users">
                 <div class="content">
diff --git app/design/frontend/base/default/template/review/form.phtml app/design/frontend/base/default/template/review/form.phtml
index 6b9024a..82f966c 100644
--- app/design/frontend/base/default/template/review/form.phtml
+++ app/design/frontend/base/default/template/review/form.phtml
@@ -28,6 +28,7 @@
     <h2><?php echo $this->__('Write Your Own Review') ?></h2>
     <?php if ($this->getAllowWriteReviewFlag()): ?>
     <form action="<?php echo $this->getAction() ?>" method="post" id="review-form">
+        <?php echo $this->getBlockHtml('formkey'); ?>
         <fieldset>
             <?php echo $this->getChildHtml('form_fields_before')?>
             <h3><?php echo $this->__("You're reviewing:"); ?> <span><?php echo $this->htmlEscape($this->getProductInfo()->getName()) ?></span></h3>
diff --git app/design/frontend/base/default/template/sales/reorder/sidebar.phtml app/design/frontend/base/default/template/sales/reorder/sidebar.phtml
index 8d3490f..65c0329 100644
--- app/design/frontend/base/default/template/sales/reorder/sidebar.phtml
+++ app/design/frontend/base/default/template/sales/reorder/sidebar.phtml
@@ -38,6 +38,7 @@
         <strong><span><?php echo $this->__('My Orders') ?></span></strong>
     </div>
     <form method="post" action="<?php echo $this->getFormActionUrl() ?>" id="reorder-validate-detail">
+        <?php echo $this->getBlockHtml('formkey'); ?>
         <div class="block-content">
             <p class="block-subtitle"><?php echo $this->__('Last Ordered Items') ?></p>
             <ol id="cart-sidebar-reorder">
diff --git app/design/frontend/base/default/template/tag/customer/view.phtml app/design/frontend/base/default/template/tag/customer/view.phtml
index 23d7809..e8d2a3a 100644
--- app/design/frontend/base/default/template/tag/customer/view.phtml
+++ app/design/frontend/base/default/template/tag/customer/view.phtml
@@ -52,7 +52,9 @@
             </td>
             <td>
                 <?php if($_product->isSaleable()): ?>
-                    <button type="button" title="<?php echo $this->__('Add to Cart') ?>" class="button btn-cart" onclick="setLocation('<?php echo $this->getUrl('checkout/cart/add',array('product'=>$_product->getId())) ?>')"><span><span><?php echo $this->__('Add to Cart') ?></span></span></button>
+                    <?php $params[Mage_Core_Model_Url::FORM_KEY] = Mage::getSingleton('core/session')->getFormKey() ?>
+                    <?php $params['product'] = $_product->getId(); ?>
+                    <button type="button" title="<?php echo $this->__('Add to Cart') ?>" class="button btn-cart" onclick="setLocation('<?php echo $this->getUrl('checkout/cart/add', $params) ?>')"><span><span><?php echo $this->__('Add to Cart') ?></span></span></button>
                 <?php endif; ?>
                 <?php if ($this->helper('wishlist')->isAllow()) : ?>
                 <ul class="add-to-links">
diff --git app/design/frontend/base/default/template/wishlist/view.phtml app/design/frontend/base/default/template/wishlist/view.phtml
index dfc4ecf..6a0dd23 100644
--- app/design/frontend/base/default/template/wishlist/view.phtml
+++ app/design/frontend/base/default/template/wishlist/view.phtml
@@ -52,20 +52,36 @@
             </fieldset>
         </form>
 
+        <form id="wishlist-allcart-form" action="<?php echo $this->getUrl('*/*/allcart') ?>" method="post">
+            <?php echo $this->getBlockHtml('formkey') ?>
+            <div class="no-display">
+                <input type="hidden" name="wishlist_id" id="wishlist_id" value="<?php echo $this->getWishlistInstance()->getId() ?>" />
+                <input type="hidden" name="qty" id="qty" value="" />
+            </div>
+        </form>
+
         <script type="text/javascript">
         //<![CDATA[
-        var wishlistForm = new Validation($('wishlist-view-form'));
-        function addAllWItemsToCart() {
-            var url = '<?php echo $this->getUrl('*/*/allcart', array('wishlist_id' => $this->getWishlistInstance()->getId())) ?>';
-            var separator = (url.indexOf('?') >= 0) ? '&' : '?';
-            $$('#wishlist-view-form .qty').each(
-                function (input, index) {
-                    url += separator + input.name + '=' + encodeURIComponent(input.value);
-                    separator = '&';
-                }
-            );
-            setLocation(url);
-        }
+            var wishlistForm = new Validation($('wishlist-view-form'));
+            var wishlistAllCartForm = new Validation($('wishlist-allcart-form'));
+
+            function calculateQty() {
+                var itemQtys = new Array();
+                $$('#wishlist-view-form .qty').each(
+                    function (input, index) {
+                        var idxStr = input.name;
+                        var idx = idxStr.replace( /[^\d.]/g, '' );
+                        itemQtys[idx] = input.value;
+                    }
+                );
+
+                $$('#qty')[0].value = JSON.stringify(itemQtys);
+            }
+
+            function addAllWItemsToCart() {
+                calculateQty();
+                wishlistAllCartForm.form.submit();
+            }
         //]]>
         </script>
     </div>
diff --git app/design/frontend/default/iphone/template/checkout/cart.phtml app/design/frontend/default/iphone/template/checkout/cart.phtml
index 567c88f..2bcf244 100644
--- app/design/frontend/default/iphone/template/checkout/cart.phtml
+++ app/design/frontend/default/iphone/template/checkout/cart.phtml
@@ -45,6 +45,7 @@
         </ul>
     <?php endif; ?>
     <form action="<?php echo $this->getUrl('checkout/cart/updatePost') ?>" method="post">
+        <?php echo $this->getBlockHtml('formkey') ?>
         <fieldset>
             <table id="shopping-cart-table" class="data-table cart-table">
                 <tfoot>
diff --git app/design/frontend/default/iphone/template/customer/form/login.phtml app/design/frontend/default/iphone/template/customer/form/login.phtml
index a6d2ac1..7d61c3b 100644
--- app/design/frontend/default/iphone/template/customer/form/login.phtml
+++ app/design/frontend/default/iphone/template/customer/form/login.phtml
@@ -39,6 +39,7 @@
     <?php /* Extensions placeholder */ ?>
     <?php echo $this->getChildHtml('customer.form.login.extra')?>
     <form action="<?php echo $this->getPostActionUrl() ?>" method="post" id="login-form">
+        <?php echo $this->getBlockHtml('formkey') ?>
         <div class="col2-set">
             <div class="col-1 registered-users">
                 <div class="content">
diff --git app/design/frontend/default/iphone/template/persistent/customer/form/login.phtml app/design/frontend/default/iphone/template/persistent/customer/form/login.phtml
index 5decee9..871f998 100644
--- app/design/frontend/default/iphone/template/persistent/customer/form/login.phtml
+++ app/design/frontend/default/iphone/template/persistent/customer/form/login.phtml
@@ -38,6 +38,7 @@
     </div>
     <?php echo $this->getMessagesBlock()->getGroupedHtml() ?>
     <form action="<?php echo $this->getPostActionUrl() ?>" method="post" id="login-form">
+        <?php echo $this->getBlockHtml('formkey') ?>
         <div class="col2-set">
             <div class="col-1 registered-users">
                 <div class="content">
diff --git app/design/frontend/default/iphone/template/wishlist/view.phtml app/design/frontend/default/iphone/template/wishlist/view.phtml
index f2a27fd..550e860 100644
--- app/design/frontend/default/iphone/template/wishlist/view.phtml
+++ app/design/frontend/default/iphone/template/wishlist/view.phtml
@@ -48,20 +48,36 @@
             </fieldset>
         </form>
 
+        <form id="wishlist-allcart-form" action="<?php echo $this->getUrl('*/*/allcart') ?>" method="post">
+            <?php echo $this->getBlockHtml('formkey') ?>
+            <div class="no-display">
+                <input type="hidden" name="wishlist_id" id="wishlist_id" value="<?php echo $this->getWishlistInstance()->getId() ?>" />
+                <input type="hidden" name="qty" id="qty" value="" />
+            </div>
+        </form>
+
         <script type="text/javascript">
         //<![CDATA[
         var wishlistForm = new Validation($('wishlist-view-form'));
-        function addAllWItemsToCart() {
-            var url = '<?php echo $this->getUrl('*/*/allcart', array('wishlist_id' => $this->getWishlistInstance()->getId())) ?>';
-            var separator = (url.indexOf('?') >= 0) ? '&' : '?';
+            var wishlistAllCartForm = new Validation($('wishlist-allcart-form'));
+
+            function calculateQty() {
+                var itemQtys = new Array();
             $$('#wishlist-view-form .qty').each(
                 function (input, index) {
-                    url += separator + input.name + '=' + encodeURIComponent(input.value);
-                    separator = '&';
+                        var idxStr = input.name;
+                        var idx = idxStr.replace( /[^\d.]/g, '' );
+                        itemQtys[idx] = input.value;
                 }
             );
-            setLocation(url);
+
+                $$('#qty')[0].value = JSON.stringify(itemQtys);
         }
+
+            function addAllWItemsToCart() {
+                calculateQty();
+                wishlistAllCartForm.form.submit();
+            }
         //]]>
         </script>
     </div>
diff --git app/etc/modules/Mage_All.xml app/etc/modules/Mage_All.xml
index 34d0b98..7a47a0c 100644
--- app/etc/modules/Mage_All.xml
+++ app/etc/modules/Mage_All.xml
@@ -275,7 +275,7 @@
             <active>true</active>
             <codePool>core</codePool>
             <depends>
-                <Mage_Core/>
+                <Mage_Uploader/>
             </depends>
         </Mage_Cms>
         <Mage_Reports>
@@ -397,5 +397,12 @@
                 <Mage_Core/>
             </depends>
         </Mage_Index>
+        <Mage_Uploader>
+            <active>true</active>
+            <codePool>core</codePool>
+            <depends>
+                <Mage_Core/>
+            </depends>
+        </Mage_Uploader>
     </modules>
 </config>
diff --git app/locale/en_US/Mage_Media.csv app/locale/en_US/Mage_Media.csv
index 110331b..504a44a 100644
--- app/locale/en_US/Mage_Media.csv
+++ app/locale/en_US/Mage_Media.csv
@@ -1,3 +1,2 @@
 "An error occurred while creating the image.","An error occurred while creating the image."
 "The image does not exist or is invalid.","The image does not exist or is invalid."
-"This content requires last version of Adobe Flash Player. <a href=""%s"">Get Flash</a>","This content requires last version of Adobe Flash Player. <a href=""%s"">Get Flash</a>"
diff --git app/locale/en_US/Mage_Uploader.csv app/locale/en_US/Mage_Uploader.csv
new file mode 100644
index 0000000..c246b24
--- /dev/null
+++ app/locale/en_US/Mage_Uploader.csv
@@ -0,0 +1,8 @@
+"Browse Files...","Browse Files..."
+"Upload Files","Upload Files"
+"Remove", "Remove"
+"There are files that were selected but not uploaded yet. After switching to another tab your selections will be lost. Do you wish to continue ?", "There are files that were selected but not uploaded yet. After switching to another tab your selections will be lost. Do you wish to continue ?"
+"Maximum allowed file size for upload is","Maximum allowed file size for upload is"
+"Please check your server PHP settings.","Please check your server PHP settings."
+"Uploading...","Uploading..."
+"Complete","Complete"
\ No newline at end of file
diff --git downloader/Maged/Controller.php downloader/Maged/Controller.php
index a5a6aa8..88839f5 100755
--- downloader/Maged/Controller.php
+++ downloader/Maged/Controller.php
@@ -367,6 +367,11 @@ final class Maged_Controller
      */
     public function connectInstallPackageUploadAction()
     {
+        if (!$this->_validateFormKey()) {
+            echo "No file was uploaded";
+            return;
+        }
+
         if (!$_FILES) {
             echo "No file was uploaded";
             return;
@@ -1090,4 +1095,27 @@ final class Maged_Controller
 
         return $messagesMap[$type];
     }
+
+    /**
+     * Validate Form Key
+     *
+     * @return bool
+     */
+    protected function _validateFormKey()
+    {
+        if (!($formKey = $_REQUEST['form_key']) || $formKey != $this->session()->getFormKey()) {
+            return false;
+        }
+        return true;
+    }
+
+    /**
+     * Retrieve Session Form Key
+     *
+     * @return string
+     */
+    public function getFormKey()
+    {
+        return $this->session()->getFormKey();
+    }
 }
diff --git downloader/Maged/Model/Session.php downloader/Maged/Model/Session.php
index 303efbb..ce9b584 100644
--- downloader/Maged/Model/Session.php
+++ downloader/Maged/Model/Session.php
@@ -221,4 +221,17 @@ class Maged_Model_Session extends Maged_Model
         }
         return Mage::getSingleton('adminhtml/url')->getUrl('adminhtml');
     }
+
+    /**
+     * Retrieve Session Form Key
+     *
+     * @return string A 16 bit unique key for forms
+     */
+    public function getFormKey()
+    {
+        if (!$this->get('_form_key')) {
+            $this->set('_form_key', Mage::helper('core')->getRandomString(16));
+        }
+        return $this->get('_form_key');
+    }
 }
diff --git downloader/Maged/View.php downloader/Maged/View.php
index 548de0f..a80357b 100755
--- downloader/Maged/View.php
+++ downloader/Maged/View.php
@@ -154,6 +154,16 @@ class Maged_View
     }
 
     /**
+     * Retrieve Session Form Key
+     *
+     * @return string
+     */
+    public function getFormKey()
+    {
+        return $this->controller()->getFormKey();
+    }
+
+    /**
      * Escape html entities
      *
      * @param   mixed $data
diff --git downloader/lib/Mage/HTTP/Client/Curl.php downloader/lib/Mage/HTTP/Client/Curl.php
index f26010a..9240b57 100644
--- downloader/lib/Mage/HTTP/Client/Curl.php
+++ downloader/lib/Mage/HTTP/Client/Curl.php
@@ -372,8 +372,8 @@ implements Mage_HTTP_IClient
         $uriModified = $this->getSecureRequest($uri, $isAuthorizationRequired);
         $this->_ch = curl_init();
         $this->curlOption(CURLOPT_URL, $uriModified);
-        $this->curlOption(CURLOPT_SSL_VERIFYPEER, false);
-        $this->curlOption(CURLOPT_SSL_VERIFYHOST, 2);
+        $this->curlOption(CURLOPT_SSL_VERIFYPEER, true);
+        $this->curlOption(CURLOPT_SSL_VERIFYHOST, 'TLSv1');
         $this->getCurlMethodSettings($method, $params, $isAuthorizationRequired);
 
         if(count($this->_headers)) {
diff --git downloader/template/connect/packages.phtml downloader/template/connect/packages.phtml
index 92e6ea8..998e1d9 100644
--- downloader/template/connect/packages.phtml
+++ downloader/template/connect/packages.phtml
@@ -143,6 +143,7 @@ function connectPrepare(form) {
     <h4>Direct package file upload</h4>
 </div>
 <form action="<?php echo $this->url('connectInstallPackageUpload')?>" method="post" target="connect_iframe" onsubmit="onSubmit(this)" enctype="multipart/form-data">
+    <input name="form_key" type="hidden" value="<?php echo $this->getFormKey() ?>" />
     <ul class="bare-list">
         <li><span class="step-count">1</span> &nbsp; Download or build package file.</li>
         <li>
diff --git js/lib/uploader/flow.min.js js/lib/uploader/flow.min.js
new file mode 100644
index 0000000..34b888e
--- /dev/null
+++ js/lib/uploader/flow.min.js
@@ -0,0 +1,2 @@
+/*! flow.js 2.9.0 */
+!function(a,b,c){"use strict";function d(b){if(this.support=!("undefined"==typeof File||"undefined"==typeof Blob||"undefined"==typeof FileList||!Blob.prototype.slice&&!Blob.prototype.webkitSlice&&!Blob.prototype.mozSlice),this.support){this.supportDirectory=/WebKit/.test(a.navigator.userAgent),this.files=[],this.defaults={chunkSize:1048576,forceChunkSize:!1,simultaneousUploads:3,singleFile:!1,fileParameterName:"file",progressCallbacksInterval:500,speedSmoothingFactor:.1,query:{},headers:{},withCredentials:!1,preprocess:null,method:"multipart",testMethod:"GET",uploadMethod:"POST",prioritizeFirstAndLastChunk:!1,target:"/",testChunks:!0,generateUniqueIdentifier:null,maxChunkRetries:0,chunkRetryInterval:null,permanentErrors:[404,415,500,501],successStatuses:[200,201,202],onDropStopPropagation:!1},this.opts={},this.events={};var c=this;this.onDrop=function(a){c.opts.onDropStopPropagation&&a.stopPropagation(),a.preventDefault();var b=a.dataTransfer;b.items&&b.items[0]&&b.items[0].webkitGetAsEntry?c.webkitReadDataTransfer(a):c.addFiles(b.files,a)},this.preventEvent=function(a){a.preventDefault()},this.opts=d.extend({},this.defaults,b||{})}}function e(a,b){this.flowObj=a,this.file=b,this.name=b.fileName||b.name,this.size=b.size,this.relativePath=b.relativePath||b.webkitRelativePath||this.name,this.uniqueIdentifier=a.generateUniqueIdentifier(b),this.chunks=[],this.paused=!1,this.error=!1,this.averageSpeed=0,this.currentSpeed=0,this._lastProgressCallback=Date.now(),this._prevUploadedSize=0,this._prevProgress=0,this.bootstrap()}function f(a,b,c){this.flowObj=a,this.fileObj=b,this.fileObjSize=b.size,this.offset=c,this.tested=!1,this.retries=0,this.pendingRetry=!1,this.preprocessState=0,this.loaded=0,this.total=0;var d=this.flowObj.opts.chunkSize;this.startByte=this.offset*d,this.endByte=Math.min(this.fileObjSize,(this.offset+1)*d),this.xhr=null,this.fileObjSize-this.endByte<d&&!this.flowObj.opts.forceChunkSize&&(this.endByte=this.fileObjSize);var e=this;this.event=function(a,b){b=Array.prototype.slice.call(arguments),b.unshift(e),e.fileObj.chunkEvent.apply(e.fileObj,b)},this.progressHandler=function(a){a.lengthComputable&&(e.loaded=a.loaded,e.total=a.total),e.event("progress",a)},this.testHandler=function(){var a=e.status(!0);"error"===a?(e.event(a,e.message()),e.flowObj.uploadNextChunk()):"success"===a?(e.tested=!0,e.event(a,e.message()),e.flowObj.uploadNextChunk()):e.fileObj.paused||(e.tested=!0,e.send())},this.doneHandler=function(){var a=e.status();if("success"===a||"error"===a)e.event(a,e.message()),e.flowObj.uploadNextChunk();else{e.event("retry",e.message()),e.pendingRetry=!0,e.abort(),e.retries++;var b=e.flowObj.opts.chunkRetryInterval;null!==b?setTimeout(function(){e.send()},b):e.send()}}}function g(a,b){var c=a.indexOf(b);c>-1&&a.splice(c,1)}function h(a,b){return"function"==typeof a&&(b=Array.prototype.slice.call(arguments),a=a.apply(null,b.slice(1))),a}function i(a,b){setTimeout(a.bind(b),0)}function j(a){return k(arguments,function(b){b!==a&&k(b,function(b,c){a[c]=b})}),a}function k(a,b,c){if(a){var d;if("undefined"!=typeof a.length){for(d=0;d<a.length;d++)if(b.call(c,a[d],d)===!1)return}else for(d in a)if(a.hasOwnProperty(d)&&b.call(c,a[d],d)===!1)return}}var l=a.navigator.msPointerEnabled;d.prototype={on:function(a,b){a=a.toLowerCase(),this.events.hasOwnProperty(a)||(this.events[a]=[]),this.events[a].push(b)},off:function(a,b){a!==c?(a=a.toLowerCase(),b!==c?this.events.hasOwnProperty(a)&&g(this.events[a],b):delete this.events[a]):this.events={}},fire:function(a,b){b=Array.prototype.slice.call(arguments),a=a.toLowerCase();var c=!1;return this.events.hasOwnProperty(a)&&k(this.events[a],function(a){c=a.apply(this,b.slice(1))===!1||c},this),"catchall"!=a&&(b.unshift("catchAll"),c=this.fire.apply(this,b)===!1||c),!c},webkitReadDataTransfer:function(a){function b(a){g+=a.length,k(a,function(a){if(a.isFile){var e=a.fullPath;a.file(function(a){c(a,e)},d)}else a.isDirectory&&a.createReader().readEntries(b,d)}),e()}function c(a,b){a.relativePath=b.substring(1),h.push(a),e()}function d(a){throw a}function e(){0==--g&&f.addFiles(h,a)}var f=this,g=a.dataTransfer.items.length,h=[];k(a.dataTransfer.items,function(a){var f=a.webkitGetAsEntry();return f?void(f.isFile?c(a.getAsFile(),f.fullPath):f.createReader().readEntries(b,d)):void e()})},generateUniqueIdentifier:function(a){var b=this.opts.generateUniqueIdentifier;if("function"==typeof b)return b(a);var c=a.relativePath||a.webkitRelativePath||a.fileName||a.name;return a.size+"-"+c.replace(/[^0-9a-zA-Z_-]/gim,"")},uploadNextChunk:function(a){var b=!1;if(this.opts.prioritizeFirstAndLastChunk&&(k(this.files,function(a){return!a.paused&&a.chunks.length&&"pending"===a.chunks[0].status()&&0===a.chunks[0].preprocessState?(a.chunks[0].send(),b=!0,!1):!a.paused&&a.chunks.length>1&&"pending"===a.chunks[a.chunks.length-1].status()&&0===a.chunks[0].preprocessState?(a.chunks[a.chunks.length-1].send(),b=!0,!1):void 0}),b))return b;if(k(this.files,function(a){return a.paused||k(a.chunks,function(a){return"pending"===a.status()&&0===a.preprocessState?(a.send(),b=!0,!1):void 0}),b?!1:void 0}),b)return!0;var c=!1;return k(this.files,function(a){return a.isComplete()?void 0:(c=!0,!1)}),c||a||i(function(){this.fire("complete")},this),!1},assignBrowse:function(a,c,d,e){"undefined"==typeof a.length&&(a=[a]),k(a,function(a){var f;"INPUT"===a.tagName&&"file"===a.type?f=a:(f=b.createElement("input"),f.setAttribute("type","file"),j(f.style,{visibility:"hidden",position:"absolute"}),a.appendChild(f),a.addEventListener("click",function(){f.click()},!1)),this.opts.singleFile||d||f.setAttribute("multiple","multiple"),c&&f.setAttribute("webkitdirectory","webkitdirectory"),k(e,function(a,b){f.setAttribute(b,a)});var g=this;f.addEventListener("change",function(a){g.addFiles(a.target.files,a),a.target.value=""},!1)},this)},assignDrop:function(a){"undefined"==typeof a.length&&(a=[a]),k(a,function(a){a.addEventListener("dragover",this.preventEvent,!1),a.addEventListener("dragenter",this.preventEvent,!1),a.addEventListener("drop",this.onDrop,!1)},this)},unAssignDrop:function(a){"undefined"==typeof a.length&&(a=[a]),k(a,function(a){a.removeEventListener("dragover",this.preventEvent),a.removeEventListener("dragenter",this.preventEvent),a.removeEventListener("drop",this.onDrop)},this)},isUploading:function(){var a=!1;return k(this.files,function(b){return b.isUploading()?(a=!0,!1):void 0}),a},_shouldUploadNext:function(){var a=0,b=!0,c=this.opts.simultaneousUploads;return k(this.files,function(d){k(d.chunks,function(d){return"uploading"===d.status()&&(a++,a>=c)?(b=!1,!1):void 0})}),b&&a},upload:function(){var a=this._shouldUploadNext();if(a!==!1){this.fire("uploadStart");for(var b=!1,c=1;c<=this.opts.simultaneousUploads-a;c++)b=this.uploadNextChunk(!0)||b;b||i(function(){this.fire("complete")},this)}},resume:function(){k(this.files,function(a){a.resume()})},pause:function(){k(this.files,function(a){a.pause()})},cancel:function(){for(var a=this.files.length-1;a>=0;a--)this.files[a].cancel()},progress:function(){var a=0,b=0;return k(this.files,function(c){a+=c.progress()*c.size,b+=c.size}),b>0?a/b:0},addFile:function(a,b){this.addFiles([a],b)},addFiles:function(a,b){var c=[];k(a,function(a){if((!l||l&&a.size>0)&&(a.size%4096!==0||"."!==a.name&&"."!==a.fileName)&&!this.getFromUniqueIdentifier(this.generateUniqueIdentifier(a))){var d=new e(this,a);this.fire("fileAdded",d,b)&&c.push(d)}},this),this.fire("filesAdded",c,b)&&k(c,function(a){this.opts.singleFile&&this.files.length>0&&this.removeFile(this.files[0]),this.files.push(a)},this),this.fire("filesSubmitted",c,b)},removeFile:function(a){for(var b=this.files.length-1;b>=0;b--)this.files[b]===a&&(this.files.splice(b,1),a.abort())},getFromUniqueIdentifier:function(a){var b=!1;return k(this.files,function(c){c.uniqueIdentifier===a&&(b=c)}),b},getSize:function(){var a=0;return k(this.files,function(b){a+=b.size}),a},sizeUploaded:function(){var a=0;return k(this.files,function(b){a+=b.sizeUploaded()}),a},timeRemaining:function(){var a=0,b=0;return k(this.files,function(c){c.paused||c.error||(a+=c.size-c.sizeUploaded(),b+=c.averageSpeed)}),a&&!b?Number.POSITIVE_INFINITY:a||b?Math.floor(a/b):0}},e.prototype={measureSpeed:function(){var a=Date.now()-this._lastProgressCallback;if(a){var b=this.flowObj.opts.speedSmoothingFactor,c=this.sizeUploaded();this.currentSpeed=Math.max((c-this._prevUploadedSize)/a*1e3,0),this.averageSpeed=b*this.currentSpeed+(1-b)*this.averageSpeed,this._prevUploadedSize=c}},chunkEvent:function(a,b,c){switch(b){case"progress":if(Date.now()-this._lastProgressCallback<this.flowObj.opts.progressCallbacksInterval)break;this.measureSpeed(),this.flowObj.fire("fileProgress",this,a),this.flowObj.fire("progress"),this._lastProgressCallback=Date.now();break;case"error":this.error=!0,this.abort(!0),this.flowObj.fire("fileError",this,c,a),this.flowObj.fire("error",c,this,a);break;case"success":if(this.error)return;this.measureSpeed(),this.flowObj.fire("fileProgress",this,a),this.flowObj.fire("progress"),this._lastProgressCallback=Date.now(),this.isComplete()&&(this.currentSpeed=0,this.averageSpeed=0,this.flowObj.fire("fileSuccess",this,c,a));break;case"retry":this.flowObj.fire("fileRetry",this,a)}},pause:function(){this.paused=!0,this.abort()},resume:function(){this.paused=!1,this.flowObj.upload()},abort:function(a){this.currentSpeed=0,this.averageSpeed=0;var b=this.chunks;a&&(this.chunks=[]),k(b,function(a){"uploading"===a.status()&&(a.abort(),this.flowObj.uploadNextChunk())},this)},cancel:function(){this.flowObj.removeFile(this)},retry:function(){this.bootstrap(),this.flowObj.upload()},bootstrap:function(){this.abort(!0),this.error=!1,this._prevProgress=0;for(var a=this.flowObj.opts.forceChunkSize?Math.ceil:Math.floor,b=Math.max(a(this.file.size/this.flowObj.opts.chunkSize),1),c=0;b>c;c++)this.chunks.push(new f(this.flowObj,this,c))},progress:function(){if(this.error)return 1;if(1===this.chunks.length)return this._prevProgress=Math.max(this._prevProgress,this.chunks[0].progress()),this._prevProgress;var a=0;k(this.chunks,function(b){a+=b.progress()*(b.endByte-b.startByte)});var b=a/this.size;return this._prevProgress=Math.max(this._prevProgress,b>.9999?1:b),this._prevProgress},isUploading:function(){var a=!1;return k(this.chunks,function(b){return"uploading"===b.status()?(a=!0,!1):void 0}),a},isComplete:function(){var a=!1;return k(this.chunks,function(b){var c=b.status();return"pending"===c||"uploading"===c||1===b.preprocessState?(a=!0,!1):void 0}),!a},sizeUploaded:function(){var a=0;return k(this.chunks,function(b){a+=b.sizeUploaded()}),a},timeRemaining:function(){if(this.paused||this.error)return 0;var a=this.size-this.sizeUploaded();return a&&!this.averageSpeed?Number.POSITIVE_INFINITY:a||this.averageSpeed?Math.floor(a/this.averageSpeed):0},getType:function(){return this.file.type&&this.file.type.split("/")[1]},getExtension:function(){return this.name.substr((~-this.name.lastIndexOf(".")>>>0)+2).toLowerCase()}},f.prototype={getParams:function(){return{flowChunkNumber:this.offset+1,flowChunkSize:this.flowObj.opts.chunkSize,flowCurrentChunkSize:this.endByte-this.startByte,flowTotalSize:this.fileObjSize,flowIdentifier:this.fileObj.uniqueIdentifier,flowFilename:this.fileObj.name,flowRelativePath:this.fileObj.relativePath,flowTotalChunks:this.fileObj.chunks.length}},getTarget:function(a,b){return a+=a.indexOf("?")<0?"?":"&",a+b.join("&")},test:function(){this.xhr=new XMLHttpRequest,this.xhr.addEventListener("load",this.testHandler,!1),this.xhr.addEventListener("error",this.testHandler,!1);var a=h(this.flowObj.opts.testMethod,this.fileObj,this),b=this.prepareXhrRequest(a,!0);this.xhr.send(b)},preprocessFinished:function(){this.preprocessState=2,this.send()},send:function(){var a=this.flowObj.opts.preprocess;if("function"==typeof a)switch(this.preprocessState){case 0:return this.preprocessState=1,void a(this);case 1:return}if(this.flowObj.opts.testChunks&&!this.tested)return void this.test();this.loaded=0,this.total=0,this.pendingRetry=!1;var b=this.fileObj.file.slice?"slice":this.fileObj.file.mozSlice?"mozSlice":this.fileObj.file.webkitSlice?"webkitSlice":"slice",c=this.fileObj.file[b](this.startByte,this.endByte,this.fileObj.file.type);this.xhr=new XMLHttpRequest,this.xhr.upload.addEventListener("progress",this.progressHandler,!1),this.xhr.addEventListener("load",this.doneHandler,!1),this.xhr.addEventListener("error",this.doneHandler,!1);var d=h(this.flowObj.opts.uploadMethod,this.fileObj,this),e=this.prepareXhrRequest(d,!1,this.flowObj.opts.method,c);this.xhr.send(e)},abort:function(){var a=this.xhr;this.xhr=null,a&&a.abort()},status:function(a){return this.pendingRetry||1===this.preprocessState?"uploading":this.xhr?this.xhr.readyState<4?"uploading":this.flowObj.opts.successStatuses.indexOf(this.xhr.status)>-1?"success":this.flowObj.opts.permanentErrors.indexOf(this.xhr.status)>-1||!a&&this.retries>=this.flowObj.opts.maxChunkRetries?"error":(this.abort(),"pending"):"pending"},message:function(){return this.xhr?this.xhr.responseText:""},progress:function(){if(this.pendingRetry)return 0;var a=this.status();return"success"===a||"error"===a?1:"pending"===a?0:this.total>0?this.loaded/this.total:0},sizeUploaded:function(){var a=this.endByte-this.startByte;return"success"!==this.status()&&(a=this.progress()*a),a},prepareXhrRequest:function(a,b,c,d){var e=h(this.flowObj.opts.query,this.fileObj,this,b);e=j(this.getParams(),e);var f=h(this.flowObj.opts.target,this.fileObj,this,b),g=null;if("GET"===a||"octet"===c){var i=[];k(e,function(a,b){i.push([encodeURIComponent(b),encodeURIComponent(a)].join("="))}),f=this.getTarget(f,i),g=d||null}else g=new FormData,k(e,function(a,b){g.append(b,a)}),g.append(this.flowObj.opts.fileParameterName,d,this.fileObj.file.name);return this.xhr.open(a,f,!0),this.xhr.withCredentials=this.flowObj.opts.withCredentials,k(h(this.flowObj.opts.headers,this.fileObj,this,b),function(a,b){this.xhr.setRequestHeader(b,a)},this),g}},d.evalOpts=h,d.extend=j,d.each=k,d.FlowFile=e,d.FlowChunk=f,d.version="2.9.0","object"==typeof module&&module&&"object"==typeof module.exports?module.exports=d:(a.Flow=d,"function"==typeof define&&define.amd&&define("flow",[],function(){return d}))}(window,document);
\ No newline at end of file
diff --git js/lib/uploader/fusty-flow-factory.js js/lib/uploader/fusty-flow-factory.js
new file mode 100644
index 0000000..3d09bb0
--- /dev/null
+++ js/lib/uploader/fusty-flow-factory.js
@@ -0,0 +1,14 @@
+(function (Flow, FustyFlow, window) {
+  'use strict';
+
+  var fustyFlowFactory = function (opts) {
+    var flow = new Flow(opts);
+    if (flow.support) {
+      return flow;
+    }
+    return new FustyFlow(opts);
+  }
+
+  window.fustyFlowFactory = fustyFlowFactory;
+
+})(window.Flow, window.FustyFlow, window);
diff --git js/lib/uploader/fusty-flow.js js/lib/uploader/fusty-flow.js
new file mode 100644
index 0000000..4519a81
--- /dev/null
+++ js/lib/uploader/fusty-flow.js
@@ -0,0 +1,428 @@
+(function (Flow, window, document, undefined) {
+  'use strict';
+
+  var extend = Flow.extend;
+  var each = Flow.each;
+
+  function addEvent(element, type, handler) {
+    if (element.addEventListener) {
+      element.addEventListener(type, handler, false);
+    } else if (element.attachEvent) {
+      element.attachEvent("on" + type, handler);
+    } else {
+      element["on" + type] = handler;
+    }
+  }
+
+  function removeEvent(element, type, handler) {
+    if (element.removeEventListener) {
+      element.removeEventListener(type, handler, false);
+    } else if (element.detachEvent) {
+      element.detachEvent("on" + type, handler);
+    } else {
+      element["on" + type] = null;
+    }
+  }
+
+  function removeElement(element) {
+    element.parentNode.removeChild(element);
+  }
+
+  function isFunction(functionToCheck) {
+    var getType = {};
+    return functionToCheck && getType.toString.call(functionToCheck) === '[object Function]';
+  }
+
+  /**
+   * Not resumable file upload library, for IE7-IE9 browsers
+   * @name FustyFlow
+   * @param [opts]
+   * @param {bool} [opts.singleFile]
+   * @param {string} [opts.fileParameterName]
+   * @param {Object|Function} [opts.query]
+   * @param {Object} [opts.headers]
+   * @param {string} [opts.target]
+   * @param {Function} [opts.generateUniqueIdentifier]
+   * @param {bool} [opts.matchJSON]
+   * @constructor
+   */
+  function FustyFlow(opts) {
+    // Shortcut of "r instanceof Flow"
+    this.support = false;
+
+    this.files = [];
+    this.events = [];
+    this.defaults = {
+      simultaneousUploads: 3,
+      fileParameterName: 'file',
+      query: {},
+      target: '/',
+      generateUniqueIdentifier: null,
+      matchJSON: false
+    };
+
+    var $ = this;
+
+    this.inputChangeEvent = function (event) {
+      var input = event.target || event.srcElement;
+      removeEvent(input, 'change', $.inputChangeEvent);
+      var newClone = input.cloneNode(false);
+      // change current input with new one
+      input.parentNode.replaceChild(newClone, input);
+      // old input will be attached to hidden form
+      $.addFile(input, event);
+      // reset new input
+      newClone.value = '';
+      addEvent(newClone, 'change', $.inputChangeEvent);
+    };
+
+    this.opts = Flow.extend({}, this.defaults, opts || {});
+  }
+
+  FustyFlow.prototype = {
+    on: Flow.prototype.on,
+    off: Flow.prototype.off,
+    fire: Flow.prototype.fire,
+    cancel: Flow.prototype.cancel,
+    assignBrowse: function (domNodes) {
+      if (typeof domNodes.length == 'undefined') {
+        domNodes = [domNodes];
+      }
+      each(domNodes, function (domNode) {
+        var input;
+        if (domNode.tagName === 'INPUT' && domNode.type === 'file') {
+          input = domNode;
+        } else {
+          input = document.createElement('input');
+          input.setAttribute('type', 'file');
+
+          extend(domNode.style, {
+            display: 'inline-block',
+            position: 'relative',
+            overflow: 'hidden',
+            verticalAlign: 'top'
+          });
+
+          extend(input.style, {
+            position: 'absolute',
+            top: 0,
+            right: 0,
+            fontFamily: 'Arial',
+            // 4 persons reported this, the max values that worked for them were 243, 236, 236, 118
+            fontSize: '118px',
+            margin: 0,
+            padding: 0,
+            opacity: 0,
+            filter: 'alpha(opacity=0)',
+            cursor: 'pointer'
+          });
+
+          domNode.appendChild(input);
+        }
+        // When new files are added, simply append them to the overall list
+        addEvent(input, 'change', this.inputChangeEvent);
+      }, this);
+    },
+    assignDrop: function () {
+      // not supported
+    },
+    unAssignDrop: function () {
+      // not supported
+    },
+    isUploading: function () {
+      var uploading = false;
+      each(this.files, function (file) {
+        if (file.isUploading()) {
+          uploading = true;
+          return false;
+        }
+      });
+      return uploading;
+    },
+    upload: function () {
+      // Kick off the queue
+      var files = 0;
+      each(this.files, function (file) {
+        if (file.progress() == 1 || file.isPaused()) {
+          return;
+        }
+        if (file.isUploading()) {
+          files++;
+          return;
+        }
+        if (files++ >= this.opts.simultaneousUploads) {
+          return false;
+        }
+        if (files == 1) {
+          this.fire('uploadStart');
+        }
+        file.send();
+      }, this);
+      if (!files) {
+        this.fire('complete');
+      }
+    },
+    pause: function () {
+      each(this.files, function (file) {
+        file.pause();
+      });
+    },
+    resume: function () {
+      each(this.files, function (file) {
+        file.resume();
+      });
+    },
+    progress: function () {
+      var totalDone = 0;
+      var totalFiles = 0;
+      each(this.files, function (file) {
+        totalDone += file.progress();
+        totalFiles++;
+      });
+      return totalFiles > 0 ? totalDone / totalFiles : 0;
+    },
+    addFiles: function (elementsList, event) {
+      var files = [];
+      each(elementsList, function (element) {
+        // is domElement ?
+        if (element.nodeType === 1 && element.value) {
+          var f = new FustyFlowFile(this, element);
+          if (this.fire('fileAdded', f, event)) {
+            files.push(f);
+          }
+        }
+      }, this);
+      if (this.fire('filesAdded', files, event)) {
+        each(files, function (file) {
+          if (this.opts.singleFile && this.files.length > 0) {
+            this.removeFile(this.files[0]);
+          }
+          this.files.push(file);
+        }, this);
+      }
+      this.fire('filesSubmitted', files, event);
+    },
+    addFile: function (file, event) {
+      this.addFiles([file], event);
+    },
+    generateUniqueIdentifier: function (element) {
+      var custom = this.opts.generateUniqueIdentifier;
+      if (typeof custom === 'function') {
+        return custom(element);
+      }
+      return 'xxxxxxxx-xxxx-yxxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
+        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
+        return v.toString(16);
+      });
+    },
+    getFromUniqueIdentifier: function (uniqueIdentifier) {
+      var ret = false;
+      each(this.files, function (f) {
+        if (f.uniqueIdentifier == uniqueIdentifier) ret = f;
+      });
+      return ret;
+    },
+    removeFile: function (file) {
+      for (var i = this.files.length - 1; i >= 0; i--) {
+        if (this.files[i] === file) {
+          this.files.splice(i, 1);
+        }
+      }
+    },
+    getSize: function () {
+      // undefined
+    },
+    timeRemaining: function () {
+      // undefined
+    },
+    sizeUploaded: function () {
+      // undefined
+    }
+  };
+
+  function FustyFlowFile(flowObj, element) {
+    this.flowObj = flowObj;
+    this.element = element;
+    this.name = element.value && element.value.replace(/.*(\/|\\)/, "");
+    this.relativePath = this.name;
+    this.uniqueIdentifier = flowObj.generateUniqueIdentifier(element);
+    this.iFrame = null;
+
+    this.finished = false;
+    this.error = false;
+    this.paused = false;
+
+    var $ = this;
+    this.iFrameLoaded = function (event) {
+      // when we remove iframe from dom
+      // the request stops, but in IE load
+      // event fires
+      if (!$.iFrame || !$.iFrame.parentNode) {
+        return;
+      }
+      $.finished = true;
+      try {
+        // fixing Opera 10.53
+        if ($.iFrame.contentDocument &&
+          $.iFrame.contentDocument.body &&
+          $.iFrame.contentDocument.body.innerHTML == "false") {
+          // In Opera event is fired second time
+          // when body.innerHTML changed from false
+          // to server response approx. after 1 sec
+          // when we upload file with iframe
+          return;
+        }
+      } catch (error) {
+        //IE may throw an "access is denied" error when attempting to access contentDocument
+        $.error = true;
+        $.abort();
+        $.flowObj.fire('fileError', $, error);
+        return;
+      }
+      // iframe.contentWindow.document - for IE<7
+      var doc = $.iFrame.contentDocument || $.iFrame.contentWindow.document;
+      var innerHtml = doc.body.innerHTML;
+      if ($.flowObj.opts.matchJSON) {
+        innerHtml = /(\{.*\})/.exec(innerHtml)[0];
+      }
+
+      $.abort();
+      $.flowObj.fire('fileSuccess', $, innerHtml);
+      $.flowObj.upload();
+    };
+    this.bootstrap();
+  }
+
+  FustyFlowFile.prototype = {
+    getExtension: Flow.FlowFile.prototype.getExtension,
+    getType: function () {
+      // undefined
+    },
+    send: function () {
+      if (this.finished) {
+        return;
+      }
+      var o = this.flowObj.opts;
+      var form = this.createForm();
+      var params = o.query;
+      if (isFunction(params)) {
+        params = params(this);
+      }
+      params[o.fileParameterName] = this.element;
+      params['flowFilename'] = this.name;
+      params['flowRelativePath'] = this.relativePath;
+      params['flowIdentifier'] = this.uniqueIdentifier;
+
+      this.addFormParams(form, params);
+      addEvent(this.iFrame, 'load', this.iFrameLoaded);
+      form.submit();
+      removeElement(form);
+    },
+    abort: function (noupload) {
+      if (this.iFrame) {
+        this.iFrame.setAttribute('src', 'java' + String.fromCharCode(115) + 'cript:false;');
+        removeElement(this.iFrame);
+        this.iFrame = null;
+        !noupload && this.flowObj.upload();
+      }
+    },
+    cancel: function () {
+      this.flowObj.removeFile(this);
+      this.abort();
+    },
+    retry: function () {
+      this.bootstrap();
+      this.flowObj.upload();
+    },
+    bootstrap: function () {
+      this.abort(true);
+      this.finished = false;
+      this.error = false;
+    },
+    timeRemaining: function () {
+      // undefined
+    },
+    sizeUploaded: function () {
+      // undefined
+    },
+    resume: function () {
+      this.paused = false;
+      this.flowObj.upload();
+    },
+    pause: function () {
+      this.paused = true;
+      this.abort();
+    },
+    isUploading: function () {
+      return this.iFrame !== null;
+    },
+    isPaused: function () {
+      return this.paused;
+    },
+    isComplete: function () {
+      return this.progress() === 1;
+    },
+    progress: function () {
+      if (this.error) {
+        return 1;
+      }
+      return this.finished ? 1 : 0;
+    },
+
+    createIframe: function () {
+      var iFrame = (/MSIE (6|7|8)/).test(navigator.userAgent) ?
+        document.createElement('<iframe name="' + this.uniqueIdentifier + '_iframe' + '">') :
+        document.createElement('iframe');
+
+      iFrame.setAttribute('id', this.uniqueIdentifier + '_iframe_id');
+      iFrame.setAttribute('name', this.uniqueIdentifier + '_iframe');
+      iFrame.style.display = 'none';
+      document.body.appendChild(iFrame);
+      return iFrame;
+    },
+    createForm: function() {
+      var target = this.flowObj.opts.target;
+      if (typeof target === "function") {
+        target = target.apply(null);
+      }
+
+      var form = document.createElement('form');
+      form.encoding = "multipart/form-data";
+      form.method = "POST";
+      form.setAttribute('action', target);
+      if (!this.iFrame) {
+        this.iFrame = this.createIframe();
+      }
+      form.setAttribute('target', this.iFrame.name);
+      form.style.display = 'none';
+      document.body.appendChild(form);
+      return form;
+    },
+    addFormParams: function(form, params) {
+      var input;
+      each(params, function (value, key) {
+        if (value && value.nodeType === 1) {
+          input = value;
+        } else {
+          input = document.createElement('input');
+          input.setAttribute('value', value);
+        }
+        input.setAttribute('name', key);
+        form.appendChild(input);
+      });
+    }
+  };
+
+  FustyFlow.FustyFlowFile = FustyFlowFile;
+
+  if (typeof module !== 'undefined') {
+    module.exports = FustyFlow;
+  } else if (typeof define === "function" && define.amd) {
+    // AMD/requirejs: Define the module
+    define(function(){
+      return FustyFlow;
+    });
+  } else {
+    window.FustyFlow = FustyFlow;
+  }
+})(window.Flow, window, document);
diff --git js/mage/adminhtml/product.js js/mage/adminhtml/product.js
index 2ba1d51..ee0e7e7 100644
--- js/mage/adminhtml/product.js
+++ js/mage/adminhtml/product.js
@@ -34,18 +34,18 @@ Product.Gallery.prototype = {
     idIncrement :1,
     containerId :'',
     container :null,
-    uploader :null,
     imageTypes : {},
-    initialize : function(containerId, uploader, imageTypes) {
+    initialize : function(containerId, imageTypes) {
         this.containerId = containerId, this.container = $(this.containerId);
-        this.uploader = uploader;
         this.imageTypes = imageTypes;
-        if (this.uploader) {
-            this.uploader.onFilesComplete = this.handleUploadComplete
-                    .bind(this);
-        }
-        // this.uploader.onFileProgress = this.handleUploadProgress.bind(this);
-        // this.uploader.onFileError = this.handleUploadError.bind(this);
+
+        document.on('uploader:fileSuccess', function(event) {
+            var memo = event.memo;
+            if(memo && this._checkCurrentContainer(memo.containerId)) {
+                this.handleUploadComplete([{response: memo.response}]);
+            }
+        }.bind(this));
+
         this.images = this.getElement('save').value.evalJSON();
         this.imagesValues = this.getElement('save_image').value.evalJSON();
         this.template = new Template('<tr id="__id__" class="preview">' + this
@@ -56,6 +56,9 @@ Product.Gallery.prototype = {
         varienGlobalEvents.attachEventHandler('moveTab', this.onImageTabMove
                 .bind(this));
     },
+    _checkCurrentContainer: function(child) {
+        return $(this.containerId).down('#' + child);
+    },
     onImageTabMove : function(event) {
         var imagesTab = false;
         this.container.ancestors().each( function(parentItem) {
@@ -113,7 +116,6 @@ Product.Gallery.prototype = {
             newImage.disabled = 0;
             newImage.removed = 0;
             this.images.push(newImage);
-            this.uploader.removeFile(item.id);
         }.bind(this));
         this.container.setHasChanges();
         this.updateImages();
diff --git js/mage/adminhtml/uploader/instance.js js/mage/adminhtml/uploader/instance.js
new file mode 100644
index 0000000..483b2af
--- /dev/null
+++ js/mage/adminhtml/uploader/instance.js
@@ -0,0 +1,508 @@
+/**
+ * Magento Enterprise Edition
+ *
+ * NOTICE OF LICENSE
+ *
+ * This source file is subject to the Magento Enterprise Edition End User License Agreement
+ * that is bundled with this package in the file LICENSE_EE.txt.
+ * It is also available through the world-wide-web at this URL:
+ * http://www.magento.com/license/enterprise-edition
+ * If you did not receive a copy of the license and are unable to
+ * obtain it through the world-wide-web, please send an email
+ * to license@magento.com so we can send you a copy immediately.
+ *
+ * DISCLAIMER
+ *
+ * Do not edit or add to this file if you wish to upgrade Magento to newer
+ * versions in the future. If you wish to customize Magento for your
+ * needs please refer to http://www.magento.com for more information.
+ *
+ * @category    design
+ * @package     default_default
+ * @copyright Copyright (c) 2006-2016 X.commerce, Inc. and affiliates (http://www.magento.com)
+ * @license http://www.magento.com/license/enterprise-edition
+ */
+
+(function(flowFactory, window, document) {
+'use strict';
+    window.Uploader = Class.create({
+
+        /**
+         * @type {Boolean} Are we in debug mode?
+         */
+        debug: false,
+
+        /**
+         * @constant
+         * @type {String} templatePattern
+         */
+        templatePattern: /(^|.|\r|\n)({{(\w+)}})/,
+
+        /**
+         * @type {JSON} Array of elements ids to instantiate DOM collection
+         */
+        elementsIds: [],
+
+        /**
+         * @type {Array.<HTMLElement>} List of elements ids across all uploader functionality
+         */
+        elements: [],
+
+        /**
+         * @type {(FustyFlow|Flow)} Uploader object instance
+         */
+        uploader: {},
+
+        /**
+         * @type {JSON} General Uploader config
+         */
+        uploaderConfig: {},
+
+        /**
+         * @type {JSON} browseConfig General Uploader config
+         */
+        browseConfig: {},
+
+        /**
+         * @type {JSON} Misc settings to manipulate Uploader
+         */
+        miscConfig: {},
+
+        /**
+         * @type {Array.<String>} Sizes in plural
+         */
+        sizesPlural: ['bytes', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
+
+        /**
+         * @type {Number} Precision of calculation during convetion to human readable size format
+         */
+        sizePrecisionDefault: 3,
+
+        /**
+         * @type {Number} Unit type conversion kib or kb, etc
+         */
+        sizeUnitType: 1024,
+
+        /**
+         * @type {String} Default delete button selector
+         */
+        deleteButtonSelector: '.delete',
+
+        /**
+         * @type {Number} Timeout of completion handler
+         */
+        onCompleteTimeout: 1000,
+
+        /**
+         * @type {(null|Array.<FlowFile>)} Files array stored for success event
+         */
+        files: null,
+
+
+        /**
+         * @name Uploader
+         *
+         * @param {JSON} config
+         *
+         * @constructor
+         */
+        initialize: function(config) {
+            this.elementsIds = config.elementIds;
+            this.elements = this.getElements(this.elementsIds);
+
+            this.uploaderConfig = config.uploaderConfig;
+            this.browseConfig = config.browseConfig;
+            this.miscConfig =  config.miscConfig;
+
+            this.uploader = flowFactory(this.uploaderConfig);
+
+            this.attachEvents();
+
+            /**
+             * Bridging functions to retain functionality of existing modules
+             */
+            this.formatSize = this._getPluralSize.bind(this);
+            this.upload = this.onUploadClick.bind(this);
+            this.onContainerHideBefore = this.onTabChange.bind(this);
+        },
+
+        /**
+         * Array of strings containing elements ids
+         *
+         * @param {JSON.<string, Array.<string>>} ids as JSON map,
+         *      {<type> => ['id1', 'id2'...], <type2>...}
+         * @returns {Array.<HTMLElement>} An array of DOM elements
+         */
+        getElements: function (ids) {
+            /** @type {Hash} idsHash */
+            var idsHash = $H(ids);
+
+            idsHash.each(function (id) {
+                var result = this.getElementsByIds(id.value);
+
+                idsHash.set(id.key, result);
+            }.bind(this));
+
+            return idsHash.toObject();
+        },
+
+        /**
+         * Get HTMLElement from hash values
+         *
+         * @param {(Array|String)}ids
+         * @returns {(Array.<HTMLElement>|HTMLElement)}
+         */
+        getElementsByIds: function (ids) {
+            var result = [];
+            if(ids && Object.isArray(ids)) {
+                ids.each(function(fromId) {
+                    var DOMElement = $(fromId);
+
+                    if (DOMElement) {
+                        // Add it only if it's valid HTMLElement, otherwise skip.
+                        result.push(DOMElement);
+                    }
+                });
+            } else {
+                result = $(ids)
+            }
+
+            return result;
+        },
+
+        /**
+         * Attach all types of events
+         */
+        attachEvents: function() {
+            this.assignBrowse();
+
+            this.uploader.on('filesSubmitted', this.onFilesSubmitted.bind(this));
+
+            this.uploader.on('uploadStart', this.onUploadStart.bind(this));
+
+            this.uploader.on('fileSuccess', this.onFileSuccess.bind(this));
+            this.uploader.on('complete', this.onSuccess.bind(this));
+
+            if(this.elements.container && !this.elements.delete) {
+                this.elements.container.on('click', this.deleteButtonSelector, this.onDeleteClick.bind(this));
+            } else {
+                if(this.elements.delete) {
+                    this.elements.delete.on('click', Event.fire.bind(this, document, 'upload:simulateDelete', {
+                        containerId: this.elementsIds.container
+                    }));
+                }
+            }
+            if(this.elements.upload) {
+                this.elements.upload.invoke('on', 'click', this.onUploadClick.bind(this));
+            }
+            if(this.debug) {
+                this.uploader.on('catchAll', this.onCatchAll.bind(this));
+            }
+        },
+
+        onTabChange: function (successFunc) {
+            if(this.uploader.files.length && !Object.isArray(this.files)) {
+                if(confirm(
+                        this._translate('There are files that were selected but not uploaded yet. After switching to another tab your selections will be lost. Do you wish to continue ?')
+                   )
+                ) {
+                    if(Object.isFunction(successFunc)) {
+                        successFunc();
+                    } else {
+                        this._handleDelete(this.uploader.files);
+                        document.fire('uploader:fileError', {
+                            containerId: this.elementsIds.container
+                        });
+                    }
+                } else {
+                    return 'cannotchange';
+                }
+            }
+        },
+
+        /**
+         * Assign browse buttons to appropriate targets
+         */
+        assignBrowse: function() {
+            if (this.elements.browse && this.elements.browse.length) {
+                this.uploader.assignBrowse(
+                    this.elements.browse,
+                    this.browseConfig.isDirectory || false,
+                    this.browseConfig.singleFile || false,
+                    this.browseConfig.attributes || {}
+                );
+            }
+        },
+
+        /**
+         * @event
+         * @param {Array.<FlowFile>} files
+         */
+        onFilesSubmitted: function (files) {
+            files.filter(function (file) {
+                if(this._checkFileSize(file)) {
+                    alert(
+                        this._translate('Maximum allowed file size for upload is') +
+                        " " + this.miscConfig.maxSizePlural + "\n" +
+                        this._translate('Please check your server PHP settings.')
+                    );
+                    file.cancel();
+                    return false;
+                }
+                return true;
+            }.bind(this)).each(function (file) {
+                this._handleUpdateFile(file);
+            }.bind(this));
+        },
+
+        _handleUpdateFile: function (file) {
+            var replaceBrowseWithRemove = this.miscConfig.replaceBrowseWithRemove;
+            if(replaceBrowseWithRemove) {
+                document.fire('uploader:simulateNewUpload', { containerId: this.elementsIds.container });
+            }
+            this.elements.container
+                [replaceBrowseWithRemove ? 'update':'insert'](this._renderFromTemplate(
+                    this.elements.templateFile,
+                    {
+                        name: file.name,
+                        size: file.size ? '(' + this._getPluralSize(file.size) + ')' : '',
+                        id: file.uniqueIdentifier
+                    }
+                )
+            );
+        },
+
+        /**
+         * Upload button is being pressed
+         *
+         * @event
+         */
+        onUploadStart: function () {
+            var files = this.uploader.files;
+
+            files.each(function (file) {
+                var id = file.uniqueIdentifier;
+
+                this._getFileContainerById(id)
+                    .removeClassName('new')
+                    .removeClassName('error')
+                    .addClassName('progress');
+                this._getProgressTextById(id).update(this._translate('Uploading...'));
+
+                var deleteButton = this._getDeleteButtonById(id);
+                if(deleteButton) {
+                    this._getDeleteButtonById(id).hide();
+                }
+            }.bind(this));
+
+            this.files = this.uploader.files;
+        },
+
+        /**
+         * Get file-line container by id
+         *
+         * @param {String} id
+         * @returns {HTMLElement}
+         * @private
+         */
+        _getFileContainerById: function (id) {
+            return $(id + '-container');
+        },
+
+        /**
+         * Get text update container
+         *
+         * @param id
+         * @returns {*}
+         * @private
+         */
+        _getProgressTextById: function (id) {
+            return this._getFileContainerById(id).down('.progress-text');
+        },
+
+        _getDeleteButtonById: function(id) {
+            return this._getFileContainerById(id).down('.delete');
+        },
+
+        /**
+         * Handle delete button click
+         *
+         * @event
+         * @param {Event} e
+         */
+        onDeleteClick: function (e) {
+            var element = Event.findElement(e);
+            var id = element.id;
+            if(!id) {
+                id = element.up(this.deleteButtonSelector).id;
+            }
+            this._handleDelete([this.uploader.getFromUniqueIdentifier(id)]);
+        },
+
+        /**
+         * Complete handler of uploading process
+         *
+         * @event
+         */
+        onSuccess: function () {
+            document.fire('uploader:success', { files: this.files });
+            this.files = null;
+        },
+
+        /**
+         * Successfully uploaded file, notify about that other components, handle deletion from queue
+         *
+         * @param {FlowFile} file
+         * @param {JSON} response
+         */
+        onFileSuccess: function (file, response) {
+            response = response.evalJSON();
+            var id = file.uniqueIdentifier;
+            var error = response.error;
+            this._getFileContainerById(id)
+                .removeClassName('progress')
+                .addClassName(error ? 'error': 'complete')
+            ;
+            this._getProgressTextById(id).update(this._translate(
+                error ? this._XSSFilter(error) :'Complete'
+            ));
+
+            setTimeout(function() {
+                if(!error) {
+                    document.fire('uploader:fileSuccess', {
+                        response: Object.toJSON(response),
+                        containerId: this.elementsIds.container
+                    });
+                } else {
+                    document.fire('uploader:fileError', {
+                        containerId: this.elementsIds.container
+                    });
+                }
+                this._handleDelete([file]);
+            }.bind(this) , !error ? this.onCompleteTimeout: this.onCompleteTimeout * 3);
+        },
+
+        /**
+         * Upload button click event
+         *
+         * @event
+         */
+        onUploadClick: function () {
+            try {
+                this.uploader.upload();
+            } catch(e) {
+                if(console) {
+                    console.error(e);
+                }
+            }
+        },
+
+        /**
+         * Event for debugging purposes
+         *
+         * @event
+         */
+        onCatchAll: function () {
+            if(console.group && console.groupEnd && console.trace) {
+                var args = [].splice.call(arguments, 1);
+                console.group();
+                    console.info(arguments[0]);
+                    console.log("Uploader Instance:", this);
+                    console.log("Event Arguments:", args);
+                    console.trace();
+                console.groupEnd();
+            } else {
+                console.log(this, arguments);
+            }
+        },
+
+        /**
+         * Handle deletition of files
+         * @param {Array.<FlowFile>} files
+         * @private
+         */
+        _handleDelete: function (files) {
+            files.each(function (file) {
+                file.cancel();
+                var container = $(file.uniqueIdentifier + '-container');
+                if(container) {
+                    container.remove();
+                }
+            }.bind(this));
+        },
+
+        /**
+         * Check whenever file size exceeded permitted amount
+         *
+         * @param {FlowFile} file
+         * @returns {boolean}
+         * @private
+         */
+        _checkFileSize: function (file) {
+            return file.size > this.miscConfig.maxSizeInBytes;
+        },
+
+        /**
+         * Make a translation of string
+         *
+         * @param {String} text
+         * @returns {String}
+         * @private
+         */
+        _translate: function (text) {
+            try {
+                return Translator.translate(text);
+            }
+            catch(e){
+                return text;
+            }
+        },
+
+        /**
+         * Render from given template and given variables to assign
+         *
+         * @param {HTMLElement} template
+         * @param {JSON} vars
+         * @returns {String}
+         * @private
+         */
+        _renderFromTemplate: function (template, vars) {
+            var t = new Template(this._XSSFilter(template.innerHTML), this.templatePattern);
+            return t.evaluate(vars);
+        },
+
+        /**
+         * Format size with precision
+         *
+         * @param {Number} sizeInBytes
+         * @param {Number} [precision]
+         * @returns {String}
+         * @private
+         */
+        _getPluralSize: function (sizeInBytes, precision) {
+                if(sizeInBytes == 0) {
+                    return 0 + this.sizesPlural[0];
+                }
+                var dm = (precision || this.sizePrecisionDefault) + 1;
+                var i = Math.floor(Math.log(sizeInBytes) / Math.log(this.sizeUnitType));
+
+                return (sizeInBytes / Math.pow(this.sizeUnitType, i)).toPrecision(dm) + ' ' + this.sizesPlural[i];
+        },
+
+        /**
+         * Purify template string to prevent XSS attacks
+         *
+         * @param {String} str
+         * @returns {String}
+         * @private
+         */
+        _XSSFilter: function (str) {
+            return str
+                .stripScripts()
+                // Remove inline event handlers like onclick, onload, etc
+                .replace(/(on[a-z]+=["][^"]+["])(?=[^>]*>)/img, '')
+                .replace(/(on[a-z]+=['][^']+['])(?=[^>]*>)/img, '')
+            ;
+        }
+    });
+})(fustyFlowFactory, window, document);
diff --git lib/Unserialize/Parser.php lib/Unserialize/Parser.php
index 423902a..2c01684 100644
--- lib/Unserialize/Parser.php
+++ lib/Unserialize/Parser.php
@@ -34,6 +34,7 @@ class Unserialize_Parser
     const TYPE_DOUBLE = 'd';
     const TYPE_ARRAY = 'a';
     const TYPE_BOOL = 'b';
+    const TYPE_NULL = 'N';
 
     const SYMBOL_QUOTE = '"';
     const SYMBOL_SEMICOLON = ';';
diff --git lib/Unserialize/Reader/Arr.php lib/Unserialize/Reader/Arr.php
index caa979e..cd37804 100644
--- lib/Unserialize/Reader/Arr.php
+++ lib/Unserialize/Reader/Arr.php
@@ -101,7 +101,10 @@ class Unserialize_Reader_Arr
         if ($this->_status == self::READING_VALUE) {
             $value = $this->_reader->read($char, $prevChar);
             if (!is_null($value)) {
-                $this->_result[$this->_reader->key] = $value;
+                $this->_result[$this->_reader->key] =
+                    ($value == Unserialize_Reader_Null::NULL_VALUE && $prevChar == Unserialize_Parser::TYPE_NULL)
+                        ? null
+                        : $value;
                 if (count($this->_result) < $this->_length) {
                     $this->_reader = new Unserialize_Reader_ArrKey();
                     $this->_status = self::READING_KEY;
diff --git lib/Unserialize/Reader/ArrValue.php lib/Unserialize/Reader/ArrValue.php
index d2a4937..c6c0221 100644
--- lib/Unserialize/Reader/ArrValue.php
+++ lib/Unserialize/Reader/ArrValue.php
@@ -84,6 +84,10 @@ class Unserialize_Reader_ArrValue
                     $this->_reader = new Unserialize_Reader_Dbl();
                     $this->_status = self::READING_VALUE;
                     break;
+                case Unserialize_Parser::TYPE_NULL:
+                    $this->_reader = new Unserialize_Reader_Null();
+                    $this->_status = self::READING_VALUE;
+                    break;
                 default:
                     throw new Exception('Unsupported data type ' . $char);
             }
diff --git lib/Unserialize/Reader/Null.php lib/Unserialize/Reader/Null.php
new file mode 100644
index 0000000..93c7e0b
--- /dev/null
+++ lib/Unserialize/Reader/Null.php
@@ -0,0 +1,64 @@
+<?php
+/**
+ * Magento
+ *
+ * NOTICE OF LICENSE
+ *
+ * This source file is subject to the Open Software License (OSL 3.0)
+ * that is bundled with this package in the file LICENSE.txt.
+ * It is also available through the world-wide-web at this URL:
+ * http://opensource.org/licenses/osl-3.0.php
+ * If you did not receive a copy of the license and are unable to
+ * obtain it through the world-wide-web, please send an email
+ * to license@magento.com so we can send you a copy immediately.
+ *
+ * DISCLAIMER
+ *
+ * Do not edit or add to this file if you wish to upgrade Magento to newer
+ * versions in the future. If you wish to customize Magento for your
+ * needs please refer to http://www.magento.com for more information.
+ *
+ * @category    Unserialize
+ * @package     Unserialize_Reader_Null
+ * @copyright  Copyright (c) 2006-2016 X.commerce, Inc. and affiliates (http://www.magento.com)
+ * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
+ */
+
+/**
+ * Class Unserialize_Reader_Null
+ */
+class Unserialize_Reader_Null
+{
+    /**
+     * @var int
+     */
+    protected $_status;
+
+    /**
+     * @var string
+     */
+    protected $_value;
+
+    const NULL_VALUE = 'null';
+
+    const READING_VALUE = 1;
+
+    /**
+     * @param string $char
+     * @param string $prevChar
+     * @return string|null
+     */
+    public function read($char, $prevChar)
+    {
+        if ($prevChar == Unserialize_Parser::SYMBOL_SEMICOLON) {
+            $this->_value = self::NULL_VALUE;
+            $this->_status = self::READING_VALUE;
+            return null;
+        }
+
+        if ($this->_status == self::READING_VALUE && $char == Unserialize_Parser::SYMBOL_SEMICOLON) {
+            return $this->_value;
+        }
+        return null;
+    }
+}
\ No newline at end of file
