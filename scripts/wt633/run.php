<?php

ini_set('max_execution_time', 0);
ini_set('display_error', 1);
ini_set('memory_limit', -1);

include_once(__DIR__ . '/../../app/Mage.php');
Mage::getIsDeveloperMode(true);
Mage::app();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

$logFileName = 'wt633_editing_log.log';
$clearenceSubCat = array(
    'veggy_festival_body' => ['id' => 4396, 'csv' => 'veggy_festival_body.csv'],
    'veggy_festival_environment' => ['id' => 4398, 'csv' => 'veggy_festival_environment.csv'],
    'veggy_festival_mind' => ['id' => 4397, 'csv' => 'veggy_festival_mind.csv'],
);


foreach($clearenceSubCat as $key => $value){
    if (!file_exists($value['csv'])) {
        echo "Item file was not found. You should go to last sub-folder \"script/wt633/\" from Magento's root. \nThen run php file in there.\n";
        exit();
    }
}

// Remove old products
$newCategoryIds = [4396,4398,4397];
$categoryCollection = Mage::getModel('catalog/category')->getCollection();
$categoryCollection->addFieldToFilter('entity_id', $newCategoryIds);
$startTimeRemoveOldProduct = microtime(true);
echo "\n ----------------------------------------------";
echo "\n ----------------------------------------------";
Mage::log("----------------------------------------------", null, $logFileName, true);
Mage::log("----------------------------------------------", null, $logFileName, true);

echo "\n Date: " . date('d/m/Y H:i:s');
Mage::log("\n Date: " . date('d/m/Y H:i:s'), null, $logFileName, true);


echo "\n\n Removing old products of clearance categories...";
Mage::log("Removing old products of clearance categories...", null, $logFileName, true);
foreach ($categoryCollection as $aCat) {
    $productCollection = $aCat->setStoreId(0)->getProductCollection();
    echo "\n Category Id: " . $aCat->getId() . " - Total products: " . $productCollection->count();
    Mage::log("Category Id:" . $aCat->getId() . " - Total producst: " . $productCollection->count(), null, $logFileName, true);
    $tempCount = $tempCountSuccess = 0;
    foreach ($productCollection as $aProduct) {
        $tempCount++;
        $aProduct->setCategoryIds((array_diff($aProduct->getCategoryIds(), [$aCat->getId()])));
        try {
            $aProduct->save();
            $tempCountSuccess++;
        } catch (Exception $e) {
            echo "\n --- Error: " . $e->getMessage();
            Mage::log("--- Error: " . $e->getMessage(), null, $logFileName, true);
        }
    }
    echo " - Success: " . $tempCountSuccess . '/' . $tempCount;
    Mage::log(" - Success: " . $tempCountSuccess . "/" . $tempCount, null, $logFileName, true);
}
echo "\n Time for removing old products of clearance categories: " . number_format(microtime(true) - $startTimeRemoveOldProduct, 2) . " s";
Mage::log("Time for removing old products of clearance categories: " . number_format(microtime(true) - $startTimeRemoveOldProduct, 2) . " s", null, $logFileName, true);
echo "\n ----------------------------------------------";
echo "\n ----------------------------------------------";
Mage::log("----------------------------------------------", null, $logFileName, true);
Mage::log("----------------------------------------------", null, $logFileName, true);

// Get data from csv file
function strGetCsv($string)
{
    $data = array();
    $rows = str_getcsv($string, "\n");
    foreach ($rows as $row) {
        $data[] = str_getcsv($row);
    }
    unset($data[0]);// split header in CSV file
    return $data;
}

// Add product to category
foreach ($clearenceSubCat as $key => $value) {
    $sqlFileBeauty = $value['csv'];
    $sqlString = file_get_contents($sqlFileBeauty);
    $mainData = strGetCsv($sqlString);
    $data = array();
    foreach ($mainData as $item) {
        if(isset($item[0])) $data[] = $item[0];
    }
    $totalProduct = count($data);
    echo "\n Total products from " . $value['csv'] . ': ' . $totalProduct;
    Mage::log("Total products from " . $value['csv'] . ': ' . $totalProduct, null, $logFileName, true);

    //  Add product to category
    echo "\n Adding product to category \"" . $key . "\" ...\n\n";
    Mage::log("Adding product to category " . $key . " ...", null, $logFileName, true);
    $startTimeAdding = microtime(true);
    $count = 0;
    $countSuccess = 0;
    foreach ($data as $sku) {
        $count++;
        /** @var Mage_Catalog_Model_Product $product */
        try {
            echo $sku . "; ";
            $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);
            if ($product instanceof Mage_Catalog_Model_Product) {
                if ($product->getId()) {
                    // get category ids
                    $categoryIds = $product->getCategoryIds();
                    $categoryIds[] = $value['id'];
                    $product->setCategoryIds($categoryIds);
                    try {
                        $product->save();
                        $countSuccess++;
                    } catch (Exception $e) {
                        echo "\n {$sku} wasn't added";
                        echo "\n --- Error: " . $e->getMessage() . PHP_EOL;
                        Mage::log("{$sku} wasn't added", null, $logFileName, true);
                        Mage::log("Error: " . $e->getMessage(), null, $logFileName, true);
                    }
                } else {
                    echo $count . ". Product #{$sku} is not found..." . PHP_EOL;
                    Mage::log("Product #{$sku} is not found...", null, $logFileName, true);
                }
            }
        } catch (Exception $e) {
            echo "\n Problem with sku #{$sku}";
            Mage::log("\n Problem with sku #{$sku} ", null, $logFileName, true);
            echo "\n --- Error: " . $e->getMessage() . PHP_EOL;
            Mage::log("\n --- Error: " . $e->getMessage() . PHP_EOL, null, $logFileName, true);
        }

        if ($count % 200 == 0 || $count == $totalProduct) {
            echo "\n\n Total product added: " . $count . '/' . $totalProduct;
            echo "\n Added Success: " . $countSuccess . '/' . $count . "\n";
        }
    }
    echo "\n Success: " . $countSuccess . '/' . $totalProduct;
    echo "\n Time for adding product to category \"" . $key . "\": " . number_format(microtime(true) - $startTimeAdding, 2) . " s";
    Mage::log("Time for adding product to category \"" . $key . "\": " . number_format(microtime(true) - $startTimeAdding, 2) . " s", null, $logFileName, true);
    echo "\n ----------------------------------------------";
    Mage::log("----------------------------------------------", null, $logFileName, true);
}
echo "\n All done!";
Mage::log("All done!");
exit();