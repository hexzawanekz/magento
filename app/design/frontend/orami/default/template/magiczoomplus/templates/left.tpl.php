<?php
$m = intval(self::$options->getValue('selectors-margin'));
$ws = intval(self::$options->getValue('selector-max-width'));
$wm = intval(self::$options->getValue('thumb-max-width'));
$hm = intval(self::$options->getValue('thumb-max-height'));
?>
<!-- Begin magiczoomplus -->
<div class="MagicToolboxContainer selectorsLeft" style="width: <?php echo ($wm + $ws + $m)?>px">
    <?php if(count($thumbs)):?>
        <div class="small-image-container clearfix">
            <span id="arrow-back" class="arrow"></span>
            <div id="MagicToolboxSelectors<?php echo $pid?>" class="bxslider MagicToolboxSelectorsContainer<?php echo $magicscroll;?>" style="width: <?php echo $ws?>px;margin-right: <?php echo $m?>px;">
                <?php echo join("\n\t",$thumbs)?>
            </div>
            <span id="arrow-foward" class="arrow"></span>
        </div>
    <?php endif?>
    <div class="MagicToolboxMainContainer" style="width: <?php echo $wm?>px">
        <?php echo $main?>
        <?php if(isset($message)):?>
            <div class="MagicToolboxMessage"><?php echo $message?></div>
        <?php endif?>
        <?php if(!empty($magicscroll)): ?>
            <script type="text/javascript">
                MagicScroll.extraOptions.MagicToolboxSelectors<?php echo $pid?> = MagicScroll.extraOptions.MagicToolboxSelectors<?php echo $pid?> || {};
                MagicScroll.extraOptions.MagicToolboxSelectors<?php echo $pid?>.direction = 'bottom';
                <?php if(self::$options->checkValue('height', 0)): ?>
                MagicScroll.extraOptions.MagicToolboxSelectors<?php echo $pid?>.height = <?php echo $hm?>;
                <?php endif?>
            </script>
        <?php endif?>
    </div>
    <div style="clear:both"></div>
    <?php if(isset($hotspots)):?>
    <div>
        <?php echo $hotspots?>
    </div>
    <?php endif?>
</div>

<?php
    if(count($thumbs) > 4) {
        $bxEnable = 1;
    } else {
        $bxEnable = 0;
    }
?>

<script>
    jQuery(document).ready(function(){
        setTimeout(function(){
            if(<?php echo $bxEnable; ?> == 1) {
                jQuery('.bxslider').bxSlider({
                    mode: 'vertical',
                    slideWidth: 105,
                    slideHeight: 105,
                    slideMargin: 8,
                    minSlides: 4,
                    maxSlides: 4,
                    moveSlides: 1,
                    pager: false,
                    nextSelector: '#arrow-foward',
                    prevSelector: '#arrow-back',
                    nextText: '<img src="<?php echo Mage::getModel('core/design_package')->getSkinUrl('images/small-image-down.png'); ?>">',
                    prevText: '<img src="<?php echo Mage::getModel('core/design_package')->getSkinUrl('images/small-image-up.png'); ?>">'
                });
            }

            jQuery('.MagicThumb-swap').click(function() {
                jQuery('.MagicThumb-swap').removeClass('bordered');
                jQuery(this).addClass('bordered');
            });
        }, 1000);

        
    });
</script>
<!-- End magiczoomplus -->
