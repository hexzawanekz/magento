<?php
$m = intval(self::$options->getValue('selectors-margin'));
$wm = intval(self::$options->getValue('thumb-max-width'));
?>

<div class="product-img-box-wrapper">
    <!-- Begin magiczoomplus -->
    <div class="MagicToolboxContainer" style="width: <?php echo $wm?>px;">
        <?php  echo $main; ?>

        <?php if(isset($message)):?>
            <div class="MagicToolboxMessage"><?php echo $message?></div>
        <?php endif?>
    </div>
</div>


<div id="MagicToolboxSelectors<?php echo $pid?>" class="more-views MagicToolboxSelectorsContainer" style="margin-top: <?php echo $m;?>px">
    <h4><?php echo $moreViewsCaption ?></h4>
    <div class="slide-pic">
        <a class="prev" href="javascript:void(0);" style="display: block;">
            <img src="<?php echo Mage::getDesign()->getSkinUrl('images/prev.png') ?>"/>
        </a>
        <ul class="my-slider" style="height:75px">
        <?php foreach($thumbs as $thumb):?>
            <li><?php echo $thumb?></li>
        <?php endforeach?>
        </ul>
        <a class="next" href="javascript:void(0);" style="display: block;">
            <img src="<?php echo Mage::getDesign()->getSkinUrl('images/next.png') ?>"/>
        </a>
    </div>
    <div class="clear"></div>
</div>

<!-- End  -->

<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery('.my-slider').carouFredSel({
            scroll: {
                items: 1,
                effect: 'easeOutBounce',
                speed: 2500,
                pauseOnHover: false
            },
            width: "70%",
            auto: false,
            responsive: false,
            items: 5,
            next: "a.next",
            prev: "a.prev"
        });
    });
</script>
