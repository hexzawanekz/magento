<?php
/**
 * NOTICE OF LICENSE
 *
 * You may not give, sell, distribute, sub-license, rent, lease or lend
 * any portion of the Software or Documentation to anyone.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade to newer
 * versions in the future.
 *
 * @category   ET
 * @package    ET_ExtraChargeForCartRules
 * @copyright  Copyright (c) 2012 ET Web Solutions (http://etwebsolutions.com)
 * @contacts   support@etwebsolutions.com
 * @license    http://shop.etwebsolutions.com/etws-license-commercial-v1/   ETWS Commercial License (ECL1)
 */
class ET_ExtraChargeForCartRules_Model_Quote_Discount extends Rewardpoints_Model_Total_Points
{
    /**
     * Add discount total information to address
     *
     * @param   Mage_Sales_Model_Quote_Address $address
     * @return  Mage_SalesRule_Model_Quote_Discount
     */
    public function fetch(Mage_Sales_Model_Quote_Address $address)
    {
        /** @var $moduleHelper ET_ExtraChargeForCartRules_Helper_Data */
        $moduleHelper = Mage::helper('etextrachargeforcartrules');
        if ($moduleHelper->isActive()) {
            $amount = $address->getDiscountAmount();

            if ($amount != 0) {
                $description = $address->getDiscountDescription();
                if ($description) {
                    if ($amount < 0) {
                        $title = Mage::helper('sales')->__('Discount (%s)', $description);
                    } else {
                        $title = Mage::helper('etextrachargeforcartrules')->__('Extra charge (%s)', $description);
                    }
                } else {
                    if ($amount < 0) {
                        $title = Mage::helper('sales')->__('Discount');
                    } else {
                        $title = Mage::helper('etextrachargeforcartrules')->__('Extra charge');
                    }

                }
                $address->addTotal(array(
                    'code' => $this->getCode(),
                    'title' => $title,
                    'value' => $amount
                ));
            }
            return $this;
        } else {
            return parent::fetch($address);
        }

    }


}
