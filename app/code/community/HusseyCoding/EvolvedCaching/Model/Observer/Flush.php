<?php
class HusseyCoding_EvolvedCaching_Model_Observer_Flush
{
    public function adminhtmlControllerActionPostdispatchAdminhtmlCacheMassRefresh($observer)
    {
        $types = Mage::app()->getRequest()->getPost('types');
        if (in_array('evolved', $types)):
            $this->_clearCache();
        endif;
    }
    
    public function adminhtmlControllerActionPostdispatchAdminhtmlCacheFlushSystem($observer)
    {
        if (!Mage::getStoreConfig('evolvedcaching/lock/enabled')):
            $this->_clearCache();
        else:
            Mage::getSingleton('adminhtml/session')->addNotice('The full page cache is locked and has not been cleaned.');
        endif;
    }
    
    public function adminhtmlControllerActionPostdispatchAdminhtmlCacheFlushAll($observer)
    {
        $this->_clearCache();
        if (Mage::getStoreConfig('evolvedcaching/lock/enabled')):
            Mage::getSingleton('adminhtml/session')->addSuccess('The full page cache has been cleaned.');
        endif;
    }
    
    private function _clearCache()
    {
        $helper = Mage::helper('evolvedcaching/entries');
        $cachedir = @class_exists('evolved') ? EVOLVED_ROOT : MAGENTO_ROOT;
        if ($cachedir):
            $cachedir .= DS . 'var' . DS . 'evolved' . DS;

            if (is_dir($cachedir)):
                $pages = 'page_*';
                foreach (glob($cachedir . $pages) as $file):
                    @unlink($file);
                endforeach;

                $lifetime = array();
                foreach (Mage::app()->getStores() as $store):
                    $lifetime[] = (int) Mage::getStoreConfig('web/cookie/cookie_lifetime', $store->getCode());
                endforeach;
                $lifetime = max($lifetime) ? max($lifetime) : 86400;
                $users = 'user_*';
                foreach (glob($cachedir . $users) as $file):
                    $time = time();
                    $modified = filemtime($file);
                    if ($time > ($modified + $lifetime)):
                        @unlink($file);
                    endif;
                endforeach;
                
                $helper->clearEntriesOfType('files');
            endif;
        endif;
        
        if (Mage::getStoreConfig('evolvedcaching/storage/use') == '1' && @extension_loaded('memcache') && @class_exists('Memcache')):
            $host = Mage::getStoreConfig('evolvedcaching/storage/host');
            $port = (int) Mage::getStoreConfig('evolvedcaching/storage/port');
            $persistent = Mage::getStoreConfig('evolvedcaching/storage/persistent') ? true : false;
            if ($host && $port):
                $memcached = new Memcache;
                if ($persistent):
                    $result = $memcached->pconnect($host, $port);
                else:
                    $result = $memcached->connect($host, $port);
                endif;
                if ($result):
                    $memcached->flush();
                    $helper->clearEntriesOfType('memcached');
                endif;
            endif;
        elseif (Mage::getStoreConfig('evolvedcaching/storage/use') == '2' && @extension_loaded('apc') && @ini_get('apc.enabled')):
            apc_clear_cache('user');
            $helper->clearEntriesOfType('apc');
        endif;
        
        Mage::helper('evolvedcaching/varnish')->clearAllHtml();
    }
    
    public function adminhtmlCleanCatalogImagesCacheAfter($observer)
    {
        Mage::helper('evolvedcaching/varnish')->clearAllImages();
    }
    
    public function adminhtmlCleanMediaCacheAfter($observer)
    {
        Mage::helper('evolvedcaching/varnish')->clearAllCssJs();
    }
}