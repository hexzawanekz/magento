<?php
class HusseyCoding_EvolvedCaching_Model_Mysql4_Entries_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    private $_shouldValidate = false;
    
    public function _construct()
    {
        parent::_construct();
        $this->_init('evolvedcaching/entries');
    }
    
    public function setShouldValidate()
    {
        $this->_shouldValidate = true;
        
        return $this;
    }
    
    public function getShouldValidate()
    {
        return $this->_shouldValidate;
    }
}