<?php
class HusseyCoding_EvolvedCaching_Model_Warming extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('evolvedcaching/warming');
    }
    
    public function warmCache()
    {
        Mage::helper('evolvedcaching/entries')->warmCache();
    }
}