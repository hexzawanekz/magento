<?php
class HusseyCoding_EvolvedCaching_Block_Admin extends Mage_Core_Block_Template
{
    public function isEnabled()
    {
        $params = Mage::app()->getRequest()->getParams();
        $status = false;
        $config = Mage::getStoreConfig(Mage::helper('evolvedcaching')->getSetupPath());
        $key = substr($config, -7);
        $config = substr($config, 0, -7);
        $details = '';
        $config = base64_decode($config);
        for ($i = 0; $i < strlen($config); $i++):
            $c = ord(substr($config, $i));
            $c -= ord(substr($key, (($i + 1) % strlen($key))));
            $details .= chr(abs($c) & 0xFF);
        endfor;

        $details = explode(',', $details);
        $host = Mage::app()->getRequest()->getHttpHost();
        foreach ($details as $detail):
            if ($detail == $host):
                $status = true;
            endif;
        endforeach;
        if (Mage::app()->useCache('evolved') && @class_exists('evolved') && !isset($params['disabled']) && !isset($params['evolvedforward']) && $status):
            return true;
        endif;
        
        return false;
    }
}