<?php
/**
 * @category    Bubble
 * @package     Bubble_Elasticsearch
 * @version     3.1.3
 * @copyright   Copyright (c) 2015 BubbleShop (https://www.bubbleshop.net)
 */
class Bubble_Elasticsearch_Helper_Indexer_Category extends Bubble_Elasticsearch_Helper_Indexer_Abstract
{
    /**
     * Searchable attributes
     *
     * @var array
     */
    protected $_searchableAttributes;

    /**
     * Export categories according to optional filters
     *
     * @param array $filters
     * @return array
     */
    public function export($filters = array())
    {
        $result = array();

        foreach (Mage::app()->getStores() as $store) {
            /** @var $store Mage_Core_Model_Store */
            if (!$store->getIsActive()) {
                continue;
            }

            $storeId = (int) $store->getId();

            if (isset($filters['store_id'])) {
                if (!is_array($filters['store_id'])) {
                    $filters['store_id'] = array($filters['store_id']);
                }
                if (!in_array($storeId, $filters['store_id'])) {
                    continue;
                }
            }

            $this->handleMessage(' > Exporting categories of store %s', $store->getCode());

            $result[$storeId] = array();

            /** @var Mage_Catalog_Model_Resource_Category_Collection $collection */
            $attributesConfig = $this->getSearchableAttributesConfig('category', $store);
            $collection = Mage::getModel('catalog/category')->getCollection();
            $collection->setStoreId($store->getId());
            $rootCategoryId = $store->getRootCategoryId();
            $collection->addIsActiveFilter()
                ->addAttributeToSelect($attributesConfig)
                ->addAttributeToFilter('path', array('like' => "1/{$rootCategoryId}/%"));

            foreach ($collection as $category) {
                $result[$storeId][$category->getId()] = array_merge(
                    array('id' => $category->getId()),
                    $category->toArray($attributesConfig)
                );
            }
        }

        return $result;
    }

    /**
     * Retrieves all searchable category attributes
     * Possibility to filter attributes by backend type
     *
     * @param mixed $store
     * @return array
     */
    public function getSearchableAttributes($store = null)
    {
        if (null === $this->_searchableAttributes) {
            $this->_searchableAttributes = array();

            $entityType = $this->getEavConfig()->getEntityType('catalog_category');
            $entity = $entityType->getEntity();

            /* @var Mage_Catalog_Model_Resource_Category_Attribute_Collection $categoryAttributeCollection */
            $categoryAttributeCollection = Mage::getResourceModel('catalog/category_attribute_collection')
                ->setEntityTypeFilter($entityType->getEntityTypeId())
                ->addFieldToFilter('attribute_code', array(
                    'in' => $this->getSearchableAttributesConfig('category', $store)
                ));

            $attributes = $categoryAttributeCollection->getItems();
            foreach ($attributes as $attribute) {
                /** @var $attribute Mage_Catalog_Model_Resource_Eav_Attribute */
                $attribute->setEntity($entity);
                $this->_searchableAttributes[$attribute->getAttributeCode()] = $attribute;
            }
        }

        return $this->_searchableAttributes;
    }

    /**
     * Builds store index properties for indexation
     *
     * @param mixed $store
     * @return array
     */
    public function getStoreIndexProperties($store = null)
    {
        $store = Mage::app()->getStore($store);
        $cacheId = 'elasticsearch_category_index_properties_' . $store->getId();
        if (Mage::app()->useCache('config')) {
            $properties = Mage::app()->loadCache($cacheId);
            if ($properties) {
                return unserialize($properties);
            }
        }

        $properties = array();

        $attributes = $this->getSearchableAttributes($store);
        foreach ($attributes as $attribute) {
            /** @var Mage_Catalog_Model_Resource_Eav_Attribute $attribute */
            $key = $attribute->getAttributeCode();
            $attribute->setIsSearchable(true);
            $properties[$key] = $this->getAttributeProperties($attribute, $store);
        }

        $properties = new Varien_Object($properties);

        Mage::dispatchEvent('bubble_elasticsearch_index_properties', array(
            'indexer' => $this,
            'store' => $store,
            'properties' => $properties,
        ));

        $properties = $properties->getData();

        if (Mage::app()->useCache('config')) {
            $lifetime = $this->getCacheLifetime();
            Mage::app()->saveCache(serialize($properties), $cacheId, array('config'), $lifetime);
        }

        return $properties;
    }
}