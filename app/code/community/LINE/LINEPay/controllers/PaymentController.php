<?php

/**
 * LINE Pay
 * PaymentController
 *
 * @category    LINE
 * @package     LINEPay
 * @copyright   Copyright (c) 2015 LINEPay
 */
class LINE_LINEPay_PaymentController extends Mage_Core_Controller_Front_Action {
	
	/**
	 * entranceAction에서 사용할 수 있는 type들을 정의
	 * @var array
	 */
	private static $SUPPORTED_TYPES = array(
			LINE_LINEPay_Helper_Data::REQUEST_TYPE_CONFIRM,
			LINE_LINEPay_Helper_Data::REQUEST_TYPE_CANCEL,
	);
	
	/**
	 * Reserve API 결과로 받아온 payment-web url로 이동
	 */
	public function redirectAction() {
		$helper = Mage::helper('linepay');
		$order	= $helper->getSalesOrder();
		
		try {
			// 실패시
			if (empty($order)) {
				Mage::throwException($helper->__('Unable to make payment due to a temporary error. Please try again.'));
			}
			
			$paymentUrl = $order->getLinepayRedirectPaymentUrl();
			if (empty($paymentUrl)) {
					
				$payment = $order->getPayment();
				$paymentMethod = $payment->getMethodInstance();
				$paymentMethod->cancel($payment);
					
				Mage::throwException($helper->__('Unable to make payment due to a temporary error. Please try again.'));
			}
			
			$this->_redirectUrl($paymentUrl);
		}
		catch (Exception $e) {
			$helper->addErrorInCheckoutSession($e->getMessage());
			$this->_redirectUrl(Mage::getUrl(LINE_LINEPay_Helper_Data::REDIRECT_URI_TYPE_CANCEL));
		}
		
	}
	
	/**
	 * Reserve API 호출 이후 Callback을 받음
	 * 
	 * 예외발생
	 * 1. 처리가능한 type이 아닌경우
	 * 2. reserved transaction id가 없는 경우
	 * 3. 결제가 취소된 경우 (cancel 요청)
	 * 4. confirm 요청되었으나 OrderState가 Pending_Payment 상태가 아닌 경우
	 * 
	 * Redirect
	 * - 성공: 주문성공 페이지
	 * - 실패: 카트 페이지
	 */
	public function entranceAction() {
		$helper = Mage::helper('linepay');
		$order = $helper->getSalesOrder();
		
		/**
		 * @var string $type LINE_LINEPay_Helper_Data::REQUEST_TYPE_CONFIRM|REQUEST_TYPE_CANCEL
		 */
		$type = $this->getRequest()->getParam('type');
		
		$payment = $order->getPayment();
		$paymentMethod = $payment->getMethodInstance();
		
		try {
			$reservedTransactionId = $payment->getLastTransId();
			
			// 1. 지원불가 타입 && tnxId 없는 경우
			if (!in_array($type, self::$SUPPORTED_TYPES) || empty($reservedTransactionId)) {
				$helper->getLogger()->log('PaymentController.entranceAction',
						sprintf('[request type: %s][reserved tnx id: %s] - %s',
								$type, $reservedTransactionId, $helper->__('You have not entered all required payment info.')));
				
				Mage::throwException($helper->__('Payment canceled.') . ' ' . $helper->__('Please try again.'));
			}
			
			// 2. 취소시
			if ($type == LINE_LINEPay_Helper_Data::REQUEST_TYPE_CANCEL) {
				
				// reserved transaction이 있는 경우 취소처리
				if (!empty($reservedTransactionId)) {
					$paymentMethod->cancel($payment);
				}
				
				Mage::throwException($helper->__('Payment canceled.'));
			}
			
			// 3. confirm 타입이지만 reserved 되지 않은 경우
			if ($type == LINE_LINEPay_Helper_Data::REQUEST_TYPE_CONFIRM && ($order->getState() != Mage_Sales_Model_Order::STATE_PENDING_PAYMENT)) {
				$msg = $helper->__('You have no orders to proceed with payment for.') . ' ' . $helper->__('Please try again.');
				$helper->getLogger()->log('PaymentController.entranceAction',
						sprintf('[request type: %s][order state: %s] - %s', $type, $order->getState(), $msg));
				
				Mage::throwException($msg);
			}
			
			// 매입처리
			$paymentMethod->capture($payment, $order->getBaseTotalDue(), $type, $reservedTransactionId);
			
			$this->_redirectUrl(Mage::getUrl(LINE_LINEPay_Helper_Data::REDIRECT_URI_TYPE_CONFIRM));
		}
		catch (Exception $e) {
			$helper->addErrorInCheckoutSession($e->getMessage());
			
			$this->_redirectUrl(Mage::getUrl(LINE_LINEPay_Helper_Data::REDIRECT_URI_TYPE_CANCEL));
		}
		
	}
	
}