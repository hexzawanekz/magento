<?php

/**
 * LINE Pay
 * Model_Log
 *
 * @category    LINE
 * @package     LINEPay
 * @copyright   Copyright (c) 2015 LINEPay
 */
class LINE_LINEPay_Model_Log {
	
	const LOG_FILE_NAME = 'linepay.log';
	
	/**
	 * debug모드가 설정되어있고, $message가 존재할 경우
	 * ERR 레벨로 로그를 기록
	 * 
	 * @param	string $message
	 */
	public function log($title = '', $message = '') {
		
		$helper = Mage::helper('linepay');
		if (!$helper->isDebugEnabled()) {
			return;
		}
		
		Mage::log('[' . $title . '] ' . $message, Zend_Log::ERR, self::LOG_FILE_NAME);
	}
	
}