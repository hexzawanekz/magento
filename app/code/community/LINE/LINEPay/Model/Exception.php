<?php

/**
 * LINE Pay
 * Model_Exception
 *
 * @category    LINE
 * @package     LINEPay
 * @copyright   Copyright (c) 2015 LINEPay
 */
class LINE_LINEPay_Model_Exception extends Mage_Core_Exception {

	public function getCodeMsg() {

		if (empty($this->code)) {
			return '';
		}
		else {
			return '[' . $this->code . ']';
		}

	}
	
}