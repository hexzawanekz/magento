<?php

/**
 * LINE Pay
 * Model_Config
 *
 * @category    LINE
 * @package     LINEPay
 * @copyright   Copyright (c) 2015 LINEPay
 */
class LINE_LINEPay_Model_Config {
	
	const CONFIG_LINEPAY_NAME	= 'LINE Pay';
	
	const CONFIG_PACKAGE_NAME	= 'payment/linepay/';
	const CONFIG_PREFIX_SANDBOX	= 'sandbox_';
	
	const CONFIG_XML_PATH_SANDBOX_MODE			= 'sandbox_mode';
	const CONFIG_XML_PATH_CHANNEL_ID			= 'channel_id';
	const CONFIG_XML_PATH_CHANNEL_SECRET_KEY	= 'channel_secret_key';
	const CONFIG_XML_PATH_PAYMENT_ACTION		= 'payment_action';
	const CONFIG_XML_PATH_DEBUG_MODE			= 'debug_mode';
	
	/**
	 * 기록된 설정정보를 반환
	 * 
	 * @param	string $value
	 * @return	string
	 */
	protected function _getConfigData($value) {
		return Mage::getStoreConfig(self::CONFIG_PACKAGE_NAME . $value, Mage::app()->getStore());
	}

	/**
	 * sandbox 설정여부 반환
	 * 
	 * @return	boolean
	 */
	public function isSandboxEnabled() {
		
		return (bool) $this->_getConfigData(self::CONFIG_XML_PATH_SANDBOX_MODE);
	}
	
	/**
	 * debug 설정여부 반환
	 * 
	 * @return	boolean
	 */
	public function isDebugEnabled() {
	
		return (bool) $this->_getConfigData(self::CONFIG_XML_PATH_DEBUG_MODE);
	}
	
	/**
	 * Raal 채널정보 반환
	 * 
	 * @return	array
	 */
	public function getRealChannelInfo() {
		
		return array(
				'id' => $this->_getConfigData(self::CONFIG_XML_PATH_CHANNEL_ID),
				'secretKey' => $this->_getConfigData(self::CONFIG_XML_PATH_CHANNEL_SECRET_KEY)
		);
	}
	
	/**
	 * Sandbox 채널정보 반환
	 * 
	 * @return	array
	 */
	public function getSandboxChannelInfo() {
		
		return array(
				'id' => $this->_getConfigData(self::CONFIG_PREFIX_SANDBOX . self::CONFIG_XML_PATH_CHANNEL_ID),
				'secretKey' => $this->_getConfigData(self::CONFIG_PREFIX_SANDBOX . self::CONFIG_XML_PATH_CHANNEL_SECRET_KEY)
		);
	}
	
	/**
	 * payment action 정보 반환
	 * 
	 * @return	string
	 */
	public function getPaymentAction() {
		
		return $this->_getConfigData(self::CONFIG_XML_PATH_PAYMENT_ACTION);
	}
	
   
}
