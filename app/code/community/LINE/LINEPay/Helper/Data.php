<?php

/**
 * LINE Pay
 * Helper_Data
 *
 * @category    LINE
 * @package     LINEPay
 * @copyright   Copyright (c) 2015 LINEPay
 */
class LINE_LINEPay_Helper_Data extends Mage_Core_Helper_Abstract {
	
	const DATA_HOST_REAL	= 'https://api-pay.line.me';
	const DATA_HOST_SANDBOX	= 'https://sandbox-api-pay.line.me';
	
	const DATA_LINEPAY_URI_RESERVE	= '/v2/payments/request';
	const DATA_LINEPAY_URI_CONFIRM	= '/v2/payments/{transactionId}/confirm';
	const DATA_LINEPAY_URI_REFUND	= '/v2/payments/{transactionId}/refund';
	
	const REQUEST_TYPE_FAILURE	= 'failure';
	const REQUEST_TYPE_CONFIRM	= 'confirm';
	const REQUEST_TYPE_CAPTURE	= 'capture';
	const REQUEST_TYPE_CANCEL	= 'cancel';
	const REQUEST_TYPE_REDIRECT	= 'redirect';
	const REQUEST_TYPE_RESERVE	= 'reserve';
	const REQUEST_TYPE_REFUND	= 'refund';
	
	const ENTRANCE_URI	= 'linepay/payment/entrance';
	const REDIRECT_URI	= 'linepay/payment/redirect';

	const REDIRECT_URI_TYPE_CONFIRM	= 'checkout/onepage/success';
	const REDIRECT_URI_TYPE_CANCEL	= 'checkout/cart';
	
	/**
	 * 사용가능한 화폐
	 * 
	 * @var array
	 */
	private static $LINEPAY_SUPPORTED_CURRENCIES = array('TWD', 'THB', 'USD', 'JPY');
	
	/**
	 * 화폐별 Scale 정보
	 * 
	 * @var array
	 */
	private static $LINEPAY_CURRENCY_SCALE = array(
			'THB' => 2,
			'TWD' => 0,
			'USD' => 2,
			'JPY' => 0,
			'KRW' => 0
	);
	
	
	/**
	 * LINEPay Api 모델 반환
	 * 
	 * @return	LINE_LINEPay_Model_Api
	 */
	public function getAPI() {
		
		return Mage::getSingleton('linepay/api');
	}
	
	/**
	 * LINEPay Config 모델 반환
	 * 
	 * @return	LINE_LINEPay_Model_Config
	 */
	public function getConfig() {
		
		return Mage::getSingleton('linepay/config');
	}
	
	/**
	 * LINEPay Log 모델 반환
	 * 
	 * @return	LINE_LINEPay_Model_Log
	 */
	public function getLogger() {
		
		return Mage::getSingleton('linepay/log');
	}
	
	
	/**
	 * LINEPay service/Creditmemo 모델 반환
	 * 
	 * @return	LINE_LINEPay_Model_Service_Creditmemo
	 */
	public function getCreditmemoService() {
		
		return Mage::getSingleton('linepay/service_creditmemo');
	}
	
	
	/**
	 * LINEPay service/Invoice 모델 반환
	 * 
	 * @return	LINE_LINEPay_Model_Service_Invoice
	 */
	public function getInvoiceService() {
		
		return Mage::getSingleton('linepay/service_invoice');
	}
	
	/**
	 * Magento checkout/session 모델 반환
	 * 
	 * @return	Mage_Checkout_Model_Session
	 */
	public function getCheckoutSession() {
		
		return Mage::getSingleton('checkout/session');
	}

	
	/**
	 * 메시지가 존재할 경우 session에 error로 기록
	 * 
	 * @param string $message
	 */
	public function addErrorInCheckoutSession($message) {
		
		if (empty($message)) {
			
			return;
		}
		
		$this->getCheckoutSession()->addError($message);
	}
	
	/**
	 * orderId를 사용하여 Order 모델을 만들고 반환
	 * orderId가 없는 경우 session에 저장된 값 확인
	 * 
	 * @param	string $orderId
	 * @return	Mage_Sales_Model_Order|null
	 */
	public function getSalesOrder($orderId = null) {
		
		if (empty($orderId)) {
			$orderId = $this->getCheckoutSession()->getLastRealOrderId();
			
			if (empty($orderId)) {
				
				return null;
			}
		}
		
		return Mage::getModel('sales/order')->loadByIncrementId($orderId);
	}
	
	/**
	 * quoteId를 사용하여 Order 모델을 만들고 반환
	 * quoteId가 없는 경우 Session에 저장된 Order를 찾아봄
	 *
	 * @param	string $quoteId
	 * @return	Mage_Sales_Model_Quote|null
	 */
	public function getSalesQuote($quoteId = null) {
	
		if (empty($quoteId)) {
			$order = $this->getSalesOrder();
			
			if (empty($order)) {
				
				return null;
			}
			
			$quoteId = $order->getQuoteId();
			
		}
		
		return Mage::getModel('sales/quote')->load($quoteId, 'entity_id');
	}
	
	/**
	 * Cart 정보를 복귀
	 * 
	 * @param	string $quoteId
	 * @return	Mage_Sales_Model_Quote|null
	 */
	public function restoreCart($quoteId = null) {
		$quote = $this->getSalesQuote($quoteId);
		
		if (empty($quote)) {
			
			return null;
		}
		
		$quote->setIsActive(true);
	
		return $quote;
	}
		
	/**
	 * 환경정보(Real/Sandbox)를 확인 후
	 * 환경에 맞는 Channel 정보를 반환
	 *  - id
	 *  - secretKey
	 * 
	 * @return	array
	 */
	public function getChannelInfo() {
		$config = $this->getConfig();
		if ($config->isSandboxEnabled()) {
			
			return $config->getSandboxChannelInfo();
		}
		else {
			
			return $config->getRealChannelInfo();
		}
	}
	
	/**
	 * debug 설정여부 반환
	 * 
	 * @return	 boolean
	 */
	public function isDebugEnabled() {
		
		return $this->getConfig()->isDebugEnabled();
	}
	
	/**
	 * payment_action 정보 반환
	 * 
	 * @return	string
	 */
	public function getPaymentAction() {
		
		return $this->getConfig()->getPaymentAction();
	}
	
	/**
	 * 환경설정(Real/Sandbox) 정보에 맞는 host 정보 반환
	 * 
	 * @return	string self::DATA_HOST_REAL|DATA_HOST_SANDBOX
	 */
	public function getHost() {
		$host = '';
		if ($this->getConfig()->isSandboxEnabled()) {
			$host = self::DATA_HOST_SANDBOX;
		}
		else {
			$host = self::DATA_HOST_REAL;
		}
	
		return $host;
	}
	
	
	/**
	 * $type에 맞는 URI를 반환
	 * URI template에 $info에 포함된 정보가 있는 경우
	 * $info의 값과 조합된 새로운 URI를 반환
	 * 
	 * @param	string $type	self::REQUEST_TYPE_RESERVE|REQUEST_TYPE_CONFIRM|REQUEST_TYPE_REFUND
	 * @param	array $info		
	 * @return	string
	 */
	public function getURI($type, $info=array()) {
		$uri = '';
		
		switch ($type) {
			case self::REQUEST_TYPE_RESERVE :
				$uri = self::DATA_LINEPAY_URI_RESERVE;
				break;
			
			case self::REQUEST_TYPE_CONFIRM :
				$uri = self::DATA_LINEPAY_URI_CONFIRM;
				break;
				
			case self::REQUEST_TYPE_REFUND:
				$uri = self::DATA_LINEPAY_URI_REFUND;
				break;
		}
		
		$newUri = $uri;
		foreach ($info as $key => $value) {
			$newUri = str_replace('{'.$key.'}', $value, $newUri);
		}
		
		return $newUri;
		
	}
	
	/**
	 * order 정보를 바탕으로 reserve-api에 전달할 array를 반환한다.
	 * array에는 productName, productImageUrl를 담는다.
	 * 
	 * productName
	 * 	- 1개		: 첫번째 아이템 이름
	 * 	- 2개 이상	: 첫번째 아이템 이름 + 나머지 아이템 개수
	 * 
	 * productImageUrl
	 *  - 첫번째 아이템의 URL 정보
	 *
	 * @param	Mage_Sales_Model_Order $order
	 * @return	array
	 */
	public function getProductInfo($order) {
		$productInfo = array();
		$items = $order->getAllVisibleItems();
		$flag = false;
		$actualAmount = 0;
	
		if (sizeof($items) > 0) {
			foreach ($items as $item) {
				$qty = $item->getQtyOrdered();
				if ($qty) {
	
					if (!$flag) {
						$product = $item->getProduct();
						$productInfo['productName'] = $product->getName();
						$productInfo['productImageUrl'] = Mage::getModel('catalog/product_media_config')->getMediaUrl($product->getThumbnail());
						$flag = true;
					}
	
					$actualAmount += $qty;
				}
			}
		}
	
		if ( $actualAmount > 1) {
			$actualFirstProductName = $productInfo['productName'];
			$actualExtraAmount = $actualAmount - 1;
	
			$productInfo['productName'] = $actualFirstProductName . ' +' . $actualExtraAmount;
		}
	
		return $productInfo;
	}
	
	/**
	 * 마젠토에 설정된 기본 화폐코드 반환
	 * 
	 * @return	string
	 */
	public function getBaseCurrencyCode() {
		
		return Mage::app()->getStore()->getBaseCurrencyCode();
	}
	
	/**
	 * 전달 받은 $amount의 소수점 정확도를 반환
	 * 
	 * @param	number $amount
	 * @return	number
	 */
	public function getAmountPrecision($amount = 0) {
		
		if (is_string($amount)) {
			$amount = (float) $amount;
		}
		$strl = strlen($amount);
		
		$strp = strpos($amount, '.');
		$strp = ($strp !== false)?$strp+1:$strl;
		
		return ($strl - $strp);
	}
	
	/**
	 * 전달받은 화폐코드의 Scale을 반환
	 * 전달받은 정보가 없는 경우 BaseCurrencyCode를 사용
	 * 
	 * @param	string $currencyCode
	 * @return	number
	 */
	public function getCurrencyScale($currencyCode = null) {
		
		if ($currencyCode == null) {
			$currencyCode = $this->getBaseCurrencyCode();
		}
		
		if (in_array(strtoupper($currencyCode), self::$LINEPAY_CURRENCY_SCALE)) {
			
			return self::$LINEPAY_CURRENCY_SCALE[strtoupper($currencyCode)];
		}
		// 설정되지 않은 통화의 스케일은 0으로 설정
		else {
			
			return 0;
		}
		
	}
	
	/**
	 * 해당 화폐에 맞는 number_format을 반환
	 *
	 * @param	number|string $amount
	 * @param	string $currency
	 * @return	string
	 */
	public function getStandardized($amount, $currency = null) {
		$scale = $this->getCurrencyScale($currency);
	
		if (is_string($amount)) {
			$amount = floatval($amount);
		}
	
		return number_format($amount, $scale, '.', '');
	}
	
	/**
	 * 기본화페코드를 바탕으로 전달받은 $amount의 Scale이 적절한지 확인
	 * 
	 * @param	number $amount
	 * @return	boolean
	 */
	public function validCurrencyScale($amount) {

		return ($this->getCurrencyScale() >= $this->getAmountPrecision($amount));
	}
	
	/**
	 * 전달받은 화폐코드를 라인페이 플러그인에서 사용할 수 있는지 확인
	 * 전달받은 정보가 없는 경우 BaseCurrencyCode를 사용
	 * 
	 * @param	string $currentCurrencyCode
	 * @return	boolean
	 */
	public function validCurrencyCode($currentCurrencyCode = null) {
		
		if ($currentCurrencyCode == null) {
			$currentCurrencyCode = $this->getBaseCurrencyCode();
		}
		
		return in_array(strtoupper($currentCurrencyCode), self::$LINEPAY_SUPPORTED_CURRENCIES);
	}
	
	/**
	 * 설정된 channel 정보가 유요한지 확인
	 * 
	 * @param	array $channelInfo
	 * @return	boolean
	 */
	public function validChannelInfo($channelInfo = array()) {
		
		if (sizeof($channelInfo) != 2) {
			$channelInfo = $this->getChannelInfo();
		}
		
		return !(empty($channelInfo) || empty($channelInfo['id']) || empty($channelInfo['secretKey']));
		
	}
	
	/**
	 * 한 줄을 추가함
	 * 
	 * @return	string
	 */
	public function addNewLine($str = '') {
		
		return PHP_EOL . "\t" . '- ' . $str;
	}
	
}
