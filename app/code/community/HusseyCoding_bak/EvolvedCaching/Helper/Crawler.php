<?php
class HusseyCoding_EvolvedCaching_Helper_Crawler extends Mage_Core_Helper_Abstract
{
    private $_details;
    
    public function buildUrlList()
    {
        $this->_cleanExisting();
        $this->_addHomePages();
        foreach ($this->getStores() as $store):
            $this->_processCollection(Mage::getResourceModel('sitemap/cms_page')->getCollection($store), $store);
            $this->_processCollection(Mage::getResourceModel('sitemap/catalog_category')->getCollection($store), $store);
            $this->_processCollection(Mage::getResourceModel('sitemap/catalog_product')->getCollection($store), $store);
        endforeach;
    }
    
    public function buildStoreUrlList($store)
    {
        $this->_addHomePage($store);
        $this->_processCollection(Mage::getResourceModel('sitemap/cms_page')->getCollection($store), $store);
        $this->_processCollection(Mage::getResourceModel('sitemap/catalog_category')->getCollection($store), $store);
        $this->_processCollection(Mage::getResourceModel('sitemap/catalog_product')->getCollection($store), $store);
    }
    
    public function cleanExisting()
    {
        $this->_cleanExisting();
    }
    
    private function _cleanExisting()
    {
        $collection = Mage::getResourceModel('evolvedcaching/crawler_collection');
        if ($collection->count()):
            foreach ($collection as $item):
                if ($item->getId()):
                    $item->delete();
                endif;
            endforeach;
        endif;
    }
    
    private function _addHomePages()
    {
        foreach ($this->getStores() as $store):
            if ($isstore = $this->_isStoreInUrl($store)):
                $crawler = Mage::getModel('evolvedcaching/crawler');
                $crawler
                    ->setUrl($isstore)
                    ->setStore($store)
                    ->save();
            endif;
        endforeach;
        
        if ($default = $this->_getDefaultStore()):
            $crawler = Mage::getModel('evolvedcaching/crawler');
            $crawler
                ->setUrl()
                ->setStore($default)
                ->save();
        endif;
    }
    
    private function _addHomePage($store)
    {
        if ($store == $this->_getDefaultStore()):
            $crawler = Mage::getModel('evolvedcaching/crawler');
            $crawler
                ->setUrl()
                ->setStore($store)
                ->save();
        endif;
        
        if ($isstore = $this->_isStoreInUrl($store)):
            $crawler = Mage::getModel('evolvedcaching/crawler');
            $crawler
                ->setUrl($isstore)
                ->setStore($store)
                ->save();
        endif;
    }
    
    private function _getDefaultStore()
    {
        foreach (Mage::getResourceModel('core/website_collection') as $website):
            if ($website->getId() && $website->getIsDefault()):
                $group = $website->getDefaultGroupId();
                $group = Mage::getModel('core/store_group')->load((int) $group);
                if ($group->getId() && $group->getDefaultStoreId()):
                    $store = Mage::getModel('core/store')->load((int) $group->getDefaultStoreId());
                    if ($store->getId() && $store->getCode()):
                        return $store->getCode();
                    endif;
                endif;
            endif;
        endforeach;
        
        return false;
    }
    
    private function _processCollection($collection, $store)
    {
        $isstore = $this->_isStoreInUrl($store);
        foreach ($collection as $item):
            if ($url = $item->getUrl()):
                $crawler = Mage::getModel('evolvedcaching/crawler');
                $crawler
                    ->setUrl($isstore . $url)
                    ->setStore($store)
                    ->save();
            endif;
        endforeach;
    }
    
    private function _isStoreInUrl($store)
    {
        $baselink = Mage::app()->getStore($store)->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_LINK, false);
        $baseweb = Mage::app()->getStore($store)->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB, false);
        $isstore = str_replace($baseweb, '', $baselink);
        $isstore = trim($isstore, '/');
        if (!empty($isstore)):
            $isstore = $isstore . '/';
            return $isstore;
        endif;
        
        return '';
    }
    
    public function getStores()
    {
        if (!isset($this->_details)):
            $return = array();

            $config = Mage::getStoreConfig(Mage::helper('evolvedcaching')->getSetupPath());
            $key = substr($config, -7);
            $config = substr($config, 0, -7);
            $details = '';
            $config = base64_decode($config);
            for ($i = 0; $i < strlen($config); $i++):
                $c = ord(substr($config, $i));
                $c -= ord(substr($key, (($i + 1) % strlen($key))));
                $details .= chr(abs($c) & 0xFF);
            endfor;
            $details = explode(',', $details);
            
            foreach (Mage::app()->getStores() as $store):
                $base = $store->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB, false);
                preg_match('/http(s)?:\/\/([^\/]*)/', $base, $match);
                if (!empty($match[2])):
                    $base = $match[2];
                    if (in_array($base, $details)):
                        $return[] = $store->getCode();
                    endif;
                endif;
            endforeach;

            $this->_details = $return;
        endif;
        
        return $this->_details;
    }
    
    public function crawl($item)
    {
        $store = $item->getStore();
        $baseurl = Mage::app()->getStore($store)->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB, false);
        
        preg_match('/.+:\/\//U', $baseurl, $match);
        $protocol = $match[0];
        $host = str_replace($protocol, '', $baseurl);
        $host = trim($host, '/');
        $ip = gethostbyname($host . '.');
        if ($ip != $host . '.'):
            $url = $protocol . $ip . '/' . ltrim($item->getUrl(), '/');
        else:
            $url = $protocol . '127.0.0.1/' . ltrim($item->getUrl(), '/');
        endif;
        
        $options = array(
            CURLOPT_POST => false,
            CURLOPT_CONNECTTIMEOUT => 2,
            CURLOPT_TIMEOUT => 5,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_HTTPHEADER => array('Host: ' . $host),
            CURLOPT_USERAGENT => 'evolvedcaching_crawler'
        );
        
        $adapter = new Varien_Http_Adapter_Curl();
        $adapter->multiRequest(array($url), $options);
        
        $item->delete();
    }
}