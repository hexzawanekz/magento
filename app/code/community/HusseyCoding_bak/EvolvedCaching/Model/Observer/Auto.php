<?php
class HusseyCoding_EvolvedCaching_Model_Observer_Auto
{
    private $_cms;
    
    public function adminhtmlCatalogProductSaveAfter($observer)
    {
        if (!$this->_isDataflow()):
            if (Mage::getStoreConfig('evolvedcaching/autoclear/product')):
                $product = $observer->getProduct();
                Mage::helper('evolvedcaching/entries')->clearProductCache($product);
            endif;
        endif;
    }
    
    public function adminhtmlControllerActionPostdispatchAdminhtmlCatalogProductSave($observer)
    {
        if (!$this->_isDataflow()):
            $this->_clearBlockHtmlCache();
            Mage::helper('evolvedcaching/entries')->processWarmQueue();
        endif;
    }
    
    public function adminhtmlCatalogCategorySaveAfter($observer)
    {
        if (Mage::getStoreConfig('evolvedcaching/autoclear/category')):
            $category = $observer->getCategory();
            Mage::helper('evolvedcaching/entries')->clearCategoryCache($category);
        endif;
    }
    
    public function adminhtmlControllerActionPostdispatchAdminhtmlCatalogCategorySave($observer)
    {
        $this->_clearBlockHtmlCache();
        Mage::helper('evolvedcaching/entries')->processWarmQueue();
    }
    
    public function adminhtmlCmsPageSaveAfter($observer)
    {
        if (Mage::getStoreConfig('evolvedcaching/autoclear/cms')):
            $this->_cms = $observer->getObject();
            Mage::helper('evolvedcaching/entries')->clearCmsCache($this->_cms);
        endif;
    }
    
    public function adminhtmlControllerActionPostdispatchAdminhtmlCmsPageSave($observer)
    {
        $this->_clearBlockHtmlCache();
        Mage::helper('evolvedcaching/entries')->processWarmQueue($this->_cms);
    }
    
    private function _clearBlockHtmlCache()
    {
        if (Mage::getStoreConfig('evolvedcaching/autoclear/blocks')):
            Mage::helper('evolvedcaching/entries')->clearBlockHtmlCache();
        endif;
    }
    
    private function _isDataflow()
    {
        if (Mage::app()->getRequest()->getControllerName() == 'system_convert_gui') return true;
        
        return false;
    }
    
    public function adminhtmlCatalogProductDeleteBefore($observer)
    {
        Mage::helper('evolvedcaching/entries')->clearProductCache($observer->getProduct());
    }
    
    public function adminhtmlControllerActionPostdispatchAdminhtmlCatalogProductDelete($observer)
    {
        $this->adminhtmlControllerActionPostdispatchAdminhtmlCatalogProductSave($observer);
    }
    
    public function adminhtmlCmsPageDeleteBefore($observer)
    {
        $this->_cms = $observer->getObject();
        Mage::helper('evolvedcaching/entries')->clearCmsCache($this->_cms);
    }
    
    public function adminhtmlSalesOrderCreditmemoRefund($observer)
    {
        $this->_refreshItems($observer->getCreditmemo());
    }
    
    public function adminhtmlSalesOrderPlaceAfter($observer)
    {
        $this->_refreshItems($observer->getOrder());
    }
    
    public function adminhtmlOrderCancelAfter($observer)
    {
        $this->_refreshItems($observer->getOrder());
    }
    
    private function _refreshItems($object)
    {
        $items = $object->getAllItems();
        if (!empty($items) && is_array($items)):
            foreach ($items as $item):
                $product = Mage::getModel('catalog/product')->load($item->getProductId());
                if ($product->getId()):
                    Mage::helper('evolvedcaching/entries')->clearProductCache($product);
                endif;
            endforeach;
            
            if (Mage::getStoreConfig('evolvedcaching/autoclear/blocks')):
                Mage::helper('evolvedcaching/entries')->clearBlockHtmlCache();
            endif;
            Mage::helper('evolvedcaching/entries')->processWarmQueue();
        endif;
    }
    
    public function adminhtmlControllerActionPostdispatchAdminhtmlCatalogProductMassStatus($observer)
    {
        if (Mage::getStoreConfig('evolvedcaching/autoclear/product')):
            $products = Mage::app()->getRequest()->getPost('product');
            if (!empty($products) && is_array($products)):
                foreach ($products as $product):
                    $product = Mage::getModel('catalog/product')->load((int) $product);
                    if ($product->getId()):
                        Mage::helper('evolvedcaching/entries')->clearProductCache($product);
                    endif;
                endforeach;
            endif;
        endif;
        
        $this->_clearBlockHtmlCache();
        Mage::helper('evolvedcaching/entries')->processWarmQueue();
    }
}