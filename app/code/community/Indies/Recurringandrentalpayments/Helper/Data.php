<?php

/**
 *
 * Do not edit or add to this file if you wish to upgrade the module to newer
 * versions in the future. If you wish to customize the module for your
 * needs please contact us to https://www.milople.com/magento-extensions/contacts/
 *
 * @category     Ecommerce
 * @package      Indies_Recurringandrentalpayments
 * @copyright    Copyright (c) 2015 Milople Technologies Pvt. Ltd. All Rights Reserved.
 * @url          https://www.milople.com/magento-extensions/recurring-and-subscription-payments.html
 *
 * Milople was known as Indies Services earlier.
 *
 **/
class Indies_Recurringandrentalpayments_Helper_Data extends Mage_Core_Helper_Abstract
{

    public function checkoutAllowedForGuest()
    {
        if (Mage::getStoreConfig(Indies_Recurringandrentalpayments_Helper_Config::XML_PATH_GENERAL_ANONYMOUS_SUBSCRIPTIONS, Mage::app()->getStore()) == '1')
            return true;
        return false;
    }

    public function isOrderStatusValidForActivation($order)
    {
        if (
            ($order->hasInvoices() && Mage::getStoreConfig(Indies_Recurringandrentalpayments_Helper_Config::XML_PATH_ACTIVE_ORDER_STATUS, Mage::app()->getStore()) == "processing" && $order->getStatus() != 'closed' && $order->getStatus() != 'canceled') ||
            ($order->hasShipments() && Mage::getStoreConfig(Indies_Recurringandrentalpayments_Helper_Config::XML_PATH_ACTIVE_ORDER_STATUS, Mage::app()->getStore()) == "complete" && $order->getStatus() != 'closed' && $order->getStatus() != 'canceled') ||
            ($order->getIncrementId() && Mage::getStoreConfig(Indies_Recurringandrentalpayments_Helper_Config::XML_PATH_ACTIVE_ORDER_STATUS, Mage::app()->getStore()) == "pending" && $order->getStatus() != 'closed' && $order->getStatus() != 'canceled') ||
            ($order->getIncrementId() && $order->getStatus() == 'invoiced')
        ) {
            return true;
        }
        return false;
    }

    public static function isSubscriptionType($typeId)
    {

        if ($typeId instanceof Mage_Catalog_Model_Product) {
            $typeId = $typeId->getTypeId();
        } elseif (($typeId instanceof Mage_Sales_Model_Order_Item) || ($typeId instanceof Mage_Sales_Model_Quote_Item)) {
            $plans_product = Mage::getModel('recurringandrentalpayments/plans_product')->load($typeId->getId(), 'product_id');

            $additionaprice = Mage::getModel('recurringandrentalpayments/plans')->load($plans_product->getPlanId(), 'plan_id');

            if (Mage::getModel('recurringandrentalpayments/plans')->load($plans_product->getPlanId(), 'plan_id')) {
                $typeId = $typeId->getProductType();
                return true;
            }
        }
        return false;
    }

    public function isSubscriptionItemInvoiced(Indies_Recurringandrentalpayments_Model_Subscription_Item $item)
    {
        $invoiced = (float)$item->getOrderItem()->getQtyInvoiced();
        if ($invoiced)
            return true;
        return false;
    }

    public function getAllSubscriptionTypes()
    {
        $types = Mage::getModel('recurringandrentalpayments/terms')->getCollection();
        $out = array();
        foreach ($types as $type) {
            $out[] = $type->getLabel();
        }
        return $out;
    }

    public function getDomain()
    {
        $domain = $_SERVER['SERVER_NAME'];

        $temp = explode('.', $domain);
        $exceptions = array(
            'co.uk',
            'com.au',
            'com.hk',
            'co.nz',
            'co.in',
            'com.sg'
        );

        $count = count($temp);
        $last = $temp[($count - 2)] . '.' . $temp[($count - 1)];

        if (in_array($last, $exceptions)) {
            $new_domain = $temp[($count - 3)] . '.' . $temp[($count - 2)] . '.' . $temp[($count - 1)];
        } else {
            $new_domain = $temp[($count - 2)] . '.' . $temp[($count - 1)];
        }
        return $new_domain;
    }


    public function checkEntry($domain, $serial)
    {
        $key = sha1(base64_decode('UmVjdXJyaW5nQW5kU3Vic2NyaXB0aW9uUGF5bWVudHM='));
        if (sha1($key . $domain) == $serial) {
            return true;
        }
        return false;
    }

    public function getRnrSubscriptionPrice($product, $includeCatalogRule = false)
    {
        return Mage::getSingleton('recurringandrentalpayments/product_price')->getRnrSubscriptionPrice($product, $includeCatalogRule);
    }


    public function getRnrFirstPeriodPrice($product)
    {
        return Mage::getSingleton('recurringandrentalpayments/product_price')->getRnrFirstPeriodPrice($product);
    }

    public function canRun($temp = '')
    {
//		if($_SERVER['SERVER_NAME'] == "localhost" || $_SERVER['SERVER_NAME'] == "127.0.0.1")
//		{
//			return true;
//		}
//		if(!$temp)
//		{
//			$temp = Mage::getStoreConfig('recurringandrentalpayments/license_status_group/serial_key',Mage::app()->getStore());
//		}
//		$url = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB);
//		$parsedUrl = parse_url($url);
//		$host = explode('.', $parsedUrl['host']);
//		$subdomains = array_slice($host, 0, count($host) - 2 );
//
//		if(sizeof($subdomains) && ($subdomains[0] == 'test'|| $subdomains[0] == 'demo' || $subdomains[0] == 'dev'))
//		{
//			return true;
//		}
//		$original = $this->checkEntry($_SERVER['SERVER_NAME'], $temp);
//		$wildcard = $this->checkEntry($this->getDomain(), $temp);
//
//		if(!$original && !$wildcard)
//		{
//			return false;
//		}
//		return true;
        return true;
    }

    public function getMessage()
    {
        return base64_decode('PGRpdiBzdHlsZT0iYm9yZGVyOjNweCBzb2xpZCAjRkYwMDAwOyBtYXJnaW46MTVweCAwOyBwYWRkaW5nOjVweDsiPkxpY2Vuc2Ugb2YgPGI+UmVjdXJyaW5nIGFuZCBTdWJzY3JpcHRpb24gUGF5bWVudHM8L2I+IGV4dGVuc2lvbiBoYXMgYmVlbiB2aW9sYXRlZC4gVG8gZ2V0IHNlcmlhbCBrZXkgcGxlYXNlIGNvbnRhY3QgdXMgb24gPGI+aHR0cHM6Ly93d3cubWlsb3BsZS5jb20vbWFnZW50by1leHRlbnNpb25zL2NvbnRhY3RzLzwvYj48L2Rpdj4=');
    }

    public function getAdminMessage()
    {
        return $this->__(base64_decode('PGRpdj5MaWNlbnNlIG9mIDxiPk1pbG9wbGUgUmVjdXJyaW5nIGFuZCBTdWJzY3JpcHRpb24gUGF5bWVudHM8L2I+IGV4dGVuc2lvbiBoYXMgYmVlbiB2aW9sYXRlZC4gVG8gZ2V0IHNlcmlhbCBrZXkgcGxlYXNlIGNvbnRhY3QgdXMgb24gPGI+aHR0cHM6Ly93d3cubWlsb3BsZS5jb20vbWFnZW50by1leHRlbnNpb25zL2NvbnRhY3RzLzwvYj48L2Rpdj4='));
    }

    public function isEnabled()
    {
        if (Mage::getStoreConfig(Indies_Recurringandrentalpayments_Helper_Config::XML_PATH_MODULE_STATUS, Mage::app()->getStore()) == '1'
            && (!Mage::getStoreConfig('advanced/modules_disable_output/Indies_Recurringandrentalpayments', Mage::app()->getStore()))
        ) {
            return true;
        }
        return false;
    }

    public function getFilter($data)
    {
        $result = array();
        $filter = new Zend_Filter();
        $filter->addFilter(new Zend_Filter_StringTrim());

        if ($data) {
            foreach ($data as $key => $value) {
                if (is_array($value)) {
                    $result[$key] = $this->getFilter($value);
                } else {
                    $result[$key] = $filter->filter($value);
                }
            }
        }
        return $result;
    }

    public function isApplyDiscount()
    {
        if (Mage::getStoreConfig(Indies_Recurringandrentalpayments_Helper_Config::XML_PATH_APPLY_DISCOUNT, Mage::app()->getStore()) == '1')
            return true;
        return false;

    }

    public function discountAvailableTo()
    {
        return Mage::getStoreConfig('recurringandrentalpayments/discount_group/discount_available_to', Mage::app()->getStore());
    }

    public function selectedCustomerGroup()
    {
        return Mage::getStoreConfig('recurringandrentalpayments/discount_group/apply_discount_group', Mage::app()->getStore());
    }

    public function applyDiscountOn()
    {
        return Mage::getStoreConfig('recurringandrentalpayments/discount_group/apply_discount_on', Mage::app()->getStore());
    }

    public function applyDiscountType()
    {
        return Mage::getStoreConfig('recurringandrentalpayments/discount_group/discount_cal_type', Mage::app()->getStore());
    }

    public function discountAmount()
    {
        return Mage::getStoreConfig('recurringandrentalpayments/discount_group/discount_amount', Mage::app()->getStore());
    }

    public function convertDiscountAmount($amount)
    {
        $current_currency_code = Mage::app()->getStore()->getCurrentCurrencyCode();
        $base_currency_code = Mage::app()->getStore()->getBaseCurrencyCode();
        if ($current_currency_code != $base_currency_code) {
            $currencyRates = Mage::getModel('directory/currency')->getCurrencyRates($base_currency_code, $current_currency_code);
            $currentCurrencyRate = $currencyRates[$currentCurrencyCode];
            $amount = $amount / $currentCurrencyRate;

        }
        return $amount;
    }

    /* Check about valid to display terms and conditions link on cart page */
    public function displayTermsandConditions()
    {
        $enable = Mage::getStoreConfig('recurringandrentalpayments/clause_settings/enable');
        $clause_detail = Mage::getStoreConfig('recurringandrentalpayments/clause_settings/clause');
        $quote = Mage::getSingleton('checkout/session')->getQuote()->getAllVisibleItems();
        foreach ($quote as $item) {
            $infoBuyRequest = $item->getOptionByCode('info_buyRequest');
            $buyRequest = new Varien_Object(unserialize($infoBuyRequest->getValue()));
            $isSubscribed = 0;
            if ($buyRequest->getIndiesRecurringandrentalpaymentsSubscriptionType()) {
                $isSubscribed = 1;
                break;
            }
        }
        if ($this->canRun() && $this->isEnabled() && $enable && $isSubscribed) {
            return true;
        }
        return false;
    }

    public function checkAvailabilityForTermsDisplay($id)
    {
        $isavailable = Mage::getStoreConfig(Indies_Recurringandrentalpayments_Helper_Config::XML_PATH_GENERAL_ANONYMOUS_SUBSCRIPTIONS);
        $plans_product = Mage::getModel('recurringandrentalpayments/plans_product')->load($id);
        $plan = Mage::getModel('recurringandrentalpayments/plans')->load($plans_product->getPlanId());
        $_loggedIn = Mage::getSingleton('customer/session')->isLoggedIn();
        if ($plan->getPlanStatus() == 1) {
            if ($isavailable == 1 || (($isavailable == 2) && $_loggedIn)) {
                $available = 1;
            } elseif ($isavailable == 3 && $_loggedIn) {
                $available = 0;
                $groupId = Mage::getSingleton('customer/session')->getCustomerGroupId();
                $customer_group = explode(',', Mage::getStoreConfig(Indies_Recurringandrentalpayments_Helper_Config::XML_PATH_CUSTOMER_GROUP));
                if (in_array($groupId, $customer_group)) $available = 1;
                else $available = 3;
            } else {
                $available = 0;
            }
        } else {
            $available = 3;
        }
        return $available;
    }

    public function getBundleItemsPrice($termid, $item_amount, $count = 0)
    {
        $types = Mage::getModel('recurringandrentalpayments/terms')->load($termid);
        $price = $types->getPrice();
        $discountTerm = $types->getDiscountterm();
        $recurringDiscount = 1 - ($discountTerm / 100);
        $recurringDiscount = pow($recurringDiscount, ($count - 1));
        $price = $price * $recurringDiscount;

        if ($types->getPriceCalculationType() == 1) {
            $price = ($item_amount * $types->getPrice() / 100);
            $recurringDiscount = 1 - ($discountTerm / 100);
            $recurringDiscount = pow($recurringDiscount, ($count - 1));
            $price = $price * $recurringDiscount;

        }
        $price = Mage::helper('directory')->currencyConvert($price, Mage::app()->getStore()->getBaseCurrencyCode(), Mage::app()->getStore()->getCurrentCurrencyCode());
        return $price;
    }

    public function calculateParentBundlePrice($params, $item)
    {
        $termid = $params['indies_recurringandrentalpayments_subscription_type'];
        $types = Mage::getModel('recurringandrentalpayments/terms')->load($termid);
        $cal = 0;
        if ($types->getPriceCalculationType() == 1) $cal = 1;

        $sel_bundle_options = $item->getProduct()->getTypeInstance(true)->getOrderOptions($item->getProduct());
        $price = 0;
        $amount = 0;
        $product = Mage::getModel('catalog/product')->load($params['product']);

        if ($product->getPrice() > 0) // Fixed Price Product
        {
            $amount = $types->getPrice();
            if ($cal == 1) {
                $amount = $product->getPrice() * $types->getPrice() / 100;
            }
        }

        // Change Bundle Product Price
        if ($sel_bundle_options) {
            if (isset($sel_bundle_options['bundle_options'])) {
                $options = $sel_bundle_options['bundle_options'];
                foreach ($options as $opt) {
                    $value = $opt['value'];
                    foreach ($value as $v) {
                        if ($item->getProduct()->getFirstPeriodPrice() > 0) {
                            $price = $item->getProduct()->getFirstPeriodPrice();
                        } else {
                            $price = $types->getPrice() * $v['qty'];
                            if ($cal == 1) {
                                $price = ($v['price'] * $v['qty'] * $types->getPrice() / 100);
                            }
                        }
                        $amount = $amount + $price;
                        $item->setCustomPrice($price);
                        $item->setOriginalCustomPrice($price);
                    }
                }
            }
        }
        return $amount;
    }

    public function calCustomOptionPrice($info, $orderItem)
    {
        # Start : to add custom option price in next order price
        $options = $info->getOptions();
        $totalCustomOptionPrice = 0;

        if (sizeof($options)) {
            foreach ($options as $key => $value) {
                $optionData = $orderItem->getProduct()->getOptionById($key);
                if ($optionData['type'] == 'field') {
                    $totalCustomOptionPrice = $optionData['price'];
                } else {
                    # if check box then $value will be an array in if condition will fire
                    if (is_array($value)) {
                        foreach ($value as $vKey => $optionId) {
                            if ($optionData != NULL)// check is option exist, if exist then $optionData will not be null
                            {
                                foreach ($optionData->getValues() as $v) {
                                    if ($v['option_type_id'] == $optionId) {
                                        $totalCustomOptionPrice += $v->getPrice();
                                    }
                                }
                            }
                        }
                    } else # else for drop down where only one value
                    {
                        if ($optionData != NULL)// check is option exist, if exist then $optionData will not be null
                        {
                            foreach ($optionData->getValues() as $v) {
                                if ($v['option_type_id'] == $value) {
                                    $totalCustomOptionPrice += $v->getPrice();
                                }
                            }
                        }
                    }
                }
            }
        }
        return $totalCustomOptionPrice;
    }

    public function getAveragePlan($planId, $termId, $productId)
    {
        $total = 0;
        $count = 0;
        $symbol = Mage::app()->getLocale()->currency(Mage::app()->getStore()->getCurrentCurrencyCode())->getSymbol();
        $termsPrice = Mage::getModel('recurringandrentalpayments/termsprice')->getCollection()
            ->addFieldToSelect('*')
            ->addFieldToFilter('plan_id', array('eq' => $planId))
            ->addFieldToFilter('term_id', array('eq' => $termId))
            ->addFieldToFilter('product_id', array('eq' => $productId));
        if ($termsPrice->getData() != null && !empty($termsPrice)) {
            foreach ($termsPrice as $price) {
                $count++;
                $total = $total + $price->getData('term_price');
            }

            $average = $total / $count;
            return $symbol . number_format($average);
        } else {
            return 0;
        }

    }

    public function getPlanTable($planId, $termId, $productId, $type)
    {
        $termsPrice = Mage::getModel('recurringandrentalpayments/termsprice')->getCollection()
            ->addFieldToSelect('*')
            ->addFieldToFilter('plan_id', array('eq' => $planId))
            ->addFieldToFilter('term_id', array('eq' => $termId))
            ->addFieldToFilter('product_id', array('eq' => $productId));
        if ($termsPrice->getData() != null && !empty($termsPrice)) {
            $string = '';
            foreach ($termsPrice as $price) {
                if ($type == 'term_number') {
                    $string .= '|' . $price->getData($type);
                } else
                    $string .= '|฿' . FLOOR($price->getData($type));
            }
            return $string;
        } else {
            return '';
        }
    }

    public function getMailPlanTable($planId, $termId, $productId)
    {
        $termsPrice = Mage::getModel('recurringandrentalpayments/termsprice')->getCollection()
            ->addFieldToSelect('*')
            ->addFieldToFilter('plan_id', array('eq' => $planId))
            ->addFieldToFilter('term_id', array('eq' => $termId))
            ->addFieldToFilter('product_id', array('eq' => $productId));
        if ($termsPrice->getData() != null && !empty($termsPrice)) {
            $string = '';
            $string .= '<table style="border: 3px solid gray;width:100%">';
            $string .= '<tr style="line-height: 20px;">';
            $string .= '<th style="border-bottom:3px solid gray">AUTOSHIP</th>';
            $string .= '<th style="border-bottom:3px solid gray">PRICE PLAN</th>';
            $string .= '</tr>';
            foreach ($termsPrice as $price) {
                $string .= '<tr style="border-bottom:3px solid gray">';
                $string .= '<td align="middle">' . $price->getData('term_number') . '</td>';
                $string .= '<td align="middle">฿' . $price->getData('term_price') . '</td>';
                $string .= '</tr>';
            }
            $string .= '</table>';
            return $string;
        } else {
            return '';
        }
    }

    public function getBestAveragePrice($planId, $productId)
    {
        $min = 0;
        $terms = Mage::getModel('recurringandrentalpayments/terms')->getCollection()
            ->addFieldToFilter('plan_id', $planId);
        foreach ($terms as $term) {
            $total = 0;
            $count = 0;
            $termsPrice = Mage::getModel('recurringandrentalpayments/termsprice')->getCollection()
                ->addFieldToSelect('*')
                ->addFieldToFilter('plan_id', array('eq' => $planId))
                ->addFieldToFilter('term_id', array('eq' => $term->getId()))
                ->addFieldToFilter('product_id', array('eq' => $productId));
            foreach ($termsPrice as $price) {
                $count++;
                $total = $total + $price->getData('term_price');
            }
            $average = $total / $count;

            if ($min == 0) $min = $average;
            else {
                if ($min > $average) $min = $average;
            }
        }
        $symbol = Mage::app()->getLocale()->currency(Mage::app()->getStore()->getCurrentCurrencyCode())->getSymbol();
        return $symbol . number_format($min);
    }

    public function getAveragePriceByTerms($planId, $termNumber, $productId)
    {
        $term = Mage::getModel('recurringandrentalpayments/terms')->getCollection()
            ->addFieldToFilter('plan_id', array('eq' => $planId))
            ->addFieldToFilter('noofterms', array('eq' => $termNumber))
            ->getFirstItem();

        $total = 0;
        $count = 0;
        $termsPrice = Mage::getModel('recurringandrentalpayments/termsprice')->getCollection()
            ->addFieldToSelect('*')
            ->addFieldToFilter('plan_id', array('eq' => $planId))
            ->addFieldToFilter('term_id', array('eq' => $term->getId()))
            ->addFieldToFilter('product_id', array('eq' => $productId));
        foreach ($termsPrice as $price) {
            $count++;
            $total = $total + $price->getData('term_price');
        }
        $average = $total / $count;

        $symbol = Mage::app()->getLocale()->currency(Mage::app()->getStore()->getCurrentCurrencyCode())->getSymbol();
        $average = $symbol . number_format($average, 2, '.', '');
        return $average;
    }

    public function getSubscriptionPaymentData($order, $payment_model)
    {
        $result = array();

        $autoship_item_collection = Mage::getModel('recurringandrentalpayments/subscription_item')->getCollection();
        $autoship_items = $autoship_item_collection->addFieldToSelect('*')
            ->addFieldToFilter('primary_order_id', $order->getId())->getData();

        if (!$autoship_items) {
            return $result;
        }
        // Return response
        // recurring
        // order_prefix
        // allow_accumulate
        // max_acculumate_amount
        // recurring count
        // charge next on date
        // charge on date
        $subscription_ids = array();

        $result['recurring'] = 'Y';
        $result['order_prefix'] = $order->getId();
        $result['recurring_amount'] = 0;
        $result['allow_accumulate'] = 'Y';
        $result['max_accumulate_amount'] = 0;

        $maxAmount = 0;
        $amount = 0;
        foreach ($autoship_items as $item) {
            if (!in_array($item['subscription_id'], $subscription_ids)) {
                array_push($subscription_ids, $item['subscription_id']);
            }
            $subscription = Mage::getModel('recurringandrentalpayments/subscription')->load($item['subscription_id']);
            $termId = $subscription->getData('term_type');
            $term = Mage::getModel('recurringandrentalpayments/terms')->load($termId);
            $termNum = $term->getData('noofterms');
            $order_item = Mage::getModel('sales/order_item')
                ->load($item['primary_order_item_id'], 'item_id')->getData();
            $recurringQuantity = $order_item['qty_ordered'];
            $product = Mage::getModel('catalog/product')->load($order_item['product_id']);
            $price = $term->getPrice();
            $discountTerm = $term->getDiscountterm();
            $recurringDiscount = 1 - ($discountTerm / 100);
            $recurringDiscount = pow($recurringDiscount, (2 - 1));
            $price = $price * $recurringDiscount;
            if ($term->getPriceCalculationType() == 1) {
                $price = ($product->getPrice() * $term->getPrice() / 100);
                $recurringDiscount = 1 - ($discountTerm / 100);
                $recurringDiscount = pow($recurringDiscount, (2 - 1));
                $price = $price * $recurringDiscount;
            }
            $amount += $price * $recurringQuantity;
            $maxAmount += ($price * $recurringQuantity) * $termNum;
        }
        $shippingFee = 0;
        $amount = $amount + $shippingFee;
        $maxAmount = $maxAmount + $shippingFee;
        $amount = (string)$amount;
        $maxAmount = ceil($maxAmount);
        $result['recurring_amount'] = ceil($amount);
        $result['max_accumulate_amount'] = $payment_model->getAmount($maxAmount);
        $result['recurring_amount'] = $payment_model->getAmount((string)$result['recurring_amount']);

        $sequences = Mage::getModel('recurringandrentalpayments/sequence')->getCollection();
        $total_sequence = $sequences->addFieldToSelect('*')
            ->addFieldToFilter('subscription_id', array('in' => $subscription_ids));

        $result['recurring_count'] = $total_sequence->count() - 1;

        // can we not do another SQL?
        $upcoming = Mage::getModel('recurringandrentalpayments/sequence')->getCollection();
        $upcoming_sequence = $upcoming->addFieldToSelect('*')
            ->addFieldToFilter('subscription_id', array('in' => $subscription_ids))
            ->addFieldToFilter('transaction_status', array('null' => true))
            ->setOrder('date', 'ASC');
        $next_date = new Datetime($upcoming_sequence->getFirstItem()['date']);
        $current_date = new Datetime($order->getCreatedAt());
        $result['charge_next_date'] = $next_date->format('dmY');
        $result['charge_on_date'] = $current_date->format('dm');

        Mage::log($result);
        return $result;
    }

    /**
     * This function have 2 mission. Save recurring unique id and match recurring transaction with magento order
     * @param $order
     * @param $response
     * @return bool
     */
    public function saveRecurringOrderInformation($order, $response)
    {
        Mage::log("------------------------------------------", null, 'recurring_auto.log', true);
        Mage::log($response, null, 'recurring_auto.log', true);
        $today = Mage::getModel('core/date')->date('Y-m-d');
        Mage::log($today, null, 'recurring_auto.log', true);
        $unique_recurring_id = $response['recurring_unique_id'];
        $_collections = Mage::getModel('recurringandrentalpayments/subscription_item')->getCollection();
        $subscriptions = $_collections->addFieldToSelect('subscription_id')
            ->addFieldToFilter('primary_order_id', $order->getId());
        foreach ($subscriptions as $sub) {
            $subId = $sub->getData('subscription_id');
            $model = Mage::getModel('recurringandrentalpayments/subscription')->load($subId);
            $realId = $model->getRealId();
            $realPaymentId = $model->getRealPaymentId();
            if ($realId == '' || $realId == NULL || $realPaymentId == '' || $realPaymentId == NULL) {
                $model->setRealId($unique_recurring_id);
                $model->setRealPaymentId($unique_recurring_id);
                try {
                    $model->save();
                } catch (Exception $e) {
                    Mage::logException($e);
                }
            }
            Mage::log("Start create and match order", null, 'recurring_auto.log', true);
            $order2c2pId = $response['order_id'];
            $order2c2p = Mage::getModel('sales/order')->loadByIncrementId($order2c2pId);
            if ($response['approval_code'] != NULL && $response['transaction_datetime'] != NULL) {
                if (!$order2c2p->getId()) {

                    Mage::register('recurring_unique_id', $order2c2pId);
                    $today = Mage::getModel('core/date')->date('Y-m-d');
                    $sequence = Mage::getModel('recurringandrentalpayments/sequence')
                        ->getCollection()
                        ->addFieldToFilter('subscription_id', array('eq' => $subId))
                        ->addFieldToFilter('date', array('eq' => $today))->getFirstItem();
                    Mage::log($sequence->getData(), null, 'recurring_auto.log', true);

                    $subs = Mage::getModel('recurringandrentalpayments/subscription')->load($subId);
                    $subs->payBySequence($sequence);
                    $orderMagentoId = $sequence->getData('order_id');

                    $sequence->set2c2pOrderId($order2c2pId);
                    $sequence->save();
                    if ($orderMagentoId != NULL && $orderMagentoId != "") {
                        $order = Mage::getModel('sales/order')->load($orderMagentoId);
                        if ($order->canInvoice()) {
                            Mage::log("Create Invoice", null, 'recurring_auto.log', true);
                            $invoice = Mage::getModel('sales/service_order', $order)->prepareInvoice();
                            $invoice->setRequestedCaptureCase(Mage_Sales_Model_Order_Invoice::CAPTURE_ONLINE);
                            $invoice->getOrder()->addStatusHistoryComment(
                                'Recurring Invoiced by 2c2p order id: ' . $order2c2pId, true
                            );
                            $invoice->addComment('Invoiced by 2c2p order id: ' . $order2c2pId);
                            $invoice->register();
                            $transactionSave = Mage::getModel('core/resource_transaction')
                                ->addObject($invoice)
                                ->addObject($invoice->getOrder());
                            $transactionSave->save();
                        }
                        $normal2c2p = Mage::getModel('normal2c2p/normal2c2p');

                        if($response['payment_status'] != EMST_Normal2c2p_Model_PaymentChannel::PAY_STATUS_PENDING){
                            $normal2c2p->_saveAdditionalInfo($order, $response['payment_status'], $response['channel_response_code'], $response['channel_response_desc']);
                        }
                    }
                }
            }
        }
        Mage::log("------------------------------------------", null, 'recurring_auto.log', true);

        return true;
    }

    /**
     * Use to calculate shipping fee base on religion
     * @param $order
     * @return int
     */
    public function calculateShippingFee($order)
    {
        $religionId = $order->getShippingAddress()->getData('region_id');
        $orderTotalPrice = $order->getSubtotal();
        if ($religionId == 561) {
            if ($orderTotalPrice >= 1000) return 0;
            else if ($orderTotalPrice >= 500 && $orderTotalPrice < 1000) return 50;
            else if ($orderTotalPrice < 500) return 100;
        } else {
            if ($orderTotalPrice >= 1000) return 0;
            else if ($orderTotalPrice >= 500 && $orderTotalPrice < 1000) return 100;
            else if ($orderTotalPrice < 500) return 200;
        }
    }

    /**
     * Send request to to 2c2p for recurring update.
     * @param $body
     * @return mixed
     */
    public function send2c2pRequest($body)
    {
        $url = Mage::getStoreConfig('payment/normal2c2p/recurring_url');
        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/x-www-form-urlencoded',
            'Connection: Keep-Alive'
        ));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Expect:  "));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);


        if (curl_errno($ch)) {
            echo 'Curl error: ' . curl_error($ch);
            die;
        }
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }

    /**
     * This function is use to update price to 2c2p
     * @param $subscription
     * @return string
     */
    public function updateSubscription($subscription,$processType = '')
    {
        if($processType == "C"){
            $recurringStatus = "N";
        }else{
            $recurringStatus = "Y";
        }

        $body = "<RecurringMaintenanceRequest>";
        $body .= "<version>2.1</version>";
        $body .= "<timeStamp></timeStamp>";
        $autoship_item_collection = Mage::getModel('recurringandrentalpayments/subscription_item')->getCollection();
        $autoship_items = $autoship_item_collection->addFieldToSelect('*')
            ->addFieldToFilter('subscription_id', $subscription->getData('id'))->getData();

        $recurringUniqueId = $subscription->getData('real_payment_id');
        $merchantID = Mage::getStoreConfig('payment/normal2c2p/merchant_id');

        $body .= "<merchantID>" . $merchantID . "</merchantID>";
        $body .= "<recurringUniqueID>" . $recurringUniqueId . "</recurringUniqueID>";
        $body .= "<processType>".$processType."</processType>";
        $body .= "<recurringStatus>" . $recurringStatus . "</recurringStatus>";

        $maxAmount = 0;
        $amount = 0;
//        $termCount = Mage::getModel('recurringandrentalpayments/termscount')->getCollection()
//            ->addFieldToSelect('*')
//            ->addFieldToFilter('customer_id', array('eq' => $subscription->getCustomerId()))
//            ->addFieldToFilter('subscription_id', array('eq' => $subscription->getId()))
//            ->getFirstItem();

        $term = Mage::getModel('recurringandrentalpayments/terms')->load($subscription->getData('term_type'));
        $termNum = $term->getData('noofterms');

//        if (empty($termCount) || $termCount == null || !$termCount) {
//            $count = 2;
//        } else {
//            $count = $termCount->getData('count');
//            if ($count == null || $count < 2) $count = 2;
//        }
//
//        if ($count > $termNum) {
//            $count = 2;
//        }

        $upcoming = Mage::getModel('recurringandrentalpayments/sequence')->getCollection();
        $upcoming_sequence = $upcoming->addFieldToSelect('*')
            ->addFieldToFilter('subscription_id', array('in' => $subscription->getId()))
            ->addFieldToFilter('transaction_status', array('null' => true))
            ->setOrder('date', 'ASC');

        $next_date = new Datetime($upcoming_sequence->getFirstItem()['date']);
        $chargeNextDate = $next_date->format('dmY');
        if($processType == "C"){
            $chargeNextDate = "";
        }
        $count = count($upcoming_sequence->getData());
        $currentCount = ($termNum - $count) + 1;

        foreach ($autoship_items as $item) {
            $order_item = Mage::getModel('sales/order_item')
                ->load($item['primary_order_item_id'], 'item_id')->getData();
            $product = Mage::getModel('catalog/product')->load($order_item['product_id']);
            $recurringQuantity = $order_item['qty_ordered'];
            $price = $term->getPrice();
            $discountTerm = $term->getDiscountterm();
            $recurringDiscount = 1 - ($discountTerm / 100);
            $recurringDiscount = pow($recurringDiscount, ($currentCount - 1));
            $price = $price * $recurringDiscount;
            if ($term->getPriceCalculationType() == 1) {
                $price = ($product->getPrice() * $term->getPrice() / 100);
                $recurringDiscount = 1 - ($discountTerm / 100);
                $recurringDiscount = pow($recurringDiscount, ($currentCount - 1));
                $price = $price * $recurringDiscount;
            }
            $amount += $price * $recurringQuantity;
            $maxAmount += ($price * $recurringQuantity) * $termNum;
        }

//        $orderId = $autoship_items[0]['primary_order_id'];
//        $order = Mage::getModel('sales/order')->load($orderId);
        $shippingFee = 0;
        $amount = $amount + $shippingFee;
        $maxAmount = $maxAmount + $shippingFee;

        $allowAmount = "N";
        $secret_key = Mage::getStoreConfig('payment/normal2c2p/secret_key');
        $normal2c2p = Mage::getSingleton('normal2c2p/normal2c2p');

        $amount = $normal2c2p->getAmount((string)ceil($amount));
        $maxAmount = $normal2c2p->getAmount((string)ceil($maxAmount));
        $recurringCount = $termNum - 1;

        $version = "2.1";

        $termDateType = $term->getData('termsper');
        if ($termDateType == 'day') $recurringInterval = 1;
        else if ($termDateType == 'week') $recurringInterval = 7;
        else if ($termDateType == 'month') $recurringInterval = 30;
        else if ($termDateType == 'year') $recurringInterval = 365;


        $strSignatureString = $version . $merchantID . $recurringUniqueId . $processType . $recurringStatus
            . $amount . $allowAmount . $maxAmount . $recurringInterval . $recurringCount . $chargeNextDate;
        $hashValue = hash_hmac('sha1', $strSignatureString, $secret_key, false);

        $body .= "<amount>" . $amount . "</amount>";
        $body .= "<allowAccumulate>" . $allowAmount . "</allowAccumulate>";
        $body .= "<maxAccumulateAmount>" . $maxAmount . "</maxAccumulateAmount>";
        $body .= "<recurringInterval>" . $recurringInterval . "</recurringInterval>";
        $body .= "<recurringCount>" . $recurringCount . "</recurringCount>";
        $body .= "<chargeNextDate>" . $chargeNextDate . "</chargeNextDate>";
        $body .= "<hashValue>" . $hashValue . "</hashValue>";
        $body .= "</RecurringMaintenanceRequest>";

        $publicKey2c2p = Mage::getStoreConfig('payment/normal2c2p/recurring_2c2p_publickey');
        $urlKey = Mage::getBaseDir();
        $publicKey = $urlKey . Mage::getStoreConfig('payment/normal2c2p/recurring_publickey_patch');
        $privateKey = $urlKey . Mage::getStoreConfig('payment/normal2c2p/recurring_private_patch');

        Mage::log($body,null,'recurring_response.log',true);
        Mage::log($strSignatureString,null,'recurring_response.log',true);

        $body = $this->encrypt($body, $publicKey2c2p);
        $body = "paymentRequest=" . $body;
        $response = $this->send2c2pRequest($body);
        $xmlResponse = $this->decrypt($response, $publicKey, $privateKey, '2c2p');
        $responseArray = $this->convertXMLToArray($xmlResponse);

        Mage::log($responseArray['respCode'] . " : " . $responseArray['respReason'], null, '2c2pRecurring_Update.log', true);
        return $responseArray;
    }

    /**
     * This function is use to encrypt data with 2c2p public key
     * @param $text
     * @param $publickey
     * @return mixed|string
     */
    public function encrypt($text, $publickey)
    {
        $urlKey = Mage::getBaseDir('media') . "/autoship/encrypt";

        if (!file_exists($urlKey . "/tmp/")) {
            mkdir($urlKey . "/tmp/", 0777, true);
        }
        $filename = $urlKey . "/tmp/" . time() . ".txt";
        $this->text_to_file($text, $filename);
        $filename_enc = $urlKey . "/tmp/" . time() . ".enc";

//		$key = file_get_contents($publickey);
        $key = $publickey;
        if (openssl_pkcs7_encrypt($filename, $filename_enc, $key,
            array())) {
            // message encrypted - send it!
            unlink($filename);
            if (!$handle = fopen($filename_enc, 'r')) {
                echo "Cannot open file ($filename_enc)";
                exit;
            }

            $contents = fread($handle, filesize($filename_enc));
            fclose($handle);
            $contents = str_replace("MIME-Version: 1.0", "", $contents);
            $contents = str_replace("Content-Disposition: attachment; filename=\"smime.p7m\"", "", $contents);
            $contents = str_replace("Content-Type: application/x-pkcs7-mime; smime-type=enveloped-data; name=\"smime.p7m\"",
                "", $contents);
            $contents = str_replace("Content-Transfer-Encoding: base64", "", $contents);
            $contents = str_replace("\n", "", $contents);
            unlink($filename_enc);
            return $contents;
        }
    }

    /**
     * This function is use for decrypt data data base on 2c2p key pair
     * @param $text
     * @param string $publickey
     * @param string $privatekey
     * @param $password
     * @return string
     * @throws Mage_Core_Exception
     */
    public function decrypt($text, $publickey = '', $privatekey = '', $password)
    {
        $arr = str_split($text, 64);
        $text = "";
        foreach ($arr as $val) {
            $text .= $val . "\n";
        }

        $text = "MIME-Version: 1.0
Content-Disposition: attachment; filename=\"smime.p7m\"
Content-Type: application/pkcs7-mime; smime-type=enveloped-data; name=\"smime.p7m\"
Content-Transfer-Encoding: base64

" . $text;

        $text = rtrim($text, "\n");

        $urlKey = Mage::getBaseDir('media') . "/autoship/decrypt";

        if (!file_exists($urlKey . "/tmp/")) {
            mkdir($urlKey . "/tmp/", 0777, true);
        }

        $infilename = $urlKey . "/tmp/" . time() . ".txt";
        $this->text_to_file($text, $infilename);
        $outfilename = $urlKey . "/tmp/" . time() . ".dec";

        $public = file_get_contents($publickey);
        $private = array(file_get_contents($privatekey), $password);

        if (openssl_pkcs7_decrypt($infilename, $outfilename, $public, $private)) {
            unlink($infilename);
            $content = file_get_contents($outfilename);
            unlink($outfilename);
            return $content;
        } else {
            unlink($outfilename);
            unlink($infilename);
            echo "DECRYPT FAIL";
            Mage::throwException($this->__('Fail to decrypt data'));
            exit;
        }
    }

    private function text_to_file($text, $filename)
    {
        if (!$handle = fopen($filename, 'w')) {
            echo "Cannot open file ($filename)";
            Mage::throwException($this->__("Cannot open file ($filename)"));
            exit;
        }

        // Write $somecontent to our opened file.
        if (fwrite($handle, $text) === FALSE) {
            echo "Cannot write to file ($filename)";
            Mage::throwException($this->__("Cannot write to file ($filename)"));
            exit;
        }
    }

    public function convertXMLToArray($xml)
    {
        $obj = simplexml_load_string($xml);
        $array = json_decode(json_encode($obj), true);
        return $array;
    }
}
