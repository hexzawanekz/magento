<?php
$installer = $this;
$installer->startSetup();
$installer->getConnection()
    ->addColumn($installer->getTable('recurringandrentalpayments/terms'),'discountterm', array(
        'type' => Varien_Db_Ddl_Table::TYPE_FLOAT,
        'length' => 11,
        'nullable' => false,
        'default' => 0,
        'comment' => 'Discount Per Term'
    ));
$installer->endSetup();

?>