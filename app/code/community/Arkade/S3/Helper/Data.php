<?php

class Arkade_S3_Helper_Data extends Mage_Core_Helper_Data
{
    const XML_PATH_DEFAULT_EXPIRY_DATE  = 'arkade_s3/general/default_expiry_date';
    const REQUEST_HEADER_EXPIRES        = 'Expires';

    private $client = null;

    /**
     * Get default expiry date set in Module Configuration
     *
     * @return mixed|string
     */
    public function getExpiryDate()
    {
        /** @var Mage_Core_Model_Date $date */
        $date = Mage::getModel('core/date');

        if($expiryDate = Mage::getStoreConfig(self::XML_PATH_DEFAULT_EXPIRY_DATE)){
            return gmdate('D, d M Y H:i:s T', $date->timestamp(time() + 86400 * (int)$expiryDate));
        }
        return gmdate('D, d M Y H:i:s T', $date->timestamp(time() + 86400 * 30));
    }

    /**
     * @return Zend_Service_Amazon_S3
     */
    public function getClient()
    {
        if (is_null($this->client)) {
            $this->client = new Zend_Service_Amazon_S3(
                $this->getAccessKey(),
                $this->getSecretKey(),
                $this->getRegion()
            );
        }
        return $this->client;
    }

    /**
     * @param string $filePath
     * @param string $prefix
     * @return string
     */
    public function getObjectKey($filePath, $prefix = null)
    {
        if (!is_null($prefix)) {
            $filePath = $prefix . '/' . $filePath;
        }
        return $this->getBucket() . '/' . $filePath;
    }

    /**
     * Returns the AWS access key.
     *
     * @return string
     */
    public function getAccessKey()
    {
        return Mage::getStoreConfig('arkade_s3/general/access_key');
    }

    /**
     * Returns the AWS secret key.
     *
     * @return string
     */
    public function getSecretKey()
    {
        return Mage::getStoreConfig('arkade_s3/general/secret_key');
    }

    /**
     * Returns the AWS region that we're using, e.g. ap-southeast-2.
     *
     * @return string
     */
    public function getRegion()
    {
        return Mage::getStoreConfig('arkade_s3/general/region');
    }

    /**
     * Returns the S3 bucket where we want to store all our images.
     *
     * @return string
     */
    public function getBucket()
    {
        return Mage::getStoreConfig('arkade_s3/general/bucket');
    }
}
