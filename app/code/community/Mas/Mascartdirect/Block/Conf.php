<?php 
class Mas_Mascartdirect_Block_Conf extends Mage_Core_Block_Template
{
	public function __construct(){
		parent::__construct();        
	}
	
	public function _toHtml()
	{
		if (!$this->getRequest()->getParam('mascartdirect')) {
			return '';
		}
		return parent::_toHtml();
	}
	
	public function getProductId()
	{
		return Mage::registry('current_product')->getId();
	}
	
	public function getCartLink()
	{
		return $this->getUrl('mascartdirect/index/add', array('product' => $this->getProductId(), 'addtocart' => true));
	}
	
	public function getCheckoutLink()
	{
		return $this->getUrl('mascartdirect/index/add', array('product' => $this->getProductId(), 'bypass' => true));
	}
}