<?php 
/**
 * Mas_Mascartdirect extension by Makarovsoft.com
 * 
 * @category   	Mas
 * @package		Mas_Mascartdirect
 * @copyright  	Copyright (c) 2014
 * @license		http://makarovsoft.com/license.txt
 * @author		makarovsoft.com
 */
/**
 * Mascartdirect module install script
 *
 * @category	Mas
 * @package		Mas_Mascartdirect
 * 
 */
$this->startSetup();
$this->endSetup();