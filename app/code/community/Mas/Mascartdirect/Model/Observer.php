<?php
/**
 * Mas_Mascartdirect extension by Makarovsoft.com
 * 
 * @category    Mas
 * @package     Mas_Mascartdirect
 * @copyright   Copyright (c) 2014
 * @license     http://makarovsoft.com/license.txt
 * @author      makarovsoft.com
 */
/**
 * Adminhtml observer
 *
 * @category    Mas
 * @package     Mas_Mascartdirect
 * 
 */
class Mas_Mascartdirect_Model_Observer {
    
    public function bypassCart(Varien_Event_Observer $observer) {
        if (Mage::registry('bypass')) {
            $response = $observer->getResponse();
            $response->setRedirect(Mage::getUrl('checkout/onepage'));
            Mage::getSingleton('checkout/session')->setNoCartRedirect(true); 
        } else {
            $response = $observer->getResponse();

        if(!$_GET['utm_source']==""){
//              $response->setRedirect('/checkout/cart/?utm_source=PPTV&utm_medium=MamPal&utm_campaign=TVCommerce');
$url = '/checkout/cart/?utm_source='.$_GET['utm_source'].'&utm_medium='.$_GET['utm_medium'].'&utm_campaign='.$_GET['utm_campaign'].'';
                $response->setRedirect($url);
        }else{
        $response->setRedirect(Mage::getUrl('checkout/cart'));
            }
        Mage::getSingleton('checkout/session')->setNoCartRedirect(true);

        }
    }
}
