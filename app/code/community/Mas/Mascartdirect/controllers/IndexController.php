<?php
/**
 * Mas_Masemailpricelist extension by Makarovsoft.com
 * 
 * @category   	Mas
 * @package		Mas_Masemailpricelist
 * @copyright  	Copyright (c) 2014
 * @license		http://makarovsoft.com/license.txt
 * @author		makarovsoft.com
 */
/**
 * Email admin controller
 *
 * @category	Mas
 * @package		Mas_Masemailpricelist
 * 
 */
require_once 'Mage/Checkout/controllers/CartController.php';
class Mas_Mascartdirect_IndexController extends Mage_Checkout_CartController  {
	
	public function addAction() 
	{        
		$cart = Mage::getSingleton('checkout/cart');
		
		$cart->init();
		if (Mage::getStoreConfig('mascartdirect/general/clear')) {
        	$cart->truncate();
        }
        
		if (!($formKey = $this->getRequest()->getParam('form_key', null)) || $formKey != Mage::getSingleton('core/session')->getFormKey()) {
			$this->getRequest()->setParams(array('form_key' => Mage::getSingleton('core/session')->getFormKey()));
    	}
    	
    	if ($this->getRequest()->getParam('bypass')) {
            Mage::register('bypass', true);
    	}
    	
    	if ($this->getRequest()->getParam('addtocart')) {
            Mage::register('addtocart', true);
    	}
        parent::addAction();
	}
	
	public function addgroupAction()
	{
		$cart = Mage::getSingleton('checkout/cart');
		
        $cart->init();
        if (Mage::getStoreConfig('mascartdirect/general/clear')) {
        	$cart->truncate();
        }
        
		$orderItemIds = $this->getRequest()->getParam('ids', array());
		$cart->addProductsByIds($orderItemIds);
    	parent::addgroupAction();
	}
}	
	
