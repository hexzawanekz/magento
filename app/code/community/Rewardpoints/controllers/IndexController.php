<?php
/**
 * J2T RewardsPoint2
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@j2t-design.com so we can send you a copy immediately.
 *
 * @category   Magento extension
 * @package    RewardsPoint2
 * @copyright  Copyright (c) 2009 J2T DESIGN. (http://www.j2t-design.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Rewardpoints_IndexController extends Mage_Core_Controller_Front_Action
{
    
    public function indexAction()
    {
        if ($this->getRequest()->isPost() && $this->getRequest()->getPost('email')) {
            $session         = Mage::getSingleton('core/session');
            $emails           = $this->getRequest()->getPost('email'); //trim((string) $this->getRequest()->getPost('email'));
            $names            = $this->getRequest()->getPost('name'); //trim((string) $this->getRequest()->getPost('name'));

            
            $customerSession = Mage::getSingleton('customer/session');
            //$errors = array();
            try {
                foreach ($emails as $key_email => $email){
                    $name = trim((string) $names[$key_email]);
                    $email = trim((string) $email);
                    
                    ///////////////////////////////////////////
                    
                    $no_errors = true;
                    if (!Zend_Validate::is($email, 'EmailAddress')) {
                        //Mage::throwException($this->__('Please enter a valid email address.'));
                        //$errors[] = $this->__('Wrong email address (%s).', $email);
                        $session->addError($this->__('Wrong email address (%s).', $email));
                        $no_errors = false;
                    }
                    if ($name == ''){
                        //Mage::throwException($this->__('Please enter your friend name.'));
                        //$errors[] = $this->__('Friend name is required for (%s) on line %s.', $email, ($key_email+1));
                        $session->addError($this->__('Friend name is required for email: %s on line %s.', $email, ($key_email+1)));
                        $no_errors = false;
                    }
                    
                    if ($no_errors){
                        $referralModel = Mage::getModel('rewardpoints/referral');

                        $customer = Mage::getModel('customer/customer')
                                        ->setWebsiteId(Mage::app()->getStore()->getWebsiteId())
                                        ->loadByEmail($email);

                        if ($referralModel->isSubscribed($email) || $customer->getEmail() == $email) {
                            //Mage::throwException($this->__('Email %s has been already submitted.', $email));
                            $session->addError($this->__('Email %s has been already registered.', $email));
                        } else {
                            if ($referralModel->subscribe($customerSession->getCustomer(), $email, $name)) {
                                $session->addSuccess($this->__('Email %s was successfully invited.', $email));
                            } else {
                                $session->addError($this->__('There was a problem with the invitation email %s.', $email));
                            }
                        }
                    }
                    
                    ///////////////////////////////////////////
                }
                
            }
            catch (Mage_Core_Exception $e) {
                $session->addException($e, $this->__('%s', $e->getMessage()));
            }
            catch (Exception $e) {
                print_r($e);
                die;
                $session->addException($e, $this->__('There was a problem with the invitation.'));
            }
        }

        /*$handles = array('default');
        if (Mage::getSingleton('customer/session')->isLoggedIn()) {
            $handles[] = 'customer_account';
        }
        $this->loadLayout($handles);
        $this->renderLayout();*/

        $this->loadLayout();
        $this->renderLayout();


    }
    

    public function referralAction()
    {
        $this->indexAction();
    }

    public function pointsAction()
    {
        $this->indexAction();
    }


    public function goReferralAction(){
        $userId = (int) $this->getRequest()->getParam('referrer');
        $productId = (int) $this->getRequest()->getParam('sharedPid');
		if($userId) {
			Mage::getSingleton('rewardpoints/session')->setReferralUser($userId);
		}
        //Mage::getSingleton('rewardpoints/session')->getReferralUser()
        //$url = Mage::getUrl();
        //$this->getResponse()->setRedirect($url);
		/* if($productId) {
			$url = Mage::getModel('catalog/product')->load($productId)->getProductUrl();
		}
		if($url) {
			$this->_redirectUrl($url);
		} */
        if($productId) {
			$this->_redirect('catalog/product/view',array('id'=>$productId));
		}
        elseif ($url_redirection = Mage::getStoreConfig('rewardpoints/registration/referral_redirection', Mage::app()->getStore()->getId())){
            $this->_redirect($url_redirection);
        } else {
            $pageId = Mage::getStoreConfig(Mage_Cms_Helper_Page::XML_PATH_HOME_PAGE);
            if (!Mage::helper('cms/page')->renderPage($this, $pageId)) {
                $this->_forward('defaultIndex');
            }
        }
        
    }

    public function removequotationAction(){
        Mage::getSingleton('rewardpoints/session')->setProductChecked(0);
        Mage::helper('rewardpoints/event')->setCreditPoints(0);
        Mage::helper('checkout/cart')->getCart()->getQuote()
                ->setRewardpointsQuantity(NULL)
                ->setRewardpointsDescription(NULL)
                ->setBaseRewardpoints(NULL)
                ->setRewardpoints(NULL)
                ->save();
        $refererUrl = $this->_getRefererUrl();
        if (empty($refererUrl)) {
            $refererUrl = empty($defaultUrl) ? Mage::getBaseUrl() : $defaultUrl;
        }
        $this->getResponse()->setRedirect($refererUrl);
    }

    public function quotationAction(){
        $couponCode = Mage::getSingleton('checkout/cart')->getQuote()->getCouponCode();
        if($couponCode == null || $couponCode == ""){
//            $cart = Mage::getSingleton('checkout/cart')->getQuote();
//            foreach ($cart->getAllItems() as $item) {
//                $buyInfo = $item->getBuyRequest();
//                $custom = $buyInfo->getIndiesRecurringandrentalpaymentsSubscriptionType();
//                if($custom && $custom > 0){
//                    $session = Mage::getSingleton('core/session');
//                    $session->addError($this->__('Coupon code and reward points cannot be used with autoship order.'));
//                    $refererUrl = $this->_getRefererUrl();
//                    if (empty($refererUrl)) {
//                        $refererUrl = empty($defaultUrl) ? Mage::getBaseUrl() : $defaultUrl;
//                    }
//                    $this->getResponse()->setRedirect($refererUrl);
//                    return;
//                }
//            }
            $session = Mage::getSingleton('core/session');
            $points_value = $this->getRequest()->getPost('points_to_be_used');
            if (Mage::getStoreConfig('rewardpoints/default/max_point_used_order', Mage::app()->getStore()->getId())){
                if ((int)Mage::getStoreConfig('rewardpoints/default/max_point_used_order', Mage::app()->getStore()->getId()) < $points_value){
                    $points_max = (int)Mage::getStoreConfig('rewardpoints/default/max_point_used_order', Mage::app()->getStore()->getId());
                    $session->addError($this->__('You tried to use %s loyalty points, but you can use a maximum of %s points per shopping cart.', $points_value, $points_max));
                    $points_value = $points_max;
                }
            }

            if(Mage::getStoreConfig('rewardpoints/default/minimun_grandtotal_order', Mage::app()->getStore()->getId())){
                $minimumPrice = Mage::getStoreConfig('rewardpoints/default/minimun_grandtotal_order', Mage::app()->getStore()->getId());
                $quote = Mage::helper('checkout/cart')->getCart()->getQuote();
                $subtotal = $quote->getSubtotal();
                if ($subtotal < $minimumPrice){
                    $session = Mage::getSingleton('core/session');
                    $session->addError($this->__('Reward Points were not applied, your order total must be at least ฿ %s',$minimumPrice));
                    $refererUrl = $this->_getRefererUrl();
                    if (empty($refererUrl)) {
                        $refererUrl = empty($defaultUrl) ? Mage::getBaseUrl() : $defaultUrl;
                    }
                    $this->getResponse()->setRedirect($refererUrl);
                }else{
                    $quote_id = Mage::helper('checkout/cart')->getCart()->getQuote()->getId();

                    Mage::getSingleton('rewardpoints/session')->setProductChecked(0);
                    Mage::getSingleton('rewardpoints/session')->setShippingChecked(0);

                    Mage::helper('rewardpoints/event')->setCreditPoints($points_value);


                    Mage::helper('checkout/cart')->getCart()->getQuote()
                        ->setRewardpointsQuantity($points_value)
                        ->save();

                    $refererUrl = $this->_getRefererUrl();
                    if (empty($refererUrl)) {
                        $refererUrl = empty($defaultUrl) ? Mage::getBaseUrl() : $defaultUrl;
                    }
                    $this->getResponse()->setRedirect($refererUrl);
                }
            }else{
                $quote_id = Mage::helper('checkout/cart')->getCart()->getQuote()->getId();

                Mage::getSingleton('rewardpoints/session')->setProductChecked(0);
                Mage::getSingleton('rewardpoints/session')->setShippingChecked(0);

                Mage::helper('rewardpoints/event')->setCreditPoints($points_value);


                Mage::helper('checkout/cart')->getCart()->getQuote()
                    ->setRewardpointsQuantity($points_value)
                    ->save();

                $refererUrl = $this->_getRefererUrl();
                if (empty($refererUrl)) {
                    $refererUrl = empty($defaultUrl) ? Mage::getBaseUrl() : $defaultUrl;
                }
                $this->getResponse()->setRedirect($refererUrl);
            }


        }else{
            $session = Mage::getSingleton('core/session');
            $session->addError($this->__('Coupon code and reward points cannot be used at the same time.'));
            $refererUrl = $this->_getRefererUrl();
            if (empty($refererUrl)) {
                $refererUrl = empty($defaultUrl) ? Mage::getBaseUrl() : $defaultUrl;
            }
            $this->getResponse()->setRedirect($refererUrl);
        }
    }

    public function preDispatch()
    {
        
        parent::preDispatch();
        $action = $this->getRequest()->getActionName();
        $actions_array = array('referral', 'points');
        if (in_array($action, $actions_array)){
            $loginUrl = Mage::helper('customer')->getLoginUrl();

            if (!Mage::getSingleton('customer/session')->authenticate($this, $loginUrl)) {
                $this->setFlag('', self::FLAG_NO_DISPATCH, true);
            }
        }
    }

    /**
     * Response array via JSON
     * @param array $result
     */
    protected function _ajaxResponse($result = array())
    {
        $this->getResponse()->setBody(Zend_Json::encode($result));
        return;
    }

    /**
     * Retrives actual cart content
     * @return string
     */
    protected function _getCartContent()
    {
        try {
            $layout = Mage::app()->getLayout();
            $update = $layout->getUpdate()->addHandle('checkout_cart_index')->addHandle('default')->load();
            $layout->generateXml()->generateBlocks($layout->getNode('cart.content'));
            $session = Mage::getSingleton('core/session');
            $layout->getMessagesBlock()->addMessages($session->getMessages(true));
            $body = $layout->getBlock('cart.content')->toHtml();
        } catch (Exception $e) {
            $body = $layout->getBlock('cart.content')->toHtml();
        }
        return $body;
    }

    /**
     * Is add action
     * @return boolen
     */
    protected function _isAddAction()
    {
        return ($this->getRequest()->getActionName() === 'add');
    }

    /**
     * Fill response body by cart content
     * Required for Ajax response
     * @return AW_Mobile_CartController
     */
    protected function _goBack()
    {
        if (($quote = Mage::helper('checkout/cart')->getCart()->getQuote()) && $this->_isAddAction()) {
            foreach ($quote->getAllItems() as $item) {
                if (!$item->getPrice() && !$item->getHasError()) {
                    $item->setPrice($item->getProduct()->getFinalPrice())
                        ->calcRowTotal()
                        ->save();
                }
            }
        }

        $block = new AW_Mobile_Block_Checkout_Links();
        $linkHtml = $block->getLinkHtml();

        $result = array(
            'cart_content' => $this->_getCartContent(),
            'link_content' => $linkHtml,
        );

        $this->_ajaxResponse($result);
        return $this;
    }

    public function applyRewardpointAction(){

        $removeReward = false;
        $rewardPoint = (string) $this->getRequest()->getParam('coupon_code');
        if ($this->getRequest()->getParam('remove') == 1) {
            $removeReward = true;
        }

        if($removeReward)
        {
            Mage::getSingleton('rewardpoints/session')->setProductChecked(0);
            Mage::helper('rewardpoints/event')->setCreditPoints(0);
            Mage::helper('checkout/cart')->getCart()->getQuote()
                    ->setRewardpointsQuantity(NULL)
                    ->setRewardpointsDescription(NULL)
                    ->setBaseRewardpoints(NULL)
                    ->setRewardpoints(NULL)
                    ->collectTotals()
                    ->save();
            
            $this->_goBack();
        } 
        else
        {
            $session = Mage::getSingleton('core/session');
            $points_value = $this->getRequest()->getPost('points_to_be_used');
            $error = false;

            $customerId = Mage::getModel('customer/session')->getCustomerId();
            $reward_model = Mage::getModel('rewardpoints/stats');
            $current = $reward_model->getPointsCurrent($customerId, Mage::app()->getStore()->getId());

            if ($current < $points_value) {
                $session->addError(Mage::helper('rewardpoints')->__('Not enough points available.'));
                $error = true;
            }
            
            $step = Mage::getStoreConfig('rewardpoints/default/step_value', Mage::app()->getStore()->getId());
            $step_apply = Mage::getStoreConfig('rewardpoints/default/step_apply', Mage::app()->getStore()->getId());
            if ($step > $points_value && $step_apply){
                $session->addError(Mage::helper('rewardpoints')->__('The minimum required points is not reached.'));
                $error = true;
            }

            if ($step_apply){
                if (($points_value % $step) != 0){
                    $session->addError(Mage::helper('rewardpoints')->__('Amount of points wrongly used.'));
                    $error = true;
                }
            }

            if (Mage::getStoreConfig('rewardpoints/default/max_point_used_order', Mage::app()->getStore()->getId())){
                if ((int)Mage::getStoreConfig('rewardpoints/default/max_point_used_order', Mage::app()->getStore()->getId()) < $points_value){
                    $points_max = (int)Mage::getStoreConfig('rewardpoints/default/max_point_used_order', Mage::app()->getStore()->getId());
                    $session->addError($this->__('You tried to use %s loyalty points, but you can use a maximum of %s points per shopping cart.', $points_value, $points_max));
                    $points_value = $points_max;
                    $error = true;
                }
            }


            if(!$error)
            {                
                $quote_id = Mage::helper('checkout/cart')->getCart()->getQuote()->getId();

                Mage::getSingleton('rewardpoints/session')->setProductChecked(0);
                Mage::getSingleton('rewardpoints/session')->setShippingChecked(0);
                
                Mage::helper('rewardpoints/event')->setCreditPoints($points_value);
                
                
                Mage::helper('checkout/cart')->getCart()->getQuote()
                        ->setRewardpointsQuantity($points_value)
                        ->collectTotals()
                        ->save();
                
                $this->_goBack();
            }
            else
            {
                $this->_goBack();
            }

            
        }
    }
    
}