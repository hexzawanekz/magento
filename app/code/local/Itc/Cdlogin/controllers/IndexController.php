<?php

require_once 'Itc/Cdlogin/controllers/ModuleController.php';

class Itc_Cdlogin_IndexController extends Itc_Cdlogin_ModuleController
{
    public function indexAction()
    {
        $allStores=Mage::app()->getStores(); //get list of all stores,websites

        if (Mage::getSingleton('core/session')->getLogincd() === null) {
            Mage::getSingleton('core/session')->setLogincd(1);
        }

        $data = array(
            'result' => true,
            'sid' => Mage::getModel("core/session")->getEncryptedSessionId(),
        );

        $currentUrl = Mage::getStoreConfig('web/unsecure/base_url'); //get current domain url
        $currentSecuredUrl = Mage::getStoreConfig('web/secure/base_url');
        $requestUrls = array();
        $session = Mage::getSingleton('customer/session');
        if ($session->isLoggedIn()) {
            foreach ($allStores as $_eachStoreId => $val)
            {
                $_storeId = Mage::app()->getStore($_eachStoreId)->getId();
                $urls= Mage::app()->getStore($_storeId)->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB); //get url using store id

                if ($currentUrl != $urls && !in_array($urls, $requestUrls) &&
                    $currentSecuredUrl != $urls && !in_array($urls, $requestUrls)) {
                    $requestUrls[] = $urls;
                }
            }
            Mage::getSingleton('core/session')->setLogincd(0);
        }
        $this->getResponse()->setHeader('Pragma', 'no-cache');
        $this->getResponse()->setHeader('Content-Type', 'application/json; charset=utf-8');
        $data['urls'] = $requestUrls;
        $this->_response->setBody(json_encode($data));
    }

    public function globalLinksAction() {
        $layout = $this->getLayout();

        $response = array('result' => 1);

        $loginBlock = $layout->createBlock('Petloft_All_Block_Html_Globalheader')
            ->setTemplate('page/header/login.phtml');

//        $collectionUrl = Mage::getUrl('user/').Mage::getModel('catalog/product_url')->formatUrlKey(Mage::helper('customer')->getCustomerName())."/";
        if (Mage::getSingleton('customer/session')->isLoggedIn()) {
            $loginBlock->addLink($this->__('My Account Page'), Mage::helper('customer')->getAccountUrl(), $this->__('My Account Page'), false, array(), 11);
            $loginBlock->addLink($this->__('Address Book'), Mage::helper('customer')->getAddressUrl(), $this->__('Address Book'), false, array(), 11);
//            $loginBlock->addLink($this->__('My Collection'), $collectionUrl,$this->__('My Collection'), false, array(), 11);
            //$loginBlock->addLink($this->__('Track Order'), '#', $this->__('Track Order'), false, array(), 12);
            $loginBlock->addLink($this->__('Log Out'), Mage::helper('customer')->getLogoutUrl(), $this->__('Log Out'), false, array(), 13);
        } else {
            $fbBlock = $layout->createBlock('core/template')->setTemplate('FB-Twitter-Connect/button.link.phtml')
                ->setPosition(15);
            $layout->addBlock($fbBlock, 'fb.button.link');
            $loginBlock->addLink($this->__('Log In'), Mage::helper('customer')->getLoginUrl(), $this->__('Log In'), false, array(), 11);
            $loginBlock->addLink($this->__('Register'), Mage::helper('customer')->getRegisterUrl(), $this->__('Register'), false, array(), 10);
            $loginBlock->addLinkBlock('fb.button.link');
        }
        //$loginBlock->addCartLink();

        $response['body']['login'] = $loginBlock->renderView();
        $usernameBlock = $layout->createBlock('Petloft_All_Block_Html_Globalheader')
            ->setTemplate('page/header/username.phtml')
            ->addLink(Mage::helper('customer')->getCustomerFirstname(). ' ' . Mage::helper('customer')->getCustomerLastname(), '', 'CustomerName', false, array(), 12);

        $response['body']['username'] = $usernameBlock->renderView();
        $this->getResponse()->setHeader('Pragma', 'no-cache');
        $this->getResponse()->setBody(json_encode($response))->setHeader('Content-type', 'application/json');
    }
	public function productViewAction() {
		$id = $this->getRequest()->getParam('id');
		$cSession = Mage::getSingleton('customer/session');
		// $productCookie = $cSession->getCookie()->get('viewed_simple');
		// if($productCookie == $cSession->getProductCookie() && $cSession->getProductViewSimpleId() == $id) {
			// return ;
		// }
		// if($productCookie != $cSession->getProductCookie()) {
			// $cookieValue = mt_rand();
			// $cSession->getCookie()->set('viewed_simple',$cookieValue,0);
			// $cSession->setProductCookie($cookieValue);
		// }
		// $cSession->setProductViewSimpleId($id);
		$customerId = $cSession->getCustomer()->getId();		
		Mage::dispatchEvent('product_view_simple', array('product_id' => $id,'customer_id'=>$customerId));
	}
}