<?php

require_once 'Mage/Customer/controllers/AccountController.php';

class Itc_Cdlogin_AccountController extends Mage_Customer_AccountController
{
    /**
     * Customer logout action
     */
    public function logoutAction()
    {
        $this->_getSession()->logout()
            ->setBeforeAuthUrl(null);

        $this->_redirect('*/*/logoutSuccess');
    }
    
    public function loginAction()
    {
        // Redirect to homepage when enter customer/account/login (not used now)
        $this->_redirect('/');

        $lastUrl = $this->_getRefererUrl();
        if(strpos($lastUrl, 'customer/account') == FALSE){
            Mage::getSingleton('core/session')->setLastVisit($lastUrl);
        }
        if ($this->_getSession()->isLoggedIn()) {
            $this->_redirect('*/*/');
            return;
        }
        $this->getResponse()->setHeader('Login-Required', 'true');
        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('catalog/session');
        $this->renderLayout();
    }
    
    protected function _loginPostRedirect(){
        $session = $this->_getSession();

        if (!$session->getBeforeAuthUrl() || $session->getBeforeAuthUrl() == Mage::getBaseUrl()) {
            // Set default URL to redirect customer to
            $session->setBeforeAuthUrl(Mage::helper('customer')->getAccountUrl());
            // Redirect customer to the last page visited after logging in
            if ($session->isLoggedIn()) {
                if (!Mage::getStoreConfigFlag(
                    Mage_Customer_Helper_Data::XML_PATH_CUSTOMER_STARTUP_REDIRECT_TO_DASHBOARD
                )) {
                    $referer = $this->getRequest()->getParam(Mage_Customer_Helper_Data::REFERER_QUERY_PARAM_NAME);
                    if ($referer) {
                        // Rebuild referer URL to handle the case when SID was changed
                        $referer = Mage::getModel('core/url')
                            ->getRebuiltUrl(Mage::helper('core')->urlDecode($referer));
                        if ($this->_isUrlInternal($referer)) {
                            $session->setBeforeAuthUrl($referer);
                        }
                    }
                } else if ($session->getAfterAuthUrl()) {
                    $session->setBeforeAuthUrl($session->getAfterAuthUrl(true));
                }
            } else {
                $session->setBeforeAuthUrl(Mage::helper('customer')->getLoginUrl());
            }
        } else if ($session->getBeforeAuthUrl() == Mage::helper('customer')->getLogoutUrl()) {
            $session->setBeforeAuthUrl(Mage::helper('customer')->getDashboardUrl());
        } else {
            if (!$session->getAfterAuthUrl()) {
                $session->setAfterAuthUrl($session->getBeforeAuthUrl());
            }
            if ($session->isLoggedIn()) {
                $session->setBeforeAuthUrl($session->getAfterAuthUrl(true));
            }
        }
        $url = $session->getBeforeAuthUrl(true);
        if(strpos($url,'onestepcheckout') > 0 || strpos($url,'checkout') > 0){
            $this->_redirectUrl($url);
        }
        elseif(!$session->isLoggedIn()){
            $this->_redirectUrl(Mage::helper('customer')->getLoginUrl());
        }
        elseif(Mage::getSingleton('core/session')->getLastVisit()){
            $this->_redirectUrl(Mage::getSingleton('core/session')->getLastVisit());
        }
        else{
            $this->_redirectUrl(Mage::helper('customer')->getDashboardUrl());
        }
    }
    
    protected function _getRefererUrl()
    {
        $refererUrl = $this->getRequest()->getServer('HTTP_REFERER');
        if ($url = $this->getRequest()->getParam(self::PARAM_NAME_REFERER_URL)) {
            $refererUrl = $url;
        }
        if ($url = $this->getRequest()->getParam(self::PARAM_NAME_BASE64_URL)) {
            $refererUrl = Mage::helper('core')->urlDecode($url);
        }
        if ($url = $this->getRequest()->getParam(self::PARAM_NAME_URL_ENCODED)) {
            $refererUrl = Mage::helper('core')->urlDecode($url);
        }

        if (!$this->_isUrlInternal($refererUrl)) {
            $refererUrl = Mage::app()->getStore()->getBaseUrl();
        }
        return $refererUrl;
    }

    public function loginPostAction()
    {
        if ($login = $this->getRequest()->getParam('is_ajax')) {
            $result = array();
            if ($this->_getSession()->isLoggedIn()) {
                return;
            }
            $session = $this->_getSession();
            if ($this->getRequest()->getParams()) {
                $login = $this->getRequest()->getParam('login');
                if (!empty($login['username']) && !empty($login['password'])) {
                    try {
                        $session->login($login['username'], $login['password']);
                        if ($session->getCustomer()->getIsJustConfirmed()) {
                            $this->_welcomeCustomer($session->getCustomer(), true);
                        }
                        $result['success'] = 1;
                        if($this->getRequest()->getParam('url')){
                            $result['redirect'] = Mage::getUrl($this->getRequest()->getParam('url'));
                        }else{
                            $result['redirect'] = Mage::helper('customer')->getAccountUrl();
                        }

                    } catch (Mage_Core_Exception $e) {
                        $result['success'] = 0;
                        switch ($e->getCode()) {
                            case Mage_Customer_Model_Customer::EXCEPTION_EMAIL_NOT_CONFIRMED:
                                $value = Mage::helper('customer')->getEmailConfirmationUrl($login['username']);
                                $result['message'] = Mage::helper('customer')->__('This account is not confirmed. <a href="%s">Click here</a> to resend confirmation email.', $value);
                                break;
                            case Mage_Customer_Model_Customer::EXCEPTION_INVALID_EMAIL_OR_PASSWORD:
                                $result['message'] = $e->getMessage();
                                break;
                            default:
                                $result['message'] = $e->getMessage();
                        }
                    } catch (Exception $e) {
                        // Mage::logException($e); // PA DSS violation: this exception log can disclose customer password
                    }
                } else {
                    $result['success'] = 0;
                    $result['error'] = $this->__('Login and password are required.');
                }
            }
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));

        } else {
            parent::loginPostAction();
        }
    }

    public function forgotPasswordPostAction()
    {
        $email = (string) $this->getRequest()->getPost('email');
        if ($email) {
            if (!Zend_Validate::is($email, 'EmailAddress')) {
                $this->_getSession()->setForgottenEmail($email);
                $this->_getSession()->addError($this->__('Invalid email address.'));
                $this->_redirect('*/*/forgotpassword');
                return;
            }

            /** @var $customer Mage_Customer_Model_Customer */
            $customer = Mage::getModel('customer/customer')
                ->setWebsiteId(Mage::app()->getStore()->getWebsiteId())
                ->loadByEmail($email);

            if ($customer->getId()) {
                try {
                    $newResetPasswordLinkToken = Mage::helper('customer')->generateResetPasswordLinkToken();
                    $customer->changeResetPasswordLinkToken($newResetPasswordLinkToken);
                    $customer->sendPasswordResetConfirmationEmail();
                } catch (Exception $exception) {
                    $this->_getSession()->addError($exception->getMessage());
                    $this->_redirect('*/*/forgotpassword');
                    return;
                }
            }
            $this->_getSession()
                ->addSuccess(Mage::helper('customer')->__('If there is an account associated with %s you will receive an email with a link to reset your password.', Mage::helper('customer')->htmlEscape($email)));
            $this->_redirectReferer();
            return;
        } else {
            $this->_getSession()->addError($this->__('Please enter your email.'));
            $this->_redirect('*/*/forgotpassword');
            return;
        }
    }
}