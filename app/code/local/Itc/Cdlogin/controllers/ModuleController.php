<?php

class Itc_Cdlogin_ModuleController extends Mage_Core_Controller_Front_Action
{

    protected $savedUrl = null;

    public function preDispatch() {
        parent::preDispatch();
        $this->savedUrl = Mage::getSingleton('core/session')->getLastUrl();
        return $this;
    }

    public function postDispatch() {
        parent::postDispatch();

        Mage::getSingleton('core/session')->setLastUrl($this->savedUrl);

        return $this;
    }
}