<?php

require_once 'Itc/Cdlogin/controllers/ModuleController.php';

class Itc_Cdlogin_LoginController extends Itc_Cdlogin_ModuleController
{
    public function indexAction()
    {
        $this->getResponse()->setHeader('Pragma', 'no-cache');
        $this->getResponse()->setHeader('Content-Type', 'application/json; charset=utf-8');
        $this->getResponse()->setHeader('P3P', 'CP="NOI ADM DEV COM NAV OUR STP"');
        $data = array('success'=>1);
        $this->_response->setBody($_GET['callback'] . '('.json_encode($data).')');
    }
}