<?php

require_once 'Itc/Cdlogin/controllers/ModuleController.php';

class Itc_Cdlogin_LogoutController extends Itc_Cdlogin_ModuleController
{
    public function indexAction()
    {
        $this->getResponse()->setHeader('Pragma', 'no-cache');
        $this->getResponse()->setHeader('Content-Type', 'application/json; charset=utf-8');
        $session = Mage::getModel('customer/session');
        $cookie = Mage::getSingleton('core/cookie');
        $cookie->delete($session->getSessionName());
        $data = array('success'=>1);
        $this->_response->setBody($_POST['callback'] . '('.json_encode($data).')');
    }
}