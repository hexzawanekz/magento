<?php
class Itc_Cdlogin_Block_Cdlogin extends Mage_Core_Block_Template
{
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }
    
     public function getCdlogin()     
     { 
        if (!$this->hasData('cdlogin')) {
            $this->setData('cdlogin', Mage::registry('cdlogin'));
        }
        return $this->getData('cdlogin');
        
    }
}