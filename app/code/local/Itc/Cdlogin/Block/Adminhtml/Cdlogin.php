<?php
class Itc_Cdlogin_Block_Adminhtml_Cdlogin extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_cdlogin';
    $this->_blockGroup = 'cdlogin';
    $this->_headerText = Mage::helper('cdlogin')->__('Item Manager');
    $this->_addButtonLabel = Mage::helper('cdlogin')->__('Add Item');
    parent::__construct();
  }
}