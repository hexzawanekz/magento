<?php

class Itc_Cdlogin_Block_Adminhtml_Cdlogin_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('cdlogin_form', array('legend'=>Mage::helper('cdlogin')->__('Item information')));
     
      $fieldset->addField('title', 'text', array(
          'label'     => Mage::helper('cdlogin')->__('Title'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'title',
      ));

      $fieldset->addField('filename', 'file', array(
          'label'     => Mage::helper('cdlogin')->__('File'),
          'required'  => false,
          'name'      => 'filename',
	  ));
		
      $fieldset->addField('status', 'select', array(
          'label'     => Mage::helper('cdlogin')->__('Status'),
          'name'      => 'status',
          'values'    => array(
              array(
                  'value'     => 1,
                  'label'     => Mage::helper('cdlogin')->__('Enabled'),
              ),

              array(
                  'value'     => 2,
                  'label'     => Mage::helper('cdlogin')->__('Disabled'),
              ),
          ),
      ));
     
      $fieldset->addField('content', 'editor', array(
          'name'      => 'content',
          'label'     => Mage::helper('cdlogin')->__('Content'),
          'title'     => Mage::helper('cdlogin')->__('Content'),
          'style'     => 'width:700px; height:500px;',
          'wysiwyg'   => false,
          'required'  => true,
      ));
     
      if ( Mage::getSingleton('adminhtml/session')->getCdloginData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getCdloginData());
          Mage::getSingleton('adminhtml/session')->setCdloginData(null);
      } elseif ( Mage::registry('cdlogin_data') ) {
          $form->setValues(Mage::registry('cdlogin_data')->getData());
      }
      return parent::_prepareForm();
  }
}