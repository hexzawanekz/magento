<?php

class Itc_Cdlogin_Block_Adminhtml_Cdlogin_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'cdlogin';
        $this->_controller = 'adminhtml_cdlogin';
        
        $this->_updateButton('save', 'label', Mage::helper('cdlogin')->__('Save Item'));
        $this->_updateButton('delete', 'label', Mage::helper('cdlogin')->__('Delete Item'));
		
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('cdlogin_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'cdlogin_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'cdlogin_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    public function getHeaderText()
    {
        if( Mage::registry('cdlogin_data') && Mage::registry('cdlogin_data')->getId() ) {
            return Mage::helper('cdlogin')->__("Edit Item '%s'", $this->htmlEscape(Mage::registry('cdlogin_data')->getTitle()));
        } else {
            return Mage::helper('cdlogin')->__('Add Item');
        }
    }
}