<?php

class Itc_Cdlogin_Block_Adminhtml_Cdlogin_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

  public function __construct()
  {
      parent::__construct();
      $this->setId('cdlogin_tabs');
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('cdlogin')->__('Item Information'));
  }

  protected function _beforeToHtml()
  {
      $this->addTab('form_section', array(
          'label'     => Mage::helper('cdlogin')->__('Item Information'),
          'title'     => Mage::helper('cdlogin')->__('Item Information'),
          'content'   => $this->getLayout()->createBlock('cdlogin/adminhtml_cdlogin_edit_tab_form')->toHtml(),
      ));
     
      return parent::_beforeToHtml();
  }
}