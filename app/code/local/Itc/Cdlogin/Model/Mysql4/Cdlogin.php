<?php

class Itc_Cdlogin_Model_Mysql4_Cdlogin extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {    
        // Note that the cdlogin_id refers to the key field in your database table.
        $this->_init('cdlogin/cdlogin', 'cdlogin_id');
    }
}