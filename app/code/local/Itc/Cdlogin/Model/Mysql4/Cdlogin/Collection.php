<?php

class Itc_Cdlogin_Model_Mysql4_Cdlogin_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('cdlogin/cdlogin');
    }
}