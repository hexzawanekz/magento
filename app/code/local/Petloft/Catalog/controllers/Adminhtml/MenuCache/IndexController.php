<?php

class Petloft_Catalog_Adminhtml_MenuCache_IndexController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
        /** @var Petloft_Catalog_Model_Navigation_Renderer $modelObj */
        $modelObj = Mage::getModel('petloftcatalog/navigation_renderer');

        $modelObj->deleteAll();

        Mage::getSingleton('core/session')->addSuccess($this->__('Successfully clear html cache for menu navigation at front pages.'));

        $this->_redirectReferer();
    }

    public function updateMenuAction()
    {
        /** @var Petloft_Catalog_Model_Navigation_Html $modelObj */
        $modelObj = Mage::getModel('petloftcatalog/navigation_html');

        $result = $modelObj->update();

        if (is_array($result)) {
            $message = 'Status of processes:';
            foreach ($result as $key => $value) {
                $message .= ' "' . $key . '" (' . ($value ? 'SUCCESS' : 'FAIL') . '),';
            }
            Mage::getSingleton('core/session')->addSuccess($message);
        } elseif($result !== false) {
            Mage::getSingleton('core/session')->addError('Error: ' . $result);
        } else {
            Mage::getSingleton('core/session')->addError('Could not find any activated stores. Or it has some errors, should check "menu_caching.log" to know the reason (for developer).');
        }

        $this->_redirectReferer();
    }

    public function updateMobileAction(){
        /** @var Petloft_Catalog_Model_Navigation_Html $modelObj */
        $modelObj = Mage::getModel('petloftcatalog/navigation_html');
        $result = $modelObj->updateMobile();

        if($result === true){
            Mage::getSingleton('adminhtml/session')->addSuccess("The mobile menu was generated successfully.");
        }else{
            Mage::getSingleton('adminhtml/session')->addError($result);
        }

        $this->_redirectReferer();
    }
}