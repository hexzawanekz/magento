<?php

class Petloft_Catalog_Model_Cache_Cache extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('petloftcatalog_menucache/cache');
    }
}