<?php

/**
 * Catalog view layer model
 *
 * @category    Petloft
 * @package     Petloft_Catalog
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Petloft_Catalog_Model_Bestseller extends Mage_Catalog_Model_Layer
{

    /**
     * Initialize product collection
     *
     * @param Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Collection $collection
     * @return Mage_Catalog_Model_Layer
     */
    public function prepareProductCollection($collection)
    {
        $collection
            ->addAttributeToSelect(Mage::getSingleton('catalog/config')->getProductAttributes())
            ->addMinimalPrice()
            ->addFinalPrice()
            ->addTaxPercents()
            ->addUrlRewrite($this->getCurrentCategory()->getId());

        /*Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($collection);
        Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($collection);*/

        return $this;
    }
}