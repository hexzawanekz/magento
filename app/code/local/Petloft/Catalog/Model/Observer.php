<?php

/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Sales to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_Sales
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
 */
class Petloft_Catalog_Model_Observer
{
    /**
     * @param $observer
     * event: save after simple product (petloft_catalog_save_after_save_super_prices)
     * Change pricing value and configurable value after save product
     * ! This function is very complex :(
     */
    public function saveSuperPrices($observer)
    {
        try {
            /** @var Mage_Catalog_Model_Product $product */
            $product = $observer->getEvent()->getProduct();
            if ($product->getTypeId() !== Mage_Catalog_Model_Product_Type::TYPE_SIMPLE) {
                return;
            }

            // Get parent Ids
            $parentIds = Mage::getResourceSingleton('catalog/product_type_configurable')
                ->getParentIdsByChild($product->getId());
            if (empty($parentIds)) {
                return;
            }

            foreach ($parentIds as $id) {
                /** @var Mage_Catalog_Model_Product $parent */
                $parent = Mage::getModel('catalog/product')->load($id);
                if (!$parent->getId()) {
                    continue;
                }
                $config = $this->getSuperPrices($product, $parent);
                if ($config) {
                    // Set supper attribute field
                    $parent->setConfigurableAttributesData($config);
                }
                // Re-set best value for configurable product
                /** @var Mage_Catalog_Model_Product $bestValue */
                $bestValue = $this->getBestProduct($parent, $product);

                // Get new best value
                if ($bestValue) {
                    if ($bestValue->getId() == $product->getId()){
                        $bestPrice = $product->getFinalPrice();
                        $msrp = $product->getMsrp();
                    } else {
                        $bestPrice = $bestValue->getFinalPrice();
                        $msrp = $bestValue->getMsrp();
                    }
                    $parent->setPrice($bestPrice);
                    $parent->setMsrp($msrp);
                }

                // Save configurable product
                $parent->save();
            }
        } catch (Exception $e) {
            Mage::log($e->getMessage());
        }
    }

    /**
     * @param $parent
     * @return array
     * for save simple
     */
    public function getSuperPrices($product, $parent)
    {
        $attributes = $parent->getTypeInstance(true)
            ->getConfigurableAttributesAsArray($parent);

        $configurable = Mage::getModel('catalog/product_type_configurable')->setProduct($parent);
        $simplesCollection = $configurable->getUsedProductCollection()
            ->addAttributeToSelect('*')
            ->addFilterByRequiredOptions();

        // Get best product
        $bestProduct = $this->getBestProduct($parent, $product);
        if ($bestProduct) {
            if ($bestProduct->getId() == $product->getId()) {
                $parentPrice = $product->getFinalPrice();
            } else {
                $parentPrice = $bestProduct->getFinalPrice();
            }
        } else {
            $parentPrice = $parent->getFinalPrice();
        }
        if (!$attributes) {
            return array();
        } else {
            // Hide price if needed
            $count = 0;

            // Re-set attributes array
            foreach ($attributes as &$attribute) {
                if (isset($attribute['values']) && is_array($attribute['values'])) {
                    $attributeCode = isset($attribute['attribute_code']) ? $attribute['attribute_code'] : '';
                    if ($count == 0) {
                        $count++;
                        foreach ($attribute['values'] as &$attributeValue) {
                            $valueIndx = isset($attributeValue['value_index']) ? $attributeValue['value_index'] : '';
                            foreach ($simplesCollection as $sProduct) {
                                if ($sProduct->getData($attributeCode) == $valueIndx) {
                                    if ($product->getId() == $sProduct->getId()) {
                                        $currentPrice = $product->getFinalPrice();
                                    } else {
                                        $currentPrice = $sProduct->getFinalPrice();
                                    }
                                    $attributeValue['pricing_value'] = $currentPrice - $parentPrice;
                                    $attributeValue['is_percent'] = 0;
                                    $attributeValue['can_edit_price'] = 0;
                                    $attributeValue['can_read_price'] = 1;
                                }
                            }
                        }
                    } else {
                        foreach ($attribute['values'] as &$attributeValue) {
                            $valueIndx = isset($attributeValue['value_index']) ? $attributeValue['value_index'] : '';
                            foreach ($simplesCollection as $sProduct) {
                                if ($sProduct->getData($attributeCode) == $valueIndx) {
                                    $attributeValue['pricing_value'] = 0;
                                    $attributeValue['is_percent'] = 0;
                                    $attributeValue['can_edit_price'] = 0;
                                    $attributeValue['can_read_price'] = 1;
                                }
                            }
                        }
                    }
                }
            }
        }
        return $attributes;
    }

    /**
     * @param $observer
     * event: save before configurable product (petloft_catalog_save_before_save_best_value)
     * ! This function is very complex :(
     * @throws Exception
     */
    public function saveBestValue($observer)
    {
        $product = $observer->getEvent()->getProduct();
        if ($product->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE) {
            // Set supper attribute field
            $configAttributes = $this->getConfigurableAttributeValues($product);
            if ($configAttributes) {
                $product->setConfigurableAttributesData($configAttributes);
            }

            // Re-set best value for configurable product
            $configChild = $product->getConfigurableProductsData();
            /** @var Mage_Catalog_Model_Product $bestValue */
            $bestValue = $this->getBestProductAfterCreate($configChild);
            if ($bestValue) {
                $product->setPrice($bestValue->getFinalPrice());
                $product->setMsrp($bestValue->getMsrp());
            }
        }
    }

    /**
     * @param $parent
     * @return $configAttributes
     * for save configurable
     */
    public function getConfigurableAttributeValues($parent)
    {
        $configAttributes = $parent->getConfigurableAttributesData();
        $configChild = $parent->getConfigurableProductsData();

        // Child array
        $children = array();
        $bestProduct = $this->getBestProductAfterCreate($configChild);
        if ($bestProduct) {
            $parentPrice = $bestProduct->getFinalPrice();
        } else {
            $parentPrice = $parent->getFinalPrice();
        }

        $count = 0;
        $pos = 0;
        // Get attribute at first (if user drag the attribute)
        foreach ($configAttributes as $attribute) {
            if ($attribute['position'] == 0) {
                $pos = $count;
            } else {
                $count++;
            }
        }

        // Set data for children list
        foreach ($configChild as $id => $data) {
            if (!isset($data[$pos]['value_index'])) {
                continue;
            }
            $children[$data[$pos]['value_index']] = Mage::getModel('catalog/product')->load($id)->getFinalPrice();
        }

        if (empty($children)) {
            return null;
        }
        // Re-set config attributes array
        $count = 0;
        foreach ($configAttributes as &$attribute) {
            if (isset($attribute['values']) && is_array($attribute['values'])) {
                if ($attribute['position'] == 0 && $count == 0) {
                    $count++;
                    foreach ($attribute['values'] as &$attributeValue) {
                        $valueIndx = isset($attributeValue['value_index']) ? $attributeValue['value_index'] : '';
                        foreach ($children as $indx => $finalPrice) {
                            if ($indx == $valueIndx) {
                                $attributeValue['pricing_value'] = $finalPrice - $parentPrice;
                                $attributeValue['is_percent'] = 0;
                                $attributeValue['can_edit_price'] = 0;
                                $attributeValue['can_read_price'] = 1;
                            }
                        }
                    }
                } else {
                    foreach ($attribute['values'] as &$attributeValue) {
                        $attributeValue['pricing_value'] = 0;
                        $attributeValue['is_percent'] = 0;
                        $attributeValue['can_edit_price'] = 0;
                        $attributeValue['can_read_price'] = 1;
                    }
                }
            }
        }
        return $configAttributes;
    }

    /**
     * Rewrite get best product function
     * @param null $_product
     * @param bool|false $showOffText
     * @return bool|Mage_Core_Model_Abstract
     * WT-596: make function simpler
     */
    public function getBestProduct($_product = null, $current_simple)
    {
        if (($_product != null) && ($_product->getTypeId() == "configurable")) {
            $childIds = $_product->getTypeInstance()->getUsedProductIds();
            $_bestProduct = false;
            foreach ($childIds as $childId) {
                /** @var Mage_Catalog_Model_Product $_simpleproduct */
                $_simpleproduct = Mage::getModel('catalog/product')->load($childId);

                /* Get Unit */
                $_productMeasurement = $_simpleproduct->getAdminAttributeText('pet_food_cost_per_type');
                if (strtolower($_productMeasurement) == "kilo") {
                    $_simpleproduct->setUnit($_simpleproduct->getWeight() / 1000);
                } else if (strtolower($_productMeasurement) == "litre") {
                    $_simpleproduct->setUnit($_simpleproduct->getWeight());
                } else if ($_simpleproduct->getData('pet_food_packaging_count') != null) {
                    $_simpleproduct->setUnit($_simpleproduct->getData('pet_food_packaging_count'));
                } else {
                    $_simpleproduct->setUnit(1);
                }

                /* if simple product equal current simple: price will be updated */
                if ($_simpleproduct->getId() == $current_simple->getId()) {
                    $final_price = $current_simple->getFinalPrice();
                } else {
                    $final_price = $_simpleproduct->getFinalPrice();
                }

                /* Set unit based on price and Unit above */
                $_simpleproduct->setPricePerUnit($final_price / $_simpleproduct->getUnit());

                /* Compare to find best product */
                if (!$_bestProduct && $_simpleproduct->getData('is_in_stock')) {
                    $_bestProduct = $_simpleproduct;
                } else {
                    if (($_simpleproduct->getData('is_in_stock')) && ($_simpleproduct->getPricePerUnit() < $_bestProduct->getPricePerUnit())) {
                        $_bestProduct = $_simpleproduct;
                    }
                }
            }
            return $_bestProduct;
        }
    }

    /**
     * @param null $_product
     * @param bool|false $showOffText
     * @return bool
     */
    public function getBestProductAfterCreate($configChild)
    {
        if ($configChild != null) {
            $_bestProduct = false;
            foreach ($configChild as $key => $value) {
                /** @var Mage_Catalog_Model_Product $_simpleproduct */
                $_simpleproduct = Mage::getModel('catalog/product')->load($key);

                /* Get Unit */
                $_productMeasurement = $_simpleproduct->getAdminAttributeText('pet_food_cost_per_type');
                if (strtolower($_productMeasurement) == "kilo") {
                    $_simpleproduct->setUnit($_simpleproduct->getWeight() / 1000);
                } else if (strtolower($_productMeasurement) == "litre") {
                    $_simpleproduct->setUnit($_simpleproduct->getWeight());
                } else if ($_simpleproduct->getData('pet_food_packaging_count') != null) {
                    $_simpleproduct->setUnit($_simpleproduct->getData('pet_food_packaging_count'));
                } else {
                    $_simpleproduct->setUnit(1);
                }
                /* Set Unit */
                $_simpleproduct->setPricePerUnit($_simpleproduct->getFinalPrice() / $_simpleproduct->getUnit());

                /* Compare to find best product */
                if (!$_bestProduct && $_simpleproduct->getData('is_in_stock')) {
                    $_bestProduct = $_simpleproduct;
                } else {
                    if (($_simpleproduct->getData('is_in_stock')) && ($_simpleproduct->getPricePerUnit() < $_bestProduct->getPricePerUnit())) {
                        $_bestProduct = $_simpleproduct;
                    }
                }
            }
            return $_bestProduct;
        }
    }

    /*Relink child products to their configurable*/
    public function catalogControllerProductInitBefore(Varien_Event_Observer $observer)
    {
        $pid = Mage::app()->getRequest()->getParam('id');
        /** @var Mage_Catalog_Model_Product $product */
        $product = Mage::getModel('catalog/product')->load($pid);
        if ($product->getVisibility() == 1) {
            $parentIds = Mage::getModel('catalog/product_type_configurable')->getParentIdsByChild($pid);
            if (count($parentIds) > 0) {
                $firstParentId = $parentIds[0];
                /** @var Mage_Catalog_Model_Product $parentProduct */
                $parentProduct = Mage::getModel('catalog/product')->load($firstParentId);
                $parentUrl = $parentProduct->getProductUrl();
                Mage::app()->getFrontController()->getResponse()->setRedirect($parentUrl)->sendResponse();
            }
        }
    }
    /*End Relink child products to their configurable*/
}
