<?php

class Petloft_Catalog_Model_Cronjob
{
    const LOG_FILENAME = "menu_cron_logs.log";

    /**
     * Regenerate html cache for menu navigation bar, both desktop and mobile versions
     * Start on each 8h15 AM every days
     */
    public function regenerateMenuCache()
    {
        // set max_execution_time
        ini_set('max_execution_time', 0);

        // set memory_limit
        ini_set('memory_limit', '-1');

        /** @var Petloft_Catalog_Model_Navigation_Html $modelObj */
        $modelObj = Mage::getModel('petloftcatalog/navigation_html');
        /** @var Mage_Core_Model_Date $date */
        $date = Mage::getModel('core/date');

        $today = $date->date('d/m/Y H:i A');

        Mage::log("--- Start regenerating menu html cache on {$today}", null, self::LOG_FILENAME);

        // update for mobile
        $rs1 = $modelObj->updateMobile();
        if ($rs1 === true) {
            Mage::log("Mobile: SUCCESS", null, self::LOG_FILENAME);
        } else {
            Mage::log("Mobile: FAIL", null, self::LOG_FILENAME);
            Mage::log("- Error: " . $rs1, null, self::LOG_FILENAME);
        }

        // update for desktop
        $rs2 = $modelObj->update();
        if (is_array($rs2)) {
            Mage::log("Desktop: SUCCESS", null, self::LOG_FILENAME);
        } elseif ($rs2 !== false) {
            Mage::log("Desktop: FAIL", null, self::LOG_FILENAME);
            Mage::log("- Error: " . $rs2, null, self::LOG_FILENAME);
        } else {
            Mage::log("Desktop: FAIL", null, self::LOG_FILENAME);
        }

        Mage::log("--- End", null, self::LOG_FILENAME);
    }
}