<?php

$installer = $this;
$installer->startSetup();

$installer->run("
    DROP TABLE IF EXISTS `instagram_media_recents`;
");

$installer->run("
    CREATE TABLE `instagram_media_recents` (
      `id` int(11) NOT NULL auto_increment,
      `store_id` int(11) NOT NULL,
      `link` text NOT NULL,
      `thumb` text NOT NULL,
      `caption` text NULL,
      PRIMARY KEY  (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");
$installer->endSetup();