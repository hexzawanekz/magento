<?php

$installer = $this;
$installer->startSetup();
$attrGroupName = 'Whatsnew Details';

if($installer->getAttribute('catalog_product','features')) {
	$installer->removeAttribute('catalog_product', 'features');
}

$installer->updateAttribute('catalog_product', 'ingredients', array(    
    'label'       => 'Ingredients',
    'type'        => 'text',
    'input'       => 'textarea',
    'visible'     => true,
    'required'	=> false,
    'user_defined'  => true,
    'wysiwyg_enabled'           => true,
    'is_html_allowed_on_front'   => true,
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE
));

$installer->addAttribute('catalog_product', 'features', array(    
    'label'       => 'Features',
    'type'        => 'text',
    'input'       => 'textarea',
    'visible'     => true,
    'required'	=> false,
    'user_defined'  => true,
    'wysiwyg_enabled'           => true,
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE
));

$installer->addAttributeToGroup('catalog_product', '*baby diapers', $attrGroupName, 'pet_food_packaging_type');
$installer->addAttributeToGroup('catalog_product', '*baby diapers', $attrGroupName, 'pet_food_cost_per_type');
$installer->addAttributeToGroup('catalog_product', '*baby diapers', $attrGroupName, 'pet_food_packaging_count');
$installer->addAttributeToGroup('catalog_product', '*baby food', $attrGroupName, 'ingredients');

$installer->addAttributeToGroup('catalog_product', '*baby diapers', $attrGroupName, 'features');
$installer->addAttributeToGroup('catalog_product', '*baby care', $attrGroupName, 'features');
$installer->addAttributeToGroup('catalog_product', '*baby general products', $attrGroupName, 'features');

$installer->endSetup();