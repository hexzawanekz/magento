<?php

$installer = $this;
$installer->startSetup();
$attrGroupName = 'Shipping Details';
if ($installer->getAttribute('catalog_product', 'shipping_duration')) {
    $installer->removeAttribute('catalog_product', 'shipping_duration');
}
$installer->addAttribute(Mage_Catalog_Model_Product::ENTITY,
    'shipping_duration',
    array(
        'label' => 'Shipping duration',
        'type' => 'text',
        'input' => 'select',
        'visible' => true,
        'required' => false,
        'user_defined' => true,
        'wysiwyg_enabled' => true,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
        'option' => array(
            'value' => array(
                'simple_ship' => array('Simple Product', 'Simple Product'),
                'special_ship' => array('Dropship and Special Product', 'Dropship and Special Product')
            )
        )
    )
);
$attributeId = $installer->getAttributeId('catalog_product', 'shipping_duration');
$allAttributeSetIds = $installer->getAllAttributeSetIds('catalog_product');

foreach ($allAttributeSetIds as $attributeSetId) {

    $attributeGroup = $installer->getAttributeGroup('catalog_product', $attributeSetId, $attrGroupName);

    if (!$attributeGroup) {
        $installer->addAttributeGroup('catalog_product', $attributeSetId, $attrGroupName, 1000);
    }

    $installer->addAttributeToSet('catalog_product', $attributeSetId, $attrGroupName, $attributeId);
}

$installer->endSetup();

