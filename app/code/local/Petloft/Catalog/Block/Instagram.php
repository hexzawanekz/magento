<?php

class Petloft_Catalog_Block_Instagram extends Mage_Core_Block_Template
{
    public function getWidgetData()
    {
        /** @var Petloft_Catalog_Model_Instagram $model */
        $model = Mage::getModel('petloftcatalog/instagram');
        return $model->getMediaRecent();
    }

    public function getTitle()
    {
        return Mage::getStoreConfig('petloftcatalog/instagram_widget/widget_title');
    }

    public function getMobileTitle()
    {
        return Mage::getStoreConfig('petloftcatalog/instagram_widget/widget_mobile_title');
    }
}