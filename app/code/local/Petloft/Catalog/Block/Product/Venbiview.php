<?php

class Petloft_Catalog_Block_Product_Venbiview extends Mage_Catalog_Block_Product_View
{
	public function setVenbiTemplate($template){
		if($this->getProduct()->getTypeID()==Mage_Catalog_Model_Product_Type::TYPE_SIMPLE) {
			$this->setTemplate('catalog/product/view-simple.phtml');
		} else {
			$this->setTemplate($template);
		}
	}
}