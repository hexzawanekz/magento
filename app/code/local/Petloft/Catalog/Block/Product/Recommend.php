<?php

/**
 * Product list
 *
 * @category   Petloft
 * @package    Petloft_Catalog
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Petloft_Catalog_Block_Product_Recommend extends Petloft_Catalog_Block_Product_List
{
	/**
     * Get catalog layer model
     *
     * @return Petloft_Catalog_Model_Recommend
     */
    public function getLayer()
    {
        $layer = Mage::registry('current_layer');
        if ($layer) {
            return $layer;
        }
        return Mage::getSingleton('petloftcatalog/recommend');
    }

    public function getRecommendAllowAttributes($parent)
    {
        return $parent->getTypeInstance(true)
            ->getConfigurableAttributes($parent);
    }

    /**
     * Get Allowed Products
     *
     * @return array
     */
    public function getRecommendAllowProducts($parent)
    {
        // if (!$this->getData('allow_products')) {
            $products = array();
            $skipSaleableCheck = Mage::helper('catalog/product')->getSkipSaleableCheck();
            $allProducts = $parent->getTypeInstance(true)
                ->getUsedProducts(null, $parent);
            foreach ($allProducts as $product) {
                if ($product->isSaleable() || $skipSaleableCheck) {
                    $products[] = $product;
                }
            }
            $this->setAllowProducts($products);
        // }
        return $this->getData('allow_products');
    }

    public function getRecommendOptionAttribute($parent, $childId)
    {
        $allowProducts = $this->getRecommendAllowProducts($parent);
        $options = array();
        foreach($allowProducts as $product)
        {
            $productId  = $product->getId();
            $allowAttributes = $this->getRecommendAllowAttributes($parent);
            foreach ($allowAttributes as $attribute)
            {
                $productAttribute   = $attribute->getProductAttribute();
                $productAttributeId = $productAttribute->getId();
                $attributeValue     = $product->getData($productAttribute->getAttributeCode());
                if (!isset($options[$productAttributeId])) {
                    $options[$productAttributeId] = array();
                }

                if (!isset($options[$productAttributeId][$attributeValue])) {
                    $options[$productAttributeId][$attributeValue] = array();
                }
                $options[$productAttributeId][$attributeValue][] = $productId;
            }
        }

        $e = array();
        $allowAttributes = $this->getRecommendAllowAttributes($parent);
        foreach ($allowAttributes as $attribute)
        {
            $productAttribute = $attribute->getProductAttribute();
            $attributeId = $productAttribute->getId();

            $prices = $attribute->getPrices();

            if($prices && is_array($prices)) {
                foreach ($prices as $value)
                {
                    if(isset($options[$attributeId])) {
                        if(isset($options[$attributeId][$value['value_index']])) {
                            $productsIndex = $options[$attributeId][$value['value_index']];
                            if(in_array($childId, $productsIndex))
                            {
                                $e['attributeId'] = $attributeId;
                                $e['attributeValue'] = $value['value_index'];
                                return $e;
                            }
                        }
                    }
                }
            }

        }
        return false;
    }
}