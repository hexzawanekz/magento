<?php

/**
 * Petloft extension for Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * 
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade
 * the Petloft Inventory module to newer versions in the future.
 * If you wish to customize the Petloft Inventory module for your needs
 * please refer to http://www.magentocommerce.com for more information.
 *
 * @category   Petloft
 * @package    Petloft_Inventory
 * @copyright  Copyright (C) 2011 Petloft Web ltd (http://Petloftweb.com/)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
$installer = $this;
$installer->startSetup();

$installer->getConnection()->addColumn($installer->getTable('cataloginventory_stock_item'), 'person_limit',
                                                            "INT unsigned NOT NULL default '0'");
$installer->run("
CREATE TABLE `{$this->getTable('cataloginventory_stock_item_limit')}` (
    `item_id` int unsigned NOT NULL auto_increment PRIMARY KEY,
    `product_id` int(10) unsigned NOT NULL default '0',
    `customer_id` int(10) unsigned NOT NULL default '0',
    `qty` INT unsigned NOT NULL default '0',
    KEY `FK_CATALOGINVENTORY_STOCK_ITEM_LIMIT_PRODUCT` (`product_id`),
    KEY `FK_CATALOGINVENTORY_STOCK_ITEM_LIMIT_CUSTOMER` (`customer_id`),
    CONSTRAINT `FK_CATALOGINVENTORY_STOCK_ITEM_LIMIT_PRODUCT` FOREIGN KEY (`product_id`)
    REFERENCES `{$this->getTable('catalog_product_entity')}` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `FK_CATALOGINVENTORY_STOCK_ITEM_LIMIT_CUSTOMER` FOREIGN KEY (`customer_id`)
    REFERENCES `{$this->getTable('customer_entity')}` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Inventory Stock Item Limit';
");

$installer->endSetup();
