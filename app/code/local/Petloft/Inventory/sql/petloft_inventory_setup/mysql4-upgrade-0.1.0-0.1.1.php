<?php

/**
 * Petloft extension for Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade
 * the Petloft Inventory module to newer versions in the future.
 * If you wish to customize the Petloft Inventory module for your needs
 * please refer to http://www.magentocommerce.com for more information.
 *
 * @category   Petloft
 * @package    Petloft_Inventory
 * @copyright  Copyright (C) 2011 Petloft Web ltd (http://Petloftweb.com/)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
$installer = $this;
$installer->startSetup();


$installer->run("
CREATE TABLE `{$this->getTable('cataloginventory_stock_item_temp')}` (
    `item_id` int unsigned NOT NULL auto_increment PRIMARY KEY,
    `product_id` int(10) unsigned NOT NULL default '0',
    `product_name` varchar(255),
    `netsuite_name` varchar(255),
    `qty` INT unsigned NOT NULL default '0',
    created_at datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Inventory Stock Item Temp';
");

$installer->endSetup();
