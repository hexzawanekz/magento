<?php

/**
 * Petloft extension for Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade
 * the Petloft Inventory module to newer versions in the future.
 * If you wish to customize the Petloft Inventory module for your needs
 * please refer to http://www.magentocommerce.com for more information.
 *
 * @category   Petloft
 * @package    Petloft_Inventory
 * @copyright  Copyright (C) 2013
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Widget Grid
 *
 * @category   Petloft
 * @package    Petloft_Inventory
 * @subpackage Block
 * @author
 */

class Petloft_Inventory_Block_Adminhtml_Inventory_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * Initialize grid widget
     *
     * @return void
     */
    public function __construct($attributes = array())
    {
        parent::__construct($attributes);
        $this->setTemplate('inventory/widget/grid.phtml');
    }

    /**
     * prepare grid collection
     *
     * @return void
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('inventory/temp')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * prepare grid column
     *
     * @return void
     */
    protected function _prepareColumns()
    {

        $this->addColumn('product_id',
                         array(
            'header' => Mage::helper('inventory')->__('Product Id'),
            'width' => '80px',
            'type' => 'text',
            'index' => 'product_id',
            'filter' => false,
        ));

        $this->addColumn('product_name',
                         array(
            'header' => Mage::helper('inventory')->__('Product Name'),
            'index' => 'product_name',
            'filter' => false,
        ));

        $this->addColumn('netsuite_name',
                         array(
            'header' => Mage::helper('inventory')->__('Netsuite Name'),
            'index' => 'netsuite_name',
            'filter' => false,
        ));

        $this->addColumn('qty',
                         array(
            'header' => Mage::helper('inventory')->__('QTY'),
            'index' => 'qty',
            'type' => 'int',
            'width' => '80px',
            'filter' => false,
        ));

        $this->addColumn('created_at',
                         array(
            'header' => Mage::helper('inventory')->__('Uploaded Date'),
            'index' => 'created_at',
            'type' => 'datetime',
            'width' => '180px',
            'filter' => false,
        ));
        return parent::_prepareColumns();
    }

    /**
     * Helper
     *
     * @return Mage_Catalog_Helper_Data
     */
    protected function _helper()
    {
        return Mage::helper('inventory');
    }

    /**
     * get search button
     *
     * @return void
     */
    public function getSearchButtonHtml()
    {
        return;
    }

    /**
     * get reset filter button
     *
     * @return void
     */
    public function getResetFilterButtonHtml()
    {
        return;
    }

    /**
     * Prepare Mass Action
     *
     * @return \Petloft_Inventory_Block_Adminhtml_Inventory_Grid
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('item_id');
        $this->getMassactionBlock()->setFormFieldName('item_ids');
        $this->getMassactionBlock()->setUseSelectAll(false);

        if (Mage::getSingleton('admin/session')->isAllowed('catalog/inventory/actions/delete')) {
            $this->getMassactionBlock()->addItem('delete_item',
                                                 array(
                'label' => Mage::helper('inventory')->__('Delete selected items'),
                'url' => $this->getUrl('*/inventory/massDelete'),
                'confirm' => Mage::helper('inventory')->__('Are you sure?')
            ));
        }

        if (Mage::getSingleton('admin/session')->isAllowed('catalog/inventory/actions/add')) {
            $this->getMassactionBlock()->addItem('add_inventory',
                                                 array(
                'label' => Mage::helper('inventory')->__('Add inventory to stock'),
                'url' => $this->getUrl('*/inventory/massAdd'),
                'confirm' => Mage::helper('inventory')->__('Are you sure?')
            ));
        }

        if (Mage::getSingleton('admin/session')->isAllowed('catalog/inventory/actions/subtract')) {
            $this->getMassactionBlock()->addItem('subtract_inventory',
                                                 array(
                'label' => Mage::helper('inventory')->__('Subtract inventory from stock'),
                'url' => $this->getUrl('*/inventory/massSubtract'),
                'confirm' => Mage::helper('inventory')->__('Are you sure?')
            ));
        }

        if (Mage::getSingleton('admin/session')->isAllowed('catalog/inventory/actions/update')) {
            $this->getMassactionBlock()->addItem('update_inventory',
                                                 array(
                'label' => Mage::helper('inventory')->__('Update inventory in stock'),
                'url' => $this->getUrl('*/inventory/massUpdate'),
                'confirm' => Mage::helper('inventory')->__('Are you sure?')
            ));
        }

        return $this;
    }
}