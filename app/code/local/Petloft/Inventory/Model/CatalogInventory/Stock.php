<?php

/**
 * Petloft extension for Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade
 * the Petloft Inventory module to newer versions in the future.
 * If you wish to customize the Petloft Inventory module for your needs
 * please refer to http://www.magentocommerce.com for more information.
 *
 * @category   Petloft
 * @package    Petloft_Inventory
 * @copyright  Copyright (C) 2013
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Rewrited inventory stock model
 *
 * @category   Petloft
 * @package    Petloft_Inventory
 * @subpackage Model
 * @author     
 */
class Petloft_Inventory_Model_CatalogInventory_Stock extends Mage_CatalogInventory_Model_Stock
{

    protected $_customerId;

    /**
     * Subtract product qtys from stock.
     * Return array of items that require full save
     *
     * @param array $items items to sale
     *
     * @return array
     */
    public function registerProductsSale($items)
    {
        $qtys = $this->_prepareProductQtys($items);
        $item = Mage::getModel('cataloginventory/stock_item');
        $this->_getResource()->beginTransaction();
        $stockInfo = $this->_getResource()->getProductsStock($this, array_keys($qtys), true);
        $fullSaveItems = array();
        $registedItem = array();

        foreach ($stockInfo as $itemInfo) {
            $item->setData($itemInfo);
            $productId = $item->getProductId();

            if (!$item->checkQty($qtys[$item->getProductId()])) {
                $this->_revertItem($registedItem);
                Mage::throwException(Mage::helper('cataloginventory')->__('Not all products are available in the requested quantity'));
            }

            if (!$item->checkPersonLimit($qtys[$productId], $this->getCustomerId())) {
                $this->_revertItem($registedItem);
                $productName = Mage::getModel('catalog/product')->load($productId)->getName();
                $errorMessage = sprintf($productName. ' is only %s items are allowed per person.', $item->getPersonLimit());

                $this->throwException($errorMessage);
            }


            $item->subtractQty($qtys[$item->getProductId()]);
            if (!$item->verifyStock() || $item->verifyNotification()) {
                $fullSaveItems[] = clone $item;
            }

            Mage::getModel('inventory/person_limit')->updateQty($productId, $this->getCustomerId(), $qtys[$productId]);
            $registedItem[$productId] = $qtys[$productId];
        }
        $this->_getResource()->correctItemsQty($this, $qtys, '-');
        $this->_getResource()->commit();
        return $fullSaveItems;
    }

    /**
     * Set Customer id
     *
     * @param int $id customer id
     *
     * @return Petloft_Inventory_Model_CatalogInventory_Stock
     */
    public function setCustomerId($id)
    {
        $this->_customerId = $id;
        return $this;
    }

    /**
     * Get Customer Id
     *
     * @return int
     */
    public function getCustomerId()
    {
        return $this->_customerId;
    }    

    /**
     * Throw stock exception
     *
     * @param string $message message
     *
     * @return Petloft_Inventory_Model_CatalogInventory_Stock
     */
    public function throwException($message)
    {
        if (Mage::app()->getStore()->getId() != Mage_Core_Model_App::ADMIN_STORE_ID) {
            throw new Petloft_Inventory_Exception(Mage::helper('cataloginventory')->__($message));
        }
        return $this;
    }

    /**
     * Revert Products Sale
     *
     * @param unknown_type $items items
     *
     * @return Petloft_Inventory_Model_CatalogInventory_Stock
     */
	/**
     *
     * @param unknown_type $items
     */
    public function revertProductsSale($items)
    {        
        foreach ($items as $productId => $item) {
            if (empty($item['item'])) {
                $stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($productId);
            } else {
                $stockItem = $item['item'];
            }
            $qty = $item['qty'];
            $this->_getResource()->correctItemsQty($this, array($productId => $qty), '+');
            Mage::getModel('inventory/person_limit')->updateQty($productId, $this->getCustomerId(), $qty, '-');
        }
        return $this;
    }

    /**
     * Revert Products Sale
     *
     * @param array $qtys qtys
     *
     * @return Petloft_Inventory_Model_CatalogInventory_Stock
     */
    protected function _revertItem($qtys)
    {
        if (count($qtys) > 0) {
            foreach ($qtys as $productId => $qty) {
                $stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($productId);
                $this->_getResource()->correctItemsQty($this, array($productId => $qty), '+');               
                Mage::getModel('inventory/person_limit')->updateQty($productId, $this->getCustomerId(), $qty, '-');
            }
        }
        return $this;
    }
}