<?php

/**
 * Petloft extension for Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade
 * the Petloft Inventory module to newer versions in the future.
 * If you wish to customize the Petloft Inventory module for your needs
 * please refer to http://www.magentocommerce.com for more information.
 *
 * @category   Petloft
 * @package    Petloft_Inventory
 * @copyright  Copyright (C) 2013
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Rewrited inventory stock item model
 *
 * @category   Petloft
 * @package    Petloft_Inventory
 * @subpackage Model
 * @author
 */
class Petloft_Inventory_Model_CatalogInventory_Stock_Item extends Mage_CatalogInventory_Model_Stock_Item
{
    protected $_quote;

    /**
     * Check id product available per person
     *
     * @param int $qty        ordered qty
     * @param int $customerId customer id
     *
     * @return bool
     */
    public function checkPersonLimit($qty, $customerId)
    {
        if (!$this->getPersonLimit()) {
            return true;
        }
        $limit = Mage::getModel('inventory/person_limit')
                ->loadLimit($this->getProductId(), $customerId);
        $preQty = ($limit->getId()) ? $limit->getQty() : 0;
        $qty = max(1, $qty);
        return ($this->getPersonLimit() >= ($qty + $preQty));
    }

    /**
     * Get available person qty
     *
     * @param int $customerId customerId
     *
     * @return int
     */
    public function getAvailablePersonQty($customerId)
    {
        $limit = Mage::getModel('inventory/person_limit')
                ->loadLimit($this->getProductId(), $customerId);
        $personalLimit = ($limit->getId()) ? max(0, $this->getPersonLimit() - $limit->getQty()) : $this->getPersonLimit();
        return min($this->getPersonLimit(), $personalLimit);
    }

    /**
     * Checking quote item quantity
     *
     * @param mixed $qty        quantity of this item (item qty x parent item qty)
     * @param mixed $summaryQty quantity of this product in whole shopping cart which should
     *                          be checked for stock availability
     * @param mixed $origQty    original qty of item (not multiplied on parent item qty)
     *
     * @return Varien_Object
     */
    public function checkQuoteItemQty($qty, $summaryQty, $origQty = 0)
    {
        $result = new Varien_Object();
        $result->setHasError(false);

        if (!is_numeric($qty)) {
            $qty = Mage::app()->getLocale()->getNumber($qty);
        }

        /**
         * Check quantity type
         */
        $result->setItemIsQtyDecimal($this->getIsQtyDecimal());
        try {
            if (!$this->getIsQtyDecimal()) {
                $result->setHasQtyOptionUpdate(true);
                $qty = intval($qty);

                /**
                 * Adding stock data to quote item
                 */
                $result->setItemQty($qty);

                if (!is_numeric($qty)) {
                    $qty = Mage::app()->getLocale()->getNumber($qty);
                }
                $origQty = intval($origQty);
                $result->setOrigQty($origQty);
            }

            if ($this->getMinSaleQty() && ($qty) < $this->getMinSaleQty()) {
                $result->setQuoteMessageIndex('qty');
                throw new Petloft_Inventory_Exception(Mage::helper('cataloginventory')
                        ->__('The minimum quantity allowed for purchase is %s.', $this->getMinSaleQty() * 1));
            }
            
            if ($qty > $this->getDynamicMaxSaleQty($qty)) {
                $result->setQuoteMessageIndex('qty');
                throw new Petloft_Inventory_Exception(Mage::helper('cataloginventory')
                        ->__('The maximum quantity allowed for purchase is %s.', $this->getDynamicMaxSaleQty($qty) * 1));
            }

            if (!$this->getManageStock()) {
                return $result;
            }

            $customer  = Mage::helper('customer')->getCurrentCustomer();
            if(!$this->checkPersonLimit($qty,$customer->getId())){
                $result->setHasError(true);
                $result->setQuoteMessageIndex('qty');
                //Zend_Debug::dump($result->getData());die();
                throw new Petloft_Inventory_Exception(Mage::helper('cataloginventory')
                    ->__('You have reached limit allowed per person for this product'));

            }

            if (!$this->getIsInStock()) {
                $result->setQuoteMessageIndex('stock');
                $result->setItemUseOldQty(true);
                throw new Petloft_Inventory_Exception(Mage::helper('cataloginventory')
                        ->__('This product is currently out of stock.'));
            }

//            if ($this->getQuote() && !$this->checkPersonLimit($qty, $this->getQuote()->getCustomerId())) {
//                $personalLimit = $this->getAvailablePersonQty($this->getQuote()->getCustomerId());
//                if ($personalLimit > 0) {
//                    $message = Mage::helper('cataloginventory')
//                            ->__('Only %s items are allowed per person. You can buy %s more.', $this->getPersonLimit(),
//                                 $personalLimit);
//                    $result->setItemQty($personalLimit);
//                } else {
//                    $message = Mage::helper('cataloginventory')
//                            ->__('You have reached limit allowed per person.');
//                }
//                $result->setHasQtyOptionUpdate(true);
//                $result->setQuoteMessageIndex('stock');
//                throw new Petloft_Inventory_Exception($message);
//            }
            $result->addData($this->checkQtyIncrements($qty)->getData());

            if ($result->getHasError()) {
                return $result;
            }

            $this->_checkBackorderedQty($result, $qty, $summaryQty);
        } catch (Petloft_Inventory_Exception $e) {
            $result->setHasError(true)
                    ->setMessage($e->getMessage())
                    ->setQuoteMessage(Mage::helper('cataloginventory')
                            ->__('Some of the products cannot be ordered in requested quantity.'))
            ;
        }

        return $result;
    }

    /**
     * Set quote
     *
     * @param Mage_Sales_Model_Quote $quote Quote
     *
     * @return void
     */
    public function setQuote($quote)
    {
        $this->_quote = $quote;
    }

    /**
     * Get quote
     *
     * @return Mage_Sales_Model_Quote
     */
    public function getQuote()
    {
        return $this->_quote;
    }

    /**
     * Set qty
     *
     * @param int $diffQty qty difference
     *
     * @return Petloft_Inventory_Model_CatalogInventory_Stock_Item
     */
    public function setCurrentQty($diffQty)
    {
        $newQty = $this->_getResource()->updateQty($this, $diffQty, '');
        $this->setData('qty', $newQty);
        return $this;
    }

    /**
     * Check backordered qty
     *
     * @param Varien_Object &$result    result
     * @param mixed         $qty        quantity of this item (item qty x parent item qty)
     * @param mixed         $summaryQty quantity of this product in whole shopping cart which should
     *                                  be checked for stock availability
     *
     * @return void
     */
    protected function _checkBackorderedQty(&$result, $qty, $summaryQty)
    {
        if (!$this->checkQty($summaryQty)) {
            $message = Mage::helper('cataloginventory')->__('The requested quantity for "%s" is not available.',
                                                            $this->getProductName());
            $result->setQuoteMessageIndex('qty');
            throw new Petloft_Inventory_Exception($message);
        } else {
            if (($this->getQty() - $summaryQty) >= 0 || !$this->getProductName()) {
                return;
            }
            if ($this->getIsChildItem()) {
                $backorderQty = ($this->getQty() > 0) ? ($summaryQty - $this->getQty()) * 1 : $qty * 1;

                $result->setItemBackorders(min($backorderQty, $qty));
            } else {
                $orderedItems = $this->getOrderedItems();
                $itemsLeft = ($this->getQty() > $orderedItems) ? ($this->getQty() - $orderedItems) * 1 : 0;
                $backorderQty = ($itemsLeft > 0) ? ($qty - $itemsLeft) * 1 : $qty * 1;

                if ($backorderQty > 0) {
                    $result->setItemBackorders($backorderQty);
                }
                $this->setOrderedItems($orderedItems + $qty);
            }

            if ($this->getBackorders() == Mage_CatalogInventory_Model_Stock::BACKORDERS_YES_NOTIFY) {
                if (!$this->getIsChildItem()) {
                    $result->setMessage(Mage::helper('cataloginventory')
                                    ->__('This product is not available in the requested quantity. ' .
                                            '%s of the items will be backordered.', ($backorderQty * 1)));
                } else {
                    $result->setMessage(Mage::helper('cataloginventory')
                                    ->__('"%s" is not available in the requested quantity. ' .
                                            '%s of the items will be backordered.', $this->getProductName(),
                                         ($backorderQty * 1)));
                }
            }
        }
    }

    public function isWholesaleQty($qty)
    {
        if (!$qty) {
            return false;
        }

        if ($this->getMaxWholesaleQty() && $qty > $this->getMaxSaleQty()) {
            return true;
        }

        return false;
    }

    public function getDynamicMaxSaleQty($qty)
    {
        if ($this->isWholesaleQty($qty)) {
            return $this->getMaxWholesaleQty();
        }

        return $this->getMaxSaleQty();
    }
}
