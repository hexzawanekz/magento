<?php
class Petloft_Inventory_Model_CatalogInventory_Observer extends Mage_CatalogInventory_Model_Observer
{
    /**
     * Check product inventory data when quote item quantity declaring
     *
     * Rewrited to add additional checks
     *
     * @param Varien_Event_Observer $observer observer
     *
     * @return Mage_CatalogInventory_Model_Observer
     */
    public function checkQuoteItemQty($observer)
    {
        $quoteItem = $observer->getEvent()->getItem();
        /* @var $quoteItem Mage_Sales_Model_Quote_Item */
        if (!$quoteItem || !$quoteItem->getProductId() || !$quoteItem->getQuote()
                || $quoteItem->getQuote()->getIsSuperMode()) {
            return $this;
        }

        /**
         * Get Qty
         */
        $qty = $quoteItem->getQty();
        /**
         * Check item for options
         */
        //Zend_Debug::dump($quoteItem->getQtyOptions());die();
        if (($options = $quoteItem->getQtyOptions()) && $qty > 0) {
            $qty = $quoteItem->getProduct()->getTypeInstance(true)->prepareQuoteItemQty($qty, $quoteItem->getProduct());
            $quoteItem->setData('qty', $qty);

            $stockItem = $quoteItem->getProduct()->getStockItem();

            if ($stockItem) {
                $result = $stockItem->checkQtyIncrements($qty);
                if ($result->getHasError()) {
                    $quoteItem->setHasError(true)
                            ->setMessage($result->getMessage());
                    $quoteItem->getQuote()->setHasError(true)
                            ->addMessage($result->getQuoteMessage(), $result->getQuoteMessageIndex());
                }
            }

            foreach ($options as $option) {
                /* @var $option Mage_Sales_Model_Quote_Item_Option */
                $optionQty = $qty * $option->getValue();
                $increaseOptionQty = ($quoteItem->getQtyToAdd()
                                ? $quoteItem->getQtyToAdd()
                                : $qty) * $option->getValue();

                $stockItem = $option->getProduct()->getStockItem();
                /* @var $stockItem Mage_CatalogInventory_Model_Stock_Item */
                if (!$stockItem instanceof Mage_CatalogInventory_Model_Stock_Item) {
                    Mage::throwException(Mage::helper('cataloginventory')
                                    ->__('The stock item for Product in option is not valid.'));
                }

                $stockItem->setOrderedItems(0);
                /**
                 * define that stock item is child for composite product
                 */
                $stockItem->setIsChildItem(true);
                /**
                 * don't check qty increments value for option product
                 */
                $stockItem->setSuppressCheckQtyIncrements(true);

                $qtyForCheck = $this->_getQuoteItemQtyForCheck($option->getProduct()->getId(), $quoteItem->getId(),
                                                               $increaseOptionQty);

                $stockItem->setQuote($quoteItem->getQuote());
                $result = $stockItem->checkQuoteItemQty($optionQty, $qtyForCheck, $option->getValue());

                if (!is_null($result->getItemIsQtyDecimal())) {
                    $option->setIsQtyDecimal($result->getItemIsQtyDecimal());
                }

                if ($result->getHasQtyOptionUpdate()) {
                    $option->setHasQtyOptionUpdate(true);
                    $quoteItem->updateQtyOption($option, $result->getOrigQty());
                    $option->setValue($result->getOrigQty());
                    /**
                     * if option's qty was updates we also need to update quote item qty
                     */
                    $quoteItem->setData('qty', intval($qty));
                }
                if (!is_null($result->getMessage())) {
                    $option->setMessage($result->getMessage());
                }
                if (!is_null($result->getItemBackorders())) {
                    $option->setBackorders($result->getItemBackorders());
                }

                if ($result->getHasError()) {
                    $option->setHasError(true);
                    $quoteItem->setHasError(true)
                            ->setMessage($result->getQuoteMessage());
                    $quoteItem->getQuote()->setHasError(true)
                            ->addMessage($result->getQuoteMessage(), $result->getQuoteMessageIndex());
                }

                $stockItem->unsIsChildItem();
            }
        } else {
            $stockItem = $quoteItem->getProduct()->getStockItem();
            /* @var $stockItem Mage_CatalogInventory_Model_Stock_Item */
            if (!$stockItem instanceof Mage_CatalogInventory_Model_Stock_Item) {
                Mage::throwException(Mage::helper('cataloginventory')->__('The stock item for Product is not valid.'));
            }

            /**
             * When we work with subitem (as subproduct of bundle or configurable product)
             */
            if ($quoteItem->getParentItem()) {
                $rowQty = $quoteItem->getParentItem()->getQty() * $qty;
                /**
                 * we are using 0 because original qty was processed
                 */
                $qtyForCheck = $this->_getQuoteItemQtyForCheck($quoteItem->getProduct()->getId(),
                        $quoteItem->getId(), 0);
            } else {
                $increaseQty = $quoteItem->getQtyToAdd()
                        ? $quoteItem->getQtyToAdd()
                        : $qty;
                $rowQty = $qty;
                $qtyForCheck = $this->_getQuoteItemQtyForCheck($quoteItem->getProduct()->getId(), $quoteItem->getId(),
                                                               $increaseQty);
            }

            $productTypeCustomOption = $quoteItem->getProduct()->getCustomOption('product_type');
            if (!is_null($productTypeCustomOption)) {
                // Check if product related to current item is a part of grouped product
                if ($productTypeCustomOption->getValue() == Mage_Catalog_Model_Product_Type_Grouped::TYPE_CODE) {
                    $stockItem->setIsChildItem(true);
                }
            }
            $stockItem->setQuote($quoteItem->getQuote());
            $result = $stockItem->checkQuoteItemQty($rowQty, $qtyForCheck, $qty);

            if ($stockItem->hasIsChildItem()) {
                $stockItem->unsIsChildItem();
            }

            if (!is_null($result->getItemIsQtyDecimal())) {
                $quoteItem->setIsQtyDecimal($result->getItemIsQtyDecimal());
                if ($quoteItem->getParentItem()) {
                    $quoteItem->getParentItem()->setIsQtyDecimal($result->getItemIsQtyDecimal());
                }
            }

            /**
             * Just base (parent) item qty can be changed
             * qty of child products are declared just during add process
             * exception for updating also managed by product type
             */
            if ($result->getHasQtyOptionUpdate()
                    && (!$quoteItem->getParentItem()
                    || $quoteItem->getParentItem()->getProduct()->getTypeInstance(true)
                            ->getForceChildItemQtyChanges($quoteItem->getParentItem()->getProduct()))
            ) {
                $quoteItem->setData('qty', $result->getOrigQty());
            }

            if (!is_null($result->getItemUseOldQty())) {
                $quoteItem->setUseOldQty($result->getItemUseOldQty());
            }
            if (!is_null($result->getMessage())) {
                $quoteItem->setMessage($result->getMessage());
                if ($quoteItem->getParentItem()) {
                    $quoteItem->getParentItem()->setMessage($result->getMessage());
                }
            }

            if (!is_null($result->getItemBackorders())) {
                $quoteItem->setBackorders($result->getItemBackorders());
            }

            if ($result->getHasError()) {
                $quoteItem->setHasError(true);
                $quoteItem->getQuote()->setHasError(true)
                        ->addMessage($result->getQuoteMessage(), $result->getQuoteMessageIndex());

            }
        }
        $maxQty = $this->getCheckoutMaxQty();
        if (sizeof($quoteItem->getQuote()->getAllVisibleItems()) > $maxQty && $maxQty > 0) {
            $quoteItem->getQuote()->setHasError(true)
                    ->addMessage(Mage::helper('cataloginventory')
                    ->__('Sorry, you cannot have more than %s products in a cart.', $maxQty), 'max_qty');
        }

        return $this;
    }

    /**
     * Revert quote items inventory data (cover not success order place case)
     *
     * @param Varien_Event_Observer $observer observer
     *
     * @return void
     */
    public function revertQuoteInventory($observer)
    {
        $quote = $observer->getEvent()->getQuote();
        $items = $this->_getProductsQty($quote->getAllItems());

        Mage::getSingleton('cataloginventory/stock')->setCustomerId($quote->getCustomerId())
                ->revertProductsSale($items);

        // Clear flag, so if order placement retried again with success - it will be processed
        $quote->setInventoryProcessed(false);
    }

    /**
     * Get checkout max qty
     *
     * @return void
     */
    public function getCheckoutMaxQty()
    {
        return (int)Mage::getStoreConfig('checkout/cart/max_qty');
    }

    /**
     * Cancel order item
     *
     * @param Varien_Event_Observer $observer observer
     * @return  Mage_CatalogInventory_Model_Observer
     */
    public function cancelOrderItem($observer)
    {
        $item = $observer->getEvent()->getItem();
        $productId = $item->getProductId();

        $children = $item->getChildrenItems();
        $qty = $item->getQtyOrdered() - max($item->getQtyShipped(), $item->getQtyInvoiced()) - $item->getQtyCanceled();

        if ($item->getId() && ($productId) && empty($children) && $qty) {
            Mage::getSingleton('cataloginventory/stock')->backItemQty($productId, $qty);
            Mage::getModel('inventory/person_limit')->updateQty($productId, $item->getOrder()->getCustomerId(), $qty,
                                                                '-');
        }
        return $this;
    }

    /**
     * [refundOrderInventory description]
     *
     * @param [type] $observer [description]
     *
     * @return [type]           [description]
     */
    public function refundOrderInventory($observer)
    {
        $creditmemo = $observer->getEvent()->getCreditmemo();
        foreach ($creditmemo->getAllItems() as $item) {
            if ($item->hasBackToStock()) {
                $productId = $item->getProductId();
                if ($item->getQty()) {
                    Mage::getSingleton('cataloginventory/stock')->backItemQty($productId, $item->getQty());
                    Mage::getModel('inventory/person_limit')
                            ->updateQty($productId, $creditmemo->getCustomerId(), $item->getQty(), '-');
                }
            }
        }
        return $this;
    }

    /**
     * Subtract quote items qtys from stock items related with quote items products.
     *
     * Used before order placing to make order save/place transaction smaller
     * Also called after every successful order placement to ensure subtraction of inventory
     *
     * @param Varien_Event_Observer $observer
     */
    public function subtractQuoteInventory(Varien_Event_Observer $observer)
    {
        $quote = $observer->getEvent()->getQuote();

        // Maybe we've already processed this quote in some event during order placement
        // e.g. call in event 'sales_model_service_quote_submit_before' and later in 'checkout_submit_all_after'
        if ($quote->getInventoryProcessed()) {
            return;
        }
        $items = $this->_getProductsQty($quote->getAllItems());

        /**
         * Remember items
         */
        $this->_itemsForReindex = Mage::getSingleton('cataloginventory/stock')
                        ->setCustomerId($quote->getCustomerId())->registerProductsSale($items);

        $quote->setInventoryProcessed(true);
        return $this;
    }
}