<?php

/**
 * Petloft extension for Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * 
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade
 * the Petloft Inventory module to newer versions in the future.
 * If you wish to customize the Petloft Inventory module for your needs
 * please refer to http://www.magentocommerce.com for more information.
 *
 * @category   Petloft
 * @package    Petloft_Inventory
 * @copyright  Copyright (C) 2013
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Rewrite cart model to add additional checks before adding
 *
 * @category   Petloft
 * @package    Petloft_Inventory
 * @subpackage Model
 * @author     
 */
class Petloft_Inventory_Model_Checkout_Cart extends Mage_Checkout_Model_Cart
{

    /**
     * Add product to shopping cart (quote)
     *
     * @param int|Mage_Catalog_Model_Product $productInfo product info
     * @param mixed                          $requestInfo request info
     * @return  Mage_Checkout_Model_Cart
     */
    public function addProduct($productInfo, $requestInfo = null)
    {
        $product = $this->_getProduct($productInfo);
        $request = $this->_getProductRequest($requestInfo);

        $productId = $product->getId();

        if ($product->getStockItem()) {
            /* @var $stockItem Petloft_Inventory_Model_CatalogInventory_Stock_Item */
            $stockItem = $product->getStockItem();
            $minimumQty = $stockItem->getMinSaleQty();
            //If product was not found in cart and there is set minimal qty for it
            if ($minimumQty && $minimumQty > 0 && $request->getQty() < $minimumQty &&
                    !$this->getQuote()->hasProductId($productId)
            ) {
                $request->setQty($minimumQty);
            }
        }

        if (!$productId) {
            Mage::throwException(Mage::helper('checkout')->__('The product does not exist.'));
            echo "Product Not Exist";
        }
        try {
            $customer = ($this->getQuote()->getCustomer()) ? : $this->getCheckoutSession()->getCustomer();
            Mage::dispatchEvent('checkout_cart_product_add_before',
                                array(
                'quote' => $this->getQuote(),
                'product' => $product,
                'customer' => $customer,
                'qty'=> $request->getQty()
                    )
            );
            $quoteItem = $this->getQuote()->addProduct($product, $request);
        } catch (Mage_Core_Exception $e) {
            echo $e->getMessage();
            Mage::logException($e);

            $this->getCheckoutSession()->setUseNotice(false);
            $redirectUrl = ($product->hasOptionsValidationFail()) ? $product->getUrlModel()->getUrl(
                            $product, array('_query' => array('startcustomization' => 1))
                    ) : $product->getProductUrl();
            $this->getCheckoutSession()->setRedirectUrl($redirectUrl);
            throw $e;
        }

        Mage::dispatchEvent('checkout_cart_product_add_after',
                            array(
            'quote_item' => $quoteItem,
            'product' => $product,
                )
        );
        $this->getCheckoutSession()->setLastAddedProductId($productId);
        return $this;
    }
}
