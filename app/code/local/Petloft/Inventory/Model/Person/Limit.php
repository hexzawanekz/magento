<?php

/**
 * Petloft extension for Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * 
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade
 * the Petloft Inventory module to newer versions in the future.
 * If you wish to customize the Petloft Inventory module for your needs
 * please refer to http://www.magentocommerce.com for more information.
 *
 * @category   Petloft
 * @package    Petloft_Inventory
 * @copyright  Copyright (C) 2013
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Person limit model
 *
 * @category   Petloft
 * @package    Petloft_Inventory
 * @subpackage Model
 * @author     
 */
class Petloft_Inventory_Model_Person_Limit extends Mage_Core_Model_Abstract
{

    /**
     * Initialize model
     * 
     * @return void
     */
    protected function _construct()
    {
        $this->_init('inventory/person_limit');
    }

    /**
     * Load product limit for a customer
     * 
     * @param Mage_Catalog_Model_Product|int   $product  Product object o product Id 
     * @param Mage_Customer_Model_Customer|int $customer Customer object o customer Id 
     * @return Petloft_Inventory_Model_Person_Limit
     */
    public function loadLimit($product, $customer)
    {
        if (is_object($product)) {
            $product = $product->getId();
        }
        if (is_object($customer)) {
            $customer = $customer->getId();
        }
        $this->_getResource()->loadLimit($this, $product, $customer);
        $this->_afterLoad();
        $this->setOrigData();
        $this->_hasDataChanges = false;
        $this->setCustomerId($customer);
        $this->setProductId($product);
        return $this;
    }

    /**
     * Update customer bought qty
     * 
     * @param int    $product  product
     * @param int    $customer customer
     * @param int    $qty      qty
     * @param string $operator operator
     * @return Petloft_Inventory_Model_Person_Limit 
     */
    public function updateQty($product, $customer, $qty, $operator = "+")
    {
        if (is_null($customer)) {
            return $this;
        }
        if ($operator != '+') {
            $qty = -$qty;
        }
        $this->loadLimit($product, $customer);
        $this->setQty($this->getOrigData('qty') + $qty);
        $this->save();
        return $this;
    }
}
