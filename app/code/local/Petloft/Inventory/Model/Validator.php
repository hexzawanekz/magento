<?php
/**
 * Petloft Web extension for Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade
 * the Petloft Inventory module to newer versions in the future.
 * If you wish to customize the Petloft Inventory module for your needs
 * please refer to http://www.magentocommerce.com for more information.
 *
 * @category   Petloft
 * @package    Petloft_Inventory
 * @copyright  Copyright (C) 2013
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Inventory validator model
 *
 * @category   Petloft
 * @package    Petloft_Inventory
 * @subpackage Model
 * @author     Denis Obukhov <denis.obukhov@Petloftweb.com>
 */
class Petloft_Inventory_Model_Validator extends Mage_Core_Model_Abstract
{
    
    /**
     * Validate product before add to cart
     *
     * @param Varien_Object $object object
     *
     * @return void
     */
    public function validateBeforeAddProduct($object)
    {
        $quote     = $object->getQuote();
        $product   = $object->getProduct();
        $customer  = Mage::helper('customer')->getCurrentCustomer();
        $qty = $object->getQty();
        $stockItem = $product->getStockItem();
        if ($qty > $stockItem->getDynamicMaxSaleQty($qty)) {
            $message = Mage::helper('checkout')
                        ->__('The maximum quantity allowed for purchase is "%s".', $stockItem->getDynamicMaxSaleQty($qty));
            Mage::throwException($message);
        }
        if ($stockItem->getPersonLimit()) {
            $personLimit = $stockItem->getAvailablePersonQty($customer->getId());
            if ($personLimit < 1) {
                $message = Mage::helper('checkout')
                        ->__('You have reached limit allowed per person for "%s".', $product->getName());
                if (count($quote->getAllVisibleItems())) {
                    Mage::throwException($message);
                } else {
                    throw new Petloft_Inventory_Exception($message);
                }
            } elseif ($item = $quote->getItemByProduct($product)){
                if($item->getQty()+ $qty > $personLimit){
                    Mage::throwException(Mage::helper('checkout')
                        ->__('Only %s items of "%s" are allowed per person.',
                            $stockItem->getPersonLimit(), $item->getName(), $personLimit));
                }
            } elseif($qty > $personLimit){
                Mage::throwException(Mage::helper('checkout')
                        ->__('Only %s items of "%s" are allowed per person.',
                            $stockItem->getPersonLimit(), $product->getName(), $personLimit));
            }
        }
    }    
}
