<?php

/**
 * Petloft extension for Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade
 * the Petloft Inventory module to newer versions in the future.
 * If you wish to customize the Petloft Inventory module for your needs
 * please refer to http://www.magentocommerce.com for more information.
 *
 * @category   Petloft
 * @package    Petloft_Inventory
 * @copyright  Copyright (C) 2013
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Inventory Helper
 *
 * @category   Petloft
 * @package    Petloft_Inventory
 * @subpackage Helper
 * @author
 */
 
require_once 'PHPToolkit/NetSuiteService.php';
 
class Petloft_Inventory_Helper_Data extends Mage_Core_Helper_Abstract
{

    public function setConfiguration($account, $email, $password, $host, $role, $endpoint = '2013_1')
    {
        global $nshost, $nsendpoint;
        global $nsaccount, $nsemail, $nsrole, $nspassword;

        $nsaccount = $account;
        $nsemail = $email;
        $nspassword = $password;
        $nshost = $host;
        $nsrole = $role;
        $nsendpoint = $endpoint;
    }
    
    
    public function getSaveSearchData($saveSearchId) 
    {
        $maxRow = 1000;
        $service = new NetSuiteService();
        $service->setSearchPreferences(true, $maxRow);

        $search = new ItemSearchAdvanced();
        $search->savedSearchId = $saveSearchId;

        $request = new SearchRequest();
        $request->searchRecord = $search;
        
        $searchResponse = $service->search($request);
        if (!$searchResponse->searchResult->status->isSuccess) {
            echo 'Netsuite search error.';
        }
        
        $result = array();

        $totalRecords = $searchResponse->searchResult->totalRecords;
        $records = $searchResponse->searchResult->searchRowList->searchRow;
        $i=1;
        
        foreach ($records as $record) {
             $result[] = $record;        
        }
        //return $result;

        while (($searchResponse->searchResult->pageIndex * $maxRow) < $totalRecords) {
            $moreRequest = new SearchMoreWithIdRequest();
            $moreRequest->pageIndex = $searchResponse->searchResult->pageIndex + 1;
            $moreRequest->searchId = $searchResponse->searchResult->searchId;

            $searchResponse = $service->searchMoreWithId($moreRequest);

            if (!$searchResponse->searchResult->status->isSuccess) {
                echo 'Netsuite get more search result error.';
            }

            $records = $searchResponse->searchResult->searchRowList->searchRow;

            foreach ($records as $record) {  
                $result[] = $record;
            }
        }
        return $result;
    }
    
}