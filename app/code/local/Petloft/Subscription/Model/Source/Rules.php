<?php
class Petloft_Subscription_Model_Source_Rules extends Varien_Object
{
    public function toOptionArray()
    {
        $rules = Mage::getModel('salesrule/rule')->getCollection()->addFieldToFilter('is_active',array('eq'=>1));
		$_rules=array();
		foreach($rules as $rule){
			$_rules[]=array('value' => $rule->getId(), 'label' => Mage::helper('subscription')->__($rule->getName()));
		}
        array_unshift($_rules,array('value'=>'', 'label'=>Mage::helper('subscription')->__('-- Please Select --')));
        return $_rules;
    }
}