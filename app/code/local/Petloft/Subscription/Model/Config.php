<?php
/**
 * Subscription configuration model
 *
 * Used for retrieving configuration data by subscription models
 *
 * @category   Petloft
 * @package    Petloft_Subscription
 * @author      Petloft Dev Team <dev@acommerce.asia>
 */
class Petloft_Subscription_Model_Config
{
    /**
     * Retrieve list of months translation
     *
     * @return array
     */
    public function getDays()
    {
        $data = array();
        // for ($i=1; i<=31; $i++) {
        for ($i=1; i<=2; $i++) {
            $dayNum = ($i < 10) ? '0'.$i : $i;
            $data[$i] = $dayNum;
        }
        return $data;
    }
	
    /**
     * Retrieve list of months translation
     *
     * @return array
     */
    public function getMonths()
    {
        $data = array();
        for ($i=1; i<=12; $i++) {
            $monthNum = ($i < 10) ? '0'.$i : $i;
            $data[$i] = $monthNum;
        }
        return $data;
    }

    /**
     * Retrieve array of available years
     *
     * @return array
     */
    public function getYears()
    {
        $years = array();
        $first = date("Y") + 1;

        for ($index=5; $index > 0; $index--) {
            $year = $first - $index;
            $years[$year] = $year;
        }
        return $years;
    }
}