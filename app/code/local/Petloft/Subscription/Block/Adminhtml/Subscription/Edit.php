<?php
/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Subscription to newer
 * versions in the future.
 *
 * @category    Petloft
 * @package     Petloft_Subscription
 * @copyright   Copyright (c) 2012 aCommerce (http://www.acommerce.asia)
 * @license     http://www.acommerce.asia/LICENSE-E.txt
*/

class Petloft_Subscription_Block_Adminhtml_Subscription_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'subscription';
        $this->_controller = 'adminhtml_subscription';
        
        $this->_updateButton('save', 'label', Mage::helper('subscription')->__('Save Item'));
        $this->_updateButton('delete', 'label', Mage::helper('subscription')->__('Delete Item'));
		
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('subscription_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'subscription_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'subscription_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";

        $this->_removeButton('reset');
    }

    public function getHeaderText()
    {
        if( Mage::registry('subscription_data') && Mage::registry('subscription_data')->getId() ) {            
            return Mage::helper('subscription')->__("Edit Subscription '%s'", $this->htmlEscape(Mage::registry('subscription_data')->getEmail()));
        } else {
            return Mage::helper('subscription')->__('Add Item');
        }
    }
}