<?php
/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Subscription to newer
 * versions in the future.
 *
 * @category    Petloft
 * @package     Petloft_Subscription
 * @copyright   Copyright (c) 2012 aCommerce (http://www.acommerce.asia)
 * @license     http://www.acommerce.asia/LICENSE-E.txt
*/

class Petloft_Subscription_Block_Subscription extends Mage_Core_Block_Template
{
    public function _prepareLayout()
    {
        return parent::_prepareLayout();
    }
    
     public function getSubscription()
     { 
        if (!$this->hasData('subscription')) {
            $this->setData('subscription', Mage::registry('subscription'));
        }
        return $this->getData('subscription');
        
    }

    public function getDays()
    {
        $days[0] = $this->__('Day');
        for ($i=1; i<=31; $i++) {
            $days[$i] = ($i < 10) ? '0'.$i : $i;;
        }
        return $days;
    }
    
    /**
     * Retrieve list of months translation
     *
     * @return array
     */
    public function getMonths()
    {
        $months[0] = $this->__('Month');
        for ($i=1; i<=12; $i++) {
            $months[$i] = ($i < 10) ? '0'.$i : $i;;
        }
        return $months;
    }

    /**
     * Retrieve array of available years
     *
     * @return array
     */
    public function getYears()
    {
        $years = array();
        $first = date("Y") + 2;

        $years[0] = $this->__('Year');
        for ($index=5; $index > 0; $index--) {
            $year = $first - $index;
            $years[$year] = $year;
        }
        return $years;
    }

    /**
     * Return action url for form
     *
     * @return string
     */
    public function getSaveUrl()
    {
        return $this->getUrl('subscription/index/save');
    }
}