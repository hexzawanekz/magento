<?php
/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Subscription to newer
 * versions in the future.
 *
 * @category    Petloft
 * @package     Petloft_Subscription
 * @copyright   Copyright (c) 2012 aCommerce (http://www.acommerce.asia)
 * @license     http://www.acommerce.asia/LICENSE-E.txt
*/

class Petloft_Subscription_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {		
		$this->loadLayout();
		$this->renderLayout();
    }

    public function saveAction()
    {
    	$data = Mage::app()->getRequest()->getPost();
    	$model = Mage::getModel('subscription/subscription');

    	$result = array();

    	if ($data){
			try {
				if(($data['year']=="") && ($data['month']=="") && ($data['day']=="")) {
					$birth = "";
				} else {
					$birth = $data['year'].'-'.$data['month'].'-'.$data['day'];
				}
                
				$model->setEmail($data['email'])
                        ->setFirstName($data['first_name'])
                        ->setLastName($data['last_name'])
                        ->setBabyBirthday($birth)
                        ->setTryingToConceive($data['trying_conceive'])
                        ->setBabyName($data['baby_name'])
                        ->setCreatedTime(strftime('%Y-%m-%d %H:%M:%S', time()))
                        ->setUpdateTime(strftime('%Y-%m-%d %H:%M:%S', time()))
						->save();

				$result['success'] = true;
			} catch(Exception $e) {
				$result['success'] = false;
				$result['message'] = $e->getMessage();
			}
		}
    	
    	$this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
        return;		

    }
}