<?php

class Petloft_Customer_AddressController extends Mage_Core_Controller_Front_Action
{
    public function saveDefaultAddressAction()
    {
        $result = array('status' => false);
        if($request = $this->getRequest()){
            if($request->isAjax()){
                if($add_id = $request->getParam('id')){
                    /** @var Mage_Customer_Model_Session $customerSession */
                    $customerSession = Mage::getSingleton('customer/session');
                    /** @var Mage_Customer_Model_Customer $customer */
                    $customer = $customerSession->getCustomer();

                    foreach($customer->getAddresses() as $address){
                        /** @var Mage_Customer_Model_Address $address */
                        if($address->getId() == $add_id){
                            try{
                                $address->setIsDefaultBilling(true);
                                $address->setIsDefaultShipping(true);
                                $address->save();
                                $result = array('status' => true);
                            }catch(Exception $e){}
                        }
                    }
                }
            }
        }

        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody(json_encode($result));
    }
}