<?php


/**
 * Customer Data Helper
 *
 * @category   Petloft
 * @package    Petloft_Customer
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Petloft_Customer_Helper_Data extends Mage_Customer_Helper_Data
{
    public function getAddressUrl()
    {
        return $this->_getUrl('customer/address');
    }

    public function getCustomerLastname()
    {
        return $this->getCustomer()->getLastname();
    }

    /**
     * Retrieve current customer first name
     *
     * @return string
     */
    public function getCustomerFirstname()
    {
        return $this->getCustomer()->getFirstname();        
    }

    public function getLoginUrlParams() {
        $params = array();

        $referer = $this->_getRequest()->getParam(self::REFERER_QUERY_PARAM_NAME);

        if (!$referer && !Mage::getStoreConfigFlag(self::XML_PATH_CUSTOMER_STARTUP_REDIRECT_TO_DASHBOARD)
            && !Mage::getSingleton('customer/session')->getNoReferer()
        ) {
            if (strpos(Mage::getUrl('*/*/*', array('_current' => true, '_use_rewrite' => true)), 'cdlogin') !== false) {
                $referer = Mage::helper('core')->urlEncode(Mage::getSingleton('core/session')->getLastUrl());
            } else {
                $referer = Mage::getUrl('*/*/*', array('_current' => true, '_use_rewrite' => true));
                $referer = Mage::helper('core')->urlEncode($referer);
            }
        }

        if ($referer) {
            $params = array(self::REFERER_QUERY_PARAM_NAME => $referer);
        }

        return $params;
    }
}
