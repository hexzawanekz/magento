<?php

/**
 * Shopping ajax cart controller
 */

require_once 'Mage/Checkout/controllers/CartController.php';

class Petloft_Sidebar_ApiController extends Mage_Checkout_CartController
{ 

    public function getblockshippingAction()
    {
        $this->ajax_getblockshippingAction();
    }

    public function deleteAction()
    {
        $this->ajax_deleteAction();        
    }

    protected function getItemsCountText()
    {
        $cart  = $this->_getCart();
        $count = $cart->getSummaryQty();

        if ($count == 1) {
            $text = $this->__('(%s item)', $count);
        } elseif ($count > 0) {
            $text = $this->__('(%s items)', $count);
        } else {
            $text = $this->__('');
        }
        return $text;
    }

    protected function getBlockShippingHtml()
    {
        $cart  = $this->_getCart();
        $count = $cart->getSummaryQty();

        /* Right cart */
        $html_block_shipping = '';
        if($cart->getSummaryQty() > 0)
        {
            $layout = $this->loadLayout('cart_right_ajax')
                            ->_initLayoutMessages('checkout/session')
                            ->_initLayoutMessages('catalog/session')
                            ->getLayout();

            $block = $layout->getBlock('cart_sidebar_ajax');
            $html_block_shipping = $block->toHtml();
        }

        return $html_block_shipping;
    }

    public function ajax_getblockshippingAction()
    {
        $json['html_block_shipping'] = $this->getBlockShippingHtml();
        $json['status'] = 1;
        
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($json));
    }


    /**
     * Delete shoping cart item action
     */
    public function ajax_deleteAction()
    {
        $cart   = $this->_getCart();
        $id = (int) $this->getRequest()->getParam('id');
        
        $json = array();
        $json['status'] = 0;
        $block_message = '';
        if ($id) {
            try {
                $this->_getCart()->removeItem($id)->save();                
                // $this->_getSession()->addSuccess($this->__('Removed the item.'));
                $json['status'] = 1;
            } catch (Exception $e) {
                // $this->_getSession()->addError($this->__('Cannot remove the item.'));
                Mage::logException($e);
                $json['status'] = 0;
            }
        }
        // $this->_redirectReferer(Mage::getUrl('*/*'));
        // $block_message = $this->loadLayout()
                    // ->_initLayoutMessages('checkout/session')
                    // ->_initLayoutMessages('catalog/session')
                    // ->getLayout()
                    // ->getMessagesBlock()
                    // ->getGroupedHtml();
        $json['message'] = $block_message;
        $json['items_count_text'] = $this->getItemsCountText();
        $json['html_block_shipping'] = $this->getBlockShippingHtml();

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($json));
    }

}
