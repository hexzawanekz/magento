<?php

class Petloft_Filter_Helper_Data extends Mage_Core_Helper_Abstract
{
    protected $_filters = array(
							'pet_size',
							'pet_life_stage',
							'pet_food_type',
							'pet_food_brands',
							'pet_food_speciality',
							'breed',
							'pet_product_type'
							);
	public function getActiveFilterAttributes() {
		return $this->_filters;
	}
	public function getCoreSession() {
		return Mage::getSingleton('core/session');
	}
	public function unsetFilter($code = null) {
		$session = $this->getCoreSession();
		if(!$code) {
			foreach($this->getActiveFilterAttributes() as $filter) {
				$session->setData($filter, '');
			}
		}
		else {
			$session->setData($code, '');
		}
	}
}