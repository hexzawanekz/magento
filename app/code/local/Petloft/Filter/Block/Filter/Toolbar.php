<?php
class Petloft_Filter_Block_Filter_Toolbar extends Mage_Catalog_Block_Product_List_Toolbar
{
   
    protected function _construct() {
        parent::_construct();        
        $this->setTemplate('petloftfilter/category/filter/toolbar.phtml');
    }
	public function getSortOrderParams($order,$direction=null) {
		$params = array();
		if (is_null($order)) {
            $order = $this->getCurrentOrder() ? $this->getCurrentOrder() : $this->_availableOrder[0];
        }
		if(!$direction) {
			$direction = $this->getCurrentDirection();
		}
		$params[$this->getOrderVarName()] = $order;
		$params[$this->getDirectionVarName()] = $direction;
		$params[$this->getPageVarName()] = null;
		return $this->buildParams($params);
	}
	public function getLimitParams($limit) {
		$params = array($this->getLimitVarName()=>$limit,$this->getPageVarName()=>null);
		return $this->buildParams($params);
	}
	protected function buildParams($prams) {
		return '?'.http_build_query($prams,'','&');
	}
}
