<?php
$installer = $this;

$installer->startSetup();

$installer->run("
DROP TABLE IF EXISTS utm_tracking;
CREATE TABLE IF NOT EXISTS utm_tracking(
  `id` int(11) unsigned NOT NULL auto_increment,
  `order_increment_id` int(11) NOT NULL DEFAULT 0,
  `utm_source` varchar(255) NOT NULL DEFAULT '',
  `utm_medium` varchar(255) NOT NULL DEFAULT '',
  `utm_term` varchar(255) NOT NULL DEFAULT '',
  `utm_content` varchar(255) NOT NULL DEFAULT '',
  `utm_campaign` varchar(255) NOT NULL DEFAULT '',
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;    
");

$installer->endSetup();