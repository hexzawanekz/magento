<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Customer
 * @copyright   Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Petloft_UTMTracking_Model_Observer
{
	public function addCustomerRegistrationTrackingUTMData($observer) {
		try{			
			if(!$this->getEnableConfig()) {
				return ;
			}
			$customer = $observer->getEvent()->getCustomer();
			if(!$customer->getId()) {				
				$customer->addData($this->getUtm());
			}
		}
		catch(Exception $e) {
			Mage::log($e->getMessage());
		}
	}
	public function getEnableConfig() {
		$store = Mage::app()->getStore();
		if($store->isAdmin()) {
			return false;
		}
		return Mage::getStoreConfig('petloftutmtracking/trackingconfig/enable', $store->getId());
	}
	public function addCustomerOrderTrackingUTMData($observer) {
	    if(!Mage::registry('sm_utm_tracking')) {
	        Mage::register('sm_utm_tracking',true);
            try {
                if (!$this->getEnableConfig()) {
                    return;
                }
                $order = $observer->getEvent()->getOrder();
                if ($order->getCanAddUtm() && $order->getId()) {
                    $data = $this->getUtm();
                    $resource = Mage::getSingleton('core/resource');
                    $writeConnection = $resource->getConnection('core_write');
                    $query = "INSERT INTO utm_tracking SET order_increment_id = '{$order->getIncrementId() }',utm_source = '{$data['utm_source']}',utm_medium = '{$data['utm_medium']}',utm_term = '{$data['utm_term']}',utm_content = '{$data['utm_content']}',utm_campaign = '{$data['utm_campaign']}'";
                    $writeConnection->query($query);
                }
            } catch (Exception $e) {
                Mage::log($e->getMessage());
            }
        }else Mage::unregister('sm_utm_tracking');
	}
	public function addUtmForNewOrder($observer) {
		$order = $observer->getEvent()->getOrder();
		if(!$order->getId()) {
			$order->setCanAddUtm(true);
		}
	}
	private function getUtm(){
		$track = array('utm_source'=>'utmcsr',
						'utm_medium'=>'utmcmd',
						'utm_term'=>'utmctr',
						'utm_content'=>'utmcct',
						'utm_campaign'=>'utmccn');
						
		$utm = array('utm_source'=>'undefined',
					 'utm_medium'=>'undefined',
					 'utm_term'=>'undefined',
					 'utm_content'=>'undefined',
					 'utm_campaign'=>'undefined');
		if(!empty($_COOKIE['__utmz'])){
			$pattern = "/(utmcsr=([^\|]*)[\|]?)|(utmccn=([^\|]*)[\|]?)|(utmcmd=([^\|]*)[\|]?)|(utmctr=([^\|]*)[\|]?)|(utmcct=([^\|]*)[\|]?)/i";
			preg_match_all($pattern, $_COOKIE['__utmz'], $matches);
			if(!empty($matches[0])){
				foreach($matches[0] as $match){					
					$match = trim($match, "|");
					list($k, $v) = explode("=", $match);
					$column =  array_search($k,$track);
					if($column !== false && trim($v)) {
						$utm[$column] = $v;
					}
				}
			}
		}
		if(!empty($_COOKIE['__utmz2'])){
			$arrayId = explode("&",$_COOKIE['__utmz2']);
			$utm['utm_subid'] = $arrayId[0];
			$utm['utm_click'] = $arrayId[1];
		}
		return $utm;
	}

}
