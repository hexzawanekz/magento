<?php

class Petloft_Wholesale_Model_Observer
{
    public function addSendPickupEmail($observer)
    {
        $container = $observer->getBlock();
        if ($container !== null && $container->getType() == 'adminhtml/sales_order_view' && $container->getOrder()->getIsWholesale()) {
            $data = [
                'label' => 'Send Pickup Email',
                'class' => '',
                'onclick' => 'setLocation(\'' . Mage::helper('adminhtml')->getUrl('adminhtml/sales_order/sendPickupEmail', ["order_id" => $container->getOrder()->getId()]) . '\')'
            ];
            $container->addButton('', $data);
        }

        return $this;
    }
}
