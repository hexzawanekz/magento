<?php

class Petloft_FacebookApi_Model_Resource_Facebookuser extends Mage_Core_Model_Resource_Db_Abstract
{
    /**
     * Define main table
     *
     */
    protected function _construct()
    {
        $this->_init('facebookapi/facebookuser', 'customer_id');
    }   
}
