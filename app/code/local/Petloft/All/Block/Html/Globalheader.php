<?php
/**
 * Links block
 *
 * @category    Mage
 * @package     Mage_Checkout
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Petloft_All_Block_Html_Globalheader extends Mage_Page_Block_Template_Links
{
    protected $_globalheader_links = array();

    function __construct()
    {
        $this->setTemplate('page/html/global_header.phtml');
    }

    public function addCartLink()
    {
        if (Mage::helper('core')->isModuleOutputEnabled('Mage_Checkout')) {
             $count = $this->getSummaryQty() ? $this->getSummaryQty()
             : $this->helper('checkout/cart')->getSummaryCount();
             /*if ($count == 1) {
                $text = '<i class="mycart-ico"></i>' . $this->__('My Cart <span>(%s item)</span>', $count);
            } elseif ($count > 0) {
                $text = '<i class="mycart-ico"></i>' . $this->__('My Cart <span>(%s items)</span>', $count);
            } else {
                $text = '<i class="mycart-ico"></i>' . $this->__('My Cart <span></span>');
            }*/

            $text = '';
            $this->addLink($text, 'checkout/cart', "", true, array(), 50, null, 'class="global-header-link-cart"');
        }

        return $this;
    }

    public function addCheckoutLink()
    {
        if (!$this->helper('checkout')->canOnepageCheckout()) {
            return $this;
        }

        if (Mage::helper('core')->isModuleOutputEnabled('Mage_Checkout')) {
            $text = $this->__('Checkout');
            if (Mage::helper('core')->isModuleOutputEnabled('IWD_OnepageCheckout') )
            {
                $this->addLink(
                    '<i class="checkout-ico"></i>' . $text, 'onepagecheckout', $text,
                    true, array('_secure' => true), 60, null,
                    'class="global-header-link-checkout"'
                );
            }
            else
            {
                // return parent::addCheckoutLink();
                $this->addLink(
                    '<i class="checkout-ico"></i>' . $text, 'checkout', $text,
                    true, array('_secure' => true), 60, null,
                    'class="global-header-link-checkout"'
                );
            }
        }

        return $this;
    }
}
