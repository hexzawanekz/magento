<?php
/**
 * Links block
 *
 * @category    Mage
 * @package     Mage_Checkout
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Petloft_All_Block_Links extends Mage_Core_Block_Template
{
    /**
     * Add shopping cart link to parent block
     *
     * @return Mage_Checkout_Block_Links
     */
    public function addCartLink()
    {
        $parentBlock = $this->getParentBlock();
        if ($parentBlock && Mage::helper('core')->isModuleOutputEnabled('Mage_Checkout')) {
             $count = $this->getSummaryQty() ? $this->getSummaryQty()
             : $this->helper('checkout/cart')->getSummaryCount();
             if ($count == 1) {
                $text = $this->__('My Cart <span>(%s item)</span>', $count);
            } elseif ($count > 0) {
                $text = $this->__('My Cart <span>(%s items)</span>', $count);
            } else {
                $text = $this->__('My Cart<span></span>');
            }

            $parentBlock->removeLinkByUrl($this->getUrl('checkout/cart'));
            $parentBlock->addLink($text, 'checkout/cart', "", true, array(), 50, null, 'class="top-link-cart"');
        }

        return $this;
    }

    public function addRegisterLink()
    {
        $parentBlock = $this->getParentBlock();
        $text = $this->__('Register');
        $parentBlock->addLink($text, 'customer/account/create', "", true, array(), 50, null, '');

        return $this;
    }

    public function removeTopLink($param)
    {
        $parentBlock = $this->getParentBlock();
        $parentBlock->removeLinkByUrl($this->getUrl($param));

        return $this;
    }

}
