<?php
class Magently_CustomProductList_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function getCustomCollection()
    {
        $attr = 'manufacturer';
        $brand = 6;
        return Mage::getModel('catalog/product')->getCollection()->addAttributeToSelect(
            $attr)->addAttributeToFilter(
                    array(
                        array('attribute'=>$attr, 'eq'=>$brand)
                    )
                )->addAttributeToSelect(
                    array('price', 'name')
                );
    }
}