<?php

$installer = $this;

$installer->startSetup();

$installer->run("

DROP TABLE IF EXISTS {$this->getTable('supermenu_cache')};
CREATE TABLE {$this->getTable('supermenu_cache')} (
  `id` int(11) unsigned NOT NULL auto_increment,
  `text_item` varchar(255) NOT NULL default '',
  `cache_content` longtext NOT NULL default '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8; 
");

$installer->endSetup();