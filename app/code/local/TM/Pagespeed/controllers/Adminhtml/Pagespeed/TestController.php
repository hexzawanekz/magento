<?php

class TM_Pagespeed_Adminhtml_Pagespeed_TestController extends Mage_Adminhtml_Controller_Action
{
    public function testAction()
    {
        $varName = $this->getRequest()->getParam('var_name');
        Mage::getSingleton('adminhtml/session')->setData($varName, true);
        $this->_redirectReferer();
    }

    public function smushAction()
    {
        try {
            $path = base64_decode($this->getRequest()->getParam('path'));

            $service = new TM_Pagespeed_Model_Service_Util();
            // $path =  'frontend' . DS . 'base' . DS . 'default' . DS . 'images' . DS . 'cvv.gif' ;
            $filename = BP . DS  . 'skin' . DS . $path;
            $archiveFilename = $service->getArchiveFilename($filename);
            if (!file_exists($archiveFilename)) {
                $data = $service->smush($filename);

            } else {
                $sourceSize = filesize($archiveFilename);
                $destinationSize = filesize($filename);
                $percent = (($sourceSize - $destinationSize) * 100) / $sourceSize;

                $data = array(
                    'source'    => $archiveFilename,
                    'path'      => $filename,
                    'src_size'  => $sourceSize,
                    'dest_size' => $destinationSize,
                    'percent'   => $percent,
                    'smushed'   => $percent > 0 ,
                );
            }

            $source = str_replace(Mage::getBaseDir('skin'), Mage::getBaseUrl('skin'), $data['source']);
            $path = str_replace(Mage::getBaseDir('skin'), Mage::getBaseUrl('skin'), $data['path']);

            echo $this->__("%d percent was compresed size before %d after %d", $data['percent'], $data['src_size'], $data['dest_size']) .
            '<br/>' .
            $source .
            '<br/>' .
            '<img class="grid-image" src="' . $source . '" id="path_source">' .
            '<br/>' .
            $path .
            '<br/>' .
            '<img class="grid-image" src="' . $path . '" id="path_source">';
        } catch (Mage_Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            // Mage::logException($e);
            echo $e->getMessage();
        }
    }

    /**
     * Check the permission to run it
     *
     * @return boolean
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')
            ->isAllowed('templates_master/pagespeed/test');
    }
}
