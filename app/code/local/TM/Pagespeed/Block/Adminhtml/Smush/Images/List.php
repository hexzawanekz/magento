<?php
class TM_Pagespeed_Block_Adminhtml_Smush_Images_List extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_blockGroup = 'tm_pagespeed';
        $this->_controller = 'adminhtml_smush_images_list';

        $this->_headerText = Mage::helper('TM_Pagespeed')->__('Not optimized images');
        parent::__construct();
        $this->_removeButton('add');
    }
}