<?php

class TM_Pagespeed_Block_Adminhtml_System_Config_Form_Field_Test_Gif
    extends Mage_Adminhtml_Block_System_Config_Form_Field
{

    /**
     * Prepare layout.
     * Add files to use dialog windows
     *
     * @return Mage_Adminhtml_Block_System_Email_Template_Edit_Form
     */
    protected function _prepareLayout()
    {
        if ($head = $this->getLayout()->getBlock('head')) {
            $head->addItem('js', 'prototype/window.js')
                ->addItem('js_css', 'prototype/windows/themes/default.css')
                ->addCss('lib/prototype/windows/themes/magento.css')
                ->addItem('js', 'mage/adminhtml/variables.js');
        }
        return parent::_prepareLayout();
    }

    // protected $_path = 'frontend/base/default/images/cvv.gif';
    protected $_path = 'frontend/base/default/images/moneybookers/banner_120_de.gif';

    public function render(Varien_Data_Form_Element_Abstract $element)
    {
        $isTest = Mage::getSingleton('adminhtml/session')->getCheckCssOnConfig();
        if ($isTest) {
            Mage::getSingleton('adminhtml/session')->setCheckCssOnConfig(false);
            return '';
        }
        $id = '_' . strtolower(get_class($this));
        $url  = $this->getUrl('adminhtml/pagespeed_test/smush', array(
            'path' => base64_encode($this->_path)
        ));

        $title = 'Compression results';

        $_id = $element->getHtmlId();

        $html = '<td class="label" colspan="100">'
            . '<button id="' . $_id . '" '
            . 'onclick="check' . $id . '()" class="scalable save" type="button"><span><span><span>'
            . $this->__('Test compression')
            . '</span></span></span></button>'
            // . '<p class="note"><span>' . $this->__('Enable checking js compressing') . '</span></p>'
            . '<script type="text/javascript">'
            . " function check" . $id . '()'
            . " {
                    var url = '" . $url . "';

                    new Ajax.Request(url, {
                        method: 'post',
                        onSuccess: function(transport) {
                            //console.log(transport.responseText);

                            var html" . $id . " =  transport.responseText,

                            config" . $id . " = {
                                closable:true,
                                resizable:true,
                                draggable:true,
                                className:'magento',
                                windowClassName:'popup-window',
                                title:'" . $title . "',
                                top:50,
                                width: 600,
                                height: 500,
                                zIndex:1000,
                                recenterAuto:false,
                                hideEffect:Element.hide,
                                showEffect:Element.show
                            };

                            Dialog.info(html" . $id . ', config' . $id . ");
                        }
                    });

                    return false;
               }

            "
            . '</script>'
            . '</br></td>'
            ;
        return $this->_decorateRowHtml($element, $html);
    }

    /**
     * Decorate field row html
     *
     * @param Varien_Data_Form_Element_Abstract $element
     * @param string $html
     * @return string
     */
    protected function _decorateRowHtml($element, $html)
    {
        return '<tr id="row_' . $element->getHtmlId() . '">' . $html . '</tr>';
    }
}
