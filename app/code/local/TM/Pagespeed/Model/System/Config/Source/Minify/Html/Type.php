<?php
class TM_Pagespeed_Model_System_Config_Source_Minify_Html_Type
{
    const LITE_TYPE    = 'lite';
    const DEFAULT_TYPE = 'default';

    /**
     *
     * @return array
     */
    public function toOptionHash()
    {
        return $this->_toOptionHash();
    }

    /**
     *
     * @return array
     */
    protected function _toOptionHash()
    {
        $result = array();
        foreach (array(self::DEFAULT_TYPE, self::LITE_TYPE) as $value) {
            $result[$value] = Mage::helper('TM_Pagespeed')->__(ucfirst($value));
        }
        return $result;
    }

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        $result = array();
        foreach ($this->_toOptionHash() as $key => $value) {
            $result[] = array('value' => $key, 'label' => $value);
        }
        return $result;
    }
}
