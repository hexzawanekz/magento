<?php
class TM_Pagespeed_Model_System_Config_Source_Smush_Type
{
    const UTIL  = 'util';
    const YAHOO = 'yahoo';
    const RESMUSH = 'resmush';

    /**
     *
     * @return array
     */
    public function toOptionHash()
    {
        return $this->_toOptionHash();
    }

    /**
     *
     * @return array
     */
    protected function _toOptionHash()
    {
        $result = array();
        $services = array(self::UTIL);
        $url = TM_Pagespeed_Model_Service_Yahoo_SmushIt::SERVICE_API_URL;
        list($status) = @get_headers($url);
        if ($status && strpos($status, '404') == false) {
            $services[] = self::YAHOO;
        }

        $url = TM_Pagespeed_Model_Service_ReSmushIt::SERVICE_API_URL;
        list($status) = @get_headers($url);
        if ($status && strpos($status, '404') == false) {
            $services[] = self::RESMUSH;
        }

        foreach ($services as $value) {
            $result[$value] = Mage::helper('TM_Pagespeed')->__(ucfirst($value));
        }
        return $result;
    }

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        $result = array();
        foreach ($this->_toOptionHash() as $key => $value) {
            $result[] = array('value' => $key, 'label' => $value);
        }
        return $result;
    }
}
