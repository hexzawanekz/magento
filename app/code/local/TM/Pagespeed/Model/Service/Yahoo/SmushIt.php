<?php
/**
 *
 * Smush.it PHP Library, a simple PHP library for accessing the Yahoo! Smush.it™
 * lossless image compressor
 * @author  Ghislain PHU <contact@ghislainphu.fr>
 * @version 1.0
 *
 * https://github.com/GhislainPhu/php-lib-smushit
 */
class TM_Pagespeed_Model_Service_Yahoo_SmushIt extends TM_Pagespeed_Model_Service_Abstract
{
    /**
     * Flag used to preserve objects with errors
     */
    const KEEP_ERRORS = 0x01;

    /**
     * Flag used to throw exceptions when an error
     * occurred
     */
//    const THROW_EXCEPTION = 0x02;
    /**
     * Internal flag used to notify that the current
     * source is a local file
     */
    const LOCAL_ORIGIN = 0x04;

    /**
     * Internal flag used to notify that the current
     * source is a remote file (source is an URL)
     */
    const REMOTE_ORIGIN = 0x08;

    /**
     * The base URL of the Yahoo! Smush.it™ API
     */
    const SERVICE_API_URL = "http://www.smushit.com/ysmush.it/ws.php";

    /**
     * Maximum filesize allowed by Yahoo! Smush.it™ API
     */
    const SERVICE_API_LIMIT = 1048576; // 1MB limitation

    /**
     * Error message
     * @access public
     * @var string | null
     */
    public $error;

    /**
     * Source URI
     * @access public
     * @var string | null
     */
    public $source;

    /**
     * Compressed image
     * @access public
     * @var string | null
     */
    public $destination;

    /**
     * Compressed image URL
     * @access public
     * @var string | null
     */
    public $dest;

    /**
     * Filesize of the source image (in Bytes)
     * @access public
     * @var int | null
     */
    public $sourceSize;

    /**
     * Filesize of the compressed image (in Bytes)
     * @access public
     * @var int | null
     */
    public $destinationSize;

    /**
     * Saving percentage
     * @access public
     * @var float | null
     */
    public $percent;

    /**
     * Effective flags
     * @access protected
     * @var int | null
     */
    protected $_flags = null;

    public function setFlags($flags = null)
    {
        $this->_flags = $flags;
        return $this;
    }

    /**
     *
     * @param  string $path file path
     * @return false | array
     */
    public function smush($path)
    {
        if (!is_string($path) || !$this->_check($path)) {
            return false;
        }
        return $this->_smush();
    }

    /**
     * Check if source is a readable local file and doesn't exceed filesize limit
     * @access protected
     * @param  string    $path Location of the current image to compress
     * @return bool
     */
    protected function _check($path)
    {
        if ($this->_setSource($path) === false) {
            $this->error = "$path is not a valid path";
        } elseif ($this->_hasFlag(self::LOCAL_ORIGIN)) {
            if (!is_readable($path)) {
                $this->error = "$path is not readable";
            } elseif (filesize($path) > self::SERVICE_API_LIMIT) {
                $this->error = "$path exceeds 1MB size limit";
            }
        }
        if (!empty($this->error)) {
            return false;
        }
        return true;
    }

    /**
     * Check if the flag $flag is set in the current object
     * @access protected
     * @param  int  $flag The flag to check
     * @return bool
     */
    protected function _hasFlag($flag)
    {
        return (bool)($this->_flags & $flag);
    }

    /**
     * Set SmushIt::$source and check if it's a local or remote file
     * @access protected
     * @param  string $flag The source to set
     * @return false | null
     */
    protected function _setSource($source)
    {
        $this->source = $source;
        if (filter_var($this->source, FILTER_VALIDATE_URL) !== false) {
            $this->_flags |= self::REMOTE_ORIGIN;
        } elseif (file_exists($this->source) && !is_dir($this->source)) {
            $this->_flags |= self::LOCAL_ORIGIN;
        } else {
            return false;
        }
    }

    /**
     * Send current source to the API and get response
     * @access protected
     */
    protected function _smush()
    {
        $handle = curl_init();
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($handle, CURLOPT_TIMEOUT, 20);
        curl_setopt($handle, CURLOPT_CONNECTTIMEOUT, 10);
        if ($this->_hasFlag(self::LOCAL_ORIGIN)) {
            curl_setopt($handle, CURLOPT_URL, self::SERVICE_API_URL);
            curl_setopt($handle, CURLOPT_POST, true);
            if (!defined('CURLOPT_SAFE_UPLOAD')) {
                define('CURLOPT_SAFE_UPLOAD', -1);
            }
            @curl_setopt($handle, CURLOPT_SAFE_UPLOAD, false);
            if (!defined('CURLOPT_POSTFIELDS')) {
                define('CURLOPT_POSTFIELDS', 10015);
            }
            @curl_setopt($handle, CURLOPT_POSTFIELDS, array('files' => '@' . $this->source));
        } else {
            curl_setopt($handle, CURLOPT_URL, self::SERVICE_API_URL . '?img=' . $this->source);
        }
        $json = curl_exec($handle);
        curl_close($handle);

        if ($json === false) {
            Mage::throwException('Yahoo Smush It service unavailabale');
            $this->error = 408;
            return false;
        }
        //prepare response
        $r = json_decode($json);
        if (empty($r)) {
            Mage::throwException('Yahoo Smush It service unavailabale');
            $this->error = 406;
            return false;
        }
        $this->error           = empty($r->error)     ? null : $r->error;
        $this->destination     = empty($r->dest)      ? null : $r->dest;
        $this->sourceSize      = empty($r->src_size)  ? null : intval($r->src_size);
        $this->destinationSize = empty($r->dest_size) ? null : intval($r->dest_size);
        $this->percent         = empty($r->percent)   ? null : floatval($r->percent);

        // save origin source
        $archivedFilename = $this_>_getArchiveFilename($this->source);
        if (!file_exists($archivedFilename)) {
            if (!copy($this->source, $archivedFilename)) {
                return false;
            }
        }

        if (!copy($this->destination, $this->source)) {
            return false;
        }
        chmod($this->source, 0644);
        chmod($this->destination, 0644);

        return array(
            'source'    => $archivedFilename,
            'path'      => $this->source,
            'src_size'  => $this->sourceSize,
            'dest_size' => $this->destinationSize,
            'percent'   => $this->percent,
            'smushed'   => empty($this->error),
            'created'   => now(),
        );
    }

    public function restore($source, $path)
    {
        return file_exists($source)
            && is_readable($source)
            && is_writable($path)
            && copy($source, $path);
    }

    public function check()
    {
        $url = self::SERVICE_API_URL;
        list($status) = @get_headers($url);
        return $status && strpos($status, '404') == false;
    }
}
