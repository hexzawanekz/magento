<?php
class TM_Pagespeed_Model_Service_ReSmushIt extends TM_Pagespeed_Model_Service_Yahoo_SmushIt
{
    /**
     * The base URL of the reSmush.it™ API
     */
    const SERVICE_API_URL = "http://api.resmush.it/ws.php";

    const USER_AGENT = 'ShushIt PHP Client/1.2 (+http://github.com/davgothic/SmushIt)';

    /**
     *
     * @param  string $path file path
     * @return false | array
     */
    public function smush($path)
    {
        if (!is_string($path) || !$this->_check($path)) {
            return false;
        }
        return $this->_smush();
    }

    protected function _getCurlValue($filename, $contentType = null, $postname = null)
    {
        if (null == $contentType) {
            $contentType = mime_content_type($filename);
        }

        if (null == $postname) {
            $postname = basename($filename);
        }
        // PHP 5.5 introduced a CurlFile object that deprecates the old @filename syntax
        // See: https://wiki.php.net/rfc/curl-file-upload
        if (function_exists('curl_file_create')) {
            return curl_file_create($filename, $contentType, $postname);
        }

        // Use the old style if using an older version of PHP
        $value = "@{$filename};filename=" . $postname;
        if ($contentType) {
            $value .= ';type=' . $contentType;
        }

        return $value;
    }

    /**
     * Send current source to the API and get response
     * @access protected
     */
    protected function _smush()
    {
        $handle = curl_init();
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($handle, CURLINFO_HEADER_OUT, true);
        // curl_setopt($handle, CURLOPT_HEADER, true);
        curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($handle, CURLOPT_TIMEOUT, 20);
        curl_setopt($handle, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($handle, CURLOPT_USERAGENT, self::USER_AGENT);
        if ($this->_hasFlag(self::LOCAL_ORIGIN)) {
            curl_setopt($handle, CURLOPT_URL, self::SERVICE_API_URL);
            curl_setopt($handle, CURLOPT_POST, true);
            if (!defined('CURLOPT_SAFE_UPLOAD')) {
                define('CURLOPT_SAFE_UPLOAD', -1);
            }
            // @curl_setopt($handle, CURLOPT_SAFE_UPLOAD, false);
            if (!defined('CURLOPT_POSTFIELDS')) {
                define('CURLOPT_POSTFIELDS', 10015);
            }
            $postData = array('files' => $this->_getCurlValue($this->source));
            curl_setopt(
                $handle,
                CURLOPT_POSTFIELDS,
                $postData
            );
        } else {
            curl_setopt(
                $handle,
                CURLOPT_URL,
                self::SERVICE_API_URL . http_build_query(array('img' => $this->source))
            );
        }
        $json = curl_exec($handle);
        curl_close($handle);

        if ($json === false) {
            Mage::throwException('reSmush It service unavailabale');
            $this->error = 408;
            return false;
        }
        //prepare response
        $r = json_decode($json);
        if (empty($r)) {
            Mage::throwException('reSmush It service unavailabale');
            $this->error = 406;
            return false;
        }
        $this->error           = empty($r->error)     ? null : $r->error;
        $this->destination     = empty($r->dest)      ? null : $r->dest;
        $this->sourceSize      = empty($r->src_size)  ? null : intval($r->src_size);
        $this->destinationSize = empty($r->dest_size) ? null : intval($r->dest_size);
        $this->percent         = empty($r->percent)   ? null : floatval($r->percent);

        // save origin source
        $archivedFilename = $this->_getArchiveFilename($this->source);
        if (!file_exists($archivedFilename)) {
            if (!copy($this->source, $archivedFilename)) {
                return false;
            }
        }

        if (!copy($this->destination, $this->source)) {
            return false;
        }
        chmod($this->source, 0644);
        //chmod($this->destination, 0644);

        return array(
            'source'    => $archivedFilename,
            'path'      => $this->source,
            'src_size'  => $this->sourceSize,
            'dest_size' => $this->destinationSize,
            'percent'   => $this->percent,
            'smushed'   => empty($this->error),
            'created'   => now(),
        );
    }

    public function restore($source, $path)
    {
        return file_exists($source)
            && is_readable($source)
            && is_writable($path)
            && copy($source, $path);
    }
}
