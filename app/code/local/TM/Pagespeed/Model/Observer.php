<?php
class TM_Pagespeed_Model_Observer
{
    const ENABLE_CONFIG          = 'tm_pagespeed/general/enabled';
    const SMUSHIT_ENABLE_CONFIG  = 'tm_pagespeed/smushit/enabled';
    const SMUSHIT_COUNT_CONFIG   = 'tm_pagespeed/smushit/count';
    const EXPIRES_ENABLE_CONFIG  = 'tm_pagespeed/expires/enabled';
    const EXPIRES_DAY_CONFIG     = 'tm_pagespeed/expires/day';
    const MINIFY_CONTENT_ENABLE_CONFIG = 'tm_pagespeed/minify_content/enabled';
    const MINIFY_CONTENT_TYPE_CONFIG   = 'tm_pagespeed/minify_content/type';

    /**
     *
     * @return boolean
     */
    protected function _isAjax()
    {
        return Mage::app()->getRequest()->isAjax();
    }

    protected function _isActual($config)
    {
        return ((bool) Mage::getStoreConfig(self::ENABLE_CONFIG))
            && !Mage::app()->getRequest()->isAjax()
            && !Mage::app()->getStore()->isAdmin()
            && ((bool) Mage::getStoreConfig($config))
        ;
    }

    protected function _isRootBlock($observer)
    {
        $block = $observer->getBlock();
        if (!$block) {
            return false;
        }
        $blockName = $block->getNameInLayout();
        if (empty($blockName) || $blockName !== 'root') {
            return false;
        }
        return true;
    }

    public function minifyOutput($observer)
    {
        // return;
        if (!$this->_isActual(self::MINIFY_CONTENT_ENABLE_CONFIG)
            || !$this->_isRootBlock($observer)) {

            return;
        }

        $transport = $observer->getTransport();
        $html = $transport->getHtml();
        if (stripos($html, '<!DOCTYPE html') === false) {
            return;
        }

        Varien_Profiler::enable();
        $timerName = __METHOD__;
        Varien_Profiler::start($timerName);

        $type = Mage::getStoreConfig(self::MINIFY_CONTENT_TYPE_CONFIG);
        // Zend_Debug::dump($type);
        switch ($type) {
            case TM_Pagespeed_Model_System_Config_Source_Minify_Html_Type::LITE_TYPE:
                $html = TM_Pagespeed_Model_Minify_Html::liteMin($html);
                break;

            case TM_Pagespeed_Model_System_Config_Source_Minify_Html_Type::DEFAULT_TYPE:
            default:
                $html = TM_Pagespeed_Model_Minify_Html::min($html);
                break;
        }

        $transport->setHtml($html);

        Varien_Profiler::stop($timerName);
//        Zend_Debug::dump(Varien_Profiler::fetch($timerName));
    }

    public function setPageExpires($observer)
    {
        if(Mage::registry('download_emarsys_csv')){
            Mage::unregister('download_emarsys_csv');
        }else{
            if (!$this->_isActual(self::EXPIRES_ENABLE_CONFIG)) {
                return;
            }
            /** @var $response Mage_Core_Controller_Response_Http */
            $response = $observer->getResponse();
            $day = (int) Mage::getStoreConfig(self::EXPIRES_DAY_CONFIG); //12
            $accessPlus = 60 * 60 * 24 * $day;
//          $response->setHeader('Expires', gmdate('D, d M Y H:i:s \G\M\T', time() + (3600 * 3600 * 20)));
//          $response->setRawHeader('Expires: ' . gmdate('D, d M Y H:i:s \G\M\T', time() + $accessPlus));
        }
    }

    /**
     *
     * @param Mage_Cron_Model_Schedule $schedule
     * @return TM_Pagespeed_Model_Observer
     */
    public function smush($schedule)
    {
        $isEnabled = ((bool) Mage::getStoreConfig(self::SMUSHIT_ENABLE_CONFIG))
            && ((bool) Mage::getStoreConfig(self::ENABLE_CONFIG));
        if (!$isEnabled) {
            return;
        }
        $model = Mage::getModel("TM_Pagespeed/smushit");
        $model->useThrowException(false);
        $model->check();
        $paths = $model->getPaths();
        $count = (int) Mage::getStoreConfig(self::SMUSHIT_COUNT_CONFIG);
        $model->smush($paths, $count);

        return $this;
    }
}