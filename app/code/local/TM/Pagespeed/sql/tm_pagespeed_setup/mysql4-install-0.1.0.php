<?php

//throw new Exception;
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$table = $installer->getConnection()
    ->newTable($installer->getTable('tm_pagespeed_smushit'))
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, 11, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        ), 'ID')
    ->addColumn('path', Varien_Db_Ddl_Table::TYPE_TEXT, 200, array(
        'nullable'  => false,
        ), 'Path')
    ->addColumn('source', Varien_Db_Ddl_Table::TYPE_TEXT, 200, array(
        'nullable'  => false,
        ), 'Source')
    ->addColumn('src_size', Varien_Db_Ddl_Table::TYPE_INTEGER, 7, array(
        'unsigned'  => true,
        'nullable'  => false,
        ), 'Source size')
    ->addColumn('dest_size', Varien_Db_Ddl_Table::TYPE_INTEGER, 7, array(
        'unsigned'  => true,
        'nullable'  => false,
        ), 'Destanation size')
    ->addColumn('percent', Varien_Db_Ddl_Table::TYPE_SMALLINT, 5, array(
        'unsigned'  => true,
        'nullable'  => true,
        ), 'Percent')
    ->addColumn('smushed', Varien_Db_Ddl_Table::TYPE_TINYINT, 1, array(
        'nullable'  => false,
        'default'   => 0,
        ), 'Optimized')
    ->addColumn('created', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, 10, array(
        'unsigned'  => true,
        'nullable'  => false,
        'default'   => Varien_Db_Ddl_Table::TIMESTAMP_INIT,
        ), 'Created')
    ;
$installer->getConnection()->createTable($table);

$installer->endSetup();
