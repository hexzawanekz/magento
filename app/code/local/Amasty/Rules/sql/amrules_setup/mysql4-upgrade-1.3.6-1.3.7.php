<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$connection = $installer->getConnection();

$productAttributesTable = $installer->getTable('salesrule/product_attribute');
$storesTable = $installer->getTable('core/store');

$connection->addColumn($productAttributesTable, 'store_id', array(
    'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
    'length' => null,
    'unsigned' => true,
    'nullable' => true,
    'comment' => 'Store Reference'
));

$connection->addForeignKey($installer->getFkName(
    'salesrule/product_attribute', 'store_id',
    'core/store', 'store_id'
), $productAttributesTable, 'store_id',
    $storesTable, 'store_id',
    Varien_Db_Ddl_Table::ACTION_CASCADE,
    Varien_Db_Ddl_Table::ACTION_NO_ACTION
);

$installer->endSetup();
