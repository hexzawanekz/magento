<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$connection = $installer->getConnection();

$rulesStoreTable = $installer->getTable('amrules/store');
$rulesTable = $installer->getTable('salesrule/rule');
$storesTable = $installer->getTable('core/store');

$table = $connection->newTable($rulesStoreTable)
    ->addColumn('rule_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'unsigned' => true,
            'nullable' => false,
            'primary' => true
        ),
        'Rule Id'
    )
    ->addColumn('store_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
            'unsigned' => true,
            'nullable' => false,
            'primary' => true
        ),
        'Store Id'
    )
    ->addIndex(
        $installer->getIdxName('amrules/store', array('rule_id')),
        array('rule_id')
    )
    ->addIndex(
        $installer->getIdxName('amrules/store', array('store_id')),
        array('store_id')
    )
    ->addForeignKey($installer->getFkName('amrules/store', 'rule_id', 'salesrule/rule', 'rule_id'),
        'rule_id', $rulesTable, 'rule_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
    )
    ->addForeignKey($installer->getFkName('amrules/store', 'store_id', 'core/store', 'store_id'),
        'store_id', $storesTable, 'store_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE
    )
    ->setComment('Sales Rules To Stores Relations');

$connection->createTable($table);

$installer->endSetup();
