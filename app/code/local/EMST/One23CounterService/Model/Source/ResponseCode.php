<?php
class EMST_One23CounterService_Model_Source_ResponseCode 
{
	public function getResponseCode() {
		return array(
			"000" => "SUCCESS ",
			"001" => "PENDING ",
			"002" => "TIMEOUT ",
			"003" => "INVALID MESSAGE ",
			"004" => "INVALID PROFILE(MERCHANT) ID ",
			"005" => "DUPLICATED INVOICE ",
			"006" => "INVALID AMOUNT ",
			"007" => "INSUFFICIENT BALANCE ",
			"008" => "INVALID CURRENCY CODE ",
			"009" => "PAYMENT EXPIRED ",
			"010" => "PAYMENT CANCELED BY PAYER ",
			"011" => "INVALID PAYEE ID ",
			"998" => "INTERNAL ERROR ",
			"999" => "SYSTEM ERROR ",
		);
	}
	
}