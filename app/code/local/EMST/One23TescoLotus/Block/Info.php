<?php
class EMST_One23TescoLotus_Block_Info extends Mage_Payment_Block_Info
{
    
    protected function _prepareSpecificInformation($transport = null)
    {
        $transport = parent::_prepareSpecificInformation($transport);
        $payment = $this->getInfo();
        return $transport->addData($payment->getAdditionalInformation());
    }
}
