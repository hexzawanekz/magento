<?php
class EMST_One23TescoLotus_PaymentController extends Mage_Core_Controller_Front_Action
{
	protected $_order = null;
	protected $_checkout_session = null;
	protected $_payment_model = null;
	protected $_order_id = null;
	
    protected function getOrder() {
		if ($this->_order == null)
            $this->_order = Mage::getModel('sales/order')->loadByIncrementId($this->getOrderId());
        return $this->_order;
    }
	protected function getOrderId() {
		if(!$this->_order_id) {
			$this->_order_id = $this->getSession()->getLastRealOrderId();
		}
		return $this->_order_id;
	}
	public function setOrder(Mage_Sales_Model_Order $order) {
		$this->_order = $order;
	}
	protected function getSession() {
		if(!$this->_checkout_session)
			$this->_checkout_session=Mage::getSingleton('checkout/session');
		return $this->_checkout_session;
	}
	public function sendTransactionAction() {
		try {
			$order = $this->getOrder();
			$session = $this->getSession();
			$one23tescolotus = $this->getPaymentModel()->setOrder($order);
			if(!$order->getId() || $order->getStatus() == 'complete' || $session->getRedirected() == $order->getId() || !$one23tescolotus->validateRedirect($order)) {
				Mage::getSingleton('checkout/session')->clear();
				$this->norouteAction();
				return;
			}
			$src_data = $one23tescolotus->getRequestData();
			$one23tescolotus
				->setDatafile($src_data['sourcefile'])
				->setEncryptfile($src_data['outputfile'])
				->encryptData();
			
			$order->addStatusHistoryComment(
			Mage::helper('one23tescolotus')->__('Customer was redirected to 123-Tesco Lotus')
			)->save();
			$this->getResponse()
			->setBody($this->getLayout()
			->createBlock('one23tescolotus/redirect')
			->setModel($one23tescolotus)
			->toHtml());
			$session->setRedirected($order->getId());
		}
		catch (Mage_Payment_Model_Info_Exception $e) {
			$this->_getCoreSession()->addError($e->getMessage());
			$this->getSession()->clear();
			$this->_redirect('');
		}
		catch(Exception $e) {
			Mage::log($e->getMessage(),null, 'One23TescoLotus.log');
			$this->_getCoreSession()->addError($this->__('There was an error, please try again'));
			$this->getSession()->clear();
			$this->_redirect('');
		}
	}
	public function merchantAction() {
		try{
			$response_code = Mage::getModel('one23tescolotus/source_responseCode')->getResponseCode();
			$one23tescolotus = $this->getPaymentModel()->setType('MerchantUrl');
			$decrypt = $this->decryptData($this->getRequest()->getParam('OneTwoThreeRes'));
			$msg = '';
			$this->_order_id = $decrypt->InvoiceNo;
			if($this->getOrder()->getId()) {
				$payment = $this->getOrder()->getPayment();
				$channel = $payment->getAdditionalInformation('Channel');
				$agent = $payment->getAdditionalInformation('Bank');
				$hash = $one23tescolotus->hashData($one23tescolotus->getMerchantId().trim($decrypt->InvoiceNo).trim($decrypt->RefNo1));
				
				
				if($hash != (string)$decrypt->HashValue) {
					$msg = 'Invalid Hash value';
				}
				elseif(((string)$decrypt->ResponseCode == '000' || (string)$decrypt->ResponseCode == '001')) {
					$this->_redirect('checkout/onepage/success');	
					$this->getOrder()->addStatusHistoryComment(
						Mage::helper('one23tescolotus')->__('Customer returned from 123-Tesco Lotus')
						)->save();					
					return;
				}
				elseif(isset($response_code[$decrypt->ResponseCode])) {
					
					$msg = $response_code[$decrypt->ResponseCode];
				}
				else {
					$msg = 'Unknown error from the gateway system';
				}
			}
		}
		catch(Exception $e) {
			Mage::log($e->getMessage(),null, 'One23TescoLotus.log');
			$msg = 'There was an error occur while trying to verify your payment';
		}
		if($msg) {
			$this->_getCoreSession()->addError($msg);			
		}
		$this->_redirect('');
	}
	protected function decryptData($data) {
		$response = "\r\n".$data;
		$response = trim($response);
		$one23tescolotus = $this->getPaymentModel()
				->setEncryptResponse($response)
				->decryptData();
		$decrypt = Mage::getModel('core/config_base',$one23tescolotus->getDecryptedRes())->getNode();
		return $decrypt;
	}
	protected function getPaymentModel() {
		if(!$this->_payment_model) {
			$this->_payment_model = Mage::getModel('one23tescolotus/one23TescoLotus');
		}
		return $this->_payment_model;
	}
	public function apiCallUrlAction() {
		try{
			$response_code = Mage::getModel('one23tescolotus/source_responseCode')->getResponseCode();
			$one23tescolotus = $this->getPaymentModel()->setType('APICall');
		    $decrypt = $this->decryptData($this->getRequest()->getParam('OneTwoThreeRes'));
			$this->_order_id = $decrypt->InvoiceNo;
		    $order = $this->getOrder();
			$hash = $one23tescolotus->hashData($one23tescolotus->getMerchantId().$decrypt->InvoiceNo.$decrypt->RefNo1);
			$response = $one23tescolotus->getResponseData();
			if(!$order->getId()) {	
				$response['FailureReason'] = 'Invalid InvoiceNo';
				return;
			}
			$response['MessageID'] = $decrypt->InvoiceNo;
		    $one23tescolotus->setOrder($order)
				->setParsedData($one23tescolotus->parseXml($decrypt));
		    if((string)$decrypt->ResponseCode == '000' && (string)$decrypt->HashValue == $hash) {
				$one23tescolotus->saveInvoice();
		    }
		    else {
				
				$one23tescolotus->logRequest();
		    }
			$response['Result'] = 'SUCCESS';
		}
		catch (Mage_Payment_Model_Info_Exception $e) {			
			$response['FailureReason'] = $e->getMessage();
			$response['Result'] = 'FAILURE';;
		}
		catch(Exception $e) {			
			Mage::log($e->getMessage(),null, 'One23TescoLotus.log');
			$response['FailureReason'] = 'System exception';
			$response['Result'] = 'FAILURE';
		}
		try {
			$xml = $one23tescolotus->getSimpleXml($response,'APICallUrlRes');
			$src_data = $one23tescolotus->writeRequest($xml,array('API_PrintBack','Xml','Encrypt'));
			$one23tescolotus
				->setDatafile($src_data['sourcefile'])
				->setEncryptfile($src_data['outputfile'])
				->encryptData();
		}
		catch(Exception $e) {
			Mage::log($e->getMessage(),null, 'One23TescoLotus.log');
		}
		echo $one23tescolotus->getEncryptedReq();
	}
	protected function _getCoreSession() {
		return Mage::getSingleton('core/session');
	}
}
