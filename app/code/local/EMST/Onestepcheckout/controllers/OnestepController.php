<?php

/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Onestepcheckout to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_Onestepcheckout
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
 */
require_once Mage::getModuleDir('controllers', 'Mage_Checkout') . DS . 'OnepageController.php';

class EMST_Onestepcheckout_OnestepController extends Mage_Checkout_OnepageController {

    protected $_sectionUpdateFunctions = array(
        'payment-method' => '_getPaymentMethodsHtml',
        'shipping-method' => '_getShippingMethodsHtml',
        'review' => '_getReviewHtml',
    );
    protected $_actionsNoCheck = array('index');
    protected $_onestep = null;
    protected $_ouputName = '';
    protected $_sections = array('shipping-method', 'payment-method', 'review');

    public function preDispatch() {
        Mage_Checkout_Controller_Action::preDispatch();
        if (!$this->_getHelper()->validateCandidateDesign()) {
            $this->_redirect($this->_getHelper()->getDefaultCheckoutUrlPath());
            $this->setFlag('', self::FLAG_NO_DISPATCH, true);
            return;
        }
        $this->_preDispatchValidateCustomer();

        $checkoutSessionQuote = $this->_getCheckoutSession()->getQuote();
        if ($checkoutSessionQuote->getIsMultiShipping()) {
            $checkoutSessionQuote->setIsMultiShipping(false);
            $checkoutSessionQuote->removeAllAddresses();
        }
        if ($this->_getHelper()->requireLoggingIn() && !$this->getCustomerSession()->isLoggedIn()) {
            $this->getCustomerSession()->setBeforeAuthUrl(Mage::getUrl('*/*/*', array('_secure' => true)));
            $this->_redirect('customer/account/login');
            $this->setFlag('', self::FLAG_NO_DISPATCH, true);
            return;
        } elseif (!$this->_getHelper()->requireLoggingIn() && !$this->_canShowstep()) {
//            $this->norouteAction();
//            $this->setFlag('', self::FLAG_NO_DISPATCH, true);
            return;
        }
        return $this;
    }

    protected function _initCheckout() {
        $payment = $this->getLayout()->getBlock('checkout.payment.methods');
        if ($payment) {
            $methods = $payment->getMethods();
            if (is_array($methods) && sizeof($methods) == 1) {
                reset($methods);
                $this->_getCheckout()->setDefaultPaymentMethod(current($methods)->getCode());
            }
        }
        $this->_getCheckout()->initCheckout();
        return $this;
    }

    public function indexAction() {
        $this->loadLayout();
        try {
            $quote = $this->_getQuote()->getShippingAddress()->setCollectShippingRates(true)->save();

            if (!$this->_getQuote()->hasItems() || $quote->getHasError() || $this->_getQuote()->getHasError()) {
                $this->_redirect('checkout/cart');
                return;
            }
            if (!$quote->validateMinimumAmount()) {
                $error = Mage::getStoreConfig('sales/minimum_order/error_message') ?
                        Mage::getStoreConfig('sales/minimum_order/error_message') :
                        Mage::helper('checkout')->__('Subtotal must exceed minimum order amount');

                Mage::getSingleton('checkout/session')->addError($error);
                $this->_redirect('checkout/cart');
                return;
            }
            Mage::getSingleton('checkout/session')->setCartWasUpdated(false);
            $this->_initCheckout();
        } catch (Mage_Core_Exception $e) {
            $this->_getCheckoutSession()->addError($e->getMessage());
        }

        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('checkout/session');
        $this->renderLayout();
    }

    public function estimateAction() {
        if ($this->_expireAjax()) {
            return;
        }
        try {
            $result = array();
            $data = $this->getRequest()->getParam('billing');
            if (!$data) {
                $data = $this->getRequest()->getParam('shipping');
            }

            /** @var Mage_Sales_Model_Quote_Address $spAddress */
            $spAddress = $this->_getQuote()->getShippingAddress()
                    ->addData($this->_getHelper()->setCityAsRegion($data))
                    ->setCollectShippingRates(true);
            //->save();

            $spMethod = $this->_getCheckout()->getShippingMethod($spAddress);
            $spAddress->setShippingMethod($spMethod)->save();

            $this->_getQuote()->collectTotals();
            $this->_getHelper()->validateFreeMethod($this->_getQuote());
            $this->_getQuote()->save();
            $result['update_section'] = $this->getSectionHtml($this->_sections);
        } catch (Exception $e) {
            Mage::logException($e);
            $result['error'] = array('message' => 'There was problem while trying to update your address');
        }
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }

    public function setBillingAddressAction() {
        if ($this->_expireAjax()) {
            return;
        }
        try {
            $data = $this->getRequest()->getPost('billing', array());
            if ($this->_getHelper()->hideShippingAddress()) {
                $data['use_for_shipping'] = 1;
            }
            $customerAddressId = $this->getRequest()->getPost('billing_address_id', false);
            if (isset($data['email'])) {
                $data['email'] = trim($data['email']);
            }


            $result = $this->_getCheckout()->setBilling($this->_getHelper()->setCityAsRegion($data), $customerAddressId);
            $this->_getHelper()->validateFreeMethod($this->_getQuote());
            $this->_getQuote()->save();
            if (!isset($result['error'])) {
                $result['update_section'] = $this->getSectionHtml($this->_sections);
            }
        } catch (Exception $e) {
            Mage::logException($e);
            $result['error'] = array('message' => 'There was problem while trying to update your Billing address');
        }
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }

    public function setShippingAddressAction() {
        if ($this->_expireAjax()) {
            return;
        }
        if ($this->_getHelper()->hideShippingAddress()) {
            return;
        }
        try {
            $data = $this->getRequest()->getPost('shipping', array());
            $customerAddressId = $this->getRequest()->getPost('shipping_address_id', false);

            $result = $this->_getCheckout()->setShipping($this->_getHelper()->setCityAsRegion($data), $customerAddressId);
            $this->_getHelper()->validateFreeMethod($this->_getQuote());
            $this->_getQuote()->save();
            if (!isset($result['error'])) {
                $result['update_section'] = $this->getSectionHtml($this->_sections);
            }
        } catch (Exception $e) {
            $result['error'] = array('message' => 'There was problem while trying to update your shipping address');
            Mage::logException($e);
        }
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }

    public function saveShippingMethodAction() {
        try {
            if ($this->_expireAjax()) {
                return;
            }

            $data = $this->getRequest()->getPost('shipping_method', '');
            $result = $this->_getCheckout()->saveShippingMethod($data);
            // $quote = $this->_getQuote();
            if (!$result) {
                Mage::dispatchEvent('checkout_controller_onepage_save_shipping_method', array('request' => $this->getRequest(),
                    'quote' => $this->_getQuote()));
            }
            $this->_getQuote()->collectTotals();
            $this->_getHelper()->validateFreeMethod($this->_getQuote());
            $this->_getQuote()->save();
            $result['update_section'] = $this->getSectionHtml(array('payment-method', 'review'));
        } catch (Exception $e) {
            $result['error'] = array('message' => 'There was problem while trying to update Shipping method');
            Mage::logException($e);
        }
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }

    public function savePaymentMethodAction() {
        if ($this->_expireAjax()) {
            return;
        }
        try {
            $result = array();

            $this->_getQuote()->removePayment();
            $this->_getQuote()->setTotalsCollectedFlag(false);

            $data = $this->getRequest()->getPost('payment', array());
            if (!$this->_getHelper()->validateFreeMethod($this->_getQuote(), $data)) {
                $data['method'] = 'free';
            }
            $result = $this->_getCheckout()->savePayment($data);

            $redirectUrl = $this->_getCheckout()->getQuote()->getPayment()->getCheckoutRedirectUrl();
            if (empty($result['error']) && !$redirectUrl) {
                $result['update_section'] = $this->getSectionHtml(array('review'));
            }
            if ($redirectUrl) {
                $result['redirect'] = $redirectUrl;
            }
        } catch (Mage_Payment_Exception $e) {
            if ($e->getFields()) {
                $result['fields'] = $e->getFields();
            }
            $result['error'] = $e->getMessage();
        } catch (Mage_Core_Exception $e) {
            $result['error'] = $e->getMessage();
        } catch (Exception $e) {
            Mage::logException($e);
            $result['error'] = $this->__('Unable to set Payment Method.');
        }
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }

    public function saveAction() {
        if ($this->_expireAjax()) {
            return;
        }
        $billing = $this->getRequest()->getPost('billing');
        $shipping = $this->getRequest()->getPost('shipping');
        $shippingMethod = $this->getRequest()->getPost('shipping_method');
        $paymentMethod = $this->getRequest()->getPost('payment');
        try {
            if (!$this->getRequest()->isPost()) {
                $this->_ajaxRedirectResponse();
                return;
            }
            if (!$this->_getHelper()->validateFreeMethod($this->_getQuote(), $paymentMethod)) {
                $paymentMethod['method'] = 'free';
            }
			if (!empty($shippingMethod)) {
                $result = $this->_getCheckout()->saveShippingMethod($shippingMethod);
            }
            if (!empty($billing)) {
                $customerAddressId = $this->getRequest()->getPost('billing_address_id', false);

                $result = $this->_getCheckout()->setBilling($this->_getHelper()->setCityAsRegion($billing), $customerAddressId);
            }
            if (!isset($result['error']) && !empty($shipping) && isset($billing['use_for_shipping']) && !$billing['use_for_shipping']) {
                $customerAddressId = $this->getRequest()->getPost('shipping_address_id', false);

                $result = $this->_getCheckout()->setShipping($this->_getHelper()->setCityAsRegion($shipping), $customerAddressId);
            }
            /* if (!isset($result['error']) && !empty($shippingMethod)) {
                $result = $this->_getCheckout()->saveShippingMethod($shippingMethod);
            } */
            if (!isset($result['error'])) {
                if ($requiredAgreements = Mage::helper('checkout')->getRequiredAgreementIds()) {
                    $postedAgreements = array_keys($this->getRequest()->getPost('agreement', array()));
                    if ($diff = array_diff($requiredAgreements, $postedAgreements)) {
                        $result['success'] = false;
                        $result['error'] = true;
                        $result['error_messages'] = $this->__('Please agree to all the terms and conditions before placing the order.');
                        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
                        return;
                    }
                }
                if ($paymentMethod) {
                    $this->_getQuote()->getPayment()->importData($paymentMethod);
                }
                $this->_revalidateInputData();
                $this->_getCheckout()->saveOrder();

                $redirectUrl = $this->_getCheckout()->getCheckout()->getRedirectUrl();
                $result['success'] = true;
                $result['error'] = false;
            }
            /*
             * Fix cannot save customer password
             */
            if($billing['customer_password']){
                Mage::getModel('customer/session')->getCustomer()->setPassword($billing['customer_password'])->save();
            }
        } catch (Mage_Payment_Model_Info_Exception $e) {
            $message = $e->getMessage();
            if (!empty($message)) {
                $result['error_messages'] = $message;
            }
        } catch (Mage_Core_Exception $e) {
            Mage::logException($e);
            Mage::helper('checkout')->sendPaymentFailedEmail($this->getOnepage()->getQuote(), $e->getMessage());
            $result['success'] = false;
            $result['error'] = true;
            $result['error_messages'] = $e->getMessage();
        } catch (Exception $e) {
            Mage::logException($e);
            Mage::helper('checkout')->sendPaymentFailedEmail($this->getOnepage()->getQuote(), $e->getMessage());
            $result['success'] = false;
            $result['error'] = true;
            $result['error_messages'] = $this->__('There was an error processing your order. Please contact us or try again later.');
        }
        $this->_getQuote()->save();
        if (isset($redirectUrl)) {
            $result['redirect'] = $redirectUrl;
        }

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }

    protected function _revalidateInputData() {
        $this->_getQuote()->getShippingAddress()->setCollectShippingRates(true);
        $this->_getQuote()->collectTotals();
    }

    protected function _getLayoutMessageSection() {
        return array(
            'name' => 'layout-message',
            'html' => $this->_initLayoutMessages('checkout/session')
                    ->_initLayoutMessages('catalog/session')->getLayout()->getMessagesBlock()->getGroupedHtml(),
        );
    }

    protected function getSectionHtml($type) {
        $result = array();
        if (is_array($type)) {
            foreach ($type as $_type) {
                $result[] = $this->getSectionHtml($_type);
            }
        } else {
            $this->_ouputName = $type;
            switch ($type) {
                case 'review':
                    $result = $this->_getBlockHtml('onestepcheckout_onestep_review');
                    break;
                case 'shipping-method':
                    $result = $this->_getBlockHtml('onestepcheckout_onestep_shippingmethod');
                    break;
                case 'payment-method':
                    $result = $this->_getBlockHtml('onestepcheckout_onestep_paymentmethod');
                    break;
                case 'layout-message':
                    $result = $this->_getLayoutMessageSection();
                    break;
                default:
                    break;
            }
        }
        return $result;
    }

    protected function _getBlockHtml($blockName) {
        $layout = $this->getLayout();
        $layout->getUpdate()->addHandle($blockName)->merge($blockName);
        $layout->generateXml()->generateBlocks();
        $output = $layout->getOutput();
        return array(
            'name' => $this->_ouputName,
            'html' => $output
        );
    }

    public function isActive() {
        return $this->_getHelper()->isActive();
    }

    /**
     * @return Mage_Sales_Model_Quote
     */
    protected function _getQuote() {
        return $this->_getCheckout()->getQuote();
    }

    protected function _getCheckoutSession() {
        return Mage::getSingleton('checkout/session');
    }

    public function getCustomerSession() {
        return Mage::getSingleton('customer/session');
    }

    public function _canShowstep() {
        return $this->getCustomerSession()->isLoggedIn() || !in_array($this->getRequest()->getActionName(), $this->_actionsNoCheck) || $this->_getHelper()->isAllowedGuestCheckout($this->_getCheckout()->getQuote()) || !$this->_getHelper()->isCustomerMustBeLogged();
    }

    /**
     * @return EMST_Onestepcheckout_Helper_Data
     */
    protected function _getHelper() {
        return Mage::helper('onestepcheckout');
    }

    /**
     * @return EMST_Onestepcheckout_Model_Checkout_Type_Onestep
     */
    public function _getCheckout() {
        if (!$this->_onestep) {
            $this->_onestep = Mage::getSingleton('onestepcheckout/checkout_type_onestep');
        }
        return $this->_onestep;
    }

    public function successAction() {
        $session = $this->_getCheckout()->getCheckout();
        if (!$session->getLastSuccessQuoteId()) {
            $this->_redirect('checkout/cart');
            return;
        }

        $lastQuoteId = $session->getLastQuoteId();
        $lastOrderId = $session->getLastOrderId();
        $lastRecurringProfiles = $session->getLastRecurringProfileIds();
        if (!$lastQuoteId || (!$lastOrderId && empty($lastRecurringProfiles))) {
            $this->_redirect('checkout/cart');
            return;
        }

        $session->clear();
        $this->loadLayout();
        //Subscribe newsletter
        $customerEmail = Mage::getModel('sales/order')->load($lastOrderId)->getCustomerEmail();
        $this->_autoSubscribe($customerEmail);
        //End subscriber newsletter
        $this->_initLayoutMessages('checkout/session');
        Mage::dispatchEvent('checkout_onepage_controller_success_action', array('order_ids' => array($lastOrderId)));
        $this->renderLayout();
        $session->clearHelperData();
    }

    /**
     * save checkout billing address
     */
    public function saveMobileBillingAction() {
        if ($this->_expireAjax()) {
            return;
        }
        if ($this->getRequest()->isPost()) {
//            $postData = $this->getRequest()->getPost('billing', array());
//            $data = $this->_filterPostData($postData);
            $data = $this->getRequest()->getPost('billing', array());
            $customerAddressId = $this->getRequest()->getPost('billing_address_id', false);

            if (isset($data['email'])) {
                $data['email'] = trim($data['email']);
            }
            $result = $this->getOnepage()->saveBilling($data, $customerAddressId);

            if (!isset($result['error'])) {
                /* check quote for virtual */
                if ($this->getOnepage()->getQuote()->isVirtual()) {
                    $result['goto_section'] = 'payment';
                    $result['update_section'] = $this->getSectionHtml('payment-method');
                } elseif (isset($data['use_for_shipping']) && $data['use_for_shipping'] == 1) {
                    $result['goto_section'] = 'shipping_method';
                    $result['update_section'] = $this->getSectionHtml('shipping-method');

                    $result['allow_sections'] = array('shipping');
                    $result['duplicateBillingInfo'] = 'true';
                } else {
                    $result['goto_section'] = 'shipping';
                }
            }

            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
        }
    }

    /**
     * Shipping method save action
     */
    public function saveMobileShippingMethodAction() {
        if ($this->_expireAjax()) {
            return;
        }
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getPost('shipping_method', '');
            $result = $this->getOnepage()->saveShippingMethod($data);
            /*
              $result will have erro data if shipping method is empty
             */
            if (!$result) {
                Mage::dispatchEvent('checkout_controller_onepage_save_shipping_method', array('request' => $this->getRequest(),
                    'quote' => $this->getOnepage()->getQuote()));
                $this->getOnepage()->getQuote()->collectTotals();
                $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));

                $result['goto_section'] = 'payment';
                $result['update_section'] = $this->getSectionHtml('payment-method');
            }
            $this->getOnepage()->getQuote()->collectTotals()->save();
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
        }
    }

    /**
     * Save payment ajax action
     *
     * Sets either redirect or a JSON response
     */
    public function saveMobilePaymentAction() {
        if ($this->_expireAjax()) {
            return;
        }
        try {
            if (!$this->getRequest()->isPost()) {
                $this->_ajaxRedirectResponse();
                return;
            }

            // set payment to quote
            $this->_getQuote()->removePayment();
            $this->_getQuote()->setTotalsCollectedFlag(false);

            $result = array();
            $data = $this->getRequest()->getPost('payment', array());
            $result = $this->getOnepage()->savePayment($data);

            // get section and redirect data
            $redirectUrl = $this->getOnepage()->getQuote()->getPayment()->getCheckoutRedirectUrl();
            if (empty($result['error']) && !$redirectUrl) {
                $this->loadLayout('checkout_onepage_review');
                $result['goto_section'] = 'review';
                $result['update_section'] = $this->getSectionHtml('review');
            }
            if ($redirectUrl) {
                $result['redirect'] = $redirectUrl;
            }
        } catch (Mage_Payment_Exception $e) {
            if ($e->getFields()) {
                $result['fields'] = $e->getFields();
            }
            $result['error'] = $e->getMessage();
        } catch (Mage_Core_Exception $e) {
            $result['error'] = $e->getMessage();
        } catch (Exception $e) {
            Mage::logException($e);
            $result['error'] = $this->__('Unable to set Payment Method.');
        }
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }
    
    /*
     * Login action on checkout page
     */
    public function loginAction() {
        if (Mage::getSingleton('customer/session')->isLoggedIn()) {
            $this->_redirect('*/*/');
            return;
        }
        $session = Mage::getSingleton('customer/session');

        if ($this->getRequest()->isPost()) {
            $login = $this->getRequest()->getPost('login');
            if (!empty($login['username']) && !empty($login['password'])) {
                try {
                    $session->login($login['username'], $login['password']);
                    if ($session->getCustomer()->getIsJustConfirmed()) {
                        $this->_welcomeCustomer($session->getCustomer(), true);
                    }
                } catch (Mage_Core_Exception $e) {
                    switch ($e->getCode()) {
                        case Mage_Customer_Model_Customer::EXCEPTION_EMAIL_NOT_CONFIRMED:
                            $value = Mage::helper('customer')->getEmailConfirmationUrl($login['username']);
                            $message = Mage::helper('customer')->__('This account is not confirmed. <a href="%s">Click here</a> to resend confirmation email.', $value);
                            break;
                        case Mage_Customer_Model_Customer::EXCEPTION_INVALID_EMAIL_OR_PASSWORD:
                            $message = $e->getMessage();
                            break;
                        default:
                            $message = $e->getMessage();
                    }
                    $session->addError($message);
                    $session->setUsername($login['username']);
                } catch (Exception $e) {
                    // Mage::logException($e); // PA DSS violation: this exception log can disclose customer password
                }
            } else {
                $session->addError($this->__('Login and password are required.'));
            }
        }
        $this->_redirect('*/*/');
    }
    
    /*
     * Subscriber newsletter
     */
    protected function _autoSubscribe($email)
    {
        $subscriber = Mage::getModel('newsletter/subscriber')->loadByEmail($email);
        if($subscriber->getStatus() != Mage_Newsletter_Model_Subscriber::STATUS_SUBSCRIBED &&
                $subscriber->getStatus() != Mage_Newsletter_Model_Subscriber::STATUS_UNSUBSCRIBED) {
            $subscriber->setImportMode(true)->subscribe($email);
        }
    }

}
