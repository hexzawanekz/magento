<?php
/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Onestepcheckout to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_Onestepcheckout
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
*/

class EMST_Onestepcheckout_Block_Onepage_Shipping extends Mage_Checkout_Block_Onepage_Shipping
{
	public function getAddressCollection()
    {
        $collection = $this->getData('address_collection');
        if (is_null($collection)) {
            $collection = $this->getCustomer()->getAddresses();
            $this->setData('address_collection', $collection);
        }
        return $collection;
    }
	public function getSetAddressUrl() {
		return $this->getUrl('*/*/setShippingAddress');
	}
	public function getEstimateUrl() {
		return $this->getUrl('*/*/estimate');
	}
	public function isDefault($address) {
		return ($this->getCustomer()->getDefaultShipping() == $address->getId())?true:false;
	}
	public function getSubDistrict($address) {
		return Mage::helper('onestepcheckout')->getDistrictData($address,1);
	}
	public function getDistrict($address) {
		return Mage::helper('onestepcheckout')->getDistrictData($address,2);
	}
	public function hideShippingAddress() {
		return Mage::helper('onestepcheckout')->hideShippingAddress();
	}
}