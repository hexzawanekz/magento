<?php

class EMST_NetsuiteExport_Model_Resource_BundleItems extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct() {
        $this->_init('netsuiteexport/sales_flat_order_item_bundles', 'item_id');
    }
}