<?php

class EMST_NetsuiteExport_Model_BundleItems extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('netsuiteexport/bundleItems');
    }

    public function getBundleItemsInOrder($orderId, $productId, $storeId)
    {
        /** @var EMST_NetsuiteExport_Model_BundleItems $bundleItem */
        $bundleItem = Mage::getModel('netsuiteexport/bundleItems')->getCollection()
            ->addFieldToSelect('*')
            ->addFieldToFilter('order_id', array('eq' => $orderId))
            ->addFieldToFilter('store_id', array('eq' => $storeId))
            ->addFieldToFilter('product_id', array('eq' => $productId))
            ->getFirstItem();

        if($bundleItem->getData('item_id')){
            return array(
                'bundle_sku' => $bundleItem->getData('bundle_sku'),
                'bundle_qty' => $bundleItem->getData('bundle_qty'),
                'bundle_price' => $bundleItem->getData('bundle_price'),
            );
        }
        return false;
    }
}