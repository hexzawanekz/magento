<?php
class EMST_NetsuiteExport_Model_Source_TimeFilter
{
	protected $_timeFilter = array(
	array('value'=>'10','label'=>'10 Days'),
	array('value'=>'20','label'=>'20 Days'),
	array('value'=>'30','label'=>'30 Days'),
	array('value'=>'90','label'=>'3 Months'),
	array('value'=>'all','label'=>'All'),
	);
	
	public function toOptionArray() {        
        return $this->_timeFilter;
    }
}
