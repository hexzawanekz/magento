<?php
/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Sales to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_Sales
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
*/

class EMST_NetsuiteExport_Model_Cronjobs
{	
	CONST UPDATE_EXIST = 1; // items successfully imported already
	CONST NEW_ITEM = 0; // new items
	CONST UPDATE = 2; // items have been updated in magento and need to be updated to netsuite
	CONST NETSUITE_EXIST = 3; // status for sync, point out that order already exist in netsuite(imported) and need to be updated
	CONST CANCEL_UPDATE = 4; // status for sync, update canceled order
	CONST NETSUITE_CUSTOMER_NEW = 1; // new customers(have not yet purchased yet)
	CONST NETSUITE_CUSTOMER_EXIST = 0; // existing customer(purchased item already)
	protected $_helper = null;
	
	protected $_countryNames = null;
	protected $_shippingMethods = null;
	protected $_netsuiteConfig = null;
	protected $_outerAdapter = null;
	protected $_readAdapter = null;
	protected $_writeAdapter = null;
	protected $_salesResource = null;
	protected $_outerResource = null;
	protected $_salesOuterTable = null;
	protected $_addedIncrement = null;
	
	
	protected function _getHelper() {
		if(!$this->_helper) {
			$this->_helper = Mage::helper('netsuiteexport');
		}
		return $this->_helper;
	}
	protected function _getChangedOrderIds($status = EMST_NetsuiteExport_Model_Resource_Sales::UPDATE) { // default get changed orders
		$changedOrders  = Mage::getModel('netsuiteexport/sales')
			->getCollection()
			->addFieldToFilter('status',$status); // get updated orders
		return $changedOrders->getAllIds();
	}
	public function exportUpdatedOrders($ids = null) {
		if(is_null($ids)) {
			$ids = $this->_getChangedOrderIds();
		}
		
		if(!$ids || !sizeof($ids)) {
			return ;
		}
		$update = $this->_getCollection($ids);
		
		$this->exportToOuterDb($update);
	}
	public function retryFailedOrders() {	
		$ids = $this->_getChangedOrderIds(EMST_NetsuiteExport_Model_Resource_Sales::FAILURE);		
		if(!sizeof($ids)) {
			return ;
		}
		$failedOrders = $this->_getCollection($ids);
		$this->exportToOuterDb($failedOrders);
	}
	public function exportNewOrders() {
		$this->exportToOuterDb($this->_getCollection());
	}
	protected function addQuoteArrElement($arr) {
		foreach($arr as $v=>$k) {
			$arr[$v] = "'".$k."'";
		}
		return $arr;
	}
	protected function _getCollection($ids = null) {
		$netCc = $this->_getHelper()->getNetsuiteSalesConfig();
		$orderItems = Mage::getResourceModel('netsuiteexport/salesExport_collection')
			->addRequireFields()
			->addInvoiceFields()
			->addOrderItems()
			->addPaymentToSelect()
			->addAddressFields()
			->addStatusFilter($netCc['statusfilter'])
			->addPaymentFilter($netCc['paymentfilter'])
			->addShippingFilter($netCc['shippingfilter'])
			->addTimeFilter($netCc['timefilter'])				
			->fillterLoggedItems($netCc['timefilter'])				
			->setOrder('increment_id')
			;
		if($ids) {			
			if(is_array($ids) && sizeof($ids)) {
				$orderItems->addFieldToFilter('main_table.increment_id',array('in'=>$ids));
			}
			else {
				$orderItems->addFieldToFilter('main_table.increment_id',$ids);
			}
		}		
		return $orderItems;
	}
	public function manuallyExport($ids) {
		if($ids && !is_array($ids)) {
			$ids = array($ids);
		}
		if(!sizeof($ids)) {
			return false;
		}
		$manualOrders = $this->_getCollection($ids);
		if(!$manualOrders->count()) {
			return false;
		}
		$this->exportToOuterDb($manualOrders);
		return true;
	}
	public function exportCancelOrders() {
		$ids = $this->_getChangedOrderIds(EMST_NetsuiteExport_Model_Resource_Sales::CANCEL_UPDATE);		
		if(!sizeof($ids)) {
			return ;
		}
		$ids = implode(',',$this->addQuoteArrElement($ids));
		
		$outerTable = $this->getOuterTable();
		$this->_getOuterAdapter()->update($outerTable,array('sync_status'=>EMST_NetsuiteExport_Model_Cronjobs::CANCEL_UPDATE),
			" order_id in ({$ids})");
		$logTable = $this->_getSalesTable();
		$this->getSalesWriteAdapter()->update($logTable,array('status'=>EMST_NetsuiteExport_Model_Resource_Sales::SUCCESS),
			" order_increment_id in ({$ids})");		
	}
	public function salesOrderExport() {
		try{
			if(!$this->_getHelper()->isCronActive()) {
				return ;
			}
			// update order to export
			$this->exportUpdatedOrders();
			$this->retryFailedOrders();
			$this->exportNewOrders();
			$this->exportCancelOrders();
			
		}
		catch(Exception $e) {
			Mage::log($e->getMessage(),null,'Cronjob.log');
		}
	}	
	protected function _checkAccType($item) {
		if($item->getCustomerId()) {
			$customer = Mage::getModel('customer/customer')->load($item->getCustomerId());
			$type = $customer->getAccType();
			$customer->setAccType(self::NETSUITE_CUSTOMER_EXIST)->save();
			if($type === self::NETSUITE_CUSTOMER_EXIST) {
				return self::NETSUITE_CUSTOMER_EXIST;
			}
		}
		return self::NETSUITE_CUSTOMER_NEW;
	}
	public function exportToOuterDb($coll) {
		if(!sizeof($coll)) {
			return ;
		}
		$adapter = $this->_getOuterAdapter();
		$salesAdapter = $this->getSalesWriteAdapter();
		$adapter->beginTransaction();
		$salesAdapter->beginTransaction();
		try {
            $itemLine = array();
           
			$tracking = array();
            foreach($coll as $item) {
				$verify = $this->_verifyItem($item);
				
				if(!$verify) {
					continue;
				}
				$status = self::NEW_ITEM;
				/* if($item->getAccountType()=== self::NETSUITE_CUSTOMER_EXIST) {
					$accType = self::NETSUITE_CUSTOMER_EXIST;
				}
				else {					
					$accType = $this->_checkAccType($item);
				} */
				$item->setAccType(0);
				if($this->_addedIncrement != $item->getIncrementId()) {
					foreach($this->_getHelper()->getItemType() as $type) {
						if(!$item->getData('cod_fee') && $type == EMST_NetsuiteExport_Helper_Data::ITEM_TYPE_COD_FEE) {
							continue;
						}
						if(!$item->getData('rewardpoints') && $type == EMST_NetsuiteExport_Helper_Data::ITEM_TYPE_REWARD_POINTS) {
							continue;
						}
						if(!$item->getData('shipping_incl_tax') && $type == EMST_NetsuiteExport_Helper_Data::ITEM_TYPE_SHIPPING_AMOUNT) {
							continue;
						}
						$discount = abs(abs($item->getData('discount_amount')) - abs($item->getData('rewardpoints')));
						if(((float)$discount<=0.001 || (int)$discount == 0) && $type == EMST_NetsuiteExport_Helper_Data::ITEM_TYPE_DISCOUNT_RULES) {
							continue;
						}
						$item->setType($type);
						$data = $this->_extractData($item,$status);	
						$itemLine[] = $data;
						/* if($verify === self::UPDATE_EXIST) {
							$this->_updateData($data,$external);
						}
						else {
							$itemLine[] = $data;
						} */
					}
				}
				else {
					$item->setType(EMST_NetsuiteExport_Helper_Data::ITEM_TYPE_PRODUCT_PRICE);
					$data = $this->_extractData($item,$status);
					$itemLine[] = $data;
					/* if($verify === self::UPDATE_EXIST) {
						$this->_updateData($data);
					}
					else {
						$itemLine[] = $data;
					} */
				}
				$this->_addedIncrement = $item->getIncrementId();
				if (count($itemLine) >= 500) {
					$this->_saveData($itemLine);					
					$itemLine = array();
				}
				if (count($tracking) >= 500) {
					$this->_logInfo($tracking);	
					$tracking = array();
				}
				$current = array('order_increment_id'=>$item->getIncrementId(),
				'status'=>EMST_NetsuiteExport_Model_Resource_Sales::SUCCESS,
				'created_at'=>Varien_Date::now());
				if(!in_array($current,$tracking)) {
					$tracking[] = $current;
				}
            }			
            $this->_saveData($itemLine);
			$this->_logInfo($tracking);
        } catch (Mage_Core_Exception $e) {
            $adapter->rollback();
            $salesAdapter->rollback();
            Mage::throwException($e->getMessage());
        } catch (Exception $e) {
            $adapter->rollback();
            $salesAdapter->rollback();
            Mage::throwException($e->getMessage());
        }
        $adapter->commit();
        $salesAdapter->commit();
	}
	protected function _getOuterAdapter() {
		if(!$this->_outerAdapter) {
			$this->_outerAdapter = $this->_getOuterResource()->getReadConnection();
		}
		return $this->_outerAdapter;
	}
	protected function _getSalesReadAdapter() {
		if(!$this->_readAdapter) {
			$this->_readAdapter = $this->_getSalesResource()->getReadConnection();
		}
		return $this->_readAdapter;
	}
	protected function getSalesWriteAdapter() {
		if(!$this->_writeAdapter) {
			$this->_writeAdapter = $this->_getSalesResource()->getWriteConnection();
		}
		return $this->_writeAdapter;
	}
	protected function _getSalesResource() {
		if(!$this->_salesResource) {
			$this->_salesResource = Mage::getSingleton('netsuiteexport/sales')->getResource();
		}
		return $this->_salesResource;
	}
	protected function _getOuterResource() {
		if(!$this->_outerResource) {
			$this->_outerResource = Mage::getSingleton('netsuiteexport/salesExternal')->getResource();
		}
		return $this->_outerResource;
	}
	public function getOuterTable() {
		if(!$this->_salesOuterTable) {
			$name = $this->_getHelper()->getSalesOuterTable();
			if(!$name) {
				$name = $this->_getOuterResource()->getTable('netsuiteexport/netsuite_sales_order');
			}
			$this->_salesOuterTable = $name;
		}
		return $this->_salesOuterTable;
	}
	protected function _getSalesTable() {
		return $this->_getSalesResource()->getTable('netsuiteexport/netsuite_sales_order');
	}
	protected function _saveData(array $data)  {
        if (!empty($data)) {
            $rows_result = $this->_getOuterAdapter()->insertOnDuplicate($this->getOuterTable(), $data);
        }

        return $this;
    }
	protected function _updateData(array $data)  {
        if (!empty($data)) {
            $rows_result = $this->_getOuterAdapter()->update($this->getOuterTable(), $data,"order_id = '{$data['order_id']}'");
        }

        return $this;
    }
	protected function _logInfo($data) {
		if (!empty($data)) {
            $rows_result = $this->getSalesWriteAdapter()->insertOnDuplicate($this->_getSalesTable(), $data);
        }
        return $this;
	}
	protected function _magentoVerify($item) {
		$mAdapter = $this->_getSalesReadAdapter();		
		$mSql = $mAdapter->select()->from(array('e' => $this->_getSalesTable()), 
		'e.status')
		->where("e.order_increment_id = ?", $item->getIncrementId());		
		$result = $mAdapter->raw_fetchRow($mSql,'status');
		return $result;
	}
	protected function _outerVerify($item) {
		$oAdapter = $this->_getOuterAdapter();
        $oSql = $oAdapter->select()->from(array('e' => $this->getOuterTable()), 
		'e.order_id')
		->where("e.order_id = ?", $item->getIncrementId());
		return $oAdapter->raw_fetchRow($oSql,'order_id');
	}
	protected function _verifyItem($item) {	
		
		$innerExist = $this->_magentoVerify($item);
		
		if ($this->_outerVerify($item)) {				
		
			if($innerExist === false) {
				$this->getSalesWriteAdapter()->insert($this->_getSalesTable(),array('order_increment_id'=>$item->getIncrementId(),
				'status'=>EMST_NetsuiteExport_Model_Resource_Sales::SUCCESS));
			}
			elseif($innerExist == EMST_NetsuiteExport_Model_Resource_Sales::UPDATE) { // imported but need to update				
				$this->_getOuterAdapter()->delete($this->getOuterTable(),"order_id ='{$item->getIncrementId()}'");
				return true;
			}
			
            return false;
        }
		else {
			if($innerExist === false) { // not imported yet, need to be processed
				$this->getSalesWriteAdapter()->update($this->_getSalesTable(),array('status'=>0),"order_increment_id = '{$item->getIncrementId()}'");
			}
			elseif($innerExist == EMST_NetsuiteExport_Model_Resource_Sales::SUCCESS) { // successfully imported, no need to improt again
				return false;
			}
			
            
		}
        return true;
	}
	protected function _netsuiteConfig($item) {
		if(!$this->_netsuiteConfig) {
			$this->_netsuiteConfig = $this->_getHelper()->getNetsuiteConfigFields($item);
		}
		return $this->_netsuiteConfig;
	}
	protected function _extractData($item,$status = 0) {
		$config = $this->_netsuiteConfig($item);
		return array(			
			'customer_id'=>$item->getCustomerId(),
			'external_id'=>$item->getCustomerId(),
			'item_external_id'=>$this->_getHelper()->renderItemName($item,true),
			'firstname'=>$item->getCustomerFirstname(),
			'lastname'=>$item->getCustomerLastname(),
			'customer_status'=>$config['netsuite_status'],
			'subsidiary'=>$config['subsidiary'],
			'individual'=>'T',
			'default_billing'=>'T',
			'default_shipping'=>'T',
			// 'company'=>$item->getBillingCompany(),
			'order_id'=>$item->getIncrementId(),
			'invoice_date'=>$this->_getHelper()->formatDatetime($item->getData('invoice_created_at')),
			// 'order_date'=>$this->_getHelper()->formatDateTime($item->getData('order_created_at')), // DD/MM/YYY H:i:s
			'order_date'=>$this->_getHelper()->formatDatetime($item->getData('order_created_at')), // YYYY/MM/DD H:i:s					
			'email'=>$item->getCustomerEmail(),
			'payment_type'=>$this->_getHelper()->getPaymentMethod($item->getData('payment_method')),
			'payment_received'=>$item->getData('payment_received'),
			'shipping_type'=>$this->_getHelper()->getShippingMethod($item->getData('shipping_method')),
			'billing_addressee'=>$item->getBillingName(),
			'billing_address1'=>$this->_getHelper()->getBillingStreet($item),
			'billing_address2'=>$this->_getHelper()->getBillingStreet($item,2),
			'billing_city'=>$item->getBillingCity(),
			'billing_state'=>$item->getBillingRegion(),
			'billing_zip'=>$item->getBillingPostcode(),
			'billing_country'=>$this->_getHelper()->renderCountry($item->getBillingCountryId()),
			'billing_phone'=>$this->_getHelper()->renderPhone($item->getBillingPhone()),
			'shipping_addressee'=>$item->getShippingName(),
			'shipping_address1'=>$this->_getHelper()->getShippingStreet($item),
			'shipping_address2'=>$this->_getHelper()->getShippingStreet($item,2),
			'shipping_city'=>$item->getShippingCity(),
			'shipping_state'=>$item->getShippingRegion(),
			'shipping_zip'=>$item->getShippingPostcode(),
			'shipping_country'=>$this->_getHelper()->renderCountry($item->getShippingCountryId()),
			'shipping_phone'=>$this->_getHelper()->renderPhone($item->getShippingPhone()),
			'item'=>$this->_getHelper()->renderItemName($item),
			'qty'=>$this->_getHelper()->rederQty($item),
			'rate'=>$this->_getHelper()->renderItemPrice($item),
			// 'discount'=>$item->getCouponRuleName(), // remove discount column to make them to be line items
			'item_price_level'=>$this->_getHelper()->renderPriceLevel($item),
			'location'=>$this->_getHelper()->renderOtherConfig('warehouse_location',$item),
			'business_unit'=>$this->_getHelper()->renderOtherConfig('business_unit',$item),
			'tax_code'=>$this->_getHelper()->renderTaxCode($item),
			'sync_status'=>$status,
			'account_type'=>$item->getAccType(),
			'timestamp'=>$this->_getHelper()->formatDatetime(),
			'gross_total'=>$this->_getHelper()->renderGrossTotal($item),
		);
	}
} 