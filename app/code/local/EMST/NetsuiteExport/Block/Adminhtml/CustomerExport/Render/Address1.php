<?php
class EMST_NetsuiteExport_Block_Adminhtml_CustomerExport_Render_Address1 extends EMST_NetsuiteExport_Block_Adminhtml_CustomerExport_Render_Address
{
    public function render(Varien_Object $row)
    {
        $value = $row->getData($this->getColumn()->getIndex());
		if($value) {
			return $this->escapeHtml($this->getBillingStreet($value,1));
		}
		return $value;
    }
}