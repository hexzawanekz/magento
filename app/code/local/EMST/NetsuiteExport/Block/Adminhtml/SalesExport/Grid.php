<?php
/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade NetsuiteExport to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_NetsuiteExport
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
*/

class EMST_NetsuiteExport_Block_Adminhtml_SalesExport_Grid extends EMST_NetsuiteExport_Block_Adminhtml_Widget_Grid
{
	public function __construct()
	{
		parent::__construct();
		$this->setId('netsuiteexportGrid');
		$this->setDefaultSort('netsuiteexport_id');
		$this->setDefaultDir('ASC');
		 $this->setUseAjax(true);
		$this->setSaveParametersInSession(true);
	}
    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel('netsuiteexport/salesExport_collection')
			->addRequireFields()
			->addInvoiceFields()
			->addOrderItems()
			->addPaymentToSelect()
			->addStatusFilter() //
			->addAddressFields();
		
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

	protected function _prepareColumns()
	{
		 $this->addColumn('real_order_id', array(
            'header'=> Mage::helper('netsuiteexport')->__('Order #'),
            // 'width' => '80px',
            'type'  => 'text',
            'index' => 'increment_id',
        ));

        $this->addColumn('order_created_at', array(
            'header' => Mage::helper('netsuiteexport')->__('Purchased On'),
            'index' => 'order_created_at',
            'type' => 'datetime',
            //'width' => '100px',
        ));
		$itemFieldName = Mage::helper('netsuiteexport')->getItemFieldName();
        $this->addColumn($itemFieldName, array(
            'header' => Mage::helper('netsuiteexport')->__('Item Name'),
            'index' => $itemFieldName,			
			'filter' => false,
        ));
		$this->addColumn('billing_name', array(
            'header' => Mage::helper('netsuiteexport')->__('Bill to Name'),
            'index' => 'billing_name',			
			'filter' => false,
        ));
		$this->addColumn('payment_method', array(
            'header' => Mage::helper('netsuiteexport')->__('Payment Method'),
			'index' => 'payment_method',
            'type' => 'options',
			'options' => Mage::helper('netsuiteexport')->getStorePaymentMethods(),
			// 'filter' => false
        ));
		$this->addColumn('shipping_method', array(
            'header' => Mage::helper('netsuiteexport')->__('Shipping Method'),
			'index' => 'shipping_method',
			// 'renderer' => 'netsuiteexport/adminhtml_salesExport_render_shipping',
			'type' => 'options',
			'options' => Mage::helper('netsuiteexport')->getSalesShippingMethods()
        ));
        $this->addColumn('grand_total', array(
            'header' => Mage::helper('netsuiteexport')->__('G.T. (Purchased)'),
            'index' => 'grand_total',
            'type'  => 'currency',
            'currency' => 'order_currency_code',
        ));

        $this->addColumn('status', array(
            'header' => Mage::helper('netsuiteexport')->__('Status'),
            'index' => 'status',
			// 'filter' => false,
            'type'  => 'options',
            // 'width' => '70px',
            'options' => Mage::getSingleton('sales/order_config')->getStatuses(),
        ));
        
		
		$this->addExportType('*/*/exportCsv', Mage::helper('netsuiteexport')->__('CSV'));
		// $this->addExportType('*/*/exportXml', Mage::helper('netsuiteexport')->__('XML'));
	  
		return parent::_prepareColumns();
	}
	public function getRowUrl($row)
	{
		return null;
	}
	public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }
}