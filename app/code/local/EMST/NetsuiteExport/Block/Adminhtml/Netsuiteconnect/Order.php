<?php
class EMST_NetsuiteExport_Block_Adminhtml_Netsuiteconnect_Order extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct() {
		$this->_controller = 'adminhtml_netsuiteconnect_order';
		$this->_blockGroup = 'netsuiteexport';
		$this->_headerText = Mage::helper('netsuiteexport')->__('Acommerce Connect');
		parent::__construct();
		$this->_removeButton('add');
	}
	
}
