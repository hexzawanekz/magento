<?php

$installer = $this;
$installer->startSetup();
$installer->_conn->addColumn($this->getTable('sales_flat_order'), 'is_exported', 'INTEGER(1) DEFAULT 0');
$installer->_conn->addColumn($this->getTable('sales_flat_order_grid'), 'is_exported', 'INTEGER(1) DEFAULT 0');

$installer->run("
    UPDATE `{$installer->getTable('sales_flat_order')}` 
    SET is_exported = 1 
    WHERE increment_id IN (SELECT `order_increment_id` FROM `{$installer->getTable('netsuite_sales_order')}`)
");

$installer->run("
    UPDATE `{$installer->getTable('sales_flat_order_grid')}` 
    SET is_exported = 1 
    WHERE increment_id IN (SELECT `order_increment_id` FROM `{$installer->getTable('netsuite_sales_order')}`)
");

$installer->endSetup();