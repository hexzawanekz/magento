<?php

$installer = $this;
$installer->startSetup();
$setup = new Mage_Catalog_Model_Resource_Setup('core_setup');
$eId = $setup->getEntityTypeId('catalog_product');
$configFields = array(
				'warehouse_location' => 'Warehouse Location',
				'business_unit' => 'Bussiness Unit',
				'subsidiary' => 'Subsidiary',
				'tax_code' => 'Tax Code',
				'discount_tax_code' => 'Tax Code For Discount',
				'product_price' => 'Item Price Level(Products)',
				'shipping_amount' => 'Item Price Level(Shipping)',
				'cod_fee' => 'Price Level(COD)',
				'reward_points' => 'Price Level(Reward Points)',
				'discount_rules' => 'Price Level(Discount Rules)',
				'netsuite_status' => 'Customer Status',
				);
foreach($setup->getAllAttributeSetIds() as $setId) {
	if(!$setup->getAttributeGroup($eId,$setId,'Netsuite'))	{
		$setup->addAttributeGroup($eId,$setId,'Netsuite');
	}
	foreach($configFields as $config => $label) {
		if(!$setup->getAttribute($eId,$config)) {
			$setup->addAttribute($eId, $config, array(
			'label'        =>$label,    
			'type'      =>'varchar',
			'input'     =>'text',
			'visible'   =>1,
			'required'  =>false,
			'position'  =>1,
			'global'    =>false,
			'user_defined'    =>1
			));			
		}
		$setup->addAttributeToGroup($eId,$setId,'Netsuite',$config,1);
	}
}
$installer->endSetup();
