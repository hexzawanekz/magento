<?php

class EMST_Kbank_PaymentController extends Mage_Core_Controller_Front_Action
{
    /**
     * Order instance
     */
    protected $_order;

    /**
     *  Get order
     *
     *  @param    none
     *  @return	  Mage_Sales_Model_Order
     */
    public function getOrder()
    {
        if ($this->_order == null) {
            $session = Mage::getSingleton('checkout/session');
            $this->_order = Mage::getModel('sales/order');
            $this->_order->loadByIncrementId($session->getLastRealOrderId());
        }
        return $this->_order;
    }

    public function redirectAction()
    {
        $session = Mage::getSingleton('checkout/session');
        $order = $this->getOrder();
        if (!$order->getId() || ($order->getIncrementId()==$session->getRedirected()) || !Mage::getModel('kbank/payment')->validateRedirect($order)) {
			Mage::getSingleton('checkout/session')->clear();
            $this->norouteAction();
            return;
        }
        
        if (!in_array($order->getStatus(), array('canceled', 'closed', 'complete'))) {
            $order->addStatusHistoryComment(Mage::helper('kbank')->__('Customer was redirected to Kbank'))->save();
        }

        $this->getResponse()
                ->setBody($this->getLayout()
                        ->createBlock('kbank/redirect')
                        ->setOrder($order)
                        ->toHtml());
		$session->setRedirected($order->getIncrementId());
    }

    public function returnAction()
    {
        $salt = '';
        if ($this->getOrder()) $salt = $this->getOrder()->getId() . 'returned';

        // if (isset($_SESSION['kbank_return'])) {
            // if ($_SESSION['kbank_return'] == $salt) {
                // if (Mage::getSingleton('core/session')->getUsedAPI()) {
                    // $protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443)
                                // ? "https://" : "http://";
                    // $domainName = $_SERVER['HTTP_HOST'];
                    // $local = substr($_SERVER['REQUEST_URI'], 0, strpos($_SERVER['REQUEST_URI'], 'kbank'));
                    // Mage::app()->getFrontController()->getResponse()->setRedirect($protocol . $domainName . $local . 'mobileapi/billing/end');
                // } else {
                    // $this->_redirect('');
                // }
                // return;
            // }
        // }
        $_SESSION['kbank_return'] = $salt;
        $data = $this->getRequest()->getPost();
        if ($data) {
            if ($this->_validate($data)) {
                $order = $this->getOrder();
                if (!$order->getId()) {
                    $this->norouteAction();
                    return;
                }
                if (!in_array($order->getStatus(), array('canceled', 'closed', 'complete'))) {
                    $data = Zend_Json::encode($data);
                    $data = str_replace(',', ', ', $data);
                    $order->addStatusHistoryComment(Mage::helper('kbank')->__('Customer successfully returned from Kbank') . '<br/>' . $data)
					->save();
                    $order->sendNewOrderEmail();
                }
                $this->saveInvoice($order);
                $this->_redirect('kbank/payment/success');
            } else {
                $this->_redirect('kbank/payment/error');
            }
        } else {
            $this->_redirect('kbank/payment/error');
        }
    }

    protected function saveInvoice(Mage_Sales_Model_Order $order)
    {
        if ($order->canInvoice()) {
            $errors = '';
            $apiIvoice = Mage::getModel('sales/order_invoice_api');
            $orderIncrementId = $order->getIncrementId();
            try {
                $invoiceId = $apiIvoice->create($orderIncrementId, null, Mage::helper('sales')->__('Invoiced'), true,
                                                                                                   true);
                if ($invoiceId) {
                    $invoice = Mage::getModel('sales/order_invoice')->loadByIncrementId($invoiceId);
                    if ($invoice->canCapture()) $invoice->capture()->save();
                }
            } catch (Exception $e) {
                $errors = empty($errors) ? $orderIncrementId . ": " . $e->getMessage() : $errors . "<br/>" . $orderIncrementId . ": " . $e->getMessage();
                Mage::log('Fail to invoice:' . $errors . ' Bug by minhhaislvl@gmail.com');
            }
        }
        return false;
    }

    public function successAction()
    {
        $session = Mage::getSingleton('core/session');
        $session->setQuoteId($session->getKbankPaymentQuoteId());
        $session->unsKbankPaymentQuoteId();
       
        // if (Mage::getSingleton('core/session')->getUsedAPI()) {
            // $protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443) ? "https://"
                        // : "http://";
            // $domainName = $_SERVER['HTTP_HOST'];
            // $local = substr($_SERVER['REQUEST_URI'], 0, strpos($_SERVER['REQUEST_URI'], 'kbank'));
            // Mage::app()->getFrontController()->getResponse()->setRedirect($protocol . $domainName . $local . 'mobileapi/billing/end');
        // } else {
            // $this->_redirect('checkout/onepage/success');
        // }
		$this->_redirect('checkout/onepage/success');
    }

    public function errorAction()
    {
        $session = Mage::getSingleton('core/session');
        $errorMsg = Mage::helper('kbank')->__(' There was an error occurred during paying process.');

        $order = $this->getOrder();

        if (!$order->getId()) {
            $this->norouteAction();
            return;
        }

        if ($order instanceof Mage_Sales_Model_Order && $order->getId()) {            
            if (!in_array($order->getStatus(), array('canceled', 'closed', 'complete'))) {
                $order->addStatusHistoryComment(Mage::helper('kbank')->__('Customer returned from Kbank.') . $errorMsg);
                $order->save();
            }
        }
        
        $session->addError(Mage::helper('kbank')->__('Customer returned from Kbank.') . Mage::helper('kbank')->__(' There was an error occurred during paying process.'));
        Mage::getSingleton('checkout/session')->unsLastRealOrderId();
        // if (Mage::getSingleton('core/session')->getUsedAPI()) {
            // $protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443) ? "https://"
                        // : "http://";
            // $domainName = $_SERVER['HTTP_HOST'];
            // $local = substr($_SERVER['REQUEST_URI'], 0, strpos($_SERVER['REQUEST_URI'], 'kbank'));
            // Mage::app()->getFrontController()->getResponse()->setRedirect($protocol . $domainName . $local . 'mobileapi/billing/end');
        // } else {
            // $this->_redirect('');
        // }
		$this->_redirect('');
    }

    protected function _validate($data)
    {
        $lastOrder = $this->getOrder();

        if (!isset($data['HOSTRESP']) || ($data['HOSTRESP'] != '00')) {
            return false;
        }

        if (!isset($data['RETURNINV'])) {
            return false;
        } else {
            $orderId = ltrim($data['RETURNINV'], '0');
            $amount = (int) ltrim($data['AMOUNT'], '0');
            $order = Mage::getModel('sales/order')->loadByIncrementId($orderId);
            //$orderPayment = $order->getPayment()->getMethodInstance()->getCode();
            $orderAmount = $order->getBaseGrandTotal() * 100;
            $payment = Mage::getModel('kbank/payment');

            if ($lastOrder->getIncrementId() != $orderId) {
                return false;
            }

            if ($amount != $orderAmount) {
                return false;
            }

            if ($payment->isAuthenticationIp()) {
                $ip = $this->_getIpAddress();
                if (!in_array($ip, $payment->getIpAddress())) {
                    return false;
                }
            }
            /* if($orderPayment != $payment->getCode()) {
              return false;
              } */
        }
        return true;
    }

    protected function _getIpAddress()
    {
        if (isset($_SERVER['HTTP_REFERER'])) {
            $host = parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST);
            $ip = gethostbyname($host);
            return $ip;
        } else {
            return '127.0.0.1';
        }
    }
}
