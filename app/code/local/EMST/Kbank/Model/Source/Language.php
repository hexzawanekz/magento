<?php

class EMST_Kbank_Model_Source_Language
{
    public function toOptionArray()
    {
        return array(
            array('value' => 'EN', 'label' => Mage::helper('kbank')->__('English')),
            array('value' => 'FR', 'label' => Mage::helper('kbank')->__('French')),
            array('value' => 'DE', 'label' => Mage::helper('kbank')->__('German')),
            array('value' => 'IT', 'label' => Mage::helper('kbank')->__('Italian')),
            array('value' => 'ES', 'label' => Mage::helper('kbank')->__('Spain')),
            array('value' => 'NL', 'label' => Mage::helper('kbank')->__('Dutch')),
        );
    }
}



