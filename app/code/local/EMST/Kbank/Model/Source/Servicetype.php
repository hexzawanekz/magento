<?php

class EMST_Kbank_Model_Source_Servicetype
{
    public function toOptionArray()
    {
        return array(
            array('value' => 'trade_create_by_buyer', 'label' => Mage::helper('kbank')->__('Products')),
            array('value' => 'create_direct_pay_by_user', 'label' => Mage::helper('kbank')->__('Virtual Products')),
        );
    }
}



