<?php
/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Regions to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_Regions
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
*/
class EMST_Regions_Model_Resource_Region extends Mage_Directory_Model_Resource_Region
{
	protected $_locale = null;
    protected function _getLoadSelect($field, $value, $object)
    {
        $select  = Mage_Core_Model_Resource_Db_Abstract::_getLoadSelect($field, $value, $object);
        $adapter = $this->_getReadAdapter();
        $locale       = ($this->getLocale())?$this->getLocale():Mage::app()->getDistroLocaleCode();;

        $regionField = $adapter->quoteIdentifier($this->getMainTable() . '.' . $this->getIdFieldName());

        $condition = $adapter->quoteInto('lrn.locale = ?', $locale);
        $select->joinLeft(
            array('lrn' => $this->_regionNameTable),
            "{$regionField} = lrn.region_id AND {$condition}",
            array());

		$select->columns(array('name'), 'lrn');
		
        return $select;
    }
	public function setLocale($locale) {
		$this->_locale = $locale;
		return $this;
	}
	public function getLocale() {
		return $this->_locale;
	}
}
