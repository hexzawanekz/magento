<?php
/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Regions to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_Regions
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
*/

class EMST_Regions_Block_Adminhtml_Regions_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct()
	{
      parent::__construct();
      $this->setId('regionsGrid');
      // $this->setDefaultSort('region_id');
      // $this->setDefaultDir('ASC');
      $this->setDefaultFilter(array('country_id'=>Mage::helper('core')->getDefaultCountry()));
      $this->setSaveParametersInSession(true);
	}

	protected function _prepareCollection()
	{
		$collection = Mage::getModel('directory/region')->getCollection();
		if($this->getParam('store')) {
			$locale = Mage::helper('emst_regions')->getStoreLocale($this->getParam('store'));
			$collection->addBindParam(':region_locale', $locale);			
		}		
		$this->setCollection($collection);		
		return parent::_prepareCollection();
	}

	protected function _setCollectionOrder($column)
    {
        $collection = $this->getCollection();
        if ($collection) {
            $columnIndex = $column->getFilterIndex() ?
                $column->getFilterIndex() : $column->getIndex();
            $collection->unshiftOrder($columnIndex, strtoupper($column->getDir()));
        }
        return $this;
    }
	
	protected function _prepareColumns()
	{
		$this->addColumn('region_id', array(
            'header'        => Mage::helper('emst_regions')->__('ID'),
            'align'         => 'left',
            'width'         => '20px',
            'index'         => 'region_id',
            'filter_index'  => 'main_table.region_id',
        ));
        
        $this->addColumn('country_id', array(
            'header'  => Mage::helper('emst_regions')->__('Country'),
            'align'   => 'left',
            'index'   => 'country_id',
            'type'    => 'options',
            'options' => Mage::helper('emst_regions')->getCountiesOptions(),
        ));
        $this->addColumn('name', array(
            'header' => Mage::helper('emst_regions')->__('Name'),
            'align'  => 'left',
            'index'  => 'name',
        ));
		return parent::_prepareColumns();
	}

	protected function _prepareMassaction()
    {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('region');
        
        $this->getMassactionBlock()->addItem('delete', array(
            'label'   => Mage::helper('emst_regions')->__('Delete'),
            'url'     => $this->getUrl('*/*/massDelete'),
            'confirm' => Mage::helper('emst_regions')->__('Are you sure?')
        ));
        parent::_prepareMassaction();
    }

	public function getRowUrl($row)
	{
		$locale = Mage::helper('emst_regions')->getStoreLocale($this->getParam('store'));
		return $this->getUrl('*/*/edit', array('id' => $row->getId(),'locale'=>$locale,'store'=>$this->getParam('store')));
	}
	
}