<?php


/**
 * BBGateway payment model
 *
 */
class EMST_BBGateway_Model_Bbgateway extends Mage_Payment_Model_Method_Abstract
{
    protected 	$_code  = 'bbgateway';
    protected 	$_formBlockType = 'bbgateway/form';
	protected 	$_infoBlockType = 'bbgateway/info';
	
    protected 	$_canAuthorize            	= true;
    protected 	$_isGateway               	= true;
	protected   $_canCapture              	= true;
	protected 	$_canRefund                 = true;
	protected 	$_canVoid                   = true;
    protected 	$_canUseForMultishipping  	= true;
	protected 	$_canUseInternal            = false;
	
	protected 	$_xml = null;
	protected 	$_root = null;
	
	CONST 	TYPE_SALES = 'N';
	CONST 	TYPE_AUTH = 'H';
	
	public function validateRedirect($order) {
		$method = $order->getPayment()->getMethodInstance();
		if($method->getCode() == $this->getCode()) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public function isAvailable($quote=null)
	{
		$active = parent::isAvailable($quote);
      if($quote && $active) {			 
			if($this->getConfigData('dev')) {
				$allowed_emails = explode(',',$this->getConfigData('email'));
				$customer_email=Mage::getSingleton('customer/session')->getCustomer()->getEmail();
				if(in_array($customer_email,$allowed_emails)) {
					return true;
			    }
				else {
					return false;
				}
			}
			else {
				return true;
			}
      }
      return $active;
	}
	
	public function useApi() {
		return $this->getConfigData('use_api');
	}
	
	public function getApiUrl() {
		return $this->getConfigData('api');
	}
	public function getApiUsername() {
		return $this->getConfigData('api_username');
	}
	public function getApiPassword() {
		return $this->getConfigData('api_password');
	}
	public function getAllowReversal() {
		return $this->getConfigData('allow_reversal');
	}
    public function getStatus() 
	{
		return $this->getConfigData('order_status');
	}
	public function getCustomText()
	{
        $customtext = $this->getConfigData('customtext');
        return $customtext;
    }
	public function getGatewayUrl() 
	{
		return $this->getConfigData('gateway');
	}
	
	public function getMerchantId() 
	{
		return $this->getConfigData('merchant_id');
	}	
	public function getCancelUrl() {
		return Mage::getUrl('bbgateway/payment/cancel',array('_secure' => true));
	}
	public function getFailUrl() {
		return Mage::getUrl('bbgateway/payment/fail',array('_secure' => true));
	}
	public function getSuccessUrl() {
		return Mage::getUrl('bbgateway/payment/success',array('_secure' => true));
	}
	protected function getCurrentOrder() 
	{
		$order = $this->getOrder();
        if (!($order instanceof Mage_Sales_Model_Order) || !$order->getId()) {
            Mage::throwException($this->_getHelper()->__('Cannot retrieve order object'));
		}
		return $order;
	}
	public function getOrderPlaceRedirectUrl()
    {
        return Mage::getUrl('bbgateway/payment/redirect');
    }
	public function getLanguage() {
		return $this->getConfigData('lang');
	}
	public function getCurrencyCode() {
		return $this->getConfigData('currency_code');
	}
	public function getPayType() {
		return $this->getConfigData('pay_type');
	}
	public function getPayMethod() {
		return $this->getConfigData('pay_method');
	}
	public function getRemark() {
		return $this->getConfigData('remark');
	}
	public function getRedirectSec() {
		return $this->getConfigData('redirect_sec');
	}
	public function getOrderRef($num) {
		if($num) {
			return $this->getConfigData('userDefined'.$num);
		}
		return null;
	}
	
	public function getStandardCheckoutFormFields() {	
		$order = $this->getCurrentOrder();
        return array(
					   'orderRef'  	=> $order->getIncrementId(),
					   'amount'  	=> Mage::helper('bbgateway')->formatAmount($order->getBaseGrandTotal()),
					   'currCode'  	=> $this->getCurrencyCode(),
					   'lang'  		=> $this->getLanguage(),
					   'cancelUrl'  => $this->getCancelUrl(),
					   'failUrl'  	=> $this->getFailUrl(),
					   'successUrl' => $this->getSuccessUrl(),
					   'merchantId' => $this->getMerchantId(),
					   'payType'  	=> $this->getPayType(),
					   'payMethod'  => $this->getPayMethod(),
					   'remark'  	=> $this->getRemark(),
					   'redirect'  	=> $this->getRedirectSec(),
					   'orderRef1'  => $this->getOrderRef(1),
					   'orderRef2'  => $this->getOrderRef(2),
					   'orderRef3'  => $this->getOrderRef(3),
					   'orderRef4'  => $this->getOrderRef(4),
					   'orderRef5'  => $this->getOrderRef(5),
					);
	}	
	protected function getXml() {
		if(!$this->_xml) {
			$this->_xml = new DOMDocument('1.0','ISO-8859-1');
			$this->_xml->formatOutput = true;
		}
		return $this->_xml;
	}
	protected function getRoot($root) {
		if(!$this->_root) {
			$xml = $this->getXml();
			$this->_root = $xml->createElement($root);
			$xml->appendChild($this->_root);
		}
		return $this->_root;
	}
	public function getSimpleXml(array $array,$root = 'Request',$child = '') {
		$xml = $this->getXml(); 		
		if($child) {
			$root = $child;
		}
		else {
			$root = $this->getRoot($root);
		}
        foreach($array as $k=>$v) {
			if(is_numeric($k) && is_array($v)) {
				foreach($v as $sub_el=>$att) {
					$sub_el=$xml->createElement($sub_el);
					if(is_array($att)) {
						foreach($att as $at1=>$av1) {
							$at1 = $xml->createAttribute($at1);
							$at1->value = $av1;
							$sub_el->appendChild($at1);
						}
					}
					else {
						throw new Mage_Payment_Model_Info_Exception($this->_getHelper()->__('Invalid element attribute'));
					}
					$root->appendChild($sub_el);
				}
			}
			elseif(!is_numeric($k)) {
				$k=$xml->createElement($k);
				$root->appendChild($k);
			}			
			else {
				throw new Mage_Payment_Model_Info_Exception($this->_getHelper()->__('Invalid array format'));
			}
			if(!is_array($v)) {	
				if($v) {
					$k->appendChild($xml->createTextNode($v));
				}				
			}
            elseif(!is_numeric($k)) {
				
				$this->getSimpleXml($v,'',$k);
				
			}
        }
		
		if($child) {
			return;
		}
		
        $xml = $this->removeFirst($xml->saveXML());
		
		return $xml;
    }
	protected function removeFirst($xml) {
		$return ='';
		$lines = explode("\n", $xml, 2);
		if(!preg_match('/^\<\?xml/', $lines[0])) {
			$return = $lines[0];
		}
		$return .= $lines[1];
		return $return;
	}
	protected function importPaymentInfo() {
		$payment = $this->getOrder()->getPayment();
		$was = $payment->getAdditionalInformation();
		$new = array_merge($this->getSuccessInfo($parsed),$was);
		$payment->setAdditionalInformation($new);
		return $payment;
	}
	public function saveInvoice()
    {	
		 try {
			$order = $this->getOrder();	
			$payment = $this->logInfo();
			if ($order->canInvoice()) {
				$payment->registerCaptureNotification($order->getBaseGrandTotal());
				$order->save();
			}
			$invoice = $payment->getCreatedInvoice();
			if ($invoice && !$order->getEmailSent()) {				
				
				$order->sendNewOrderEmail()->addStatusHistoryComment(
						Mage::helper('bbgateway')->__('Notified customer about invoice #%s.', $invoice->getIncrementId())
					)
					->setIsCustomerNotified(true)
					->save();
			}
			return true;
		}
		catch(Exception $e) {
			Mage::log($e->getMessage(),null, 'BBGateway.log');
		}
        return false;
    }
	public function logInfo() {
		$info = $this->getInfo();
		$payment = $this->getOrder()->getPayment()->setTransactionId($info['PayRef']);
		$was = $payment->getAdditionalInformation();
		$new = array_merge($info,$was);
		$payment->setAdditionalInformation($new);
		return $payment;
	}
	public function canVoid(Varien_Object $payment)
    {
        return $this->getConfigData('allow_void');
    }
	public function canCapture()
    {
        return $this->getConfigData('allow_capture');
    }
	public function canReversal()
    {
        return $this->getConfigData('allow_reversal');
    }
	public function capture(Varien_Object $payment, $amount)
    {
        if (!$this->canCapture() || !$this->useApi()) {
           return false;
        }
		$result = $this->setOrder($payment->getOrder())
					->setInfo($payment->getAdditionalInformation())
					->_prepare()
					->capture();
		$info = array_merge($payment->getAdditionalInformation(),$result->getResponse());
		if(!isset($info['PayRef'])) {
			Mage::throwException(Mage::helper('bbgateway')->__('Could not capture without transaction Id.'));
		}
		$payment->setTransactionId($info['PayRef'])->setAdditionalInformation($info);
		if(!$result->getValidatedStatus()) {
			// $payment->save();
			Mage::throwException(Mage::helper('bbgateway')->__('Wrong amount captured on iPay gateway.'));
		}
		else {
			Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('bbgateway')->__('The order was captured successfully.'));
		}
        return $this;
    }
	public function void(Varien_Object $payment)
    {
		if(!$this->canVoid() || !$this->useApi()) {
			return $this;
		}
		$api = $this->setOrder($payment->getOrder())
					->setInfo($payment->getAdditionalInformation())
					->_prepare();
		$info = $payment->getAdditionalInformation();
		if(isset($info['payment_type']) && isset($info['PayRef'])) {			
			$type = $info['payment_type'];
			if($type==self::TYPE_SALES) {
				$result = $api->reversal();
			}
			else {
				$result = $api->void();
			}
			if(!$result->getValidatedStatus()) {
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('bbgateway')->__('Can\'t void the transaction on iPay gateway.'));
			}
			else {
				Mage::getSingleton('adminhtml/session')->addError(Mage::helper('bbgateway')->__('Successfully void the transaction on iPay gateway.'));
			}
			$info = array_merge($payment->getAdditionalInformation(),$result->getResponse());
			$payment->setTransactionId($info['PayRef'])->setAdditionalInformation($info);
		}
		else {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('bbgateway')->__('No transaction for this order to void.'));
		}
        return $this;
    }
	public function query() {
		return $this->_prepare()
					->query();
	}
	protected function _prepare() {
		$info = $this->getInfo();
		if(!$info || !isset($info['PayRef'])) {
			Mage::throwException(Mage::helper('bbgateway')->__('Can\'t request to iPay\'s API with empty data or transaction Id.'));
		}
		$api = Mage::getModel('bbgateway/api')
						->setOrder($this->getOrder())
						->setPayModel($this)
						->setPayRef($info['PayRef']);
		return $api;
	}
	
	public function debugData($debugData,$action=null)
    {
        if ($this->getDebugFlag()) {
            Mage::getModel('core/log_adapter', 'payment_' . $this->getCode(). '_'. $action . '.log')
               ->setFilterDataKeys($this->_debugReplacePrivateDataKeys)
               ->log($debugData);
        }
    }
	public function assignData($data)
    {
		$additional_inf['additional_information'] = array('payment_type'=>$this->getPayType());
		return parent::assignData($additional_inf);        
    }

}
