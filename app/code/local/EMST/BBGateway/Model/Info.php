<?php


/**
 * BBGateway payment model
 *
 */
class EMST_BBGateway_Model_Info
{
	public function filterDataForDisplaying($data) {
		$display = array();		
		$display['Bank Reference'] = isset($data['Ord'])?$data['Ord']:'';
		$display['Holder'] = isset($data['Holder'])?$data['Holder']:'';
		$display['Transaction ID'] = isset($data['PayRef'])?$data['PayRef']:'';
		$display['Payer\' IP address'] = isset($data['sourceIp'])?$data['sourceIp']:'';
		$display['Payment Type'] = isset($data['payment_type'])?Mage::getModel('bbgateway/source_paymentTypes')->getPaymenTypeLable($data['payment_type']):'';
		return $display;
	}
}
