<?php 
class EMST_BBGateway_Model_Source_Attribute extends Mage_Eav_Model_Entity_Attribute_Backend_Abstract
{
	public function afterLoad($object) {
		$this->processJson($object,'decode');
	}
    public function beforeSave($object) {
		$this->processJson($object,'encode');
	}
	protected function processJson($object,$type) {
		$attributeName = $this->getAttribute()->getName();
        if ($object->hasData($attributeName)) {
            try {
                $value = Zend_Json::$type($object->getData($attributeName));
            } catch (Exception $e) {
                throw new Exception("Cannot process Stored_cc json");
            }			
            $object->setData($attributeName, $value);
        }
	}
}
  