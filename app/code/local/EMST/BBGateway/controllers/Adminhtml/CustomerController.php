<?php
require_once 'Mage/Adminhtml/controllers/CustomerController.php';
class EMST_BBGateway_Adminhtml_CustomerController extends Mage_Adminhtml_CustomerController
{
	public function CardBBLAction() {
		$this->_initCustomer();
		$this->getResponse()->setBody($this->getLayout()
            ->createBlock('bbgateway/adminhtml_customer_edit_tabs_card_info')->toHtml());
	}
}