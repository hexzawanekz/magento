<?php
/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade TableRate to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     emst_reports
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
*/

$installer = $this;

$installer->startSetup();	
	$table = $installer->getConnection()
    ->newTable($installer->getTable('emst_reports/viewed_product'))
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        ), 'Primary key')
    ->addColumn('product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 11, array(
        'nullable'  => false,
        ), 'Product Id')
    ->addColumn('customer_id', Varien_Db_Ddl_Table::TYPE_TEXT, 11, array(
        'nullable'  => false,
        ), 'Customer Id')
    ->addColumn('added_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
        'nullable'  => false,
        ), 'Date added')
    ->addColumn('updated_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
        'nullable'  => false,
        ), 'Updated At')
    ->addColumn('remote_ip', Varien_Db_Ddl_Table::TYPE_VARCHAR, 30, array(
        'nullable'  => false,
        ), 'Customer Ip Address')
	->addColumn('view_nums', Varien_Db_Ddl_Table::TYPE_INTEGER, 11, array(
        'nullable'  => false,
        'default'  => 0,
        ), 'Number of views per customer')
    ->addIndex($installer->getIdxName('emst_reports/viewed_product', array('product_id', 'customer_id'), Varien_Db_Adapter_Interface::INDEX_TYPE_INDEX),
        array('product_id', 'customer_id'), array('type' => Varien_Db_Adapter_Interface::INDEX_TYPE_INDEX))
    ->setComment('emst_reports Viewed products');
$installer->getConnection()->createTable($table);
$installer->endSetup();