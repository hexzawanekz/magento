<?php

$installer = $this;
$installer->startSetup();
$installer->getConnection()
	->addColumn($installer->getTable('emst_reports/viewed_product'), 'store_id', 
			array(
				'TYPE'      => Varien_Db_Ddl_Table::TYPE_SMALLINT,
				'LENGTH'    => 2,
				'NULLABLE'  => true,
				'COMMENT'   => 'store_id'
			));
$installer->getConnection()->addIndex(
	$installer->getTable('emst_reports/viewed_product'),
	$installer->getIdxName('emst_reports/viewed_product', array('store_id')),
	array('store_id')
);
$installer->endSetup();
