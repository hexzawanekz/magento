<?php
/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Reports to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_Reports
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
*/

class EMST_Reports_Model_Resource_Quote_Collection extends Mage_Reports_Model_Resource_Quote_Collection
{
    public function prepareForAbandonedReport($storeIds, $filter = null)
    {
        $this
			->addRequiredFields()
			->addFieldToFilter('items_count', array('neq' => '0'))
            ->addFieldToFilter('main_table.is_active', '1')
            // ->setOrder('updated_at')
			;
        if (is_array($storeIds) && !empty($storeIds)) {
            $this->addFieldToFilter('store_id', array('in' => $storeIds));
        }

        return $this;
    }
	public function addRequiredFields() {
		$this->addFilterToMap('customer_id','main_table.customer_id');
		$this->addFilterToMap('customer_email','main_table.customer_email');
		$this->addFilterToMap('created_at','main_table.created_at');
		$this->addFilterToMap('send_email','main_table.send_email');
		$this->getSelect()
			->reset(Zend_Db_Select::COLUMNS)
			->columns('customer_id', 'main_table')
			->columns('customer_email', 'main_table')
			->columns('created_at', 'main_table')
			->columns('updated_at', 'main_table')
			// ->columns('send_email', 'main_table')
			;
		
		return $this;
	}
	public function addProductItems() {
		$this
			->addFilterToMap('qty', 'quote_items.qty')
			->addFilterToMap('product_id', 'quote_items.product_id');
		$this->getSelect()->joinInner(
                array('quote_items' => $this->getTable('sales/quote_item')),
                'quote_items.quote_id = main_table.entity_id',
                array('qty'=>'quote_items.qty','product_id'=>'quote_items.product_id'));
		Mage::getResourceHelper('core')->prepareColumnsList($this->getSelect());
		return $this;
	}
	 /**
     * Add ExpiredTime
     *
     * @param integer $time - hour     
     * @return EMST_Reports_Model_Resource_Quote_Collection
     */
	public function addExpiredTime($time=24) {
		$this->getSelect()->where('main_table.created_at <= DATE_SUB(?, INTERVAL '.intval($time).' HOUR)',Varien_Date::now());
		return $this;
	}
}