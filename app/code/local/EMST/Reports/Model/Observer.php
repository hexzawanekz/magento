<?php
/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Sales to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_Reports
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
*/

class EMST_Reports_Model_Observer
{
	public function productViewSimple($observer) {		
		try{
			$productId = $observer->getEvent()->getProductId();
			$customerId = $observer->getEvent()->getCustomerId();			
			if(!$productId) {
				return;
			}	
			$ip = Mage::helper('core/http')->getRemoteAddr();
			$coll = Mage::getModel('emst_reports/reports')->getCollection()
					->addFieldToFilter('updated_at',array('gt'=>Varien_Date::now(true)))
					->addFieldToFilter('store_id',Mage::app()->getStore()->getId())
					->addFieldToFilter('product_id',$productId);
			if($customerId) {				
				$coll->addFieldToFilter('customer_id',$customerId);
				
			}
			else {
				$coll ->addFieldToFilter('remote_ip',$ip);
			}
			$update = $coll->getFirstItem();
			if($update->getId()) {
				$update->setViewNums($update->getViewNums()+1)							
					   ->save();
			}
			else{
				Mage::getModel('emst_reports/reports')
					->setProductId($productId)
					->setCustomerId($customerId)
					->setAddedAt(Varien_Date::now())					
					->setRemoteIp($ip)
					->setViewNums(1)
					->save();
			}
		}
		catch(Exception $e) {
			Mage::logException($e);
		}
		return;
	}
} 