<?php

/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade SalesExport to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_SalesExport
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
 */
class EMST_Sales_Model_Resource_Channel_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    public function _construct() {
        parent::_construct();
        $this->_init('emst_sales/channel');
    }
	protected function _toOptionArray($valueField='', $labelField='name', $additional=array()) {
        if(!$valueField) {
			$valueField = 'channel_id';
		}
		return parent::_toOptionArray($valueField, $labelField='name', $additional=array());
    }
	public function toOptionArray() {
		$options = parent::toOptionArray();
		array_unshift($options,array('value'=>'','label'=>'Please Select'));
		return $options;
	}
	/* protected function _initSelect() {
        parent::_initSelect();
		// $this->addStoreFilter();
        return $this;
    } */
	public function addStoreFilter($store=0) {
		$this->addFieldToFilter('store_id',$store);
		return $this;
	}
	public function addTypeFilter($type) {
		$this->addFieldToFilter('type',$type);
		return $this;
	}
	public function addSubChannelFilter() {
		$this->addFieldToFilter('type',EMST_Sales_Model_Source_Types::MAIN_CHANNEL);
		return $this;
	}
	public function addMainChannelFilter() {
		$this->addFieldToFilter('type',EMST_Sales_Model_Source_Types::SUB_CHANNEL);
		return $this;
	}
	public function addChannelSourceFilter() {
		$this->addFieldToFilter('type',EMST_Sales_Model_Source_Types::CHANNEL_SOURCE);
		return $this;
	}
}