<?php

/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade SalesExport to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_SalesExport
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
 */
class EMST_Sales_Model_Resource_Order_Grid_Collection extends Mage_Sales_Model_Resource_Order_Grid_Collection
{
    protected $_instantMethods = array(
        Mage_Paypal_Model_Config::METHOD_WPS,
        'kbank_payment',
        'bbgateway',
        'cc2p'
    );

    protected $_codMethods = array(
        'cashondelivery',
        'phoenix_cashondelivery'
    );

    protected function _construct()
    {
        parent::_construct();
    }

    /**
     * join sales order collection to payment
     *
     * @return order collection
     */
    public function addPaymentToSelect()
    {
        $this->addFilterToMap('payment_method', 'payment.method');
        $this->join(
                array('payment' => 'sales/order_payment'), 'main_table.entity_id = payment.parent_id',
                array('payment_method' => 'payment.method')
        );
        return $this;
    }

    public function addOrderToSelect()
    {
        $this->addFilterToMap('created_at', 'main_table.created_at');
        $this->addFilterToMap('store_id', 'main_table.store_id');
        $this->addFilterToMap('increment_id', 'main_table.increment_id');
        $this->join(
                array('order' => 'order'), 'order.increment_id = main_table.increment_id',
                array('customer_email' => 'order.customer_email', 'is_wholesale' => 'order.is_wholesale','market_place'=> 'order.market_place','old_order_id' => 'order.old_order_id')
            );
        return $this;
    }

    protected function getInstantMethods()
    {
        return "'" . implode("','", $this->_instantMethods) . "'";
    }
	public function setInstantMethods($method) {
		if(!$method || !is_array($method) || empty($method)) {
			return $this;
		}
		$this->_instantMethods = $method;

		return $this;
	}

    protected function getCodMethods()
    {
        return "'" . implode("','", $this->_codMethods) . "'";
    }

    public function addTimeFilter($instant=0, $cod=0)
    {
        $methods = $this->getInstantMethods();
        $codMethods = $this->getCodMethods();
        $where = '';
        if($instant) {
            $where.= '(main_table.created_at <= DATE_SUB(NOW(),INTERVAL ' . $instant . ' HOUR) and payment.method in (' . $methods . '))';
        }
        if($cod) {
            $where.= ($instant)?' or ':'';
            $where.='(main_table.created_at <= DATE_SUB(NOW(),INTERVAL ' . $cod . ' HOUR) and payment.method in (' . $codMethods . '))';
        }
        if($where) {
            $this->getSelect()->where($where);
        }
        return $this;
    }

    public function addPendingOrders()
    {
        $this->addFieldToFilter('main_table.status', array('in' => array('pending', 'pending_payment')));
        return $this;
    }
}
