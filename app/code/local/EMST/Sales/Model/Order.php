<?php
/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Sales to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_Sales
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
*/

class EMST_Sales_Model_Order extends Mage_Sales_Model_Order
{
    const XML_PATH_INVALID_EMAIL_ENABLED        = 'invalid_email/general/active';
    const XML_PATH_INVALID_EMAIL_TEMPLATE       = 'invalid_email/email/template';
    const XML_PATH_INVALID_EMAIL_SENDER_NAME    = 'invalid_email/email/sendername';
    const XML_PATH_INVALID_EMAIL_SENDER_EMAIL   = 'invalid_email/email/senderemail';

    const XML_PATH_WHOLESALE_EMAIL_TEMPLATE       = 'sales_email/wholesale/template';
    const XML_PATH_WHOLESALE_EMAIL_GUEST_TEMPLATE = 'sales_email/wholesale/guest_template';
    const XML_PATH_WHOLESALE_EMAIL_IDENTITY       = 'sales_email/wholesale/identity';
    const XML_PATH_WHOLESALE_EMAIL_COPY_TO        = 'sales_email/wholesale/copy_to';
    const XML_PATH_WHOLESALE_EMAIL_COPY_METHOD    = 'sales_email/wholesale/copy_method';

    const XML_PATH_WHOLESALE_PICKUP_EMAIL_TEMPLATE  = 'sales_email/wholesale/pickup_template';
    const XML_PATH_WHOLESALE_PICKUP_COPY_TO         = 'sales_email/wholesale/pickup_order_copy_to';
    const XML_PATH_WHOLESALE_PICKUP_COPY_METHOD     = 'sales_email/wholesale/pickup_copy_method';

	const STATE_SHIPPED             = 'shipped';
	const STATE_INVOICED            = 'invoiced';
	const STATE_INVALID            	= 'invalid';
	protected $_unchangeState = array(
		self:: STATE_COMPLETE,
		self:: STATE_CLOSED,
		self:: STATE_CANCELED,
		self:: STATE_HOLDED,
		self:: STATE_PAYMENT_REVIEW,
	);
	/**
     * ....
     */
    protected function _checkState()
    {
        if (!$this->getId()) {
            return $this;
        }
		// check and add 2 new states
		$oldState = $this->getState();
        $userNotification = $this->hasCustomerNoteNotify() ? $this->getCustomerNoteNotify() : null;
		if(!$this->canShip() && $this->hasShipments() && $oldState !== self::STATE_SHIPPED 
		&& $this->_validateState($oldState)) {
			$this->_setState(self::STATE_SHIPPED, true, '', $userNotification);
		}
		if(!$this->canInvoice() && $this->hasInvoices() && $oldState !== self::STATE_INVOICED 
		&& $this->_validateState($oldState)) {
			$this->_setState(self::STATE_INVOICED, true, '', $userNotification);
		}
        if (!$this->isCanceled()
            && !$this->canUnhold()
            && !$this->canInvoice()
            && !$this->canShip()) {
            if (0 == $this->getBaseGrandTotal() || $this->canCreditmemo()) {
                if ($this->getState() !== self::STATE_COMPLETE) {
                    $this->_setState(self::STATE_COMPLETE, true, '', $userNotification);
                }
            }
            /**
             * Order can be closed just in case when we have refunded amount.
             * In case of "0" grand total order checking ForcedCanCreditmemo flag
             */
            elseif (floatval($this->getTotalRefunded()) || (!$this->getTotalRefunded()
                && $this->hasForcedCanCreditmemo())
            ) {
                if ($this->getState() !== self::STATE_CLOSED) {
                    $this->_setState(self::STATE_CLOSED, true, '', $userNotification);
                }
            }
        }

        if ($this->getState() == self::STATE_NEW && $this->getIsInProcess()) {
            $this->setState(self::STATE_PROCESSING, true, '', $userNotification);
        }
        return $this;
    }

	protected function _validateState($state) {
		if(!in_array($state,$this->_unchangeState)) {
				return true;
		}
		return false;
	}

    /**
     * Send email with order data
     *
     * @return Mage_Sales_Model_Order
     */
    public function sendNewOrderEmail($isWholesaleOrder = false)
    {
        $storeId = $this->getStore()->getId();

        if (!Mage::helper('sales')->canSendNewOrderEmail($storeId)) {
            return $this;
        }
        // Get the destination email addresses to send copies to
        $copyTo = $this->_getEmails(self::XML_PATH_EMAIL_COPY_TO);
        $copyMethod = Mage::getStoreConfig(self::XML_PATH_EMAIL_COPY_METHOD, $storeId);

        // Start store emulation process
        $appEmulation = Mage::getSingleton('core/app_emulation');
        $initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation($storeId);

        try {
            // Retrieve specified view block from appropriate design package (depends on emulated store)
            $paymentBlock = Mage::helper('payment')->getInfoBlock($this->getPayment())
                ->setIsSecureMode(true);
            $paymentBlock->getMethod()->setStore($storeId);
            $paymentBlockHtml = $paymentBlock->toHtml();
        } catch (Exception $exception) {
            // Stop store emulation process
            $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);
            throw $exception;
        }

        // Stop store emulation process
        $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);

        // Retrieve corresponding email template id and customer name
        if ($this->getCustomerIsGuest()) {
            $templateId = Mage::getStoreConfig(self::XML_PATH_EMAIL_GUEST_TEMPLATE, $storeId);
            if ($isWholesaleOrder) {
                $templateId = Mage::getStoreConfig(self::XML_PATH_WHOLESALE_EMAIL_GUEST_TEMPLATE, $storeId);
            }
            $customerName = $this->getBillingAddress()->getName();
        } else {
            $templateId = Mage::getStoreConfig(self::XML_PATH_EMAIL_TEMPLATE, $storeId);
            if ($isWholesaleOrder) {
                $templateId = Mage::getStoreConfig(self::XML_PATH_WHOLESALE_EMAIL_TEMPLATE, $storeId);
            }
            $customerName = $this->getCustomerName();
        }

        $mailer = Mage::getModel('core/email_template_mailer');
        $emailInfo = Mage::getModel('core/email_info');
        $emailInfo->addTo($this->getCustomerEmail(), $customerName);
        if ($copyTo && $copyMethod == 'bcc') {
            // Add bcc to customer email
            foreach ($copyTo as $email) {
                $emailInfo->addBcc($email);
            }
        }
        $mailer->addEmailInfo($emailInfo);

        // Email copies are sent as separated emails if their copy method is 'copy'
        if ($copyTo && $copyMethod == 'copy') {
            foreach ($copyTo as $email) {
                $emailInfo = Mage::getModel('core/email_info');
                $emailInfo->addTo($email);
                $mailer->addEmailInfo($emailInfo);
            }
        }

        // Delivery days
        $items = $this->getAllVisibleItems();
        $htmlDeliveryEn = 'We deliver in Bangkok in 1-3 working days.<br/><br/>We deliver in order regions of Thailand in 3-5 working days.';
        $htmlDeliveryTh = 'ทางเราจะจัดส่งสินค้าให้ถึงคุณภายใน 1-3 วันทำการสำหรับกรุงเทพฯและภายใน 3-5 <br/><br/>วันทำการสำหรับต่างจังหวัดทุกจังหวัดทั่วประเทศ';
        // Shipping Description
        $shippingDescription = $this->getShippingDescription();
        // Product ids array: $pIds
        $pIds = array();
        foreach($items as $item){
            /** @var Mage_Sales_Model_Order_Item $item */
            $pIds[] = $item->getProductId();
        }
        if(count($pIds) > 0){
            $flagDropship = $flagSpecial = false;
            $collection = Mage::getResourceModel('catalog/product_collection')
                ->addFieldToFilter('entity_id', $pIds)
                ->joinField('qty',
                    'cataloginventory/stock_item',
                    'qty',
                    'product_id=entity_id',
                    '{{table}}.stock_id=1',
                    'left')
                ->addAttributeToSelect('*');
            $max = 0;
            foreach ($collection as $product) {
                /** @var Mage_Catalog_Model_Product $product */
                if($product->getData('override_inventory') == 0 || $product->getStockItem()->getIsInStock() == 0) {
                    if ($product->getAttributeText('shipping_duration') == 'Dropship Product') {
                        $temp = 14;
                        if ($max < $temp) $max = $temp;
                    }
                    if ($product->getAttributeText('shipping_duration') == 'Special Order Product') {
                        if($product->getStockItem()->getManageStock() == 1){
                            if($product->getStockItem()->getIsInStock() != 0){
                                $temp = 10;
                            }
                        }else{
                            if($product->getStockItem()->getIsInStock() == 0){
                                $temp = 10;
                            }else{
                                $temp = 4;
                            }
                        }

                        if ($max < $temp) $max = $temp;
                    }
                    if ($product->getAttributeText('shipping_duration') == 'Consignment and Buy-in Product' &&  $product->getStockItem()->getIsInStock() == 1) {
                        $temp = 4;
                        if ($max < $temp) $max = $temp;
                    }
                }else{
                    $temp = 10;
                    if ($max < $temp) $max = $temp;
                }
            }
            if($max == 14){
                $htmlDeliveryEn = 'We deliver in 7-14 working days.';
                $htmlDeliveryTh = 'จัดส่งภายใน 7-14 วัน';
                $shippingDescription = str_replace('2-3','7-14', $shippingDescription);
            } else if($max == 10){
                $htmlDeliveryEn = 'We deliver in 7-10 working days.';
                $htmlDeliveryTh = 'จัดส่งภายใน 7-10 วัน';
                $shippingDescription = str_replace('2-3','7-10', $shippingDescription);
            }else if($max == 4){
                $htmlDeliveryEn = 'We deliver in 1-4 working days.';
                $htmlDeliveryTh = 'จัดส่งภายใน 1-4 วัน';
                $shippingDescription = str_replace('2-3','1-4', $shippingDescription);
            }else{
                $htmlDeliveryEn = 'We deliver in 3-10 working days.';
                $htmlDeliveryTh = 'จัดส่งภายใน 3-10 วัน';
                $shippingDescription = str_replace('2-3','3-10', $shippingDescription);
            }
        }

        // Set all required params and send emails
        $mailer->setSender(Mage::getStoreConfig(self::XML_PATH_EMAIL_IDENTITY, $storeId));
        if ($isWholesaleOrder) {
            $mailer->setSender(Mage::getStoreConfig(self::XML_PATH_WHOLESALE_EMAIL_IDENTITY, $storeId));
        }
        $mailer->setStoreId($storeId);
        $mailer->setTemplateId($templateId);
        $mailer->setTemplateParams(array(
                'order'        => $this,
                'billing'      => $this->getBillingAddress(),
                'payment_html' => $paymentBlockHtml,
                'delivery_en' => $htmlDeliveryEn,
                'delivery_th' => $htmlDeliveryTh,
                'shippingDescription' => $shippingDescription
            )
        );
        $mailer->send();

        $this->setEmailSent(true);
        $this->_getResource()->saveAttribute($this, 'email_sent');

        return $this;
    }

    /**
     * Send pickup email with order data
     *
     * @return Mage_Sales_Model_Order
     */
    public function sendNewPickupEmail()
    {
        $storeId = $this->getStore()->getId();

        // Get the destination email addresses to send copies to
        $copyTo = $this->_getEmails(self::XML_PATH_WHOLESALE_PICKUP_COPY_TO);
        $copyMethod = Mage::getStoreConfig(self::XML_PATH_WHOLESALE_PICKUP_COPY_METHOD, $storeId);

        // Start store emulation process
        $appEmulation = Mage::getSingleton('core/app_emulation');
        $initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation($storeId);

        try {
            // Retrieve specified view block from appropriate design package (depends on emulated store)
            $paymentBlock = Mage::helper('payment')->getInfoBlock($this->getPayment())
                ->setIsSecureMode(true);
            $paymentBlock->getMethod()->setStore($storeId);
            $paymentBlockHtml = $paymentBlock->toHtml();
        } catch (Exception $exception) {
            // Stop store emulation process
            $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);
            throw $exception;
        }

        // Stop store emulation process
        $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);

        // Retrieve corresponding email template id and customer name
        $templateId = Mage::getStoreConfig(self::XML_PATH_WHOLESALE_PICKUP_EMAIL_TEMPLATE, $storeId);
        if ($this->getCustomerIsGuest()) {
            $customerName = $this->getBillingAddress()->getName();
        } else {
            $customerName = $this->getCustomerName();
        }

        $mailer = Mage::getModel('core/email_template_mailer');
        $emailInfo = Mage::getModel('core/email_info');
        $emailInfo->addTo($this->getCustomerEmail(), $customerName);
        if ($copyTo && $copyMethod == 'bcc') {
            // Add bcc to customer email
            foreach ($copyTo as $email) {
                $emailInfo->addBcc($email);
            }
        }
        $mailer->addEmailInfo($emailInfo);

        // Email copies are sent as separated emails if their copy method is 'copy'
        if ($copyTo && $copyMethod == 'copy') {
            foreach ($copyTo as $email) {
                $emailInfo = Mage::getModel('core/email_info');
                $emailInfo->addTo($email);
                $mailer->addEmailInfo($emailInfo);
            }
        }

        // Delivery days
        $items = $this->getAllVisibleItems();
        $htmlDeliveryEn = 'We deliver in Bangkok in 1-3 working days.<br/><br/>We deliver in order regions of Thailand in 3-5 working days.';
        $htmlDeliveryTh = 'ทางเราจะจัดส่งสินค้าให้ถึงคุณภายใน 1-3 วันทำการสำหรับกรุงเทพฯและภายใน 3-5 <br/><br/>วันทำการสำหรับต่างจังหวัดทุกจังหวัดทั่วประเทศ';
        // Shipping Description
        $shippingDescription = $this->getShippingDescription();
        // Product ids array: $pIds
        $pIds = array();
        foreach($items as $item){
            /** @var Mage_Sales_Model_Order_Item $item */
            $pIds[] = $item->getProductId();
        }
        if(count($pIds) > 0){
            $flagDropship = $flagSpecial = false;
            $collection = Mage::getResourceModel('catalog/product_collection')
                ->addFieldToFilter('entity_id', $pIds)
                ->joinField('qty',
                    'cataloginventory/stock_item',
                    'qty',
                    'product_id=entity_id',
                    '{{table}}.stock_id=1',
                    'left')
                ->addAttributeToSelect('*');
            $max = 0;
            foreach ($collection as $product) {
                /** @var Mage_Catalog_Model_Product $product */
                if($product->getData('override_inventory') == 0 || $product->getStockItem()->getIsInStock() == 0) {
                    if ($product->getAttributeText('shipping_duration') == 'Dropship Product') {
                        $temp = 14;
                        if ($max < $temp) $max = $temp;
                    }
                    if ($product->getAttributeText('shipping_duration') == 'Special Order Product') {
                        if($product->getStockItem()->getManageStock() == 1){
                            if($product->getStockItem()->getIsInStock() != 0){
                                $temp = 10;
                            }
                        }else{
                            if($product->getStockItem()->getIsInStock() == 0){
                                $temp = 10;
                            }else{
                                $temp = 4;
                            }
                        }

                        if ($max < $temp) $max = $temp;
                    }
                    if ($product->getAttributeText('shipping_duration') == 'Consignment and Buy-in Product' &&  $product->getStockItem()->getIsInStock() == 1) {
                        $temp = 4;
                        if ($max < $temp) $max = $temp;
                    }
                }else{
                    $temp = 10;
                    if ($max < $temp) $max = $temp;
                }
            }
            if($max == 14){
                $htmlDeliveryEn = 'We deliver in 7-14 working days.';
                $htmlDeliveryTh = 'จัดส่งภายใน 7-14 วัน';
                $shippingDescription = str_replace('2-3','7-14', $shippingDescription);
            } else if($max == 10){
                $htmlDeliveryEn = 'We deliver in 7-10 working days.';
                $htmlDeliveryTh = 'จัดส่งภายใน 7-10 วัน';
                $shippingDescription = str_replace('2-3','7-10', $shippingDescription);
            }else if($max == 4){
                $htmlDeliveryEn = 'We deliver in 1-4 working days.';
                $htmlDeliveryTh = 'จัดส่งภายใน 1-4 วัน';
                $shippingDescription = str_replace('2-3','1-4', $shippingDescription);
            }else{
                $htmlDeliveryEn = 'We deliver in 3-10 working days.';
                $htmlDeliveryTh = 'จัดส่งภายใน 3-10 วัน';
                $shippingDescription = str_replace('2-3','3-10', $shippingDescription);
            }
        }

        // Set all required params and send emails
        $mailer->setSender(Mage::getStoreConfig(self::XML_PATH_WHOLESALE_EMAIL_IDENTITY, $storeId));
        $mailer->setStoreId($storeId);
        $mailer->setTemplateId($templateId);
        $mailer->setTemplateParams(array(
                'order'        => $this,
                'billing'      => $this->getBillingAddress(),
                'payment_html' => $paymentBlockHtml,
                'delivery_en' => $htmlDeliveryEn,
                'delivery_th' => $htmlDeliveryTh,
                'shippingDescription' => $shippingDescription
            )
        );
        $mailer->send();

        $this->setEmailSent(true);
        $this->_getResource()->saveAttribute($this, 'email_sent');

        return $this;
    }
}
