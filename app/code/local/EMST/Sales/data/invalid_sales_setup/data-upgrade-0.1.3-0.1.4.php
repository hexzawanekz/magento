<?php


/** @var Mage_Core_Model_Resource_Setup $installer */
$installer = $this;

$installer->startSetup();

/** @var Mage_Adminhtml_Model_Email_Template $mailTemplateModel */
$mailTemplateModel = Mage::getModel('core/email_template');
$mailTemplateModel->loadByCode('Order Invalid Orami');

if ($id = $mailTemplateModel->getId()) {
    /** @var Mage_Core_Model_Config $config */
    $config = Mage::getModel('core/config');
    $config->saveConfig(EMST_Sales_Model_Order::XML_PATH_INVALID_EMAIL_TEMPLATE, $id);
}

$installer->endSetup();