<?php
/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade SalesExport to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_Sales
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
*/

class EMST_Sales_Block_Adminhtml_Channel_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
  public function __construct()
  {
      parent::__construct();
      $this->setId('channelGrid');
      $this->setDefaultSort('channel_id');
      $this->setDefaultDir('ASC');
      $this->setSaveParametersInSession(true);
  }

  protected function _prepareCollection()
  {
      $collection = Mage::getModel('emst_sales/channel')->getCollection();
      $this->setCollection($collection);
      return parent::_prepareCollection();
  }

  protected function _prepareColumns()
  {
      $this->addColumn('channel_id', array(
          'header'    => Mage::helper('emst_sales')->__('ID'),
          'align'     =>'right',
          'width'     => '50px',
          'index'     => 'channel_id',
      ));

      $this->addColumn('name', array(
          'header'    => Mage::helper('emst_sales')->__('Name'),
          'align'     =>'left',
          'index'     => 'name',
      ));

	  /*
      $this->addColumn('content', array(
			'header'    => Mage::helper('emst_sales')->__('Item Content'),
			'width'     => '150px',
			'index'     => 'content',
      ));
	  */
	  $types = Mage::getModel('emst_sales/source_types')->getOptionArray();
      $this->addColumn('type', array(
          'header'    => Mage::helper('emst_sales')->__('Type'),
          'align'     => 'left',
          // 'width'     => '80px',
          'index'     => 'type',
          'type'      => 'options',
          'options'   => $types,
      ));
	  
        $this->addColumn('action',
            array(
                'header'    =>  Mage::helper('emst_sales')->__('Action'),
                'width'     => '100',
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => array(
                    array(
                        'caption'   => Mage::helper('emst_sales')->__('Edit'),
                        'url'       => array('base'=> '*/*/edit'),
                        'field'     => 'id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
                'is_system' => true,
        ));
		
		$this->addExportType('*/*/exportCsv', Mage::helper('emst_sales')->__('CSV'));
		$this->addExportType('*/*/exportXml', Mage::helper('emst_sales')->__('XML'));
	  
      return parent::_prepareColumns();
  }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('channel_id');
        $this->getMassactionBlock()->setFormFieldName('emst_sales_channel');

        $this->getMassactionBlock()->addItem('delete', array(
             'label'    => Mage::helper('emst_sales')->__('Delete'),
             'url'      => $this->getUrl('*/*/massDelete'),
             'confirm'  => Mage::helper('emst_sales')->__('Are you sure?')
        ));

        $statuses = Mage::getSingleton('emst_sales/source_types')->getOptionArray();

        array_unshift($statuses, array('label'=>'', 'value'=>''));
        $this->getMassactionBlock()->addItem('types', array(
             'label'=> Mage::helper('emst_sales')->__('Change channel type'),
             'url'  => $this->getUrl('*/*/massTypes', array('_current'=>true)),
             'additional' => array(
                    'visibility' => array(
                         'name' => 'type',
                         'type' => 'select',
                         'class' => 'required-entry',
                         'label' => Mage::helper('emst_sales')->__('types'),
                         'values' => $statuses
                     )
             )
        ));
        return $this;
    }

  public function getRowUrl($row)
  {
      return $this->getUrl('*/*/edit', array('id' => $row->getId()));
  }

}