<?php
/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade SalesExport to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_Sales
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
*/

class EMST_Sales_Block_Adminhtml_Channel extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_channel';
    $this->_blockGroup = 'emst_sales';
    $this->_headerText = Mage::helper('emst_sales')->__('Channel Manager');
    $this->_addButtonLabel = Mage::helper('emst_sales')->__('Add Channel');
    parent::__construct();
  }
}