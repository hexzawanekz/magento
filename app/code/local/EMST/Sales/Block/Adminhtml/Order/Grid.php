<?php
/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade SalesExport to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_Sales
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
*/

class EMST_Sales_Block_Adminhtml_Order_Grid extends Mage_Adminhtml_Block_Sales_Order_Grid
{
	
    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel('emst_sales/order_grid_collection')
			->addPaymentToSelect()
			->addOrderToSelect();
        $this->setCollection($collection);
        return Mage_Adminhtml_Block_Widget_Grid::_prepareCollection();
    }
    
    protected function _prepareColumns()
    {

        $this->addColumn('real_order_id',
                         array(
            'header' => Mage::helper('sales')->__('Order #'),
            'width' => '80px',
            'type' => 'text',
            'index' => 'increment_id',
        ));

        if (!Mage::app()->isSingleStoreMode()) {
            $this->addColumn('store_id',
                             array(
                'header' => Mage::helper('sales')->__('Purchased From (Store)'),
                'index' => 'store_id',
                'type' => 'store',
                'store_view' => true,
                'display_deleted' => true,
            ));
        }

        $this->addColumn('created_at',
                         array(
            'header' => Mage::helper('sales')->__('Purchased On'),
            'index' => 'created_at',
            'type' => 'datetime',
            'width' => '100px',
        ));

        $this->addColumn('customer_id',
                         array(
            'header' => Mage::helper('sales')->__('Customer ID'),
            'index' => 'customer_id',
            'width' => '70px',
        ));

        $this->addColumn('billing_name',
                         array(
            'header' => Mage::helper('sales')->__('Bill to Name'),
            'index' => 'billing_name',
        ));

        $this->addColumn('customer_email',
						 array(
			'header' => Mage::helper('sales')->__('Customer Email'),
			'index' => 'customer_email',
			'is_system' => !$this->isAdmin()
		));

        $this->addColumn('payment_method',
                         array(
            'header' => Mage::helper('sales')->__('Payment Type'),
            'index' => 'payment_method',
            'type' => 'options',
            'width' => '120px',
            'options' => Mage::helper('emst_sales')->getStorePaymentMethods(),
        ));

        $this->addColumn('grand_total',
                         array(
            'header' => Mage::helper('sales')->__('G.T. (Purchased)'),
            'index' => 'grand_total',
            'filter_index' => 'main_table.grand_total',
            'type' => 'currency',
            'currency' => 'order_currency_code',
        ));

        $this->addColumn('status',
                         array(
            'header' => Mage::helper('sales')->__('Status'),
            'index' => 'status',
            'filter_index' => 'main_table.status',
            'type' => 'options',
            'options' => Mage::getSingleton('sales/order_config')->getStatuses(),
        ));

        $this->addColumn('is_wholesale', array(
            'header' => Mage::helper('sales')->__('Wholesale'),
            'index' => 'is_wholesale',
            'filter_index' => 'order.is_wholesale',
            'type' => 'options',
            'width' => '70px',
            'options' => array('1' => 'Yes', '0' => 'No')
        ));

        $this->addColumn('market_place', array(
            'header' => Mage::helper('sales')->__('MarketPlace'),
            'index' => 'market_place',
            'filter_index' => 'order.market_place',
            'type' => 'options',
            'width' => '70px',
            'options' => array('1' => 'Yes', '0' => 'No' , '' => 'No' , '2'=> 'Mixed')
        ));


        $this->addColumn('old_order_id', array(
            'header' => Mage::helper('sales')->__('Old Order ID'),
            'index' => 'old_order_id',
            'filter_index' => 'order.old_order_id',
            'type' => 'options',
            'width' => '70px',
            'options' => array( "Yes" => 'Yes', "No" => 'No'),
            'renderer' => 'Mage_Adminhtml_Block_Sales_Order_Renderer_Old',
            'filter_condition_callback' => array($this, '_filterIsSaleItem'),
        ));

        if (Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/view')) {
            $this->addColumn('action',
                             array(
                'header' => Mage::helper('sales')->__('Action'),
                'width' => '50px',
                'type' => 'action',
                'getter' => 'getId',
                'actions' => array(
                    array(
                        'caption' => Mage::helper('sales')->__('View'),
                        'url' => array('base' => '*/sales_order/view'),
                        'field' => 'order_id'
                    )
                ),
                'filter' => false,
                'sortable' => false,
                'index' => 'stores',
                'is_system' => true,
            ));
        }
        $this->addRssList('rss/order/new', Mage::helper('sales')->__('New Order RSS'));

        $this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV'));
        $this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel XML'));

        $this->sortColumnsByOrder();
        return $this;
    }

    protected function isAdmin()
    {
        $admins = Mage::getModel('admin/roles')->load(1)->getRoleUsers();
        $curId = Mage::getSingleton('admin/session')->getUser()->getId();
        if (in_array($curId, $admins)) {
            return true;
        }
        return false;
    }

    protected function _filterIsSaleItem($collection, $column)
    {
        $value = $column->getFilter()->getValue();

        if (!$value) {
            return $this;
        }

        if ($value == "Yes") {
            $this->getCollection()->getSelect()->where("old_order_id != 0");
        } else {
            $this->getCollection()->getSelect()->where("old_order_id = 0");
        }

        return $this;

    }
}
