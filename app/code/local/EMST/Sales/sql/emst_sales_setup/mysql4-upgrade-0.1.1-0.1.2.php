<?php
/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade TableRate to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_TableRate
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
*/

$installer = $this;

$installer->startSetup();
	
	$write = $this->getConnection();
	$stateTable = $installer->getTable('sales/order_status_state');
	$statusTable = $installer->getTable('sales/order_status');
	// create and assign new "Pending COD" status to new state
	$write->insertOnDuplicate(
		$statusTable,
		array(
			'status'     => EMST_Sales_Model_Order::STATE_INVALID,
			'label'      => 'Invalid'
		)
	);
	$write->insertOnDuplicate(
		$stateTable,
		array(
			'status'     => EMST_Sales_Model_Order::STATE_INVALID,
			'state'      => EMST_Sales_Model_Order::STATE_INVALID,
			'is_default' => 1
		)
	);
	
$installer->endSetup();