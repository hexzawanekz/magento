<?php
/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Sales to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_Sales
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
*/

class EMST_Sales_Helper_Data extends Mage_Core_Helper_Abstract
{
	protected $_paymentMethods = array();
	protected $_config = null;
	
	public function getStorePaymentMethods() {
		if(empty($this->_paymentMethods)) {
			$methods = Mage::getSingleton('payment/config')->getActiveMethods();
			foreach($methods as $method){
				$this->_paymentMethods[$method->getCode()]=$method->getTitle();
			} 
		}
		return $this->_paymentMethods;
	}
	public function getChannelOptionByType($type) {
		if(!is_numeric($type)) { 
			$type = $this->getChannelTypesModel()->getTypeByFieldName($type);
		}
		if(!is_numeric($type)) {
			throw Mage::exception('Mage_Core', Mage::helper('emst_sales')->__('Wrong Channel Type'));
		}
		return Mage::getModel('emst_sales/channel')->getCollection()
				->addTypeFilter($type)
				->toOptionArray();
	}
	public function getChannelFields() {
		return $this->getChannelTypesModel()->getChannelFields();
	}
	public function getChannelTypesModel() {
		return Mage::getModel('emst_sales/source_types');
	}
	public function uploadCSV($filename)
    {
        if (isset($_FILES[$filename]['name']) && $_FILES[$filename]['name'] != '') {
            $uploader = new Varien_File_Uploader($filename);
            $uploader->setAllowedExtensions(array('csv'));
            $uploader->setAllowRenameFiles(false);
            $uploader->setFilesDispersion(false);
            $path = Mage::getBaseDir('var') . DS . 'import' . DS . $filename . DS;
            $name = date('d-m-y-H-i-s', time()) . '_' . rand(0, 10) . '.csv';
            $uploader->save($path, $name);
            return $path . $name;
        }
        return false;
    }

    public function readCSV($path)
    {
        $check = file_exists($path);
        if ($check) {
            $check = is_file($path);
        }
        if ($check) {
            $file = file($path);
            unset($file[0]);
            return $this->validateContent($file);
        }
        return false;
    }

    public function checkNullValue(array $row)
    {
        foreach ($row as $element) {
            if (trim($element) == '') return true;
        }
        return false;
    }

    public function validateContent(array $file)
    {
        if (sizeof($file) == 0) return false;        
        $invalid = array();
        $valid = array();
		$i = 1;
        foreach ($file as $row) {
            $row_ = explode(',', $row);
            if (sizeof($row_) != 3 || $this->checkNullValue($row_)) {               
               $invalid[] = $i;
            }
			else {
				$valid[] = $row_;
			}
			++$i;
        }
        return array('invalid' => $invalid,'valid'=>$valid);
    }
	
	public function isCronActive() {
		return (bool) Mage::getStoreConfig('emst_sales/cronconfig/enable');
	}
	public function isInstantCronActive() {
		return (bool) Mage::getStoreConfig('emst_sales/instantconfig/enable');
	}
	public function getConfig($storeId = null) {
		if(!$this->_config) {
			$config = array(
				'instant_payment'=>'',
				'one23_payment' => '',
				'cod_payment'=>'',
			);		
			foreach($config as $k=>$v) {
				$config[$k] = Mage::getStoreConfig('emst_sales/cronconfig/'.$k, $storeId);
			}
			$this->_config = $config;
		}
		return $this->_config;
	}
}