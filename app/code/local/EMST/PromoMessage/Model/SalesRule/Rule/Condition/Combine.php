<?php
/**
 * Developed by Acommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.Acommerce.asia/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PromoMessage to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_PromoMessage
 * @copyright   Copyright (c) 2012 Acommerce (http://www.Acommerce.asia)
 * @license     http://www.Acommerce.asia/LICENSE-E.txt
*/


class EMST_PromoMessage_Model_SalesRule_Rule_Condition_Combine extends Mage_SalesRule_Model_Rule_Condition_Combine
{
	protected $_notifyAll = false;
	
	protected $_applyFor = array('order_num','sales_amount');
	protected function _showMessage($cond,$object) {
		// TODO: put this value to be configurable
		$showAll = $this->_notifyAll;
		if($cond->getRule()->getCouponType() != Mage_SalesRule_Model_Rule::COUPON_TYPE_NO_COUPON) {
			$attribute = $cond->getAttributeName();
			$operator = $cond->getOperator();
			if(is_object($attribute) || is_object($operator)) {
				return $this;
			}
			$operator = $this->_parseOperator($operator);
			$attribute = is_array($attribute)?implode(',',$attribute):$attribute;
			$configuredValue = $cond->getValue();
			$currValue = $this->getValue();
			$msg = '';
			if($showAll) {
				$msg = 'Failed to apply the coupon "%s" '. "{$attribute} {$operator} {$configuredValue}";
			}
			else {
				switch($cond->getAttribute()){
					case 'order_num':
						if($configuredValue == 0){
							$msg = Mage::helper('checkout')->__('The coupon code "%s" can be only applied on your first time order');
						}else{
							$msg = Mage::helper('checkout')->__('The coupon code %s apply for only customer who has at least %s completed order(s)', '"%s"', $configuredValue);
						}
						break;
					case 'sales_amount':
						$msg = Mage::helper('checkout')->__('Coupon code %s does not reach the minimum spend of %s', ' "%s"', $configuredValue);
						break;
					case 'payment_method':
						$msg = Mage::helper('checkout')->__('Please apply this coupon after choosing correct payment method.');
						break;
					default:
						$msg = 'Coupon code "%s" is not valid for the product(s) in cart';
						break;
				}
			}			
			Mage::helper('promomessage')->addMessage($msg);
		}
	}
	protected function _parseOperator($op) {
		$default = array(
                '=='  => 'should be',
                '!='  => 'could not be applied for',
                '>='  => 'should be equals or greater than',
                '<='  => 'should be equals or less than',
                '>'   => 'should be greater than',
                '<'   => 'should be less than',
                '{}'  => 'could not be applied only for',
                '!{}' => 'could not be applied for',
                '()'  => 'can be applied only for',
                '!()' => 'could not be applied for'
            );
		if(isset($default[$op])) {
			return $default[$op];
		}
		return $op;
	}
    public function validate(Varien_Object $object) {
        if (!$this->getConditions()) {
            return true;
        }

        $all    = $this->getAggregator() === 'all';
        $true   = (bool)$this->getValue();
		
        foreach ($this->getConditions() as $cond) {
            $validated = $cond->validate($object);			
            if ($all && $validated !== $true) {
				$this->_showMessage($cond,$object);
                return false;
            } elseif (!$all && $validated === $true) {
                return true;
            }
        }
        return $all ? true : false;
    }
}
