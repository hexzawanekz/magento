<?php


class EMST_One23KBT_Block_Redirect extends Mage_Core_Block_Abstract
{
	protected function _toHtml()
	{
		$one23KBT = $this->getModel();
        $form = new Varien_Data_Form();
        $form->setAction($one23KBT->getGatewayUrl())
            ->setId('123kbt_payment_checkout')
            ->setName('123kbt_payment_checkout')
            ->setMethod('POST')
            ->setUseContainer(true);
        foreach ($one23KBT->getStandardCheckoutFormFields() as $field => $value) {
            $form->addField($field, 'hidden', array('name' => $field, 'value' => $value));
        }

        $formHTML = $form->toHtml();

        $html = '<html>';
		$html .= '<head><meta content="text/html; charset=utf-8" http-equiv="Content-Type"></head>';
		$html .= '<body>';
        $html.= $this->__('You will be redirected to 123 - KBT in a few seconds.');
        $html.= $formHTML;
        $html.= '<script type="text/javascript">document.getElementById("123kbt_payment_checkout").submit();</script>';
        $html.= '</body></html>';


        return $html;
    }

}
