<?php
class EMST_One23KBT_PaymentController extends Mage_Core_Controller_Front_Action
{
	protected $_order = null;
	protected $_checkout_session = null;
	protected $_payment_model = null;
	protected $_order_id = null;
	
    protected function getOrder() {
		if ($this->_order == null)
            $this->_order = Mage::getModel('sales/order')->loadByIncrementId($this->getOrderId());
        return $this->_order;
    }
	protected function getOrderId() {
		if(!$this->_order_id) {
			$this->_order_id = $this->getSession()->getLastRealOrderId();
		}
		return $this->_order_id;
	}
	public function setOrder(Mage_Sales_Model_Order $order) {
		$this->_order = $order;
	}
	protected function getSession() {
		if(!$this->_checkout_session)
			$this->_checkout_session=Mage::getSingleton('checkout/session');
		return $this->_checkout_session;
	}
	public function sendTransactionAction() {
		try {
			$order = $this->getOrder();
			$session = $this->getSession();
			$one23kbt = $this->getPaymentModel()->setOrder($order);
			if(!$order->getId() || $order->getStatus() == 'complete' || $session->getRedirected() == $order->getId() || !$one23kbt->validateRedirect($order)) {
				Mage::getSingleton('checkout/session')->clear();
				$this->norouteAction();
				return;
			}
			$src_data = $one23kbt->getRequestData();
			$one23kbt
				->setDatafile($src_data['sourcefile'])
				->setEncryptfile($src_data['outputfile'])
				->encryptData();
			
			$order->addStatusHistoryComment(
			Mage::helper('one23kbt')->__('Customer was redirected to 123-KBT')
			)->save();
			$this->getResponse()
			->setBody($this->getLayout()
			->createBlock('one23kbt/redirect')
			->setModel($one23kbt)
			->toHtml());
			$session->setRedirected($order->getId());
		}
		catch (Mage_Payment_Model_Info_Exception $e) {
			$this->_getCoreSession()->addError($e->getMessage());
			$this->getSession()->clear();
			$this->_redirect('');
		}
		catch(Exception $e) {
			Mage::log($e->getMessage(),null, 'One23KBT.log');
			$this->_getCoreSession()->addError($this->__('There was an error, please try again'));
			$this->getSession()->clear();
			$this->_redirect('');
		}
	}
	public function merchantAction() {
		try{
			$response_code = Mage::getModel('one23kbt/source_responseCode')->getResponseCode();
			$one23kbt = $this->getPaymentModel()->setType('MerchantUrl');
			$decrypt = $this->decryptData($this->getRequest()->getParam('OneTwoThreeRes'));
			$this->_order_id = $decrypt->InvoiceNo;
			$payment = $this->getOrder()->getPayment();
			$channel = $payment->getAdditionalInformation('Channel');
			$agent = $payment->getAdditionalInformation('Bank');
			$hash = $one23kbt->hashData($one23kbt->getMerchantId().trim($decrypt->InvoiceNo).trim($decrypt->RefNo1));
			$msg = '';
			
			if($hash != (string)$decrypt->HashValue) {
				$msg = 'Invalid Hash value';
			}
			elseif(((string)$decrypt->ResponseCode == '000' || (string)$decrypt->ResponseCode == '001')) {
				$this->_redirect('checkout/onepage/success');
			}
			elseif(isset($response_code[$decrypt->ResponseCode])) {
				
				$msg = $response_code[$decrypt->ResponseCode];
			}
			else {
				$msg = 'Unknown error from the gateway system';
			}
		}
		catch(Exception $e) {
			Mage::log($e->getMessage(),null, 'One23KBT.log');
			$msg = 'There was an error occur while trying to verify your payment';
		}
		if($msg) {
			$this->_getCoreSession()->addError($msg);
			$this->_redirect('');
		}
	}
	protected function decryptData($data) {
		$response = "\r\n".$data;
		$response = trim($response);
		$one23kbt = $this->getPaymentModel()
				->setEncryptResponse($response)
				->decryptData();
		$decrypt = Mage::getModel('core/config_base',$one23kbt->getDecryptedRes())->getNode();
		return $decrypt;
	}
	protected function getPaymentModel() {
		if(!$this->_payment_model) {
			$this->_payment_model = Mage::getModel('one23kbt/one23KBT');
		}
		return $this->_payment_model;
	}
	public function apiCallUrlAction() {
		try{
			$response_code = Mage::getModel('one23kbt/source_responseCode')->getResponseCode();
			$one23kbt = $this->getPaymentModel()->setType('APICall');
		    $decrypt = $this->decryptData($this->getRequest()->getParam('OneTwoThreeRes'));
			$this->_order_id = $decrypt->InvoiceNo;
		    $order = $this->getOrder();
			$hash = $one23kbt->hashData($one23kbt->getMerchantId().$decrypt->InvoiceNo.$decrypt->RefNo1);
			$response = $one23kbt->getResponseData();
			if(!$order->getId()) {	
				$response['FailureReason'] = 'Invalid InvoiceNo';
				return;
			}
			$response['MessageID'] = $decrypt->InvoiceNo;
		    $one23kbt->setOrder($order)
				->setParsedData($one23kbt->parseXml($decrypt));
		    if((string)$decrypt->ResponseCode == '000' && (string)$decrypt->HashValue == $hash) {
				$one23kbt->saveInvoice();
		    }
		    else {
				
				$one23kbt->logRequest();
		    }
			$response['Result'] = 'SUCCESS';
		}
		catch (Mage_Payment_Model_Info_Exception $e) {			
			$response['FailureReason'] = $e->getMessage();
			$response['Result'] = 'FAILURE';;
		}
		catch(Exception $e) {			
			Mage::log($e->getMessage(),null, 'One23KBT.log');
			$response['FailureReason'] = 'System exception';
			$response['Result'] = 'FAILURE';
		}
		try {
			$xml = $one23kbt->getSimpleXml($response,'APICallUrlRes');
			$src_data = $one23kbt->writeRequest($xml,array('API_PrintBack','Xml','Encrypt'));
			$one23kbt
				->setDatafile($src_data['sourcefile'])
				->setEncryptfile($src_data['outputfile'])
				->encryptData();
		}
		catch(Exception $e) {
			Mage::log($e->getMessage(),null, 'One23KBT.log');
		}
		echo $one23kbt->getEncryptedReq();
	}
	protected function _getCoreSession() {
		return Mage::getSingleton('core/session');
	}
}
