<?php
class EMST_Cc2p_Model_Cronjobs {
	protected $_paymodel = null;
	public function checkTransaction() {
		$cc2p = $this->getPayModel();
		if($cc2p->isCronEnable() && $cc2p->getInquiryUrl()) {
			
			$ids = Mage::getResourceModel('cc2p/order_grid_collection')
							->addFieldToFilter('main_table.status', array('nin' => $this->_getExcludedStates()))
							->addFieldToFilter('created_at',array('gt'=>"DATE_SUB(".Varien_Date::now().", INTERVAL 30 MINUTE)"))
							->getAllIds();
			
			Mage::helper('cc2p')->transactionInquiry($ids,$cc2p->cronInvoice());
		}
	}
	 protected function _getExcludedStates() {
        return array(
            Mage_Sales_Model_Order::STATE_CLOSED,
            Mage_Sales_Model_Order::STATE_CANCELED,
            Mage_Sales_Model_Order::STATE_HOLDED,
            Mage_Sales_Model_Order::STATE_PROCESSING,
            Mage_Sales_Model_Order::STATE_COMPLETE,
        );
    }
	public function getPayModel() {
		if(!$this->_paymodel) {
			$this->_paymodel = Mage::getModel('cc2p/cc2p');
		}
		return $this->_paymodel;
	}
}
