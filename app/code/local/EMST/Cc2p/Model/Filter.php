<?php
/**
 * Developed by Acommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.Acommerce.asia/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade SalesExport to newer
 * versions in the future.
 *
 * @category    Acommerce
 * @package     Acommerce_2C2P
 * @copyright   Copyright (c) 2012 Acommerce (http://www.Acommerce.asia)
 * @license     http://www.Acommerce.asia/LICENSE-E.txt
*/

class EMST_Cc2p_Model_Filter extends Mage_Core_Model_Email_Template_Filter
{	
	public function itemDirective($construction) {
		$params = $this->_getIncludeParameters($construction[2]);
		$order = $this->_getVariable('order');
		if(!isset($params['data']) || !($order instanceof Mage_Sales_Model_Order)) {
			return null;
		}
		$data = explode(',',$params['data']);
		$string = '';
		foreach($order->getAllVisibleItems() as $item) {
			$string .= '<mes:Item>';
			foreach($data as $elm) {
				$field = explode('=',$elm);
				if(!isset($field[1]))	{
					continue;
				}
				if(!trim($field[0])) {
					$string .= "<{$field[1]}/>";
				}
				else {
					$value = $item->getData($field[0]);
					if(!$value) {
						$value = Mage::getResourceModel('catalog/product')->getAttributeRawValue($item->getProductId(),$field[0],$item->getStoreId());
					}
					if(!$value) {
						$value = $order->getData($field[0]);
					}
					if($field[0]=='qty_ordered') {
						$value = (int)$value;
					}
					else if(is_numeric($value)) {
						if($field[0] == 'row_total') {
							$value -= $item->getDiscountAmount();
						}
						$value = number_format($value,2,'.','');
					}
					elseif(!is_numeric($value)) {
						$value = Mage::helper('core')->escapeHtml($value);
					}
					$string .= "<{$field[1]}>{$value}</{$field[1]}>";
				}
			}
			$string .= '</mes:Item>';
		}
        return $string;
	}
} 