<?php

class EMST_Cc2p_Model_Resource_Card extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {         
        $this->_init('cc2p/card', 'card_id');
    }
}