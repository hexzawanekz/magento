<?php
class EMST_Cc2p_Model_Source_AeonType
{
	public function toOptionArray() {
		$_methods = array(
			'1' => 'Confirm Order',
			'2' => 'Confirm Payment',
		);
		return $_methods;
    }
	
}