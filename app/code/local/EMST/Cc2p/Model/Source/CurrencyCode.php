<?php
class EMST_Cc2p_Model_Source_CurrencyCode
{
	public function getCurrencyCode() {
		return array(
			"THB" => "764" ,
			"USD" => "840" ,
			"SGD" => "702" ,
			"JPY" => "392" ,
			"GBP" => "826" ,
			"MYR" => "458" ,
			"EUR" => "978" 
		);
	}
	public function toOptionArray()
    {
		$options = array();
		foreach($this->getCurrencyCode() as $lb=>$vl) {
			$options[] = array('value' => $vl, 'label' => $lb);
		}
        return $options;
    }
}