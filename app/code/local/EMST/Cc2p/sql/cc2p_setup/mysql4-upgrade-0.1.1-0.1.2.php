<?php

$installer = $this;
$installer->startSetup();
if($installer->getAttribute('customer','stored_cc')) {
	$installer->removeAttribute('customer', 'stored_cc');
}
$installer->run(" 
CREATE TABLE {$this->getTable('cc2p_card')} (
	`card_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`customer_id` INT(11) UNSIGNED NOT NULL,
	`cc_stored_unique_id` VARCHAR(255) NULL DEFAULT NULL,
	`cc_owner` VARCHAR(255) NOT NULL,
	`cc_type` VARCHAR(255) NOT NULL,
	`cc_number` VARCHAR(255) NOT NULL,
	`cc_exp` VARCHAR(255) NOT NULL,
	`cc_currency` VARCHAR(255) NULL DEFAULT NULL,
	`added_at` DATETIME NULL DEFAULT NULL,
	PRIMARY KEY (`card_id`),
	CONSTRAINT cc2p_unique_index UNIQUE INDEX (customer_id,cc_stored_unique_id),
	CONSTRAINT `FK_cc2p_card_customer_entity` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;");
$installer->endSetup();
