<?php
class EMST_Cc2p_Block_Form extends Mage_Payment_Block_Form_Cc
{
	protected $_cards = array();
	protected $_payModel = null;
    protected function _construct() {
		parent::_construct();
        $this->setTemplate('cc2p/form.phtml');
    }
	public function getPeriods() {
		return $this->_getPaymentModel()->getPeriods($this->getQuote(),true);
	}
	public function hasPeriod() {
		return count($this->getPeriods());
	}
	public function periodEnabled() {
		return $this->_getPaymentModel()->getUseInstallment();
	}
	public function getBanks() {
		return $this->_getPaymentModel()->getBanks();
	}
	public function getPeriodsBankJson() {
		$bJs = array();
		foreach($this->getPeriods() as $vl=>$lb) {
			$period = $this->_getPaymentModel()->getPeriod($vl,true);
			if($period) {
				$bJs[$vl] = $period['bank'];
			}
		}
		return Mage::helper('core')->jsonEncode($bJs);
	}
	public function getPeriodsBankJson1() {
		$bJs = array();
		foreach($this->getBanks() as $code=>$name) {
			$bankPeriods = array();
			foreach($this->getPeriods() as $vl=>$lb) {
				$period = $this->_getPaymentModel()->getPeriod($vl,true);
				if($period && $period['bank']==$code) {
					$bankPeriods[] = array('value'=>$vl,'lable'=>$lb);
				}
			}
			$bJs[$code] = $bankPeriods;
		}
		return Mage::helper('core')->jsonEncode($bJs);
	}
	protected function _getPaymentModel() {
		if(!$this->_payModel) {
			$this->_payModel = Mage::getModel('cc2p/cc2p')->setOrder($this->getQuote());
		}
		return $this->_payModel;
	}
	public function getCcStored() {
		if(sizeof($this->_cards)<=1) {
			$cards = Mage::getModel('cc2p/card')
					->getCollection()
					->addCustomerIdFilter(Mage::getSingleton('customer/session')->getCustomer()->getId());
			foreach($cards as $card) {
				$this->_cards[$card->getCcType()] = $card->getCcNumber();
			}
			//$this->_cards['new'] = $this->__('new');
		}
		return $this->_cards;
	}
	public function hasCards() {
		$cards = $this->getCcStored();
		if(sizeof($cards)>=1) {
			return true;
		}
		else {
			return false;
		}
	}
	public function getQuote() {
		return Mage::getSingleton('checkout/session')->getQuote();
	}
	public function getHeaderText() {
		return $this->getMethod()->getHeaderText();
	}
	public function getFooterText() {
		return $this->getMethod()->getFooterText();
	}
	public function getDes() {
		$items = $this->getQuote()->getAllItems();
		$pr_id = '';
		foreach ($items as $item) {
			$pr_id = $item->getProductId();
		}
		return Mage::getModel('catalog/product')->load($pr_id)->getUrlKey();
	}
	public function getAmount() {
		return $this->getQuote()->getGrandTotal();
	}
	public function getCurrency() {
		return $this->getMethod()->getCurrencyCode();
	}
	public function getUserDefined($num) {
		return $this->getMethod()->getUserDefined($num);
	}
	public function getPayCategoryID() {
		return $this->getMethod()->getPayCategoryID();
	}
	public function getCardHolderName() {
		if($this->getInfoData('cc_owner')) {
			return $this->getInfoData('cc_owner');
		}
		else {
			$customer = Mage::getSingleton('customer/session')->getCustomer();

			return $customer->getFirstname().' '.$customer->getLastname();
		}
	}
	 public function getCountryCollection()
    {
        $collection = $this->getData('country_collection');
        if (is_null($collection)) {
            $collection = Mage::getModel('directory/country')->getResourceCollection();
			if($this->getCountryFilter()) {
				$collection ->addCountryCodeFilter($this->getCountryFilter());
			}
            $this->setData('country_collection', $collection);
        }

        return $collection;
    }

    public function getCountryHtmlSelect($defValue=null, $name='payment[panCountry]', $id=null, $title='Country of Issuer Bank')
    {
        if (is_null($defValue)) {
            $defValue = $this->getCountryId();
        }
		if(!$id) {
			$id = $this->getMethodCode().'_cc_country';
		}
        $cacheKey = 'DIRECTORY_COUNTRY_SELECT_ALL_'.Mage::app()->getStore()->getCode();
        if (Mage::app()->useCache('config') && $cache = Mage::app()->loadCache($cacheKey)) {
            $options = unserialize($cache);
        } else {
            $options = $this->getCountryCollection()->toOptionArray(Mage::helper('directory')->__('--Please Select--'));
            if (Mage::app()->useCache('config')) {
                Mage::app()->saveCache(serialize($options), $cacheKey, array('config'));
            }
        }
        $html = $this->getLayout()->createBlock('core/html_select')
            ->setName($name)
            ->setId($id)
            ->setTitle(Mage::helper('directory')->__($title))
            ->setClass('validate-select')
            ->setValue($defValue)
            ->setOptions($options)
            ->getHtml();
        return $html;
    }
	public function getCountryId()
    {
        $countryId = $this->getData('country_id');
        if (is_null($countryId)) {
            $countryId = Mage::helper('core')->getDefaultCountry();
        }
        return $countryId;
    }
	public function getCcAvailableCountry() {
		$selected = $this->getInfoData('panCountry')?$this->getInfoData('panCountry'):null;
		if($this->getMethod()->getConfigData('allowspecific')==1){
            $availableCountries = explode(',', $this->getMethod()->getConfigData('specificcountry'));
			$this->setCountryFilter($availableCountries);
        }
		return $this->getCountryHtmlSelect($selected);
	}

	public function getCustomerEmail() {
		$stored_email = $this->htmlEscape($this->getInfoData('cardholderEmail'));
		$customer_email = Mage::getSingleton('customer/session')->getCustomer()->getEmail();
		return $stored_email ? $stored_email: $customer_email;
	}
}
