<?php
/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Onestepcheckout to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_CC2P
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
*/
class EMST_Cc2p_Block_Adminhtml_System_Config_Fieldset_Hint
    extends Mage_Adminhtml_Block_Abstract
    implements Varien_Data_Form_Element_Renderer_Interface
{
    public function render(Varien_Data_Form_Element_Abstract $element) {
		$elementOriginalData = $element->getOriginalData();
        if (isset($elementOriginalData['action_link'])) {
            $url = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB).$elementOriginalData['action_link'];
			return sprintf('<div style="padding:10px;background-color:#fff;border:1px solid #ddd;margin-bottom:7px;">%s: %s</div>',$this->__('Action URL'),$url);
        }
    }
}
