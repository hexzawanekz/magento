<?php
/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Onestepcheckout to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_Onestepcheckout
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
*/

class EMST_Cc2p_Block_Adminhtml_System_Config_Form_Field_Period extends Mage_Adminhtml_Block_System_Config_Form_Field_Array_Abstract
{
    public function __construct() {
        
        $this->_addAfter = true;
        $this->_addButtonLabel = Mage::helper('adminhtml')->__('Add');
        parent::__construct();
        $this->setTemplate('cc2p/system/config/form/field/array.phtml');
    }
    protected function _prepareLayout() {
		$this->addColumn('period_label', array(
            'label' => Mage::helper('adminhtml')->__('Label'),
            'style' => 'width:120px',
        ));
        $this->addColumn('period_value', array(
            'label' => Mage::helper('adminhtml')->__('Value'),
            'style' => 'width:20px',
        ));
        $this->addColumn('bank', array(
            'label' => Mage::helper('adminhtml')->__('Bank'),
            'type'	=> 'select',
            'renderer' =>  $this->getLayout()->createBlock('cc2p/adminhtml_system_config_form_field_render_select'),
            'source'=>'cc2p/source_banks',
            'style' => 'width:120px',
        ));
        $this->addColumn('interest_type', array(
            'label' => Mage::helper('adminhtml')->__('Payment Type'),
            'type'	=> 'select',
            'renderer' =>  $this->getLayout()->createBlock('cc2p/adminhtml_system_config_form_field_render_select'),
            'source'=>'cc2p/source_interestType',
            'style' => 'width:144px',
        ));
        $this->addColumn('minimum', array(
            'label' => Mage::helper('adminhtml')->__('Minimum'),
            'style' => 'width:50px',
        ));
        $this->addColumn('promo', array(
            'label' => Mage::helper('adminhtml')->__('Promo Code'),
            'style' => 'width:70px',
        ));
        $this->addColumn('valid_from', array(
            'label' => Mage::helper('adminhtml')->__('Valid From'),
            'style' => 'width: 70px',
            'type'	=> 'date',
            //'renderer' => new EMST_Cc2p_Block_Adminhtml_System_Config_Form_Field_Render_Date,
            'renderer' => $this->getLayout()->createBlock('cc2p/adminhtml_system_config_form_field_render_date')
        ));
        $this->addColumn('valid_to', array(
            'label' => Mage::helper('adminhtml')->__('Valid To'),
            'style' => 'width:70px',
            'type'	=> 'date',
            'renderer' => new EMST_Cc2p_Block_Adminhtml_System_Config_Form_Field_Render_Date
        ));
        return parent::_prepareLayout();
	}
    
    public function addColumn($name, $params) {
        $this->_columns[$name] = array(
            'label'     => empty($params['label']) ? 'Column' : $params['label'],
            'size'      => empty($params['size'])  ? false    : $params['size'],
            'style'     => empty($params['style'])  ? null    : $params['style'],
            'class'     => empty($params['class'])  ? null    : $params['class'],
            'type'  	=> empty($params['type']) ? '' : $params['type'],
            'source'  	=> empty($params['source']) ? '' : $params['source'],
            'renderer'  => false,
        );
        if ((!empty($params['renderer'])) && ($params['renderer'] instanceof Mage_Core_Block_Abstract)) {
            $this->_columns[$name]['renderer'] = $params['renderer'];
        }
    }
}
