<?php 
class EMST_Cc2p_Block_Adminhtml_Customer_Edit_Tabs_Card extends Mage_Adminhtml_Block_Widget_Accordion
{
	 protected function _prepareLayout()
    {
        $customer = Mage::registry('current_customer');

        $this->setId('customerViewCard');

        $this->addItem('stored_cc', array(
            'title'       => Mage::helper('cc2p')->__('Stored Credit Card'),
            'ajax'        => true,
            'content_url' => $this->getUrl('*/*/card', array('_current' => true)),
        ));
    }
}