<?php
class EMST_Cc2p_Block_Adminhtml_Transaction_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct() {
		parent::__construct();
		$this->setId('Cc2pGrid');
		$this->setDefaultSort('created_at');
		$this->setDefaultDir('DESC');
		$this->setUseAjax(true);		
		$this->setSaveParametersInSession(true);
	}

	protected function _prepareCollection() {
		$collection = Mage::getResourceModel('cc2p/order_grid_collection');
		$this->setCollection($collection);		
		return parent::_prepareCollection();
	}
	protected function _prepareColumns() {
		$this->addColumn('increment_id', array(
            'header'        => Mage::helper('cc2p')->__('#'),
            'align'         => 'left',
            'width'         => '20px',
            'index'         => 'increment_id',
            'filter_index'  => 'main_table.increment_id',
        ));        
        if (!Mage::app()->isSingleStoreMode()) {
            $this->addColumn('store_id',
                             array(
                'header' => Mage::helper('sales')->__('Purchased From (Store)'),
                'index' => 'store_id',
                'type' => 'store',
				'width' => '150px',
                'store_view' => true,
                'display_deleted' => true,
            ));
        }
		$this->addColumn('created_at',
                         array(
            'header' => Mage::helper('sales')->__('Purchased On'),
            'index' => 'created_at',
            'type' => 'datetime',
            'width' => '150px',
        ));
		$this->addColumn('customer_id',
                         array(
            'header' => Mage::helper('sales')->__('Customer ID'),
            'index' => 'customer_id',
            'width' => '50px',
        ));
		$this->addColumn('grand_total',
                         array(
            'header' => Mage::helper('sales')->__('G.T. (Purchased)'),
            'index' => 'grand_total',
            'type' => 'currency',
            'currency' => 'order_currency_code',
        ));
		$this->addColumn('gateway_status',
                         array(
            'header' => Mage::helper('sales')->__('Gateway Status'),
            'filter' => false,
            'index' => 'status',
			'renderer' => 'netsuiteexport/adminhtml_salesExport_render_shipping',
            'type' => 'options',
            'options' => Mage::getSingleton('cc2p/source_responseCode')->getResponseCode(),
        ));
		$this->addColumn('status',
                         array(
            'header' => Mage::helper('sales')->__('Status'),
            'index' => 'status',
            'type' => 'options',
            'options' => Mage::getSingleton('sales/order_config')->getStatuses(),
        ));
		return parent::_prepareColumns();
	}
	protected function _prepareMassaction()
    {
        $this->setMassactionIdField('increment_id');
        $this->getMassactionBlock()->setFormFieldName('order_id');

        $this->getMassactionBlock()->addItem('query', array(
             'label'    => Mage::helper('cc2p')->__('Query'),
             'url'      => $this->getUrl('*/*/query'),
             'confirm'  => Mage::helper('cc2p')->__('Are you sure?')
        ));
		$this->getMassactionBlock()->addItem('queryInvoice', array(
             'label'    => Mage::helper('cc2p')->__('Query and Invoice'),
             'url'      => $this->getUrl('*/*/queryInvoice'),
             'confirm'  => Mage::helper('cc2p')->__('Are you sure?')
        ));
        return $this;
    }
	public function getGridUrl() {
		return $this->getUrl('*/*/grid',array('_current'=>true));
	}
	public function getRowUrl($row) {		
		return '#';//$this->getUrl('*/*/detail',array('id'=>$row->getIncrementId()));
	}
	
}
