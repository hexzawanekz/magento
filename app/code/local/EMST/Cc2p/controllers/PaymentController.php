<?php
class EMST_Cc2p_PaymentController extends Mage_Core_Controller_Front_Action
{
	protected $_order = null;
	protected $_checkout_session = null;

	public function testAction() {
		//Zend_Debug::dump(Mage::getModel('cc2p/cc2p')->validItem(Mage::getSingleton('checkout/session')->getQuote()));
	}
	public function pointsAction() {
		$memberId =  $this->getRequest()->getParam('memberid');
		$redirectId =  $this->getRequest()->getParam('redirectid');
		$ecid =  $this->getRequest()->getParam('ecid');
		$corptag =  $this->getRequest()->getParam('corptag');
		if(!$memberId || !$redirectId) {
			return;
		}
		Mage::getSingleton('customer/session')
		->setPointMemberId($memberId)
		->setEcId($ecid)
		->setCorptag($corptag)
		->setPointRedirectId($redirectId);
		$this->_redirect('');
	}
    protected function getOrder() {
		if ($this->_order == null)
            $this->_order = Mage::getModel('sales/order')->loadByIncrementId($this->getSession()->getLastRealOrderId());
        return $this->_order;
    }
	public function setOrder(Mage_Sales_Model_Order $order) {
		$this->_order = $order;
	}
	protected function getSession() {
		if(!$this->_checkout_session)
			$this->_checkout_session=Mage::getSingleton('checkout/session');
		return $this->_checkout_session;
	}
	public function sendTransactionAction() {
		try {
			$cc2p = $this->_prepareData();
			if(!$cc2p) {
				return ;
			}
			$order = $this->getOrder();	
			$order->addStatusHistoryComment(Mage::helper('cc2p')->__('Customer was redirected to 2c2p'))->save();
			$this->getResponse()
			->setBody($this->getLayout()
			->createBlock('cc2p/redirect')
			->setModel($cc2p)
			->toHtml());			
		}
		catch(Exception $e) {
			Mage::log($e->getMessage(),null, 'Cc2p.log');
			$this->_getCoreSession()->addError($this->__('There was an error, please try again'));
			$this->getSession()->clear();
			$this->_redirect('');
		}
	}
	public function cc2pAction() {
		try{
            $response = $this->getRequest()->getParam('paymentResponse');
			//echo 'paymentResponse Request:'. $response.'<br/>';
			//exit;
			$response="\r\n".$response;
			$response = trim($response);
		    $cc2p = Mage::getModel('cc2p/cc2p');
			$cc2p	->setType($cc2p::TYPE_PAYMENT)
					->setActionType($cc2p::ACTION_TYPE_RESPONSE)
					->setEncryptResponse($response)
					->decryptData();

			//var_dump($cc2p);
			//exit();
		    $decrypt = $cc2p->getDecryptedRes();
		    $order = Mage::getModel('sales/order')->loadByIncrementId((string)$decrypt->uniqueTransactionCode);
			if(!$order->getId()) {
				$this->_getCoreSession()->addError(Mage::helper('cc2p')->__('There is no order exists'));
				$this->_redirect('');
				return;
			}
		    $cc2p->setOrder($order)
				->setParsedData($cc2p->parseXml($decrypt));
			$msg = '';
		    if((string)$decrypt->respCode == '00' && (string)$decrypt->status == 'A') {
				$normal2c2p->pushDataToPointSystem();
				$result = $cc2p->saveInvoice();
				if(!$result) {
					$msg = Mage::helper('cc2p')->__('Failed to invoice your order');
				}
				
		    }
		    else {
				$cc2p->saveFailReason();
				$msg = Mage::helper('cc2p')->__('Your payment was refused by the gateway');
		    }
		}
		catch(Exception $e) {			
			Mage::log($e->getMessage(),null, 'Cc2p.log');
			$msg = Mage::helper('cc2p')->__('There was an error occur while trying to verify your payment');
		}
		if($msg) {
			$this->_getCoreSession()->addError($msg);
			$this->_redirect('');
		}
		else {
			$this->_redirect('checkout/onepage/success');
		}
	}
	protected function _prepareData() {
		$order = $this->getOrder();	
		$cc2p = Mage::getModel('cc2p/cc2p');
		$cc2p->setOrder($order)
			->setType($cc2p::TYPE_PAYMENT)
			->setActionType($cc2p::ACTION_TYPE_REQUEST);
		$session = $this->getSession();
		if(!$order->getId()||$order->getStatus() == 'complete' || $session->getRedirected() == $order->getId() || !$cc2p->validateRedirect($order)) {
			Mage::getSingleton('checkout/session')->clear();
			$this->_redirect('');
			return ;
		}
		$src_data = $cc2p->getRequestData();			
		$cc2p
			->setDatafile($src_data['sourcefile'])
			->setEncryptfile($src_data['outputfile'])
			->encryptData();
		$session->setRedirected($order->getId());
		return $cc2p;
	}
	public function none3DSPostingAction() {
		try {
			$msg = '';
			$cc2p = $this->_prepareData();
			if(!$cc2p) {
				$msg = Mage::helper('cc2p')->__('Invalid transaction data');
			}
			else {
				$decrypt = $cc2p->none3DSPosting();
				$order = Mage::getModel('sales/order')->loadByIncrementId((string)$decrypt->uniqueTransactionCode);
				if(!$order->getId()) {
					$this->_getCoreSession()->addError(Mage::helper('cc2p')->__('Invalid transaction data'));
					$this->_redirect('');
					return;
				}
				$cc2p->setOrder($order)
					->setParsedData($cc2p->parseXml($decrypt));
				
				if((string)$decrypt->respCode == '00' && (string)$decrypt->status == 'A') {
					$cc2p->pushDataToPointSystem();
					$result = $cc2p->saveInvoice();
					if(!$result) {
						$msg = Mage::helper('cc2p')->__('Failed to invoice your order');
					}
					
				}
				else {
					$cc2p->saveFailReason();
					$msg = Mage::helper('cc2p')->__('Your payment was refused by the gateway');
				}
			}
		}
		catch(Exception $e) {
			Mage::log($e->getMessage(),null, 'Cc2p.log');
			$this->_getCoreSession()->addError($this->__('There was an error, please try again'));
			$this->getSession()->clear();
			$this->_redirect('');
		}
		if($msg) {
			$this->_getCoreSession()->addError($msg);
			$this->_redirect('');
		}
		else {
			$this->_redirect('checkout/onepage/success');
		}
	}
	public function merchantAction() {
		try{
            $response = $this->getRequest()->getParam('paymentResponse');
			$response="\r\n".$response;
			$response = trim($response);
		    $cc2p = Mage::getModel('cc2p/cc2p');
			$cc2p	->setType($cc2p::TYPE_PAYMENT)
					->setActionType($cc2p::ACTION_TYPE_BACKEND_RESPONSE)
					->setEncryptResponse($response)
					->decryptData();
		    $decrypt = $cc2p->getDecryptedRes();
		    $order = Mage::getModel('sales/order')->loadByIncrementId((string)$decrypt->uniqueTransactionCode);
			if(!$order->getId()) {
				return;
			}
		    $cc2p->setOrder($order)
				->setParsedData($cc2p->parseXml($decrypt));			
		    if((string)$decrypt->respCode == '00' && (string)$decrypt->status == 'A') {
				$cc2p->pushDataToPointSystem();
				$result = $cc2p->saveInvoice();
		    }
		    else {
				$cc2p->saveFailReason();
		    }
		}
		catch(Exception $e) {			
			Mage::log($e->getMessage(),null, 'Cc2p.log');
		}
	}
	protected function _getCoreSession() {
		return Mage::getSingleton('core/session');
	}
}
