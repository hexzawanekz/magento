<?php

class EMST_TableRate_Model_Config_Tablerateindo extends Mage_Core_Model_Config_Data
{
    /**
     * Set model after save
     *
     * @return void
     */
    public function _afterSave()
    {
        Mage::getResourceModel('emst_tablerate/carrier_tableRateIndo')->uploadAndImport($this);
    }
}
