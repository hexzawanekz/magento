<?php

class EMST_TableRate_Model_Resource_Carrier_TableRateIndo_Collection extends Mage_Shipping_Model_Resource_Carrier_Tablerate_Collection
{
    protected function _construct()
    {
        // parent::_construct();
        $this->_init('emst_tablerate/carrier_tableRateIndo');

        $this->_shipTable       = $this->getMainTable();
        $this->_countryTable    = $this->getTable('directory/country');
        $this->_regionTable     = $this->getTable('directory/country_region');
    }
}