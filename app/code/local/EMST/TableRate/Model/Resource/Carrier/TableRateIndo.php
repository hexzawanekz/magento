<?php

class EMST_TableRate_Model_Resource_Carrier_TableRateIndo extends EMST_TableRate_Model_Resource_Carrier_TableRate
{
	protected $_code = 'tablerateindo';
	protected $_fileName = 'tableRateIndo';
	protected function _construct()
    {
        $this->_init('emst_tablerate/tablerateindo', 'pk');
    }
}