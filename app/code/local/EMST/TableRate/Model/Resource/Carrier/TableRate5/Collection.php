<?php
/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade TableRate to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_TableRate
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
*/

class EMST_TableRate_Model_Resource_Carrier_TableRate5_Collection extends Mage_Shipping_Model_Resource_Carrier_Tablerate_Collection
{
    protected function _construct()
    {
        // parent::_construct();
        $this->_init('emst_tablerate/carrier_tableRate5');

        $this->_shipTable       = $this->getMainTable();
        $this->_countryTable    = $this->getTable('directory/country');
        $this->_regionTable     = $this->getTable('directory/country_region');
    }
}