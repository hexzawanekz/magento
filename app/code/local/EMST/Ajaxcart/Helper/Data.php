<?php
/**
 * Developed by Acommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.Acommerce.asia/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Ajaxcart to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_Ajaxcart
 * @copyright   Copyright (c) 2012 Acommerce (http://www.Acommerce.asia)
 * @license     http://www.Acommerce.asia/LICENSE-E.txt
*/

class EMST_Ajaxcart_Helper_Data extends Mage_Core_Helper_Abstract
{
	CONST XML_PATH_ENABLE_AJAXCART = 'ajaxcart/config/enable';
	
	public function isEnabled() {
		return Mage::getStoreConfig(self::XML_PATH_ENABLE_AJAXCART);
	}
	public function getPId($id) {
		try{
			$parentIds = Mage::getSingleton('catalog/product_type_configurable')
				->getParentIdsByChild($id);
			if(!empty($parentIds)) {
				$id = $parentIds[0];
			}
		}
		catch(Exception $e) {
			Mage::logException($e);
		}
		return $id;
	}
}