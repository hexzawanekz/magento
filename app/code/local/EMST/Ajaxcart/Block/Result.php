<?php
/**
 * Developed by Acommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.Acommerce.asia/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Ajaxcart to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_Ajaxcart
 * @copyright   Copyright (c) 2012 Acommerce (http://www.Acommerce.asia)
 * @license     http://www.Acommerce.asia/LICENSE-E.txt
*/

class EMST_Ajaxcart_Block_Result extends Mage_Core_Block_Template
{
    public function getProductImage() {
		$img = '';
		try{
			$product = $this->getProduct();
			if (!$product instanceof Varien_Object || !$product->getId()) {
				return '';
			}

			$resize = $this->getResize();
			if (is_null($resize)) {
				$resize = 107;
			}
			$helper = Mage::helper('catalog/image');

			$label = $product->getData('small_image_label');
			if (empty($label)) {
				$label = $product->getName();
			}

			$img = '<img src="' . $helper->init($product, 'small_image')->resize($resize) .
				   '" alt="' . $this->htmlEscape($label) .
				   '" title="' . $this->htmlEscape($label) .
				   '" width="' . $resize .
				   '" height="' . $resize .
				   '" />';
		}
		catch(Exception $e) {
			Mage::logException($e);
		}
        return $img;
    
	}
	public function getProductName() {
		if($this->getProduct()) {
			return $this->getProduct()->getName();
		}
		return;
	}
}