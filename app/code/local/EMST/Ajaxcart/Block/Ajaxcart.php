<?php
/**
 * Developed by Acommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.Acommerce.asia/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Ajaxcart to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_Ajaxcart
 * @copyright   Copyright (c) 2012 Acommerce (http://www.Acommerce.asia)
 * @license     http://www.Acommerce.asia/LICENSE-E.txt
*/

class EMST_Ajaxcart_Block_Ajaxcart extends Mage_Core_Block_Template
{
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }
    
     public function getAjaxcart()     
     { 
        if (!$this->hasData('ajaxcart')) {
            $this->setData('ajaxcart', Mage::registry('ajaxcart'));
        }
        return $this->getData('ajaxcart');
        
    }
}