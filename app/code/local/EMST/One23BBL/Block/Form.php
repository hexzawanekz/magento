<?php
class EMST_One23BBL_Block_Form extends Mage_Payment_Block_Form
{
    protected function _construct() {
		parent::_construct();
        $this->setTemplate('one23BBL/form.phtml');        
    }
	public function getDescription() {
		return $this->getMethod()->getConfigData('description');
	}
}
