<?php
class EMST_One23BBL_PaymentController extends Mage_Core_Controller_Front_Action
{
	protected $_order = null;
	protected $_checkout_session = null;
	protected $_payment_model = null;
	protected $_order_id = null;
	
    protected function getOrder() {
		if ($this->_order == null)
            $this->_order = Mage::getModel('sales/order')->loadByIncrementId($this->getOrderId());
        return $this->_order;
    }
	protected function getOrderId() {
		if(!$this->_order_id) {
			$this->_order_id = $this->getSession()->getLastRealOrderId();
		}
		return $this->_order_id;
	}
	public function setOrder(Mage_Sales_Model_Order $order) {
		$this->_order = $order;
	}
	protected function getSession() {
		if(!$this->_checkout_session)
			$this->_checkout_session=Mage::getSingleton('checkout/session');
		return $this->_checkout_session;
	}
	public function sendTransactionAction() {
		try {
			$order = $this->getOrder();
			$session = $this->getSession();
			$one23bbl = $this->getPaymentModel()->setOrder($order);
			if(!$order->getId() || $order->getStatus() == 'complete' || $session->getRedirected() == $order->getId() || !$one23bbl->validateRedirect($order)) {
				Mage::getSingleton('checkout/session')->clear();
				$this->norouteAction();
				return;
			}
			$src_data = $one23bbl->getRequestData();
			$one23bbl
				->setDatafile($src_data['sourcefile'])
				->setEncryptfile($src_data['outputfile'])
				->encryptData();
			
			$order->addStatusHistoryComment(
			Mage::helper('one23bbl')->__('Customer was redirected to 123-BBL')
			)->save();
			$this->getResponse()
			->setBody($this->getLayout()
			->createBlock('one23bbl/redirect')
			->setModel($one23bbl)
			->toHtml());
			$session->setRedirected($order->getId());
		}
		catch (Mage_Payment_Model_Info_Exception $e) {
			$this->_getCoreSession()->addError($e->getMessage());
			$this->getSession()->clear();
			$this->_redirect('');
		}
		catch(Exception $e) {
			Mage::log($e->getMessage(),null, 'One23BBL.log');
			$this->_getCoreSession()->addError($this->__('There was an error, please try again'));
			$this->getSession()->clear();
			$this->_redirect('');
		}
	}
	public function merchantAction() {
		try{
			$response_code = Mage::getModel('one23bbl/source_responseCode')->getResponseCode();
			$one23bbl = $this->getPaymentModel()->setType('MerchantUrl');
			$decrypt = $this->decryptData($this->getRequest()->getParam('OneTwoThreeRes'));
			$this->_order_id = $decrypt->InvoiceNo;
			$payment = $this->getOrder()->getPayment();
			$channel = $payment->getAdditionalInformation('Channel');
			$agent = $payment->getAdditionalInformation('Bank');
			$hash = $one23bbl->hashData($one23bbl->getMerchantId().trim($decrypt->InvoiceNo).trim($decrypt->RefNo1));
			$msg = '';
			
			if($hash != (string)$decrypt->HashValue) {
				$msg = 'Invalid Hash value';
			}
			elseif(((string)$decrypt->ResponseCode == '000' || (string)$decrypt->ResponseCode == '001')) {
				$this->_redirect('checkout/onepage/success');
			}
			elseif(isset($response_code[$decrypt->ResponseCode])) {
				
				$msg = $response_code[$decrypt->ResponseCode];
			}
			else {
				$msg = 'Unknown error from the gateway system';
			}
		}
		catch(Exception $e) {
			Mage::log($e->getMessage(),null, 'One23BBL.log');
			$msg = 'There was an error occur while trying to verify your payment';
		}
		if($msg) {
			$this->_getCoreSession()->addError($msg);
			$this->_redirect('');
		}
	}
	protected function decryptData($data) {
		$response = "\r\n".$data;
		$response = trim($response);
		$one23bbl = $this->getPaymentModel()
				->setEncryptResponse($response)
				->decryptData();
		$decrypt = Mage::getModel('core/config_base',$one23bbl->getDecryptedRes())->getNode();
		return $decrypt;
	}
	protected function getPaymentModel() {
		if(!$this->_payment_model) {
			$this->_payment_model = Mage::getModel('one23bbl/one23BBL');
		}
		return $this->_payment_model;
	}
	public function apiCallUrlAction() {
		try{
			$response_code = Mage::getModel('one23bbl/source_responseCode')->getResponseCode();
			$one23bbl = $this->getPaymentModel()->setType('APICall');
		    $decrypt = $this->decryptData($this->getRequest()->getParam('OneTwoThreeRes'));
			$this->_order_id = $decrypt->InvoiceNo;
		    $order = $this->getOrder();
			$hash = $one23bbl->hashData($one23bbl->getMerchantId().$decrypt->InvoiceNo.$decrypt->RefNo1);
			$response = $one23bbl->getResponseData();
			if(!$order->getId()) {	
				$response['FailureReason'] = 'Invalid InvoiceNo';
				return;
			}
			$response['MessageID'] = $decrypt->InvoiceNo;
		    $one23bbl->setOrder($order)
				->setParsedData($one23bbl->parseXml($decrypt));
		    if((string)$decrypt->ResponseCode == '000' && (string)$decrypt->HashValue == $hash) {
				$one23bbl->saveInvoice();
		    }
		    else {
				
				$one23bbl->logRequest();
		    }
			$response['Result'] = 'SUCCESS';
		}
		catch (Mage_Payment_Model_Info_Exception $e) {			
			$response['FailureReason'] = $e->getMessage();
			$response['Result'] = 'FAILURE';;
		}
		catch(Exception $e) {			
			Mage::log($e->getMessage(),null, 'One23BBL.log');
			$response['FailureReason'] = 'System exception';
			$response['Result'] = 'FAILURE';
		}
		try {
			$xml = $one23bbl->getSimpleXml($response,'APICallUrlRes');
			$src_data = $one23bbl->writeRequest($xml,array('API_PrintBack','Xml','Encrypt'));
			$one23bbl
				->setDatafile($src_data['sourcefile'])
				->setEncryptfile($src_data['outputfile'])
				->encryptData();
		}
		catch(Exception $e) {
			Mage::log($e->getMessage(),null, 'One23BBL.log');
		}
		echo $one23bbl->getEncryptedReq();
	}
	protected function _getCoreSession() {
		return Mage::getSingleton('core/session');
	}
}
