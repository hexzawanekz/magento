<?php

$installer = $this;
$installer->startSetup();
$installer->getConnection()
	->addColumn($installer->getTable('actiontags/action'), 'checkout_success',
			array(
				'TYPE'      => Varien_Db_Ddl_Table::TYPE_TEXT,
				'NULLABLE'  => true,
				'COMMENT'   => 'Tag scripts on Checkout Success page'
			));

$installer->endSetup();
