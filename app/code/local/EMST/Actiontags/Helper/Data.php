<?php
class EMST_Actiontags_Helper_Data extends Mage_Core_Helper_Data
{
	CONST MAIN_CATEGORY_ATTRIBUTE = 'pet_food_main_category';
    CONST SUB_CATEGORY_ATTRIBUTE = 'pet_food_cateogory';
	protected $_mainCategory = null;
	protected $_subCategory = null;
	protected $_areaConfig = null;
    /**
     * Get template variables
     *
     * @return <array>
     */
    public function getVariables()
    {
        //customer information
        if ($this->_getCuctomerSession()->isLoggedIn()) {
            $customer = $this->_getCuctomerSession()->getCustomer();
            $values = array(
                'customer_id' => $customer->getId(),
                'first_name' => $customer->getFirstname(),
                'last_name' => $customer->getLastname(),
                'customer_email' => $customer->getEmail(),
                'subscribe_status' => (int) $this->_getIsSubscribed($customer)
            );
        }
		if($this->_areaConfig == 'checkout') {
			$salesObj = Mage::helper('checkout')->getQuote();
		}
		else {
			$orderId = Mage::helper('checkout')->getCheckout()->getLastRealOrderId();
			$salesObj = Mage::getModel('sales/order')->loadByIncrementId($orderId);
		}

        if ($salesObj->getId()) {
            $values['transation_id'] = $salesObj->getId();
            $values['order_id'] = $salesObj->getReservedOrderId();
            //$values['emarsys_purchase'] = "ScarabQueue.push(['purchase', {orderId: {$values['transation_id']}, items: [".Mage::helper('emarsys')->getQuoteInfo()."]}]);";

            //$order = Mage::getModel('sales/order')->loadByIncrementId($orderId);
            $values['gross_billing'] = $salesObj->getBaseGrandTotal();
            $utm = $this->_getUtm();
            $values['utm_medium'] = $utm['utm_medium'];

            $items = array();
            if($salesObj instanceof Mage_Sales_Model_Quote) {
				if($salesObj->isVirtual()) {
					$address = $salesObj->getBillingAddress();
				}
				else {
					$address = $salesObj->getShippingAddress();
				}
				$values['tax'] = $address->getBaseTaxAmount();
				$values['shipping_tax'] = $address->getBaseShippingTaxAmount();
				$values['shipping_discount'] = $address->getBaseDiscountAmount();
				$values['shipping'] = $address->getBaseShippingInclTax();
				foreach ($salesObj->getAllVisibleItems() as $item) {
					$stdItem = new stdclass();
					$stdItem->sku = $this->jsQuoteEscape($item->getSku());
					$stdItem->name = $this->jsQuoteEscape($item->getName());

					$product = $item->getProduct();
					$topLevel = null;
					$storeRootCat = Mage::app()->getStore()->getRootCategoryId();
					foreach($product->getCategoryCollection() as $cat) {
						$path = $cat->getPathIds();
						foreach($path as $pathId) {
						    if(Mage::getModel('catalog/category')->load($pathId)) {
							$_cate = Mage::getModel('catalog/category')->load($pathId);
							if($_cate->getLevel() == 2 && $_cate->getName() != 'แบรนด์' && $_cate->getName() != "Brands") {
							    $topLevel = $pathId;
							}
						    }
						    if(!$topLevel) $topLevel = Mage::app()->getStore()->getRootCategoryId();
						}
					}
					if($topLevel) {
                        $category = Mage::getModel('catalog/category');
                        $category->setStoreId(Mage::app()->getStore()->getStoreId());

						$stdItem->category = $this->jsQuoteEscape($category->load($topLevel)->getName());
					}

                    $stdItem->price = (int) $item->getBasePrice();
                    $stdItem->quantity = (int) $item->getQty();

					$items[] = $stdItem;

                    if ($item->getProductType=='configuration'){
                        $kit =  Mage::getModel('catalog/product')->loadByAttribute('sku',$item->getSku());
                        $productId = $kit->getId();
                    } else {
                        $productId = $product->getId();
                    }
                    $accessTradeItems .= '&goods='.$this->jsQuoteEscape($productId).'.'.(int) $item->getQty().'.'.(int) $item->getBasePrice();
				}
//				$values['items'] = Zend_Json::encode($items);
				$values['items'] = json_encode($items, JSON_UNESCAPED_UNICODE);

                $values['accessTradeItems'] = $accessTradeItems;
			}
			else {

				$values['tax'] = $salesObj->getBaseTaxAmount();
				$values['shipping_tax'] = $salesObj->getShippingTaxAmount();
				$values['shipping_discount'] = $salesObj->getShippingDiscountAmount();
				$values['shipping'] = $salesObj->getShippingInclTax();
				/*$discountAmount = 'discount_amount';
				$rowTotal = 'row_total';*/
				foreach ($salesObj->getAllVisibleItems() as $item) {
					$stdItem = new stdclass();
					$stdItem->sku = $this->jsQuoteEscape($item->getSku());
					$stdItem->name = $this->jsQuoteEscape($item->getName());
					/*$stdItem->$discountAmount = $item->getDiscountAmount();
					$stdItem->$rowTotal = $item->getRowTotalInclTax();*/

                    $product = $item->getProduct();
                    $topLevel = null;

		    foreach($product->getCategoryCollection() as $cat) {
			    $path = $cat->getPathIds();
			    foreach($path as $pathId) {
				if(Mage::getModel('catalog/category')->load($pathId)) {
				    $_cate = Mage::getModel('catalog/category')->load($pathId);
				    if($_cate->getLevel() == 2 && $_cate->getName() != 'แบรนด์' && $_cate->getName() != "Brands") {
					$topLevel = $pathId;
				    }
				}
				if(!$topLevel) $topLevel = Mage::app()->getStore()->getRootCategoryId();
			    }
		    }

                    if($topLevel) {
                        $category = Mage::getModel('catalog/category');
                        $category->setStoreId(Mage::app()->getStore()->getStoreId());

                        $stdItem->category = $this->jsQuoteEscape($category->load($topLevel)->getName());
                    }

                    $stdItem->price = (int) $item->getOriginalPrice();
                    $stdItem->quantity = (int) $item->getQtyOrdered();

					$items[] = $stdItem;

                    if ($item->getProductType=='configuration'){
                        $kit =  Mage::getModel('catalog/product')->loadByAttribute('sku',$item->getSku());
                        $productId = $kit->getId();
                    } else {
                        $productId = $product->getId();
                    }
                    $accessTradeItems .= '&goods='.$this->jsQuoteEscape($productId).'.'.(int) $item->getQty().'.'.(int) $item->getBasePrice();
				}
//				$values['items'] = Zend_Json::encode($items);
				$values['items'] = json_encode($items, JSON_UNESCAPED_UNICODE);
                $values['accessTradeItems'] = $accessTradeItems;
			}

			if($salesObj->getPayment()->getMethod()) {
				$values['payment_method'] = $salesObj->getPayment()->getMethodInstance()->getTitle();
			}
        }

        //store_id
        $values['store_id'] = Mage::app()->getStore()->getId();

        //category [removed, use category per item instead]
        //$categoryId = Mage::app()->getStore()->getRootCategoryId();
        $values['category'] = '';//$this->jsQuoteEscape(Mage::getModel('catalog/category')->load($categoryId)->getName());

        //current_date
        $currentDate = Mage::app()->getLocale()->storeDate(null, null);
        $values['current_date'] = $currentDate->toString('Y-m-d', 'php');

        return $values;
    }

    /**
     * Get html for action tag
     *
     * @param <string> $configuration configuration
     * @param <string> $customerEmail customer email
     * @return <string>
     */
    public function getHtmlForAction($configuration, $customerEmail = null)
    {
		$this->_areaConfig = $configuration;
        $variables = $this->getVariables();
        if ($customerEmail) {
            $variables['customer_email'] = $customerEmail;
        }
        $html = Mage::getModel('actiontags/action')->getConfiguration($configuration);
        return $this->filterValues($html, $variables);
    }

    /**
     * Get customer session model
     *
     * @return <Mage_Customer_Model_Session>
     */
    protected function _getCuctomerSession()
    {
        return Mage::getSingleton('customer/session');
    }

    /**
     * Check if customer is subscribed
     *
     * @param Mage_Customer_Model_Customer $customer customer
     *
     * @return bool
     */
    protected function _getIsSubscribed($customer)
    {
        $newsletter = Mage::getModel('newsletter/subscriber')->loadByCustomer($customer);
        return $newsletter->isSubscribed();

    }
	public function filterValues($template, $variables)
    {
        $processor = Mage::getModel('core/email_template_filter')
                ->setUseAbsoluteLinks(true)
                ->setPlainTemplateMode(true)
                ->setStoreId(Mage::app()->getStore()->getId())
                ->setVariables($variables)
        ;
        $template = $processor->filter($template);
        return $template;
    }
	public function getCategoriesForJsTracking($product) {
		$jsCategory = null;
		try {
			if($product->getId()) {
				$mainOptionText = $this->_getMainCatAttSource()->getOptionText($product->getPetFoodMainCategory());
				$subOptionText = $this->_getSubCatAttSource()->getOptionText($product->getPetFoodCateogory());
				$jsCategory = $mainOptionText.'-'.$subOptionText;
			}
		}
		catch(Exception $e) {
			Mage::logException($e);
		}
		return $jsCategory;
	}
	protected function _getMainCatAttSource() {
		if(!$this->_mainCategory) {
			$this->_mainCategory = Mage::getResourceSingleton('catalog/product')
								 ->getAttribute(self::MAIN_CATEGORY_ATTRIBUTE)->getSource();
		}
		return $this->_mainCategory;
	}
	protected function _getSubCatAttSource() {
		if(!$this->_subCategory) {
			$this->_subCategory = Mage::getResourceSingleton('catalog/product')
								->getAttribute(self::SUB_CATEGORY_ATTRIBUTE)->getSource();
		}
		return $this->_subCategory;
	}

    protected function _getUtm(){
        $track = array('utm_source'=>'utmcsr',
            'utm_medium'=>'utmcmd',
            'utm_term'=>'utmctr',
            'utm_content'=>'utmcct',
            'utm_campaign'=>'utmccn');

        $utm = array('utm_source'=>'undefined',
            'utm_medium'=>'undefined',
            'utm_term'=>'undefined',
            'utm_content'=>'undefined',
            'utm_campaign'=>'undefined');
        if(!empty($_COOKIE['__utmz'])){
            $pattern = "/(utmcsr=([^\|]*)[\|]?)|(utmccn=([^\|]*)[\|]?)|(utmcmd=([^\|]*)[\|]?)|(utmctr=([^\|]*)[\|]?)|(utmcct=([^\|]*)[\|]?)/i";
            preg_match_all($pattern, $_COOKIE['__utmz'], $matches);
            if(!empty($matches[0])){
                foreach($matches[0] as $match){
                    $match = trim($match, "|");
                    list($k, $v) = explode("=", $match);
                    $column =  array_search($k,$track);
                    if($column !== false && trim($v)) {
                        $utm[$column] = $v;
                    }
                }
            }
        }
        return $utm;
    }
}
