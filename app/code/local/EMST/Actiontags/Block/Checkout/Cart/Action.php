<?php
class EMST_Actiontags_Block_Checkout_Cart_Action extends Mage_Core_Block_Text
{
    /**
     * Set block content text
     * 
     * @return void
     */
    protected function _construct()
    {
        $html = Mage::helper('actiontags')->getHtmlForAction('cart');
		$this->setText($html);
    }
}
