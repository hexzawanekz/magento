<?php 
class EMST_Actiontags_Block_Adminhtml_System_Actions_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * Set block parameters
     * 
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->_blockGroup = 'actiontags';
        $this->_controller = 'adminhtml_system_actions';
        $this->_updateButton('save', 'label', Mage::helper('actiontags')->__('Save Actions'));
        $this->_removeButton('delete');
        $this->_removeButton('back');
    }

    /**
     * Header text
     * 
     * @return string
     */
    public function getHeaderText()
    {
        return Mage::helper('actiontags')->__('Action Tags');
    }
}
