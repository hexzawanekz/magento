<?php

/** @var Mage_Core_Model_Resource_Setup $this */
$this->startSetup();


/** @var EMST_Actiontags_Model_Mysql4_Action_Collection $actionTagModel */
$actionTagModel = Mage::getModel('actiontags/action')->getCollection();

$actionItem = $actionTagModel->addFieldToFilter('store_id', Mage_Core_Model_App::ADMIN_STORE_ID)->getFirstItem();

$actionItem = Mage::getModel('actiontags/action')->load($actionItem->getId());

$code = <<<HTML
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-T3ZMK9"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-T3ZMK9');</script>
<!-- End Google Tag Manager -->
HTML;
$actionItem->setData('checkout_success', $code);
$actionItem->save();



/////////////////////////////////////////////////////////////////////////////////////////////////



/** @var EMST_Actiontags_Model_Mysql4_Action_Collection $actionTagModel */
$actionTagModel = Mage::getModel('actiontags/action')->getCollection();

$actionItem = $actionTagModel->addFieldToFilter('store_id', Mage::app()->getStore('moxyen')->getId())->getFirstItem();

$actionItem = Mage::getModel('actiontags/action')->load($actionItem->getId());

$code = <<<HTML
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-T3ZMK9"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-T3ZMK9');</script>
<!-- End Google Tag Manager -->
HTML;
$actionItem->setData('checkout_success', $code);
$actionItem->save();



/////////////////////////////////////////////////////////////////////////////////////////////////



/** @var EMST_Actiontags_Model_Mysql4_Action_Collection $actionTagModel */
$actionTagModel = Mage::getModel('actiontags/action')->getCollection();

$actionItem = $actionTagModel->addFieldToFilter('store_id', Mage::app()->getStore('moxyth')->getId())->getFirstItem();

$actionItem = Mage::getModel('actiontags/action')->load($actionItem->getId());

$code = <<<HTML
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-T3ZMK9"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-T3ZMK9');</script>
<!-- End Google Tag Manager -->
HTML;
$actionItem->setData('checkout_success', $code);
$actionItem->save();



$this->endSetup();