<?php

/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade SalesExport to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_One23Cash
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
 */
class EMST_One23cash_Model_Resource_Order_Grid_Collection extends Mage_Sales_Model_Resource_Order_Grid_Collection
{
    
    protected function _construct() {
        parent::_construct();
    }
	protected function _initSelect(){
        parent::_initSelect();
		$this->getSelect()->reset(Zend_Db_Select::COLUMNS);
        $this->getSelect()->columns(array('increment_id','store_id','created_at','customer_id','grand_total','status','order_currency_code'), 'main_table');
		$this->add123CashMethod();
        return $this;
    }
	public function add123CashMethod() {
		$this->addFilterToMap('payment_method', 'payment.method');
        $this->join(
				array('payment'=>'sales/order_payment'),
				'main_table.entity_id = payment.parent_id AND payment.method = "one23cash"',
				array('payment_method'=>'payment.method')
				);
		return $this;
	}
	public function addTimeFilter($duration,$before) {
		$now = Varien_Date::now();
		$first = $duration-$before;
		$this->getSelect()->where("main_table.created_at between DATE_SUB('{$now}', INTERVAL {$duration} SECOND) and 
		DATE_SUB('{$now}', INTERVAL {$first} SECOND)");
		return $this;
	}
	public function addSmsNotifyFailedStatusFilter() {
		$status = EMST_One23cash_Model_One23cash::SMS_STATUS_FAILED;
		$this
		->getSelect()
		->where("transaction.notify_sms={$status} or transaction.notify_sms=null");
		return $this;
	}
	protected function _getAllIdsSelect($limit = null, $offset = null) {
        $idsSelect = clone $this->getSelect();
        $idsSelect->reset(Zend_Db_Select::ORDER);
        $idsSelect->reset(Zend_Db_Select::LIMIT_COUNT);
        $idsSelect->reset(Zend_Db_Select::LIMIT_OFFSET);
        $idsSelect->reset(Zend_Db_Select::COLUMNS);
        $idsSelect->columns('increment_id', 'main_table');
        $idsSelect->limit($limit, $offset);
        return $idsSelect;
    }
    public function addTransactionTracking()
    {
		$joinTable = $this->getTable('one23cash/one23_order_transaction');
		$transaction = 'transaction';
		$this
            ->addFilterToMap('created_at', 'main_table.created_at')
            ->addFilterToMap('order_increment', $transaction . '.order_increment')
            ->addFilterToMap('transaction_id', $transaction . '.transaction_id')
            ->addFilterToMap('payer_email', $transaction . '.payer_email')
            ->addFilterToMap('paid_amount', $transaction . '.paid_amount')
            ->addFilterToMap('agent_code', $transaction . '.agent_code')
            ->addFilterToMap('channel_code', $transaction . '.channel_code')
            ->addFilterToMap('inquery_created_at', $transaction . '.created_at')
            ->addFilterToMap('trans_status', $transaction . '.status')
			->addFilterToMap('sent_sms', $transaction . '.status')
			->addFilterToMap('notify_sms', $transaction . '.status')			
            ->addFilterToMap('status', 'main_table.status')            
            ->addFilterToMap('additional_info', $transaction . '.additional_info')
			;
        $this
		->getSelect()
		->joinLeft(
            array($transaction => $joinTable), "main_table.increment_id = {$transaction}.order_increment",
			array('order_increment','transaction_id','payer_email','paid_amount','agent_code','channel_code','inquery_created_at'=>'created_at','trans_status'=>'status','sent_sms','notify_sms')
        );
        return $this;
    }
	public function addInvoiceChecking() {
		$invoiceAliasName = 'invoice_table';
        $joinTable = $this->getTable('sales/invoice');
		$this->addFilterToMap('invoice_id', $invoiceAliasName . '.entity_id');		
		$this
            ->getSelect()
            ->joinLeft(
                array($invoiceAliasName => $joinTable),
                "main_table.entity_id = {$invoiceAliasName}.order_id",
                array(
                    'invoice_id'=>$invoiceAliasName . '.entity_id',
                    'invoice'=>'IF('.$invoiceAliasName . '.state = '.Mage_Sales_Model_Order_Invoice::STATE_PAID.',0,1)',
                )
            );
        return $this;
	}
}
