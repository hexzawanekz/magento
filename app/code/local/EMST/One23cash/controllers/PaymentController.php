<?php
class EMST_One23cash_PaymentController extends Mage_Core_Controller_Front_Action
{
	protected $_order = null;
	protected $_checkout_session = null;
	protected $_payment_model = null;
	protected $_order_id = null;
	
	public function testAction() {
		$xml = $this->getRequest()->getParam('test');
		if($xml) {
			$one23cash = $this->getPaymentModel()->setType('TEST');
			$src_data = $one23cash->writeRequest($xml);
			$one23cash
				->setDatafile($src_data['sourcefile'])
				->setEncryptfile($src_data['outputfile'])
				->encryptData();
			$this->getResponse()->setBody($one23cash->getEncryptedReq());
		}
		// test sms
		$sms = $this->getRequest()->getParam('sms');
		$result = null;
		if($sms) {
			$result = Mage::getModel('one23cash/one23cash')
			->setData($sms)
			->setCreatedAt(now())
			->sendSms()
			;
		}
		$smsForm = "<form type = 'POST' name='sms' action='".Mage::getUrl()."'>
			<div>".$result."</div>
			<table texalign='left'>
			<tr><td>Channel:</td><td><select name ='sms[channel]' id='channel'>
					<option value='atm'></option>ATM<option value='overthecounter'>OVERTHECOUNTER</option><option value='bankcounter'>BANKCOUNTER</option>
					<option value='ibanking'>IBANKING</option><option value='webpay'>WEBPAY</option>
					</select></td></tr>
			
			<tr><td>Telephone:</td><td><input type='text' name ='sms[telephone]'/></td></tr>
			<tr><td>Order Increment(Ref2):</td><td><input type='text' name ='sms[increment]'/></td></tr>
			<tr><td>Order Amount:</td><td><input type='text' name ='sms[amount]'/></td></tr>
			<tr><td>Transaction ID(Ref1):</td><td><input type='text' name ='sms[ref1]'/></td></tr>
			<tr><td colspan='2' align='center'>Created At: ".now()."</td></tr>
			<tr><td colspan='2' align='center'><button type='submit' value='submit'>Submit</button></td></tr>
			</table></form><script type = 'text/javascript'>alert(window.location);alert(document.baseURI);</script>
			";
		$this->getResponse()->setBody($smsForm);
	}
	public function inquiryAction() {
		$noOrderMsg = "Order Id is not specified.<br/>";
		$data = "";
		if ($ids = $this->getRequest()->getParam('order_id')) {
            try {
                $ids = array_unique(explode(',',trim($ids)));
                if(sizeof($ids)<1) {
					$data .= $noOrderMsg;
				}
				$result = Mage::helper('one23cash')->rawInquiry($ids);
				$success = isset($result[0])?$result[0]:0;
				if($success>0) {
					$data .= "{$success} order(s) have been queried.<br/>";
				}				
				if(isset($result[2]) && is_array($result[2]) && !empty($result[2])) {
					foreach($result[2] as $k=>$message) {
						$data .= "{$k}: {$message}<br/>";
					}
				}
				if(isset($result[1])) {
					$data .= $result[1];
				}
            } catch (Exception $e){
                $data .= $e->getMessage().'<br/>';
            }
        } else {
            $data .= $noOrderMsg;
        }
		$this->getResponse()->setBody($data);
	}
    protected function getOrder() {
		if ($this->_order == null)
            $this->_order = Mage::getModel('sales/order')->loadByIncrementId($this->getOrderId());
        return $this->_order;
    }
	protected function getOrderId() {
		if(!$this->_order_id) {
			$this->_order_id = $this->getSession()->getLastRealOrderId();
		}
		return $this->_order_id;
	}
	public function setOrder(Mage_Sales_Model_Order $order) {
		$this->_order = $order;
	}
	protected function getSession() {
		if(!$this->_checkout_session)
			$this->_checkout_session=Mage::getSingleton('checkout/session');
		return $this->_checkout_session;
	}
	public function sendTransactionAction() {
		try {
			$order = $this->getOrder();
			$session = $this->getSession();
			$one23cash = $this->getPaymentModel()->setOrder($order);			
			if(!$order->getId() || $order->getStatus() == 'complete' || $session->getRedirected() == $order->getId() || !$one23cash->validateRedirect($order)) {
				Mage::getSingleton('checkout/session')->clear();
				$this->norouteAction();
				return;
			}
			$src_data = $one23cash->getRequestData();
			$one23cash
				->setDatafile($src_data['sourcefile'])
				->setEncryptfile($src_data['outputfile'])
				->encryptData();
			
			$order->addStatusHistoryComment(
			Mage::helper('one23cash')->__('Customer was redirected to one23cash')
			)->save();
			$this->getResponse()
			->setBody($this->getLayout()
			->createBlock('one23cash/redirect')
			->setModel($one23cash)
			->toHtml());
			$session->setRedirected($order->getId());
		}
		catch (Mage_Payment_Model_Info_Exception $e) {
			$this->_getCoreSession()->addError($e->getMessage());
			$this->getSession()->clear();
			$this->_redirect('');
		}
		catch(Exception $e) {
			Mage::log($e->getMessage(),null, 'One23cash.log');
			$this->_getCoreSession()->addError($this->__('There was an error, please try again'));
			$this->getSession()->clear();
			$this->_redirect('');
		}
	}
	public function merchantAction() {
		try{
			$response_code = Mage::getModel('one23cash/source_responseCode')->getResponseCode();
			$one23cash = $this->getPaymentModel()->setType('MerchantUrl');
			$decrypt = $this->decryptData($this->getRequest()->getParam('OneTwoThreeRes'));
			$msg = '';
			$this->_order_id = $decrypt->InvoiceNo;
			if($this->getOrder()->getId()) {
				$payment = $this->getOrder()->getPayment();
				$channel = $payment->getAdditionalInformation('Channel');
				$agent = $payment->getAdditionalInformation('Bank');
				$hash = $one23cash->hashData($one23cash->getMerchantId().trim($decrypt->InvoiceNo).trim($decrypt->RefNo1));
				
				
				if($hash != (string)$decrypt->HashValue) {
					$msg = 'Invalid Hash value';
				}
				elseif(((string)$decrypt->ResponseCode == '000' || (string)$decrypt->ResponseCode == '001')) {
					$this->_redirect('checkout/onepage/success');
					$this->_redirect('checkout/onepage/success');	
						$this->getOrder()->addStatusHistoryComment(
							Mage::helper('one23cash')->__('Customer returned from 123-Cash')
							)->save();					
						return;
				}
				elseif(isset($response_code[$decrypt->ResponseCode])) {
					
					$msg = $response_code[$decrypt->ResponseCode];
				}
				else {
					$msg = 'Unknown error from the gateway system';
				}
			}
		}
		catch(Exception $e) {
			Mage::log($e->getMessage(),null, 'One23cash.log');
			$msg = 'There was an error occur while trying to verify your payment';
		}
		if($msg) {
			$this->_getCoreSession()->addError($msg);
			
		}
		$this->_redirect('');
	}
	protected function decryptData($data) {
		$response = "\r\n".$data;
		$response = trim($response);
		$one23cash = $this->getPaymentModel()
				->setEncryptResponse($response)
				->decryptData();
		$decrypt = Mage::getModel('core/config_base',$one23cash->getDecryptedRes())->getNode();
		return $decrypt;
	}
	protected function getPaymentModel() {
		if(!$this->_payment_model) {
			$this->_payment_model = Mage::getModel('one23cash/one23cash');
		}
		return $this->_payment_model;
	}
	public function apiCallUrlAction() {
		try{
			$response_code = Mage::getModel('one23cash/source_responseCode')->getResponseCode();
			$one23cash = $this->getPaymentModel()->setType('APICall');
		    $decrypt = $this->decryptData($this->getRequest()->getParam('OneTwoThreeRes'));
			$this->_order_id = $decrypt->InvoiceNo;
		    $order = $this->getOrder();
			$hash = $one23cash->hashData($one23cash->getMerchantId().$decrypt->InvoiceNo.$decrypt->RefNo1);
			$response = $one23cash->getResponseData();
			if(!$order->getId()) {	
				$response['FailureReason'] = 'Invalid InvoiceNo';
				return;
			}
			$response['MessageID'] = $decrypt->InvoiceNo;
		    $one23cash->setOrder($order)
				->setParsedData($one23cash->parseXml($decrypt));
		    if((string)$decrypt->ResponseCode == '000' && (string)$decrypt->HashValue == $hash) {
				$one23cash->saveInvoice();
		    }
		    else {
				
				$one23cash->logRequest();
		    }
			$transactionModel = Mage::getModel('one23cash/transaction')->load($order->getIncrementId());
			if(!$transactionModel->getId() || !$transactionModel->getSentSms()) {
				$one23cash->setSmsType($one23cash::SMS_TYPE_NOTIFY)
					->setSmsEvent($one23cash::EVENT_NOTIFY)
					->sendSms();
			}
			$response['Result'] = 'SUCCESS';
		}
		catch (Mage_Payment_Model_Info_Exception $e) {			
			$response['FailureReason'] = $e->getMessage();
			$response['Result'] = 'FAILURE';;
		}
		catch(Exception $e) {			
			Mage::log($e->getMessage(),null, 'One23cash.log');
			$response['FailureReason'] = 'System exception';
			$response['Result'] = 'FAILURE';
		}
		try {
			$xml = $one23cash->getSimpleXml($response,'APICallUrlRes');
			$src_data = $one23cash->writeRequest($xml,array('API_PrintBack','Xml','Encrypt'));
			$one23cash
				->setDatafile($src_data['sourcefile'])
				->setEncryptfile($src_data['outputfile'])
				->encryptData();
		}
		catch(Exception $e) {
			Mage::log($e->getMessage(),null, 'One23cash.log');
		}
		echo $one23cash->getEncryptedReq();
	}
	protected function _getCoreSession() {
		return Mage::getSingleton('core/session');
	}
}
