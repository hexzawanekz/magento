<?php


class EMST_One23cash_Block_Redirect extends Mage_Core_Block_Abstract
{
	protected function _toHtml()
	{
		$one23cash = $this->getModel();
        $form = new Varien_Data_Form();
        $form->setAction($one23cash->getGatewayUrl())
            ->setId('one23cash_payment_checkout')
            ->setName('one23cash_payment_checkout')
            ->setMethod('POST')
            ->setUseContainer(true);
        foreach ($one23cash->getStandardCheckoutFormFields() as $field => $value) {
            $form->addField($field, 'hidden', array('name' => $field, 'value' => $value));
        }

        $formHTML = $form->toHtml();

        $html = '<html>';
		$html .= '<head><meta content="text/html; charset=utf-8" http-equiv="Content-Type"></head>';
		$html .= '<body>';
        $html.= $this->__('You will be redirected to OneTwoThree Gateway in a few seconds.');
        $html.= $formHTML;
        $html.= '<script type="text/javascript">document.getElementById("one23cash_payment_checkout").submit();</script>';
        $html.= '</body></html>';


        return $html;
    }

}
