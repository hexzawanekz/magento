<?php
class EMST_Normal2c2p_Model_Source_Banks
{
	public function toOptionArray() {
		$_banks = array();
		foreach(Mage::getModel('normal2c2p/normal2c2p')->getBanks() as $code => $name) {
			$_banks[]  = array('value'=>$code,'label'=>$name);
		}
		return $_banks;
    }
}
