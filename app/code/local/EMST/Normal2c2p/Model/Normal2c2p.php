<?php


class EMST_Normal2c2p_Model_Normal2c2p extends Mage_Payment_Model_Method_Abstract
{

    protected $_code = 'normal2c2p';
    protected $_formBlockType = 'normal2c2p/form';
    protected $_infoBlockType = 'normal2c2p/info';
    protected $_isInitializeNeeded      = true;
    protected $_canUseInternal          = true;
    protected $_canUseForMultishipping  = false;

    const PROCESS_TYPE_INQUIRY = 'I';
    const PROCESS_TYPE_RECURRING = 'CR';

    const ACTION_TYPE_REQUEST = 'Request';
    const ACTION_TYPE_RESPONSE = 'Response';
    const ACTION_TYPE_BACKEND_RESPONSE = 'Response-Backend';

    const TYPE_PAYMENT = 'Payment';
    const TYPE_INQUIRY = 'Inquiry';

    const FIELD_NAME = 'n_2c2p_';

    const PAY_CHANNEL = 'Payment Channel';
    const PAY_STATUS = 'Payment Status';
    const PAY_RESPONSE_CODE = 'Channel Response Code';
    const PAY_RESPONSE_DESC = 'Channel Response Description';
    const CASH_LOG_FILE = 'Normal2c2p_backend.log';

    public function getNamePrefix()
    {
        return self::FIELD_NAME;
    }

    public function getOrderPlaceRedirectUrl()
    {
        return Mage::getUrl('normal2c2p/payment/redirect');
    }

    public function getVersion()
    {
        return $this->getConfigData('version');
    }

    public function getMerchantId()
    {
        return $this->getConfigData('merchant_id');
    }

    public function getCurrencyCode()
    {
        $code = $this->getConfigData('currencyCode');
        if (!$code) {
            $code = Mage::app()->getBaseCurrencyCode();
            $currencies = Mage::getModel('normal2c2p/source_currencyCode')->getCurrencyCode();
            $code = $currencies[$code];
        }

        return $code;
    }

    public function getPayCategoryID()
    {
        return $this->getConfigData('payCategoryID');
    }

    public function getPromotion(){
        $promotion = null;

        $prefix = $this->getNamePrefix();
        $input_info = Mage::getSingleton('checkout/session')->getData($prefix . 'cc2p');
        $period = isset($input_info[$prefix . 'period']) ? $this->getPeriod($input_info[$prefix . 'period'], true) : false;
        if (isset($input_info[$prefix . 'period']) && $input_info[$prefix . 'period'] && $period !== false) {
            $promotion = $period['promo'];
        }
        return $promotion;
    }

    public function getPaymentDescription(){
        $detail = 'Refer to Confirmation Order for order details.';
        return $detail;
    }
    public function getInvoiceNo(){
        return '';
    }
    public function getAmount($amount)
    {
        return str_pad($amount * 100, 12, '0', STR_PAD_LEFT);
    }

    public function getPaymentExpiryTime()
    {
        $currentTime = Mage::getModel('core/date')->timestamp(time());
        $expiryTime = Mage::getStoreConfig(EMST_Normal2c2p_Model_PaymentChannel::XML_PAYMENT_EXPIRY_TIME) * 60 * 60;
        return date('Y-m-d H:i:s', $currentTime + $expiryTime);
    }

    public function getHash($pay_channel = ''){
        /** @var Mage_Sales_Model_Order $order */
        $order = $this->getOrder();
        $version = $this->getVersion();
        $merchant_id = $this->getMerchantId();
        $order_id = $order->getIncrementId();
        $payment_description = $this->getPaymentDescription();
        $invoice_no = $order->getId();
        $currency = $this->getCurrencyCode();

        if ($pay_channel == EMST_Normal2c2p_Model_PaymentChannel::CASH_PAYMENT_CHANNEL) {
            $payment_channel = '1';
        } else {
            $payment_channel = 'A';
        }

        $amount = $this->getAmount($order->getBaseGrandTotal());
        $customer_email = Mage::getSingleton('customer/session')->getCustomer()->getEmail();
        $pay_category_id = $this->getPayCategoryID();
        $promotion = $this->getPromotion();
        $user_defined_1 = $this->getUserDefined(1);
        $user_defined_2 = $this->getUserDefined(2);
        $user_defined_3 = $this->getUserDefined(3);
        $user_defined_4 = $this->getUserDefined(4);
        $user_defined_5 = $this->getUserDefined(5);
        $result_url_1 ='';
        $result_url_2 ='';

        $recurring = '';
        $order_prefix = '';
        $recurring_amount = '';
        $allow_accumulate = '';
        $max_accumulate_amount = '';
        $recurring_count = '';
        $charge_next_date = '';
        $charge_on_date = '';

        $autoship = Mage::helper('recurringandrentalpayments')->getSubscriptionPaymentData($order, $this);

        if ($autoship) {
            $recurring = $autoship['recurring'];
            $order_prefix = $autoship['order_prefix'];
            $recurring_amount = $autoship['recurring_amount'];
            $allow_accumulate = $autoship['allow_accumulate'];
            $max_accumulate_amount = $autoship['max_accumulate_amount'];
            $recurring_count = $autoship['recurring_count'];
            $charge_next_date = $autoship['charge_next_date'];
            $charge_on_date = $autoship['charge_on_date'];
        }

        if ($pay_channel == EMST_Normal2c2p_Model_PaymentChannel::CASH_PAYMENT_CHANNEL) {
            $payment_channel = EMST_Normal2c2p_Model_PaymentChannel::PAY_OP_ONE_TWO_THREE;
        } elseif ($pay_channel == EMST_Normal2c2p_Model_PaymentChannel::CREDIT_DEBIT_CARDS) {
            $payment_channel = EMST_Normal2c2p_Model_PaymentChannel::PAY_OP_CREDIT_CARD;
        } 
        // elseif ($pay_channel == EMST_Normal2c2p_Model_PaymentChannel::SAMSUNG_PAY) {
        //     $payment_channel = EMST_Normal2c2p_Model_PaymentChannel::PAY_OP_SAMSUNG_PAY;
        // } 
        else {
            $payment_channel = EMST_Normal2c2p_Model_PaymentChannel::PAY_OP_ALL;
        }
        $payment_expiry = $this->getPaymentExpiryTime();
        $secret_key = $this->getSecretKey();

        if($autoship && $pay_channel != EMST_Normal2c2p_Model_PaymentChannel::CASH_PAYMENT_CHANNEL){
            $strSignatureString =$version . $merchant_id . $payment_description . $order_id
                . $invoice_no . $currency . $amount . $customer_email . $pay_category_id .
                $promotion . $user_defined_1 . $user_defined_2 . $user_defined_3 .
                $user_defined_4 . $user_defined_5 . $result_url_1 . $result_url_2.
                $recurring . $order_prefix . $recurring_amount . $allow_accumulate .
                $max_accumulate_amount . $recurring_count . $charge_next_date . $charge_on_date .
                $payment_channel . $payment_expiry;
        }else{
            $strSignatureString = $version . $merchant_id . $payment_description . $order_id
                . $invoice_no . $currency . $amount . $customer_email . $pay_category_id .
                $promotion . $user_defined_1 . $user_defined_2 . $user_defined_3 .
                $user_defined_4 . $user_defined_5 . $result_url_1 . $result_url_2. $payment_channel . $payment_expiry;
        }

        $HashValue = hash_hmac('sha1', $strSignatureString, $secret_key, false);
        return $HashValue;
    }

    public function getUserDefined($num)
    {
        return $this->getConfigData('userDefined' . $num);
    }
    public function getSecretKey()
    {
        return $this->getConfigData('secret_key');
    }

    public function getGatewayRedirectUrl()
    {
        return $this->getConfigData('redirect');
    }

    public function saveInvoice($payment_status = null)
    {
        try {
            /** @var Mage_Sales_Model_Order $order */
            $order = $this->getOrder();
            $parsed = $this->getParsedData();
            $payment = $this->importPaymentInfo($parsed);
            if($payment_status == EMST_Normal2c2p_Model_PaymentChannel::PAY_STATUS_PENDING){

                $payment->save();
                if (!$order->getIsWholesale()) {
                    $order->sendNewOrderEmail()->addStatusHistoryComment(Mage::helper('normal2c2p')->__('Notified customer about their new order.'))
                        ->setIsCustomerNotified(true)
                        ->save();
                }

            } else {

                if ($order->canInvoice()) {
                    $payment->setCcTransId($parsed['transaction_ref'])
                        ->setCcApproval($parsed['payment_status'])
                        ->setTransactionId($parsed['transaction_ref'])
                        ->registerCaptureNotification($parsed['amount']);

                    $order->save();
                }
                $invoice = $payment->getCreatedInvoice();
                $payment->save();
                if ($invoice && !$order->getEmailSent()) {

                    $order->sendNewOrderEmail()->addStatusHistoryComment(Mage::helper('normal2c2p')->__('Notified customer about invoice #%s.', $invoice->getIncrementId()))
                        ->setIsCustomerNotified(true)
                        ->save();
                }

            }
            return true;
        } catch (Exception $e) {
            Mage::log($e->getMessage(), null, 'Normal2c2p.log');
        }
        return false;
    }

    public function parseXml($xml)
    {
        $return = array();
        foreach ($xml as $k => $v) {
            if ($k == 'amount') {
                $v = $v / 100;
            }
            if ($k == 'amt') {
                $v = $v / 100;
            }
            $return[$k] = trim((string)$v);
        }
        return $return;
    }

    public function pushDataToPointSystem()
    {
        $order = $this->getOrder();

        $cusSess = Mage::getSingleton('customer/session');
        if (!$this->getParsedData() || !$this->getAeonConfig('enable') || !$this->getAeonConfig('url') || !$order || !$order->getId() || !$cusSess->getPointMemberId() || !$cusSess->getPointRedirectId()) {
            return;
        }
        try {
            $soapAction = null;
            $userAgent = 'PHP WebService Client';
            $auth = "{$this->getAeonConfig('username')}:{$this->getAeonConfig('password')}";
            $soapData = $this->getSoapData();
            $urlObj = Mage::getModel('core/url')->parseUrl($this->getAeonConfig('url'));
            $sendData = '';
            switch ($this->getAeonConfig('method')) {
                case "GET":
                    break;

                case "POST":
                    break;

                case "SOAP":
                    $dom = new DOMDocument("1.0", 'UTF-8');
                    $dom->loadXML($soapData);
                    $dom->formatOutput = true;
                    $soap = $dom->saveXml();

                    $sendData = "POST " . $urlObj->getPath() . " HTTP/1.0\r\n";
                    $sendData .= "Authorization: Basic " . base64_encode($auth) . "\r\n";
                    $sendData .= "Host: " . $urlObj->getHost() . "\r\n";
                    $sendData .= "Content-Type: text/xml; charset=UTF-8\r\n";
                    $sendData .= "Content-Length: " . strlen($soap) . "\r\n";
                    $sendData .= "SOAPAction: $soapAction\r\n";
                    $sendData .= "User-Agent: " . $userAgent . "\r\n";
                    $sendData .= "\r\n";
                    $sendData .= $soap;
                    break;
                default:
                    throw new Exception("Invalid Method: " . $this->getAeonConfig('method'));
            }
            $port = $urlObj->getPort();
            $scheme = '';
            switch ($urlObj->getScheme()) {
                case 'https':
                    if (!function_exists('openssl_verify'))
                        throw new Exception("Please remove the comment in the line ';extension=php_openssl.dll' in Apache/bin/php.ini and restart the server!");

                    $scheme = 'ssl://';
                    $port = ($port === null) ? 443 : $port;
                    break;

                case 'http':
                    $scheme = '';
                    $port = ($port === null) ? 80 : $port;
                    break;

                default:
                    throw new Exception("Invalid protocol in: " . $urlObj->getPath());
            }

            $socket = @fsockopen($scheme . $urlObj->getHost(), $port, $errNo, $errStr, 30);

            if (!$socket)
                throw new Exception ("Unable to establish connection to host " . $urlObj->getHost() . " $errStr");


            fwrite($socket, $sendData);

            $Response = "";
            while (!feof($socket)) {
                // Read blocks of 1000 Bytes
                $Response .= fgets($socket, 1000);
            }

            fclose($socket);

            // Between Header and ResponseBody there are two empty lines

            list($header, $responseBody) = explode("\r\n\r\n", $Response, 2);
            $split = preg_split("/\r\n|\n|\r/", $header);

            /* // Decode the first line of the header: "HTTP/1.1 200 OK"
            list($protocol, $statusCode, $statusText) = explode(' ', trim(array_shift($split)), 3); */
            list($protocol, $statusCode, $statusText) = explode(' ', trim(array_shift($split)), 3);
            $storedData = array(
                'Import To AEON Point Stystem' => 'Failed',
                'Reason' => $statusCode . ':' . $statusText,
            );
            if (trim($responseBody)) {
                $xml = simplexml_load_string($responseBody);
                $xml->registerXPathNamespace('resns', $this->getAeonConfig('point_xmlns'));
                $resCode = $xml->xpath('//resns:ResponseCode[1]');
                if (!$resCode || empty($resCode)) {
                    return;
                }
                $resCode = (string)$resCode[0];
                $storedData['Import To AEON Point Stystem'] = ($resCode == '00000') ? 'Success' : 'Failed';
                $resDes = $xml->xpath('//resns:ResponseDescription[1]');
                $storedData['Reason'] = (string)$resDes[0];
            }
            $payment = $this->getOrder()->getPayment();
            $was = $payment->getAdditionalInformation();
            $new = array_merge($was, $storedData);
            $payment->setAdditionalInformation($new)->save();
        } catch (Exception $e) {
            Mage::log($e->getMessage(), null, 'Normal2c2p-AEON.log');
        }

    }

    /**
     * @param $parsed
     * @return Mage_Sales_Model_Order_Payment
     */
    protected function importPaymentInfo($parsed)
    {
        $payment = $this->getOrder()->getPayment();
        $was = $payment->getAdditionalInformation();
        $new = array_merge($was, $this->getSuccessInfo($parsed));
        //$new = $this->getSuccessInfo($parsed);
        $payment->setAdditionalInformation($new);
        return $payment;
    }

    protected function getSuccessInfo($parsed)
    {
        $additionalInfo = array();

        if($value = $parsed['transaction_ref']){
            $additionalInfo['Transaction Ref No.'] = $value;
        }
        if($value = $parsed['payment_channel']){
            $additionalInfo[$this::PAY_CHANNEL] = $value;
        }
        if($value = $parsed['payment_status']){
            $additionalInfo[$this::PAY_STATUS] = $this->getPaymentChannel()->getPaymentStatusByText($value);
        }
        if($value = $parsed['masked_pan']){
            $additionalInfo['Masked Credit/Debit Card Number'] = $value;
        }
        if($value = $parsed['approval_code']){
            $additionalInfo['Approval Code'] = $value;
        }
        if($value = (string)$parsed['eci']){
            $additionalInfo['ECI'] = $value;
        }
        if($value = $parsed['channel_response_code']){
            $additionalInfo[$this::PAY_RESPONSE_CODE] = $value;
        }
        if($value = $parsed['channel_response_desc']){
            $additionalInfo[$this::PAY_RESPONSE_DESC] = $value;
        }elseif($code = $parsed['channel_response_code']){
            $additionalInfo[$this::PAY_RESPONSE_DESC] = $this->getPaymentChannel()->getCashPaymentDesc($code);
        }
        if($parsed['payment_channel'] == EMST_Normal2c2p_Model_PaymentChannel::CASH_PAYMENT_CHANNEL){
            if($value = $parsed['paid_channel']){
                $additionalInfo['Paid Channel'] = $this->getPaymentChannel()->getPaidChannelByText($parsed['payment_channel'], $value);
            }
            if($value = $parsed['paid_agent']){
                $additionalInfo['Paid Agent'] = $this->getPaymentChannel()->getCashAgentChannel($parsed['payment_channel'], $value);
            }
        }elseif($parsed['payment_channel'] == EMST_Normal2c2p_Model_PaymentChannel::IPP_TRANSACTION){
            if($value = $parsed['paid_agent']){
                $additionalInfo['Bank'] = $this->getPaymentChannel()->getBank($value);
            }
        }

        return $additionalInfo;
    }

    protected function getFailInfo($parsed)
    {
        $data = $this->getSuccessInfo();
        $currencies = Mage::getModel('normal2c2p/source_responseCode')->getResponseCode();
        $data['response_code'] = $currencies[$parsed['pan']];
        $data['amount'] = $parsed['amt'];
        $data['invoice_no'] = $parsed['uniqueTransactionCode'];
        return $data;
    }

    public function saveFailReason()
    {
        $this->importPaymentInfo($this->getParsedData())->save();
        // $order->save();
    }

    /**
     * @return EMST_Normal2c2p_Model_PaymentChannel
     */
    public function getPaymentChannel()
    {
        return Mage::getSingleton('normal2c2p/paymentChannel');
    }

    public function saveOrderForCashPayment(Mage_Sales_Model_Order $order, $payment_status, $channel_code, $channel_desc)
    {
        if($channel_desc == ''){
            $channel_desc = $this->getPaymentChannel()->getCashPaymentDesc($channel_code);
        }
        // check cash payment status
        if ($payment_status != EMST_Normal2c2p_Model_PaymentChannel::PAY_STATUS_PENDING) {
            // save additional info
            if ($this->_saveAdditionalInfo($order, $payment_status, $channel_code, $channel_desc)) {

                // save order status
                // send notice mail to customer
                try{
                    $status = $payment_status == EMST_Normal2c2p_Model_PaymentChannel::PAY_STATUS_ERROR ? false : true;
                    if($payment_status == EMST_Normal2c2p_Model_PaymentChannel::PAY_STATUS_SUCCESS){
                        $state = EMST_Sales_Model_Order::STATE_INVOICED;
                    }elseif($payment_status == EMST_Normal2c2p_Model_PaymentChannel::PAY_STATUS_REJECTED || $payment_status == EMST_Normal2c2p_Model_PaymentChannel::PAY_STATUS_USER_CANCEL){
                        $state = EMST_Sales_Model_Order::STATE_CANCELED;
                        // cancel order
                        if($order->canCancel()){
                            $order->cancel();
                        }
                    }else if($payment_status == EMST_Normal2c2p_Model_PaymentChannel::PAY_STATUS_EXPIRED){
                        $state = EMST_Sales_Model_Order::STATE_CANCELED;
                        if($order->canCancel()){
                            $order->cancel();
                        }
                    }else{
                        $state = EMST_Sales_Model_Order::STATE_INVALID;
                        // cancel order
                        if($order->canCancel()){
                            $order->cancel();
                        }
                    }
                    $order->setState($state, $status, $channel_desc, true);
                    $order->save();
                } catch (Exception $e){
                    Mage::log("Fails when saving order #{$order->getIncrementId()}'s state. See exception message below.",null,$this::CASH_LOG_FILE);
                    Mage::log($e->getMessage(),null,$this::CASH_LOG_FILE);
                }
            }
        }
    }

    public function _saveAdditionalInfo(Mage_Sales_Model_Order $order, $payment_status, $channel_code, $channel_desc)
    {
        try{
            $payment = $order->getPayment();
            $info = $payment->getAdditionalInformation();
            $info[EMST_Normal2c2p_Model_Normal2c2p::PAY_STATUS] = $this->getPaymentChannel()->getPaymentStatusByText($payment_status);
            $info[EMST_Normal2c2p_Model_Normal2c2p::PAY_RESPONSE_CODE] = $channel_code;
            $info[EMST_Normal2c2p_Model_Normal2c2p::PAY_RESPONSE_DESC] = $channel_desc;
            // save
            $payment->setAdditionalInformation($info);
            $payment->save();
            return true;
        } catch (Exception $e){
            Mage::log("Fails when saving order #{$order->getIncrementId()}'s additional info. See exception message below.",null,$this::CASH_LOG_FILE);
            Mage::log($e->getMessage(),null,$this::CASH_LOG_FILE);
            return false;
        }
    }
}
