<?php

/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade SalesExport to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_Normal2c2p
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
 */
class EMST_Normal2c2p_Model_Resource_Order_Grid_Collection extends Mage_Sales_Model_Resource_Order_Grid_Collection
{

	protected function _initSelect(){
        parent::_initSelect();
		$this->getSelect()->reset(Zend_Db_Select::COLUMNS);
        $this->getSelect()->columns(array('increment_id','store_id','created_at','customer_id','grand_total','status','order_currency_code'), 'main_table');
		$this->add2C2PMethod();
        return $this;
    }
	public function add2C2PMethod() {
		$this->addFilterToMap('payment_method', 'payment.method');
        $this->join(
				array('payment'=>'sales/order_payment'),
				'main_table.entity_id = payment.parent_id AND payment.method in ("cc2p", "normal2c2p")',
				array('payment_method'=>'payment.method','additional_information')
				);
		return $this;
	}
	protected function _getAllIdsSelect($limit = null, $offset = null) {
        $idsSelect = clone $this->getSelect();
        $idsSelect->reset(Zend_Db_Select::ORDER);
        $idsSelect->reset(Zend_Db_Select::LIMIT_COUNT);
        $idsSelect->reset(Zend_Db_Select::LIMIT_OFFSET);
        $idsSelect->reset(Zend_Db_Select::COLUMNS);
        $idsSelect->columns('increment_id', 'main_table');
        $idsSelect->limit($limit, $offset);
        return $idsSelect;
    }
}