<?php
/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade SalesExport to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_Sales
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
*/

class EMST_Normal2c2p_Adminhtml_Normal2c2pTransactionsController extends Mage_Adminhtml_Controller_Action
{
	protected function _initAction() {
		$this->loadLayout()
			->_setActiveMenu('sales/normal2c2p')
			->_addBreadcrumb(Mage::helper('adminhtml')->__('Transactions Tracking'), Mage::helper('adminhtml')->__('Transactions Tracking'));
		
		return $this;
	}   
	public function testAction() {
		
	}
	public function indexAction() {
		$this->_initAction()
			->_addContent($this->getLayout()->createBlock('normal2c2p/adminhtml_transaction','emst_normal2c2p_transactions'))
			->renderLayout();
	}

	public function gridAction() {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('normal2c2p/adminhtml_transaction_grid')->toHtml()
        );
    }
	public function queryInvoiceAction() {
		$this->_query(true);        
	}
	public function queryAction() {		
		$this->_query();       
	}
	protected function _query($invoice = false) {
		if ($ids = $this->getRequest()->getParam('order_id')) {
            try {
                
                if(!is_array($ids)) {
					$ids = array($ids);
				}
				$result = Mage::helper('normal2c2p')->transactionInquiry($ids,$invoice);
				$number = isset($result[0])?$result[0]:array(0,0);
				$query = 0;
				$inv = 0;
				if(is_array($number)) {
					$query = isset($number[1])?$number[1]:0;
					$inv = isset($number[1])?$number[1]:0;
				}
				if($query>0) {
					$this->_getSession()->addSuccess(Mage::helper('adminhtml')->__('%s order(s) have been queried.',$query));
				}
				if($invoice  && $inv>0) {
					$this->_getSession()->addSuccess(Mage::helper('adminhtml')->__('%s order(s) have been invoiced.',$inv));
				}
				if(isset($result[1]) && is_array($result[1]) && !empty($result[1])) {
					foreach($result[1] as $k=>$message) {
						$this->_getSession()->addError($k.': '.$message);
					}
				}
            } catch (Exception $e){
                $this->_getSession()->addError($e->getMessage());
            }
        } else {
            $this->_getSession()->addError(Mage::helper('adminhtml')->__("Order Id is not specified."));
        }
		 $this->_redirect('*/*/index');
	}
}