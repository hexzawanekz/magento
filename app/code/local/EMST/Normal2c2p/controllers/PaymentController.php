<?php
class EMST_Normal2c2p_PaymentController extends Mage_Core_Controller_Front_Action
{
    protected $_order = null;
    protected $_checkout_session = null;

    public function testAction() {
        //Zend_Debug::dump(Mage::getModel('normal2c2p/normal2c2p')->validItem(Mage::getSingleton('checkout/session')->getQuote()));
    }

    public function ajaxSaveTempServiceAction()
    {
        if($this->getRequest()->isAjax()){
            $param = $this->getRequest()->getParam('service');

            /** @var Mage_Checkout_Model_Session $session */
            $session = Mage::getSingleton('checkout/session');

            $session->setNormalPaymentChannel($param);

            //echo $session->getNormalPaymentChannel();
        }
    }

    public function redirectAction(){
        $order = $this->getOrder();
        $model = Mage::getModel('normal2c2p/normal2c2p');
        $model->setOrder($order);
        $this->getResponse()
            ->setBody($this->getLayout()
                ->createBlock('normal2c2p/request')
                ->setModel($model)
                ->toHtml());
    }

    public function pointsAction() {
        $memberId =  $this->getRequest()->getParam('memberid');
        $redirectId =  $this->getRequest()->getParam('redirectid');
        $ecid =  $this->getRequest()->getParam('ecid');
        $corptag =  $this->getRequest()->getParam('corptag');
        Mage::getSingleton('customer/session')
            ->setPointMemberId($memberId)
            ->setEcId($ecid)
            ->setCorptag($corptag)
            ->setPointRedirectId($redirectId);
        $this->_redirect('');
    }
    protected function getOrder() {
        if ($this->_order == null)
            $this->_order = Mage::getModel('sales/order')->loadByIncrementId($this->getSession()->getLastRealOrderId());
        return $this->_order;
    }
    public function setOrder(Mage_Sales_Model_Order $order) {
        $this->_order = $order;
    }
    protected function getSession() {
        if(!$this->_checkout_session)
            $this->_checkout_session=Mage::getSingleton('checkout/session');
        return $this->_checkout_session;
    }
    public function sendTransactionAction() {
        try {
            $normal2c2p = $this->_prepareData();
            if(!$normal2c2p) {
                return ;
            }
            $order = $this->getOrder();
            $order->addStatusHistoryComment(Mage::helper('normal2c2p')->__('Customer was redirected to 2c2p'))->save();
            $this->getResponse()
                ->setBody($this->getLayout()
                    ->createBlock('normal2c2p/redirect')
                    ->setModel($normal2c2p)
                    ->toHtml());
        }
        catch(Exception $e) {
            Mage::log($e->getMessage(),null, 'Normal2c2p.log');
            $this->_getCoreSession()->addError($this->__('There was an error, please try again'));
            $this->getSession()->clear();
            $this->_redirect('');
        }
    }

    /**
     * Get response from service background job of Cash Payment,
     * when it call this action from direct url and submit form data
     *
     * Example for response:
     *
     * Array
    (
        [version] => 6.7
        [request_timestamp] => 2016-05-13 17:36:05
        [merchant_id] => 223
        [order_id] => 122739
        [invoice_no] => 1600000364
        [currency] => 764
        [amount] => 000000060500
        [transaction_ref] => 0901720888
        [approval_code] =>
        [transaction_datetime] =>
        [payment_channel] => 002
        [payment_status] => 001
        [channel_response_code] => 001
        [channel_response_desc] =>
        [masked_pan] =>
        [stored_card_unique_id] =>
        [backend_invoice] => 122739
        [paid_channel] => BANKCOUNTER
        [paid_agent] => SCB
        [user_defined_1] =>
        [user_defined_2] =>
        [user_defined_3] =>
        [user_defined_4] =>
        [user_defined_5] =>
        [browser_info] => Type=Firefox46,Name=Firefox,Ver=46.0
        [hash_value] => 3F92415BAEFF062E2BE8453D994996933F740307
    )
     */
    public function normal2c2pBackendAction()
    {
        $response = $this->getRequest()->getParams() ? $this->getRequest()->getParams() : array();

        // validate data
        if(isset($response['order_id'])
            && isset($response['invoice_no'])
            && isset($response['payment_status'])
            && isset($response['channel_response_code'])
            && isset($response['channel_response_desc'])
            && isset($response['payment_channel'])){

            // load order by invoice no.
            /** @var Mage_Sales_Model_Order $order */
            $order = Mage::getModel('sales/order')->load($response['invoice_no']);
            if ($order->getId()) {
                // match increment id from both sites
                if ($order->getIncrementId() == $response['order_id']) {

                    if(isset($response['recurring_unique_id']) && $response['recurring_unique_id'] != ""  && $response['recurring_unique_id'] != NULL){
                        $helper = Mage::helper('recurringandrentalpayments');
                        $helper->saveRecurringOrderInformation($order,$response);
                    }

                    /** @var EMST_Normal2c2p_Model_Normal2c2p $normal2c2p */
                    $normal2c2p = Mage::getModel('normal2c2p/normal2c2p');

                    if($response['payment_status'] != EMST_Normal2c2p_Model_PaymentChannel::PAY_STATUS_PENDING){
                        $normal2c2p->saveOrderForCashPayment($order, $response['payment_status'], $response['channel_response_code'], $response['channel_response_desc']);
                    }else{
                        $normal2c2p->setType($normal2c2p::TYPE_PAYMENT)
                            ->setActionType($normal2c2p::ACTION_TYPE_RESPONSE)
                            ->setResponse($response)
                            ->setOrder($order)
                            ->setParsedData($normal2c2p->parseXml($response));

                        $result = true;
                        if ($response['payment_channel'] == EMST_Normal2c2p_Model_PaymentChannel::CASH_PAYMENT_CHANNEL) {
                            if($response['payment_status'] == EMST_Normal2c2p_Model_PaymentChannel::PAY_STATUS_SUCCESS || $response['payment_status'] == EMST_Normal2c2p_Model_PaymentChannel::PAY_STATUS_PENDING) {
                                $result = $normal2c2p->saveInvoice($response['payment_status']);
                            }else{
                                $normal2c2p->saveFailReason();
                                $result = false;
                            }
                        }else{
                            if($response['payment_status'] == EMST_Normal2c2p_Model_PaymentChannel::PAY_STATUS_SUCCESS) {
                                $result = $normal2c2p->saveInvoice();
                            }else{
                                $normal2c2p->saveFailReason();
                                $result = false;
                            }
                        }
                        if($result == false){
                            if ($order->canCancel()) {
                                try {
                                    $order->cancel();

                                    // remove status history set in _setState
                                    $order->getStatusHistoryCollection(true);

                                    $order->save();
                                } catch (Exception $e) {
                                    Mage::log($e, null, EMST_Normal2c2p_Model_Normal2c2p::CASH_LOG_FILE,true);
                                }
                            }
                        }
                    }
                    $this->_logResponseData($response);

                } else {
                    // can't match increment id with magento merchant site
                    Mage::log("Increment ID {$response['order_id']} did not match with order {$response['invoice_no']} loaded from Magento merchant site. See any response below.", null, EMST_Normal2c2p_Model_Normal2c2p::CASH_LOG_FILE,true);
                    $this->_logResponseData($response);
                    $newOrderId = substr((string)$response['order_id'],0,-5);
                    $order = Mage::getModel('sales/order')->load($newOrderId);
                    if($order->getId()){
                        if(isset($response['recurring_unique_id']) && $response['recurring_unique_id'] != ""  && $response['recurring_unique_id'] != NULL){
                            $helper = Mage::helper('recurringandrentalpayments');
                            $helper->saveRecurringOrderInformation($order,$response);
                        }
                    }
                }
            } else {
                // can't load order by id
                Mage::log("Function could not found SO by Increment ID: {$response['order_id']}. See any response below.", null, EMST_Normal2c2p_Model_Normal2c2p::CASH_LOG_FILE,true);
                $this->_logResponseData($response);
                $newOrderId = substr((string)$response['order_id'],0,-5);
                $order = Mage::getModel('sales/order')->load($newOrderId);
                if($order->getId()){
                    if(isset($response['recurring_unique_id']) && $response['recurring_unique_id'] != ""  && $response['recurring_unique_id'] != NULL){
                        $helper = Mage::helper('recurringandrentalpayments');
                        $helper->saveRecurringOrderInformation($order,$response);
                    }
                }
            }
        }else{
            Mage::log('Data was not found. See any response below.',null,EMST_Normal2c2p_Model_Normal2c2p::CASH_LOG_FILE,true);
            $this->_logResponseData($response);
        }

        // done
    }

    /**
     * Log response data from Cash payment service
     *
     * @param $response
     */
    protected function _logResponseData($response)
    {
        if(isset($response['hash_value'])){unset($response['hash_value']);}
        if(isset($response['merchant_id'])){unset($response['merchant_id']);}
        if(isset($response['transaction_ref'])){unset($response['transaction_ref']);}
        Mage::log($response,null,EMST_Normal2c2p_Model_Normal2c2p::CASH_LOG_FILE,true);
    }

    /**
     * Get response from payment service and process order
     * Return checkout success page if success
     * Or redirect to cart page, show error message and re-add old items, cancel fail order.
     */
    public function normal2c2pResponseAction()
    {
        /** @var Mage_Sales_Model_Order $order */
        $order = null;
        try{
            $response = $this->getRequest()->getParams();

            /** @var EMST_Normal2c2p_Model_Normal2c2p $normal2c2p */
            $normal2c2p = Mage::getModel('normal2c2p/normal2c2p');
            $normal2c2p	->setType($normal2c2p::TYPE_PAYMENT)
                ->setActionType($normal2c2p::ACTION_TYPE_RESPONSE)
                ->setResponse($response);

            //var_dump($normal2c2p);
            //exit();

            // load order from response
            $order = Mage::getModel('sales/order')->load($response['invoice_no']);

            // check order if exists
            if(!$order->getId()) {
                $this->_getCoreSession()->addError(Mage::helper('normal2c2p')->__('There is no order exists.'));
                $this->_redirect('');
                return;
            }

            // set order and response to object
            $normal2c2p->setOrder($order)
                ->setParsedData($normal2c2p->parseXml($response));

            // process order
            $msg = '';
            if ($response['payment_channel'] == EMST_Normal2c2p_Model_PaymentChannel::CASH_PAYMENT_CHANNEL) {

                // when paid with cash channel
                if($response['payment_status'] == EMST_Normal2c2p_Model_PaymentChannel::PAY_STATUS_SUCCESS || $response['payment_status'] == EMST_Normal2c2p_Model_PaymentChannel::PAY_STATUS_PENDING){
                    $result = $normal2c2p->saveInvoice($response['payment_status']);
                    if(!$result) {
                        $msg = Mage::helper('normal2c2p')->__('Failed to invoice your order.');
                    }
                }else{
                    $normal2c2p->saveFailReason();
                    $msg = Mage::helper('normal2c2p')->__('Your payment was refused by the gateway.');
                }

            } else {

                if($response['payment_status'] == EMST_Normal2c2p_Model_PaymentChannel::PAY_STATUS_SUCCESS) {
                    $normal2c2p->pushDataToPointSystem();
                    $result = $normal2c2p->saveInvoice();
                    if(!$result) {
                        $msg = Mage::helper('normal2c2p')->__('Failed to invoice your order.');
                    }

                } else {
                    $normal2c2p->saveFailReason();
                    $msg = Mage::helper('normal2c2p')->__('Your payment was refused by the gateway.');
                }
            }
        }
        catch(Exception $e) {
            Mage::log($e,null, 'Normal2c2p.log');
            $msg = Mage::helper('normal2c2p')->__('There was an error occur while trying to verify your payment.');
        }

        if($msg) {
            if ($order->canCancel()) {
                try {
                    $order->cancel();

                    // remove status history set in _setState
                    $order->getStatusHistoryCollection(true);

                    // do some more stuff here
                    // ...

                    $order->save();
                } catch (Exception $e) {
                    Mage::logException($e);
                }
            }
            $quoteId = $order->getQuoteId();
            /** @var Mage_Sales_Model_Quote $oldQuote */
            $oldQuote  = Mage::getModel('sales/quote')->load($quoteId);
            Mage::app()->setCurrentStore($oldQuote->getStoreId());
            $this->_redirect('normal2c2p/payment/restoreCart',array('quoteId'=>$quoteId, 'msg'=>$msg));
        }
            //$this->_redirect('checkout/onepage/success');
        else {
                $oid =  $order->getStoreId() ;
                switch ($oid) {
                        case '18' :
                        $this->_redirectUrl('https://www.petloft.com/eng/checkout/onepage/success');
                        break;
                        case '19' :
                        $this->_redirectUrl('https://www.petloft.com/tha/checkout/onepage/success');
                        break;
                        default :
                        $this->_redirect('checkout/onepage/success');
                        break;
                }
        }
    }

    public function normal2c2pAction() {

        try{
            $response = $this->getRequest()->getParam('paymentResponse');
            //echo 'paymentResponse Request:'. $response.'<br/>';
            //exit;
            $response="\r\n".$response;
            $response = trim($response);
            $normal2c2p = Mage::getModel('normal2c2p/normal2c2p');
            $normal2c2p	->setType($normal2c2p::TYPE_PAYMENT)
                ->setActionType($normal2c2p::ACTION_TYPE_RESPONSE)
                ->setEncryptResponse($response)
                ->decryptData();

            //var_dump($normal2c2p);
            //exit();
            $decrypt = $normal2c2p->getDecryptedRes();
            $order = Mage::getModel('sales/order')->loadByIncrementId((string)$decrypt->uniqueTransactionCode);
            if(!$order->getId()) {
                $this->_getCoreSession()->addError(Mage::helper('normal2c2p')->__('There is no order exists'));
                $this->_redirect('');
                return;
            }
            $normal2c2p->setOrder($order)
                ->setParsedData($normal2c2p->parseXml($decrypt));
            $msg = '';
            if((string)$decrypt->respCode == '00' && (string)$decrypt->status == 'A') {
                $normal2c2p->pushDataToPointSystem();
                $result = $normal2c2p->saveInvoice();
                if(!$result) {
                    $msg = Mage::helper('normal2c2p')->__('Failed to invoice your order');
                }

            }
            else {
                $normal2c2p->saveFailReason();
                $msg = Mage::helper('normal2c2p')->__('Your payment was refused by the gateway');
            }
        }
        catch(Exception $e) {
            Mage::log($e->getMessage(),null, 'Normal2c2p.log');
            $msg = Mage::helper('normal2c2p')->__('There was an error occur while trying to verify your payment');
        }

        if($msg) {
            if ($order->canCancel()) {
                try {
                    $order->cancel();

                    // remove status history set in _setState
                    $order->getStatusHistoryCollection(true);

                    // do some more stuff here
                    // ...

                    $order->save();
                } catch (Exception $e) {
                    Mage::logException($e);
                }
            }
            $quoteId = $order->getQuoteId();
            $oldQuote  = Mage::getModel('sales/quote')->load($quoteId);
            Mage::app()->setCurrentStore($oldQuote->getStoreId());
            $this->_redirect('normal2c2p/payment/restoreCart',array('quoteId'=>$quoteId, 'msg'=>$msg));
        }
        else {
            $this->_redirect('checkout/onepage/success');
        }
    }

    public function restoreCartAction(){
        $quoteId = $this->getRequest()->getParam('quoteId');
        $msg = $this->getRequest()->getParam('msg');
        $quote  = Mage::getModel('sales/quote')->load($quoteId);
        $customer = Mage::getModel("customer/customer")->load($quote->getCustomerId());
        if(!Mage::getSingleton('customer/session')->isLoggedIn()){
            $session = Mage::getSingleton('customer/session')->setCustomerAsLoggedIn($customer);
        }
        $cart = Mage::getSingleton('checkout/cart');
        $cart->init();
        $cart->save();
        $quoteId2 = Mage::getSingleton('checkout/session')->getQuote()->getId();
        $newQuote = Mage::getModel('sales/quote')->load($quoteId2);
        $newQuote->merge($quote);
        $newQuote->collectTotals()->save();
        Mage::getSingleton('core/session')->addError($msg);
        $this->_redirect('checkout/onepage');
    }

    protected function _prepareData() {
        $order = $this->getOrder();
        $normal2c2p = Mage::getModel('normal2c2p/normal2c2p');
        $normal2c2p->setOrder($order)
            ->setType($normal2c2p::TYPE_PAYMENT)
            ->setActionType($normal2c2p::ACTION_TYPE_REQUEST);
        $session = $this->getSession();
        if(!$order->getId()||$order->getStatus() == 'complete' || $session->getRedirected() == $order->getId() || !$normal2c2p->validateRedirect($order)) {
            Mage::getSingleton('checkout/session')->clear();
            $this->_redirect('');
            return ;
        }
        $src_data = $normal2c2p->getRequestData();
        $normal2c2p
            ->setDatafile($src_data['sourcefile'])
            ->setEncryptfile($src_data['outputfile'])
            ->encryptData();
        $session->setRedirected($order->getId());
        return $normal2c2p;
    }
    public function none3DSPostingAction() {
        try {
            $msg = '';
            $normal2c2p = $this->_prepareData();
            if(!$normal2c2p) {
                $msg = Mage::helper('normal2c2p')->__('Invalid transaction data');
            }
            else {
                $decrypt = $normal2c2p->none3DSPosting();
                $order = Mage::getModel('sales/order')->loadByIncrementId((string)$decrypt->uniqueTransactionCode);
                if(!$order->getId()) {
                    $this->_getCoreSession()->addError(Mage::helper('normal2c2p')->__('Invalid transaction data'));
                    $this->_redirect('');
                    return;
                }
                $normal2c2p->setOrder($order)
                    ->setParsedData($normal2c2p->parseXml($decrypt));

                if((string)$decrypt->respCode == '00' && (string)$decrypt->status == 'A') {
                    $normal2c2p->pushDataToPointSystem();
                    $result = $normal2c2p->saveInvoice();
                    if(!$result) {
                        $msg = Mage::helper('normal2c2p')->__('Failed to invoice your order');
                    }

                }
                else {
                    $normal2c2p->saveFailReason();
                    $msg = Mage::helper('normal2c2p')->__('Your payment was refused by the gateway');
                }
            }
        }
        catch(Exception $e) {
            Mage::log($e->getMessage(),null, 'Normal2c2p.log');
            $this->_getCoreSession()->addError($this->__('There was an error, please try again'));
            $this->getSession()->clear();
            $this->_redirect('');
        }
        if($msg) {
            $this->_getCoreSession()->addError($msg);
            $this->_redirect('');
        }
        else {
            $this->_redirect('checkout/onepage/success');
        }
    }
    public function merchantAction() {
        try{
            $response = $this->getRequest()->getParam('paymentResponse');
            $response="\r\n".$response;
            $response = trim($response);
            $normal2c2p = Mage::getModel('normal2c2p/normal2c2p');
            $normal2c2p	->setType($normal2c2p::TYPE_PAYMENT)
                ->setActionType($normal2c2p::ACTION_TYPE_BACKEND_RESPONSE)
                ->setEncryptResponse($response)
                ->decryptData();
            $decrypt = $normal2c2p->getDecryptedRes();
            $order = Mage::getModel('sales/order')->loadByIncrementId((string)$decrypt->uniqueTransactionCode);
            if(!$order->getId()) {
                return;
            }
            $normal2c2p->setOrder($order)
                ->setParsedData($normal2c2p->parseXml($decrypt));
            if((string)$decrypt->respCode == '00' && (string)$decrypt->status == 'A') {
                $normal2c2p->pushDataToPointSystem();
                $result = $normal2c2p->saveInvoice();
            }
            else {
                $normal2c2p->saveFailReason();
            }
        }
        catch(Exception $e) {
            Mage::log($e->getMessage(),null, 'Normal2c2p.log');
        }
    }

    /**
     * @return Mage_Core_Model_Session
     */
    protected function _getCoreSession() {
        return Mage::getSingleton('core/session');
    }

    protected function cancelOrder($order){

    }
    protected function restoreLastQuote($order){

    }

}
