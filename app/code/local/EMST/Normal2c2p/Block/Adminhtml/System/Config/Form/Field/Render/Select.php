<?php
/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Onestepcheckout to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_Normal2c2p
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
*/

class EMST_Normal2c2p_Block_Adminhtml_System_Config_Form_Field_Render_Select extends Mage_Core_Block_Abstract
{
    protected function _toHtml() {		
		$column = $this->getColumn();
		$html = '<select name="'.$this->getInputName().'" id="#{_id}_'.$this->getColumnName().'" value="#{'.$this->getColumnName().'}" '.
		(isset($column['style']) ? ' style="'.$column['style'] . '"' : '') .'>';		
        $values = Mage::getModel($column['source'])->toOptionArray();
        if ($values) {
            foreach ($values as $option) {
                $html.= $this->_optionToHtml($option);
            }
        }

        $html.= '</select>';
		return $html;
    }
    protected function _optionToHtml($option) {
        $html = '<option value="'.$this->_escape($option['value']).'"';
		$html.= '>'.$this->_escape($option['label']). '</option>';
        return $html;
    }
    protected function _escape($string) {
        return htmlspecialchars($string, ENT_COMPAT);
    }
}
