<?php
class EMST_Normal2c2p_Block_Adminhtml_Transaction extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct() {
		$this->_controller = 'adminhtml_transaction';
		$this->_blockGroup = 'normal2c2p';
		$this->_headerText = Mage::helper('normal2c2p')->__('2C2P Transaction Tracking');
		parent::__construct();
		$this->_removeButton('add');
	}
	
}
