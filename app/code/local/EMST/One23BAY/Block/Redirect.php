<?php


class EMST_One23BAY_Block_Redirect extends Mage_Core_Block_Abstract
{
	protected function _toHtml()
	{
		$one23BAY = $this->getModel();
        $form = new Varien_Data_Form();
        $form->setAction($one23BAY->getGatewayUrl())
            ->setId('123bay_payment_checkout')
            ->setName('123bay_payment_checkout')
            ->setMethod('POST')
            ->setUseContainer(true);
        foreach ($one23BAY->getStandardCheckoutFormFields() as $field => $value) {
            $form->addField($field, 'hidden', array('name' => $field, 'value' => $value));
        }

        $formHTML = $form->toHtml();

        $html = '<html>';
		$html .= '<head><meta content="text/html; charset=utf-8" http-equiv="Content-Type"></head>';
		$html .= '<body>';
        $html.= $this->__('You will be redirected to 123 BAY Gateway in a few seconds.');
        $html.= $formHTML;
        $html.= '<script type="text/javascript">document.getElementById("123bay_payment_checkout").submit();</script>';
        $html.= '</body></html>';


        return $html;
    }

}
