<?php


/**
 * One23Kbank payment model
 *
 */
class EMST_One23Kbank_Model_One23Kbank extends Mage_Payment_Model_Method_Abstract
{
    protected 	$_code  = 'one23kbank';
    protected 	$_formBlockType = 'one23kbank/form';
	protected 	$_infoBlockType = 'one23kbank/info';
    protected 	$_canAuthorize            = true;
    protected 	$_canUseInternal          = false;
    protected 	$_canUseForMultishipping  = false;
	protected 	$_xml = null;
	protected 	$_root = null;
	
	public function validateRedirect($order) {
		$method = $order->getPayment()->getMethodInstance();
		if($method->getCode() == $this->getCode()) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public function isAvailable($quote=null)
	{
		$active = parent::isAvailable($quote);
      if($quote && $active) {			 
			if($this->getConfigData('dev')) {
				$allowed_emails = explode(',',$this->getConfigData('email'));
				$customer_email=Mage::getSingleton('customer/session')->getCustomer()->getEmail();
				if(in_array($customer_email,$allowed_emails)) {
					return true;
			    }
				else {
					return false;
				}
			}
			else {
				return true;
			}
      }
      return $active;
	}
	
    public function getStatus() 
	{
		return $this->getConfigData('order_status');
	}
	public function getGatewayUrl() 
	{
		return $this->getConfigData('gateway');
	}
	
	public function getMerchantId() 
	{
		return $this->getConfigData('merchant_id');
	}
	public function getMerchantUrl() 
	{
		return Mage::getUrl('one23kbank/payment/merchant',array('_secure' => true));
	}
	public function getApiCallUrl() {
		return Mage::getUrl('one23kbank/payment/apiCallUrl',array('_secure' => true));
	}
	public function getOrderPlaceRedirectUrl()
    {
        return Mage::getUrl('one23kbank/payment/sendTransaction');
    }
	public function getVersion() 
	{
		return $this->getConfigData('version');
	}
	public function getKeepFiles() {
		return $this->getConfigData('keepfiles');
	}
	public function getSecretKey() {
		return $this->getConfigData('secret_key');
	}
	public function getSlipInfo() {
		return $this->getConfigData('slip_info');
	}
	public function getCountryCode() {
		return 'THA';
	}
	public function getTimestamp() {
		$millisec = round((microtime(true)-time())*1000);
		return Mage::getModel('core/date')->date('Y-m-d H:i:s:').$millisec;
	}
	public function getResponseData() {
		if(!$this->getData('response_data')) {
			return array(
							'Version' 		=> $this->getVersion(),
							'TimeStamp' 	=> $this->getTimestamp(),
							'MessageID' 	=> '',
							'Result' 		=> '',
							'FailureReason' => '',
						);
		}
		else {
			return $this->getData('response_data');
		}
	}
	public function getStandardCheckoutFormFields() {		
        return array(
					   'OneTwoThreeReq'  => $this->getEncryptedReq()
					);
	}
	protected function getXml() 
	{
		if(!$this->_xml) {
			$this->_xml = new DOMDocument('1.0','ISO-8859-1');
			$this->_xml->formatOutput = true;
		}
		return $this->_xml;
	}
	protected function getRoot($root)
	{
		if(!$this->_root) {
			$xml = $this->getXml();
			$this->_root = $xml->createElement($root);
			$xml->appendChild($this->_root);
		}
		return $this->_root;
	}
	public function getSimpleXml(array $array,$root = 'OneTwoThreeReq',$child = '') 
	{
		$xml = $this->getXml(); 		
		if($child) {
			$root = $child;
		}
		else {
			$root = $this->getRoot($root);
		}
        foreach($array as $k=>$v) {
			if(is_numeric($k) && is_array($v)) {
				foreach($v as $sub_el=>$att) {
					$sub_el=$xml->createElement($sub_el);
					if(is_array($att)) {
						foreach($att as $at1=>$av1) {
							$at1 = $xml->createAttribute($at1);
							$at1->value = $av1;
							$sub_el->appendChild($at1);
						}
					}
					else {
						throw new Mage_Payment_Model_Info_Exception($this->_getHelper()->__('Invalid element attribute'));
					}
					$root->appendChild($sub_el);
				}
			}
			elseif(!is_numeric($k)) {
				$k=$xml->createElement($k);
				$root->appendChild($k);
			}			
			else {
				throw new Mage_Payment_Model_Info_Exception($this->_getHelper()->__('Invalid array format'));
			}
			if(!is_array($v)) {	
				if($v) {
					$k->appendChild($xml->createTextNode($v));
				}				
			}
            elseif(!is_numeric($k)) {
				
				$this->getSimpleXml($v,'',$k);
				
			}
        }
		
		if($child) {
			return;
		}
		
        $xml = $this->removeFirst($xml->saveXML());
		
		return $xml;
    }
	protected function getKey($type='Public')
	{
		$ext = ($type=='Public')?"cer":"pem";		
		$key = glob(Mage::getBaseDir('lib').DS.'one23Kbank'.DS.'Cert'.DS.$type.DS."*.".$ext);		
		if(empty($key)) {			
			throw new Mage_Payment_Model_Info_Exception($this->_getHelper()->__('Cannot find '.$ext.' '.$type.' key file'));
		}
		return file_get_contents($key[0]);
	}
	public function writeRequest($xml,$type=array('Request','Xml','Encrypt')) 
	{
		$io = new Varien_Io_File();
		$path = $this->getOutputFolder($type[0]);
        
		if($this->getOrder()) {
			$id = $this->getOrder()->getIncrementId();
		}
		else {
			$id = uniqid();
		}
		
        $file = $path . $type[1]. $id . '.txt';
        $io->setAllowCreateFolders(true);
        $io->open(array('path' => $path));        
        $io->write($file,$xml);
        $io->close();
		$encrypt = $path . $type[2].$id . '.txt';
		file_put_contents($encrypt, '');
		chmod($encrypt, 0777);
		return array('sourcefile'=>$file,'outputfile'=>$encrypt);
	}
	protected function removeFirst($xml) 
	{
		$return ='';
		$lines = explode("\n", $xml, 2);
		if(!preg_match('/^\<\?xml/', $lines[0])) {
			$return = $lines[0];
		}
		$return .= $lines[1];
		return $return;
	}
	public function getUserDefined($num) {
		return $this->getConfigData('userDefined'.$num);
	}
	public function getPayCategoryID() {
		return $this->getConfigData('payCategoryID');
	}
	public function getCurrencyCode() {
		$code = $this->getConfigData('currencyCode');
		if(!$code) {
			$code = Mage::app()->getBaseCurrencyCode();
			$currencies = Mage::getModel('one23kbank/source_currencyCode')->getCurrencyCode();
			$code = $currencies[$code];
		}
		
		return $code;
	}
	public function getRequestData() 
	{
		$order = $this->getOrder();
		$detail = '';
		$items = array();
		$i = 1;
		foreach ($order->getAllItems() as $item) {
			$detail .= $item->getUrlKey();
			$price = $item->getBaseRowTotalInclTax()*100;
			$price = str_pad($price,12,'0',STR_PAD_LEFT);
			$items[] = array('PaymentItem'=>array('id'=>$i,'name'=>$item->getUrlKey(),'price'=>$price,'quantity'=>$item->getQtyOrdered()));
			$i++;
		}
		
		$amount = $order->getBaseGrandTotal()*100;
		$amount = str_pad($amount,12,'0',STR_PAD_LEFT);
		$customer = Mage::getSingleton('customer/session')->getCustomer();			
		$name = $customer->getFirstname().' '.$customer->getLastname();
		$payment = $this->getOrder()->getPayment();
		$channel = $payment->getAdditionalInformation('Channel');
		$agent = $payment->getAdditionalInformation('Bank');
		$hash = $this->hashData($this->getMerchantId().$order->getIncrementId().$amount);
		
		$req_data = array(
			'Version' 					=> $this->getVersion(),
			'TimeStamp' 				=> $this->getTimestamp(),
			'MessageID' 				=> $order->getIncrementId(),
			'MerchantID' 				=> $this->getMerchantId(),
			'InvoiceNo' 				=> $order->getIncrementId(),
			'Amount' 					=> $amount,
			'Discount' 					=> str_pad('00',12,'0',STR_PAD_LEFT),
			'ServiceFee' 				=> str_pad('00',12,'0',STR_PAD_LEFT),
			'ShippingFee' 				=> str_pad('00',12,'0',STR_PAD_LEFT),
			'CurrencyCode' 				=> $this->getCurrencyCode(),
			'CountryCode' 				=> $this->getCountryCode(),
			'ProductDesc' 				=> substr($detail,0,255),
			'PaymentItems' 				=> $items,
			'PayerName' 				=> $name,
			'PayerEmail' 				=> $customer->getEmail(),
			'ShippingAddress' 			=> '',
			'MerchantUrl' 				=> $this->getMerchantUrl(),
			'APICallUrl' 				=> $this->getApiCallUrl(),
			'AgentCode' 				=> $agent,
			'ChannelCode' 				=> $channel,
			'PayInSlipInfo' 			=> $this->getSlipInfo(),
			'userDefined1' 				=> $this->getUserDefined(1),
			'userDefined2' 				=> $this->getUserDefined(2),
			'userDefined3' 				=> $this->getUserDefined(3),
			'userDefined4' 				=> $this->getUserDefined(4),
			'userDefined5' 				=> $this->getUserDefined(5),
			'HashValue' 				=> $hash,
			
		);
		
		$xml = $this->getSimpleXml($req_data);
		return $this->writeRequest($xml);
	}
	
	protected function getOutputFolder($type) {
		return Mage::getBaseDir('var') . DS . 'one23Kbank' . DS.$type.DS;
	}
	public function encryptData() 
	{
		$data = $this->getDatafile();
		$encrypt = $this->getEncryptfile();
		$encrypted_req = '';
		 
		if(openssl_pkcs7_encrypt($data, $encrypt, $this->getKey(), array())) {	
			$io = new Varien_Io_File();
			$io->open(array('path' => $this->getOutputFolder('Request')));
			$encrypted_req = $io->read($encrypt);
			$io->close();
			if(!$this->getKeepFiles()) {
				$io->rm($data);
				$io->rm($encrypt);
			}
			
		}
		else {
			throw new Mage_Payment_Model_Info_Exception($this->_getHelper()->__('Fail to encrypt data'));
		}
		$encrypted_req = trim($this->removeHeader($encrypted_req));
		
		$this->setEncryptedReq($encrypted_req);
		return $this;
	}
	protected function removeHeader($content) 
	{
		$content=str_replace("MIME-Version: 1.0","",$content);
		$content=str_replace("Content-Disposition: attachment; filename=\"smime.p7m\"","",$content);
		$content=str_replace("Content-Type: application/x-pkcs7-mime; smime-type=enveloped-data; name=\"smime.p7m\"","",$content);
		$content=str_replace("Content-Transfer-Encoding: base64","",$content);
		return $content;
	}
	public function decryptData() 
	{
		$encrypt_response = 'MIME-Version: 1.0'.PHP_EOL.
		'Content-Disposition: attachment; filename="smime.p7m"'.PHP_EOL.
		'Content-Type: application/x-pkcs7-mime; smime-type=enveloped-data; name="smime.p7m"'.PHP_EOL.
		'Content-Transfer-Encoding: base64'.PHP_EOL.PHP_EOL.
		$this->getEncryptResponse();

		// add by yoa
		$encrypt_response=wordwrap($encrypt_response, 64, "\n", true);

		$src = $this->writeRequest($encrypt_response,array($this->getType(),'Encrypt','Decrypt'));
		$encrypted = $src['sourcefile'];
		$decrypted = $src['outputfile'];
		$private_key = array($this->getKey('Private'), "123");
		if (openssl_pkcs7_decrypt($encrypted, $decrypted, $this->getKey(), $private_key)) {
			$io = new Varien_Io_File();
			$io->open(array('path' => $this->getOutputFolder($this->getType())));
			$decrypted_res = Mage::getModel('core/config_base',$io->read($decrypted))->getNode();
			if($this->getKeepFiles() && ($orderid = (string) $decrypted_res->InvoiceNo)) {
				$io->mv($encrypted,$this->getOutputFolder($this->getType()).'Encrypt'.$orderid.'.txt');
				$io->mv($decrypted,$this->getOutputFolder($this->getType()).'Decrypt'.$orderid.'.txt');				
				$io->close();
			}
			else {
				$io->close();
				$io->rm($encrypted);
				$io->rm($decrypted);
			}
			
		} 
		else {
			throw new Mage_Payment_Model_Info_Exception($this->_getHelper()->__('Fail to decrypt data'));
		}
		$this->setDecryptedRes($decrypted_res);		
		return $this;
	}
	public function hashData($data) 
	{
		$signData = hash_hmac('sha1', $data, $this->getSecretKey(), false);
		$signData =  strtoupper($signData);
		return urlencode($signData);
	}
	public function logRequest() 
	{
		$this->importPaymentInfo($this->getParsedData())->save();
		//$order->save();
	}
	public function parseXml($xml) 
	{
		$return = array();
		foreach($xml as $k=>$v) {
			$return[$k] = (string)$v;
		}
		return $return;
	}
	protected function getSuccessInfo($parsed) 
	{
		$res_code = Mage::getModel('one23kbank/source_responseCode')->getResponseCode();
		$ResponseCode = isset($res_code[$parsed['ResponseCode']])? $res_code[$parsed['ResponseCode']]: $parsed['ResponseCode'];
		return array(
			'Version' 			=> $parsed['Version'],
			'TimeStamp' 		=> $parsed['TimeStamp'],
			'CompletedDateTime' => $parsed['CompletedDateTime'],
			'MessageID' 		=> $parsed['MessageID'],
			'ResponseCode' 		=> $ResponseCode,
			'RefNo1' 			=> $parsed['RefNo1'],
			
		);
	}
	protected function importPaymentInfo($parsed) {
		$payment = $this->getOrder()->getPayment();
		$was = $payment->getAdditionalInformation();
		$new = array_merge($this->getSuccessInfo($parsed),$was);
		$payment->setAdditionalInformation($new);
		return $payment;
	}
	public function saveInvoice()
    {	
		try {
			$order = $this->getOrder();						
			$parsed = $this->getParsedData();
			$payment = $this->importPaymentInfo($parsed);
			$payment->setTransactionId($parsed['RefNo1'])
					->registerCaptureNotification($order->getGrandTotal());
			$order->save();
			$invoice = $payment->getCreatedInvoice();
			if ($invoice && !$order->getEmailSent()) {
				$order->sendNewOrderEmail()->addStatusHistoryComment(
						Mage::helper('one23kbank')->__('Notified customer about invoice #%s.', $invoice->getIncrementId())
					)
					->setIsCustomerNotified(true)
					->save();
			}
			/* if ($order->canShip()) {
				$shipment = $order->prepareShipment()->register();			
				$shipment->getOrder()->setActionFlag(Mage_Sales_Model_Order::ACTION_FLAG_SHIP, true);                    
				Mage::getModel('core/resource_transaction')	->addObject($shipment->getOrder())
															->addObject($shipment)->save();
			} */
			return true;
		}
		catch(Exception $e) {
			Mage::log($e->getMessage(),null, 'One23Kbank.log');
		}
        return false;
    }
	public function assignData($data)
    {
		
		if(!$data) {
			return ;
		}
		if(!is_object($data) ) {
			$data = new Varien_Object($data);
		}
		$additional_inf = array();		
		$channel_agent = explode('_',$data->getData('agent_'.$this->_code));
		if(sizeof($channel_agent)<=1) {
			return ;
		}
		$additional_inf['additional_information'] = array('Channel'=>strtoupper($channel_agent[0]),'Bank'=>strtoupper($channel_agent[1]));
       return parent::assignData($additional_inf);        
    }
}
