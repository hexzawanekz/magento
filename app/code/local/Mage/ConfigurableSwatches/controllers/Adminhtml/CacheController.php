<?php

class Mage_ConfigurableSwatches_Adminhtml_CacheController extends Mage_Adminhtml_Controller_Action
{
    /**
     * Clean configurable swatches files cache
     */
    public function cleanSwatchesAction()
    {
        try {
            /** @var Mage_ConfigurableSwatches_Helper_Productimg $helper */
            $helper = Mage::helper('configurableswatches/productimg');

            $helper->clearSwatchesCache();

            Mage::dispatchEvent('clean_configurable_swatches_cache_after');

            $this->_getSession()->addSuccess(
                Mage::helper('adminhtml')->__('The configurable swatches image cache was cleaned.')
            );
        }
        catch (Mage_Core_Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        }
        catch (Exception $e) {
            $this->_getSession()->addException(
                $e,
                Mage::helper('adminhtml')->__('An error occurred while clearing the configurable swatches image cache.')
            );
        }

        $adminFrontName = Mage::getConfig()->getNode('admin/routers/adminhtml/args/frontName');

        $this->_redirect("{$adminFrontName}/cache");
    }

    /**
     * Check if cache management is allowed
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('configurableswatches/cache');
    }
}