<?php

class Mage_ConfigurableSwatches_Block_Adminhtml_Cache_Additional extends Mage_Adminhtml_Block_Cache_Additional
{
    public function getCleanImagesUrl()
    {
        return $this->getUrl('*/cache/cleanImages');
    }

    public function getCleanMediaUrl()
    {
        return $this->getUrl('*/cache/cleanMedia');
    }

    public function getCleanSwatchesUrl()
    {
        return $this->getUrl('configurableswatches/cache/cleanSwatches');
    }
}