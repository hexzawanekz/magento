<?php
class Mage_Adminhtml_Block_Sales_Order_Renderer_Old extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $value =  $row->getData($this->getColumn()->getIndex());

        if($value == 0 || $value == NULL){
            return "No";
        }else return "Yes";

    }
}