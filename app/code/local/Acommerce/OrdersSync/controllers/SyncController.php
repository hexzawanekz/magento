<?php

/**
 * Acommerce extension for Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade
 * the Acommerce AdminLog module to newer versions in the future.
 * If you wish to customize the Acommerce AdminLog module for your needs
 * please refer to http://www.magentocommerce.com for more information.
 *
 * @category   Acommerce
 * @package    Acommerce_Publishers
 * @copyright  Copyright (C) 2011 Acommerce Web ltd (http://www.acommerce.asia/)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Publishers Controllers
 *
 * @category   Acommerce
 * @package    Acommerce_Publishers
 * @subpackage Controller
 * @author     dev acommerce <dev@acommerce.asia>
 */
class Acommerce_OrdersSync_SyncController extends Mage_Core_Controller_Front_Action
{
    public function testAction() {
        $info = array();
        $info['order_id'] = 'LIN111111111';
        //$info['tracking'] = '12345';
        //$info['provider'] = 'Test';
        $info['trackings'] = array(array('provider' => 'Test', 'tracking' => '12345'), array('provider' => 'Test1', 'tracking' => '123456'));
        $info['items'] = array('TEST1' => 2, 'test' =>2, 'test4' => 5);
        $info = json_decode(json_encode($info));
        echo json_encode($info);
        //var_dump((array)$info->trackings);

    }

    public function orderStatusAction() {
        $info = file_get_contents('php://input');

        if(!empty($info)) {
            $info = json_decode($info);
            $result = array();

            if(!property_exists($info, 'order_id') || !property_exists($info, 'status')) {
                $result = array('error' => array('code' => 500, 'message' => 'Wrong format'));
            } else {
                $orderId = str_replace('LINLIN', 'LIN', $info->order_id);
                $pos = strpos($orderId, '-');

                if ($pos !== false) {
                    $orderId = substr($orderId, 0, $pos);
                }

                $order = Mage::getModel('sales/order')->loadByIncrementId($orderId);

                if ($order->getId()) {
                    if(strtolower($info->status) == 'complete') {
                        /*$order->hasShipments() && */
                        if($order->getStatus() != 'complete') {
                            $this->createInvoice($order);
                        }
                    } elseif(strtolower($info->status) == 'canceled') {
                        if($order->getStatus() != 'complete') {
                            $order->cancel()->save();
                        }
                    } elseif(strtolower($info->status) == 'holded') {
                        if($order->getStatus() != 'complete') {
                            $order->hold()->save();
                        }
                    }
                    $result = array('success' => array('code' => 1, 'message' => 'Already updated order status'));
                } else {
                    $result = array('error' => array('code' => 500, 'message' => 'Not exist sales order'));
                }
            }
            echo json_encode($result);
        }
    }

    public function getOrderAction(){
        $orderId = $this->getRequest()->getParam('order');
        echo base64_encode($orderId);
    }

    public function addShipmentAction() {

        //{"order_id":"LIN100008101","trackings":[{"provider":"Test","tracking":"12345"},{"provider":"Test1","tracking":"123456"}],"items":{"TEST1":2,"test":2,"test4":5}}

        $info = file_get_contents('php://input');

        if(!empty($info)) {
            $info = json_decode($info);

            if(!property_exists($info, 'order_id')) {
                $result = array('error' => array('code' => 500, 'message' => 'Wrong format'));
            } else {

                $orderId = str_replace('LINLIN', 'LIN', $info->order_id);
                $pos = strpos($orderId, '-');

                if ($pos !== false) {
                    $orderId = substr($orderId, 0, $pos);
                }

                $order = Mage::getModel('sales/order')->loadByIncrementId($orderId);
                $result = array();

                if ($order->getId()) {
                    /*$order->hasShipments() && */
                    $qtys = array();
                    if(!property_exists($info, 'items')) {
                        $info->items = null;
                    }

                    if(!is_null($info->items)) {
                        $items = (array)$info->items;
                        foreach($order->getAllItems() as $item) {
                            if(isset($items[$item->getSku()])) {
                                if($items[$item->getSku()] > ($item->getQtyOrdered() - $item->getQtyShipped())) {
                                    $qty = $item->getQtyOrdered() - $item->getQtyShipped();
                                } else {
                                    $qty = $items[$item->getSku()];
                                }
                                if($qty > 0) {
                                    $qtys[$item->getId()] = $qty;
                                }
                            }
                        }
                    }
                    $this->createShipment($order, $info->trackings, $qtys);
                    $result = array('success' => array('code' => 1, 'message' => 'Already added shipment'));
                } else {
                    $result = array('error' => array('code' => 500, 'message' => 'Not exist sales order'));
                }
            }
            echo json_encode($result);
        }

    }

    protected function createInvoice($order)
    {
        if ($order->canInvoice()) {
            $invoice = Mage::getModel('sales/service_order', $order)->prepareInvoice();
            $invoice->register();
            $invoice->setEmailSent(true);
            $invoice->getOrder()->setCustomerNoteNotify(true);
            $invoice->getOrder()->setIsInProcess(true);

            $transactionSave = Mage::getModel('core/resource_transaction')
                            ->addObject($invoice)
                            ->addObject($invoice->getOrder())->save();
            $invoice->sendEmail(true);
        }
    }

    protected function createShipment($order, $trackings, $qtys)
    {
        if ($order->canShip()) {
            $shipment = $order->prepareShipment($qtys)->register();
            $shipment->getOrder()->setIsInProcess(true);
            $shipment->getOrder()->setActionFlag(Mage_Sales_Model_Order::ACTION_FLAG_SHIP, true);

            Mage::getModel('core/resource_transaction')
                    ->addObject($shipment->getOrder())
                    ->addObject($shipment)->save();

            if ($shipment) {
                $shipment->sendEmail(true, '');
                foreach($trackings as $tracking) {
                    $track = Mage::getModel('sales/order_shipment_track')
                            ->setNumber($tracking->tracking)
                            ->setCarrierCode('custom')
                            ->setTitle($tracking->provider);

                    $shipment->addTrack($track)
                            ->save();
                }
            }
        } else {

           $shipments = $order->getShipmentsCollection();
           if ($shipments) {
                foreach($shipments as $shipment) {
                    foreach($trackings as $tracking) {
                        $track = Mage::getModel('sales/order_shipment_track')
                                ->setNumber($tracking->tracking)
                                ->setCarrierCode('custom')
                                ->setTitle($tracking->provider);

                        $shipment->addTrack($track)
                                ->save();
                    }
                    $break;
                }
            }
        }
    }
}