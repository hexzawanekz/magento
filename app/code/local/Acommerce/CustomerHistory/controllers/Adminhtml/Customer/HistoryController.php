<?php
/**
 * Developed by Acommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.Acommerce.asia/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade CustomerHistory to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_CustomerHistory
 * @copyright   Copyright (c) 2012 Acommerce (http://www.Acommerce.asia)
 * @license     http://www.Acommerce.asia/LICENSE-E.txt
*/

class Acommerce_CustomerHistory_Adminhtml_Customer_HistoryController extends Mage_Adminhtml_Controller_Action
{

	protected function _initAction() {
		$this->loadLayout()
			->_setActiveMenu('customer/history')
			->_addBreadcrumb(Mage::helper('adminhtml')->__('History Manager'), Mage::helper('adminhtml')->__('History Manager'));
		
		return $this;
	}   
 
	public function indexAction() {
		$this->_initAction()
			->renderLayout();
	}

	public function editAction() {
		$id     = $this->getRequest()->getParam('id');		
		$model  = Mage::getModel('customerhistory/history')->load($id);
		$customerId = ($this->getRequest()->getParam('customer_id'))?$this->getRequest()->getParam('customer_id'):$model->getCustomerId();
		//var_dump($customerId);exit;
		if(!$customerId) {
			$this->_forward('customer');
			return;
		}
		if ($model->getId() || $id == 0) {
			$data = $this->_getSession()->getFormData(true);			
			if (!empty($data)) {
				$model->setData($data);
			}
			if(!$id || !$model->getCustomerId()) {
				$model->addData(array('status'=>1,'customer_id'=>$customerId));
			}			
			Mage::register('customerhistory_data', $model);
			$this->_initAction()
				->renderLayout();
		}
		else {
			$this->_getSession()->addError(Mage::helper('customerhistory')->__('History does not exist'));
			$this->_redirect('*/*/');
		}
	}
	public function gridAction() {
		$this->loadLayout();				
		$result = $this->getLayout()->getBlock('customer.history.grid')->toHtml();
		$this->getResponse()->setBody($result);
	}
	public function viewgridAction() {
		$this->loadLayout();				
		$result = $this->getLayout()->getBlock('customer.view.history.grid')->toHtml();
		$this->getResponse()->setBody($result);
	}
	public function salesgridAction() {
		$this->loadLayout();				
		$result = $this->getLayout()->getBlock('sales.order.view.history.grid')->toHtml();
		$this->getResponse()->setBody($result);
	}
	public function newAction() {
		if($this->getRequest()->getParam('customer_id')) {
			$this->_forward('edit');
		}
		else {
			$this->_forward('customer');
		}
	}
	public function customerAction() {		
		$this->loadLayout();		
		$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
		$this->renderLayout();
	}
	public function customergridAction() {
		$this->loadLayout();		
		$result = $this->getLayout()->getBlock('customer.history.grid.customer.grid')->toHtml();
		$this->getResponse()->setBody($result);
	}
	public function saveAction() {
		if ($data = $this->getRequest()->getPost()) {
			$model = Mage::getModel('customerhistory/history');		
			$model->setData($data)
				->setId($this->getRequest()->getParam('id',null));			
			try {				
				$model->save();
				$this->_getSession()->addSuccess(Mage::helper('customerhistory')->__('History was successfully saved'));
				$this->_getSession()->setFormData(false);

				if ($this->getRequest()->getParam('back')) {
					$this->_redirect('*/*/edit', array('id' => $model->getId()));
					return;
				}
				$this->_redirect('*/*/');
				return;
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
                $this->_getSession()->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        $this->_getSession()->addError(Mage::helper('customerhistory')->__('Unable to find item to save'));
        $this->_redirect('*/*/');
	}
 
	public function deleteAction() {
		if( $this->getRequest()->getParam('id') > 0 ) {
			try {
				$model = Mage::getModel('customerhistory/history');
				 
				$model->setId($this->getRequest()->getParam('id'))
					->delete();
					 
				$this->_getSession()->addSuccess(Mage::helper('adminhtml')->__('History was successfully deleted'));
				$this->_redirect('*/*/');
			} catch (Exception $e) {
				$this->_getSession()->addError($e->getMessage());
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
			}
		}
		$this->_redirect('*/*/');
	}

    public function massDeleteAction() {
        $customerhistoryIds = $this->getRequest()->getParam('customerhistory');        
        if(!is_array($customerhistoryIds)) {
			$this->_getSession()->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {				
                foreach ($customerhistoryIds as $customerhistoryId) {
                    $customerhistory = Mage::getModel('customerhistory/history')->load($customerhistoryId);
                    $customerhistory->delete();
                }
                $this->_getSession()->addSuccess(
                    Mage::helper('adminhtml')->__(
                        'Total of %d record(s) were successfully deleted', count($customerhistoryIds)
                    )
                );
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }
  
    public function exportCsvAction() {
        $fileName   = 'CustomerHistory.csv';
        $grid   	= $this->getLayout()->createBlock('customerhistory/adminhtml_customer_history_grid');
		$this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
    }

    public function exportXmlAction() {
        $fileName   = 'customerhistory.xml';
        $grid    	= $this->getLayout()->createBlock('customerhistory/adminhtml_customer_history_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
    }
}
