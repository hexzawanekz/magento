<?php
/**
 * Developed by Acommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.Acommerce.asia/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade CustomerHistory to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_CustomerHistory
 * @copyright   Copyright (c) 2012 Acommerce (http://www.Acommerce.asia)
 * @license     http://www.Acommerce.asia/LICENSE-E.txt
*/

class Acommerce_CustomerHistory_Block_Adminhtml_Customer_History_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
	protected function _prepareForm() {
		$form = new Varien_Data_Form();
		$this->setForm($form);
		$fieldset = $form->addFieldset('customerhistory_form', array('legend'=>Mage::helper('customerhistory')->__('Information')));
     
		$fieldset->addField('customer_id', 'text', array(
			'label'     => Mage::helper('customerhistory')->__('Customer ID'),
			'class'     => 'required-entry',
			'required'  => true,
			//'disabled'  => true,
			'name'      => 'customer_id',
		));
		
		$fieldset->addField('status', 'select', array(
			'label'     => Mage::helper('customerhistory')->__('Status'),
			'name'      => 'status',
			'values'    => Mage::getModel('customerhistory/source_status')->getOptions(),
		));
     
		$fieldset->addField('content', 'editor', array(
			'name'      => 'content',
			'label'     => Mage::helper('customerhistory')->__('Content'),
			'title'     => Mage::helper('customerhistory')->__('Content'),
			'style'     => 'width:700px; height:300px;',
			'wysiwyg'   => false,
			'required'  => true,
		));
     
		if (Mage::registry('customerhistory_data')) {
			$form->setValues(Mage::registry('customerhistory_data')->getData());
		}
		return parent::_prepareForm();
	}
	
}
