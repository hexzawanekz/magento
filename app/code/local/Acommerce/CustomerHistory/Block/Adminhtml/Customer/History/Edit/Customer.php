<?php
/**
 * Developed by Acommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.Acommerce.asia/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade CustomerHistory to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_CustomerHistory
 * @copyright   Copyright (c) 2012 Acommerce (http://www.Acommerce.asia)
 * @license     http://www.Acommerce.asia/LICENSE-E.txt
*/

class Acommerce_CustomerHistory_Block_Adminhtml_Customer_History_Edit_Customer extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct() {
		$this->_controller = 'adminhtml_customer_history_edit_customer';
		$this->_blockGroup = 'customerhistory';
		$this->_headerText = Mage::helper('customerhistory')->__('History Manager');
		$this->_backButtonLabel = $this->__('Back');
		$this->_addBackButton();
		parent::__construct();
		$this->_removeButton('add');
	}
	
    public function getHeaderText() {
        return Mage::helper('sales')->__('Please Select a Customer');
    }
    public function getBackUrl() {
		$id = Mage::registry('customerhistory_data')?Mage::registry('customerhistory_data')->getId():null;
        return ($id)?$this->getUrl('*/*/'):$this->getUrl('*/*/index');
    }

}
