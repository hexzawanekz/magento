<?php
/**
 * Developed by Acommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.Acommerce.asia/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade CustomerHistory to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_CustomerHistory
 * @copyright   Copyright (c) 2012 Acommerce (http://www.Acommerce.asia)
 * @license     http://www.Acommerce.asia/LICENSE-E.txt
*/

class Acommerce_CustomerHistory_Block_Adminhtml_Customer_History_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	protected $_showName = false;
	
	public function __construct() {
		parent::__construct();
		$this->setId('customerHistoryGrid');
		$this->setDefaultSort('history_id');
		$this->setDefaultDir('ASC');
		$this->setUseAjax(true);
		$this->_showName = $this->_helper()->showNameEmail();
		$this->setSaveParametersInSession(true);
	}
	protected function _helper($module = 'customerhistory') {
		return Mage::helper('customerhistory');
	}
	protected function _prepareCollection() {
		if($this->_showName) {
			$collection = Mage::getResourceModel('customerhistory/customer_collection')
						->addHistory()
						->addNameToSelect()
						->addAttributeToSelect('email');
		}
		else {
			$collection = Mage::getModel('customerhistory/history')->getCollection();
		}
		
		$this->setCollection($collection);
		return parent::_prepareCollection();
	}

	protected function _prepareColumns() {
		$this->addColumn('history_id', array(
			'header'    => $this->_helper()->__('ID'),
			'align'     =>'right',
			'width'     => '50px',
			'index'     => 'history_id',
		));
		
		$this->addColumn('customer_id', array(
			'header'    => $this->_helper()->__('Customer ID'),
			'align'     =>'left',
			'width'     => '50px',
			'index'     => 'customer_id',
		));
		if($this->_showName) {
			$this->addColumn('name', array(
				'header'    => $this->_helper()->__('Customer Name'),
				'align'     =>'left',
				'width'     =>'170px',
				'index'     => 'name',
			));
			$this->addColumn('email', array(
				'header'    =>$this->_helper()->__('Email'),
				'width'     =>'140px',
				'index'     =>'email'
			));
		}
		$this->addColumn('content', array(
			'header'    => $this->_helper()->__('Description'),
			'align'     =>'left',
			'index'     => 'content',
		));
		$this->addColumn('created_at', array(
			'header'    => $this->_helper()->__('Created At'),
			'align'     => 'left',
			'width'     => '80px',
			'index'     => 'created_at',
			'type'      => 'datetime',
		));
		$this->addColumn('updated_at', array(
			'header'    => $this->_helper()->__('Updated At'),
			'align'     => 'left',
			'width'     => '80px',
			'index'     => 'updated_at',
			'type'      => 'datetime',
		));
		$this->addColumn('status', array(
			'header'    => $this->_helper()->__('Status'),
			'align'     => 'left',
			'width'     => '80px',
			'index'     => 'status',
			'type'    	=> 'options',
			'options'	=> Mage::getModel('customerhistory/source_status')->getOptions()
		));
	  
        $this->addColumn('action',
            array(
                'header'    =>  $this->_helper()->__('Action'),
                'width'     => '100',
                'type'      => 'action',
                'getter'    => 'getHistoryId',
                'actions'   => array(
                    array(
                        'caption'   => $this->_helper()->__('Edit'),
                        'url'       => array('base'=> 'adminhtml/customer_history/edit'),
                        'field'     => 'id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'history_id',
                'is_system' => true,
        ));
		
		$this->addExportType('*/*/exportCsv', $this->_helper()->__('CSV'));
		$this->addExportType('*/*/exportXml', $this->_helper()->__('XML'));
	  
		return parent::_prepareColumns();
	}

    protected function _prepareMassaction() {
        $this->setMassactionIdField('history_id');
        $this->getMassactionBlock()->setFormFieldName('customerhistory');
        $this->getMassactionBlock()->addItem('delete', array(
             'label'    => $this->_helper()->__('Delete'),
             'url'      => $this->getUrl('*/*/massDelete'),
             'confirm'  => $this->_helper()->__('Are you sure?')
        ));

        $statuses = Mage::getSingleton('customerhistory/source_status')->getOptions(false);
        $this->getMassactionBlock()->addItem('status', array(
             'label'=> $this->_helper()->__('Change status'),
             'url'  => $this->getUrl('*/*/massStatus', array('_current'=>true)),
             'additional' => array(
                    'visibility' => array(
                         'name' => 'status',
                         'type' => 'select',
                         'class' => 'required-entry',
                         'label' => $this->_helper()->__('Status'),
                         'values' => $statuses
                     )
             )
        ));
        return $this;
	}

	public function getRowUrl($row) {		
		return $this->getUrl('*/*/edit', array('id' => $row->getHistoryId()));	
	}
	public function getGridUrl() {
		return $this->getUrl('*/*/grid');	
	}
}
