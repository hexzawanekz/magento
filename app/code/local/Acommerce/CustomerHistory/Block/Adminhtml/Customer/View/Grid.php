<?php
/**
 * Developed by Acommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.Acommerce.asia/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade CustomerHistory to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_CustomerHistory
 * @copyright   Copyright (c) 2012 Acommerce (http://www.Acommerce.asia)
 * @license     http://www.Acommerce.asia/LICENSE-E.txt
*/

class Acommerce_CustomerHistory_Block_Adminhtml_Customer_View_Grid extends Acommerce_CustomerHistory_Block_Adminhtml_Customer_History_Grid
{
	public function __construct() {
		parent::__construct();
		$this->_showName = false;
	}
	protected function _getCustomerId() {
		if($customer = Mage::registry('current_customer')) {
			return $customer->getId();
		}		
		if($id = $this->getRequest()->getParam('customer_id')) {
			return $id;
		}
		return null;
	}
	protected function _prepareColumns() {		
		parent::_prepareColumns();
		$this->removeColumn('customer_id');
		$this->removeColumn('history_id');
		$this->_exportTypes = array();
		return $this;
	}
	protected function _prepareCollection() {
		$collection = Mage::getModel('customerhistory/history')->getCollection()
						->addFieldToFilter('customer_id',$this->_getCustomerId());						
		$this->setCollection($collection);
		
		return Mage_Adminhtml_Block_Widget_Grid::_prepareCollection();
	}
    protected function _prepareMassaction() {        
        return $this;
	}

	public function getRowUrl($row) {		
		return $this->getUrl('adminhtml/customer_history/edit', array('id' => $row->getHistoryId()));
	}
	public function getGridUrl() {
		return $this->getUrl('adminhtml/customer_history/viewgrid',array('customer_id'=>$this->_getCustomerId()));	
	}
}
