<?php
/**
 * Developed by Acommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.Acommerce.asia/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade CustomerHistory to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_CustomerHistory
 * @copyright   Copyright (c) 2012 Acommerce (http://www.Acommerce.asia)
 * @license     http://www.Acommerce.asia/LICENSE-E.txt
*/

class Acommerce_CustomerHistory_Model_Resource_Customer_Collection extends Mage_Customer_Model_Resource_Customer_Collection
{
	CONST 		HISTORY_ALIAS_NAME 	= 'ac_history';
	
	protected 	$_historyColumns 	= array('history_id','content','created_at','updated_at','status','customer_id');
	
    public function addHistory() {	
		foreach($this->_historyColumns as $column) {
			$this->addFilterToMap($column,self::HISTORY_ALIAS_NAME.'.'.$column);
		}
		$this->getSelect()
			->reset(Varien_Db_Select::COLUMNS)
			->columns('email')
			->joinInner(
				array(self::HISTORY_ALIAS_NAME=>$this->getTable('customerhistory/history')),
				'ac_history.customer_id=e.entity_id'
			);
		
		return $this;
	}
	protected function _getAllIdsSelect($limit = null, $offset = null) {
        $idsSelect = parent::_getAllIdsSelect($limit, $offset);
        $idsSelect->reset(Zend_Db_Select::COLUMNS);
        $idsSelect->columns(self::HISTORY_ALIAS_NAME.'.history_id');        
        return $idsSelect;
    }
	public function addFieldToFilter($attribute, $condition = null) {
		if(in_array($attribute,$this->_historyColumns)) {
			return Varien_Data_Collection_Db::addFieldToFilter($attribute, $condition);
		}
        return $this->addAttributeToFilter($attribute, $condition);
    }
}
