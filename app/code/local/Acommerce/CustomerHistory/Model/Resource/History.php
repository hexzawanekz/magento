<?php
/**
 * Developed by Acommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.Acommerce.asia/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade CustomerHistory to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_CustomerHistory
 * @copyright   Copyright (c) 2012 Acommerce (http://www.Acommerce.asia)
 * @license     http://www.Acommerce.asia/LICENSE-E.txt
*/

class Acommerce_CustomerHistory_Model_Resource_History extends Mage_Core_Model_Mysql4_Abstract
{
	 /*protected $_serializableFields   = array(
					'additional_info' => array(null, array())
				);*/
    public function _construct() {
        // Note that the customerhistory_id refers to the key field in your database table.
        $this->_init('customerhistory/history', 'history_id');
    }
    protected function _beforeSave(Mage_Core_Model_Abstract $object) {
		if($object->isObjectNew()) {
			$object->setCreatedAt(Varien_Date::now());
		}
		$object->setUpdatedAt(Varien_Date::now());
        return $this;
    }
}
