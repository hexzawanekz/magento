<?php
/**
 * Developed by Acommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.Acommerce.asia/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade CustomerHistory to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_CustomerHistory
 * @copyright   Copyright (c) 2012 Acommerce (http://www.Acommerce.asia)
 * @license     http://www.Acommerce.asia/LICENSE-E.txt
*/

class Acommerce_CustomerHistory_Model_Source_Status extends Mage_Core_Model_Abstract
{
	CONST STATUS_ENABLE 	= 1;
	CONST STATUS_DISABLE 	= 0;	
	public function getOptionArray($null = true) {
		$options = array(
			array('value'=>self::STATUS_ENABLE,'label'=> 'Enable'),
			array('value'=>self::STATUS_DISABLE,'label'=> 'Disable'),
		);
		if($null) {
			array_unshift($options, array('label'=>'Please Select', 'value'=>''));
		}
		return $options;
    }
    public function getOptions($null=false) {		
		foreach($this->getOptionArray($null) as $option) {
			$options[$option['value']] = $option['label'];
		}
		return $options;
	}
}
