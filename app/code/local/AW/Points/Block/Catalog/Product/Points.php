<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Points
 * @version    1.7.3
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Points_Block_Catalog_Product_Points extends Mage_Core_Block_Template
{
	protected $_rate = 0;
    public function canShow()
    {
        return Mage::helper('points/config')->getShowPointsBlockAtProductPage() && ($this->getPoints() > 0);
    }	
	
	public function getPoints_origin()
    {
        if (is_null($this->getData('points'))) {
            try {
                $pointsSummary = 0;

                /* Points amount after order complete */
                $pointsSummary += Mage::getModel('points/rate')
                    ->loadByDirection(AW_Points_Model_Rate::CURRENCY_TO_POINTS)
                    ->exchange($this->getProduct()->getFinalPrice())
                ;

                $maximumPointsPerCustomer = Mage::helper('points/config')->getMaximumPointsPerCustomer();
                if ($maximumPointsPerCustomer) {
                    $customersPoints = 0;
                    $customer = Mage::getSingleton('customer/session')->getCustomer();
                    if ($customer) {
                        $customersPoints = Mage::getModel('points/summary')->loadByCustomer($customer)->getPoints();
                    }
                    if ($pointsSummary + $customersPoints > $maximumPointsPerCustomer) {
                        $pointsSummary = $maximumPointsPerCustomer - $customersPoints;
                    }
                }
                $this->setData('points', $pointsSummary);
            } catch (Exception $e) {

            }
        }
        return $this->getData('points');
    }
	public function getJsPoints($product = null) {
		$store = Mage::app()->getStore();
		$template = str_replace('%s', '#{price}', $store->getCurrentCurrency()->getOutputFormat());
		$points = array('points'=>0,'template'=>$template);		
		if(!$product) {
			$product = $this->getProduct();
		}
		if(!($product instanceof Mage_Catalog_Model_Product)) {
			return $points;
		}
		$points['points']=$this->getPoints($product->getFinalPrice());
		/* $key = 'points_config_'.$product->getId();
		if (is_null($this->getData($key))) {
			$points['points'] = $this->getPoints($product->getFinalPrice());
			if($product->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE) {			
				$jsonConfig = Mage::helper('core')->jsonDecode($this->getLayout()->getBlock('product.info.options.configurable')->getJsonConfig());			
				$basePoints = isset($jsonConfig['basePrice'])?$jsonConfig['basePrice']:0;
				$points['points'] = $this->getPoints($basePoints);
				$options = array();
				foreach($jsonConfig as $config) {
					if(isset($config['id']) && isset($config['options'])) {
						foreach($config['options'] as $option) {
							if(isset($option['id']) && isset($option['price'])) {
								$options[$config['id']] = array($option['id']=>$option['price']);
							}
						}
					}
				}
				$points['config']['options'] = $options;
			}
			$this->setData($key, $points);
		}
        return $this->getData($key); */
		return $points;
	}
	public function getCurrPrice() {
		$product = $this->getProduct();
		if($product) {
			return $product->getFinalPrice();
		}
	}
	public function getCurrentRate() {
		return $this->_rate;
	}
	public function getPoints($price=0,$key=null) {
		$key = 'points_'.$key;
		if (is_null($this->getData($key))) {
            try {
                $pointsSummary = 0;
                /* Points amount after order complete */
                $rate = Mage::getModel('points/rate')
                    ->loadByDirection(AW_Points_Model_Rate::CURRENCY_TO_POINTS)
                    //->exchange($price)
                ;
				$this->_rate = $rate->getPoints()/$rate->getMoney();
				$pointsSummary += $rate->exchange($price);
                $maximumPointsPerCustomer = Mage::helper('points/config')->getMaximumPointsPerCustomer();
                if ($maximumPointsPerCustomer) {
                    $customersPoints = 0;
                    $customer = Mage::getSingleton('customer/session')->getCustomer();
                    if ($customer) {
                        $customersPoints = Mage::getModel('points/summary')->loadByCustomer($customer)->getPoints();
                    }
                    if ($pointsSummary + $customersPoints > $maximumPointsPerCustomer) {
                        $pointsSummary = $maximumPointsPerCustomer - $customersPoints;
                    }
                }
                $this->setData($key, $pointsSummary);
            } catch (Exception $e) {
				Mage::logException($e);
            }
        }
        return $this->getData($key);
	}
	public function getProduct() {
		if(!Mage::registry('current_product')) {
			return $this->getParentBlock()->getProduct();
		}
		return Mage::registry('current_product');
	}
    public function getMoney()
    {
        if (is_null($this->getData('money'))) {
            $money = 0;
            try {
                $money = Mage::getModel('points/rate')
                    ->loadByDirection(AW_Points_Model_Rate::POINTS_TO_CURRENCY)
                    ->exchange($this->getPoints())
                ;
            } catch (Exception $e) {

            }
            $this->setData('money', $money);
        }
        return $this->getData('money');
    }

    public function customerIsGuest()
    {
        return Mage::getModel('customer/session')->getCustomer()->getId() ? false : true;
    }
}