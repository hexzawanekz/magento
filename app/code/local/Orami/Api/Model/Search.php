<?php
class Orami_Api_Model_Search extends Orami_Api_Model_Abstract {

    /**
     * Get algolia categories search result
     * @param $query
     * @param $storeId
     * @return array
     */
    public function getCategoriesResult($query,$storeId){
        $categoryHelper = Mage::helper('algoliasearch/entity_categoryhelper');
        $algoliaHelper = Mage::helper('algoliasearch/algoliahelper');
        $configHelper = Mage::helper('algoliasearch/config');
        $index_name = $categoryHelper->getIndexName($storeId);
        $categoryLimit = Mage::getStoreConfig('algoliasearch/autocomplete/nb_of_categories_suggestions');

        $number_of_results = $categoryLimit;

        if ($configHelper->isInstantEnabled()) {
            $number_of_results = min($configHelper->getNumberOfProductResults($storeId), 1000);
        }

        $answer = $algoliaHelper->query($index_name, $query, array(
            'hitsPerPage'            => $number_of_results, // retrieve all the hits (hard limit is 1000)
            'attributesToRetrieve'   => 'objectID',
            'removeWordsIfNoResults' => $configHelper->getRemoveWordsIfNoResult($storeId),
            'analyticsTags'          => 'backend-search',
        ));

        $data = array();

        foreach ($answer['hits'] as $i => $hit) {
            $data[] = array(
                'id' => $hit['objectID'],
                'name' => str_replace("</em>","",str_replace("<em>","",$hit['_highlightResult']['name']['value'])),
                'path' => str_replace("</em>","",str_replace("<em>","",$hit['_highlightResult']['path']['value'])),
                'url' => str_replace("</em>","",str_replace("<em>","",$hit['_highlightResult']['url']['value']))
            );
        }

        return $data;
    }

    /** Get algolia product search result
     * @param $query
     * @param $storeId
     * @return array
     */
    public function getProductResult($query, $storeId)
    {
        $productHelper = Mage::helper('algoliasearch/entity_producthelper');
        $algoliaHelper = Mage::helper('algoliasearch/algoliahelper');
        $configHelper = Mage::helper('algoliasearch/config');
        $resultsLimit = $configHelper->getResultsLimit($storeId);

        $index_name = $productHelper->getIndexName($storeId);
        $productLimit = Mage::getStoreConfig('algoliasearch/autocomplete/nb_of_products_suggestions');

        $number_of_results = $productLimit;

        if ($configHelper->isInstantEnabled()) {
            $number_of_results = min($configHelper->getNumberOfProductResults($storeId), 1000);
        }

        $answer = $algoliaHelper->query($index_name, $query, array(
            'hitsPerPage'            => $number_of_results,
            'attributesToRetrieve'   => 'objectID',
            'attributesToHighlight'  => '',
            'attributesToSnippet'    => '',
            'numericFilters'         => 'visibility_search=1',
            'removeWordsIfNoResults' => $configHelper->getRemoveWordsIfNoResult($storeId),
            'analyticsTags'          => 'backend-search',
        ));

        $data = array();

        foreach ($answer['hits'] as $i => $hit) {
            $productId = $hit['objectID'];

            if ($productId) {
                $data[$productId] = $resultsLimit - $i;
            }
        }

        return $data;
    }

    /**
     * Get algolia query suggestion
     * @param $query
     * @param $storeId
     * @return array
     */
    public function getSuggestionResult($query, $storeId){
        $suggestionHelper = Mage::helper('algoliasearch/entity_suggestionhelper');
        $algoliaHelper = Mage::helper('algoliasearch/algoliahelper');
        $configHelper = Mage::helper('algoliasearch/config');
        $index_name = $suggestionHelper->getIndexName($storeId);

        $suggestionLimit = Mage::getStoreConfig('algoliasearch/autocomplete/nb_of_queries_suggestions');
        $number_of_results = $suggestionLimit;

        if ($configHelper->isInstantEnabled()) {
            $number_of_results = min($configHelper->getNumberOfProductResults($storeId), 1000);
        }

        $answer = $algoliaHelper->query($index_name, $query, array(
            'hitsPerPage'            => $number_of_results, // retrieve all the hits (hard limit is 1000)
            'attributesToRetrieve'   => 'objectID',
            'removeWordsIfNoResults' => $configHelper->getRemoveWordsIfNoResult($storeId),
            'analyticsTags'          => 'backend-search',
        ));

        $data = array();

        foreach ($answer['hits'] as $i => $hit) {
            $data[] = array('id' => $hit['objectID'],);
        }
        return $data;
    }
}