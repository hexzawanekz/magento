<?php


class Orami_Api_Model_Filter extends Orami_Api_Model_Abstract {

    public function getAttributeFilter($filter,$filterName){
        $url = Mage::getBaseUrl();

        $products = Mage::getModel('catalog/product')
            ->getCollection()
            ->addAttributeToFilter($filterName, $filter);

        $data = array();
        foreach($products as $product){
            $cats = $product->getCategoryIds();
            foreach ($cats as $category_id) {
                $_cat = Mage::getModel('catalog/category')->load($category_id);
                $data[$_cat->getData('entity_id')] = array(
                    "name" => $_cat->getName(),
                    "id" => $_cat->getId(),
                    "url" => $url
                        . "mobileapi/index/index?cmd=catalog&page=1&limit=50&dir=desc&category_id="
                        . $_cat->getId(),
                );
            }
        }
        return $data;
    }

    public function getFilterData($category,$filter = '',$filterName = ''){
        if ($category->getId()) {
            if($filter == "" || $filterName == "" ||$filterName == NULL || $filter == NULL) {
                $layer = Mage::getModel("catalog/layer");
                $layer->setCurrentCategory($category);
                $attributes = $layer->getFilterableAttributes();

                $minPrice = $this->getMinimumPrice($category);
                $maxPrice = $this->getMaximumPrice($category);

                $data = array();
                $children = Mage::getModel('catalog/category')->getCategories($category->getId());
                $url = Mage::getBaseUrl();

                foreach ($children as $cat) {
                    $data['category'][] = array("name" => $cat->getName(), "url" => $url
                        . "mobileapi/index/index?cmd=catalog&page=1&limit=50&dir=desc&category_id="
                        . $cat->getId());
                }

                foreach ($attributes as $attribute) {
                    if ($attribute->getAttributeCode() == 'price') {
                        $filterBlockName = 'catalog/layer_filter_price';
                    } elseif ($attribute->getBackendType() == 'decimal') {
                        $filterBlockName = 'catalog/layer_filter_decimal';
                    } else {
                        $filterBlockName = 'catalog/layer_filter_attribute';
                    }

                    $result = Mage::app()->getLayout()->createBlock($filterBlockName)->setLayer($layer)->setAttributeModel($attribute)->init();
                    $data[$attribute->getAttributeCode()]["name"] = $attribute->getData("frontend_label");
                    foreach ($result->getItems() as $option) {
                        if ($attribute->getAttributeCode() != "price") {

                            $data[$attribute->getAttributeCode()]["attribute"][] = array(
                                "name" => $option->getLabel(),
                                "id" => $option->getValue(),
                                'request_value' => $option->getRequestVar(),
                                "url" => $url
                                    . "mobileapi/index/index?cmd=catalog&page=1&limit=50&dir=desc&category_id="
                                    . $category->getId()
                                    . "&sort=bestseller" . "&attribute_name=" . $attribute->getAttributeCode() . "&filter=" . $option->getValue()
                            );
                        } else {
                            $data[$attribute->getAttributeCode()]["min"] = array(
                                "name" => "Minimum Price",
                                "value" => $minPrice);
                            $data[$attribute->getAttributeCode()]["max"] = array(
                                "name" => "Maximum Price",
                                "value" => $maxPrice);
                        }
                    }
                }
                return $data;
            }else{
                return $this->getAttributeFilter($filter,$filterName);
            }
        }else{
            return array();
        }
    }

    /**
     * get category minimum price.
     * @param $category
     * @return bool|int|string
     */
    public function getMinimumPrice($category)
    {
        if($category instanceof Mage_Catalog_Model_Category){
            $productColl = Mage::getModel('catalog/product')->getCollection()
                ->addCategoryFilter($category)
                ->addAttributeToSort('price', 'asc')
                ->addAttributeToFilter('status', 1)
                ->addAttributeToFilter('visibility', array('in' => array(2,4)))
                ->setPageSize(1)
                ->load();

            $lowestProductPrice = $productColl->getFirstItem()->getFinalPrice();
            return is_numeric($lowestProductPrice) ? $lowestProductPrice : 0;
        }
        return false;
    }

    /**
     * get category maximum price.
     * @param $category
     * @return bool|int|string
     */
    public function getMaximumPrice($category)
    {
        if($category instanceof Mage_Catalog_Model_Category){
            $productColl = Mage::getModel('catalog/product')->getCollection()
                ->addCategoryFilter($category)
                ->addAttributeToFilter('status', 1)
                ->addAttributeToFilter('visibility', array('in' => array(2,4)))
                ->addAttributeToSort('price', 'desc')
                ->setPageSize(1)
                ->load();

            $highestProductPrice = $productColl->getFirstItem()->getFinalPrice();
            return is_numeric($highestProductPrice) ? $highestProductPrice : 0;
        }
        return false;
    }
}