<?php
set_time_limit(0);
class Orami_Api_Model_Cron {
    public function runCronMenu() {
            Mage::app()->setCurrentStore('17');
            $_helper = Mage::helper ( 'catalog/category' );
            $_categories = $_helper->getStoreCategories ();
            $_categorylist = array ();
            $key = "menu";
            if (count ( $_categories ) > 0) {
                    foreach ( $_categories as $_category ) {
                            if(Mage::getModel('mobile/menu')->_hasProducts($_category->getId())) {
                                    $_helper->getCategoryUrl($_category);
                                    $childMenu = Mage::getModel('catalog/category')->load($_category->getId())->getAllChildren();
                                    $childMenu = explode(',', $childMenu);
                                    array_shift($childMenu);
                                    $child = array();
                                    $main_id = $_category->getId();
                                foreach ($childMenu as $childSec) {
                                    if (Mage::getModel('mobile/menu')->_hasProducts($childSec)) {
                                            if((Mage::getModel('catalog/category')->load($childSec)->getLevel()) == '3') {
                                                $id = $childSec;
                                                $child[$childSec] = array('id' => $id,
                                                'main_cate_name' => Mage::getModel('catalog/category')->load($childSec)->getName(),
                                                'is_active' => Mage::getModel('catalog/category')->load($childSec)->getIsActive(),
                                                //'level' => Mage::getModel('catalog/category')->load($childSec)->getLevel(),
                                                'include_in_menu' => Mage::getModel('catalog/category')->load($childSec)->getIncludeInMenu()
                                                );
                                            }
                                            if((Mage::getModel('catalog/category')->load($childSec)->getLevel()) == '4'){
                                                $child[$id]['child'][] = array(
                                                'id' => Mage::getModel('catalog/category')->load($childSec)->getId(),
                                                'name' => Mage::getModel('catalog/category')->load($childSec)->getName(),
                                                'is_active' => Mage::getModel('catalog/category')->load($childSec)->getIsActive(),
                                                'level ' => Mage::getModel('catalog/category')->load($childSec)->getLevel(),
                                                'include_in_menu' => Mage::getModel('catalog/category')->load($childSec)->getIncludeInMenu(),
                                                'parent_id' => Mage::getModel('catalog/category')->load($childSec)->getParentId()
                                                );
                                            }
                                    }
                                }
                                //$child[] = (object)$child;
                                $child = array_values($child);
                                $_categorylist [] = array(
                                        'category_id' => $_category->getId(),
                                        'name' => $_category->getName(),
                                        'is_active' => $_category->getIsActive(),
                                        'include_in_menu' => $_category->getIncludeInMenu(),
                                        'child' => $child
                                );
                            }
                    }
                    $data = $_categorylist;
                    Mage::app()->getCache()->save(serialize($data), $key);
        }
    }

    public function superMenuCron(){
        $storeId = Mage::app()->getStore("moxyen")->getId();
        Mage::app()->setCurrentStore($storeId);

        $menuCollection = Mage::getModel('supermenu/supermenu')->getCollection();
        $storeIds = array(Mage_Core_Model_App::ADMIN_STORE_ID, $storeId);
        $menuCollection->getSelect()
            ->join(array('menu_store' => Mage::getModel('core/resource')->getTableName('supermenu_store')), 'main_table.supermenu_id = menu_store.supermenu_id')
            ->where('main_table.status=?', Magebuzz_Supermenu_Model_Status::STATUS_ENABLED)
            ->where('menu_store.store_id in (?)', $storeIds)
            ->group('main_table.supermenu_id')
            ->order('main_table.sort_order', 'ASC');

        $position = Magebuzz_Supermenu_Model_Config_Position::TOP_MENU;
        $position = array($position, Magebuzz_Supermenu_Model_Config_Position::BOTH_MENU);
        $menuCollection->addFieldToFilter('position', array('in' => $position));

        foreach ($menuCollection as $menuItem) {
            $childData = $menuItem->getCategorysContent();
            $categoryIds = explode(',',$childData);

            foreach ($categoryIds as $childSec) {
                if (Mage::getModel('mobile/menu')->_hasProducts($childSec)) {

                    if((Mage::getModel('catalog/category')->load($childSec)->getLevel()) == '3') {
                        $id = $childSec;
                        $child[$childSec] = array('id' => $id,
                            'main_cate_name' => Mage::getModel('catalog/category')->load($childSec)->getName(),
                            'is_active' => Mage::getModel('catalog/category')->load($childSec)->getIsActive(),
                            //'level' => Mage::getModel('catalog/category')->load($childSec)->getLevel(),
                            'include_in_menu' => Mage::getModel('catalog/category')->load($childSec)->getIncludeInMenu()
                        );
                    }

                    if((Mage::getModel('catalog/category')->load($childSec)->getLevel()) == '4'){
                        $child[$id]['child'][] = array(
                            'id' => Mage::getModel('catalog/category')->load($childSec)->getId(),
                            'name' => Mage::getModel('catalog/category')->load($childSec)->getName(),
                            'is_active' => Mage::getModel('catalog/category')->load($childSec)->getIsActive(),
                            'level ' => Mage::getModel('catalog/category')->load($childSec)->getLevel(),
                            'include_in_menu' => Mage::getModel('catalog/category')->load($childSec)->getIncludeInMenu(),
                            'parent_id' => Mage::getModel('catalog/category')->load($childSec)->getParentId()
                        );
                    }
                }
            }

            $catLink = $menuItem->getData("link_item");
            $catLink = str_replace(".html","",$catLink);
            $_category = Mage::getModel('catalog/category')
                ->getCollection()
                ->addAttributeToSelect('*')
                ->addAttributeToFilter('url_key', $catLink)
                ->addAttributeToFilter('level',2)->getFirstItem();

            $child = array_values($child);

            if ($_category->getId()) {
                $_categorylist [] = array(
                    'category_id' => $_category->getId(),
                    'name' => $_category->getName(),
                    'is_active' => $_category->getIsActive(),
                    'include_in_menu' => $_category->getIncludeInMenu(),
                    'child' => $child
                );
            }else{
                $_categorylist [] = array(
                    'name' => $menuItem->getData('text_item'),
                    'url' =>  $menuItem->getData('link_item'),
                );
            }
        }
        $data = json_encode($_categorylist);
        $data = str_replace("'","",$data);
        $key = "menu";
        $resource = Mage::getSingleton('core/resource');

        $writeConnection = $resource->getConnection('core_write');

        $query = "TRUNCATE TABLE supermenu_cache";
        $writeConnection->query($query);

        $query = "INSERT INTO supermenu_cache (`text_item`, `cache_content`) VALUES ('{$key}','{$data}')";
        $writeConnection->query($query);
    }
}
