<?php

/**
 * Class Orami_Api_NavigationController
 */
class Orami_Api_NavigationController extends Mage_Core_Controller_Front_Action {

    /**
     * Get super menu data
     */
    public function superMenuAction(){
        try {
            $helper = Mage::helper('supermenu');
            $menuCollection = Mage::getModel('supermenu/supermenu')->getCollection();
            $storeIds = array(Mage_Core_Model_App::ADMIN_STORE_ID, Mage::app()->getStore()->getId());
            $menuCollection->getSelect()
                ->join(array('menu_store' => Mage::getModel('core/resource')->getTableName('supermenu_store')), 'main_table.supermenu_id = menu_store.supermenu_id')
                ->where('main_table.status=?', Magebuzz_Supermenu_Model_Status::STATUS_ENABLED)
                ->where('menu_store.store_id in (?)', $storeIds)
                ->group('main_table.supermenu_id')
                ->order('main_table.sort_order', 'ASC');

            $position = Magebuzz_Supermenu_Model_Config_Position::TOP_MENU;
            $position = array($position, Magebuzz_Supermenu_Model_Config_Position::BOTH_MENU);
            $menuCollection->addFieldToFilter('position', array('in' => $position));

            $data = array();
            if (count($menuCollection->getData()) < 1) {

                echo json_encode(
                    array(
                        'code' => 98,
                        'msg' => 'Categories data is empty',
                        'model' => $data
                    )
                );
            } else {
                foreach ($menuCollection as $menuItem) {
                    $itemLink = $helper->getLinkbyItem($menuItem);

                    $childData = $this->getChildInfo($menuItem);
                    $data[] = array(
                        "name" => $menuItem->getData('text_item'),
                        "category_url" => $itemLink,
                        "child_category" => $childData->getData(),
                    );
                }

                echo json_encode(
                    array(
                        'code' => 0,
                        'msg' => 'get super menu data success!',
                        'model' => $data
                    )
                );
            }
        }catch(Exception $e){
            echo json_encode(
                array(
                    'code' => 99,
                    'msg' => $e->getMessage(),
                    'model' => array(),
                )
            );
        }
    }

    /**
     * Get child categories by parent category
     * @param $item
     * @return Object
     */
    public function getChildInfo($item){
        $categoryidsTxt = $item->getCategorysContent();
        $categoryIds = explode(',',$categoryidsTxt);
        $store = Mage::app()->getStore()->getId();
        $categories = Mage::getResourceModel('catalog/category_collection');
        $categories->addAttributeToSelect('*')
            ->addAttributeToFilter('entity_id', array('in'=>$categoryIds))
            ->addFieldToFilter('is_active', 1)
            ->addAttributeToSort('position');
        $categories->setStore($store);
        return $categories;
    }
}