<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Orami
 * @package     Orami_Marketplace
 * @copyright   Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

class Orami_Marketplace_Model_Observer extends Varien_Object
{
    const XML_PATCH_TOKEN_URL        = 'marketplace/marketplace_group/token_api_url';
    const XML_PATCH_ENABLE_LIVE  = 'marketplace/marketplace_group/using_acomm_live';

    const XML_PATCH_USERNAME_TEST = 'marketplace/marketplace_group/test_user_name';
    const XML_PATCH_API_KEY_TEST     = 'marketplace/marketplace_group/test_api_key';
    const XML_PATCH_URL_TEST = 'marketplace/marketplace_group/test_api_url';

    const XML_PATCH_USERNAME_LIVE = 'marketplace/marketplace_group/live_user_name';
    const XML_PATCH_API_KEY_LIVE     = 'marketplace/marketplace_group/live_api_key';
    const XML_PATCH_URL_LIVE = 'marketplace/marketplace_group/live_api_url';

    /**
     * Check Market Pro
     * @param $observer
     */
    public function checkAcommOrder($observer){
        $order = $observer->getEvent()->getOrder();
        $countNormal = 0;
        $countMarket = 0;
        foreach ($order->getAllItems() as $item) {
            $product = Mage::getModel("catalog/product")->load($item->getProduct()->getId());
            $marketProduct = $product->getData("market_place");
            if($marketProduct == "1") $countMarket++;
            else $countNormal++;
        }
        if($countNormal > 0 && $countMarket > 0) $order->setMarketPlace("2");
        else if($countMarket == 0) $order->setMarketPlace("0");
        else if($countNormal == 0 && $countMarket > 0) $order->setMarketPlace("1");
    }

    public function callApi($url, $header, $method = "",$content = NULL)
    {
        $liveEnable = Mage::getStoreConfig(self::XML_PATCH_ENABLE_LIVE);
        if ($liveEnable == 1){
            $userName = Mage::getStoreConfig(self::XML_PATCH_USERNAME_LIVE);
            $apiKey = Mage::getStoreConfig(self::XML_PATCH_API_KEY_LIVE);
        }else{
            $userName = Mage::getStoreConfig(self::XML_PATCH_USERNAME_TEST);
            $apiKey = Mage::getStoreConfig(self::XML_PATCH_API_KEY_TEST);
        }

        if ($content != NULL) $dataContent = $content;
        else $dataContent = "{\r\n \"auth\":\r\n {\r\n \"apiKeyCredentials\":\r\n { \r\n \"username\": \"$userName\", \r\n \"apiKey\": \"$apiKey\"\r\n }\r\n } \r\n}";

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_POSTFIELDS => $dataContent,
            CURLOPT_HTTPHEADER => $header,
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            $data = array("status" => "FAIL", "message" => $err);
            return $data;
        } else {
            $data = array("status" => "OK", "message" => "Get Data", "data" => $response);
            return $data;
        }
    }

    /**
     * Get secret token authorization
     * @return array
     */
    public function getSecretToken()
    {
        $url = Mage::getStoreConfig(self::XML_PATCH_TOKEN_URL);
        $header = array("cache-control: no-cache");
        $response = $this->callApi($url, $header, "POST");
        if ($response['status'] == "FAIL") {
            $data = array("status" => "FAIL", "message" => $response['message']);
            return $data;
        } else {
            $array = json_decode(trim($response['data']), TRUE);
            $token = $array['token']['token_id'];
            $data = array("status" => "OK", "message" => "Get Secret Token Successfully", "token" => $token);
            return $data;
        }
    }

    public function marketplaceOrder($order)
    {
        $allExportedOrders = array();
        $marketplace = $order->getMarketPlace();
        $payment = $order->getPayment()->getMethod();
        $status = $order->getStatus();
        $st = array('pending','invoiced','processing');
        if (in_array($status, $st) || $payment == 'cashondelivery') {
            if ($marketplace == '1') {
                $address = $order->getShippingAddress();
                $billing = $order->getBillingAddress();
                $shippingMethod = $address->getRegion();
                switch ($shippingMethod) {
                    case 'Bangkok':
                    case 'กรุงเทพ':
                        $shippingMethod = 'EXPRESS_1_2_DAYS';
                        break;
                    default:
                        $shippingMethod = 'STANDARD_2_4_DAYS';
                        break;
                }

                switch ($payment) {
                    case 'cashondelivery':
                        $payment = 'COD';
                        break;
                    default:
                        $payment = 'NON_COD';
                        break;
                }
                $orderDate = $order->getCreatedAtStoreDate()->toString('Y-m-d\TH:i:s\Z', 'php');
                $invoiceDate = false;
                $streets = $address->getStreet();
                $customerid = $order->getCustomerId();
                $netsuiteorder['orderCreatedTime'] = $orderDate;
                $netsuiteorder['order_id'] = $order->getRealOrderId();
                $netsuiteorder['customerInfo']['addressee'] = $order->getCustomerFirstname() . ' ' . $order->getCustomerLastname();
                $netsuiteorder['customerInfo']['address1'] = $billing->getStreet(1);
                $netsuiteorder['customerInfo']['province'] = $billing->getRegion();
                $netsuiteorder['customerInfo']['postalCode'] = $billing->getPostcode();
                $netsuiteorder['customerInfo']['country'] = 'Thailand';
                $netsuiteorder['customerInfo']['phone'] = $billing->getTelephone();
                $netsuiteorder['customerInfo']['email'] = $order->getCustomerEmail();
                $shipping_addressee = (($address->getFirstname()) ? $address->getFirstname() : '') . ' ' . (($address->getLastname()) ? $address->getLastname() : '');
                $netsuiteorder['orderShipmentInfo']['addressee'] = $shipping_addressee;
                $netsuiteorder['orderShipmentInfo']['address1'] = $address->getStreet(1);
                $netsuiteorder['orderShipmentInfo']['address2'] = $address->getStreet(2);
                $netsuiteorder['orderShipmentInfo']['subDistrict'] = $address->getSubDistrict() ? $address->getSubDistrict() : "";
                $netsuiteorder['orderShipmentInfo']['district'] = $address->getDistrict();
                $netsuiteorder['orderShipmentInfo']['city'] = "";
                $netsuiteorder['orderShipmentInfo']['province'] = $address->getRegion();
                $netsuiteorder['orderShipmentInfo']['postalCode'] = $address->getPostcode();
                $netsuiteorder['orderShipmentInfo']['country'] = 'Thailand';
                $netsuiteorder['orderShipmentInfo']['phone'] = $address->getTelephone();
                $netsuiteorder['orderShipmentInfo']['email'] = $order->getCustomerEmail();
                $netsuiteorder['paymentType'] = $payment;
                $netsuiteorder['shippingType'] = $shippingMethod;
                $netsuiteorder['grossTotal'] = $order->getSubtotal() * 1;
                $netsuiteorder['currUnit'] = 'THB';
                $discount = $order->getDiscountAmount();
                $coupon = $order->getCouponCode();
                $items = array();
                foreach ($order->getAllItems() as $item) {
                    $q = $item->getQtyOrdered();
                    $t = $item->getPrice();
                    $pid = $item->getId();
                    $partner = $item->getProduct()->getResource()->getAttribute('partner_id')->getFrontend()->getValue($item->getProduct());
                    switch ($partner) {
                        case 'S26' :
                            $partner = '955';
                            break;
                        case 'DSG' :
                            $partner = '727';
                            break;
                        case 'Babylove Thailand' :
                            $partner = '476';
                            break;
                        case 'Certainty Thailand' :
                            $partner = '477';
                            break;
                        case 'Kira Kira' :
                            $partner = '773';
                            break;
                        default :
                            $partner = '955';
                            break;
                    }
                    $items[] = array(
                         'partnerId'            => $partner,
                        // 'itemId'           => $item->getSku(),
                        //'partnerId' => "demoth1",
                        'itemId' => $item->getSku(),
                        'qty' => $item->getQtyOrdered() * 1,
                        'subTotal' => $q * $t
                    );
                    //send discount //
                        if ($discount != ''){
                            if($coupon != ''){ // check reward or coupon discpunt
                                $items[] = array(
                                'partnerId'            => $partner,
                                'itemId' => '50000000002',
                                'qty' => 1,
                                'subTotal' => $discount * 1
                            );
                            }else{
                               $items[] = array(
                                'partnerId'            => $partner,
                                'itemId' => '50000000001',
                                'qty' => 1,
                                'subTotal' => $discount * 1
                            ); 
                            }
                        }
                }
                $netsuiteorder['orderItems'] = $items;
                $allExportedOrders = $netsuiteorder;
            }
        }
        return $allExportedOrders;
    }

    /**
     * Send Order Data to Acomm
     * @param $observer
     */
     public function sendAcommOrder($observer)
    {   
        try {
            $order = $observer->getEvent()->getOrder();
            $marketplace = $order->getMarketPlace();
            $increment = $order->getIncrementId();
            if($marketplace == '1'){
                $data = $this->getSecretToken();
                $liveEnable = Mage::getStoreConfig(self::XML_PATCH_ENABLE_LIVE);
                if ($liveEnable == 1){
                    $url = Mage::getStoreConfig(self::XML_PATCH_URL_LIVE).$increment;
                }else{
                    $url = Mage::getStoreConfig(self::XML_PATCH_URL_TEST).$increment;
                }
                if ($data["status"] == "FAIL") {
                    Mage::log("Fail to get token", null, "sendMarketplaceOrder.log", true);
                } else {
                    $token = $data["token"];
                    $data = $this->marketplaceOrder($order);
                    $jsonData = json_encode($data);
                    $header = array(
                        "X-Subject-Token:".$token,
                        "Content-Type: application/json;charset=UTF-8",
                        "User-Agent: Awesome-Products-App",
                    );
                    Mage::log($jsonData,null,"sendMarketplaceOrder.log",true);
                    $dataApi = $this->callApi($url,$header,"PUT",$jsonData);
                    Mage::log($dataApi,null,"sendMarketplaceOrder.log",true);
                }
            }
        }   catch (Exception $e) {
            Mage::log($e->getMessage(), null, 'sendMarketplaceOrder.log');
        }
    }

    public function logAcommOrder($observer)
    {
        $order = $observer->getEvent()->getOrder();
        $marketplace = $order->getMarketPlace();
        $increment = $order->getIncrementId();
        if($marketplace == '1'){
            $data = $this->marketplaceOrder($order);
            $jsonData = json_encode($data);
            Mage::log($jsonData,null,"MarketplaceOrder.log",true);
        }
    }


    // public function send123AcommOrder($observer)
    // {
    //     $order = $observer->getEvent()->getOrder();
    //     $marketplace = $order->getMarketPlace();
    //     if($marketplace === '1'){
    //         $stateProcessing = $order::STATE_PROCESSING;
    //         $payment = $order->getPayment();
    //         $method = $payment->getMethod();
    //         if ($method == 'normal2c2p'){
    //         $channel = $payment->getAdditionalInformation();
    //         $channel = $channel['Paid Agent'];
    //             if ($order->getState() == $stateProcessing && $channel == '123Service' ) {
    //                 $data = $this->getSecretToken();
    //                 $liveEnable = Mage::getStoreConfig(self::XML_PATCH_ENABLE_LIVE);
    //                 if ($liveEnable == 1){
    //                     $url = Mage::getStoreConfig(self::XML_PATCH_URL_LIVE).$order->getIncrementId();
    //                 }else{
    //                     $url = Mage::getStoreConfig(self::XML_PATCH_URL_TEST).$order->getIncrementId();
    //                 }
    //                 if ($data["status"] == "FAIL") {
    //                     Mage::log("Fail to get token", null, "sendMarketplaceOrder.log", true);
    //                 } else {
    //                     $token = $data["token"];
    //                     $data = $this->marketplaceOrder($order);
    //                     $jsonData = json_encode($data);
    //                     $header = array(
    //                         "X-Subject-Token:".$token,
    //                         "Content-Type: application/json;charset=UTF-8",
    //                         "User-Agent: Awesome-Products-App",
    //                     );
    //                     Mage::log($jsonData,null,"sendMarketplaceOrder.log",true);
    //                     $dataApi = $this->callApi($url,$header,"PUT",$jsonData);
    //                     Mage::log($dataApi,null,"sendMarketplaceOrder.log",true);
    //                 }
    //             }
    //         }
    //     }
    // }
}