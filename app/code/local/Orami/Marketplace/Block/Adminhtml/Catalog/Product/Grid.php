<?php

class Orami_Marketplace_Block_Adminhtml_Catalog_Product_Grid extends Mage_Adminhtml_Block_Catalog_Product_Grid
{
    public function setCollection($collection)
    {
        $collection->addAttributeToSelect('market_place');
        parent::setCollection($collection);
    }

    protected function _prepareColumns()
    {
        $this->addColumnAfter('market_place', array(
        'header'    =>  Mage::helper('catalog')->__('MarketPlace'),
        'width'     =>  '50px',
        'align'     =>'right',
        'sortable'  => true,
        'index'     =>  'market_place',
        'type'      =>  'options',
        'options' => array('1' => 'Yes', '0' => 'No'),
        ),status);
        return parent::_prepareColumns();
    }
}