<?php

/**
 * Class Orami_Api_OrderController
 * create by Phap
 * date 6/12/17
 */
class Orami_Marketplace_OrderController extends Mage_Core_Controller_Front_Action
{
    protected $_netsuiteStatues = array(
        'REJECTED_BY_CUSTOMER',
        'DELIVERED',
        'IN_TRANSIT',
        'PREPARING_DELIVERY',
        'FAILED_TO_DELIVER',
        'CANCELLED',
    );

    protected $_allowedStatuses = array(
        'COMPLETED',
        'ERROR',
        'CANCELLED',
    );


    protected $_offlineMethod = array(
        'cashondelivery',
        'nocost',
    );

    public function getOrderAction()
    {
        date_default_timezone_set('Asia/Bangkok');
        $current_time = explode(':', date("H:i", time()));
        $current_time = intval($current_time[0] . $current_time[1]);
        if ($current_time > 130 && $current_time < 330) {
            Mage::log('Now is limited time, Cannot send SO during this moment', null, 'data.log');
            return array();
        }
        $allExportedOrders = array();
        $datetime = Mage::app()->getRequest()->getParam('datetime');
        $p = Mage::app()->getRequest()->getParam('page');
        $orders = Mage::getModel('sales/order')->getCollection()
            ->addFieldToFilter('market_place', 1)
            ->addFieldTofilter('created_at', array('lt' => $datetime))
            ->setPageSize(20)
            ->setCurPage($p);
        $count = 0;
        foreach ($orders as $order) {
            $address = $order->getShippingAddress();
            $billing = $order->getBillingAddress();
            $shippingMethod = $address->getShippingMethod();
            $payment = $order->getPayment()->getMethod();
            $orderDate = $order->getCreatedAtStoreDate()->toString('d/m/Y', 'php');
            $invoiceDate = false;
            $streets = $address->getStreet();
            $customerid = $order->getCustomerId();
            $netsuiteorder['order_id'] = $order->getRealOrderId();
            $netsuiteorder['customerInfo']['addressee'] = $order->getCustomerFirstname() . ' ' . $order->getCustomerLastname();
            $netsuiteorder['customerInfo']['address1'] = $billing->getStreet(1);
            $netsuiteorder['customerInfo']['province'] = $billing->getRegion();
            $netsuiteorder['customerInfo']['postalCode'] = $billing->getPostcode();
            $netsuiteorder['customerInfo']['contry'] = 'Thailand';
            $netsuiteorder['customerInfo']['phone'] = $billing->getTelephone();
            $netsuiteorder['customerInfo']['email'] = $order->getCustomerEmail();
            $shipping_addressee = (($address->getFirstname()) ? $address->getFirstname() : '') . ' ' . (($address->getLastname()) ? $address->getLastname() : '');
            $netsuiteorder['orderShipmentInfo']['addressee'] = $shipping_addressee;
            $netsuiteorder['orderShipmentInfo']['address1'] = $address->getStreet(1);
            $netsuiteorder['orderShipmentInfo']['address2'] = $address->getStreet(2);
            $netsuiteorder['orderShipmentInfo']['subDistrict'] = $address->getSubDistrict();
            $netsuiteorder['orderShipmentInfo']['district'] = $address->getDistrict();
            $netsuiteorder['orderShipmentInfo']['city'] = '';
            $netsuiteorder['orderShipmentInfo']['province'] = $address->getRegion();
            $netsuiteorder['orderShipmentInfo']['postalCode'] = $address->getPostcode();
            $netsuiteorder['orderShipmentInfo']['country'] = 'Thailand';
            $netsuiteorder['orderShipmentInfo']['phone'] = $address->getTelephone();
            $netsuiteorder['orderShipmentInfo']['email'] = $order->getCustomerEmail();
            $netsuiteorder['paymentType'] = $payment;
            $netsuiteorder['shippingType'] = $shippingMethod;
            $netsuiteorder['grossTotal'] = $order->getSubtotal();
            $netsuiteorder['currUnit'] = 'THB';
            $items = array();
            foreach ($order->getAllItems() as $item) {
                $q = $item->getQtyOrdered();
                $t = $item->getPrice();
                $pid = $item->getId();
                $partner = $item->getProduct()->getResource()->getAttribute('partner_id')->getFrontend()->getValue($item->getProduct());
                switch ($partner) {
                    case 'S26' :
                        $partner = '710';
                        break;
                    case 'DSG' :
                        $partner = '727';
                        break;
                    default :
                        $partner = 'null';
                        break;
                }
                $items[] = array(
                    'partnerId' => $partner,
                    'itemId' => $item->getSku(),
                    'qty' => $item->getQtyOrdered(),
                    'subTotal' => $q * $t
                );
            }
            $netsuiteorder['orderItems'] = $items;
            $allExportedOrders[] = $netsuiteorder;
            $count++;
        }
        $page = ceil($count / 20);
        echo json_encode(array('page count' => $page, 'data' => $allExportedOrders));
    }

    public function updateOrderAction()
    {   
        $calltime = gmdate("Y-m-d\TH:i:s\Z");
        Mage::log($calltime, null, 'marketplace_shipment.log',true);
        $data = file_get_contents("php://input");
        Mage::log("Data acomm send to Magento:", null, 'marketplace_shipment.log',true);
        Mage::log($data, null, 'marketplace_shipment.log',true);

        $events = json_decode( "[".$data."]", true);
        $i = 0;
        foreach ($events as $event) {
            $order_id = $event['orderId'];
            $count = $event['orderDetailedStatus'];
            end($count);
            $key = key($count);

            $order_create = $event['orderCreatedTime'];
            $dateOrder = $this->_convertDate($order_create);

            $order_update = $event['orderDetailedStatus'][$key]['orderUpdatedTime'];
            $dateShip = $this->_convertDate($order_update);

            $status = $event['orderDetailedStatus'][$key]['orderDetailedStatus'];
            $provider = 'aCommerce';

            if (in_array($status, $this->_allowedStatuses)) {
                Mage::log("start " . $order_id, null, 'marketplace_shipment.log');
                $order = Mage::getModel('sales/order')->loadByIncrementId($order_id);
                if ($order->getId()) {
                    if ($status == 'COMPLETED') {
                        $dateShipped = $order_update;
                        if ($dateShipped) {
                            $shipment = $this->_getShipment($order, $dateShipped);
                            if (!$shipment->getTotalQty()) {
                                return;
                            }
                            if (!$order->hasInvoices()) {
                                $this->_createInvoice($shipment);
                            }
                            $order->save();
                        }
                    } else {
                        $payment = $order->getPayment();
                        if ($order->canCancel()) {
                            try {
                                $order->cancel();
                                $order->addStatusHistoryComment("Automatically cancelled from NetSuite's shipment import file.");
                                $order->save();
                            } catch (Exception $e) {
                                Mage::log('----- ' . $e->getMessage(), null, 'marketplace_shipment.log');
                            }
                        } else {
                            if (!in_array($payment->getMethod(), $this->_offlineMethod)) {
                                try {
                                    $order->addStatusHistoryComment("Automatically cancelled from NetSuite's shipment import file.");
                                    $order->setStatus(Mage_Sales_Model_Order::STATE_CLOSED);
                                    $order->save();
                                } catch (Exception $e) {
                                    Mage::log('----- ' . $e->getMessage(), null, 'marketplace_shipment.log');
                                }
                            }
                        }
                    }

                }
                Mage::log("\n" . "end " . $order_id . "\n ---------- \n", null, 'marketplace_shipment.log');
            }else {
                Mage::log($order_id." : ".$status. "updated".$order_update, null, 'marketplace_shipment.log');
            }
        }
    }


    public function _getShipment(Mage_Sales_Model_Order $order, $dateShipped)
    {
        /** @var Mage_Sales_Model_Order_Shipment $shipment */
        if (!$order->hasShipments()) {
            $shipment = $order->prepareShipment();
            if ($shipment->getTotalQty()) {
                $shipment->register();
                $shipment->addComment("Automatically updated from NetSuite's shipment import file.", false);
                $shipment->getOrder()->setIsInProcess(true);
                $shipment->setData('created_at', $dateShipped);
                $shipment->setData('updated_at', $dateShipped);
                $shipment->save();
            }
            return $shipment;
        } else {
            $shipment = $order->getShipmentsCollection()->getFirstItem();
            if ($shipment) {
                $shipment->setData('created_at', $dateShipped);
                $shipment->setData('updated_at', $dateShipped);
                $shipment->save();
                return $shipment;
            }
        }
    }

    public function _createInvoice(Mage_Sales_Model_Order_Shipment $shipment)
    {
        $order = $shipment->getOrder();

        if ($order->canInvoice()) {
            /** @var Mage_Sales_Model_Order_Invoice $invoice */
            $invoice = Mage::getModel('sales/service_order', $order)->prepareInvoice();
            $invoice->setRequestedCaptureCase(Mage_Sales_Model_Order_Invoice::CAPTURE_ONLINE);
            $invoice->register();
            $invoice->save();
        }
    }

    public function _convertDate($date)
    {
        return preg_replace('@([0-9]+)/([0-9]+)/([0-9]+)\s([0-9]+):([0-9]+):([0-9]+)@', '$3-$2-$1 $4:$5:$6', $date);
    }
}

