<?php
class Orami_MagazineWidget_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }
    public  function apache_request_headers() {
        foreach($_SERVER as $key=>$value) {
            if (substr($key,0,5)=="HTTP_") {
                $key=str_replace(" ","-",ucwords(strtolower(str_replace("_"," ",substr($key,5)))));
                $out[$key]=$value;
            }else{
                $out[$key]=$value;
            }
        }
        return $out;
    }

    public function sendAction() {
        $request_headers        = $this->apache_request_headers();
        $http_origin            = $request_headers['REQUEST_SCHEME'];
        
        if($http_origin == 'http'){
            $uri = 'http://www.orami.co.th/magazine';
        }else{
            $uri = 'https://www.orami.co.th/magazine';
        }
        
        $allowed_http_origins   = array(
                                "http",
                                "https"
                                );
        if (in_array($http_origin, $allowed_http_origins)){
            $this->getResponse()->setHeader('Access-Control-Allow-Origin', $uri);
        }

        $CID =                  $_POST['CID'];
        $SID  =                 $_POST['SID'];
        $session_id  =  $_POST['session_id'];
        $f    =         $_POST['f'];
        $p    =         $_POST['p'];
        $a    =         $_POST['a'];
        $el   =         $_POST['el'];
        $endlink   =    $_POST['endlink'];
        $llid    =      $_POST['llid'];
        $c     =        $_POST['c'];
        $optin   =      $_POST['optin'];
        $counted  =     $_POST['counted'];
        $inp_32304 =    (int)$_POST['inp_32304'];
        $inp_32305 =    (int)$_POST['inp_32305'];
        $inp_32303 =    (int)$_POST['inp_32303'];
        $inp_3 = $_POST['inp_3'];
        $post_data='CID='.$CID.'&SID='.$SID.'&session_id='.$session_id.'&f='.$f.'&p='.$p.'&a='.$a.'&el='.$el.'&endlink='.$endlink.'&llid='.$llid.'&c='.$c.'&optin='.$optin.'&counted='.$counted.'&inp_32304='.$inp_32304.'&inp_32305='.$inp_32305.'&inp_32303='.$inp_32303.'&inp_3='.$inp_3.'';
        $this->PostToHost('http://click.orami.co.th/u/register.php?',$post_data);
        }

         public function submitAction() {
        $CID =                  $_POST['CID'];
        $SID  =                 $_POST['SID'];
        $session_id  =  $_POST['session_id'];
        $f    =         $_POST['f'];
        $p    =         $_POST['p'];
        $a    =         $_POST['a'];
        $el   =         $_POST['el'];
        $endlink   =    $_POST['endlink'];
        $llid    =      $_POST['llid'];
        $c     =        $_POST['c'];
        $optin   =      $_POST['optin'];
        $counted  =     $_POST['counted'];
        $inp_10832 =    (int)$_POST['inp_10832'];
        $inp_24286 =    (int)$_POST['inp_24286'];
        $inp_24289 =    (int)$_POST['inp_24289'];
        $inp_10831 =    (int)$_POST['inp_10831'];
        $inp_10830 =    (int)$_POST['inp_10830'];
        $inp_10829 =    (int)$_POST['inp_10829'];
        $inp_24287 =    (int)$_POST['inp_24287'];
        $inp_24288 =    (int)$_POST['inp_24288'];
        $inp_3 = $_POST['inp_3'];
        $post_data='CID='.$CID.'&SID='.$SID.'&session_id='.$session_id.'&f='.$f.'&p='.$p.'&a='.$a.'&el='.$el.'&endlink='.$endlink.'&llid='.$llid.'&c='.$c.'&optin='.$optin.'&counted='.$counted.'&inp_10832='.$inp_10832.'&inp_24286='.$inp_24286.'&inp_24289='.$inp_24289.'&inp_10831='.$inp_10831.'&inp_10830='.$inp_10830.'&inp_10829='.$inp_10829.'&inp_24287='.$inp_24287.'&inp_24288='.$inp_24288.'&inp_3='.$inp_3.'';
$url = 'http://click.orami.co.th/u/register.php';
$ch = curl_init( $url );
curl_setopt( $ch, CURLOPT_POST, 1);
curl_setopt( $ch, CURLOPT_POSTFIELDS, $post_data);
curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
curl_setopt( $ch, CURLOPT_HEADER, 0);
curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);
$response = curl_exec( $ch );
echo '<iframe src="https://www.orami.co.th/popup/orami_popup/thankyou.php" 
scrolling="no" marginheight="0" marginwidth="0" style="width:370px;height:560px;"></iframe>';
//        $this->PostToHost('http://click.orami.co.th/u/register.php?',$post_data);
        }
        public function PostToHost($host,$data_to_send) {
                echo header('Location:'.$host.''.$data_to_send.'');
        }
}