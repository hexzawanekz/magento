<?php
class Orami_MagazineWidget_Block_Display extends Mage_Core_Block_Template
{
	protected $_cat_name;
	protected $_utm;
	protected $_title;
	protected $_allow_subscribe;

	public function setCategory($catName)
	{
		$this->_cat_name = $catName;
	}

	public function setUtm($utmCode)
	{
		$this->_utm = $utmCode;
	}

	public function getUtm()
	{
		return $this->_utm;
	}

	public function setTitle($title)
	{
		$this->_title = $title;
	}

	public function getTitle()
	{
		if (!empty($this->_title)) {
			return $this->_title;
		} else {
			return "Orami Magazine";
		}
	}

	public function setSubscribe($allowSubscribe)
	{
		$this->_allow_subscribe = $allowSubscribe;
	}

	public function getSubscribe()
	{
		return $this->_allow_subscribe;
	}

    public function magazineFeed()
    {
        $cache = Mage::app()->getCache();
        $cacheKey = 'magazine_cache_'.$this->_cat_name;
        $obj = $cache->load($cacheKey);

        if (!$obj) {
            $magazine_content = "https://www.orami.co.th/magazine/affiliate/articles/".$this->_cat_name."?count=3&page=1";
            $obj = file_get_contents($magazine_content);
            $cache->save($obj, $cacheKey, array('magazine_cache'), 3600);
        }
        return json_decode($obj);
    }
}
