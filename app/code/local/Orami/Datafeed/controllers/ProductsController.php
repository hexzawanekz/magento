<?php

/**
 * Class Orami_Datafeed_ProductsController
 */
class Orami_Datafeed_ProductsController extends Mage_Core_Controller_Front_Action {

public function getProductAction()
    {
                ini_set('memory_limit', '-1');
                ini_set('max_execution_time', 900);
                Mage::app()->setCurrentStore(16);
                $page =  1;
                $limit = ($this->getRequest()->getParam('limit')) ? ($this->getRequest()->getParam('limit')) : 200;
                $dir = 'desc';
                $month_ini = new DateTime("first day of last year");
                $category = Mage::getModel('catalog/product')->getCollection();
                        $collection = $category->addAttributeToFilter('status', 1)->addAttributeToFilter('visibility', array('neq' => 1))->addAttributeToSort('revenue_last_month', $dir);
                        Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($collection);
                        $pages = $collection->setPageSize($limit)->getLastPageNumber();
                        $collection->setPage($page, $limit);
                        $product_list = $this->getProductList($collection, 'catalog');
                echo json_encode( $product_list);
    }

     public function getProductList($products, $mod = 'product')
    {
        $baseCurrency = Mage::app()->getStore()->getBaseCurrency()->getCode();
        $currentCurrency = Mage::app()->getStore()->getCurrentCurrencyCode();
        $store_id = Mage::app()->getStore()->getId();
        $product_list = array();
        $products_model = Mage::getModel('dfw/products');
        $main = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB);
        foreach ($products as $product) {
            if ($mod == 'catalog') {
                $product = Mage::getModel('catalog/product')->load($product ['entity_id']);
            }
            $summaryData = Mage::getModel('review/review_summary')->setStoreId($store_id)->load($product->getId());
            $price = ($product->getSpecialPrice()) == null ? ($product->getPrice()) : ($product->getSpecialPrice());
            $regular_price_with_tax = number_format(Mage::helper('directory')->currencyConvert($product->getPrice(), $baseCurrency, $currentCurrency), 2, '.', '');
            $final_price_with_tax = number_format(Mage::helper('directory')->currencyConvert($product->getSpecialPrice(), $baseCurrency, $currentCurrency), 2, '.', '');
                $temp_product = array();
                $options = array();
                $price = array();
        $attributes = $product->getAttributes();
        foreach ($attributes as $attribute) {
                        if( !in_array ( $attribute->getAttributeCode() , array('features' ,'media_gallery') )){
                        $id[] =  $attribute->getAttributeCode($product);
                        $att[] = $attribute->getFrontend()->getValue($product);
                        }
        } $temp_product['orami'] = array_combine($id, $att);
        $gallery_images = $product->getMediaGalleryImages();
        $items = array();
        foreach($gallery_images as $g_image) {
            $items[] = $g_image['url'];
        }
        $temp_product['orami']['media_gallery'] = implode(', ', $items);
         $store_id = Mage::app()->getStore()->getId();
        $product_list = array();
        $products_model = Mage::getModel('dfw/products');
        $main = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB);
        foreach ($products as $product) {
            if ($mod == 'catalog') {
                $product = Mage::getModel('catalog/product')->load($product ['entity_id']);
            }
            $summaryData = Mage::getModel('review/review_summary')->setStoreId($store_id)->load($product->getId());
            $price = ($product->getSpecialPrice()) == null ? ($product->getPrice()) : ($product->getSpecialPrice());
            $regular_price_with_tax = number_format(Mage::helper('directory')->currencyConvert($product->getPrice(), $baseCurrency, $currentCurrency), 2, '.', '');
            $final_price_with_tax = number_format(Mage::helper('directory')->currencyConvert($product->getSpecialPrice(), $baseCurrency, $currentCurrency), 2, '.', '');
                $temp_product = array();
                $options = array();
                $price = array();
        $attributes = $product->getAttributes();
        foreach ($attributes as $attribute) {
                        if( !in_array ( $attribute->getAttributeCode() , array('features' ,'media_gallery') )){
                        $id[] =  $attribute->getAttributeCode($product);
                        $att[] = $attribute->getFrontend()->getValue($product);
                        }
        } $temp_product['orami'] = array_combine($id, $att);
        $gallery_images = $product->getMediaGalleryImages();
        $items = array();
        foreach($gallery_images as $g_image) {
            $items[] = $g_image['url'];
        }
        $temp_product['orami']['media_gallery'] = implode(', ', $items);
            array_push($product_list, $temp_product);
        }
        return $product_list;
    }
}

    public function getCurrencyPrice($price){
        return Mage::helper('core')->currency($price, false, false);
    }

}
