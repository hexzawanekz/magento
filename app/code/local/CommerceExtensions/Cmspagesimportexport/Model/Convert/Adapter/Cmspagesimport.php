<?php
/**
 * Cmspagesimport.php
 * CommerceExtensions @ InterSEC Solutions LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.commerceextensions.com/LICENSE-M1.txt
 *
 * @category   CMS Pages
 * @package    Cmspagesimport
 * @copyright  Copyright (c) 2003-2010 CommerceExtensions @ InterSEC Solutions LLC. (http://www.commerceextensions.com)
 * @license    http://www.commerceextensions.com/LICENSE-M1.txt
 */ 
 
class CommerceExtensions_Cmspagesimportexport_Model_Convert_Adapter_Cmspagesimport extends Mage_Eav_Model_Convert_Adapter_Entity
{
    protected $_stores;

 /**
     *  Init stores
     */
    protected function _initStores ()
    {
        if (is_null($this->_stores)) {
            $this->_stores = Mage::app()->getStores(true, true);
            foreach ($this->_stores as $code => $store) {
                $this->_storesIdCode[$store->getId()] = $code;
            }
        }
    }
 		/**
     * Retrieve store object by code
     *
     * @param string $store
     * @return Mage_Core_Model_Store
     */
    public function getStoreByCode($store)
    {
        $this->_initStores();
        /**
         * In single store mode all data should be saved as default
         */
        if (Mage::app()->isSingleStoreMode()) {
            return Mage::app()->getStore(Mage_Catalog_Model_Abstract::DEFAULT_STORE_ID);
        }

        if (isset($this->_stores[$store])) {
            return $this->_stores[$store];
        }
        return false;
    }
    public function parse()
    {
        $batchModel = Mage::getSingleton('dataflow/batch');
        /* @var $batchModel Mage_Dataflow_Model_Batch */

        $batchImportModel = $batchModel->getBatchImportModel();
        $importIds = $batchImportModel->getIdCollection();

        foreach ($importIds as $importId) {
            //print '<pre>'.memory_get_usage().'</pre>';
            $batchImportModel->load($importId);
            $importData = $batchImportModel->getBatchData();

            $this->saveRow($importData);
        }
    }

    /**
     * Save Customer Review (import)
     *
     * @param array $importData
     * @throws Mage_Core_Exception
     * @return bool
     */
	public function _escape($str)
	{
			$search=array("\\","\0","\n","\r","\x1a","'",'"');
			$replace=array("\\\\","\\0","\\n","\\r","\Z","\'",'\"');
			return str_replace($search,$replace,$str);
	}
    public function saveRow(array $importData)
    {
				
				 $resource = Mage::getSingleton('core/resource');
				 $prefix = Mage::getConfig()->getNode('global/resources/db/table_prefix'); 
				 $write = $resource->getConnection('core_write');
				 $read = $resource->getConnection('core_read');
				 $finalstoreids = array();
				 
				 //CONVERT DATE TIME TO MYSQL DATE TIME
				 if(isset($importData['created_at']) && $importData['created_at'] !="") {
				 	$CreatedDateTime = strtotime($importData['created_at']);	
				 } else {
				 	$CreatedDateTime = strtotime(date("m/d/Y, g:i a"));	
				 }
				 
				 if(isset($importData['status']) && $importData['status'] !="") {
				 	 if($importData['status'] == "Enabled") {
						$isactive = "1";	
					 } else {
						$isactive = "0";	
					 }
				 }
				 if(isset($importData['layout_update_xml']) && $importData['layout_update_xml'] !="") {
				 	$layout_update_xml = trim($importData['layout_update_xml']);	
				 } else {
					$layout_update_xml = "NULL";	
				 }
				 if(isset($importData['custom_theme']) && $importData['custom_theme'] !="") {
				 	$custom_theme = "'".$importData['custom_theme']."'";	
				 } else {
					$custom_theme = "NULL";	
				 }
				 if(isset($importData['custom_root_template']) && $importData['custom_root_template'] !="") {
				 	$custom_root_template = trim($importData['custom_root_template']);	
				 } else {
					$custom_root_template = "";	
				 }
				 if(isset($importData['custom_layout_update_xml']) && $importData['custom_layout_update_xml'] !="") {
				 	$custom_layout_update_xml = "'".$importData['custom_layout_update_xml']."'";	
				 } else {
					$custom_layout_update_xml = "NULL";	
				 }
				 if(isset($importData['custom_theme_from']) && $importData['custom_theme_from'] !="") {
				 	$custom_theme_from = "'".$importData['custom_theme_from']."'";	
				 } else {
					$custom_theme_from = "NULL";	
				 }
				 if(isset($importData['custom_theme_to']) && $importData['custom_theme_to'] !="") {
				 	$custom_theme_to = "'".$importData['custom_theme_to']."'";	
				 } else {
					$custom_theme_to = "NULL";	
				 }
				 
				 $finalpagetitle = str_replace('"', "'", $importData['page_title']);
				 $finalcontentheading = str_replace('"', "'", $importData['content_heading']);
				 $finalcontent = str_replace('"', "'", $importData['content']);
				 #$finalcontent = $importData['content'];
				 $finalmetadescription = str_replace('"', "'", $importData['meta_description']);
				 $finalmetakeyword = str_replace('"', "'", $importData['meta_keyword']);
				 
				 if(isset($importData['storeview'])) {
				 	// INSERTS CMS PAGE STORE DATA
					$delimiteddata = explode(',',$importData['storeview']);
					foreach ($delimiteddata as $individualstoreName) {
				 
					 $store = $this->getStoreByCode($individualstoreName);
					 $finalstoreids[] = $store->getId();
					 }
				 }
				 
				 
				 //UPDATE FUCTIONALITY ADDED
				 $select_qry2 = "SELECT identifier FROM ".$prefix."cms_page WHERE identifier = '".$importData['url_key']."'";
				 $rows2 = $read->fetchAll($select_qry2);
				 if(count($rows2) >0) {
				 
				     foreach($rows2 as $data2)
				 	 { 
					 	#echo "DATE: " . date("Y-m-d H:i:s", $CreatedDateTime);
						$dateforimport = date("Y-m-d H:i:s", $CreatedDateTime);
						$finaldateforimport = (string)$dateforimport;
					 	$write_qry_update = $write->query("UPDATE ".$prefix."cms_page SET title = \"".addslashes($finalpagetitle)."\", root_template = \"".$importData['layout']."\", meta_keywords = \"".addslashes($finalmetakeyword)."\", meta_description = \"".addslashes($finalmetadescription)."\", content_heading = \"".$finalcontentheading."\", content = \"".$this->_escape($finalcontent)."\", is_active = \"".$isactive."\", layout_update_xml = \"".$layout_update_xml."\", custom_theme = ".$custom_theme.", custom_root_template = \"".$custom_root_template."\", custom_layout_update_xml = ".$custom_layout_update_xml.", custom_theme_from = ".$custom_theme_from.", custom_theme_to = ".$custom_theme_to.", update_time = \"".$finaldateforimport."\" WHERE identifier = \"".$importData['url_key']."\"");
						#echo "UPDATE `".$prefix."cms_page` SET title = \"".addslashes($finalpagetitle)."\", root_template = '".$importData['layout']."', meta_keywords = \"".addslashes($finalmetakeyword)."\", meta_description = \"".addslashes($finalmetadescription)."\", content_heading = \"".addslashes($finalcontentheading)."\", content = \"".addslashes($finalcontent)."\", update_time = \"".date("Y-m-d H:i:s", $CreatedDateTime)."\", is_active = '".$isactive."', layout_update_xml = \"".addslashes($layout_update_xml)."\", custom_theme = ".$custom_theme.", custom_root_template = '".$custom_root_template."', custom_layout_update_xml = ".$custom_layout_update_xml.", custom_theme_from = ".$custom_theme_from.", custom_theme_to = ".$custom_theme_to." WHERE identifier = '".$importData['url_key']."'";
						
					 }
				
				 } else {	 
				// INSERTS MAIN CMS PAGE DATA
				#$dateforimport = date("Y-m-d H:i:s", $CreatedDateTime);
				#$finaldateforimport = (string)$dateforimport;
				#$write_qry_insert = $write->query("Insert into `".$prefix."cms_page` (title,root_template,meta_keywords,meta_description,identifier,content_heading,content,is_active,sort_order,layout_update_xml,custom_theme,custom_root_template,custom_layout_update_xml,custom_theme_from,custom_theme_to,creation_time,update_time) VALUES (\"".addslashes($finalpagetitle)."\",\"".$importData['layout']."\",\"".addslashes($finalmetakeyword)."\",\"".addslashes($finalmetadescription)."\",\"".$importData['url_key']."\",\"".$finalcontentheading."\",\"".$this->_escape($finalcontent)."\",\"".$isactive."\",\"0\",\"".$layout_update_xml."\",".$custom_theme.",\"".$custom_root_template."\",".$custom_layout_update_xml.",".$custom_theme_from.",".$custom_theme_to.",\"".$finaldateforimport."\",\"".$finaldateforimport."\")");
				 
				#echo "Insert into `".$prefix."cms_page` (title,root_template,meta_keywords,meta_description,identifier,content_heading,content,is_active,sort_order,layout_update_xml,custom_theme,custom_root_template,custom_layout_update_xml,custom_theme_from,custom_theme_to,creation_time,update_time) VALUES (\"".addslashes($finalpagetitle)."\",\"".$importData['layout']."\",\"".addslashes($finalmetakeyword)."\",\"".addslashes($finalmetadescription)."\",\"".$importData['url_key']."\",\"".$finalcontentheading."\",\"".$finalcontent."\",\"".$isactive."\",\"0\",\"".$layout_update_xml."\",".$custom_theme.",'".$custom_root_template."',".$custom_layout_update_xml.",".$custom_theme_from.",".$custom_theme_to.",".$finaldateforimport.",".$finaldateforimport.")";
				 
				$newBlock = Mage::getModel('cms/page')
							->setTitle(urldecode($importData['page_title']))
							->setContent(urldecode($importData['content']))
							->setIdentifier(urldecode($importData['url_key']))
							->setContentHeading(urldecode($importData['content_heading']))
							->setMetaKeywords(urldecode($importData['meta_keyword']))
							->setMetaDescription(urldecode($importData['meta_description']))
							->setCustomLayoutUpdateXml(urldecode($custom_layout_update_xml))
							->setIsActive($isactive)
							->setStores($finalstoreids)
							->setRootTemplate($importData['layout'])
							->setLayoutUpdateXml($layout_update_xml)
							->setCustomRootTemplate($custom_root_template)
							->setCustomTheme($custom_theme)
							->setCustomThemeFrom($custom_theme_from)
							->setCustomThemeTo($custom_theme_to)
							->save();
				
				$lastInsertId = $write->fetchOne('SELECT last_insert_id()'); 
				 
				// INSERTS CMS PAGE STORE DATA
				#$delimiteddata = explode(',',$importData['storeview']);
				#foreach ($delimiteddata as $individualstoreName) {
				 
				#$store = $this->getStoreByCode($individualstoreName);
				 /*
				 $allstores = Mage::app()->getStores(true, true);
				 foreach ($allstores as $code => $store) {
						$storesIdCode[] = $code;
						echo "CODE ID: " . $code;
				 }
				 $write->query("Insert into `".$prefix."cms_page_store` (page_id,store_id) VALUES ('".$lastInsertId."','".$store->getId()."')");
				 */
				#}
				
			}//if count is over 0
        return true;
    }

}

?>