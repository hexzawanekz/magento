<?php
/**
 * Cmspagesexport.php
 * CommerceExtensions @ InterSEC Solutions LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.commerceextensions.com/LICENSE-M1.txt
 *
 * @category   CMS Pages
 * @package    Cmspagesexport
 * @copyright  Copyright (c) 2003-2010 CommerceExtensions @ InterSEC Solutions LLC. (http://www.commerceextensions.com)
 * @license    http://www.commerceextensions.com/LICENSE-M1.txt
 */ 
 
class CommerceExtensions_Cmspagesimportexport_Model_Convert_Parser_Cmspagesexport extends Mage_Eav_Model_Convert_Parser_Abstract
{
/**
     * @deprecated not used anymore
     */
    public function parse()
    {
			return $this;
		}
 /**
     * Unparse (prepare data) loaded categories
     *
     * @return Mage_Catalog_Model_Convert_Adapter_Customerreviewexport
     */
    public function unparse()
    {
				 $ByStoreID = $this->getVar('store');
				 $recordlimit = $this->getVar('recordlimit');
				 $resource = Mage::getSingleton('core/resource');
				 $prefix = Mage::getConfig()->getNode('global/resources/db/table_prefix');
				 $read = $resource->getConnection('core_read');
				 $row = array();
				 
				 
				if($ByStoreID != "") {
				 $select_qry = "SELECT ".$prefix."cms_page.page_id, ".$prefix."cms_page.title, ".$prefix."cms_page.root_template, ".$prefix."cms_page.meta_keywords, ".$prefix."cms_page.meta_description, ".$prefix."cms_page.identifier, ".$prefix."cms_page.content_heading, ".$prefix."cms_page.content, ".$prefix."cms_page.creation_time, ".$prefix."cms_page.update_time, ".$prefix."cms_page.is_active, ".$prefix."cms_page.layout_update_xml, ".$prefix."cms_page.custom_theme, ".$prefix."cms_page.custom_root_template, ".$prefix."cms_page.custom_layout_update_xml, ".$prefix."cms_page.custom_theme_from, ".$prefix."cms_page.custom_theme_to FROM ".$prefix."cms_page INNER JOIN ".$prefix."cms_page_store ON ".$prefix."cms_page_store.page_id = ".$prefix."cms_page.page_id WHERE ".$prefix."cms_page_store.store_id = '".$ByStoreID."'";
				 } else {
				 $select_qry = "SELECT ".$prefix."cms_page.page_id, ".$prefix."cms_page.title, ".$prefix."cms_page.root_template, ".$prefix."cms_page.meta_keywords, ".$prefix."cms_page.meta_description, ".$prefix."cms_page.identifier, ".$prefix."cms_page.content_heading, ".$prefix."cms_page.content, ".$prefix."cms_page.creation_time, ".$prefix."cms_page.update_time, ".$prefix."cms_page.is_active, ".$prefix."cms_page.layout_update_xml, ".$prefix."cms_page.custom_theme, ".$prefix."cms_page.custom_root_template, ".$prefix."cms_page.custom_layout_update_xml, ".$prefix."cms_page.custom_theme_from, ".$prefix."cms_page.custom_theme_to FROM ".$prefix."cms_page";
				 }
				 
					$rows = $read->fetchAll($select_qry);
					foreach($rows as $data)
					 { 
					 
					 			$row["created_at"] = $data['creation_time'];
					 			$row["page_title"] = $data['title'];
					 			$row["url_key"] = $data['identifier'];
								$select__store_ids_qry = "SELECT store_id FROM ".$prefix."cms_page_store WHERE page_id = '".$data['page_id']."'";
								
								$storeviewsforexport="";
				        		$finalstoreviewsforexport="";
								
								$storeidrows = $read->fetchAll($select__store_ids_qry);
								foreach($storeidrows as $datastoreid)
								{ 
									 $allstores = Mage::app()->getStores(true, true);
									 foreach ($allstores as $code => $store) {
											$storesIdCode[] = $code;
											if($store->getId() == $datastoreid['store_id']) {
												$storeviewsforexport .= $code . ",";
												#echo "CODE ID: " . $store->getId();
											}
									 }
								}
								$finalstoreviewsforexport = substr_replace($storeviewsforexport,"",-1);
					 			$row["storeview"] = $finalstoreviewsforexport;
								
								if($data['is_active'] == "1") {
								 	$row["status"] = "Enabled";
								} else {
								 	$row["status"] = "Disabled";
								}
					 			$row["content_heading"] = $data['content_heading'];
					 			$row["content"] = $data['content'];
					 			$row["layout"] = $data['root_template'];
					 			$row["meta_keyword"] = $data['meta_keywords'];
					 			$row["meta_description"] = $data['meta_description'];
					 			$row["layout_update_xml"] = $data['layout_update_xml'];
					 			$row["custom_theme"] = $data['custom_theme'];
					 			$row["custom_root_template"] = $data['custom_root_template'];
					 			$row["custom_layout_update_xml"] = $data['custom_layout_update_xml'];
					 			$row["custom_theme_from"] = $data['custom_theme_from'];
					 			$row["custom_theme_to"] = $data['custom_theme_to'];
								
					 			$batchExport = $this->getBatchExportModel()
                ->setId(null)
                ->setBatchId($this->getBatchModel()->getId())
                ->setBatchData($row)
                ->setStatus(1)
                ->save();
					 }
					 
					 
        return $this;
		}
}

?>