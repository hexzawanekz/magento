<?php

/**
 * LINE Pay
 * Model_Api
 *
 * @category    LINE
 * @package     LINEPay
 * @copyright   Copyright (c) 2015 LINEPay
 */
class LINE_LINEPay_Model_Api {

    /**
     * LINEPay Reserve API 요청
     *
     * @param	Mage_Sales_Model_Order $order
     * @return	stdClass
     */
    public function reserve($order) {
        $helper			= Mage::helper('linepay');
        $currency		= $order->getBaseCurrencyCode();
        $stdAmount		= $helper->getStandardized($order->getBaseTotalDue(), $currency);
        $uri	= $helper->getURI(LINE_LINEPay_Helper_Data::REQUEST_TYPE_RESERVE);
        $body	= array (
            'productName' => $order->getIncrementId(),
            'productImageUrl' => Mage::getDesign()->getSkinUrl('images/orami-150.png'),
            'amount' => $stdAmount,
            'currency' => $currency,
            'confirmUrl' => Mage::getUrl(LINE_LINEPay_Helper_Data::ENTRANCE_URI, array('_query' => 'type=confirm')),
            'confirmUrlType' => 'CLIENT',
            'cancelUrl' => Mage::getUrl(LINE_LINEPay_Helper_Data::ENTRANCE_URI, array('_query' => 'type=cancel')),
            'orderId' => $order->getIncrementId(),
            'payType' => 'NORMAL',
            'capture' => 'true',
        );
        return self::requestLINEPayAPI($uri, $body);
    }

    /**
     * LINEPay Confirm API 요청
     *
     * @param	Mage_Sales_Model_Order $order
     * @param	string $transactionId
     * @return	stdClass
     */
    public function confirm($order, $transactionId) {
        $helper		= Mage::helper('linepay');
        $currency	= $order->getBaseCurrencyCode();
        $stdAmount	= $helper->getStandardized($order->getBaseTotalDue(), $currency);

        $uri	= $helper->getURI(LINE_LINEPay_Helper_Data::REQUEST_TYPE_CONFIRM, array('transactionId' => $transactionId));
        $body	= array(
            'amount'	=> $stdAmount,
            'currency'	=> $currency,);


        try{
                // store
                $store = $order->getStore();
                $storeId = $order->getStoreId();

                // email info
                $emailTemplateId = Mage::getStoreConfig(Mage_Sales_Model_Order::XML_PATH_EMAIL_TEMPLATE);
                /** @var Mage_Core_Model_Email_Template $mailTemplateModel */
                $mailTemplateModel = Mage::getModel('core/email_template')->load($emailTemplateId);
                $customerEmail = $order->getCustomerEmail();
                $customer = Mage::getModel('customer/customer')->load($order->getCustomerID());

                $sender = array(
                    'name' 	=> 'Orami Sales',
                    'email' => 'support@orami.co.th',
                );

                /** @var Mage_Core_Model_Translate $translate */
                $translate = Mage::getSingleton('core/translate');

                $translate->setTranslateInline(false);

                $mailTemplateModel->setDesignConfig(array('area' => 'frontend', 'store' => $storeId))
                    ->sendTransactional(
                        $mailTemplateModel,
                        $sender,
                        $customerEmail,
                        '',
                        array(
                            'store' => $store,
                            'order' => $order,
                            'customer' => $customer,
                        ));
                $translate->setTranslateInline(true);
        }catch(Exception $e){
            Mage::log($e, null, 'emst_invalid_email.log');
        }

        return self::requestLINEPayAPI($uri, $body, 40);

    }

    /**
     * LINEPay Refund API 요청
     *
     * @param	string $transactionId
     * @param	string $stdRefundAmount
     * @param	boolean $force
     * @return	stdClass
     */
    public function refund($transactionId, $stdRefundAmount, $force = false) {

        $body = array( 'refundAmount' => $stdRefundAmount);
        if ($force) {
            $body = null;
        }

        return self::requestLINEPayAPI(
            Mage::helper('linepay')->getURI(LINE_LINEPay_Helper_Data::REQUEST_TYPE_REFUND, array('transactionId' => $transactionId)),
            $body);

    }

    /**
     * 제공받은 정보로 LINE Pay API를 호출한다.
     *
     * LINE Pay에 API를 호출할 수 있도록 Header 정보를 구성한다.
     *
     * @param	string $url
     * @param	array|null $body
     * @param	number $timeout
     * @throws	LINEPayException
     * @return	array
     */
    protected function requestLINEPayAPI($uri, $body = null, $timeout = 20) {

        $helper = Mage::helper('linepay');

        try {
            $channelInfo	= $helper->getChannelInfo();
            $host			= $helper->getHost();
            $headers = array (
                'Content-Type: ' . 'application/json; charset=UTF-8',
                'X-LINE-ChannelId: ' . $channelInfo['id'],
                'X-LINE-ChannelSecret: ' . $channelInfo['secretKey'],
            );

            // body가 존재할 경우에만 추가
            if (is_array($body)) {
                $body = json_encode($body);
            }

            $response = $this->requestHTTP($host . $uri, $body, array (
                'headers' => $headers,
                'timeout' => $timeout,
            ));

            $responseBody = json_decode($response);
            $returnCode = $responseBody->returnCode;

            // LINE Pay API 응답 결과가 성공이 아닌 경우에만 LINE_LINEPay_Model_Exception 발생
            if ($returnCode != '0000') {

                throw Mage::exception(
                    'LINE_LINEPay_Model',
                    $helper->addNewLine(
                        sprintf('[API.request][return code: %s] - %s',
                            $returnCode, $helper->__('Unable to make payment due to a temporary error. Please try again.'))) .
                    $helper->addNewLine($response),
                    $returnCode);
            }


            return $responseBody->info;
        }
        catch (LINE_LINEPay_Model_Exception $e) {
            throw $e;
        }
        catch (Exception $e) {
            Mage::throwException($helper->addNewLine('[API.request]' . $e->getMessage()));
        }

    }

    /**
     * 제공받은 정보로 HTTP 요청을 호출하고 결과를 반환한다.
     *
     * @param	string $url
     * @param	string $body
     * @param	array $options
     * @throws	LINEPayException
     * @return	$httpBody
     */
    protected function requestHTTP($url, $body, $options = array()) {
        $helper = Mage::helper('linepay');

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $options['headers']);
        curl_setopt($ch, CURL_HTTP_VERSION_1_1, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_VERBOSE, true);
        curl_setopt($ch, CURLOPT_HEADER, true);

        curl_setopt($ch, CURLOPT_TIMEOUT, $options['timeout']);
        $response = curl_exec($ch);

        $statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $contentSize = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        curl_close($ch);

        $httpHeader = substr($response, 0, $contentSize);
        $httpBody = substr($response, $contentSize);

        if ($statusCode != 200 || empty($httpBody)) {
            Mage::throwException(sprintf('[status: %d] - %s',
                $statusCode,
                $helper->__('Unable to make payment due to a temporary error. Please try again.')));
        }

        return $httpBody;
    }

}