<?php

class IWD_OnepageCheckout_Block_Onepage_Billship extends Mage_Checkout_Block_Onepage_Billing
{
    public function getBillAddress()
    {
        return $this->getQuote()->getBillingAddress();
    }
    
    public function getShipAddress()
    {
        return $this->getQuote()->getShippingAddress();
    }

    public function getCustomerBillAddr()
    {
        // 26-03-2013 comment the line below and call to my function to change address from select option to radio button
    	//return $this->buildCustomerAddress('billing');

        return $this->buildCustomerAddressRadio('billing');
    }
    
    public function getBillingCountriesSelectBox()
    {
    	return $this->buildCountriesSelectBox('billing');
    }

    public function getCustomerShipAddr()
    {
    	return $this->buildCustomerAddress('shipping');
    }

    public function getShippingCountriesSelectBox()
    {
    	return $this->buildCountriesSelectBox('shipping');
    }

    // 26-03-2013 this is new function for address on checkout page. it return radio button (address)
    public function buildCustomerAddressRadio($addr_type)
    {
        if ($this->isCustomerLoggedIn()) {
            $options = array();
            foreach ($this->getCustomer()->getAddresses() as $address) {
                $options[] = array(
                    'value'=>$address->getId(),
                    'label'=>$address->format('oneline')
                );
            }

            switch($addr_type)
            {
                case 'billing':
                    $address = $this->getCustomer()->getPrimaryBillingAddress();
                    break;
                case 'shipping':
                    $address = $this->getCustomer()->getPrimaryShippingAddress();
                    break;
            } 

            if ($address) {
                $addressId = $address->getId();
            } else {
                if($addr_type == 'billing')
                    $obj    = $this->getBillAddress();
                else
                    $obj    = $this->getShipAddress();

                $addressId = $obj->getId();
            }

            $return_html = '';

            foreach($options as $key=>$value){
                $return_html .= '<br><input type="radio" id="'.$addr_type.'_address_'.$value['value'].'" name="'.$addr_type.'_address_id_radio" value="'.$value['value'].'" onclick="billing.newAddress(false);" > '.$value['label'];
            }

            $return_html .= '<br><br><a href="javascript:void(0);" id="add_new_address_link" onclick="billing.newAddress(true);">Add new address</a>';

            return $return_html;
        }
        return '';
    }
    // 26-03-2013 this is new function for address on checkout page. it return radio button (address)

    public function buildCustomerAddress($addr_type)
    {
        if ($this->isCustomerLoggedIn()) {
            $options = array();
            foreach ($this->getCustomer()->getAddresses() as $address) {
                $options[] = array(
                    'value'=>$address->getId(),
                    'label'=>$address->format('oneline')
                );
            }

        	switch($addr_type)
        	{
        		case 'billing':
        			$address = $this->getCustomer()->getPrimaryBillingAddress();
        			break;
        		case 'shipping':
        			$address = $this->getCustomer()->getPrimaryShippingAddress();
        			break;
        	} 

            if ($address) {
                $addressId = $address->getId();
            } else {
            	if($addr_type == 'billing')
            		$obj	= $this->getBillAddress();
            	else
            		$obj	= $this->getShipAddress();

                $addressId = $obj->getId();
            }

            $select = $this->getLayout()->createBlock('core/html_select')
            							->setId("{$addr_type}_customer_address")->setName("{$addr_type}_address_id")
            							->setValue($addressId)->setOptions($options)
										->setExtraParams('onchange="'.$addr_type.'.newAddress(!this.value)"')
										->setClass('customer_address');

            $select->addOption('', Mage::helper('checkout')->__('New Address'));            
            return $select->getHtml();
        }
        return '';
    }

    public function buildCountriesSelectBox($addr_type)
    {
		if($addr_type == 'billing')
			$obj	= $this->getBillAddress();
		else
			$obj	= $this->getShipAddress();
    	
        $countryId = $obj->getCountryId();
        if (is_null($countryId)) {
            $countryId = Mage::getStoreConfig('general/country/default');
        }
        $select = $this->getLayout()->createBlock('core/html_select')
        							->setId("{$addr_type}:country_id")->setName("{$addr_type}[country_id]")
									->setValue($countryId)->setOptions($this->getCountryOptions())
									->setTitle(Mage::helper('checkout')->__('Country'))
									->setClass('validate-select');

		if($addr_type == 'shipping')
			$select->setExtraParams('onchange="shipping.setSameAsBilling(false);"');

        return $select->getHtml();        
    }
}
