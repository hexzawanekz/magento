<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category   Mage
 * @package    Sanoga_AEON
 * @author	   
 * 
 */


/**
 * AEON Standard Checkout Controller
 *
 * @category   Mage
 * @package    Sanoga_AEON
 * @author      Magento Core Team <core@magentocommerce.com>
 */
      
 
class Sanoga_AEON_AEONController extends Mage_Core_Controller_Front_Action
{
    
    /**
     * Order instance
     */
    protected $_order;

    /**
     *  Get order
     *
     *  @return	  Mage_Sales_Model_Order
     */
    public function getOrder()
    {
        if ($this->_order == null) {
        }
        return $this->_order;
    }

    protected function _expireAjax()
    {
        if (!Mage::getSingleton('checkout/session')->getQuote()->hasItems()) {
            $this->getResponse()->setHeader('HTTP/1.1','403 Session Expired');
            exit;
        }
    }

    /**
     * Get singleton with AEON strandard order transaction information
     *
     * @return Sanoga_AEON_Model_Standard
     */
    public function getStandard()
    {
        return Mage::getSingleton('AEON/standard');
    }

    /**
     * When a customer chooses AEON on Checkout/Payment page
     *
     */
    public function redirectAction()
    {
		
		$session = Mage::getSingleton('checkout/session');
		$session->setAEONStandardQuoteId($session->getQuoteId());
		$order = Mage::getModel('sales/order');
		$order->load(Mage::getSingleton('checkout/session')->getLastOrderId());
		$order->sendNewOrderEmail();
		$order->save();
		
		$this->getResponse()->setBody($this->getLayout()->createBlock('AEON/form_redirect')->toHtml());
		$session->unsQuoteId();

    }

	/* custom by NUS */
	#######
	#######
	#######
	#######
    public function getstatusAction()
    {
		
	/*
	$session = Mage::getSingleton('checkout/session');
		$session->setAEONStandardQuoteId($session->getQuoteId());
		$order = Mage::getModel('sales/order');
		$order->load(Mage::getSingleton('checkout/session')->getLastOrderId());
		$order->sendNewOrderEmail();
		$order->save();
		
		$this->getResponse()->setBody($this->getLayout()->createBlock('AEON/form_redirect')->toHtml());
		$session->unsQuoteId();
*/

    }
    #########
    #########
    #########
    #########
    /* end custom by NUS */
    
    
    /**
     * When a customer cancel payment from AEON.
     */
    public function cancelAction($AEONCode,$AEONState)
    {
        $session = Mage::getSingleton('checkout/session');
        $session->setQuoteId($session->getAEONStandardQuoteId(true));

        // cancel order
        if ($session->getLastRealOrderId()) {
            $order = Mage::getModel('sales/order')->loadByIncrementId($session->getLastRealOrderId());
            if ($order->getId()) {
            	if(isset($_REQUEST["invoiceNo"])) $PaymentRst_InvoiceNo =$_REQUEST["invoiceNo"];
				if(!empty($PaymentRst_InvoiceNo)) $PaymentRst_InvoiceNo = str_replace("SANO", "", $PaymentRst_InvoiceNo); 
                //$order->cancel()->save();
				$state = Mage_Sales_Model_Order::STATE_CANCELED;
				$state = 'payment_fail_aeon';
				$message="AEON Return Code : " .  $AEONCode . ", State : ". $AEONState;
				$order->setStatus($state,true,$message);
/* 				$order->setState($state, true, $message); */
				$order->save();
/* 				mail('peeracha.n@sanoga.com', 'CANCEL ['.$PaymentRst_InvoiceNo."] ".date("d M Y H:i:s"), ''); */
				
/* 					$MerchantInvoiceNo = $PaymentRst_InvoiceNo; */
/* 					$order = Mage::getModel('sales/order'); */
/* 					$order->loadByIncrementId($MerchantInvoiceNo); */
						
/* 					$state = Mage_Sales_Model_Order::STATE_CANCELED; */
/* 					$message="AEON Return Code : " .  $AEONCode . ", State : ". $AEONState; */
/* 					$order->setState($state, true, $message); */
/* 					$order->save(); */
					$mail_msg = '$PaymentRst_ResponseCode '. $PaymentRst_ResponseCode."\n";
					$mail_msg .=  '$PaymentRst_Status '.$PaymentRst_Status."\n";
					$mail_msg .=  '$PaymentRst_fraudCode '.$PaymentRst_fraudCode."\n";
					
					mail('peeracha.n@sanoga.com', 'CANCEL ['.$order->getId()."] ".date("d M Y H:i:s"), $mail_msg); 

            }
            
        }

		Mage::getSingleton('checkout/session')->addError("AEON Payment has been cancelled and the transaction has been declined.");
		$this->_redirect('checkout/cart');
		
    }

    /**
     * when AEON returns
     * The order information at this point is in POST
     * variables.  However, you don't want to "process" the order until you
     * get validation from the IPN.
     */
    public function  successAction()
    {
     //return 'status';
     /*
	if (!$this->getRequest()->isPost()) {
        	throw new Exception(' Wrong request type:  should be Post.', 10);
        }
*/


/*

$paymentID = “”, $respCode = “”, $pan = “”, $amount = “”, $invoiceNo = “”, $tranRef = “”, $ approvalCode = “”, $eci = “”, $dateTime = “”, $status = “”; $paymentID = _REQUEST[“paymentID”];
$respCode = _REQUEST[“respCode”];
$pan = _REQUEST[“pan””];
*/


//echo $paymentID = $_REQUEST['paymentID'];
//echo '<br />';
        $status = true;

		$response = $this->getRequest()->getPost();		 
		// zend_debug::dump($response);die;
		if (empty($response))  {
            $status = false;
        //	throw new Exception('Response doesn\'t contain GET /POST elements.', 20);
         
        }
		
		
		#######
		####### GET PARAMETER
		#######
		#######
		$PaymentRst_ResponseCode = ""; $PaymentRst_ApproveCode = ""; 
		$PaymentRst_InvoiceNo=""; $PaymentRst_Pan=""; $PaymentRst_ExpiryDate = "";
		
		$PaymentRst_Amount= ""; $PaymentRst_Ref1 = ""; $PaymentRst_Ref2 = ""; 
		
		$PaymentRst_Ref3 = ""; $PaymentRst_Ref4 = ""; $PaymentRst_Ref5 = ""; $PaymentRst_Browser = "";
		
		$PaymentRst_Reference = "";  $PaymentRst_TimeStamp = ""; $PaymentRst_CardType = "";
		$PaymentRst_CreditcardNo = "";  $PaymentRst_Warning = ""; $PaymentRst_Checksum = "";
        
		if(isset($_REQUEST["paymentID"])) $PaymentRst_PaymentCode =$_REQUEST["paymentID"];
		if(isset($_REQUEST["respCode"])) $PaymentRst_ResponseCode =$_REQUEST["respCode"];
		
		if(isset($_REQUEST["approvalCode"])) $PaymentRst_ApproveCode =$_REQUEST["approvalCode"];
		
/* 		if(isset($_REQUEST["invoiceNo"])) $PaymentRst_InvoiceNo=(int)$_REQUEST["invoiceNo"]; */
		if(isset($_REQUEST["pan"])) $PaymentRst_Pan =$_REQUEST["pan"];
		if(isset($_REQUEST["invoiceNo"])) $PaymentRst_InvoiceNo =$_REQUEST["invoiceNo"];
		if(!empty($PaymentRst_InvoiceNo)) $PaymentRst_InvoiceNo = str_replace("SANO", "", $PaymentRst_InvoiceNo); 
		
		if(isset($_REQUEST["dateTime"])) $PaymentRst_ExpiryDate=$_REQUEST["dateTime"];
		
		if(isset($_REQUEST["amount"])) $PaymentRst_Amount =$_REQUEST["amount"];
		if(isset($_REQUEST["tranRef"])) $PaymentRst_Reference =$_REQUEST["tranRef"];
		/*new */
		if(isset($_REQUEST["eci"])) $PaymentRst_ECI =$_REQUEST["eci"];
		if(isset($_REQUEST["status"])) $PaymentRst_Status=$_REQUEST["status"];
		if(isset($_REQUEST["fraudCode"])) $PaymentRst_fraudCode =$_REQUEST["fraudCode"];
		/*end new */
		
		
		if(isset($_REQUEST["userDefined1"])) $PaymentRst_Ref1 =$_REQUEST["userDefined1"];
		if(isset($_REQUEST["userDefined2"])) $PaymentRst_Ref2 =$_REQUEST["userDefined2"];
		if(isset($_REQUEST["userDefined3"])) $PaymentRst_Ref3 =$_REQUEST["userDefined3"];
		if(isset($_REQUEST["userDefined4"])) $PaymentRst_Ref4 =$_REQUEST["userDefined4"];
		if(isset($_REQUEST["userDefined5"])) $PaymentRst_Ref5 =$_REQUEST["userDefined5"];
		
		
		if(isset($_REQUEST["browser_info"])) $PaymentRst_Browser =$_REQUEST["browser_info"];
		
				
		
		if(isset($_REQUEST["request_timestamp"])) $PaymentRst_TimeStamp =$_REQUEST["request_timestamp"];
		if(isset($_REQUEST["CARDTYPE"])) $PaymentRst_CardType =$_REQUEST["CARDTYPE"];
		if(isset($_REQUEST["CREDITCARDNO"])) $PaymentRst_CreditcardNo =$_REQUEST["CREDITCARDNO"];
		if(isset($_REQUEST["WARNINGLIGHT"])) $PaymentRst_Warning =$_REQUEST["WARNINGLIGHT"];
		
		if(isset($_REQUEST["hashValue"])) $PaymentRst_Checksum =$_REQUEST["hashValue"];
		
		
		#########
		#########
		######### end GET PARAMETER
		#########
		#########
		
		//=> AEON Return Data
		$PaymentRst_ReturnData = "";
		
			
		$PaymentRst_ReturnData .= "\n- Response code = ". $PaymentRst_ResponseCode  ."\n" . 
				"- Approval Code = ". $PaymentRst_ApproveCode  ."\n" . 
				"- Invoice Number = ". $PaymentRst_InvoiceNo  ."\n" . 
				"- Last 4 digits of the Card Number = ". $PaymentRst_Pan  ."\n" . 
				"- Expiry Date = ". $PaymentRst_ExpiryDate  ."\n" . 
				"" .
				"- Leading zero amount without any decimal numbers = ". $PaymentRst_Amount ."\n" . 
				"- Merchant Ref#1 = ". $PaymentRst_Ref1 ."\n" . 
				"- Merchant Ref#2 = ". $PaymentRst_Ref2  ."\n" . 
				"- Merchant Ref#3 = ". $PaymentRst_Ref3  ."\n" . 
				"- Client Browser = ". $PaymentRst_Browser  ."\n" . 
				"" . 
				"- Reference Code = ". $PaymentRst_Reference  ."\n" . 
				"- Transaction Date and Time = ". $PaymentRst_TimeStamp ."\n" . 
				"- Card type information = ". $PaymentRst_CardType  ."\n" . 
				"- Masked Credit Card information = ". $PaymentRst_CreditcardNo ."\n" . 
				"- Fraud Warning Light = ". $PaymentRst_Warning  ."\n" . 
				"" .
				"- Hash Value = ". $PaymentRst_Checksum  ."\n"; 
				
		
		$PaymentRst_ReturnData .= "";
		
		///echo $PaymentRst_ReturnData;
				
		 
		 
		 
		 
		
		if ($PaymentRst_ResponseCode !=="") {
			
			if ($PaymentRst_InvoiceNo !== "") {
				

				if ($PaymentRst_ResponseCode == "00") {
					//mail('peeracha.n@sanoga.com', 'Response from AEON SUCCESS ['.$PaymentRst_InvoiceNo.'] '.date("d M Y H:i:s"), $PaymentRst_ReturnData);
					//=> Success Code 
/* 					$MerchantInvoiceNo = substr($PaymentRst_InvoiceNo,-9); */
					$MerchantInvoiceNo = $PaymentRst_InvoiceNo;
		
					$order = Mage::getModel('sales/order');
					$order->loadByIncrementId($MerchantInvoiceNo);
					/* set status to complete payment by AEON nus*/
					/* end set status to complete payment by AEON */
					
					//$state = Mage::getStoreConfig('payment/AEON/payment_success_status');
					$state = Mage_Sales_Model_Order::STATE_PROCESSING;
					$state = 'payment_success_aeon';
/* 					$message=Mage::helper('AEON')->__('Your payment is authorized by AEON ('. $PaymentRst_ReturnData .').'); */
					$message=Mage::helper('AEON')->__('Your payment is authorized by AEON.');
					$order->setStatus($state,true,$message);
/* 				  	$order->setState($state, true, $message); */
				  	
				  	//$order->setState("processing", true, $message);
					//$order->setStatus("processing");
									
					//Mage_Sales_Model_Order::STATE_COMPLETE
					$order->save();
					$session = Mage::getSingleton('checkout/session');
					$session->setQuoteId($session->getAEONStandardQuoteId(true));
					
 
					
					/**
					* set the quote as inactive after back from AEON
					*/
					
					//Mage::getSingleton('checkout/session')->getQuote()->setIsActive(false)->save(); 
					//$this->_redirect('checkout/onepage/success', array('_secure'=>true)); 
				} else {
					//mail('peeracha.n@sanoga.com', 'Response from AEON REJECT'.date("d M Y H:i:s"), $PaymentRst_ReturnData);
					//=> Return Result Code
					//$this->getCheckout()->setAEONErrorMessage('AEON not affect the transaction back.('. $PaymentRst_ReturnData .')'  );   
					//$this->cancelAction($PaymentRst_ReturnData,1);
					

					$MerchantInvoiceNo = $PaymentRst_InvoiceNo;
					$order = Mage::getModel('sales/order');
					$order->loadByIncrementId($MerchantInvoiceNo);
						
					$state = Mage_Sales_Model_Order::STATE_CANCELED;
					$state = 'payment_fail_aeon';
					$message="AEON Return Code : " .  $AEONCode . ", State : ". $AEONState;
/* 					$order->setState($state, true, $message);  */
					$order->setStatus($state,true,$message);
					$order->save();
					$mail_msg = '$PaymentRst_ResponseCode '. $PaymentRst_ResponseCode."\n";
					$mail_msg .=  '$PaymentRst_Status '.$PaymentRst_Status."\n";
					$mail_msg .=  '$PaymentRst_fraudCode '.$PaymentRst_fraudCode."\n";
					
					//mail('peeracha.n@sanoga.com', 'CANCEL ['.$PaymentRst_InvoiceNo."] ".date("d M Y H:i:s"), $mail_msg);
				
					return false;
				}
				
			} else {
				//mail('peeracha.n@sanoga.com', 'Response from AEON REJECT II'.date("d M Y H:i:s"), $PaymentRst_ReturnData);
				//=> Other Code Return Result Code
				//$this->getCheckout()->setAEONErrorMessage('AEON not affect the transaction back.('. $PaymentRst_ReturnData .')'  );   
				//$this->cancelAction($PaymentRst_ReturnData,1);
				
					$MerchantInvoiceNo = $PaymentRst_InvoiceNo;
					$order = Mage::getModel('sales/order');
					$order->loadByIncrementId($MerchantInvoiceNo);
						
					$state = Mage_Sales_Model_Order::STATE_CANCELED;
					$state = 'payment_fail_aeon';
					$message="AEON Return Code : " .  $AEONCode . ", State : ". $AEONState;
					$order->setStatus($state,true,$message);
/* 					$order->setState($state, true, $message); */
					$order->save();
					$mail_msg = '$PaymentRst_ResponseCode '. $PaymentRst_ResponseCode."\n";
					$mail_msg .=  '$PaymentRst_Status '.$PaymentRst_Status."\n";
					$mail_msg .=  '$PaymentRst_fraudCode '.$PaymentRst_fraudCode."\n";
					
					//mail('peeracha.n@sanoga.com', 'CANCEL ['.$PaymentRst_InvoiceNo."] ".date("d M Y H:i:s"), $mail_msg);

				
				
				return false;
			}
		
		}
		else
		{
		
				
		//mail('peeracha.n@sanoga.com', 'Response from AEON REJECT III'.date("d M Y H:i:s"), $PaymentRst_ReturnData);
			//=>  Return Result Code
			//$this->getCheckout()->setAEONErrorMessage('AEON not affect the transaction back.('. $PaymentRst_ReturnData .')'  );   
			//$this->cancelAction($PaymentRst_ReturnData,1);
					$MerchantInvoiceNo = $PaymentRst_InvoiceNo;
					$order = Mage::getModel('sales/order');
					$order->loadByIncrementId($MerchantInvoiceNo);
						
					$state = Mage_Sales_Model_Order::STATE_CANCELED;
					$state = 'payment_fail_aeon';
					$message="AEON Return Code : " .  $AEONCode . ", State : ". $AEONState;
					$order->setStatus($state,true,$message);
/* 					$order->setState($state, true, $message); */
					$order->save();
					$mail_msg = '$PaymentRst_ResponseCode '. $PaymentRst_ResponseCode."\n";
					$mail_msg .=  '$PaymentRst_Status '.$PaymentRst_Status."\n";
					$mail_msg .=  '$PaymentRst_fraudCode '.$PaymentRst_fraudCode."\n";
					
					//mail('peeracha.n@sanoga.com', 'CANCEL ['.$PaymentRst_InvoiceNo."] ".date("d M Y H:i:s"), $mail_msg);

			return false;		
		}	
		
		
		
		
        
    }
	public function errorAction()
    {
    	$this->_redirect('checkout/onepage/');
    }
    
	public function statusAction()
    {
    	if(isset($_REQUEST["status"])) $ReturnStatus=$_REQUEST["status"];
/*     	$ReturnStatus = $_REQUEST['status']; */
    	$state = array("AP","RS");
/*     	if ($ReturnStatus=="AP"): */
		if(in_array($ReturnStatus,$state)):
    		Mage::getSingleton('checkout/session')->getQuote()->setIsActive(false)->save(); 
			$this->_redirect('checkout/onepage/success', array('_secure'=>true)); 
    	else:
/*     		$this->_redirect('checkout/onepage/'); */
/*     		$this->_redirect('customer/account/'); */

			require_once('app/Mage.php');
			umask(022);
			Mage::app();
			
			//Mage::app('default');
			//Mage::getSingleton('core/session', array('name' => 'frontend'));
			//$sessionOrder = Mage::getSingleton("customer/session"); 
			
			//echo 'y';
			// check admin credentials
/* 			Mage::getSingleton('core/session', array('name' => 'adminhtml')); */
/* 			$admin = Mage::getSingleton('admin/session');  */
			//Mage::getSingleton('core/session', array('name' => 'frontend'));
		//	$sessionCustomer = Mage::getSingleton("customer/session"); 
			//if ($sessionCustomer->isLoggedIn()):
				//$orderIncrementId = "1100001327";
				//$order = Mage::getModel('sales/order')->loadByIncrementId($orderIncrementId);
				//$order->setState(Mage_Sales_Model_Order::STATE_PROCESSING, true)->save();
				//echo $orderIncrementId;
			//endif;  
 
 
    		//echo $ReturnStatus."XX";
    		//$this->_redirect('checkout/onepage/');
    	endif; 
    	
    	
       
    }
    
     /**
     * Get singleton of Checkout Session Model
     *
     * @return Mage_Checkout_Model_Session
     */
    public function getCheckout()
    {
        return Mage::getSingleton('checkout/session');
    }




	function takeCancel($MerchantInvoiceNo,$order){
	global $AEONCode;
	global $AEONState;
	global $_POST;
	global $PaymentRst_ResponseCode;
	global $PaymentRst_fraudCode;
	global $PaymentRst_Status;
				
/* 				$MerchantInvoiceNo = $PaymentRst_InvoiceNo; */
		
/* 				$order = Mage::getModel('sales/order'); */
				$order->loadByIncrementId($MerchantInvoiceNo);
					
				$state = Mage_Sales_Model_Order::STATE_CANCELED;
				$state = 'payment_fail_aeon';
				$message="AEON Return Code : " .  $AEONCode . ", State : ". $AEONState;
/* 				$order->setState($state, true, $message); */
				$order->setStatus($state,true,$message);
				$order->save();
				$mail_msg = '$PaymentRst_ResponseCode '. $PaymentRst_ResponseCode."\n";
				$mail_msg .=  '$PaymentRst_Status '.$PaymentRst_Status."\n";
				$mail_msg .=  '$PaymentRst_fraudCode '.$PaymentRst_fraudCode."\n";
				
				
			//	mail('peeracha.n@sanoga.com', 'CANCEL ['.$PaymentRst_InvoiceNo."] ".date("d M Y H:i:s"), $mail_msg);
	}
}
