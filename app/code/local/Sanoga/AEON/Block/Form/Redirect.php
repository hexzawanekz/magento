<?php 
 class Sanoga_AEON_Block_Form_Redirect extends Mage_Core_Block_Abstract
{
    protected function _toHtml()
    {
		 $AEON = Mage::getModel('AEON/method_AEON');

        $form = new Varien_Data_Form();
        $form->setAction($AEON->getAEONUrl())
            ->setId('AEON_standard_checkout')
            ->setName('sendform')
            ->setMethod('post')
		    ->setUseContainer(true);
        foreach ($AEON->getStandardCheckoutFormFields('redirect') as $field=>$value) {
           $form->addField($field, 'hidden', array('name'=>$field, 'value'=>$value));
           if ($field=="order_id") $PaymentRst_OrderID = $value;
        }
        
        
        
        /* 		nus 2014-02-18 */
####### set payment_pending payment state
#######
#######

/*
require_once 'app/Mage.php';
	umask(0);
	Mage::app('default');
*/
/*  echo $PaymentRst_OrderID; */
	 
/* 		if(isset($_REQUEST["order_id"])) $PaymentRst_OrderID =$_REQUEST["order_id"]; */
if (!empty($PaymentRst_OrderID)):
		$order = Mage::getModel('sales/order');//->loadByIncrementId($PaymentRst_OrderID);
		$order = new Mage_Sales_Model_Order();
		$order->loadByIncrementId($PaymentRst_OrderID); 
 
		$state = "payment_processing_aeon";
		$message="Payment : Customer lost connection";
		$order->setStatus("payment_processing_aeon",true,"Customer is doing payment process");
		$order->save();
endif;				
		/*		$order->setState($state, true, $message);
				$order->save();
*/
####### end set payment_pending payment state
#######
#######
/* 		nus 2014-02-18 */		

        
        
        /*nus*/
 
        
        
        /* end nus */
		
        $html = '<html>
        <head>
       <style>
       body{
       font-size:16px; 
       margin-top:40px;
       font-family:Tahoma,verdana;
       }
       td{font-size:16px;font-family:Tahoma,verdana;}
       .logo{
       margin:20px 0px;
       }
       ul.nav {list-style-type:none;
       
       }
        ul.nav  li{float:left;padding-left:20px;padding-right:20px;}
       </style>
        </head>
				<body style="text-align:center;">
';

		//This is for Webtrekk Implementation
		$html.= '<script type="text/javascript" src="http://www.sanoga.com/webtrekk/webtrekk_v3.js"></script>';
		$html.= '<script type="text/javascript">';
		$html.= 'var wt = new webtrekkV3();';
		$html.= 'wt.contentId = "CheckoutCart__ www_sanoga_com.AEON.AEON.redirect";';
		$html.= 'wt.contentGroup = { 1 : "th", 2 : "CheckoutCart" };';
		$html.= 'wt.sendinfo();';
		$html.= '</script>';
		$html.= '<noscript><div><img src="http://sanoga01.webtrekk-asia.net/144677628704712/wt.pl?p=323,0" height="1" width="1" alt="" /></div></noscript>';
		//End of Webtrekk Implementation

//$html .= Mage::getStoreConfig('payment/AEON/title');
$html .= '
				<table cellspacing="0" cellpadding="0" width="900" style="margin-top:46px;margin-bottom:20px;" align="center">
					<tbody><tr>
						<td align="center">
							<img src="http://sanoga.com/skin/frontend/sanoga/default/images/logo.gif">
						</td>
					<!--	<td style="font-size:15px; padding:10px 0px 0px 58px; vertical-align:middle; word-spacing:5px">
							<strong>จัดส่งฟรีเมื่อซื้อครบ&nbsp;</strong>1,500.- <strong>โทร. 090-989-5220 </strong><img style="margin-top:-5px" src="http://sanoga.com/media/wysiwyg/phone_logo.png"><img style="margin-top:-5px; margin-left:4px" src="http://sanoga.com/media/wysiwyg/line-logo-3.png" title="LINE ID: SANOGA"> 
						</td> 	-->
																							</tr>
				
				 
				


				';
			
		//$_redirecttext = $this->__('Please wait. You will be redirected to AEON in a few seconds.<br/><br/>'); 
		if (Mage::getStoreConfig('payment/AEON/redirecttext') !="") {
				$_redirecttext = Mage::getStoreConfig('payment/AEON/redirecttext');
		}
		
		$_redirectfooter = $this->__('<br/><br/>');
		if (Mage::getStoreConfig('payment/AEON/redirectfooter') !="") {
				$_redirectfooter = Mage::getStoreConfig('payment/AEON/redirectfooter');
		}
		
		
		
	
		
		
       $html .= '<tr><td align=center>';
	   if(!empty($_redirecttext)) $html.= '<p style=padding-top:80px;padding-bottom:40px>'.$_redirecttext.'</p>';
       //$html .= '<p>Please wait. You will be redirected to AEON in a few seconds.</p>	';
       $html .= '</td></tr>';
       
       
       $html .= '</tbody></table>';
       
	   $html.= $_redirectfooter; 
	   
	   $html .= '
 
	   <p align="center" style="font-size:12px;padding-top:50px;">© 2013 SANOGA (Thailand) Co., Ltd. All Rights Reserved</p>   
	  
	  ';

       $html.= $form->toHtml();
       
       $html.= '<script type="text/javascript">
	   			  function formsubmit()
				  {
				  	document.sendform.submit();	
				  }
				  setTimeout("formsubmit()", 100);
	            </script>';	  
        $html.= '</body></html>';

        return $html; 
    }
}