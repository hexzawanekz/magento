<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category   Mage
 * @package    Sanoga_AEON
  * @author	   
 * 
 */



class Sanoga_AEON_Model_Method_AEON extends Mage_Payment_Model_Method_Abstract
{
    protected $_formBlockType = 'AEON/form_AEON';
    protected $_infoBlockType = 'AEON/info_AEON';
    protected $_canSaveAEON     = false;
	protected $_code  = 'AEON';
    
	/**
     * Assign data to info model instance
     *
     * @param   mixed $data
     * @return  Sanoga_AEON_Model_Info
     */
    public function assignData($data)
    {
		 

        if (!($data instanceof Varien_Object)) {
            $data = new Varien_Object($data);
        }
        $info = $this->getInfoInstance();
		$info->setAEONType($this->getAEONAccountId1())	
			->setMerchant_Id($data->getMerchant_Id())
			->setTerminal_Id($data->getTerminal_Id())
			->setAmount($data->getAmount())
			->setRedirect_Url($data->getRedirect_Url())
			->setResponse_Url($data->getResponse_Url())
			->setIp_Address($data->getIp_Address())
			->setDetails($data->getDetails())
			->setOrder_Id($data->getOrder_Id());
		
        return $this;
    }

    /**
     * Prepare info instance for save
     *
     * @return Sanoga_AEON_Model_Abstract
     */
    public function prepareSave()
    {
        $info = $this->getInfoInstance();
        if ($this->_canSaveAEON) {
            $info->setAEONNumberEnc($info->encrypt($info->getAEONNumber()));
        }
        $info->setAEONNumber(null)
            ->setAEONCid(null);
        return $this;
    }
	public function getProtocolVersion()
    {
        return '1.0';//$this->getConfigData('protocolversion');
    }
	
	/**
     * Get paypal session namespace
     *
     * @return Mage_AEON_Model_Session
     */
    public function getSession()
    {
        return Mage::getSingleton('AEON/session');
    }

    /**
     * Get checkout session namespace
     *
     * @return Mage_Checkout_Model_Session
     */
    public function getCheckout()
    {
        return Mage::getSingleton('checkout/session');
    }
	/**
     * Get current quote
     *
     * @return Mage_Sales_Model_Quote
     */
    public function getQuote()
    {
        
	    return $this->getCheckout()->getQuote();
    }
	
	public function getStandardCheckoutFormFields($option = '')
    {
        if ($this->getQuote()->getIsVirtual()) {
            $a = $this->getQuote()->getBillingAddress();
            $b = $this->getQuote()->getShippingAddress();
        } else {
            $a = $this->getQuote()->getShippingAddress();
            $b = $this->getQuote()->getBillingAddress();
        }
		 
		$data = $this->getQuoteData($option);
		
		$InvoiceNo = str_pad($data['INVOICENO'],20, "0", STR_PAD_LEFT);
		$AmountText = str_pad(number_format($data['AMOUNT'],2, '', ''),12, "0", STR_PAD_LEFT);
		$ProductInfo = str_replace(".", " ",trim($data['PRODUCTINFO']));
		if (strlen($ProductInfo) > 250) {
			$ProductInfo = substr($ProductInfo,0,250);
		}
		
		$version = $data['VERSION'];
		$merchant_id = $data['MERCHANTID'];
		$payment_description = $ProductInfo;
		$order_id = $this->getCheckout()->getLastRealOrderId();
		$invoice_no = $InvoiceNo;
		$currency = "764"; // $data['PAYCURRENCY'];
		$amount = $AmountText;
		$customer_email = $data['CUSTEMAIL'];
		$pay_category_id = $data['PAYCATEGORYID'];
		$promotion = "";
		$user_defined_1 = "";
		$user_defined_2 = "";
		$user_defined_3 = "";
		$user_defined_4 = "";
		$user_defined_5 = "";
		$result_url_1 = $data['RESULTURL1'];
		$result_url_2 = $data['RESULTURL2'];
		
		$secret_key = $data['AuthenticationKey'];
		$strSignatureString = $version . $merchant_id . $payment_description . $order_id . $invoice_no . $currency . $amount . 
						$customer_email . $pay_category_id . $promotion . $user_defined_1 . $user_defined_2 . $user_defined_3 . 
						$user_defined_4 . $user_defined_5 . $result_url_1 . $result_url_2;
		$HashValue = hash_hmac('sha1', $strSignatureString, $secret_key, false);
		
		$sArr = array(
			'version' => $version,
			'merchant_id' => $merchant_id,
			'merchantID' => '912119',
			'payment_description' => $payment_description,
			'order_id' => $order_id,
			'invoice_no' => $invoice_no,
			'invoiceNo' => "SANO".(int)$invoice_no,
			
			'currency' => $currency,
			'currencyCode' => $currency,
			'amount' => $amount,
			'customer_email' => $customer_email,
			'paymentID' => 'SANOGA',
/* 			'paymentID' => 'SANOGATHAILAND',  */
			
			'pay_category_id' => $pay_category_id,
			'promotion' => $promotion,
			'user_defined_1' => $user_defined_1,
			'user_defined_2' => $user_defined_2,
			'user_defined_3' => $user_defined_3,
			'user_defined_4' => $user_defined_4,
			'user_defined_5' => $user_defined_5,
			'result_url_1' => $result_url_1,
			'result_url_2' => $result_url_2,
			'productDesc' => date("d M Y H:i:s"), 
/* 			$customer_email */
/* 			'hash_value' => $HashValue */
			'hash_value' => '94E8E91C29E73B9648011FADBAE19849B520B24B' 
		);
		
		/* $CheckSumData = $data['VERSION'] . $data['MERCHANTID'] . "AEON Payment" . "" . $InvoiceNo . $data['PAYCURRENCY'] .
			 $AmountText . $data['CUSTEMAIL']. $data['PAYCATEGORYID']. $data['PROMOTION'] . 
			 $data['REF1'] . $data['REF2'] . $data['REF3'] . "" . "" . $data['RESULTURL1'] . $data['RESULTURL2'];
		
		$HashValue = hash_hmac('sha1', $CheckSumData, "Bt19N5pRawKu", false);
			
		$sArr = array(	
			'version'                   => $data['VERSION'],
			'merchant_id'               => $data['MERCHANTID'],
			'payment_description'		=> "AEON Payment",
			'order_id'					=> "",
			'invoice_no'				=> $InvoiceNo,
			'currency'					=> $data['PAYCURRENCY'],
			'amount'					=> $AmountText,
			'customer_email'			=> $data['CUSTEMAIL'],
			'pay_category_id'			=> $data['PAYCATEGORYID'],
			'promotion'					=> $data['PROMOTION'],
			'user_defined_1'			=> $data['REF1'],
			'user_defined_2'			=> $data['REF2'],
			'user_defined_3'			=> $data['REF3'],
			'user_defined_4'			=> "",
			'user_defined_5'			=> "",
			'result_url_1'				=> $data['RESULTURL1'],
			'result_url_2'				=> $data['RESULTURL2'],
			'hash_value'				=> $HashValue,
			
			
			'PRODUCTINFO'				=> $ProductInfo,
			'INVOICENO' 				=> $InvoiceNo,
			'REF1'                     	=> $data['REF1'],
			'REF2'                     	=> $data['REF2'],
			'REF3'                     	=> $data['REF3'],
			'AMOUNT'               		=> $AmountText,
			'PROMOTION'           		=> $data['PROMOTION'],
			'CUSTEMAIL'          		=> $data['CUSTEMAIL'],
			'PAYCURRENCY'            	=> $data['PAYCURRENCY'],
			'PAYCATEGORYID'           	=> $data['PAYCATEGORYID'],
			'RESULTURL1'              	=> $data['RESULTURL1'],
			'RESULTURL2'              	=> $data	['RESULTURL2'],
			'CHECKSUM'                 	=> "XXXXXXXX" . MD5($CheckSumData),
			); */
			
        $sReq = '';
        $rArr = array();
        foreach ($sArr as $k=>$v) {
            /*
            replacing & char with and. otherwise it will break the post
            */
            //$value =  str_replace("&","and",$v);
			$value =  $v;
            $rArr[$k] =  $value;
            $sReq .= '&'.$k.'='.$value;
        }
		 
        return $rArr;
    }

    public function getAEONUrl()
    {
		 $url=$this->_getAEONConfig()->getAEONServerUrl();
         return $url;
    }
	
	
	 public function getOrderPlaceRedirectUrl()
    {
	         return Mage::getUrl('AEON/AEON/redirect');
    }
    
    
	public function getQuoteData($option = '')
    {					
	
		if ($option == 'redirect') {
    		$orderIncrementId = $this->getCheckout()->getLastRealOrderId();
    		$quote = Mage::getModel('sales/order')->loadByIncrementId($orderIncrementId);
		} else {
			$quote = $this->getQuote();
		}

		$data=array();		 	
		if ($quote)
		{
			if ($quote->getIsVirtual()) {
				$a = $quote->getBillingAddress();
				$b = $quote->getShippingAddress();
			} else {
				$a = $quote->getShippingAddress();
				$b = $quote->getBillingAddress();
			}
				
			
			$grand_total =  $quote->getGrandTotal();
			//$grand_total = $grand_total*100;
			
			$data['VERSION']           	= $this->_getAEONConfig()->getPaymentVersion();
			$data['MERCHANTID']           	= $this->_getAEONConfig()->getPaymentMerchantID();
			$data['REF1']           	= "";
			$data['REF2']           	= "";
			$data['REF3']           	= "";
			
			$data['AMOUNT']                     = $grand_total;

			$data['PROMOTION']           	= $this->_getAEONConfig()->getPaymentPromotionCode();
			$data['CUSTEMAIL']        = $a->getEmail();
			$data['PAYCURRENCY']    = $this->_getAEONConfig()->getPaymentCurrencyCode();
			$data['PAYCATEGORYID'] = $this->_getAEONConfig()->getPaymentCategoryID();
			
			$data['RESULTURL1']    = $this->_getAEONConfig()->getAEONRedirecturl();
			$data['RESULTURL2']    = "";
			
			$data['CHECKSUM']      = "";
			
			$data['AuthenticationKey']  = $this->_getAEONConfig()->getMerchantAuthenticationKey();
			
			$data['IPADDRESS']     = $_SERVER['REMOTE_ADDR'];
			
			$items = $quote->getAllItems();
			foreach($items as $item) {
				$details .= $item->getName() . " ";
			}
			
			$data['PRODUCTINFO']                    = $details;
			$data['INVOICENO']                   = $this->getCheckout()->getLastRealOrderId();
		}
		 
		return $data; 
	}
	
	protected function _getAEONConfig()
    {
        return Mage::getSingleton('AEON/config');
    }
}
 
 