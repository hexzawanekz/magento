<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at http://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   Advanced Sphinx Search Pro
 * @version   2.2.8
 * @revision  179
 * @copyright Copyright (C) 2013 Mirasvit (http://mirasvit.com/)
 */


/**
 * Results block, handle all content types search results
 *
 * @category Mirasvit
 * @package  Mirasvit_SearchIndex
 */
class Mirasvit_SearchIndex_Block_Results extends Mage_CatalogSearch_Block_Result
{
    protected $_indexes = null;
    /**
     * Retrieve all enabled indexes
     * @return array
     */
    public function getIndexes()
    {
        if ($this->_indexes == null) {
            $this->_indexes = Mage::helper('searchindex/index')->getIndexes(true);
            foreach ($this->_indexes as $code => $index) {
                $index->setContentBlock($this->getContentBlock($index));
            }
        }

        return $this->_indexes;
    }

    /**
     * Return url to search by specific index
     * @param  Mirasvit_SearchIndex_Model_Index_Abstract $index
     * @return string
     */
    public function getIndexUrl($index)
    {
        return Mage::getUrl('*/*/*', array(
            '_current' => true,
            '_query'   => array('index' => $index->getCode(), 'p' => null)
        ));
    }

    /**
     * Return first index with results greater zero or catalog index
     * @return Mirasvit_SearchIndex_Model_Index_Abstract
     */
    public function getFirstMatchedIndex()
    {
        foreach ($this->getIndexes() as $index) {
            if ($index->getCountResults()) {
                return $index;
            }
        }

        return Mage::helper('searchindex/index')->getIndex('catalog');
    }

    /**
     * Return current index or first matched index
     * @return Mirasvit_SearchIndex_Model_Index_Abstract
     */
    public function getCurrentIndex()
    {
        $indexCode    = $this->getRequest()->getParam('index');
        $currentIndex = Mage::helper('searchindex/index')->getIndex($indexCode);
        if ($indexCode === null || $currentIndex->getCountResults() == 0) {
            $currentIndex = $this->getFirstMatchedIndex();
        }
        return $currentIndex;
    }

    public function getListBlock()
    {
        Mage::unregister('current_layer');
        Mage::register('current_layer', Mage::getSingleton('catalogsearch/layer'));

        return $this->getChild('search_result_list');
    }

    /**
     * Return current search content
     * @return string
     */
    public function getCurrentContent()
    {
        $currentIndex = $this->getCurrentIndex();

        return $this->getContentBlock($currentIndex)->toHtml();
    }

    public function getContentBlock($indexModel)
    {
        if ($indexModel->getCode() == 'catalog') {
            return $this->getChild('search_result_list');
        }

        return $this->getChild('searchindex_result_'.$indexModel->getCode());
    }
	protected function _prepareLayout()
    {
        // add Home breadcrumb
        $breadcrumbs = $this->getLayout()->getBlock('breadcrumbs');
        if ($breadcrumbs) {
            $title = $this->__("Search results for: '%s'", $this->helper('catalogsearch')->getQueryText());

            $breadcrumbs->addCrumb('home', array(
                'label' => $this->__('Home'),
                'title' => $this->__('Go to Home Page'),
                'link'  => Mage::getBaseUrl()
            ))->addCrumb('search', array(
                'label' => $title,
                'title' => $title
            ));
        }

        // modify page title
        $store_name = Mage::app()->getStore()->getName();
        $tmp = explode(' ', $store_name);
        $vertical = strtolower($tmp[0]);
        $titleSuffix = Mage::getStoreConfig('design/head/title_suffix');
        $found = stripos($titleSuffix, $vertical);

        if ($found>-1){
            $suffix = '';
        } else {
            $suffix = $this->getLayout()->getBlock('head')->getDefaultTitle();
        }

        switch ($vertical) {
            case 'petloft':
                $title = $this->__("%s: Buy pet products at %s", $this->helper('catalogsearch')->getEscapedQueryText(), $suffix);
                break;

            case 'venbi':
                $title = $this->__("%s: Buy baby and mom products at %s", $this->helper('catalogsearch')->getEscapedQueryText(), $suffix);
                break;

            case 'sanoga':
                $title = $this->__("%s: Buy health supplements at %s", $this->helper('catalogsearch')->getEscapedQueryText(), $suffix);
                break;

            case 'lafema':
                $title = $this->__("%s: Buy cosmetics at %s", $this->helper('catalogsearch')->getEscapedQueryText(), $suffix);
                break;

            default:
                $title = $this->__("%s: Buy pet products at %s", $this->helper('catalogsearch')->getEscapedQueryText(), $suffix);
                break;
        }

        // $title = $this->__("%s: Buy at Petloft.com", $this->helper('catalogsearch')->getEscapedQueryText());
        $this->getLayout()->getBlock('head')->setTitle($title);

        $description = $this->__("Shop %s pet food only at PetLoft.com - Free shipping & delivery", $this->helper('catalogsearch')->getEscapedQueryText());
        $this->getLayout()->getBlock('head')->setDescription($description);

        return Mage_Core_Block_Template::_prepareLayout();
    }
}