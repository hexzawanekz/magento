<?php

class Mirasvit_SearchIndex_Model_Resource_Catalogsearch_Fulltext_Collection extends Mage_CatalogSearch_Model_Mysql4_Fulltext_Collection
{
	protected function _applyProductLimitations() {
        $this->_prepareProductLimitationFilters();
        $this->_productLimitationJoinWebsite();
        $this->_productLimitationJoinPrice();
        $filters = $this->_productLimitationFilters;

        if (!isset($filters['category_id']) && !isset($filters['visibility'])) {
            return $this;
        }

        $conditions = array(
            'cat_index.product_id=e.entity_id',
            $this->getConnection()->quoteInto('cat_index.store_id=?', $filters['store_id'])
        );
//        if (isset($filters['visibility']) && !isset($filters['store_table'])) {
//            $conditions[] = $this->getConnection()
//                ->quoteInto('cat_index.visibility IN(?)', $filters['visibility']);
//        }
        $conditions[] = $this->getConnection()
            ->quoteInto('cat_index.category_id=?', $filters['category_id']);
//        if (isset($filters['category_is_anchor'])) {
//            $conditions[] = $this->getConnection()
//                ->quoteInto('cat_index.is_parent=?', $filters['category_is_anchor']);
//        }

        $joinCond = join(' AND ', $conditions);
        $fromPart = $this->getSelect()->getPart(Zend_Db_Select::FROM);
        if (isset($fromPart['cat_index'])) {
            $fromPart['cat_index']['joinCondition'] = $joinCond;
            $this->getSelect()->setPart(Zend_Db_Select::FROM, $fromPart);
        }
        else {
            $this->getSelect()->join(
                array('cat_index' => $this->getTable('catalog/category_product_index')),
                $joinCond,
                array('cat_index_position' => 'position')
            );
        }
//		if (!isset($fromPart['cat_product'])) {
//            $this->getSelect()->join(
//				array('cat_product'=>$this->getTable('catalog/category_product')),
//				'cat_product.category_id=cat_index.category_id and cat_index.product_id=cat_product.product_id',
//				array()
//			);
//        }
        $this->_productLimitationJoinStore();

        Mage::dispatchEvent('catalog_product_collection_apply_limitations_after', array(
            'collection'    => $this
        ));

        return $this;
    }
}
