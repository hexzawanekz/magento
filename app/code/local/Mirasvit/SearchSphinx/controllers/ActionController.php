<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at http://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   Advanced Sphinx Search Pro
 * @version   2.2.8
 * @revision  179
 * @copyright Copyright (C) 2013 Mirasvit (http://mirasvit.com/)
 */


class Mirasvit_SearchSphinx_ActionController extends Mage_Core_Controller_Front_Action
{
    public function startAction()
    {
        return $this->_getEngine()->doStart();
    }

    public function stopAction()
    {
        return $this->_getEngine()->doStop();
    }

    public function reindexAction()
    {
        return $this->_getEngine()->doReindex();
    }

    public function reindexdeltaAction()
    {
        return $this->_getEngine()->doReindexDelta();
    }

    protected function _getEngine()
    {
        return Mage::getSingleton('searchsphinx/engine_sphinx');
    }
}