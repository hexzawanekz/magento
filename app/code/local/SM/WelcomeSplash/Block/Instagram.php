<?php

class SM_WelcomeSplash_Block_Instagram extends Mage_Core_Block_Template
{
    public function getWidgetData()
    {
        $token = Mage::getStoreConfig('welcome_splash/instagram_widget/access_token');

        if($token){
            $userRequest = "https://api.instagram.com/v1/users/self/?access_token={$token}";

            $result = @file_get_contents($userRequest);

            if($result){
                $obj = @json_decode($result);

                if($userId = $obj->data->id){

                    $mediaRequest = "https://api.instagram.com/v1/users/{$userId}/media/recent/?access_token={$token}";

                    $data = @file_get_contents($mediaRequest);

                    if($data){
                        if($data = @json_decode($data)){
                            if($data = $data->data){
                                $final = array();
                                foreach($data as $obj){
                                    if($link = $obj->link){
                                        $thumb = '';
                                        if($thumb == '' && $obj->images->low_resolution->url){
                                            $thumb = $obj->images->low_resolution->url;
                                        }elseif($thumb == '' && $obj->images->standard_resolution->url){
                                            $thumb = $obj->images->standard_resolution->url;
                                        }elseif($thumb == '' && $obj->images->thumbnail->url){
                                            $thumb = $obj->images->thumbnail->url;
                                        }

                                        $final[] = array(
                                            'link'      => $link,
                                            'thumb'     => $thumb,
                                            'caption'   => $obj->caption->text ? $obj->caption->text : '',
                                        );
                                    }
                                }
                                return $final;
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    public function getTitle()
    {
        return Mage::getStoreConfig('welcome_splash/instagram_widget/widget_title');
    }

    public function getMobileTitle()
    {
        return Mage::getStoreConfig('welcome_splash/instagram_widget/widget_mobile_title');
    }
}