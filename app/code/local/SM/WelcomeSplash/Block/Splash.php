<?php

class SM_WelcomeSplash_Block_Splash extends Mage_Core_Block_Template {
    public function getBestProductCollection() {

        $whatHotItems = Mage::getStoreConfig('welcome_splash/product_list/new_recommend_item_perpage');
        $whatHotCategoryId = Mage::getStoreConfig('welcome_splash/product_list/what_hot_category_id');

        /** @var Mage_Catalog_Model_Category $categoryModel */
        $categoryModel = Mage::getModel('catalog/category')->load($whatHotCategoryId);
        $productCollection = $categoryModel->getProductCollection()
            ->addAttributeToSelect('*')
            ->setPageSize($whatHotItems)
            ->addAttributeToFilter('status', 1)
            ->setOrder('position', 'ASC');

        return $productCollection;
    }

    public function getNewProducts() {

        $newArrivalItems = Mage::getStoreConfig('welcome_splash/product_list/new_arrival_item_perpage');
        $newArrivalCategoryId = Mage::getStoreConfig('welcome_splash/product_list/new_arrival_category_id');
        
        /** @var Mage_Catalog_Model_Category $categoryModel */
        $categoryModel = Mage::getModel('catalog/category')->load($newArrivalCategoryId);
        $productCollection = $categoryModel->getProductCollection()
            ->addAttributeToSelect('*')
            ->setPageSize($newArrivalItems)
            ->addAttributeToFilter('status', 1)
            ->setOrder('position', 'ASC');

        return $productCollection;
    }

    /**
     * Get True Url for Product
     * @param Mage_Catalog_Model_Product $_product
     * @return mixed
     */
    public function getProductUrl(Mage_Catalog_Model_Product $_product) {

        //Get a Random Category
        $categoryId = min($_product->getCategoryIds());
        if($categoryId == 1) {
            $categoryId = $_product->getCategoryIds()[1];
        }
        $categoryPath = explode('/',Mage::getModel('catalog/category')->load($categoryId)->getPath());
        //Get Root Category Of Current Website
        $rootCategoryId = $categoryPath[1];

        /** @var Mage_Core_Model_Mysql4_Store_Group_Collection $groupStoreCollection */
        $groupStoreCollection = Mage::getModel('core/store_group')->getCollection();
        $storeId = $groupStoreCollection->addFieldToFilter('root_category_id', $rootCategoryId)
            ->getFirstItem()
            ->getDefaultStoreId();

        $storeName = $groupStoreCollection->getFirstItem()->getName();

        $lang = (preg_match('/th/', Mage::app()->getStore()->getCode()) ? 'th' : 'en');
        switch ($storeName) {
            case "Venbi" :
                if($lang == 'en') $storeCode = 'ven'; else $storeCode = 'vth';
                break;
            case "Lafema" :
                if($lang == 'en') $storeCode = 'len'; else $storeCode = 'lth';
                break;
            case "Sanoga" :
                if($lang == 'en') $storeCode = 'sen'; else $storeCode = 'sth';
                break;
            case "Moxy" :
                if($lang == 'en') $storeCode = 'moxyen'; else $storeCode = 'moxyth';
                break;
            default :
                if($lang == 'en') $storeCode = 'en'; else $storeCode = 'th';
        }
        $storeId = Mage::getModel('core/store')->load($storeCode, 'code')->getId();
        return $_product->setStoreId($storeId)->getProductUrl();
    }

    /**
     * Retrieve path to Favicon
     *
     * @return string
     */
    protected function _getFaviconFile()
    {
        $folderName = Mage_Adminhtml_Model_System_Config_Backend_Image_Favicon::UPLOAD_DIR;
        $storeConfig = Mage::getStoreConfig('design/head/shortcut_icon');
        $faviconFile = Mage::getBaseUrl('media') . $folderName . '/' . $storeConfig;
        $absolutePath = Mage::getBaseDir('media') . '/' . $folderName . '/' . $storeConfig;

        if(!is_null($storeConfig) && $this->_isFile($absolutePath)) {
            $url = $faviconFile;
        } else {
            $url = $this->getSkinUrl('favicon.ico');
        }
        return $url;
    }

    /**
     * If DB file storage is on - find there, otherwise - just file_exists
     *
     * @param string $filename
     * @return bool
     */
    protected function _isFile($filename) {
        if (Mage::helper('core/file_storage_database')->checkDbUsage() && !is_file($filename)) {
            Mage::helper('core/file_storage_database')->saveFileToFilesystem($filename);
        }
        return is_file($filename);
    }
}