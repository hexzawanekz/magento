<?php

// init Magento
Mage::app();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

// get store ids
$en     = Mage::getModel('core/store')->load('en', 'code')->getId();// Petloft
$th     = Mage::getModel('core/store')->load('th', 'code')->getId();
$len    = Mage::getModel('core/store')->load('len', 'code')->getId();// Lafema
$lth    = Mage::getModel('core/store')->load('lth', 'code')->getId();
$ven    = Mage::getModel('core/store')->load('ven', 'code')->getId();// Venbi
$vth    = Mage::getModel('core/store')->load('vth', 'code')->getId();
$sen    = Mage::getModel('core/store')->load('sen', 'code')->getId();// Sanoga
$sth    = Mage::getModel('core/store')->load('sth', 'code')->getId();
$moxyen = Mage::getModel('core/store')->load('moxyen', 'code')->getId();// Moxy
$moxyth = Mage::getModel('core/store')->load('moxyth', 'code')->getId();

$enStores = array($en,$len,$sen,$ven,$moxyen);
$thStores = array($th,$lth,$sth,$vth,$moxyth);

// welcome-main-nav

//==========================================================================
// English
//==========================================================================
$blockTitle = "Welcome splash navigation ENG";
$blockIdentifier = "welcome-main-nav";
$blockStores = $enStores;
$blockIsActive = 1;
$content = <<<EOD
<ul class="nav">
<li class="active"><a>welcome</a></li>
<li class="beauty-nav-item"><a href="http://lafema.com/en/">beauty</a></li>
<li class="fashion-nav-item"><a href="http://moxy.co.th/en/moxy/fashion">fashion</a></li>
<li class="living-nav-item"><a href="http://moxy.co.th/en/moxy/homeliving">home décor</a></li>
<li class="elec-nav-item"><a href="http://moxy.co.th/en/moxy/electronics">electronics</a></li>
<li class="baby-nav-item"><a href="http://venbi.com/en/">mom &amp; baby</a></li>
<li class="heath-nav-item"><a href="http://sanoga.com/en/">Health &amp; Sports</a></li>
<li class="pet-nav-item"><a href="http://petloft.com/en/">pets</a></li>
</ul>
EOD;

// delete old block if have
/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// Thai
//==========================================================================
$blockTitle = "Welcome splash navigation THAI";
$blockIdentifier = "welcome-main-nav";
$blockStores = $thStores;
$blockIsActive = 1;
$content = <<<EOD
<ul class="nav">
<li class="active"><a>หน้าแรก</a></li>
<li class="beauty-nav-item"><a href="http://lafema.com/th/">ความงาม</a></li>
<li class="fashion-nav-item"><a href="http://moxy.co.th/th/moxy/fashion">แฟชั่น</a></li>
<li class="living-nav-item"><a href="http://moxy.co.th/th/moxy/homeliving">ของแต่งบ้าน</a></li>
<li class="elec-nav-item"><a href="http://moxy.co.th/th/moxy/electronics">อิเล็กทรอนิกส์</a></li>
<li class="baby-nav-item"><a href="http://venbi.com/th/">แม่และเด็ก</a></li>
<li class="heath-nav-item"><a href="http://sanoga.com/th/">สุขภาพ &amp; สปอร์ต</a></li>
<li class="pet-nav-item"><a href="http://petloft.com/th/">สัตว์เลี้ยง</a></li>
</ul>
EOD;

// delete old block if have
/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================