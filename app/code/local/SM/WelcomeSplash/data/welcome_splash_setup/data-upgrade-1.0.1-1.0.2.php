<?php

/** @var Mage_Core_Model_Resource_Setup $installer */
$installer = $this;

$installer->startSetup();

/** @var Mage_Catalog_Model_Resource_Eav_Mysql4_Category_Collection $categoryCollection */
$categoryCollection = Mage::getModel('catalog/category')->getCollection();

$rootMoxySite = Mage::app()->getStore('moxyen')->getRootCategoryId();

/** Welcome Splash Category Id */
$categoryId = $categoryCollection->addAttributeToSelect('*')
    ->addAttributeToFilter('parent_id', $rootMoxySite)
    ->addAttributeToFilter('url_key', 'welcome-splash')
    ->getFirstItem()
    ->getId();
Mage::getModel('catalog/category')
    ->setStoreId(0)
    ->load($categoryId)
    ->setIncludeInMenu(0)
    ->setData('include_in_menu', 0)
    ->save();

$installer->endSetup();

