<?php

// init Magento
Mage::app();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

$moxyen = Mage::getModel('core/store')->load('moxyen', 'code')->getId();// Moxy
$moxyth = Mage::getModel('core/store')->load('moxyth', 'code')->getId();

//==========================================================================
// Welcome global link sites EN
//==========================================================================
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($moxyen, $withAdmin = false)
    ->addFieldToFilter('identifier', 'welcome-main-nav')
    ->getFirstItem();

if ($block->getId()) $block->delete();// if exists then delete
$content = <<<EOD
<ul class="nav">
<li class="active"><a href="{{store url}}">welcome</a></li>
<li class="beauty-nav-item"><a href="{{store direct_url='beauty.html'}}">beauty</a></li>
<li class="fashion-nav-item"><a href="{{store direct_url='fashion.html'}}">fashion</a></li>
<li class="living-nav-item"><a href="{{store direct_url='home-living.html'}}">home d&eacute;cor</a></li>
<li class="appliances-nav-item"><a href="{{store direct_url='home-appliances.html'}}">home appliances</a></li>
<li class="elec-nav-item"><a href="{{store direct_url='electronics.html'}}">electronics</a></li>
<li class="baby-nav-item"><a href="{{store direct_url='baby.html'}}">mom &amp; baby</a></li>
<li class="heath-nav-item"><a href="{{store direct_url='health.html'}}">Health &amp; Sports</a></li>
<li class="pet-nav-item"><a href="{{store direct_url='pets.html'}}">pets</a></li>
<!--<li class="active"><a href="http://upcoming.moxy.co.th/magazine/">magazine</a></li>-->
</ul>
EOD;
/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block');
$block->setTitle('Welcome splash navigation ENG');
$block->setIdentifier('welcome-main-nav');
$block->setStores($moxyen);
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================


//==========================================================================
// Welcome global link sites TH
//==========================================================================
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($moxyth, $withAdmin = false)
    ->addFieldToFilter('identifier', 'welcome-main-nav')
    ->getFirstItem();

if ($block->getId()) $block->delete();// if exists then delete
$content = <<<EOD
<ul class="nav">
<li class="active"><a href="{{store url}}">หน้าแรก</a></li>
<li class="beauty-nav-item"><a href="{{store direct_url='beauty.html'}}">ความงาม</a></li>
<li class="fashion-nav-item"><a href="{{store direct_url='fashion.html'}}">แฟชั่น</a></li>
<li class="living-nav-item"><a href="{{store direct_url='home-living.html'}}">ของแต่งบ้าน</a></li>
<li class="appliances-nav-item"><a href="{{store direct_url='home-appliances.html'}}">เครื่องใช้ไฟฟ้า<br/>ภายในบ้าน</a></li>
<li class="elec-nav-item"><a href="{{store direct_url='electronics.html'}}">อิเล็กทรอนิกส์</a></li>
<li class="baby-nav-item"><a href="{{store direct_url='baby.html'}}">แม่และเด็ก</a></li>
<li class="heath-nav-item"><a href="{{store direct_url='health.html'}}">สุขภาพ &amp; สปอร์ต</a></li>
<li class="pet-nav-item"><a href="{{store direct_url='pets.html'}}">สัตว์เลี้ยง</a></li>
<!--<li class="active"><a href="http://upcoming.moxy.co.th/magazine/">นิตยสาร</a></li>-->
</ul>
EOD;
/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block');
$block->setTitle('Welcome splash navigation THAI');
$block->setIdentifier('welcome-main-nav');
$block->setStores($moxyth);
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// Welcome global link sites EN
//==========================================================================
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter(array($moxyen,$moxyth), $withAdmin = false)
    ->addFieldToFilter('identifier', 'welcome-mobile-follow-link')
    ->getFirstItem();

if ($block->getId()) $block->delete();// if exists then delete
$content = <<<EOD
<ul>
    <li><a href="https://www.facebook.com/moxyst"><span class="fb-icon">facebook</span></a></li>
    <li><a href="https://twitter.com/moxyst"><span class="twitter-icon">twitter</span></a></li>
    <li><a href="https://www.instagram.com/moxy_th/"><span class="instagram-icon">instagram</span></a></li>
    <li><a href="https://pinterest.com/#"><span class="pinterest-icon">pinterest</span></a></li>
    <li><a href="https://plus.google.com/+Moxyst"><span class="gg-icon">google plus</span></a></li>
</ul>
EOD;
/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block');
$block->setTitle('Welcome Mobile Follow Links');
$block->setIdentifier('welcome-mobile-follow-link');
$block->setStores(array($moxyen,$moxyth));
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================



