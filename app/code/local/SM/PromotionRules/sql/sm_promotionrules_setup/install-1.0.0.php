<?php

/** @var Mage_Core_Model_Resource_Setup $installer */
$installer = $this;

$installer->startSetup();

//Change dataType of from_date field
$installer->getConnection()->changeColumn(
    $installer->getTable('salesrule/rule'),
    'from_date',
    'from_date',
    array(
        'type'      => Varien_Db_Ddl_Table::TYPE_DATETIME,
        'comment'   => 'From DateTime'
    )
);

//Change dataType of to_date field
$installer->getConnection()->changeColumn(
    $installer->getTable('salesrule/rule'),
    'to_date',
    'to_date',
    array(
        'type'      => Varien_Db_Ddl_Table::TYPE_DATETIME,
        'comment'   => 'From DateTime'
    )
);

$installer->endSetup();