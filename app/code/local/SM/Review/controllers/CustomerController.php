<?php
/**
 * Customer reviews controller
 *
 * @category    Mage
 * @package     Mage_Review
 * @author      Magento Core Team <core@magentocommerce.com>
 */
require_once 'Mage/Review/controllers/CustomerController.php';

class SM_Review_CustomerController extends Mage_Review_CustomerController
{
    public function productAction(){
        $product = Mage::getModel('catalog/product')->load($this->getRequest()->getParam('id'));
        if(!$product->getId()){
            $this->_forward('noRoute');
        }
        if(!Mage::helper('catalog/product')->canShow($product)){
            $this->_forward('noRoute');
        }
        if (!in_array(Mage::app()->getStore()->getWebsiteId(), $product->getWebsiteIds())) {
            $this->_forward('noRoute');
        }
        Mage::register('product', $product);
        $this->loadLayout();
        $this->renderLayout();
    }
}
