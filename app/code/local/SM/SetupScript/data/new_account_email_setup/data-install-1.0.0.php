<?php
/**WT-240**/

// init Magento
Mage::app();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

$moxyen = Mage::getModel('core/store')->load('moxyen', 'code')->getId();// Moxy
$moxyth = Mage::getModel('core/store')->load('moxyth', 'code')->getId();

// Moxy TH
//==========================================================================
//==========================================================================

/** @var Mage_Core_Model_Email_Template $emailTemplateModel */
$emailTemplateModel = Mage::getModel('core/email_template');
$emailTemplateModel->setTemplateCode("New Account (MOXY)");
$emailTemplateModel->setTemplateSubject("Welcome, {{var customer.name}}!");

$templateContent = <<<HTML
<meta charset="utf-8"/>
<div class="mail-container" style="width:600px; padding:0px 33px; font-family: Tahoma; font-size: 14px; color: #3c3d41; margin: 0px;">
<div class="mail-header" style="width:600px; font-family: Tahoma; font-size: 14px; color: #3c3d41; margin: 0px;">
		<div class="mail-logo" style="width:100%; margin:0px auto;">
			<a href="http://moxy.co.th/"><img src="{{skin _area="frontend"  url="images/email/moxy-new-account-banner.png"}}" style="width:100%;" /></a>
		</div>
		<div class="english-below" style="min-height: 12px; max-height: 12px; margin-bottom: 50px; line-height: 12px; padding:0px 5px;">
			<span style="font-size:12px;"><img src="{{skin _area="frontend"  url="images/email/flag-en.png"}}" height="12px;"/> Scroll down for English version</span>
		</div>
</div>

	<div class="mail-body" style="margin:50px auto; width: 95%;">
		<p>เรียน คุณ {{var customer.name}},</p>

		<br/>
		<p>ยินดีต้อนรับเข้าสู่ MOXY!</p>

		<br/>
		<p>ขอขอบคุณสำหรับการลงทะเบียนกับ MOXY เรามีความยินดีเป็นอย่างยิ่งที่คุณเข้า</p>
		<p>ร่วมเป็นส่วนหนึ่งในนักช้อปไลฟสไตล์</p>
		<br/>

		<p>ข้อมูลสำหรับการล็อคอินของคุณคือ:</p>
		<p>{{var customer.email}}</p>
		<p>{{var customer.password}}</p>
<br/>

		<p>เราขอมอบส่วนลดสุดพิเศษให้กับคุณ <span style="color: #ee5191;">200 บาท*</span> เพียงใส่โค้ด <span style="color: #ee5191;">MOXYLIFE</span></p>
		<p>ก่อนดำเนินการชำระค่าสินค้า</p>
		<br/>

		<p><a href="https://www.moxy.co.th/th/" style="color: #3c3d41;">คลิกตรงนี้ เพื่อช้อปได้เลย!</a></p>
		<br/>

		<p>Warm regards, </p>
		<p>Moxy Team</p>
		<p style="color: grey; font-style: italic; font-size: 12px;">*เมื่อมีการสั่งซื้อขั้นต่ำ 1,000 บาท</p>
		<p style="color: grey; font-style: italic; font-size: 12px;">โค้ดนี้สามารถใช้ได้เพียงหนึ่งครั้ง</p>

	</div>

	<div class="navigation-th" style="margin-top: 24px; overflow:hidden;">
		<div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #ee5191; border-bottom: 2px solid #ee5191; text-align:center;">
			<span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 16; padding: 0px 10px;"><a href="http://www.moxy.co.th/th/fashion.html" style="color: #3c3d41; text-decoration: none;">ความงาม</a></span>
			<span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 16; padding: 0px 10px;"><a href="http://www.moxy.co.th/th/fashion.html" style="color: #3c3d41; text-decoration: none;">แฟชั่น</a></span>
			<span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 16; padding: 0px 10px;"><a href="http://www.moxy.co.th/th/home-living.html" style="color: #3c3d41; text-decoration: none;">ของแต่งบ้าน</a></span>
			<span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 16; padding: 0px 10px;"><a href="http://www.moxy.co.th/th/gadgets-electronics.html" style="color: #3c3d41; text-decoration: none;">อิเล็กทรอนิกส์</a></span>
<span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 16; padding: 0px 10px;"><a href="http://www.moxy.co.th/th/venbi.html" style="color: #3c3d41; text-decoration: none;">แม่และเด็ก</a></span>
<span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 16; padding: 0px 10px;"><a href="http://www.moxy.co.th/th/sanoga.html" style="color: #3c3d41; text-decoration: none;">สุขภาพ</a></span>
			<span class="nav-item" style="line-height: 33px; font-size: 16; padding: 0px 10px;"><a href="http://www.moxy.co.th/th/petloft.html" style="color: #3c3d41; text-decoration: none;">สัตว์เลี้ยง</a></span>
		</div>
	</div>

	<div class="email-footer" style="min-height: 82px; max-heigh: 82px; background: #bbb; margin-top: 8px; color: #fff; padding: 0px 15px;">
		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px"><img src="{{skin _area="frontend"  url="images/email/mailbox.png"}}" style="width:18px;"/> ติดต่อเรา</p>
			<p style="font-size: 10px; margin: 0;">02-106-8222</p>
			<p style="margin: 0; font-size: 10px;"><a href="mailto:support@moxy.co.th" style="color: #fff; text-decoration: none;">support@moxy.co.th</a></p>
		</div>

		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">ติดตามที่</p>
			<div class="follow-icons" style="width: 100%">
				<a href="https://www.facebook.com/moxyst"><img src="{{skin _area="frontend"  url="images/email/facebook_follow.png"}}" style="width: 25px;" alt="Facebook fanpage"></a>
				<a href="https://twitter.com/moxyst"><img src="{{skin _area="frontend"  url="images/email/twitter_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
				<a href="https://webstagra.ms/moxyst/"><img src="{{skin _area="frontend"  url="images/email/instagram_follow.png"}}" style="width: 25px;" alt="Instagram fanpage"></a>
				<a href="https://www.pinterest.com/moxyst/"><img src="{{skin _area="frontend"  url="images/email/pinterest_follow.png"}}" style="width: 25px;" alt="Pinterest fanpage"></a>
				<a href="https://plus.google.com/+Moxyst"><img src="{{skin _area="frontend"  url="images/email/google_follow.png"}}" style="width: 25px;" alt="Google fanpage"></a>
			</div>
		</div>

		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">ช่องทางการชำระเงิน</p>
			<img src="{{skin _area="frontend"  url="images/email/payment-th.png"}}" alt="ช่องทางการชำระเงิน" style="width: 109px;">
		</div>
	</div>

	<div class="footer" style="padding: 0px 33px; margin-top: 18px;">
		<div class="copyr" style="float: left; font-size: 12px;">&copy;2015 Whatsnew Co.,Ltd. All Rights Reserved</div>
		<div class="links" style="float: right; font-size: 12px;">
			<a href="http://www.moxy.co.th/th/about-us/" style="color: #3c3d41;"><span>เกี่ยวกับเรา</span></a> |
			<a href="http://www.moxy.co.th/th/terms-and-conditions/" style="color: #3c3d41;"><span>ข้อกำหนดและเงื่อนไข</span></a> |
			<a href="http://www.moxy.co.th/th/privacy-policy/" style="color: #3c3d41;"><span>ความเป็นส่วนตัว</span></a>
		</div>
	</div>
	<div style="clear:both"></div>
	<hr style="margin: 40px auto; width: 95%;">

	<!--English Version-->
	<div class="mail-body" style="margin:50px auto; width: 95%;">
		<p>Dear {{var customer.name}},</p>

		<br/>
		<p>Welcome to Moxy!</p>

		<br/>
		<p>We are delighted to include you among our community of stylish shoppers. </p>
		<br/>

		<p>Remember your log-in credentials:</p>
		<p>{{var customer.email}}</p>
		<p>{{var customer.password}}</p>
<br/>

		<p>To celebrate your subscription, Moxy offers you a gift voucher of</p>
		<p><span style="color: #ee5191;">200 บาท*</span> using the code <span style="color: #ee5191;">MOXYLIFE</span></p>
		<br/>

		<p>Click <a href="https://www.moxy.co.th/en/" style="color: #3c3d41;">here</a> to start shopping!</p>
		<br/>

		<p>Warm regards, </p>
		<p>Moxy Team</p>
		<p style="color: grey; font-style: italic; font-size: 12px;">*With a minimum purchase of 1,000 THB</p>
		<p style="color: grey; font-style: italic; font-size: 12px;">*Voucher is valid for one use only</p>
	</div>

	<div class="navigation-th" style="margin-top: 24px; overflow:hidden;">
		<div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #ee5191; border-bottom: 2px solid #ee5191; text-align:center;">
			<span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 16; padding: 0px 8px;"><a href="http://www.moxy.co.th/en/lafema.html" style="color: #3c3d41; text-decoration: none;">Beauty</a></span>
			<span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 16; padding: 0px 8px;"><a href="http://www.moxy.co.th/en/fashion.html" style="color: #3c3d41; text-decoration: none;">Fashion</a></span>
			<span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 16; padding: 0px 8px;"><a href="http://www.moxy.co.th/en/home-living.html" style="color: #3c3d41; text-decoration: none;">Home Decor</a></span>
			<span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 16; padding: 0px 8px;"><a href="http://www.moxy.co.th/en/gadgets-electronics.html" style="color: #3c3d41; text-decoration: none;">Electronics</a></span>
			<span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 16; padding: 0px 8px;"><a href="http://www.moxy.co.th/en/venbi.html" style="color: #3c3d41; text-decoration: none;">Mom &amp; Baby</a></span>
			<span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 16; padding: 0px 8px;"><a href="http://www.moxy.co.th/en/sanoga.html" style="color: #3c3d41; text-decoration: none;">Health &amp; Sports</a></span>
			<span class="nav-item" style="line-height: 33px; font-size: 16; padding: 0px 8px;"><a href="http://www.moxy.co.th/en/petloft.html" style="color: #3c3d41; text-decoration: none;">Pets</a></span>
		</div>
	</div>

	<div class="email-footer" style="min-height: 82px; max-heigh: 82px; background: #bbb; margin-top: 8px; color: #fff; padding: 0px 15px;">
		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px"><img src="{{skin _area="frontend"  url="images/email/mailbox.png"}}" style="width:18px;"/> Contact Us</p>
			<p style="font-size: 10px; margin: 0;">02-106-8222</p>
			<p style="margin: 0; font-size: 10px;"><a href="mailto:support@moxy.co.th" style="color: #fff; text-decoration: none;">support@moxy.co.th</a></p>
		</div>

		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">Follow Us</p>
			<div class="follow-icons" style="width: 100%">
				<a href="https://www.facebook.com/moxyst"><img src="{{skin _area="frontend"  url="images/email/facebook_follow.png"}}" style="width: 25px;" alt="Facebook fanpage"></a>
				<a href="https://twitter.com/moxyst"><img src="{{skin _area="frontend"  url="images/email/twitter_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
				<a href="https://webstagra.ms/moxyst/"><img src="{{skin _area="frontend"  url="images/email/instagram_follow.png"}}" style="width: 25px;" alt="Instagram fanpage"></a>
				<a href="https://www.pinterest.com/moxyst/"><img src="{{skin _area="frontend"  url="images/email/pinterest_follow.png"}}" style="width: 25px;" alt="Pinterest fanpage"></a>
				<a href="https://plus.google.com/+Moxyst"><img src="{{skin _area="frontend"  url="images/email/google_follow.png"}}" style="width: 25px;" alt="Google fanpage"></a>
			</div>
		</div>

		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">Payment Options</p>
			<img src="{{skin _area="frontend"  url="images/email/payment.png"}}" alt="Payment Options" style="width: 109px;">
		</div>
	</div>

	<div class="footer" style="padding: 0px 33px; margin-top: 18px;">
		<div class="copyr" style="float: left; font-size: 12px;">&copy;2015 Whatsnew Co.,Ltd. All Rights Reserved</div>
		<div class="links" style="float: right; font-size: 12px;">
			<a href="http://www.moxy.co.th/en/about-us/" style="color: #3c3d41;"><span>About Us</span></a> |
			<a href="http://www.moxy.co.th/en/terms-and-conditions/" style="color: #3c3d41;"><span>Term & Conditions</span></a> |
			<a href="http://www.moxy.co.th/en/privacy-policy/" style="color: #3c3d41;"><span>Privacy Policy</span></a>
		</div>
	</div>
	<div style="clear:both"></div>
</div>
HTML;


$emailTemplateModel->setTemplateText($templateContent);
$emailTemplateModel->save();
$emailTemplateId = $emailTemplateModel->getId();

/** @var Mage_Core_Model_Config $coreConfigModel */
$coreConfigModel = Mage::getModel('core/config');
$coreConfigModel->saveConfig("customer/create_account/email_template", $emailTemplateId);
