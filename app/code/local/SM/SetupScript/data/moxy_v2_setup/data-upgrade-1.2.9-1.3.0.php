<?php
/** @var Mage_Core_Model_Resource_Setup $this */
$installer = $this;
$installer->startSetup();

//// Force the store to be admin
//Mage::app()->setUpdateMode(false);
//Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
//if (!Mage::registry('isSecureArea'))
//    Mage::register('isSecureArea', 1);

$moxyen = Mage::getModel('core/store')->load('moxyen', 'code')->getId();
$moxyth = Mage::getModel('core/store')->load('moxyth', 'code')->getId();

//==========================================================================
// Site Footer Links
//==========================================================================
$blockId = 'footer_links_1';

/** @var Mage_Cms_Model_Block $block */
$blocks = Mage::getModel('cms/block')->getCollection()
    ->addFieldToFilter('identifier', $blockId);

foreach ($blocks as $block) {
    if ($block->getId()) $block->delete(); // if exists then delete
}

// English
$blockTitle = 'Site Footer Links EN';
$blockId = 'footer_links_1';
$blockStores = array($moxyen);

$content = <<<EOD
<div class="col-link">
    <h3 class="title col1">Information</h3>
    <ul class="footer_links col1">
        <li><a href="{{store url='about-us'}}">About Us</a></li>
        <li><a href="{{store url='community'}}">Community</a></li>
        <li><a href="{{store url='newsroom'}}">Newsroom</a></li>
        <li><a href="{{store url='careers'}}">Careers</a></li>
        <!--<li><a href="http://www.whatsnew.asia/">Corporate</a></li>-->
        <li><a href="{{store url='contacts'}}">Contact Us</a></li>
    </ul>
</div>
<div class="col-link">
    <h3 class="title col2">Security &amp; Privacy</h3>
    <ul class="footer_links col2">
        <li><a href="{{store url='terms-and-conditions'}}">Terms &amp; Conditions</a></li>
        <li><a href="{{store url='privacy-policy'}}">Privacy Policy</a></li>
    </ul>
</div>
<div class="col-link">
    <h3 class="title col3">Services &amp; Support</h3>
    <ul class="footer_links col3">
        <li><a href="{{store url='help'}}">Help &amp; FAQ</a></li>
        <li><a onclick="location.reload();" href="{{store url='help'}}#howtoorder">How To Order</a></li>
        <li><a onclick="location.reload();" href="{{store url='help'}}#payments">Payments</a></li>
        <li><a onclick="location.reload();" href="{{store url='help'}}#rewardpoints">Reward Points</a></li>
    </ul>
</div>
<div class="col-link">
    <ul class="footer_links col4">
        <li><a onclick="location.reload();" href="{{store url='help'}}#subscriptions">Product Autoship</a></li>
        <li><a onclick="location.reload();" href="{{store url='help'}}#shipping">Shipping &amp; Delivery</a></li>
        <li><a onclick="location.reload();" href="{{store url='help'}}#returns">Cancellation &amp; Returns</a></li>
        <li><a href="{{store url='return-policy'}}">Return Policy</a></li>
    </ul>
</div>
<script type="text/javascript">// <![CDATA[
    jQuery(document).ready(function () {

        var checking_size = 690;

        jQuery(window).resize(function () {
            var maxwidth = jQuery(window).width();
            if(maxwidth <= checking_size){
                jQuery('ul.footer_links').hide();
                jQuery('h3.title').css('cursor', 'pointer');
            }else{
                jQuery('ul.footer_links').show();
                jQuery('h3.title').css('cursor', 'inherit');
            }
        });
        jQuery('h3.title.col1').click(function () {
            if(jQuery(window).width() <= checking_size){
                jQuery('ul.footer_links.col1').toggle(200);
            }
        });
        jQuery('h3.title.col2').click(function () {
            if(jQuery(window).width() <= checking_size){
                jQuery('ul.footer_links.col2').toggle(200);
            }
        });
        jQuery('h3.title.col3').click(function () {
            if(jQuery(window).width() <= checking_size){
                jQuery('ul.footer_links.col3').toggle(200);
                jQuery('ul.footer_links.col4').toggle(200);
            }
        });
    });
// ]]></script>
EOD;

$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockId);
$block->setStores($blockStores);
$block->setIsActive(1);
$block->setContent($content);
$block->save();


// Thailand
$blockTitle = 'Site Footer Links TH';
$blockId = 'footer_links_1';
$blockStores = array($moxyth);

$content = <<<EOD
<div class="col-link">
    <h3 class="title col1">ข้อมูล</h3>
    <ul class="footer_links col1">
        <li><a href="{{store url='about-us'}}">เกี่ยวกับเรา</a></li>
        <li><a href="{{store url='community'}}">คอมมูนิตี้</a></li>
        <li><a href="{{store url='newsroom'}}">ห้องข่าว</a></li>
        <li><a href="{{store url='careers'}}">ร่วมงานกับเรา</a></li>
        <!--<li><a href="http://www.whatsnew.asia/">องค์กร</a></li>-->
        <li><a href="{{store url='contacts'}}">ติดต่อเรา</a></li>
    </ul>
</div>
<div class="col-link">
    <h3 class="title col2">นโยบายความเป็นส่วนตัว</h3>
    <ul class="footer_links col2">
        <li><a href="{{store url='terms-and-conditions'}}">ข้อกำหนดและเงื่อนไข</a></li>
        <li><a href="{{store url='privacy-policy'}}">ความเป็นส่วนตัว</a></li>
    </ul>
</div>
<div class="col-link">
    <h3 class="title col3">ช่วยเหลือ</h3>
    <ul class="footer_links col3">
        <li><a href="{{store url='help'}}">คำถามที่พบบ่อย</a></li>
        <li><a onclick="location.reload();" href="{{store url='help'}}#howtoorder">วิธีการสั่งซื้อ</a></li>
        <li><a onclick="location.reload();" href="{{store url='help'}}#payments">การชำระเงิน</a></li>
        <li><a onclick="location.reload();" href="{{store url='help'}}#rewardpoints">คะแนนสะสม</a></li>
    </ul>
</div>
<div class="col-link">
    <ul class="footer_links col4">
        <li><a onclick="location.reload();" href="{{store url='help'}}#subscriptions">การสั่งซื้อแบบต่อเนื่อง</a></li>
        <li><a onclick="location.reload();" href="{{store url='help'}}#shipping">การจัดส่งสินค้า</a></li>
        <li><a onclick="location.reload();" href="{{store url='help'}}#returns">การคืนสินค้าและขอคืนเงิน</a></li>
        <li><a href="{{store url='return-policy'}}">นโยบายการคืนสินค้า</a></li>
    </ul>
</div>
<script type="text/javascript">// <![CDATA[
    jQuery(document).ready(function () {

        var checking_size = 690;

        jQuery(window).resize(function () {
            var maxwidth = jQuery(window).width();
            if(maxwidth <= checking_size){
                jQuery('ul.footer_links').hide();
                jQuery('h3.title').css('cursor', 'pointer');
            }else{
                jQuery('ul.footer_links').show();
                jQuery('h3.title').css('cursor', 'inherit');
            }
        });
        jQuery('h3.title.col1').click(function () {
            if(jQuery(window).width() <= checking_size){
                jQuery('ul.footer_links.col1').toggle(200);
            }
        });
        jQuery('h3.title.col2').click(function () {
            if(jQuery(window).width() <= checking_size){
                jQuery('ul.footer_links.col2').toggle(200);
            }
        });
        jQuery('h3.title.col3').click(function () {
            if(jQuery(window).width() <= checking_size){
                jQuery('ul.footer_links.col3').toggle(200);
                jQuery('ul.footer_links.col4').toggle(200);
            }
        });
    });
// ]]></script>
EOD;

$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockId);
$block->setStores($blockStores);
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================

$installer->endSetup();