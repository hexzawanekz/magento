<?php
/**
 * Smartosc Ha Noi
 * ductm1@smartosc.com
 * Back in stock (MOXY THAILAND)
 */

$template = Mage::getModel('core/email_template');

$template_text = <<<HTML
<div class="product-heading" style="text-align: center; width: 590px;">
			<h3>{{var product}}</h3>
		</div>
		<div class="email-wrapper" style="width: 590px;">
			<div class="logo-left" style="display: inline-block;">
				<a href="http://www.moxyst.com/?utm_source=edm&amp;utm_medium=header_middle&amp;utm_campaign=20150615_mx_intropage"><img alt="Logo-Left" src="{{skin url="images/backinstock/header_th.jpg"}}"></a>
			</div>
		</div>
		<div class="navigation" style="width: 590px; text-align: center;">
			<ul class="nav">
				<li class="nav-item" style="display: inline-block;border-right: 2px solid rgb(90, 88, 89); padding-right: 20px;">
					<a href="http://www.moxyst.com/th/fashion.html?utm_source=edm&amp;utm_medium=nav_makeup&amp;utm_campaign=20150615_mx_intropage"
					style="color: rgb(90, 88, 89); text-decoration: none; font-weight: bold;">FASHION</a>
				</li>
				<li class="nav-item" style="display: inline-block;border-right: 2px solid rgb(90, 88, 89); padding-right: 20px;">
					<a href="http://www.moxyst.com/th/gadgets-and-electronics.html?utm_source=edm&amp;utm_medium=nav_skincare&amp;utm_campaign=20150615_mx_intropage"
					style="color: rgb(90, 88, 89); text-decoration: none; font-weight: bold;">GADGETS</a>
				</li>
				<li class="nav-item" style="display: inline-block;border-right: 2px solid rgb(90, 88, 89); padding-right: 20px;">
					<a href="http://www.moxyst.com/th/home-decor.html?utm_source=edm&amp;utm_medium=nav_bathbody&amp;utm_campaign=20150615_mx_intropage"
					style="color: rgb(90, 88, 89); text-decoration: none; font-weight: bold;">HOME</a>
				</li>
				<li class="nav-item" style="display: inline-block;border-right: 2px solid rgb(90, 88, 89); padding-right: 20px;">
					<a href="http://www.moxyst.com/th/home-appliances.html?utm_source=edm&amp;utm_medium=nav_fragrance&amp;utm_campaign=20150615_mx_intropage"
					style="color: rgb(90, 88, 89); text-decoration: none; font-weight: bold;">APPLIANCES</a>
				</li>
				<li class="nav-item" style="display: inline-block;padding-right: 20px;">
					<a href="http://www.moxyst.com/th/lifestyle.html?utm_source=edm&amp;utm_medium=nav_brands&amp;utm_campaign=20150615_mx_intropage"
					style="color: rgb(90, 88, 89); text-decoration: none; font-weight: bold;">LIFESTYLE</a>
				</li>
			</ul>
		</div>
		<div style="text-align: center; width: 590px;" class="email-banner">
			<a href="http://www.moxyst.com/th?utm_source=edm&amp;utm_medium=hero_banner&amp;utm_campaign=20150615_mx_intropage"><img width="" style="width: 590px;" alt="Moxyst Banner" src="{{skin url="images/backinstock/hero-banner.jpg"}}"></a>
		</div>
		<div style="width: 590px; padding: 1px;" class="email-content">
			<p style="font-size:110%">เรียน คุณ {{var customer}},</p>
    		<p>เนื่องจากในวันที่ {{var date}} คุณได้อีกเมลถามเกี่ยวกับสินค้า {{var product}} ทาง Moxy</p>
    		<p>มีความยินดีจะแจ้งให้ท่านทราบว่า ขณะนี้เรามีสินค้าตัวนี้ในสต็อกแล้วค่ะ</p><br/>
    		<p>ซื้อเลยตอนนี้ !</p>
			<div class="email-table" style="width: 568px; border: 1px solid rgba(0, 0, 0, 0.2); min-height: 200px; padding: 10px; font-family: Tahoma;">
				<div class="product-photo" style="width: 200px; float : left; max-height: 200px;">
					<a href="{{var product_url}}?utm_source=edm&amp;utm_medium=reminder&amp;utm_campaign=moxy_outofstock_reminder"><img src="{{var product_image}}" style="width:200px;" alt="{{var product}}"></a>
				</div>
				<div class="product-name" style="float: left; min-height: 100px; width: 207px; margin-top: 65px; text-align:center;">
					<p>{{var product}}</p>
				</div>
				<div class="product-more" style="border-left: 1px solid rgba(0, 0, 0, 0.33); float: left; min-height: 200px; width: 150px; padding-left: 10px; text-align: center; color : #5a5859;">
					<p class="strong" style="font-size: 33px;font-weight: bold;margin: 0;padding: 0;">ด่วน!</p>
		            <p class="medium" style="font-size : 24px;">สินค้ามีจำนวนจำกัด</p>
					<div class="buynow-btn" style="background: rgb(73, 69, 70) none repeat scroll 0px 0px; padding: 0px;">
						<a style="color: rgb(255, 255, 255); text-decoration: none; font-size: 20px; display: block; line-height: 100%; padding: 15px; margin-bottom: 0px;"
						href="{{var product_url}}?utm_source=edm&amp;utm_medium=reminder&amp;utm_campaign=moxy_outofstock_reminder">ซื้อเลย!</a>
					</div>
				</div>
				<div style="clear:both;"></div>
			</div>
		</div>
		<div class="email-footer" style="width: 590px;">
			<div class="email-footer-main">
				<a href="http://www.moxyst.com/?utm_source=edm&amp;utm_medium=footer&amp;utm_campaign=20150615_mx_intropage">
					<img alt="Moxy Footer" style="width:590px;" src="{{skin url="images/backinstock/footer_th.jpg"}}"> <!--{{skin url="images/backinstock/footer.jpg"}}-->
				</a>
			</div>

			<div class="email-footer-3-cols">
				<div class="email-footer-left" style="float:left;">
					<img src="{{skin url="images/backinstock/footer_1.jpg"}}" alt="MOXY"/>
				</div>
				<div class="email-footer-mid" style="float:left;">
					<img src="{{skin url="images/backinstock/footer_2.jpg"}}" alt="MOXY"/>
				</div>
				<div class="email-footer-right" style="float:left; width: 190px; text-align : center;">
					<img src="{{skin url="images/backinstock/footer_3.jpg"}}" alt="MOXY" style="width : 100%;"/>
					<div class="social-facebook" style="padding-right: 4px; border-right: 1px dotted rgb(204, 204, 204); display: inline-block;"><a href="https://www.facebook.com/moxyst"><img src="{{skin url="images/backinstock/facebook_icon.jpg"}}" alt="Facebook Page of Moxy" style="width : 35px; min-height:35px;" /></a></div>
					<div class="social-twit" style="padding-right: 4px; border-right: 1px dotted rgb(204, 204, 204); display: inline-block;"><a href="https://twitter.com/moxyst"><img src="{{skin url="images/backinstock/twitter_icon.jpg"}}" alt="Twitter of Moxy" style="width : 35px; min-height:35px;" /></a></div>
					<div class="social-pin" style="padding-right: 4px; border-right: 1px dotted rgb(204, 204, 204); display: inline-block;"><a href="https://www.pinterest.com/moxyst/"><img src="{{skin url="images/backinstock/pinterest_icon.jpg"}}" alt="Pinterest Moxy Street" style="width : 35px; min-height:35px;" /></a></div>
					<div class="social-gplus" style="display : inline-block; padding-right:4px;"><a href="https://plus.google.com/+Moxyst"><img src="{{skin url="images/backinstock/gplus_icon.jpg"}}" alt="Google Plus of Moxy" style="width : 35px; min-height:35px;" /></a></div>
				</div>
				<div style="clear:both;"></div>
			</div>
		</div>
HTML;

/** @var Mage_Core_Model_Email_Template $model */
$model = Mage::getModel('core/email_template');
$model->loadByCode('Back in stock (MOXY THAILAND)')
    ->setTemplateCode('Back in stock (MOXY THAILAND)')
    ->setTemplateText($template_text)
    ->setTemplateSubject('{{var product}} is back in stock!')
    ->setTemplateType(Mage_Core_Model_Email_Template::TYPE_HTML);

try {
    $model->save();
} catch (Mage_Exception $e) {
    throw $e;
}