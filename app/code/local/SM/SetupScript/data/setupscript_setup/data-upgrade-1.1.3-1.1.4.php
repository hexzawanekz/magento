<?php
try {
    $installer = $this;
    $installer->startSetup();

    // Force the store to be admin
    Mage::app()->setUpdateMode(false);
    Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
    if (!Mage::registry('isSecureArea'))
        Mage::register('isSecureArea', 1);

    // get store view ids
    $venbiEn = Mage::app()->getStore('ven')->getId();
    $venbiMobi = Mage::app()->getStore('vm')->getId();
    $venbiTh = Mage::app()->getStore('vth')->getId();
    //
    $sanogaEn = Mage::app()->getStore('sen')->getId();
    $sanogaMobi = Mage::app()->getStore('sm')->getId();
    $sanogaTh = Mage::app()->getStore('sth')->getId();
    //
    $lafemaEn = Mage::app()->getStore('len')->getId();
    $lafemaMobi = Mage::app()->getStore('lm')->getId();
    $lafemaTh = Mage::app()->getStore('lth')->getId();

    //==========================================================================
    // Venbi Menu Footer Help Links
    //==========================================================================
    $venbiContent = <<<EOD
    <ul class="level0">
        <li class="level1"><a title="FAQ" href="{{store url}}help/"> <span>FAQ</span> </a></li>
        <li class="level1"><a title="How To Order" href="{{store url}}help/#howtoorder"> <span>How To Order</span> </a></li>
        <li class="level1"><a title="Payments" href="{{store url}}help/#payments"> <span>Payments</span> </a></li>
        <li class="level1"><a title="Shipping &amp; Delivery" href="{{store url}}help/#shipping"> <span>Shipping &amp; Delivery</span> </a></li>
        <li class="level1"><a title="Reward Points" href="{{store url}}help/#rewardpoints"> <span>Reward Points</span> </a></li>
        <li class="level1"><a title="Cancellation &amp; Returns" href="{{store url}}help/#returns"> <span>Cancellation &amp; Returns</span> </a></li>
    </ul>
EOD;

    $venbiData = array(
        'title' => "Venbi Menu Footer Help Links",
        'identifier' => "menu_footer_help_links",
        'stores' => array($venbiEn, $venbiMobi, $venbiTh),
        'is_active' => 1,
        'content' => $venbiContent,
    );

    // check exists
    $blockVenbi = Mage::getModel('cms/block')->getCollection()
        ->addStoreFilter(array($venbiEn, $venbiMobi, $venbiTh), $withAdmin = false)
        ->addFieldToFilter('identifier', "menu_footer_help_links")
        ->getFirstItem();
    if ($blockVenbi->getId() == null) {
        // create block
        $blockVenbi = Mage::getModel('cms/block');
        $blockVenbi->setData($venbiData);
        $blockVenbi->save();
    }
    //==========================================================================
    //==========================================================================
    //==========================================================================


    //==========================================================================
    // Sanoga Menu Footer Help Links
    //==========================================================================
    $sanogaContent = <<<EOD
    <ul class="level0">
        <li class="level1"><a title="FAQ" href="{{store url}}help/"> <span>FAQ</span> </a></li>
        <li class="level1"><a title="How To Order" href="{{store url}}help/#howtoorder"> <span>How To Order</span> </a></li>
        <li class="level1"><a title="Payments" href="{{store url}}help/#payments"> <span>Payments</span> </a></li>
        <li class="level1"><a title="Shipping &amp; Delivery" href="{{store url}}help/#shipping"> <span>Shipping &amp; Delivery</span> </a></li>
        <li class="level1"><a title="Reward Points" href="{{store url}}help/#rewardpoints"> <span>Reward Points</span> </a></li>
        <li class="level1"><a title="Cancellation &amp; Returns" href="{{store url}}help/#returns"> <span>Cancellation &amp; Returns</span> </a></li>
    </ul>
EOD;

    $sanogaData = array(
        'title' => "Sanoga Menu Footer Help Links",
        'identifier' => "menu_footer_help_links",
        'stores' => array($sanogaEn, $sanogaMobi, $sanogaTh),
        'is_active' => 1,
        'content' => $sanogaContent,
    );

    // check exists
    $blockSanoga = Mage::getModel('cms/block')->getCollection()
        ->addStoreFilter(array($sanogaEn, $sanogaMobi, $sanogaTh), $withAdmin = false)
        ->addFieldToFilter('identifier', "menu_footer_help_links")
        ->getFirstItem();
    if ($blockSanoga->getId() == null) {
        // create block
        $blockSanoga = Mage::getModel('cms/block');
        $blockSanoga->setData($sanogaData);
        $blockSanoga->save();
    }
    //==========================================================================
    //==========================================================================
    //==========================================================================


    //==========================================================================
    // Lafema Menu Footer Help Links
    //==========================================================================
    $lafemaContent = <<<EOD
    <ul class="level0">
        <li class="level1"><a title="FAQ" href="{{store url}}help/"> <span>FAQ</span> </a></li>
        <li class="level1"><a title="How To Order" href="{{store url}}help/#howtoorder"> <span>How To Order</span> </a></li>
        <li class="level1"><a title="Payments" href="{{store url}}help/#payments"> <span>Payments</span> </a></li>
        <li class="level1"><a title="Shipping &amp; Delivery" href="{{store url}}help/#shipping"> <span>Shipping &amp; Delivery</span> </a></li>
        <li class="level1"><a title="Reward Points" href="{{store url}}help/#rewardpoints"> <span>Reward Points</span> </a></li>
        <li class="level1"><a title="Cancellation &amp; Returns" href="{{store url}}help/#returns"> <span>Cancellation &amp; Returns</span> </a></li>
    </ul>
EOD;

    $lafemaData = array(
        'title' => "Lafema Menu Footer Help Links",
        'identifier' => "menu_footer_help_links",
        'stores' => array($lafemaEn, $lafemaMobi, $lafemaTh),
        'is_active' => 1,
        'content' => $lafemaContent,
    );

    // check exists
    $blockLafema = Mage::getModel('cms/block')->getCollection()
        ->addStoreFilter(array($lafemaEn, $lafemaMobi, $lafemaTh), $withAdmin = false)
        ->addFieldToFilter('identifier', "menu_footer_help_links")
        ->getFirstItem();
    if ($blockLafema->getId() == null) {
        // create block
        $blockLafema = Mage::getModel('cms/block');
        $blockLafema->setData($lafemaData);
        $blockLafema->save();
    }
    //==========================================================================
    //==========================================================================
    //==========================================================================

    $installer->endSetup();
} catch (Excpetion $e) {
    Mage::logException($e);
    Mage::log("ERROR IN SETUP " . $e->getMessage());
}