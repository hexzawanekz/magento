<?php
try {
    $installer = $this;
    $installer->startSetup();

    // Force the store to be admin
    Mage::app()->setUpdateMode(false);
    Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
    if (!Mage::registry('isSecureArea'))
        Mage::register('isSecureArea', 1);

    // get store view ids
    $moxyEn = Mage::app()->getStore('moxyen')->getId();
    $moxyTh = Mage::app()->getStore('moxyth')->getId();


    //==========================================================================
    // Moxy Mobile Home Banner
    //==========================================================================
    $moxyHomeContent = <<<EOD
    <p><a title="Homepage" href="#"> <img style="width: 100%;" src="{{media url='wysiwyg/moxy_mobile_home_banner_welcome300.jpg'}}" alt="Homepage" /> </a></p>
EOD;

    // check exists
    /** @var Mage_Cms_Model_Block $moxyHomeBlock */
    $moxyHomeBlock = Mage::getModel('cms/block')->getCollection()
        ->addStoreFilter(array($moxyEn, $moxyTh), $withAdmin = false)
        ->addFieldToFilter('identifier', "home_banner")
        ->getFirstItem();
    // delete old if exists
    if ($moxyHomeBlock->getId())
        $moxyHomeBlock->delete();
    // save new content
    $moxyHomeBlock = Mage::getModel('cms/block');
    $moxyHomeBlock->setTitle('Moxy Mobile Home Banner');
    $moxyHomeBlock->setIdentifier('home_banner');
    $moxyHomeBlock->setStores(array($moxyEn, $moxyTh));
    $moxyHomeBlock->setIsActive(1);
    $moxyHomeBlock->setContent($moxyHomeContent);
    $moxyHomeBlock->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================



    //==========================================================================
    // Moxy Mobile Fashion Category Banner
    //==========================================================================
    $moxyFashionContent = <<<EOD
    <p><a title="Fashion" href="{{store url}}brand/fashion"> <img style="width: 100%;" src="{{media url='wysiwyg/moxy_mobile_fashion_banner17.jpg'}}" alt="Fashion" /> </a></p>
EOD;

    // check exists
    /** @var Mage_Cms_Model_Block $moxyFashionBlock */
    $moxyFashionBlock = Mage::getModel('cms/block')->getCollection()
        ->addStoreFilter(array($moxyEn, $moxyTh), $withAdmin = false)
        ->addFieldToFilter('identifier', "moxy_fashion_banner")
        ->getFirstItem();
    // delete old if exists
    if ($moxyFashionBlock->getId())
        $moxyFashionBlock->delete();
    // save new content
    $moxyFashionBlock = Mage::getModel('cms/block');
    $moxyFashionBlock->setTitle('Moxy Mobile Fashion Category Banner');
    $moxyFashionBlock->setIdentifier('moxy_fashion_banner');
    $moxyFashionBlock->setStores(array($moxyEn, $moxyTh));
    $moxyFashionBlock->setIsActive(1);
    $moxyFashionBlock->setContent($moxyFashionContent);
    $moxyFashionBlock->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================



    //==========================================================================
    // Moxy Mobile Home-Living Category Banner
    //==========================================================================
    $moxyLivingContent = <<<EOD
    <p><a title="Home-living" href="{{store url}}brand/homeliving"> <img style="width: 100%;" src="{{media url='wysiwyg/moxy_mobile_homeliving_banner17.png'}}" alt="Home-living" /> </a></p>
EOD;

    // check exists
    /** @var Mage_Cms_Model_Block $moxyLivingBlock */
    $moxyLivingBlock = Mage::getModel('cms/block')->getCollection()
        ->addStoreFilter(array($moxyEn, $moxyTh), $withAdmin = false)
        ->addFieldToFilter('identifier', "moxy_living_banner")
        ->getFirstItem();
    // delete old if exists
    if ($moxyLivingBlock->getId())
        $moxyLivingBlock->delete();
    // save new content
    $moxyLivingBlock = Mage::getModel('cms/block');
    $moxyLivingBlock->setTitle('Moxy Mobile Home-Living Category Banner');
    $moxyLivingBlock->setIdentifier('moxy_living_banner');
    $moxyLivingBlock->setStores(array($moxyEn, $moxyTh));
    $moxyLivingBlock->setIsActive(1);
    $moxyLivingBlock->setContent($moxyLivingContent);
    $moxyLivingBlock->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================



    //==========================================================================
    // Moxy Mobile Electronics Category Banner
    //==========================================================================
    $moxyElectronicsContent = <<<EOD
    <p><a title="Electronics" href="{{store url}}brand/electronics"> <img style="width: 100%;" src="{{media url='wysiwyg/moxy_mobile_electronics_banner17.jpg'}}" alt="Electronics" /> </a></p>
EOD;

    // check exists
    /** @var Mage_Cms_Model_Block $moxyElectronicsBlock */
    $moxyElectronicsBlock = Mage::getModel('cms/block')->getCollection()
        ->addStoreFilter(array($moxyEn, $moxyTh), $withAdmin = false)
        ->addFieldToFilter('identifier', "moxy_electronics_banner")
        ->getFirstItem();
    // delete old if exists
    if ($moxyElectronicsBlock->getId())
        $moxyElectronicsBlock->delete();
    // save new content
    $moxyElectronicsBlock = Mage::getModel('cms/block');
    $moxyElectronicsBlock->setTitle('Moxy Mobile Electronics Category Banner');
    $moxyElectronicsBlock->setIdentifier('moxy_electronics_banner');
    $moxyElectronicsBlock->setStores(array($moxyEn, $moxyTh));
    $moxyElectronicsBlock->setIsActive(1);
    $moxyElectronicsBlock->setContent($moxyElectronicsContent);
    $moxyElectronicsBlock->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================

    $installer->endSetup();
} catch (Excpetion $e) {
    Mage::logException($e);
    Mage::log("ERROR IN SETUP " . $e->getMessage());
}