<?php
try {
    $installer = $this;
    $installer->startSetup();

    // Force the store to be admin
    Mage::app()->setUpdateMode(false);
    Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
    if (!Mage::registry('isSecureArea'))
        Mage::register('isSecureArea', 1);

    // get store view ids
    $moxyEn = Mage::app()->getStore('moxyen')->getId();
    $moxyTh = Mage::app()->getStore('moxyth')->getId();


    //==========================================================================
    // Moxy Mobile Menu Footer Follow Links
    //==========================================================================
    $moxyContent = <<<EOD
    <ul class="level0">
        <li class="level1"><a title="Facebook" href="https://www.facebook.com/moxyst"> <span>Facebook</span> </a></li>
        <li class="level1"><a title="Twitter" href="https://twitter.com/moxyst"> <span>Twitter</span> </a></li>
        <li class="level1"><a title="Google" href="https://plus.google.com/+Moxyst/videos"> <span>Google</span> </a></li>
        <li class="level1"><a title="Newsletter" href="http://www.moxyst.com/newsletter-archive/"> <span>Newsletter</span> </a></li>
    </ul>
EOD;

    // check exists
    /** @var Mage_Cms_Model_Block $moxyFollowBlock */
    $moxyFollowBlock = Mage::getModel('cms/block')->getCollection()
        ->addStoreFilter(array($moxyEn, $moxyTh), $withAdmin = false)
        ->addFieldToFilter('identifier', "menu_footer_follow_links")
        ->getFirstItem();
    // delete old if exists
    if ($moxyFollowBlock->getId())
        $moxyFollowBlock->delete();
    // save new content
    $moxyFollowBlock = Mage::getModel('cms/block');
    $moxyFollowBlock->setTitle('Moxy Mobile Menu Footer Follow Links');
    $moxyFollowBlock->setIdentifier('menu_footer_follow_links');
    $moxyFollowBlock->setStores(array($moxyEn, $moxyTh));
    $moxyFollowBlock->setIsActive(1);
    $moxyFollowBlock->setContent($moxyContent);
    $moxyFollowBlock->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================

    //==========================================================================
    // Moxy Mobile Menu Footer Information Links
    //==========================================================================
    $moxyInfoContent = <<<EOD
    <ul class="level0">
        <li class="level1"><a title="Return Policy" href="{{store url}}return-policy/"> <span>Return Policy</span> </a></li>
        <li class="level1"><a title="About Us" href="{{store url}}about-us/"> <span>About Us</span> </a></li>
        <li class="level1"><a title="Terms &amp; Conditions" href="{{store url}}terms-and-conditions/"> <span>Terms &amp; Conditions</span> </a></li>
        <li class="level1"><a title="Privacy Policy" href="{{store url}}privacy-policy/"> <span>Privacy Policy</span> </a></li>
    </ul>
EOD;

    // check exists
    /** @var Mage_Cms_Model_Block $moxyInfoBlock */
    $moxyInfoBlock = Mage::getModel('cms/block')->getCollection()
        ->addStoreFilter(array($moxyEn, $moxyTh), $withAdmin = false)
        ->addFieldToFilter('identifier', "menu_footer_info_links")
        ->getFirstItem();
    // delete old if exists
    if ($moxyInfoBlock->getId())
       $moxyInfoBlock->delete();
    // save new content
    $moxyInfoBlock = Mage::getModel('cms/block');
    $moxyInfoBlock->setTitle('Moxy Mobile Menu Footer Information Links');
    $moxyInfoBlock->setIdentifier('menu_footer_info_links');
    $moxyInfoBlock->setStores(array($moxyEn, $moxyTh));
    $moxyInfoBlock->setIsActive(1);
    $moxyInfoBlock->setContent($moxyInfoContent);
    $moxyInfoBlock->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================

    //==========================================================================
    // Moxy Mobile Menu Footer Help Links
    //==========================================================================
    $moxyHelpContent = <<<EOD
    <ul class="level0">
        <li class="level1"><a title="FAQ" href="{{store url}}help/"> <span>FAQ</span> </a></li>
        <li class="level1"><a title="How To Order" href="{{store url}}help/#howtoorder"> <span>How To Order</span> </a></li>
        <li class="level1"><a title="Payments" href="{{store url}}help/#payments"> <span>Payments</span> </a></li>
        <li class="level1"><a title="Shipping &amp; Delivery" href="{{store url}}help/#shipping"> <span>Shipping &amp; Delivery</span> </a></li>
        <li class="level1"><a title="Reward Points" href="{{store url}}help/#rewardpoints"> <span>Reward Points</span> </a></li>
        <li class="level1"><a title="Cancellation &amp; Returns" href="{{store url}}help/#returns"> <span>Cancellation &amp; Returns</span> </a></li>
    </ul>
EOD;

    // check exists
    /** @var Mage_Cms_Model_Block $moxyHelpBlock */
    $moxyHelpBlock = Mage::getModel('cms/block')->getCollection()
        ->addStoreFilter(array($moxyEn, $moxyTh), $withAdmin = false)
        ->addFieldToFilter('identifier', "menu_footer_help_links")
        ->getFirstItem();
    // delete old if exists
    if ($moxyHelpBlock->getId())
        $moxyHelpBlock->delete();
    // save new content
    $moxyHelpBlock = Mage::getModel('cms/block');
    $moxyHelpBlock->setTitle('Moxy Mobile Menu Footer Help Links');
    $moxyHelpBlock->setIdentifier('menu_footer_help_links');
    $moxyHelpBlock->setStores(array($moxyEn, $moxyTh));
    $moxyHelpBlock->setIsActive(1);
    $moxyHelpBlock->setContent($moxyHelpContent);
    $moxyHelpBlock->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================

    $installer->endSetup();
} catch (Excpetion $e) {
    Mage::logException($e);
    Mage::log("ERROR IN SETUP " . $e->getMessage());
}