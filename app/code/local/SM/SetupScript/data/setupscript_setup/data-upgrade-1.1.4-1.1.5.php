<?php

// Base-name
$name           = 'moxy';
$engStoreName   = 'Moxy English';
$engStoreCode   = 'moxyen';
$thStoreName    = 'Moxy Thai';
$thStoreCode    = 'moxyth';
$storeIdMoxyEn  = Mage::getModel('core/store')->load($engStoreCode, 'code')->getId();
$storeIdMoxyTh  = Mage::getModel('core/store')->load($thStoreCode, 'code')->getId();

try {
    $installer = $this;
    $installer->startSetup();

    // Force the store to be admin
    Mage::app()->setUpdateMode(false);
    Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
    if (!Mage::registry('isSecureArea'))
        Mage::register('isSecureArea', 1);

    //==========================================================================
    // All Brands Petloft EN Page
    //==========================================================================
    $pageTitle = "All Brands";
    $pageIdentifier = "all-brands";
    $pageStores = array(1);
    $pageIsActive = 1;
    $pageUnderVersionControl = 0;
    $pageContentHeading = "";
    $pageContent = <<<EOD
<p>{{block type="core/template" name="all-brands" template="brands/all.phtml"}}</p>
<p>{{block type="cms/block" block_id="banners_home"}}</p>
EOD;

    $pageRootTemplate = 'one_column';
    $pageLayoutUpdateXml = <<<EOD
EOD;


    $pageCustomLayoutUpdateXML = <<<EOD
EOD;

    $pageMetaKeywords = "";
    $pageMetaDescription = "";

    $page = Mage::getModel('cms/page')->getCollection()
        ->addStoreFilter(array(1), $withAdmin = true)
        ->addFieldToFilter('identifier', $pageIdentifier)
        ->getFirstItem();
    if ($page->getId() == 0) {
        $page = Mage::getModel('cms/page');
    }
    else
    {
        // if exists then repair
        $page = Mage::getModel('cms/page')->load($page->getId());
    }

    $page->setTitle($pageTitle);
    $page->setIdentifier($pageIdentifier);
    $page->setStores($pageStores);
    $page->setIsActive($pageIsActive);
    $page->setUnderVersionControl($pageUnderVersionControl);
    $page->setContentHeading($pageContentHeading);
    $page->setContent($pageContent);
    $page->setRootTemplate($pageRootTemplate);
    $page->setLayoutUpdateXml($pageLayoutUpdateXml);
    $page->setCustomLayoutUpdateXml($pageCustomLayoutUpdateXML);
    $page->setMetaKeywords($pageMetaKeywords);
    $page->setMetaDescription($pageMetaDescription);
    $page->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================

    //==========================================================================
    // All Brands Petloft TH Page
    //==========================================================================
    $pageTitle = "ทุกแบรนด์";
    $pageIdentifier = "all-brands";
    $pageStores = array('2','3');
    $pageIsActive = 1;
    $pageUnderVersionControl = 0;
    $pageContentHeading = "";
    $pageContent = <<<EOD
<p>{{block type="core/template" name="all-brands" template="brands/all.phtml"}}</p>
<p>{{block type="cms/block" block_id="banners_home"}}</p>
EOD;

    $pageRootTemplate = 'one_column';
    $pageLayoutUpdateXml = <<<EOD
EOD;


    $pageCustomLayoutUpdateXML = <<<EOD
EOD;

    $pageMetaKeywords = "";
    $pageMetaDescription = "";

    $page = Mage::getModel('cms/page')->getCollection()
        ->addStoreFilter(array('2','3'), $withAdmin = true)
        ->addFieldToFilter('identifier', $pageIdentifier)
        ->getFirstItem();
    if ($page->getId() == 0) {
        $page = Mage::getModel('cms/page');
    }
    else
    {
        // if exists then repair
        $page = Mage::getModel('cms/page')->load($page->getId());
    }

    $page->setTitle($pageTitle);
    $page->setIdentifier($pageIdentifier);
    $page->setStores($pageStores);
    $page->setIsActive($pageIsActive);
    $page->setUnderVersionControl($pageUnderVersionControl);
    $page->setContentHeading($pageContentHeading);
    $page->setContent($pageContent);
    $page->setRootTemplate($pageRootTemplate);
    $page->setLayoutUpdateXml($pageLayoutUpdateXml);
    $page->setCustomLayoutUpdateXml($pageCustomLayoutUpdateXML);
    $page->setMetaKeywords($pageMetaKeywords);
    $page->setMetaDescription($pageMetaDescription);
    $page->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================

    //==========================================================================
    // All Brands Sanoga TH Page
    //==========================================================================
    $pageTitle = "แบรนด์สินค้า อาหารเสริม สินค้าสุขภาพและความงาม | Sanoga Thailand";
    $pageIdentifier = "all-brands";
    $pageStores = array('11','12');
    $pageIsActive = 1;
    $pageUnderVersionControl = 0;
    $pageContentHeading = "";
    $pageContent = <<<EOD
<p>{{block type="core/template" name="all-brands" template="brands/all.phtml"}}</p>
<p>{{block type="cms/block" block_id="banners_home"}}</p>
EOD;

    $pageRootTemplate = 'one_column';
    $pageLayoutUpdateXml = <<<EOD
EOD;


    $pageCustomLayoutUpdateXML = <<<EOD
EOD;

    $pageMetaKeywords = "";
    $pageMetaDescription = "ร้านขายผลิตภัณฑ์อาหารเสริมเพื่อสุขภาพออนไลน์ เครื่องสำอางและความงาม คุณภาพแท้ 100% จากแบรนด์ชั้นนำทั่วโลก สินค้ามี อย. ทุกชิ้น บริการจัดส่งฟรีถึงบ้าน";

    $page = Mage::getModel('cms/page')->getCollection()
        ->addStoreFilter(array('11','12'), $withAdmin = true)
        ->addFieldToFilter('identifier', $pageIdentifier)
        ->getFirstItem();
    if ($page->getId() == 0) {
        $page = Mage::getModel('cms/page');
    }
    else
    {
        // if exists then repair
        $page = Mage::getModel('cms/page')->load($page->getId());
    }

    $page->setTitle($pageTitle);
    $page->setIdentifier($pageIdentifier);
    $page->setStores($pageStores);
    $page->setIsActive($pageIsActive);
    $page->setUnderVersionControl($pageUnderVersionControl);
    $page->setContentHeading($pageContentHeading);
    $page->setContent($pageContent);
    $page->setRootTemplate($pageRootTemplate);
    $page->setLayoutUpdateXml($pageLayoutUpdateXml);
    $page->setCustomLayoutUpdateXml($pageCustomLayoutUpdateXML);
    $page->setMetaKeywords($pageMetaKeywords);
    $page->setMetaDescription($pageMetaDescription);
    $page->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================


    //==========================================================================
    // All Brands Sanoga EN Page
    //==========================================================================
    $pageTitle = "All Brands";
    $pageIdentifier = "all-brands";
    $pageStores = array(10);
    $pageIsActive = 1;
    $pageUnderVersionControl = 0;
    $pageContentHeading = "";
    $pageContent = <<<EOD
<p>{{block type="core/template" name="all-brands" template="brands/all.phtml"}}</p>
<p>{{block type="cms/block" block_id="banners_home"}}</p>
EOD;

    $pageRootTemplate = 'one_column';
    $pageLayoutUpdateXml = <<<EOD
EOD;


    $pageCustomLayoutUpdateXML = <<<EOD
EOD;

    $pageMetaKeywords = "";
    $pageMetaDescription = "";

    $page = Mage::getModel('cms/page')->getCollection()
        ->addStoreFilter(array(10), $withAdmin = true)
        ->addFieldToFilter('identifier', $pageIdentifier)
        ->getFirstItem();
    if ($page->getId() == 0) {
        $page = Mage::getModel('cms/page');
    }
    else
    {
        // if exists then repair
        $page = Mage::getModel('cms/page')->load($page->getId());
    }

    $page->setTitle($pageTitle);
    $page->setIdentifier($pageIdentifier);
    $page->setStores($pageStores);
    $page->setIsActive($pageIsActive);
    $page->setUnderVersionControl($pageUnderVersionControl);
    $page->setContentHeading($pageContentHeading);
    $page->setContent($pageContent);
    $page->setRootTemplate($pageRootTemplate);
    $page->setLayoutUpdateXml($pageLayoutUpdateXml);
    $page->setCustomLayoutUpdateXml($pageCustomLayoutUpdateXML);
    $page->setMetaKeywords($pageMetaKeywords);
    $page->setMetaDescription($pageMetaDescription);
    $page->save();
    //==========================================================================
    //==========================================================================

    //==========================================================================
    // All Brands Venbi EN Page
    //==========================================================================
    $pageTitle = "All Brands";
    $pageIdentifier = "all-brands";
    $pageStores = array(5);
    $pageIsActive = 1;
    $pageUnderVersionControl = 0;
    $pageContentHeading = "";
    $pageContent = <<<EOD
<p>{{block type="core/template" name="all-brands" template="brands/all.phtml"}}</p>
<p>{{block type="cms/block" block_id="banners_home"}}</p>
EOD;

    $pageRootTemplate = 'one_column';
    $pageLayoutUpdateXml = <<<EOD
EOD;


    $pageCustomLayoutUpdateXML = <<<EOD
EOD;

    $pageMetaKeywords = "";
    $pageMetaDescription = "";

    $page = Mage::getModel('cms/page')->getCollection()
        ->addStoreFilter(array(5), $withAdmin = true)
        ->addFieldToFilter('identifier', $pageIdentifier)
        ->getFirstItem();
    if ($page->getId() == 0) {
        $page = Mage::getModel('cms/page');
    }
    else
    {
        // if exists then repair
        $page = Mage::getModel('cms/page')->load($page->getId());
    }

    $page->setTitle($pageTitle);
    $page->setIdentifier($pageIdentifier);
    $page->setStores($pageStores);
    $page->setIsActive($pageIsActive);
    $page->setUnderVersionControl($pageUnderVersionControl);
    $page->setContentHeading($pageContentHeading);
    $page->setContent($pageContent);
    $page->setRootTemplate($pageRootTemplate);
    $page->setLayoutUpdateXml($pageLayoutUpdateXml);
    $page->setCustomLayoutUpdateXml($pageCustomLayoutUpdateXML);
    $page->setMetaKeywords($pageMetaKeywords);
    $page->setMetaDescription($pageMetaDescription);
    $page->save();
    //==========================================================================
    //==========================================================================

    //==========================================================================
    // All Brands Venbi TH Page
    //==========================================================================
    $pageTitle = "ทุกแบรนด์";
    $pageIdentifier = "all-brands";
    $pageStores = array('4','6');
    $pageIsActive = 1;
    $pageUnderVersionControl = 0;
    $pageContentHeading = "";
    $pageContent = <<<EOD
<p>{{block type="core/template" name="all-brands" template="brands/all.phtml"}}</p>
<p>{{block type="cms/block" block_id="banners_home"}}</p>
EOD;

    $pageRootTemplate = 'one_column';
    $pageLayoutUpdateXml = <<<EOD
EOD;


    $pageCustomLayoutUpdateXML = <<<EOD
EOD;

    $pageMetaKeywords = "";
    $pageMetaDescription = "";

    $page = Mage::getModel('cms/page')->getCollection()
        ->addStoreFilter(array('4','6'), $withAdmin = true)
        ->addFieldToFilter('identifier', $pageIdentifier)
        ->getFirstItem();
    if ($page->getId() == 0) {
        $page = Mage::getModel('cms/page');
    }
    else
    {
        // if exists then repair
        $page = Mage::getModel('cms/page')->load($page->getId());
    }

    $page->setTitle($pageTitle);
    $page->setIdentifier($pageIdentifier);
    $page->setStores($pageStores);
    $page->setIsActive($pageIsActive);
    $page->setUnderVersionControl($pageUnderVersionControl);
    $page->setContentHeading($pageContentHeading);
    $page->setContent($pageContent);
    $page->setRootTemplate($pageRootTemplate);
    $page->setLayoutUpdateXml($pageLayoutUpdateXml);
    $page->setCustomLayoutUpdateXml($pageCustomLayoutUpdateXML);
    $page->setMetaKeywords($pageMetaKeywords);
    $page->setMetaDescription($pageMetaDescription);
    $page->save();
    //==========================================================================
    //==========================================================================

    //==========================================================================
    // All Brands Lafema TH Page
    //==========================================================================
    $pageTitle = "ทุกแบรนด์";
    $pageIdentifier = "all-brands";
    $pageStores = array('14','15');
    $pageIsActive = 1;
    $pageUnderVersionControl = 0;
    $pageContentHeading = "";
    $pageContent = <<<EOD
<p>{{block type="core/template" name="all-brands" template="brands/all.phtml"}}</p>
<p>{{block type="cms/block" block_id="banners_home"}}</p>
EOD;

    $pageRootTemplate = 'one_column';
    $pageLayoutUpdateXml = <<<EOD
EOD;


    $pageCustomLayoutUpdateXML = <<<EOD
EOD;

    $pageMetaKeywords = "";
    $pageMetaDescription = "";

    $page = Mage::getModel('cms/page')->getCollection()
        ->addStoreFilter(array('14','15'), $withAdmin = true)
        ->addFieldToFilter('identifier', $pageIdentifier)
        ->getFirstItem();
    if ($page->getId() == 0) {
        $page = Mage::getModel('cms/page');
    }
    else
    {
        // if exists then repair
        $page = Mage::getModel('cms/page')->load($page->getId());
    }

    $page->setTitle($pageTitle);
    $page->setIdentifier($pageIdentifier);
    $page->setStores($pageStores);
    $page->setIsActive($pageIsActive);
    $page->setUnderVersionControl($pageUnderVersionControl);
    $page->setContentHeading($pageContentHeading);
    $page->setContent($pageContent);
    $page->setRootTemplate($pageRootTemplate);
    $page->setLayoutUpdateXml($pageLayoutUpdateXml);
    $page->setCustomLayoutUpdateXml($pageCustomLayoutUpdateXML);
    $page->setMetaKeywords($pageMetaKeywords);
    $page->setMetaDescription($pageMetaDescription);
    $page->save();
    //==========================================================================
    //==========================================================================

    //==========================================================================
    // All Brands Lafema EN Page
    //==========================================================================
    $pageTitle = "All Brands";
    $pageIdentifier = "all-brands";
    $pageStores = array(13);
    $pageIsActive = 1;
    $pageUnderVersionControl = 0;
    $pageContentHeading = "";
    $pageContent = <<<EOD
<p>{{block type="core/template" name="all-brands" template="brands/all.phtml"}}</p>
<p>{{block type="cms/block" block_id="banners_home"}}</p>
EOD;

    $pageRootTemplate = 'one_column';
    $pageLayoutUpdateXml = <<<EOD
EOD;


    $pageCustomLayoutUpdateXML = <<<EOD
EOD;

    $pageMetaKeywords = "";
    $pageMetaDescription = "";

    $page = Mage::getModel('cms/page')->getCollection()
        ->addStoreFilter(array(13), $withAdmin = true)
        ->addFieldToFilter('identifier', $pageIdentifier)
        ->getFirstItem();
    if ($page->getId() == 0) {
        $page = Mage::getModel('cms/page');
    }
    else
    {
        // if exists then repair
        $page = Mage::getModel('cms/page')->load($page->getId());
    }

    $page->setTitle($pageTitle);
    $page->setIdentifier($pageIdentifier);
    $page->setStores($pageStores);
    $page->setIsActive($pageIsActive);
    $page->setUnderVersionControl($pageUnderVersionControl);
    $page->setContentHeading($pageContentHeading);
    $page->setContent($pageContent);
    $page->setRootTemplate($pageRootTemplate);
    $page->setLayoutUpdateXml($pageLayoutUpdateXml);
    $page->setCustomLayoutUpdateXml($pageCustomLayoutUpdateXML);
    $page->setMetaKeywords($pageMetaKeywords);
    $page->setMetaDescription($pageMetaDescription);
    $page->save();
    //==========================================================================
    //==========================================================================

    //==========================================================================
    // All Brands Page
    //==========================================================================
    $pageTitle = "All Brands";
    $pageIdentifier = "all-brands";
    $pageStores = array("$storeIdMoxyEn", "$storeIdMoxyTh");
    $pageIsActive = 1;
    $pageUnderVersionControl = 0;
    $pageContentHeading = "";
    $pageContent = <<<EOD
<p>{{block type="core/template" name="all-brands" template="brands/all.phtml"}}</p>
<p>{{block type="cms/block" block_id="banners_home"}}</p>
EOD;

    $pageRootTemplate = 'one_column';
    $pageLayoutUpdateXml = <<<EOD
EOD;


    $pageCustomLayoutUpdateXML = <<<EOD
EOD;

    $pageMetaKeywords = "";
    $pageMetaDescription = "";

    $page = Mage::getModel('cms/page')->getCollection()
        ->addStoreFilter(array("$storeIdMoxyEn", "$storeIdMoxyTh"), $withAdmin = true)
        ->addFieldToFilter('identifier', $pageIdentifier)
        ->getFirstItem();
    if ($page->getId() == 0) {
        $page = Mage::getModel('cms/page');
    }
    else
    {
        // if exists then repair
        $page = Mage::getModel('cms/page')->load($page->getId());
    }

    $page->setTitle($pageTitle);
    $page->setIdentifier($pageIdentifier);
    $page->setStores($pageStores);
    $page->setIsActive($pageIsActive);
    $page->setUnderVersionControl($pageUnderVersionControl);
    $page->setContentHeading($pageContentHeading);
    $page->setContent($pageContent);
    $page->setRootTemplate($pageRootTemplate);
    $page->setLayoutUpdateXml($pageLayoutUpdateXml);
    $page->setCustomLayoutUpdateXml($pageCustomLayoutUpdateXML);
    $page->setMetaKeywords($pageMetaKeywords);
    $page->setMetaDescription($pageMetaDescription);
    $page->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================

    $installer->endSetup();
} catch (Excpetion $e) {
    Mage::logException($e);
    Mage::log("ERROR IN SETUP " . $e->getMessage());
}
