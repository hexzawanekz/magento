<?php

class SM_ShippingBadge_Block_Onepage_Shipping_Method_Available extends Mage_Checkout_Block_Onepage_Shipping_Method_Available
{
     public function checkDropshipProduct()
    {
        /** @var Mage_Sales_Model_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        /** @var Mage_Sales_Model_Quote_Item $items */
        $items = $quote->getAllVisibleItems();
        // Product ids array: $pIds
//        $pIds = array();
        $check = array();
        foreach ($items as $item) {
            /** @var Mage_Sales_Model_Quote_Item $item */
            $pIds = $item->getProductId();
            $id = $pIds;
            $product = Mage::getModel('catalog/product')->load($id);;
            $check[] = $product->getShippingDuration();
        }

        return $check;
    }
}