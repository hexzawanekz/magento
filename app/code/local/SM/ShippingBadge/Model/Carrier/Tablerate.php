<?php

class SM_ShippingBadge_Model_Carrier_Tablerate extends Mage_Shipping_Model_Carrier_Tablerate
{
    public function collectRates(Mage_Shipping_Model_Rate_Request $request)
    {
        if (!$this->getConfigFlag('active')) {
            return false;
        }

        // exclude Virtual products price from Package value if pre-configured
        if (!$this->getConfigFlag('include_virtual_price') && $request->getAllItems()) {
            foreach ($request->getAllItems() as $item) {
                if ($item->getParentItem()) {
                    continue;
                }
                if ($item->getHasChildren() && $item->isShipSeparately()) {
                    foreach ($item->getChildren() as $child) {
                        if ($child->getProduct()->isVirtual()) {
                            $request->setPackageValue($request->getPackageValue() - $child->getBaseRowTotal());
                        }
                    }
                } elseif ($item->getProduct()->isVirtual()) {
                    $request->setPackageValue($request->getPackageValue() - $item->getBaseRowTotal());
                }
            }
        }

        // Free shipping by qty
        $freeQty = 0;
        if ($request->getAllItems()) {
            foreach ($request->getAllItems() as $item) {
                if ($item->getProduct()->isVirtual() || $item->getParentItem()) {
                    continue;
                }

                if ($item->getHasChildren() && $item->isShipSeparately()) {
                    foreach ($item->getChildren() as $child) {
                        if ($child->getFreeShipping() && !$child->getProduct()->isVirtual()) {
                            $freeQty += $item->getQty() * ($child->getQty() - (is_numeric($child->getFreeShipping()) ? $child->getFreeShipping() : 0));
                        }
                    }
                } elseif ($item->getFreeShipping()) {
                    $freeQty += ($item->getQty() - (is_numeric($item->getFreeShipping()) ? $item->getFreeShipping() : 0));
                }
            }
        }

        // Get Quote
        $quote = null;
        foreach ($request->getAllItems() as $item){
            $quote = $item->getQuote();
            break;
        }

        // Set Order Type
        if ($quote) {
            if ($quote->getIsWholesale() == 1) {
                $request->setOrderType(self::ORDER_TYPE_WHOLESALE);
            } else {
                $request->setOrderType(self::ORDER_TYPE_RETAIL);
            }
        }

        if (!$request->getConditionName()) {
            $request->setConditionName($this->getConfigData('condition_name') ? $this->getConfigData('condition_name') : $this->_default_condition_name);
        }

        // Package weight and qty free shipping
        $oldWeight = $request->getPackageWeight();
        $oldQty = $request->getPackageQty();

        $request->setPackageWeight($request->getFreeMethodWeight());
        $request->setPackageQty($oldQty - $freeQty);

        $result = Mage::getModel('shipping/rate_result');
        $rate = $this->getRate($request);

        $request->setPackageWeight($oldWeight);
        $request->setPackageQty($oldQty);

        if (!empty($rate) && $rate['price'] >= 0) {
            $method = Mage::getModel('shipping/rate_result_method');

            $method->setCarrier('tablerate');
            $method->setCarrierTitle($this->getConfigData('title'));

            $method->setMethod('bestway');
            $method->setMethodTitle($this->getConfigData('name'));

            if (
                ($request->getFreeShipping() === true || ($request->getPackageQty() == $freeQty) || $this->checkDropshipProduct()) &&
                $quote->getIsWholesale() == 0
            ) {
                $shippingPrice = 0;
            } else {
                $shippingPrice = $this->getFinalPriceWithHandlingFee($rate['price']);
            }

            $method->setPrice($shippingPrice);
            $method->setCost($rate['cost']);

            $result->append($method);
        }

        return $result;
    }

    public function checkDropshipProduct()
    {
        /** @var Mage_Sales_Model_Quote $quote */
        $quote = Mage::getSingleton('checkout/cart')->getQuote();
        /** @var Mage_Sales_Model_Quote_Item $items */
        $items = $quote->getAllVisibleItems();
        // Product ids array: $pIds
        $pIds = array();
        foreach ($items as $item) {
            /** @var Mage_Sales_Model_Quote_Item $item */
            $pIds[] = $item->getProductId();
        }

        $totalItems = $quote->getItemsCount();
        if ($totalItems > 0) {
            $count = 0;
            $collection = Mage::getResourceModel('catalog/product_collection')
                ->addFieldToFilter('entity_id', $pIds)
                ->addAttributeToSelect('shipping_duration');
            foreach ($collection as $product) {
                /** @var Mage_Catalog_Model_Product $product */
                if ($product->getAttributeText('shipping_duration') == 'Dropship Product') {
                    $count++;
                }
            }
            if ($count == $totalItems) {
                return true;
            }
        }
        return false;
    }
}