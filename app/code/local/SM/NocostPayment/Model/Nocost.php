<?php
class SM_NocostPayment_Model_Nocost extends Mage_Payment_Model_Method_Abstract{
    protected $_code = "nocost";
    protected $_isInitializeNeeded      = true;
    protected $_canUseInternal          = true;
    protected $_canUseCheckout          = false;
    protected $_canUseForMultishipping  = false;
}