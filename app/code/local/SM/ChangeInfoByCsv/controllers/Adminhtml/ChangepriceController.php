<?php

class SM_ChangeInfoByCsv_Adminhtml_ChangepriceController extends Mage_Adminhtml_Controller_Action{

    public function indexAction() {
        $this->loadLayout();
        $this->_addBreadcrumb(
            Mage::helper('adminhtml')->__('Catalog'),
            Mage::helper('adminhtml')->__('Change Price by Csv')
        );
        $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
        $this->_addContent($this->getLayout()->createBlock('sm_changeinfobycsv/adminhtml_upload'));
        $this->_addLeft($this->getLayout()->createBlock('sm_changeinfobycsv/adminhtml_upload_edit_tabs'));
        $this->renderLayout();
    }

    public function uploadAction(){
        ini_set('max_execution_time', 0);
        ini_set('display_error', 1);
        ini_set('memory_limit', -1);

        /** @var SM_ChangeInfoByCsv_Model_ChangeInfo $model */
        $model = Mage::getModel('sm_changeinfobycsv/changeInfo');

        if($this->getRequest()->isPost()) {
            if(isset($_FILES['filename']) && $_FILES['filename']['name'] != ''){

                try {
                    $uploader = new Varien_File_Uploader('filename');
                    $uploader->setAllowedExtensions(array('csv'));
                    $uploader->setAllowRenameFiles(true);
                    $uploader->setFilesDispersion(false);
                    $path = Mage::getBaseDir('media') . DS. 'upload_tmp_files' .DS;
                    if(!is_dir($path)){
                        mkdir($path, 0777, true);
                    }
                    $uploader->save($path);
                    $newFilename = $uploader->getUploadedFileName();

                    $fullPath = $path.$newFilename;
                } catch (Exception $e) {
                    throw $e;
                }

                $model->setFilePath($fullPath);
                $model->changeInfoData();

                //Removing temp file
                unlink($fullPath);
            }
        }

        $this->loadLayout();
        $this->renderLayout();
    }

    protected function _isAllowed()
    {
        return true;
    }
}