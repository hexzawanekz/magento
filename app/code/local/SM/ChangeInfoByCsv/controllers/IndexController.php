<?php

class SM_ChangeInfoByCsv_IndexController extends Mage_Core_Controller_Front_Action {
    public function indexAction(){
        /** @var SM_ChangeInfoByCsv_Model_ChangeInfo $model */
        $model = Mage::getModel('sm_changeinfobycsv/changeInfo');

        if($this->getRequest()->isPost()) {
            if(isset($_FILES['csvFile']) && $_FILES['csvFile']['name'] != ''){

                try {
                    $uploader = new Varien_File_Uploader('csvFile');
                    $uploader->setAllowedExtensions(array('csv'));
                    $uploader->setAllowRenameFiles(true);
                    $uploader->setFilesDispersion(false);
                    $path = Mage::getBaseDir('media') . DS. 'upload_tmp_files' .DS;
                    if(!is_dir($path)){
                        mkdir($path, 0777, true);
                    }
                    $uploader->save($path);
                    $newFilename = $uploader->getUploadedFileName();

                    $fullPath = $path.$newFilename;
                } catch (Exception $e) {
                    throw $e;
                }

                $model->setFilePath($fullPath);
                $model->changeInfoData();
            }
        }

        $this->loadLayout();
        $this->renderLayout();
    }
}