<?php

class SM_ChangeInfoByCsv_Model_CsvParser extends Varien_File_Csv{

    public function  SM_ChangeInfoByCsv_Model_CsvParser($filePath, $lineLength = 0, $determiter = ',', $enclosure = '"'){
        $this->_filePath = $filePath;
        $this->_lineLength = $lineLength;
        $this->_delimiter = $determiter;
        $this->_enclosure = $enclosure;
    }

    public function getArrayData(){
        $data = array();

        $rawData = $this->getData($this->_filePath);

        $headerCount = count($rawData[0]);
        $rawDataCount = count($rawData);

        for($i = 1; $i < $rawDataCount; $i++){

            if(count($rawData[$i]) !== $headerCount){
                throw new Mage_Exception('Invalid format.');
            }
            $tmpArr = array();
            for($headerOffset = 0; $headerOffset < $headerCount; $headerOffset++){
                $tmpArr[$rawData[0][$headerOffset]] = $rawData[$i][$headerOffset];
            }

            $data[] = $tmpArr;
        }

        return $data;
    }

    public function getCsvHeader(){
        return $this->getData($this->_filePath)[0];
    }
}