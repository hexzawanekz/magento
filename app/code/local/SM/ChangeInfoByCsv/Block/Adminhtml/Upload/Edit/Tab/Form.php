<?php

class SM_ChangeInfoByCsv_Block_Adminhtml_Upload_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset('slider_form', array('legend'=>Mage::helper('sm_changeinfobycsv')->__('Upload Csv File')));


        $fieldset->addField('filename', 'file', array(
            'label'     => Mage::helper('sm_changeinfobycsv')->__('File'),
            'required'  => false,
            'name'      => 'filename',
        ));

        return parent::_prepareForm();
    }
}