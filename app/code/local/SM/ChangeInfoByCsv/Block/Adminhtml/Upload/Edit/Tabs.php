<?php
class SM_ChangeInfoByCsv_Block_Adminhtml_Upload_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs{
    public function __construct()
    {
        parent::__construct();
        $this->setId('slider_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('sm_changeinfobycsv')->__('Change Price by Csv'));
    }

    protected function _beforeToHtml()
    {
        $this->addTab('form_section', array(
            'label'     => Mage::helper('sm_changeinfobycsv')->__('Upload Csv File'),
            'title'     => Mage::helper('sm_changeinfobycsv')->__('Upload Csv File'),
            'content'   => $this->getLayout()->createBlock('sm_changeinfobycsv/adminhtml_upload_edit_tab_form')->toHtml(),
        ));

        return parent::_beforeToHtml();
    }
}