<?php

$installer = $this;
$installer->startSetup();

$setup = new Mage_Catalog_Model_Resource_Eav_Mysql4_Setup('core_setup');
$setup->addAttribute('catalog_product', 'promotion_flag', array(
    'group' => 'Whatsnew Details',
    'label' => 'Promotion Flag',
    'note' => '',
    'type' => 'int',    //backend_type
    'input' => 'select',    //frontend_input
    'frontend_class' => '',
    'source' => 'eav/entity_attribute_source_boolean',
    'backend' => '',
    'frontend' => '',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'required' => 0,
    'is_configurable' => 0,
    'visible_on_front' => 1,
    'used_in_product_listing' => 1,
    'sort_order' => 69
));

$setup->addAttribute('catalog_product', 'promotion_message', array(
    'group' => 'Whatsnew Details',
    'label' => 'Promotion Message',
    'note' => 'Should be under 20 characters',
    'type' => 'varchar',    //backend_type
    'input' => 'text',    //frontend_input
    'frontend_class' => '',
    'backend' => '',
    'frontend' => '',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'required' => 0,
    'is_configurable' => 0,
    'visible_on_front' => 1,
    'used_in_product_listing' => 1,
    'sort_order' => 70
));

$installer->endSetup();