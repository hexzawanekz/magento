<?php
/** @var Mage_Core_Model_Resource_Setup $installer */
$installer = $this;

$installer->startSetup();

//==========================================================================
// Bank Image Footer
//==========================================================================

/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter(array(0), $withAdmin = false)
    ->addFieldToFilter('identifier', 'bank_image_footer')
    ->getFirstItem();

if ($block->getId()) $block->delete();// if exists then delete
$content = <<<EOD
<div class="bank-list-images">
    <p class="payment_partners_footer">Payment Partners</p>
    <div id="bank_list_images"></div>
</div>
<div class="acommerce-logo-footer">
    <p class="shipping_partners_footer">Shipping Partners</p>
    <div id="acommerce_logo_images"></div>
</div>
<div class="verfied-logo-footer">
    <p class="verified-partner-footer">Verified Services</p>
    <div id="verfied-ssl_images"></div>
    <div id="verfied-dbd_images"></div>
</div>
<div class="pay-over-footer">
    <p class="pay-over">Pay Over The Counter</p>
    <div id="counter_images"></div>
</div>
EOD;

$block = Mage::getModel('cms/block');
$block->setTitle('Bank Image Footer');
$block->setIdentifier('bank_image_footer');
$block->setStores(array(0));
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================

$installer->endSetup();