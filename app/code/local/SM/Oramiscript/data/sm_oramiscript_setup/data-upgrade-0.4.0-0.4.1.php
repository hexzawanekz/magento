<?php
// init Magento
Mage::app();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

$moxyen = Mage::getModel('core/store')->load('moxyen', 'code')->getId();
$moxyth = Mage::getModel('core/store')->load('moxyth', 'code')->getId();

/** @var Mage_Cms_Model_Resource_Block_Collection $allBlocks */
$allBlocks = Mage::getModel('cms/block')->getCollection()
    ->addFieldToFilter('identifier', 'home-middle-banner');
if($allBlocks->count() > 0) {
    /** @var Mage_Cms_Model_Block $item */
    foreach ($allBlocks as $item) {
        if ($item->getId()) $item->delete(); // delete old blocks
    }
}

//==========================================================================
// Home middle Banner (English)
//==========================================================================
$blockTitle = "Homepage Middle Banner";
$blockIdentifier = "home-middle-banner";
$blockStores = array($moxyen);
$blockIsActive = 1;
$blockContent = <<<EOD
<div id="home-middle-banner">
<div id="home-middle-banner-container">
<h3 class="line-thought">Inspiration Of The Day | <span style="color: #ff6c6c;">Orami Magazine</span></h3>
<p id="middle-banner-text">A collection of inspirational articles around fashion, mother and child to love and lifestyle</p>
<div class="row">
<div class="col-sm-6"><a href="http://magazine.orami.co.th/G1-health-sugar-detox-loss-weight-in-21-days/?utm_source=HomePage&amp;utm_medium=MGZbanner&amp;utm_campaign=Article"><img title="" src="{{media url="wysiwyg/welcomePage/HomepageBanner/HP-Magazine/HP-MAG-1-160517.jpg"}}" alt="" /></a></div>
<div class="col-sm-6"><a href="http://magazine.orami.co.th/G1-mom-7-foods-make-kids-happy-and-improve-behavior/?utm_source=HomePage&amp;utm_medium=MGZbanner&amp;utm_campaign=Article"><img title="" src="{{media url="wysiwyg/welcomePage/HomepageBanner/HP-Magazine/HP-MAG-2-160517.jpg"}}" alt="" /></a></div>
</div>
</div>
</div>
EOD;
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================


//==========================================================================
// Home middle Banner (Thai)
//==========================================================================
$blockTitle = "Homepage Middle Banner";
$blockIdentifier = "home-middle-banner";
$blockStores = array($moxyth);
$blockIsActive = 1;
$blockContent = <<<EOD
<div id="home-middle-banner">
<div id="home-middle-banner-container">
<h3 class="line-thought">Inspiration Of The Day | <span style="color: #ff6c6c;">Orami Magazine</span></h3>
<p id="middle-banner-text">A collection of inspirational articles around fashion, mother and child to love and lifestyle</p>
<div class="row">
<div class="col-sm-6"><a href="http://magazine.orami.co.th/G1-health-sugar-detox-loss-weight-in-21-days/?utm_source=HomePage&amp;utm_medium=MGZbanner&amp;utm_campaign=Article"><img title="" src="{{media url="wysiwyg/welcomePage/HomepageBanner/HP-Magazine/HP-MAG-1-160517.jpg"}}" alt="" /></a></div>
<div class="col-sm-6"><a href="http://magazine.orami.co.th/G1-mom-7-foods-make-kids-happy-and-improve-behavior/?utm_source=HomePage&amp;utm_medium=MGZbanner&amp;utm_campaign=Article"><img title="" src="{{media url="wysiwyg/welcomePage/HomepageBanner/HP-Magazine/HP-MAG-2-160517.jpg"}}" alt="" /></a></div>
</div>
</div>
</div>
EOD;
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================


/** @var Mage_Cms_Model_Resource_Block_Collection $allBlocks */
$allBlocks = Mage::getModel('cms/block')->getCollection()
    ->addFieldToFilter('identifier', 'home-slider-banner');
if($allBlocks->count() > 0) {
    /** @var Mage_Cms_Model_Block $item */
    foreach ($allBlocks as $item) {
        if ($item->getId()) $item->delete(); // delete old blocks
    }
}
//==========================================================================
// Home Slider Banner (English)
//==========================================================================
$blockTitle = "Homepage Slider Banner";
$blockIdentifier = "home-slider-banner";
$blockStores = array($moxyen);
$blockIsActive = 1;
$blockContent = <<<EOD
<div id="home-slider-banner">
<div id="home-slider-banner-mobile">
<ul id="home-slides-content-mobile" style="position: relative; width: 100%; height: 100%;">
<li style="position: absolute; top: 0; left: 0; display: none; z-index: 1; opacity: 1;"><a style="width: 100%;" href="https://www.orami.co.th/th/pets/brands/ciao.html"><img src="{{media url="wysiwyg/welcomePage/HomepageBanner/HP-Slider/Slider-CALO-25OFF-150517.jpg"}}" alt="" /></a></li>
<li style="position: absolute; top: 0; left: 0; display: none; z-index: 1; opacity: 1;"><a style="width: 100%;" href="https://www.orami.co.th/th/deal/all-deals/health-beauty-buy-1-get-1.html"><img src="{{media url="wysiwyg/welcomePage/HomepageBanner/HP-Slider/Slider-HEALTH-&-BEAUTY-BOGO-VER6+25OFF-150517.jpg"}}" alt="" /></a></li>
<li style="position: absolute; top: 0; left: 0; display: none; z-index: 1; opacity: 1;"><a style="width: 100%;" href="https://www.orami.co.th/th/baby/deals/all-diaper-deals/merries-promotion-15-30-may.html"><img src="{{media url="wysiwyg/welcomePage/HomepageBanner/HP-Slider/Slider-Merries-Tape-promotion-15052017-11OFF.jpg"}}" alt="" /></a></li>
<li style="position: absolute; top: 0; left: 0; display: none; z-index: 1; opacity: 1;"><a style="width: 100%;" href="https://www.orami.co.th/th/deal/beautiful-deals/rainy-ready.html"><img src="{{media url="wysiwyg/welcomePage/HomepageBanner/HP-Slider/Slider-Ready-For-Rain-05052017.jpg"}}" alt="" /></a></li>
<li style="position: absolute; top: 0; left: 0; display: none; z-index: 1; opacity: 1;"><a style="width: 100%;" href="https://www.orami.co.th/th/health/deals/healthy-food-supplement.html"><img src="{{media url="wysiwyg/welcomePage/HomepageBanner/HP-Slider/Slider-VITAMIN-AND-FOOD-SUPPLEMENTS-150517.jpg"}}" alt="" /></a></li>
<li style="position: absolute; top: 0; left: 0; display: none; z-index: 1; opacity: 1;"><a style="width: 100%;" href="https://www.orami.co.th/th/beauty/deals/beauty-special-discount-30.html
"><img src="{{media url="wysiwyg/welcomePage/HomepageBanner/HP-Slider/Slider-Welcome-Beauty-Clearance--150517.jpg"}}" alt="" /></a></li>
</ul>
</div>
<div id="home-slider-banner-desktop">
<ul id="home-slides-content-desktop">
<li><a style="width: 100%;" href="https://www.orami.co.th/th/pets/brands/ciao.html"><img src="{{media url="wysiwyg/welcomePage/HomepageBanner/HP-Slider/Slider-CALO-25OFF-150517.jpg"}}" alt="" /></a></li>
<li><a style="width: 100%;" href="https://www.orami.co.th/th/deal/all-deals/health-beauty-buy-1-get-1.html"><img src="{{media url="wysiwyg/welcomePage/HomepageBanner/HP-Slider/Slider-HEALTH-&-BEAUTY-BOGO-VER6+25OFF-150517.jpg"}}" alt="" /></a></li>
<li><a style="width: 100%;" href="https://www.orami.co.th/th/baby/deals/all-diaper-deals/merries-promotion-15-30-may.html"><img src="{{media url="wysiwyg/welcomePage/HomepageBanner/HP-Slider/Slider-Merries-Tape-promotion-15052017-11OFF.jpg"}}" alt="" /></a></li>
<li><a style="width: 100%;" href="https://www.orami.co.th/th/deal/beautiful-deals/rainy-ready.html"><img src="{{media url="wysiwyg/welcomePage/HomepageBanner/HP-Slider/Slider-Ready-For-Rain-05052017.jpg"}}" alt="" /></a></li>
<li><a style="width: 100%;" href="https://www.orami.co.th/th/health/deals/healthy-food-supplement.html"><img src="{{media url="wysiwyg/welcomePage/HomepageBanner/HP-Slider/Slider-VITAMIN-AND-FOOD-SUPPLEMENTS-150517.jpg"}}" alt="" /></a></li>
<li><a style="width: 100%;" href="https://www.orami.co.th/th/beauty/deals/beauty-special-discount-30.html"><img src="{{media url="wysiwyg/welcomePage/HomepageBanner/HP-Slider/Slider-Welcome-Beauty-Clearance--150517.jpg"}}" alt="" /></a></li>
</ul>
</div>
</div>
EOD;
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================


//==========================================================================
// Home Slider Banner (Thai)
//==========================================================================
$blockTitle = "Homepage Slider Banner";
$blockIdentifier = "home-slider-banner";
$blockStores = array($moxyth);
$blockIsActive = 1;
$blockContent = <<<EOD
<div id="home-slider-banner">
<div id="home-slider-banner-mobile">
<ul id="home-slides-content-mobile" style="position: relative; width: 100%; height: 100%;">
<li style="position: absolute; top: 0; left: 0; display: none; z-index: 1; opacity: 1;"><a style="width: 100%;" href="https://www.orami.co.th/th/pets/brands/ciao.html"><img src="{{media url="wysiwyg/welcomePage/HomepageBanner/HP-Slider/Slider-CALO-25OFF-150517.jpg"}}" alt="" /></a></li>
<li style="position: absolute; top: 0; left: 0; display: none; z-index: 1; opacity: 1;"><a style="width: 100%;" href="https://www.orami.co.th/th/deal/all-deals/health-beauty-buy-1-get-1.html"><img src="{{media url="wysiwyg/welcomePage/HomepageBanner/HP-Slider/Slider-HEALTH-&-BEAUTY-BOGO-VER6+25OFF-150517.jpg"}}" alt="" /></a></li>
<li style="position: absolute; top: 0; left: 0; display: none; z-index: 1; opacity: 1;"><a style="width: 100%;" href="https://www.orami.co.th/th/baby/deals/all-diaper-deals/merries-promotion-15-30-may.html"><img src="{{media url="wysiwyg/welcomePage/HomepageBanner/HP-Slider/Slider-Merries-Tape-promotion-15052017-11OFF.jpg"}}" alt="" /></a></li>
<li style="position: absolute; top: 0; left: 0; display: none; z-index: 1; opacity: 1;"><a style="width: 100%;" href="https://www.orami.co.th/th/deal/beautiful-deals/rainy-ready.html"><img src="{{media url="wysiwyg/welcomePage/HomepageBanner/HP-Slider/Slider-Ready-For-Rain-05052017.jpg"}}" alt="" /></a></li>
<li style="position: absolute; top: 0; left: 0; display: none; z-index: 1; opacity: 1;"><a style="width: 100%;" href="https://www.orami.co.th/th/health/deals/healthy-food-supplement.html"><img src="{{media url="wysiwyg/welcomePage/HomepageBanner/HP-Slider/Slider-VITAMIN-AND-FOOD-SUPPLEMENTS-150517.jpg"}}" alt="" /></a></li>
<li style="position: absolute; top: 0; left: 0; display: none; z-index: 1; opacity: 1;"><a style="width: 100%;" href="https://www.orami.co.th/th/beauty/deals/beauty-special-discount-30.html
"><img src="{{media url="wysiwyg/welcomePage/HomepageBanner/HP-Slider/Slider-Welcome-Beauty-Clearance--150517.jpg"}}" alt="" /></a></li>
</ul>
</div>
<div id="home-slider-banner-desktop">
<ul id="home-slides-content-desktop">
<li><a style="width: 100%;" href="https://www.orami.co.th/th/pets/brands/ciao.html"><img src="{{media url="wysiwyg/welcomePage/HomepageBanner/HP-Slider/Slider-CALO-25OFF-150517.jpg"}}" alt="" /></a></li>
<li><a style="width: 100%;" href="https://www.orami.co.th/th/deal/all-deals/health-beauty-buy-1-get-1.html"><img src="{{media url="wysiwyg/welcomePage/HomepageBanner/HP-Slider/Slider-HEALTH-&-BEAUTY-BOGO-VER6+25OFF-150517.jpg"}}" alt="" /></a></li>
<li><a style="width: 100%;" href="https://www.orami.co.th/th/baby/deals/all-diaper-deals/merries-promotion-15-30-may.html"><img src="{{media url="wysiwyg/welcomePage/HomepageBanner/HP-Slider/Slider-Merries-Tape-promotion-15052017-11OFF.jpg"}}" alt="" /></a></li>
<li><a style="width: 100%;" href="https://www.orami.co.th/th/deal/beautiful-deals/rainy-ready.html"><img src="{{media url="wysiwyg/welcomePage/HomepageBanner/HP-Slider/Slider-Ready-For-Rain-05052017.jpg"}}" alt="" /></a></li>
<li><a style="width: 100%;" href="https://www.orami.co.th/th/health/deals/healthy-food-supplement.html"><img src="{{media url="wysiwyg/welcomePage/HomepageBanner/HP-Slider/Slider-VITAMIN-AND-FOOD-SUPPLEMENTS-150517.jpg"}}" alt="" /></a></li>
<li><a style="width: 100%;" href="https://www.orami.co.th/th/beauty/deals/beauty-special-discount-30.html"><img src="{{media url="wysiwyg/welcomePage/HomepageBanner/HP-Slider/Slider-Welcome-Beauty-Clearance--150517.jpg"}}" alt="" /></a></li>
</ul>
</div>
</div>
EOD;
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================


$allBlocks = Mage::getModel('cms/block')->getCollection()
    ->addFieldToFilter('identifier', 'welcome-shortcut');
if ($allBlocks->count() > 0) {
    /** @var Mage_Cms_Model_Block $item */
    foreach ($allBlocks as $item) {
        if ($item->getId()) $item->delete(); // delete old blocks
    }
}

//==========================================================================
// Welcome splash shortcut link (English)
//==========================================================================
$blockTitle = "Welcome splash shortcut link";
$blockIdentifier = "welcome-shortcut";
$blockStores = array($moxyen);
$blockIsActive = 1;
$blockContent = <<<EOD
<div class="grid-wall">
<div class="row">
<div class="col-sm-6">
<div class="row">
<div class="col-sm-12">
<div class="img-banner"><a href="https://www.orami.co.th/th/electronics/gadgets/lifestyle-gadget.html"><img title="Cheero" src="{{media url="wysiwyg/welcomePage/HomepageBanner/HP-Hero-Product/HP-Hero-Product-Electronics-Scooter-150517.jpg"}}" alt="Cheero" /></a></div>
</div>
</div>
<div class="row">
<div class="col-xs-6">
<div class="img-banner"><a href="https://www.orami.co.th/th/beauty/deals/clean-up-your-face.html"><img title="Orijen" src="{{media url="wysiwyg/welcomePage/HomepageBanner/HP-Square-Product/HP-Square-Product-Facial-Cleanser-150517.jpg"}}" alt="Orijen" /></a></div>
</div>
<div class="col-xs-6">
<div class="img-banner"><a href="https://www.orami.co.th/th/fashion/deals/new-mom-essential.html"><img title="Hospro" src="{{media url="wysiwyg/welcomePage/HomepageBanner/HP-Square-Product/HP-Square-Product-NEW-MOM-ESSENTIALS--150517.jpg"}}" alt="Hospro" /></a></div>
</div>
</div>
</div>
<div class="col-sm-6">
<div class="row">
<div class="col-sm-12">
<div class="img-banner"><a href="https://www.orami.co.th/th/home-appliances/deals/50-best-home-appliances.html"><img title="All about cleaning" src="{{media url="wysiwyg/welcomePage/HomepageBanner/HP-Hero/HP-Hero-50-Best-Home-Appliances.jpg"}}" alt="All about cleaning" /></a></div>
</div>
</div>
</div>
</div>
<div class="row">
<div class="col-sm-6">
<div class="row">
<div class="col-sm-12">
<div class="img-banner"><a href="https://www.orami.co.th/catalog/category/view/id/4943.html"><img title="All about cleaning" src="{{media url="wysiwyg/welcomePage/HomepageBanner/HP-Hero/HP-Hero-huggies50-9-23MAY.jpg}}" alt="All about cleaning" /></a></div>
</div>
</div>
</div>
<div class="col-sm-6">
<div class="row">
<div class="col-sm-12">
<div class="img-banner"><a href="https://www.orami.co.th/th/deal/for-your-home-deals/ergotrend-officeintrend-special-price.html"><img title="Cheero" src="{{media url="wysiwyg/welcomePage/HomepageBanner/HP-Hero-Product/HP-Hero-Product-Ergotrend-and-Office-intrend-11052017.jpg}}" alt="Cheero" /></a></div>
</div>
</div>
<div class="row">
<div class="col-xs-6">
<div class="img-banner"><a href="https://www.orami.co.th/th/baby/deals/pigeon-promotion.html"><img title="Orijen" src="{{media url="wysiwyg/welcomePage/HomepageBanner/HP-Square-Product/HP-Square-Product-pigeon-08052017.jpg}}" alt="Orijen" /></a></div>
</div>
<div class="col-xs-6">
<div class="img-banner"><a href="https://www.orami.co.th/th/electronics/mobiles-tablets-accessories/batteries-chargers.html?mobile_accessories=5144"><img title="Hospro" src="{{media url="wysiwyg/welcomePage/HomepageBanner/HP-Square-Product/HP-Square-Product-POWERBANK-150517.jpg}}" alt="Hospro" /></a></div>
</div>
</div>
</div>
</div>
<div class="row">
<div class="col-sm-6">
<div class="row">
<div class="col-sm-12">
<div class="img-banner"><a href="https://www.orami.co.th/th/fashion/deals/exclusive-luggage-on-orami.html"><img title="Cheero" src="{{media url="wysiwyg/welcomePage/HomepageBanner/HP-Hero-Product/HP-Hero-Product-Kamilliant+KAMIL30-150517.jpg}}" alt="Cheero" /></a></div>
</div>
</div>
<div class="row">
<div class="col-xs-6">
<div class="img-banner"><a href="https://www.orami.co.th/th/beauty/brands/remington.html"><img title="Orijen" src="{{media url="wysiwyg/welcomePage/HomepageBanner/HP-Square-Product/HP-Square-Product-Remington-150517.jpg}}" alt="Orijen" /></a></div>
</div>
<div class="col-xs-6">
<div class="img-banner"><a href="https://www.orami.co.th/th/home-appliances/deals/welcome-rainy-season.html"><img title="Hospro" src="{{media url="wysiwyg/welcomePage/HomepageBanner/HP-Square-Product/HP-Square-Product-Welcome-Rainy-Season-150517.jpg}}" alt="Hospro" /></a></div>
</div>
</div>
</div>
<div class="col-sm-6">
<div class="row">
<div class="col-sm-12">
<div class="img-banner"><a href="https://www.orami.co.th/th/deal/for-your-home-deals/kitchen-summer-sales.html"><img title="All about cleaning" src="{{media url="wysiwyg/welcomePage/HomepageBanner/HP-Hero/HP-Hero-Kitchen-Summer-Sales-12052017-25OFF.jpg}}" alt="All about cleaning" /></a></div>
</div>
</div>
</div>
</div>
</div>
EOD;
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================


//==========================================================================
// Welcome splash shortcut link (Thai)
//==========================================================================
$blockTitle = "Welcome splash shortcut link";
$blockIdentifier = "welcome-shortcut";
$blockStores = array($moxyth);
$blockIsActive = 1;
$blockContent = <<<EOD
<div class="grid-wall">
<div class="row">
<div class="col-sm-6">
<div class="row">
<div class="col-sm-12">
<div class="img-banner"><a href="https://www.orami.co.th/th/electronics/gadgets/lifestyle-gadget.html"><img title="Cheero" src="{{media url="wysiwyg/welcomePage/HomepageBanner/HP-Hero-Product/HP-Hero-Product-Electronics-Scooter-150517.jpg"}}" alt="Cheero" /></a></div>
</div>
</div>
<div class="row">
<div class="col-xs-6">
<div class="img-banner"><a href="https://www.orami.co.th/th/beauty/deals/clean-up-your-face.html"><img title="Orijen" src="{{media url="wysiwyg/welcomePage/HomepageBanner/HP-Square-Product/HP-Square-Product-Facial-Cleanser-150517.jpg"}}" alt="Orijen" /></a></div>
</div>
<div class="col-xs-6">
<div class="img-banner"><a href="https://www.orami.co.th/th/fashion/deals/new-mom-essential.html"><img title="Hospro" src="{{media url="wysiwyg/welcomePage/HomepageBanner/HP-Square-Product/HP-Square-Product-NEW-MOM-ESSENTIALS--150517.jpg"}}" alt="Hospro" /></a></div>
</div>
</div>
</div>
<div class="col-sm-6">
<div class="row">
<div class="col-sm-12">
<div class="img-banner"><a href="https://www.orami.co.th/th/home-appliances/deals/50-best-home-appliances.html"><img title="All about cleaning" src="{{media url="wysiwyg/welcomePage/HomepageBanner/HP-Hero/HP-Hero-50-Best-Home-Appliances.jpg"}}" alt="All about cleaning" /></a></div>
</div>
</div>
</div>
</div>
<div class="row">
<div class="col-sm-6">
<div class="row">
<div class="col-sm-12">
<div class="img-banner"><a href="https://www.orami.co.th/catalog/category/view/id/4943.html"><img title="All about cleaning" src="{{media url="wysiwyg/welcomePage/HomepageBanner/HP-Hero/HP-Hero-huggies50-9-23MAY.jpg}}" alt="All about cleaning" /></a></div>
</div>
</div>
</div>
<div class="col-sm-6">
<div class="row">
<div class="col-sm-12">
<div class="img-banner"><a href="https://www.orami.co.th/th/deal/for-your-home-deals/ergotrend-officeintrend-special-price.html"><img title="Cheero" src="{{media url="wysiwyg/welcomePage/HomepageBanner/HP-Hero-Product/HP-Hero-Product-Ergotrend-and-Office-intrend-11052017.jpg}}" alt="Cheero" /></a></div>
</div>
</div>
<div class="row">
<div class="col-xs-6">
<div class="img-banner"><a href="https://www.orami.co.th/th/baby/deals/pigeon-promotion.html"><img title="Orijen" src="{{media url="wysiwyg/welcomePage/HomepageBanner/HP-Square-Product/HP-Square-Product-pigeon-08052017.jpg}}" alt="Orijen" /></a></div>
</div>
<div class="col-xs-6">
<div class="img-banner"><a href="https://www.orami.co.th/th/electronics/mobiles-tablets-accessories/batteries-chargers.html?mobile_accessories=5144"><img title="Hospro" src="{{media url="wysiwyg/welcomePage/HomepageBanner/HP-Square-Product/HP-Square-Product-POWERBANK-150517.jpg}}" alt="Hospro" /></a></div>
</div>
</div>
</div>
</div>
<div class="row">
<div class="col-sm-6">
<div class="row">
<div class="col-sm-12">
<div class="img-banner"><a href="https://www.orami.co.th/th/fashion/deals/exclusive-luggage-on-orami.html"><img title="Cheero" src="{{media url="wysiwyg/welcomePage/HomepageBanner/HP-Hero-Product/HP-Hero-Product-Kamilliant+KAMIL30-150517.jpg}}" alt="Cheero" /></a></div>
</div>
</div>
<div class="row">
<div class="col-xs-6">
<div class="img-banner"><a href="https://www.orami.co.th/th/beauty/brands/remington.html"><img title="Orijen" src="{{media url="wysiwyg/welcomePage/HomepageBanner/HP-Square-Product/HP-Square-Product-Remington-150517.jpg}}" alt="Orijen" /></a></div>
</div>
<div class="col-xs-6">
<div class="img-banner"><a href="https://www.orami.co.th/th/home-appliances/deals/welcome-rainy-season.html"><img title="Hospro" src="{{media url="wysiwyg/welcomePage/HomepageBanner/HP-Square-Product/HP-Square-Product-Welcome-Rainy-Season-150517.jpg}}" alt="Hospro" /></a></div>
</div>
</div>
</div>
<div class="col-sm-6">
<div class="row">
<div class="col-sm-12">
<div class="img-banner"><a href="https://www.orami.co.th/th/deal/for-your-home-deals/kitchen-summer-sales.html"><img title="All about cleaning" src="{{media url="wysiwyg/welcomePage/HomepageBanner/HP-Hero/HP-Hero-Kitchen-Summer-Sales-12052017-25OFF.jpg}}" alt="All about cleaning" /></a></div>
</div>
</div>
</div>
</div>
</div>
EOD;
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================