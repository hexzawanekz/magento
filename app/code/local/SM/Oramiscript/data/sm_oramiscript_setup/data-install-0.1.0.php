<?php

//==========================================================================
// Bank Image Footer
//==========================================================================

/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter(array(0), $withAdmin = false)
    ->addFieldToFilter('identifier', 'bank_image_footer')
    ->getFirstItem();

if ($block->getId()) $block->delete();// if exists then delete
$content = <<<EOD
<div class="bank-list-images">
<p class="payment_partners_footer">Payment Partners</p>
<img src="{{skin url="images/bn_payment.png"}}" alt="" /></div>
<div class="acommerce-logo-footer">
<p class="shipping_partners_footer">Shipping Partners</p>
<img src="{{skin url="images/bn_shipping.png"}}" alt="" /></div>
EOD;

$block = Mage::getModel('cms/block');
$block->setTitle('Bank Image Footer');
$block->setIdentifier('bank_image_footer');
$block->setStores(array(0));
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================