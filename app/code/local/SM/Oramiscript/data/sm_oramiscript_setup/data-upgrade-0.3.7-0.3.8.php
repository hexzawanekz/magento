<?php
// init Magento
Mage::app();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

$moxyen = Mage::getModel('core/store')->load('moxyen', 'code')->getId();
$moxyth = Mage::getModel('core/store')->load('moxyth', 'code')->getId();

/** @var Mage_Cms_Model_Resource_Block_Collection $allBlocks */
$allBlocks = Mage::getModel('cms/block')->getCollection()
    ->addFieldToFilter('identifier', 'home-middle-banner');
if($allBlocks->count() > 0) {
    /** @var Mage_Cms_Model_Block $item */
    foreach ($allBlocks as $item) {
        if ($item->getId()) $item->delete(); // delete old blocks
    }
}

//==========================================================================
// Home middle Banner (English)
//==========================================================================
$blockTitle = "Homepage Middle Banner";
$blockIdentifier = "home-middle-banner";
$blockStores = array($moxyen);
$blockIsActive = 1;
$blockContent = <<<EOD
<div id="home-middle-banner">
<div id="home-middle-banner-container">
<h3 class="line-thought">Inspiration For Day | <span style="color: #ff6c6c;">Orami Magazine</span></h3>
<p id="middle-banner-text">A collection of inspirational articles around fashion, mother and child to love and lifestyle</p>
<img title="" src="{{media url="wysiwyg/welcomePage/magazine-banner.jpg"}}" alt="" /></div>
</div>
EOD;
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// Home middle Banner (Thai)
//==========================================================================
$blockTitle = "Homepage Middle Banner";
$blockIdentifier = "home-middle-banner";
$blockStores = array($moxyth);
$blockIsActive = 1;
$blockContent = <<<EOD
<div id="home-middle-banner">
<div id="home-middle-banner-container">
<h3 class="line-thought">Inspiration For Day | <span style="color: #ff6c6c;">Orami Magazine</span></h3>
<p id="middle-banner-text">A collection of inspirational articles around fashion, mother and child to love and lifestyle</p>
<img title="" src="{{media url="wysiwyg/welcomePage/magazine-banner.jpg"}}" alt="" /></div>
</div>
EOD;
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================


/** @var Mage_Cms_Model_Resource_Block_Collection $allBlocks */
$allBlocks = Mage::getModel('cms/block')->getCollection()
    ->addFieldToFilter('identifier', 'home-slider-banner');
if($allBlocks->count() > 0) {
    /** @var Mage_Cms_Model_Block $item */
    foreach ($allBlocks as $item) {
        if ($item->getId()) $item->delete(); // delete old blocks
    }
}
//==========================================================================
// Home Slider Banner (English)
//==========================================================================
$blockTitle = "Homepage Slider Banner";
$blockIdentifier = "home-slider-banner";
$blockStores = array($moxyen);
$blockIsActive = 1;
$blockContent = <<<EOD
<div id="home-slider-banner">
<div id="home-slider-banner-mobile">
<ul id="home-slides-content-mobile" style="position: relative; width: 100%; height: 100%;">
<li style="position: absolute; top: 0; left: 0; display: none; z-index: 1; opacity: 1;"><a style="width: 100%;" href="#"><img src="{{skin url="images/SamplePictures/1.jpg"}}" alt="" /></a></li>
<li style="position: absolute; top: 0; left: 0; display: none; z-index: 1; opacity: 1;"><a style="width: 100%;" href="#"><img src="{{skin url="images/SamplePictures/2.jpg"}}" alt="" /></a></li>
<li style="position: absolute; top: 0; left: 0; display: none; z-index: 1; opacity: 1;"><a style="width: 100%;" href="#"><img src="{{skin url="images/SamplePictures/3.jpg"}}" alt="" /></a></li>
<li style="position: absolute; top: 0; left: 0; display: none; z-index: 1; opacity: 1;"><a style="width: 100%;" href="#"><img src="{{skin url="images/SamplePictures/4.jpg"}}" alt="" /></a></li>
<li style="position: absolute; top: 0; left: 0; display: none; z-index: 1; opacity: 1;"><a style="width: 100%;" href="#"><img src="{{skin url="images/SamplePictures/5.jpg"}}" alt="" /></a></li>
<li style="position: absolute; top: 0; left: 0; display: none; z-index: 1; opacity: 1;"><a style="width: 100%;" href="#"><img src="{{skin url="images/SamplePictures/6.jpg"}}" alt="" /></a></li>
<li style="position: absolute; top: 0; left: 0; display: none; z-index: 1; opacity: 1;"><a style="width: 100%;" href="#"><img src="{{skin url="images/SamplePictures/7.jpg"}}" alt="" /></a></li>
<li style="position: absolute; top: 0; left: 0; display: none; z-index: 1; opacity: 1;"><a style="width: 100%;" href="#"><img src="{{skin url="images/SamplePictures/8.jpg"}}" alt="" /></a></li>
<li style="position: absolute; top: 0; left: 0; display: none; z-index: 1; opacity: 1;"><a style="width: 100%;" href="#"><img src="{{skin url="images/SamplePictures/9.jpg"}}" alt="" /></a></li>
<li style="position: absolute; top: 0; left: 0; display: none; z-index: 1; opacity: 1;"><a style="width: 100%;" href="#"><img src="{{skin url="images/SamplePictures/10.jpg"}}" alt="" /></a></li>
<li style="position: absolute; top: 0; left: 0; display: none; z-index: 1; opacity: 1;"><a style="width: 100%;" href="#"><img src="{{skin url="images/SamplePictures/11.jpg"}}" alt="" /></a></li>
</ul>
</div>
<div id="home-slider-banner-desktop">
<ul id="home-slides-content-desktop">
<li><a style="width: 100%;" href="#"><img src="{{skin url="images/SamplePictures/1.jpg"}}" alt="" /></a></li>
<li><a style="width: 100%;" href="#"><img src="{{skin url="images/SamplePictures/2.jpg"}}" alt="" /></a></li>
<li><a style="width: 100%;" href="#"><img src="{{skin url="images/SamplePictures/3.jpg"}}" alt="" /></a></li>
<li><a style="width: 100%;" href="#"><img src="{{skin url="images/SamplePictures/4.jpg"}}" alt="" /></a></li>
<li><a style="width: 100%;" href="#"><img src="{{skin url="images/SamplePictures/5.jpg"}}" alt="" /></a></li>
<li><a style="width: 100%;" href="#"><img src="{{skin url="images/SamplePictures/6.jpg"}}" alt="" /></a></li>
<li><a style="width: 100%;" href="#"><img src="{{skin url="images/SamplePictures/7.jpg"}}" alt="" /></a></li>
<li><a style="width: 100%;" href="#"><img src="{{skin url="images/SamplePictures/8.jpg"}}" alt="" /></a></li>
<li><a style="width: 100%;" href="#"><img src="{{skin url="images/SamplePictures/9.jpg"}}" alt="" /></a></li>
<li><a style="width: 100%;" href="#"><img src="{{skin url="images/SamplePictures/10.jpg"}}" alt="" /></a></li>
<li><a style="width: 100%;" href="#"><img src="{{skin url="images/SamplePictures/11.jpg"}}" alt="" /></a></li>
</ul>
</div>
</div>
EOD;
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================


//==========================================================================
// Home Slider Banner (Thai)
//==========================================================================
$blockTitle = "Homepage Slider Banner";
$blockIdentifier = "home-slider-banner";
$blockStores = array($moxyth);
$blockIsActive = 1;
$blockContent = <<<EOD
<div id="home-slider-banner">
<div id="home-slider-banner-mobile">
<ul id="home-slides-content-mobile" style="position: relative; width: 100%; height: 100%;">
<li style="position: absolute; top: 0; left: 0; display: none; z-index: 1; opacity: 1;"><a style="width: 100%;" href="#"><img src="{{skin url="images/SamplePictures/1.jpg"}}" alt="" /></a></li>
<li style="position: absolute; top: 0; left: 0; display: none; z-index: 1; opacity: 1;"><a style="width: 100%;" href="#"><img src="{{skin url="images/SamplePictures/2.jpg"}}" alt="" /></a></li>
<li style="position: absolute; top: 0; left: 0; display: none; z-index: 1; opacity: 1;"><a style="width: 100%;" href="#"><img src="{{skin url="images/SamplePictures/3.jpg"}}" alt="" /></a></li>
<li style="position: absolute; top: 0; left: 0; display: none; z-index: 1; opacity: 1;"><a style="width: 100%;" href="#"><img src="{{skin url="images/SamplePictures/4.jpg"}}" alt="" /></a></li>
<li style="position: absolute; top: 0; left: 0; display: none; z-index: 1; opacity: 1;"><a style="width: 100%;" href="#"><img src="{{skin url="images/SamplePictures/5.jpg"}}" alt="" /></a></li>
<li style="position: absolute; top: 0; left: 0; display: none; z-index: 1; opacity: 1;"><a style="width: 100%;" href="#"><img src="{{skin url="images/SamplePictures/6.jpg"}}" alt="" /></a></li>
<li style="position: absolute; top: 0; left: 0; display: none; z-index: 1; opacity: 1;"><a style="width: 100%;" href="#"><img src="{{skin url="images/SamplePictures/7.jpg"}}" alt="" /></a></li>
<li style="position: absolute; top: 0; left: 0; display: none; z-index: 1; opacity: 1;"><a style="width: 100%;" href="#"><img src="{{skin url="images/SamplePictures/8.jpg"}}" alt="" /></a></li>
<li style="position: absolute; top: 0; left: 0; display: none; z-index: 1; opacity: 1;"><a style="width: 100%;" href="#"><img src="{{skin url="images/SamplePictures/9.jpg"}}" alt="" /></a></li>
<li style="position: absolute; top: 0; left: 0; display: none; z-index: 1; opacity: 1;"><a style="width: 100%;" href="#"><img src="{{skin url="images/SamplePictures/10.jpg"}}" alt="" /></a></li>
<li style="position: absolute; top: 0; left: 0; display: none; z-index: 1; opacity: 1;"><a style="width: 100%;" href="#"><img src="{{skin url="images/SamplePictures/11.jpg"}}" alt="" /></a></li>
</ul>
</div>
<div id="home-slider-banner-desktop">
<ul id="home-slides-content-desktop">
<li><a style="width: 100%;" href="#"><img src="{{skin url="images/SamplePictures/1.jpg"}}" alt="" /></a></li>
<li><a style="width: 100%;" href="#"><img src="{{skin url="images/SamplePictures/2.jpg"}}" alt="" /></a></li>
<li><a style="width: 100%;" href="#"><img src="{{skin url="images/SamplePictures/3.jpg"}}" alt="" /></a></li>
<li><a style="width: 100%;" href="#"><img src="{{skin url="images/SamplePictures/4.jpg"}}" alt="" /></a></li>
<li><a style="width: 100%;" href="#"><img src="{{skin url="images/SamplePictures/5.jpg"}}" alt="" /></a></li>
<li><a style="width: 100%;" href="#"><img src="{{skin url="images/SamplePictures/6.jpg"}}" alt="" /></a></li>
<li><a style="width: 100%;" href="#"><img src="{{skin url="images/SamplePictures/7.jpg"}}" alt="" /></a></li>
<li><a style="width: 100%;" href="#"><img src="{{skin url="images/SamplePictures/8.jpg"}}" alt="" /></a></li>
<li><a style="width: 100%;" href="#"><img src="{{skin url="images/SamplePictures/9.jpg"}}" alt="" /></a></li>
<li><a style="width: 100%;" href="#"><img src="{{skin url="images/SamplePictures/10.jpg"}}" alt="" /></a></li>
<li><a style="width: 100%;" href="#"><img src="{{skin url="images/SamplePictures/11.jpg"}}" alt="" /></a></li>
</ul>
</div>
</div>
EOD;
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================