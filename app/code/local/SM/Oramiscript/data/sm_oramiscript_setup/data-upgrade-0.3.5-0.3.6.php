<?php

$moxyen = Mage::getModel('core/store')->load('moxyen', 'code')->getId();// Moxy
$moxyth = Mage::getModel('core/store')->load('moxyth', 'code')->getId();

//==========================================================================
// Guarantee moxy EN
//==========================================================================
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($moxyen, $withAdmin = false)
    ->addFieldToFilter('identifier', 'guarantee_moxy')
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$content = <<<EOD
<p>
<div id="guarantee_sprite_image"></div>
</p>
EOD;
/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block');
$block->setTitle('guarantee_moxy EN');
$block->setIdentifier('guarantee_moxy');
$block->setStores($moxyen);
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================

//==========================================================================
// Guarantee moxy TH
//==========================================================================
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($moxyth, $withAdmin = false)
    ->addFieldToFilter('identifier', 'guarantee_moxy')
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$content = <<<EOD
<p>
<div id="guarantee_sprite_image"></div>
</p>
EOD;
/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block');
$block->setTitle('guarantee_moxy TH');
$block->setIdentifier('guarantee_moxy');
$block->setStores($moxyth);
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================