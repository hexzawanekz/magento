<?php
/** @var Mage_Core_Model_Resource_Setup $installer */
$installer = $this;

$installer->startSetup();

//==========================================================================
// Bank Image Footer
//==========================================================================

/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter(array(0), $withAdmin = false)
    ->addFieldToFilter('identifier', 'bank_image_footer')
    ->getFirstItem();

if ($block->getId()) $block->delete();// if exists then delete
$content = <<<EOD
<div class="bank-list-images">
    <p class="payment_partners_footer">Payment Partners</p>
    <img src="{{skin url="images/footer/payment_partner.png"}}" alt="" />
</div>
<div class="acommerce-logo-footer">
    <p class="shipping_partners_footer">Shipping Partners</p>
    <img src="{{skin url="images/footer/acommerce.png"}}" alt="" />
</div>
<div class="verfied-logo-footer">
    <p class="verified-partner-footer">Verified Services</p>
    <img src="{{skin url="images/footer/verified_service_without_dbd.png"}}" alt="" />
    <script id="dbd-init" type="text/javascript" src="https://www.trustmarkthai.com/callbackData/initialize.js?t=89e1e97527-22-5-45ff32a8f57f26615329ec7b5134ec3420d"></script>
    <div id="Certificate-banners"></div>
</div>
<div class="pay-over-footer">
    <p class="pay-over">Pay Over The Counter</p>
    <img src="{{skin url="images/footer/pay_over_the_counter.png"}}" alt="" />
</div>
EOD;

$block = Mage::getModel('cms/block');
$block->setTitle('Bank Image Footer');
$block->setIdentifier('bank_image_footer');
$block->setStores(array(0));
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================

$installer->endSetup();