<?php


/** @var Mage_Core_Model_Resource_Setup $installer */
$installer = $this;

$installer->startSetup();

/** Reset Password Mail Template Moxy */
/** @var Mage_Adminhtml_Model_Email_Template $resetPassword */
$resetPassword = Mage::getModel('adminhtml/email_template');
$resetPassword->loadByCode('Reset Password Moxy');
$mailContent = <<<HTML
<meta charset="utf-8"/>
<div class="mail-container" style="width:600px; min-width:600px; padding:0px 33px; font-family:  'Trebuchet MS', Tahoma; font-size: 14px; color: #3c3d41; margin: 0px;">
    <div class="mail-header" style="width:95%; min-height: 110px; max-height: 110px; position: relative; margin:0px auto;">
        <div class="mail-logo" style="width:220px; min-height: 110px; max-height: 110px; margin:0px auto; text-align:center;">
            <a href="http://moxy.co.th/"><img src="{{skin _area="frontend"  url="images/email/moxy-orami-logo.png"}}" style="width:220px; max-height: 110px; min-height: 110px;" /></a>
        </div>

        <div class="english-below" style="min-height: 16px; max-height: 16px; position: absolute; bottom: 0px; line-height: 16px;">
            <span><img src="{{skin _area="frontend"  url="images/email/flag-en.png"}}" height="16px;"/> Scroll down for English version</span>
        </div>
    </div>

    <div class="header-image" style="width: 100%; height: auto; position: relative; margin:60px auto; margin-bottom: 80px; text-align: center">
        <img src="{{skin _area="frontend"  url="images/email/password_reset_moxy.png"}}" style="width:100%; height:auto;" alt="Reset Password" />
    </div>

    <div class="mail-body" style="margin:50px auto; width: 95%;">
        <p>เรียน คุณ {{var customer.name}},</p>
        <br/>

        <p>ทางเราได้รับคำขอเปลี่ยนรหัสผ่านสำหรับบัญชีของคุณ</p>
        <br>

        <a href="{{store url="customer/account/resetpassword/" _query_id=.id _query_token=.rp_token}}" style="color: #fff; text-decoration: none; background: #ff6c6c;padding: 10px;">เปลี่ยนรหัสผ่าน</a>
        <br>
        <br>
        <p>หากคุณข้ามอีเมลล์นี้ไป รหัสของคุณยังคงเหมือนเดิม</p>
        <p>หากคุณไม่ได้ส่งคำขอนี้ แจ้งให้เราทราบ</p>
        <br>

        <p>Warm Regards,</p>
        <p>Moxy Team</p>
    </div>


    <!-- Footer -->
    <div class="navigation-th" style="margin-top: 24px; overflow:hidden;">
        <div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #ff6c6c; border-bottom: 2px solid #ff6c6c; text-align:center;">
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding-right: 5px;"><a href="http://www.moxy.co.th/th/beauty.html" style="color: #3c3d41; text-decoration: none;">ความงาม</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/fashion.html" style="color: #3c3d41; text-decoration: none;">แฟชั่น</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/home-living.html" style="color: #3c3d41; text-decoration: none;">ของแต่งบ้าน</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/home-appliances.html" style="color: #3c3d41; text-decoration: none;">เครื่องใช้ไฟฟ้าภายในบ้าน</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/electronics.html" style="color: #3c3d41; text-decoration: none;">อิเล็กทรอนิกส์</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/baby.html" style="color: #3c3d41; text-decoration: none;">แม่และเด็ก</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/health.html" style="color: #3c3d41; text-decoration: none;">สุขภาพ</a></span>
            <span class="nav-item" style=" line-height: 33px; font-size: 12px; padding-left: 5px;"><a href="http://www.moxy.co.th/th/pets.html" style="color: #3c3d41; text-decoration: none;">สัตว์เลี้ยง</a></span>
        </div>
    </div>

    <div class="email-footer" style="min-height: 85px; max-heigh: 85px; background: #E7E7E7; margin-top: 8px; color: #3c3d41; padding: 0 15px 5px 15px;">
        <div class="footer-col" style="float:left; padding: 17px 1% 17px 2%; width: 27%; color: #3c3d41;">
            <p style="font-size: 13px; margin: 0px 0px 0px"><img src="{{skin _area='frontend'  url='images/email/mail.png'}}" style="width:20px; float:left; margin-right:5px; margin-left:-2px;"/> ติดต่อเรา</p><div style="clear: both;"></div>
            <p style="font-size: 10px; margin: 0;">02-106-8222</p>
            <p style="margin: 0; font-size: 10px;"><a href="mailto:support@moxy.co.th" style="color: #3C3D41; text-decoration: none;">support@moxy.co.th</a></p>
        </div>

        <div class="footer-col" style="float:left; padding: 17px 1% 17px 0; width: 32%;">
            <p style="font-size: 13px; margin: 0px 0px 0px">ติดตามที่</p>
            <div class="follow-icons" style="width: 100%">
                <a href="https://www.facebook.com/moxyst" style="float: left; width: 16%; padding-right: 2%;"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-fb.png"}}" alt="Facebook fanpage"></a>
                <a href="https://twitter.com/moxyst" style="float: left; width: 16%; padding-right: 2%;"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-twitter.png"}}" alt="Twitter fanpage"></a>
                <a href="https://www.instagram.com/moxy_th/" style="float: left; width: 16%; padding-right: 2%;"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-instagram.png"}}" alt="Instagram fanpage"></a>
                <a href="https://plus.google.com/+Moxyst" style="float: left; width: 16%; padding-right: 2%;"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-googleplus.png"}}" alt="Google fanpage"></a>
                <a href="https://www.pinterest.com/MoxySEA1/" style="float: left; width: 16%; padding-right: 2%;"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-pinterest.png"}}" alt="Pinterest fanpage"></a>
            </div>
        </div>

        <div class="footer-col" style="float:left; padding: 17px 0 17px 3%; width: 34%;">
            <p style="font-size: 13px;margin: 0px 0px 0px">ช่องทางการชำระเงิน</p>
            <img src="{{skin _area="frontend"  url="images/email/payment_new.png"}}" alt="ช่องทางการชำระเงิน" style="margin-top: 3px;">
            <div class="cod" style="font-size: 10px; margin-top: -3px;">
                <img src="{{skin _area='frontend'  url='images/email/COD.png'}}" style="float: left; margin-right: 2px;">
                <p style="float: left; margin: 3px 0 0; font-size: 10px;"> ช่องทางการชำระเงิน</p>
            </div>
        </div>
    </div>

    <div class="footer" style="padding: 0; margin-top: 18px;">
        <div class="copyr" style="text-align: center; font-size: 13px;">&copy;2016 Whatsnew Co.,Ltd. All Rights Reserved</div>
        <div class="links" style="text-align: center; font-size: 13px; margin-top: 5px;">
            <a href="http://www.moxy.co.th/th/about-us/" style="color: #3c3d41;"><span>เกี่ยวกับเรา</span></a> |
            <a href="http://www.moxy.co.th/th/terms-and-conditions/" style="color: #3c3d41;"><span>ข้อกำหนดและเงื่อนไข</span></a> |
            <a href="http://www.moxy.co.th/th/privacy-policy/" style="color: #3c3d41;"><span>ความเป็นส่วนตัว</span></a>
        </div>
    </div>
    <div style="clear:both"></div>
    <!-- End Footer -->

    <hr style="margin: 40px auto; width: 95%;">

    <!--English Version-->
    <div class="mail-body" style="margin:50px auto; width: 95%;">
        <p>Dear {{var customer.name}},</p>
        <br/>

        <p>We just received a request to reset your password.</p>
        <p>Don’t worry, you can simply click on the button and create a new one.</p>
        <br>
        <a href="{{store url="customer/account/resetpassword/" _query_id=.id _query_token=.rp_token}}" style="color: #fff; text-decoration: none; background: #ff6c6c;padding: 10px;">RESET</a>
        <br>
        <br>
        <p>Ignore this message if you didn’t request a new password.</p>
        <br>

        <p>Warm Regards,</p>
        <p>Moxy Team</p>
    </div>

    <!-- Footer -->
    <div class="navigation-th" style="margin-top: 24px; overflow:hidden;">
        <div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #ff6c6c; border-bottom: 2px solid #ff6c6c; text-align:center;">
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding-right: 5px;"><a href="http://www.moxy.co.th/th/beauty.html" style="color: #3c3d41; text-decoration: none;">Beauty</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/fashion.html" style="color: #3c3d41; text-decoration: none;">Fashion</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/home-living.html" style="color: #3c3d41; text-decoration: none;">Home Decor</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/home-appliances.html" style="color: #3c3d41; text-decoration: none;">Home Appliances</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/electronics.html" style="color: #3c3d41; text-decoration: none;">Electronics</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/baby.html" style="color: #3c3d41; text-decoration: none;">Baby & Mom</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/health.html" style="color: #3c3d41; text-decoration: none;">Health & Sports</a></span>
            <span class="nav-item" style=" line-height: 33px; font-size: 12px; padding-left: 5px;"><a href="http://www.moxy.co.th/th/pets.html" style="color: #3c3d41; text-decoration: none;">Pets</a></span>
        </div>
    </div>

    <div class="email-footer" style="min-height: 85px; max-heigh: 85px; background: #E7E7E7; margin-top: 8px; color: #3c3d41;padding: 0 15px 5px 15px;">
        <div class="footer-col" style="float:left; padding: 17px 1% 17px 2%; width: 27%; color: #3c3d41;">
            <p style="font-size: 13px; margin: 0px 0px 0px"><img src="{{skin _area='frontend'  url='images/email/mail.png'}}" style="width:20px; float:left; margin-right:5px; margin-left: -2px;"/> Contact Us</p><div style="clear: both;"></div>
            <p style="font-size: 10px; margin: 0;">02-106-8222</p>
            <p style="margin: 0; font-size: 10px;"><a href="mailto:support@moxy.co.th" style="color: #3C3D41; text-decoration: none;">support@moxy.co.th</a></p>
        </div>

        <div class="footer-col" style="float:left; padding: 17px 1% 17px 0; width: 32%;">
            <p style="font-size: 13px; margin: 0px 0px 0px">Follow Us</p>
            <div class="follow-icons" style="width: 100%">
                <a href="https://www.facebook.com/moxyst" style="float: left; width: 16%; padding-right: 2%;"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-fb.png"}}" alt="Facebook fanpage"></a>
                <a href="https://twitter.com/moxyst" style="float: left; width: 16%; padding-right: 2%;"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-twitter.png"}}" alt="Twitter fanpage"></a>
                <a href="https://www.instagram.com/moxy_th/" style="float: left; width: 16%; padding-right: 2%;"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-instagram.png"}}" alt="Instagram fanpage"></a>
                <a href="https://plus.google.com/+Moxyst" style="float: left; width: 16%; padding-right: 2%;"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-googleplus.png"}}" alt="Google fanpage"></a>
                <a href="https://www.pinterest.com/MoxySEA1/" style="float: left; width: 16%; padding-right: 2%;"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-pinterest.png"}}" alt="Pinterest fanpage"></a>
            </div>
        </div>

        <div class="footer-col" style="float:left; padding: 17px 0 17px 3%; width: 34%;">
            <p style="font-size: 13px;margin: 0px 0px 0px">Payment Options</p>
            <img src="{{skin _area="frontend"  url="images/email/payment_new.png"}}" alt="Payment Options" style="margin-top: 3px;">
            <div class="cod" style="font-size: 10px; margin-top: -3px;">
                <img src="{{skin _area='frontend'  url='images/email/COD.png'}}" style="float: left; margin-right: 2px;">
                <p style="float: left; margin: 3px 0 0; font-size: 10px;"> Cash on Delivery</p>
            </div>
        </div>
    </div>

    <div class="footer" style="padding: 0; margin-top: 18px;">
        <div class="copyr" style="text-align: center; font-size: 13px;">&copy;2016 Whatsnew Co.,Ltd. All Rights Reserved</div>
        <div class="links" style="text-align: center; font-size: 13px; margin-top: 5px;">
            <a href="http://www.moxy.co.th/en/about-us/" style="color: #3c3d41;"><span>About Us</span></a> |
            <a href="http://www.moxy.co.th/en/terms-and-conditions/" style="color: #3c3d41;"><span>Term & Conditions</span></a> |
            <a href="http://www.moxy.co.th/en/privacy-policy/" style="color: #3c3d41;"><span>Privacy Policy</span></a>
        </div>
    </div>
    <div style="clear:both"></div>
    <!-- End Footer -->
</div>
HTML;
$resetPassword->setTemplateText($mailContent);
$resetPassword->save();

/** Back in Stock Mail Template Moxy */
/** @var Mage_Adminhtml_Model_Email_Template $backInStock */
$backInStock = Mage::getModel('adminhtml/email_template');
$backInStock->loadByCode('Back in Stock Moxy');
$mailContent = <<<HTML
<meta charset="utf-8"/>
<div class="mail-container" style="width:600px; min-width:600px; padding:0px 33px; font-family:  'Trebuchet MS', Tahoma; font-size: 14px; color: #3c3d41; margin: 0px;">
    <div class="mail-header" style="width:95%; min-height: 110px; max-height: 110px; position: relative; margin:0px auto;">
        <div class="mail-logo" style="width:220px; min-height: 110px; max-height: 110px; margin:0px auto; text-align:center">
            <a href="http://moxy.co.th/"><img src="{{skin url="images/email/moxy-orami-logo.png"}}" style="width:220px; max-height: 110px; min-height: 110px;" /></a>
        </div>

        <div class="english-below" style="min-height: 16px; max-height: 16px; position: absolute; bottom: 0px; line-height: 16px;">
            <span><img src="{{skin url="images/email/flag-en.png"}}" height="16px;"/> Scroll down for English version</span>
        </div>
    </div>

    <div class="header-image" style="width: 100%; height: auto; position: relative; margin:40px auto; text-align: center">
        <img src="{{skin _area="frontend"  url="images/email/its_backinstock_thai.png"}}" style="width:100%; height:auto;" alt="Back in stock" />
    </div>

    <div class="product-info" style="width: 100%; margin:5px auto; padding:10px; border: 1px solid #e7e7e7;">
        <div class="product-image" style="width: 36%; padding: 5px; border: 1px solid #e7e7e7; float: left;">
            <a href="{{var product_url}}?utm_source=edm&amp;utm_medium=reminder&amp;utm_campaign=moxy_outofstock_reminder"><img src="{{var product_image}}" alt="{{var product}}" style="border: none; width: 95%;"/></a>
        </div>

        <div class="product-detail" style="float: right; width: 60%; text-align: center; margin-top: 15px;">
            <p><strong>{{var product}}</strong></p>

            <p><strong>{{var product_price}}</strong></p>

            <a href="{{var product_url}}?utm_source=edm&amp;utm_medium=reminder&amp;utm_campaign=moxy_outofstock_reminder" style=" background: #ff6c6c;color: #fff;padding: 10px;text-decoration: none;" >สั่งซื้อตอนนี้เลย</a>
        </div>

        <div style="clear: both;"></div>
    </div>
    <div class="mail-body" style="margin:50px auto; width: 95%;">
        <p>เรียน คุณ {{var customer.name}},</p>
        <br/>

        <p>คุณได้ทำการเพิ่มสินค้า {{var product}} เมื่อวันที่ {{var date}} เข้าไปยังตะกร้าสินค้าก่อนหน้านี้ </p>
        <p>เรามีความยินดีจะแจ้งว่าสินค้าตอนนี้ได้กลับเข้าสู่สต็อกสินค้าแล้ว</p>
        <br>

        <p>Warm Regards,</p>
        <p>Moxy Team</p>
    </div>

    <!-- Footer -->
    <div class="navigation-th" style="margin-top: 24px; overflow:hidden;">
        <div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #ff6c6c; border-bottom: 2px solid #ff6c6c; text-align:center;">
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding-right: 5px;"><a href="http://www.moxy.co.th/th/beauty.html" style="color: #3c3d41; text-decoration: none;">ความงาม</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/fashion.html" style="color: #3c3d41; text-decoration: none;">แฟชั่น</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/home-living.html" style="color: #3c3d41; text-decoration: none;">ของแต่งบ้าน</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/home-appliances.html" style="color: #3c3d41; text-decoration: none;">เครื่องใช้ไฟฟ้าภายในบ้าน</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/electronics.html" style="color: #3c3d41; text-decoration: none;">อิเล็กทรอนิกส์</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/baby.html" style="color: #3c3d41; text-decoration: none;">แม่และเด็ก</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/health.html" style="color: #3c3d41; text-decoration: none;">สุขภาพ</a></span>
            <span class="nav-item" style=" line-height: 33px; font-size: 12px; padding-left: 5px;"><a href="http://www.moxy.co.th/th/pets.html" style="color: #3c3d41; text-decoration: none;">สัตว์เลี้ยง</a></span>
        </div>
    </div>

    <div class="email-footer" style="min-height: 85px; max-heigh: 85px; background: #E7E7E7; margin-top: 8px; color: #3c3d41; padding: 0 15px 5px 15px;">
        <div class="footer-col" style="float:left; padding: 17px 1% 17px 2%; width: 27%; color: #3c3d41;">
            <p style="font-size: 13px; margin: 0px 0px 0px"><img src="{{skin _area='frontend'  url='images/email/mail.png'}}" style="width:20px; float:left; margin-right:5px; margin-left:-2px;"/> ติดต่อเรา</p><div style="clear: both;"></div>
            <p style="font-size: 10px; margin: 0;">02-106-8222</p>
            <p style="margin: 0; font-size: 10px;"><a href="mailto:support@moxy.co.th" style="color: #3C3D41; text-decoration: none;">support@moxy.co.th</a></p>
        </div>

        <div class="footer-col" style="float:left; padding: 17px 1% 17px 0; width: 32%;">
            <p style="font-size: 13px; margin: 0px 0px 0px">ติดตามที่</p>
            <div class="follow-icons" style="width: 100%">
                <a href="https://www.facebook.com/moxyst" style="float: left; width: 16%; padding-right: 2%;"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-fb.png"}}" alt="Facebook fanpage"></a>
                <a href="https://twitter.com/moxyst" style="float: left; width: 16%; padding-right: 2%;"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-twitter.png"}}" alt="Twitter fanpage"></a>
                <a href="https://www.instagram.com/moxy_th/" style="float: left; width: 16%; padding-right: 2%;"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-instagram.png"}}" alt="Instagram fanpage"></a>
                <a href="https://plus.google.com/+Moxyst" style="float: left; width: 16%; padding-right: 2%;"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-googleplus.png"}}" alt="Google fanpage"></a>
                <a href="https://www.pinterest.com/MoxySEA1/" style="float: left; width: 16%; padding-right: 2%;"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-pinterest.png"}}" alt="Pinterest fanpage"></a>
            </div>
        </div>

        <div class="footer-col" style="float:left; padding: 17px 0 17px 3%; width: 34%;">
            <p style="font-size: 13px;margin: 0px 0px 0px">ช่องทางการชำระเงิน</p>
            <img src="{{skin _area="frontend"  url="images/email/payment_new.png"}}" alt="ช่องทางการชำระเงิน" style="margin-top: 3px;">
            <div class="cod" style="font-size: 10px; margin-top: -3px;">
                <img src="{{skin _area='frontend'  url='images/email/COD.png'}}" style="float: left; margin-right: 2px;">
                <p style="float: left; margin: 3px 0 0; font-size: 10px;"> ช่องทางการชำระเงิน</p>
            </div>
        </div>
    </div>

    <div class="footer" style="padding: 0; margin-top: 18px;">
        <div class="copyr" style="text-align: center; font-size: 13px;">&copy;2016 Whatsnew Co.,Ltd. All Rights Reserved</div>
        <div class="links" style="text-align: center; font-size: 13px; margin-top: 5px;">
            <a href="http://www.moxy.co.th/th/about-us/" style="color: #3c3d41;"><span>เกี่ยวกับเรา</span></a> |
            <a href="http://www.moxy.co.th/th/terms-and-conditions/" style="color: #3c3d41;"><span>ข้อกำหนดและเงื่อนไข</span></a> |
            <a href="http://www.moxy.co.th/th/privacy-policy/" style="color: #3c3d41;"><span>ความเป็นส่วนตัว</span></a>
        </div>
    </div>
    <div style="clear:both"></div>
    <!-- End Footer -->

    <hr style="margin: 40px auto; width: 95%;">

    <!--English Version-->
    <div class="header-image" style="width:100%; height: auto; position: relative; margin:40px auto; text-align: center">
        <img src="{{skin _area='frontend'  url='images/email/its_backinstock_en.png'}}" style="width:100%; height:auto;" alt="Back in stock" />
    </div>

    <div class="product-info" style="width: 100%; margin:5px auto; padding:10px; border: 1px solid #e7e7e7;">
        <div class="product-image" style="width: 36%; padding: 5px; border: 1px solid #e7e7e7; float: left;">
            <a href="{{var product_url}}?utm_source=edm&amp;utm_medium=reminder&amp;utm_campaign=moxy_outofstock_reminder"><img src="{{var product_image}}" alt="{{var product}}" style="border: none; width: 95%;"/></a>
        </div>

        <div class="product-detail" style="float: right; width: 60%; text-align: center; margin-top: 15px;">
            <p><strong>{{var product}}</strong></p>
            <p><strong>{{var product_price}}</strong></p>
            <a href="{{var product_url}}?utm_source=edm&amp;utm_medium=reminder&amp;utm_campaign=moxy_outofstock_reminder" style=" background: #ff6c6c;color: #fff;padding: 10px;text-decoration: none;" >BUY IT NOW</a>
        </div>

        <div style="clear: both;"></div>
    </div>
    <div class="mail-body" style="margin:50px auto; width: 95%;">
        <p>Dear {{var customer.name}},</p>
        <br/>

        <p>On {{var date}} , you tried to purchase {{var product}}. </p>
        <p>We have a good news for you, it is back in stock.</p>
        <br>
        <p>Get your hands on it now while stock lasts.</p>

        <p>Warm Regards,</p>
        <p>Moxy Team</p>
    </div>

    <!-- Footer -->
    <div class="navigation-th" style="margin-top: 24px; overflow:hidden;">
        <div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #ff6c6c; border-bottom: 2px solid #ff6c6c; text-align:center;">
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding-right: 5px;"><a href="http://www.moxy.co.th/th/beauty.html" style="color: #3c3d41; text-decoration: none;">Beauty</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/fashion.html" style="color: #3c3d41; text-decoration: none;">Fashion</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/home-living.html" style="color: #3c3d41; text-decoration: none;">Home Decor</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/home-appliances.html" style="color: #3c3d41; text-decoration: none;">Home Appliances</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/electronics.html" style="color: #3c3d41; text-decoration: none;">Electronics</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/baby.html" style="color: #3c3d41; text-decoration: none;">Baby & Mom</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/health.html" style="color: #3c3d41; text-decoration: none;">Health & Sports</a></span>
            <span class="nav-item" style=" line-height: 33px; font-size: 12px; padding-left: 5px;"><a href="http://www.moxy.co.th/th/pets.html" style="color: #3c3d41; text-decoration: none;">Pets</a></span>
        </div>
    </div>

    <div class="email-footer" style="min-height: 85px; max-heigh: 85px; background: #E7E7E7; margin-top: 8px; color: #3c3d41;padding: 0 15px 5px 15px;">
        <div class="footer-col" style="float:left; padding: 17px 1% 17px 2%; width: 27%; color: #3c3d41;">
            <p style="font-size: 13px; margin: 0px 0px 0px"><img src="{{skin _area='frontend'  url='images/email/mail.png'}}" style="width:20px; float:left; margin-right:5px; margin-left: -2px;"/> Contact Us</p><div style="clear: both;"></div>
            <p style="font-size: 10px; margin: 0;">02-106-8222</p>
            <p style="margin: 0; font-size: 10px;"><a href="mailto:support@moxy.co.th" style="color: #3C3D41; text-decoration: none;">support@moxy.co.th</a></p>
        </div>

        <div class="footer-col" style="float:left; padding: 17px 1% 17px 0; width: 32%;">
            <p style="font-size: 13px; margin: 0px 0px 0px">Follow Us</p>
            <div class="follow-icons" style="width: 100%">
                <a href="https://www.facebook.com/moxyst" style="float: left; width: 16%; padding-right: 2%;"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-fb.png"}}" alt="Facebook fanpage"></a>
                <a href="https://twitter.com/moxyst" style="float: left; width: 16%; padding-right: 2%;"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-twitter.png"}}" alt="Twitter fanpage"></a>
                <a href="https://www.instagram.com/moxy_th/" style="float: left; width: 16%; padding-right: 2%;"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-instagram.png"}}" alt="Instagram fanpage"></a>
                <a href="https://plus.google.com/+Moxyst" style="float: left; width: 16%; padding-right: 2%;"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-googleplus.png"}}" alt="Google fanpage"></a>
                <a href="https://www.pinterest.com/MoxySEA1/" style="float: left; width: 16%; padding-right: 2%;"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-pinterest.png"}}" alt="Pinterest fanpage"></a>
            </div>
        </div>

        <div class="footer-col" style="float:left; padding: 17px 0 17px 3%; width: 34%;">
            <p style="font-size: 13px;margin: 0px 0px 0px">Payment Options</p>
            <img src="{{skin _area="frontend"  url="images/email/payment_new.png"}}" alt="Payment Options" style="margin-top: 3px;">
            <div class="cod" style="font-size: 10px; margin-top: -3px;">
                <img src="{{skin _area='frontend'  url='images/email/COD.png'}}" style="float: left; margin-right: 2px;">
                <p style="float: left; margin: 3px 0 0; font-size: 10px;"> Cash on Delivery</p>
            </div>
        </div>
    </div>

    <div class="footer" style="padding: 0; margin-top: 18px;">
        <div class="copyr" style="text-align: center; font-size: 13px;">&copy;2016 Whatsnew Co.,Ltd. All Rights Reserved</div>
        <div class="links" style="text-align: center; font-size: 13px; margin-top: 5px;">
            <a href="http://www.moxy.co.th/en/about-us/" style="color: #3c3d41;"><span>About Us</span></a> |
            <a href="http://www.moxy.co.th/en/terms-and-conditions/" style="color: #3c3d41;"><span>Term & Conditions</span></a> |
            <a href="http://www.moxy.co.th/en/privacy-policy/" style="color: #3c3d41;"><span>Privacy Policy</span></a>
        </div>
    </div>
    <div style="clear:both"></div>
    <!-- End Footer -->
</div>
HTML;
$backInStock->setTemplateText($mailContent);
$backInStock->save();

/** Order Confirmation Mail Template Moxy */
/** @var Mage_Adminhtml_Model_Email_Template $orderConfirmation */
$orderConfirmation = Mage::getModel('adminhtml/email_template');
$orderConfirmation->loadByCode('Order Confirmation Moxy');
$mailContent = <<<HTML
<meta charset="utf-8"/>
<div class="mail-container" style="width:600px; min-width:600px; padding:0px 33px; font-family:  'Trebuchet MS', Tahoma; font-size: 14px; color: #3c3d41; margin: 0px;">
    <div class="mail-header" style="width:95%; min-height: 110px; max-height: 110px; position: relative; margin:0px auto;">
        <div class="mail-logo" style="width:220px; min-height: 110px; max-height: 110px; margin:0px auto; text-align:center;">
            <a href="http://moxy.co.th/"><img src="{{skin _area="frontend"  url="images/email/moxy-orami-logo.png"}}" style="width:220px; max-height: 110px; min-height: 110px;" /></a>
        </div>

        <div class="english-below" style="min-height: 16px; max-height: 16px; position: absolute; bottom: 0px; line-height: 16px;">
            <span><img src="{{skin _area="frontend"  url="images/email/flag-en.png"}}" height="16px;"/> Scroll down for English version</span>
        </div>
    </div>

    <div class="header-image" style="width: 100%; position: relative; margin:30px auto; text-align: center">
        <img src="{{skin _area="frontend"  url="images/email/thank_for_your_order_moxy.png"}}" style="width:100%; height: auto;" alt="Thank for your order" />
    </div>

    <div class="mail-body" style="margin:50px auto; width: 95%;">
        <p>เรียน คุณ {{var order.getCustomerName()}},</p>
        <br/>

        <p>ขอขอบคุณสำหรับการสั่งซื้อจาก Moxy</p>
        <p>เลขที่ใบสั่งซื้อของคุณคือ  #{{var order.increment_id}}</p>
        <br>

        <p>ทางเราจะจัดส่งสินค้าให้ถึงคุณภายใน 1-3 วันทำการสำหรับกรุงเทพฯและภายใน 3-5</p>
        <p>วันทำการสำหรับต่างจังหวัดทุกจังหวัดทั่วประเทศ</p>
        <br>

        <p>คุณสามารถเช็คสถานะการสั่งซื้อของคุณที่ : <a href="{{store url="customer/account/"}}" style="color:#28438c; text-decoration:none;">{{store url="customer/account/"}}</a></p>
        <p>หากคุณมีคำถามหรือข้อสงสัยใดๆสามารถติดต่อเราได้ที่ 02-106-8222</p>
        <br>

        <p>Warm Regards,</p>
        <p>Moxy Team</p>
    </div>

    <!--Billing-->
    <div class="order-detail" style="width: 95%; margin:0 auto; font-size: 12px;">
        <div class="billing-info" style="float: left; width: 49%;">
            <div class="heading" style="background: #ff6c6c; font-size: 12; color: #fff; padding: 3px 0px; float: left; width: 100%;"><div style="margin: 0px 5px; text-transform: uppercase;">ข้อมูลในการออกใบเสร็จ</div></div>
            <div class="billing-content" style="float: left; width: 100%; background: #e7e7e7; min-height: 175px;">
                <div style="margin: 0px 5px;">
                    {{var order.getBillingAddress().format('html')}}
                </div>
            </div>
        </div>

        <div class="payment-method" style="float: right; width: 49%; font-size: 12px;">
            <div class="heading" style="background: #ff6c6c; font-size: 12; color: #fff; padding: 3px 0px; float: left; width: 100%;"><div style="margin: 0px 5px; text-transform: uppercase;">วิธีการชำระเงิน</div></div>
            <div class="billing-content" style="float: left; width: 100%; background: #e7e7e7; min-height: 175px;">
                <div style="margin: 0px 5px;">
                    {{var payment_html}}
                </div>
            </div>
        </div>

        <div style="clear: both;"></div>
    </div>

    {{depend order.getIsNotVirtual()}}
    <!--Shipping-->
    <div class="order-detail" style="width: 95%; margin:15px auto; font-size: 12px;">
        <div class="billing-info" style="float: left; width: 49%;">
            <div class="heading" style="background: #ff6c6c; font-size: 12; color: #fff; padding: 3px 0px; float: left; width: 100%;"><div style="margin: 0px 5px; text-transform: uppercase;">ข้อมูลในการจัดส่งสินค้า</div></div>
            <div class="billing-content" style="float: left; width: 100%; background: #e7e7e7; min-height: 175px;">
                <div style="margin: 0px 5px;">
                    {{var order.getShippingAddress().format('html')}}
                </div>
            </div>
        </div>

        <div class="payment-method" style="float: right; width: 49%; font-size: 12px;">
            <div class="heading" style="background: #ff6c6c; font-size: 12; color: #fff; padding: 3px 0px; float: left; width: 100%;"><div style="margin: 0px 5px; text-transform: uppercase;">วิธีการส่งสินค้า</div></div>
            <div class="billing-content" style="float: left; width: 100%; background: #e7e7e7; min-height: 175px;">
                <div style="margin: 0px 5px;">
                    {{var order.getShippingDescription()}}
                </div>
            </div>
        </div>

        <div style="clear: both;"></div>
    </div>
    {{/depend}}

    <!--Order Item-->
    {{layout handle="sales_email_order_items" order=\$order language="th"}}
    <p style="font-size:12px; margin:0 0 10px 0">{{var order.getEmailCustomerNote()}}</p>
    <!--/End OrderItem-->
    <div class="banner" style="max-height: 120px; position: relative; margin:45px auto;">
        {{block type="cms/block" block_id="email_banner_en" }}
    </div>

    <!-- Footer -->
    <div class="navigation-th" style="margin-top: 24px; overflow:hidden;">
        <div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #ff6c6c; border-bottom: 2px solid #ff6c6c; text-align:center;">
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding-right: 5px;"><a href="http://www.moxy.co.th/th/beauty.html" style="color: #3c3d41; text-decoration: none;">ความงาม</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/fashion.html" style="color: #3c3d41; text-decoration: none;">แฟชั่น</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/home-living.html" style="color: #3c3d41; text-decoration: none;">ของแต่งบ้าน</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/home-appliances.html" style="color: #3c3d41; text-decoration: none;">เครื่องใช้ไฟฟ้าภายในบ้าน</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/electronics.html" style="color: #3c3d41; text-decoration: none;">อิเล็กทรอนิกส์</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/baby.html" style="color: #3c3d41; text-decoration: none;">แม่และเด็ก</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/health.html" style="color: #3c3d41; text-decoration: none;">สุขภาพ</a></span>
            <span class="nav-item" style=" line-height: 33px; font-size: 12px; padding-left: 5px;"><a href="http://www.moxy.co.th/th/pets.html" style="color: #3c3d41; text-decoration: none;">สัตว์เลี้ยง</a></span>
        </div>
    </div>

    <div class="email-footer" style="min-height: 85px; max-heigh: 85px; background: #E7E7E7; margin-top: 8px; color: #3c3d41; padding: 0 15px 5px 15px;">
        <div class="footer-col" style="float:left; padding: 17px 1% 17px 2%; width: 27%; color: #3c3d41;">
            <p style="font-size: 13px; margin: 0px 0px 0px"><img src="{{skin _area='frontend'  url='images/email/mail.png'}}" style="width:20px; float:left; margin-right:5px; margin-left:-2px;"/> ติดต่อเรา</p><div style="clear: both;"></div>
            <p style="font-size: 10px; margin: 0;">02-106-8222</p>
            <p style="margin: 0; font-size: 10px;"><a href="mailto:support@moxy.co.th" style="color: #3C3D41; text-decoration: none;">support@moxy.co.th</a></p>
        </div>

        <div class="footer-col" style="float:left; padding: 17px 1% 17px 0; width: 32%;">
            <p style="font-size: 13px; margin: 0px 0px 0px">ติดตามที่</p>
            <div class="follow-icons" style="width: 100%">
                <a href="https://www.facebook.com/moxyst" style="float: left; width: 16%; padding-right: 2%;"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-fb.png"}}" alt="Facebook fanpage"></a>
                <a href="https://twitter.com/moxyst" style="float: left; width: 16%; padding-right: 2%;"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-twitter.png"}}" alt="Twitter fanpage"></a>
                <a href="https://www.instagram.com/moxy_th/" style="float: left; width: 16%; padding-right: 2%;"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-instagram.png"}}" alt="Instagram fanpage"></a>
                <a href="https://plus.google.com/+Moxyst" style="float: left; width: 16%; padding-right: 2%;"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-googleplus.png"}}" alt="Google fanpage"></a>
                <a href="https://www.pinterest.com/MoxySEA1/" style="float: left; width: 16%; padding-right: 2%;"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-pinterest.png"}}" alt="Pinterest fanpage"></a>
            </div>
        </div>

        <div class="footer-col" style="float:left; padding: 17px 0 17px 3%; width: 34%;">
            <p style="font-size: 13px;margin: 0px 0px 0px">ช่องทางการชำระเงิน</p>
            <img src="{{skin _area="frontend"  url="images/email/payment_new.png"}}" alt="ช่องทางการชำระเงิน" style="margin-top: 3px;">
            <div class="cod" style="font-size: 10px; margin-top: -3px;">
                <img src="{{skin _area='frontend'  url='images/email/COD.png'}}" style="float: left; margin-right: 2px;">
                <p style="float: left; margin: 3px 0 0; font-size: 10px;"> ช่องทางการชำระเงิน</p>
            </div>
        </div>
    </div>

    <div class="footer" style="padding: 0; margin-top: 18px;">
        <div class="copyr" style="text-align: center; font-size: 13px;">&copy;2016 Whatsnew Co.,Ltd. All Rights Reserved</div>
        <div class="links" style="text-align: center; font-size: 13px; margin-top: 5px;">
            <a href="http://www.moxy.co.th/th/about-us/" style="color: #3c3d41;"><span>เกี่ยวกับเรา</span></a> |
            <a href="http://www.moxy.co.th/th/terms-and-conditions/" style="color: #3c3d41;"><span>ข้อกำหนดและเงื่อนไข</span></a> |
            <a href="http://www.moxy.co.th/th/privacy-policy/" style="color: #3c3d41;"><span>ความเป็นส่วนตัว</span></a>
        </div>
    </div>
    <div style="clear:both"></div>
    <!-- End Footer -->

    <hr style="margin: 40px auto; width: 95%;">

    <!--English Version-->
    <div class="mail-body" style="margin:50px auto; width: 95%;">
        <p>Dear {{var order.getCustomerName()}},</p>

        <br/>
        <p>Thank you for your purchasing at Moxy.</p>
        <p>Your order number is #{{var order.increment_id}}</p>
        <br>

        <p>We deliver in Bangkok in 1-3 working days.</p>
        <p>We deliver in order regions of Thailand in 3-5 working days.</p>
        <br>

        <p>Track the status of your order here : <a href="{{store url="customer/account/"}}" style="color:#28438c; text-decoration:none;">{{store url="customer/account/"}}</a></p>
        <p> You can also reach our customer service at +662-106-8222</p>
        <br>

        <p>Warm Regards,</p>
        <p>Moxy Team</p>
    </div>

    <!--Billing-->
    <div class="order-detail" style="width: 95%; margin:0 auto; font-size: 12px;">
        <div class="billing-info" style="float: left; width: 49%;">
            <div class="heading" style="background: #ff6c6c; font-size: 12; color: #fff; padding: 3px 0px; float: left; width: 100%;"><div style="margin: 0px 5px; text-transform: uppercase;">Billing Information</div></div>
            <div class="billing-content" style="float: left; width: 100%; background: #e7e7e7; min-height: 175px;">
                <div style="margin: 0px 5px;">
                    {{var order.getBillingAddress().format('html')}}
                </div>
            </div>
        </div>

        <div class="payment-method" style="float: right; width: 49%; font-size: 12px;">
            <div class="heading" style="background: #ff6c6c; font-size: 12; color: #fff; padding: 3px 0px; float: left; width: 100%;"><div style="margin: 0px 5px; text-transform: uppercase;">Payment Method</div></div>
            <div class="billing-content" style="float: left; width: 100%; background: #e7e7e7; min-height: 175px;">
                <div style="margin: 0px 5px;">
                    {{var payment_html}}
                </div>
            </div>
        </div>

        <div style="clear: both;"></div>
    </div>
    {{depend order.getIsNotVirtual()}}
    <!--Shipping-->
    <div class="order-detail" style="width: 95%; margin:15px auto; font-size: 12px;">
        <div class="billing-info" style="float: left; width: 49%;">
            <div class="heading" style="background: #ff6c6c; font-size: 12; color: #fff; padding: 3px 0px; float: left; width: 100%;"><div style="margin: 0px 5px; text-transform: uppercase;">Shipping Information</div></div>
            <div class="billing-content" style="float: left; width: 100%; background: #e7e7e7; min-height: 175px;">
                <div style="margin: 0px 5px;">
                    {{var order.getShippingAddress().format('html')}}
                </div>
            </div>
        </div>

        <div class="payment-method" style="float: right; width: 49%; font-size: 12px;">
            <div class="heading" style="background: #ff6c6c; font-size: 12; color: #fff; padding: 3px 0px; float: left; width: 100%;"><div style="margin: 0px 5px; text-transform: uppercase;">Shipping Method</div></div>
            <div class="billing-content" style="float: left; width: 100%; background: #e7e7e7; min-height: 175px;">
                <div style="margin: 0px 5px;">
                    {{var order.getShippingDescription()}}
                </div>
            </div>
        </div>

        <div style="clear: both;"></div>
    </div>
    {{/depend}}
    <!--Order Item-->
    {{layout handle="sales_email_order_items" order=\$order language="en"}}
    <p style="font-size:12px; margin:0 0 10px 0">{{var order.getEmailCustomerNote()}}</p>
    <!--/End OrderItem-->
    <div class="banner" style="max-height: 120px; position: relative; margin:45px auto;">
        {{block type="cms/block" block_id="email_banner_en" }}
    </div>

    <!-- Footer -->
    <div class="navigation-th" style="margin-top: 24px; overflow:hidden;">
        <div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #ff6c6c; border-bottom: 2px solid #ff6c6c; text-align:center;">
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding-right: 5px;"><a href="http://www.moxy.co.th/th/beauty.html" style="color: #3c3d41; text-decoration: none;">Beauty</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/fashion.html" style="color: #3c3d41; text-decoration: none;">Fashion</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/home-living.html" style="color: #3c3d41; text-decoration: none;">Home Decor</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/home-appliances.html" style="color: #3c3d41; text-decoration: none;">Home Appliances</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/electronics.html" style="color: #3c3d41; text-decoration: none;">Electronics</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/baby.html" style="color: #3c3d41; text-decoration: none;">Baby & Mom</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/health.html" style="color: #3c3d41; text-decoration: none;">Health & Sports</a></span>
            <span class="nav-item" style=" line-height: 33px; font-size: 12px; padding-left: 5px;"><a href="http://www.moxy.co.th/th/pets.html" style="color: #3c3d41; text-decoration: none;">Pets</a></span>
        </div>
    </div>

    <div class="email-footer" style="min-height: 85px; max-heigh: 85px; background: #E7E7E7; margin-top: 8px; color: #3c3d41;padding: 0 15px 5px 15px;">
        <div class="footer-col" style="float:left; padding: 17px 1% 17px 2%; width: 27%; color: #3c3d41;">
            <p style="font-size: 13px; margin: 0px 0px 0px"><img src="{{skin _area='frontend'  url='images/email/mail.png'}}" style="width:20px; float:left; margin-right:5px; margin-left: -2px;"/> Contact Us</p><div style="clear: both;"></div>
            <p style="font-size: 10px; margin: 0;">02-106-8222</p>
            <p style="margin: 0; font-size: 10px;"><a href="mailto:support@moxy.co.th" style="color: #3C3D41; text-decoration: none;">support@moxy.co.th</a></p>
        </div>

        <div class="footer-col" style="float:left; padding: 17px 1% 17px 0; width: 32%;">
            <p style="font-size: 13px; margin: 0px 0px 0px">Follow Us</p>
            <div class="follow-icons" style="width: 100%">
                <a href="https://www.facebook.com/moxyst" style="float: left; width: 16%; padding-right: 2%;"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-fb.png"}}" alt="Facebook fanpage"></a>
                <a href="https://twitter.com/moxyst" style="float: left; width: 16%; padding-right: 2%;"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-twitter.png"}}" alt="Twitter fanpage"></a>
                <a href="https://www.instagram.com/moxy_th/" style="float: left; width: 16%; padding-right: 2%;"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-instagram.png"}}" alt="Instagram fanpage"></a>
                <a href="https://plus.google.com/+Moxyst" style="float: left; width: 16%; padding-right: 2%;"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-googleplus.png"}}" alt="Google fanpage"></a>
                <a href="https://www.pinterest.com/MoxySEA1/" style="float: left; width: 16%; padding-right: 2%;"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-pinterest.png"}}" alt="Pinterest fanpage"></a>
            </div>
        </div>

        <div class="footer-col" style="float:left; padding: 17px 0 17px 3%; width: 34%;">
            <p style="font-size: 13px;margin: 0px 0px 0px">Payment Options</p>
            <img src="{{skin _area="frontend"  url="images/email/payment_new.png"}}" alt="Payment Options" style="margin-top: 3px;">
            <div class="cod" style="font-size: 10px; margin-top: -3px;">
                <img src="{{skin _area='frontend'  url='images/email/COD.png'}}" style="float: left; margin-right: 2px;">
                <p style="float: left; margin: 3px 0 0; font-size: 10px;"> Cash on Delivery</p>
            </div>
        </div>
    </div>

    <div class="footer" style="padding: 0; margin-top: 18px;">
        <div class="copyr" style="text-align: center; font-size: 13px;">&copy;2016 Whatsnew Co.,Ltd. All Rights Reserved</div>
        <div class="links" style="text-align: center; font-size: 13px; margin-top: 5px;">
            <a href="http://www.moxy.co.th/en/about-us/" style="color: #3c3d41;"><span>About Us</span></a> |
            <a href="http://www.moxy.co.th/en/terms-and-conditions/" style="color: #3c3d41;"><span>Term & Conditions</span></a> |
            <a href="http://www.moxy.co.th/en/privacy-policy/" style="color: #3c3d41;"><span>Privacy Policy</span></a>
        </div>
    </div>
    <div style="clear:both"></div>
    <!-- End Footer -->
</div>
HTML;
$orderConfirmation->setTemplateText($mailContent);
$orderConfirmation->save();

/** Order Cancellation Mail Template Moxy */
/** @var Mage_Adminhtml_Model_Email_Template $mailTemplateModel */
$mailTemplateModel = Mage::getModel('adminhtml/email_template');

$mailTemplateModel->loadByCode('Order Cancel Moxy');
$mailTemplateModel->setTemplateCode('Order Cancel Moxy');
$mailTemplateModel->setTemplateSubject('{{var store.getFrontendName()}}: Your order #{{var order.increment_id}} has been canceled');
$mailTemplateModel->setTemplateType(2);
$mailContent = <<<HTML
<meta charset="utf-8"/>
<div class="mail-container" style="width:600px; min-width:600px; padding:0px 33px; font-family:  'Trebuchet MS', Tahoma; font-size: 14px; color: #3c3d41; margin: 0px;">
    <div class="mail-header" style="width:95%; min-height: 110px; max-height: 110px; position: relative; margin:0px auto;">
        <div class="mail-logo" style="width:220px; min-height: 110px; max-height: 110px; margin:0px auto; padding-left:30px; text-align:center">
            <a href="http://moxy.co.th/"><img src="{{skin _area="frontend"  url="images/email/moxy-orami-logo.png"}}" style="width:220px; max-height: 110px; min-height: 110px;" /></a>
        </div>

        <div class="english-below" style="min-height: 16px; max-height: 16px; position: absolute; bottom: 0px; line-height: 16px;">
            <span><img src="{{skin _area="frontend"  url="images/email/flag-en.png"}}" height="16px;"/> Scroll down for English version</span>
        </div>
    </div>

    <div class="header-image" style="width: 100%; height: auto; position: relative; margin:30px auto; text-align: center">
        <img src="{{skin _area="frontend"  url="images/email/order_cancellation_moxy.png"}}" style="width:100%; height: auto;" alt="Order has been cancelled" />
    </div>

    <div class="mail-body" style="margin:50px auto; width: 95%;">
        <p>เรียน คุณ {{var order.getCustomerName()}},</p>
        <br/>
        <p>คุณได้ยกเลิกรายการเลขสั่งซื้อ #{{var order.increment_id}} เรียบร้อยแล้ว.</p>
        <p>ขอบคุณค่ะ</p>
        <br/>
        <p>Warm Regards,</p>
        <p>Moxy Team</p>
    </div>

    <!-- Footer -->
    <div class="navigation-th" style="margin-top: 24px; overflow:hidden;">
        <div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #ff6c6c; border-bottom: 2px solid #ff6c6c; text-align:center;">
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding-right: 5px;"><a href="http://www.moxy.co.th/th/beauty.html" style="color: #3c3d41; text-decoration: none;">ความงาม</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/fashion.html" style="color: #3c3d41; text-decoration: none;">แฟชั่น</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/home-living.html" style="color: #3c3d41; text-decoration: none;">ของแต่งบ้าน</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/home-appliances.html" style="color: #3c3d41; text-decoration: none;">เครื่องใช้ไฟฟ้าภายในบ้าน</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/electronics.html" style="color: #3c3d41; text-decoration: none;">อิเล็กทรอนิกส์</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/baby.html" style="color: #3c3d41; text-decoration: none;">แม่และเด็ก</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/health.html" style="color: #3c3d41; text-decoration: none;">สุขภาพ</a></span>
            <span class="nav-item" style=" line-height: 33px; font-size: 12px; padding-left: 5px;"><a href="http://www.moxy.co.th/th/pets.html" style="color: #3c3d41; text-decoration: none;">สัตว์เลี้ยง</a></span>
        </div>
    </div>

    <div class="email-footer" style="min-height: 85px; max-heigh: 85px; background: #E7E7E7; margin-top: 8px; color: #3c3d41; padding: 0 15px 5px 15px;">
        <div class="footer-col" style="float:left; padding: 17px 1% 17px 2%; width: 27%; color: #3c3d41;">
            <p style="font-size: 13px; margin: 0px 0px 0px"><img src="{{skin _area='frontend'  url='images/email/mail.png'}}" style="width:20px; float:left; margin-right:5px; margin-left:-2px;"/> ติดต่อเรา</p><div style="clear: both;"></div>
            <p style="font-size: 10px; margin: 0;">02-106-8222</p>
            <p style="margin: 0; font-size: 10px;"><a href="mailto:support@moxy.co.th" style="color: #3C3D41; text-decoration: none;">support@moxy.co.th</a></p>
        </div>

        <div class="footer-col" style="float:left; padding: 17px 1% 17px 0; width: 32%;">
            <p style="font-size: 13px; margin: 0px 0px 0px">ติดตามที่</p>
            <div class="follow-icons" style="width: 100%">
                <a href="https://www.facebook.com/moxyst" style="float: left; width: 16%; padding-right: 2%;"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-fb.png"}}" alt="Facebook fanpage"></a>
                <a href="https://twitter.com/moxyst" style="float: left; width: 16%; padding-right: 2%;"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-twitter.png"}}" alt="Twitter fanpage"></a>
                <a href="https://www.instagram.com/moxy_th/" style="float: left; width: 16%; padding-right: 2%;"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-instagram.png"}}" alt="Instagram fanpage"></a>
                <a href="https://plus.google.com/+Moxyst" style="float: left; width: 16%; padding-right: 2%;"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-googleplus.png"}}" alt="Google fanpage"></a>
                <a href="https://www.pinterest.com/MoxySEA1/" style="float: left; width: 16%; padding-right: 2%;"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-pinterest.png"}}" alt="Pinterest fanpage"></a>
            </div>
        </div>

        <div class="footer-col" style="float:left; padding: 17px 0 17px 3%; width: 34%;">
            <p style="font-size: 13px;margin: 0px 0px 0px">ช่องทางการชำระเงิน</p>
            <img src="{{skin _area="frontend"  url="images/email/payment_new.png"}}" alt="ช่องทางการชำระเงิน" style="margin-top: 3px;">
            <div class="cod" style="font-size: 10px; margin-top: -3px;">
                <img src="{{skin _area='frontend'  url='images/email/COD.png'}}" style="float: left; margin-right: 2px;">
                <p style="float: left; margin: 3px 0 0; font-size: 10px;"> ช่องทางการชำระเงิน</p>
            </div>
        </div>
    </div>

    <div class="footer" style="padding: 0px 33px; margin-top: 18px;">
        <div class="copyr" style="text-align: center; font-size: 12px;">&copy;2016 Whatsnew Co.,Ltd. All Rights Reserved</div>
        <div class="links" style="text-align: center; font-size: 12px; margin-top: 5px;">
            <a href="http://www.moxy.co.th/th/about-us/" style="color: #3c3d41;"><span>เกี่ยวกับเรา</span></a> |
            <a href="http://www.moxy.co.th/th/terms-and-conditions/" style="color: #3c3d41;"><span>ข้อกำหนดและเงื่อนไข</span></a> |
            <a href="http://www.moxy.co.th/th/privacy-policy/" style="color: #3c3d41;"><span>ความเป็นส่วนตัว</span></a>
        </div>
    </div>
    <div style="clear:both"></div>
    <!-- End Footer -->

    <hr style="margin: 40px auto; width: 95%;">

    <!--English Version-->
    <div class="mail-body" style="margin:50px auto; width: 95%;">
        <p>Dear {{var order.getCustomerName()}},</p>

        <br/>
        <p>Your order #{{var order.increment_id}} has been successfully cancelled.</p>
        <p>We hope you enjoyed shopping with us.</p>
        <br>

        <p>Warm Regards,</p>
        <p>Moxy Team</p>
    </div>

    <!-- Footer -->
    <div class="navigation-th" style="margin-top: 24px; overflow:hidden;">
        <div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #ff6c6c; border-bottom: 2px solid #ff6c6c; text-align:center;">
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding-right: 5px;"><a href="http://www.moxy.co.th/th/beauty.html" style="color: #3c3d41; text-decoration: none;">Beauty</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/fashion.html" style="color: #3c3d41; text-decoration: none;">Fashion</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/home-living.html" style="color: #3c3d41; text-decoration: none;">Home Decor</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/home-appliances.html" style="color: #3c3d41; text-decoration: none;">Home Appliances</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/electronics.html" style="color: #3c3d41; text-decoration: none;">Electronics</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/baby.html" style="color: #3c3d41; text-decoration: none;">Baby & Mom</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/health.html" style="color: #3c3d41; text-decoration: none;">Health & Sports</a></span>
            <span class="nav-item" style=" line-height: 33px; font-size: 12px; padding-left: 5px;"><a href="http://www.moxy.co.th/th/pets.html" style="color: #3c3d41; text-decoration: none;">Pets</a></span>
        </div>
    </div>

    <div class="email-footer" style="min-height: 85px; max-heigh: 85px; background: #E7E7E7; margin-top: 8px; color: #3c3d41;padding: 0 15px 5px 15px;">
        <div class="footer-col" style="float:left; padding: 17px 1% 17px 2%; width: 27%; color: #3c3d41;">
            <p style="font-size: 13px; margin: 0px 0px 0px"><img src="{{skin _area='frontend'  url='images/email/mail.png'}}" style="width:20px; float:left; margin-right:5px; margin-left: -2px;"/> Contact Us</p><div style="clear: both;"></div>
            <p style="font-size: 10px; margin: 0;">02-106-8222</p>
            <p style="margin: 0; font-size: 10px;"><a href="mailto:support@moxy.co.th" style="color: #3C3D41; text-decoration: none;">support@moxy.co.th</a></p>
        </div>

        <div class="footer-col" style="float:left; padding: 17px 1% 17px 0; width: 32%;">
            <p style="font-size: 13px; margin: 0px 0px 0px">Follow Us</p>
            <div class="follow-icons" style="width: 100%">
                <a href="https://www.facebook.com/moxyst" style="float: left; width: 16%; padding-right: 2%;"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-fb.png"}}" alt="Facebook fanpage"></a>
                <a href="https://twitter.com/moxyst" style="float: left; width: 16%; padding-right: 2%;"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-twitter.png"}}" alt="Twitter fanpage"></a>
                <a href="https://www.instagram.com/moxy_th/" style="float: left; width: 16%; padding-right: 2%;"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-instagram.png"}}" alt="Instagram fanpage"></a>
                <a href="https://plus.google.com/+Moxyst" style="float: left; width: 16%; padding-right: 2%;"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-googleplus.png"}}" alt="Google fanpage"></a>
                <a href="https://www.pinterest.com/MoxySEA1/" style="float: left; width: 16%; padding-right: 2%;"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-pinterest.png"}}" alt="Pinterest fanpage"></a>
            </div>
        </div>

        <div class="footer-col" style="float:left; padding: 17px 0 17px 3%; width: 34%;">
            <p style="font-size: 13px;margin: 0px 0px 0px">Payment Options</p>
            <img src="{{skin _area="frontend"  url="images/email/payment_new.png"}}" alt="Payment Options" style="margin-top: 3px;">
            <div class="cod" style="font-size: 10px; margin-top: -3px;">
                <img src="{{skin _area='frontend'  url='images/email/COD.png'}}" style="float: left; margin-right: 2px;">
                <p style="float: left; margin: 3px 0 0; font-size: 10px;"> Cash on Delivery</p>
            </div>
        </div>
    </div>

    <div class="footer" style="padding: 0px 33px; margin-top: 18px;">
        <div class="copyr" style="text-align: center; font-size: 12px;">&copy;2016 Whatsnew Co.,Ltd. All Rights Reserved</div>
        <div class="links" style="text-align: center; font-size: 12px; margin-top: 5px;">
            <a href="http://www.moxy.co.th/en/about-us/" style="color: #3c3d41;"><span>About Us</span></a> |
            <a href="http://www.moxy.co.th/en/terms-and-conditions/" style="color: #3c3d41;"><span>Term & Conditions</span></a> |
            <a href="http://www.moxy.co.th/en/privacy-policy/" style="color: #3c3d41;"><span>Privacy Policy</span></a>
        </div>
    </div>
    <div style="clear:both"></div>
    <!-- End Footer -->
</div>
HTML;

$mailTemplateModel->setTemplateText($mailContent);
$mailTemplateModel->save();

/** Cart Reminder Mail Template Moxy */
/** @var Mage_Adminhtml_Model_Email_Template $cartReminder */
$cartReminder = Mage::getModel('adminhtml/email_template');
$cartReminder->loadByCode('Cart Reminder Moxy');
$mailContent = <<<HTML
<meta charset="utf-8"/>
<div class="mail-container" style="width:600px; min-width:600px; padding:0px 33px; font-family:  'Trebuchet MS', Tahoma; font-size: 14px; color: #3c3d41; margin: 0px;">
    <div class="mail-header" style="width:95%; min-height: 110px; max-height: 110px; position: relative; margin:0px auto;">
        <div class="mail-logo" style="width:220px; min-height: 110px; max-height: 110px; margin:0px auto; margin:0px auto;">
            <a href="http://moxy.co.th/"><img src="{{skin _area="frontend"  url="images/email/moxy-orami-logo.png"}}" style="width:220px; max-height: 110px; min-height: 110px;" /></a>
        </div>

        <div class="english-below" style="min-height: 16px; max-height: 16px; position: absolute; bottom: 0px; line-height: 16px;">
            <span><img src="{{skin _area="frontend"  url="images/email/flag-en.png"}}" height="16px;"/> Scroll down for English version</span>
        </div>
    </div>

    <div class="header-image" style="width: 100%; height: auto; position: relative; margin:0px auto; margin-top:40px; text-align: center">
        <img src="{{skin _area="frontend"  url="images/email/feel_like_shopping_today_thai.png"}}" style="width:100%; height:auto;" alt="Cart Reminder" />
    </div>

    <div class="mail-body" style="margin:50px auto; width: 95%;">
        <div class="left-main-body" style="float: left; width: 57%; padding: 0px 5px; border-right: 1px solid #e7e7e7;">
            <p>เรียน คุณ {{var customer_name}},</p>
            <br/>

            <p>มีสินค้าบางอย่างหลงเหลืออยู่ในตะกร้าสินค้าของคุณ</p>
            <p>คุณต้องการจะสั่งซื้อวันนี้ไหม ?</p>
            <br>

            <p>คุณสามารถสั่งซื้อตอนนี้และชำระเงินในภายหลัง เพื่อความสะดวกสบายที่ยิ่งขึ้น</p>
            <p>เลือกดูวิธีการชำระเงินอื่นๆได้<a href="http://www.moxy.co.th/th/help/#payments" style="color: #3c3d41;">ที่น</a></p>
            <br>

            <p>Have a beautiful day!</p>
            <br>

            <p>Warm Regards,</p>
            <p>Moxy Team</p>
        </div>

        <div class="right-main-body" style="width: 38%; float: right; padding:0px 5px; text-align: center;">
            {{var products}}
            <a href="{{store url="checkout/cart/"}}" style="display: block; width: 50%; color: #fff; text-decoration: none; margin:10px auto; background: #ff6c6c;padding: 10px;clear:both;">สั่งซื้อ</a>
            <p>สั่งซื้อตอนนี้ก่อนที่ของจะหมด</p>
        </div>

        <div style="clear: both;"></div>
    </div>

    <!-- Footer -->
    <div class="navigation-th" style="margin-top: 24px; overflow:hidden;">
        <div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #ff6c6c; border-bottom: 2px solid #ff6c6c; text-align:center;">
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding-right: 5px;"><a href="http://www.moxy.co.th/th/beauty.html" style="color: #3c3d41; text-decoration: none;">ความงาม</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/fashion.html" style="color: #3c3d41; text-decoration: none;">แฟชั่น</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/home-living.html" style="color: #3c3d41; text-decoration: none;">ของแต่งบ้าน</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/home-appliances.html" style="color: #3c3d41; text-decoration: none;">เครื่องใช้ไฟฟ้าภายในบ้าน</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/electronics.html" style="color: #3c3d41; text-decoration: none;">อิเล็กทรอนิกส์</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/baby.html" style="color: #3c3d41; text-decoration: none;">แม่และเด็ก</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/health.html" style="color: #3c3d41; text-decoration: none;">สุขภาพ</a></span>
            <span class="nav-item" style=" line-height: 33px; font-size: 12px; padding-left: 5px;"><a href="http://www.moxy.co.th/th/pets.html" style="color: #3c3d41; text-decoration: none;">สัตว์เลี้ยง</a></span>
        </div>
    </div>

    <div class="email-footer" style="min-height: 85px; max-heigh: 85px; background: #E7E7E7; margin-top: 8px; color: #3c3d41; padding: 0 15px 5px 15px;">
        <div class="footer-col" style="float:left; padding: 17px 1% 17px 2%; width: 27%; color: #3c3d41;">
            <p style="font-size: 13px; margin: 0px 0px 0px"><img src="{{skin _area='frontend'  url='images/email/mail.png'}}" style="width:20px; float:left; margin-right:5px; margin-left:-2px;"/> ติดต่อเรา</p><div style="clear: both;"></div>
            <p style="font-size: 10px; margin: 0;">02-106-8222</p>
            <p style="margin: 0; font-size: 10px;"><a href="mailto:support@moxy.co.th" style="color: #3C3D41; text-decoration: none;">support@moxy.co.th</a></p>
        </div>

        <div class="footer-col" style="float:left; padding: 17px 1% 17px 0; width: 32%;">
            <p style="font-size: 13px; margin: 0px 0px 0px">ติดตามที่</p>
            <div class="follow-icons" style="width: 100%">
                <a href="https://www.facebook.com/moxyst" style="float: left; width: 16%; padding-right: 2%;"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-fb.png"}}" alt="Facebook fanpage"></a>
                <a href="https://twitter.com/moxyst" style="float: left; width: 16%; padding-right: 2%;"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-twitter.png"}}" alt="Twitter fanpage"></a>
                <a href="https://www.instagram.com/moxy_th/" style="float: left; width: 16%; padding-right: 2%;"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-instagram.png"}}" alt="Instagram fanpage"></a>
                <a href="https://plus.google.com/+Moxyst" style="float: left; width: 16%; padding-right: 2%;"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-googleplus.png"}}" alt="Google fanpage"></a>
                <a href="https://www.pinterest.com/MoxySEA1/" style="float: left; width: 16%; padding-right: 2%;"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-pinterest.png"}}" alt="Pinterest fanpage"></a>
            </div>
        </div>

        <div class="footer-col" style="float:left; padding: 17px 0 17px 3%; width: 34%;">
            <p style="font-size: 13px;margin: 0px 0px 0px">ช่องทางการชำระเงิน</p>
            <img src="{{skin _area="frontend"  url="images/email/payment_new.png"}}" alt="ช่องทางการชำระเงิน" style="margin-top: 3px;">
            <div class="cod" style="font-size: 10px; margin-top: -3px;">
                <img src="{{skin _area='frontend'  url='images/email/COD.png'}}" style="float: left; margin-right: 2px;">
                <p style="float: left; margin: 3px 0 0; font-size: 10px;"> ช่องทางการชำระเงิน</p>
            </div>
        </div>
    </div>

    <div class="footer" style="padding: 0; margin-top: 18px;">
        <div class="copyr" style="text-align: center; font-size: 13px;">&copy;2016 Whatsnew Co.,Ltd. All Rights Reserved</div>
        <div class="links" style="text-align: center; font-size: 13px; margin-top: 5px;">
            <a href="http://www.moxy.co.th/th/about-us/" style="color: #3c3d41;"><span>เกี่ยวกับเรา</span></a> |
            <a href="http://www.moxy.co.th/th/terms-and-conditions/" style="color: #3c3d41;"><span>ข้อกำหนดและเงื่อนไข</span></a> |
            <a href="http://www.moxy.co.th/th/privacy-policy/" style="color: #3c3d41;"><span>ความเป็นส่วนตัว</span></a>
        </div>
    </div>
    <div style="clear:both"></div>
    <!-- End Footer -->

    <hr style="margin: 40px auto; width: 95%;">

    <!--English Version-->
    <div class="header-image" style="width: 100%; height: auto; position: relative; margin:0px auto; margin-top:40px; text-align: center">
        <img src="{{skin _area="frontend"  url="images/email/feel_like_shopping_today_en.png"}}" style="width:100%; height:auto;" alt="Cart Reminder" />
    </div>

    <div class="mail-body" style="margin:50px auto; width: 95%;">
        <div class="left-main-body" style="float: left; width: 57%; padding: 0px 5px; border-right: 1px solid #e7e7e7;">
            <p>Dear {{var customer_name}},</p>

            <br/>
            <p>Looks like you have something left in your shopping cart. Do you feel like purchasing it today? </p>
            <br>

            <p>For your convenience, You can shop now and pay later with cash-on-delivery. </p>
            <p>Check our conditions of payment <a href="http://www.moxy.co.th/en/help/#payments" style="color: #3c3d41;">here</a>.</p>
            <br>

            <p>Have a beautiful day!</p>
            <br>

            <p>Warm Regards,</p>
            <p>Moxy Team</p>
        </div>
        <div class="right-main-body" style="width: 38%; float: right; padding:0px 5px; text-align: center;">
            {{var products}}
            <a href="{{store url="checkout/cart/"}}"
            style="display: block; width: 50%; color: #fff; text-decoration: none; margin:10px auto; background: #ff6c6c;padding: 10px;clear:both;">BUY IT NOW</a>
            <p>Buy now before stock last</p>
        </div>

        <div style="clear: both;"></div>
    </div>

    <!-- Footer -->
    <div class="navigation-th" style="margin-top: 24px; overflow:hidden;">
        <div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #ff6c6c; border-bottom: 2px solid #ff6c6c; text-align:center;">
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding-right: 5px;"><a href="http://www.moxy.co.th/th/beauty.html" style="color: #3c3d41; text-decoration: none;">Beauty</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/fashion.html" style="color: #3c3d41; text-decoration: none;">Fashion</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/home-living.html" style="color: #3c3d41; text-decoration: none;">Home Decor</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/home-appliances.html" style="color: #3c3d41; text-decoration: none;">Home Appliances</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/electronics.html" style="color: #3c3d41; text-decoration: none;">Electronics</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/baby.html" style="color: #3c3d41; text-decoration: none;">Baby & Mom</a></span>
            <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a href="http://www.moxy.co.th/th/health.html" style="color: #3c3d41; text-decoration: none;">Health & Sports</a></span>
            <span class="nav-item" style=" line-height: 33px; font-size: 12px; padding-left: 5px;"><a href="http://www.moxy.co.th/th/pets.html" style="color: #3c3d41; text-decoration: none;">Pets</a></span>
        </div>
    </div>

    <div class="email-footer" style="min-height: 85px; max-heigh: 85px; background: #E7E7E7; margin-top: 8px; color: #3c3d41;padding: 0 15px 5px 15px;">
        <div class="footer-col" style="float:left; padding: 17px 1% 17px 2%; width: 27%; color: #3c3d41;">
            <p style="font-size: 13px; margin: 0px 0px 0px"><img src="{{skin _area='frontend'  url='images/email/mail.png'}}" style="width:20px; float:left; margin-right:5px; margin-left: -2px;"/> Contact Us</p><div style="clear: both;"></div>
            <p style="font-size: 10px; margin: 0;">02-106-8222</p>
            <p style="margin: 0; font-size: 10px;"><a href="mailto:support@moxy.co.th" style="color: #3C3D41; text-decoration: none;">support@moxy.co.th</a></p>
        </div>

        <div class="footer-col" style="float:left; padding: 17px 1% 17px 0; width: 32%;">
            <p style="font-size: 13px; margin: 0px 0px 0px">Follow Us</p>
            <div class="follow-icons" style="width: 100%">
                <a href="https://www.facebook.com/moxyst" style="float: left; width: 16%; padding-right: 2%;"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-fb.png"}}" alt="Facebook fanpage"></a>
                <a href="https://twitter.com/moxyst" style="float: left; width: 16%; padding-right: 2%;"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-twitter.png"}}" alt="Twitter fanpage"></a>
                <a href="https://www.instagram.com/moxy_th/" style="float: left; width: 16%; padding-right: 2%;"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-instagram.png"}}" alt="Instagram fanpage"></a>
                <a href="https://plus.google.com/+Moxyst" style="float: left; width: 16%; padding-right: 2%;"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-googleplus.png"}}" alt="Google fanpage"></a>
                <a href="https://www.pinterest.com/MoxySEA1/" style="float: left; width: 16%; padding-right: 2%;"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-pinterest.png"}}" alt="Pinterest fanpage"></a>
            </div>
        </div>

        <div class="footer-col" style="float:left; padding: 17px 0 17px 3%; width: 34%;">
            <p style="font-size: 13px;margin: 0px 0px 0px">Payment Options</p>
            <img src="{{skin _area="frontend"  url="images/email/payment_new.png"}}" alt="Payment Options" style="margin-top: 3px;">
            <div class="cod" style="font-size: 10px; margin-top: -3px;">
                <img src="{{skin _area='frontend'  url='images/email/COD.png'}}" style="float: left; margin-right: 2px;">
                <p style="float: left; margin: 3px 0 0; font-size: 10px;"> Cash on Delivery</p>
            </div>
        </div>
    </div>

    <div class="footer" style="padding: 0; margin-top: 18px;">
        <div class="copyr" style="text-align: center; font-size: 13px;">&copy;2016 Whatsnew Co.,Ltd. All Rights Reserved</div>
        <div class="links" style="text-align: center; font-size: 13px; margin-top: 5px;">
            <a href="http://www.moxy.co.th/en/about-us/" style="color: #3c3d41;"><span>About Us</span></a> |
            <a href="http://www.moxy.co.th/en/terms-and-conditions/" style="color: #3c3d41;"><span>Term & Conditions</span></a> |
            <a href="http://www.moxy.co.th/en/privacy-policy/" style="color: #3c3d41;"><span>Privacy Policy</span></a>
        </div>
    </div>
    <div style="clear:both"></div>
    <!-- End Footer -->
</div>
HTML;
$cartReminder->setTemplateText($mailContent);
$cartReminder->save();

/** How to order Mail Template Moxy */
/** @var Mage_Newsletter_Model_Template $howToOrder */
$howToOrder = Mage::getModel('newsletter/template');
$howToOrder->loadByCode('HOW TO ORDER MOXY');
$mailContent = <<<HTML
<meta charset="utf-8"/>
<div class="mail-container" style="width: 600px; min-width: 600px; padding: 0px 33px; font-family: 'Trebuchet MS', Tahoma; font-size: 14px; color: #3c3d41; margin: 0px;">
    <div class="mail-header" style="width: 95%; min-height: 110px; max-height: 110px; margin: 0px auto;">
        <div class="mail-logo" style="width: 220px; min-height: 110px; max-height: 110px; margin: 0px auto; text-align: center;"><a href="http://moxy.co.th/"><img style="width: 220px; max-height: 110px; min-height: 110px;" src="{{skin _area='frontend'  url='images/email/moxy-orami-logo.png'}}" alt="" /></a></div>
    </div>
    <div class="header-image" style="width: 600px; min-width: 600px; max-height: 360px; min-height: 360px; padding: 0; position: relative; margin: 50px auto; text-align: center;"><span><img style="width: 600px; max-height: 360px; min-height: 360px;" src="{{skin _area='frontend'  url='images/email/order_step_all.png'}}" alt="Order Steps All" /></span></div>
    <div class="tryit" style="width: 95%; text-align: center; margin: 40px auto; margin-top: 0; font-size: 24px; text-transform: uppercase;">
        <p>Try It Now!</p>
    </div>
    <div class="mail-body product-list" style="width: 100%; margin: 5px auto;">
        <div class="product-item" style="width: 32%; border: solid 1px #bbb; padding-bottom: 10px; margin: 5px 2px; float: left;">
            <div class="product-image"><img style="border: none;" src="{{skin _area="frontend"  url="images/email/kat-to.jpg"}}" alt="" width="100%" /></div>
            <div class="product-name" style="text-align: center; white-space: nowrap; width: 95%; overflow: hidden; text-overflow: ellipsis; margin: 5px auto;"><a style="color: #3c3d41; text-decoration: none;" href="http://www.moxy.co.th/th/pets/kat-to-cat-litter-apple-10lt.html">KAT-TO Cat Litter Apple (10lt)</a></div>
            <div class="product-more" style="margin-top: 10px; padding: 0px 5px;">
                <div class="product-price" style="float: left;">Price <strong>฿139</strong></div>
                <div class="product-discount" style="color: #ff6c6c; float: right;">-23%</div>
                <div style="clear: both;">&nbsp;</div>
            </div>
        </div>
        <div class="product-item" style="width: 32%; border: solid 1px #bbb; padding-bottom: 10px; margin: 5px 2px; float: left;">
            <div class="product-image"><img style="border: none;" src="{{skin _area="frontend"  url="images/email/facial-treatment-essence.jpg"}}" alt="" width="100%" /></div>
            <div class="product-name" style="text-align: center; white-space: nowrap; width: 95%; overflow: hidden; text-overflow: ellipsis; margin: 5px auto;"><a style="color: #3c3d41; text-decoration: none;" href="http://www.moxy.co.th/en/beauty/best-sellers/sk-ii-facial-treatment-essence-30ml.html">SK-II Facial Treatment Essence 30ml</a></div>
            <div class="product-more" style="margin-top: 10px; padding: 0px 5px;">
                <div class="product-price" style="float: left;">Price <strong>฿549</strong></div>
                <div class="product-discount" style="color: #ff6c6c; float: right;">-44%</div>
                <div style="clear: both;">&nbsp;</div>
            </div>
        </div>
        <div class="product-item" style="width: 32%; border: solid 1px #bbb; padding-bottom: 10px; margin: 5px 2px; float: left;">
            <div class="product-image"><img style="border: none;" src="{{skin _area="frontend"  url="images/email/peachy-salmon.png"}}" alt="" width="100%" /></div>
            <div class="product-name" style="text-align: center; white-space: nowrap; width: 95%; overflow: hidden; text-overflow: ellipsis; margin: 5px auto;"><a style="color: #3c3d41; text-decoration: none;" href="http://www.moxy.co.th/en/deal/peachy-salmon-brown-rice-hot-pot-125g-x-7.html">Peachy Salmon+Brown rice hot pot (125g) x 7 ซอง</a></div>
            <div class="product-more" style="margin-top: 10px; padding: 0px 5px;">
                <div class="product-price" style="float: left;">Price <strong>฿429</strong></div>
                <div class="product-discount" style="color: #ff6c6c; float: right;">-10%</div>
                <div style="clear: both;">&nbsp;</div>
            </div>
        </div>
        <div style="clear: both;">&nbsp;</div>
    </div>
    <!-- Footer -->
    <div class="navigation-th" style="margin-top: 24px; overflow: hidden;">
        <div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #ff6c6c; border-bottom: 2px solid #ff6c6c; text-align: center;"><span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding-right: 5px;"><a style="color: #3c3d41; text-decoration: none;" href="http://www.moxy.co.th/th/beauty.html">ความงาม</a></span> <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a style="color: #3c3d41; text-decoration: none;" href="http://www.moxy.co.th/th/fashion.html">แฟชั่น</a></span> <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a style="color: #3c3d41; text-decoration: none;" href="http://www.moxy.co.th/th/home-living.html">ของแต่งบ้าน</a></span> <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a style="color: #3c3d41; text-decoration: none;" href="http://www.moxy.co.th/th/home-appliances.html">เครื่องใช้ไฟฟ้าภายในบ้าน</a></span> <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a style="color: #3c3d41; text-decoration: none;" href="http://www.moxy.co.th/th/electronics.html">อิเล็กทรอนิกส์</a></span> <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a style="color: #3c3d41; text-decoration: none;" href="http://www.moxy.co.th/th/baby.html">แม่และเด็ก</a></span> <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a style="color: #3c3d41; text-decoration: none;" href="http://www.moxy.co.th/th/health.html">สุขภาพ</a></span> <span class="nav-item" style="line-height: 33px; font-size: 12px; padding-left: 5px;"><a style="color: #3c3d41; text-decoration: none;" href="http://www.moxy.co.th/th/pets.html">สัตว์เลี้ยง</a></span></div>
    </div>
    <div class="email-footer" style="min-height: 85px; max-heigh: 85px; background: #E7E7E7; margin-top: 8px; color: #3c3d41; padding: 0 15px 5px 15px;">
        <div class="footer-col" style="float: left; padding: 17px 1% 17px 2%; width: 27%; color: #3c3d41;">
            <p style="font-size: 13px; margin: 0px 0px 0px;"><img style="width: 20px; float: left; margin-right: 5px; margin-left: -2px;" src="{{skin _area='frontend'  url='images/email/mail.png'}}" alt="" /> ติดต่อเรา</p>
            <div style="clear: both;">&nbsp;</div>
            <p style="font-size: 10px; margin: 0;">02-106-8222</p>
            <p style="margin: 0; font-size: 10px;"><a style="color: #3c3d41; text-decoration: none;" href="mailto:support@moxy.co.th">support@moxy.co.th</a></p>
        </div>
        <div class="footer-col" style="float: left; padding: 17px 1% 17px 0; width: 32%;">
            <p style="font-size: 13px; margin: 0px 0px 0px;">ติดตามที่</p>
            <div class="follow-icons" style="width: 100%;"><a style="float: left; width: 16%; padding-right: 2%;" href="https://www.facebook.com/moxyst"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-fb.png"}}" alt="Facebook fanpage" /></a> <a style="float: left; width: 16%; padding-right: 2%;" href="https://twitter.com/moxyst"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-twitter.png"}}" alt="Twitter fanpage" /></a> <a style="float: left; width: 16%; padding-right: 2%;" href="https://www.instagram.com/moxy_th/"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-instagram.png"}}" alt="Instagram fanpage" /></a> <a style="float: left; width: 16%; padding-right: 2%;" href="https://plus.google.com/+Moxyst"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-googleplus.png"}}" alt="Google fanpage" /></a> <a style="float: left; width: 16%; padding-right: 2%;" href="https://www.pinterest.com/MoxySEA1/"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-pinterest.png"}}" alt="Pinterest fanpage" /></a></div>
        </div>
        <div class="footer-col" style="float: left; padding: 17px 0 17px 3%; width: 34%;">
            <p style="font-size: 13px; margin: 0px 0px 0px;">ช่องทางการชำระเงิน</p>
            <img style="margin-top: 3px;" src="{{skin _area="frontend"  url="images/email/payment_new.png"}}" alt="ช่องทางการชำระเงิน" />
            <div class="cod" style="font-size: 10px; margin-top: -3px;"><img style="float: left; margin-right: 2px;" src="{{skin _area='frontend'  url='images/email/COD.png'}}" alt="" />
                <p style="float: left; margin: 3px 0 0; font-size: 10px;">ช่องทางการชำระเงิน</p>
            </div>
        </div>
    </div>
    <div class="footer" style="padding: 0; margin-top: 18px;">
        <div class="copyr" style="text-align: center; font-size: 13px;">&copy;2016 Whatsnew Co.,Ltd. All Rights Reserved</div>
        <div class="links" style="text-align: center; font-size: 13px; margin-top: 5px;"><a style="color: #3c3d41;" href="http://www.moxy.co.th/th/about-us/"><span>เกี่ยวกับเรา</span></a> | <a style="color: #3c3d41;" href="http://www.moxy.co.th/th/terms-and-conditions/"><span>ข้อกำหนดและเงื่อนไข</span></a> | <a style="color: #3c3d41;" href="http://www.moxy.co.th/th/privacy-policy/"><span>ความเป็นส่วนตัว</span></a></div>
    </div>
    <div style="clear: both;">&nbsp;</div>
    <!-- End Footer --></div>
HTML;
$howToOrder->setTemplateText($mailContent);
$howToOrder->save();

/** Customer Satisfaction Mail Template Moxy */
/** @var Mage_Newsletter_Model_Template $customerSatisfaction */
$customerSatisfaction = Mage::getModel('newsletter/template');
$customerSatisfaction->loadByCode('Customer Satisfaction Moxy');
$mailContent = <<<HTML
<meta charset="utf-8"/>
<div class="mail-container" style="width: 600px; min-width: 600px; padding: 0px 33px; font-family: 'Trebuchet MS', Tahoma; font-size: 14px; color: #3c3d41; margin: 0px;">
    <div class="mail-header" style="width: 95%; min-height: 110px; max-height: 110px; position: relative; margin: 0px auto;">
        <div class="mail-logo" style="width: 220px; min-height: 110px; max-height: 110px; margin: 0px auto; text-align: center;"><a href="http://moxy.co.th/"><img style="width: 220px; max-height: 110px; min-height: 110px;" src="{{skin _area="frontend"  url="images/email/moxy-orami-logo.png"}}" alt="" /></a></div>
        <div class="english-below" style="min-height: 16px; max-height: 16px; position: absolute; bottom: 0px; line-height: 16px;"><span><img src="{{skin _area="frontend"  url="images/email/flag-en.png"}}" alt="" height="16px;" /> Scroll down for English version</span></div>
    </div>
    <div class="header-image" style="width: 100%; height: auto; position: relative; margin: 40px auto; margin-bottom: 80px; text-align: center;"><img style="width: 100%; height: auto;" src="{{skin _area="frontend"  url="images/email/satisfaction-survey.png"}}" alt="Customer Satisfaction" /></div>
    <div class="mail-body" style="margin: 50px auto; width: 95%;">
        <p>เรียน คุณ {{var customer_name}},</p>
        <br />
        <p>ขอขอบคุณทุกท่านที่ใช้บริการกับ Moxy by Orami!</p>
        <p style="line-height: 1.8em;">เราหวังว่าคุณจะได้รับการบริการที่ดีและ มีโอกาสใช้บริการกับเราอีกในครั้งถัดไป ขอความกรุณาท่านที่ได้รับอีเมล์นี้ สละเวลาเพื่อตอบแบบสอบถามด้านล่างนี้ ใช้เวลาเพียง 2 นาที เราจะนำไปปรับปรุงและพัฒนาการบริการที่ดียิ่งขึ้น สำหรับ</p>
        <br />
        <p>คลิกเลือกเพื่อตอบแบบสำรวจ:</p>
        <p><a href="https://www.research.net/r/Q8QWD9L?SO={{var order_id}}">https://www.research.net/r/Q8QWD9L?SO={{var order_id}}</a></p>
        <br />
        <p>ขอบคุณค่ะ</p>
        <br />
        <p>Moxy Team</p>
    </div>
    <!-- Footer -->
    <div class="navigation-th" style="margin-top: 24px; overflow: hidden;">
        <div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #ff6c6c; border-bottom: 2px solid #ff6c6c; text-align: center;"><span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding-right: 5px;"><a style="color: #3c3d41; text-decoration: none;" href="http://www.moxy.co.th/th/beauty.html">ความงาม</a></span> <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a style="color: #3c3d41; text-decoration: none;" href="http://www.moxy.co.th/th/fashion.html">แฟชั่น</a></span> <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a style="color: #3c3d41; text-decoration: none;" href="http://www.moxy.co.th/th/home-living.html">ของแต่งบ้าน</a></span> <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a style="color: #3c3d41; text-decoration: none;" href="http://www.moxy.co.th/th/home-appliances.html">เครื่องใช้ไฟฟ้าภายในบ้าน</a></span> <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a style="color: #3c3d41; text-decoration: none;" href="http://www.moxy.co.th/th/electronics.html">อิเล็กทรอนิกส์</a></span> <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a style="color: #3c3d41; text-decoration: none;" href="http://www.moxy.co.th/th/baby.html">แม่และเด็ก</a></span> <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a style="color: #3c3d41; text-decoration: none;" href="http://www.moxy.co.th/th/health.html">สุขภาพ</a></span> <span class="nav-item" style="line-height: 33px; font-size: 12px; padding-left: 5px;"><a style="color: #3c3d41; text-decoration: none;" href="http://www.moxy.co.th/th/pets.html">สัตว์เลี้ยง</a></span></div>
    </div>
    <div class="email-footer" style="min-height: 85px; max-heigh: 85px; background: #E7E7E7; margin-top: 8px; color: #3c3d41; padding: 0 15px 5px 15px;">
        <div class="footer-col" style="float: left; padding: 17px 1% 17px 2%; width: 27%; color: #3c3d41;">
            <p style="font-size: 13px; margin: 0px 0px 0px;"><img style="width: 20px; float: left; margin-right: 5px; margin-left: -2px;" src="{{skin _area='frontend'  url='images/email/mail.png'}}" alt="" /> ติดต่อเรา</p>
            <div style="clear: both;">&nbsp;</div>
            <p style="font-size: 10px; margin: 0;">02-106-8222</p>
            <p style="margin: 0; font-size: 10px;"><a style="color: #3c3d41; text-decoration: none;" href="mailto:support@moxy.co.th">support@moxy.co.th</a></p>
        </div>
        <div class="footer-col" style="float: left; padding: 17px 1% 17px 0; width: 32%;">
            <p style="font-size: 13px; margin: 0px 0px 0px;">ติดตามที่</p>
            <div class="follow-icons" style="width: 100%;"><a style="float: left; width: 16%; padding-right: 2%;" href="https://www.facebook.com/moxyst"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-fb.png"}}" alt="Facebook fanpage" /></a> <a style="float: left; width: 16%; padding-right: 2%;" href="https://twitter.com/moxyst"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-twitter.png"}}" alt="Twitter fanpage" /></a> <a style="float: left; width: 16%; padding-right: 2%;" href="https://www.instagram.com/moxy_th/"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-instagram.png"}}" alt="Instagram fanpage" /></a> <a style="float: left; width: 16%; padding-right: 2%;" href="https://plus.google.com/+Moxyst"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-googleplus.png"}}" alt="Google fanpage" /></a> <a style="float: left; width: 16%; padding-right: 2%;" href="https://www.pinterest.com/MoxySEA1/"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-pinterest.png"}}" alt="Pinterest fanpage" /></a></div>
        </div>
        <div class="footer-col" style="float: left; padding: 17px 0 17px 3%; width: 34%;">
            <p style="font-size: 13px; margin: 0px 0px 0px;">ช่องทางการชำระเงิน</p>
            <img style="margin-top: 3px;" src="{{skin _area="frontend"  url="images/email/payment_new.png"}}" alt="ช่องทางการชำระเงิน" />
            <div class="cod" style="font-size: 10px; margin-top: -3px;"><img style="float: left; margin-right: 2px;" src="{{skin _area='frontend'  url='images/email/COD.png'}}" alt="" />
                <p style="float: left; margin: 3px 0 0; font-size: 10px;">ช่องทางการชำระเงิน</p>
            </div>
        </div>
    </div>
    <div class="footer" style="padding: 0; margin-top: 18px;">
        <div class="copyr" style="text-align: center; font-size: 13px;">&copy;2016 Whatsnew Co.,Ltd. All Rights Reserved</div>
        <div class="links" style="text-align: center; font-size: 13px; margin-top: 5px;"><a style="color: #3c3d41;" href="http://www.moxy.co.th/th/about-us/"><span>เกี่ยวกับเรา</span></a> | <a style="color: #3c3d41;" href="http://www.moxy.co.th/th/terms-and-conditions/"><span>ข้อกำหนดและเงื่อนไข</span></a> | <a style="color: #3c3d41;" href="http://www.moxy.co.th/th/privacy-policy/"><span>ความเป็นส่วนตัว</span></a></div>
    </div>
    <div style="clear: both;">&nbsp;</div>
    <!-- End Footer --><hr style="margin: 40px auto; width: 95%;" /><!--English Version-->
    <h3 style="folor: #3C3D41; text-align: center; font-size: 20px; font-weight: bold; letter-spacing: 4px;">TELL US HOW WE DID</h3>
    <div class="mail-body" style="margin: 50px auto; width: 95%;">
        <p>Dear {{var customer_name}},</p>
        <br />
        <p>Thank you for shopping with us! We hope it was a good experience for you.</p>
        <br />
        <p>Help us provide the best service by answering a few quick questions about your experience with us. The Survey will only take 2 minutes and each answer helps us to grow and develop better for you.</p>
        <br />
        <p>Click here to do the survey:</p>
        <p><a href="https://www.research.net/r/Q8QWD9L?SO={{var order_id}}">https://www.research.net/r/Q8QWD9L?SO={{var order_id}}</a></p>
        <br />
        <p>Thank you and Warm Regards,</p>
        <p>Moxy Team</p>
    </div>
    <!-- Footer -->
    <div class="navigation-th" style="margin-top: 24px; overflow: hidden;">
        <div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #ff6c6c; border-bottom: 2px solid #ff6c6c; text-align: center;"><span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding-right: 5px;"><a style="color: #3c3d41; text-decoration: none;" href="http://www.moxy.co.th/th/beauty.html">Beauty</a></span> <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a style="color: #3c3d41; text-decoration: none;" href="http://www.moxy.co.th/th/fashion.html">Fashion</a></span> <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a style="color: #3c3d41; text-decoration: none;" href="http://www.moxy.co.th/th/home-living.html">Home Decor</a></span> <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a style="color: #3c3d41; text-decoration: none;" href="http://www.moxy.co.th/th/home-appliances.html">Home Appliances</a></span> <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a style="color: #3c3d41; text-decoration: none;" href="http://www.moxy.co.th/th/electronics.html">Electronics</a></span> <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a style="color: #3c3d41; text-decoration: none;" href="http://www.moxy.co.th/th/baby.html">Baby &amp; Mom</a></span> <span class="nav-item" style="border-right: 2px solid #ff6c6c; line-height: 33px; font-size: 12px; padding: 0px 5px;"><a style="color: #3c3d41; text-decoration: none;" href="http://www.moxy.co.th/th/health.html">Health &amp; Sports</a></span> <span class="nav-item" style="line-height: 33px; font-size: 12px; padding-left: 5px;"><a style="color: #3c3d41; text-decoration: none;" href="http://www.moxy.co.th/th/pets.html">Pets</a></span></div>
    </div>
    <div class="email-footer" style="min-height: 85px; max-heigh: 85px; background: #E7E7E7; margin-top: 8px; color: #3c3d41; padding: 0 15px 5px 15px;">
        <div class="footer-col" style="float: left; padding: 17px 1% 17px 2%; width: 27%; color: #3c3d41;">
            <p style="font-size: 13px; margin: 0px 0px 0px;"><img style="width: 20px; float: left; margin-right: 5px; margin-left: -2px;" src="{{skin _area='frontend'  url='images/email/mail.png'}}" alt="" /> Contact Us</p>
            <div style="clear: both;">&nbsp;</div>
            <p style="font-size: 10px; margin: 0;">02-106-8222</p>
            <p style="margin: 0; font-size: 10px;"><a style="color: #3c3d41; text-decoration: none;" href="mailto:support@moxy.co.th">support@moxy.co.th</a></p>
        </div>
        <div class="footer-col" style="float: left; padding: 17px 1% 17px 0; width: 32%;">
            <p style="font-size: 13px; margin: 0px 0px 0px;">Follow Us</p>
            <div class="follow-icons" style="width: 100%;"><a style="float: left; width: 16%; padding-right: 2%;" href="https://www.facebook.com/moxyst"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-fb.png"}}" alt="Facebook fanpage" /></a> <a style="float: left; width: 16%; padding-right: 2%;" href="https://twitter.com/moxyst"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-twitter.png"}}" alt="Twitter fanpage" /></a> <a style="float: left; width: 16%; padding-right: 2%;" href="https://www.instagram.com/moxy_th/"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-instagram.png"}}" alt="Instagram fanpage" /></a> <a style="float: left; width: 16%; padding-right: 2%;" href="https://plus.google.com/+Moxyst"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-googleplus.png"}}" alt="Google fanpage" /></a> <a style="float: left; width: 16%; padding-right: 2%;" href="https://www.pinterest.com/MoxySEA1/"><img style="width: 100%; height: auto; max-height: 30px;" src="{{skin _area="frontend"  url="images/email/follow-button-pinterest.png"}}" alt="Pinterest fanpage" /></a></div>
        </div>
        <div class="footer-col" style="float: left; padding: 17px 0 17px 3%; width: 34%;">
            <p style="font-size: 13px; margin: 0px 0px 0px;">Payment Options</p>
            <img style="margin-top: 3px;" src="{{skin _area="frontend"  url="images/email/payment_new.png"}}" alt="Payment Options" />
            <div class="cod" style="font-size: 10px; margin-top: -3px;"><img style="float: left; margin-right: 2px;" src="{{skin _area='frontend'  url='images/email/COD.png'}}" alt="" />
                <p style="float: left; margin: 3px 0 0; font-size: 10px;">Cash on Delivery</p>
            </div>
        </div>
    </div>
    <div class="footer" style="padding: 0; margin-top: 18px;">
        <div class="copyr" style="text-align: center; font-size: 13px;">&copy;2016 Whatsnew Co.,Ltd. All Rights Reserved</div>
        <div class="links" style="text-align: center; font-size: 13px; margin-top: 5px;"><a style="color: #3c3d41;" href="http://www.moxy.co.th/en/about-us/"><span>About Us</span></a> | <a style="color: #3c3d41;" href="http://www.moxy.co.th/en/terms-and-conditions/"><span>Term &amp; Conditions</span></a> | <a style="color: #3c3d41;" href="http://www.moxy.co.th/en/privacy-policy/"><span>Privacy Policy</span></a></div>
    </div>
    <div style="clear: both;">&nbsp;</div>
    <!-- End Footer -->
</div>
HTML;
$customerSatisfaction->setTemplateText($mailContent);
$customerSatisfaction->save();

$installer->endSetup();