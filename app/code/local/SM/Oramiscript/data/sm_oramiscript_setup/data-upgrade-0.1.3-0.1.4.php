<?php

//==========================================================================
// Footer Share Links
//==========================================================================
$blockTitle = 'Footer Share Links';
$blockId = 'banks_footer';
$blockStores = array(0);

/** @var Mage_Cms_Model_Block $block */
$blocks = Mage::getModel('cms/block')->getCollection()
    ->addFieldToFilter('identifier', $blockId);

foreach($blocks as $block){
    if ($block->getId()) $block->delete();// if exists then delete
}
$content = <<<EOD
<a href="https://www.facebook.com/moxyst" target="_blank">
    <span class="fa-stack facebook-footer">
        <i class="fa fa-circle fa-stack-2x"></i>
        <i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
    </span>
</a>
<a  href="https://twitter.com/ORAMI_TH" target="_blank">
    <span class="fa-stack twitter-footer">
        <i class="fa fa-circle fa-stack-2x"></i>
        <i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
    </span>
</a>
<a href="https://www.instagram.com/orami_thailand/" target="_blank">
    <span class="fa-stack instagram-footer">
        <i class="fa fa-circle fa-stack-2x"></i>
        <i class="fa fa-instagram fa-stack-1x fa-inverse"></i>
    </span>
</a>
<a href="https://www.youtube.com/channel/UCMSbWSmzS3ZU12KE2ZfkuSQ" target="_blank">
    <span class="fa-stack youtube-footer">
        <i class="fa fa-circle fa-stack-2x"></i>
        <i class="fa fa-youtube fa-stack-1x fa-inverse"></i>
    </span>
</a>
<a href="https://www.pinterest.com/OramiTH/" target="_blank">
    <span class="fa-stack pinterest-footer">
        <i class="fa fa-circle fa-stack-2x"></i>
        <i class="fa fa-pinterest-p fa-stack-1x fa-inverse"></i>
    </span>
</a>
EOD;

$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockId);
$block->setStores($blockStores);
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================