<?php

// init Magento
Mage::app();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

$moxyen = Mage::getModel('core/store')->load('moxyen', 'code')->getId();
$moxyth = Mage::getModel('core/store')->load('moxyth', 'code')->getId();

//==========================================================================
// Homepage Bottom Banner (English)
//==========================================================================
$blockTitle = "Welcome Bottom Banner";
$blockIdentifier = "welcome-bottom-banner";
$blockStores = array($moxyen);
$blockIsActive = 0;
$blockContent = <<<EOD
<div id="welcome-bottom-banner">
    <ul>
        <li><a href="#"> <img title="" src="{{media url="wysiwyg/welcomePage/homepagebanner_tesco_270.jpg"}}" alt="" /> </a></li>
        <li><a href="#"> <img title="" src="{{media url="wysiwyg/welcomePage/homepagebanner_tesco_270.jpg"}}" alt="" /> </a></li>
        <li><a href="#"> <img title="" src="{{media url="wysiwyg/welcomePage/homepagebanner_tesco_270.jpg"}}" alt="" /> </a></li>
        <li><a href="#"> <img title="" src="{{media url="wysiwyg/welcomePage/homepagebanner_tesco_270.jpg"}}" alt="" /> </a></li>
    </ul>
    <div class="close-bottom-banner">&nbsp;</div>
</div>
EOD;
/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================

//==========================================================================
// Welcome Bottom Banner (Thai)
//==========================================================================
$blockTitle = "Welcome Bottom Banner";
$blockIdentifier = "welcome-bottom-banner";
$blockStores = array($moxyth);
$blockIsActive = 0;
$blockContent = <<<EOD
<div id="welcome-bottom-banner">
    <ul>
        <li><a href="#"> <img title="" src="{{media url="wysiwyg/welcomePage/homepagebanner_tesco_270.jpg"}}" alt="" /> </a></li>
        <li><a href="#"> <img title="" src="{{media url="wysiwyg/welcomePage/homepagebanner_tesco_270.jpg"}}" alt="" /> </a></li>
        <li><a href="#"> <img title="" src="{{media url="wysiwyg/welcomePage/homepagebanner_tesco_270.jpg"}}" alt="" /> </a></li>
        <li><a href="#"> <img title="" src="{{media url="wysiwyg/welcomePage/homepagebanner_tesco_270.jpg"}}" alt="" /> </a></li>
    </ul>
    <div class="close-bottom-banner">&nbsp;</div>
</div>
EOD;
/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================