<?php

// init Magento
Mage::app();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

$moxyen = Mage::getModel('core/store')->load('moxyen', 'code')->getId();
$moxyth = Mage::getModel('core/store')->load('moxyth', 'code')->getId();

/** @var Mage_Cms_Model_Resource_Block_Collection $allBlocks */
$allBlocks = Mage::getModel('cms/block')->getCollection()
    ->addFieldToFilter('identifier', 'welcome-shortcut');
if($allBlocks->count() > 0) {
    /** @var Mage_Cms_Model_Block $item */
    foreach ($allBlocks as $item) {
        if ($item->getId()) $item->delete(); // delete old blocks
    }
}

//==========================================================================
// Welcome splash shortcut link (English)
//==========================================================================
$blockTitle = "Welcome splash shortcut link";
$blockIdentifier = "welcome-shortcut";
$blockStores = array($moxyen);
$blockIsActive = 1;
$blockContent = <<<EOD
<div class="grid-wall">
    <div class="row">
        <div class="col-sm-6">
            <div class="row">
                <div class="col-sm-12">
                    <div class="img-banner"><a href="http://www.orami.co.th/beauty/brands/cetaphil.html"><img title="Shop Now" src="{{media url="wysiwyg/Welcome-Page--Hero-Banner-cetaphil.jpg"}}" alt="Shop Now" /></a></div>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="row">
                <div class="col-xs-6 up-row">
                    <div class="img-banner"><a href="http://www.orami.co.th/cheero-power-plus-10050mah-danboard-version-flower-series.html" target="_self"><img title="Cheero" src="{{media url="wysiwyg/Welcome-Page-Banner-_270x550_-cheero-Power-Plus.jpg"}}" alt="Cheero" /></a></div>
                </div>
                <div class="col-xs-6 up-row">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="img-banner"><a href="http://www.orami.co.th/catalogsearch/result/index/?dir=asc&amp;order=price&amp;q=lifestyle+12" target="_self"><img title="Lifestyle" src="{{media url="wysiwyg/Welcome-Page-Small-Banner-LifeStyles.jpg"}}" alt="Lifestyle" /></a></div>
                        </div>
                        <div class="col-sm-12 item-last">
                            <div class="img-banner"><a href="http://www.orami.co.th/catalogsearch/result/?q=lotus+mattress" target="_self"><img title="Lotus" src="{{media url="wysiwyg/Welcome-Page-Small-Banner-Lotus.jpg"}}" alt="Lotus" /></a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="row">
                <div class="col-xs-6">
                    <div class="img-banner"><a href="http://www.orami.co.th/catalogsearch/result/index/?dir=asc&amp;order=price&amp;q=orijen"><img title="Orijen" src="{{media url="wysiwyg/Welcome-Page-Small-Banner-Origen.jpg"}}" alt="Orijen" /></a></div>
                </div>
                <div class="col-xs-6">
                    <div class="img-banner"><a href="http://www.orami.co.th/catalogsearch/result/?q=merries+pants&amp;price=1650-1650"><img title="Merries" src="{{media url="wysiwyg/Welcome-Page-Small-Banner-merries-060616.jpg"}}" alt="&quot;Merries" /></a></div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6">
                    <div class="img-banner"><a href="http://www.orami.co.th/catalogsearch/result/index/?dir=asc&amp;order=price&amp;q=orijen"><img title="Orijen" src="{{media url="wysiwyg/Welcome-Page-Small-Banner-Origen.jpg"}}" alt="Orijen" /></a></div>
                </div>
                <div class="col-xs-6">
                    <div class="img-banner"><a href="http://www.orami.co.th/catalogsearch/result/?q=merries+pants&amp;price=1650-1650"><img title="Merries" src="{{media url="wysiwyg/Welcome-Page-Small-Banner-merries-060616.jpg"}}" alt="&quot;Merries" /></a></div>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="row">
                <div class="col-sm-12">
                    <div class="img-banner"><a href="http://www.orami.co.th/beauty/brands/cetaphil.html"><img title="Shop Now" src="{{media url="wysiwyg/Welcome-Page--Hero-Banner-cetaphil.jpg"}}" alt="Shop Now" /></a></div>
                </div>
            </div>
        </div>
    </div>
</div>
EOD;
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================


//==========================================================================
// Welcome splash shortcut link (Thai)
//==========================================================================
$blockTitle = "Welcome splash shortcut link";
$blockIdentifier = "welcome-shortcut";
$blockStores = array($moxyth);
$blockIsActive = 1;
$blockContent = <<<EOD
<div class="grid-wall">
    <div class="row">
        <div class="col-sm-6">
            <div class="row">
                <div class="col-sm-12">
                    <div class="img-banner"><a href="http://www.orami.co.th/beauty/brands/cetaphil.html"><img title="Shop Now" src="{{media url="wysiwyg/Welcome-Page--Hero-Banner-cetaphil.jpg"}}" alt="Shop Now" /></a></div>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="row">
                <div class="col-xs-6 up-row">
                    <div class="img-banner"><a href="http://www.orami.co.th/cheero-power-plus-10050mah-danboard-version-flower-series.html" target="_self"><img title="Cheero" src="{{media url="wysiwyg/Welcome-Page-Banner-_270x550_-cheero-Power-Plus.jpg"}}" alt="Cheero" /></a></div>
                </div>
                <div class="col-xs-6 up-row">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="img-banner"><a href="http://www.orami.co.th/catalogsearch/result/index/?dir=asc&amp;order=price&amp;q=lifestyle+12" target="_self"><img title="Lifestyle" src="{{media url="wysiwyg/Welcome-Page-Small-Banner-LifeStyles.jpg"}}" alt="Lifestyle" /></a></div>
                        </div>
                        <div class="col-sm-12 item-last">
                            <div class="img-banner"><a href="http://www.orami.co.th/catalogsearch/result/?q=lotus+mattress" target="_self"><img title="Lotus" src="{{media url="wysiwyg/Welcome-Page-Small-Banner-Lotus.jpg"}}" alt="Lotus" /></a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="row">
                <div class="col-xs-6">
                    <div class="img-banner"><a href="http://www.orami.co.th/catalogsearch/result/index/?dir=asc&amp;order=price&amp;q=orijen"><img title="Orijen" src="{{media url="wysiwyg/Welcome-Page-Small-Banner-Origen.jpg"}}" alt="Orijen" /></a></div>
                </div>
                <div class="col-xs-6">
                    <div class="img-banner"><a href="http://www.orami.co.th/catalogsearch/result/?q=merries+pants&amp;price=1650-1650"><img title="Merries" src="{{media url="wysiwyg/Welcome-Page-Small-Banner-merries-060616.jpg"}}" alt="&quot;Merries" /></a></div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6">
                    <div class="img-banner"><a href="http://www.orami.co.th/catalogsearch/result/index/?dir=asc&amp;order=price&amp;q=orijen"><img title="Orijen" src="{{media url="wysiwyg/Welcome-Page-Small-Banner-Origen.jpg"}}" alt="Orijen" /></a></div>
                </div>
                <div class="col-xs-6">
                    <div class="img-banner"><a href="http://www.orami.co.th/catalogsearch/result/?q=merries+pants&amp;price=1650-1650"><img title="Merries" src="{{media url="wysiwyg/Welcome-Page-Small-Banner-merries-060616.jpg"}}" alt="&quot;Merries" /></a></div>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="row">
                <div class="col-sm-12">
                    <div class="img-banner"><a href="http://www.orami.co.th/beauty/brands/cetaphil.html"><img title="Shop Now" src="{{media url="wysiwyg/Welcome-Page--Hero-Banner-cetaphil.jpg"}}" alt="Shop Now" /></a></div>
                </div>
            </div>
        </div>
    </div>
</div>
EOD;
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================


//==========================================================================
// Homepage Content Banner (English)
//==========================================================================
$blockTitle = "Welcome Content Banner";
$blockIdentifier = "welcome-content-banner";
$blockStores = array($moxyen);
$blockIsActive = 1;
$blockContent = <<<EOD
<div class="welcome-content-banner">
    <a href="#">
        <img title="" src='{{media url="wysiwyg/welcomePage/945x300.jpg"}}' alt="" />
    </a>
</div>
EOD;
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================

//==========================================================================
// Welcome Content Banner (Thai)
//==========================================================================
$blockTitle = "Welcome Content Banner";
$blockIdentifier = "welcome-content-banner";
$blockStores = array($moxyth);
$blockIsActive = 1;
$blockContent = <<<EOD
<div class="welcome-content-banner">
    <a href="#">
        <img title="" src='{{media url="wysiwyg/welcomePage/945x300.jpg"}}' alt="" />
    </a>
</div>
EOD;
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================

//==========================================================================
// Homepage Brand Logo Icons (English)
//==========================================================================
$blockTitle = "Welcome Brand Logo Icons";
$blockIdentifier = "welcome-brand-logo";
$blockStores = array($moxyen);
$blockIsActive = 1;
$blockContent = <<<EOD
<div class="welcome-brand-logo">
    <h3 class="line-thought desktop">Our Popular Brands</h3>
    <h3 class="line-thought mobile">Our Popular Brands</h3>
    <div class="brand-items">
        <ul>
            <li><img src="{{media url='wysiwyg/welcomePage/Brands/brands_mamypoko.jpg'}}" alt="" /></li>
            <li><img src="{{media url='wysiwyg/welcomePage/Brands/brands_apple.jpg'}}" alt="" /></li>
            <li><img src="{{media url='wysiwyg/welcomePage/Brands/brands_royalcanin.jpg'}}" alt="" /></li>
            <li><img src="{{media url='wysiwyg/welcomePage/Brands/brands_vistra.jpg'}}" alt="" /></li>
            <li><img src="{{media url='wysiwyg/welcomePage/Brands/brands_casio.jpg'}}" alt="" /></li>
            <li><img src="{{media url='wysiwyg/welcomePage/Brands/brands_nyx.jpg'}}" alt="" /></li>
            <li><img src="{{media url='wysiwyg/welcomePage/Brands/brands_urban.jpg'}}" alt="" /></li>
            <li><img src="{{media url='wysiwyg/welcomePage/Brands/brands_electrolux.jpg'}}" alt="" /></li>
            <li><img src="{{media url='wysiwyg/welcomePage/Brands/brands_brands.jpg'}}" alt="" /></li>
            <li><img src="{{media url='wysiwyg/welcomePage/Brands/brands_price.jpg'}}" alt="" /></li>
            <li><img src="{{media url='wysiwyg/welcomePage/Brands/brands_360.jpg'}}" alt="" /></li>
            <li><img src="{{media url='wysiwyg/welcomePage/Brands/brands_friskies.jpg'}}" alt="" /></li>
            <li><img src="{{media url='wysiwyg/welcomePage/Brands/brands_samsung.jpg'}}" alt="" /></li>
            <li><img src="{{media url='wysiwyg/welcomePage/Brands/brands_huggies.jpg'}}" alt="" /></li>
            <li><img src="{{media url='wysiwyg/welcomePage/Brands/brands_longchamp.jpg'}}" alt="" /></li>
            <li><img src="{{media url='wysiwyg/welcomePage/Brands/brands_loreal.jpg'}}" alt="" /></li>
        </ul>
        <div style="clear: both;">&nbsp;</div>
    </div>
</div>
EOD;
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================

//==========================================================================
// Welcome Brand Logo Icon (Thai)
//==========================================================================
$blockTitle = "Welcome Brand Logo Icons";
$blockIdentifier = "welcome-brand-logo";
$blockStores = array($moxyth);
$blockIsActive = 1;
$blockContent = <<<EOD
<div class="welcome-brand-logo">
    <h3 class="line-thought desktop">Our Popular Brands</h3>
    <h3 class="line-thought mobile">Our Popular Brands</h3>
    <div class="brand-items">
        <ul>
            <li><img src="{{media url='wysiwyg/welcomePage/Brands/brands_mamypoko.jpg'}}" alt="" /></li>
            <li><img src="{{media url='wysiwyg/welcomePage/Brands/brands_apple.jpg'}}" alt="" /></li>
            <li><img src="{{media url='wysiwyg/welcomePage/Brands/brands_royalcanin.jpg'}}" alt="" /></li>
            <li><img src="{{media url='wysiwyg/welcomePage/Brands/brands_vistra.jpg'}}" alt="" /></li>
            <li><img src="{{media url='wysiwyg/welcomePage/Brands/brands_casio.jpg'}}" alt="" /></li>
            <li><img src="{{media url='wysiwyg/welcomePage/Brands/brands_nyx.jpg'}}" alt="" /></li>
            <li><img src="{{media url='wysiwyg/welcomePage/Brands/brands_urban.jpg'}}" alt="" /></li>
            <li><img src="{{media url='wysiwyg/welcomePage/Brands/brands_electrolux.jpg'}}" alt="" /></li>
            <li><img src="{{media url='wysiwyg/welcomePage/Brands/brands_brands.jpg'}}" alt="" /></li>
            <li><img src="{{media url='wysiwyg/welcomePage/Brands/brands_price.jpg'}}" alt="" /></li>
            <li><img src="{{media url='wysiwyg/welcomePage/Brands/brands_360.jpg'}}" alt="" /></li>
            <li><img src="{{media url='wysiwyg/welcomePage/Brands/brands_friskies.jpg'}}" alt="" /></li>
            <li><img src="{{media url='wysiwyg/welcomePage/Brands/brands_samsung.jpg'}}" alt="" /></li>
            <li><img src="{{media url='wysiwyg/welcomePage/Brands/brands_huggies.jpg'}}" alt="" /></li>
            <li><img src="{{media url='wysiwyg/welcomePage/Brands/brands_longchamp.jpg'}}" alt="" /></li>
            <li><img src="{{media url='wysiwyg/welcomePage/Brands/brands_loreal.jpg'}}" alt="" /></li>
        </ul>
        <div style="clear: both;">&nbsp;</div>
    </div>
</div>
EOD;
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================

//==========================================================================
// Home ENG
//==========================================================================
$title = 'ORAMI by Moxy The Online Shopping Destination for Women';
$identifier = "home";
$stores = array($moxyen);
$status = 1;
$content = <<<EOD
{{block type="core/template" name="subscription" as="subscription" template="subscription/subscribe.phtml"}}
EOD;
$rootTemplate = "one_column";
$layoutXML = <<<EOD
<reference name="head">
    <block type="core/text" name="homepage.metadata" after="-">
        <action method="addText"><text><![CDATA[<meta name="google-site-verification" content="IDwIJ1YbPmxWd60Ej1zWtvkggB95TZAyX5lIPOFn5s4"/>]]></text></action>
    </block>
</reference>

<reference name="content">
    <block type="cms/block" name="home.sliders">
        <action method="setBlockId"><block_id>welcome-shortcut</block_id></action>
    </block>

    <block type="petloftcatalog/product_homegrid" name="home.product.grid1" as="homegrid1" template="petloft/catalog/product/homegrid.phtml">
        <action method="setCategoryId"><id>2655</id></action>
    </block>

    <block type="cms/block" name="home.promotion.banner">
        <action method="setBlockId"><block_id>welcome-promotion-banner</block_id></action>
    </block>

    <block type="petloftcatalog/product_homegrid" name="home.product.grid2" as="homegrid2" template="petloft/catalog/product/homegrid.phtml">
        <action method="setCategoryId"><id>2656</id></action>
    </block>

    <block type="cms/block" name="welcome.content.banner">
        <action method="setBlockId"><block_id>welcome-content-banner</block_id></action>
    </block>

    <block type="cms/block" name="welcome.brand.logo">
        <action method="setBlockId"><block_id>welcome-brand-logo</block_id></action>
    </block>
</reference>
EOD;
$metaDesc = 'Beauty, Fashion, Home Decor, Gadgets & Electronics, Health & Sports, Mom & Babies, Pet Products - shipped directly to you!';

/** @var Mage_Cms_Model_Page $page */
$page = Mage::getModel('cms/page')->getCollection()
    ->addStoreFilter($stores, $withAdmin = false)
    ->addFieldToFilter('identifier', $identifier)
    ->getFirstItem();

if($page->getId()) $page->delete();
$page = Mage::getModel('cms/page');
$page->setTitle($title);
$page->setIdentifier($identifier);
$page->setIsActive($status);
$page->setContent($content);
$page->setRootTemplate($rootTemplate);
$page->setLayoutUpdateXml($layoutXML);
$page->setMetaDescription($metaDesc);
$page->setStores($stores);
$page->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// Home THAI
//==========================================================================
$title = 'ORAMI by Moxy เว็บช้อปปิ้งออนไลน์ สำหรับสาวนักช้อปทุกคน';
$identifier = "home";
$stores = array($moxyth);
$status = 1;
$content = <<<EOD
{{block type="core/template" name="subscription" as="subscription" template="subscription/subscribe.phtml"}}
EOD;
$rootTemplate = "one_column";
$layoutXML = <<<EOD
<reference name="head">
    <block type="core/text" name="homepage.metadata" after="-">
        <action method="addText"><text><![CDATA[<meta name="google-site-verification" content="IDwIJ1YbPmxWd60Ej1zWtvkggB95TZAyX5lIPOFn5s4"/>]]></text></action>
    </block>
</reference>

<reference name="content">
    <block type="cms/block" name="home.sliders">
        <action method="setBlockId"><block_id>welcome-shortcut</block_id></action>
    </block>

    <block type="petloftcatalog/product_homegrid" name="home.product.grid1" as="homegrid1" template="petloft/catalog/product/homegrid.phtml">
        <action method="setCategoryId"><id>2655</id></action>
    </block>

    <block type="cms/block" name="home.promotion.banner">
        <action method="setBlockId"><block_id>welcome-promotion-banner</block_id></action>
    </block>

    <block type="petloftcatalog/product_homegrid" name="home.product.grid2" as="homegrid1" template="petloft/catalog/product/homegrid.phtml">
        <action method="setCategoryId"><id>2656</id></action>
    </block>

    <block type="cms/block" name="welcome.content.banner">
        <action method="setBlockId"><block_id>welcome-content-banner</block_id></action>
    </block>

    <block type="cms/block" name="welcome.brand.logo">
        <action method="setBlockId"><block_id>welcome-brand-logo</block_id></action>
    </block>
</reference>
EOD;
$metaDesc = 'เครื่องสำอาง แฟชั่น ของแต่งบ้าน แก็ตเจ็ต อีเล็คทรอนิกส์ สินค้าเพื่อสุขภาพ สินค้าสำหรับแม่และเด็ก และสินค้าสำหรับสัตว์เลี้ยง พร้อมส่งตรงถึงหน้าบ้านคุณ!';

/** @var Mage_Cms_Model_Page $page */
$page = Mage::getModel('cms/page')->getCollection()
    ->addStoreFilter($stores, $withAdmin = false)
    ->addFieldToFilter('identifier', $identifier)
    ->getFirstItem();

if($page->getId()) $page->delete();
$page = Mage::getModel('cms/page');
$page->setTitle($title);
$page->setIdentifier($identifier);
$page->setIsActive($status);
$page->setContent($content);
$page->setRootTemplate($rootTemplate);
$page->setLayoutUpdateXml($layoutXML);
$page->setMetaDescription($metaDesc);
$page->setStores($stores);
$page->save();
//==========================================================================
//==========================================================================
//==========================================================================