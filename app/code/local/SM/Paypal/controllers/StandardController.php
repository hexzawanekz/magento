<?php
require_once Mage::getModuleDir('controllers', "Mage_Paypal") . DS . "StandardController.php";

class SM_Paypal_StandardController extends Mage_Paypal_StandardController
{

    /**
     * When a customer cancel payment from paypal.
     */
    public function cancelAction()
    {
        $session = Mage::getSingleton('checkout/session');
        $session->setQuoteId($session->getPaypalStandardQuoteId(true));
        if ($session->getLastRealOrderId()) {
            $order = Mage::getModel('sales/order')->loadByIncrementId($session->getLastRealOrderId());
            if ($order->getId()) {
                $order->cancel()->save();
                $userSession = Mage::getSingleton('customer/session');
                $cart = Mage::getSingleton('checkout/cart');
                $cart->init();
                $items = $order->getAllVisibleItems();
                foreach ($items as $item) {
                    $product = Mage::getModel('catalog/product')->load($item->getProductId());
                    $qty = $item->getQtyOrdered();
                    $cart->addProduct($product->getId(), $qty);
                }
                $userSession->setCartWasUpdated(true);
                $cart->save();
            }
        }
        $this->_redirect('checkout/cart');
    }

}