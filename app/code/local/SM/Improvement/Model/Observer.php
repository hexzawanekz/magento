<?php

class SM_Improvement_Model_Observer{

    /**
     * Create cookie for login successfully customer
     * @param Varien_Event_Observer $observer
     */
    public function createCustomerCookie(Varien_Event_Observer $observer){
        try{
            $customer = $observer->getEvent()->getCustomer();
            $customerId = $customer->getId();
            $customerEmail = $customer->getEmail();

            $helper = Mage::helper('improvement');
            $code = $helper->encodeCustomerId($customerId,$customerEmail);
            setcookie('customer', $code, time() + 260000, "/");
        }catch(Exception $e){
            Mage::log($e->getMessage(),null,'customer_cookie_error.log',true);
        }
    }

    /**
     * Remove encoded cookie when customer logout
     * @param Varien_Event_Observer $observer
     */
    public function removeCustomerCookie(Varien_Event_Observer $observer){
        try{
           if(isset($_COOKIE['customer'])){
               unset($_COOKIE['customer']);
               setcookie('customer', null, -1, "/");
           }
        }catch(Exception $e){
            Mage::log($e->getMessage(),null,'customer_cookie_error.log',true);
        }
    }

    /**
     * Auto login when customer choose keep me loggin
     * @param Varien_Event_Observer $observer
     */
    public function autoLogin(Varien_Event_Observer $observer){
        if(!Mage::getSingleton('customer/session')->isLoggedIn()){
            $cooki = Mage::getModel('core/cookie');
            $customerNumber = $cooki->get('user_number');
            $keepLogin = $cooki->get('keeplogin');
            if($keepLogin == 1){
                try {
                    $websiteId = Mage::app()->getWebsite()->getId();
                    $customer = Mage::getModel('customer/customer');
                    $customer->setWebsiteId($websiteId);
                    $customer->load($customerNumber);
                    $session = Mage::getSingleton('customer/session');
                    if ($customer->getId()) {
                        $session->setCustomerAsLoggedIn($customer);
                        $session->save();
                    }
                }catch (Exception $e){

                }
            }
        }
    }
    /**
     * Special fix for 'Durex' case
     * @param Varien_Event_Observer $observer
     */
    public function fixSearch(Varien_Event_Observer $observer){
//        $block = Mage::app()->getLayout()->getBlock('search_result_list');
//        $query = Mage::helper('catalogsearch')->getQuery();
//        $query = strtolower($query);
//        if ($query == "durex") {
//            if ($block) {
//                $collection = $block->getLoadedProductCollection();
//            }
//        }
    }

    /**
     * Set store to admin store when save product.
     * @param Varien_Event_Observer $observer
     */
    public function setAdminStore(Varien_Event_Observer $observer){
        Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
        $product = $observer->getEvent()->getProduct();
        if($product->getData('override_inventory') == 1) {
            $stock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product);
            Mage::register('quantity_before_' . $product->getId(), $stock->getQty());
        }
    }

    public function logQuantityChange(Varien_Event_Observer $observer){
        $product = $observer->getEvent()->getProduct();
        if(Mage::registry('quantity_before_' . $product->getId())){
            $oldQuantity = Mage::registry('quantity_before_' . $product->getId());
            $stock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product);
            $newQuantity = $stock->getQty();
            if($oldQuantity != $newQuantity) {
                Mage::log('-------------------------------------------------------', null, 'product_inventory_change.log', true);
                Mage::log('Product: ' . $product->getName() . ' - ID: ' . $product->getId(), null, 'product_inventory_change.log', true);
                Mage::log('Quantity Change From ' . (int)$oldQuantity . ' To ' . (int)$newQuantity, null, 'product_inventory_change.log', true);
                Mage::unregister('quantity_before_' . $product->getId());

            }
        }
    }

    /**
     * Save customer custom tax/VAT value
     * @param Varien_Event_Observer $observer
     */
    public function saveCustomerTaxValue(Varien_Event_Observer $observer){
        if(Mage::registry('tax_value')){
            $customer = $observer->getEvent()->getCustomer();
            $customer->setTaxvat(Mage::registry('tax_value'))->save();
            Mage::unregister('tax_value');
        }
    }


    /**
     * Export encrypted customer email for Criteo.
     */
    public function criteoEmailExportCsv(){
        ini_set('memory_limit', -1);
        ini_set('max_execution_time', 0);
        //Folder to store Data.
        $folder = 'media' . DS . 'CriteoEmailExport' . DS;
        if (!is_dir($folder)) {
            mkdir($folder, 0777, true);
        }

        $fileName = "CriteoEmailList";

        $emailData = array();

        $customers = Mage::getModel('customer/customer')
            ->getCollection();
        foreach ($customers as $customer){
            $email = $customer->getEmail();
            $email = md5($email);
            $email = hash('sha256',$email);
            $emailData[$customer->getId()] = $email;
        }

        if(count($emailData) > 0){
            // Keep an backup file
            if (file_exists($folder . $fileName . '.csv')) {
                if (file_exists($folder . 'bak_' . $fileName . '.csv')) {
                    unlink($folder . 'bak_' . $fileName . '.csv');
                }
                rename($folder . $fileName . '.csv', $folder . 'bak_' . $fileName . '.csv');
            }

            // Open new file to write data
            $fp = fopen($folder . $fileName . '.csv', 'w+');
            foreach ($emailData as $_result) {
                $_result = sprintf("\"%s\"\n", $_result);
                fwrite($fp, $_result);
            }
            fclose($fp);
        }
    }

    public function applyCoupon(Varien_Event_Observer $observer){
         $controller = $observer->getControllerAction();
         $cp = $controller->getRequest()->getParam('coupon_code');
                $cpcode = Mage::getModel('salesrule/coupon')->load($cp, 'code');
                $count = $cpcode->getTimesUsed();
                $active = Mage::getStoreConfig('improvement/general/active',Mage::app()->getStore());
                $first = Mage::getStoreConfig('improvement/general/first',Mage::app()->getStore());
                $next = Mage::getStoreConfig('improvement/general/next',Mage::app()->getStore());
                $limit = Mage::getStoreConfig('improvement/general/limit',Mage::app()->getStore());
        if($active == "1"){
            if($cp == $first){
                if($count >= $limit) {
                        Mage::getSingleton('core/session')->setCouponFirst($cp);
                        Mage::getSingleton('core/session')->setCouponNext($next);
                }
            }
        }
    }

}