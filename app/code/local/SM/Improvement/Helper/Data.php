<?php
class SM_Improvement_Helper_Data extends Mage_Core_Helper_Abstract{
    const XML_PATH_IMPROVEMENT_FIRST    = 'improvement/general/first';
    const XML_PATH_IMPROVEMENT_NEXT     = 'improvement/general/next';
    const XML_PATH_IMPROVEMENT_LIMIT    = 'improvement/general/limit';
    const XML_PATH_IMPROVEMENT_ACTIVE   = 'improvement/general/active';

    /**
     * This function will create a encode string base on customer Id and customer Email
     * @param $customerId
     * @param $customerEmail
     * @return array|string
     */
    public function encodeCustomerId($customerId,$customerEmail){
        $customerId = base64_encode($customerId);
        $customerId = rawurlencode($customerId);

        $customerEmail = base64_encode($customerEmail);
        $customerEmail = rawurlencode($customerEmail);

        $customerEncode = "osc_customer_cookie";
        $customerEncode = base64_encode($customerEncode);

        $code = $customerId.$customerEncode.$customerEmail;

        $code = base64_encode($code);
        $code = rawurlencode($code);

        return $code;
    }

    public function calculateNeededPrice(){
        $limitation = 1001;
        $quote = Mage::getModel('checkout/session')->getQuote();
        $quoteData = $quote->getData();
        $subTotal = $quoteData['subtotal'];
        if($subTotal >= $limitation){
            return 0;
        }else{
            $needed = $limitation - $subTotal;
            return $needed;
        }
    }

}

?>