<?php
class SM_Improvement_CartController extends Mage_Core_Controller_Front_Action
{
    public function addCartAction()
    {
        $post_data = Mage::app()->getRequest()->getPost();
        $productId = $post_data['product_id'];
        $quantity = $post_data['quantity'];

        if ($quantity == NULL || $quantity == "") $quantity = 1;
        $product = Mage::getModel('catalog/product')->load($productId);
        try{
            if($product->isSaleable()) {
                $cart = Mage::getSingleton('checkout/cart');
                $cart->addProduct($product, array('qty' => $quantity));
                $cart->save();

                echo 'Product Add To Cart Successfully';
                header('Access-Control-Allow-Origin: *');
            }else{
                echo "This Product Is Not Saleable";
                header('Access-Control-Allow-Origin: *');
            }
        }catch (Exception $e){
            echo $e->getMessage();
            header('Access-Control-Allow-Origin: *');
        }

    }

    public function getCartAction(){
        $_coreHelper = Mage::helper('core');
        $cart = Mage::getModel('checkout/cart')->getQuote();
        $data = array();
        foreach ($cart->getAllItems() as $item) {
            if($item->getProductType() == 'simple') {
                $price = $this->getSmallestPrice($item->getProduct()->getPrice(),$item->getProduct()->getSpecialPrice(),$item->getProduct()->getMsrp());
                $data[] = array(
                    "image" => $item->getProduct()->getThumbnail(),
                    "name" => $item->getProduct()->getName(),
                    "price" => $_coreHelper->currency($price, true, false),
                    "quantity" => ($item->getParentItem() != null) ? $item->getParentItem()->getQty() : $item->getQty() ,
                    "total" => $_coreHelper->currency($cart->getGrandTotal(), true, false)
                );
            }
        }
        echo json_encode($data);
        header('Access-Control-Allow-Origin: *');
    }

    public function getSmallestPrice($price,$specialPrice,$msrp){
        if($specialPrice == NULL || $specialPrice == "") $specialPrice = 0;
        if($msrp == NULL ||$msrp == "") $msrp = 0;
        if($price <=  $msrp){
            if($price <= $specialPrice ){
                return $price;
            }else{
                if ($specialPrice != 0) return $specialPrice; else return $price;
            }
        }else{
            if ($price > $specialPrice){
                if ($specialPrice != 0) return $specialPrice; else return $price;
            }else{
                return $price;
            }
        }
    }
}