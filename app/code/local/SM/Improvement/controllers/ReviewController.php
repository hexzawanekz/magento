<?php
class SM_Improvement_ReviewController extends Mage_Core_Controller_Front_Action
{
    public function addReviewAction()
    {
        $post_data = Mage::app()->getRequest()->getPost();
        $customerNickname = $post_data['customer_nickname'];
        $reviewTitle = $post_data['review_title'];
        $reviewDescription = $post_data['review_des'];
        $productId = $post_data['product_id'];
        $ratingValue = $post_data['rating_value'];
        $customer = Mage::getSingleton('customer/session')->getCustomer();

        $_review = Mage::getModel('review/review')
            ->setEntityPkValue($productId)
            ->setStatusId(2)
            ->setTitle($reviewTitle)
            ->setDetail($reviewDescription)
            ->setEntityId(1)
            ->setStoreId(Mage::app()->getStore()->getId())
            ->setStores(array(Mage::app()->getStore()->getId()))
            ->setCustomerId($customer->getId())
            ->setNickname($customerNickname)
            ->save();

        $rating_options = array(
            1 => array(1,2,3,4,5),
            2 => array(6,7,8,9,10),
            3 => array(11,12,13,14,15)
        );

        foreach($rating_options as $rating_id => $option_ids):
            try {
                $_rating = Mage::getModel('rating/rating')
                    ->setRatingId($rating_id)
                    ->setReviewId($_review->getId())
                    ->addOptionVote($option_ids[$ratingValue-1],$productId);
            } catch (Exception $e) {
                echo $e->getMessage();
            }
        endforeach;
        echo 'Review Successfully Added (Waiting For Approve)';
        header('Access-Control-Allow-Origin: *');
    }
}