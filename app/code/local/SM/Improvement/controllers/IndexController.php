<?php
class SM_Improvement_IndexController extends Mage_Core_Controller_Front_Action
{
    public function getMsrpAction()
    {
        $coreHelper = Mage::helper('core');

        $post_data = Mage::app()->getRequest()->getPost();
        $productId = $post_data['product_id'];
        $attributeValue = $post_data['attribute_value'];
        $product = Mage::getModel('catalog/product')->load($productId);
        $childProducts = Mage::getModel('catalog/product_type_configurable')->getUsedProducts(null,$product);
        $productAttributeOptions = $product->getTypeInstance(true)->getConfigurableAttributesAsArray($product);
        foreach($childProducts as $child){
            $childProductsArray[$child->getData($productAttributeOptions[0]['attribute_code'])] = $child->getData('entity_id');
        }

        $selectedChild = $childProductsArray[$attributeValue];
        $childProduct = Mage::getSingleton('catalog/product')->load($selectedChild);

        $msrp = $childProduct->getData('msrp');
        $discount = round((($msrp - $childProduct->getPrice()) / $msrp) * 100);
        $msrp = $coreHelper->formatPrice($msrp, false);
        $img = $childProduct->getId();
        $childImageUrl = "";
//        $childImageUrl = $product->getMediaGalleryImages()->getItemByColumnValue('label',$childProduct->getId())->getFile();
//        $childImageUrl = Mage::helper('catalog/image')->init($product, 'image', $childImageUrl)->__toString();
//        $data = array(
//            'product_image_url' => $img,
//            'msrp' => $msrp,
//            'discount' => $discount,
//            'quantity' => $childProduct->getStockItem()->getData('qty'),
//            'manage_stock' => $childProduct->getStockItem()->getData('manage_stock'),
//            'child_img_url' => $childImageUrl,
//        );
        $str = $img .'|'.$msrp .'|'.$discount.'|'.$childProduct->getStockItem()->getData('qty').'|'.$childProduct->getStockItem()->getData('manage_stock').'|'.$childImageUrl;
        echo $str;
        header('Access-Control-Allow-Origin: *');

    }
}