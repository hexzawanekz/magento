<?php

// init Magento
Mage::app();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
$config = new Mage_Core_Model_Config();
// get store ids
$en     = Mage::getModel('core/store')->load('en', 'code')->getId();// Petloft
$th     = Mage::getModel('core/store')->load('th', 'code')->getId();
$len    = Mage::getModel('core/store')->load('len', 'code')->getId();// Lafema
$lth    = Mage::getModel('core/store')->load('lth', 'code')->getId();
$ven    = Mage::getModel('core/store')->load('ven', 'code')->getId();// Venbi
$vth    = Mage::getModel('core/store')->load('vth', 'code')->getId();
$sen    = Mage::getModel('core/store')->load('sen', 'code')->getId();// Sanoga
$sth    = Mage::getModel('core/store')->load('sth', 'code')->getId();
$moxyen = Mage::getModel('core/store')->load('moxyen', 'code')->getId();// Moxy
$moxyth = Mage::getModel('core/store')->load('moxyth', 'code')->getId();


//==========================================================================
// Newsletter Unsubscription Success Petloft EN
//==========================================================================

$template_text =  <<<HTML
<body style="background:#e3e3e3; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; margin:0; padding:0;">
<div style="background:#e3e3e3; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; margin:0; padding:0;">
<table cellspacing="0" cellpadding="0" border="0" width="100%">
<tr>
    <td align="center" valign="top" style="padding:20px 0 20px 0">
        <table bgcolor="#FFFFFF" cellspacing="0" cellpadding="0" border="0" width="590">
            <!-- [ header starts here] -->
            <tr>
                <td valign="middle" style="padding: 0 25px;" height="80"><a href="{{store url=""}}"><img src="{{skin url='images/logo_email.gif'}}" alt="{{var logo_alt}}" border="0"/></a></td>
            </tr>
            <tr>
                <td height="10"><img src="{{skin url='images/email-line.gif'}}"  style="margin-bottom:10px;" border="0"/></td>
            </tr>
            <!-- [ middle starts here] -->
            <tr>
                <td valign="top" style="padding:15px 30px;">
                    <h1 style="font-size:24px; font-weight:bold; line-height:24px; margin:0 0 11px 0;color:#28448d;">Dear {{htmlescape var=\$customer.name}},</h1>
                        <br>
                        <p style="font-size:13px; line-height:18px;color:#505050;margin: 0 0 16px 0;">You have successfully unsubscribed to our newsletter.</p>

                            <p style="font-size:13px; line-height:18px;color:#505050; margin:0;">We are sorry to see you leave :-( </p>
                            <br>
                            <span style="font-size:13px; line-height:18px;color:#505050;">If you have a moment, please let us know why at <a href="mailto:support@moxy.co.th " style="color:#28438c; text-decoration:none;">support@moxy.co.th </a></span>
                            <br><br>
                            <span style="font-size:13px; line-height:18px;color:#505050; margin:0;">With Love, </span>

<br><br>
<span style="font-size:13px; line-height:18px;color:#505050;font-weight:bold;">The Moxy Team</span>

                        </td>
                    </tr>
            <tr>
                <td height="30"><img src="{{skin url='images/email-line.gif'}}"  style="margin-top:20px;" border="0"/></td>
            </tr>
            <tr>
                <td style="padding:20px;">
                    <span style="font:normal 11px 'Arial';color:#505050;">For Customer Service inquiries, please email <a href="mailto:{{config path='trans_email/ident_support/email'}}" style="color:#28438c; text-decoration:none;">{{config path='trans_email/ident_support/email'}}</a>.
                    Have a suggestion, comment, or general feedback? Please send an email to <a href="mailto:{{config path='trans_email/ident_support/email'}}" style="color:#28438c; text-decoration:none;">{{config path='trans_email/ident_support/email'}}</a> to let us know.</span><br/><br/>
                    <span style="font:normal 11px 'Arial';color:#505050;">
                    This email was sent to you by PetLoft.com. To ensure delivery to your inbox (not bulk or junk folders), you can add <a href="mailto:{{config path='trans_email/ident_support/email'}}" style="color:#28438c; text-decoration:none;">{{config path='trans_email/ident_support/email'}}</a> to your address book or safe list.</span><br/><br/>
                    <table cellpadding="0" cellspacing="0" width="590">
                        <tr>
                            <td width="450">
                                <span style="font:normal 11px 'Arial';color:#505050;line-height:16px;">
                                            &copy; 2013 Whatsnew Co.,Ltd. All Rights Reserved.<br/>
                                            Sethiwan Tower - 17th Floor, <br/>139 Pan Road, Silom, Bangrak, Bangkok, 10500 Thailand</span><br/><br/>
                                            <span style="font:normal 11px 'Arial';color:#505050;line-height:16px;">
                                            <a href="{{store url="privacy-policy"}}" style="color:#505050; text-decoration:none;">Privacy Policy</a> | <a href="{{store url="contacts"}}" style="color:#505050; text-decoration:none;">Contact us</a></span>
                            </td>
                            <td valign="bottom" align="right">
                                <a href="https://www.facebook.com/petloftthai" target="_blank"><img src="{{skin url='images/email-facebook.gif'}}"  style="margin-right:10px;" border="0"/></a>
                                <a href="https://twitter.com/PetLoft" target="_blank"><img src="{{skin url='images/email-twitter.gif'}}"  style="margin-right:10px;" border="0"/></a>
                                <a href="https://plus.google.com/u/0/b/113812097105339950445/113812097105339950445/op/iph" target="_blank"><img src="{{skin url='images/email-google.gif'}}" border="0"/></a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </td>
</tr>
</table>
</div>
</body>
HTML;



$model = Mage::getModel('core/email_template');
$model->loadByCode('Newsletter Unsubscription Success (Petloft English)')
    ->setTemplateCode('Newsletter Unsubscription Success (Petloft English)')
    ->setTemplateText($template_text)
    ->setTemplateSubject('Sorry to say Goodbye')
    ->setTemplateType(Mage_Core_Model_Email_Template::TYPE_HTML);
try {
    $model->save();
    $config->saveConfig('newsletter/subscription/un_email_template', $model->getId() , 'stores', $en);
} catch (Mage_Exception $e) {
    throw $e;
}

$model->loadByCode('Newsletter Unsubscription Success (Lafema English)')
    ->setTemplateCode('Newsletter Unsubscription Success (Lafema English)')
    ->setTemplateText($template_text)
    ->setTemplateSubject('Sorry to say Goodbye')
    ->setTemplateType(Mage_Core_Model_Email_Template::TYPE_HTML);
try {
    $model->save();
    $config->saveConfig('newsletter/subscription/un_email_template', $model->getId() , 'stores', $len);
} catch (Mage_Exception $e) {
    throw $e;
}

$model->loadByCode('Newsletter Unsubscription Success (Sanoga English)')
    ->setTemplateCode('Newsletter Unsubscription Success (Sanoga English)')
    ->setTemplateText($template_text)
    ->setTemplateSubject('Sorry to say Goodbye')
    ->setTemplateType(Mage_Core_Model_Email_Template::TYPE_HTML);
try {
    $model->save();
    $config->saveConfig('newsletter/subscription/un_email_template', $model->getId() , 'stores', $sen);
} catch (Mage_Exception $e) {
    throw $e;
}

$model->loadByCode('Newsletter Unsubscription Success (Venbi English)')
    ->setTemplateCode('Newsletter Unsubscription Success (Venbi English)')
    ->setTemplateText($template_text)
    ->setTemplateSubject('Sorry to say Goodbye')
    ->setTemplateType(Mage_Core_Model_Email_Template::TYPE_HTML);
try {
    $model->save();
    $config->saveConfig('newsletter/subscription/un_email_template', $model->getId() , 'stores', $ven);
} catch (Mage_Exception $e) {
    throw $e;
}

$model->loadByCode('Newsletter Unsubscription Success (Moxy English)')
    ->setTemplateCode('Newsletter Unsubscription Success (Moxy English)')
    ->setTemplateText($template_text)
    ->setTemplateSubject('Sorry to say Goodbye')
    ->setTemplateType(Mage_Core_Model_Email_Template::TYPE_HTML);
try {
    $model->save();
    $config->saveConfig('newsletter/subscription/un_email_template', $model->getId() , 'stores', $moxyen);
} catch (Mage_Exception $e) {
    throw $e;
}
//==========================================================================
//==========================================================================
