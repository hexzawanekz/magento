<?php

//==========================================================================
// Message from the CEO Petloft
//==========================================================================

$template_text =  <<<HTML
<div class="mail-container" style="width: 600px; padding: 0px 33px; font-family: Tahoma; font-size: 14px; color: #3c3d41; margin: 0px; font-weight: normal;">
<div class="mail-header" style="width: 95%; min-height: 110px; max-height: 110px; position: relative; margin: 0px auto;">
<div class="mail-logo" style="width: 220px; min-height: 110px; max-height: 110px; margin: 0px auto;"><a href="http://petloft.com/"><img style="width: 220px; max-height: 110px; min-height: 110px;" src="{{skin _area="frontend"  url="images/email/petloft-logo.png"}}" alt="" /></a></div>
<div class="english-below" style="min-height: 16px; max-height: 16px; margin-bottom: 25px; margin-top: -25px; position: absolute; bottom: 0px; line-height: 16px; font-size: 11px; color: #3c3d41;"><span><img style="vertical-align: middle;" src="{{skin _area="frontend"  url="images/email/flag-en.png"}}" alt="" height="16px;" /> Scroll down for English version</span></div>
</div>
<img style="width: 600px;" src="{{skin _area="frontend"  url="images/email/message_from_ceo.png"}}" alt="MESSAGE FROM THE CEO" />
<div class="mail-body" style="margin: 0px auto; width: 95%;">
<p style="margin-top: 0px; margin-bottom: 30px; color: #3c3d41;">เรียน {{var name}}</p>
<p style="margin-top: 0px; margin-bottom: 30px; color: #3c3d41;">ยินดีต้อนรับเข้าสู่ ว้อทส์นิวส์ ผู้นำด้านเว็ปไซต์ออนไลน์ช้อปปิ้งอันดับหนึ่งในประเทศไทย</p>
<p style="margin-top: 0px; margin-bottom: 30px; line-height: 20px; color: #3c3d41;">นับเป็นเวลา 2 ปีแล้วที่เราได้ดำเนินกิจการธุรกิจออนไลน์และให้บริการแก่ลูกค้าจำนวนหลายพันคน พวกเรารู้สึกภาคภูมิใจและเป็นเกียรติอย่างยิ่งที่คุณให้ความไว้วางใจในการเลือกซื้อสินค้ากับเราซึ่งมีให้เลือกมากมายหลายร้อยแบรนด์ดัง พวกเราสัญญาว่าจะให้บริการที่ดีเยี่ยมต่อลูกค้าของเราด้วยบริการจัดส่งสินค้าที่รวดเร็ว และจะคอยติดตามคำสั่งซื้อของท่านจนกว่าจะแน่ใจว่าท่านได้รับสินค้าส่งตรงถึงที่หมายเรียบร้อย</p>
<p style="color: #3c3d41; margin-bottom: 30px;">สัมผัสประสบการณ์ใหม่ในการช้อปปิ้งออนไลน์ที่มีสินค้าหลากหลายและบริการรวดเร็วประทับใจกับว้อทส์นิว</p>
<p style="margin: 10px 0; color: #3c3d41;">ด้วยความนับถือ</p>
<div class="ceo-signature" style="width: 120px; max-height: 80px; min-height: 80px;"><img style="width: 100%;" src="{{skin _area="frontend"  url="images/email/harprem-signature.png"}}" alt="" /></div>
</div>
<div class="navigation-th" style="margin-top: 24px; overflow: hidden;">
<div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 3px solid #006ab4; border-bottom: 3px solid #006ab4; padding: 0px 17px; overflow: hidden;"><span class="nav-item" style="border-right: 2px solid #006ab4; line-height: 33px; font-size: 20px; padding: 0px 28px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://petloft.com/th/dog-food.html">สุนัข</a></span> <span class="nav-item" style="border-right: 2px solid #006ab4; line-height: 33px; font-size: 20px; padding: 0px 28px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://petloft.com/th/cat-food.html">แมว</a></span> <span class="nav-item" style="border-right: 2px solid #006ab4; line-height: 33px; font-size: 20px; padding: 0px 28px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://petloft.com/th/fish-products.html">ปลา</a></span> <span class="nav-item" style="border-right: 2px solid #006ab4; line-height: 33px; font-size: 20px; padding: 0px 28px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://www.petloft.com/th/bird.html">นก</a></span> <span class="nav-item" style="line-height: 33px; font-size: 18px; padding: 0px 28px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://petloft.com/th/small-pets-products.html">สัตว์เลี้ยงขนาดเล็ก</a></span></div>
</div>
<div class="email-footer" style="background: #bbb; margin-top: 8px; color: #fff; padding: 0px 15px;">
<div class="footer-col" style="float: left; padding: 17px 24px; width: 141px;">
<p style="font-size: 16px; margin: 0px 0px 10px 0px;"><img style="width: 18px;" src="{{skin _area="frontend"  url="images/email/mailbox.png"}}" alt="" /> ติดต่อเรา</p>
<p style="font-size: 12px; margin: 0;">02-106-8222</p>
<p style="margin: 0; font-size: 12px;"><a style="font-size: 12px !important; color: #fff; font-weight: normal !important; text-decoration: none;" href="mailto:support@petloft.com" target="_blank">support@petloft.com</a></p>
</div>
<div class="footer-col" style="float: left; padding: 17px 10px; width: 164px;">
<p style="font-size: 16px; margin: 0px 0px 10px 0px;">ติดตามที่</p>
<div class="follow-icons" style="width: 100%;"><a href="https://www.facebook.com/petloftthai"><img style="width: 25px;" src="{{skin _area="frontend"  url="images/email/facebook_follow.png"}}" alt="Facebook fanpage" /></a> <a href="https://twitter.com/PetLoft"><img style="width: 25px;" src="{{skin _area="frontend"  url="images/email/twitter_follow.png"}}" alt="Twitter fanpage" /></a> <a href="https://instagram.com/petloft"><img style="width: 25px;" src="{{skin _area="frontend"  url="images/email/instagram_follow.png"}}" alt="Instagram fanpage" /></a> <a href="https://www.pinterest.com/petloft/"><img style="width: 25px;" src="{{skin _area="frontend"  url="images/email/pinterest_follow.png"}}" alt="Pinterest fanpage" /></a> <a href="https://plus.google.com/u/0/b/113812097105339950445/113812097105339950445/op/iph"><img style="width: 25px;" src="{{skin _area="frontend"  url="images/email/google_follow.png"}}" alt="Google fanpage" /></a></div>
</div>
<div class="footer-col" style="float: left; padding: 17px 24px; width: 141px;">
<p style="font-size: 16px; margin: 0px 0px 5px 0px;">ช่องทางการชำระเงิน</p>
<img style="width: 109px;" src="{{skin _area="frontend"  url="images/email/payment-th.png"}}" alt="ช่องทางการชำระเงิน" /></div>
<div style="clear: both; max-height: 0px;">&nbsp;</div>
</div>
<div class="footer" style="padding: 0px 33px; margin-top: 18px;">
<div class="copyr" style="float: left; font-size: 11px; color: #3c3d41;">&copy;2015 Whatsnew Co.,Ltd. All Rights Reserved</div>
<div class="links" style="float: right; font-size: 11px; color: #3c3d41;"><a style="color: #3c3d41; text-decoration: none; font-weight: normal !important;" href="http://www.petloft.com/th/about-us/"><span>เกี่ยวกับเรา</span></a> | <a style="color: #3c3d41; text-decoration: none; font-weight: normal !important;" href="http://www.petloft.com/th/terms-and-conditions/"><span>ข้อกำหนดและเงื่อนไข</span></a> | <a style="color: #3c3d41; text-decoration: none; font-weight: normal !important;" href="http://www.petloft.com/th/privacy-policy/"><span>ความเป็นส่วนตัว</span></a></div>
</div>
<div style="clear: both; max-height: 0px;">&nbsp;</div>
<hr style="margin: 40px auto; width: 95%;" /><!--English Version-->
<div class="mail-body" style="margin: 0px auto; width: 95%;">
<p style="margin-top: 0px; margin-bottom: 30px; color: #3c3d41;">Dear {{var name}},</p>
<p style="margin-top: 0px; margin-bottom: 30px; color: #3c3d41;">Welcome to Moxy, Thailand's #1 destination for e-shopping!</p>
<p style="margin-top: 0px; margin-bottom: 30px; line-height: 20px; color: #3c3d41;">We proudly operate a family of online stores dedicated to <a style="color: #3c3d41;" href="http://www.moxy.co.th/en/moxy/fashion"><span style="text-decoration: underline;">Fashion &amp; Lifestyle</span></a>, <a style="color: #3c3d41;" href="http://www.lafema.com/en/"><span style="text-decoration: underline;">Beauty</span></a>, <a style="color: #3c3d41;" href="http://www.venbi.com/en/"><span style="text-decoration: underline;">Mom &amp; Baby</span></a>, <a style="color: #3c3d41;" href="http://www.sanoga.com/en/"><span style="text-decoration: underline;">Health</span></a>, and <a style="color: #3c3d41;" href="http://www.petloft.com/en/"><span style="text-decoration: underline;">Pets</span></a>. Each of our websites offers a wide selection of trusted, quality products from your favorite brands.</p>
<p style="margin-top: 0px; margin-bottom: 30px; line-height: 20px; color: #3c3d41;">In our two years of business, thousands of customers have already shopped with us. Our perfected processes can promise to give you the quickest service and delivery possible, keeping in close contact with you at every step of your purchase.</p>
<p style="color: #3c3d41;">Shopping with Moxy is easy, safe and fun.</p>
<p style="padding-bottom: 30px; color: #3c3d41;">We are looking forward to giving you the best experience.</p>
<p style="margin: 10px 0; color: #3c3d41;">Warm regards,</p>
<div class="ceo-signature" style="width: 120px; max-height: 80px; min-height: 80px; padding-bottom: 30px;"><img style="width: 100%;" src="{{skin _area="frontend"  url="images/email/harprem-signature.png"}}" alt="" /></div>
</div>
<div class="navigation-th" style="margin-top: 24px; overflow: hidden;">
<div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 3px solid #006ab4; border-bottom: 3px solid #006ab4; padding: 0px 30px; overflow: hidden;"><span class="nav-item" style="border-right: 2px solid #006ab4; line-height: 33px; font-size: 20px; padding: 0px 29px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://petloft.com/en/dog-food.html">Dogs</a></span> <span class="nav-item" style="border-right: 2px solid #006ab4; line-height: 33px; font-size: 20px; padding: 0px 29px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://petloft.com/en/cat-food.html">Cats</a></span> <span class="nav-item" style="border-right: 2px solid #006ab4; line-height: 33px; font-size: 20px; padding: 0px 29px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://petloft.com/en/fish-products.html">Fish</a></span> <span class="nav-item" style="border-right: 2px solid #006ab4; line-height: 33px; font-size: 20px; padding: 0px 29px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://www.petloft.com/en/bird.html">Bird</a></span> <span class="nav-item" style="line-height: 33px; font-size: 18px; padding: 0px 29px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://petloft.com/en/small-pets-products.html">Small Pets</a></span></div>
</div>
<div class="email-footer" style="background: #bbb; margin-top: 8px; color: #fff; padding: 0px 15px;">
<div class="footer-col" style="float: left; padding: 17px 24px; width: 141px;">
<p style="font-size: 16px; margin: 0px 0px 10px 0px;"><img style="width: 18px;" src="{{skin _area="frontend"  url="images/email/mailbox.png"}}" alt="" /> Contact Us</p>
<p style="font-size: 12px; margin: 0;">02-106-8222</p>
<p style="margin: 0; font-size: 11px;"><a style="font-size: 12px !important; color: #fff; font-weight: normal !important; text-decoration: none;" href="mailto:support@petloft.com" target="_blank">support@petloft.com</a></p>
</div>
<div class="footer-col" style="float: left; padding: 17px 10px; width: 164px;">
<p style="font-size: 16px; margin: 0px 0px 10px 0px;">Follow Us</p>
<div class="follow-icons" style="width: 100%;"><a href="https://www.facebook.com/petloftthai"><img style="width: 25px;" src="{{skin _area="frontend"  url="images/email/facebook_follow.png"}}" alt="Facebook fanpage" /></a> <a href="https://twitter.com/PetLoft"><img style="width: 25px;" src="{{skin _area="frontend"  url="images/email/twitter_follow.png"}}" alt="Twitter fanpage" /></a> <a href="https://instagram.com/petloft"><img style="width: 25px;" src="{{skin _area="frontend"  url="images/email/instagram_follow.png"}}" alt="Instagram fanpage" /></a> <a href="https://www.pinterest.com/petloft/"><img style="width: 25px;" src="{{skin _area="frontend"  url="images/email/pinterest_follow.png"}}" alt="Pinterest fanpage" /></a> <a href="https://plus.google.com/u/0/b/113812097105339950445/113812097105339950445/op/iph"><img style="width: 25px;" src="{{skin _area="frontend"  url="images/email/google_follow.png"}}" alt="Google fanpage" /></a></div>
</div>
<div class="footer-col" style="float: left; padding: 17px 24px; width: 141px;">
<p style="font-size: 16px; margin: 0px 0px 5px 0px;">Payment Options</p>
<img style="width: 109px;" src="{{skin _area="frontend"  url="images/email/payment.png"}}" alt="Payment Options" /></div>
<div style="clear: both; max-height: 0px;">&nbsp;</div>
</div>
<div class="footer" style="padding: 0px 33px; margin-top: 18px;">
<div class="copyr" style="float: left; font-size: 11px;">&copy;2015 Whatsnew Co.,Ltd. All Rights Reserved</div>
<div class="links" style="float: right; font-size: 11px;"><a style="color: #3c3d41; text-decoration: none; font-weight: normal !important;" href="http://www.petloft.com/en/about-us/"><span>About Us</span></a> | <a style="color: #3c3d41; text-decoration: none; font-weight: normal !important;" href="http://www.petloft.com/en/terms-and-conditions/"><span>Term &amp; Conditions</span></a> | <a style="color: #3c3d41; text-decoration: none; font-weight: normal !important;" href="http://www.petloft.com/en/privacy-policy/"><span>Privacy Policy</span></a></div>
</div>
<div style="clear: both; max-height: 0px;">&nbsp;</div>
</div>
HTML;



$model = Mage::getModel('newsletter/template');
$model->loadByCode('MESSAGE FROM THE CEO PETLOFT')
    ->setTemplateCode('Message from the CEO Petloft')
    ->setTemplateText($template_text)
    ->setTemplateSubject('Dear, {{var name}}!')
    ->setTemplateType(Mage_Core_Model_Email_Template::TYPE_HTML)
    ->setTemplateSenderName('Petloft Customer Service')
    ->setTemplateSenderEmail('support@petloft.com');

try {
    $model->save();
} catch (Mage_Exception $e) {
    throw $e;
}
//==========================================================================
//==========================================================================

//==========================================================================
// Message from the CEO Moxy
//==========================================================================

$template_text =  <<<HTML
<div class="mail-container" style="width: 600px; padding: 0px 33px; font-family: Tahoma; font-size: 14px; color: #3c3d41; margin: 0px; font-weight: normal;">
<div class="mail-header" style="width: 95%; min-height: 110px; max-height: 110px; position: relative; margin: 0px auto;">
<div class="mail-logo" style="width: 220px; min-height: 110px; max-height: 110px; margin: 0px auto;"><a href="http://moxy.co.th/"><img style="width: 220px; max-height: 110px; min-height: 110px;" src="{{skin _area="frontend"  url="images/email/moxy-logo.png"}}" alt="" /></a></div>
<div class="english-below" style="min-height: 16px; max-height: 16px; margin-bottom: 25px; margin-top: -25px; position: absolute; bottom: 0px; line-height: 16px; font-size: 11px; color: #3c3d41;"><span><img style="vertical-align: middle;" src="{{skin _area="frontend"  url="images/email/flag-en.png"}}" alt="" height="16px;" /> Scroll down for English version</span></div>
</div>
<img style="width: 600px;" src="{{skin _area="frontend"  url="images/email/message_from_ceo.png"}}" alt="MESSAGE FROM THE CEO" />
<div class="mail-body" style="margin: 0px auto; width: 95%;">
<p style="margin-top: 0px; margin-bottom: 30px; color: #3c3d41;">เรียน {{var name}}</p>
<p style="margin-top: 0px; margin-bottom: 30px; color: #3c3d41;">ยินดีต้อนรับเข้าสู่ ว้อทส์นิวส์ ผู้นำด้านเว็ปไซต์ออนไลน์ช้อปปิ้งอันดับหนึ่งในประเทศไทย</p>
<p style="margin-top: 0px; margin-bottom: 30px; color: #3c3d41; line-height: 20px;">นับเป็นเวลา 2 ปีแล้วที่เราได้ดำเนินกิจการธุรกิจออนไลน์และให้บริการแก่ลูกค้าจำนวนหลายพันคน พวกเรารู้สึกภาคภูมิใจและเป็นเกียรติอย่างยิ่งที่คุณให้ความไว้วางใจในการเลือกซื้อสินค้ากับเราซึ่งมีให้เลือกมากมายหลายร้อยแบรนด์ดัง พวกเราสัญญาว่าจะให้บริการที่ดีเยี่ยมต่อลูกค้าของเราด้วยบริการจัดส่งสินค้าที่รวดเร็ว และจะคอยติดตามคำสั่งซื้อของท่านจนกว่าจะแน่ใจว่าท่านได้รับสินค้าส่งตรงถึงที่หมายเรียบร้อย</p>
<p style="color: #3c3d41; margin-bottom: 30px;">สัมผัสประสบการณ์ใหม่ในการช้อปปิ้งออนไลน์ที่มีสินค้าหลากหลายและบริการรวดเร็วประทับใจกับว้อทส์นิว</p>
<p style="margin: 10px 0; color: #3c3d41;">ด้วยความนับถือ</p>
<div class="ceo-signature" style="width: 120px; max-height: 80px; min-height: 80px;"><img style="width: 100%;" src="{{skin _area="frontend"  url="images/email/harprem-signature.png"}}" alt="" /></div>
</div>
<div class="navigation-th" style="margin-top: 24px; overflow: hidden;">
<div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #ee5191; border-bottom: 2px solid #ee5191; padding: 0px 11px;"><span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 16; padding: 0px 19px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://www.moxy.co.th/th/fashion.html">แฟชั่น</a></span> <span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 16; padding: 0px 19px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://www.moxy.co.th/th/gadgets-electronics.html">แก็ดเจ็ต</a></span> <span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 16; padding: 0px 19px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://www.moxy.co.th/th/accessories.html">เครื่องประดับ</a></span> <span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 16; padding: 0px 19px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://www.moxy.co.th/en/moxy/homeliving">ของแต่งบ้าน</a></span> <span class="nav-item" style="line-height: 33px; font-size: 16; padding: 0px 19px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://www.moxy.co.th/th/lifestyle.html">ไลฟ์สไตล์</a></span></div>
</div>
<div class="email-footer" style="background: #bbb; margin-top: 8px; color: #fff; padding: 0px 15px;">
<div class="footer-col" style="float: left; padding: 17px 24px; width: 141px;">
<p style="font-size: 16px; margin: 0px 0px 10px 0px;"><img style="width: 18px;" src="{{skin _area="frontend"  url="images/email/mailbox.png"}}" alt="" /> ติดต่อเรา</p>
<p style="font-size: 12px; margin: 0;">02-106-8222</p>
<p style="margin: 0; font-size: 12px;"><a style="font-size: 12px !important; color: #fff; text-decoration: none !important; font-weight: normal !important;" href="mailto:support@moxyst.com" target="_blank">support@moxyst.com</a></p>
</div>
<div class="footer-col" style="float: left; padding: 17px 10px; width: 164px;">
<p style="font-size: 16px; margin: 0px 0px 10px 0px;">ติดตามที่</p>
<div class="follow-icons" style="width: 100%;"><a href="https://www.facebook.com/moxyst"><img style="width: 25px;" src="{{skin _area="frontend"  url="images/email/facebook_follow.png"}}" alt="Facebook fanpage" /></a> <a href="https://twitter.com/moxyst"><img style="width: 25px;" src="{{skin _area="frontend"  url="images/email/twitter_follow.png"}}" alt="Twitter fanpage" /></a> <a href="https://webstagra.ms/moxyst/"><img style="width: 25px;" src="{{skin _area="frontend"  url="images/email/instagram_follow.png"}}" alt="Instagram fanpage" /></a> <a href="https://www.pinterest.com/moxyst/"><img style="width: 25px;" src="{{skin _area="frontend"  url="images/email/pinterest_follow.png"}}" alt="Pinterest fanpage" /></a> <a href="https://plus.google.com/+Moxyst"><img style="width: 25px;" src="{{skin _area="frontend"  url="images/email/google_follow.png"}}" alt="Google fanpage" /></a></div>
</div>
<div class="footer-col" style="float: left; padding: 17px 24px; width: 141px;">
<p style="font-size: 16px; margin: 0px 0px 10px 0px;">ช่องทางการชำระเงิน</p>
<img style="width: 109px;" src="{{skin _area="frontend"  url="images/email/payment-th.png"}}" alt="ช่องทางการชำระเงิน" /></div>
<div style="clear: both; max-height: 0px;">&nbsp;</div>
</div>
<div class="footer" style="padding: 0px 33px; margin-top: 18px;">
<div class="copyr" style="float: left; font-size: 11px; color: #3c3d41;">&copy;2015 Whatsnew Co.,Ltd. All Rights Reserved</div>
<div class="links" style="float: right; font-size: 11px;"><a style="color: #3c3d41; text-decoration: none; font-weight: normal !important;" href="http://www.moxy.co.th/th/about-us/"><span>เกี่ยวกับเรา</span></a> | <a style="color: #3c3d41; text-decoration: none; font-weight: normal !important;" href="http://www.moxy.co.th/th/terms-and-conditions/"><span>ข้อกำหนดและเงื่อนไข</span></a> | <a style="color: #3c3d41; text-decoration: none; font-weight: normal !important;" href="http://www.moxy.co.th/th/privacy-policy/"><span>ความเป็นส่วนตัว</span></a></div>
</div>
<div style="clear: both; max-height: 0px;">&nbsp;</div>
<hr style="margin: 40px auto; width: 95%;" /><!--English Version-->
<div class="mail-body" style="margin: 0px auto; width: 95%;">
<p style="margin-top: 0px; margin-bottom: 30px; color: #3c3d41;">Dear {{var name}},</p>
<p style="margin-top: 0px; margin-bottom: 30px; color: #3c3d41;">Welcome to Moxy, Thailand's #1 destination for e-shopping!</p>
<p style="margin-top: 0px; margin-bottom: 30px; color: #3c3d41; line-height: 20px;">We proudly operate a family of online stores dedicated to Fashion &amp; Lifestyle, Beauty, Mom &amp; Baby, Health, and Pets. (link every vertical to website) Each of our websites offers a wide selection of trusted, quality products from your favorite brands.</p>
<p style="margin-top: 0px; margin-bottom: 30px; color: #3c3d41; line-height: 20px;">In our two years of business, thousands of customers have already shopped with us. Our perfected processes can promise to give you the quickest service and delivery possible, keeping in close contact with you at every step of your purchase.</p>
<p style="color: #3c3d41;">Shopping with Moxy is easy, safe and fun.</p>
<p style="padding-bottom: 30px; color: #3c3d41;">We are looking forward to giving you the best experience.</p>
<p style="margin: 10px 0; color: #3c3d41;">Warm regards,</p>
<div class="ceo-signature" style="width: 112px; max-height: 80px; min-height: 80px;"><img style="width: 100%;" src="{{skin _area="frontend"  url="images/email/harprem-signature.png"}}" alt="" /></div>
</div>
<div class="navigation-th" style="margin-top: 24px; overflow: hidden;">
<div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #ee5191; border-bottom: 2px solid #ee5191; padding: 0px 11px;"><span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 16; padding: 0px 19px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://www.moxy.co.th/en/fashion.html">Fashion</a></span> <span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 16; padding: 0px 19px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://www.moxy.co.th/en/gadgets-electronics.html">Gadgets</a></span> <span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 16; padding: 0px 19px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://www.moxy.co.th/en/accessories.html">Accessories</a></span> <span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 16; padding: 0px 19px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://www.moxy.co.th/en/moxy/homeliving">Home Decor</a></span> <span class="nav-item" style="line-height: 33px; font-size: 16; padding: 0px 19px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://www.moxy.co.th/en/lifestyle.html">Lifestyle</a></span></div>
</div>
<div class="email-footer" style="background: #bbb; margin-top: 8px; color: #fff; padding: 0px 15px;">
<div class="footer-col" style="float: left; padding: 17px 24px; width: 141px;">
<p style="font-size: 16px; margin: 0px 0px 10px 0px;"><img style="width: 18px;" src="{{skin _area="frontend"  url="images/email/mailbox.png"}}" alt="" /> Contact Us</p>
<p style="font-size: 12px; margin: 0;">02-106-8222</p>
<p style="margin: 0; font-size: 12px !important; color: #fff; font-weight: normal !important;"><a style="font-size: 12px !important; color: #fff; text-decoration: none !important; font-weight: normal !important;" href="mailto:support@moxyst.com" target="_blank">support@moxyst.com</a></p>
</div>
<div class="footer-col" style="float: left; padding: 17px 10px; width: 164px;">
<p style="font-size: 16px; margin: 0px 0px 10px 0px;">Follow Us</p>
<div class="follow-icons" style="width: 100%;"><a href="https://www.facebook.com/moxyst"><img style="width: 25px;" src="{{skin _area="frontend"  url="images/email/facebook_follow.png"}}" alt="Facebook fanpage" /></a> <a href="https://twitter.com/moxyst"><img style="width: 25px;" src="{{skin _area="frontend"  url="images/email/twitter_follow.png"}}" alt="Twitter fanpage" /></a> <a href="https://webstagra.ms/moxyst/"><img style="width: 25px;" src="{{skin _area="frontend"  url="images/email/instagram_follow.png"}}" alt="Instagram fanpage" /></a> <a href="https://www.pinterest.com/moxyst/"><img style="width: 25px;" src="{{skin _area="frontend"  url="images/email/pinterest_follow.png"}}" alt="Pinterest fanpage" /></a> <a href="https://plus.google.com/+Moxyst"><img style="width: 25px;" src="{{skin _area="frontend"  url="images/email/google_follow.png"}}" alt="Google fanpage" /></a></div>
</div>
<div class="footer-col" style="float: left; padding: 17px 24px; width: 141px;">
<p style="font-size: 16px; margin: 0px 0px 5px 0px;">Payment Options</p>
<img style="width: 109px;" src="{{skin _area="frontend"  url="images/email/payment.png"}}" alt="Payment Options" /></div>
<div style="clear: both; max-height: 0px;">&nbsp;</div>
</div>
<div class="footer" style="padding: 0px 33px; margin-top: 18px;">
<div class="copyr" style="float: left; font-size: 11px;">&copy;2015 Whatsnew Co.,Ltd. All Rights Reserved</div>
<div class="links" style="float: right; font-size: 11px;"><a style="color: #3c3d41; text-decoration: none; font-weight: normal !important;" href="http://www.moxy.co.th/en/about-us/"><span>About Us</span></a> | <a style="color: #3c3d41; text-decoration: none; font-weight: normal !important;" href="http://www.moxy.co.th/en/terms-and-conditions/"><span>Term &amp; Conditions</span></a> | <a style="color: #3c3d41; text-decoration: none; font-weight: normal !important;" href="http://www.moxy.co.th/en/privacy-policy/"><span>Privacy Policy</span></a></div>
</div>
<div style="clear: both; max-height: 0px;">&nbsp;</div>
</div>
HTML;



$model = Mage::getModel('newsletter/template');
$model->loadByCode('MESSAGE FROM THE CEO MOXY')
    ->setTemplateCode('Message from the CEO Moxy')
    ->setTemplateText($template_text)
    ->setTemplateSubject('Dear, {{var name}}!')
    ->setTemplateType(Mage_Core_Model_Email_Template::TYPE_HTML)
    ->setTemplateSenderName('Moxy Customer Service')
    ->setTemplateSenderEmail('support@moxyst.com');

try {
    $model->save();
} catch (Mage_Exception $e) {
    throw $e;
}
//==========================================================================
//==========================================================================

//==========================================================================
// Message from the CEO Sanoga
//==========================================================================

$template_text =  <<<HTML
<div class="mail-container" style="width: 600px; padding: 0px 33px; font-family: Tahoma; font-size: 14px; color: #3c3d41; margin: 0px; font-weight: normal;">
<div class="mail-header" style="width: 95%; min-height: 110px; max-height: 110px; position: relative; margin: 0px auto;">
<div class="mail-logo" style="width: 220px; min-height: 110px; max-height: 110px; margin: 0px auto;"><a href="http://sanoga.com/"><img style="width: 220px; max-height: 110px; min-height: 110px;" src="{{skin _area="frontend" url="images/email/sanoga-logo.png"}}" alt="" /></a></div>
<div class="english-below" style="min-height: 16px; max-height: 16px; margin-bottom: 25px; margin-top: -25px; color: #3c3d41; position: absolute; bottom: 0px; line-height: 16px; font-size: 11px;"><span><img style="vertical-align: middle;" src="{{skin _area="frontend"  url="images/email/flag-en.png"}}" alt="" height="16px;" /> Scroll down for English version</span></div>
</div>
<img style="width: 600px;" src="{{skin _area="frontend"  url="images/email/message_from_ceo.png"}}" alt="MESSAGE FROM THE CEO" />
<div class="mail-body" style="margin: 0px auto; width: 95%;">
<p style="margin-top: 0px; margin-bottom: 30px; color: #3c3d41;">เรียน {{var name}}</p>
<p style="margin-top: 0px; margin-bottom: 30px; color: #3c3d41;">ยินดีต้อนรับเข้าสู่ ว้อทส์นิวส์ ผู้นำด้านเว็ปไซต์ออนไลน์ช้อปปิ้งอันดับหนึ่งในประเทศไทย</p>
<p style="margin-top: 0px; margin-bottom: 30px; color: #3c3d41; line-height: 20px;">นับเป็นเวลา 2 ปีแล้วที่เราได้ดำเนินกิจการธุรกิจออนไลน์และให้บริการแก่ลูกค้าจำนวนหลายพันคน พวกเรารู้สึกภาคภูมิใจและเป็นเกียรติอย่างยิ่งที่คุณให้ความไว้วางใจในการเลือกซื้อสินค้ากับเราซึ่งมีให้เลือกมากมายหลายร้อยแบรนด์ดัง พวกเราสัญญาว่าจะให้บริการที่ดีเยี่ยมต่อลูกค้าของเราด้วยบริการจัดส่งสินค้าที่รวดเร็ว และจะคอยติดตามคำสั่งซื้อของท่านจนกว่าจะแน่ใจว่าท่านได้รับสินค้าส่งตรงถึงที่หมายเรียบร้อย</p>
<p style="color: #3c3d41; padding-bottom: 30px;">สัมผัสประสบการณ์ใหม่ในการช้อปปิ้งออนไลน์ที่มีสินค้าหลากหลายและบริการรวดเร็วประทับใจกับว้อทส์นิว</p>
<p style="margin: 10px 0; color: #3c3d41;">ด้วยความนับถือ</p>
<div class="ceo-signature" style="width: 120px; max-height: 80px; min-height: 80px;"><img style="width: 100%;" src="{{skin _area="frontend"  url="images/email/harprem-signature.png"}}" alt="" /></div>
</div>
<div class="navigation-th" style="margin-top: 24px; text-align: center; overflow: hidden;">
<div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #6abc45; border-bottom: 2px solid #6abc45; padding: 0px 0px; overflow: hidden;"><span class="nav-item" style="border-right: 2px solid #6abc45; line-height: 33px; font-size: 14; padding: 0px 12px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://www.sanoga.com/th/health-care.html">ดูแลสุขภาพ</a></span> <span class="nav-item" style="border-right: 2px solid #6abc45; line-height: 33px; font-size: 14; padding: 0px 12px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://www.sanoga.com/th/beauty.html">ความงาม</a></span> <span class="nav-item" style="border-right: 2px solid #6abc45; line-height: 33px; font-size: 14; padding: 0px 12px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://www.sanoga.com/th/workout-fitness.html">ออกกำลังกาย-ฟิตเนส</a></span> <span class="nav-item" style="border-right: 2px solid #6abc45; line-height: 33px; font-size: 14; padding: 0px 12px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://www.sanoga.com/th/weight-loss.html">อาหารเสริมลดน้ำหนัก</a></span> <span class="nav-item" style="line-height: 33px; font-size: 14; padding: 0px 12px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://www.sanoga.com/th/sexual-well-being.html">สุขภาพ-เซ็กส์</a></span></div>
</div>
<div class="email-footer" style="background: #bbb; margin-top: 8px; color: #fff; padding: 0px 15px;">
<div class="footer-col" style="float: left; padding: 17px 24px; width: 141px;">
<p style="font-size: 16px; margin: 0px 0px 10px 0px;"><img style="width: 18px;" src="{{skin _area="frontend" url="images/email/mailbox.png"}}" alt="" /> ติดต่อเรา</p>
<p style="font-size: 12px; margin: 0;">02-106-8222</p>
<p style="margin: 0; font-size: 12px;"><a style="font-size: 12px !important; color: #fff; font-weight: normal !important;" href="mailto:support@sanoga.com" target="_blank">support@sanoga.com</a></p>
</div>
<div class="footer-col" style="float: left; padding: 17px 10px; width: 164px;">
<p style="font-size: 16px; margin: 0px 0px 10px 0px;">ติดตามที่</p>
<div class="follow-icons" style="width: 100%;"><a href="https://www.facebook.com/SanogaThailand"><img style="width: 25px;" src="{{skin _area="frontend" url="images/email/facebook_follow.png"}}" alt="Facebook fanpage" /></a> <a href="https://twitter.com/sanoga_thailand"><img style="width: 25px;" src="{{skin _area="frontend" url="images/email/twitter_follow.png"}}" alt="Twitter fanpage" /></a> <a href="#"><img style="width: 25px;" src="{{skin _area="frontend" url="images/email/instagram_follow.png"}}" alt="Instagram fanpage" /></a> <a href="https://www.pinterest.com/sanoga/"><img style="width: 25px;" src="{{skin _area="frontend" url="images/email/pinterest_follow.png"}}" alt="Pinterest fanpage" /></a> <a href="https://plus.google.com/114390899173698565828/"><img style="width: 25px;" src="{{skin _area="frontend" url="images/email/google_follow.png"}}" alt="Google fanpage" /></a></div>
</div>
<div class="footer-col" style="float: left; padding: 17px 24px; width: 141px;">
<p style="font-size: 16px; margin: 0px 0px 5px 0px;">ช่องทางการชำระเงิน</p>
<img style="width: 109px;" src="{{skin _area="frontend" url="images/email/payment-th.png"}}" alt="ช่องทางการชำระเงิน" /></div>
<div style="clear: both; max-height: 0px;">&nbsp;</div>
</div>
<div class="footer" style="padding: 0px 33px; margin-top: 18px;">
<div class="copyr" style="float: left; font-size: 11px; color: #3c3d41;">&copy;2015 Whatsnew Co.,Ltd. All Rights Reserved</div>
<div class="links" style="float: right; font-size: 11px;"><a style="color: #3c3d41; text-decoration: none; font-weight: normal !important;" href="http://www.sanoga.com/th/about-us/"><span>เกี่ยวกับเรา</span></a> | <a style="color: #3c3d41; text-decoration: none; font-weight: normal !important;" href="http://www.sanoga.com/th/terms-and-conditions/"><span>ข้อกำหนดและเงื่อนไข</span></a> | <a style="color: #3c3d41; text-decoration: none; font-weight: normal !important;" href="http://www.sanoga.com/th/privacy-policy/"><span>ความเป็นส่วนตัว</span></a></div>
</div>
<div style="clear: both; max-height: 0px;">&nbsp;</div>
<hr style="margin: 40px auto; width: 95%;" /><!--English Version-->
<div class="mail-body" style="margin: 0px auto; width: 95%;">
<p style="margin-top: 0px; margin-bottom: 30px; color: #3c3d41;">Dear {{var name}},</p>
<p style="margin-top: 0px; margin-bottom: 30px; color: #3c3d41;">Welcome to Moxy, Thailand's #1 destination for e-shopping!</p>
<p style="margin-top: 0px; margin-bottom: 30px; color: #3c3d41; line-height: 20px;">We proudly operate a family of online stores dedicated to Fashion &amp; Lifestyle, Beauty, Mom &amp; Baby, Health, and Pets. (link every vertical to website) Each of our websites offers a wide selection of trusted, quality products from your favorite brands.</p>
<p style="margin-top: 0px; margin-bottom: 30px; color: #3c3d41; line-height: 20px;">In our two years of business, thousands of customers have already shopped with us. Our perfected processes can promise to give you the quickest service and delivery possible, keeping in close contact with you at every step of your purchase.</p>
<p style="color: #3c3d41;">Shopping with Moxy is easy, safe and fun.</p>
<p style="padding-bottom: 30px; color: #3c3d41;">We are looking forward to giving you the best experience.</p>
<p style="margin: 10px 0; color: #3c3d41;">Warm regards,</p>
<div class="ceo-signature" style="width: 120px; max-height: 80px; min-height: 80px;"><img style="width: 100%;" src="{{skin _area="frontend"  url="images/email/harprem-signature.png"}}" alt="" /></div>
</div>
<div class="navigation-th" style="margin-top: 24px; text-align: center; overflow: hidden;">
<div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #6abc45; border-bottom: 2px solid #6abc45; padding: 0px 0px; overflow: hidden;"><span class="nav-item" style="border-right: 2px solid #6abc45; line-height: 33px; font-size: 14; padding: 0px 12px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://www.sanoga.com/en/health-care.html">Health Care</a></span> <span class="nav-item" style="border-right: 2px solid #6abc45; line-height: 33px; font-size: 14; padding: 0px 12px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://www.sanoga.com/en/beauty.html">Beauty</a></span> <span class="nav-item" style="border-right: 2px solid #6abc45; line-height: 33px; font-size: 14; padding: 0px 12px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://www.sanoga.com/en/workout-fitness.html">Workout &amp; Fitness</a></span> <span class="nav-item" style="border-right: 2px solid #6abc45; line-height: 33px; font-size: 22px; padding: 0px 12px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://www.sanoga.com/en/weight-loss.html">Weight Loss</a></span> <span class="nav-item" style="line-height: 33px; font-size: 14; padding: 0px 12px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://www.sanoga.com/en/sexual-well-being.html">Sexual Well-Being</a></span></div>
</div>
<div class="email-footer" style="background: #bbb; margin-top: 8px; color: #fff; padding: 0px 15px;">
<div class="footer-col" style="float: left; padding: 17px 24px; width: 141px;">
<p style="font-size: 16px; margin: 0px 0px 10px 0px;"><img style="width: 18px;" src="{{skin _area="frontend" url="images/email/mailbox.png"}}" alt="" /> Contact Us</p>
<p style="font-size: 12px; margin: 0;">02-106-8222</p>
<p style="margin: 0; font-size: 12px;"><a style="font-size: 12px !important; color: #fff; font-weight: normal !important;" href="mailto:support@sanoga.com" target="_blank">support@sanoga.com</a></p>
</div>
<div class="footer-col" style="float: left; padding: 17px 10px; width: 164px;">
<p style="font-size: 16px; margin: 0px 0px 10px 0px;">Follow Us</p>
<div class="follow-icons" style="width: 100%;"><a href="https://www.facebook.com/SanogaThailand"><img style="width: 25px;" src="{{skin _area="frontend" url="images/email/facebook_follow.png"}}" alt="Facebook fanpage" /></a> <a href="https://twitter.com/sanoga_thailand"><img style="width: 25px;" src="{{skin _area="frontend" url="images/email/twitter_follow.png"}}" alt="Twitter fanpage" /></a> <a href="#"><img style="width: 25px;" src="{{skin _area="frontend" url="images/email/instagram_follow.png"}}" alt="Instagram fanpage" /></a> <a href="https://www.pinterest.com/sanoga/"><img style="width: 25px;" src="{{skin _area="frontend" url="images/email/pinterest_follow.png"}}" alt="Pinterest fanpage" /></a> <a href="https://plus.google.com/114390899173698565828/"><img style="width: 25px;" src="{{skin _area="frontend" url="images/email/google_follow.png"}}" alt="Google fanpage" /></a></div>
</div>
<div class="footer-col" style="float: left; padding: 17px 24px; width: 141px;">
<p style="font-size: 16px; margin: 0px 0px 5px 0px;">Payment Options</p>
<img style="width: 109px;" src="{{skin _area="frontend" url="images/email/payment.png"}}" alt="Payment Options" /></div>
<div style="clear: both; max-height: 0px;">&nbsp;</div>
</div>
<div class="footer" style="padding: 0px 33px; margin-top: 18px;">
<div class="copyr" style="float: left; font-size: 11px;">&copy;2015 Whatsnew Co.,Ltd. All Rights Reserved</div>
<div class="links" style="float: right; font-size: 11px;"><a style="color: #3c3d41; text-decoration: none; font-weight: normal !important;" href="http://www.sanoga.com/en/about-us/"><span>About Us</span></a> | <a style="color: #3c3d41; text-decoration: none; font-weight: normal !important;" href="http://www.sanoga.com/en/terms-and-conditions/"><span>Term &amp; Conditions</span></a> | <a style="color: #3c3d41; text-decoration: none; font-weight: normal !important;" href="http://www.sanoga.com/en/privacy-policy/"><span>Privacy Policy</span></a></div>
</div>
<div style="clear: both; max-height: 0px;">&nbsp;</div>
</div>
HTML;



$model = Mage::getModel('newsletter/template');
$model->loadByCode('MESSAGE FROM THE CEO SANOGA')
    ->setTemplateCode('Message from the CEO Sanoga')
    ->setTemplateText($template_text)
    ->setTemplateSubject('Dear, {{var name}}!')
    ->setTemplateType(Mage_Core_Model_Email_Template::TYPE_HTML)
    ->setTemplateSenderName('Sanoga Customer Service')
    ->setTemplateSenderEmail('support@sanoga.com');

try {
    $model->save();
} catch (Mage_Exception $e) {
    throw $e;
}
//==========================================================================
//==========================================================================

//==========================================================================
// Message from the CEO Venbi
//==========================================================================

$template_text =  <<<HTML
<div class="mail-container" style="width: 600px; padding: 0px 33px; font-family: Tahoma; font-size: 14px; color: #3c3d41; margin: 0px; font-weight: normal;">
<div class="mail-header" style="width: 95%; min-height: 110px; max-height: 110px; position: relative; margin: 0px auto;">
<div class="mail-logo" style="width: 220px; min-height: 110px; max-height: 110px; margin: 0px auto;"><a href="http://venbi.com/"><img style="width: 220px; max-height: 110px; min-height: 110px;" src="{{skin _area="frontend"  url="images/email/venbi-logo.png"}}" alt="" /></a></div>
<div class="english-below" style="min-height: 16px; max-height: 16px; margin-bottom: 25px; color: #3c3d41; margin-top: -25px; position: absolute; bottom: 0px; line-height: 16px; font-size: 11px;"><span><img style="vertical-align: middle;" src="{{skin _area="frontend"  url="images/email/flag-en.png"}}" alt="" height="16px;" /> Scroll down for English version</span></div>
</div>
<img style="width: 600px;" src="{{skin _area="frontend"  url="images/email/message_from_ceo.png"}}" alt="MESSAGE FROM THE CEO" />
<div class="mail-body" style="margin: 0px auto; width: 95%;">
<p style="margin-top: 0px; margin-bottom: 30px; color: #3c3d41;">เรียน {{var name}}</p>
<p style="margin-top: 0px; margin-bottom: 30px; color: #3c3d41;">ยินดีต้อนรับเข้าสู่ ว้อทส์นิวส์ ผู้นำด้านเว็ปไซต์ออนไลน์ช้อปปิ้งอันดับหนึ่งในประเทศไทย</p>
<p style="margin-top: 0px; margin-bottom: 30px; color: #3c3d41; line-height: 20px;">นับเป็นเวลา 2 ปีแล้วที่เราได้ดำเนินกิจการธุรกิจออนไลน์และให้บริการแก่ลูกค้าจำนวนหลายพันคน พวกเรารู้สึกภาคภูมิใจและเป็นเกียรติอย่างยิ่งที่คุณให้ความไว้วางใจในการเลือกซื้อสินค้ากับเราซึ่งมีให้เลือกมากมายหลายร้อยแบรนด์ดัง พวกเราสัญญาว่าจะให้บริการที่ดีเยี่ยมต่อลูกค้าของเราด้วยบริการจัดส่งสินค้าที่รวดเร็ว และจะคอยติดตามคำสั่งซื้อของท่านจนกว่าจะแน่ใจว่าท่านได้รับสินค้าส่งตรงถึงที่หมายเรียบร้อย</p>
<p style="color: #3c3d41; padding-bottom: 30px;">สัมผัสประสบการณ์ใหม่ในการช้อปปิ้งออนไลน์ที่มีสินค้าหลากหลายและบริการรวดเร็วประทับใจกับว้อทส์นิว</p>
<p style="margin: 10px 0; color: #3c3d41;">ด้วยความนับถือ</p>
<div class="ceo-signature" style="width: 112px; max-height: 80px; min-height: 80px;"><img style="width: 100%;" src="{{skin _area="frontend"  url="images/email/harprem-signature.png"}}" alt="" /></div>
</div>
<div class="navigation-th" style="margin-top: 24px; overflow: hidden;">
<div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #f5ea14; border-bottom: 2px solid #f5ea14; padding: 0px 0px; overflow: hidden;"><span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 6px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://www.venbi.com/th/formula-nursing.html">นมและอุปกรณ์</a></span> <span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 5px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://www.venbi.com/th/diapers.html">ผ้าอ้อมสำเร็จรูป</a></span> <span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 5px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://www.venbi.com/th/bath-baby-care.html">ผลิตภัณฑ์อาบน้ำและเบบี้แคร์</a></span> <span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 5px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://www.venbi.com/th/toys.html">ของเล่น</a></span> <span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 5px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://www.venbi.com/th/clothing.html">เสื้อผ้า</a></span> <span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 5px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://www.venbi.com/th/gear.html">เตียงและรถเข็น</a></span> <span class="nav-item" style="line-height: 33px; font-size: 11; padding: 0px 6px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://www.venbi.com/th/mom-maternity.html">แม่-การตั้งครรภ์</a></span></div>
</div>
<div class="email-footer" style="background: #bbb; margin-top: 8px; color: #fff; padding: 0px 15px;">
<div class="footer-col" style="float: left; padding: 17px 24px; width: 141px;">
<p style="font-size: 16px; margin: 0px 0px 10px 0px;"><img style="width: 18px;" src="{{skin _area="frontend"  url="images/email/mailbox.png"}}" alt="" /> ติดต่อเรา</p>
<p style="font-size: 12px; margin: 0;">02-106-8222</p>
<p style="margin: 0; font-size: 12px;"><a style="font-size: 12px !important; color: #fff; font-weight: normal !important;" href="mailto:support@venbi.com" target="_blank">support@venbi.com</a></p>
</div>
<div class="footer-col" style="float: left; padding: 17px 10px; width: 164px;">
<p style="font-size: 16px; margin: 0px 0px 10px 0px;">ติดตามที่</p>
<div class="follow-icons" style="width: 100%;"><a href="https://www.facebook.com/venbithai"><img style="width: 25px;" src="{{skin _area="frontend"  url="images/email/facebook_follow.png"}}" alt="Facebook fanpage" /></a> <a href="https://twitter.com/venbithai"><img style="width: 25px;" src="{{skin _area="frontend"  url="images/email/twitter_follow.png"}}" alt="Twitter fanpage" /></a> <a href="http://webstagr.am/venbi_thailand"><img style="width: 25px;" src="{{skin _area="frontend"  url="images/email/instagram_follow.png"}}" alt="Instagram fanpage" /></a> <a href="https://www.pinterest.com/VenbiThailand/"><img style="width: 25px;" src="{{skin _area="frontend"  url="images/email/pinterest_follow.png"}}" alt="Pinterest fanpage" /></a> <a href="https://plus.google.com/+VenbiThai"><img style="width: 25px;" src="{{skin _area="frontend"  url="images/email/google_follow.png"}}" alt="Google fanpage" /></a></div>
</div>
<div class="footer-col" style="float: left; padding: 17px 24px; width: 141px;">
<p style="font-size: 16px; margin: 0px 0px 5px 0px;">ช่องทางการชำระเงิน</p>
<img style="width: 109px;" src="{{skin _area="frontend"  url="images/email/payment-th.png"}}" alt="ช่องทางการชำระเงิน" /></div>
<div style="clear: both; max-height: 0px;">&nbsp;</div>
</div>
<div class="footer" style="padding: 0px 33px; margin-top: 18px;">
<div class="copyr" style="float: left; font-size: 11px;">&copy;2015 Whatsnew Co.,Ltd. All Rights Reserved</div>
<div class="links" style="float: right; font-size: 11px;"><a style="color: #3c3d41; text-decoration: none; font-weight: normal !important;" href="http://www.venbi.com/th/about-us/"><span>เกี่ยวกับเรา</span></a> | <a style="color: #3c3d41; text-decoration: none; font-weight: normal !important;" href="http://www.venbi.com/th/terms-and-conditions/"><span>ข้อกำหนดและเงื่อนไข</span></a> | <a style="color: #3c3d41; text-decoration: none; font-weight: normal !important;" href="http://www.venbi.com/th/privacy-policy/"><span>ความเป็นส่วนตัว</span></a></div>
</div>
<div style="clear: both; max-height: 0px;">&nbsp;</div>
<hr style="margin: 40px auto; width: 95%;" /><!--English Version-->
<div class="mail-body" style="margin: 0px auto; width: 95%;">
<p style="margin-top: 0px; margin-bottom: 30px; color: #3c3d41;">Dear {{var name}},</p>
<p style="margin-top: 0px; margin-bottom: 30px; color: #3c3d41;">Welcome to Moxy, Thailand's #1 destination for e-shopping!</p>
<p style="margin-top: 0px; margin-bottom: 30px; color: #3c3d41; line-height: 20px;">We proudly operate a family of online stores dedicated to Fashion &amp; Lifestyle, Beauty, Mom &amp; Baby, Health, and Pets. (link every vertical to website) Each of our websites offers a wide selection of trusted, quality products from your favorite brands.</p>
<p style="margin-top: 0px; margin-bottom: 30px; color: #3c3d41; line-height: 20px;">In our two years of business, thousands of customers have already shopped with us. Our perfected processes can promise to give you the quickest service and delivery possible, keeping in close contact with you at every step of your purchase.</p>
<p style="color: #3c3d41;">Shopping with Moxy is easy, safe and fun.</p>
<p style="padding-bottom: 30px; color: #3c3d41;">We are looking forward to giving you the best experience.</p>
<p style="margin: 10px 0; color: #3c3d41;">Warm regards,</p>
<div class="ceo-signature" style="width: 112px; max-height: 80px; min-height: 80px;"><img style="width: 100%;" src="{{skin _area="frontend"  url="images/email/harprem-signature.png"}}" alt="" /></div>
</div>
<div class="navigation-th" style="margin-top: 24px; overflow: hidden;">
<div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #f5ea14; border-bottom: 2px solid #f5ea14; padding: 0px 0px; overflow: hidden;"><span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 5px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://www.venbi.com/en/formula-nursing.html">Formula &amp; Nursing</a></span> <span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 5px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://www.venbi.com/en/diapers.html">Diapers</a></span> <span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 5px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://www.venbi.com/en/bath-baby-care.html">Bath &amp; Baby Care</a></span> <span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 5px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://www.venbi.com/en/toys.html">Toys</a></span> <span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 5px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://www.venbi.com/en/clothing.html">Clothing</a></span> <span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 5px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://www.venbi.com/en/gear.html">Gear</a></span> <span class="nav-item" style="line-height: 33px; font-size: 11; padding: 0px 6px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://www.venbi.com/en/mom-maternity.html">Mom &amp; Maternity</a></span></div>
</div>
<div class="email-footer" style="background: #bbb; margin-top: 8px; color: #fff; padding: 0px 15px;">
<div class="footer-col" style="float: left; padding: 17px 24px; width: 141px;">
<p style="font-size: 16px; margin: 0px 0px 10px 0px;"><img style="width: 18px;" src="{{skin _area="frontend"  url="images/email/mailbox.png"}}" alt="" /> Contact Us</p>
<p style="font-size: 12px; margin: 0;">02-106-8222</p>
<p style="margin: 0; font-size: 12px;"><a style="font-size: 12px !important; color: #fff; font-weight: normal !important;" href="mailto:support@venbi.com" target="_blank">support@venbi.com</a></p>
</div>
<div class="footer-col" style="float: left; padding: 17px 10px; width: 164px;">
<p style="font-size: 16px; margin: 0px 0px 10px 0px;">Follow Us</p>
<div class="follow-icons" style="width: 100%;"><a href="https://www.facebook.com/venbithai"><img style="width: 25px;" src="{{skin _area="frontend"  url="images/email/facebook_follow.png"}}" alt="Facebook fanpage" /></a> <a href="https://twitter.com/venbithai"><img style="width: 25px;" src="{{skin _area="frontend"  url="images/email/twitter_follow.png"}}" alt="Twitter fanpage" /></a> <a href="http://webstagr.am/venbi_thailand"><img style="width: 25px;" src="{{skin _area="frontend"  url="images/email/instagram_follow.png"}}" alt="Instagram fanpage" /></a> <a href="https://www.pinterest.com/VenbiThailand/"><img style="width: 25px;" src="{{skin _area="frontend"  url="images/email/pinterest_follow.png"}}" alt="Pinterest fanpage" /></a> <a href="https://plus.google.com/+VenbiThai"><img style="width: 25px;" src="{{skin _area="frontend"  url="images/email/google_follow.png"}}" alt="Google fanpage" /></a></div>
</div>
<div class="footer-col" style="float: left; padding: 17px 24px; width: 141px;">
<p style="font-size: 16px; margin: 0px 0px 5px 0px;">Payment Options</p>
<img style="width: 109px;" src="{{skin _area="frontend"  url="images/email/payment.png"}}" alt="Payment Options" /></div>
<div style="clear: both; max-height: 0px;">&nbsp;</div>
</div>
<div class="footer" style="padding: 0px 33px; margin-top: 18px;">
<div class="copyr" style="float: left; font-size: 11px;">&copy;2015 Whatsnew Co.,Ltd. All Rights Reserved</div>
<div class="links" style="float: right; font-size: 11px;"><a style="color: #3c3d41; text-decoration: none; font-weight: normal !important;" href="http://www.venbi.com/en/about-us/"><span>About Us</span></a> | <a style="color: #3c3d41; text-decoration: none; font-weight: normal !important;" href="http://www.venbi.com/en/terms-and-conditions/"><span>Term &amp; Conditions</span></a> | <a style="color: #3c3d41; text-decoration: none; font-weight: normal !important;" href="http://www.venbi.com/en/privacy-policy/"><span>Privacy Policy</span></a></div>
</div>
<div style="clear: both; max-height: 0px;">&nbsp;</div>
</div>
HTML;



$model = Mage::getModel('newsletter/template');
$model->loadByCode('MESSAGE FROM THE CEO VENBI')
    ->setTemplateCode('Message from the CEO Venbi')
    ->setTemplateText($template_text)
    ->setTemplateSubject('Dear, {{var name}}!')
    ->setTemplateType(Mage_Core_Model_Email_Template::TYPE_HTML)
    ->setTemplateSenderName('Venbi Customer Service')
    ->setTemplateSenderEmail('support@venbi.com');

try {
    $model->save();
} catch (Mage_Exception $e) {
    throw $e;
}
//==========================================================================
//==========================================================================

//==========================================================================
// Message from the CEO Lafema
//==========================================================================

$template_text =  <<<HTML
<div class="mail-container" style="width: 600px; padding: 0px 33px; font-family: Tahoma; font-size: 14px; color: #3c3d41; margin: 0px; font-weight: normal;">
<div class="mail-header" style="width: 95%; min-height: 110px; max-height: 110px; position: relative; margin: 0px auto;">
<div class="mail-logo" style="width: 220px; min-height: 110px; max-height: 110px; margin: 0px auto;"><a href="http://lafema.com/"><img style="width: 220px; max-height: 110px; min-height: 110px;" src="{{skin _area="frontend"  url="images/email/lafema-logo.png"}}" alt="" /></a></div>
<div class="english-below" style="min-height: 16px; max-height: 16px; margin-bottom: 25px; margin-top: -25px; color: #3c3d41; position: absolute; bottom: 0px; line-height: 16px; font-size: 11px;"><span><img style="vertical-align: middle;" src="{{skin _area="frontend"  url="images/email/flag-en.png"}}" alt="" height="16px;" /> Scroll down for English version</span></div>
</div>
<img style="width: 600px;" src="{{skin _area="frontend"  url="images/email/message_from_ceo.png"}}" alt="MESSAGE FROM THE CEO" />
<div class="mail-body" style="margin: 0px auto; width: 95%;">
<p style="margin-top: 0px; margin-bottom: 30px; color: #3c3d41;">เรียน {{var name}}</p>
<p style="margin-top: 0px; margin-bottom: 30px; color: #3c3d41;">ยินดีต้อนรับเข้าสู่ ว้อทส์นิวส์ ผู้นำด้านเว็ปไซต์ออนไลน์ช้อปปิ้งอันดับหนึ่งในประเทศไทย</p>
<p style="margin-top: 0px; margin-bottom: 30px; color: #3c3d41; line-height: 20px;">นับเป็นเวลา 2 ปีแล้วที่เราได้ดำเนินกิจการธุรกิจออนไลน์และให้บริการแก่ลูกค้าจำนวนหลายพันคน พวกเรารู้สึกภาคภูมิใจและเป็นเกียรติอย่างยิ่งที่คุณให้ความไว้วางใจในการเลือกซื้อสินค้ากับเราซึ่งมีให้เลือกมากมายหลายร้อยแบรนด์ดัง พวกเราสัญญาว่าจะให้บริการที่ดีเยี่ยมต่อลูกค้าของเราด้วยบริการจัดส่งสินค้าที่รวดเร็ว และจะคอยติดตามคำสั่งซื้อของท่านจนกว่าจะแน่ใจว่าท่านได้รับสินค้าส่งตรงถึงที่หมายเรียบร้อย</p>
<p style="color: #3c3d41; padding-bottom: 30px;">สัมผัสประสบการณ์ใหม่ในการช้อปปิ้งออนไลน์ที่มีสินค้าหลากหลายและบริการรวดเร็วประทับใจกับว้อทส์นิว</p>
<p style="margin: 10px 0; color: #3c3d41;">ด้วยความนับถือ,</p>
<div class="ceo-signature" style="width: 120px; max-height: 80px; min-height: 80px;"><img style="width: 100%;" src="{{skin _area="frontend"  url="images/email/harprem-signature.png"}}" alt="" /></div>
</div>
<div class="navigation-th" style="margin-top: 24px; text-align: center; overflow: hidden;">
<div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #f5a2b2; border-bottom: 2px solid #f5a2b2; padding: 0px 0px;"><span class="nav-item" style="border-right: 2px solid #f5a2b2; line-height: 33px; font-size: 16; padding: 0px 18px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://www.lafema.com/th/makeup.html">เมคอัพ</a></span> <span class="nav-item" style="border-right: 2px solid #f5a2b2; line-height: 33px; font-size: 16; padding: 0px 18px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://www.lafema.com/th/skincare.html">ดูแลผิวหน้า</a></span> <span class="nav-item" style="border-right: 2px solid #f5a2b2; line-height: 33px; font-size: 16; padding: 0px 18px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://www.lafema.com/th/bath-body-hair.html">ดูแลผิวกาย-ผม</a></span> <span class="nav-item" style="border-right: 2px solid #f5a2b2; line-height: 33px; font-size: 16; padding: 0px 18px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://www.lafema.com/th/fragrance.html">น้ำหอม</a></span> <span class="nav-item" style="line-height: 33px; font-size: 16; padding: 0px 12px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://www.lafema.com/th/men.html">สำหรับผู้ชาย</a></span></div>
</div>
<div class="email-footer" style="min-height: 82px; max-heigh: 82px; background: #bbb; margin-top: 8px; color: #fff; padding: 0px 15px;">
<div class="footer-col" style="float: left; padding: 17px 24px; width: 141px;">
<p style="font-size: 16px; margin: 0px 0px 10px 0px;"><img style="width: 18px;" src="{{skin _area="frontend"  url="images/email/mailbox.png"}}" alt="" /> ติดต่อเรา</p>
<p style="font-size: 12px; margin: 0;">02-106-8222</p>
<p style="margin: 0; font-size: 12px;"><a style="font-size: 12px !important; color: #fff; font-weight: normal !important;" href="mailto:support@lafema.com" target="_blank">support@lafema.com</a></p>
</div>
<div class="footer-col" style="float: left; padding: 17px 10px; width: 164px;">
<p style="font-size: 16px; margin: 0px 0px 5px 0px;">ติดตามที่</p>
<div class="follow-icons" style="width: 100%;"><a href="https://www.facebook.com/lafemathai"><img style="width: 25px;" src="{{skin _area="frontend"  url="images/email/facebook_follow.png"}}" alt="Facebook fanpage" /></a> <a href="https://twitter.com/lafemath"><img style="width: 25px;" src="{{skin _area="frontend"  url="images/email/twitter_follow.png"}}" alt="Twitter fanpage" /></a> <a href="https://instagram.com/lafema.th/"><img style="width: 25px;" src="{{skin _area="frontend"  url="images/email/instagram_follow.png"}}" alt="Instagram fanpage" /></a> <a href="https://www.pinterest.com/lafema_thai/"><img style="width: 25px;" src="{{skin _area="frontend"  url="images/email/pinterest_follow.png"}}" alt="Pinterest fanpage" /></a> <a href="https://plus.google.com/103314178165403417268/"><img style="width: 25px;" src="{{skin _area="frontend"  url="images/email/google_follow.png"}}" alt="Google fanpage" /></a></div>
</div>
<div class="footer-col" style="float: left; padding: 17px 24px; width: 141px;">
<p style="font-size: 16px; margin: 0px 0px 10px 0px;">ช่องทางการชำระเงิน</p>
<img style="width: 109px;" src="{{skin _area="frontend"  url="images/email/payment-th.png"}}" alt="ช่องทางการชำระเงิน" /></div>
<div style="clear: both; max-height: 0px;">&nbsp;</div>
</div>
<div class="footer" style="padding: 0px 33px; margin-top: 18px;">
<div class="copyr" style="float: left; font-size: 11px;">&copy;2015 Whatsnew Co.,Ltd. All Rights Reserved</div>
<div class="links" style="float: right; font-size: 11px;"><a style="color: #3c3d41; text-decoration: none; font-weight: normal !important;" href="http://www.lafema.com/th/about-us/"><span>เกี่ยวกับเรา</span></a> | <a style="color: #3c3d41; text-decoration: none; font-weight: normal !important;" href="http://www.lafema.com/th/terms-and-conditions/"><span>ข้อกำหนดและเงื่อนไข</span></a> | <a style="color: #3c3d41; text-decoration: none; font-weight: normal !important;" href="http://www.lafema.com/th/privacy-policy/"><span>ความเป็นส่วนตัว</span></a></div>
</div>
<div style="clear: both; max-height: 0px;">&nbsp;</div>
<hr style="margin: 40px auto; width: 95%;" /><!--English Version-->
<div class="mail-body" style="margin: 0px auto; width: 95%;">
<p style="margin-top: 0px; margin-bottom: 30px; color: #3c3d41;">Dear {{var name}}</p>
<p style="margin-top: 0px; margin-bottom: 30px; color: #3c3d41;">Welcome to Moxy, Thailand's #1 destination for e-shopping!</p>
<p style="margin-top: 0px; margin-bottom: 30px; color: #3c3d41; line-height: 20px;">We proudly operate a family of online stores dedicated to Fashion &amp; Lifestyle, Beauty, Mom &amp; Baby, Health, and Pets. (link every vertical to website) Each of our websites offers a wide selection of trusted, quality products from your favorite brands.</p>
<p style="margin-top: 0px; margin-bottom: 30px; color: #3c3d41; line-height: 20px;">In our two years of business, thousands of customers have already shopped with us. Our perfected processes can promise to give you the quickest service and delivery possible, keeping in close contact with you at every step of your purchase.</p>
<p style="color: #3c3d41;">Shopping with Moxy is easy, safe and fun.</p>
<p style="padding-bottom: 30px; color: #3c3d41;">We are looking forward to giving you the best experience.</p>
<p style="margin: 10px 0; color: #3c3d41;">Warm regards,</p>
<div class="ceo-signature" style="width: 120px; max-height: 80px; min-height: 80px;"><img style="width: 100%;" src="{{skin _area="frontend"  url="images/email/harprem-signature.png"}}" alt="" /></div>
</div>
<div class="navigation-th" style="margin-top: 24px; text-align: center; overflow: hidden;">
<div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #f5a2b2; border-bottom: 2px solid #f5a2b2; padding: 0px 0px;"><span class="nav-item" style="border-right: 2px solid #f5a2b2; line-height: 33px; font-size: 16; padding: 0px 18px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://www.lafema.com/en/makeup.html">Makeup</a></span> <span class="nav-item" style="border-right: 2px solid #f5a2b2; line-height: 33px; font-size: 16; padding: 0px 18px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://www.lafema.com/en/skincare.html">Skin Care</a></span> <span class="nav-item" style="border-right: 2px solid #f5a2b2; line-height: 33px; font-size: 16; padding: 0px 18px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://www.lafema.com/en/bath-body-hair.html">Bath &amp; Body-Hair</a></span> <span class="nav-item" style="border-right: 2px solid #f5a2b2; line-height: 33px; font-size: 16; padding: 0px 18px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://www.lafema.com/en/fragrance.html">Fragrance</a></span> <span class="nav-item" style="line-height: 33px; font-size: 16; padding: 0px 18px;"><a style="color: #3c3d41; text-decoration: none; font-size: 22px !important;" href="http://www.lafema.com/en/men.html">Men</a></span></div>
</div>
<div class="email-footer" style="min-height: 82px; max-heigh: 82px; background: #bbb; margin-top: 8px; color: #fff; padding: 0px 15px;">
<div class="footer-col" style="float: left; padding: 17px 24px; width: 141px;">
<p style="font-size: 16px; margin: 0px 0px 10px 0px;"><img style="width: 18px;" src="{{skin _area="frontend"  url="images/email/mailbox.png"}}" alt="" /> Contact Us</p>
<p style="font-size: 12px; margin: 0;">02-106-8222</p>
<p style="margin: 0; font-size: 12px;"><a style="font-size: 12px !important; color: #fff; font-weight: normal !important;" href="mailto:support@lafema.com" target="_blank">support@lafema.com</a></p>
</div>
<div class="footer-col" style="float: left; padding: 17px 10px; width: 164px;">
<p style="font-size: 16px; margin: 0px 0px 10px 0px;">Follow Us</p>
<div class="follow-icons" style="width: 100%;"><a href="https://www.facebook.com/lafemathai"><img style="width: 25px;" src="{{skin _area="frontend"  url="images/email/facebook_follow.png"}}" alt="Facebook fanpage" /></a> <a href="https://twitter.com/lafemath"><img style="width: 25px;" src="{{skin _area="frontend"  url="images/email/twitter_follow.png"}}" alt="Twitter fanpage" /></a> <a href="https://instagram.com/lafema.th/"><img style="width: 25px;" src="{{skin _area="frontend"  url="images/email/instagram_follow.png"}}" alt="Instagram fanpage" /></a> <a href="https://www.pinterest.com/lafema_thai/"><img style="width: 25px;" src="{{skin _area="frontend"  url="images/email/pinterest_follow.png"}}" alt="Pinterest fanpage" /></a> <a href="https://plus.google.com/103314178165403417268/"><img style="width: 25px;" src="{{skin _area="frontend"  url="images/email/google_follow.png"}}" alt="Google fanpage" /></a></div>
</div>
<div class="footer-col" style="float: left; padding: 17px 24px; width: 141px;">
<p style="font-size: 16px; margin: 0px 0px 5px 0px;">Payment Options</p>
<img style="width: 109px;" src="{{skin _area="frontend"  url="images/email/payment.png"}}" alt="Payment Options" /></div>
<div style="clear: both; max-height: 0px;">&nbsp;</div>
</div>
<div class="footer" style="padding: 0px 33px; margin-top: 18px;">
<div class="copyr" style="float: left; font-size: 11px;">&copy;2015 Whatsnew Co.,Ltd. All Rights Reserved</div>
<div class="links" style="float: right; font-size: 11px;"><a style="color: #3c3d41; text-decoration: none; font-weight: normal !important;" href="http://www.lafema.com/en/about-us/"><span>About Us</span></a> | <a style="color: #3c3d41; text-decoration: none; font-weight: normal !important;" href="http://www.lafema.com/en/terms-and-conditions/"><span>Term &amp; Conditions</span></a> | <a style="color: #3c3d41; text-decoration: none; font-weight: normal !important;" href="http://www.lafema.com/en/privacy-policy/"><span>Privacy Policy</span></a></div>
</div>
<div style="clear: both; max-height: 0px;">&nbsp;</div>
</div>
HTML;



$model = Mage::getModel('newsletter/template');
$model->loadByCode('MESSAGE FROM THE CEO LAFEMA')
    ->setTemplateCode('Message from the CEO Lafema')
    ->setTemplateText($template_text)
    ->setTemplateSubject('Dear, {{var name}}!')
    ->setTemplateType(Mage_Core_Model_Email_Template::TYPE_HTML)
    ->setTemplateSenderName('Lafema Customer Service')
    ->setTemplateSenderEmail('support@lafema.com');

try {
    $model->save();
} catch (Mage_Exception $e) {
    throw $e;
}
//==========================================================================
//==========================================================================

