<?php

// init Magento
Mage::app();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
$config = new Mage_Core_Model_Config();
// get store ids
$en     = Mage::getModel('core/store')->load('en', 'code')->getId();// Petloft
$th     = Mage::getModel('core/store')->load('th', 'code')->getId();
$len    = Mage::getModel('core/store')->load('len', 'code')->getId();// Lafema
$lth    = Mage::getModel('core/store')->load('lth', 'code')->getId();
$ven    = Mage::getModel('core/store')->load('ven', 'code')->getId();// Venbi
$vth    = Mage::getModel('core/store')->load('vth', 'code')->getId();
$sen    = Mage::getModel('core/store')->load('sen', 'code')->getId();// Sanoga
$sth    = Mage::getModel('core/store')->load('sth', 'code')->getId();
$moxyen = Mage::getModel('core/store')->load('moxyen', 'code')->getId();// Moxy
$moxyth = Mage::getModel('core/store')->load('moxyth', 'code')->getId();


$template = Mage::getModel('core/email_template');

//==========================================================================
// ORDER CONFIRMATION PETLOFT
//==========================================================================
$template_text =
    <<<HTML
<meta charset="utf-8"/>
<div class="mail-container" style="width:600px; padding:0px 33px; font-family:  'Trebuchet MS', Tahoma; font-size: 14px; color: #3c3d41; margin: 0px;">
    <div class="mail-header" style="width:95%; min-height: 110px; max-height: 110px; position: relative; margin:0px auto;">
        <div class="mail-logo" style="width:220px; min-height: 110px; max-height: 110px; margin:0px auto;">
            <a href="http://petloft.com/"><img src="{{skin _area="frontend"  url="images/email/petloft-logo.png"}}" style="width:220px; max-height: 110px; min-height: 110px;" /></a>
        </div>

        <div class="english-below" style="min-height: 16px; max-height: 16px; position: absolute; bottom: 0px; line-height: 16px;">
            <span><img src="{{skin _area="frontend"  url="images/email/flag-en.png"}}" height="16px;"/> Scroll down for English version</span>
        </div>
    </div>

    <img style="width: 100%; margin-top:10px;" src="{{skin _area="frontend"  url="images/email/thank_for_your_order_petloft.png"}}" alt="Thank for your order" />

    <div class="mail-body" style="margin:50px auto; width: 95%;">
        <p>เรียน คุณ {{htmlescape var=\$order.getCustomerName()}},</p>
        <br/>

        <p>ขอขอบคุณสำหรับการสั่งซื้อจาก Moxy</p>
        <p>เลขที่ใบสั่งซื้อของคุณคือ (9-digit order number)</p>
        <br>

        <p>ทางเราจะจัดส่งสินค้าให้ถึงคุณภายใน 1-3 วันทำการสำหรับกรุงเทพฯและภายใน 3-5</p>
        <p>วันทำการสำหรับต่างจังหวัดทุกจังหวัดทั่วประเทศ</p>
        <br>

        <p>คุณสามารถเช็คสถานะการสั่งซื้อของคุณที่ : <a href="{{store url="customer/account/"}}" style="color:#28438c; text-decoration:none;">{{store url="customer/account/"}}</a></p>
        <p>หากคุณมีคำถามหรือข้อสงสัยใดๆสามารถติดต่อเราได้ที่ 02-106-8222</p>
        <br>

        <p>Warm Regards,</p>
        <p>Moxy Team</p>
    </div>

    <!--Billing-->
    <div class="order-detail" style="width: 95%; margin:0 auto; font-size: 12px;">
        <div class="billing-info" style="float: left; width: 49%;">
            <div class="heading" style="background: #0e80c0; font-size: 12; color: #fff; padding: 3px 0px; float: left; width: 100%;"><div style="margin: 0px 5px; text-transform: uppercase;">ข้อมูลในการออกใบเสร็จ</div></div>
            <div class="billing-content" style="float: left; width: 100%; background: #e7e7e7; min-height: 175px;">
                <div style="margin: 0px 5px;">
            {{var order.getBillingAddress().format('html')}}
                </div>
            </div>
        </div>

        <div class="payment-method" style="float: right; width: 49%; font-size: 12px;">
            <div class="heading" style="background: #0e80c0; font-size: 12; color: #fff; padding: 3px 0px; float: left; width: 100%;"><div style="margin: 0px 5px; text-transform: uppercase;">วิธีการชำระเงิน</div></div>
            <div class="billing-content" style="float: left; width: 100%; background: #e7e7e7; min-height: 175px;">
                <div style="margin: 0px 5px;">
                      {{var payment_html}}
                </div>
            </div>
        </div>

        <div style="clear: both;"></div>
    </div>

  {{depend order.getIsNotVirtual()}}
    <!--Shipping-->
    <div class="order-detail" style="width: 95%; margin:15px auto; font-size: 12px;">
        <div class="billing-info" style="float: left; width: 49%;">
            <div class="heading" style="background: #0e80c0; font-size: 12; color: #fff; padding: 3px 0px; float: left; width: 100%;"><div style="margin: 0px 5px; text-transform: uppercase;">ข้อมูลในการจัดส่งสินค้า</div></div>
            <div class="billing-content" style="float: left; width: 100%; background: #e7e7e7; min-height: 175px;">
                <div style="margin: 0px 5px;">
                      {{var order.getShippingAddress().format('html')}}
                </div>
            </div>
        </div>

        <div class="payment-method" style="float: right; width: 49%; font-size: 12px;">
            <div class="heading" style="background: #0e80c0; font-size: 12; color: #fff; padding: 3px 0px; float: left; width: 100%;"><div style="margin: 0px 5px; text-transform: uppercase;">วิธีการส่งสินค้า</div></div>
            <div class="billing-content" style="float: left; width: 100%; background: #e7e7e7; min-height: 175px;">
                <div style="margin: 0px 5px;">
                     {{var order.getShippingDescription()}}
                </div>
            </div>
        </div>

        <div style="clear: both;"></div>
    </div>
       {{/depend}}

    <!--Order Item-->
    {{layout handle="sales_email_order_items" order=\$order language="th"}}
<p style="font-size:12px; margin:0 0 10px 0">{{var order.getEmailCustomerNote()}}</p>
    <!--/End OrderItem-->

	<div class="navigation-th" style="margin-top: 24px; overflow:hidden;">
		<div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #006ab4; border-bottom: 2px solid #006ab4; padding: 0px 17px; overflow: hidden;">
			<span class="nav-item" style="border-right: 2px solid #006ab4; line-height: 33px; font-size: 18px; padding: 0px 28px;"><a href="http://petloft.com/th/dog-food.html" style="color: #3c3d41; text-decoration: none;">สุนัข</a></span>
			<span class="nav-item" style="border-right: 2px solid #006ab4; line-height: 33px; font-size: 18px; padding: 0px 28px;"><a href="http://petloft.com/th/cat-food.html" style="color: #3c3d41; text-decoration: none;">แมว</a></span>
			<span class="nav-item" style="border-right: 2px solid #006ab4; line-height: 33px; font-size: 18px; padding: 0px 28px;"><a href="http://petloft.com/th/fish-products.html" style="color: #3c3d41; text-decoration: none;">ปลา</a></span>
			<span class="nav-item" style="border-right: 2px solid #006ab4; line-height: 33px; font-size: 18px; padding: 0px 28px;"><a href="http://www.petloft.com/th/bird.html" style="color: #3c3d41; text-decoration: none;">นก</a></span>
			<span class="nav-item" style="line-height: 33px; font-size: 18px; padding: 0px 28px;"><a href="http://petloft.com/th/small-pets-products.html" style="color: #3c3d41; text-decoration: none;">สัตว์เลี้ยงขนาดเล็ก</a></span>
		</div>
	</div>

    <div class="email-footer" style="background: #bbb; margin-top: 8px; color: #fff; padding: 0px 15px;">
        <div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
            <p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px"><img src="{{skin _area="frontend"  url="images/email/mailbox.png"}}" style="width:18px;"/> ติดต่อเรา</p>
            <p style="font-size: 10px; margin: 0;">02-106-8222</p>
            <p style="margin: 0; font-size: 10px;">support@petloft.com</p>
        </div>

        <div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
            <p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">ติดตามที่</p>
            <div class="follow-icons" style="width: 100%">
    <a href="https://www.facebook.com/petloftthai"><img src="{{skin _area="frontend"  url="images/email/facebook_follow.png"}}" style="width: 25px;" alt="Facebook fanpage"></a>
                <a href="https://twitter.com/PetLoft"><img src="{{skin _area="frontend"  url="images/email/twitter_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
                <a href="https://instagram.com/petloft"><img src="{{skin _area="frontend"  url="images/email/instagram_follow.png"}}" style="width: 25px;" alt="Instagram fanpage"></a>
                <a href="https://www.pinterest.com/petloft/"><img src="{{skin _area="frontend"  url="images/email/pinterest_follow.png"}}" style="width: 25px;" alt="Pinterest fanpage"></a>
                <a href="https://plus.google.com/110158894140498579470"><img src="{{skin _area="frontend"  url="images/email/google_follow.png"}}" style="width: 25px;" alt="Google fanpage"></a>
            </div>
        </div>

        <div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
            <p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">ช่องทางการชำระเงิน</p>
            <img src="{{skin _area="frontend"  url="images/email/payment-th.png"}}" alt="ช่องทางการชำระเงิน" style="width: 109px;">
        </div>

        <div class="clear" style="clear:both;"></div>
    </div>

    <div class="footer" style="padding: 0px 33px; margin-top: 18px;">
        <div class="copyr" style="float: left; font-size: 12px;">&copy;2015 Whatsnew Co.,Ltd. All Rights Reserved</div>
        <div class="links" style="float: right; font-size: 12px;">
            <a href="http://www.petloft.com/th/about-us/" style="color: #3c3d41;"><span>เกี่ยวกับเรา</span></a> |
            <a href="http://www.petloft.com/th/terms-and-conditions/" style="color: #3c3d41;"><span>ข้อกำหนดและเงื่อนไข</span></a> |
            <a href="http://www.petloft.com/th/privacy-policy/" style="color: #3c3d41;"><span>ความเป็นส่วนตัว</span></a>
        </div>
    </div>
    <div style="clear:both"></div>
    <hr style="margin: 40px auto; width: 95%;">

    <!--English Version-->
    <div class="mail-body" style="margin:50px auto; width: 95%;">
        <p>Dear {{htmlescape var=\$order.getCustomerName()}},</p>

        <br/>
        <p>Thank you for your purchasing at Moxy.</p>
        <p>Your order number is (9-digit order number)</p>
        <br>

        <p>We deliver in Bangkok in 1-3 working days.</p>
        <p>We deliver in order regions of Thailand in 3-5 working days.</p>
        <br>

        <p>Track the status of your order here : <a href="{{store url="customer/account/"}}" style="color:#28438c; text-decoration:none;">{{store url="customer/account/"}}</a></p>
        <p> You can also reach our customer service at +662-106-8222</p>
        <br>

        <p>Warm Regards,</p>
        <p>Moxy Team</p>
    </div>

    <!--Billing-->
    <div class="order-detail" style="width: 95%; margin:0 auto; font-size: 12px;">
        <div class="billing-info" style="float: left; width: 49%;">
            <div class="heading" style="background: #0e80c0; font-size: 12; color: #fff; padding: 3px 0px; float: left; width: 100%;"><div style="margin: 0px 5px; text-transform: uppercase;">Billing Information</div></div>
            <div class="billing-content" style="float: left; width: 100%; background: #e7e7e7; min-height: 175px;">
                <div style="margin: 0px 5px;">
                      {{var order.getBillingAddress().format('html')}}
                </div>
            </div>
        </div>

        <div class="payment-method" style="float: right; width: 49%; font-size: 12px;">
            <div class="heading" style="background: #0e80c0; font-size: 12; color: #fff; padding: 3px 0px; float: left; width: 100%;"><div style="margin: 0px 5px; text-transform: uppercase;">Payment Method</div></div>
            <div class="billing-content" style="float: left; width: 100%; background: #e7e7e7; min-height: 175px;">
                <div style="margin: 0px 5px;">
                     {{var payment_html}}
                </div>
            </div>
        </div>

        <div style="clear: both;"></div>
    </div>
     {{depend order.getIsNotVirtual()}}
    <!--Shipping-->
    <div class="order-detail" style="width: 95%; margin:15px auto; font-size: 12px;">
        <div class="billing-info" style="float: left; width: 49%;">
            <div class="heading" style="background: #0e80c0; font-size: 12; color: #fff; padding: 3px 0px; float: left; width: 100%;"><div style="margin: 0px 5px; text-transform: uppercase;">Shipping Information</div></div>
            <div class="billing-content" style="float: left; width: 100%; background: #e7e7e7; min-height: 175px;">
                <div style="margin: 0px 5px;">
                     {{var order.getShippingAddress().format('html')}}
                </div>
            </div>
        </div>

        <div class="payment-method" style="float: right; width: 49%; font-size: 12px;">
            <div class="heading" style="background: #0e80c0; font-size: 12; color: #fff; padding: 3px 0px; float: left; width: 100%;"><div style="margin: 0px 5px; text-transform: uppercase;">Shipping Method</div></div>
            <div class="billing-content" style="float: left; width: 100%; background: #e7e7e7; min-height: 175px;">
                <div style="margin: 0px 5px;">
                     {{var order.getShippingDescription()}}
                </div>
            </div>
        </div>

        <div style="clear: both;"></div>
    </div>
    {{/depend}}
    <!--Order Item-->
            {{layout handle="sales_email_order_items" order=\$order language="en"}}
            <p style="font-size:12px; margin:0 0 10px 0">{{var order.getEmailCustomerNote()}}</p>
    <!--/End OrderItem-->

	<div class="navigation-th" style="margin-top: 24px; overflow:hidden;">
		<div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #006ab4; border-bottom: 2px solid #006ab4; padding: 0px 30px; overflow: hidden;">
			<span class="nav-item" style="border-right: 2px solid #006ab4; line-height: 33px; font-size: 18px; padding: 0px 29px;"><a href="http://petloft.com/en/dog-food.html" style="color: #3c3d41; text-decoration: none;">Dogs</a></span>
			<span class="nav-item" style="border-right: 2px solid #006ab4; line-height: 33px; font-size: 18px; padding: 0px 29px;"><a href="http://petloft.com/en/cat-food.html" style="color: #3c3d41; text-decoration: none;">Cats</a></span>
			<span class="nav-item" style="border-right: 2px solid #006ab4; line-height: 33px; font-size: 18px; padding: 0px 29px;"><a href="http://petloft.com/en/fish-products.html" style="color: #3c3d41; text-decoration: none;">Fish</a></span>
			<span class="nav-item" style="border-right: 2px solid #006ab4; line-height: 33px; font-size: 18px; padding: 0px 29px;"><a href="http://www.petloft.com/en/bird.html" style="color: #3c3d41; text-decoration: none;">Bird</a></span>
			<span class="nav-item" style="line-height: 33px; font-size: 18px; padding: 0px 29px;"><a href="http://petloft.com/en/small-pets-products.html" style="color: #3c3d41; text-decoration: none;">Small Pets</a></span>
		</div>
	</div>
    <div class="email-footer" style="background: #bbb; margin-top: 8px; color: #fff; padding: 0px 15px;">
        <div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
            <p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px"><img src="{{skin _area="frontend"  url="images/email/mailbox.png"}}" style="width:18px;"/> Contact Us</p>
            <p style="font-size: 10px; margin: 0;">02-106-8222</p>
            <p style="margin: 0; font-size: 10px;">support@petloft.com</p>
        </div>

        <div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
            <p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">Follow Us</p>
            <div class="follow-icons" style="width: 100%">
    <a href="https://www.facebook.com/petloftthai"><img src="{{skin _area="frontend"  url="images/email/facebook_follow.png"}}" style="width: 25px;" alt="Facebook fanpage"></a>
                <a href="https://twitter.com/PetLoft"><img src="{{skin _area="frontend"  url="images/email/twitter_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
                <a href="https://instagram.com/petloft"><img src="{{skin _area="frontend"  url="images/email/instagram_follow.png"}}" style="width: 25px;" alt="Instagram fanpage"></a>
                <a href="https://www.pinterest.com/petloft/"><img src="{{skin _area="frontend"  url="images/email/pinterest_follow.png"}}" style="width: 25px;" alt="Pinterest fanpage"></a>
                <a href="https://plus.google.com/110158894140498579470"><img src="{{skin _area="frontend"  url="images/email/google_follow.png"}}" style="width: 25px;" alt="Google fanpage"></a>
            </div>
        </div>

        <div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
            <p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">Payment Options</p>
            <img src="{{skin _area="frontend"  url="images/email/payment.png"}}" alt="Payment Options" style="width: 109px;">
        </div>

        <div class="clear" style="clear:both;"></div>
    </div>

    <div class="footer" style="padding: 0px 33px; margin-top: 18px;">
        <div class="copyr" style="float: left; font-size: 12px;">&copy;2015 Whatsnew Co.,Ltd. All Rights Reserved</div>
        <div class="links" style="float: right; font-size: 12px;">
            <a href="http://www.petloft.com/en/about-us/" style="color: #3c3d41;"><span>About Us</span></a> |
            <a href="http://www.petloft.com/en/terms-and-conditions/" style="color: #3c3d41;"><span>Term & Conditions</span></a> |
            <a href="http://www.petloft.com/en/privacy-policy/" style="color: #3c3d41;"><span>Privacy Policy</span></a>
        </div>
    </div>
    <div style="clear:both"></div>
</div>
HTML;

/** @var Mage_Core_Model_Email_Template $model */
$model = Mage::getModel('core/email_template');
$model->loadByCode('Order Confirmation Petloft')
    ->setTemplateCode('Order Confirmation Petloft')
    ->setTemplateText($template_text)
    ->setTemplateSubject('{{var store.getFrontendName()}}: New Order # {{var order.increment_id}}')
    ->setTemplateType(Mage_Core_Model_Email_Template::TYPE_HTML);

try {
    $model->save();
    $config->saveConfig('sales_email/order/template', $model->getId() , 'stores', $en);
    $config->saveConfig('sales_email/order/guest_template', $model->getId() , 'stores', $en);
    $config->saveConfig('sales_email/order/template', $model->getId() , 'stores', $th);
    $config->saveConfig('sales_email/order/guest_template', $model->getId() , 'stores', $th);
} catch (Mage_Exception $e) {
    throw $e;
}
//==========================================================================
//==========================================================================

//==========================================================================
// ORDER CONFIRMATION MOXY
//==========================================================================
$template_text =
    <<<HTML
<meta charset="utf-8"/>
<div class="mail-container" style="width:600px; padding:0px 33px; font-family:  'Trebuchet MS', Tahoma; font-size: 14px; color: #3c3d41; margin: 0px;">
    <div class="mail-header" style="width:95%; min-height: 110px; max-height: 110px; position: relative; margin:0px auto;">
        <div class="mail-logo" style="width:220px; min-height: 110px; max-height: 110px; margin:0px auto;">
		       	<a href="http://moxy.co.th/"><img src="{{skin _area="frontend"  url="images/email/moxy-logo.png"}}" style="width:220px; max-height: 110px; min-height: 110px;" /></a>
	       </div>

        <div class="english-below" style="min-height: 16px; max-height: 16px; position: absolute; bottom: 0px; line-height: 16px;">
            <span><img src="{{skin _area="frontend"  url="images/email/flag-en.png"}}" height="16px;"/> Scroll down for English version</span>
        </div>
    </div>

    <img style="width: 100%; margin-top:10px;" src="{{skin _area="frontend"  url="images/email/thank_for_your_order_moxy.png"}}" alt="Thank for your order" />

    <div class="mail-body" style="margin:50px auto; width: 95%;">
        <p>เรียน คุณ {{htmlescape var=\$order.getCustomerName()}},</p>
        <br/>

        <p>ขอขอบคุณสำหรับการสั่งซื้อจาก Moxy</p>
        <p>เลขที่ใบสั่งซื้อของคุณคือ (9-digit order number)</p>
        <br>

        <p>ทางเราจะจัดส่งสินค้าให้ถึงคุณภายใน 1-3 วันทำการสำหรับกรุงเทพฯและภายใน 3-5</p>
        <p>วันทำการสำหรับต่างจังหวัดทุกจังหวัดทั่วประเทศ</p>
        <br>

        <p>คุณสามารถเช็คสถานะการสั่งซื้อของคุณที่ : <a href="{{store url="customer/account/"}}" style="color:#28438c; text-decoration:none;">{{store url="customer/account/"}}</a></p>
        <p>หากคุณมีคำถามหรือข้อสงสัยใดๆสามารถติดต่อเราได้ที่ 02-106-8222</p>
        <br>

        <p>Warm Regards,</p>
        <p>Moxy Team</p>
    </div>

    <!--Billing-->
    <div class="order-detail" style="width: 95%; margin:0 auto; font-size: 12px;">
        <div class="billing-info" style="float: left; width: 49%;">
            <div class="heading" style="background: #ed5192; font-size: 12; color: #fff; padding: 3px 0px; float: left; width: 100%;"><div style="margin: 0px 5px; text-transform: uppercase;">ข้อมูลในการออกใบเสร็จ</div></div>
            <div class="billing-content" style="float: left; width: 100%; background: #e7e7e7; min-height: 175px;">
                <div style="margin: 0px 5px;">
            {{var order.getBillingAddress().format('html')}}
                </div>
            </div>
        </div>

        <div class="payment-method" style="float: right; width: 49%; font-size: 12px;">
            <div class="heading" style="background: #ed5192; font-size: 12; color: #fff; padding: 3px 0px; float: left; width: 100%;"><div style="margin: 0px 5px; text-transform: uppercase;">วิธีการชำระเงิน</div></div>
            <div class="billing-content" style="float: left; width: 100%; background: #e7e7e7; min-height: 175px;">
                <div style="margin: 0px 5px;">
                      {{var payment_html}}
                </div>
            </div>
        </div>

        <div style="clear: both;"></div>
    </div>

  {{depend order.getIsNotVirtual()}}
    <!--Shipping-->
    <div class="order-detail" style="width: 95%; margin:15px auto; font-size: 12px;">
        <div class="billing-info" style="float: left; width: 49%;">
            <div class="heading" style="background: #ed5192; font-size: 12; color: #fff; padding: 3px 0px; float: left; width: 100%;"><div style="margin: 0px 5px; text-transform: uppercase;">ข้อมูลในการจัดส่งสินค้า</div></div>
            <div class="billing-content" style="float: left; width: 100%; background: #e7e7e7; min-height: 175px;">
                <div style="margin: 0px 5px;">
                      {{var order.getShippingAddress().format('html')}}
                </div>
            </div>
        </div>

        <div class="payment-method" style="float: right; width: 49%; font-size: 12px;">
            <div class="heading" style="background: #ed5192; font-size: 12; color: #fff; padding: 3px 0px; float: left; width: 100%;"><div style="margin: 0px 5px; text-transform: uppercase;">วิธีการส่งสินค้า</div></div>
            <div class="billing-content" style="float: left; width: 100%; background: #e7e7e7; min-height: 175px;">
                <div style="margin: 0px 5px;">
                     {{var order.getShippingDescription()}}
                </div>
            </div>
        </div>

        <div style="clear: both;"></div>
    </div>
       {{/depend}}

    <!--Order Item-->
    {{layout handle="sales_email_order_items" order=\$order language="th"}}
<p style="font-size:12px; margin:0 0 10px 0">{{var order.getEmailCustomerNote()}}</p>
    <!--/End OrderItem-->

   <div class="navigation-th" style="margin-top: 24px; overflow: hidden; text-align: center;">
		<div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #ee5191; border-bottom: 2px solid #ee5191; padding: 0px 11px;">
			<span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 17; padding: 0px 4px;"><a href="http://moxy.co.th/th/moxy/fashion" style="color: #3c3d41; text-decoration: none;">แฟชั่น</a></span>
			<span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 17; padding: 0px 4px;"><a href="http://moxy.co.th/th/moxy/homeliving" style="color: #3c3d41; text-decoration: none;">ของแต่งบ้าน</a></span>
			<span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 17; padding: 0px 4px;"><a href="http://moxy.co.th/th/moxy/electronics/" style="color: #3c3d41; text-decoration: none;">อิเล็กทรอนิกส์</a></span>
			<span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 17; padding: 0px 4px;"><a href="http://lafema.com/th/" style="color: #3c3d41; text-decoration: none;">ความงาม</a></span>
			<span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 17; padding: 0px 4px;"><a href="http://venbi.com/th/" style="color: #3c3d41; text-decoration: none;">แม่และเด็ก</a></span>
			<span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 17; padding: 0px 4px;"><a href="http://sanoga.com/th/" style="color: #3c3d41; text-decoration: none;">สุขภาพ</a></span>
			<span class="nav-item" style="line-height: 33px; font-size: 17; padding: 0px 4px;"><a href="http://petloft.com/th/" style="color: #3c3d41; text-decoration: none;">สัตว์เลี้ยง</a></span>
		</div>
	</div>

    <div class="email-footer" style="background: #bbb; margin-top: 8px; color: #fff; padding: 0px 15px;">
        <div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
            <p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px"><img src="{{skin _area="frontend"  url="images/email/mailbox.png"}}" style="width:18px;"/> ติดต่อเรา</p>
            <p style="font-size: 10px; margin: 0;">02-106-8222</p>
            <p style="margin: 0; font-size: 10px;">support@moxyst.com</p>
        </div>

        		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">ติดตามที่</p>
			<div class="follow-icons" style="width: 100%">
				<a href="https://www.facebook.com/moxyst"><img src="{{skin _area="frontend"  url="images/email/facebook_follow.png"}}" style="width: 25px;" alt="Facebook fanpage"></a>
				<a href="https://twitter.com/moxyst"><img src="{{skin _area="frontend"  url="images/email/twitter_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
				<a href="https://instagram.com/moxy_th/"><img src="{{skin _area="frontend"  url="images/email/instagram_follow.png"}}" style="width: 25px;" alt="Instagram fanpage"></a>
				<a href="https://www.pinterest.com/MoxySEA1/"><img src="{{skin _area="frontend"  url="images/email/pinterest_follow.png"}}" style="width: 25px;" alt="Pinterest fanpage"></a>
				<a href="https://plus.google.com/+Moxyst"><img src="{{skin _area="frontend"  url="images/email/google_follow.png"}}" style="width: 25px;" alt="Google fanpage"></a>
			</div>
		</div>

        <div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
            <p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">ช่องทางการชำระเงิน</p>
            <img src="{{skin _area="frontend"  url="images/email/payment-th.png"}}" alt="ช่องทางการชำระเงิน" style="width: 109px;">
        </div>

        <div class="clear" style="clear:both;"></div>
    </div>

    <div class="footer" style="padding: 0px 33px; margin-top: 18px;">
        <div class="copyr" style="float: left; font-size: 12px;">&copy;2015 Whatsnew Co.,Ltd. All Rights Reserved</div>
        <div class="links" style="float: right; font-size: 12px;">
            <a href="http://www.moxy.co.th/th/about-us/" style="color: #3c3d41;"><span>เกี่ยวกับเรา</span></a> |
            <a href="http://www.moxy.co.th/th/terms-and-conditions/" style="color: #3c3d41;"><span>ข้อกำหนดและเงื่อนไข</span></a> |
            <a href="http://www.moxy.co.th/th/privacy-policy/" style="color: #3c3d41;"><span>ความเป็นส่วนตัว</span></a>
        </div>
    </div>
    <div style="clear:both"></div>
    <hr style="margin: 40px auto; width: 95%;">

    <!--English Version-->
    <div class="mail-body" style="margin:50px auto; width: 95%;">
        <p>Dear {{htmlescape var=\$order.getCustomerName()}},</p>

        <br/>
        <p>Thank you for your purchasing at Moxy.</p>
        <p>Your order number is (9-digit order number)</p>
        <br>

        <p>We deliver in Bangkok in 1-3 working days.</p>
        <p>We deliver in order regions of Thailand in 3-5 working days.</p>
        <br>

        <p>Track the status of your order here : <a href="{{store url="customer/account/"}}" style="color:#28438c; text-decoration:none;">{{store url="customer/account/"}}</a></p>
        <p> You can also reach our customer service at +662-106-8222</p>
        <br>

        <p>Warm Regards,</p>
        <p>Moxy Team</p>
    </div>

    <!--Billing-->
    <div class="order-detail" style="width: 95%; margin:0 auto; font-size: 12px;">
        <div class="billing-info" style="float: left; width: 49%;">
            <div class="heading" style="background: #ed5192; font-size: 12; color: #fff; padding: 3px 0px; float: left; width: 100%;"><div style="margin: 0px 5px; text-transform: uppercase;">Billing Information</div></div>
            <div class="billing-content" style="float: left; width: 100%; background: #e7e7e7; min-height: 175px;">
                <div style="margin: 0px 5px;">
                      {{var order.getBillingAddress().format('html')}}
                </div>
            </div>
        </div>

        <div class="payment-method" style="float: right; width: 49%; font-size: 12px;">
            <div class="heading" style="background: #ed5192; font-size: 12; color: #fff; padding: 3px 0px; float: left; width: 100%;"><div style="margin: 0px 5px; text-transform: uppercase;">Payment Method</div></div>
            <div class="billing-content" style="float: left; width: 100%; background: #e7e7e7; min-height: 175px;">
                <div style="margin: 0px 5px;">
                     {{var payment_html}}
                </div>
            </div>
        </div>

        <div style="clear: both;"></div>
    </div>
     {{depend order.getIsNotVirtual()}}
    <!--Shipping-->
    <div class="order-detail" style="width: 95%; margin:15px auto; font-size: 12px;">
        <div class="billing-info" style="float: left; width: 49%;">
            <div class="heading" style="background: #ed5192; font-size: 12; color: #fff; padding: 3px 0px; float: left; width: 100%;"><div style="margin: 0px 5px; text-transform: uppercase;">Shipping Information</div></div>
            <div class="billing-content" style="float: left; width: 100%; background: #e7e7e7; min-height: 175px;">
                <div style="margin: 0px 5px;">
                     {{var order.getShippingAddress().format('html')}}
                </div>
            </div>
        </div>

        <div class="payment-method" style="float: right; width: 49%; font-size: 12px;">
            <div class="heading" style="background: #ed5192; font-size: 12; color: #fff; padding: 3px 0px; float: left; width: 100%;"><div style="margin: 0px 5px; text-transform: uppercase;">Shipping Method</div></div>
            <div class="billing-content" style="float: left; width: 100%; background: #e7e7e7; min-height: 175px;">
                <div style="margin: 0px 5px;">
                     {{var order.getShippingDescription()}}
                </div>
            </div>
        </div>

        <div style="clear: both;"></div>
    </div>
    {{/depend}}
    <!--Order Item-->
            {{layout handle="sales_email_order_items" order=\$order language="en"}}
            <p style="font-size:12px; margin:0 0 10px 0">{{var order.getEmailCustomerNote()}}</p>
    <!--/End OrderItem-->

    <div class="navigation-th" style="margin-top: 24px; overflow: hidden; text-align: center;">
		<div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #ee5191; border-bottom: 2px solid #ee5191; padding: 0px 11px;">
			<span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 18; padding: 0px 7px;"><a href="http://moxy.co.th/en/moxy/fashion" style="color: #3c3d41; text-decoration: none;">Fashion</a></span>
			<span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 18; padding: 0px 7px;"><a href="http://moxy.co.th/en/moxy/homeliving" style="color: #3c3d41; text-decoration: none;">Living</a></span>
			<span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 18; padding: 0px 7px;"><a href="http://moxy.co.th/en/moxy/electronics/" style="color: #3c3d41; text-decoration: none;">Electronics</a></span>
			<span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 18; padding: 0px 7px;"><a href="http://lafema.com/en/" style="color: #3c3d41; text-decoration: none;">Beauty</a></span>
			<span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 18; padding: 0px 7px;"><a href="http://venbi.com/en/" style="color: #3c3d41; text-decoration: none;">Baby & Mom</a></span>
			<span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 18; padding: 0px 7px;"><a href="http://sanoga.com/en/" style="color: #3c3d41; text-decoration: none;">Health</a></span>
			<span class="nav-item" style="line-height: 33px; font-size: 18; padding: 0px 7px;"><a href="http://petloft.com/en/" style="color: #3c3d41; text-decoration: none;">Pets</a></span>
		</div>
	</div>

    <div class="email-footer" style="background: #bbb; margin-top: 8px; color: #fff; padding: 0px 15px;">
        <div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
            <p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px"><img src="{{skin _area="frontend"  url="images/email/mailbox.png"}}" style="width:18px;"/> Contact Us</p>
            <p style="font-size: 10px; margin: 0;">02-106-8222</p>
            <p style="margin: 0; font-size: 10px;">support@moxyst.com</p>
        </div>

       		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">Follow Us</p>
			<div class="follow-icons" style="width: 100%">
				<a href="https://www.facebook.com/moxyst"><img src="{{skin _area="frontend"  url="images/email/facebook_follow.png"}}" style="width: 25px;" alt="Facebook fanpage"></a>
				<a href="https://twitter.com/moxyst"><img src="{{skin _area="frontend"  url="images/email/twitter_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
				<a href="https://instagram.com/moxy_th/"><img src="{{skin _area="frontend"  url="images/email/instagram_follow.png"}}" style="width: 25px;" alt="Instagram fanpage"></a>
				<a href="https://www.pinterest.com/MoxySEA1/"><img src="{{skin _area="frontend"  url="images/email/pinterest_follow.png"}}" style="width: 25px;" alt="Pinterest fanpage"></a>
				<a href="https://plus.google.com/+Moxyst"><img src="{{skin _area="frontend"  url="images/email/google_follow.png"}}" style="width: 25px;" alt="Google fanpage"></a>
			</div>
		</div>

        <div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
            <p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">Payment Options</p>
            <img src="{{skin _area="frontend"  url="images/email/payment.png"}}" alt="Payment Options" style="width: 109px;">
        </div>

        <div class="clear" style="clear:both;"></div>
    </div>

    <div class="footer" style="padding: 0px 33px; margin-top: 18px;">
        <div class="copyr" style="float: left; font-size: 12px;">&copy;2015 Whatsnew Co.,Ltd. All Rights Reserved</div>
        <div class="links" style="float: right; font-size: 12px;">
            <a href="http://www.moxy.co.th/en/about-us/" style="color: #3c3d41;"><span>About Us</span></a> |
            <a href="http://www.moxy.co.th/en/terms-and-conditions/" style="color: #3c3d41;"><span>Term & Conditions</span></a> |
            <a href="http://www.moxy.co.th/en/privacy-policy/" style="color: #3c3d41;"><span>Privacy Policy</span></a>
        </div>
    </div>
    <div style="clear:both"></div>
</div>
HTML;
/** @var Mage_Core_Model_Email_Template $model */
$model = Mage::getModel('core/email_template');
$model->loadByCode('Order Confirmation Moxy')
    ->setTemplateCode('Order Confirmation Moxy')
    ->setTemplateText($template_text)
    ->setTemplateSubject('{{var store.getFrontendName()}}: New Order # {{var order.increment_id}}')
    ->setTemplateType(Mage_Core_Model_Email_Template::TYPE_HTML);

try {
    $model->save();
    $config->saveConfig('sales_email/order/template', $model->getId() , 'stores', $moxyen);
    $config->saveConfig('sales_email/order/guest_template', $model->getId() , 'stores', $moxyen);
    $config->saveConfig('sales_email/order/template', $model->getId() , 'stores', $moxyth);
    $config->saveConfig('sales_email/order/guest_template', $model->getId() , 'stores', $moxyth);
} catch (Mage_Exception $e) {
    throw $e;
}
//==========================================================================
//==========================================================================

//==========================================================================
// ORDER CONFIRMATION SANOGA
//==========================================================================
$template_text =
    <<<HTML
<meta charset="utf-8"/>
<div class="mail-container" style="width:600px; padding:0px 33px; font-family:  'Trebuchet MS', Tahoma; font-size: 14px; color: #3c3d41; margin: 0px;">
    <div class="mail-header" style="width:95%; min-height: 110px; max-height: 110px; position: relative; margin:0px auto;">
        <div class="mail-logo" style="width:220px; min-height: 110px; max-height: 110px; margin:0px auto;">
            <a href="http://sanoga.com/"><img src="{{skin _area="frontend"  url="images/email/sanoga-logo.png"}}" style="width:220px; max-height: 110px; min-height: 110px;" /></a>
        </div>

        <div class="english-below" style="min-height: 16px; max-height: 16px; position: absolute; bottom: 0px; line-height: 16px;">
            <span><img src="{{skin _area="frontend"  url="images/email/flag-en.png"}}" height="16px;"/> Scroll down for English version</span>
        </div>
    </div>

    <img style="width: 100%; margin-top:10px;" src="{{skin _area="frontend"  url="images/email/thank_for_your_order_sanoga.png"}}" alt="Thank for your order" />

    <div class="mail-body" style="margin:50px auto; width: 95%;">
        <p>เรียน คุณ {{htmlescape var=\$order.getCustomerName()}},</p>
        <br/>

        <p>ขอขอบคุณสำหรับการสั่งซื้อจาก Moxy</p>
        <p>เลขที่ใบสั่งซื้อของคุณคือ (9-digit order number)</p>
        <br>

        <p>ทางเราจะจัดส่งสินค้าให้ถึงคุณภายใน 1-3 วันทำการสำหรับกรุงเทพฯและภายใน 3-5</p>
        <p>วันทำการสำหรับต่างจังหวัดทุกจังหวัดทั่วประเทศ</p>
        <br>

        <p>คุณสามารถเช็คสถานะการสั่งซื้อของคุณที่ : <a href="{{store url="customer/account/"}}" style="color:#28438c; text-decoration:none;">{{store url="customer/account/"}}</a></p>
        <p>หากคุณมีคำถามหรือข้อสงสัยใดๆสามารถติดต่อเราได้ที่ 02-106-8222</p>
        <br>

        <p>Warm Regards,</p>
        <p>Moxy Team</p>
    </div>

    <!--Billing-->
    <div class="order-detail" style="width: 95%; margin:0 auto; font-size: 12px;">
        <div class="billing-info" style="float: left; width: 49%;">
            <div class="heading" style="background: #6ABC45; font-size: 12; color: #fff; padding: 3px 0px; float: left; width: 100%;"><div style="margin: 0px 5px; text-transform: uppercase;">ข้อมูลในการออกใบเสร็จ</div></div>
            <div class="billing-content" style="float: left; width: 100%; background: #e7e7e7; min-height: 175px;">
                <div style="margin: 0px 5px;">
            {{var order.getBillingAddress().format('html')}}
                </div>
            </div>
        </div>

        <div class="payment-method" style="float: right; width: 49%; font-size: 12px;">
            <div class="heading" style="background: #6ABC45; font-size: 12; color: #fff; padding: 3px 0px; float: left; width: 100%;"><div style="margin: 0px 5px; text-transform: uppercase;">วิธีการชำระเงิน</div></div>
            <div class="billing-content" style="float: left; width: 100%; background: #e7e7e7; min-height: 175px;">
                <div style="margin: 0px 5px;">
                      {{var payment_html}}
                </div>
            </div>
        </div>

        <div style="clear: both;"></div>
    </div>

  {{depend order.getIsNotVirtual()}}
    <!--Shipping-->
    <div class="order-detail" style="width: 95%; margin:15px auto; font-size: 12px;">
        <div class="billing-info" style="float: left; width: 49%;">
            <div class="heading" style="background: #6ABC45; font-size: 12; color: #fff; padding: 3px 0px; float: left; width: 100%;"><div style="margin: 0px 5px; text-transform: uppercase;">ข้อมูลในการจัดส่งสินค้า</div></div>
            <div class="billing-content" style="float: left; width: 100%; background: #e7e7e7; min-height: 175px;">
                <div style="margin: 0px 5px;">
                      {{var order.getShippingAddress().format('html')}}
                </div>
            </div>
        </div>

        <div class="payment-method" style="float: right; width: 49%; font-size: 12px;">
            <div class="heading" style="background: #6ABC45; font-size: 12; color: #fff; padding: 3px 0px; float: left; width: 100%;"><div style="margin: 0px 5px; text-transform: uppercase;">วิธีการส่งสินค้า</div></div>
            <div class="billing-content" style="float: left; width: 100%; background: #e7e7e7; min-height: 175px;">
                <div style="margin: 0px 5px;">
                     {{var order.getShippingDescription()}}
                </div>
            </div>
        </div>

        <div style="clear: both;"></div>
    </div>
       {{/depend}}

    <!--Order Item-->
    {{layout handle="sales_email_order_items" order=\$order language="th"}}
<p style="font-size:12px; margin:0 0 10px 0">{{var order.getEmailCustomerNote()}}</p>
    <!--/End OrderItem-->

    <div class="navigation-th" style="margin-top: 24px; text-align:center;  overflow:hidden;">
		<div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #6abc45; border-bottom: 2px solid #6abc45; padding: 0px 0px; overflow:hidden;">
			<span class="nav-item" style="border-right: 2px solid #6abc45; line-height: 33px; font-size: 14; padding: 0px 12px;"><a href="http://www.sanoga.com/th/health-care.html" style="color: #3c3d41; text-decoration: none;">ดูแลสุขภาพ</a></span>
			<span class="nav-item" style="border-right: 2px solid #6abc45; line-height: 33px; font-size: 14; padding: 0px 12px;"><a href="http://www.sanoga.com/th/beauty.html" style="color: #3c3d41; text-decoration: none;">ความงาม</a></span>
			<span class="nav-item" style="border-right: 2px solid #6abc45; line-height: 33px; font-size: 14; padding: 0px 12px;"><a href="http://www.sanoga.com/th/workout-fitness.html" style="color: #3c3d41; text-decoration: none;">ออกกำลังกาย-ฟิตเนส</a></span>
			<span class="nav-item" style="border-right: 2px solid #6abc45; line-height: 33px; font-size: 14; padding: 0px 12px;"><a href="http://www.sanoga.com/th/weight-loss.html" style="color: #3c3d41; text-decoration: none;">อาหารเสริมลดน้ำหนัก</a></span>
			<span class="nav-item" style="line-height: 33px; font-size: 14; padding: 0px 12px;"><a href="http://www.sanoga.com/th/sexual-well-being.html" style="color: #3c3d41; text-decoration: none;">สุขภาพ-เซ็กส์</a></span>
		</div>
	</div>

	<div class="email-footer" style="min-height: 82px; max-heigh: 82px; background: #bbb; margin-top: 8px; color: #fff; padding: 0px 15px;">
		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px"><img src="{{skin _area="frontend"  url="images/email/mailbox.png"}}" style="width:18px;"/> ติดต่อเรา</p>
			<p style="font-size: 10px; margin: 0;">02-106-8222</p>
			<p style="margin: 0; font-size: 10px;">support@sanoga.com</p>
		</div>

		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">ติดตามที่</p>
			<div class="follow-icons" style="width: 100%">
				<a href="https://www.facebook.com/SanogaThailand"><img src="{{skin _area="frontend"  url="images/email/facebook_follow.png"}}" style="width: 25px;" alt="Facebook fanpage"></a>
				<a href="https://twitter.com/sanoga_thailand"><img src="{{skin _area="frontend"  url="images/email/twitter_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
				<a href="#"><img src="{{skin _area="frontend"  url="images/email/instagram_follow.png"}}" style="width: 25px;" alt="Instagram fanpage"></a>
				<a href="https://www.pinterest.com/sanoga/"><img src="{{skin _area="frontend"  url="images/email/pinterest_follow.png"}}" style="width: 25px;" alt="Pinterest fanpage"></a>
				<a href="https://plus.google.com/114390899173698565828/"><img src="{{skin _area="frontend"  url="images/email/google_follow.png"}}" style="width: 25px;" alt="Google fanpage"></a>
			</div>
		</div>

		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">ช่องทางการชำระเงิน</p>
			<img src="{{skin _area="frontend"  url="images/email/payment-th.png"}}" alt="ช่องทางการชำระเงิน" style="width: 109px;">
		</div>
	</div>

	<div class="footer" style="padding: 0px 33px; margin-top: 18px;">
		<div class="copyr" style="float: left; font-size: 12px;">&copy;2015 Whatsnew Co.,Ltd. All Rights Reserved</div>
		<div class="links" style="float: right; font-size: 12px;">
			<a href="http://www.sanoga.com/th/about-us/" style="color: #3c3d41;"><span>เกี่ยวกับเรา</span></a> |
			<a href="http://www.sanoga.com/th/terms-and-conditions/" style="color: #3c3d41;"><span>ข้อกำหนดและเงื่อนไข</span></a> |
			<a href="http://www.sanoga.com/th/privacy-policy/" style="color: #3c3d41;"><span>ความเป็นส่วนตัว</span></a>
		</div>
	</div>
    <div style="clear:both"></div>
    <hr style="margin: 40px auto; width: 95%;">

    <!--English Version-->
    <div class="mail-body" style="margin:50px auto; width: 95%;">
        <p>Dear {{htmlescape var=\$order.getCustomerName()}},</p>

        <br/>
        <p>Thank you for your purchasing at Moxy.</p>
        <p>Your order number is (9-digit order number)</p>
        <br>

        <p>We deliver in Bangkok in 1-3 working days.</p>
        <p>We deliver in order regions of Thailand in 3-5 working days.</p>
        <br>

        <p>Track the status of your order here : <a href="{{store url="customer/account/"}}" style="color:#28438c; text-decoration:none;">{{store url="customer/account/"}}</a></p>
        <p> You can also reach our customer service at +662-106-8222</p>
        <br>

        <p>Warm Regards,</p>
        <p>Moxy Team</p>
    </div>

    <!--Billing-->
    <div class="order-detail" style="width: 95%; margin:0 auto; font-size: 12px;">
        <div class="billing-info" style="float: left; width: 49%;">
            <div class="heading" style="background: #6ABC45; font-size: 12; color: #fff; padding: 3px 0px; float: left; width: 100%;"><div style="margin: 0px 5px; text-transform: uppercase;">Billing Information</div></div>
            <div class="billing-content" style="float: left; width: 100%; background: #e7e7e7; min-height: 175px;">
                <div style="margin: 0px 5px;">
                      {{var order.getBillingAddress().format('html')}}
                </div>
            </div>
        </div>

        <div class="payment-method" style="float: right; width: 49%; font-size: 12px;">
            <div class="heading" style="background: #6ABC45; font-size: 12; color: #fff; padding: 3px 0px; float: left; width: 100%;"><div style="margin: 0px 5px; text-transform: uppercase;">Payment Method</div></div>
            <div class="billing-content" style="float: left; width: 100%; background: #e7e7e7; min-height: 175px;">
                <div style="margin: 0px 5px;">
                     {{var payment_html}}
                </div>
            </div>
        </div>

        <div style="clear: both;"></div>
    </div>
     {{depend order.getIsNotVirtual()}}
    <!--Shipping-->
    <div class="order-detail" style="width: 95%; margin:15px auto; font-size: 12px;">
        <div class="billing-info" style="float: left; width: 49%;">
            <div class="heading" style="background: #6ABC45; font-size: 12; color: #fff; padding: 3px 0px; float: left; width: 100%;"><div style="margin: 0px 5px; text-transform: uppercase;">Shipping Information</div></div>
            <div class="billing-content" style="float: left; width: 100%; background: #e7e7e7; min-height: 175px;">
                <div style="margin: 0px 5px;">
                     {{var order.getShippingAddress().format('html')}}
                </div>
            </div>
        </div>

        <div class="payment-method" style="float: right; width: 49%; font-size: 12px;">
            <div class="heading" style="background: #6ABC45; font-size: 12; color: #fff; padding: 3px 0px; float: left; width: 100%;"><div style="margin: 0px 5px; text-transform: uppercase;">Shipping Method</div></div>
            <div class="billing-content" style="float: left; width: 100%; background: #e7e7e7; min-height: 175px;">
                <div style="margin: 0px 5px;">
                     {{var order.getShippingDescription()}}
                </div>
            </div>
        </div>

        <div style="clear: both;"></div>
    </div>
    {{/depend}}
    <!--Order Item-->
            {{layout handle="sales_email_order_items" order=\$order language="en"}}
            <p style="font-size:12px; margin:0 0 10px 0">{{var order.getEmailCustomerNote()}}</p>
    <!--/End OrderItem-->

    <div class="navigation-th" style="margin-top: 24px; text-align:center;  overflow:hidden;">
		<div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #6abc45; border-bottom: 2px solid #6abc45; padding: 0px 0px; overflow:hidden;">
			<span class="nav-item" style="border-right: 2px solid #6abc45; line-height: 33px; font-size: 14; padding: 0px 12px;"><a href="http://www.sanoga.com/en/health-care.html" style="color: #3c3d41; text-decoration: none;">Health Care</a></span>
			<span class="nav-item" style="border-right: 2px solid #6abc45; line-height: 33px; font-size: 14; padding: 0px 12px;"><a href="http://www.sanoga.com/en/beauty.html" style="color: #3c3d41; text-decoration: none;">Beauty</a></span>
			<span class="nav-item" style="border-right: 2px solid #6abc45; line-height: 33px; font-size: 14; padding: 0px 12px;"><a href="http://www.sanoga.com/en/workout-fitness.html" style="color: #3c3d41; text-decoration: none;">Workout & Fitness</a></span>
			<span class="nav-item" style="border-right: 2px solid #6abc45; line-height: 33px; font-size: 14; padding: 0px 12px;"><a href="http://www.sanoga.com/en/weight-loss.html" style="color: #3c3d41; text-decoration: none;">Weight Loss</a></span>
			<span class="nav-item" style="line-height: 33px; font-size: 14; padding: 0px 12px;"><a href="http://www.sanoga.com/en/sexual-well-being.html" style="color: #3c3d41; text-decoration: none;">Sexual Well-Being</a></span>
		</div>
	</div>

	<div class="email-footer" style="min-height: 82px; max-heigh: 82px; background: #bbb; margin-top: 8px; color: #fff; padding: 0px 15px;">
		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px"><img src="{{skin _area="frontend"  url="images/email/mailbox.png"}}" style="width:18px;"/> Contact Us</p>
			<p style="font-size: 10px; margin: 0;">02-106-8222</p>
			<p style="margin: 0; font-size: 10px;">support@sanoga.com</p>
		</div>

	<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">Follow Us</p>
			<div class="follow-icons" style="width: 100%">
				<a href="https://www.facebook.com/SanogaThailand"><img src="{{skin _area="frontend"  url="images/email/facebook_follow.png"}}" style="width: 25px;" alt="Facebook fanpage"></a>
				<a href="https://twitter.com/sanoga_thailand"><img src="{{skin _area="frontend"  url="images/email/twitter_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
				<a href="#"><img src="{{skin _area="frontend"  url="images/email/instagram_follow.png"}}" style="width: 25px;" alt="Instagram fanpage"></a>
				<a href="https://www.pinterest.com/sanoga/"><img src="{{skin _area="frontend"  url="images/email/pinterest_follow.png"}}" style="width: 25px;" alt="Pinterest fanpage"></a>
				<a href="https://plus.google.com/114390899173698565828/"><img src="{{skin _area="frontend"  url="images/email/google_follow.png"}}" style="width: 25px;" alt="Google fanpage"></a>
			</div>
		</div>

		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">Payment Options</p>
			<img src="{{skin _area="frontend"  url="images/email/payment.png"}}" alt="Payment Options" style="width: 109px;">
		</div>
	</div>

	<div class="footer" style="padding: 0px 33px; margin-top: 18px;">
		<div class="copyr" style="float: left; font-size: 12px;">&copy;2015 Whatsnew Co.,Ltd. All Rights Reserved</div>
		<div class="links" style="float: right; font-size: 12px;">
			<a href="http://www.sanoga.com/en/about-us/" style="color: #3c3d41;"><span>About Us</span></a> |
			<a href="http://www.sanoga.com/en/terms-and-conditions/" style="color: #3c3d41;"><span>Term & Conditions</span></a> |
			<a href="http://www.sanoga.com/en/privacy-policy/" style="color: #3c3d41;"><span>Privacy Policy</span></a>
		</div>
	</div>
    <div style="clear:both"></div>
</div>
HTML;
/** @var Mage_Core_Model_Email_Template $model */
$model = Mage::getModel('core/email_template');
$model->loadByCode('Order Confirmation Sanoga')
    ->setTemplateCode('Order Confirmation Sanoga')
    ->setTemplateText($template_text)
    ->setTemplateSubject('{{var store.getFrontendName()}}: New Order # {{var order.increment_id}}')
    ->setTemplateType(Mage_Core_Model_Email_Template::TYPE_HTML);

try {
    $model->save();
    $config->saveConfig('sales_email/order/template', $model->getId() , 'stores', $sen);
    $config->saveConfig('sales_email/order/guest_template', $model->getId() , 'stores', $sen);
    $config->saveConfig('sales_email/order/template', $model->getId() , 'stores', $sth);
    $config->saveConfig('sales_email/order/guest_template', $model->getId() , 'stores', $sth);
} catch (Mage_Exception $e) {
    throw $e;
}
//==========================================================================
//==========================================================================

//==========================================================================
// ORDER CONFIRMATION VENBI
//==========================================================================
$template_text =
    <<<HTML
<meta charset="utf-8"/>
<div class="mail-container" style="width:600px; padding:0px 33px; font-family:  'Trebuchet MS', Tahoma; font-size: 14px; color: #3c3d41; margin: 0px;">
    <div class="mail-header" style="width:95%; min-height: 110px; max-height: 110px; position: relative; margin:0px auto;">
        <div class="mail-logo" style="width:220px; min-height: 110px; max-height: 110px; margin:0px auto;">
            <a href="http://venbi.com/"><img src="{{skin _area="frontend"  url="images/email/venbi-logo.png"}}" style="width:220px; max-height: 110px; min-height: 110px;" /></a>
        </div>

        <div class="english-below" style="min-height: 16px; max-height: 16px; position: absolute; bottom: 0px; line-height: 16px;">
            <span><img src="{{skin _area="frontend"  url="images/email/flag-en.png"}}" height="16px;"/> Scroll down for English version</span>
        </div>
    </div>

    <img style="width: 100%; margin-top:10px;" src="{{skin _area="frontend"  url="images/email/thank_for_your_order_venbi.png"}}" alt="Thank for your order" />

    <div class="mail-body" style="margin:50px auto; width: 95%;">
        <p>เรียน คุณ {{htmlescape var=\$order.getCustomerName()}},</p>
        <br/>

        <p>ขอขอบคุณสำหรับการสั่งซื้อจาก Moxy</p>
        <p>เลขที่ใบสั่งซื้อของคุณคือ (9-digit order number)</p>
        <br>

        <p>ทางเราจะจัดส่งสินค้าให้ถึงคุณภายใน 1-3 วันทำการสำหรับกรุงเทพฯและภายใน 3-5</p>
        <p>วันทำการสำหรับต่างจังหวัดทุกจังหวัดทั่วประเทศ</p>
        <br>

        <p>คุณสามารถเช็คสถานะการสั่งซื้อของคุณที่ : <a href="{{store url="customer/account/"}}" style="color:#28438c; text-decoration:none;">{{store url="customer/account/"}}</a></p>
        <p>หากคุณมีคำถามหรือข้อสงสัยใดๆสามารถติดต่อเราได้ที่ 02-106-8222</p>
        <br>

        <p>Warm Regards,</p>
        <p>Moxy Team</p>
    </div>

    <!--Billing-->
    <div class="order-detail" style="width: 95%; margin:0 auto; font-size: 12px;">
        <div class="billing-info" style="float: left; width: 49%;">
            <div class="heading" style="background: #f5ea14; font-size: 12; color: #fff; padding: 3px 0px; float: left; width: 100%;"><div style="margin: 0px 5px; text-transform: uppercase;">ข้อมูลในการออกใบเสร็จ</div></div>
            <div class="billing-content" style="float: left; width: 100%; background: #e7e7e7; min-height: 175px;">
                <div style="margin: 0px 5px;">
            {{var order.getBillingAddress().format('html')}}
                </div>
            </div>
        </div>

        <div class="payment-method" style="float: right; width: 49%; font-size: 12px;">
            <div class="heading" style="background: #f5ea14; font-size: 12; color: #fff; padding: 3px 0px; float: left; width: 100%;"><div style="margin: 0px 5px; text-transform: uppercase;">วิธีการชำระเงิน</div></div>
            <div class="billing-content" style="float: left; width: 100%; background: #e7e7e7; min-height: 175px;">
                <div style="margin: 0px 5px;">
                      {{var payment_html}}
                </div>
            </div>
        </div>

        <div style="clear: both;"></div>
    </div>

  {{depend order.getIsNotVirtual()}}
    <!--Shipping-->
    <div class="order-detail" style="width: 95%; margin:15px auto; font-size: 12px;">
        <div class="billing-info" style="float: left; width: 49%;">
            <div class="heading" style="background: #f5ea14; font-size: 12; color: #fff; padding: 3px 0px; float: left; width: 100%;"><div style="margin: 0px 5px; text-transform: uppercase;">ข้อมูลในการจัดส่งสินค้า</div></div>
            <div class="billing-content" style="float: left; width: 100%; background: #e7e7e7; min-height: 175px;">
                <div style="margin: 0px 5px;">
                      {{var order.getShippingAddress().format('html')}}
                </div>
            </div>
        </div>

        <div class="payment-method" style="float: right; width: 49%; font-size: 12px;">
            <div class="heading" style="background: #f5ea14; font-size: 12; color: #fff; padding: 3px 0px; float: left; width: 100%;"><div style="margin: 0px 5px; text-transform: uppercase;">วิธีการส่งสินค้า</div></div>
            <div class="billing-content" style="float: left; width: 100%; background: #e7e7e7; min-height: 175px;">
                <div style="margin: 0px 5px;">
                     {{var order.getShippingDescription()}}
                </div>
            </div>
        </div>

        <div style="clear: both;"></div>
    </div>
       {{/depend}}

    <!--Order Item-->
    {{layout handle="sales_email_order_items" order=\$order language="th"}}
<p style="font-size:12px; margin:0 0 10px 0">{{var order.getEmailCustomerNote()}}</p>
    <!--/End OrderItem-->

   <div class="navigation-th" style="margin-top: 24px; overflow:hidden;">
		<div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #f5ea14; border-bottom: 2px solid #f5ea14; padding: 0px 0px; overflow: hidden;">
			<span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 6px;"><a href="http://www.venbi.com/th/formula-nursing.html" style="color: #3c3d41; text-decoration: none;">นมและอุปกรณ์</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 5px;"><a href="http://www.venbi.com/th/diapers.html" style="color: #3c3d41; text-decoration: none;">ผ้าอ้อมสำเร็จรูป</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 5px;"><a href="http://www.venbi.com/th/bath-baby-care.html" style="color: #3c3d41; text-decoration: none;">ผลิตภัณฑ์อาบน้ำและเบบี้แคร์</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 5px;"><a href="http://www.venbi.com/th/toys.html" style="color: #3c3d41; text-decoration: none;">ของเล่น</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 5px;"><a href="http://www.venbi.com/th/clothing.html" style="color: #3c3d41; text-decoration: none;">เสื้อผ้า</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 5px;"><a href="http://www.venbi.com/th/gear.html" style="color: #3c3d41; text-decoration: none;">เตียงและรถเข็น</a></span>
			<span class="nav-item" style="line-height: 33px; font-size: 11; padding: 0px 6px;"><a href="http://www.venbi.com/th/mom-maternity.html" style="color: #3c3d41; text-decoration: none;">แม่-การตั้งครรภ์</a></span>
		</div>
	</div>

	<div class="email-footer" style="min-height: 82px; max-heigh: 82px; background: #bbb; margin-top: 8px; color: #fff; padding: 0px 15px;">
		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px"><img src="{{skin _area="frontend"  url="images/email/mailbox.png"}}" style="width:18px;"/> ติดต่อเรา</p>
			<p style="font-size: 10px; margin: 0;">02-106-8222</p>
			<p style="margin: 0; font-size: 10px;">support@venbi.com</p>
		</div>

		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">ติดตามที่</p>
				<div class="follow-icons" style="width: 100%">
				<a href="https://www.facebook.com/venbithai"><img src="{{skin _area="frontend"  url="images/email/facebook_follow.png"}}" style="width: 25px;" alt="Facebook fanpage"></a>
				<a href="https://twitter.com/venbithai"><img src="{{skin _area="frontend"  url="images/email/twitter_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
				<a href="http://webstagr.am/venbi_thailand"><img src="{{skin _area="frontend"  url="images/email/instagram_follow.png"}}" style="width: 25px;" alt="Instagram fanpage"></a>
				<a href="https://www.pinterest.com/VenbiThailand/"><img src="{{skin _area="frontend"  url="images/email/pinterest_follow.png"}}" style="width: 25px;" alt="Pinterest fanpage"></a>
				<a href="https://plus.google.com/+VenbiThai"><img src="{{skin _area="frontend"  url="images/email/google_follow.png"}}" style="width: 25px;" alt="Google fanpage"></a>
			</div>
		</div>

		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">ช่องทางการชำระเงิน</p>
			<img src="{{skin _area="frontend"  url="images/email/payment-th.png"}}" alt="ช่องทางการชำระเงิน" style="width: 109px;">
		</div>
	</div>

	<div class="footer" style="padding: 0px 33px; margin-top: 18px;">
		<div class="copyr" style="float: left; font-size: 12px;">&copy;2015 Whatsnew Co.,Ltd. All Rights Reserved</div>
		<div class="links" style="float: right; font-size: 12px;">
			<a href="http://www.venbi.com/th/about-us/" style="color: #3c3d41;"><span>เกี่ยวกับเรา</span></a> |
			<a href="http://www.venbi.com/th/terms-and-conditions/" style="color: #3c3d41;"><span>ข้อกำหนดและเงื่อนไข</span></a> |
			<a href="http://www.venbi.com/th/privacy-policy/" style="color: #3c3d41;"><span>ความเป็นส่วนตัว</span></a>
		</div>
	</div>
    <div style="clear:both"></div>
    <hr style="margin: 40px auto; width: 95%;">

    <!--English Version-->
    <div class="mail-body" style="margin:50px auto; width: 95%;">
        <p>Dear {{htmlescape var\order.getCustomerName()}},</p>

        <br/>
        <p>Thank you for your purchasing at Moxy.</p>
        <p>Your order number is (9-digit order number)</p>
        <br>

        <p>We deliver in Bangkok in 1-3 working days.</p>
        <p>We deliver in order regions of Thailand in 3-5 working days.</p>
        <br>

        <p>Track the status of your order here : <a href="{{store url="customer/account/"}}" style="color:#28438c; text-decoration:none;">{{store url="customer/account/"}}</a></p>
        <p> You can also reach our customer service at +662-106-8222</p>
        <br>

        <p>Warm Regards,</p>
        <p>Moxy Team</p>
    </div>

    <!--Billing-->
    <div class="order-detail" style="width: 95%; margin:0 auto; font-size: 12px;">
        <div class="billing-info" style="float: left; width: 49%;">
            <div class="heading" style="background: #f5ea14; font-size: 12; color: #fff; padding: 3px 0px; float: left; width: 100%;"><div style="margin: 0px 5px; text-transform: uppercase;">Billing Information</div></div>
            <div class="billing-content" style="float: left; width: 100%; background: #e7e7e7; min-height: 175px;">
                <div style="margin: 0px 5px;">
                      {{var order.getBillingAddress().format('html')}}
                </div>
            </div>
        </div>

        <div class="payment-method" style="float: right; width: 49%; font-size: 12px;">
            <div class="heading" style="background: #f5ea14; font-size: 12; color: #fff; padding: 3px 0px; float: left; width: 100%;"><div style="margin: 0px 5px; text-transform: uppercase;">Payment Method</div></div>
            <div class="billing-content" style="float: left; width: 100%; background: #e7e7e7; min-height: 175px;">
                <div style="margin: 0px 5px;">
                     {{var payment_html}}
                </div>
            </div>
        </div>

        <div style="clear: both;"></div>
    </div>
     {{depend order.getIsNotVirtual()}}
    <!--Shipping-->
    <div class="order-detail" style="width: 95%; margin:15px auto; font-size: 12px;">
        <div class="billing-info" style="float: left; width: 49%;">
            <div class="heading" style="background: #f5ea14; font-size: 12; color: #fff; padding: 3px 0px; float: left; width: 100%;"><div style="margin: 0px 5px; text-transform: uppercase;">Shipping Information</div></div>
            <div class="billing-content" style="float: left; width: 100%; background: #e7e7e7; min-height: 175px;">
                <div style="margin: 0px 5px;">
                     {{var order.getShippingAddress().format('html')}}
                </div>
            </div>
        </div>

        <div class="payment-method" style="float: right; width: 49%; font-size: 12px;">
            <div class="heading" style="background: #f5ea14; font-size: 12; color: #fff; padding: 3px 0px; float: left; width: 100%;"><div style="margin: 0px 5px; text-transform: uppercase;">Shipping Method</div></div>
            <div class="billing-content" style="float: left; width: 100%; background: #e7e7e7; min-height: 175px;">
                <div style="margin: 0px 5px;">
                     {{var order.getShippingDescription()}}
                </div>
            </div>
        </div>

        <div style="clear: both;"></div>
    </div>
    {{/depend}}
    <!--Order Item-->
            {{layout handle="sales_email_order_items" order=\$order language="en"}}
            <p style="font-size:12px; margin:0 0 10px 0">{{var order.getEmailCustomerNote()}}</p>
    <!--/End OrderItem-->

    <div class="navigation-th" style="margin-top: 24px; overflow:hidden;">
		<div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #f5ea14; border-bottom: 2px solid #f5ea14; padding: 0px 0px; overflow: hidden;">
			<span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 5px;"><a href="http://www.venbi.com/en/formula-nursing.html" style="color: #3c3d41; text-decoration: none;">Formula & Nursing</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 5px;"><a href="http://www.venbi.com/en/diapers.html" style="color: #3c3d41; text-decoration: none;">Diapers</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 5px;"><a href="http://www.venbi.com/en/bath-baby-care.html" style="color: #3c3d41; text-decoration: none;">Bath & Baby Care</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 5px;"><a href="http://www.venbi.com/en/toys.html" style="color: #3c3d41; text-decoration: none;">Toys</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 5px;"><a href="http://www.venbi.com/en/clothing.html" style="color: #3c3d41; text-decoration: none;">Clothing</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 5px;"><a href="http://www.venbi.com/en/gear.html" style="color: #3c3d41; text-decoration: none;">Gear</a></span>
			<span class="nav-item" style="line-height: 33px; font-size: 11; padding: 0px 6px;"><a href="http://www.venbi.com/en/mom-maternity.html" style="color: #3c3d41; text-decoration: none;">Mom & Maternity</a></span>
		</div>
	</div>

	<div class="email-footer" style="min-height: 82px; max-heigh: 82px; background: #bbb; margin-top: 8px; color: #fff; padding: 0px 15px;">
		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px"><img src="{{skin _area="frontend"  url="images/email/mailbox.png"}}" style="width:18px;"/> Contact Us</p>
			<p style="font-size: 10px; margin: 0;">02-106-8222</p>
			<p style="margin: 0; font-size: 10px;">support@venbi.com</p>
		</div>

		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">Follow Us</p>
				<div class="follow-icons" style="width: 100%">
				<a href="https://www.facebook.com/venbithai"><img src="{{skin _area="frontend"  url="images/email/facebook_follow.png"}}" style="width: 25px;" alt="Facebook fanpage"></a>
				<a href="https://twitter.com/venbithai"><img src="{{skin _area="frontend"  url="images/email/twitter_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
				<a href="http://webstagr.am/venbi_thailand"><img src="{{skin _area="frontend"  url="images/email/instagram_follow.png"}}" style="width: 25px;" alt="Instagram fanpage"></a>
				<a href="https://www.pinterest.com/VenbiThailand/"><img src="{{skin _area="frontend"  url="images/email/pinterest_follow.png"}}" style="width: 25px;" alt="Pinterest fanpage"></a>
				<a href="https://plus.google.com/+VenbiThai"><img src="{{skin _area="frontend"  url="images/email/google_follow.png"}}" style="width: 25px;" alt="Google fanpage"></a>
			</div>
		</div>

		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">Payment Options</p>
			<img src="{{skin _area="frontend"  url="images/email/payment.png"}}" alt="Payment Options" style="width: 109px;">
		</div>
	</div>

	<div class="footer" style="padding: 0px 33px; margin-top: 18px;">
		<div class="copyr" style="float: left; font-size: 12px;">&copy;2015 Whatsnew Co.,Ltd. All Rights Reserved</div>
		<div class="links" style="float: right; font-size: 12px;">
			<a href="http://www.venbi.com/en/about-us/" style="color: #3c3d41;"><span>About Us</span></a> |
			<a href="http://www.venbi.com/en/terms-and-conditions/" style="color: #3c3d41;"><span>Term & Conditions</span></a> |
			<a href="http://www.venbi.com/en/privacy-policy/" style="color: #3c3d41;"><span>Privacy Policy</span></a>
		</div>
	</div>
    <div style="clear:both"></div>
</div>
HTML;
/** @var Mage_Core_Model_Email_Template $model */
$model = Mage::getModel('core/email_template');
$model->loadByCode('Order Confirmation Venbi')
    ->setTemplateCode('Order Confirmation Venbi')
    ->setTemplateText($template_text)
    ->setTemplateSubject('{{var store.getFrontendName()}}: New Order # {{var order.increment_id}}')
    ->setTemplateType(Mage_Core_Model_Email_Template::TYPE_HTML);

try {
    $model->save();
    $config->saveConfig('sales_email/order/template', $model->getId() , 'stores', $ven);
    $config->saveConfig('sales_email/order/guest_template', $model->getId() , 'stores', $ven);
    $config->saveConfig('sales_email/order/template', $model->getId() , 'stores', $vth);
    $config->saveConfig('sales_email/order/guest_template', $model->getId() , 'stores', $vth);
} catch (Mage_Exception $e) {
    throw $e;
}
//==========================================================================
//==========================================================================

//==========================================================================
// ORDER CONFIRMATION LAFEMA
//==========================================================================
$template_text =
    <<<HTML
<meta charset="utf-8"/>
<div class="mail-container" style="width:600px; padding:0px 33px; font-family:  'Trebuchet MS', Tahoma; font-size: 14px; color: #3c3d41; margin: 0px;">
    <div class="mail-header" style="width:95%; min-height: 110px; max-height: 110px; position: relative; margin:0px auto;">
        <div class="mail-logo" style="width:220px; min-height: 110px; max-height: 110px; margin:0px auto;">
            <a href="http://lafema.com/"><img src="{{skin _area="frontend"  url="images/email/lafema-logo.png"}}" style="width:220px; max-height: 110px; min-height: 110px;" /></a>
        </div>

        <div class="english-below" style="min-height: 16px; max-height: 16px; position: absolute; bottom: 0px; line-height: 16px;">
            <span><img src="{{skin _area="frontend"  url="images/email/flag-en.png"}}" height="16px;"/> Scroll down for English version</span>
        </div>
    </div>

    <img style="width: 100%; margin-top:10px;" src="{{skin _area="frontend"  url="images/email/thank_for_your_order_lafema.png"}}" alt="Thank for your order" />

    <div class="mail-body" style="margin:50px auto; width: 95%;">
        <p>เรียน คุณ {{htmlescape var=\$order.getCustomerName()}},</p>
        <br/>

        <p>ขอขอบคุณสำหรับการสั่งซื้อจาก Moxy</p>
        <p>เลขที่ใบสั่งซื้อของคุณคือ (9-digit order number)</p>
        <br>

        <p>ทางเราจะจัดส่งสินค้าให้ถึงคุณภายใน 1-3 วันทำการสำหรับกรุงเทพฯและภายใน 3-5</p>
        <p>วันทำการสำหรับต่างจังหวัดทุกจังหวัดทั่วประเทศ</p>
        <br>

        <p>คุณสามารถเช็คสถานะการสั่งซื้อของคุณที่ : <a href="{{store url="customer/account/"}}" style="color:#28438c; text-decoration:none;">{{store url="customer/account/"}}</a></p>
        <p>หากคุณมีคำถามหรือข้อสงสัยใดๆสามารถติดต่อเราได้ที่ 02-106-8222</p>
        <br>

        <p>Warm Regards,</p>
        <p>Moxy Team</p>
    </div>

    <!--Billing-->
    <div class="order-detail" style="width: 95%; margin:0 auto; font-size: 12px;">
        <div class="billing-info" style="float: left; width: 49%;">
            <div class="heading" style="background: #f5a2b2; font-size: 12; color: #fff; padding: 3px 0px; float: left; width: 100%;"><div style="margin: 0px 5px; text-transform: uppercase;">ข้อมูลในการออกใบเสร็จ</div></div>
            <div class="billing-content" style="float: left; width: 100%; background: #e7e7e7; min-height: 175px;">
                <div style="margin: 0px 5px;">
            {{var order.getBillingAddress().format('html')}}
                </div>
            </div>
        </div>

        <div class="payment-method" style="float: right; width: 49%; font-size: 12px;">
            <div class="heading" style="background: #f5a2b2; font-size: 12; color: #fff; padding: 3px 0px; float: left; width: 100%;"><div style="margin: 0px 5px; text-transform: uppercase;">วิธีการชำระเงิน</div></div>
            <div class="billing-content" style="float: left; width: 100%; background: #e7e7e7; min-height: 175px;">
                <div style="margin: 0px 5px;">
                      {{var payment_html}}
                </div>
            </div>
        </div>

        <div style="clear: both;"></div>
    </div>

  {{depend order.getIsNotVirtual()}}
    <!--Shipping-->
    <div class="order-detail" style="width: 95%; margin:15px auto; font-size: 12px;">
        <div class="billing-info" style="float: left; width: 49%;">
            <div class="heading" style="background: #f5a2b2; font-size: 12; color: #fff; padding: 3px 0px; float: left; width: 100%;"><div style="margin: 0px 5px; text-transform: uppercase;">ข้อมูลในการจัดส่งสินค้า</div></div>
            <div class="billing-content" style="float: left; width: 100%; background: #e7e7e7; min-height: 175px;">
                <div style="margin: 0px 5px;">
                      {{var order.getShippingAddress().format('html')}}
                </div>
            </div>
        </div>

        <div class="payment-method" style="float: right; width: 49%; font-size: 12px;">
            <div class="heading" style="background: #f5a2b2; font-size: 12; color: #fff; padding: 3px 0px; float: left; width: 100%;"><div style="margin: 0px 5px; text-transform: uppercase;">วิธีการส่งสินค้า</div></div>
            <div class="billing-content" style="float: left; width: 100%; background: #e7e7e7; min-height: 175px;">
                <div style="margin: 0px 5px;">
                     {{var order.getShippingDescription()}}
                </div>
            </div>
        </div>

        <div style="clear: both;"></div>
    </div>
       {{/depend}}

    <!--Order Item-->
    {{layout handle="sales_email_order_items" order=\$order language="th"}}
<p style="font-size:12px; margin:0 0 10px 0">{{var order.getEmailCustomerNote()}}</p>
    <!--/End OrderItem-->

    <div class="navigation-th" style="margin-top: 24px; text-align:center; overflow:hidden;">
		<div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #f5a2b2; border-bottom: 2px solid #f5a2b2; padding: 0px 0px;">
			<span class="nav-item" style="border-right: 2px solid #f5a2b2; line-height: 33px; font-size: 16; padding: 0px 18px;"><a href="http://www.lafema.com/th/makeup.html" style="color: #3c3d41; text-decoration: none;">เมคอัพ</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5a2b2; line-height: 33px; font-size: 16; padding: 0px 18px;"><a href="http://www.lafema.com/th/skincare.html" style="color: #3c3d41; text-decoration: none;">ดูแลผิวหน้า</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5a2b2; line-height: 33px; font-size: 16; padding: 0px 18px;"><a href="http://www.lafema.com/th/bath-body-hair.html" style="color: #3c3d41; text-decoration: none;">ดูแลผิวกาย-ผม</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5a2b2; line-height: 33px; font-size: 16; padding: 0px 18px;"><a href="http://www.lafema.com/th/fragrance.html" style="color: #3c3d41; text-decoration: none;">น้ำหอม</a></span>
			<span class="nav-item" style="line-height: 33px; font-size: 16; padding: 0px 12px;"><a href="http://www.lafema.com/th/men.html" style="color: #3c3d41; text-decoration: none;">สำหรับผู้ชาย</a></span>
		</div>
	</div>

	<div class="email-footer" style="min-height: 82px; max-heigh: 82px; background: #bbb; margin-top: 8px; color: #fff; padding: 0px 15px;">
		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px"><img src="{{skin _area="frontend"  url="images/email/mailbox.png"}}" style="width:18px;"/> ติดต่อเรา</p>
			<p style="font-size: 10px; margin: 0;">02-106-8222</p>
			<p style="margin: 0; font-size: 10px;">support@lafema.com</p>
		</div>

		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">ติดตามที่</p>
			<div class="follow-icons" style="width: 100%">
				<a href="https://www.facebook.com/lafemathai"><img src="{{skin _area="frontend"  url="images/email/facebook_follow.png"}}" style="width: 25px;" alt="Facebook fanpage"></a>
				<a href="https://twitter.com/lafemath"><img src="{{skin _area="frontend"  url="images/email/twitter_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
				<a href="https://instagram.com/lafema.th/"><img src="{{skin _area="frontend"  url="images/email/instagram_follow.png"}}" style="width: 25px;" alt="Instagram fanpage"></a>
				<a href="https://www.pinterest.com/lafema_thai/"><img src="{{skin _area="frontend"  url="images/email/pinterest_follow.png"}}" style="width: 25px;" alt="Pinterest fanpage"></a>
				<a href="https://plus.google.com/103314178165403417268/"><img src="{{skin _area="frontend"  url="images/email/google_follow.png"}}" style="width: 25px;" alt="Google fanpage"></a>
			</div>
		</div>

		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">ช่องทางการชำระเงิน</p>
			<img src="{{skin _area="frontend"  url="images/email/payment-th.png"}}" alt="ช่องทางการชำระเงิน" style="width: 109px;">
		</div>
	</div>

	<div class="footer" style="padding: 0px 33px; margin-top: 18px;">
		<div class="copyr" style="float: left; font-size: 12px;">&copy;2015 Whatsnew Co.,Ltd. All Rights Reserved</div>
		<div class="links" style="float: right; font-size: 12px;">
			<a href="http://www.lafema.com/th/about-us/" style="color: #3c3d41;"><span>เกี่ยวกับเรา</span></a> |
			<a href="http://www.lafema.com/th/terms-and-conditions/" style="color: #3c3d41;"><span>ข้อกำหนดและเงื่อนไข</span></a> |
			<a href="http://www.lafema.com/th/privacy-policy/" style="color: #3c3d41;"><span>ความเป็นส่วนตัว</span></a>
		</div>
	</div>
    <div style="clear:both"></div>
    <hr style="margin: 40px auto; width: 95%;">

    <!--English Version-->
    <div class="mail-body" style="margin:50px auto; width: 95%;">
        <p>Dear {{htmlescape var=\$order.getCustomerName()}},</p>

        <br/>
        <p>Thank you for your purchasing at Moxy.</p>
        <p>Your order number is (9-digit order number)</p>
        <br>

        <p>We deliver in Bangkok in 1-3 working days.</p>
        <p>We deliver in order regions of Thailand in 3-5 working days.</p>
        <br>

        <p>Track the status of your order here : <a href="{{store url="customer/account/"}}" style="color:#28438c; text-decoration:none;">{{store url="customer/account/"}}</a></p>
        <p> You can also reach our customer service at +662-106-8222</p>
        <br>

        <p>Warm Regards,</p>
        <p>Moxy Team</p>
    </div>

    <!--Billing-->
    <div class="order-detail" style="width: 95%; margin:0 auto; font-size: 12px;">
        <div class="billing-info" style="float: left; width: 49%;">
            <div class="heading" style="background: #f5a2b2; font-size: 12; color: #fff; padding: 3px 0px; float: left; width: 100%;"><div style="margin: 0px 5px; text-transform: uppercase;">Billing Information</div></div>
            <div class="billing-content" style="float: left; width: 100%; background: #e7e7e7; min-height: 175px;">
                <div style="margin: 0px 5px;">
                      {{var order.getBillingAddress().format('html')}}
                </div>
            </div>
        </div>

        <div class="payment-method" style="float: right; width: 49%; font-size: 12px;">
            <div class="heading" style="background: #f5a2b2; font-size: 12; color: #fff; padding: 3px 0px; float: left; width: 100%;"><div style="margin: 0px 5px; text-transform: uppercase;">Payment Method</div></div>
            <div class="billing-content" style="float: left; width: 100%; background: #e7e7e7; min-height: 175px;">
                <div style="margin: 0px 5px;">
                     {{var payment_html}}
                </div>
            </div>
        </div>

        <div style="clear: both;"></div>
    </div>
     {{depend order.getIsNotVirtual()}}
    <!--Shipping-->
    <div class="order-detail" style="width: 95%; margin:15px auto; font-size: 12px;">
        <div class="billing-info" style="float: left; width: 49%;">
            <div class="heading" style="background: #f5a2b2; font-size: 12; color: #fff; padding: 3px 0px; float: left; width: 100%;"><div style="margin: 0px 5px; text-transform: uppercase;">Shipping Information</div></div>
            <div class="billing-content" style="float: left; width: 100%; background: #e7e7e7; min-height: 175px;">
                <div style="margin: 0px 5px;">
                     {{var order.getShippingAddress().format('html')}}
                </div>
            </div>
        </div>

        <div class="payment-method" style="float: right; width: 49%; font-size: 12px;">
            <div class="heading" style="background: #f5a2b2; font-size: 12; color: #fff; padding: 3px 0px; float: left; width: 100%;"><div style="margin: 0px 5px; text-transform: uppercase;">Shipping Method</div></div>
            <div class="billing-content" style="float: left; width: 100%; background: #e7e7e7; min-height: 175px;">
                <div style="margin: 0px 5px;">
                     {{var order.getShippingDescription()}}
                </div>
            </div>
        </div>

        <div style="clear: both;"></div>
    </div>
    {{/depend}}
    <!--Order Item-->
            {{layout handle="sales_email_order_items" order=\$order language="en"}}
            <p style="font-size:12px; margin:0 0 10px 0">{{var order.getEmailCustomerNote()}}</p>
    <!--/End OrderItem-->

    <div class="navigation-th" style="margin-top: 24px; text-align:center; overflow:hidden;">
		<div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #f5a2b2; border-bottom: 2px solid #f5a2b2; padding: 0px 0px;">
			<span class="nav-item" style="border-right: 2px solid #f5a2b2; line-height: 33px; font-size: 16; padding: 0px 18px;"><a href="http://www.lafema.com/en/makeup.html" style="color: #3c3d41; text-decoration: none;">Makeup</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5a2b2; line-height: 33px; font-size: 16; padding: 0px 18px;"><a href="http://www.lafema.com/en/skincare.html" style="color: #3c3d41; text-decoration: none;">Skin Care</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5a2b2; line-height: 33px; font-size: 16; padding: 0px 18px;"><a href="http://www.lafema.com/en/bath-body-hair.html" style="color: #3c3d41; text-decoration: none;">Bath & Body-Hair</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5a2b2; line-height: 33px; font-size: 16; padding: 0px 18px;"><a href="http://www.lafema.com/en/fragrance.html" style="color: #3c3d41; text-decoration: none;">Fragrance</a></span>
			<span class="nav-item" style="line-height: 33px; font-size: 16; padding: 0px 18px;"><a href="http://www.lafema.com/en/men.html" style="color: #3c3d41; text-decoration: none;">Men</a></span>
		</div>
	</div>

	<div class="email-footer" style="min-height: 82px; max-heigh: 82px; background: #bbb; margin-top: 8px; color: #fff; padding: 0px 15px;">
		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px"><img src="{{skin _area="frontend"  url="images/email/mailbox.png"}}" style="width:18px;"/> Contact Us</p>
			<p style="font-size: 10px; margin: 0;">02-106-8222</p>
			<p style="margin: 0; font-size: 10px;">support@lafema.com</p>
		</div>

		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">Follow Us</p>
							<div class="follow-icons" style="width: 100%">
				<a href="https://www.facebook.com/lafemathai"><img src="{{skin _area="frontend"  url="images/email/facebook_follow.png"}}" style="width: 25px;" alt="Facebook fanpage"></a>
				<a href="https://twitter.com/lafemath"><img src="{{skin _area="frontend"  url="images/email/twitter_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
				<a href="https://instagram.com/lafema.th/"><img src="{{skin _area="frontend"  url="images/email/instagram_follow.png"}}" style="width: 25px;" alt="Instagram fanpage"></a>
				<a href="https://www.pinterest.com/lafema_thai/"><img src="{{skin _area="frontend"  url="images/email/pinterest_follow.png"}}" style="width: 25px;" alt="Pinterest fanpage"></a>
				<a href="https://plus.google.com/103314178165403417268/"><img src="{{skin _area="frontend"  url="images/email/google_follow.png"}}" style="width: 25px;" alt="Google fanpage"></a>
			</div>
		</div>

		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">Payment Options</p>
			<img src="{{skin _area="frontend"  url="images/email/payment.png"}}" alt="Payment Options" style="width: 109px;">
		</div>
	</div>

	<div class="footer" style="padding: 0px 33px; margin-top: 18px;">
		<div class="copyr" style="float: left; font-size: 12px;">&copy;2015 Whatsnew Co.,Ltd. All Rights Reserved</div>
		<div class="links" style="float: right; font-size: 12px;">
			<a href="http://www.lafema.com/en/about-us/" style="color: #3c3d41;"><span>About Us</span></a> |
			<a href="http://www.lafema.com/en/terms-and-conditions/" style="color: #3c3d41;"><span>Term & Conditions</span></a> |
			<a href="http://www.lafema.com/en/privacy-policy/" style="color: #3c3d41;"><span>Privacy Policy</span></a>
		</div>
	</div>
    <div style="clear:both"></div>
</div>
HTML;
/** @var Mage_Core_Model_Email_Template $model */
$model = Mage::getModel('core/email_template');
$model->loadByCode('Order Confirmation Lafema')
    ->setTemplateCode('Order Confirmation Lafema')
    ->setTemplateText($template_text)
    ->setTemplateSubject('{{var store.getFrontendName()}}: New Order # {{var order.increment_id}}')
    ->setTemplateType(Mage_Core_Model_Email_Template::TYPE_HTML);

try {
    $model->save();
    $config->saveConfig('sales_email/order/template', $model->getId() , 'stores', $len);
    $config->saveConfig('sales_email/order/guest_template', $model->getId() , 'stores', $len);
    $config->saveConfig('sales_email/order/template', $model->getId() , 'stores', $lth);
    $config->saveConfig('sales_email/order/guest_template', $model->getId() , 'stores', $lth);
} catch (Mage_Exception $e) {
    throw $e;
}
//==========================================================================
//==========================================================================

//==========================================================================
// RESET PASSWORD PETLOFT
//==========================================================================
$template_text =
    <<<HTML
<meta charset="utf-8"/>
<div class="mail-container" style="width:600px; padding:0px 33px; font-family:  'Trebuchet MS', Tahoma; font-size: 14px; color: #3c3d41; margin: 0px;">
    <div class="mail-header" style="width:95%; min-height: 110px; max-height: 110px; position: relative; margin:0px auto;">
        <div class="mail-logo" style="width:220px; min-height: 110px; max-height: 110px; margin:0px auto;">
            <a href="http://petloft.com/"><img src="{{skin _area="frontend"  url="images/email/petloft-logo.png"}}" style="width:220px; max-height: 110px; min-height: 110px;" /></a>
        </div>

        <div class="english-below" style="min-height: 16px; max-height: 16px; position: absolute; bottom: 0px; line-height: 16px;">
            <span><img src="{{skin _area="frontend"  url="images/email/flag-en.png"}}" height="16px;"/> Scroll down for English version</span>
        </div>
    </div>

	<img style="width: 100%; margin-top:40px;" src="{{skin _area="frontend"  url="images/email/password_reset_petloft.png"}}" alt="Thank for your order" />

	<div class="mail-body" style="margin:50px auto; width: 95%;">
		<p>เรียน คุณ {{var customer.name}},</p>
		<br/>

		<p>ทางเราได้รับคำขอเปลี่ยนรหัสผ่านสำหรับบัญชีของคุณ</p>
		<br>

		<a href="{{store url="customer/account/resetpassword/" _query_id=\$customer.id _query_token=\$customer.rp_token}}" style="color: #fff; text-decoration: none; background: #0e80c0;padding: 10px;">เปลี่ยนรหัสผ่าน</a>
		<br>
		<br>
		<p>หากคุณข้ามอีเมลล์นี้ไป รหัสของคุณยังคงเหมือนเดิม</p>
		<p>หากคุณไม่ได้ส่งคำขอนี้ แจ้งให้เราทราบ</p>
		<br>

		<p>Warm Regards,</p>
		<p>Moxy Team</p>
	</div>



	<div class="navigation-th" style="margin-top: 24px; overflow:hidden;">
		<div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #006ab4; border-bottom: 2px solid #006ab4; padding: 0px 17px; overflow: hidden;">
			<span class="nav-item" style="border-right: 2px solid #006ab4; line-height: 33px; font-size: 18px; padding: 0px 28px;"><a href="http://petloft.com/th/dog-food.html" style="color: #3c3d41; text-decoration: none;">สุนัข</a></span>
			<span class="nav-item" style="border-right: 2px solid #006ab4; line-height: 33px; font-size: 18px; padding: 0px 28px;"><a href="http://petloft.com/th/cat-food.html" style="color: #3c3d41; text-decoration: none;">แมว</a></span>
			<span class="nav-item" style="border-right: 2px solid #006ab4; line-height: 33px; font-size: 18px; padding: 0px 28px;"><a href="http://petloft.com/th/fish-products.html" style="color: #3c3d41; text-decoration: none;">ปลา</a></span>
			<span class="nav-item" style="border-right: 2px solid #006ab4; line-height: 33px; font-size: 18px; padding: 0px 28px;"><a href="http://www.petloft.com/th/bird.html" style="color: #3c3d41; text-decoration: none;">นก</a></span>
			<span class="nav-item" style="line-height: 33px; font-size: 18px; padding: 0px 28px;"><a href="http://petloft.com/th/small-pets-products.html" style="color: #3c3d41; text-decoration: none;">สัตว์เลี้ยงขนาดเล็ก</a></span>
		</div>
	</div>

    <div class="email-footer" style="background: #bbb; margin-top: 8px; color: #fff; padding: 0px 15px;">
        <div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
            <p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px"><img src="{{skin _area="frontend"  url="images/email/mailbox.png"}}" style="width:18px;"/> ติดต่อเรา</p>
            <p style="font-size: 10px; margin: 0;">02-106-8222</p>
            <p style="margin: 0; font-size: 10px;">support@petloft.com</p>
        </div>

        <div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
            <p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">ติดตามที่</p>
            <div class="follow-icons" style="width: 100%">
    <a href="https://www.facebook.com/petloftthai"><img src="{{skin _area="frontend"  url="images/email/facebook_follow.png"}}" style="width: 25px;" alt="Facebook fanpage"></a>
                <a href="https://twitter.com/PetLoft"><img src="{{skin _area="frontend"  url="images/email/twitter_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
                <a href="https://instagram.com/petloft"><img src="{{skin _area="frontend"  url="images/email/instagram_follow.png"}}" style="width: 25px;" alt="Instagram fanpage"></a>
                <a href="https://www.pinterest.com/petloft/"><img src="{{skin _area="frontend"  url="images/email/pinterest_follow.png"}}" style="width: 25px;" alt="Pinterest fanpage"></a>
                <a href="https://plus.google.com/110158894140498579470"><img src="{{skin _area="frontend"  url="images/email/google_follow.png"}}" style="width: 25px;" alt="Google fanpage"></a>
            </div>
        </div>

        <div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
            <p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">ช่องทางการชำระเงิน</p>
            <img src="{{skin _area="frontend"  url="images/email/payment-th.png"}}" alt="ช่องทางการชำระเงิน" style="width: 109px;">
        </div>

        <div class="clear" style="clear:both;"></div>
    </div>

    <div class="footer" style="padding: 0px 33px; margin-top: 18px;">
        <div class="copyr" style="float: left; font-size: 12px;">&copy;2015 Whatsnew Co.,Ltd. All Rights Reserved</div>
        <div class="links" style="float: right; font-size: 12px;">
            <a href="http://www.petloft.com/th/about-us/" style="color: #3c3d41;"><span>เกี่ยวกับเรา</span></a> |
            <a href="http://www.petloft.com/th/terms-and-conditions/" style="color: #3c3d41;"><span>ข้อกำหนดและเงื่อนไข</span></a> |
            <a href="http://www.petloft.com/th/privacy-policy/" style="color: #3c3d41;"><span>ความเป็นส่วนตัว</span></a>
        </div>
    </div>
	<div style="clear:both"></div>
	<hr style="margin: 40px auto; width: 95%;">

	<!--English Version-->
	<div class="mail-body" style="margin:50px auto; width: 95%;">
		<p>Dear {{var customer.name}},</p>
		<br/>

		<p>We just received a request to reset your password.</p>
		<p>Don’t worry, you can simply click on the button and create a new one.</p>
		<br>
		<a href="{{store url="customer/account/resetpassword/" _query_id=\$customer.id _query_token=\$customer.rp_token}}" style="color: #fff; text-decoration: none; background: #0e80c0;padding: 10px;">RESET</a>
		<br>
		<br>
		<p>Ignore this message if you didn’t request a new password.</p>
		<br>

		<p>Warm Regards,</p>
		<p>Moxy Team</p>
	</div>

	<div class="navigation-th" style="margin-top: 24px; overflow:hidden;">
		<div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #006ab4; border-bottom: 2px solid #006ab4; padding: 0px 30px; overflow: hidden;">
			<span class="nav-item" style="border-right: 2px solid #006ab4; line-height: 33px; font-size: 18px; padding: 0px 29px;"><a href="http://petloft.com/en/dog-food.html" style="color: #3c3d41; text-decoration: none;">Dogs</a></span>
			<span class="nav-item" style="border-right: 2px solid #006ab4; line-height: 33px; font-size: 18px; padding: 0px 29px;"><a href="http://petloft.com/en/cat-food.html" style="color: #3c3d41; text-decoration: none;">Cats</a></span>
			<span class="nav-item" style="border-right: 2px solid #006ab4; line-height: 33px; font-size: 18px; padding: 0px 29px;"><a href="http://petloft.com/en/fish-products.html" style="color: #3c3d41; text-decoration: none;">Fish</a></span>
			<span class="nav-item" style="border-right: 2px solid #006ab4; line-height: 33px; font-size: 18px; padding: 0px 29px;"><a href="http://www.petloft.com/en/bird.html" style="color: #3c3d41; text-decoration: none;">Bird</a></span>
			<span class="nav-item" style="line-height: 33px; font-size: 18px; padding: 0px 29px;"><a href="http://petloft.com/en/small-pets-products.html" style="color: #3c3d41; text-decoration: none;">Small Pets</a></span>
		</div>
	</div>

    <div class="email-footer" style="background: #bbb; margin-top: 8px; color: #fff; padding: 0px 15px;">
        <div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
            <p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px"><img src="{{skin _area="frontend"  url="images/email/mailbox.png"}}" style="width:18px;"/> Contact Us</p>
            <p style="font-size: 10px; margin: 0;">02-106-8222</p>
            <p style="margin: 0; font-size: 10px;">support@petloft.com</p>
        </div>

        <div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
            <p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">Follow Us</p>
            <div class="follow-icons" style="width: 100%">
    <a href="https://www.facebook.com/petloftthai"><img src="{{skin _area="frontend"  url="images/email/facebook_follow.png"}}" style="width: 25px;" alt="Facebook fanpage"></a>
                <a href="https://twitter.com/PetLoft"><img src="{{skin _area="frontend"  url="images/email/twitter_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
                <a href="https://instagram.com/petloft"><img src="{{skin _area="frontend"  url="images/email/instagram_follow.png"}}" style="width: 25px;" alt="Instagram fanpage"></a>
                <a href="https://www.pinterest.com/petloft/"><img src="{{skin _area="frontend"  url="images/email/pinterest_follow.png"}}" style="width: 25px;" alt="Pinterest fanpage"></a>
                <a href="https://plus.google.com/110158894140498579470"><img src="{{skin _area="frontend"  url="images/email/google_follow.png"}}" style="width: 25px;" alt="Google fanpage"></a>
            </div>
        </div>

        <div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
            <p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">Payment Options</p>
            <img src="{{skin _area="frontend"  url="images/email/payment.png"}}" alt="Payment Options" style="width: 109px;">
        </div>

        <div class="clear" style="clear:both;"></div>
    </div>

    <div class="footer" style="padding: 0px 33px; margin-top: 18px;">
        <div class="copyr" style="float: left; font-size: 12px;">&copy;2015 Whatsnew Co.,Ltd. All Rights Reserved</div>
        <div class="links" style="float: right; font-size: 12px;">
            <a href="http://www.petloft.com/en/about-us/" style="color: #3c3d41;"><span>About Us</span></a> |
            <a href="http://www.petloft.com/en/terms-and-conditions/" style="color: #3c3d41;"><span>Term & Conditions</span></a> |
            <a href="http://www.petloft.com/en/privacy-policy/" style="color: #3c3d41;"><span>Privacy Policy</span></a>
        </div>
    </div>
	<div style="clear:both"></div>
</div>
HTML;
/** @var Mage_Core_Model_Email_Template $model */
$model = Mage::getModel('core/email_template');
$model->loadByCode('Reset Password Petloft')
    ->setTemplateCode('Reset Password Petloft')
    ->setTemplateText($template_text)
    ->setTemplateSubject('Password Reset Confirmation for {{var customer.name}}')
    ->setTemplateType(Mage_Core_Model_Email_Template::TYPE_HTML);

try {
    $model->save();
    $config->saveConfig('customer/password/forgot_email_template', $model->getId() , 'stores', $en);
    $config->saveConfig('customer/password/forgot_email_template', $model->getId() , 'stores', $th);
} catch (Mage_Exception $e) {
    throw $e;
}
//==========================================================================
//==========================================================================

//==========================================================================
// RESET PASSWORD MOXY
//==========================================================================
$template_text =
    <<<HTML
<meta charset="utf-8"/>
<div class="mail-container" style="width:600px; padding:0px 33px; font-family:  'Trebuchet MS', Tahoma; font-size: 14px; color: #3c3d41; margin: 0px;">
    <div class="mail-header" style="width:95%; min-height: 110px; max-height: 110px; position: relative; margin:0px auto;">
        <div class="mail-logo" style="width:220px; min-height: 110px; max-height: 110px; margin:0px auto;">
            <a href="http://moxy.co.th/"><img src="{{skin _area="frontend"  url="images/email/moxy-logo.png"}}" style="width:220px; max-height: 110px; min-height: 110px;" /></a>
        </div>

        <div class="english-below" style="min-height: 16px; max-height: 16px; position: absolute; bottom: 0px; line-height: 16px;">
            <span><img src="{{skin _area="frontend"  url="images/email/flag-en.png"}}" height="16px;"/> Scroll down for English version</span>
        </div>
    </div>

	<img style="width: 100%; margin-top:40px;" src="{{skin _area="frontend"  url="images/email/password_reset_moxy.png"}}" alt="Thank for your order" />

	<div class="mail-body" style="margin:50px auto; width: 95%;">
		<p>เรียน คุณ {{var customer.name}},</p>
		<br/>

		<p>ทางเราได้รับคำขอเปลี่ยนรหัสผ่านสำหรับบัญชีของคุณ</p>
		<br>

		<a href="{{store url="customer/account/resetpassword/" _query_id=\$customer.id _query_token=\$customer.rp_token}}" style="color: #fff; text-decoration: none; background: #EE5191;padding: 10px;">เปลี่ยนรหัสผ่าน</a>
		<br>
		<br>
		<p>หากคุณข้ามอีเมลล์นี้ไป รหัสของคุณยังคงเหมือนเดิม</p>
		<p>หากคุณไม่ได้ส่งคำขอนี้ แจ้งให้เราทราบ</p>
		<br>

		<p>Warm Regards,</p>
		<p>Moxy Team</p>
	</div>



	<div class="navigation-th" style="margin-top: 24px; overflow: hidden; text-align: center;">
		<div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #ee5191; border-bottom: 2px solid #ee5191; padding: 0px 11px;">
			<span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 17; padding: 0px 4px;"><a href="http://moxy.co.th/th/moxy/fashion" style="color: #3c3d41; text-decoration: none;">แฟชั่น</a></span>
			<span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 17; padding: 0px 4px;"><a href="http://moxy.co.th/th/moxy/homeliving" style="color: #3c3d41; text-decoration: none;">ของแต่งบ้าน</a></span>
			<span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 17; padding: 0px 4px;"><a href="http://moxy.co.th/th/moxy/electronics/" style="color: #3c3d41; text-decoration: none;">อิเล็กทรอนิกส์</a></span>
			<span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 17; padding: 0px 4px;"><a href="http://lafema.com/th/" style="color: #3c3d41; text-decoration: none;">ความงาม</a></span>
			<span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 17; padding: 0px 4px;"><a href="http://venbi.com/th/" style="color: #3c3d41; text-decoration: none;">แม่และเด็ก</a></span>
			<span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 17; padding: 0px 4px;"><a href="http://sanoga.com/th/" style="color: #3c3d41; text-decoration: none;">สุขภาพ</a></span>
			<span class="nav-item" style="line-height: 33px; font-size: 17; padding: 0px 4px;"><a href="http://petloft.com/th/" style="color: #3c3d41; text-decoration: none;">สัตว์เลี้ยง</a></span>
		</div>
	</div>

    <div class="email-footer" style="background: #bbb; margin-top: 8px; color: #fff; padding: 0px 15px;">
        <div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
            <p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px"><img src="{{skin _area="frontend"  url="images/email/mailbox.png"}}" style="width:18px;"/> ติดต่อเรา</p>
            <p style="font-size: 10px; margin: 0;">02-106-8222</p>
            <p style="margin: 0; font-size: 10px;">support@moxyst.com</p>
        </div>

        		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">ติดตามที่</p>
			<div class="follow-icons" style="width: 100%">
				<a href="https://www.facebook.com/moxyst"><img src="{{skin _area="frontend"  url="images/email/facebook_follow.png"}}" style="width: 25px;" alt="Facebook fanpage"></a>
				<a href="https://twitter.com/moxyst"><img src="{{skin _area="frontend"  url="images/email/twitter_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
				<a href="https://instagram.com/moxy_th/"><img src="{{skin _area="frontend"  url="images/email/instagram_follow.png"}}" style="width: 25px;" alt="Instagram fanpage"></a>
				<a href="hhttps://www.pinterest.com/MoxySEA1/"><img src="{{skin _area="frontend"  url="images/email/pinterest_follow.png"}}" style="width: 25px;" alt="Pinterest fanpage"></a>
				<a href="https://plus.google.com/+Moxyst"><img src="{{skin _area="frontend"  url="images/email/google_follow.png"}}" style="width: 25px;" alt="Google fanpage"></a>
			</div>
		</div>

        <div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
            <p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">ช่องทางการชำระเงิน</p>
            <img src="{{skin _area="frontend"  url="images/email/payment-th.png"}}" alt="ช่องทางการชำระเงิน" style="width: 109px;">
        </div>

        <div class="clear" style="clear:both;"></div>
    </div>

    <div class="footer" style="padding: 0px 33px; margin-top: 18px;">
        <div class="copyr" style="float: left; font-size: 12px;">&copy;2015 Whatsnew Co.,Ltd. All Rights Reserved</div>
        <div class="links" style="float: right; font-size: 12px;">
            <a href="http://www.moxy.co.th/th/about-us/" style="color: #3c3d41;"><span>เกี่ยวกับเรา</span></a> |
            <a href="http://www.moxy.co.th/th/terms-and-conditions/" style="color: #3c3d41;"><span>ข้อกำหนดและเงื่อนไข</span></a> |
            <a href="http://www.moxy.co.th/th/privacy-policy/" style="color: #3c3d41;"><span>ความเป็นส่วนตัว</span></a>
        </div>
    </div>
	<div style="clear:both"></div>
	<hr style="margin: 40px auto; width: 95%;">

	<!--English Version-->
	<div class="mail-body" style="margin:50px auto; width: 95%;">
		<p>Dear {{var customer.name}},</p>
		<br/>

		<p>We just received a request to reset your password.</p>
		<p>Don’t worry, you can simply click on the button and create a new one.</p>
		<br>
		<a href="{{store url="customer/account/resetpassword/" _query_id=\$customer.id _query_token=\$customer.rp_token}}" style="color: #fff; text-decoration: none; background: #EE5191;padding: 10px;">RESET</a>
		<br>
		<br>
		<p>Ignore this message if you didn’t request a new password.</p>
		<br>

		<p>Warm Regards,</p>
		<p>Moxy Team</p>
	</div>

 <div class="navigation-th" style="margin-top: 24px; overflow: hidden; text-align: center;">
		<div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #ee5191; border-bottom: 2px solid #ee5191; padding: 0px 11px;">
			<span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 18; padding: 0px 7px;"><a href="http://moxy.co.th/en/moxy/fashion" style="color: #3c3d41; text-decoration: none;">Fashion</a></span>
			<span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 18; padding: 0px 7px;"><a href="http://moxy.co.th/en/moxy/homeliving" style="color: #3c3d41; text-decoration: none;">Living</a></span>
			<span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 18; padding: 0px 7px;"><a href="http://moxy.co.th/en/moxy/electronics/" style="color: #3c3d41; text-decoration: none;">Electronics</a></span>
			<span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 18; padding: 0px 7px;"><a href="http://lafema.com/en/" style="color: #3c3d41; text-decoration: none;">Beauty</a></span>
			<span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 18; padding: 0px 7px;"><a href="http://venbi.com/en/" style="color: #3c3d41; text-decoration: none;">Baby & Mom</a></span>
			<span class="nav-item" style="border-right: 2px solid #ee5191; line-height: 33px; font-size: 18; padding: 0px 7px;"><a href="http://sanoga.com/en/" style="color: #3c3d41; text-decoration: none;">Health</a></span>
			<span class="nav-item" style="line-height: 33px; font-size: 18; padding: 0px 7px;"><a href="http://petloft.com/en/" style="color: #3c3d41; text-decoration: none;">Pets</a></span>
		</div>
	</div>

    <div class="email-footer" style="background: #bbb; margin-top: 8px; color: #fff; padding: 0px 15px;">
        <div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
            <p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px"><img src="{{skin _area="frontend"  url="images/email/mailbox.png"}}" style="width:18px;"/> Contact Us</p>
            <p style="font-size: 10px; margin: 0;">02-106-8222</p>
            <p style="margin: 0; font-size: 10px;">support@moxyst.com</p>
        </div>

       		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">Follow Us</p>
			<div class="follow-icons" style="width: 100%">
				<a href="https://www.facebook.com/moxyst"><img src="{{skin _area="frontend"  url="images/email/facebook_follow.png"}}" style="width: 25px;" alt="Facebook fanpage"></a>
				<a href="https://twitter.com/moxyst"><img src="{{skin _area="frontend"  url="images/email/twitter_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
				<a href="https://instagram.com/moxy_th/"><img src="{{skin _area="frontend"  url="images/email/instagram_follow.png"}}" style="width: 25px;" alt="Instagram fanpage"></a>
				<a href="https://www.pinterest.com/MoxySEA1/"><img src="{{skin _area="frontend"  url="images/email/pinterest_follow.png"}}" style="width: 25px;" alt="Pinterest fanpage"></a>
				<a href="https://plus.google.com/+Moxyst"><img src="{{skin _area="frontend"  url="images/email/google_follow.png"}}" style="width: 25px;" alt="Google fanpage"></a>
			</div>
		</div>

        <div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
            <p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">Payment Options</p>
            <img src="{{skin _area="frontend"  url="images/email/payment.png"}}" alt="Payment Options" style="width: 109px;">
        </div>

        <div class="clear" style="clear:both;"></div>
    </div>

    <div class="footer" style="padding: 0px 33px; margin-top: 18px;">
        <div class="copyr" style="float: left; font-size: 12px;">&copy;2015 Whatsnew Co.,Ltd. All Rights Reserved</div>
        <div class="links" style="float: right; font-size: 12px;">
            <a href="http://www.moxy.co.th/en/about-us/" style="color: #3c3d41;"><span>About Us</span></a> |
            <a href="http://www.moxy.co.th/en/terms-and-conditions/" style="color: #3c3d41;"><span>Term & Conditions</span></a> |
            <a href="http://www.moxy.co.th/en/privacy-policy/" style="color: #3c3d41;"><span>Privacy Policy</span></a>
        </div>
    </div>
	<div style="clear:both"></div>
</div>
HTML;
/** @var Mage_Core_Model_Email_Template $model */
$model = Mage::getModel('core/email_template');
$model->loadByCode('Reset Password Moxy')
    ->setTemplateCode('Reset Password Moxy')
    ->setTemplateText($template_text)
    ->setTemplateSubject('Password Reset Confirmation for {{var customer.name}}')
    ->setTemplateType(Mage_Core_Model_Email_Template::TYPE_HTML);

try {
    $model->save();
    $config->saveConfig('customer/password/forgot_email_template', $model->getId() , 'stores', $moxyen);
    $config->saveConfig('customer/password/forgot_email_template', $model->getId() , 'stores', $moxyth);
} catch (Mage_Exception $e) {
    throw $e;
}
//==========================================================================
//==========================================================================

//==========================================================================
// RESET PASSWORD SANOGA
//==========================================================================
$template_text =
    <<<HTML
<meta charset="utf-8"/>
<div class="mail-container" style="width:600px; padding:0px 33px; font-family:  'Trebuchet MS', Tahoma; font-size: 14px; color: #3c3d41; margin: 0px;">
    <div class="mail-header" style="width:95%; min-height: 110px; max-height: 110px; position: relative; margin:0px auto;">
        <div class="mail-logo" style="width:220px; min-height: 110px; max-height: 110px; margin:0px auto;">
            <a href="http://sanoga.com/"><img src="{{skin _area="frontend"  url="images/email/sanoga-logo.png"}}" style="width:220px; max-height: 110px; min-height: 110px;" /></a>
        </div>

        <div class="english-below" style="min-height: 16px; max-height: 16px; position: absolute; bottom: 0px; line-height: 16px;">
            <span><img src="{{skin _area="frontend"  url="images/email/flag-en.png"}}" height="16px;"/> Scroll down for English version</span>
        </div>
    </div>

	<img style="width: 100%; margin-top:40px;" src="{{skin _area="frontend"  url="images/email/password_reset_sanoga.png"}}" alt="Thank for your order" />

	<div class="mail-body" style="margin:50px auto; width: 95%;">
		<p>เรียน คุณ {{var customer.name}},</p>
		<br/>

		<p>ทางเราได้รับคำขอเปลี่ยนรหัสผ่านสำหรับบัญชีของคุณ</p>
		<br>

		<a href="{{store url="customer/account/resetpassword/" _query_id=\$customer.id _query_token=\$customer.rp_token}}" style="color: #fff; text-decoration: none; background: #6ABC45;padding: 10px;">เปลี่ยนรหัสผ่าน</a>
		<br>
		<br>
		<p>หากคุณข้ามอีเมลล์นี้ไป รหัสของคุณยังคงเหมือนเดิม</p>
		<p>หากคุณไม่ได้ส่งคำขอนี้ แจ้งให้เราทราบ</p>
		<br>

		<p>Warm Regards,</p>
		<p>Moxy Team</p>
	</div>


<div class="navigation-th" style="margin-top: 24px; text-align:center;  overflow:hidden;">
		<div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #6abc45; border-bottom: 2px solid #6abc45; padding: 0px 0px; overflow:hidden;">
			<span class="nav-item" style="border-right: 2px solid #6abc45; line-height: 33px; font-size: 14; padding: 0px 12px;"><a href="http://www.sanoga.com/th/health-care.html" style="color: #3c3d41; text-decoration: none;">ดูแลสุขภาพ</a></span>
			<span class="nav-item" style="border-right: 2px solid #6abc45; line-height: 33px; font-size: 14; padding: 0px 12px;"><a href="http://www.sanoga.com/th/beauty.html" style="color: #3c3d41; text-decoration: none;">ความงาม</a></span>
			<span class="nav-item" style="border-right: 2px solid #6abc45; line-height: 33px; font-size: 14; padding: 0px 12px;"><a href="http://www.sanoga.com/th/workout-fitness.html" style="color: #3c3d41; text-decoration: none;">ออกกำลังกาย-ฟิตเนส</a></span>
			<span class="nav-item" style="border-right: 2px solid #6abc45; line-height: 33px; font-size: 14; padding: 0px 12px;"><a href="http://www.sanoga.com/th/weight-loss.html" style="color: #3c3d41; text-decoration: none;">อาหารเสริมลดน้ำหนัก</a></span>
			<span class="nav-item" style="line-height: 33px; font-size: 14; padding: 0px 12px;"><a href="http://www.sanoga.com/th/sexual-well-being.html" style="color: #3c3d41; text-decoration: none;">สุขภาพ-เซ็กส์</a></span>
		</div>
	</div>

	<div class="email-footer" style="min-height: 82px; max-heigh: 82px; background: #bbb; margin-top: 8px; color: #fff; padding: 0px 15px;">
		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px"><img src="{{skin _area="frontend"  url="images/email/mailbox.png"}}" style="width:18px;"/> ติดต่อเรา</p>
			<p style="font-size: 10px; margin: 0;">02-106-8222</p>
			<p style="margin: 0; font-size: 10px;">support@sanoga.com</p>
		</div>

		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">ติดตามที่</p>
			<div class="follow-icons" style="width: 100%">
				<a href="https://www.facebook.com/SanogaThailand"><img src="{{skin _area="frontend"  url="images/email/facebook_follow.png"}}" style="width: 25px;" alt="Facebook fanpage"></a>
				<a href="https://twitter.com/sanoga_thailand"><img src="{{skin _area="frontend"  url="images/email/twitter_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
				<a href="#"><img src="{{skin _area="frontend"  url="images/email/instagram_follow.png"}}" style="width: 25px;" alt="Instagram fanpage"></a>
				<a href="https://www.pinterest.com/sanoga/"><img src="{{skin _area="frontend"  url="images/email/pinterest_follow.png"}}" style="width: 25px;" alt="Pinterest fanpage"></a>
				<a href="https://plus.google.com/114390899173698565828/"><img src="{{skin _area="frontend"  url="images/email/google_follow.png"}}" style="width: 25px;" alt="Google fanpage"></a>
			</div>
		</div>

		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">ช่องทางการชำระเงิน</p>
			<img src="{{skin _area="frontend"  url="images/email/payment-th.png"}}" alt="ช่องทางการชำระเงิน" style="width: 109px;">
		</div>
	</div>

	<div class="footer" style="padding: 0px 33px; margin-top: 18px;">
		<div class="copyr" style="float: left; font-size: 12px;">&copy;2015 Whatsnew Co.,Ltd. All Rights Reserved</div>
		<div class="links" style="float: right; font-size: 12px;">
			<a href="http://www.sanoga.com/th/about-us/" style="color: #3c3d41;"><span>เกี่ยวกับเรา</span></a> |
			<a href="http://www.sanoga.com/th/terms-and-conditions/" style="color: #3c3d41;"><span>ข้อกำหนดและเงื่อนไข</span></a> |
			<a href="http://www.sanoga.com/th/privacy-policy/" style="color: #3c3d41;"><span>ความเป็นส่วนตัว</span></a>
		</div>
	</div>
	<div style="clear:both"></div>
	<hr style="margin: 40px auto; width: 95%;">

	<!--English Version-->
	<div class="mail-body" style="margin:50px auto; width: 95%;">
		<p>Dear {{var customer.name}},</p>
		<br/>

		<p>We just received a request to reset your password.</p>
		<p>Don’t worry, you can simply click on the button and create a new one.</p>
		<br>
		<a href="{{store url="customer/account/resetpassword/" _query_id=\$customer.id _query_token=\$customer.rp_token}}" style="color: #fff; text-decoration: none; background: #6ABC45;padding: 10px;">RESET</a>
		<br>
		<br>
		<p>Ignore this message if you didn’t request a new password.</p>
		<br>

		<p>Warm Regards,</p>
		<p>Moxy Team</p>
	</div>

<div class="navigation-th" style="margin-top: 24px; text-align:center;  overflow:hidden;">
		<div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #6abc45; border-bottom: 2px solid #6abc45; padding: 0px 0px; overflow:hidden;">
			<span class="nav-item" style="border-right: 2px solid #6abc45; line-height: 33px; font-size: 14; padding: 0px 12px;"><a href="http://www.sanoga.com/en/health-care.html" style="color: #3c3d41; text-decoration: none;">Health Care</a></span>
			<span class="nav-item" style="border-right: 2px solid #6abc45; line-height: 33px; font-size: 14; padding: 0px 12px;"><a href="http://www.sanoga.com/en/beauty.html" style="color: #3c3d41; text-decoration: none;">Beauty</a></span>
			<span class="nav-item" style="border-right: 2px solid #6abc45; line-height: 33px; font-size: 14; padding: 0px 12px;"><a href="http://www.sanoga.com/en/workout-fitness.html" style="color: #3c3d41; text-decoration: none;">Workout & Fitness</a></span>
			<span class="nav-item" style="border-right: 2px solid #6abc45; line-height: 33px; font-size: 14; padding: 0px 12px;"><a href="http://www.sanoga.com/en/weight-loss.html" style="color: #3c3d41; text-decoration: none;">Weight Loss</a></span>
			<span class="nav-item" style="line-height: 33px; font-size: 14; padding: 0px 12px;"><a href="http://www.sanoga.com/en/sexual-well-being.html" style="color: #3c3d41; text-decoration: none;">Sexual Well-Being</a></span>
		</div>
	</div>

	<div class="email-footer" style="min-height: 82px; max-heigh: 82px; background: #bbb; margin-top: 8px; color: #fff; padding: 0px 15px;">
		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px"><img src="{{skin _area="frontend"  url="images/email/mailbox.png"}}" style="width:18px;"/> Contact Us</p>
			<p style="font-size: 10px; margin: 0;">02-106-8222</p>
			<p style="margin: 0; font-size: 10px;">support@sanoga.com</p>
		</div>

	<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">Follow Us</p>
			<div class="follow-icons" style="width: 100%">
				<a href="https://www.facebook.com/SanogaThailand"><img src="{{skin _area="frontend"  url="images/email/facebook_follow.png"}}" style="width: 25px;" alt="Facebook fanpage"></a>
				<a href="https://twitter.com/sanoga_thailand"><img src="{{skin _area="frontend"  url="images/email/twitter_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
				<a href="#"><img src="{{skin _area="frontend"  url="images/email/instagram_follow.png"}}" style="width: 25px;" alt="Instagram fanpage"></a>
				<a href="https://www.pinterest.com/sanoga/"><img src="{{skin _area="frontend"  url="images/email/pinterest_follow.png"}}" style="width: 25px;" alt="Pinterest fanpage"></a>
				<a href="https://plus.google.com/114390899173698565828/"><img src="{{skin _area="frontend"  url="images/email/google_follow.png"}}" style="width: 25px;" alt="Google fanpage"></a>
			</div>
		</div>

		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">Payment Options</p>
			<img src="{{skin _area="frontend"  url="images/email/payment.png"}}" alt="Payment Options" style="width: 109px;">
		</div>
	</div>

	<div class="footer" style="padding: 0px 33px; margin-top: 18px;">
		<div class="copyr" style="float: left; font-size: 12px;">&copy;2015 Whatsnew Co.,Ltd. All Rights Reserved</div>
		<div class="links" style="float: right; font-size: 12px;">
			<a href="http://www.sanoga.com/en/about-us/" style="color: #3c3d41;"><span>About Us</span></a> |
			<a href="http://www.sanoga.com/en/terms-and-conditions/" style="color: #3c3d41;"><span>Term & Conditions</span></a> |
			<a href="http://www.sanoga.com/en/privacy-policy/" style="color: #3c3d41;"><span>Privacy Policy</span></a>
		</div>
	</div>
	<div style="clear:both"></div>
</div>
HTML;
/** @var Mage_Core_Model_Email_Template $model */
$model = Mage::getModel('core/email_template');
$model->loadByCode('Reset Password Sanoga')
    ->setTemplateCode('Reset Password Sanoga')
    ->setTemplateText($template_text)
    ->setTemplateSubject('Password Reset Confirmation for {{var customer.name}}')
    ->setTemplateType(Mage_Core_Model_Email_Template::TYPE_HTML);

try {
    $model->save();
    $config->saveConfig('customer/password/forgot_email_template', $model->getId() , 'stores', $sen);
    $config->saveConfig('customer/password/forgot_email_template', $model->getId() , 'stores', $sth);
} catch (Mage_Exception $e) {
    throw $e;
}
//==========================================================================
//==========================================================================

//==========================================================================
// RESET PASSWORD VENBI
//==========================================================================
$template_text =
    <<<HTML
<meta charset="utf-8"/>
<div class="mail-container" style="width:600px; padding:0px 33px; font-family:  'Trebuchet MS', Tahoma; font-size: 14px; color: #3c3d41; margin: 0px;">
    <div class="mail-header" style="width:95%; min-height: 110px; max-height: 110px; position: relative; margin:0px auto;">
        <div class="mail-logo" style="width:220px; min-height: 110px; max-height: 110px; margin:0px auto;">
            <a href="http://venbi.com/"><img src="{{skin _area="frontend"  url="images/email/venbi-logo.png"}}" style="width:220px; max-height: 110px; min-height: 110px;" /></a>
        </div>

        <div class="english-below" style="min-height: 16px; max-height: 16px; position: absolute; bottom: 0px; line-height: 16px;">
            <span><img src="{{skin _area="frontend"  url="images/email/flag-en.png"}}" height="16px;"/> Scroll down for English version</span>
        </div>
    </div>

	<img style="width: 100%; margin-top:40px;" src="{{skin _area="frontend"  url="images/email/password_reset_venbi.png"}}" alt="Thank for your order" />

	<div class="mail-body" style="margin:50px auto; width: 95%;">
		<p>เรียน คุณ {{var customer.name}},</p>
		<br/>

		<p>ทางเราได้รับคำขอเปลี่ยนรหัสผ่านสำหรับบัญชีของคุณ</p>
		<br>

		<a href="{{store url="customer/account/resetpassword/" _query_id=\$customer.id _query_token=\$customer.rp_token}}" style="color: #fff; text-decoration: none; background: #F6EB47;padding: 10px;">เปลี่ยนรหัสผ่าน</a>
		<br>
		<br>
		<p>หากคุณข้ามอีเมลล์นี้ไป รหัสของคุณยังคงเหมือนเดิม</p>
		<p>หากคุณไม่ได้ส่งคำขอนี้ แจ้งให้เราทราบ</p>
		<br>

		<p>Warm Regards,</p>
		<p>Moxy Team</p>
	</div>
	<div class="navigation-th" style="margin-top: 24px; overflow:hidden;">
		<div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #f5ea14; border-bottom: 2px solid #f5ea14; padding: 0px 0px; overflow: hidden;">
			<span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 6px;"><a href="http://www.venbi.com/th/formula-nursing.html" style="color: #3c3d41; text-decoration: none;">นมและอุปกรณ์</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 5px;"><a href="http://www.venbi.com/th/diapers.html" style="color: #3c3d41; text-decoration: none;">ผ้าอ้อมสำเร็จรูป</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 5px;"><a href="http://www.venbi.com/th/bath-baby-care.html" style="color: #3c3d41; text-decoration: none;">ผลิตภัณฑ์อาบน้ำและเบบี้แคร์</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 5px;"><a href="http://www.venbi.com/th/toys.html" style="color: #3c3d41; text-decoration: none;">ของเล่น</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 5px;"><a href="http://www.venbi.com/th/clothing.html" style="color: #3c3d41; text-decoration: none;">เสื้อผ้า</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 5px;"><a href="http://www.venbi.com/th/gear.html" style="color: #3c3d41; text-decoration: none;">เตียงและรถเข็น</a></span>
			<span class="nav-item" style="line-height: 33px; font-size: 11; padding: 0px 6px;"><a href="http://www.venbi.com/th/mom-maternity.html" style="color: #3c3d41; text-decoration: none;">แม่-การตั้งครรภ์</a></span>
		</div>
	</div>

	<div class="email-footer" style="min-height: 82px; max-heigh: 82px; background: #bbb; margin-top: 8px; color: #fff; padding: 0px 15px;">
		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px"><img src="{{skin _area="frontend"  url="images/email/mailbox.png"}}" style="width:18px;"/> ติดต่อเรา</p>
			<p style="font-size: 10px; margin: 0;">02-106-8222</p>
			<p style="margin: 0; font-size: 10px;">support@venbi.com</p>
		</div>

		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">ติดตามที่</p>
				<div class="follow-icons" style="width: 100%">
				<a href="https://www.facebook.com/venbithai"><img src="{{skin _area="frontend"  url="images/email/facebook_follow.png"}}" style="width: 25px;" alt="Facebook fanpage"></a>
				<a href="https://twitter.com/venbithai"><img src="{{skin _area="frontend"  url="images/email/twitter_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
				<a href="http://webstagr.am/venbi_thailand"><img src="{{skin _area="frontend"  url="images/email/instagram_follow.png"}}" style="width: 25px;" alt="Instagram fanpage"></a>
				<a href="https://www.pinterest.com/VenbiThailand/"><img src="{{skin _area="frontend"  url="images/email/pinterest_follow.png"}}" style="width: 25px;" alt="Pinterest fanpage"></a>
				<a href="https://plus.google.com/+VenbiThai"><img src="{{skin _area="frontend"  url="images/email/google_follow.png"}}" style="width: 25px;" alt="Google fanpage"></a>
			</div>
		</div>

		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">ช่องทางการชำระเงิน</p>
			<img src="{{skin _area="frontend"  url="images/email/payment-th.png"}}" alt="ช่องทางการชำระเงิน" style="width: 109px;">
		</div>
	</div>

	<div class="footer" style="padding: 0px 33px; margin-top: 18px;">
		<div class="copyr" style="float: left; font-size: 12px;">&copy;2015 Whatsnew Co.,Ltd. All Rights Reserved</div>
		<div class="links" style="float: right; font-size: 12px;">
			<a href="http://www.venbi.com/th/about-us/" style="color: #3c3d41;"><span>เกี่ยวกับเรา</span></a> |
			<a href="http://www.venbi.com/th/terms-and-conditions/" style="color: #3c3d41;"><span>ข้อกำหนดและเงื่อนไข</span></a> |
			<a href="http://www.venbi.com/th/privacy-policy/" style="color: #3c3d41;"><span>ความเป็นส่วนตัว</span></a>
		</div>
	</div>
	<div style="clear:both"></div>
	<hr style="margin: 40px auto; width: 95%;">

	<!--English Version-->
	<div class="mail-body" style="margin:50px auto; width: 95%;">
		<p>Dear {{var customer.name}},</p>
		<br/>

		<p>We just received a request to reset your password.</p>
		<p>Don’t worry, you can simply click on the button and create a new one.</p>
		<br>
		<a href="{{store url="customer/account/resetpassword/" _query_id=\$customer.id _query_token=\$customer.rp_token}}" style="color: #fff; text-decoration: none; background: #F6EB47;padding: 10px;">RESET</a>
		<br>
		<br>
		<p>Ignore this message if you didn’t request a new password.</p>
		<br>

		<p>Warm Regards,</p>
		<p>Moxy Team</p>
	</div>

	<div class="navigation-th" style="margin-top: 24px; overflow:hidden;">
		<div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #f5ea14; border-bottom: 2px solid #f5ea14; padding: 0px 0px; overflow: hidden;">
			<span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 5px;"><a href="http://www.venbi.com/en/formula-nursing.html" style="color: #3c3d41; text-decoration: none;">Formula & Nursing</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 5px;"><a href="http://www.venbi.com/en/diapers.html" style="color: #3c3d41; text-decoration: none;">Diapers</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 5px;"><a href="http://www.venbi.com/en/bath-baby-care.html" style="color: #3c3d41; text-decoration: none;">Bath & Baby Care</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 5px;"><a href="http://www.venbi.com/en/toys.html" style="color: #3c3d41; text-decoration: none;">Toys</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 5px;"><a href="http://www.venbi.com/en/clothing.html" style="color: #3c3d41; text-decoration: none;">Clothing</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5ea14; line-height: 33px; font-size: 11; padding: 0px 5px;"><a href="http://www.venbi.com/en/gear.html" style="color: #3c3d41; text-decoration: none;">Gear</a></span>
			<span class="nav-item" style="line-height: 33px; font-size: 11; padding: 0px 6px;"><a href="http://www.venbi.com/en/mom-maternity.html" style="color: #3c3d41; text-decoration: none;">Mom & Maternity</a></span>
		</div>
	</div>

	<div class="email-footer" style="min-height: 82px; max-heigh: 82px; background: #bbb; margin-top: 8px; color: #fff; padding: 0px 15px;">
		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px"><img src="{{skin _area="frontend"  url="images/email/mailbox.png"}}" style="width:18px;"/> Contact Us</p>
			<p style="font-size: 10px; margin: 0;">02-106-8222</p>
			<p style="margin: 0; font-size: 10px;">support@venbi.com</p>
		</div>

		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">Follow Us</p>
				<div class="follow-icons" style="width: 100%">
				<a href="https://www.facebook.com/venbithai"><img src="{{skin _area="frontend"  url="images/email/facebook_follow.png"}}" style="width: 25px;" alt="Facebook fanpage"></a>
				<a href="https://twitter.com/venbithai"><img src="{{skin _area="frontend"  url="images/email/twitter_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
				<a href="http://webstagr.am/venbi_thailand"><img src="{{skin _area="frontend"  url="images/email/instagram_follow.png"}}" style="width: 25px;" alt="Instagram fanpage"></a>
				<a href="https://www.pinterest.com/VenbiThailand/"><img src="{{skin _area="frontend"  url="images/email/pinterest_follow.png"}}" style="width: 25px;" alt="Pinterest fanpage"></a>
				<a href="https://plus.google.com/+VenbiThai"><img src="{{skin _area="frontend"  url="images/email/google_follow.png"}}" style="width: 25px;" alt="Google fanpage"></a>
			</div>
		</div>

		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">Payment Options</p>
			<img src="{{skin _area="frontend"  url="images/email/payment.png"}}" alt="Payment Options" style="width: 109px;">
		</div>
	</div>

	<div class="footer" style="padding: 0px 33px; margin-top: 18px;">
		<div class="copyr" style="float: left; font-size: 12px;">&copy;2015 Whatsnew Co.,Ltd. All Rights Reserved</div>
		<div class="links" style="float: right; font-size: 12px;">
			<a href="http://www.venbi.com/en/about-us/" style="color: #3c3d41;"><span>About Us</span></a> |
			<a href="http://www.venbi.com/en/terms-and-conditions/" style="color: #3c3d41;"><span>Term & Conditions</span></a> |
			<a href="http://www.venbi.com/en/privacy-policy/" style="color: #3c3d41;"><span>Privacy Policy</span></a>
		</div>
	</div>
	<div style="clear:both"></div>
</div>
HTML;
/** @var Mage_Core_Model_Email_Template $model */
$model = Mage::getModel('core/email_template');
$model->loadByCode('Reset Password Venbi')
    ->setTemplateCode('Reset Password Venbi')
    ->setTemplateText($template_text)
    ->setTemplateSubject('Password Reset Confirmation for {{var customer.name}}')
    ->setTemplateType(Mage_Core_Model_Email_Template::TYPE_HTML);

try {
    $model->save();
    $config->saveConfig('customer/password/forgot_email_template', $model->getId() , 'stores', $ven);
    $config->saveConfig('customer/password/forgot_email_template', $model->getId() , 'stores', $vth);
} catch (Mage_Exception $e) {
    throw $e;
}
//==========================================================================
//==========================================================================

//==========================================================================
// RESET PASSWORD LAFEMA
//==========================================================================
$template_text =
    <<<HTML
<meta charset="utf-8"/>
<div class="mail-container" style="width:600px; padding:0px 33px; font-family:  'Trebuchet MS', Tahoma; font-size: 14px; color: #3c3d41; margin: 0px;">
    <div class="mail-header" style="width:95%; min-height: 110px; max-height: 110px; position: relative; margin:0px auto;">
        <div class="mail-logo" style="width:220px; min-height: 110px; max-height: 110px; margin:0px auto;">
            <a href="http://lafema.com/"><img src="{{skin _area="frontend"  url="images/email/lafema-logo.png"}}" style="width:220px; max-height: 110px; min-height: 110px;" /></a>
        </div>

        <div class="english-below" style="min-height: 16px; max-height: 16px; position: absolute; bottom: 0px; line-height: 16px;">
            <span><img src="{{skin _area="frontend"  url="images/email/flag-en.png"}}" height="16px;"/> Scroll down for English version</span>
        </div>
    </div>

	<img style="width: 100%; margin-top:40px;" src="{{skin _area="frontend"  url="images/email/password_reset_lafema.png"}}" alt="Thank for your order" />

	<div class="mail-body" style="margin:50px auto; width: 95%;">
		<p>เรียน คุณ {{var customer.name}},</p>
		<br/>

		<p>ทางเราได้รับคำขอเปลี่ยนรหัสผ่านสำหรับบัญชีของคุณ</p>
		<br>

		<a href="{{store url="customer/account/resetpassword/" _query_id=\$customer.id _query_token=\$customer.rp_token}}" style="color: #fff; text-decoration: none; background: #F5A2B2;padding: 10px;">เปลี่ยนรหัสผ่าน</a>
		<br>
		<br>
		<p>หากคุณข้ามอีเมลล์นี้ไป รหัสของคุณยังคงเหมือนเดิม</p>
		<p>หากคุณไม่ได้ส่งคำขอนี้ แจ้งให้เราทราบ</p>
		<br>

		<p>Warm Regards,</p>
		<p>Moxy Team</p>
	</div>



	<div class="navigation-th" style="margin-top: 24px; text-align:center; overflow:hidden;">
		<div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #f5a2b2; border-bottom: 2px solid #f5a2b2; padding: 0px 0px;">
			<span class="nav-item" style="border-right: 2px solid #f5a2b2; line-height: 33px; font-size: 16; padding: 0px 18px;"><a href="http://www.lafema.com/th/makeup.html" style="color: #3c3d41; text-decoration: none;">เมคอัพ</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5a2b2; line-height: 33px; font-size: 16; padding: 0px 18px;"><a href="http://www.lafema.com/th/skincare.html" style="color: #3c3d41; text-decoration: none;">ดูแลผิวหน้า</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5a2b2; line-height: 33px; font-size: 16; padding: 0px 18px;"><a href="http://www.lafema.com/th/bath-body-hair.html" style="color: #3c3d41; text-decoration: none;">ดูแลผิวกาย-ผม</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5a2b2; line-height: 33px; font-size: 16; padding: 0px 18px;"><a href="http://www.lafema.com/th/fragrance.html" style="color: #3c3d41; text-decoration: none;">น้ำหอม</a></span>
			<span class="nav-item" style="line-height: 33px; font-size: 16; padding: 0px 12px;"><a href="http://www.lafema.com/th/men.html" style="color: #3c3d41; text-decoration: none;">สำหรับผู้ชาย</a></span>
		</div>
	</div>

	<div class="email-footer" style="min-height: 82px; max-heigh: 82px; background: #bbb; margin-top: 8px; color: #fff; padding: 0px 15px;">
		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px"><img src="{{skin _area="frontend"  url="images/email/mailbox.png"}}" style="width:18px;"/> ติดต่อเรา</p>
			<p style="font-size: 10px; margin: 0;">02-106-8222</p>
			<p style="margin: 0; font-size: 10px;">support@lafema.com</p>
		</div>

		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">ติดตามที่</p>
			<div class="follow-icons" style="width: 100%">
				<a href="https://www.facebook.com/lafemathai"><img src="{{skin _area="frontend"  url="images/email/facebook_follow.png"}}" style="width: 25px;" alt="Facebook fanpage"></a>
				<a href="https://twitter.com/lafemath"><img src="{{skin _area="frontend"  url="images/email/twitter_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
				<a href="https://instagram.com/lafema.th/"><img src="{{skin _area="frontend"  url="images/email/instagram_follow.png"}}" style="width: 25px;" alt="Instagram fanpage"></a>
				<a href="https://www.pinterest.com/lafema_thai/"><img src="{{skin _area="frontend"  url="images/email/pinterest_follow.png"}}" style="width: 25px;" alt="Pinterest fanpage"></a>
				<a href="https://plus.google.com/103314178165403417268/"><img src="{{skin _area="frontend"  url="images/email/google_follow.png"}}" style="width: 25px;" alt="Google fanpage"></a>
			</div>
		</div>

		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">ช่องทางการชำระเงิน</p>
			<img src="{{skin _area="frontend"  url="images/email/payment-th.png"}}" alt="ช่องทางการชำระเงิน" style="width: 109px;">
		</div>
	</div>

	<div class="footer" style="padding: 0px 33px; margin-top: 18px;">
		<div class="copyr" style="float: left; font-size: 12px;">&copy;2015 Whatsnew Co.,Ltd. All Rights Reserved</div>
		<div class="links" style="float: right; font-size: 12px;">
			<a href="http://www.lafema.com/th/about-us/" style="color: #3c3d41;"><span>เกี่ยวกับเรา</span></a> |
			<a href="http://www.lafema.com/th/terms-and-conditions/" style="color: #3c3d41;"><span>ข้อกำหนดและเงื่อนไข</span></a> |
			<a href="http://www.lafema.com/th/privacy-policy/" style="color: #3c3d41;"><span>ความเป็นส่วนตัว</span></a>
		</div>
	</div>
	<div style="clear:both"></div>
	<hr style="margin: 40px auto; width: 95%;">

	<!--English Version-->
	<div class="mail-body" style="margin:50px auto; width: 95%;">
		<p>Dear {{var customer.name}},</p>
		<br/>

		<p>We just received a request to reset your password.</p>
		<p>Don’t worry, you can simply click on the button and create a new one.</p>
		<br>
		<a href="{{store url="customer/account/resetpassword/" _query_id=\$customer.id _query_token=\$customer.rp_token}}" style="color: #fff; text-decoration: none; background: #F5A2B2;padding: 10px;">RESET</a>
		<br>
		<br>
		<p>Ignore this message if you didn’t request a new password.</p>
		<br>

		<p>Warm Regards,</p>
		<p>Moxy Team</p>
	</div>

	 <div class="navigation-th" style="margin-top: 24px; text-align:center; overflow:hidden;">
		<div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #f5a2b2; border-bottom: 2px solid #f5a2b2; padding: 0px 0px;">
			<span class="nav-item" style="border-right: 2px solid #f5a2b2; line-height: 33px; font-size: 16; padding: 0px 18px;"><a href="http://www.lafema.com/en/makeup.html" style="color: #3c3d41; text-decoration: none;">Makeup</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5a2b2; line-height: 33px; font-size: 16; padding: 0px 18px;"><a href="http://www.lafema.com/en/skincare.html" style="color: #3c3d41; text-decoration: none;">Skin Care</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5a2b2; line-height: 33px; font-size: 16; padding: 0px 18px;"><a href="http://www.lafema.com/en/bath-body-hair.html" style="color: #3c3d41; text-decoration: none;">Bath & Body-Hair</a></span>
			<span class="nav-item" style="border-right: 2px solid #f5a2b2; line-height: 33px; font-size: 16; padding: 0px 18px;"><a href="http://www.lafema.com/en/fragrance.html" style="color: #3c3d41; text-decoration: none;">Fragrance</a></span>
			<span class="nav-item" style="line-height: 33px; font-size: 16; padding: 0px 18px;"><a href="http://www.lafema.com/en/men.html" style="color: #3c3d41; text-decoration: none;">Men</a></span>
		</div>
	</div>

	<div class="email-footer" style="min-height: 82px; max-heigh: 82px; background: #bbb; margin-top: 8px; color: #fff; padding: 0px 15px;">
		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px"><img src="{{skin _area="frontend"  url="images/email/mailbox.png"}}" style="width:18px;"/> Contact Us</p>
			<p style="font-size: 10px; margin: 0;">02-106-8222</p>
			<p style="margin: 0; font-size: 10px;">support@lafema.com</p>
		</div>

		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">Follow Us</p>
							<div class="follow-icons" style="width: 100%">
				<a href="https://www.facebook.com/lafemathai"><img src="{{skin _area="frontend"  url="images/email/facebook_follow.png"}}" style="width: 25px;" alt="Facebook fanpage"></a>
				<a href="https://twitter.com/lafemath"><img src="{{skin _area="frontend"  url="images/email/twitter_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
				<a href="https://instagram.com/lafema.th/"><img src="{{skin _area="frontend"  url="images/email/instagram_follow.png"}}" style="width: 25px;" alt="Instagram fanpage"></a>
				<a href="https://www.pinterest.com/lafema_thai/"><img src="{{skin _area="frontend"  url="images/email/pinterest_follow.png"}}" style="width: 25px;" alt="Pinterest fanpage"></a>
				<a href="https://plus.google.com/103314178165403417268/"><img src="{{skin _area="frontend"  url="images/email/google_follow.png"}}" style="width: 25px;" alt="Google fanpage"></a>
			</div>
		</div>

		<div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
			<p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">Payment Options</p>
			<img src="{{skin _area="frontend"  url="images/email/payment.png"}}" alt="Payment Options" style="width: 109px;">
		</div>
	</div>

	<div class="footer" style="padding: 0px 33px; margin-top: 18px;">
		<div class="copyr" style="float: left; font-size: 12px;">&copy;2015 Whatsnew Co.,Ltd. All Rights Reserved</div>
		<div class="links" style="float: right; font-size: 12px;">
			<a href="http://www.lafema.com/en/about-us/" style="color: #3c3d41;"><span>About Us</span></a> |
			<a href="http://www.lafema.com/en/terms-and-conditions/" style="color: #3c3d41;"><span>Term & Conditions</span></a> |
			<a href="http://www.lafema.com/en/privacy-policy/" style="color: #3c3d41;"><span>Privacy Policy</span></a>
		</div>
	</div>
	<div style="clear:both"></div>
</div>
HTML;
/** @var Mage_Core_Model_Email_Template $model */
$model = Mage::getModel('core/email_template');
$model->loadByCode('Reset Password Lafema')
    ->setTemplateCode('Reset Password Lafema')
    ->setTemplateText($template_text)
    ->setTemplateSubject('Password Reset Confirmation for {{var customer.name}}')
    ->setTemplateType(Mage_Core_Model_Email_Template::TYPE_HTML);

try {
    $model->save();
    $config->saveConfig('customer/password/forgot_email_template', $model->getId() , 'stores', $len);
    $config->saveConfig('customer/password/forgot_email_template', $model->getId() , 'stores', $lth);
} catch (Mage_Exception $e) {
    throw $e;
}
//==========================================================================
//==========================================================================