<?php
/**
 * Created by PhpStorm.
 * User: ducnt
 * Date: 7/28/15
 * Time: 3:02 PM
 */ 
class SM_Layoutelements_Helper_Data extends Mage_Core_Helper_Abstract {

    public function stripDescription($str = '', $size = 80)
    {
        if (strlen($str) > $size) {
            $str = substr($str, 0, $size) . '...';
        }
        return $str;
    }
}