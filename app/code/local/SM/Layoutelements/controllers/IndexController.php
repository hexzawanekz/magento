<?php
/**
 * Created by PhpStorm.
 * User: ducnt
 * Date: 8/18/15
 * Time: 2:54 PM
 */

class SM_Layoutelements_IndexController extends Mage_Core_Controller_Front_Action {


    public function indexAction(){
        $collection =  Mage::getModel('sm_recommendationanalysis/productsflat')
            ->getCollection()
            ->addFieldToSelect('combinationProduct')
            ->addFieldToFilter('baseProduct', 6648);
        $productIds = array();
        foreach($collection as $item){
            $productIds[] = $item->getData('combinationProduct');
        }
        $prodCollection = Mage::getResourceModel('catalog/product_collection')
            ->addAttributeToSelect('entity_id')
            ->addAttributeToFilter('entity_id', array('in' => $productIds))

        ;
        Mage::getSingleton('catalog/product_status')->addSaleableFilterToCollection($prodCollection);
        Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($prodCollection);
        Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($prodCollection);
        $prodCollection->getSelect()->limit(8);


        return nulll;
    }

    public function testAction(){
        ini_set('max_execution_time', 0);
        ini_set('display_error', 1);
        ini_set('memory_limit', -1);
        // get store ids
        $en     = Mage::getModel('core/store')->load('en', 'code')->getId();// Petloft
        $th     = Mage::getModel('core/store')->load('th', 'code')->getId();
        $len    = Mage::getModel('core/store')->load('len', 'code')->getId();// Lafema
        $lth    = Mage::getModel('core/store')->load('lth', 'code')->getId();
        $ven    = Mage::getModel('core/store')->load('ven', 'code')->getId();// Venbi
        $vth    = Mage::getModel('core/store')->load('vth', 'code')->getId();
        $sen    = Mage::getModel('core/store')->load('sen', 'code')->getId();// Sanoga
        $sth    = Mage::getModel('core/store')->load('sth', 'code')->getId();
        $moxyen = Mage::getModel('core/store')->load('moxyen', 'code')->getId();// Moxy
        $moxyth = Mage::getModel('core/store')->load('moxyth', 'code')->getId();


        $enStoresIds = array($en, $len, $ven, $sen);
        $thStoresIds = array(0, $th, $lth, $vth, $sth);

        $defaultEnStore = $moxyen;
        $defaultThStore = $moxyth;

        /** @var Mage_Review_Model_Resource_Review_Collection $reviewCollection */
        $reviewCollection = Mage::getModel('review/review')->getCollection();

        /** @var Mage_Review_Model_Review $_review */
        foreach($reviewCollection as $_review){
            /** @var Mage_Review_Model_Review $review */
            $review = (Mage::getModel('review/review')->load($_review->getId()));

            if(count(array_intersect($review->getStores(), $enStoresIds)) < count(array_intersect($review->getStores(), $thStoresIds))) {
                /** Review of THAI Stores*/
                $review->setData('stores', $thStoresIds);
            } else {
                /** Review of ENG Stores */
                $review->setData('stores', $enStoresIds);
            }

            $review->save();
        }
    }
} 