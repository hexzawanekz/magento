<?php

class SM_Filter_Block_Catalog_Layer_View extends Mage_Catalog_Block_Layer_View
{
    protected function _initBlocks()
    {
        $this->_stateBlockName = 'sm_filter/catalog_layer_state';
        $this->_categoryBlockName = 'sm_filter/catalog_layer_filter_category';
        $this->_attributeFilterBlockName = 'sm_filter/catalog_layer_filter_attribute';
        $this->_priceFilterBlockName = 'sm_filter/catalog_layer_filter_price';
        $this->_decimalFilterBlockName = 'sm_filter/catalog_layer_filter_decimal';
    }

    protected function _getAttributeFilterBlockName()
    {
        return 'sm_filter/catalog_layer_filter_attribute';
    }

    /**
     * if current category belong to deal / all deals: return true
     * @return bool
     */
    public function checkAllDealsSubCat()
    {
        $currentCategory = $this->getLayer()->getCurrentCategory();
        $parentIds = $currentCategory->getPathIds();
        $cats = Mage::getModel('catalog/category')
            ->getCollection()
            ->addFieldToFilter('entity_id', $parentIds)
            ->addAttributeToSelect('*');
        foreach($cats as $cat){
            /** @var Mage_Catalog_Model_Category $cat */
            if($cat->getName() == 'All Deals' || $cat->getName() == 'โปรโมชั่นทั้งหมด'){
                return true;
            }
        }
        return false;
    }
}