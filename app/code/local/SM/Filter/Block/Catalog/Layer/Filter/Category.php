<?php

class SM_Filter_Block_Catalog_Layer_Filter_Category extends SM_Filter_Block_Catalog_Layer_Filter_Abstract
{
    public function __construct()
    {
        parent::__construct();
        $this->_filterModelName = 'catalog/layer_filter_category';
    }
}