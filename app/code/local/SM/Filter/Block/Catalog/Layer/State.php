<?php

class SM_Filter_Block_Catalog_Layer_State extends Mage_Catalog_Block_Layer_State
{
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('catalog/layer/state.phtml');
    }
}