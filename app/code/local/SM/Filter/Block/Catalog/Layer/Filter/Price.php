<?php

class SM_Filter_Block_Catalog_Layer_Filter_Price extends SM_Filter_Block_Catalog_Layer_Filter_Abstract
{
    public function __construct()
    {
        parent::__construct();

        $this->_filterModelName = 'sm_filter/catalog_layer_filter_price';
    }

    protected function _prepareFilter()
    {
        $this->_filter->setAttributeModel($this->getAttributeModel());
        return $this;
    }
}