<?php

class SM_Filter_Block_CatalogSearch_Layer_Filter_Attribute extends SM_Filter_Block_Catalog_Layer_Filter_Attribute
{
    public function __construct()
    {
        parent::__construct();
        $this->_filterModelName = 'catalogsearch/layer_filter_attribute';
    }
}