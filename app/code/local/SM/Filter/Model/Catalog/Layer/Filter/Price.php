<?php

class SM_Filter_Model_Catalog_Layer_Filter_Price extends Mage_Catalog_Model_Layer_Filter_Price
{
    /**
     * Get minimum price from layer products set
     *
     * @return float
     */
    public function getMinPriceInt()
    {
        $minPrice = $this->getData('min_price_int');
        if (is_null($minPrice)) {
            $minPrice = $this->getLayer()->getProductCollection()->getMinPrice();
            $minPrice = floor($minPrice);
            $this->setData('min_price_int', $minPrice);
        }

        return $minPrice;
    }

    public function getMinimumPrice($category)
    {
        if($category instanceof Mage_Catalog_Model_Category){
            $productColl = Mage::getModel('catalog/product')->getCollection()
                ->addCategoryFilter($category)
                ->addAttributeToSort('price', 'asc')
                ->addAttributeToFilter('status', 1)
                ->addAttributeToFilter('visibility', array('in' => array(2,4)))
                ->setPageSize(1)
                ->load();

            $lowestProductPrice = $productColl->getFirstItem()->getFinalPrice();
            return is_numeric($lowestProductPrice) ? $lowestProductPrice : 0;
        }
        return false;
    }

    public function getMaximumPrice($category)
    {
        if($category instanceof Mage_Catalog_Model_Category){
            $productColl = Mage::getModel('catalog/product')->getCollection()
                ->addCategoryFilter($category)
                ->addAttributeToFilter('status', 1)
                ->addAttributeToFilter('visibility', array('in' => array(2,4)))
                ->addAttributeToSort('price', 'desc')
                ->setPageSize(1)
                ->load();

            $highestProductPrice = $productColl->getFirstItem()->getFinalPrice();
            return is_numeric($highestProductPrice) ? $highestProductPrice : 0;
        }
        return false;
    }

    /**
     * @param $key (search key)
     * @return max price of search category
     * get max price in case of Search page
     */
    public function getSearchMaxPrice($key)
    {
        $collection = Mage::getResourceModel('catalog/product_collection');
        $collection->addAttributeToFilter('name', array('like' => '%' . $key . '%'))
            ->addAttributeToFilter('status', 1)
            ->addAttributeToFilter('visibility', array('in' => array(3,4)))
            ->setOrder('price', 'DESC');
        $maxPrice =  $collection->getFirstItem()->getFinalPrice();
        return is_numeric($maxPrice) ? $maxPrice : 0;
    }

    /**
     * @param $key (search key)
     * @return min price of search category
     * get min price in case of Search page
     */
    public function getSearchMinPrice($key)
    {
        $collection = Mage::getResourceModel('catalog/product_collection');
        $collection->addAttributeToFilter('name', array('like' => '%' . $key . '%'))
            ->addAttributeToFilter('status', 1)
            ->addAttributeToFilter('visibility', array('in' => array(3,4)))
            ->setOrder('price', 'ASC');
        $minPrice =  $collection->getFirstItem()->getFinalPrice();
        return is_numeric($minPrice) ? $minPrice : 0;
    }
}