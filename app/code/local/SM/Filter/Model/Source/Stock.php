<?php

class SM_Filter_Model_Source_Stock
{
    public function toOptionArray()
    {
        $options = array();
        
        $options[] = array(
            'value' => 0,
            'label' => Mage::helper('sm_filter')->__('No'),
        );         
        $options[] = array(
            'value' => 1,
            'label' => Mage::helper('sm_filter')->__('Yes'),
        );         
        $options[] = array(
            'value' => 2,
            'label' => Mage::helper('sm_filter')->__('Yes for Catalog, No for Search'),
        );         

        return $options;
    }
}