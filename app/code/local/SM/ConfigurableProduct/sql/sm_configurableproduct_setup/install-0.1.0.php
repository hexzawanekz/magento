<?php

// Add attribute "colors" to
$installer = $this;
$setup = new Mage_Eav_Model_Entity_Setup('core_setup');
$installer->startSetup();

$attributeGroup = 'General';
$attributeCode = 'colors';

// Update "color" attribute: change scope to global
$setup->updateAttribute('catalog_product', $attributeCode, 'is_global', Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL);
$setup->updateAttribute('catalog_product', $attributeCode, 'is_configurable', '1');
$setup->updateAttribute('catalog_product', $attributeCode, 'frontend_input', 'select');

// Add the attribute to the existing groups:
$attributeSetCollection = Mage::getResourceModel('eav/entity_attribute_set_collection')->load();
/**
 * @var  $id
 * @var Mage_Eav_Model_Entity_Attribute_Set $attributeSet $attributeSet
 */
foreach ($attributeSetCollection as $id => $attributeSet) {
    $attributeSetName = $attributeSet->getAttributeSetName();
    $setup->addAttributeToGroup('catalog_product', $attributeSetName, $attributeGroup, $attributeCode, '13');
}

// Done:
$installer->endSetup();