<?php

class SM_CsvMarketingFiles_Block_Adminhtml_System_Config_Form_Button extends Mage_Adminhtml_Block_System_Config_Form_Field {
    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        $this->setElement($element);

        $backUrl = Mage::helper('core/url')->getCurrentUrl();
        $param = trim($element->getLabel());



        $url = $this->getUrl('sm_csvmarketingfiles/index/index');
        $url .= '?param='.$param.'&backurl='.base64_encode($backUrl);

        $html = $this->getLayout()->createBlock('adminhtml/widget_button')
            ->setType('button')
            ->setClass('scalable')
            ->setLabel('Export CSV')
            ->setOnClick("setLocation('{$url}')")
            ->toHtml();

        return $html;
    }
}