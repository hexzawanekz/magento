<?php

class SM_CsvMarketingFiles_IndexController extends Mage_Core_Controller_Front_Action {
    public function indexAction() {
        $code = $this->getRequest()->getParam('param');
        if($code == '') {
            $this->_redirect('noRoute');
        }

        /** @var SM_CsvMarketingFiles_Helper_Data $helper */
        $helper = Mage::helper('sm_csvmarketingfiles');
        $sftp = $helper->open();

        if ($sftp instanceof Varien_Io_File || $sftp instanceof Varien_Io_Sftp) {
            $lastestExportData = Mage::getStoreConfig(SM_CsvMarketingFiles_Helper_Data::XML_PATH_CSV_MARKETING_DATE_EXPORT);

            if($code === 'Moxy All') {
                $filename = $lastestExportData.' - MoxystAll.csv';
                $downloadFileName = $lastestExportData.'-MoxystAll.csv';
            } else {
                $filename = $lastestExportData.' - '.$code.'.csv';
                $downloadFileName = $lastestExportData.'-'.$code.'.csv';
            }

            //Read file from server
            $fileContent = $sftp->read($filename);

            header("Content-type: text/csv");
            header("Content-Disposition: attachment; filename=".$downloadFileName);
            header("Pragma: no-cache");
            header("Expires: 0");

            echo $fileContent;
        }

        //$this->_redirectUrl($backUrl);
    }
}