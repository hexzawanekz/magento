<?php

class SM_Transactionemails_Block_Adminhtml_Satisfaction_Grid_Renderer_Templatecode extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Action
{
    public function render(Varien_Object $row)
    {
        /** @var Mage_Newsletter_Model_Template $template */
        $template = Mage::getModel('newsletter/template')->load($row->getData('template_id'));
        return $template->getTemplateCode();
    }
}