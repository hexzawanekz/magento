<?php

class SM_Transactionemails_Block_Adminhtml_Satisfaction_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('satisfaction_grid');
        //$this->setUseAjax(true);
        $this->setDefaultSort('ID');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('sm_transactionemails/queue')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns(){
        $this->addColumn('queue_id', array(
            'index' => 'queue_id',
            'header' => Mage::helper('sm_transactionemails')->__('ID'),
            'type' => 'number',
            'sortable' => true,
            'width' => '20px',
        ));

        $this->addColumn('subject', array(
                'index' => 'subject',
                'header' => Mage::helper('sm_transactionemails')->__('Subject'),
                'type'  => 'text',
                'sortable'  => true,
                'width' => '400px')
        );

        $this->addColumn('template_id', array(
                'index' => 'template_id',
                'header' => Mage::helper('sm_transactionemails')->__('Template Id'),
                'type'  => 'number',
                'sortable'=>true,
                'width'     => '20px'
            )
        );

        $this->addColumn('template_code', array(
                'header' => Mage::helper('sm_transactionemails')->__('Template Code'),
                'type'  => 'text',
                'sortable'=>true,
                'width'     => '350px',
                'renderer' => 'sm_transactionemails/adminhtml_satisfaction_grid_renderer_templatecode'
            )
        );

        $this->addColumn('customer_name', array(
                'index' => 'customer_name',
                'header' => Mage::helper('sm_transactionemails')->__('Customer Name'),
                'type'  => 'text',
                'sortable'=>true,
                'width'     => '150px'
            )
        );

        $this->addColumn('receiver', array(
                'index' => 'receiver',
                'header' => Mage::helper('sm_transactionemails')->__('Receiver'),
                'type'  => 'text',
                'sortable'=>true,
                'width'     => '250px'
            )
        );

        $this->addColumn('start_at', array(
                'index' => 'start_at',
                'header' => Mage::helper('sm_transactionemails')->__('Start At'),
                'type'  => 'date',
                'sortable'=>true,
                'width'     => '300px'
            )
        );

        $this->addColumn('finish_at', array(
                'index' => 'finish_at',
                'header' => Mage::helper('sm_transactionemails')->__('Finish At'),
                'type'  => 'date',
                'sortable'=>true,
                'width'     => '300px'
            )
        );

        $this->addColumn('status', array(
                'index' => 'status',
                'header' => Mage::helper('sm_transactionemails')->__('Status'),
                'type'  => 'options',
                'sortable'=>true,
                'options'   => array(
                    1 	=> Mage::helper('sm_transactionemails')->__('Sent'),
                    0 	=> Mage::helper('sm_transactionemails')->__('Not Sent'),
                ),
                'width'     => '150px'
            )
        );

        $this->addExportType('*/*/exportCsv', Mage::helper('sm_transactionemails')->__('CSV'));
        $this->addExportType('*/*/exportXml', Mage::helper('sm_transactionemails')->__('XML'));

        return parent::_prepareColumns();
    }
}