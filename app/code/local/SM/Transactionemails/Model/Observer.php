<?php

/**
 * Created by PhpStorm.
 * User: smartosc
 * Date: 3/10/2016
 * Time: 1:47 PM
 */
class SM_Transactionemails_Model_Observer
{
    /**
     * @param $observer
     * Add email to queue when a order change status to Complete
     * That email will be send after 3 days
     */
    public function addEmailToQueue($observer)
    {
        /** @var Mage_Sales_Model_Order $order */
        $order = $observer->getOrder();
        $status = $order->getStatus();
        if ($status == 'complete') {
            $templateCode = 'Customer Satisfaction Orami';
            $day = 3;

            /** @var SM_Transactionemails_Model_Queue $queue */
            $queue = Mage::getModel('sm_transactionemails/queue');

            /** @var Mage_Newsletter_Model_Template $template */
            $template = Mage::getModel('newsletter/template')->getCollection()
                ->addFieldToFilter('template_code', $templateCode)
                ->getFirstItem();
//            $collection = $queue->getCollection();
//            $collection->addFieldToFilter('template_id', $template->getId())
//                ->addFieldToFilter('receiver', $order->getCustomerEmail())
//                ->getFirstItem();
//            if ($collection->getSize() > 0) {
//                return;
//            }
            if (!$template->getData()) {
                return;
            }
            $queue->setTemplateId($template->getId())
                ->setQueueStatus(Mage_Newsletter_Model_Queue::STATUS_NEVER);

            if ($queue->getQueueStatus() == Mage_Newsletter_Model_Queue::STATUS_NEVER) {
                $date = date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s') . ' +' . $day . ' days'));
                $queue->setData('start_at', $date);
            }
            $queue->setData('subject', $template->getTemplateSubject())
                ->setData('sender_name', $template->getTemplateSenderName())
                ->setData('sender_email', $template->getTemplateSenderEmail())
                ->setData('template_id', $template->getId())
                ->setData('receiver', $order->getCustomerEmail())
                ->setData('order_id', $order->getIncrementId())
                ->setData('customer_name', $order->getCustomerName())
                ->setData('finish_at', null)
                ->setData('status', 0);
            $queue->save();
        }
    }

    /**
     * A Cronjob for send customer satisfaction mail
     * It will be sent every 5 minutes
     */
    public function scheduleSendMail()
    {
        /** @var SM_Transactionemails_Model_Queue $queue */
        $queue = Mage::getModel('sm_transactionemails/queue');

        $queueCollection = $queue->getCollection();
        $queueCollection->addFieldToFilter('status', 0)->addFieldToFilter('start_at', array('lteq' => date('Y-m-d H:i:s')));

        $templateCode = 'Customer Satisfaction Orami';
        /** @var Mage_Newsletter_Model_Template $template */
        $template = Mage::getModel('newsletter/template')->getCollection()
            ->addFieldToFilter('template_code', $templateCode)
            ->getFirstItem();

        if($queueCollection->getSize() == 0){
            return;
        }
        if(!$template->getData()){
            return;
        }

        /** @var Mage_Core_Model_Email_Template $sender */
        $sender = Mage::getModel('core/email_template');
        $sender->setSenderName($template->getTemplateSenderName())
            ->setSenderEmail($template->getTemplateSenderEmail())
            ->setTemplateSubject($template->getTemplateSubject())
            ->setTemplateType($template->getTemplateType())
            ->setTemplateText($template->getTemplateText())
            ->setTemplateStyles($template->getTemplateStyles())
            ->setTemplateFilter(Mage::helper('newsletter')->getTemplateProcessor());

        foreach($queueCollection as $item){
            $successSend = $sender->send($item->getData('receiver'), $item->getData('customer_name'),
                array('customer_name' => $item->getData('customer_name'), 'order_id' => $item->getData('order_id')));
            if($successSend){
                $item->setData('status', 1);
                $item->setData('finish_at', date('Y-m-d H:i:s'));
                $item->save();
            };
        }
    }
}