<?php

/**
 * Created by PhpStorm.
 * User: smartosc
 * Date: 3/11/2016
 * Time: 4:33 PM
 */
class SM_Transactionemails_Model_Resource_Queue_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    public function _construct()
    {
        $this->_init('sm_transactionemails/queue');
    }
}