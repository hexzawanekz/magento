<?php

/**
 * Created by PhpStorm.
 * User: smartosc
 * Date: 3/11/2016
 * Time: 4:32 PM
 */
class SM_Transactionemails_Model_Resource_Queue extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('sm_transactionemails/queue', 'queue_id');
    }
}