<?php

/**
 * Warning: this class will run very slowly because we have a lot of nested for loop.
 * We have to use them because there's no other choice.
 * Find out to log file 'netsuite_inventory.log' to see the results of work.
 */
class SM_NetsuiteInventorySync_Model_Cronjobs
{
    // non-exported orders collection
    private $_orders = null;

    // resource
    private $_resource = null;
    private $_readConnection = null;
    private $_writeConnection = null;
    private $_productResource = null;

    // table name
    private $_tblOrder = '';
    private $_tblStockItem = '';
    private $_tblOrderItem = '';

    /**
     * Main function
     */
    public function syncFromNetSuite()
    {
        // set max_execution_time
        ini_set('max_execution_time', 0);

        // set memory_limit
        ini_set('memory_limit', '-1');

        // init resource
        $this->_resource = Mage::getSingleton('core/resource');
        $this->_readConnection = $this->_resource->getConnection('core_read');
        $this->_writeConnection = $this->_resource->getConnection('core_write');
        $this->_productResource = Mage::getResourceModel('catalog/product');

        // get table name
        $this->_tblOrder = $this->_resource->getTableName("sales/order");
        $this->_tblStockItem = $this->_resource->getTableName("cataloginventory/stock_item");
        $this->_tblOrderItem = $this->_resource->getTableName('sales/order_item');

        // query all non-exported orders which have status not like "On Hold", "Invalid", "Canceled", "Return and Refund", and "Complete"
        $query = "SELECT `entity_id` FROM {$this->_tblOrder} WHERE `is_exported` = 0 AND `status` NOT LIKE 'holded' AND `status` NOT LIKE 'invalid' AND `status` NOT LIKE 'canceled' AND `status` NOT LIKE 'closed' AND `status` NOT LIKE 'complete';";
        $this->_orders = $this->_readConnection->fetchAll($query);
        $this->orderIds = array();
        foreach($this->_orders as $order) {
            array_push($this->orderIds, $order['entity_id']);
        }

        // do main process
        Mage::log('---------- START SYNCING INVENTORY ----------', null, 'netsuite_inventory.log');
        $this->schedule();
        Mage::log('---------- FINISH SYNCING INVENTORY ----------', null, 'netsuite_inventory.log');

        // reindex stock data
//        Mage::log('---------- START REINDEX DATA ----------', null, 'netsuite_inventory.log');
//        $process = Mage::getSingleton('index/indexer')->getProcessByCode("cataloginventory_stock");
//        $process->reindexAll();
//        Mage::log('---------- FINISH REINDEX DATA ----------', null, 'netsuite_inventory.log');
    }

    public function schedule()
    {
        /** @var SM_NetsuiteInventorySync_Helper_Data $helper */
        $helper = Mage::helper('netsuiteinventorysync');
        $sftp = $helper->open();

        if ($sftp instanceof Varien_Io_File || $sftp instanceof Varien_Io_Sftp) {
            // get list of data file on server folder
            $dataListFiles = array();
            $list = $sftp->ls();

            if ($sftp instanceof Varien_Io_Sftp) {
                foreach ($list as $item) {
                    if (strpos($item['id'], '.csv') != false) {
                        $dataListFiles[$item['text']] = $item['id'];
                    }
                    unset($item);
                }

                $archiveDir = Mage::getStoreConfig(SM_NetsuiteInventorySync_Helper_Data::XML_PATH_INVENTORY_SYNC_SFTP_PATH) . DS . 'Archive';
            } else {
                $directoryPath = Mage::getStoreConfig(SM_NetsuiteInventorySync_Helper_Data::XML_PATH_INVENTORY_SYNC_INTERNAL_PATH);
                foreach ($list as $item) {
                    if (isset($item['filetype']) && $item['filetype'] == 'csv' && isset($item['text'])) {
                        $dataListFiles[$item['text']] = $directoryPath . DS . $item['text'];
                    }
                    unset($item);
                }

                $archiveDir = Mage::getStoreConfig(SM_NetsuiteInventorySync_Helper_Data::XML_PATH_INVENTORY_SYNC_INTERNAL_PATH) . DS . 'Archive';
            }


            // just read only 1 last data file
            if (count($dataListFiles) > 0) {
                ksort($dataListFiles);
                // get last element
                $file = end($dataListFiles);
                $text = array_search($file, $dataListFiles);
                // read last inventory file to sync
                Mage::log("FILE OPENED: $text", null, 'netsuite_inventory.log');
                $content = $sftp->read($file);
                $data = $this->_strGetCsv($content);
                foreach ($data as $item1) {
                    if (isset($item1[0]) && $item1[0] != "")
                        $this->_updateStockItemQuantity($item1[0], $item1[1]);
                }
                // move all inventory files into Archive folder
                foreach ($dataListFiles as $text => $id)
                    $sftp->mv($id, $archiveDir . '/' . $text);
            }
            // Make a new record for netsuiteinventorysycn_schedule
            /** @var SM_NetsuiteInventorySync_Model_Schedule $schedule */
            $schedule = Mage::getModel('netsuiteinventorysync/schedule');
            $schedule->setData('job_code', 'sm_netsuite_inventory_sync');
            $schedule->setData('finish_at', date('Y-m-d H:i:s'));
            $schedule->save();
            $sftp->close();
        }

        // update kit products
        Mage::log("----- Start Updating Quantity of Kit Products", null, 'netsuite_inventory.log');
        try {
            $this->_updateKitProductQuantity();
        } catch (Exception $e) {
            Mage::log("----- kit-error: " . $e->getMessage(), null, 'netsuite_inventory.log');
            return;
        }
        Mage::log("----- End Updating Quantity of Kit Products", null, 'netsuite_inventory.log');
    }

    /**
     * Get data in import file
     *
     * @param $string
     * @return array
     */
    protected function _strGetCsv($string)
    {
        $data = array();
        $rows = str_getcsv($string, "\n");
        foreach ($rows as $row) {
            $data[] = str_getcsv($row);
        }
        unset($data[0]);
        return $data;
    }

    protected function _updateStockItemQuantity($sku = '', $qty = 0)
    {
        try {
            // get product id and check exists
            $productId = $this->_productResource->getIdBySku($sku);
            if(!$productId) return;

            $overrideInventory = $this->_getAttributeValue($productId, 'override_inventory');
            if($overrideInventory == 1) return;

            // get product qty from stock item
            $stockQuery = "SELECT `item_id`,`qty` FROM {$this->_tblStockItem} WHERE `product_id` = {$productId}";
            $stockItem = $this->_readConnection->fetchAll($stockQuery);
            if(count($stockItem) > 0){
                $stockItem = reset($stockItem);
                $stockItem['qty'] = (int)$stockItem['qty'];
            }else{
                return;
            }

            // calculate reserved item in non-exported orders
            $reservedQty = 0;
            $escapedSku = $this->_readConnection->quote($sku);
            $escapedSku = rtrim($escapedSku, "'");
            $escapedSku = ltrim($escapedSku, "'");

            $orderItemsQuery = "SELECT b.qty_ordered, c.bundle_sku, c.bundle_qty "
                . "FROM sales_flat_order a INNER JOIN sales_flat_order_item b ON "
                . "b.order_id = a.entity_id INNER JOIN sales_flat_order_item_bundles c ON "
                . "c.order_id = b.order_id AND c.product_id = b.product_id WHERE "
                . "a.entity_id IN (".implode(',', $this->orderIds).") AND "
                . "b.product_type = 'simple' AND c.bundle_sku LIKE '%{$escapedSku}%' "
                . "UNION ALL "
                . "SELECT b.qty_ordered, '', '' from sales_flat_order a "
                . "INNER JOIN sales_flat_order_item b ON b.order_id = a.entity_id "
                . "WHERE a.entity_id IN (".implode(',', $this->orderIds).") AND "
                . "b.product_type = 'simple' AND b.sku = '{$escapedSku}'";
            $orderItems = $this->_readConnection->fetchAll($orderItemsQuery);

            foreach ($orderItems as $orderItem) {
                $isBundled = $orderItem['bundle_sku'];
                $orderedQty = (int) $orderItem['qty_ordered'];
                if (!$isBundled) {
                    $reservedQty += $orderedQty;
                } else {
                    $items = explode("#", $isBundled);
                    $qtys = explode("#", $orderItem['bundle_qty']);
                    foreach ($items as $idx => $childBundleItem) {
                        if ($childBundleItem != $sku) continue;
                        $reservedQty += $orderedQty * $qtys[$idx];
                    }
                }
            }

            $totalReserved = $reservedQty;
            if ($qty - $totalReserved <= 0) {
                $newStock = 0;
                $inStock = 0;
            } else {
                $newStock = $qty - $totalReserved;
                $inStock = 1;
            }
            if($newStock != $stockItem['qty']){
                $stockItemSavingQuery = "UPDATE {$this->_tblStockItem} SET `qty` = {$newStock}, `is_in_stock` = {$inStock} WHERE `item_id` = {$stockItem['item_id']}";
                $this->_writeConnection->query($stockItemSavingQuery);
                Mage::log("----- updated {$sku} with qty({$qty}) and changed stock from {$stockItem['qty']} to {$newStock}", null, 'netsuite_inventory.log');
            }else{
                Mage::log("----- non-update {$sku} with qty({$qty}) because old stock({$stockItem['qty']}) = new stock({$newStock})", null, 'netsuite_inventory.log');
            }
        } catch (Exception $e) {
            Mage::log("----- error with " . $sku . " (message: " . $e->getMessage() . ")", null, 'netsuite_inventory.log');
            return;
        }
    }

    protected function _updateKitProductQuantity()
    {
        // check attribute exists
        $attributeChecker = Mage::getModel('catalog/resource_eav_attribute')->loadByCode('catalog_product', 'bundle_enable');
        if(is_null($attributeChecker->getId())) return;

        $attributeChecker1 = Mage::getModel('catalog/resource_eav_attribute')->loadByCode('catalog_product', 'bundle_sku');
        if(is_null($attributeChecker1->getId())) return;

        // get collection of kit products
        $kits = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('bundle_enable', '1')->addAttributeToFilter('bundle_sku',array('neq' => ''));

        // loop collection to update stock
        foreach ($kits as $kit) {
            // get kit product id
            $kitProductId = $kit->getData('entity_id');
            $kitSku = $kit->getSku();
            $overrideInventory = $this->_getAttributeValue($kitProductId, 'override_inventory');
            if($overrideInventory == 1) continue;

            if($kit->getData('bundle_sku') == null || $kit->getData('bundle_sku') == '') continue;
            // get stock item of kit product by id
            $kitStockQuery = "SELECT `item_id`,`qty` FROM {$this->_tblStockItem} WHERE `product_id` = {$kitProductId}";
            $kitStock = $this->_readConnection->fetchAll($kitStockQuery);
            if(count($kitStock) > 0){
                $kitStock = reset($kitStock);
                $kitStock['qty'] = (int)$kitStock['qty'];
            }else{
                continue;
            }

            // calculation stock
            $bundleSku = $this->_getAttributeValue($kitProductId, 'bundle_sku');
            $bundleQty = $this->_getAttributeValue($kitProductId, 'bundle_qty');
            $bundleSkus = explode('#', $bundleSku);
            $bundleQtys = explode('#', $bundleQty);

            $kitBundleQty = array();
            $totalKitQty = 0;
            foreach ($bundleSkus as $key => $sku) {
                // check bundle product exists
                $bundleId = $this->_productResource->getIdBySku($sku);
                if (!$bundleId) continue;

                // get stock item of bundle product by id
                $bundleStockQuery = "SELECT `qty` FROM {$this->_tblStockItem} WHERE `product_id` = {$bundleId}";
                $bundleStock = $this->_readConnection->fetchAll($bundleStockQuery);
                if(count($bundleStock) > 0){
                    $bundleStock = reset($bundleStock);
                    $bundleStock['qty'] = (int)$bundleStock['qty'];
                }else{
                    continue;
                }
                $kitBundleQty[$key] = floor($bundleStock['qty'] / $bundleQtys[$key]);
            }

            if (count($kitBundleQty) > 0) {
                $totalKitQty = min($kitBundleQty);
            }

            if ($totalKitQty <= 0) {
                $totalKitQty = 0;
                $inStock = 0;
            } else {
                $inStock = 1;
            }

            if($totalKitQty != $kitStock['qty']){
                $kitStockSavingQuery = "UPDATE {$this->_tblStockItem} SET `qty` = {$totalKitQty}, `is_in_stock` = {$inStock} WHERE `item_id` = {$kitStock['item_id']}";
                $this->_writeConnection->query($kitStockSavingQuery);
                Mage::log("----- updated {$kitSku} and changed stock from {$kitStock['qty']} to {$totalKitQty}", null, 'netsuite_inventory.log');
            }else{
                Mage::log("----- non-update {$kitSku} because old stock({$kitStock['qty']}) = new stock({$totalKitQty})", null, 'netsuite_inventory.log');
            }
            unset($kitBundleQty);
            unset($kit);
        }
        unset($kits);
    }

    protected function _getAttributeValue($productId, $attributeCode)
    {
        return $this->_productResource->getAttributeRawValue($productId, $attributeCode, Mage_Core_Model_App::ADMIN_STORE_ID);
    }

}