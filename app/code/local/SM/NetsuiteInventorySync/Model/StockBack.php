<?php

class SM_NetsuiteInventorySync_Model_StockBack extends Mage_Core_Model_Abstract
{
    protected $_resource = null;
    protected $_readConnection = null;
    protected $_writeConnection = null;
    protected $_tblStockItem = '';
    protected $_kitProducts = null;
    protected $_attributeChecker = null;
    
    public function __construct()
    {
        ini_set('max_execution_time', 0);
        ini_set('memory_limit', '-1');

        $this->_resource = Mage::getModel('core/resource');
        $this->_readConnection = $this->_resource->getConnection('core_read');
        $this->_writeConnection = $this->_resource->getConnection('core_write');
        $this->_tblStockItem = $this->_resource->getTableName("cataloginventory/stock_item");
        // check attribute exists
        $this->_attributeChecker = Mage::getModel('catalog/resource_eav_attribute')->loadByCode('catalog_product', 'bundle_enable');
    }

    /**
     * Used to return stock back to products in order
     *
     * @param Mage_Sales_Model_Order $order
     */
    public function returnStockBackToProduct(Mage_Sales_Model_Order $order)
    {
        try{
            Mage::log("--- Start processing order #{$order->getData('increment_id')}", null, 'sm_netsuiteInventorySync_returnStock.log');
            foreach ($order->getAllItems() as $item) {
                // get old stock
                $query = "SELECT `item_id`,`qty` FROM {$this->_tblStockItem} WHERE `product_id` = {$item->getProductId()}";
                $result = $this->_readConnection->fetchAll($query);
                if (count($result) > 0) {
                    $result = reset($result);
                    $result['qty'] = (int)$result['qty'];
                } else {
                    continue;
                }

                // calc new stock
                $qtyOrdered = floor($item->getQtyOrdered());
                $newStock = $qtyOrdered + $result['qty'];

                // check that product is in stock
                $isInStock = 0;
                if ($newStock > 0) $isInStock = 1;

                // save new stock
                $query1 = "UPDATE {$this->_tblStockItem} SET `qty` = {$newStock}, `is_in_stock` = {$isInStock} WHERE `item_id` = {$result['item_id']}";
                $this->_writeConnection->query($query1);
                Mage::log("return stock for {$item->getSku()}: current_qty({$result['qty']}) + qty_ordered({$qtyOrdered}) = {$newStock}", null, 'sm_netsuiteInventorySync_returnStock.log');
            }
            Mage::log("--- End process order #{$order->getData('increment_id')}", null, 'sm_netsuiteInventorySync_returnStock.log');
            Mage::log("--------------------------------------------", null, 'sm_netsuiteInventorySync_returnStock.log');
        } catch (Exception $e) {
            Mage::log($e->getMessage(), null, 'sm_netsuiteInventorySync.log');
        }
    }
    
    /**
     * The function is used to sync stock between bundle and single products
     *
     * Note:
     * flag = true   - when getting stock out
     *      = false  - when returning stock back to product
     */
    public function updateStock(Mage_Sales_Model_Order $order, $flag = true, $status = null)
    {
        try {
            /** @var Mage_Catalog_Model_Resource_Product $productResource */
            $productResource = Mage::getResourceModel('catalog/product');

            if ($order instanceof Mage_Sales_Model_Order) {
                Mage::log("--- Start processing order #{$order->getData('increment_id')}", null, 'sm_netsuiteInventorySync_updateStock.log');
                foreach ($order->getAllItems() as $item) {
                    // just apply for simple product
                    if ($item->getProductType() == 'simple') {

                        Mage::log("------ Start processing product {$item->getSku()}", null, 'sm_netsuiteInventorySync_updateStock.log');

                        if($status == 'cancel' || $status == 'refund'){
                            /** @var Mage_Sales_Model_Order_Item $item */
                            // Fix inventory issue when the order is canceled or refund (compared to the timestamp) (WT-573)
                            //$dtz = new DateTimeZone(Mage::app()->getStore()->getConfig('general/locale/timezone'));
                            //$offset = $dtz->getOffset(new DateTime('now', $dtz));
                            //$createdAt = date('Y-m-d H:i:s', strtotime($item->getUpdatedAt() . '+' . round($offset / 3600) . ' hours'));
                            $createdAt = date_format(date_create($item->getCreatedAt()), 'Y-m-d H:i:s');
                            $lastSchedule = Mage::getResourceModel('netsuiteinventorysync/schedule_collection')
                                ->setOrder('schedule_id', 'DESC')
                                ->getFirstItem();
                            $cronTimeFinish = $lastSchedule->getData('finish_at');
                            if($createdAt < $cronTimeFinish){
                                $queryRead = "SELECT `item_id`,`qty` FROM {$this->_tblStockItem} WHERE `product_id` = {$item->getProductId()}";
                                $result = $this->_readConnection->fetchAll($queryRead);
                                if(count($result) > 0){
                                    /*
                                     * If the timestamp of that order is earlier than the timestamp of the last inventory import,
                                     * please do not increment stock for the items in that order.
                                     */
                                    $newQty = $result[0]['qty'] - $item->getQtyOrdered();

                                    $isInStock = 0;
                                    if ($newQty > 0) $isInStock = 1;
                                    // Save new stock
                                    $queryUpdate = "UPDATE {$this->_tblStockItem} SET `qty` = {$newQty}, `is_in_stock` = {$isInStock} WHERE `item_id` = {$result[0]['item_id']}";
                                    $this->_writeConnection->query($queryUpdate);
                                }
                            }
                        }

                        // check product is single or bundle
                        $isBundle = $productResource->getAttributeRawValue($item->getProductId(), 'bundle_enable', Mage_Core_Model_App::ADMIN_STORE_ID);
                        if ($isBundle) {
                            $stockItem = $item->getProduct()->getStockItem();
                            if (!$stockItem->isWholesaleQty($item->getQtyOrdered())) {
                                Mage::log("------ Product {$item->getSku()} is bundle", null, 'sm_netsuiteInventorySync_updateStock.log');
                                // if bundle
                                $singleSku = $productResource->getAttributeRawValue($item->getProductId(), 'bundle_sku', Mage_Core_Model_App::ADMIN_STORE_ID);
                                $singleQty = $productResource->getAttributeRawValue($item->getProductId(), 'bundle_qty', Mage_Core_Model_App::ADMIN_STORE_ID);
                                $arrSingleSku = explode('#', $singleSku);
                                $arrSingleQty = explode('#', $singleQty);
                                if (count($arrSingleSku) > 0) {
                                    foreach ($arrSingleSku as $key => $sku) {
                                        // check product
                                        $proId = $productResource->getIdBySku($sku);
                                        if ($proId) {
                                            // load stock object of each single
                                            $query = "SELECT `item_id`,`qty` FROM {$this->_tblStockItem} WHERE `product_id` = {$proId}";
                                            $result = $this->_readConnection->fetchAll($query);
                                            if (count($result) > 0) {
                                                $result = reset($result);
                                                $result['qty'] = (int)$result['qty'];
                                            } else {
                                                continue;
                                            }

                                            // calculate qty ordered for single
                                            $singleQtyOrdered = $arrSingleQty[$key] * $item->getQtyOrdered();
                                            // calculate new stock
                                            $newSingleStock = 0;
                                            if($flag){
                                                if ($result['qty'] >= $singleQtyOrdered) $newSingleStock = $result['qty'] - $singleQtyOrdered;
                                            }else{
                                                $newSingleStock = $result['qty'] + $singleQtyOrdered;
                                            }
                                            // check that product is in stock
                                            $isInStock = 0;
                                            if ($newSingleStock > 0) $isInStock = 1;
                                            // save new stock
                                            $query1 = "UPDATE {$this->_tblStockItem} SET `qty` = {$newSingleStock}, `is_in_stock` = {$isInStock} WHERE `item_id` = {$result['item_id']}";
                                            $this->_writeConnection->query($query1);
                                            $this->_syncStockFromSingleToBundle($productResource, $sku);
                                            Mage::log("update related single {$sku} from old_qty({$result['qty']}) to new_qty({$newSingleStock})", null, 'sm_netsuiteInventorySync_updateStock.log');
                                        }
                                    }
                                }
                            }
                        } else {
                            Mage::log("------ Product {$item->getSku()} is single", null, 'sm_netsuiteInventorySync_updateStock.log');
                            // if single
                            $this->_syncStockFromSingleToBundle($productResource, $item->getSku());
                        }

                        Mage::log("------ End process product {$item->getSku()}", null, 'sm_netsuiteInventorySync_updateStock.log');
                    }
                }
                Mage::log("--- End process order #{$order->getData('increment_id')}", null, 'sm_netsuiteInventorySync_updateStock.log');
                Mage::log("--------------------------------------------", null, 'sm_netsuiteInventorySync_updateStock.log');
            }

            unset($productResource);
        } catch (Exception $e) {
            Mage::log($e->getMessage(), null, 'sm_netsuiteInventorySync.log');
        }
    }

    /**
     * Used to sync stock from single to bundles product
     * 
     * @param Mage_Catalog_Model_Resource_Product $productResource
     * @param $sku - of product that needs to be synced with related bundles
     */
    protected function _syncStockFromSingleToBundle(Mage_Catalog_Model_Resource_Product $productResource, $sku)
    {
        try{
            if(!is_null($this->_attributeChecker->getId())){

                // get collection of kit products
                $this->_kitProducts = Mage::getModel('catalog/product')->getCollection()
                    ->addAttributeToSelect('entity_id')
                    ->addAttributeToSelect('sku')
                    ->addAttributeToSelect('bundle_sku')
                    ->addAttributeToSelect('bundle_qty')
                    ->addAttributeToFilter('bundle_enable', '1')
                    ->addAttributeToFilter('bundle_sku', array('like' => '%'.$sku.'%'));

                // loop each product to find bundle related
                foreach ($this->_kitProducts as $product) {

                    $singleSku = $product->getData('bundle_sku');
                    $arrSingleSku = explode('#', $singleSku);
                    // check product in bundle related
                    if (in_array($sku, $arrSingleSku)) {

                        // get stock item of kit product by id
                        $query = "SELECT `item_id`,`qty` FROM {$this->_tblStockItem} WHERE `product_id` = {$product->getData('entity_id')}";
                        $result = $this->_readConnection->fetchAll($query);
                        if (count($result) > 0) {
                            $result = reset($result);
                            $result['qty'] = (int)$result['qty'];
                        } else {
                            continue;
                        }

                        // calculation stock
                        $singleQty = $product->getData('bundle_qty');
                        $arrSingleQty = explode('#', $singleQty);

                        $kitBundleQty = array();
                        $totalKitQty = 0;
                        foreach ($arrSingleSku as $key => $skuSingle) {
                            // check bundle product exists
                            $singleRelatedId = $productResource->getIdBySku($skuSingle);
                            if (!$singleRelatedId) continue;

                            // get stock item of bundle product by id
                            $query1 = "SELECT `qty` FROM {$this->_tblStockItem} WHERE `product_id` = {$singleRelatedId}";
                            $result1 = $this->_readConnection->fetchAll($query1);
                            if (count($result1) > 0) {
                                $result1 = reset($result1);
                                $result1['qty'] = (int)$result1['qty'];
                            } else {
                                continue;
                            }
                            $kitBundleQty[$key] = floor($result1['qty'] / $arrSingleQty[$key]);
                        }

                        if (count($kitBundleQty) > 0) {
                            $totalKitQty = min($kitBundleQty);
                        }

                        if ($totalKitQty <= 0) {
                            $totalKitQty = 0;
                            $inStock = 0;
                        } else {
                            $inStock = 1;
                        }

                        if ($totalKitQty != $result['qty']) {
                            $query2 = "UPDATE {$this->_tblStockItem} SET `qty` = {$totalKitQty}, `is_in_stock` = {$inStock} WHERE `item_id` = {$result['item_id']}";
                            $this->_writeConnection->query($query2);
                            Mage::log("update related bundle {$product->getData('sku')} from old_qty({$result['qty']}) to new_qty({$totalKitQty})", null, 'sm_netsuiteInventorySync_updateStock.log');
                        }else{
                            Mage::log("found related bundle {$product->getData('sku')}, old_qty({$result['qty']}) = new_qty({$totalKitQty})", null, 'sm_netsuiteInventorySync_updateStock.log');
                        }
                    }
                }

                $this->_kitProducts->clear();
            }
        }catch (Exception $e){
            Mage::log($e->getMessage(), null, 'sm_netsuiteInventorySync.log');
        }
    }


    /**
     * Check weather current product added has available stock, and check from other related bundle items
     * before add it into cart
     *
     * @param Mage_Catalog_Model_Product    $product_added
     * @param int                           $qty_ordered
     * @param string                        $action
     * @return bool|string
     */
    public function checkAvailableStock($product_added, $qty_ordered, $action = 'add')
    {
        /** @var Mage_Checkout_Model_Session $session */
        $session = Mage::getSingleton('checkout/session');
        $quote = $session->getQuote();
        $quoteItems = $quote->getAllVisibleItems();

        /** @var Mage_Catalog_Model_Resource_Product $productResource */
        $productResource = Mage::getResourceModel('catalog/product');

        /** @var Mage_CatalogInventory_Model_Stock_Item $inventoryObj */
        $inventoryObj = Mage::getModel('cataloginventory/stock_item');

        $itemsInArray = array();
        foreach($quoteItems as $item){
            /** @var Mage_Sales_Model_Quote_Item $item */
            $itemsInArray[$item->getProductId()] = array(
                'quote_id'      => $item->getItemId(),
                'sku'           => $item->getSku(),
                'qty'           => $item->getQty(),
                'bundle_enable' => $this->_getAttributeValue($productResource, $item->getProductId(), 'bundle_enable'),
                'bundle_sku'    => $this->_getAttributeValue($productResource, $item->getProductId(), 'bundle_sku'),
                'bundle_qty'    => $this->_getAttributeValue($productResource, $item->getProductId(), 'bundle_qty'),
            );
        }

        //Zend_Debug::dump($itemsInArray);die;
        //return $itemsInArray;

        if($inventoryObj->loadByProduct($product_added)->getManageStock() && $product_added->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_SIMPLE){
            if($this->_getAttributeValue($productResource, $product_added->getId(), 'bundle_enable')){
                // if bundle
                $bundle_skus = explode('#', $this->_getAttributeValue($productResource, $product_added->getId(), 'bundle_sku'));
                $bundle_qtys = explode('#', $this->_getAttributeValue($productResource, $product_added->getId(), 'bundle_qty'));

                // check other single items first
                foreach($itemsInArray as $pid => $item){

                    if($pid != $product_added->getId() && // except current product
                        !$item['bundle_enable'] && // if not be bundle product
                        in_array($item['sku'], $bundle_skus)){

                        $totalSingleInBundle = $bundle_qtys[array_search($item['sku'],$bundle_skus)];
                        $totalSingleQtyOrdered = $totalSingleInBundle * (int)$qty_ordered;
                        if(array_key_exists($product_added->getId(), $itemsInArray) && $action == 'add'){
                            $totalSingleQtyOrdered += $totalSingleInBundle * $itemsInArray[$product_added->getId()]['qty'];
                        }

                        // totals of single products added into cart
                        $totalSingleInCart = $totalSingleQtyOrdered + $item['qty'];

                        // get current stock of single
                        $availableStock = (int)$inventoryObj->loadByProduct(Mage::getModel('catalog/product')->load($pid))->getQty();

                        //return $totalSingleInCart.' --- '.$availableStock;

                        if($totalSingleInCart > $availableStock){
                            if(array_key_exists($product_added->getId(), $itemsInArray)){
                                try{
                                    $quote->removeItem($itemsInArray[$product_added->getId()]['quote_id']);
                                    $quote->save();
                                }catch(Exception $e){
                                    continue;
                                }
                            }
                            return 'Product name "'. $product_added->getName() .'" doesn\'t have enough stock for your cart.';
                        }
                    }
                }

                // check other bundle items
                foreach($itemsInArray as $pid => $item){

                    if($pid != $product_added->getId() && // except current product
                        $item['bundle_enable']){

                        foreach(explode('#', $item['bundle_sku']) as $tempSku){
                            if(in_array($tempSku, $bundle_skus)){

                                $totalSingleInBundle = $bundle_qtys[array_search($item['sku'],$bundle_skus)];
                                $totalSingleQtyOrdered = $totalSingleInBundle * (int)$qty_ordered;
                                if(array_key_exists($product_added->getId(), $itemsInArray) && $action == 'add'){
                                    $totalSingleQtyOrdered += $totalSingleInBundle * $itemsInArray[$product_added->getId()]['qty'];
                                }
                                // found any other bundle item that relates to this single
                                // check current single item in cart, if yes, increase totals in cart
                                foreach($itemsInArray as $pid2 => $item2){
                                    if(!in_array($pid2, array($pid, $product_added->getId())) && $item2['bundle_enable'] && in_array($tempSku, explode('#', $item2['bundle_sku']))){
                                        $totalSingleQtyOrdered += explode('#', $item2['bundle_qty'])[array_search($tempSku, explode('#', $item2['bundle_sku']))] * $item2['qty'];
                                    }elseif(!in_array($pid2, array($pid, $product_added->getId())) && !$item2['bundle_enable'] && $tempSku == $item2['sku']){
                                        $totalSingleQtyOrdered += $item2['qty'];
                                    }
                                }

                                // totals of single products added into cart
                                $totalSingleInCart = $totalSingleQtyOrdered + (explode('#', $item['bundle_qty'])[array_search($tempSku, explode('#', $item['bundle_sku']))] * $item['qty']);

                                // get current stock of single
                                $tempId = $productResource->getIdBySku($tempSku);
                                $availableStock = (int)$inventoryObj->loadByProduct(Mage::getModel('catalog/product')->load($tempId))->getQty();

                                //return $totalSingleInCart.' --- '.$availableStock;

                                if($totalSingleInCart > $availableStock){
                                    if(array_key_exists($product_added->getId(), $itemsInArray)){
                                        try{
                                            $quote->removeItem($itemsInArray[$product_added->getId()]['quote_id']);
                                            $quote->save();
                                        }catch(Exception $e){
                                            continue;
                                        }
                                    }
                                    return 'Product name "'. $product_added->getName() .'" doesn\'t have enough stock for your cart.';
                                }
                            }
                        }
                    }
                }

            }else{
                // if single

                // totals of single products added into cart
                $totalSingleInCart = 0;
                // get current stock of single
                $availableStock = (int)$inventoryObj->loadByProduct($product_added)->getQty();

                // check all quote items if has bundle item is product added
                foreach($itemsInArray as $pid => $item){

                    if($pid != $product_added->getId() && // except current product
                        $item['bundle_enable'] && // if bundle enabled
                        in_array($product_added->getSku(), explode('#',$item['bundle_sku']))){

                        $totalSingleInBundle = explode('#',$item['bundle_qty'])[array_search($product_added->getSku(), explode('#',$item['bundle_sku']))];
                        $totalSingleQtyOrdered = $totalSingleInBundle * $item['qty'];
                        if(array_key_exists($product_added->getId(), $itemsInArray) && $action == 'add'){
                            $totalSingleQtyOrdered += $itemsInArray[$product_added->getId()]['qty'];
                        }

                        // increase total qty of single item in cart
                        $totalSingleInCart += $totalSingleQtyOrdered;
                    }
                }

                $totalSingleInCart += (int)$qty_ordered;

                //return $totalSingleInCart.' --- '.$availableStock;

                if($totalSingleInCart > $availableStock){
                    if(array_key_exists($product_added->getId(), $itemsInArray)){
                        try{
                            $quote->removeItem($itemsInArray[$product_added->getId()]['quote_id']);
                            $quote->save();
                        }catch(Exception $e){}
                    }
                    return 'Product name "'. $product_added->getName() .'" doesn\'t have enough stock for your cart.';
                }
            }
        }

        return true;
    }

    protected function _getAttributeValue($resource, $productId, $attributeCode)
    {
        /** @var Mage_Catalog_Model_Resource_Product $resource */
        return $resource->getAttributeRawValue($productId, $attributeCode, Mage_Core_Model_App::ADMIN_STORE_ID);
    }
}
