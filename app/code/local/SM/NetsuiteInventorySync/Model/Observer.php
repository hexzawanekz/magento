<?php

class SM_NetsuiteInventorySync_Model_Observer extends Mage_Core_Model_Abstract
{
    /**
     * The function is used to sync stock between bundle and single products
     *
     * @param Varien_Event_Observer $observer
     */
    public function syncStockWhenCancelOrder(Varien_Event_Observer $observer)
    {
        try {
            $order = $observer->getEvent()->getOrder();
            if ($order instanceof Mage_Sales_Model_Order) {
                // return stock back to product when order is canceled
                Mage::getSingleton('netsuiteinventorysync/stockBack')->updateStock($order, false, 'cancel');
            }
        } catch (Exception $e) {
            Mage::log($e->getMessage(), null, 'sm_netsuiteInventorySync.log');
        }
    }

    /**
     * The function is used to sync stock between bundle and single products
     *
     * @param Varien_Event_Observer $observer
     */
    public function syncStockWhenRefundOrder(Varien_Event_Observer $observer)
    {
        try {
            $params = $observer->getEvent()->getControllerAction()->getRequest()->getParams();
            if(is_array($params) && array_key_exists('order_id', $params)){
                $order = Mage::getModel('sales/order')->load($params['order_id']);
                if ($order instanceof Mage_Sales_Model_Order) {
                    // return stock back to product when order is returned
                    Mage::getSingleton('netsuiteinventorysync/stockBack')->updateStock($order, false, 'refund');
                }
            }
        } catch (Exception $e) {
            Mage::log($e->getMessage(), null, 'sm_netsuiteInventorySync.log');
        }
    }

    /**
     * The function is used to sync stock between bundle and single products
     *
     * @param Varien_Event_Observer $observer
     */
    public function syncStockWhenPlaceOrder(Varien_Event_Observer $observer)
    {
        try {

            /** @var Mage_Sales_Model_Order $order */
            $order = $observer->getEvent()->getOrder();
            if ($order instanceof Mage_Sales_Model_Order) {
                // sync stock with related products when order is placed
                Mage::getSingleton('netsuiteinventorysync/stockBack')->updateStock($order);
            }
        } catch (Exception $e) {
            Mage::log($e->getMessage(), null, 'sm_netsuiteInventorySync.log');
        }
    }

    /**
     * The function is used to sync stock between bundle and single products
     *
     * @param Varien_Event_Observer $observer
     */
    public function syncStockWhenReorderInAdmin(Varien_Event_Observer $observer)
    {
        try {
            $id = 0;
            $data = $observer->getEvent()->getControllerAction()->getResponse();
            if($data instanceof Mage_Core_Controller_Response_Http){
                $headers = $data->getHeaders();
                foreach($headers as $header){
                    if(in_array('Location', $header)){
                        $url = $header['value'];
                        $url = str_replace('http://', '', $url);
                        $url = str_replace('https://', '', $url);
                        $arrUrl = explode('/', $url);
                        // find new order ID in link
                        $tempVal = '';
                        foreach($arrUrl as $value){
                            if($tempVal == 'order_id'){
                                if(is_numeric($value)) $id = $value;
                                break;
                            }else{
                                $tempVal = $value;
                            }
                        }
                        break;
                    }
                }
            }

            if($id != 0){
                $order = Mage::getModel('sales/order')->load($id);
                if ($order instanceof Mage_Sales_Model_Order) {
                    // sync stock with related products when reordering
                    Mage::getSingleton('netsuiteinventorysync/stockBack')->updateStock($order);
                }
            }
        } catch (Exception $e) {
            Mage::log($e->getMessage(), null, 'sm_netsuiteInventorySync.log');
        }
    }
}