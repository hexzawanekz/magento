<?php

class SM_NetsuiteInventorySync_Model_Resource_Schedule_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    public function _construct()
    {
        $this->_init('netsuiteinventorysync/schedule');
    }
}