<?php

class SM_NetsuiteInventorySync_Model_Resource_Schedule extends Mage_Core_Model_Resource_Db_Abstract
{
    public function _construct()
    {
        $this->_init('netsuiteinventorysync/schedule', 'schedule_id');
    }
}