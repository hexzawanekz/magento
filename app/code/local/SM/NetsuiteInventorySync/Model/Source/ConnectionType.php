<?php

class SM_NetsuiteInventorySync_Model_Source_ConnectionType
{
    public function toOptionArray()
    {
        return array(
            array('value' => 0, 'label' => 'Internal directory path'),
            array('value' => 1, 'label' => 'via SFTP folder'),
        );
    }
}