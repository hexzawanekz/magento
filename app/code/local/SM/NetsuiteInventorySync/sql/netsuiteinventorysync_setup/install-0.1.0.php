<?php

$installer = $this;
$setup = new Mage_Eav_Model_Entity_Setup('core_setup');
$installer->startSetup();

$setup->addAttribute('catalog_product', 'override_inventory', array(
    'group' => 'General',
    'label' => 'Override inventory import from Netsuite',
    'note' => '',
    'type' => 'int',    //backend_type
    'input' => 'select',    //frontend_input
    'frontend_class' => '',
    'source' => 'eav/entity_attribute_source_boolean',
    'backend' => '',
    'frontend' => '',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_WEBSITE,
    'required' => false,
    'visible_on_front' => false,
    'is_configurable' => false,
    'used_in_product_listing' => false,
    'sort_order' => 3,
    'default' => '0'
));

$installer->endSetup();