<?php

class SM_NetsuiteInventorySync_Helper_Data extends Mage_Core_Helper_Abstract
{
    const XML_PATH_INVENTORY_SYNC_ACTIVE        = 'netsuiteinventory/general/active';

    const XML_PATH_INVENTORY_SYNC_CONNECT_TYPE  = 'netsuiteinventory/general/type';

    const XML_PATH_INVENTORY_SYNC_INTERNAL_PATH = 'netsuiteinventory/general/internal_path';

    const XML_PATH_INVENTORY_SYNC_SFTP_HOST     = 'netsuiteinventory/general/host';
    const XML_PATH_INVENTORY_SYNC_SFTP_USERNAME = 'netsuiteinventory/general/user';
    const XML_PATH_INVENTORY_SYNC_SFTP_PASSWORD = 'netsuiteinventory/general/password';
    const XML_PATH_INVENTORY_SYNC_SFTP_PATH     = 'netsuiteinventory/general/path';

    /**
     * Open connection to directory folder
     *
     * @return bool|Varien_Io_File|Varien_Io_Sftp
     * @throws Exception
     */
    public function open()
    {
        try{
            // check if module is enabled
            if(Mage::getStoreConfig(self::XML_PATH_INVENTORY_SYNC_ACTIVE)){

                // check if connect type is via SFTP folder
                if(Mage::getStoreConfig(self::XML_PATH_INVENTORY_SYNC_CONNECT_TYPE)){
                    // get info to access
                    $host       = Mage::getStoreConfig(self::XML_PATH_INVENTORY_SYNC_SFTP_HOST);
                    $username   = Mage::getStoreConfig(self::XML_PATH_INVENTORY_SYNC_SFTP_USERNAME);
                    $password   = Mage::getStoreConfig(self::XML_PATH_INVENTORY_SYNC_SFTP_PASSWORD);
                    $path       = Mage::getStoreConfig(self::XML_PATH_INVENTORY_SYNC_SFTP_PATH);

                    if($host && $username && $password && $path){
                        // open accessing
                        $sftp = new Varien_Io_Sftp();
                        $sftp->open(
                            array(
                                'host'      => $host,
                                'username'  => $username,
                                'password'  => $password,
                            )
                        );

                        // create Archive path
                        $cd = $path;
                        $archiveDir = $path . DS . 'Archive';

                        // check dir exists
                        if (!is_dir($archiveDir)){
                            mkdir($archiveDir, 0777, true);
                        }

                        // go to folder
                        if (!empty($cd)){
                            $sftp->cd($cd);
                        }

                        return $sftp;
                    }
                }else{
                    // check if connect type is internal folder

                    if($directoryPath = Mage::getStoreConfig(self::XML_PATH_INVENTORY_SYNC_INTERNAL_PATH)){
                        $file = new Varien_Io_File();
                        if($file->open(array('path' => $directoryPath))){
                            $archiveDir = $directoryPath . DS . 'Archive';

                            // check dir exists
                            if (!is_dir($archiveDir)){
                                mkdir($archiveDir, 0777, true);
                            }

                            return $file;
                        }
                    }
                }
            }
            return false;
        }catch(Exception $e){
            Mage::log($e, null, 'netsuite_inventory_connect.log');
            throw $e;
        }
    }
}
