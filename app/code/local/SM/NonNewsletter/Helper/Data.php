<?php
class SM_NonNewsletter_Helper_Data extends Mage_Core_Helper_Abstract{

    const XML_PATH_NEWSLETTER_MAILS  = 'sm_nonnewsletter/newsletter_subscription_transactional_mails/enabled';
    const XML_PATH_NEWUSER_MAILS = 'sm_nonnewsletter/welcome_newuser_transactional_mails/mail_enabled';

    public function getNewsletterSubscriptionMailEnabled()
    {
        return Mage::getStoreConfig(self::XML_PATH_NEWSLETTER_MAILS);
    }

    public function getNewuserMailEnabled()
    {
        return Mage::getStoreConfig(self::XML_PATH_NEWUSER_MAILS);
    }
}