<?php
/**
 * Created by PhpStorm.
 * User: ducnt
 * Date: 8/28/15
 * Time: 3:04 PM
 */ 
class SM_Newsletter_Helper_Data extends Mage_Core_Helper_Abstract {


    public function getProductBySkuString($sku){
        $skus  = explode(',',$sku);
        $collection = Mage::getResourceModel('catalog/product_collection')
            ->addAttributeToSelect('*')
            ->addAttributeToFilter(
                'sku', array('in' => $skus)
            );
        $collection->load();

        return $collection;
    }
}