<?php

class SM_Emarsys_Model_Observer extends Mage_Core_Model_Abstract
{

    protected $_categoryPaths = array();
    protected $_categoryPathsExtend = array();
    protected $_categories = array();

    public function customerRegisterSuccess(Varien_Event_Observer $observer)
    {
        $customer = $observer->getEvent()->getCustomer();
        $params = Mage::app()->getRequest()->getParams();
        $str = "";
        //get value of checkbox in the form

        if ($params['inp_10829']) {
            $str .= '&inp_10829=1';
        }
        if ($params['inp_10830']) {
            $str .= '&inp_10830=1';
        }
        if ($params['inp_10831']) {
            $str .= '&inp_10831=1';
        }
        if ($params['inp_10832']) {
            $str .= '&inp_10832=1';
        }
        if ($params['inp_24286']) {
            $str .= '&inp_24286=1';
        }
        if ($params['inp_24287']) {
            $str .= '&inp_24287=1';
        }
        if ($params['inp_24288']) {
            $str .= '&inp_24288=1';
        }
        if ($params['inp_24289']) {
            $str .= '&inp_24289=1';
        }
        if ($params['inp_24286'] || $params['inp_24287'] || $params['inp_24288'] || $params['inp_24289']) {
            $str .= '&inp_10828=1';
        }
        if ($params['inp_39793']) {
            $str .= '&inp_39793=1';
        }
        if ($params['inp_39794']) {
            $str .= '&inp_39794=1';
        }

        $customerId = $customer->getId();
        $email = $customer->getEmail();
        $fName = $customer->getFirstname();
        $lName = $customer->getLastname();
        $genDer = Mage::getResourceSingleton('customer/customer')->getAttribute('gender')->getSource()->getOptionText($customer->getGender());
        $dob = date('Y-m-d', strtotime($customer->getDob()));
        $birthMonth = date('F', strtotime($customer->getDob()));
        $link = Mage::helper('emarsys')->getLink();

        $fields = Mage::helper('emarsys')->getField();
        $lastElement = end($fields);
        $vals = "";

        $storeCode = Mage::app()->getStore()->getCode();
        if($storeCode == 'eng' || $storeCode == 'tha'){
            $shop = "pet";
        }else{
            $shop = "moxy";
        }
        foreach ($fields as $key => $value) {
            if ($value != $lastElement) {
                if($value['fieldname'] == "f" && $shop == "pet") {
                    $vals .= $value['fieldname'] . '=' . 4498 . '&';
                }else{
                    $vals .= $value['fieldname'] . '=' . $value['fieldvalue'] . '&';
                }
            } else {
                if($value['fieldname'] == "f" && $shop == "pet") {
                    $vals .= $value['fieldname'] . '=' . 4498;
                }else{
                    $vals .= $value['fieldname'] . '=' . $value['fieldvalue'];
                }            }
        }

        $emarsysUrl = $link . '?' . $vals . '&inp_1=' . $fName . '&inp_2=' . $lName . '&inp_3=' . $email . '&inp_5=' . $genDer . '&inp_4=' . $dob . '&inp_22338=' . $birthMonth . '&inp_23821=' . $customerId . $str;
        $result = file_get_contents($emarsysUrl);

        //Mage::log('URL - '.$emarsysUrl, null, 'Customer_Save_Success.log');
        //Mage::log('Result - '.$result, null, 'Customer_Save_Success.log');
        //Mage::log($customer->getData(), null, 'Customer_Save_Success.log');
    }

    public function checkCustomer(Varien_Event_Observer $observer)
    {
        $customer = $observer->getEvent()->getCustomer();
        if (!$customer->getId() && Mage::app()->getRequest()->getActionName() == 'saveOrder') {
            Mage::register('new_customer', true);
        }
    }

    public function customerCheckoutRegister(Varien_Event_Observer $observer)
    {
        if (Mage::registry('new_customer') && Mage::app()->getRequest()->getActionName() == 'saveOrder') {
            $customer = $observer->getEvent()->getCustomer();
            if ($customer->getId()) {
                $customerId = $customer->getId();
                $email = $customer->getEmail();
                $fName = $customer->getFirstname();
                $lName = $customer->getLastname();
                $genDer = Mage::getResourceSingleton('customer/customer')->getAttribute('gender')->getSource()->getOptionText($customer->getGender());
                $dob = date('Y-m-d', strtotime($customer->getDob()));
                $birthMonth = date('F', strtotime($customer->getDob()));
                $link = Mage::helper('emarsys')->getLink();

                $fields = Mage::helper('emarsys')->getField();
                $lastElement = end($fields);
                $vals = "";
                foreach ($fields as $key => $value) {
                    if ($value != $lastElement) {
                        $vals .= $value['fieldname'] . '=' . $value['fieldvalue'] . '&';
                    } else {
                        $vals .= $value['fieldname'] . '=' . $value['fieldvalue'];
                    }
                }
                $emarsysUrl = $link . '?' . $vals . '&inp_1=' . $fName . '&inp_2=' . $lName . '&inp_3=' . $email . '&inp_5=' . $genDer . '&inp_4=' . $dob . '&inp_22338=' . $birthMonth . '&inp_23821=' . $customerId;
                $result = file_get_contents($emarsysUrl);
            }
            Mage::unregister('new_customer');
        }
    }

    protected function _drawCategoryPaths()
    {
        /** @var Mage_Catalog_Model_Category $categories */
        $categories = Mage::getModel('catalog/category')->getCollection()
            ->addAttributeToSelect(array('entity_id', 'path'))
            ->load();

        /** @var Mage_Catalog_Model_Category $_category */
        foreach ($categories as $_category) {
            //Split Category Id
            $categoryIds = explode('/', $_category->getPath());
            array_shift($categoryIds);
            $rootCategoryId = array_shift($categoryIds);

            //Get default store id of current category
            $groupId = Mage::getModel('core/store_group')
                ->getCollection()
                ->addFieldToFilter('root_category_id', $rootCategoryId)
                ->getFirstItem()->getId();

            $stores = Mage::getModel('core/store')
                ->getCollection()
                ->addFieldToFilter('group_id', $groupId);

            $storeId = 0;
            foreach ($stores as $store) {
                if ($storeId == 0) {
                    $storeId = $store->getStoreId();
                } else {
                    $storeIdEn = $store->getStoreId();
                }
            }

            //Load current category with default store id
            $categoryId = $_category->getId();
            $this->_categoryPaths[$categoryId] = '';

            if (count($categoryIds) >= 1) {
                $split = '';
                foreach ($categoryIds as $_categoryId) {
                    $curCategoryName = Mage::getModel('catalog/category')->setStoreId($storeId)->load($_categoryId)->getName();
                    $this->_categoryPaths[$categoryId] = $this->_categoryPaths[$categoryId] . $split . $curCategoryName;
                    $split = ' > ';
                }

                $split = '';
                foreach ($categoryIds as $_categoryId) {
                    $curCategoryName = Mage::getModel('catalog/category')->setStoreId($storeIdEn)->load($_categoryId)->getName();
                    $this->_categoryPathsExtend[$categoryId] = $this->_categoryPathsExtend[$categoryId] . $split . $curCategoryName;
                    $split = ' > ';
                }
            }
        }
    }

    protected function _getIgnoreCategoryList()
    {
        //remove WelcomeSplash out.
        $output = array();

        /** @var Mage_Catalog_Model_Resource_Category_Collection $categoryCollection */
        $categoryCollection = Mage::getModel('catalog/category')->getCollection();
        $categoryList = $categoryCollection->addAttributeToFilter('name', 'Welcome Splash');

        foreach ($categoryList as $_category) {
            $output[] = $_category->getId();

            $childCategory = Mage::getModel('catalog/category')->load($_category->getId())->getChildrenCategories();

            foreach ($childCategory as $_childCategory) {
                $output[] = $_childCategory->getId();
            }
        }

        return $output;
    }

    /**
     * Configured to run in server's crontabs, instead of Magento's cron
     */
    public function emarsysExportCsv()
    {
        ini_set('memory_limit', -1);
        ini_set('max_execution_time', 0);

        //Folder to store Data.
        $folder = 'media' . DS . 'EmarsysExportedFile' . DS;
        if (!is_dir($folder)) {
            mkdir($folder, 0777, true);
        }

        $this->_drawCategoryPaths();

        /** @var Mage_Core_Model_Store_Group $_group */
        $_group = Mage::app()->getStore('moxyth')->getGroup();
        $rootCategoryId = $_group->getRootCategoryId();
        $storeId = $_group->getDefaultStoreId();
        // set current store as front
        Mage::app()->setCurrentStore(Mage::app()->getStore($storeId)->getCode());

        $groupStoreName = 'Orami';//$_group->getName();
        $mediaUrl = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);
        if(strpos($mediaUrl,'https') === false){
            $mediaUrl = str_replace('http','https',$mediaUrl);
        }
        //Get path of root category
        $rootpath = Mage::getModel('catalog/category')
            ->setStoreId($storeId)
            ->load($rootCategoryId)
            ->getPath();

        //Get all child categories of current root category
        $categories = Mage::getModel('catalog/category')->setStoreId($storeId)
            ->getCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('path', array("like" => $rootpath . "/" . "%"));

        //Reset result array
        $result = array();
        /** @var Mage_CatalogInventory_Model_Stock_Item $stockObj */
        $stockObj = Mage::getModel('cataloginventory/stock_item');
        foreach ($categories as $_category) {
            /** @var Mage_Catalog_Model_Category $_category */
            /** @var Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Collection $products */
            $products = Mage::getModel('catalog/product')
                ->setStoreId($storeId)
                ->getCollection()
                ->joinField('category_id', 'catalog/category_product', 'category_id', 'product_id = entity_id', null, 'left')
                ->addAttributeToSelect(array('name', 'id', 'sku', 'producturl', 'image', 'price', 'msrp', 'status', 'cost'))
                ->addAttributeToFilter('category_id', array('in' => array($_category->getId())));


            /** @var Mage_Catalog_Model_Product $_product */
            foreach ($products as $_product) {
                $productSku = $_product->getSku();
                $productUrl = $_product->getProductUrl($_category);
                $productTitle = str_replace('"', '""', $_product->getName());
                if ($_product->getImage() == 'no_selection') {
                    $productImg = $mediaUrl . 'catalog/product/placeholder/' . Mage::getStoreConfig("catalog/placeholder/thumbnail_placeholder");
                } else {
                    $productImg = $mediaUrl . 'catalog/product' . $_product->getImage();
                }
                $categoryPath = $this->_categoryPaths[$_category->getId()] . '|' . $this->_categoryPathsExtend[$_category->getId()];

                //check if product is available or not.
                if ($_product->getPrice() > 0) {
                    if ($_product->getStatus() == Mage_Catalog_Model_Product_Status::STATUS_ENABLED) {
                        $stockObj->loadByProduct($_product);
                        if ($stockObj->getManageStock() == 1) {
                            if ($stockObj->getIsInStock() == 0) {
                                $productAvailable = "False";
                            } else {
                                if ($stockObj->getData('qty') > 5) {
                                    $productAvailable = "True";
                                } else {
                                    $productAvailable = "False";
                                }
                            }
                        } else {
                            $productAvailable = "True";
                        }
                    } else {
                        $productAvailable = "False";
                    }
                } else {
                    $productAvailable = "False";
                }


                $productPrice = $_product->getPrice();
                $productMsrp = $_product->getMsrp();
                $margin = $_product->getPrice() - $_product->getCost();

                if (!isset($result[$_product->getId()])) {
                    if ($productSku != '' && $productTitle != '' && $productPrice != '')
                        $result[$_product->getId()] = array($productSku, $productUrl, $productTitle, $productImg, $categoryPath, $productAvailable, $productPrice, $productMsrp, $margin);
                } else {
                    $result[$_product->getId()][4] = $result[$_product->getId()][4] . '|' . $categoryPath;
                }
            }
        }

        if (count($result) > 0) {
            // Keep an backup file
            if (file_exists($folder . $groupStoreName . '.csv')) {
                if (file_exists($folder . 'bak_' . $groupStoreName . '.csv')) {
                    unlink($folder . 'bak_' . $groupStoreName . '.csv');
                }
                rename($folder . $groupStoreName . '.csv', $folder . 'bak_' . $groupStoreName . '.csv');
            }

            // Open new file to write data
            $fp = fopen($folder . $groupStoreName . '.csv', 'w+');

            // Write header of CSV
            fwrite($fp, "item,link,title,image,category,available,price,msrp,c_margin\n");

            foreach ($result as $_result) {
                $_result = sprintf(
                    "\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\"\n",
                    $_result[0], $_result[1], $_result[2], $_result[3], $_result[4], $_result[5], $_result[6], $_result[7], $_result[8]
                );
                fwrite($fp, $_result);
            }
            fclose($fp);
        }
    }
}
?>
