<?php

/** @var $this Mage_Core_Model_Resource_Setup */

$this->startSetup();

/** @var Mage_Core_Model_Config $modelConfig */
$modelConfig = Mage::getModel('core/config');
/** @var Mage_Core_Model_Store $modelStore */
$modelStore = Mage::getModel('core/store');

$petloftEnId = $modelStore->load('en')->getId();

//Default Config
$modelConfig->saveConfig('emarsys/emarsys_configuration/owner_id', '410525792');
$modelConfig->saveConfig('emarsys/emarsys_configuration/f_value', '611');

//Petloft
$storeId = $modelStore->load('en')->getId();
$modelConfig->saveConfig('emarsys/emarsys_configuration/owner_id', '410525792', 'stores', $storeId);
$modelConfig->saveConfig('emarsys/emarsys_configuration/f_value', '611', 'stores', $storeId);

$storeId = $modelStore->load('th')->getId();
$modelConfig->saveConfig('emarsys/emarsys_configuration/owner_id', '410525792', 'stores', $storeId);
$modelConfig->saveConfig('emarsys/emarsys_configuration/f_value', '611', 'stores', $storeId);

//Lafema
$storeId = $modelStore->load('len')->getId();
$modelConfig->saveConfig('emarsys/emarsys_configuration/owner_id', '453209528', 'stores', $storeId);
$modelConfig->saveConfig('emarsys/emarsys_configuration/f_value', '609', 'stores', $storeId);

$storeId = $modelStore->load('lth')->getId();
$modelConfig->saveConfig('emarsys/emarsys_configuration/owner_id', '453209528', 'stores', $storeId);
$modelConfig->saveConfig('emarsys/emarsys_configuration/f_value', '609', 'stores', $storeId);


//Sanoga
$storeId = $modelStore->load('sen')->getId();
$modelConfig->saveConfig('emarsys/emarsys_configuration/owner_id', '437681378', 'stores', $storeId);
$modelConfig->saveConfig('emarsys/emarsys_configuration/f_value', '610', 'stores', $storeId);

$storeId = $modelStore->load('sth')->getId();
$modelConfig->saveConfig('emarsys/emarsys_configuration/owner_id', '437681378', 'stores', $storeId);
$modelConfig->saveConfig('emarsys/emarsys_configuration/f_value', '610', 'stores', $storeId);

//Venbi
$storeId = $modelStore->load('ven')->getId();
$modelConfig->saveConfig('emarsys/emarsys_configuration/owner_id', '454755839', 'stores', $storeId);
$modelConfig->saveConfig('emarsys/emarsys_configuration/f_value', '608', 'stores', $storeId);

$storeId = $modelStore->load('vth')->getId();
$modelConfig->saveConfig('emarsys/emarsys_configuration/owner_id', '454755839', 'stores', $storeId);
$modelConfig->saveConfig('emarsys/emarsys_configuration/f_value', '608', 'stores', $storeId);




$this->endSetup();
?>