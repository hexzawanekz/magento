<?php


class SM_Emarsys_Block_Product_Bought extends Mage_Catalog_Block_Product_Abstract {

    public function getFlatRecommended(){

        $product = $this->getProduct();
        if($product->getTypeId()=='simple'){
            $collection =  Mage::getModel('sm_recommendationanalysis/productsflat')
                ->getCollection()
                ->addFieldToSelect('*')
                ->addFieldToFilter('baseProduct', $product->getId())
                ->setOrder('weighted_metric', 'DESC');
            $productIds = array();
            foreach($collection as $item){
                $productIds[] = $item->getData('combinationProduct');
            }
            $prodCollection = Mage::getResourceModel('catalog/product_collection')
                ->addAttributeToSelect('entity_id')
                ->addAttributeToFilter('entity_id', array('in' => $productIds))
            ;
            Mage::getSingleton('catalog/product_status')->addSaleableFilterToCollection($prodCollection);
            Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($prodCollection);
            Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($prodCollection);
            $prodCollection->getSelect()->limit(8);
            if($prodCollection->count() >= 4){
                return $prodCollection;
            }
        }

        return null;

    }

    /**
     * Second function
     *
     * @return Mage_Catalog_Model_Resource_Product_Collection|null
     */
    public function getFlatRecommended2()
    {
        /** @var Mage_Catalog_Model_Product $product */
        $product = $this->getProduct();

        /** @var Mage_Core_Model_Resource $resource */
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');

        $query = "select combinationProduct from
            (select rankingSub2.combinationProduct, rankingSub2.rank from
            (
            select rawSub.combinationProduct, @curRank := @curRank + 1 AS rank, RAND() as random
            from
            (
            select rpf.combinationProduct from recommendation_products_flat rpf
            inner join catalog_product_entity_int cpei ON rpf.combinationProduct = cpei.entity_id AND cpei.attribute_id = 273 AND cpei.store_id = 0 AND cpei.value = 1
            inner join catalog_product_website cpw ON rpf.combinationProduct = cpw.product_id
            left join cataloginventory_stock_item cisi ON rpf.combinationProduct = cisi.product_id
            where rpf.baseProduct = {$product->getId()} AND (cisi.is_in_stock = 1 OR (cisi.manage_stock = 0 AND cisi.use_config_manage_stock = 0)) AND rpf.filter = 1
            group by rpf.combinationProduct
            order by rpf.weighted_metric desc
            ) rawSub, (SELECT @curRank := 0) r
            ) rankingSub2
            where rankingSub2.rank < 11
            order by rankingSub2.random
            limit 5) rankingsub
            order by rank asc";

        $result = $readConnection->fetchAll($query);

        if(count($result) < 5){
            return null;//viewed_also_bought_emarsys
        }else{
            $random = rand(0,10);
            if($random < 8){
                return null;//viewed_also_bought_emarsys
            }else{
                $babyId = Mage::getModel('catalog/category')->getCollection()
                    ->addAttributeToFilter('url_key', 'baby')
                    ->getFirstItem()->getId();
                $petsId = Mage::getModel('catalog/category')->getCollection()
                    ->addAttributeToFilter('url_key', 'pets')
                    ->getFirstItem()->getId();
                if(in_array($babyId,$product->getCategoryIds()) || in_array($petsId,$product->getCategoryIds())) {
                    return null;//viewed_also_bought_emarsys
                }else{
                    $ids = array();
                    foreach ($result as $row) {
                        if (isset($row['combinationProduct'])) {
                            $ids[] = $row['combinationProduct'];
                        }
                    }
                    /** @var Mage_Catalog_Model_Resource_Product_Collection $prodCollection */
                    $prodCollection = Mage::getResourceModel('catalog/product_collection')
                        ->addAttributeToSelect('entity_id')
                        ->addAttributeToFilter('entity_id', array('in' => $ids));

                    if ($prodCollection->count() >= 4) {
                        return $prodCollection;//viewed_also_bought_inhouse
                    } else {
                        return null;//viewed_also_bought_emarsys
                    }
                }
            }
        }
    }
}