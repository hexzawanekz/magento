<?php
// Base-name
$name = 'moxy';
$engStoreName = 'Moxy English';
$engStoreCode = 'moxyen';
$thStoreName = 'Moxy Thai';
$thStoreCode = 'moxyth';

// Init Magento
Mage::app();
// Set current store
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

// Load the website
$website = Mage::getModel('core/website')->load('base', 'code');
if ($website->getId() > 0) {
    $websiteId = $website->getId();

// Get the group
    $group = Mage::getModel('core/store_group');
    $groups = Mage::getModel('core/store_group')->getCollection()
        ->addFieldToSelect('*')
        ->addFieldToFilter('name', array('like' => ucfirst($name)))
        ->addFieldToFilter('website_id', $websiteId);
    if ($groups->count() > 0 && $groups->count() < 2) {
        $group = $groups->getFirstItem();
    }
    $groupId = $group->getGroupId();

// English
    $engStore = Mage::getModel('core/store');
    $engStoresChecker = Mage::getModel('core/store')->getCollection()
        ->addFieldToSelect('*')
        ->addFieldToFilter('code', $engStoreCode)
        ->addFieldToFilter('website_id', $websiteId)
        ->addFieldToFilter('group_id', $groupId);
    if ($engStoresChecker->count() > 0 && $engStoresChecker->count() < 2) {
        $engStore = $engStoresChecker->getFirstItem();
    }
    $engStoreId = $engStore->getStoreId();

// Thai
    $thStore = Mage::getModel('core/store');
    $thStoresChecker = Mage::getModel('core/store')->getCollection()
        ->addFieldToSelect('*')
        ->addFieldToFilter('code', $thStoreCode)
        ->addFieldToFilter('website_id', $websiteId)
        ->addFieldToFilter('group_id', $groupId);
    if ($thStoresChecker->count() > 0 && $thStoresChecker->count() < 2) {
        $thStore = $thStoresChecker->getFirstItem();
    }
    $thStoreId = $thStore->getStoreId();

// Save config
    $configurator = new Mage_Core_Model_Config();
    $configurator->saveConfig('web/cookie/cookie_domain', 'stage.moxyst.com', 'stores', $engStoreId);
    $configurator->saveConfig('web/cookie/cookie_domain', 'stage.moxyst.com', 'stores', $thStoreId);
}
