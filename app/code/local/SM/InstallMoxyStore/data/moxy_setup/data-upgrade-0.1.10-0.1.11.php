<?php

// Base-name
$name           = 'moxy';
$engStoreName   = 'Moxy English';
$engStoreCode   = 'moxyen';
$thStoreName    = 'Moxy Thai';
$thStoreCode    = 'moxyth';
$storeIdMoxyEn  = Mage::getModel('core/store')->load($engStoreCode, 'code')->getId();
$storeIdMoxyTh  = Mage::getModel('core/store')->load($thStoreCode, 'code')->getId();
$newProductsCateId = 0;
$bestSellersCateId = 0;
try {
    $installer = $this;
    $installer->startSetup();

    // Force the store to be admin
    Mage::app()->setUpdateMode(false);
    Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
    if (!Mage::registry('isSecureArea'))
        Mage::register('isSecureArea', 1);

    // Load the website
    $website = Mage::getModel('core/website')->load('base', 'code');
    if ($website->getId() > 0) {
        $websiteId = $website->getId();
        // Get the group
        $group = Mage::getModel('core/store_group');
        $groups = Mage::getModel('core/store_group')->getCollection()
            ->addFieldToSelect('*')
            ->addFieldToFilter('name', array('like' => ucfirst($name)))
            ->addFieldToFilter('website_id', $websiteId);
        if ($groups->count() > 0 && $groups->count() < 2) {
            $group = $groups->getFirstItem();
        }
        $groupId = $group->getGroupId();

// English
        $engStore = Mage::getModel('core/store');
        $engStoresChecker = Mage::getModel('core/store')->getCollection()
            ->addFieldToSelect('*')
            ->addFieldToFilter('code', $engStoreCode)
            ->addFieldToFilter('website_id', $websiteId)
            ->addFieldToFilter('group_id', $groupId);
        if ($engStoresChecker->count() > 0 && $engStoresChecker->count() < 2) {
            $engStore = $engStoresChecker->getFirstItem();
        }
        $engStoreId = $engStore->getStoreId();

// Thai
        $thStore = Mage::getModel('core/store');
        $thStoresChecker = Mage::getModel('core/store')->getCollection()
            ->addFieldToSelect('*')
            ->addFieldToFilter('code', $thStoreCode)
            ->addFieldToFilter('website_id', $websiteId)
            ->addFieldToFilter('group_id', $groupId);
        if ($thStoresChecker->count() > 0 && $thStoresChecker->count() < 2) {
            $thStore = $thStoresChecker->getFirstItem();
        }
        $thStoreId = $thStore->getStoreId();

        // Save config
        $configurator = new Mage_Core_Model_Config();

        $trustedCompanyCatalogEn = <<<EOD
<div class="trustedcompany-widget" style="display:block;border-bottom:1px dotted #000;margin-bottom:20px;padding-bottom:10px;padding-left:17px;"><iframe id="trustedcompany-widget" width="180" height="291" frameborder="0" scrolling="no"></iframe><a href="http://trustedcompany.com/th/moxyst.com-reviews" target="_blank" title="รีวิว Moxyst"></a><script>(function(){document.getElementById('trustedcompany-widget').src='//trustedcompany.com/embed/widget/v2?domain=moxyst.com&type=a&review=1&text=a';})();</script></div>
EOD;
        $trustedCompanyCatalogTh = <<<EOD
<div class="trustedcompany-widget" style="display:block;border-bottom:1px dotted #000;margin-bottom:20px;padding-bottom:10px;padding-left:17px;"><iframe id="trustedcompany-widget" width="180" height="291" frameborder="0" scrolling="no"></iframe><a href="http://trustedcompany.com/th/moxyst.com-reviews" target="_blank" title="รีวิว Moxyst"></a><script>(function(){document.getElementById('trustedcompany-widget').src='//trustedcompany.com/embed/widget/v2?domain=moxyst.com&type=a&review=1&text=a';})();</script></div>
EOD;
        $trustedCompanyDetailEn = <<<EOD
<div class="trustedcompany-widget" style="display:inline-block;width:220px;float:right;"><iframe id="trustedcompany-widget" width="180" height="291" frameborder="0" scrolling="no"></iframe><a href="http://trustedcompany.com/th/moxyst.com-reviews" target="_blank" title="รีวิว Moxyst"></a><script>(function(){document.getElementById('trustedcompany-widget').src='//trustedcompany.com/embed/widget/v2?domain=moxyst.com&type=a&review=1&text=a';})();</script></div>

EOD;
        $trustedCompanyDetailTh = <<<EOD
<div class="trustedcompany-widget" style="display:inline-block;width:220px;float:right;"><iframe id="trustedcompany-widget" width="180" height="291" frameborder="0" scrolling="no"></iframe><a href="http://trustedcompany.com/th/moxyst.com-reviews" target="_blank" title="รีวิว Moxyst"></a><script>(function(){document.getElementById('trustedcompany-widget').src='//trustedcompany.com/embed/widget/v2?domain=moxyst.com&type=a&review=1&text=a';})();</script></div>
EOD;

        $configurator->saveConfig('trustedcompany/product_list/is_displayed', 1, 'stores', $engStoreId);
        $configurator->saveConfig('trustedcompany/product_list/is_displayed', 1, 'stores', $thStoreId);
        $configurator->saveConfig('trustedcompany/product_list/content', $trustedCompanyCatalogEn, 'stores', $engStoreId);
        $configurator->saveConfig('trustedcompany/product_list/content', $trustedCompanyCatalogTh, 'stores', $thStoreId);

        $configurator->saveConfig('trustedcompany/product_detail/is_displayed', 1, 'stores', $engStoreId);
        $configurator->saveConfig('trustedcompany/product_detail/is_displayed', 1, 'stores', $thStoreId);
        $configurator->saveConfig('trustedcompany/product_detail/content', $trustedCompanyDetailEn, 'stores', $engStoreId);
        $configurator->saveConfig('trustedcompany/product_detail/content', $trustedCompanyDetailTh, 'stores', $thStoreId);
    }

    $installer->endSetup();
} catch (Excpetion $e) {
    Mage::logException($e);
    Mage::log("ERROR IN SETUP " . $e->getMessage());
}
