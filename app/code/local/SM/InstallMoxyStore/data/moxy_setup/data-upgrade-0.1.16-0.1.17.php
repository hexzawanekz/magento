<?php

// Base-name
$name           = 'moxy';
$engStoreName   = 'Moxy English';
$engStoreCode   = 'moxyen';
$thStoreName    = 'Moxy Thai';
$thStoreCode    = 'moxyth';
$storeIdMoxyEn  = Mage::getModel('core/store')->load($engStoreCode, 'code')->getId();
$storeIdMoxyTh  = Mage::getModel('core/store')->load($thStoreCode, 'code')->getId();
$newProductsCateId = 0;
$bestSellersCateId = 0;
try {
    $installer = $this;
    $installer->startSetup();


    //==========================================================================
    // Return Policy En Page
    //==========================================================================
    $pageTitle = "Return Policy";
    $pageIdentifier = "return-policy";
    $pageStores = array($storeIdMoxyEn);
    $pageIsActive = 1;
    $pageUnderVersionControl = 0;
    $pageContentHeading = "Return Policy";
    $pageContent = <<<EOD
<script type="text/javascript">// <![CDATA[
jQuery(document).ready(function()
{
jQuery('#small-box-links ul li a').click(function(){
jQuery('html, body').animate({
scrollTop: jQuery( jQuery.attr(this, 'href') ).offset().top
}, 500);
return false;
});
});
// ]]></script>
<div style="font-size: 16px; line-height: 22px;">We work very hard and do everything we can to make sure you are 100% satisfied with your shopping experience at Moxyst.com. However, if for some reason we have let you down or things did not go as planned, we have a hassle-free return policy and a dedicated team of folks to make it right.</div>
<div style="font-size: 16px; line-height: 22px;">&nbsp;</div>
<div id="small-box-container">
<div id="small-box1" class="small-box" style="font: bold 16px 'Tahoma'; color: #425d89; line-height: 22px;">Return Policy</div>
<div style="font-size: 16px; line-height: 22px;">At Moxy, we take pride in our quality products and services and we value our relationship with you. If at any time you feel our products do not meet your needs, we will gladly accept returns of items(s) by following our return policy.</div>
<br />
<ul style="list-style: square; margin-left: 40px; font-size: 16px; line-height: 22px;">
<li>We only accept the product that fall into following categories: defects occurred from the manufacturer, product is expired, product received does not match the order placed, product damaged during the shipping process.</li>
</ul>
</div>
<p>&nbsp;</p>
<div id="small-box2" class="small-box" style="font: bold 16px 'Tahoma'; color: #425d89; line-height: 22px;">Following the Return Policy above, please check your order upon delivery. If you find that the product does not match the order, expired, or defected, please reject and do not sign for the delivery. Please contact our customer care team as soon as possible to request a refund.</div>
<p>&nbsp;</p>
<div style="font-size: 16px; line-height: 22px;">
<p><span style="font: bold 16px 'Tahoma'; color: #425d89;">Contact us</span><br /> Customer Care Email: <a href="mailto:support@moxyst.com">support@moxyst.com</a><br /> Customer Care: Phone: 02-106-8222<br /> Customer Care Hours: 10:00-19:00 Mon-Sat</p>
</div>
EOD;

    $pageRootTemplate = 'one_column';
    $pageLayoutUpdateXml = <<<EOD
EOD;


    $pageCustomLayoutUpdateXML = <<<EOD
EOD;

    $pageMetaKeywords = "";
    $pageMetaDescription = "";

    $page = Mage::getModel('cms/page')->getCollection()
        ->addStoreFilter(array($storeIdMoxyEn), $withAdmin = true)
        ->addFieldToFilter('identifier', $pageIdentifier)
        ->getFirstItem();
    if ($page->getId() == 0) {
        $page = Mage::getModel('cms/page');
    }
    else
    {
        // if exists then repair
        $page = Mage::getModel('cms/page')->load($page->getId());
    }

    $page->setTitle($pageTitle);
    $page->setIdentifier($pageIdentifier);
    $page->setStores($pageStores);
    $page->setIsActive($pageIsActive);
    $page->setUnderVersionControl($pageUnderVersionControl);
    $page->setContentHeading($pageContentHeading);
    $page->setContent($pageContent);
    $page->setRootTemplate($pageRootTemplate);
    $page->setLayoutUpdateXml($pageLayoutUpdateXml);
    $page->setCustomLayoutUpdateXml($pageCustomLayoutUpdateXML);
    $page->setMetaKeywords($pageMetaKeywords);
    $page->setMetaDescription($pageMetaDescription);
    $page->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================

    //==========================================================================
    // Return Policy TH Page
    //==========================================================================
    $pageTitle = "นโยบายการส่งคืนสินค้า เงื่อนไขการรับคืนสินค้า | Moxy Thailand";
    $pageIdentifier = "return-policy";
    $pageStores = array($storeIdMoxyTh);
    $pageIsActive = 1;
    $pageUnderVersionControl = 0;
    $pageContentHeading = "นโยบายการส่งคืนสินค้า เงื่อนไขการรับคืนสินค้า";
    $pageContent = <<<EOD
<script type="text/javascript">// <![CDATA[
jQuery(document).ready(function()
{
jQuery('#small-box-links ul li a').click(function(){
jQuery('html, body').animate({
scrollTop: jQuery( jQuery.attr(this, 'href') ).offset().top
}, 500);
return false;
});
});
// ]]></script>
<div style="font-size: 16px; line-height: 22px;">เราทุ่มเทและทำงานอย่างหนักที่จะพยายามพัฒนาระบบทุกอย่าง เพื่อให้มั่นใจร้อยเปอร์เซ็นต์ว่าคุณจะได้พบประสบการณ์ที่ยอดเยี่ยมในการช็อปปิ้งผ่าน Moxyst.com เราหวังเป็นอย่างยิ่งว่าคุณจะกลับ มาใช้บริการของเราอย่างต่อเนื่อง และบอกต่อถึงบริการอันยอดเยี่ยมนี้แก่เพื่อนของคุณ หากเกิดกรณีสุดวิสัยที่จะทำให้เกิดความผิดพลาดเกี่ยวกับสินค้าในขั้นตอนการจัดส่งเรามีนโยบายที่จะทำการรับคืนสินค้าโดยคำนึงถึงความสะดวกและยุติธรรมแก่ลูกค้ามากที่สุด&nbsp;</div>
<div style="font-size: 16px; line-height: 22px;">&nbsp;</div>
<div id="small-box-container">
<div id="small-box1" class="small-box" style="font: bold 16px 'Tahoma'; color: #425d89; line-height: 22px;">นโยบายการคืนสินค้าและขอคืนเงิน</div>
<div style="font-size: 16px; line-height: 22px;">ที่ Moxy เรามีความภูมิใจในมาตรฐานของสินค้าและบริการอันยอดเยี่ยม เรายินดีที่จะรับคืนสินค้าเหล่านั้นภายใต้เงื่อนไขที่ระบุไว้ตามนโยบายการคืนสินค้า</div>
<br />
<div style="font-size: 16px; line-height: 22px;">เรารับคืนสินค้าตามเงื่อนไขดังต่อไปนี้เท่านั้น</div>
<ul style="list-style: square; margin-left: 40px; font-size: 16px; line-height: 22px;">
<li>สินค้ามีความบกพร่องเสียหายอันเกิดจากผู้ผลิต, สินค้าหมดอายุ, ลูกค้าได้รับสินค้าไม่ตรงตามใบสั่งซื้อ, สินค้าได้รับความเสียหายในระหว่างการจัดส่ง<strong></strong></li>
</ul>
</div>
<p>&nbsp;</p>
<div id="small-box2" class="small-box" style="font: bold 16px 'Tahoma'; color: #425d89; line-height: 22px;">ตามนโยบายการคืนสินค้าที่กำหนดข้างต้น คุณสามารถขอคืนสินค้าได้โดยการตรวจสอบความเรียบร้อยของสินค้าที่ได้รับทันทีเมื่อได้รับสินค้า หากปรากฏว่าสินค้าที่ได้รับไม่ตรงตามใบสั่ง หมดอายุ หรือเสียหายจากการจัดส่ง กรุณาปฏิเสธและงดการเซ็นรับสินค้านั้นๆ และติดต่อเจ้าหน้าที่แผนกลูกค้าสัมพันธ์เพื่อดำเนินการขอเงินคืนทันที</div>
<p>&nbsp;</p>
<div style="font-size: 16px; line-height: 22px;">
<p><span style="font: bold 16px 'Tahoma'; color: #425d89;">ติดต่อเรา</span><br /> แผนกลูกค้าสัมพันธ์<br /> อีเมล <a href="mailto:support@moxyst.com">support@moxyst.com</a><br /> โทรศัพท์ : 02-106-8222 <br /> เวลาทำการ : วันจันทร์ &ndash; เสาร์ 10:00 น. - 19:00 น.</p>
</div>
EOD;

    $pageRootTemplate = 'one_column';
    $pageLayoutUpdateXml = <<<EOD
EOD;


    $pageCustomLayoutUpdateXML = <<<EOD
EOD;

    $pageMetaKeywords = "";
    $pageMetaDescription = "";

    $page = Mage::getModel('cms/page')->getCollection()
        ->addStoreFilter(array($storeIdMoxyTh), $withAdmin = true)
        ->addFieldToFilter('identifier', $pageIdentifier)
        ->getFirstItem();
    if ($page->getId() == 0) {
        $page = Mage::getModel('cms/page');
    }
    else
    {
        // if exists then repair
        $page = Mage::getModel('cms/page')->load($page->getId());
    }

    $page->setTitle($pageTitle);
    $page->setIdentifier($pageIdentifier);
    $page->setStores($pageStores);
    $page->setIsActive($pageIsActive);
    $page->setUnderVersionControl($pageUnderVersionControl);
    $page->setContentHeading($pageContentHeading);
    $page->setContent($pageContent);
    $page->setRootTemplate($pageRootTemplate);
    $page->setLayoutUpdateXml($pageLayoutUpdateXml);
    $page->setCustomLayoutUpdateXml($pageCustomLayoutUpdateXML);
    $page->setMetaKeywords($pageMetaKeywords);
    $page->setMetaDescription($pageMetaDescription);
    $page->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================



    $installer->endSetup();
} catch (Excpetion $e) {
    Mage::logException($e);
    Mage::log("ERROR IN SETUP " . $e->getMessage());
}
