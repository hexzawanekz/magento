<?php

// Base-name
$name           = 'moxy';
$engStoreName   = 'Moxy English';
$engStoreCode   = 'moxyen';
$thStoreName    = 'Moxy Thai';
$thStoreCode    = 'moxyth';
$storeIdMoxyEn  = Mage::getModel('core/store')->load($engStoreCode, 'code')->getId();
$storeIdMoxyTh  = Mage::getModel('core/store')->load($thStoreCode, 'code')->getId();
$newProductsCateId = 0;
$bestSellersCateId = 0;
try {
    $installer = $this;
    $installer->startSetup();


    //==========================================================================
    // Terms and Conditions EN Page
    //==========================================================================
    $pageTitle = "Terms and Conditions";
    $pageIdentifier = "terms-and-conditions";
    $pageStores = array($storeIdMoxyEn);
    $pageIsActive = 1;
    $pageUnderVersionControl = 0;
    $pageContentHeading = "Terms and Conditions";
    $pageContent = <<<EOD
<div style="font: normal 14px 'Tahoma'; line-height: 22px; color: #505050;"><span style="font: bold 16px 'Tahoma';">1. Introduction</span> <br />
<p>These are the terms and conditions of our agreement which apply to all purchases of products by you from Moxyst.com, owned and operated by Whatsnew Inc., including subsidiaries and affiliates provide you, its Services (herein after referred as &ldquo;Website&rdquo;) under the following conditions. Please read the following terms carefully. If you do not agree to the following Terms &amp; Conditions you may not enter or use this Website. If you continue to browse and use this Website you are agreeing to comply with and be bound by the following terms and conditions of use, which together with our privacy policy governs Whatsnew and its use by you in relation to this Website.&nbsp;</p>
<p>&nbsp;</p>
<span style="font: bold 16px 'Tahoma';">2. Information and Product Description</span> <br />
<p>While every effort is made to update the information contained on this Website, neither Whatsnew nor any third party or data or content provider make any representations or warranties, whether express, implied in law or residual, as to the sequence, accuracy, completeness or reliability of information, opinions, any pricing information, research information, data and/or content contained on the Website (including but not limited to any information which may be provided by any third party or data or content providers) and shall not be bound in any manner by any information contained on the Website.&nbsp;</p>
<p>Whatsnew reserves the right at any time to change or discontinue without notice, any aspect or feature of this Website. No information shall be construed as advice and information is offered for information purposes only and is not intended for trading purposes. You and your company rely on the information contained on this Website at your own risk. If you find an error or omission at this site, please let us know.&nbsp;</p>
<p>&nbsp;</p>
<span style="font: bold 16px 'Tahoma';">3. Trademarks and Copyright</span> <br />
<p>The trade marks, names, logos and service marks (collectively "trademarks") displayed on this Website are all registered and unregistered trademarks of Whatsnew. Nothing contained on this Website should be construed as granting any license or right to use any trade mark without the prior written permission of Whatsnew.&nbsp;</p>
<p>&nbsp;</p>
<span style="font: bold 16px 'Tahoma';">4. External Links</span> <br />
<p>External links may be provided for your convenience, but they are beyond the control of Moxyst.com and no representation is made as to their content. Use or reliance on any external links and the content thereon provided is at your own risk. When visiting external links you must refer to the terms and conditions of use for that external Website. No hypertext links shall be created from any Website controlled by you or otherwise to this Website without the express prior written permission of Whatsnew. Please contact us if you would like to link to this Website or would like to request a link to your Website.&nbsp;</p>
<p>&nbsp;</p>
<span style="font: bold 16px 'Tahoma';">5. Public Forums and User Submissions</span> <br />
<p>Whatsnew is not responsible for any material submitted to the public areas by you (which include bulletin boards, hosted pages, chat rooms, or any other public area found on the Website. Any material (whether submitted by you or any other user) is not endorsed, reviewed or approved by Whatsnew. We reserve the right to remove any material submitted or posted by you in the public areas, without notice to you, if it becomes aware and determines, in its sole and absolute discretion that you are or there is the likelihood that you may, including but not limited to</p>
<table>
<tbody>
<tr>
<td width="15">5.1</td>
<td>defame, abuse, harass, stalk, threaten or otherwise violate the rights of other users or any third parties;</td>
</tr>
<tr>
<td>5.2</td>
<td>publish, post, distribute or disseminate any defamatory, obscene, indecent or unlawful material or information;</td>
</tr>
<tr>
<td>5.3</td>
<td>post or upload files that contain viruses, corrupted files or any other similar software or programs that may damage the operation of Whatsnew and/or a third party computer system and/or network;</td>
</tr>
<tr>
<td>5.4</td>
<td>violate any copyright, trade mark, other applicable Thailand or international laws or intellectual property rights of Whatsnew or any other third party;</td>
</tr>
<tr>
<td>5.5</td>
<td>submit content containing marketing or promotional material which is intended to solicit business.</td>
</tr>
</tbody>
</table>
<br /><br /> <span style="font: bold 16px 'Tahoma';">6. Membership</span> <br />
<p>Whatsnew Website is not available to users under the age of 18, outside the demographic target, or to any members previously banned by Whatsnew. Users are allowed only one active account. Breeching these conditions could result in account termination.&nbsp;</p>
<p>Whatsnew on occasion will offer its members promotions. In connection with these promotions, any user that receives Rewards (prizes, credits, gift cards, coupons or other benefits) from Whatsnew through the use of multiple accounts, email addresses, falsified information or fraudulent conduct, shall forfeit any Rewards gained through such activity and may be liable for civil and/or criminal penalties by law.&nbsp;</p>
<p>By using the Whatsnew Website, you acknowledge that you are of legal age to form a binding contract and are not a person barred from receiving services under the laws of the Thailand or other applicable jurisdiction. You agree to provide true and accurate information about yourself when requested by Whatsnew Website. If you provide any information that is untrue, inaccurate, or incomplete, Whatsnew has the right to suspend or terminate your account and refuse future use of its services. You are responsible for maintaining the confidentiality of your account and password, and accept all responsible for activities that occur under your account.&nbsp;</p>
<br /> <span style="font: bold 16px 'Tahoma';">7. Cancellation Due To Errors</span> <br />
<p>Whatsnew has the right to cancel an order at anytime due to typographical or unforeseen errors that results in the product(s) on the site being listed inaccurately (having the wrong price or descriptions etc.). In the event a cancellation occurs and payment for the order has been received, Whatsnew shall issue a full refund for the product in the amount in question.&nbsp;</p>
<p>&nbsp;</p>
<span style="font: bold 16px 'Tahoma';">8. Specific Use </span> <br />
<p>You further agree not to use the Website to send or post any message or material that is unlawful, harassing, defamatory, abusive, indecent, threatening, harmful, vulgar, obscene, sexually orientated, racially offensive, profane, pornographic or violates any applicable law and you hereby indemnify Whatsnew against any loss, liability, damage or expense of whatever nature which Whatsnew or any third party may suffer which is caused by or attributable to, whether directly or indirectly, your use of the Website to send or post any such message or material.&nbsp;</p>
<br /> <span style="font: bold 16px 'Tahoma';">9. Warranties</span> <br />
<p>Whatsnew makes no warranties, representations, statements or guarantees (whether express, implied in law or residual) regarding the Website, the information contained on the Website, you or your company's personal information or material and information transmitted over our system.&nbsp;</p>
<br /> <span style="font: bold 16px 'Tahoma';">10. Disclaimer</span> <br />
<p>Whatsnew shall not be responsible for and disclaims all liability for any loss, liability, damage (whether direct, indirect or consequential), personal injury or expense of any nature whatsoever which may be suffered by you or any third party (including your company). Along with result of or which may be attributable, directly or indirectly, to your access and use of the Website, any information contained on the Website, you or your company's personal information or material and information transmitted over our system. In particular, neither Whatsnew nor any third party or data or content provider shall be liable in any way to you or to any other person, firm or corporation whatsoever for any loss, liability, damage (whether direct or consequential), personal injury or expense of any nature whatsoever arising from any delays, inaccuracies, errors in, or omission of any share price information or the transmission thereof, or for any actions taken in reliance thereon or occasioned thereby or by reason of non-performance or interruption, or termination thereof.&nbsp;</p>
<p>The Whatsnew Referral Credits and Rewards Programs and its benefits are offered at the discretion of Whatsnew. Whatsnew has the right to modify or discontinue, temporarily or permanently, the services offered, including the point levels, in whole or in part for any reason, at its sole discretion, with or without prior notice to its members. Whatsnew may, among other things, withdraw, limit, or modify; increase and decrease the credits required for any Special Offer related to the Referral Credits and Rewards Programs. By becoming a member of Whatsnew, you agree that the Whatsnew Referral Credits and Rewards Programs will not be liable to you or any third-party for any modification or discontinuance of such Programs.&nbsp;</p>
<br /> <span style="font: bold 16px 'Tahoma';">11. Indemnity</span> <br />
<p>User agrees to indemnify and not hold Whatsnew (and its employees, directors, suppliers, subsidiaries, joint ventures, and legal partners) from any claim or demand. Including reasonable attorneys&rsquo; fees, from and against all losses, expenses, damages and costs resulting from any violation of these terms and conditions or any activity related to user&rsquo;s membership account due negligent or wrongful conduct.&nbsp;</p>
<br /> <span style="font: bold 16px 'Tahoma';">12. Use of the Website</span> <br />
<p>Whatsnew does not make any warranty or representation that information on the Website is appropriate for use in any jurisdiction (other than Kingdom of Thailand). By accessing the Website, you warrant and represent to Whatsnew that you are legally entitled to do so and to make use of information made available via the Website.&nbsp;</p>
<br /> <span style="font: bold 16px 'Tahoma';">13. General</span> <br />
<table>
<tbody>
<tr>
<td width="15">13.1</td>
<td>Entire Agreement . These Website terms and conditions constitute the sole record of the agreement between you and Whatsnew in relation to your use of the Website. Neither you nor Whatsnew shall be bound by any expressed or implied representation, warranty, or promise not recorded herein. Unless otherwise specifically stated, these Website terms and conditions supersede and replace all prior commitments, undertakings or representations, whether written or oral, between you and Whatsnew in respect of your use of the Website.</td>
</tr>
<tr>
<td>13.2</td>
<td>Alteration . Whatsnew may at any time modify any relevant terms and conditions, policies or notices. You acknowledge that by visiting the Website from time to time, you shall become bound to the current version of the relevant terms and conditions (the "current version") and, unless stated in the current version, all previous versions shall be superseded by the current version. You shall be responsible for reviewing the current version each time you visit the Website.</td>
</tr>
<tr>
<td>13.3</td>
<td>Conflict . Where any conflict or contradiction appears between the provisions of these Website terms and conditions and any other relevant terms and conditions, policies or notices, the other relevant terms and conditions, policies or notices which relate specifically to a particular section or module of the Website shall prevail in respect of your use of the relevant section or module of the Website.</td>
</tr>
<tr>
<td>13.4</td>
<td>Waiver . No indulgence or extension of time which either you or Whatsnew may grant to the other will constitute a waiver of or, whether by law or otherwise, limit any of the existing or future rights of the grantor in terms hereof, save in the event or to the extent that the grantor has signed a written document expressly waiving or limiting such rights.</td>
</tr>
<tr>
<td>13.5</td>
<td>Cession. Whatsnew shall be entitled to cede, assign and delegate all or any of its rights and obligations in terms of any relevant terms and conditions, policies and notices to any third party.</td>
</tr>
<tr>
<td>13.6</td>
<td>Severability . All provisions of any relevant terms and conditions, policies and notices are, notwithstanding the manner in which they have been grouped together or linked grammatically, severable from each other. Any provision of any relevant terms and conditions, policies and notices, which is or becomes unenforceable in any jurisdiction, whether due to void, invalidity, illegality, unlawfulness or for any reason whatever, shall, in such jurisdiction only and only to the extent that it is so unenforceable, be treated as pro non-script and the remaining provisions of any relevant terms and conditions, policies and notices shall remain in full force and effect.</td>
</tr>
<tr>
<td>13.7</td>
<td>Applicable laws. Any relevant terms and conditions, policies and notices shall be governed by and construed in accordance with the laws of Thailand without giving effect to any principles of conflict of law. You hereby consent to the exclusive jurisdiction of the High Court of Thailand in respect of any disputes arising in connection with the Website, or any relevant terms and conditions, policies and notices or any matter related to or in connection therewith.</td>
</tr>
<tr>
<td>13.8</td>
<td>Comments or Questions . If you have any questions, comments or concerns arising from the Website, the privacy policy or any other relevant terms and conditions, policies and notices or the way in which we are handling your personal information please <a href="{{store url='contacts'}}" target="_blank">contact us</a>.</td>
</tr>
</tbody>
</table>
<br /><br /> <span style="font: bold 16px 'Tahoma';">14. Termination</span> <br />
<p>These terms and conditions are applicable to you upon your accessing the Whatsnew Website and/or completing the registration or shopping process. These terms and conditions, or any of them, may be modified or terminated by Whatsnew without notice at any time for any reason. The provisions relating to Copyrights and Trademarks, Disclaimer, Claims, Limitation of Liability, Indemnification, Applicable Laws, Arbitration and General, shall survive any termination.&nbsp;</p>
<br /> <span style="font: bold 16px 'Tahoma';">15. Cancellations and Returns</span> <br />
<p>Orders can be cancelled if their products have not yet be delivered. Once products are delivered to the users account, they are no longer permitted to cancel. Any returns after that are handled on a case-by-case basis. Within reason, we will do what we can to ensure customer satisfaction. Unless there is something wrong with the purchase, we are generally unable to offer refunds.&nbsp;</p>
<br /> <span style="font: bold 16px 'Tahoma';">16. Credits</span> <br />
<p>The Whatsnew Referral Credits and Rewards Programs and its benefits are offered at the discretion of Whatsnew. Whatsnew has the right to modify or discontinue, temporarily or permanently, the services offered, including the point levels, in whole or in part for any reason, at its sole discretion, with or without prior notice to its members. Whatsnew may, among other things, withdraw, limit, or modify; increase and decrease the credits required for any Special Offer related to the Referral Credits and Rewards Programs. By becoming a member of Whatsnew, you agree that the Whatsnew Referral Credits and Rewards Programs will not be liable to you or any third-party for any modification or discontinuance of such Programs.&nbsp;</p>
<br /> <span style="font: bold 16px 'Tahoma';">17. Privacy </span> <br />
<p>For Delivery service, we are fully aware of our duty to keep all customers' information confidential. In addition to our duty of confidentiality to customers, we shall at all times fully observe the Personal Data (Privacy) Ordinance ("the Ordinance") in collecting, maintaining and using the personal data of customers. In particular, we observe the following principles, save otherwise appropriately agreed by the customers:&nbsp;</p>
<table>
<tbody>
<tr>
<td width="15">17.1</td>
<td>Collection of personal data from customers shall be for purposes relating to the provision of logistics services or related services;</td>
</tr>
<tr>
<td>17.2</td>
<td>All practical steps will be taken to ensure that personal data are accurate and will not be kept longer than necessary or will be destroyed in accordance with our internal retention period;</td>
</tr>
<tr>
<td>17.3</td>
<td>Personal data will not be used for any purposes other than the data that were to be used at the time of collection or purposes directly related thereto</td>
</tr>
<tr>
<td>17.4</td>
<td>Personal data will be protected against unauthorized or accidental access, processing or erasure;</td>
</tr>
<tr>
<td>17.5</td>
<td>Customers shall have the right for correction of their personal data held by us and that customers' request for access or correction will be dealt with in accordance with the Ordinance.</td>
</tr>
</tbody>
</table>
<br /><br /> <span style="font: bold 16px 'Tahoma';">18. Payment Policy</span> <br />
<table border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td style="padding: 0 7px;">VISA Card and Master Card</td>
<td>You will receive an order confirmation email within 24 hours for credit card payment. If the payment fails to go through, the system will cancel the order automatically and you will be notified.</td>
</tr>
<tr>
<td style="padding: 0 7px;">PayPal</td>
<td>You can make PayPal account or by credit cards on PayPal website. It is a fast, safe and convenient way to a payment.</td>
</tr>
<tr>
<td style="padding: 0 7px;">Cash on Delivery</td>
<td>Free of charge.</td>
</tr>
</tbody>
</table>
<br /><br />A customer's personal data is classified as confidential and can only be disclosed by us where permitted by the Ordinance or otherwise legally compelled to do so.</div>
EOD;

    $pageRootTemplate = 'one_column';
    $pageLayoutUpdateXml = <<<EOD
EOD;


    $pageCustomLayoutUpdateXML = <<<EOD
EOD;

    $pageMetaKeywords = "";
    $pageMetaDescription = "";

    $page = Mage::getModel('cms/page')->getCollection()
        ->addStoreFilter(array($storeIdMoxyEn), $withAdmin = true)
        ->addFieldToFilter('identifier', $pageIdentifier)
        ->getFirstItem();
    if ($page->getId() == 0) {
        $page = Mage::getModel('cms/page');
    }
    else
    {
        // if exists then repair
        $page = Mage::getModel('cms/page')->load($page->getId());
    }

    $page->setTitle($pageTitle);
    $page->setIdentifier($pageIdentifier);
    $page->setStores($pageStores);
    $page->setIsActive($pageIsActive);
    $page->setUnderVersionControl($pageUnderVersionControl);
    $page->setContentHeading($pageContentHeading);
    $page->setContent($pageContent);
    $page->setRootTemplate($pageRootTemplate);
    $page->setLayoutUpdateXml($pageLayoutUpdateXml);
    $page->setCustomLayoutUpdateXml($pageCustomLayoutUpdateXML);
    $page->setMetaKeywords($pageMetaKeywords);
    $page->setMetaDescription($pageMetaDescription);
    $page->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================

    //==========================================================================
    // Terms and Conditions TH Page
    //==========================================================================
    $pageTitle = "ข้อกำหนดและเงื่อนไขการใช้งานเว็บไซต์";
    $pageIdentifier = "terms-and-conditions";
    $pageStores = array($storeIdMoxyTh);
    $pageIsActive = 1;
    $pageUnderVersionControl = 0;
    $pageContentHeading = "ข้อกำหนดและเงื่อนไข";
    $pageContent = <<<EOD
<div style="font: normal 14px 'Tahoma'; line-height: 22px; color: #505050;"><span style="font: bold 16px 'Tahoma';">1. บทนำ</span> <br />บริษัท Whatsnew จำกัด ได้จัดทำเว็บไซต์ชื่อ www.moxyst.com เพื่อขายและส่งเสริมการขายสินค้าของบริษัท Whatsnew จำกัด ตลอดจนเป็นช่องทางในการติดต่อสื่อสารระหว่างบริษัท Whatsnew จำกัด กับผู้ใช้บริการ เราขอความกรุณาให้คุณได้อ่านข้อตกลงดังต่อไปนี้อย่างละเอียด หากคุณไม่ยอมรับข้อตกลงและเงื่อนไขดังกล่าว กรุณาหลีกเลี่ยงหรืออย่าเข้าใช้งานที่เว็บไซต์นี้ การที่คุณใช้เว็บไซต์นี้หรือเข้าไปดูข้อมูลในหน้าใดๆของเว็บไซต์นี้ ถือว่าคุณยอมรับข้อผูกพันทางกฎหมายที่ระบุไว้ในข้อตกลงและเงื่อนไขการใช้บริการนี้ โดยรวมถึงนโยบายความเป็นส่วนตัวที่ บริษัท Whatsnew จำกัด กำหนดขึ้นด้วย<br /><br /> <span style="font: bold 16px 'Tahoma';">2. ข้อมูลต่างๆในเว็บไซต์ </span> <br />ถึงแม้ว่า จะได้พยายามทุกวิถีทางที่จะทำให้ข้อมูลต่างๆในเว็บไซต์ของเรามีความถูกต้องมากที่สุด แต่ทาง บริษัท Whatsnew ก็ ไม่สามารถรับประกันว่าข้อมูลและส่วนประกอบดังกล่าวมีความถูกต้อง สมบูรณ์เพียงพอ ทันกาลทันเวลา หรือ เหมาะสมกับวัตถุประสงค์ใดโดยเฉพาะ ทั้งนี้ บริษัท Whatsnew จะไม่รับผิดสำหรับความผิดพลาดหรือการละเว้นใดๆในข้อมูลและส่วนประกอบนั้น (รวมไปถึงข้อมูลหรือเนื้อหาใดๆก็ตามที่มีบุคคลที่สามหรือรวมไปถึง ข้อมูลและการรับประกันที่มีการนำเสนอขึ้นมา บริษัท Whatsnew ขอสงวนสิทธิ์ที่จะทำการแก้ไขเปลี่ยนแปลงส่วนหนึ่งส่วนใดของเว็บไซต์หรือยุติการให้บริการได้ทุกเมื่อ ทั้งที่เป็นการถาวรหรือชั่วคราว โดยไม่จำเป็นต้องมีการแจ้งให้ทราบล่วงหน้าก็ได้ ข้อมูลและองค์ประกอบทุกๆส่วนในเว็บไซด์นี้ มีวัตถุประสงค์เพื่อให้ข้อมูลแก่ผู้บริโภคทั่วไปเท่านั้น ไม่มีข้อมูลส่วนหนึ่งส่วนใดที่มีวัตถุประสงค์เพื่อเป็นการให้คำแนะนำ การวินิจฉัย การให้คำปรึกษา แต่อย่างใด รวมถึงมิได้มีวัตถุประสงค์เพื่อการแลกเปลี่ยน พึงระลึกไว้ว่าคุณและผู้ติดตามของคุณใช้ข้อมูลที่ประกอบขึ้นในเว็บไซต์ของเราด้วยความเสี่ยงของตัวคุณเอง หากคุณพบข้อผิดพลาดหรือการละเลย เว้นว่างในส่วนใดของเว็บไซต์ของเรา กรุณาแจ้งให้เราทราบ&nbsp;<br /><br /> <span style="font: bold 16px 'Tahoma';">3. เครื่องหมายการค้าและลิขสิทธิ์ </span> <br />บริษัท Whatsnew เป็นเจ้าของลิขสิทธิ์ เครื่องหมายการค้า สัญลักษณ์และส่วนประกอบอื่นๆ ที่ปรากฏในทุกๆหน้าของเว็บไซต์นี้ (กล่าวโดยรวมคือ "เครื่องหมายการค้า") ห้ามมิให้ผู้ใด หรือ บริษัทใด ทำการดัดแปลง จัดเก็บในระบบที่สามารถนำมาใช้งานได้ ถ่ายโอน ลอกเลียนแบบ เผยแพร่ หรือ ใช้ข้อมูลและส่วนประกอบนั้นโดยมิได้รับความยินยอมเป็นลายลักษณ์อักษรจาก บริษัท Whatsnew<br /><br /> <span style="font: bold 16px 'Tahoma';">4. การเชื่อมโยงกับเว็บไซต์อื่น&nbsp;</span> <br />เว็บไซต์ของ บริษัท Whatsnew อาจมีการเชื่อมโยงกับเว็บไซต์อื่น เพื่อวัตถุประสงค์ในการให้ข้อมูลและเพื่อความสะดวกของคุณ บริษัท Whatsnew มิได้ให้การรับรองหรือยืนยันความถูกต้องในเนื้อหาของเว็บไซต์เหล่านั้น และมิได้สื่อโดยนัยว่า บริษัท Whatsnew ให้การรับรองหรือแนะนำข้อมูลในเว็บไซต์เหล่านั้น การเข้าเยี่ยมชมหรือใช้ข้อมูลจากเว็บไซต์อื่นๆนั้นถือเป็นหน้าที่ของคุณในการพิจารณาวิเคราะห์ความเสี่ยงที่อาจเกิดขึ้นจากการใช้ข้อมูลในเว็บไซด์เหล่านั้นด้วยตนเอง การเข้าเยี่ยมชมเว็บไซต์อื่นๆนั้น เราขอแนะนำให้คุณอ่านรายละเอียดข้อตกลงและเงื่อนไขการใช้บริการ ตลอดจนนโยบายการรักษาความปลอดภัยของเว็บไซต์ที่เชื่อมโยงนั้นก่อนเข้าใช้เว็บไซต์ดังกล่าว และเราไม่อนุญาตให้คุณทำลิงค์จากเว็บไซต์ของตนเองเพื่อเชื่อมโยงไปยังเว็บไซต์เหล่านั้นโดยมิได้มีการขออนุญาตจาก บริษัท Whatsnew เป็นลายลักษณ์อักษรเสียก่อน กรุณาติดต่อเรา หากคุณต้องการทำลิงค์เชื่อมโยงไปยังเว็บไซต์ของคุณ&nbsp;<br /><br /> <span style="font: bold 16px 'Tahoma';">5. กฎ กติกา มารยาทในการใช้งานเว็บบอร์ดและการนำเสนอข้อมูลของผู้ใช้งานเว็บไซต์ </span> <br />
<p>บริษัท Whatsnew ไม่รับผิดชอบต่อเนื้อหาใดๆที่คุณได้ทำการนำเสนอต่อสาธารณชน (ทั้งนี้รวมถึงเว็บบอร์ด หน้าข้อมูลต่างๆของเว็บไซต์ ห้องแชท หรือพื้นที่สาธารณะใดๆก็ตามที่ปรากฏอยู่ในเว็บไซต์) บริษัท Whatsnew ไม่จำเป็นต้องเห็นด้วย และ ไม่มีส่วนเกี่ยวข้องกับเนื้อหาใดๆ (ที่คุณหรือผู้ใช้งานท่านอื่นแสดงความคิดเห็นขึ้นมา) บริษัท Whatsnew ขอสงวนสิทธิ์ในการใช้ดุลยพินิจที่จะลบเนื้อหาหรือความคิดเห็นของคุณได้โดยไม่ต้องแจ้งให้คุณทราบล่วงหน้า ถ้าหากเนื้อหาหรือความคิดเห็นนั้นๆมีเจตนาหรือมีข้อสงสัยว่าเป็น</p>
<table>
<tbody>
<tr>
<td width="15">5.1</td>
<td>ข้อความหรือเนื้อหาอันเป็นการทำลายชื่อเสียง หมิ่นประมาท ส่อเสียด คุกคาม ว่ากล่าวให้ร้าย แก่ สมาชิกท่านอื่นหรือบุคคลที่ 3</td>
</tr>
<tr>
<td>5.2</td>
<td>การนำเสนอ เผยแพร่ ข้อความหรือเนื้อหาที่เป็นการนำเสนอที่ส่อไปในทางลามก อนาจารหรือขัดต่อกฎหมายใดๆ</td>
</tr>
<tr>
<td>5.3</td>
<td>การเผยแพร่หรืออัพโหลดไฟล์ข้อมูลที่มีไวรัส ไฟล์ที่ส่งผลให้เกิดการทำงานที่ไม่ปกติ หรือ ซอฟท์แวร์ หรือโปรแกรมใดๆที่อาจก่อให้เกิดความเสียหายต่อการทำงานของเว็บไซต์ของ บริษัท Whatsnew และ/หรือระบบคอมพิวเตอร์ รวมถึงระบบเครือข่ายของบุคคลที่สาม</td>
</tr>
<tr>
<td>5.4</td>
<td>ข้อความหรือเนื้อหาที่เป็นการละเมิดลิขสิทธิ์หรือเครื่องหมายการค้า ที่เป็นการผิดกฎหมายของ ประเทศไทยหรือ นานาประเทศ รวมถึงการละเมิดลิขสิทธิ์ทรัพย์สินทางปัญญาของบริษัท Whatsnew หรือบุคคลที่สาม</td>
</tr>
<tr>
<td>5.5</td>
<td>การนำเสนอข้อความหรือเนื้อหาที่เป็นการโฆษณา หรือเนื้อหาที่เป็นการประชาสัมพันธ์อันมีเจตนาเพื่อ จะกระตุ้นให้เกิดการซื้อสินค้า</td>
</tr>
</tbody>
</table>
<br /><br /> <span style="font: bold 16px 'Tahoma';">6. สมาชิกภาพ </span> <br />เว็บไซต์ของ บริษัท Whatsnew ไม่ให้บริการกับผู้ใช้งานที่มีอายุต่ำกว่า 18 ปี ซึ่งเป็นผู้ที่อยู่นอกเหนือจากกลุ่มเป้าหมายของเรา หรือสมาชิกที่เคยถูก บริษัท Whatsnew ตัดสิทธิ์การใช้งานไปแล้ว ผู้ใช้งานแต่ละท่านจะได้รับอนุญาตให้มีบัญชีผู้ใช้งานได้เพียงบัญชีเดียวเท่านั้น การละเมิดเงื่อนไขที่ได้กล่าวมาแล้วนั้นอาจส่งผลให้เกิดการระงับบัญชีผู้ใช้งานได้&nbsp;<br /> <br /> บริษัท Whatsnew อาจมีโปรโมชั่น หรือ รางวัลพิเศษให้แก่สมาชิกของเราเป็นครั้งคราว ในการรับโปรโมชั่นพิเศษดังกล่าวหากสมาชิกผู้ใช้งานท่านใดที่ได้รับรางวัล (จะเป็นของรางวัล เครดิตพิเศษ บัตรของขวัญ คูปอง หรือ ผลประโยชน์อื่นใด นอกเหนือจากนี้) จาก บริษัท Whatsnew แต่มีการสมัครสมาชิกซ้ำซ้อนเอาไว้ในหลายชื่อ หรือใช้อีเมล์แอดเดรสหลายชื่อ รวมถึง การให้ข้อมูลที่เป็นเท็จ หรือ มีการกระทำอันเป็นการหลอกลวงจะถูกตัดสิทธิในการได้รับรางวัลใดๆในกิจกรรมที่ทางเราได้จัดขึ้นและอาจจะถูกดำเนินคดีตามกฎหมายหรือมีโทษปรับตามกฎหมายอาญา&nbsp;<br /> <br /> การใช้งานเว็บไซต์ของ บริษัท Whatsnew พึงตระหนักว่าคุณต้องมีอายุครบตามที่กฎหมายกำหนดในการทำสัญญากับเราและต้องมิใช่บุคคลที่เคยถูกระงับการให้บริการตามกฎหมายของประเทศไทยหรือขอบเขตกฎหมายอื่นใด และเมื่อทาง บริษัท Whatsnew แจ้งความประสงค์ไป คุณต้องยินยอมที่จะให้ข้อมูลที่เป็นจริงและถูกต้องของตนเอง หากทาง บริษัท Whatsnew ตรวจสอบพบว่า คุณได้ให้ข้อมูลอันเป็นเท็จ ไม่ถูกต้อง หรือ ไม่ครบถ้วนสมบูรณ์ บริษัท Whatsnew มีสิทธิ์ที่จะระงับสิทธิ์ในการเป็นสมาชิกชั่วคราว ตลอดจนยกเลิกการเป็นสมาชิกของคุณ หรือปฏิเสธการสมัครสมาชิกเพื่อใช้งานของคุณอีกในอนาคต คุณจะต้องเป็นผู้รับผิดชอบแต่ผู้เดียวในการเก็บรักษาชื่อบัญชีผู้ใช้งานและรหัสผ่านของตนเองให้เป็นความลับ คุณตกลงยินยอมที่จะรับผิดชอบต่อกิจกรรมใดๆที่เกิดขึ้นภายใต้ชื่อบัญชีผู้ใช้งานของคุณเอง&nbsp;<br /><br /> <span style="font: bold 16px 'Tahoma';">7. การยกเลิกอันเกิดจากความผิดพลาด </span> <br />บริษัท Whatsnew มีสิทธิอย่างเต็มที่ที่จะยกเลิกคำสั่งซื้อได้ทุกเมื่อ หากความผิดพลาดนั้นเกิดมาจากความผิดพลาดในการพิมพ์หรือความผิดพลาดที่มิได้มีการคาดการณ์ไว้ล่วงหน้า อันเป็นผลมาจากสินค้าในเว็บไซต์ที่มีการลงรายการไว้ไม่ถูกต้อง (ไม่ว่าจะเป็นการลงราคาผิด หรือ ลงรายละเอียดที่ผิด เป็นต้น) ในกรณีที่รายการสั่งซื้อนั้นเกิดการยกเลิกขึ้นและเราได้รับการชำระเงินสำหรับคำสั่งซื้อนั้นเสร็จสมบูรณ์เรียบร้อยแล้ว บริษัท Whatsnew จะทำการคืนเงินให้ท่านเต็มจำนวนราคาสินค้า&nbsp;<br /><br /> <span style="font: bold 16px 'Tahoma';">8. การใช้งานอันไม่เหมาะสม </span> <br />คุณตกลงใจที่จะไม่ใช้เว็บไซต์ของ บริษัท Whatsnew เพื่อเป็นการส่งหรือประกาศถึงเนื้อหาหรือข้อมูลใดๆอันเป็นการขัดต่อกฎหมาย ข่มขู่ รังควาน เจตนาทำลายชื่อเสียง หมิ่นประมาท หยาบคาย คุกคาม มุ่งร้าย ลามกอนาจาร ส่อไปในทางเรื่องเพศ ก้าวร้าว ทำให้ผู้อื่นไม่พอใจ ดูหมิ่นศาสนา รวมถึงรูปภาพลามกหรือผิดกฎหมายใดๆที่บังคับใช้ ด้วยเหตุนี้หากคุณได้ทำการส่งหรือประกาศถึงเนื้อหา หรือ ข้อความ รวมถึง รูปภาพใดๆที่กล่าวมาแล้วนั้น และทาง บริษัท Whatsnew หรือ บุคคลที่สามใดๆก็ตาม ได้รับความเสียหายอันเนื่องมาจากเนื้อหาหรือข้อมูลของคุณ ไม่ว่าจะทางตรงหรือทางอ้อมก็ตามที คุณจะต้องเป็นผู้รับผิดชอบความเสียหาย รวมถึงหนี้สิน ค่าเสียหายและรายจ่ายอันเกิดขึ้นจากการที่ บริษัท Whatsnew หรือ บุคคลที่ 3 ต้องสูญเสียไป&nbsp;<br /><br /> <span style="font: bold 16px 'Tahoma';">9. การรับประกัน </span> <br /> ไม่ว่าในกรณีใดๆ บริษัท Whatsnew ไม่ขอรับประกัน รับรอง หรือ เป็นตัวแทน ต่อการนำเสนอเนื้อหาของผู้ใช้งาน หรือ ความคิดเห็น คำแนะนำ หรือคำรับประกันใดๆที่ปรากฏ (ไม่ว่าจะแจ้งไว้อย่างชัดเจนหรือบอกเป็นนัยยะทางกฎหมายก็ตามที) ในเว็บไซต์ของเรา โดยหมายรวมถึงข้อมูลส่วนตัวของคุณหรือผู้ติดตามของคุณที่มีการส่งผ่านระบบของเราด้วย <br /><br /> <span style="font: bold 16px 'Tahoma';">10. ข้อจำกัดความรับผิด </span> <br />บริษัท Whatsnew จะไม่รับผิดชอบในความเสียหายใดๆ รวมถึง ความเสียหาย สูญเสียและค่าใช้จ่ายที่เกิดขึ้นไม่ว่าโดยทางตรงหรือโดยทางอ้อม โดยเฉพาะเจาะจง โดยบังเอิญ หรือ เป็นผลสืบเนื่องที่ตามมา อาการบาดเจ็บทางร่างกาย หรือ ค่าใช้จ่ายอันเกิดขึ้นจากอาการบาดเจ็บของคุณหรือบุคคลที่สามคนใดก็ตาม (รวมถึงผู้ติดตามของคุณด้วย) รวมถึงผลลัพธ์อันเนื่องมาจากการเข้าใช้งานในเว็บไซต์ของเรา ไม่ว่าจะโดยตรงหรือโดยอ้อม ทั้งข้อมูลใดๆที่ปรากฏอยู่บนเว็บไซต์ ข้อมูลส่วนตัวของคุณหรือข้อมูลผู้ติดตามของคุณ ตลอดจนเนื้อหาและข้อมูลต่างๆที่มีการส่งผ่านระบบของเรา กล่าวให้เฉพาะเจาะจงลงไปคือ บริษัท Ardent Commerce หรือ บุคคลที่สามใดๆ หรือ ข้อมูล เนื้อหาที่มีการนำเสนอขึ้นมา จะไม่ต้องรับผิดชอบทางกฎหมายใดๆต่อคุณหรือต่อบุคคลอื่นใด ทั้งต่อบริษัทใดๆก็ตามที่ต้องประสบกับความสูญเสีย เกิดค่าใช้จ่าย ไม่ว่าจะโดยตรงหรือโดยอ้อม การบาดเจ็บทางร่างกายหรือค่าใช้จ่ายอันเกิดขึ้นจากความล่าช้า ความไม่ถูกต้อง ความผิดพลาด หรือ การเว้นว่างของข้อมูลด้านราคาที่มีการแบ่งสรรปันส่วนกัน หรือ การส่งต่อกัน หรือ การกระทำใดๆอันเกิดจากความเชื่อถือในเนื้อหา หรือ เป็นเหตุให้เกิดการไม่ประสบผลสำเร็จ หรือ การหยุดชะงัก หรือ การสิ้นสุด&nbsp;<br /> <br /> โปรแกรมแนะนำเพื่อนเพื่อรับเครดิตฟรีและโปรแกรมการรับเครดิตคืน รวมถึงโปรแกรมรับผลตอบแทนอื่นๆ ของเว็บไซต์ Moxyst.com ขึ้นอยู่กับดุลพินิจของบริษัท Whatsnew จำกัดเท่านั้น บริษัทฯ ขอสงวนสิทธิ์ในการเปลี่ยนแปลง ยกเลิกโปรแกรมทั้งเป็นการชั่วคราวหรือถาวรในทุกๆโปรแกรม ทั้งโปรแกรมแนะนำเพื่อนเพื่อรับเครดิตฟรีและโปรแกรมการรับเครดิตคืนหรือโปรแกรมรับผลตอบแทนอื่นๆ โดยไม่ต้องแจ้งสมาชิกล่วงหน้า บริษัทฯ อาจทำการเพิกถอน จำกัด เปลี่ยนแปลงเครดิต เพิ่มหรือลดจำนวนเครดิตคืน ตามความเหมาะสมของที่สอดคล้องกับโปรแกรมการแนะนำเพื่อนเพื่อรับฟรีเครดิตและโปรแกรมการรับเครดิตคืน การสมัครเป็นสมาชิก Moxyst.com ถือว่าท่านได้ยอมรับเงื่อนไขข้อตกลง ของโปรแกรมการแนะนำเพื่อนเพื่อรับฟรีเครดิต และการรับเครดิตคืน ซึ่งท่านหรือบุคคลที่สามไม่มีสิทธิ์เปลี่ยนแปลงหรือยกเลิกโปรแกรมดังกล่าวได้&nbsp;<br /><br /> <span style="font: bold 16px 'Tahoma';">11. การชดเชยค่าเสียหาย</span> <br />ผู้ใช้งานยอมรับที่จะชดเชยค่าเสียหาย และค่าใช้จ่ายอันเกิดจากการละเมิดเงื่อนไขและข้อกำหนดในการให้บริการหรือการกระทำอื่นใดที่เกี่ยวข้องกับบัญชีผู้ใช้งานของสมาชิก อันเนื่องมาจากการละเลยไม่ใส่ใจหรือการกระทำที่ผิดกฎหมาย และผู้ใช้งานจะไม่ยึดให้ บริษัท Whatsnew (หรือพนักงานของบริษัททุกคน ผู้บริหารของบริษัท ผู้ผลิตสินค้า บริษัทสาขา บริษัทร่วมทุนตลอดจนหุ้นส่วนทางกฎหมาย) ต้องรับผิดชอบต่อการเรียกร้องค่าเสียหายรวมถึงค่าใช้จ่ายของทนายความที่เกิดขึ้นด้วย&nbsp;<br /><br /> <span style="font: bold 16px 'Tahoma';">12. การใช้งานเว็บไซต์ </span> <br />บริษัท Whatsnew มิได้รับรอง รับประกัน หรือ เป็นตัวแทนของข้อมูล เนื้อหาใดๆที่ปรากฏบนหน้าเว็บไซต์ซึ่งอาจนำไปใช้เพื่อขอบเขตทางกฎหมายของประเทศอื่นใด (นอกเหนือจากประเทศไทย) เมื่อคุณเข้ามาที่เว็บไซต์ของเรา ถือว่าคุณได้รับรู้และหมายถึงการแสดงตนต่อบริษัทฯ แล้วว่า คุณมีสิทธิตามกฎหมายในการทำเช่นนั้นและให้ใช้ข้อมูลเหล่านั้นได้ในเว็บไซต์ของเรา&nbsp;<br /><br /> <span style="font: bold 16px 'Tahoma';">13. บททั่วไป </span> <br />
<table>
<tbody>
<tr>
<td width="15">13.1</td>
<td>ข้อตกลงทั้งหมด เงื่อนไขการให้บริการของเว็บไซต์ Petloft.com ทั้งหมดนี้เป็นข้อตกลงเบ็ดเสร็จและเด็ดขาดระหว่างคุณและ บริษัท Whatsnew เกี่ยวกับการที่ท่านเข้ามาใช้งานเว็บไซต์ของเรา ทั้งคุณและ บริษัท Whatsnew มิได้ถูกผูกมัด หรือแสดงนัยยะถึงการเป็นตัวแทน รับรอง หรือสัญญาใดๆที่ไม่ได้บันทึกไว้ในนี้ นอกเสียจากจะมีการเอ่ยถึงไว้อย่างเป็นพิเศษ ข้อกำหนดและเงื่อนไขในการให้บริการของเว็บไซต์ของเราเป็นฉบับปัจจุบันที่ถือเป็นฉบับล่าสุดที่ใช้แทนที่ทุกความรับผิดชอบ ภาระผูกพัน หรือ การเป็นตัวแทน ที่แล้วๆมา ไม่ว่าจะเป็นลายลักษณ์อักษรหรือปากเปล่าก็ตามที ระหว่างคุณและ บริษัท Whatsnew ที่เกี่ยวข้องกับการที่คุณใช้งานเว็บไซต์ของเรา</td>
</tr>
<tr>
<td>13.2</td>
<td>การเปลี่ยนแปลงแก้ไข บริษัท Whatsnew อาจมีการเปลี่ยนแปลง แก้ไขข้อกำหนด และ เงื่อนไขเหล่านี้ได้ทุกเวลา พึงตระหนักไว้เสมอว่า การเข้าเยี่ยมชมเว็บไซต์ของเราในแต่ละครั้ง ถือว่าคุณตกลงยินยอมต่อข้อกำหนดและเงื่อนไขที่ได้รับการแก้ไขล่าสุดแล้ว และถึงแม้ว่าการเปลี่ยนแปลงแก้ไขข้อกำหนดและเงื่อนไขครั้งล่าสุดจะมิได้มีการระบุไว้ก็ตามที แต่ให้ยึดเอาข้อกำหนดและเงื่อนไขฉบับปัจจุบันเป็นสำคัญ การเข้าเยี่ยมชมเว็บไซต์ บริษัท Whatsnew ของคุณในแต่ละครั้ง จะต้องถือเอาข้อกำหนดและเงื่อนไขล่าสุดเป็นสำคัญ และเป็นความรับผิดชอบของคุณเองที่ควรจะเยี่ยมชมหน้าดังกล่าวของเว็บไซต์เป็นระยะๆ เพื่อให้ทราบถึงเนื้อหาล่าสุดของข้อบังคับและเงื่อนไขในการใช้งานที่คุณมี</td>
</tr>
<tr>
<td>13.3</td>
<td>ความขัดแย้ง กรณีที่มีการกำหนดข้อตกลงและเงื่อนไขการใช้บริการในหน้าอื่นที่ขัดแย้งกับ ข้อตกลงและเงื่อนไขการใช้บริการนี้ ไม่ว่าจะเป็น นโยบาย หรือ ประกาศ หรือ เงื่อนไขและข้อตกลงในการใช้บริการอื่นใดก็ตาม ในส่วนที่กำหนดไว้เป็นพิเศษหรือมาตรฐานของเว็บไซต์ ให้คุณถือปฏิบัติตามข้อตกลงและเงื่อนไขการใช้บริการที่ปรากฏในหน้าอื่นนั้นอย่างเป็นกรณีพิเศษก่อน และข้อกำหนดข้อนั้นจะถือว่าแยกออกจากข้อกำหนดเหล่านี้และไม่มีผลกระทบต่อการมีผลใช้บังคับของข้อตกลงที่เหลือนี้</td>
</tr>
<tr>
<td>13.4</td>
<td>การสละสิทธิ์ ทั้งคุณหรือ บริษัท Whatsnew ไม่สามารถมอบหมาย, ถ่ายโอน, หรือให้สิทธิช่วงในสิทธิของคุณส่วนหนึ่งส่วนใดหรือทั้งหมดภาย ใต้ข้อตกลงนี้โดยปราศจากการอนุญาตเป็นลายลักษณ์อักษรล่วงหน้าก่อนได้ ยกเว้นแต่จะมีการทำคำสละสิทธิ์ออกมาเป็นลายลักษณ์อักษรชัดเจนและมีการลงนาม ของบุคคลที่มีอำนาจเพียงพอจากฝ่ายที่อนุญาตให้มีการบอกเลิกสิทธิ์</td>
</tr>
<tr>
<td>13.5</td>
<td>การถือครองสิทธิ์ บริษัท Whatsnew มีสิทธิขาดในการยกให้ โอนสิทธิ หรือ มอบหมายลิขสิทธิ์และสัญญาผูกมัดทั้งหมด ให้ตัวแทนหรือบุคคลที่สามคนอื่นใดก็ได้ จะเป็นในแง่ของเงื่อนไขการให้บริการ นโยบายและประกาศใดๆที่เกี่ยวข้องกันก็ตามที</td>
</tr>
<tr>
<td>13.6</td>
<td>การเป็นโมฆะเป็นบางส่วน ข้อกำหนดทุกประการรวมถึงเงื่อนไขในการให้บริการ นโยบาย และ ประกาศใดๆก็ตามที่เกี่ยวเนื่องกัน ไม่ว่าจะพิจารณาแบบรวมเข้าด้วยกันหรือมีการเกี่ยวโยงกันตามหลักไวยากรณ์ ประการใดก็ตามที จะมีการบังคับใช้เป็นเอกเทศ แยกเป็นอิสระจากกัน หากมีข้อกำหนดใดของเงื่อนไขในการให้บริการ นโยบาย หรือ ประกาศ ซึ่งเป็น หรือ กลายเป็น การไม่สามารถบังคับใช้ขอบเขตอำนาจในการบังคับคดีได้ ไม่ว่าจะเป็น การเป็นโมฆะ การใช้งานไม่ได้ การทำผิดกฎหมาย หรือ การไม่ชอบด้วยกฎหมาย หรือด้วยเหตุผลอื่นใดก็ตามที หรือ หากศาลหรือเขตอำนาจศาลใดเห็นว่า ข้อกำหนดเงื่อนไขของสัญญานี้ หรือ ส่วนใดส่วนหนึ่ง ของสัญญานี้ ใช้บังคับไม่ได้ ให้ใช้ข้อบังคับอื่นใดนอกเหนือจากข้อนั้นๆ เท่าที่สามารถจะใช้ได้ และ โดยให้ ส่วนที่เหลือ ของสัญญานี้มีผลบังคับใช้ได้อย่างเต็มที่</td>
</tr>
<tr>
<td>13.7</td>
<td>กฎหมายที่บังคับใช้ ข้อตกลงนี้ได้รับการควบคุมโดยกฎหมายแห่งประเทศไทย โดยไม่คำนึงถึงความขัดแย้งของหลักกฎหมาย ดังนี้เองคุณจึงเห็นชอบด้วยว่าข้อโต้แย้งหรือข้อเรียกร้องที่เกิดขึ้นจาก หรือ เกี่ยวข้องกับ ข้อตกลงนี้จะได้รับการแก้ไขด้วยศาลสูงที่ตั้งอยู่ในกรุงเทพมหานคร ประเทศไทยเท่านั้น และดังนี้เอง คุณจึงได้อนุญาตและยอมรับต่ออำนาจศาลดังกล่าวในการฟ้องร้อง ข้อโต้แย้ง หรือ ข้อเรียกร้องใดๆ อันเกิดขึ้นจากการเกี่ยวพันกับเว็บไซต์ของเรา หรือ เงื่อนไขในการให้บริการข้ออื่นๆที่เกี่ยวเนื่องกัน รวมไปถึง ประกาศ นโยบาย หรือ เนื้อหาสาระข้อมูลอื่นใดที่มีความเกี่ยวข้องหรือมีความเกี่ยวโยงกัน</td>
</tr>
<tr>
<td>13.8</td>
<td>ความคิดเห็นหรือข้อสงสัย หากคุณมีข้อสงสัย หรือ คำถามประการใดเกี่ยวกับข้อกำหนดและเงื่อนไขในการให้บริการ นโยบายความเป็นส่วนตัว หรือ เงื่อนไขการให้บริการ นโยบาย และ ประกาศ หรือแนวทางที่เราบริหารจัดการข้อมูลส่วนบุคคลของคุณก็ตาม <a href="{{store url='contacts'}}" target="_blank">กรุณาติดต่อเรา</a>ได้ทันที</td>
</tr>
</tbody>
</table>
<br /><br /> <span style="font: bold 16px 'Tahoma';">14. การสิ้นสุด </span> <br />คุณต้องปฏิบัติตามและยึดเอาเงื่อนไขในการให้บริการทั้งหมดนี้เป็นสำคัญเมื่อคุณเข้าใช้งานในเว็บไซต์ของ บริษัท Whatsnew และ/หรือเมื่อคุณทำการลงทะเบียนเพื่อเป็นสมาชิก หรือ ระหว่างการดำเนินการซื้อของคุณ ข้อกำหนดและเงื่อนไขในการให้บริการทั้งหมดนี้ หรือ แม้เพียงข้อหนึ่งข้อใดก็ตาม ทาง บริษัท Whatsnew อาจจะมีการปรับเปลี่ยน แก้ไข หรือ ตัดออก โดยไม่จำเป็นต้องมีการแจ้งให้ทราบล่วงหน้า ข้อกำหนดหรือเงื่อนไขใดๆที่เกี่ยวข้องกับ ลิขสิทธิ์ เครื่องหมายการค้า การปฏิเสธความรับผิดชอบ การอ้างสิทธิ ข้อจำกัดของความรับผิดชอบ การชดเชย กฎหมายที่นำมาบังคับใช้ การตัดสินโดยอนุญาโตตุลาการ จะยังคงมีผลอยู่อย่างสมบูรณ์ถึงแม้จะมีการยกเลิกหรือ สิ้นสุดส่วนใดส่วนหนึ่งไปก็ตามที&nbsp;<br /><br /> <span style="font: bold 16px 'Tahoma';">15. การคืนและการยกเลิกคำสั่งซื้อ </span> <br />คำสั่งซื้อสามารถจะทำการยกเลิกได้ หากคำสั่งซื้อ ของท่านยังมิได้ทำการส่งไปถึงท่าน หากเมื่อ คำสั่งซื้อได้ทำการส่งไปยังบัญชีผู้ใช้งานของท่านเรียบร้อยแล้ว ท่านจะไม่สามารถทำการยกเลิกคำสั่งซื้อนั้นได้ การขอคืนหลังสิ้นสุดการสั่งซื้อนั้นจะมีการพิจารณาบนพื้นฐานเป็นกรณีๆไป เราจะพยายามรักษาผลประโยชน์ให้สมาชิกอย่างสูงสุด ภายใต้ข้อจำกัดที่เรามี ทว่าเราไม่สามารถจะรับคืนได้ ถ้าหากว่ามิได้มีสิ่งใดผิดปกติเกิดขึ้นในการซื้อ&nbsp;<br /><br /> <span style="font: bold 16px 'Tahoma';">16. ข้อตกลงและเงื่อนไขการใช้เว็บไซต์ </span> <br />โปรแกรมแนะนำเพื่อนเพื่อรับเครดิตฟรีและโปรแกรมการรับเครดิตคืน รวมถึงโปรแกรมรับผลตอบแทนอื่นๆ ของเว็บไซต์ Moxyst.com ขึ้นอยู่กับดุลพินิจของบริษัท Whatsnew เท่านั้น บริษัทฯ ขอสงวนสิทธิ์ในการเปลี่ยนแปลง ยกเลิกโปรแกรมทั้งเป็นการชั่วคราวหรือถาวรในทุกๆโปรแกรม ทั้งโปรแกรมแนะนำเพื่อนเพื่อรับเครดิตฟรีและโปรแกรมการรับเครดิตคืนหรือโปรแกรมรับผลตอบแทนอื่นๆ โดยไม่ต้องแจ้งสมาชิกล่วงหน้า บริษัทฯ อาจทำการเพิกถอน จำกัด เปลี่ยนแปลงเครดิต เพิ่มหรือลดจำนวนเครดิตคืน ตามความเหมาะสมที่สอดคล้องกับโปรแกรมการแนะนำเพื่อนเพื่อรับฟรีเครดิตและโปรแกรมการรับเครดิตคืน การสมัครเป็นสมาชิกเว็บไซต์ Moxyst.com ถือว่าท่านได้ยอมรับเงื่อนไขข้อตกลง ของโปรแกรมการแนะนำเพื่อนเพื่อรับฟรีเครดิต และการรับเครดิตคืน ซึ่งท่านหรือบุคคลที่สามไม่มีสิทธิ์เปลี่ยนแปลงหรือยกเลิกโปรแกรมดังกล่าวได้&nbsp;<br /><br /> <span style="font: bold 16px 'Tahoma';">17. ความเป็นส่วนตัว </span> <br />
<p>ในส่วนของบริการนำส่ง เราตระหนักดีถึงภาระหน้าที่ของเราในการรักษาข้อมูลความลับทั้งปวงของลูกค้า และนอกเหนือจากภาระหน้าที่รักษาความลับต่อลูกค้า เรายังปฏิบัติตามบทบัญญัติว่าด้วยข้อมูลส่วนตัว (Personal Data (Privacy) Ordinance) ("บทบัญญัติ") ตลอดเวลา ในการรวบรวม รักษา และใช้ข้อมูลส่วนตัวของลูกค้า โดยเฉพาะอย่างยิ่งเราได้ปฏิบัติตามหลักปฏิบัติดังต่อไปนี้ เว้นแต่ในกรณีที่ลูกค้าตกลงเป็นอย่างอื่นตามความเหมาะสม</p>
<table>
<tbody>
<tr>
<td width="15">17.1</td>
<td>การรวบรวมข้อมูลส่วนตัวจากลูกค้าจะเป็นไปเพื่อวัตถุประสงค์ในการจัดหาบริการโลจิสติกส์ หรือบริการ ที่เกี่ยวข้อง</td>
</tr>
<tr>
<td>17.2</td>
<td>ในทุกขั้นตอนการปฏิบัติจะมีการดำเนินการเพื่อให้แน่ใจว่าข้อมูลส่วนตัวของลูกค้ามีความถูกต้อง และ จะไม่ถูกจัดเก็บข้อมูลไว้นานเกินความจำเป็น หรือจะถูกทำลายทิ้งตามระเบียบว่าด้วยระยะเวลาการเก็บ รักษาข้อมูลภายในของเรา</td>
</tr>
<tr>
<td>17.3</td>
<td>ข้อมูลส่วนตัวจะไม่ถูกนำไปใช้เพื่อวัตถุประสงค์อื่นใดนอกเหนือไปจากวัตถุประสงค์ ณ เวลาที่รวบรวม ข้อมูลหรือวัตถุประสงค์ที่เกี่ยวข้องโดยตรง</td>
</tr>
<tr>
<td>17.4</td>
<td>ข้อมูลส่วนตัวของลูกค้าจะได้รับการคุ้มครองจากการเข้าถึง การประมวลผล หรือการลบทิ้งที่เกิดขึ้น โดยไม่ได้รับอนุญาตหรือโดยบังเอิญ</td>
</tr>
<tr>
<td>17.5</td>
<td>ลูกค้ามีสิทธิแก้ไขข้อมูลส่วนตัวของตนซึ่งเราเก็บรักษาไว้ โดยเราจะดำเนินการกับคำร้องขอเข้าถึง หรือขอแก้ไขข้อมูลของลูกค้ารายนั้น ๆ ตามบทบัญญัติดังกล่าวข้างต้น</td>
</tr>
</tbody>
</table>
<br /><br /> <span style="font: bold 16px 'Tahoma';">18. นโยบายการชำระเงิน</span> <br />
<table border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td style="padding: 0 7px;">บัตรเครดิตวีซ่า มาสเตอร์มาสเตอร์การ์ด</td>
<td>เมื่อระบบดำเนินการแล้วเสร็จ ท่านจะได้รับ คำยืนยันการสั่งซื้อ ภายใน 24 ชั่วโมง หากการชำระเงินไม่สำเร็จระบบจะยกเลิกคำสั่งซื้อโดยอัตโนมัติ แล้วจะทำการแจ้งให้ท่านทราบ</td>
</tr>
<tr>
<td style="padding: 0 7px;">PayPal</td>
<td>ช่วยให้คุณสามารถชำระค่าสินค้าด้วยบัญชี PayPal ของคุณหรือชำระด้วยบัตรเครดิตชั้นนำต่างๆ ผ่านทางหน้าเว็บไซด์ได้ทันที โดยจะทำรายการผ่านระบบของ PayPal ทำให้การชำระเงินเป็นไปได้อย่างสะดวก รวดเร็วและปลอดภัย</td>
</tr>
<tr>
<td style="padding: 0 7px;">ชำระเงินปลายทาง (Cash on Delivery)</td>
<td>ฟรีค่าธรรมเนียมในการเก็บเงินปลายทาง</td>
</tr>
</tbody>
</table>
<br /><br />ข้อมูลส่วนตัวของลูกค้าถือเป็นความลับ และเราจะสามารถเปิดเผยข้อมูลส่วนตัวของลูกค้าได้เท่าที่ บทบัญญัติอนุญาตไว้หรือตามที่กฎหมายกำหนดเท่านั้น</div>
EOD;

    $pageRootTemplate = 'one_column';
    $pageLayoutUpdateXml = <<<EOD
EOD;


    $pageCustomLayoutUpdateXML = <<<EOD
EOD;

    $pageMetaKeywords = "";
    $pageMetaDescription = "";

    $page = Mage::getModel('cms/page')->getCollection()
        ->addStoreFilter(array($storeIdMoxyTh), $withAdmin = true)
        ->addFieldToFilter('identifier', $pageIdentifier)
        ->getFirstItem();
    if ($page->getId() == 0) {
        $page = Mage::getModel('cms/page');
    }
    else
    {
        // if exists then repair
        $page = Mage::getModel('cms/page')->load($page->getId());
    }

    $page->setTitle($pageTitle);
    $page->setIdentifier($pageIdentifier);
    $page->setStores($pageStores);
    $page->setIsActive($pageIsActive);
    $page->setUnderVersionControl($pageUnderVersionControl);
    $page->setContentHeading($pageContentHeading);
    $page->setContent($pageContent);
    $page->setRootTemplate($pageRootTemplate);
    $page->setLayoutUpdateXml($pageLayoutUpdateXml);
    $page->setCustomLayoutUpdateXml($pageCustomLayoutUpdateXML);
    $page->setMetaKeywords($pageMetaKeywords);
    $page->setMetaDescription($pageMetaDescription);
    $page->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================

    //==========================================================================
    // Privacy Policy EN Page
    //==========================================================================
    $pageTitle = "Privacy Policy";
    $pageIdentifier = "privacy-policy";
    $pageStores = array($storeIdMoxyEn);
    $pageIsActive = 1;
    $pageUnderVersionControl = 0;
    $pageContentHeading = "Privacy Policy";
    $pageContent = <<<EOD
<div style="font: normal 14px 'Tahoma'; line-height: 22px; color: #505050;"><span style="font: bold 16px 'Tahoma';">Introduction </span> <br />Moxyst.com are owned and operated by Whatsnew Inc., (herein after referred as Whatsnew) including subsidiaries and affiliates provide you, its Services (herein after referred as &ldquo;Website&rdquo;) adhere to the Privacy Policy describe herein. <br /><br /> Thank you for visiting our Website (Moxyst.com). This privacy policy tells you how we use personal information collected at this Website. Please read this privacy policy before using the Website or submitting any personal information. By using the Website, you are accepting the practices described in this privacy policy. These practices may be changed, but any changes will be posted and changes will only apply to activities and information on a going forward, not retroactive basis. You are encouraged to review the privacy policy whenever you visit the Website to make sure that you understand how any personal information you provide will be used. <br /><br /> <span style="font: bold 16px 'Tahoma';">Note: </span> <br /> The privacy practices set forth in this privacy policy are for this Website only. If you link to other Websites, please review the privacy policies posted at those sites. <br /><br /> <span style="font: bold 16px 'Tahoma';">Collection of Information</span> <br /> Whatsnew collects personally identifiable information, like names, postal addresses, email addresses, etc., when voluntarily submitted by our visitors. The information you provide is used to fulfill you specific request. This information is only used to fulfill your specific request, unless you give us permission to use it in another manner, for example to add you to one of our mailing lists. <br /><br /> <span style="font: bold 16px 'Tahoma';">Tracking (Cookie) Technology</span> <br /> The Website may use cookie and tracking technology depending on the features offered. Cookie and tracking technology are useful for gathering information such as browser type and operating system, tracking the number of visitors to the Website, and understanding how visitors use the Website. Cookies can also help customize the Website for visitors. Personal information cannot be collected via cookies and other tracking technology. However, if you previously provided personally identifiable information, cookies may be tied to such information. An aggregate cookie and tracking information may be shared with third parties. <br /><br /> <span style="font: bold 16px 'Tahoma';">Distribution of Information </span> <br /> Whatsnew may share information with governmental agencies or other companies assisting us in fraud prevention or investigation. We may do so when: (1) permitted or required by law; or, (2) trying to protect against or prevent actual or potential fraud or unauthorized transactions; or, (3) investigating fraud which has already taken place. The information is not provided to these companies for marketing purposes. <br /><br /> <span style="font: bold 16px 'Tahoma';">Commitment to Data Security </span> <br /> Your personally identifiable information is kept secure. Only authorized employees, agents and contractors (who have agreed to keep information secure and confidential) will have access to this information. All emails and newsletters from this site allow you to opt-out of further mailings. <br /><br /><strong><span style="font-size: large;">Unsubscribe Newsletter</span></strong></div>
<div style="font: normal 14px 'Tahoma'; line-height: 22px; color: #505050;">If you are not willing to receive newsletter from us, you can modify your e-mail subscriptions by,</div>
<div style="font: normal 14px 'Tahoma'; line-height: 22px; color: #505050;">- Click on a link named &ldquo;Unsubscribe&rdquo; which is embedded with every email sent by us in order to not receive future messages.</div>
<div style="font: normal 14px 'Tahoma'; line-height: 22px; color: #505050;">- Modify your e-mail subscriptions on website</div>
<div style="font: normal 14px 'Tahoma'; line-height: 22px; color: #505050;">1. Log In to your account</div>
<div style="font: normal 14px 'Tahoma'; line-height: 22px; color: #505050;">2. Select &ldquo;My Account&rdquo;</div>
<div style="font: normal 14px 'Tahoma'; line-height: 22px; color: #505050;">3. Select &ldquo;Newsletter Subscriptions&rdquo; on the menu</div>
<div style="font: normal 14px 'Tahoma'; line-height: 22px; color: #505050;">4. Uncheck &ldquo;General Subscription&rdquo; box</div>
<div style="font: normal 14px 'Tahoma'; line-height: 22px; color: #505050;">5. Save changes</div>
<div style="font: normal 14px 'Tahoma'; line-height: 22px; color: #505050;">After the unsubscribe process is completed, you will receive confirm e-mail from us.</div>
<div style="font: normal 14px 'Tahoma'; line-height: 22px; color: #505050;">&nbsp;</div>
<div style="font: normal 14px 'Tahoma'; line-height: 22px; color: #505050;">
<p><span style="font: bold 16px 'Tahoma';">Privacy Contact Information</span> <br /> If you have any questions or concerns regarding privacy at Whatsnew, please contact us at <br /> <br /> Whatsnew Co., Ltd.<br /> Sethiwan Tower - 19th Floor, <br />139 Pan Road, Silom, Bangrak, <br /> Bangkok, 10500 Thailand<br /> Tel:&nbsp;02-106-8222&nbsp;<br /> Email: <a href="mailto:support@moxyst.com">support@moxyst.com</a><br /> <br /> We reserves the right to make changes to this policy. Any changes to this policy will be posted on the Website.</p>
</div>
EOD;

    $pageRootTemplate = 'one_column';
    $pageLayoutUpdateXml = <<<EOD
EOD;


    $pageCustomLayoutUpdateXML = <<<EOD
EOD;

    $pageMetaKeywords = "";
    $pageMetaDescription = "";

    $page = Mage::getModel('cms/page')->getCollection()
        ->addStoreFilter(array($storeIdMoxyEn), $withAdmin = true)
        ->addFieldToFilter('identifier', $pageIdentifier)
        ->getFirstItem();
    if ($page->getId() == 0) {
        $page = Mage::getModel('cms/page');
    }
    else
    {
        // if exists then repair
        $page = Mage::getModel('cms/page')->load($page->getId());
    }

    $page->setTitle($pageTitle);
    $page->setIdentifier($pageIdentifier);
    $page->setStores($pageStores);
    $page->setIsActive($pageIsActive);
    $page->setUnderVersionControl($pageUnderVersionControl);
    $page->setContentHeading($pageContentHeading);
    $page->setContent($pageContent);
    $page->setRootTemplate($pageRootTemplate);
    $page->setLayoutUpdateXml($pageLayoutUpdateXml);
    $page->setCustomLayoutUpdateXml($pageCustomLayoutUpdateXML);
    $page->setMetaKeywords($pageMetaKeywords);
    $page->setMetaDescription($pageMetaDescription);
    $page->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================

    //==========================================================================
    // Privacy Policy TH Page
    //==========================================================================
    $pageTitle = "นโยบายความเป็นส่วนตัว ความปลอดภัยของข้อมูลส่วนบุคคล | Moxy Thailand";
    $pageIdentifier = "privacy-policy";
    $pageStores = array($storeIdMoxyTh);
    $pageIsActive = 1;
    $pageUnderVersionControl = 0;
    $pageContentHeading = "นโยบายความเป็นส่วนตัว ความปลอดภัยของข้อมูลส่วนบุคคล";
    $pageContent = <<<EOD
<div style="font: normal 14px 'Tahoma'; line-height: 22px; color: #505050;"><span style="font: bold 16px 'Tahoma';">บทนำ </span> <br />บริษัท Whatsnew จำกัด ผู้จัดทำเว็บไซต์ Moxyst.com (ต่อจากนี้จะเรียกว่า Whatsnew) อันรวมถึงบริษัทสาขาและบริษัทในเครือได้นำเสนอบริการให้กับคุณ (ต่อจากนี้จะเรียกว่า "เว็บไซต์") โดยเราได้ยึดเอานโยบายความเป็นส่วนตัวตามที่ได้แจ้งรายละเอียดไว้ดังต่อไปนี้.&nbsp;<br /> <br /> เราขอขอบคุณที่คุณแวะมาเยี่ยมชมเว็บไซต์ของเรา (Moxyst.com) นโยบายความเป็นส่วนตัวนี้จะอธิบายถึงวิธีที่เรารวบรวม ใช้ เปิดเผย และดำเนินการเกี่ยวกับข้อมูลที่สามารถระบุตัวตนของคุณได้ (ข้อมูลส่วนบุคคล) ซึ่งมีความเกี่ยวข้องกับการให้บริการของเราผ่านทางเว็บไซต์ กรุณาทำความเข้าใจเกี่ยวกับนโยบายความเป็นส่วนตัวนี้ก่อนจะใช้งานเว็บไซต์หรือกรอกข้อมูลส่วนตัวใดๆของคุณกับเว็บไซต์ ในการใช้งานเว็บไซต์ คุณยอมรับข้อตกลงที่แจ้งไว้ตามนโยบายความเป็นส่วนตัวนี้แล้ว นโยบายเหล่านี้อาจจะมีการเปลี่ยนแปลงได้ ทว่าเมื่อมีการเปลี่ยนแปลง เราจะแจ้งการเปลี่ยนแปลงนั้นให้คุณทราบและการเปลี่ยนแปลงจะเริ่มต้นมีผลต่อกิจกรรมและเนื้อหาข้อมูลที่จะเกิดขึ้นต่อไปในอนาคต โดยไม่มีผลย้อนหลัง ดังนั้น คุณจึงควรแวะมาตรวจดูนโยบายความเป็นส่วนตัวนี้เป็นประจำ เมื่อไรก็ตามที่คุณแวะมาที่เว็บไซต์ของเรา เพื่อให้คุณได้เข้าใจอย่างชัดเจนถึงนโยบายและแนวทางการปฏิบัติล่าสุดของเราเกี่ยวกับข้อมูลส่วนตัวของคุณที่มอบให้กับเรา&nbsp;<br /><br /> <span style="font: bold 16px 'Tahoma';">หมายเหตุ : </span> <br />นโยบายความเป็นส่วนตัวที่กำหนดขึ้นนี้สำหรับใช้ในเว็บไซต์นี้เท่านั้น หากคุณทำการเชื่อมต่อหรือลิงค์ไปยังเว็บไซต์อื่นใด กรุณาตรวจสอบนโยบายความเป็นส่วนตัวของเว็บไซต์ที่คุณแวะไปเยี่ยมชมเหล่านั้นด้วย&nbsp;<br /><br /> <span style="font: bold 16px 'Tahoma';">การเก็บรวบรวมข้อมูล </span> <br />Whatsnew จะทำการรวบรวมข้อมูลส่วนบุคคลของคุณ ซึ่งคุณมอบให้แก่เราในระหว่างที่ใช้บริการเว็บไซต์ ข้อมูลส่วนบุคคลดังกล่าวรวมถึงชื่อ ที่อยู่ หมายเลขโทรศัพท์ หรือที่อยู่อีเมล์ และอื่นๆ ข้อมูลที่คุณให้กับเรานั้นใช้เพื่อลงทะเบียนกับเว็บไซต์ของเราตามที่คุณต้องการ และข้อมูลส่วนบุคคลเหล่านี้ใช้เพื่อลงทะเบียนกับเว็บไซต์ของเราตามที่คุณต้องการเท่านั้น นอกเสียจากว่าคุณจะอนุญาตให้เราใช้ข้อมูลเหล่านั้นเพื่อจุดประสงค์อื่นใด ตัวอย่างเช่น การเพิ่มชื่อของคุณไว้ในรายชื่อเพื่อส่งข่าวสารทางอีเมล์ของเรา&nbsp;<br /><br /> <span style="font: bold 16px 'Tahoma';">เทคโนโลยีจดจำข้อมูล (คุกกี้) </span> <br />เว็บไซต์ของเราอาจใช้ คุกกี้ และ เทคโนโลยีจดจำข้อมูลมาใช้ในเว็บไซต์ ทั้งนี้ขึ้นอยู่กับลักษณะของการให้บริการที่เรานำเสนอ คุกกี้ และ เทคโนโลยีการจดจำข้อมูลนั้นใช้เพื่อประโยชน์ในการเก็บรวบรวมข้อมูล อาทิเช่น ชนิดของบราวเซอร์ที่ใช้งานและระบบปฏิบัติการ การนับจำนวนผู้ที่เข้ามาใช้งานเว็บไซต์ และ ทำความเข้าใจถึงความต้องการของผู้ใช้งานแต่ละคนในการเข้าใช้งานในเว็บไซต์ของเรา นอกเหนือจากนี้ คุกกี้ ยังสามารถช่วยให้เว็บไซต์ของเราสามารถ "ปรับเปลี่ยน" ตนเองให้เหมาะกับผู้ใช้แต่ละคนได้ด้วย การจดจำข้อมูลเกี่ยวกับการมาเยี่ยมชมเว็บไซต์ของผู้ใช้เอาไว้ ทว่า คุกกี้ หรือ เทคโนโลยีการจดจำข้อมูลใดๆ จะมิได้มีการใช้สำหรับเก็บข้อมูลส่วนตัวของคุณ แต่อย่างไรก็ตาม หากก่อนหน้านี้คุณเคยกรอกข้อมูลส่วนตัวของคุณเอาไว้แล้ว คุกกี้ อาจจะจดจำข้อมูลเหล่านั้นเอาไว้และอาจมีการแบ่งปันผลรวมทั้งหมดของ คุกกี้ และ เทคโนโลยีจดจำข้อมูล กับบุคคลที่สามได้<br /><br /> <span style="font: bold 16px 'Tahoma';">การเปิดเผยข้อมูล </span> <br />Whatsnew อาจจะมีการเปิดเผยข้อมูลส่วนบุคคลตามคำร้องขอจากเจ้าหน้าที่ผู้รักษากฎหมาย หรือตามความจำเป็นอื่นๆ เพื่อให้สอดคล้องกับกฎหมายที่บังคับใช้ หรือกับบริษัทอื่นใดที่ทำหน้าที่เป็นผู้ช่วยเราในการสอบสวน ป้องกัน หรือเพื่อการดำเนินการใดๆ ต่อการกระทำที่ผิดกฎหมาย โดยเราอาจจะมีการเปิดเผยข้อมูลดังกรณีต่อไปนี้ (1) เมื่อมีการอนุญาตและร้องขอจากเจ้าหน้าที่ผู้รักษากฎหมาย หรือ (2) เมื่อมีความพยายามจะต่อต้าน หรือ ป้องกัน การฉ้อโกง หลอกลวง หรือ การดำเนินการสั่งซื้อโดยไม่ได้รับอนุญาต อันมีความเป็นไปได้ว่าจะเกิดขึ้น หรือ (3) เมื่อมีการสืบสวนสอบสวนการฉ้อโกงที่เกิดขึ้นแล้ว แต่ข้อมูลเหล่านี้จะไม่มีการนำไปให้บริษัทอื่นใดใช้เพื่อจุดประสงค์ทางการตลาด&nbsp;<br /><br /> <span style="font: bold 16px 'Tahoma';">ความปลอดภัยของข้อมูล </span> <br />ข้อมูลส่วนตัวของคุณที่เราทำการเก็บรักษาไว้นั้นมีการเก็บรักษาไว้อย่างเหมาะสม และปลอดภัย มีก็แต่เพียงพนักงานในบริษัทของเราที่ได้รับอนุญาต ตัวแทนของเรา หรือ ผู้รับจ้างของเรา (ผู้ซึ่งตกลงแล้วว่าจะเก็บรักษาข้อมูลเหล่านี้อย่างปลอดภัยและเป็นความลับ) เท่านั้นที่สามารถเข้าถึงข้อมูลเหล่านี้ได้ อีเมล์และจดหมายข่าวทุกฉบับจากเว็บไซต์ของเราจะเปิดโอกาสให้คุณเลือกที่จะยกเลิกการรับข่าวสารในอนาคตได้&nbsp;<br /><br /> <br /><br /> <span style="font: bold 16px 'Tahoma';">วิธีการยกเลิกรับข่าวสาร </span>
<div style="font: normal 14px 'Tahoma';">หากคุณไม่ต้องการรรับข่าวสารและโปรโมชั่นจากเรา คุณสามารถกดเพื่อยกเลิกรับข้อมูลข่าวสารได้ผ่านทางช่องทางต่างๆดังนี้
<p>- ยกเลิกการรับอีเมลผ่านจดหมายข่าวที่ได้รับ สามารถทำได้โดยการคลิกที่ปุ่ม ยกเลิกรับจดหมายข่าวสาร</p>
<p>- ยกเลิกการรับอีเมลผ่านหน้าเว็บไซต์</p>
<p>1. ล็อคอินผ่านหน้าเว็บไซต์ด้วยบัญชีที่คุณต้องการยกเลิกการรับข่าวสาร</p>
<p>2. เลือกที่ &ldquo;บัญชีผู้ใช้ของฉัน&rdquo;</p>
<p>3. เลือกเมนู &ldquo;สมัครสมาชิกจดหมายข่าว&rdquo; ซึ่งจะปรากฏอยู่ทางแถบเมนูด้านซ้ายมือของหน้าจอ</p>
<p>4.นำเครื่องหมายถูกออกจากช่อง &ldquo;การสมัครสมาชิกทั่วไป&rdquo;</p>
<p>5. กดปุ่ม &ldquo;บันทึก&rdquo;</p>
<p>เมื่อการยกเลิกเสร็จสมบูรณ์แล้ว คุณจะได้รับอีเมลเพื่อยืนยันการยกเลิกรับข้อมูลข่าวสารจากเรา</p>
</div>
<span style="font: bold 16px 'Tahoma';">ติดต่อสอบถามเกี่ยวกับนโยบายความเป็นส่วนตัว</span> <br />หากคุณมีข้อสงสัย คำถาม ข้อข้องใจ หรือ ความคิดเห็นใดๆ เกี่ยวกับนโยบายความเป็นส่วนตัวของเราคุณสามารถติดต่อเราได้ตามรายละเอียดด้านล่างนี้&nbsp;<br /> <br />
<p><strong>บริษัท Whatsnew จำกัด</strong></p>
<div style="font: normal 14px 'Tahoma'; line-height: 22px; color: #505050;">อาคารเศรษฐีวรรณ ชั้น 19<br /> 139 ถนนปั้น แขวงสีลม เขตบางรัก<br /> กรุงเทพฯ ประเทศไทย 10500<br /> โทรศัพท์: 02-106-8222</div>
<div style="font: normal 14px 'Tahoma'; line-height: 22px; color: #505050;">Facebook:&nbsp;<a href="https://www.facebook.com/moxyst" target="_blank">https://www.facebook.com/moxyst</a><br />อีเมล: <a href="mailto:support@moxyst.com">support@moxyst.com</a></div>
<div style="font: normal 14px 'Tahoma'; line-height: 22px; color: #505050;">&nbsp;</div>
<p><span lang="TH">บริษัท </span><span>Whatsnew <span lang="TH">ขอสงวนสิทธิ์ในการเปลี่ยนแปลงใดๆต่อนโยบายความเป็นส่วนตัวนี้ การเปลี่ยนแปลงใดๆที่จะเกิดขึ้นกับนโยบายความเป็นส่วนตัวนี้จะมีการประกาศแจ้งเอาไว้ในเว็บไซต์ของเรา</span></span></p>
</div>
EOD;

    $pageRootTemplate = 'one_column';
    $pageLayoutUpdateXml = <<<EOD
EOD;


    $pageCustomLayoutUpdateXML = <<<EOD
EOD;

    $pageMetaKeywords = "";
    $pageMetaDescription = "";

    $page = Mage::getModel('cms/page')->getCollection()
        ->addStoreFilter(array($storeIdMoxyTh), $withAdmin = true)
        ->addFieldToFilter('identifier', $pageIdentifier)
        ->getFirstItem();
    if ($page->getId() == 0) {
        $page = Mage::getModel('cms/page');
    }
    else
    {
        // if exists then repair
        $page = Mage::getModel('cms/page')->load($page->getId());
    }

    $page->setTitle($pageTitle);
    $page->setIdentifier($pageIdentifier);
    $page->setStores($pageStores);
    $page->setIsActive($pageIsActive);
    $page->setUnderVersionControl($pageUnderVersionControl);
    $page->setContentHeading($pageContentHeading);
    $page->setContent($pageContent);
    $page->setRootTemplate($pageRootTemplate);
    $page->setLayoutUpdateXml($pageLayoutUpdateXml);
    $page->setCustomLayoutUpdateXml($pageCustomLayoutUpdateXML);
    $page->setMetaKeywords($pageMetaKeywords);
    $page->setMetaDescription($pageMetaDescription);
    $page->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================


    $installer->endSetup();
} catch (Excpetion $e) {
    Mage::logException($e);
    Mage::log("ERROR IN SETUP " . $e->getMessage());
}
