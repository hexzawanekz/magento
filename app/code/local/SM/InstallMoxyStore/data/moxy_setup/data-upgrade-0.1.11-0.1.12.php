<?php

// Base-name
$name           = 'moxy';
$engStoreName   = 'Moxy English';
$engStoreCode   = 'moxyen';
$thStoreName    = 'Moxy Thai';
$thStoreCode    = 'moxyth';
$storeIdMoxyEn  = Mage::getModel('core/store')->load($engStoreCode, 'code')->getId();
$storeIdMoxyTh  = Mage::getModel('core/store')->load($thStoreCode, 'code')->getId();
$newProductsCateId = 0;
$bestSellersCateId = 0;
try {
    $installer = $this;
    $installer->startSetup();


    //==========================================================================
    // home page Lafema en Page
    //==========================================================================
    $pageTitle = "Beauty & Cosmetic online shopping at best cheap price";
    $pageIdentifier = "home";
    $pageStores = array(13);
    $pageIsActive = 1;
    $pageUnderVersionControl = 0;
    $pageContentHeading = "";
    $pageContent = <<<EOD
<p>{{block type="core/template" name="subscription" as="subscription" template="subscription/subscribe.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="1580" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="1581" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="1582" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="1571" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="1583" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="cms/block" block_id="all-brands-slider"}}</p>
<!--<p>{{block type="cms/block" block_id="banner-bottom-homepage"}}</p>-->
<h2>&nbsp;</h2>
<div style="text-align: center;"><a title="Shop at PetLoft" onclick="_gaq.push(['_trackEvent', 'PetLoft Banner', 'Click', 'PetLoft Home Banner - Bottom',1,true])" href="http://www.petloft.com/"><img src="{{media url="wysiwyg/BottomBannersWeb/Bottombanners_petloft.jpg"}}" alt="" /></a><a title="Shop at Venbi" onclick="_gaq.push(['_trackEvent', 'Venbi Banner', 'Click', 'Venbi Home Banner - Bottom',1,true])" href="http://www.venbi.com/"><img style="padding: 0 0 0 5px;" src="{{media url="wysiwyg/BottomBannersWeb/Bottombanners_venbi.jpg"}}" alt="" /></a><a title="Shop at Sanoga" onclick="_gaq.push(['_trackEvent', 'Sanoga Banner', 'Click', 'Sanoga Home Banner - Bottom',1,true])" href="http://www.sanoga.com/"><img style="padding: 0 5px;" src="{{media url="wysiwyg/BottomBannersWeb/Bottombanners_sanoga.jpg"}}" alt="" /></a><a title="Shop at Moxy" onclick="_gaq.push(['_trackEvent', 'Moxy Banner', 'Click', 'Moxy Home Banner - Bottom',1,true])" href="http://www.moxyst.com/"><img src="{{media url="wysiwyg/BottomBannersWeb/Bottombanners_moxy.jpg"}}" alt="Shop at Moxy" /></a></div>
<h2>&nbsp;</h2>
<p><strong><span style="font-size: small;"> LAFEMA &ndash; Number on online beauty and cosmetic store. We cater over 100 world-class beauty and cosmetic brands from around the world in one website with special prices and promotions. </span></strong></p>
<p>&nbsp;</p>
<p>LAFEMA establishes the new online shopping experiences, with a wide range of product offerings including cosmetics, skin care, and cosmeceutical products for both women and men. LAFEMA deals with the brands and distributors directly so the product authenticity is guaranteed</p>
<p>&nbsp;</p>
<p><strong> All International best seller beauty and cosmetic brands in one place. </strong></p>
<p>&nbsp;</p>
<p>LAFEMA has over 100 international beauty and cosmetics brands include Urban Decay, Sleek, Clinique, CandyDoll, Catrice, Makeup Forever, Bisous Bisous, NYX, Tarte, Smasbox, Stila, Jill Stuart, Bareminerals, Sisley, Chanel, Giorgio Armani, Dior, Marc Jacobs, Tom Ford, Burberry, SKII, Shiseido, Suqqu, Nars, Lancome, Illamasqua, Laura Mercier, and many more popular brands.</p>
<p>&nbsp;</p>
<p><strong> Great service, fast delivery, secure payment, and shipping to your home. </strong></p>
<p>&nbsp;</p>
<p>Shopping at LAFEMA takes less time and energy, so our customers have more time for beauty. LAFEMA provides shipping to your home in just one click. Multiple payment methods are available such as credit card, PayPal, and cash on delivery. Your products will be delivered to your doorstep within 2-3 days. Make your shopping easier than ever.</p>
<p>{{block type="cms/block" block_id="banner_footer"}}</p>
EOD;

    $pageRootTemplate = 'one_column';
    $pageLayoutUpdateXml = <<<EOD
    <reference name="header">
    <block type="cms/block" name="slider_1">
        <!--
        The content of this block is taken from the database by its block_id.
        You can manage it in admin CMS -> Static Blocks
        -->
        <action method="setBlockId"><block_id>slider_1</block_id></action>
    </block>
    <block type="cms/block" name="banners_home">
        <!--
        The content of this block is taken from the database by its block_id.
        You can manage it in admin CMS -> Static Blocks
        -->
        <action method="setBlockId"><block_id>banners_home</block_id></action>
    </block>
</reference>
<reference name="head">
<block type="core/text" name="homepage.metadata" after="-">
<action method="addText"><text><![CDATA[<meta name="google-site-verification" content="_4n5alA2_c2rhcm4BQTl-KqLMwVGw_gyHr20g1fCalA" />]]></text></action>
</block>
</reference>
EOD;


    $pageCustomLayoutUpdateXML = <<<EOD
EOD;

    $pageMetaKeywords = "";
    $pageMetaDescription = "Lafema best shopping online with best price. Beauty and cosmetic brands from around the world with discount prices and promotional sale. Free shipping.";

    $page = Mage::getModel('cms/page')->getCollection()
        ->addStoreFilter(array(13), $withAdmin = true)
        ->addFieldToFilter('identifier', $pageIdentifier)
        ->getFirstItem();
    if ($page->getId() == 0) {
        $page = Mage::getModel('cms/page');
    }
    else
    {
        // if exists then repair
        $page = Mage::getModel('cms/page')->load($page->getId());
    }

    $page->setTitle($pageTitle);
    $page->setIdentifier($pageIdentifier);
    $page->setStores($pageStores);
    $page->setIsActive($pageIsActive);
    $page->setUnderVersionControl($pageUnderVersionControl);
    $page->setContentHeading($pageContentHeading);
    $page->setContent($pageContent);
    $page->setRootTemplate($pageRootTemplate);
    $page->setLayoutUpdateXml($pageLayoutUpdateXml);
    $page->setCustomLayoutUpdateXml($pageCustomLayoutUpdateXML);
    $page->setMetaKeywords($pageMetaKeywords);
    $page->setMetaDescription($pageMetaDescription);
    $page->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================

    //==========================================================================
    // home page Lafema th Page
    //==========================================================================
    $pageTitle = "ร้านขายเครื่องสำอางออนไลน์ ราคาถูก ส่งฟรีถึงบ้าน";
    $pageIdentifier = "home";
    $pageStores = array(15);
    $pageIsActive = 1;
    $pageUnderVersionControl = 0;
    $pageContentHeading = "";
    $pageContent = <<<EOD
<p>{{block type="core/template" name="subscription" as="subscription" template="subscription/subscribe.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="1580" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="1581" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="1582" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="1571" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="1583" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="cms/block" block_id="all-brands-slider"}}</p>
<h2>&nbsp;</h2>
<div style="text-align: center;"><a title="Shop at PetLoft" onclick="_gaq.push(['_trackEvent', 'PetLoft Banner', 'Click', 'PetLoft Home Banner - Bottom',1,true])" href="http://www.petloft.com/"><img src="{{media url="wysiwyg/BottomBannersWeb/Bottombanners_petloft.jpg"}}" alt="" /></a><a title="Shop at Venbi" onclick="_gaq.push(['_trackEvent', 'Venbi Banner', 'Click', 'Venbi Home Banner - Bottom',1,true])" href="http://www.venbi.com/"><img style="padding: 0 0 0 5px;" src="{{media url="wysiwyg/BottomBannersWeb/Bottombanners_venbi.jpg"}}" alt="" /></a><a title="Shop at Sanoga" onclick="_gaq.push(['_trackEvent', 'Sanoga Banner', 'Click', 'Sanoga Home Banner - Bottom',1,true])" href="http://www.sanoga.com/"><img style="padding: 0 5px;" src="{{media url="wysiwyg/BottomBannersWeb/Bottombanners_sanoga.jpg"}}" alt="" /></a><a title="Shop at Moxy" onclick="_gaq.push(['_trackEvent', 'Moxy Banner', 'Click', 'Moxy Home Banner - Bottom',1,true])" href="http://www.moxyst.com/"><img src="{{media url="wysiwyg/BottomBannersWeb/Bottombanners_moxy.jpg"}}" alt="Shop at Moxy" /></a></div>
<h2>&nbsp;</h2>
<h2><strong><span style="font-size: small;"> LAFEMA เว็บไซต์ช้อปปิ้งออนไลน์ที่รวบรวมเครื่องสำอางและผลิตภัณฑ์ดูแลผิว จากแบรนด์ดังทั่วโลกมากกว่า 100 แบรนด์ พร้อมโปรโมชั่นสุดพิเศษ </span></strong></h2>
<p>&nbsp;</p>
<p>LAFEMA สโตร์ออนไลน์อันดับหนึ่ง ที่คัดสรรผลิตภัณฑ์คุณภาพชั้นนำจากทั่วโลก สำหรับผู้ที่หลงใหล คลั่งไคล้ <strong>เครื่องสำอาง</strong> ผลิตภัณฑ์ดูแลผิวทั่วเรือนร่าง ตั้งแต่ผลิตภัณฑ์ความงาม ไปจนถึง<strong>ผลิตภัณฑ์เวชสำอาง</strong> สำหรับคุณผู้หญิงและคุณผู้ชาย คุณมั่นใจได้ว่าสินค้าทุกชิ้นเป็น<strong>ของแท้ 100%</strong> โดยทางเราติดต่อกับทางแบรนด์ และตัวแทนจำหน่ายโดยตรง เพื่อให้คุณมั่นใจในคุณภาพและความปลอดภัย</p>
<p>&nbsp;</p>
<p><strong> หลากหลายแบรนด์ชั้นนำ มีคุณภาพที่ขายดีทั่วโลก</strong></p>
<p>&nbsp;</p>
<p>LAFEMA ผู้นำด้านการ<strong>ช้อปปิ้งออนไลน์</strong>ในประเทศไทย ภูมิใจเสนอสินค้าเพื่อความสวยความงามจาก<strong>แบรนด์ดังทั่วโลก</strong> อาทิ Urban Decay, Sleek, Makeup Forever, Tarte, Smasbox, Stila, Jill Stuart, Bareminerals, Sisley, Chanel, Giorgio Armani, Dior, Marc Jacobs, Tom Ford, Burberry, SKII, Shiseido, Suqqu, Nars, Lancome, Laura Mercier และอื่นๆ อีกมากมายที่ขายดี และได้รับความนิยม</p>
<p>&nbsp;</p>
<p><strong> สะดวก รวดเร็ว บริการประทับใจ</strong></p>
<p>&nbsp;</p>
<p>เรามีบริการจัดส่งโดยที่คุณไม่ต้องเสียเวลาเดินทางหาซื้อให้ยุ่งยาก เพียงคลิกซื้อสินค้าในหน้าเว็บไซต์ แล้วเลือกชำระเงินด้วยวิธีที่ต้องการ เช่น ชำระผ่านบัตรเครดิต Paypal หรือ<strong>ชำระเงินปลายทาง</strong>กับพนักงานจัดส่งสินค้า ด้วยระบบขนส่งที่รวดเร็ว ทันใจ ส่งตรงถึงหน้าบ้านให้คุณได้รับสินค้าถึงมือภายในเวลา 2-3 วันทำการ</p>
<p>{{block type="cms/block" block_id="banner_footer"}}</p>
EOD;

    $pageRootTemplate = 'one_column';
    $pageLayoutUpdateXml = <<<EOD
<reference name="header">
    <block type="cms/block" name="slider_1">
        <!--
        The content of this block is taken from the database by its block_id.
        You can manage it in admin CMS -> Static Blocks
        -->
        <action method="setBlockId"><block_id>slider_1</block_id></action>
    </block>
    <block type="cms/block" name="banners_home">
        <!--
        The content of this block is taken from the database by its block_id.
        You can manage it in admin CMS -> Static Blocks
        -->
        <action method="setBlockId"><block_id>banners_home</block_id></action>
    </block>
</reference>
<reference name="head">
<block type="core/text" name="homepage.metadata" after="-">
<action method="addText"><text><![CDATA[<meta name="google-site-verification" content="_4n5alA2_c2rhcm4BQTl-KqLMwVGw_gyHr20g1fCalA" />]]></text></action>
</block>
</reference>
EOD;


    $pageCustomLayoutUpdateXML = <<<EOD
EOD;

    $pageMetaKeywords = "";
    $pageMetaDescription = "ลาฟีมา ขายเครื่องสำอาง ครีมบำรุงผิวหน้า ผิวกาย และผลิตภัณฑ์ความงามออนไลน์ราคาถูก เพื่อผู้หญิงโดยเฉพาะ บริการจัดส่งฟรีถึงบ้าน พร้อมโปรโมชั่นราคาพิเศษ";

    $page = Mage::getModel('cms/page')->getCollection()
        ->addStoreFilter(array(15), $withAdmin = true)
        ->addFieldToFilter('identifier', $pageIdentifier)
        ->getFirstItem();
    if ($page->getId() == 0) {
        $page = Mage::getModel('cms/page');
    }
    else
    {
        // if exists then repair
        $page = Mage::getModel('cms/page')->load($page->getId());
    }

    $page->setTitle($pageTitle);
    $page->setIdentifier($pageIdentifier);
    $page->setStores($pageStores);
    $page->setIsActive($pageIsActive);
    $page->setUnderVersionControl($pageUnderVersionControl);
    $page->setContentHeading($pageContentHeading);
    $page->setContent($pageContent);
    $page->setRootTemplate($pageRootTemplate);
    $page->setLayoutUpdateXml($pageLayoutUpdateXml);
    $page->setCustomLayoutUpdateXml($pageCustomLayoutUpdateXML);
    $page->setMetaKeywords($pageMetaKeywords);
    $page->setMetaDescription($pageMetaDescription);
    $page->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================

    //==========================================================================
    // home page Sanoga en Page
    //==========================================================================
    $pageTitle = "Health and beauty product online shopping at cheap price";
    $pageIdentifier = "home";
    $pageStores = array(10);
    $pageIsActive = 1;
    $pageUnderVersionControl = 0;
    $pageContentHeading = "";
    $pageContent = <<<EOD
<p>{{block type="core/template" name="subscription" as="subscription" template="subscription/subscribe.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="1576" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="1577" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="1579" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="682" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="681" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="cms/block" block_id="all-brands-slider"}}</p>
<h2>&nbsp;</h2>
<div style="text-align: center;"><a title="Shop at PetLoft" onclick="_gaq.push(['_trackEvent', 'PetLoft Banner', 'Click', 'PetLoft Home Banner - Bottom',1,true])" href="http://www.petloft.com/"><img src="{{media url="wysiwyg/BottomBannersWeb/Bottombanners_petloft.jpg"}}" alt="Shop pet products at PetLoft" /></a><a title="Shop at Venbi" onclick="_gaq.push(['_trackEvent', 'Venbi Banner', 'Click', 'Venbi Home Banner - Bottom',1,true])" href="http://www.venbi.com/"><img style="padding: 0 0 0 5px;" src="{{media url="wysiwyg/BottomBannersWeb/Bottombanners_venbi.jpg"}}" alt="Shop baby and mom products at Venbi" /></a><a title="Shop at Lafema" onclick="_gaq.push(['_trackEvent', 'Lafema Banner', 'Click', 'Lafema Home Banner - Bottom',1,true])" href="http://www.lafema.com/"><img style="padding: 0 5px;" src="{{media url="wysiwyg/BottomBannersWeb/Bottombanners_lafema.jpg"}}" alt="Shop cosmetics at Lafema" /></a><a title="Shop at Moxy" onclick="_gaq.push(['_trackEvent', 'Moxy Banner', 'Click', 'Moxy Home Banner - Bottom',1,true])" href="http://www.moxyst.com/"><img src="{{media url="wysiwyg/BottomBannersWeb/Bottombanners_moxy.jpg"}}" alt="Shop at Moxy" /></a></div>
<h2>&nbsp;</h2>
<h2>SANOGA &ndash; Thailand&rsquo;s number 1 online HEALTH store</h2>
<p>&nbsp;</p>
<p>Welcome to Thailand&rsquo;s largest <strong>online health store</strong>. No more looking around for <strong>the best deal</strong>, SANOGA is a one-stop health store where you can find 100% <strong>genuine products</strong> from household names. It is cheaper, easier and you can select what&rsquo;s best for you 24/7 with just one click!</p>
<p>&nbsp;</p>
<p>SANOGA offers over 1,000 Health products from well-known brands including <strong>Health Care</strong>, <strong>Vitamins</strong>, <strong>Collagen</strong>, <strong>Beauty</strong>, <strong>Sexual Well-being</strong>, <strong>Workout &amp; Fitness</strong>, <strong>Weight Loss</strong> including Blackmores, Beauty Wise, Body Shape, CLG500, Centrum, Collagen Star, Colly, Dr.Absolute, DYMATIZE, Durex, Eucerin, Genesis, Hi-Balanz, K-Palette, Meiji, Mega We Care, Okamoto, ProFlex, Real Elixir, Sebamed, Seoul Secret, Taurus, etc.</p>
<p>&nbsp;</p>
<p>All products on SANOGA is certified by FDA Thailand so you can trust in quality and safety.</p>
<p>&nbsp;</p>
<p>Enjoy shopping worry-free with our safe and convenient payment methods including credit card, Paypal, or <strong>Cash-on-Delivery</strong>. So no more running around, all you need to do is click to order and wait for the delivery to turn up at your doorstep in just 2-3 days. It&rsquo;s easier, cheaper and more convenient!</p>
EOD;

    $pageRootTemplate = 'one_column';
    $pageLayoutUpdateXml = <<<EOD
    <reference name="header">
    <block type="cms/block" name="slider_1">
        <!--
        The content of this block is taken from the database by its block_id.
        You can manage it in admin CMS -> Static Blocks
        -->
        <action method="setBlockId"><block_id>slider_1</block_id></action>
    </block>
    <block type="cms/block" name="banners_home">
        <!--
        The content of this block is taken from the database by its block_id.
        You can manage it in admin CMS -> Static Blocks
        -->
        <action method="setBlockId"><block_id>banners_home</block_id></action>
    </block>
</reference>
<reference name="head">
<block type="core/text" name="homepage.metadata" after="-">
<action method="addText"><text><![CDATA[<meta name="google-site-verification" content="_4n5alA2_c2rhcm4BQTl-KqLMwVGw_gyHr20g1fCalA" />]]></text></action>
</block>
</reference>
EOD;


    $pageCustomLayoutUpdateXML = <<<EOD
EOD;

    $pageMetaKeywords = "";
    $pageMetaDescription = "Buy Dietary Supplements, Vitamins, Collagen and Cosmetics online at Sanoga. Discount prices and promotional sale on all. Free Shipping. Cash on delivery.";

    $page = Mage::getModel('cms/page')->getCollection()
        ->addStoreFilter(array(10), $withAdmin = true)
        ->addFieldToFilter('identifier', $pageIdentifier)
        ->getFirstItem();
    if ($page->getId() == 0) {
        $page = Mage::getModel('cms/page');
    }
    else
    {
        // if exists then repair
        $page = Mage::getModel('cms/page')->load($page->getId());
    }

    $page->setTitle($pageTitle);
    $page->setIdentifier($pageIdentifier);
    $page->setStores($pageStores);
    $page->setIsActive($pageIsActive);
    $page->setUnderVersionControl($pageUnderVersionControl);
    $page->setContentHeading($pageContentHeading);
    $page->setContent($pageContent);
    $page->setRootTemplate($pageRootTemplate);
    $page->setLayoutUpdateXml($pageLayoutUpdateXml);
    $page->setCustomLayoutUpdateXml($pageCustomLayoutUpdateXML);
    $page->setMetaKeywords($pageMetaKeywords);
    $page->setMetaDescription($pageMetaDescription);
    $page->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================

    //==========================================================================
    // home page Sanoga th Page
    //==========================================================================
    $pageTitle = "อาหารเสริม วิตามิน คอลลาเจน เพื่อสุขภาพและความงาม";
    $pageIdentifier = "home";
    $pageStores = array(11);
    $pageIsActive = 1;
    $pageUnderVersionControl = 0;
    $pageContentHeading = "";
    $pageContent = <<<EOD
<p>{{block type="core/template" name="subscription" as="subscription" template="subscription/subscribe.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="1576" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="1577" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="1579" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="682" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="681" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="cms/block" block_id="all-brands-slider"}}</p>
<!-- <p>{{block type="cms/block" block_id="seo-keyword"}}</p> -->
<h2>&nbsp;</h2>
<div style="text-align: center;"><a title="Shop at PetLoft" onclick="_gaq.push(['_trackEvent', 'PetLoft Banner', 'Click', 'PetLoft Home Banner - Bottom',1,true])" href="http://www.petloft.com/"><img src="{{media url="wysiwyg/BottomBannersWeb/Bottombanners_petloft.jpg"}}" alt="Shop pet products at PetLoft" /></a><a title="Shop at Venbi" onclick="_gaq.push(['_trackEvent', 'Venbi Banner', 'Click', 'Venbi Home Banner - Bottom',1,true])" href="http://www.venbi.com/"><img style="padding: 0 0 0 5px;" src="{{media url="wysiwyg/BottomBannersWeb/Bottombanners_venbi.jpg"}}" alt="Shop baby and mom products at Venbi" /></a><a title="Shop at Lafema" onclick="_gaq.push(['_trackEvent', 'Lafema Banner', 'Click', 'Lafema Home Banner - Bottom',1,true])" href="http://www.lafema.com/"><img style="padding: 0 5px;" src="{{media url="wysiwyg/BottomBannersWeb/Bottombanners_lafema.jpg"}}" alt="Shop cosmetics at Lafema" /></a><a title="Shop at Moxy" onclick="_gaq.push(['_trackEvent', 'Moxy Banner', 'Click', 'Moxy Home Banner - Bottom',1,true])" href="http://www.moxyst.com/"><img src="{{media url="wysiwyg/BottomBannersWeb/Bottombanners_moxy.jpg"}}" alt="Shop at Moxy" /></a></div>
<h2>&nbsp;</h2>
<p><strong><span style="font-size: small;">SANOGA &ndash;&nbsp;สินค้าเพื่อสุขภาพและความงาม อาหารเสริม วิตามิน เครื่องสำอาง พร้อมโปรโมชั่นสุดพิเศษ</span></strong></p>
<p><strong><span style="font-size: small;"><br /></span></strong></p>
<p>SANOGA คือศูนย์รวมผลิตภัณฑ์และจัดจำหน่ายสินค้าด้าน<strong>สุขภาพและความงาม</strong> ที่ใหญ่ที่สุดในประเทศไทย ทั้ง<strong>อาหารเสริม</strong><strong> วิตามิน </strong><strong>คอลลาเจน</strong><strong> ผลิตภัณฑ์เพื่อ</strong><strong>ความงามและการดูแลสุขภาพ</strong><strong> ผลิตภัณฑ์</strong><strong>เสริมสมรรถภาพร่างกาย</strong><strong> </strong>และ<strong>เครื่องสำอาง</strong> คุณภาพแท้ 100% จากแบรนด์ชั้นนำทั่วโลก <strong>ในราคาที่ถูกที่สุด</strong> คุณสามารถใช้เวลาได้อย่างเต็มที่ในการเลือกผลิตภัณฑ์ที่ต้องการ เราได้จัดทำข้อมูลอย่างละเอียดสำหรับสินค้าแต่ละชนิด พร้อมทั้งวิธีการสั่งซื้อที่สะดวกสบายตลอด 24 ชั่วโมง ทำให้การดูแลสุขภาพของคุณครั้งนี้ง่ายขึ้นกว่าเดิม</p>
<p><strong><br /></strong></p>
<p><strong>หลากหลายแบรนด์ชั้นนำที่ขายดี และได้รับความนิยม</strong></p>
<p>&nbsp;</p>
<p>SANOGA ผู้นำด้านการ<strong>ช้อปปิ้งออนไลน์</strong>ในประเทศไทย<strong> </strong>ภูมิใจเสนอสินค้าเพื่อ<strong>สุขภาพและความงาม</strong>ภายใต้แบรนด์ดังระดับโลกกว่า 1,000 รายการ อาทิ BLACKMORES, Beauty Wise, Body Shape, Brand's, CLG 500, Centrum, Collagen Star, Colly, Dr.Absolute, DYMATIZE, Durex, Eucerin, Genesis, Hi-Balanz, K-Palette, Meiji, Mega We Care, Okamoto, ProFlex, Real Elixir, Sebamed, Seoul Secret, Taurus และอื่นๆ อีกมากมาย</p>
<p><strong><br /></strong></p>
<p><strong>มั่นใจได้ในคุณภาพและความปลอดภัย</strong><strong></strong></p>
<p>&nbsp;</p>
<p>สินค้าทุกชิ้นของ SANOGA ได้รับมาตรฐานการรับรองจากองค์การอาหารและยาแห่งประเทศไทย (อย.) จึงมั่นใจได้ใน<strong>คุณภาพ</strong>และ<strong>ความปลอดภัย</strong></p>
<p><strong><br /></strong></p>
<p><strong>สะดวก รวดเร็ว ทันใจ</strong><strong></strong></p>
<p>&nbsp;</p>
<p>เรามีบริการจัดส่งโดยที่คุณไม่ต้องเสียเวลาเดินหาซื้อและขนสินค้ากลับเองให้เมื่อยอีกต่อไป เพียงแค่เลือกสินค้าในเว็บไซต์ เลือก<a href="http://www.sanoga.com/th/help/#payments">การชำระเงิน</a>ด้วยวีธีที่ต้องการ เช่น ชำระผ่านบัตรเครดิต หรือ<strong>ชำระเงินปลายทาง</strong>กับพนักงานจัดส่งสินค้า เมื่อเลือกเสร็จแล้ว ก็รอรับสินค้าที่<strong>ส่งตรงถึงหน้าบ้าน</strong>ภายใน 1-2 วัน ได้เลย</p>
<p>&nbsp;</p>
<p>{{block type="cms/block" block_id="banner_footer"}}</p>
EOD;

    $pageRootTemplate = 'one_column';
    $pageLayoutUpdateXml = <<<EOD
<reference name="header">
    <block type="cms/block" name="slider_1">
        <!--
        The content of this block is taken from the database by its block_id.
        You can manage it in admin CMS -> Static Blocks
        -->
        <action method="setBlockId"><block_id>slider_1</block_id></action>
    </block>
    <block type="cms/block" name="banners_home">
        <!--
        The content of this block is taken from the database by its block_id.
        You can manage it in admin CMS -> Static Blocks
        -->
        <action method="setBlockId"><block_id>banners_home</block_id></action>
    </block>
</reference>
<reference name="head">
<block type="core/text" name="homepage.metadata" after="-">
<action method="addText"><text><![CDATA[<meta name="google-site-verification" content="_4n5alA2_c2rhcm4BQTl-KqLMwVGw_gyHr20g1fCalA" />]]></text></action>
</block>
</reference>
EOD;


    $pageCustomLayoutUpdateXML = <<<EOD
EOD;

    $pageMetaKeywords = "";
    $pageMetaDescription = "ซาโนก้า ร้านค้าออนไลน์ราคาถูก ขายอาหารเสริม วิตามิน คอลลาเจน และผลิตภัณฑ์ความงาม บริการจัดส่งฟรีถึงบ้าน ราคาถูก พร้อมโปรโมชั่น";

    $page = Mage::getModel('cms/page')->getCollection()
        ->addStoreFilter(array(11), $withAdmin = true)
        ->addFieldToFilter('identifier', $pageIdentifier)
        ->getFirstItem();
    if ($page->getId() == 0) {
        $page = Mage::getModel('cms/page');
    }
    else
    {
        // if exists then repair
        $page = Mage::getModel('cms/page')->load($page->getId());
    }

    $page->setTitle($pageTitle);
    $page->setIdentifier($pageIdentifier);
    $page->setStores($pageStores);
    $page->setIsActive($pageIsActive);
    $page->setUnderVersionControl($pageUnderVersionControl);
    $page->setContentHeading($pageContentHeading);
    $page->setContent($pageContent);
    $page->setRootTemplate($pageRootTemplate);
    $page->setLayoutUpdateXml($pageLayoutUpdateXml);
    $page->setCustomLayoutUpdateXml($pageCustomLayoutUpdateXML);
    $page->setMetaKeywords($pageMetaKeywords);
    $page->setMetaDescription($pageMetaDescription);
    $page->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================


    //==========================================================================
    // SEO Keyword Venbi EN block
    //==========================================================================
    $blockTitle = "SEO Keyword";
    $blockIdentifier = "seo-keyword";
    $blockStores = array(5);
    $blockIsActive = 1;
    $blockContent = <<<EOD
<div style="margin: 50px auto 30px;">
<div style="text-align: center;"><a title="Shop at PetLoft" onclick="_gaq.push(['_trackEvent', 'PetLoft Banner', 'Click', 'PetLoft Home Banner - Bottom',1,true])" href="http://www.petloft.com/"><img src="{{media url="wysiwyg/BottomBannersWeb/Bottombanners_petloft.jpg"}}" alt="Shop pet products at PetLoft" /></a><a title="Shop at Sanoga" onclick="_gaq.push(['_trackEvent', 'Sanoga Banner', 'Click', 'Sanoga Home Banner - Bottom',1,true])" href="http://www.sanoga.com/"><img style="padding: 0 0 0 5px;" src="{{media url="wysiwyg/BottomBannersWeb/Bottombanners_sanoga.jpg"}}" alt="" /></a><a title="Shop at Lafema" onclick="_gaq.push(['_trackEvent', 'Lafema Banner', 'Click', 'Lafema Home Banner - Bottom',1,true])" href="http://www.lafema.com/"><img style="padding: 0 5px;" src="{{media url="wysiwyg/BottomBannersWeb/Bottombanners_lafema.jpg"}}" alt="Shop cosmetics at Lafema" /></a><a title="Shop at Moxy" onclick="_gaq.push(['_trackEvent', 'Moxy Banner', 'Click', 'Moxy Home Banner - Bottom',1,true])" href="http://www.moxyst.com/"><img src="{{media url="wysiwyg/BottomBannersWeb/Bottombanners_moxy.jpg"}}" alt="Shop at Moxy" /></a></div>
<h2>&nbsp;</h2>
<h2>Venbi &ndash; Thailand&rsquo;s number 1 online BABY PRODUCTS store</h2>
<p>&nbsp;</p>
<p>Welcome to the number one online baby products store in Thailand. Shopping at Venbi means that you will no longer have to drive around for hours looking for the best products for your babies. Everything can now be purchased from one place. Venbi aims to be the one stop baby shop offering great service, fast delivery and multiple easy payment methods. We sell baby products for age groups between infancy up to 2 years of age. From Venbi, you can get your <strong><a href="{{store url='diapers.html'}}">diapers</a> , <a href="{{store url='baby-care/type/baby-wipes.html'}}">wipes</a> , <a href="{{store url='clothing-46/clothing/clothing.html'}}">cloths</a> , <a href="{{store url='catalogsearch/result' _query='q=bath'}}">bathing products</a> , <a href="{{store url='toys.html'}}">toys</a> , accessories for moms, <a href="{{store url='formula-feeding/formula/formula-baby.html'}}">powdered milk</a> ,</strong> and many other products. Venbi currently stocks over 40 brands of products with over 700 SKUs. Some of our best selling brands include <strong><a href="{{store url='diapers/popular-brands/mamy-poko.html'}}">Mamy Poko</a> , <a href="{{store url='diapers/popular-brands/merries.html'}}">Merries</a> , <a href="{{store url='formula-feeding/popular-brands/enfa.html'}}">Enfalac</a> , <a href="{{store url='catalogsearch/result' _query='q=enfa+pro'}}">Enfapro</a> , <a href="{{store url='diapers/popular-brands/drypers.html'}}">Drypers</a> , <a href="{{store url='diapers/popular-brands/huggies.html'}}">Huggies</a> , <a href="{{store url='formula-feeding/popular-brands/dg.html'}}">DG</a> , <a href="{{store url='formula-feeding/popular-brands/peachy.html'}}">Peachy</a> , <a href="{{store url='catalogsearch/result' _query='q=badger'}}">Badger</a> , <a href="{{store url='diapers/popular-brands/baby-love.html'}}">Babylove</a> , <a href="{{store url='toys/popular-brands/sassy.html'}}">Sassy</a> , <a href="{{store url='formula-feeding/popular-brands/similac.html'}}">Similac</a> , <a href="{{store url='baby-care/popular-brands/pigeon.html'}}">Pigeon</a> , <a href="{{store url='formula-feeding/popular-brands/s-26.html'}}">S-26</a> , <a href="{{store url='toys/popular-brands/bright-starts.html'}}">Bright Starts</a> , <a href="{{store url='toys/popular-brands/tiny-love.html'}}">Tiny love</a></strong> &nbsp;and many more.</p>
<p>&nbsp;</p>
<p>Shopping at Venbi is fun, easy, cheaper, and best of all <strong>SHIPPING.</strong> You no longer have to carry heavy cartons of diapers or formula or leave the house and your child to go purchase these products. From now on, all you have to do is come to Venbi and order your products here and in 1-2 days, it will arrive at your door <strong>FUSS FREE!</strong> With multiple payment methods such as credit card, paypal, cash on delivery and bank transfers, shopping online has never been easier.</p>
</div>
EOD;
    $block = Mage::getModel('cms/block')->getCollection()
        ->addStoreFilter(array(5), $withAdmin = false)
        ->addFieldToFilter('identifier', $blockIdentifier)
        ->getFirstItem();
    if ($block->getId() == 0) {
        $block = Mage::getModel('cms/block');
    } else { // if exists then delete
        $block->delete();
        $block = Mage::getModel('cms/block');
    }
    $block->setTitle($blockTitle);
    $block->setIdentifier($blockIdentifier);
    $block->setStores($blockStores);
    $block->setIsActive($blockIsActive);
    $block->setContent($blockContent);
    $block->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================

    //==========================================================================
    // SEO Keyword Venbi th block
    //==========================================================================
    $blockTitle = "SEO Keyword";
    $blockIdentifier = "seo-keyword";
    $blockStores = array(4);
    $blockIsActive = 1;
    $blockContent = <<<EOD
<div style="margin: 50px auto 30px;">
<div style="text-align: center;"><a title="Shop at PetLoft" onclick="_gaq.push(['_trackEvent', 'PetLoft Banner', 'Click', 'PetLoft Home Banner - Bottom',1,true])" href="http://www.petloft.com/"><img src="{{media url="wysiwyg/BottomBannersWeb/Bottombanners_petloft.jpg"}}" alt="Shop pet products at PetLoft" /></a><a title="Shop at Sanoga" onclick="_gaq.push(['_trackEvent', 'Sanoga Banner', 'Click', 'Sanoga Home Banner - Bottom',1,true])" href="http://www.sanoga.com/"><img style="padding: 0 0 0 5px;" src="{{media url="wysiwyg/BottomBannersWeb/Bottombanners_sanoga.jpg"}}" alt="" /></a><a title="Shop at Lafema" onclick="_gaq.push(['_trackEvent', 'Lafema Banner', 'Click', 'Lafema Home Banner - Bottom',1,true])" href="http://www.lafema.com/"><img style="padding: 0 5px;"  src="{{media url="wysiwyg/BottomBannersWeb/Bottombanners_lafema.jpg"}}" alt="Shop cosmetics at Lafema" /></a><a title="Shop at Moxy" onclick="_gaq.push(['_trackEvent', 'Moxy Banner', 'Click', 'Moxy Home Banner - Bottom',1,true])" href="http://www.moxyst.com/"><img src="{{media url="wysiwyg/BottomBannersWeb/Bottombanners_moxy.jpg"}}" alt="Shop at Moxy" /></a></div>
<h2>&nbsp;</h2>
<h2>Venbi.com ร้านขายสินค้าเด็กและของใช้เด็กออนไลน์ อันดับ 1 ในประเทศไทย</h2>
<p>&nbsp;</p>
<p>ยินดีต้อนรับสู่ ร้านขายสินค้า<strong>ของใช้แม่และเด็กออนไลน์</strong> อันดับ 1&lrm; ของเมืองไทย เราเป็นศูนย์รวมผลิตภัณฑ์สำหรับคุณแม่และเด็ก คุณภาพระดับพรีเมี่ยมใน<strong>ราคาถูก </strong>รวมไปถึง<a href="http://www.venbi.com/th/deals.html">โปรโมชั่นโดนๆ</a> ที่รับรองว่าจะต้องถูกใจคุณแม่มือใหม่ ให้ได้เลือกซื้อตามที่ต้องการ พร้อมการให้บริการที่ดีเยี่ยม ด้วยการ<strong>จัดส่งสินค้าที่รวดเร็ว</strong> และการชำระเงินค่าสินค้าที่ง่ายดายหลากหลายช่องทาง</p>
<p>&nbsp;</p>
<p>ที่ Venbi เราจำหน่ายสินค้าสำหรับแม่และเด็กมากมาย ไม่ว่าจะเป็น <a href="http://www.venbi.com/th/diapers.html/">ผ้าอ้อมสําเร็จรูป</a><strong> </strong><a href="http://www.venbi.com/th/clothing.html">เสื้อผ้าเด็ก</a><strong> </strong><a href="http://www.venbi.com/th/toys.html/">ของเล่นเด็ก</a><strong> </strong><a href="http://www.venbi.com/th/clothing/clothing/shoes.html">รองเท้าเด็ก</a><strong> </strong><a href="http://www.venbi.com/th/gear/type/strollers.html/">รถเข็นเด็ก</a><strong> </strong><a href="http://www.venbi.com/th/formula-feeding/formula/baby-food.html/">อาหารเสริมเด็ก</a><strong> </strong><a href="http://www.venbi.com/th/catalogsearch/result/?q=%E0%B8%82%E0%B8%AD%E0%B8%87%E0%B9%83%E0%B8%8A%E0%B9%89%E0%B9%80%E0%B8%94%E0%B9%87%E0%B8%81">ของใช้เด็กอ่อน</a><strong> </strong><a href="http://www.venbi.com/th/catalogsearch/result/?q=%E0%B9%80%E0%B8%94%E0%B9%87%E0%B8%81%E0%B9%81%E0%B8%A3%E0%B8%81%E0%B9%80%E0%B8%81%E0%B8%B4%E0%B8%94">ของใช้เด็กแรกเกิด</a><strong> </strong><a href="http://www.venbi.com/th/gear/type/baby-carriers.html/">เป้อุ้มเด็ก</a><strong> </strong><a href="http://www.venbi.com/th/formula-feeding/feeding-nursing/bottles-cups-and-teats.html/">ขวดนม</a><strong> </strong><a href="http://www.venbi.com/th/formula-feeding/formula/formula-baby.html/">นมผง</a><strong> </strong><a href="http://www.venbi.com/th/catalogsearch/result/?q=%E0%B8%AA%E0%B8%9A%E0%B8%B9%E0%B9%88">สบู่</a><strong> </strong>และ<a href="http://www.venbi.com/th/catalogsearch/result/?q=%E0%B9%81%E0%B8%8A%E0%B8%A1%E0%B8%9E%E0%B8%B9%E0%B9%80%E0%B8%94%E0%B9%87%E0%B8%81">แชมพูเด็ก</a> เป็นต้น สินค้าทุกชิ้นผ่านการคัดสรรจากแบรนด์ชั้นนำมากกว่า 70 แบรนด์ หรือรวมมากกว่า 1,500 รายการ อาทิ <a href="http://www.venbi.com/th/diapers/popular-brands/mamy-poko.html">Mamypoko</a>, <a href="http://www.venbi.com/th/formula-nursing/popular-brands/peachy.html">Peachy</a>, <a href="http://www.venbi.com/th/diapers/popular-brands/huggies.html">Huggiees</a>, <a href="http://www.venbi.com/th/diapers/popular-brands/merries.html">Merries</a>, <a href="http://www.venbi.com/th/diapers/popular-brands/baby-love.html">Babylove</a>, <a href="http://www.venbi.com/th/diapers/popular-brands/drypers.html">Drypers</a>, <a href="http://www.venbi.com/th/formula-nursing/popular-brands/enfa.html">Enfa</a>, <a href="http://www.venbi.com/th/formula-nursing/popular-brands/similac.html">Similac</a>, <a href="http://www.venbi.com/th/formula-nursing/popular-brands/s-26.html">S-26</a>, <a href="http://www.venbi.com/th/formula-nursing/popular-brands/dg.html">DG</a>, <a href="http://www.venbi.com/th/formula-nursing/popular-brands/dumex.html">Dumex</a>, <a href="http://www.venbi.com/th/toys/popular-brands/bright-starts.html">Bright starts</a>, <a href="http://www.venbi.com/th/toys/popular-brands/oball-by-rhino.html">Rhinotoys</a>, <a href="http://www.venbi.com/th/toys/popular-brands/vulli.html">Vulli</a>, <a href="http://www.venbi.com/th/toys/popular-brands/munchkin.html">Munchkin</a>, <a href="http://www.venbi.com/th/toys/popular-brands/lamaze.html">Lamaze</a>, <a href="http://www.venbi.com/th/toys/popular-brands/fisher-price.html">Fisher-Price</a> และ <a href="http://www.venbi.com/th/toys/popular-brands/the-first-years.html">The First Years</a></p>
<p>&nbsp;</p>
<p>นอกจากจะมีสินค้าที่หลากหลายกว่า ราคาถูกกว่าที่อื่นแล้ว Venbi.com ยังมีบริการจัดส่งสินค้าโดยที่คุณไม่ต้องเสียเวลาเดินหาซื้อและขนสินค้ากลับเองให้เมื่อยอีกต่อไป เพียงแค่เลือกสินค้าในเว็บไซต์ เลือก<a href="http://www.venbi.com/th/help/#payments">การชำระเงิน</a>ด้วยวีธีที่ต้องการ เช่น ชำระผ่านบัตรเครดิต, ชำระผ่าน Paypal หรือ<strong>ชำระเงินปลายทาง</strong>กับพนักงานจัดส่งสินค้า เมื่อเลือกเสร็จแล้ว ก็รอรับสินค้าที่<strong>ส่งตรงถึงหน้าบ้าน</strong>ภายใน 1-2 วัน ได้เลย</p>
</div>
EOD;
    $block = Mage::getModel('cms/block')->getCollection()
        ->addStoreFilter(array(4), $withAdmin = false)
        ->addFieldToFilter('identifier', $blockIdentifier)
        ->getFirstItem();
    if ($block->getId() == 0) {
        $block = Mage::getModel('cms/block');
    } else { // if exists then delete
        $block->delete();
        $block = Mage::getModel('cms/block');
    }
    $block->setTitle($blockTitle);
    $block->setIdentifier($blockIdentifier);
    $block->setStores($blockStores);
    $block->setIsActive($blockIsActive);
    $block->setContent($blockContent);
    $block->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================

    //==========================================================================
    // SEO Keyword Petloft EN block
    //==========================================================================
    $blockTitle = "SEO Keyword";
    $blockIdentifier = "seo-keyword";
    $blockStores = array(1);
    $blockIsActive = 1;
    $blockContent = <<<EOD
<div style="margin: 50px auto 30px;">
<div style="text-align: center;"><a title="Shop at Venbi" onclick="_gaq.push(['_trackEvent', 'Venbi Banner', 'Click', 'Venbi Home Banner - Bottom',1,true])" href="http://www.venbi.com/"><img src="{{media url="wysiwyg/BottomBannersWeb/Bottombanners_venbi.jpg"}}" alt="Shop baby and mom products at Venbi" /></a><a title="Shop at Sanoga" onclick="_gaq.push(['_trackEvent', 'Sanoga Banner', 'Click', 'Sanoga Home Banner - Bottom',1,true])" href="http://www.sanoga.com/"><img style="padding: 0 0 0 5px;" src="{{media url="wysiwyg/BottomBannersWeb/Bottombanners_sanoga.jpg"}}" alt="" /></a><a title="Shop at Lafema" onclick="_gaq.push(['_trackEvent', 'Lafema Banner', 'Click', 'Lafema Home Banner - Bottom',1,true])" href="http://www.lafema.com/"><img style="padding: 0 5px;" src="{{media url="wysiwyg/BottomBannersWeb/Bottombanners_lafema.jpg"}}" alt="Shop cosmetics at Lafema" /></a><a title="Shop at Moxy" onclick="_gaq.push(['_trackEvent', 'Moxy Banner', 'Click', 'Moxy Home Banner - Bottom',1,true])" href="http://www.moxyst.com/"><img src="{{media url="wysiwyg/BottomBannersWeb/Bottombanners_moxy.jpg"}}" alt="Shop at Moxy" /></a></div>
<h2>&nbsp;</h2>
<h2>PETLOFT &ndash; THAILAND&rsquo;S NUMBER 1 ONLINE PET STORE</h2>
<p>&nbsp;</p>
<p>Welcome to the number one online pet store in Thailand. Shopping at PetLoft means that you will no longer have to drive around for hours looking for the best products for your pets. Everything can now be purchased from one place. PetLoft aims to be the one stop pet shop offering great service, fast delivery and multiple easy payment methods.&nbsp;</p>
<p>&nbsp;</p>
<p>We sell <strong><a href="{{store url='catalogsearch/result' _query='q=dog'}}">dogs</a> , <a href="{{store url='catalogsearch/result' _query='q=cat'}}">cats</a> , <a href="{{store url='fish-products.html'}}">fish</a> , <a href="{{store url='small-pets-products.html'}}">small pets</a> , reptile ,</strong> and <strong><a href="{{store url='bird-products.html'}}">bird</a></strong> products. From PetLoft, you can get your <strong> <a href="{{store url='dog-food.html'}}">dog food</a> , <a href="{{store url='cat-food.html'}}">cat food</a> , <a href="{{store url='cat-food/cat-food-type/cat-litter.html'}}">cat litter</a> , <a href="{{store url='catalogsearch/result' _query='q=toys'}}">toys</a> , <a href="{{store url='catalogsearch/result' _query='q=accessories'}}">accessories</a> , <a href="{{store url='catalogsearch/result' _query='q=clothing'}}">clothing</a> , <a href="{{store url='catalogsearch/result' _query='q=pet+care'}}">medicine</a> , <a href="{{store url='catalogsearch/result' _query='q=shampoo'}}">shampoo</a> ,</strong> and many other products. PetLoft currently stocks over 70 brands of products with over 1,500 SKUs. Some of our best selling brands include <strong> <a href="{{store url='catalogsearch/result' _query='q=pedigree'}}">Pedigree</a> , <a href="{{store url='catalogsearch/result' _query='q=royal+canin'}}">Royal Canin</a> , <a href="{{store url='catalogsearch/result' _query='q=pinnacle'}}">Pinnacle</a> , <a href="{{store url='catalogsearch/result' _query='q=maxima'}}">Maxima</a> , <a href="{{store url='catalogsearch/result' _query='q=smart+heart'}}">SmartHeart</a> , <a href="{{store url='catalogsearch/result' _query='q=perfecta'}}">Perfecta</a> , <a href="{{store url='catalogsearch/result' _query='q=meo'}}">Me-O</a> , <a href="{{store url='catalogsearch/result' _query='q=chicken+soup'}}">Chicken Soup</a> , <a href="{{store url='catalogsearch/result' _query='q=whiskas'}}">Whiskas</a> , <a href="{{store url='catalogsearch/result' _query='q=science+diet'}}">Science Diet</a> , <a href="{{store url='catalogsearch/result' _query='q=frontline'}}">Frontline</a> , <a href="{{store url='catalogsearch/result' _query='q=cesar'}}">Cesar</a> , <a href="{{store url='catalogsearch/result' _query='q=zeal'}}">Zeal</a> , <a href="{{store url='catalogsearch/result' _query='q=anf'}}">ANF</a> , <a href="{{store url='catalogsearch/result' _query='q=dog+n+joy'}}">Dog N&rsquo; Joy</a> </strong> , and many more.&nbsp;</p>
<p>&nbsp;</p>
<p>Shopping at PetLoft is fun, easy, cheaper, and best of all <strong>SHIPPING.</strong>&nbsp;You no longer have to carry heavy bags of dog food or cat litter to your loving pets anymore. From now on, all you have to do is come to PetLoft and order your products here and in 1-2 days, it will arrive at your door <strong>FUSS FREE!</strong> With multiple payment methods such as credit card, paypal, cash on delivery, shopping online has never been easier.</p>
</div>
EOD;
    $block = Mage::getModel('cms/block')->getCollection()
        ->addStoreFilter(array(1), $withAdmin = false)
        ->addFieldToFilter('identifier', $blockIdentifier)
        ->getFirstItem();
    if ($block->getId() == 0) {
        $block = Mage::getModel('cms/block');
    } else { // if exists then delete
        $block->delete();
        $block = Mage::getModel('cms/block');
    }
    $block->setTitle($blockTitle);
    $block->setIdentifier($blockIdentifier);
    $block->setStores($blockStores);
    $block->setIsActive($blockIsActive);
    $block->setContent($blockContent);
    $block->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================

    //==========================================================================
    // SEO Keyword Petloft th block
    //==========================================================================
    $blockTitle = "SEO Keyword";
    $blockIdentifier = "seo-keyword";
    $blockStores = array(2);
    $blockIsActive = 1;
    $blockContent = <<<EOD
<div style="margin: 50px auto 30px;">
<div style="text-align: center;"><a title="Shop at Venbi" onclick="_gaq.push(['_trackEvent', 'Venbi Banner', 'Click', 'Venbi Home Banner - Bottom',1,true])" href="http://www.venbi.com/"><img src="{{media url="wysiwyg/BottomBannersWeb/Bottombanners_venbi.jpg"}}" alt="Shop baby and mom products at Venbi" /></a><a title="Shop at Sanoga" onclick="_gaq.push(['_trackEvent', 'Sanoga Banner', 'Click', 'Sanoga Home Banner - Bottom',1,true])" href="http://www.sanoga.com/"><img style="padding: 0 0 0 5px;" src="{{media url="wysiwyg/BottomBannersWeb/Bottombanners_sanoga.jpg"}}" alt="" /></a><a title="Shop at Lafema" onclick="_gaq.push(['_trackEvent', 'Lafema Banner', 'Click', 'Lafema Home Banner - Bottom',1,true])" href="http://www.lafema.com/"><img style="padding: 0 5px;" src="{{media url="wysiwyg/BottomBannersWeb/Bottombanners_lafema.jpg"}}" alt="Shop cosmetics at Lafema" /></a><a title="Shop at Moxy" onclick="_gaq.push(['_trackEvent', 'Moxy Banner', 'Click', 'Moxy Home Banner - Bottom',1,true])" href="http://www.moxyst.com/"><img src="{{media url="wysiwyg/BottomBannersWeb/Bottombanners_moxy.jpg"}}" alt="Shop at Moxy" /></a></div>
<h2>&nbsp;</h2>
<h2>PetLoft.com ร้านขายสินค้าและอาหารสัตว์เลี้ยงออนไลน์ อันดับ 1 ในประเทศไทย</h2>
<p>&nbsp;</p>
<p>ยินดีต้อนรับสู่ ร้านขายสินค้าเกี่ยวกับสัตว์เลี้ยงออนไลน์อันดับ 1&lrm; ของเมืองไทย เราเป็นศูนย์รวมผลิตภัณฑ์สำหรับสัตว์เลี้ยง <strong>อาหารสัตว์เลี้ยง</strong> <strong>อุปกรณ์สัตว์เลี้ยง</strong>ทุกประเภท คุณภาพระดับพรีเมี่ยมใน<strong>ราคาถูก</strong> รวมไปถึง<a href="http://www.petloft.com/th/clearance-sale.html">โปรโมชั่นสุดคุ้ม</a>ที่ถูกใจคนรักสัตว์ ให้ได้เลือกซื้อกันตามที่ต้องการ พร้อมการให้บริการที่ดีเยี่ยม ด้วยการ<strong>จัดส่งสินค้าที่รวดเร็ว</strong> และการชำระเงินค่าสินค้าที่ง่ายดายหลากหลายช่องทาง</p>
<p>&nbsp;</p>
<p>ที่ PetLoft เราจำหน่ายสินค้าสัตว์เลี้ยงมากมาย ไม่ว่าจะเป็น <a href="http://www.petloft.com/th/dog-food.html/">อาหารสุนัข</a><strong> </strong><a href="http://www.petloft.com/th/dog-food/dog-food-type/home-cage-pet.html">บ้านหมา</a><strong> </strong><a href="http://www.petloft.com/th/dog-food/dog-food-type/clothes.html/">เสื้อผ้าสุนัข</a><strong> </strong><a href="http://www.petloft.com/th/cat-food.html/">อาหารแมว</a><strong> </strong><a href="http://www.petloft.com/th/cat-food/cat-food-type/toys.html/">ของเล่นแมว</a><strong> </strong><a href="http://www.petloft.com/th/cat-food/cat-food-type/cat-condo.html/">คอนโดแมว</a><strong> </strong><a href="http://www.petloft.com/th/cat-food/cat-food-type/cat-litter.html/">ทรายแมว</a><strong> </strong><a href="http://www.petloft.com/th/small-pets-products/small-pet-types/rabbits.html/">อาหารกระต่าย</a><strong> </strong><a href="http://www.petloft.com/th/fish-products/fish-product-types/fish-food.html/">อาหารปลา</a><strong> </strong><a href="http://www.petloft.com/th/catalogsearch/result/?q=%E0%B8%AD%E0%B8%B2%E0%B8%AB%E0%B8%B2%E0%B8%A3%E0%B9%80%E0%B8%AA%E0%B8%A3%E0%B8%B4%E0%B8%A1">วิตามินและอาหารเสริม</a><strong> </strong><a href="http://www.petloft.com/th/catalogsearch/result/?q=%E0%B8%8A%E0%B8%B2%E0%B8%A1%E0%B9%81%E0%B8%A5%E0%B8%B0%E0%B8%AD%E0%B8%B8%E0%B8%9B%E0%B8%81%E0%B8%A3%E0%B8%93%E0%B9%8C%E0%B9%83%E0%B8%AB%E0%B9%89%E0%B8%AD%E0%B8%B2%E0%B8%AB%E0%B8%B2%E0%B8%A3">ชามและอุปกรณ์ให้อาหาร</a><strong> </strong><a href="http://www.petloft.com/th/catalogsearch/result/?q=%E0%B8%9C%E0%B8%B4%E0%B8%A7%E0%B8%AB%E0%B8%99%E0%B8%B1%E0%B8%87+%E0%B8%82%E0%B8%99">ผลิตภัณฑ์ดูแลผิวหนัง-ขน</a> เป็นต้น สินค้าทุกชิ้นผ่านการคัดสรรจากแบรนด์ชั้นนำมากกว่า 70 แบรนด์ หรือรวมมากกว่า 1,500 รายการ อาทิ&nbsp; <a href="http://www.petloft.com/th/dog-food/dog-product-brands/royal-canin.html">Royal Canin</a>, <a href="http://www.petloft.com/th/dog-food/dog-product-brands/maxima.html">Maxima</a>, <a href="http://www.petloft.com/th/cat-food/cat-food-brands/purina.html">Purina one</a>, <a href="http://www.petloft.com/th/dog-food/dog-product-brands/smartheart.html">Smartheart</a>, <a href="http://www.petloft.com/th/cat-food/cat-food-brands/whiskas.html">Whiskas</a>, <a href="http://www.petloft.com/th/dog-food/dog-product-brands/dog-n-joy.html">Dog'n joy</a>, <a href="http://www.petloft.com/th/cat-food/cat-food-brands/meo.html">Meo</a>, <a href="http://www.petloft.com/th/cat-food/cat-food-brands/sukina-petto.html">Sukina Petto</a>, <a href="http://www.petloft.com/th/dog-food/dog-product-brands/avoderm.html">Avoderm</a> และ <a href="http://www.petloft.com/th/cat-food/cat-food-brands/see-sand.html">See Sand</a></p>
<p>&nbsp;</p>
<p>นอกจากจะมีสินค้าที่หลากหลายกว่า ราคาถูกกว่าที่อื่นแล้ว PetLoft.com ยังมีบริการจัดส่งสินค้าโดยที่คุณไม่ต้องเสียเวลาเดินหาซื้อและขนสินค้ากลับเองให้เมื่อยอีกต่อไป เพียงแค่เลือกสินค้าในเว็บไซต์ เลือก<a href="http://www.petloft.com/th/help/#payments">การชำระเงิน</a>ด้วยวีธีที่ต้องการ เช่น ชำระผ่านบัตรเครดิต, ชำระผ่าน Paypal หรือ<strong>ชำระเงินปลายทาง</strong>กับพนักงานจัดส่งสินค้า เมื่อเลือกเสร็จแล้ว ก็รอรับสินค้าที่<strong>ส่งตรงถึงหน้าบ้าน</strong>ภายใน 1-2 วัน ได้เลย</p>
</div>
EOD;
    $block = Mage::getModel('cms/block')->getCollection()
        ->addStoreFilter(array(2), $withAdmin = false)
        ->addFieldToFilter('identifier', $blockIdentifier)
        ->getFirstItem();
    if ($block->getId() == 0) {
        $block = Mage::getModel('cms/block');
    } else { // if exists then delete
        $block->delete();
        $block = Mage::getModel('cms/block');
    }
    $block->setTitle($blockTitle);
    $block->setIdentifier($blockIdentifier);
    $block->setStores($blockStores);
    $block->setIsActive($blockIsActive);
    $block->setContent($blockContent);
    $block->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================

    $installer->endSetup();
} catch (Excpetion $e) {
    Mage::logException($e);
    Mage::log("ERROR IN SETUP " . $e->getMessage());
}
