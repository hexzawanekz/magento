<?php

// Base-name
$name           = 'moxy';
$engStoreName   = 'Moxy English';
$engStoreCode   = 'moxyen';
$thStoreName    = 'Moxy Thai';
$thStoreCode    = 'moxyth';
$storeIdMoxyEn  = Mage::getModel('core/store')->load($engStoreCode, 'code')->getId();
$storeIdMoxyTh  = Mage::getModel('core/store')->load($thStoreCode, 'code')->getId();
$newProductsCateId = 0;
$bestSellersCateId = 0;
try {
    $installer = $this;
    $installer->startSetup();


    //==========================================================================
    // About us EN Page
    //==========================================================================
    $pageTitle = "About Us";
    $pageIdentifier = "about-us";
    $pageStores = array($storeIdMoxyEn);
    $pageIsActive = 1;
    $pageUnderVersionControl = 0;
    $pageContentHeading = "About Us";
    $pageContent = <<<EOD
<div style="margin: 20px 0;">&nbsp;</div>
<div style="font: normal 14px 'Tahoma'; color: #505050; line-height: 22px;">
<p>Moxy is the online destination for all women in Thailand. We curate lifestyle products, from fashion accessories to home déco and gadgets, everything women need and want in their daily lives, since we know the challenges of living a balanced lifestyle when you are juggling with responsibilities! Having the convenience of it being delivered to your front door by us. With a mix of both local and international brands and new items available every week, Moxy offers a large variety of quality products to suit your lifestyle. &nbsp;&nbsp;</p>
<p>&nbsp;</p>
<p>At Moxy, we believe that purchasing online should be easy, fun and safe. Our mission is to provide an entertaining shopping experience combined with a convenient home delivery service. Our customers can choose between various secure payment options, including cash-on-delivery in Thailand. At any moment, you can reach our support team at&nbsp;<span style="text-decoration: underline;"><a href="mailto:support@moxyst.com">support@moxyst.com</a></span> or by phone +662-106-8222. Our customer service talents are at your service from Mon-Fri 09:00-18:00.</p>
<p>&nbsp;</p>
<p>Stay connected with us on&nbsp;<span style="color: #333300;"><a href="https://www.facebook.com/moxyst" target="_blank"><span style="color: #333300;"><strong><span style="text-decoration: underline;">Facebook</span></strong></span></a></span>,&nbsp;<span style="color: #333300;"><a href="https://instagram.com/moxyst/" target="_blank"><span style="color: #333300;"><strong><span style="text-decoration: underline;">Instagram</span></strong></span></a>&nbsp;</span>and&nbsp;<span style="color: #333300;"><a href="https://twitter.com/moxyst" target="_blank"><span style="color: #333300;"><strong><span style="text-decoration: underline;">Twitter</span>&nbsp;</strong></span></a></span>to get the newest updates and best deals!</p>
<p>&nbsp;</p>
<p>We are looking forward to serving you and please remember, you can&nbsp;<strong><em>HAVE IT ALL :-)</em></strong><em>.&nbsp;</em>&nbsp;&nbsp;&nbsp;</p>
<p>With Love,</p>
<strong>The Moxy Team</strong></div>
EOD;

    $pageRootTemplate = 'one_column';
    $pageLayoutUpdateXml = <<<EOD
EOD;


    $pageCustomLayoutUpdateXML = <<<EOD
EOD;

    $pageMetaKeywords = "";
    $pageMetaDescription = "";

    $page = Mage::getModel('cms/page')->getCollection()
        ->addStoreFilter(array($storeIdMoxyEn), $withAdmin = true)
        ->addFieldToFilter('identifier', $pageIdentifier)
        ->getFirstItem();
    if ($page->getId() == 0) {
        $page = Mage::getModel('cms/page');
    }
    else
    {
        // if exists then repair
        $page = Mage::getModel('cms/page')->load($page->getId());
    }

    $page->setTitle($pageTitle);
    $page->setIdentifier($pageIdentifier);
    $page->setStores($pageStores);
    $page->setIsActive($pageIsActive);
    $page->setUnderVersionControl($pageUnderVersionControl);
    $page->setContentHeading($pageContentHeading);
    $page->setContent($pageContent);
    $page->setRootTemplate($pageRootTemplate);
    $page->setLayoutUpdateXml($pageLayoutUpdateXml);
    $page->setCustomLayoutUpdateXml($pageCustomLayoutUpdateXML);
    $page->setMetaKeywords($pageMetaKeywords);
    $page->setMetaDescription($pageMetaDescription);
    $page->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================

    //==========================================================================
    // About us TH Page
    //==========================================================================
    $pageTitle = "About us | เกี่ยวกับเรา";
    $pageIdentifier = "about-us";
    $pageStores = array($storeIdMoxyTh);
    $pageIsActive = 1;
    $pageUnderVersionControl = 0;
    $pageContentHeading = "เกี่ยวกับ Moxy";
    $pageContent = <<<EOD
<div style="margin: 20px 0;">&nbsp;</div>
<div style="font: normal 14px 'Tahoma'; color: #505050; line-height: 22px;">
<p style="font-weight: normal;">Moxy คือศูนย์รวมสินค้าไลฟสไตล์ออนไลน์สำหรับผู้หญิงทุกคน เรารวบรวมคัดสรรทุกสิ่งอย่างที่ผู้หญิงต้องการในทุกๆวัน จากแบรนด์ที่น่าสนใจทั้งจากในและต่างประเทศ ด้วยสินค้าที่มีคุณภาพและดีไซน์ที่แตกต่างเพื่อเป็นตัวเลือกให้กับไลฟสไตล์ของคุณ เราเชื่อว่าการซื้อสินค้าออนไลน์จะช่วยให้คุณสนุก และ ยังช่วยอำนวยความสะดวกสบายด้วยการส่งตรงถึงหน้าบ้าน! &nbsp; &nbsp;</p>
<p style="font-weight: normal;">&nbsp;</p>
<p style="font-weight: normal;">คุณสามารถเลือกช่องทางในการจ่ายเงินได้หลากหลายวิธี รวมถึงจ่ายเงินปลายทาง หากมีข้อสงสัยเพิ่มเติมประการใด ติดต่อเราได้ที่ <span style="text-decoration: underline;"><a href="mailto:support@moxyst.com">support@moxyst.com</a></span> หรือโทรศัพท์ +662-106-8222 เราพร้อมที่จะให้คำปรึกษาเวลาจันทร์-ศุกร์ เวลา 09:00-18:00</p>
<p style="font-weight: normal;">&nbsp;</p>
<p>ติดตามเราได้ที่&nbsp;<a href="https://www.facebook.com/moxyst" target="_blank"><strong><span style="text-decoration: underline;">Facebook</span></strong></a>,&nbsp;<span><a style="font-weight: bold; text-decoration: underline;" href="https://instagram.com/moxyst/" target="_blank">Instagram</a></span><span style="color: #333300;"><span style="color: #333300;"><span>,&nbsp;</span></span></span><strong><span style="text-decoration: underline;"><a href="https://twitter.com/moxyst" target="_blank">Twitter</a></span></strong><strong>&nbsp;</strong><span style="color: #333300;"><span style="color: #333300;">สำหรับข้อมูลและข่าวสารล่าสุด</span></span></p>
<p style="font-weight: normal;">อย่าพลาดโอกาสสำหรับโปรโมชั่นใหม่ๆจากเรา!</p>
<p style="font-weight: normal;">&nbsp;</p>
<p>Moxy &ndash; Have it all :-)</p>
<strong>The Moxy Team</strong></div>
EOD;

    $pageRootTemplate = 'one_column';
    $pageLayoutUpdateXml = <<<EOD
EOD;


    $pageCustomLayoutUpdateXML = <<<EOD
EOD;

    $pageMetaKeywords = "";
    $pageMetaDescription = "Moxy คือศูนย์รวมผลิตภัณฑ์เพื่อสุขภาพและเครื่องสำอาง อาหารเสริม คอลลาเจน และวิตามินต่างๆ คุณภาพแท้ 100% สินค้ามี อย.ทุกชิ้น จากแบรนด์ชั้นนำทั่วโลก";

    $page = Mage::getModel('cms/page')->getCollection()
        ->addStoreFilter(array($storeIdMoxyTh), $withAdmin = true)
        ->addFieldToFilter('identifier', $pageIdentifier)
        ->getFirstItem();
    if ($page->getId() == 0) {
        $page = Mage::getModel('cms/page');
    }
    else
    {
        // if exists then repair
        $page = Mage::getModel('cms/page')->load($page->getId());
    }

    $page->setTitle($pageTitle);
    $page->setIdentifier($pageIdentifier);
    $page->setStores($pageStores);
    $page->setIsActive($pageIsActive);
    $page->setUnderVersionControl($pageUnderVersionControl);
    $page->setContentHeading($pageContentHeading);
    $page->setContent($pageContent);
    $page->setRootTemplate($pageRootTemplate);
    $page->setLayoutUpdateXml($pageLayoutUpdateXml);
    $page->setCustomLayoutUpdateXml($pageCustomLayoutUpdateXML);
    $page->setMetaKeywords($pageMetaKeywords);
    $page->setMetaDescription($pageMetaDescription);
    $page->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================



    $installer->endSetup();
} catch (Excpetion $e) {
    Mage::logException($e);
    Mage::log("ERROR IN SETUP " . $e->getMessage());
}
