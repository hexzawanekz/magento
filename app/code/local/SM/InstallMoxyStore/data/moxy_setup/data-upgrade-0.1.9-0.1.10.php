<?php

// Base-name
$name           = 'moxy';
$engStoreName   = 'Moxy English';
$engStoreCode   = 'moxyen';
$thStoreName    = 'Moxy Thai';
$thStoreCode    = 'moxyth';
$storeIdMoxyEn  = Mage::getModel('core/store')->load($engStoreCode, 'code')->getId();
$storeIdMoxyTh  = Mage::getModel('core/store')->load($thStoreCode, 'code')->getId();
$newProductsCateId = 0;
$bestSellersCateId = 0;
try {
    $installer = $this;
    $installer->startSetup();

    // Force the store to be admin
    Mage::app()->setUpdateMode(false);
    Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
    if (!Mage::registry('isSecureArea'))
        Mage::register('isSecureArea', 1);

    //==========================================================================
    // contact-us EN block
    //==========================================================================
    $blockTitle = "Contact Us Moxy EN";
    $blockIdentifier = "contact-us";
    $blockStores = array($storeIdMoxyEn);
    $blockIsActive = 1;
    $blockContent = <<<EOD
<div style="height: 403px;">
<div style="width: 40%; float: left; line-height: 22px; padding-left: 65px; font-size: 14px; margin-top: 20px; padding-top: 15px;">
<p><strong>Moxyst.com</strong></p>
<p>Customer service :&nbsp;<br /> 8/2 Rama 3 Road, Soi 53<br /> Bang Phongpang, Yannawa<br /> Bangkok, 10120 Thailand<br /> Tel: 02-106-8222&nbsp;<br /> Facebook:&nbsp;<a href="https://www.facebook.com/moxyst" target="_blank">https://www.facebook.com/moxyst</a><br /> Email:&nbsp;<a title="send email to Sanoga" href="mailto:support@moxyst.com" target="_self">support@moxyst.com</a></p>
<p>&nbsp;</p>
<p>Office:<br /> Whatsnew Co., Ltd.<br /> Sethiwan Tower - 17th Floor<br /> 139 Pan Road, Silom, Bangrak,<br /> Bangkok, 10500 Thailand<br /> Tel: <span>02-106-8222</span>&nbsp;<br /> Facebook:&nbsp;<a href="https://www.facebook.com/moxyst" target="_blank">https://www.facebook.com/moxyst</a><br /> Email:&nbsp;<a title="send email to Sanoga" href="mailto:support@moxyst.com" target="_self">support@moxyst.com</a></p>
</div>
<div style="margin-top: 20px; width: 50%; float: right;"><iframe style="border: 0;" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1937.973472047649!2d100.5241348!3d13.721662!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e298cdd9e5bd05%3A0x1e206241cb063178!2sSethiwan+Tower!5e0!3m2!1sen!2sth!4v1397117387281" frameborder="0" width="400" height="300"></iframe><br /><small>View <a style="color: #0000ff; text-align: left;" href="https://maps.google.com/maps/ms?msa=0&amp;msid=214150831025562562519.0004daafa9c87d88cd667&amp;hl=en&amp;ie=UTF8&amp;t=m&amp;source=embed&amp;ll=13.728265,100.537412&amp;spn=0,0">moxyst.com</a> in a larger map</small></div>
</div>
EOD;
    $block = Mage::getModel('cms/block')->getCollection()
        ->addStoreFilter(array($storeIdMoxyEn), $withAdmin = false)
        ->addFieldToFilter('identifier', $blockIdentifier)
        ->getFirstItem();
    if ($block->getId() == 0) {
        $block = Mage::getModel('cms/block');
    } else { // if exists then delete
        $block->delete();
        $block = Mage::getModel('cms/block');
    }
    $block->setTitle($blockTitle);
    $block->setIdentifier($blockIdentifier);
    $block->setStores($blockStores);
    $block->setIsActive($blockIsActive);
    $block->setContent($blockContent);
    $block->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================

    //==========================================================================
    // contact-us TH block
    //==========================================================================
    $blockTitle = "ติดต่อเรา Moxy TH";
    $blockIdentifier = "contact-us";
    $blockStores = array($storeIdMoxyTh);
    $blockIsActive = 1;
    $blockContent = <<<EOD
<div style="height: 403px;">
<div style="width: 40%; float: left; line-height: 22px; padding-left: 65px; font-size: 14px; margin-top: 20px; padding-top: 15px;">
<p><strong>Moxyst.com</strong><br /> บริการลูกค้า:<br /> 8/2 ถนนพระราม 3 ซอย 53<br /> แขวงบางโพงพาง เขตสาทร<br /> กรุงเทพฯ ประเทศไทย 10120<br /> โทรศัพท์: 02-106-8222&nbsp;<br /> Facebook:&nbsp;<a href="https://www.facebook.com/moxyst" target="_blank">https://www.facebook.com/moxyst</a><br /> Email:&nbsp;<a title="send email to Sanoga" href="mailto:support@moxyst.com" target="_self">support@moxyst.com</a><strong></strong></p>
<p>&nbsp;</p>
<p>ออฟฟิศ:<br />อาคารเศรษฐีวรรณ ชั้น 17<br />139 ถนนปั้น แขวงสีลม เขตบางรัก<br /> กรุงเทพฯ ประเทศไทย 10500<br /> โทรศัพท์: <span>02-106-8222</span>&nbsp;<br /> Facebook:&nbsp;<a href="https://www.facebook.com/moxyst" target="_blank">https://www.facebook.com/moxyst</a><br /> Email:&nbsp;<a title="send email to Sanoga" href="mailto:support@moxyst.com" target="_self">support@moxyst.com</a></p>
</div>
<div style="margin-top: 20px; width: 50%; float: right;"><iframe style="border: 0;" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1937.973472047649!2d100.5241348!3d13.721662!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e298cdd9e5bd05%3A0x1e206241cb063178!2sSethiwan+Tower!5e0!3m2!1sen!2sth!4v1397117387281" frameborder="0" width="400" height="300"></iframe><br /><small>View <a style="color: #0000ff; text-align: left;" href="https://maps.google.com/maps/ms?msa=0&amp;msid=214150831025562562519.0004daafa9c87d88cd667&amp;hl=en&amp;ie=UTF8&amp;t=m&amp;source=embed&amp;ll=13.728265,100.537412&amp;spn=0,0">Sanoga.com</a> in a larger map</small></div>
</div>
EOD;
    $block = Mage::getModel('cms/block')->getCollection()
        ->addStoreFilter(array($storeIdMoxyTh), $withAdmin = false)
        ->addFieldToFilter('identifier', $blockIdentifier)
        ->getFirstItem();
    if ($block->getId() == 0) {
        $block = Mage::getModel('cms/block');
    } else { // if exists then delete
        $block->delete();
        $block = Mage::getModel('cms/block');
    }
    $block->setTitle($blockTitle);
    $block->setIdentifier($blockIdentifier);
    $block->setStores($blockStores);
    $block->setIsActive($blockIsActive);
    $block->setContent($blockContent);
    $block->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================

    //==========================================================================
    // Banks Footer block
    //==========================================================================
    $blockTitle = "Banks Footer";
    $blockIdentifier = "banks_footer";
    $blockStores = array("$storeIdMoxyEn","$storeIdMoxyTh");
    $blockIsActive = 1;
    $blockContent = <<<EOD
<p><a href="https://plus.google.com/115593276916117018663/" target="_blank"> <span class="google-footer"><img src="{{media url="wysiwyg/footer_follow/google_follow.png"}}" alt="" width="32px" /></span> </a> <a href="https://twitter.com/venbithai" target="_blank"> <span class="twitter-footer"><img src="{{media url="wysiwyg/footer_follow/twitter_follow.png"}}" alt="" width="32px" /></span> </a> <a href="http://instagram.com/venbi_thailand" target="_blank"> <span class="instagram-footer"><img src="{{media url="wysiwyg/footer_follow/instagram_follow.png"}}" alt="" width="32px" /></span> </a> <a href="http://www.youtube.com/user/VenbiThai" target="_blank"> <span class="youtube-footer"><img src="{{media url="wysiwyg/footer_follow/youtube_follow.png"}}" alt="" width="32px" /></span> </a> <a href="https://www.facebook.com/moxyst" target="_blank"> <span class="facebook-footer"><img src="{{media url="wysiwyg/footer_follow/facebook_follow.png"}}" alt="" width="32px" /></span> </a></p>
EOD;
    $block = Mage::getModel('cms/block')->getCollection()
        ->addStoreFilter(array($storeIdMoxyEn), $withAdmin = false)
        ->addFieldToFilter('identifier', $blockIdentifier)
        ->getFirstItem();
    if ($block->getId() == 0) {
        $block = Mage::getModel('cms/block');
    } else { // if exists then delete
        $block->delete();
        $block = Mage::getModel('cms/block');
    }
    $block->setTitle($blockTitle);
    $block->setIdentifier($blockIdentifier);
    $block->setStores($blockStores);
    $block->setIsActive($blockIsActive);
    $block->setContent($blockContent);
    $block->save();
    //==========================================================================
    //==========================================================================
    //==========================================================================


// Load the website
    $website = Mage::getModel('core/website')->load('base', 'code');
    if ($website->getId() > 0) {
        $websiteId = $website->getId();
        // Get the group
        $group = Mage::getModel('core/store_group');
        $groups = Mage::getModel('core/store_group')->getCollection()
            ->addFieldToSelect('*')
            ->addFieldToFilter('name', array('like' => ucfirst($name)))
            ->addFieldToFilter('website_id', $websiteId);
        if ($groups->count() > 0 && $groups->count() < 2) {
            $group = $groups->getFirstItem();
        }
        $groupId = $group->getGroupId();

// English
        $engStore = Mage::getModel('core/store');
        $engStoresChecker = Mage::getModel('core/store')->getCollection()
            ->addFieldToSelect('*')
            ->addFieldToFilter('code', $engStoreCode)
            ->addFieldToFilter('website_id', $websiteId)
            ->addFieldToFilter('group_id', $groupId);
        if ($engStoresChecker->count() > 0 && $engStoresChecker->count() < 2) {
            $engStore = $engStoresChecker->getFirstItem();
        }
        $engStoreId = $engStore->getStoreId();

// Thai
        $thStore = Mage::getModel('core/store');
        $thStoresChecker = Mage::getModel('core/store')->getCollection()
            ->addFieldToSelect('*')
            ->addFieldToFilter('code', $thStoreCode)
            ->addFieldToFilter('website_id', $websiteId)
            ->addFieldToFilter('group_id', $groupId);
        if ($thStoresChecker->count() > 0 && $thStoresChecker->count() < 2) {
            $thStore = $thStoresChecker->getFirstItem();
        }
        $thStoreId = $thStore->getStoreId();

        // Save config
        $configurator = new Mage_Core_Model_Config();
        $configurator->saveConfig('contacts/email/recipient_email', 'support@moxyst.com', 'stores', $engStoreId);
        $configurator->saveConfig('contacts/email/recipient_email', 'support@moxyst.com', 'stores', $thStoreId);
    }


    $installer->endSetup();
} catch (Excpetion $e) {
    Mage::logException($e);
    Mage::log("ERROR IN SETUP " . $e->getMessage());
}
