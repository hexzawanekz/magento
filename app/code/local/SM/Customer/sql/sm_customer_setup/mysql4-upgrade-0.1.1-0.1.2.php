<?php

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();


$installer->run("
    ALTER TABLE {$this->getTable('sales_flat_quote_address')} ADD COLUMN `sub_district` TEXT DEFAULT NULL;
    ALTER TABLE {$this->getTable('sales_flat_order_address')} ADD COLUMN `sub_district` TEXT DEFAULT NULL;

    ALTER TABLE {$this->getTable('sales_flat_quote_address')} ADD COLUMN `district` TEXT NOT NULL;
    ALTER TABLE {$this->getTable('sales_flat_order_address')} ADD COLUMN `district` TEXT NOT NULL;
");


$installer->endSetup();