<?php

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

try{

    $attrTypeCode       = 'customer_address';
    $subDistrictCode    = 'sub_district';
    $districtCode       = 'district';
    $usedForms          = array('customer_register_address','customer_address_edit','adminhtml_customer_address');

    /**
     * create new customer attribute "sub_district"
     */
    $setup = new Mage_Eav_Model_Entity_Setup('core_setup');

    $entityTypeId     = $setup->getEntityTypeId($attrTypeCode);
    $attributeSetId   = $setup->getDefaultAttributeSetId($entityTypeId);
    $attributeGroupId = $setup->getDefaultAttributeGroupId($entityTypeId, $attributeSetId);

    $setup->addAttribute($attrTypeCode, $subDistrictCode, array(
        'input'         => 'text',
        'type'          => 'varchar',
        'label'         => 'Sub-District',
        'visible'       => 1,
        'required'      => 0,
        'user_defined'  => 1,
    ));

    $setup->addAttributeToGroup(
        $entityTypeId,
        $attributeSetId,
        $attributeGroupId,
        $subDistrictCode,
        '999'
    );

    $oAttribute = Mage::getSingleton('eav/config')->getAttribute($attrTypeCode, $subDistrictCode);
    $oAttribute->setData('used_in_forms', $usedForms);
    $oAttribute->save();

    /**
     * create new customer attribute "district"
     */
    $setup = new Mage_Eav_Model_Entity_Setup('core_setup');

    $entityTypeId     = $setup->getEntityTypeId($attrTypeCode);
    $attributeSetId   = $setup->getDefaultAttributeSetId($entityTypeId);
    $attributeGroupId = $setup->getDefaultAttributeGroupId($entityTypeId, $attributeSetId);

    $setup->addAttribute($attrTypeCode, $districtCode, array(
        'input'         => 'text',
        'type'          => 'varchar',
        'label'         => 'District',
        'visible'       => 1,
        'required'      => 1,
        'user_defined'  => 1,
    ));

    $setup->addAttributeToGroup(
        $entityTypeId,
        $attributeSetId,
        $attributeGroupId,
        $districtCode,
        '999'
    );

    $oAttribute = Mage::getSingleton('eav/config')->getAttribute($attrTypeCode, $districtCode);
    $oAttribute->setData('used_in_forms', $usedForms);
    $oAttribute->save();


}catch(Exception $e){
    throw $e;
}

$installer->endSetup();