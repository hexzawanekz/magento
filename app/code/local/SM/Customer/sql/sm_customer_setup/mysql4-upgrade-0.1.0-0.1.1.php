<?php

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();


///////////////////////////////////////////////////////////////////////////////////////
$content = <<<EOD
{{depend prefix}}{{var prefix}} {{/depend}}{{var firstname}} {{depend middlename}}{{var middlename}} {{/depend}}{{var lastname}}{{depend suffix}} {{var suffix}}{{/depend}}<br/>
{{depend company}}{{var company}}<br />{{/depend}}
{{if street1}}{{var street1}}<br />{{/if}}
{{depend street2}}{{var street2}}<br />{{/depend}}
{{depend street3}}{{var street3}}<br />{{/depend}}
{{depend street4}}{{var street4}}<br />{{/depend}}
{{if sub_district}}{{var sub_district}}<br />{{/if}}
{{if district}}{{var district}}<br />{{/if}}
{{if city}}{{var city}}, {{/if}}{{if region}}{{var region}}, {{/if}}{{if postcode}}{{var postcode}}{{/if}}<br/>
{{var country}}<br/>
{{depend telephone}}T: {{var telephone}}{{/depend}}
{{depend fax}}<br/>F: {{var fax}}{{/depend}}
{{depend vat_id}}<br/>VAT: {{var vat_id}}{{/depend}}
EOD;

/** @var Mage_Core_Model_Config $coreConfigModel */
$coreConfigModel = Mage::getModel('core/config');
$coreConfigModel->saveConfig("customer/address_templates/html", $content);
///////////////////////////////////////////////////////////////////////////////////////


$installer->endSetup();