<?php

class SM_Customer_Block_Adminhtml_Customer_Edit_Tab_Addresses extends Mage_Adminhtml_Block_Customer_Edit_Tab_Addresses
{
    protected function _prepareForm()
    {
        $form = $this->getForm();
        /** @var Varien_Data_Form_Element_Fieldset $fieldset */
        $fieldset = $form->getElement('address_fieldset');
        $fieldset->removeField('city');
        

        return parent::_prepareForm();
    }
}