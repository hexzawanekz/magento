<?php

$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */

$installer->startSetup();

$tblName = $installer->getTable('catalogsearch_query');

$table = $installer->getConnection()
    ->addColumn($tblName, 'category_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => true,
        ), 'Category ID');


$installer->endSetup();
