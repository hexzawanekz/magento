<?php

class SM_MoxyMobile_Block_Adminhtml_Config_Globallinks extends Mage_Adminhtml_Block_System_Config_Form_Field_Array_Abstract
{
    public function __construct()
    {
        $this->addColumn('label', array(
            'label' => Mage::helper('adminhtml')->__('Label'),
            'style' => 'width:120px',
        ));
        $this->addColumn('url_key', array(
            'label' => Mage::helper('adminhtml')->__('Category\'s Url-key'),
            'style' => 'width:120px',
        ));
        $this->addColumn('css_class', array(
            'label' => Mage::helper('adminhtml')->__('Css Class'),
            'style' => 'width:120px',
        ));
        $this->_addAfter = false;
        $this->_addButtonLabel = Mage::helper('adminhtml')->__('Add new Link');
        parent::__construct();
    }
}