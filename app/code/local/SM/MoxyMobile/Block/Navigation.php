<?php

class SM_MoxyMobile_Block_Navigation extends Mage_Catalog_Block_Navigation
{
    protected $arrUrlKeys = array();

    protected $_config = null;

    public function _construct()
    {
        parent::_construct();

        if($config = Mage::getStoreConfig('petloftcatalog/menu_cache/enabled')) $this->_config = $config;

        // disable Magento cache
        $this->unsetData('cache_lifetime');
        $this->unsetData('cache_tags');
    }

    public function getCategories()
    {
        if($ids = Mage::registry('category_ids'))
            return Mage::getModel('catalog/category')->getCollection()
                ->addAttributeToSelect('entity_id')
                ->addAttributeToSelect('url_key')
                ->addAttributeToFilter('entity_id', array('in' => $ids));
        else
            return null;
    }

    public function getChildCategories($parent_id, $level)
    {
        return Mage::getSingleton('catalog/category')->getCategories($parent_id, 0, 'position', true, false)->addLevelFilter($level);
    }

    public function loadCategories()
    {
        /** @var SM_MoxyMobile_Model_Navigation $modelObj */
        $modelObj = Mage::getModel('sm_moxymobile/navigation');

        $fashionArr     = $modelObj->getFashionUrlKeys();
        $livingArr      = $modelObj->getLivingUrlKeys();
        $electronicsArr = $modelObj->getElectronicUrlKeys();

        $subFolderName = '';
        $arrUrlKey = $this->getUrlKeys();
        foreach($arrUrlKey as $urlKey){
            if(in_array($urlKey, $fashionArr)){
                $subFolderName = SM_MoxyMobile_Model_Navigation::KEY_FASHION;
                break;
            }
            if(in_array($urlKey, $livingArr)){
                $subFolderName = SM_MoxyMobile_Model_Navigation::KEY_HOMELIVING;
                break;
            }
            if(in_array($urlKey, $electronicsArr)){
                $subFolderName = SM_MoxyMobile_Model_Navigation::KEY_ELECTRONICS;
                break;
            }
        }

        /** @var Petloft_Catalog_Model_Navigation_Renderer $renderer */
        $renderer = Mage::getModel('petloftcatalog/navigation_renderer');
        $store_code = Mage::app()->getStore()->getCode();
        $check_result = $subFolderName != '' ? $renderer->checkExists($store_code . '/' . $subFolderName . '/' . Petloft_Catalog_Model_Navigation_Renderer::MOBILE_FILENAME) : false;

        $html = '';
        if (!$check_result || $this->_config == false) {

            $rootCategories = $this->getCategories();
            foreach ($rootCategories as $rootCategory) {
                if ($rootCategory instanceof Mage_Catalog_Model_Category) {
                    $childCategoriesLv3 = $this->getChildCategories($rootCategory->getId(), '3');
                    foreach ($childCategoriesLv3 as $item) {
                        $html .= '<li class="level0 level-top">';
                        $html .= '<a class="level-top" href="' . Mage::getUrl($item->getRequestPath()) . '">';
                        $html .= '<span>' . $this->__($item->getName()) . '</span>';
                        $html .= '</a>';
                        $childCategoriesLv4 = $this->getChildCategories($item->getId(), '4');
                        if ($childCategoriesLv4->count() > 0) {
                            $html .= '<ul class="level0" style="display: none;">';
                            foreach ($childCategoriesLv4 as $item2) {
                                $html .= '<li class="level1">';
                                $html .= '<a href="' . Mage::getUrl($item2->getRequestPath()) . '">';
                                $html .= '<span>' . $this->__($item2->getName()) . '.</span>';
                                $html .= '</a>';
                                $html .= "</li>";
                            }
                            $html .= "</ul>";
                        }
                        $html .= "</li>";
                    }
                }
            }

            // render menu to html content
            if($this->_config == true) $renderer->renderMenuNavigationHtml($html, $store_code . '/' . $subFolderName . '/' . Petloft_Catalog_Model_Navigation_Renderer::MOBILE_FILENAME);
        }

        return $this->_config == true ? ($subFolderName != '' ? $renderer->toHtml($store_code . '/' . $subFolderName . '/' . Petloft_Catalog_Model_Navigation_Renderer::MOBILE_FILENAME) : '') : $html;
    }

    public function getUrlKeys()
    {
        if(count($this->arrUrlKeys) == 0){
            $rootCategories = $this->getCategories();
            foreach ($rootCategories as $rootCategory) {
                if ($rootCategory instanceof Mage_Catalog_Model_Category)
                    $this->arrUrlKeys[] = $rootCategory->getUrlKey();
            }
        }
        return $this->arrUrlKeys;
    }

    public function getModelObject()
    {
        return Mage::getModel('sm_moxymobile/navigation');
    }

    public function getRequestKey()
    {
        return $this->getRequest()->getParam('key') ? $this->getRequest()->getParam('key') : false;
    }
}