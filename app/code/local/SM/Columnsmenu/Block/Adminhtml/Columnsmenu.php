<?php

class SM_Columnsmenu_Block_Adminhtml_Columnsmenu extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function _construct()
    {
        $this->_blockGroup = 'columnsmenu';
        $this->_controller = 'adminhtml_columnsmenu';
        $this->_headerText = $this->__('Information Manage');
        $this->_addButtonLabel = 'Add item';
        parent::_construct();
    }

}