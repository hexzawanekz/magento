<?php

class SM_Columnsmenu_Block_Adminhtml_Columnsmenu_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function _construct()
    {
        parent::_construct();
        $this->setId('columnsmenu_grid');
        $this->setDefaultSort('columnsmenu_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    /**
     * get collection show to grid
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('columnsmenu/columnsmenu')->getCollection()
            ->setOrder('columnsmenu_id', 'DESC');
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareLayout()
    {
        return parent::_prepareLayout();
    }

    /**
     * show columns to grid
     */
    protected function _prepareColumns()
    {
        $this->addColumn('id', array(
            'header' => Mage::helper('columnsmenu')->__('ID'),
            'align' =>'center',
            'width' => '50px',
            'index' => 'columnsmenu_id',
        ));
        $this->addColumn('categoryname', array(
            'header' => Mage::helper('columnsmenu')->__('Category Name'),
            'width' => '100px',
            'index' => 'columnsmenu_category_name',
        ));
        $this->addColumn('categoryid', array(
            'header' => Mage::helper('columnsmenu')->__('Category ID'),
            'width' => '100px',
            'index' => 'columnsmenu_category_id',
        ));
        $this->addColumn('rootcategory', array(
            'header' => Mage::helper('columnsmenu')->__('Root Category'),
            'width' => '100px',
            'index' => 'columnsmenu_category_store_name',
        ));
        $this->addColumn('column', array(
            'header' => Mage::helper('columnsmenu')->__('Column'),
            'width' => '100px',
            'index' => 'columnsmenu_column',
        ));
        $this->addColumn('action', array(
            'header' => Mage::helper('columnsmenu')->__('Action'),
            'width' => '50px',
            'type' => 'action',
            'getter' => 'getId',
            'actions' => array(
                array(
                    'caption' => Mage::helper('columnsmenu')->__('Edit'),
                    'url' => array(
                        'base' => '*/*/edit',
                        'params' => array('id' => $this->getRequest()->getParam('id')),
                    ),
                    'field' => 'id'
                ),
//                array(
//                    'caption' => Mage::helper('columnsmenu')->__('Delete'),
//                    'url' => array(
//                        'base' => '*/*/delete',
//                        'params' => array('store' => $this->getRequest()->getParam('store')),
//                    ),
//                    'field' => 'id'
//                )
             ),
            'filter' => false,
            'sortable' => false,
            'index' => 'columnsmenu',
        ));

        return parent::_prepareColumns();
    }

    /*add columns massactions //select all*/
    protected function _prepareMassaction() {
        $this->setMassactionIdField('columnsmenu_id');
        $this->getMassactionBlock()->setFormFieldName('massid');
        $this->getMassactionBlock()->addItem('delete', array(
                        'label' => Mage::helper('columnsmenu')->__('Delete'),
                        'url' => $this->getUrl('*/*/massDelete'),
                        'confirm' => Mage::helper('columnsmenu')->__('Confirm Delete Have Selected Item !')
        ));
        return $this;
    }
    /**
     * return url row on grid
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit',array('id' => $row->getId()));
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }
}