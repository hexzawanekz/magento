<?php

class SM_Columnsmenu_Model_Resource_Columnsmenu extends Mage_Core_Model_Resource_Db_Abstract
{
    public function _construct()
    {
        $this->_init('columnsmenu/columnsmenu', 'columnsmenu_id');
    }
}