<?php


/** @var Mage_Core_Model_Resource_Setup $installer */
$installer = $this;

$installer->startSetup();

/** Order Cancellation Mail Template Petloft */
/** @var Mage_Adminhtml_Model_Email_Template $mailTemplateModel */
$mailTemplateModel = Mage::getModel('adminhtml/email_template');

$mailTemplateModel->loadByCode('Order Cancel Petloft');
$mailTemplateModel->setTemplateCode('Order Cancel Petloft');
$mailTemplateModel->setTemplateSubject('{{var store.getFrontendName()}}: Your order #{{var order.increment_id}} has been canceled');
$mailTemplateModel->setTemplateType(2);

$mailContent = <<<HTML
<meta charset="utf-8"/>
<div class="mail-container" style="width:600px; padding:0px 33px; font-family:  'Trebuchet MS', Tahoma; font-size: 14px; color: #3c3d41; margin: 0px;">
    <div class="mail-header" style="width:95%; min-height: 110px; max-height: 110px; position: relative; margin:0px auto;">
        <div class="mail-logo" style="width:220px; min-height: 110px; max-height: 110px; margin:0px auto;">
            <a href="http://petloft.com/"><img src="{{skin _area="frontend"  url="images/email/petloft-logo.png"}}" style="width:220px; max-height: 110px; min-height: 110px;" /></a>
        </div>

        <div class="english-below" style="min-height: 16px; max-height: 16px; position: absolute; bottom: 0px; line-height: 16px;">
            <span><img src="{{skin _area="frontend"  url="images/email/flag-en.png"}}" height="16px;"/> Scroll down for English version</span>
        </div>
    </div>

    <img style="width: 100%; margin-top:10px;" src="{{skin _area="frontend"  url="images/email/order_cancellation_petloft.png"}}" alt="Thank for your order" />

    <div class="mail-body" style="margin:50px auto; width: 95%;">
        <p>เรียน คุณ {{htmlescape var=$order.getCustomerName()}},</p>
<br/>
        <p>คุณได้ยกเลิกรายการเลขสั่งซื้อ #{{var order.increment_id}} เรียบร้อยแล้ว.</p>
        <p>ขอบคุณค่ะ</p>
<br/>
        <p>Warm Regards,</p>
        <p>Moxy Team</p>
    </div>

	<div class="navigation-th" style="margin-top: 24px; overflow:hidden;">
		<div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #006ab4; border-bottom: 2px solid #006ab4; padding: 0px 17px; overflow: hidden;">
			<span class="nav-item" style="border-right: 2px solid #006ab4; line-height: 33px; font-size: 18px; padding: 0px 28px;"><a href="http://petloft.com/th/dog-food.html" style="color: #3c3d41; text-decoration: none;">สุนัข</a></span>
			<span class="nav-item" style="border-right: 2px solid #006ab4; line-height: 33px; font-size: 18px; padding: 0px 28px;"><a href="http://petloft.com/th/cat-food.html" style="color: #3c3d41; text-decoration: none;">แมว</a></span>
			<span class="nav-item" style="border-right: 2px solid #006ab4; line-height: 33px; font-size: 18px; padding: 0px 28px;"><a href="http://petloft.com/th/fish-products.html" style="color: #3c3d41; text-decoration: none;">ปลา</a></span>
			<span class="nav-item" style="border-right: 2px solid #006ab4; line-height: 33px; font-size: 18px; padding: 0px 28px;"><a href="http://www.petloft.com/th/bird.html" style="color: #3c3d41; text-decoration: none;">นก</a></span>
			<span class="nav-item" style="line-height: 33px; font-size: 18px; padding: 0px 28px;"><a href="http://petloft.com/th/small-pets-products.html" style="color: #3c3d41; text-decoration: none;">สัตว์เลี้ยงขนาดเล็ก</a></span>
		</div>
	</div>

    <div class="email-footer" style="background: #bbb; margin-top: 8px; color: #fff; padding: 0px 15px;">
        <div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
            <p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px"><img src="{{skin _area="frontend"  url="images/email/mailbox.png"}}" style="width:18px;"/> ติดต่อเรา</p>
            <p style="font-size: 10px; margin: 0;">02-106-8222</p>
            <p style="margin: 0; font-size: 10px;color:#fff;"><a href="mailto:support@petloft.com" style="color:#fff;">support@petloft.com</a></p>
        </div>

        <div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
            <p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">ติดตามที่</p>
            <div class="follow-icons" style="width: 100%">
    <a href="https://www.facebook.com/petloftthai"><img src="{{skin _area="frontend"  url="images/email/facebook_follow.png"}}" style="width: 25px;" alt="Facebook fanpage"></a>
                <a href="https://twitter.com/PetLoft"><img src="{{skin _area="frontend"  url="images/email/twitter_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
                <a href="https://instagram.com/petloft"><img src="{{skin _area="frontend"  url="images/email/instagram_follow.png"}}" style="width: 25px;" alt="Instagram fanpage"></a>
                <a href="https://www.pinterest.com/petloft/"><img src="{{skin _area="frontend"  url="images/email/pinterest_follow.png"}}" style="width: 25px;" alt="Pinterest fanpage"></a>
                <a href="https://plus.google.com/110158894140498579470"><img src="{{skin _area="frontend"  url="images/email/google_follow.png"}}" style="width: 25px;" alt="Google fanpage"></a>
            </div>
        </div>

        <div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
            <p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">ช่องทางการชำระเงิน</p>
            <img src="{{skin _area="frontend"  url="images/email/payment-th.png"}}" alt="ช่องทางการชำระเงิน" style="width: 109px;">
        </div>

        <div class="clear" style="clear:both;"></div>
    </div>

    <div class="footer" style="padding: 0px 33px; margin-top: 18px;">
        <div class="copyr" style="float: left; font-size: 12px;">&copy;2015 Whatsnew Co.,Ltd. All Rights Reserved</div>
        <div class="links" style="float: right; font-size: 12px;">
            <a href="http://www.petloft.com/th/about-us/" style="color: #3c3d41;"><span>เกี่ยวกับเรา</span></a> |
            <a href="http://www.petloft.com/th/terms-and-conditions/" style="color: #3c3d41;"><span>ข้อกำหนดและเงื่อนไข</span></a> |
            <a href="http://www.petloft.com/th/privacy-policy/" style="color: #3c3d41;"><span>ความเป็นส่วนตัว</span></a>
        </div>
    </div>
    <div style="clear:both"></div>
    <hr style="margin: 40px auto; width: 95%;">

    <!--English Version-->
    <div class="mail-body" style="margin:50px auto; width: 95%;">
        <p>Dear {{htmlescape var=$order.getCustomerName()}},</p>

        <br/>
        <p>Your order #{{var order.increment_id}} has been successfully cancelled.</p>
        <p>We hope you enjoyed shopping with us.</p>
        <br>

        <p>Warm Regards,</p>
        <p>Moxy Team</p>
    </div>

	<div class="navigation-th" style="margin-top: 24px; overflow:hidden;">
		<div class="nav-container" style="min-height: 33px; max-height: 33px; border-top: 2px solid #006ab4; border-bottom: 2px solid #006ab4; padding: 0px 30px; overflow: hidden;">
			<span class="nav-item" style="border-right: 2px solid #006ab4; line-height: 33px; font-size: 18px; padding: 0px 29px;"><a href="http://petloft.com/en/dog-food.html" style="color: #3c3d41; text-decoration: none;">Dogs</a></span>
			<span class="nav-item" style="border-right: 2px solid #006ab4; line-height: 33px; font-size: 18px; padding: 0px 29px;"><a href="http://petloft.com/en/cat-food.html" style="color: #3c3d41; text-decoration: none;">Cats</a></span>
			<span class="nav-item" style="border-right: 2px solid #006ab4; line-height: 33px; font-size: 18px; padding: 0px 29px;"><a href="http://petloft.com/en/fish-products.html" style="color: #3c3d41; text-decoration: none;">Fish</a></span>
			<span class="nav-item" style="border-right: 2px solid #006ab4; line-height: 33px; font-size: 18px; padding: 0px 29px;"><a href="http://www.petloft.com/en/bird.html" style="color: #3c3d41; text-decoration: none;">Bird</a></span>
			<span class="nav-item" style="line-height: 33px; font-size: 18px; padding: 0px 29px;"><a href="http://petloft.com/en/small-pets-products.html" style="color: #3c3d41; text-decoration: none;">Small Pets</a></span>
		</div>
	</div>
    <div class="email-footer" style="background: #bbb; margin-top: 8px; color: #fff; padding: 0px 15px;">
        <div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
            <p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px"><img src="{{skin _area="frontend"  url="images/email/mailbox.png"}}" style="width:18px;"/> Contact Us</p>
            <p style="font-size: 10px; margin: 0;">02-106-8222</p>
            <p style="margin: 0; font-size: 10px;color:#fff;"><a href="mailto:support@petloft.com" style="color:#fff;">support@petloft.com</a></p>
        </div>

        <div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
            <p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">Follow Us</p>
            <div class="follow-icons" style="width: 100%">
    <a href="https://www.facebook.com/petloftthai"><img src="{{skin _area="frontend"  url="images/email/facebook_follow.png"}}" style="width: 25px;" alt="Facebook fanpage"></a>
                <a href="https://twitter.com/PetLoft"><img src="{{skin _area="frontend"  url="images/email/twitter_follow.png"}}" style="width: 25px;" alt="Twitter fanpage"></a>
                <a href="https://instagram.com/petloft"><img src="{{skin _area="frontend"  url="images/email/instagram_follow.png"}}" style="width: 25px;" alt="Instagram fanpage"></a>
                <a href="https://www.pinterest.com/petloft/"><img src="{{skin _area="frontend"  url="images/email/pinterest_follow.png"}}" style="width: 25px;" alt="Pinterest fanpage"></a>
                <a href="https://plus.google.com/110158894140498579470"><img src="{{skin _area="frontend"  url="images/email/google_follow.png"}}" style="width: 25px;" alt="Google fanpage"></a>
            </div>
        </div>

        <div class="footer-col" style="float:left; padding: 17px 24px; width: 141px;">
            <p style="font-size: 15px; font-weight: bold; margin: 0px 0px 0px">Payment Options</p>
            <img src="{{skin _area="frontend"  url="images/email/payment.png"}}" alt="Payment Options" style="width: 109px;">
        </div>

        <div class="clear" style="clear:both;"></div>
    </div>

    <div class="footer" style="padding: 0px 33px; margin-top: 18px;">
        <div class="copyr" style="float: left; font-size: 12px;">&copy;2015 Whatsnew Co.,Ltd. All Rights Reserved</div>
        <div class="links" style="float: right; font-size: 12px;">
            <a href="http://www.petloft.com/en/about-us/" style="color: #3c3d41;"><span>About Us</span></a> |
            <a href="http://www.petloft.com/en/terms-and-conditions/" style="color: #3c3d41;"><span>Term & Conditions</span></a> |
            <a href="http://www.petloft.com/en/privacy-policy/" style="color: #3c3d41;"><span>Privacy Policy</span></a>
        </div>
    </div>
    <div style="clear:both"></div>
</div>

HTML;

$mailTemplateModel->setTemplateText($mailContent);
$mailTemplateModel->save();

$installer->endSetup();