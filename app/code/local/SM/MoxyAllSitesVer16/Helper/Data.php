<?php

class SM_MoxyAllSitesVer16_Helper_Data extends Mage_Core_Helper_Abstract
{


    public function isInstallment($sku)
    {
        $skus = trim(Mage::getConfig('payment/normal2c2p/allow_for'));
        if ($skus) {
            $skusArray = explode(',', $skus);
            if (in_array($sku, $skusArray)) {
                return true;
            } else {
                return false;
            }
        }
        return true;
    }
}