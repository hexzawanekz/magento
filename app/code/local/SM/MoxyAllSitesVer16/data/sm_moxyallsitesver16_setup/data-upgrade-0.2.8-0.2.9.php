<?php

$en     = Mage::getModel('core/store')->load('en', 'code')->getId();// Petloft
$th     = Mage::getModel('core/store')->load('th', 'code')->getId();
$len    = Mage::getModel('core/store')->load('len', 'code')->getId();// Lafema
$lth    = Mage::getModel('core/store')->load('lth', 'code')->getId();
$ven    = Mage::getModel('core/store')->load('ven', 'code')->getId();// Venbi
$vth    = Mage::getModel('core/store')->load('vth', 'code')->getId();
$sen    = Mage::getModel('core/store')->load('sen', 'code')->getId();// Sanoga
$sth    = Mage::getModel('core/store')->load('sth', 'code')->getId();
$moxyen = Mage::getModel('core/store')->load('moxyen', 'code')->getId();// Moxy
$moxyth = Mage::getModel('core/store')->load('moxyth', 'code')->getId();

//==========================================================================
// SEO Keyword Moxy -> Petloft TH
//==========================================================================
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($moxyth, $withAdmin = false)
    ->addFieldToFilter('identifier', 'seo-keyword-petloft')
    ->getFirstItem();

if ($block->getId()) $block->delete();// if exists then delete
$content = <<<EOD
<div style="margin: 50px auto 30px;">
<p>&nbsp;</p>
<h2>PETLOFT by MOXY is Thailand&rsquo;s number 1 online destination for Pet Lovers.</h2>
<p>&nbsp;</p>
<p>Shopping at MOXY means that you will no longer have to drive around for hours looking for the best products for your pets. Everything can now be purchased from one place. We offer all essential product for your&nbsp;&nbsp;<strong><a href="{{store url='catalogsearch/result' _query='q=dog'}}">dogs</a> , <a href="{{store url='catalogsearch/result' _query='q=cat'}}">cats</a> , <a href="{{store url='fish-products.html'}}">fish</a> , <a href="{{store url='small-pets-products.html'}}">small pets</a> , reptile ,</strong> and <strong><a href="{{store url='bird-products.html'}}">bird</a></strong> products. From MOXY, you can get your&nbsp;<strong><a href="{{store url='dog-food.html'}}">dog food</a> , <a href="{{store url='cat-food.html'}}">cat food</a> , <a href="{{store url='cat-food/cat-food-type/cat-litter.html'}}">cat litter</a> , <a href="{{store url='catalogsearch/result' _query='q=toys'}}">toys</a> , <a href="{{store url='catalogsearch/result' _query='q=accessories'}}">accessories</a> , <a href="{{store url='catalogsearch/result' _query='q=clothing'}}">clothing</a> , <a href="{{store url='catalogsearch/result' _query='q=pet+care'}}">medicine</a> , <a href="{{store url='catalogsearch/result' _query='q=shampoo'}}">shampoo</a> ,</strong> and many other products. Some of our best selling brands include <strong> <a href="{{store url='catalogsearch/result' _query='q=pedigree'}}">Pedigree</a> , <a href="{{store url='catalogsearch/result' _query='q=royal+canin'}}">Royal Canin</a> , <a href="{{store url='catalogsearch/result' _query='q=pinnacle'}}">Pinnacle</a> , <a href="{{store url='catalogsearch/result' _query='q=maxima'}}">Maxima</a> , <a href="{{store url='catalogsearch/result' _query='q=smart+heart'}}">SmartHeart</a> , <a href="{{store url='catalogsearch/result' _query='q=perfecta'}}">Perfecta</a> , <a href="{{store url='catalogsearch/result' _query='q=meo'}}">Me-O</a> , <a href="{{store url='catalogsearch/result' _query='q=chicken+soup'}}">Chicken Soup</a> , <a href="{{store url='catalogsearch/result' _query='q=whiskas'}}">Whiskas</a> , <a href="{{store url='catalogsearch/result' _query='q=science+diet'}}">Science Diet</a> , <a href="{{store url='catalogsearch/result' _query='q=frontline'}}">Frontline</a> , <a href="{{store url='catalogsearch/result' _query='q=cesar'}}">Cesar</a> , <a href="{{store url='catalogsearch/result' _query='q=zeal'}}">Zeal</a> , <a href="{{store url='catalogsearch/result' _query='q=anf'}}">ANF</a> , <a href="{{store url='catalogsearch/result' _query='q=dog+n+joy'}}">Dog N&rsquo; Joy</a> </strong> , and many more.&nbsp;</p>
<p>MOXY is dedicated to provide the best customer service: easy, fast and reliable. Your satisfaction is our priority. Please feel free to contact our customer service at &hellip;&hellip;. for any information. Multiple payment methods are available such as credit card, Pay Pal, and cash-on-delivery. Your order will be delivered to your doorstep within 2 to 3 days in Bangkok and 3 to 5 days in other regions of Thailand. Make your shopping easier than ever!</p>
<p>MOXY is looking forward to serving you.&nbsp;</p>
</div>
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$block = Mage::getModel('cms/block');
$block->setTitle('SEO Keyword Petloft TH');
$block->setIdentifier('seo-keyword-petloft');
$block->setStores($moxyth);
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================

//==========================================================================
// SEO Keyword Moxy -> Petloft EN
//==========================================================================
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($moxyen, $withAdmin = false)
    ->addFieldToFilter('identifier', 'seo-keyword-petloft')
    ->getFirstItem();

if ($block->getId()) $block->delete();// if exists then delete

$content = <<<EOD
<div style="margin: 50px auto 30px;">
<p>&nbsp;</p>
<h2>Petloft รวมสินค้าสำหรับสัตว์เลี้ยงทุกประเภทไว้มากที่สุด โดย MOXY</h2>
<p>&nbsp;</p>
<p>ไม่ต้องเสียเวลาเดินหาซื้อและขนสินค้ากลับเองให้เมื่อยอีกต่อไป เพราะเรามีสินค้าสำหรับสัตว์เลี้ยงทุกชนิดไม่ว่าจะเป็น สุนุข แมว ปลา กระต่าย หนู สัตว์เลื้อยคลานและนก คุณสามารถซื้ออาหารหมา อาหารแมว ทรายแมว ของเล่น เครื่องประดับ เสื้อผ้า ยา และแชมพูให้กับสัตว์เลี้ยงของคุณได้ในคลิ๊กเดียว แบรนด์สินค้าขายดีของเราได้แก่&nbsp;&nbsp;<strong> <a href="{{store url='catalogsearch/result' _query='q=pedigree'}}">Pedigree</a> , <a href="{{store url='catalogsearch/result' _query='q=royal+canin'}}">Royal Canin</a> , <a href="{{store url='catalogsearch/result' _query='q=pinnacle'}}">Pinnacle</a> , <a href="{{store url='catalogsearch/result' _query='q=maxima'}}">Maxima</a> , <a href="{{store url='catalogsearch/result' _query='q=smart+heart'}}">SmartHeart</a> , <a href="{{store url='catalogsearch/result' _query='q=perfecta'}}">Perfecta</a> , <a href="{{store url='catalogsearch/result' _query='q=meo'}}">Me-O</a> , <a href="{{store url='catalogsearch/result' _query='q=chicken+soup'}}">Chicken Soup</a> , <a href="{{store url='catalogsearch/result' _query='q=whiskas'}}">Whiskas</a> , <a href="{{store url='catalogsearch/result' _query='q=science+diet'}}">Science Diet</a> , <a href="{{store url='catalogsearch/result' _query='q=frontline'}}">Frontline</a> , <a href="{{store url='catalogsearch/result' _query='q=cesar'}}">Cesar</a> , <a href="{{store url='catalogsearch/result' _query='q=zeal'}}">Zeal</a> , <a href="{{store url='catalogsearch/result' _query='q=anf'}}">ANF</a> , <a href="{{store url='catalogsearch/result' _query='q=dog+n+joy'}}">Dog N&rsquo; Joy</a> </strong> และอีกมากมาย</p>
<p>&nbsp;</p>
<p>สะดวก รวดเร็ว และมั่นใจได้ คือสิ่งที่ MOXY บริการให้คุณ เพราะความพึงพอใจของคุณคือสิ่งที่สำคัญที่สุดสำหรับเรา คุณสามารถติดต่อ MOXY ผ่านทางศูนย์บริการลูกค้าเพื่อสอบถามเกี่ยวกับข้อมูลต่างๆได้ตลอดเวลา และยังสามารถเลือกวิธีการชำระเงินได้ตามสะดวกไม่ว่าจะเป็น ชำระเงินผ่านบัตรเครดิตการ์ด Paypal หรือชำระเงินปลายทาง จากนั้นก็เตรียมตัวรับสินค้าที่จะถูกส่งตรงถึงหน้าบ้านคุณภายใน 2-3 วันทำการในเขตกรุงเทพฯ และ 3-5 วันทำการในเขตอื่น ไม่ว่าคุณจะอยู่ที่ไหน การช้อปปิ้งออนไลน์จะกลายเป็นเรื่องสนุกที่คุณทำได้ทุกวัน</p>
<p>MOXY พร้อมที่จะให้บริการคุณแล้ววันนี้</p>
</div>
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$block = Mage::getModel('cms/block');
$block->setTitle('SEO Keyword Petloft EN');
$block->setIdentifier('seo-keyword-petloft');
$block->setStores($moxyen);
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================

//==========================================================================
// SEO Keyword Moxy -> Sanoga TH
//==========================================================================
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($moxyth, $withAdmin = false)
    ->addFieldToFilter('identifier', 'seo-keyword-sanoga')
    ->getFirstItem();

if ($block->getId()) $block->delete();// if exists then delete

$content = <<<EOD
<div style="margin: 50px auto 30px;">
<p>Sanoga รวมสินค้าเพื่อสุขภาพและอุปกรณ์ออกกำลังกาย คัดสรรโดย MOXY</p>
<p>&nbsp;</p>
<p>คุณสามารถใช้เวลาได้อย่างเต็มที่ในการเลือกผลิตภัณฑ์ที่ต้องการ MOXY มีสินค้าให้คุณเลือกอย่างจุใจในราคาสุดคุ้ม และเราได้จัดทำข้อมูลอย่างละเอียดสำหรับสินค้าแต่ละชนิด พร้อมทั้งวิธีการสั่งซื้อที่สะดวกสบายตลอด 24 ชั่วโมง ทำให้การดูแลสุขภาพของคุณครั้งนี้ง่ายขึ้นกว่าเดิมในคลิ๊กเดียว</p>
<p>&nbsp;</p>
<p>MOXY มีสินค้าเพื่อสุขภาพและอุปกรณ์ออกกำลังกายครบครัน ไม่ว่าจะเป็นอาหารเสริม วิตามิน คอลลาเจน ผลิตภัณฑ์เสริมสมรรถภาพร่างกาย อาหารเสริมเพื่อการออกกำลังกายและฟิตเนส เวย์โปรตีน และอาหารเสริมลดน้ำหนักจากกว่า 1,000 แบรนดํดังทั่วโลก เช่น Blackmores, Beauty Wise, Body Shape, CLG500, Centrum, Collagen Star, Colly, Dr.Absolute, DYMATIZE, Durex, Eucerin, Genesis, Hi-Balanz, K-Palette, Meiji, Mega We Care, Okamoto, ProFlex, Real Elixir, Sebamed, Seoul Secret, Taurus และอื่นๆ</p>
<p>&nbsp;</p>
<p>สินค้าทุกชิ้นของ MOXY ได้รับมาตราฐานการรับรองจากองค์การอาหารและยาแห่งประเทศไทย(อย.) จึงมั่นใจได้ในคุณภาพและความปลอดภัย</p>
<p>&nbsp;</p>
<p>สะดวก รวดเร็ว และมั่นใจได้ คือสิ่งที่ MOXY บริการให้คุณ เพราะความพึงพอใจของคุณคือสิ่งที่สำคัญที่สุดสำหรับเรา คุณสามารถติดต่อ MOXY ผ่านทางศูนย์บริการลูกค้าเพื่อสอบถามเกี่ยวกับข้อมูลต่างๆได้ตลอดเวลา และยังสามารถเลือกวิธีการชำระเงินได้ตามสะดวกไม่ว่าจะเป็น ชำระเงินผ่านบัตรเครดิตการ์ด Paypal หรือชำระเงินปลายทาง จากนั้นก็เตรียมตัวรับสินค้าที่จะถูกส่งตรงถึงหน้าบ้านคุณภายใน 2-3 วันทำการในเขตกรุงเทพฯ และ 3-5 วันทำการในเขตอื่น ไม่ว่าคุณจะอยู่ที่ไหน การช้อปปิ้งออนไลน์จะกลายเป็นเรื่องสนุกที่คุณทำได้ทุกวัน</p>
<p>&nbsp;</p>
<p>MOXY พร้อมที่จะให้บริการคุณแล้ววันนี้</p>
<p>&nbsp;</p>
<p>ผลลัพท์ที่ได้ขึ้นอยู่กับแต่ละบุคคล</p>
</div>
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$block = Mage::getModel('cms/block');
$block->setTitle('SEO Keyword Sanoga TH');
$block->setIdentifier('seo-keyword-sanoga');
$block->setStores($moxyth);
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================

//==========================================================================
// SEO Keyword Moxy -> Sanoga EN
//==========================================================================
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($moxyen, $withAdmin = false)
    ->addFieldToFilter('identifier', 'seo-keyword-sanoga')
    ->getFirstItem();

if ($block->getId()) $block->delete();// if exists then delete

$content = <<<EOD
<div style="margin: 50px auto 30px;">
<p>Sanoga รวมสินค้าเพื่อสุขภาพและอุปกรณ์ออกกำลังกาย คัดสรรโดย MOXY</p>
<p>&nbsp;</p>
<p>คุณสามารถใช้เวลาได้อย่างเต็มที่ในการเลือกผลิตภัณฑ์ที่ต้องการ MOXY มีสินค้าให้คุณเลือกอย่างจุใจในราคาสุดคุ้ม และเราได้จัดทำข้อมูลอย่างละเอียดสำหรับสินค้าแต่ละชนิด พร้อมทั้งวิธีการสั่งซื้อที่สะดวกสบายตลอด 24 ชั่วโมง ทำให้การดูแลสุขภาพของคุณครั้งนี้ง่ายขึ้นกว่าเดิมในคลิ๊กเดียว</p>
<p>&nbsp;</p>
<p>MOXY มีสินค้าเพื่อสุขภาพและอุปกรณ์ออกกำลังกายครบครัน ไม่ว่าจะเป็นอาหารเสริม วิตามิน คอลลาเจน ผลิตภัณฑ์เสริมสมรรถภาพร่างกาย อาหารเสริมเพื่อการออกกำลังกายและฟิตเนส เวย์โปรตีน และอาหารเสริมลดน้ำหนักจากกว่า 1,000 แบรนดํดังทั่วโลก เช่น Blackmores, Beauty Wise, Body Shape, CLG500, Centrum, Collagen Star, Colly, Dr.Absolute, DYMATIZE, Durex, Eucerin, Genesis, Hi-Balanz, K-Palette, Meiji, Mega We Care, Okamoto, ProFlex, Real Elixir, Sebamed, Seoul Secret, Taurus และอื่นๆ</p>
<p>&nbsp;</p>
<p>สินค้าทุกชิ้นของ MOXY ได้รับมาตราฐานการรับรองจากองค์การอาหารและยาแห่งประเทศไทย(อย.) จึงมั่นใจได้ในคุณภาพและความปลอดภัย</p>
<p>&nbsp;</p>
<p>สะดวก รวดเร็ว และมั่นใจได้ คือสิ่งที่ MOXY บริการให้คุณ เพราะความพึงพอใจของคุณคือสิ่งที่สำคัญที่สุดสำหรับเรา คุณสามารถติดต่อ MOXY ผ่านทางศูนย์บริการลูกค้าเพื่อสอบถามเกี่ยวกับข้อมูลต่างๆได้ตลอดเวลา และยังสามารถเลือกวิธีการชำระเงินได้ตามสะดวกไม่ว่าจะเป็น ชำระเงินผ่านบัตรเครดิตการ์ด Paypal หรือชำระเงินปลายทาง จากนั้นก็เตรียมตัวรับสินค้าที่จะถูกส่งตรงถึงหน้าบ้านคุณภายใน 2-3 วันทำการในเขตกรุงเทพฯ และ 3-5 วันทำการในเขตอื่น ไม่ว่าคุณจะอยู่ที่ไหน การช้อปปิ้งออนไลน์จะกลายเป็นเรื่องสนุกที่คุณทำได้ทุกวัน</p>
<p>&nbsp;</p>
<p>MOXY พร้อมที่จะให้บริการคุณแล้ววันนี้</p>
<p>&nbsp;</p>
<p>ผลลัพท์ที่ได้ขึ้นอยู่กับแต่ละบุคคล</p>
</div>
EOD;
/** @var Mage_Cms_Model_Block */
$block = Mage::getModel('cms/block');
$block->setTitle('SEO Keyword Sanoga EN');
$block->setIdentifier('seo-keyword-sanoga');
$block->setStores($moxyen);
$block->setIsActive(1);
$block->setContent($content);
$block->save();

//==========================================================================
//==========================================================================
//==========================================================================


//==========================================================================
// SEO Keyword Moxy -> Venbi TH
//==========================================================================
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($moxyth, $withAdmin = false)
    ->addFieldToFilter('identifier', 'seo-keyword-venbi')
    ->getFirstItem();

if ($block->getId()) $block->delete();// if exists then delete

$content = <<<EOD
<div style="margin: 50px auto 30px;">
<p>Venbi รวมสินค้าแม่และเด็กที่ดีที่สุด คัดสรรโดย MOXY</p>
<p>&nbsp;</p>
<p>MOXY ประหยัดเวลาในการตระเวนหาของที่ดีที่สุดให้ลูกคุณ เพราะเราคัดสรรแล้วว่านี่แหละ คือสิ่งที่ดีที่สุด ครบทุกอย่างในเว็บเดียว ไม่ว่าจะเป็นผ้าอ้อม เสื้อผ้า เบบี้แคร์ ของเล่น และนมผง สินค้าสำหรับเด็กแรกเกิดถึง 2ขวบ คุณแม่จึงมีเวลาอยู่กับลูกมากขึ้นนั่นเอง</p>
<p>&nbsp;</p>
<p>สินค้าทั้งหมดของเราเป็นของแท้ 100% ซึ่งเราติดต่อกับแบรนด์ และตัวแทนจำหน่ายโดยตรงกับแบรนด์ดังทั่วโลก เช่น <a title="Mamy Poko" href="http://www.venbi.com/en/diapers/popular-brands/mamy-poko.html">Mamy Poko</a> , <a title="Merries" href="http://www.venbi.com/en/diapers/popular-brands/merries.html">Merries</a> , <a title="Enfalac" href="http://www.venbi.com/en/formula-feeding/popular-brands/enfa.html">Enfalac</a> , <a title="Enfapro" href="http://www.venbi.com/en/catalogsearch/result/?q=enfa+pro">Enfapro</a> , <a title="Drypers" href="http://www.venbi.com/en/diapers/popular-brands/drypers.html">Drypers</a> , <a title="Huggies" href="http://www.venbi.com/en/diapers/popular-brands/huggies.html">Huggies</a> , <a title="DG" href="http://www.venbi.com/en/formula-feeding/popular-brands/dg.html">DG</a> , <a title="Peachy" href="http://www.venbi.com/en/formula-feeding/popular-brands/peachy.html">Peachy</a> , <a title="Badger" href="http://www.venbi.com/en/catalogsearch/result/?q=badger">Badger</a> , <a title="Babylove" href="http://www.venbi.com/en/diapers/popular-brands/baby-love.html">Babylove</a> , <a title="Sassy" href="http://www.venbi.com/en/toys/popular-brands/sassy.html">Sassy</a> , <a title="Similac" href="http://www.venbi.com/en/formula-feeding/popular-brands/similac.html">Similac</a> , <a title="Pigeon" href="http://www.venbi.com/en/baby-care/popular-brands/pigeon.html">Pigeon</a> , <a title="S-26" href="http://www.venbi.com/en/formula-feeding/popular-brands/s-26.html">S-26</a> , <a title="Bright Starts" href="http://www.venbi.com/en/toys/popular-brands/bright-starts.html">Bright Starts</a> , <a title="Tiny love" href="http://www.venbi.com/en/toys/popular-brands/tiny-love.html">Tiny love</a> และแบรนด์ดังอื่นๆอีกมากมาย</p>
<p>&nbsp;</p>
<p>สะดวก รวดเร็ว และมั่นใจได้ คือสิ่งที่ MOXY บริการให้คุณ เพราะความพึงพอใจของคุณคือสิ่งที่สำคัญที่สุดสำหรับเรา คุณสามารถติดต่อ MOXY ผ่านทางศูนย์บริการลูกค้าเพื่อสอบถามเกี่ยวกับข้อมูลต่างๆได้ตลอดเวลา และยังสามารถเลือกวิธีการชำระเงินได้ตามสะดวกไม่ว่าจะเป็น ชำระเงินผ่านบัตรเครดิตการ์ด Paypal หรือชำระเงินปลายทาง จากนั้นก็เตรียมตัวรับสินค้าที่จะถูกส่งตรงถึงหน้าบ้านคุณภายใน 2-3 วันทำการในเขตกรุงเทพฯ และ 3-5 วันทำการในเขตอื่น ไม่ว่าคุณจะอยู่ที่ไหน การช้อปปิ้งออนไลน์จะกลายเป็นเรื่องสนุกที่คุณทำได้ทุกวัน</p>
<p>&nbsp;</p>
<p>MOXY พร้อมที่จะให้บริการคุณแล้ววันนี้</p>
</div>
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$block = Mage::getModel('cms/block');
$block->setTitle('SEO Keyword Venbi TH');
$block->setIdentifier('seo-keyword-venbi');
$block->setStores($moxyth);
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================

//==========================================================================
// SEO Keyword Moxy -> Venbi EN
//==========================================================================
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($moxyen, $withAdmin = false)
    ->addFieldToFilter('identifier', 'seo-keyword-venbi')
    ->getFirstItem();

if ($block->getId()) $block->delete();// if exists then delete

$content = <<<EOD
<div style="margin: 50px auto 30px;">
<p>VENBI is MOXY&rsquo;s Mom &amp; Baby shopping website in Thailand.</p>
<p>&nbsp;</p>
<p>Shopping with us means that you will no longer have to drive around for hours looking for the best products for your babies. Everything can now be purchased from one place. We sell baby products for age groups between infancy up to 2 years of age. From Moxy, you can get your <a title="diapers" href="http://www.venbi.com/en/diapers.html">diapers</a> , <a title="wipes" href="http://www.venbi.com/en/baby-care/type/baby-wipes.html">wipes</a> , <a title="cloths" href="http://www.venbi.com/en/clothing-46/clothing/clothing.html">cloths</a> , <a title="bathing products" href="http://www.venbi.com/en/catalogsearch/result/?q=bath">bathing products</a> , <a title="toys" href="http://www.venbi.com/en/toys.html">toys</a> , accessories for moms, <a title="powdered milk" href="http://www.venbi.com/en/formula-feeding/formula/formula-baby.html">powdered milk</a> , and many other products.</p>
<p>&nbsp;</p>
<p>All of our products are 100% authentic and we work directly with international brands to distribute their products. Our wide selection includes <a title="Mamy Poko" href="http://www.venbi.com/en/diapers/popular-brands/mamy-poko.html">Mamy Poko</a> , <a title="Merries" href="http://www.venbi.com/en/diapers/popular-brands/merries.html">Merries</a> , <a title="Enfalac" href="http://www.venbi.com/en/formula-feeding/popular-brands/enfa.html">Enfalac</a> , <a title="Enfapro" href="http://www.venbi.com/en/catalogsearch/result/?q=enfa+pro">Enfapro</a> , <a title="Drypers" href="http://www.venbi.com/en/diapers/popular-brands/drypers.html">Drypers</a> , <a title="Huggies" href="http://www.venbi.com/en/diapers/popular-brands/huggies.html">Huggies</a> , <a title="DG" href="http://www.venbi.com/en/formula-feeding/popular-brands/dg.html">DG</a> , <a title="Peachy" href="http://www.venbi.com/en/formula-feeding/popular-brands/peachy.html">Peachy</a> , <a title="Badger" href="http://www.venbi.com/en/catalogsearch/result/?q=badger">Badger</a> , <a title="Babylove" href="http://www.venbi.com/en/diapers/popular-brands/baby-love.html">Babylove</a> , <a title="Sassy" href="http://www.venbi.com/en/toys/popular-brands/sassy.html">Sassy</a> , <a title="Similac" href="http://www.venbi.com/en/formula-feeding/popular-brands/similac.html">Similac</a> , <a title="Pigeon" href="http://www.venbi.com/en/baby-care/popular-brands/pigeon.html">Pigeon</a> , <a title="S-26" href="http://www.venbi.com/en/formula-feeding/popular-brands/s-26.html">S-26</a> , <a title="Bright Starts" href="http://www.venbi.com/en/toys/popular-brands/bright-starts.html">Bright Starts</a> , <a title="Tiny love" href="http://www.venbi.com/en/toys/popular-brands/tiny-love.html">Tiny love</a> &nbsp;and many more.</p>
<p>&nbsp;</p>
<p>As the online destination for Women in Thailand, MOXY is dedicated to provide the best customer service: easy, fast and reliable. Your satisfaction is our priority. Please feel free to contact our customer service for any information. Multiple payment methods are available such as credit card, Pay Pal, and cash-on-delivery. Your order will be delivered to your doorstep within 2 to 3 days in Bangkok and 3 to 5 days in other regions of Thailand. Make your shopping easier than ever!</p>
<p>&nbsp;</p>
<p>MOXY is looking forward to serving you.</p>
</div>
EOD;
/** @var Mage_Cms_Model_Block */
$block = Mage::getModel('cms/block');
$block->setTitle('SEO Keyword Venbi EN');
$block->setIdentifier('seo-keyword-venbi');
$block->setStores($moxyen);
$block->setIsActive(1);
$block->setContent($content);
$block->save();

//==========================================================================
//==========================================================================
//==========================================================================

//==========================================================================
// SEO Keyword Moxy -> Lafema TH
//==========================================================================
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($moxyth, $withAdmin = false)
    ->addFieldToFilter('identifier', 'seo-keyword-lafema')
    ->getFirstItem();

if ($block->getId()) $block->delete();// if exists then delete

$content = <<<EOD
<div style="margin: 50px auto 30px;">
<p>LAFEMA by MOXY รวมเครื่องสำอาง น้ำหอม และผลิตภัณฑ์เพื่อความงามของ คัดสรรโดย MOXY เพื่อสาวๆ โดยเฉพาะ</p>
<p>&nbsp;</p>
<p>เราเชื่อว่าความสวยทำให้ผู้หญิงรู้สึกมั่นใจ และสาวๆทุกคนสามารถสวยได้ในสไตล์ของตัวเอง MOXY จะมาเป็นตัวช่วยของคุณในการแนะนำสินค้าที่ดีที่สุด และเหมาะกับคุณที่สุด เพื่อคุณจะได้สวยและดูดีที่สุดในแบบของคุณเอง เผยออร่าที่อยู่ในตัวคุณผ่านผิวเปล่งประกาย ริมฝีปากอวบอิ่ม และดวงตาที่คมชัดให้คนอื่นได้ชื่นชม สนุกไปกับการโชว์ความสวยในแบบของคุณให้โลกได้รู้และค้นพบความงามที่ซ่อนอยู่โดยที่คุณไม่เคยรู้มาก่อน ทุกความลับเรื่องความสวย MOXY รวมไว้ที่นี่ที่เดียว</p>
<p>&nbsp;</p>
<p>สินค้าทั้งหมดของเราเป็นของแท้ 100% ซึ่งเราติดต่อกับแบรนด์ และตัวแทนจำหน่ายโดยตรงกับแบรนด์ดังกว่า 100 แบรนด์ทั่วโลก ไม่ว่าจะเป็น Elizabeth Arden, L&rsquo;Oreal, MAYBELLINE, GARNIER, NIVEA, Jergens, Olay, &nbsp;PANTENE, POND&rsquo;S, Vasiline, TREsemm&eacute;, PHYSICIAN FORMULA, Bioderma, Evian, Bior&eacute;, Kleenex, Fracora, Sleek, Wet &lsquo;n Wild<br /> และแบรนด์ดังอื่นๆอีกมากมาย</p>
<p>&nbsp;</p>
<p>สะดวก รวดเร็ว และมั่นใจได้ คือสิ่งที่ MOXY บริการให้คุณ เพราะความพึงพอใจของคุณคือสิ่งที่สำคัญที่สุดสำหรับเรา คุณสามารถติดต่อ MOXY ผ่านทางศูนย์บริการลูกค้าเพื่อสอบถามเกี่ยวกับข้อมูลต่างๆได้ตลอดเวลา และยังสามารถเลือกวิธีการชำระเงินได้ตามสะดวกไม่ว่าจะเป็น ชำระเงินผ่านบัตรเครดิตการ์ด Paypal หรือชำระเงินปลายทาง จากนั้นก็เตรียมตัวรับสินค้าที่จะถูกส่งตรงถึงหน้าบ้านคุณภายใน 2-3 วันทำการในเขตกรุงเทพฯ และ 3-5 วันทำการในเขตอื่น ไม่ว่าคุณจะอยู่ที่ไหน การช้อปปิ้งออนไลน์จะกลายเป็นเรื่องสนุกที่คุณทำได้ทุกวัน</p>
<p>&nbsp;</p>
<p>MOXY พร้อมที่จะให้บริการคุณแล้ววันนี้</p>
</div>
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$block = Mage::getModel('cms/block');
$block->setTitle('SEO Keyword Lafema TH');
$block->setIdentifier('seo-keyword-lafema');
$block->setStores($moxyth);
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================

//==========================================================================
// SEO Keyword Moxy -> Lafema EN
//==========================================================================
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($moxyen, $withAdmin = false)
    ->addFieldToFilter('identifier', 'seo-keyword-lafema')
    ->getFirstItem();

if ($block->getId()) $block->delete();// if exists then delete

$content = <<<EOD
<div style="margin: 50px auto 30px;">
<p>LAFEMA is MOXY&rsquo;s Beauty, Cosmetics and Fragrance shopping website in Thailand.</p>
<p>&nbsp;</p>
<p>Because Beauty gives Confidence, we believe that everybody should be able to be the best version of themselves. MOXY helps you chose the right products to be pretty and look young and healthy. Glow with beautiful skin, lips, and eyes. Be unforgettable thanks to seductive fragrance. Have fun and express yourself with colorful nails. Moisture and protect your skin for long-lasting youth and softness. All humans are different and beautiful in their own, unique way. MOXY reveals Thailand&rsquo;s best beauty secrets. Discover what will make YOU shine!</p>
<p>&nbsp;</p>
<p>All of our products are 100% authentic and we work directly with international brands to distribute their products. Our wide selection includes more than 100 brands, including M.A.C., SKII, Dior, Lancome, Yves Saint Laurent, Urban Decay, Sleek, Clinique, CandyDoll, Catrice, Makeup Forever, Bisous Bisous, NYX, Tarte, Smasbox, Stila, Jill Stuart, Bareminerals, Sisley, Chanel, Giorgio Armani, Marc Jacobs, Tom Ford, Burberry,, Shiseido, Suqqu, Nars, Illamasqua, Laura Mercier, and many others.</p>
<p>&nbsp;</p>
<p>As the online destination for Women in Thailand, MOXY is dedicated to provide the best customer service: easy, fast and reliable. Your satisfaction is our priority. Please feel free to contact our customer service for any information. Multiple payment methods are available such as credit card, Pay Pal, and cash-on-delivery. Your order will be delivered to your doorstep within 2 to 3 days in Bangkok and 3 to 5 days in other regions of Thailand. Make your shopping easier than ever!</p>
<p>&nbsp;</p>
<p>MOXY is looking forward to serving you.</p>
</div>
EOD;
/** @var Mage_Cms_Model_Block */
$block = Mage::getModel('cms/block');
$block->setTitle('SEO Keyword Lafema EN');
$block->setIdentifier('seo-keyword-lafema');
$block->setStores($moxyen);
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================

//==========================================================================
// SEO Keyword Moxy -> Fashion TH
//==========================================================================
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($moxyth, $withAdmin = false)
    ->addFieldToFilter('identifier', 'seo-keyword-fashion')
    ->getFirstItem();

if ($block->getId()) $block->delete();// if exists then delete

$content = <<<EOD
<div style="margin: 50px auto 30px;">
<p>MOXY รักแฟชั่น! เครื่องประดับ แว่นตา กระเป๋า คลัช และชุดชั้นในสุดเซ็กซี่ที่ทำให้สาวๆโดดเด่นมีสไตล์</p>
<p>&nbsp;</p>
<p>ช้อปสินค้าดีไซน์จากไทยดีไซน์เนอร์และแบรนด์ดังจากต่างประเทศ สินค้าทั้งหมดของเราเป็นของแท้ 100% ซึ่งเราติดต่อกับแบรนด์ และตัวแทนจำหน่ายโดยตรง</p>
<p>&nbsp;</p>
<p>สะดวก รวดเร็ว และมั่นใจได้ คือสิ่งที่ MOXY บริการให้คุณ เพราะความพึงพอใจของคุณคือสิ่งที่สำคัญที่สุดสำหรับเรา คุณสามารถติดต่อ MOXY ผ่านทางศูนย์บริการลูกค้าเพื่อสอบถามเกี่ยวกับข้อมูลต่างๆได้ตลอดเวลา และยังสามารถเลือกวิธีการชำระเงินได้ตามสะดวกไม่ว่าจะเป็น ชำระเงินผ่านบัตรเครดิตการ์ด Paypal หรือชำระเงินปลายทาง จากนั้นก็เตรียมตัวรับสินค้าที่จะถูกส่งตรงถึงหน้าบ้านคุณภายใน 2-3 วันทำการในเขตกรุงเทพฯ และ 3-5 วันทำการในเขตอื่น ไม่ว่าคุณจะอยู่ที่ไหน การช้อปปิ้งออนไลน์จะกลายเป็นเรื่องสนุกที่คุณทำได้ทุกวัน</p>
<p>&nbsp;</p>
<p>MOXY พร้อมที่จะให้บริการคุณแล้ววันนี้</p>
</div>
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$block = Mage::getModel('cms/block');
$block->setTitle('SEO Keyword Fashion TH');
$block->setIdentifier('seo-keyword-fashion');
$block->setStores($moxyth);
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================

//==========================================================================
// SEO Keyword Moxy -> Fashion EN
//==========================================================================
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($moxyen, $withAdmin = false)
    ->addFieldToFilter('identifier', 'seo-keyword-fashion')
    ->getFirstItem();

if ($block->getId()) $block->delete();// if exists then delete

$content = <<<EOD
<div style="margin: 50px auto 30px;">
<p>MOXY loves Fashion Accessories! Our selection covers jewelry, eyewear, bags, clutches and lingerie.</p>
<p>&nbsp;</p>
<p>Discover the best of Thai designers and International brands. All of our products are 100% authentic and we work directly with the brands to distribute their products.</p>
<p>&nbsp;</p>
<p>As the online destination for Women in Thailand, MOXY is dedicated to provide the best customer service: easy, fast and reliable. Your satisfaction is our priority. Please feel free to contact our customer service for any information. Multiple payment methods are available such as credit card, Pay Pal, and cash-on-delivery. Your order will be delivered to your doorstep within 2 to 3 days in Bangkok and 3 to 5 days in other regions of Thailand. Make your shopping easier than ever!</p>
<p>&nbsp;</p>
<p>MOXY is looking forward to serving you.</p>
</div>
EOD;
/** @var Mage_Cms_Model_Block */
$block = Mage::getModel('cms/block');
$block->setTitle('SEO Keyword Fashion EN');
$block->setIdentifier('seo-keyword-fashion');
$block->setStores($moxyen);
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================


//==========================================================================
// SEO Keyword Moxy -> Home Living TH
//==========================================================================
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($moxyth, $withAdmin = false)
    ->addFieldToFilter('identifier', 'seo-keyword-home-living')
    ->getFirstItem();

if ($block->getId()) $block->delete();// if exists then delete

$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($moxyth, $withAdmin = false)
    ->addFieldToFilter('identifier', 'seo-keyword-homeliving')
    ->getFirstItem();

if ($block->getId()) $block->delete();// if exists then delete


$content = <<<EOD
<div style="margin: 50px auto 30px;">
<p>บ้านคือวิมานของเรา MOXY เลือกสรรของแต่งบ้าน เฟอร์นิเจอร์ และเครื่องใช้ในบ้านที่มอบความรู้สึกสะดวกและสบายให้กับทุกห้องในบ้านของคุณ ไม่ว่าจะเป็นห้องนั่งเล่น ห้องนอน ห้องน้ำ ห้องครัว และห้องทานอาหาร</p>
<p>&nbsp;</p>
<p>สินค้าทั้งหมดของเราเป็นของแท้ 100% ซึ่งเราติดต่อกับแบรนด์ และตัวแทนจำหน่ายโดยตรงกับแบรนด์ดังทั่วโลก เช่น Henry Cats &amp; Friends, On Time, Springmate, Tin Home Toy, Pakamian, Ikea อื่นแบรนด์ดังอื่นๆอีกมาก</p>
<p>&nbsp;</p>
<p>สะดวก รวดเร็ว และมั่นใจได้ คือสิ่งที่ MOXY บริการให้คุณ เพราะความพึงพอใจของคุณคือสิ่งที่สำคัญที่สุดสำหรับเรา คุณสามารถติดต่อ MOXY ผ่านทางศูนย์บริการลูกค้าเพื่อสอบถามเกี่ยวกับข้อมูลต่างๆได้ตลอดเวลา และยังสามารถเลือกวิธีการชำระเงินได้ตามสะดวกไม่ว่าจะเป็น ชำระเงินผ่านบัตรเครดิตการ์ด Paypal หรือชำระเงินปลายทาง จากนั้นก็เตรียมตัวรับสินค้าที่จะถูกส่งตรงถึงหน้าบ้านคุณภายใน 2-3 วันทำการในเขตกรุงเทพฯ และ 3-5 วันทำการในเขตอื่น ไม่ว่าคุณจะอยู่ที่ไหน การช้อปปิ้งออนไลน์จะกลายเป็นเรื่องสนุกที่คุณทำได้ทุกวัน</p>
<p>&nbsp;</p>
<p>MOXY พร้อมที่จะให้บริการคุณแล้ววันนี้</p>
</div>
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$block = Mage::getModel('cms/block');
$block->setTitle('SEO Keyword Home Living TH');
$block->setIdentifier('seo-keyword-home-living');
$block->setStores($moxyth);
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================

//==========================================================================
// SEO Keyword Moxy -> Home Living EN
//==========================================================================
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($moxyen, $withAdmin = false)
    ->addFieldToFilter('identifier', 'seo-keyword-home-living')
    ->getFirstItem();

if ($block->getId()) $block->delete();// if exists then delete

$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($moxyen, $withAdmin = false)
    ->addFieldToFilter('identifier', 'seo-keyword-homeliving')
    ->getFirstItem();

if ($block->getId()) $block->delete();// if exists then delete

$content = <<<EOD
<div style="margin: 50px auto 30px;">
<p>There is no place like Home, Sweet Home. Moxy has selected everything you need for a cosy and convenient living, from Home D&eacute;cor and furniture to Home Appliances. Every room is covered: living room, bedding, bath, kitchen and dining, as well as lighting and appliances.</p>
<p>&nbsp;</p>
<p>All of our products are 100% authentic and we work directly with international brands to distribute their products. Our wide selection includes brands like Henry Cats &amp; Friends, On Time, Springmate, Tin Home Toy, Pakamian, Ikea and many others.</p>
<p>&nbsp;</p>
<p>As the online destination for Women in Thailand, MOXY is dedicated to provide the best customer service: easy, fast and reliable. Your satisfaction is our priority. Please feel free to contact our customer service for any information. Multiple payment methods are available such as credit card, Pay Pal, and cash-on-delivery. Your order will be delivered to your doorstep within 2 to 3 days in Bangkok and 3 to 5 days in other regions of Thailand. Make your shopping easier than ever!</p>
<p>&nbsp;</p>
<p>MOXY is looking forward to serving you.</p>
</div>
EOD;
/** @var Mage_Cms_Model_Block */
$block = Mage::getModel('cms/block');
$block->setTitle('SEO Keyword Home Living EN');
$block->setIdentifier('seo-keyword-home-living');
$block->setStores($moxyen);
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================

//==========================================================================
// SEO Keyword Moxy -> Electronics TH
//==========================================================================
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($moxyth, $withAdmin = false)
    ->addFieldToFilter('identifier', 'seo-keyword-gadgets-electronics')
    ->getFirstItem();

if ($block->getId()) $block->delete();// if exists then delete

$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($moxyth, $withAdmin = false)
    ->addFieldToFilter('identifier', 'seo-keyword-electronics')
    ->getFirstItem();

if ($block->getId()) $block->delete();// if exists then delete


$content = <<<EOD
<div style="margin: 50px auto 30px;">
<p>แก็ตเจ็ตและอุปกรณ์ไฮเทคนี่แหละที่ MOXY หลงใหล ช้อปสินค้าอิเล็คทรอนิกส์มีสไตล์กับเรา</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>ที่ MOXY เรามีมือถือ แท็ปเล็ต คอมพิวเตอร์โน๊ตบุค หูฟัง ลำโพง กล้องโพลารอยด์ อุปกรณ์ชาร์จไฟ เพาเวอร์แบงค์ และสินค้าอิเล็คทรอนิกส์ทุกชิ้นที่มีดีไซน์โดดเด่นและแตกต่าง บ่งบอกความเป็นตัวตนของสาวๆที่ใช้ ไม่ว่าหยิบขึ้นมาใช้ตอนไหนเพื่อนๆก็ต้องทัก</p>
<p>&nbsp;</p>
<p>สินค้าทั้งหมดของเราเป็นของแท้ 100% ซึ่งเราติดต่อกับแบรนด์ และตัวแทนจำหน่ายโดยตรงกับแบรนด์ดังทั่วโลก เช่น Apple, Samsung, Arken, House of Marley, Ihealth, Moleskine และแบรนด์ดังอื่นๆอีกมากมาย</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>สะดวก รวดเร็ว และมั่นใจได้ คือสิ่งที่ MOXY บริการให้คุณ เพราะความพึงพอใจของคุณคือสิ่งที่สำคัญที่สุดสำหรับเรา คุณสามารถติดต่อ MOXY ผ่านทางศูนย์บริการลูกค้าเพื่อสอบถามเกี่ยวกับข้อมูลต่างๆได้ตลอดเวลา และยังสามารถเลือกวิธีการชำระเงินได้ตามสะดวกไม่ว่าจะเป็น ชำระเงินผ่านบัตรเครดิตการ์ด Paypal หรือชำระเงินปลายทาง จากนั้นก็เตรียมตัวรับสินค้าที่จะถูกส่งตรงถึงหน้าบ้านคุณภายใน 2-3 วันทำการในเขตกรุงเทพฯ และ 3-5 วันทำการในเขตอื่น ไม่ว่าคุณจะอยู่ที่ไหน การช้อปปิ้งออนไลน์จะกลายเป็นเรื่องสนุกที่คุณทำได้ทุกวัน</p>
<p>&nbsp;</p>
<p>MOXY พร้อมที่จะให้บริการคุณแล้ววันนี้</p>
</div>
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$block = Mage::getModel('cms/block');
$block->setTitle('SEO Keyword Electronics TH');
$block->setIdentifier('seo-keyword-gadgets-electronics');
$block->setStores($moxyth);
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================

//==========================================================================
// SEO Keyword Moxy -> Electronics EN
//==========================================================================
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($moxyen, $withAdmin = false)
    ->addFieldToFilter('identifier', 'seo-keyword-gadgets-electronics')
    ->getFirstItem();

if ($block->getId()) $block->delete();// if exists then delete

$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($moxyen, $withAdmin = false)
    ->addFieldToFilter('identifier', 'seo-keyword-electronics')
    ->getFirstItem();

if ($block->getId()) $block->delete();// if exists then delete

$content = <<<EOD
<div style="margin: 50px auto 30px;">
<p>MOXY loves Tech &amp; Gadgets! Discover our selection of modern, connected and feminine Electronics. Release your inner geek&hellip; with style!</p>
<p>&nbsp;</p>
<p>On Moxy, you can find mobiles (phones, tablets, laptops), computers, audio (headphones, earphones, speakers), cameras (polaroid, digital&hellip;) and many others (smart robots, power banks, vacuum, etc).</p>
<p>&nbsp;</p>
<p>All of our products are 100% authentic and we work directly with international brands to distribute their products. Our wide selection includes brands, like Apple, Samsung, Arken, House of Marley, Ihealth, Moleskine and many others.</p>
<p>&nbsp;</p>
<p>As the online destination for Women in Thailand, MOXY is dedicated to provide the best customer service: easy, fast and reliable. Your satisfaction is our priority. Please feel free to contact our customer service for any information. Multiple payment methods are available such as credit card, Pay Pal, and cash-on-delivery. Your order will be delivered to your doorstep within 2 to 3 days in Bangkok and 3 to 5 days in other regions of Thailand. Make your shopping easier than ever!</p>
<p>&nbsp;</p>
<p>MOXY is looking forward to serving you.</p>
</div>
EOD;
/** @var Mage_Cms_Model_Block */
$block = Mage::getModel('cms/block');
$block->setTitle('SEO Keyword Electronics EN');
$block->setIdentifier('seo-keyword-gadgets-electronics');
$block->setStores($moxyen);
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================