<?php

// init Magento
Mage::app();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

// get store ids
$en     = Mage::getModel('core/store')->load('en', 'code')->getId();// Petloft
$th     = Mage::getModel('core/store')->load('th', 'code')->getId();
$len    = Mage::getModel('core/store')->load('len', 'code')->getId();// Lafema
$lth    = Mage::getModel('core/store')->load('lth', 'code')->getId();
$ven    = Mage::getModel('core/store')->load('ven', 'code')->getId();// Venbi
$vth    = Mage::getModel('core/store')->load('vth', 'code')->getId();
$sen    = Mage::getModel('core/store')->load('sen', 'code')->getId();// Sanoga
$sth    = Mage::getModel('core/store')->load('sth', 'code')->getId();
$moxyen = Mage::getModel('core/store')->load('moxyen', 'code')->getId();// Moxy
$moxyth = Mage::getModel('core/store')->load('moxyth', 'code')->getId();

$allStores = array($th, $lth, $vth, $sth, $moxyth, $en, $len, $ven, $sen, $moxyen);

// delete old block if have
/** @var Mage_Cms_Model_Resource_Block_Collection $allBlocks */
$allBlocks = Mage::getModel('cms/block')->getCollection()
    ->addFieldToFilter('identifier', 'top_middle_banner');
if($allBlocks->count() > 0){
    /** @var Mage_Cms_Model_Block $item */
    foreach($allBlocks as $item){
        if($item->getId()) $item->delete();
    }
}

//==========================================================================
// Moxy Top Middle Banner version 1.6 (English)
//==========================================================================
$content = <<<EOD
<a title="Homepage" href="http://www.moxyst.com/en/welcome">
    <img src="{{media url='wysiwyg/moxy_top_middle_banner_v16.png'}}" alt="Homepage" />
</a>
EOD;

/** @var Mage_Cms_Model_Block $enBlock */
$enBlock = Mage::getModel('cms/block');
$enBlock->setTitle('Moxy Top Middle Banner version 1.6');
$enBlock->setIdentifier('top_middle_banner');
$enBlock->setStores(array($en, $sen, $ven, $len, $moxyen));
$enBlock->setIsActive(1);
$enBlock->setContent($content);
$enBlock->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// Moxy Top Middle Banner version 1.6 (Thai)
//==========================================================================
$content = <<<EOD
<a title="Homepage" href="http://www.moxyst.com/th/welcome">
    <img src="{{media url='wysiwyg/moxy_top_middle_banner_v16.png'}}" alt="Homepage" />
</a>
EOD;

/** @var Mage_Cms_Model_Block $enBlock */
$enBlock = Mage::getModel('cms/block');
$enBlock->setTitle('Moxy Top Middle Banner version 1.6');
$enBlock->setIdentifier('top_middle_banner');
$enBlock->setStores(array($th, $sth, $vth, $lth, $moxyth));
$enBlock->setIsActive(1);
$enBlock->setContent($content);
$enBlock->save();
//==========================================================================
//==========================================================================
//==========================================================================