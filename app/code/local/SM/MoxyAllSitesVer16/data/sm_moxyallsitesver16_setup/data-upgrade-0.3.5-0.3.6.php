<?php
// Get store code
$moxyth = Mage::getModel('core/store')->load('moxyth', 'code')->getId();

//==========================================================================
// Auction Welcome TH
//==========================================================================
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($moxyth, $withAdmin = false)
    ->addFieldToFilter('identifier', 'auction_welcome')
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete

$content = <<<EOD
<div id="auction-welcome">
    <h3 class="title">Welcome to Moxy Auction</h3>
    <div class="first"><p>Find the most valuable and limited edition celebrity memorabili</p></div>
    <ul>
        <li>- Make an offer</li>
        <li>- Follow your product</li>
        <li>- Top the others</li>
    </ul>
    <div class=" last"><p>You can have a chance to own the rarest products. Bid now!</p></div>
</div>
EOD;
/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block');
$block->setTitle('Auction Welcome TH');
$block->setIdentifier('auction_welcome');
$block->setStores($moxyth);
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================