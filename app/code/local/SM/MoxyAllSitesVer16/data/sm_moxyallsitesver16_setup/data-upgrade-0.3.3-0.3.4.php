<?php

//==========================================================================
// header_right_image th store
//==========================================================================
$blockTitle = "Checkout Success Banner";
$blockIdentifier = "oscheckout-success";
$blockIsActive = 1;

// delete old block if have
$block = Mage::getModel('cms/block')->getCollection()
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete

$content = <<<EOD
<p><img src="{{media url="wysiwyg/banner-checkout_03.jpg"}}" alt="" /></p>
EOD;

/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setIsActive(1);
$block->setStores(0);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================