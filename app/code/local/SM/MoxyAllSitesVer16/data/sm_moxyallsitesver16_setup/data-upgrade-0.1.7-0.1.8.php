<?php

// init Magento
Mage::app();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

// get store ids
$en     = Mage::getModel('core/store')->load('en', 'code')->getId();// Petloft
$th     = Mage::getModel('core/store')->load('th', 'code')->getId();
$len    = Mage::getModel('core/store')->load('len', 'code')->getId();// Lafema
$lth    = Mage::getModel('core/store')->load('lth', 'code')->getId();
$ven    = Mage::getModel('core/store')->load('ven', 'code')->getId();// Venbi
$vth    = Mage::getModel('core/store')->load('vth', 'code')->getId();
$sen    = Mage::getModel('core/store')->load('sen', 'code')->getId();// Sanoga
$sth    = Mage::getModel('core/store')->load('sth', 'code')->getId();
$moxyen = Mage::getModel('core/store')->load('moxyen', 'code')->getId();// Moxy
$moxyth = Mage::getModel('core/store')->load('moxyth', 'code')->getId();

// Fix "home" pages that have problems

//==========================================================================
// Homepage (Sanoga English)
//==========================================================================
$title = "Health and beauty product online shopping at cheap price";
$identifier = "home";
$stores = array($sen);
$isActive = 1;
$rootTemplate = "one_column";
$sortOrder = 0;
$metaDescription = "Buy Dietary Supplements, Vitamins, Collagen and Cosmetics online at Sanoga. Discount prices and promotional sale on all. Free Shipping. Cash on delivery.";
$layoutUpdateXml = <<<EOD
    <reference name="header">
    <block type="cms/block" name="slider_1">
        <!--
        The content of this block is taken from the database by its block_id.
        You can manage it in admin CMS -> Static Blocks
        -->
        <action method="setBlockId"><block_id>slider_1</block_id></action>
    </block>
    <block type="cms/block" name="banners_home">
        <!--
        The content of this block is taken from the database by its block_id.
        You can manage it in admin CMS -> Static Blocks
        -->
        <action method="setBlockId"><block_id>banners_home</block_id></action>
    </block>
</reference>
<reference name="head">
<block type="core/text" name="homepage.metadata" after="-">
<action method="addText"><text><![CDATA[<meta name="google-site-verification" content="_4n5alA2_c2rhcm4BQTl-KqLMwVGw_gyHr20g1fCalA" />]]></text></action>
</block>
</reference>
EOD;
$content = <<<EOD
<p>{{block type="core/template" name="subscription" as="subscription" template="subscription/subscribe.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="1576" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="1577" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="1579" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="682" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="681" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="cms/block" block_id="all-brands-slider"}}{{block type="cms/block" block_id="seo-keyword"}}</p>
EOD;
/** @var Mage_Cms_Model_Page $page */
$page = Mage::getModel('cms/page')->getCollection()
    ->addStoreFilter($stores, $withAdmin = false)
    ->addFieldToFilter('identifier', $identifier)
    ->getFirstItem();
if ($page->getId()) $page->delete(); // if exists then delete
$page = Mage::getModel('cms/page');
$page->setTitle($title);
$page->setIdentifier($identifier);
$page->setStores($stores);
$page->setIsActive($isActive);
$page->setRootTemplate($rootTemplate);
$page->setSortOrder($sortOrder);
$page->setMetaDescription($metaDescription);
$page->setLayoutUpdateXml($layoutUpdateXml);
$page->setContent($content);
$page->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// Homepage (Sanoga Thai)
//==========================================================================
$title = "อาหารเสริม วิตามิน คอลลาเจน เพื่อสุขภาพและความงาม";
$identifier = "home";
$stores = array($sth);
$isActive = 1;
$rootTemplate = "one_column";
$sortOrder = 0;
$metaDescription = "ซาโนก้า ร้านค้าออนไลน์ราคาถูก ขายอาหารเสริม วิตามิน คอลลาเจน และผลิตภัณฑ์ความงาม บริการจัดส่งฟรีถึงบ้าน ราคาถูก พร้อมโปรโมชั่น";
$layoutUpdateXml = <<<EOD
    <reference name="header">
    <block type="cms/block" name="slider_1">
        <!--
        The content of this block is taken from the database by its block_id.
        You can manage it in admin CMS -> Static Blocks
        -->
        <action method="setBlockId"><block_id>slider_1</block_id></action>
    </block>
    <block type="cms/block" name="banners_home">
        <!--
        The content of this block is taken from the database by its block_id.
        You can manage it in admin CMS -> Static Blocks
        -->
        <action method="setBlockId"><block_id>banners_home</block_id></action>
    </block>
</reference>
<reference name="head">
<block type="core/text" name="homepage.metadata" after="-">
<action method="addText"><text><![CDATA[<meta name="google-site-verification" content="_4n5alA2_c2rhcm4BQTl-KqLMwVGw_gyHr20g1fCalA" />]]></text></action>
</block>
</reference>
EOD;
$content = <<<EOD
<p>{{block type="core/template" name="subscription" as="subscription" template="subscription/subscribe.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="1576" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="1577" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="1579" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="682" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="681" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="cms/block" block_id="all-brands-slider"}}{{block type="cms/block" block_id="seo-keyword"}}{{block type="cms/block" block_id="banner_footer"}}</p>
EOD;
/** @var Mage_Cms_Model_Page $page */
$page = Mage::getModel('cms/page')->getCollection()
    ->addStoreFilter($stores, $withAdmin = false)
    ->addFieldToFilter('identifier', $identifier)
    ->getFirstItem();
if ($page->getId()) $page->delete(); // if exists then delete
$page = Mage::getModel('cms/page');
$page->setTitle($title);
$page->setIdentifier($identifier);
$page->setStores($stores);
$page->setIsActive($isActive);
$page->setRootTemplate($rootTemplate);
$page->setSortOrder($sortOrder);
$page->setMetaDescription($metaDescription);
$page->setLayoutUpdateXml($layoutUpdateXml);
$page->setContent($content);
$page->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// Homepage (Lafema English)
//==========================================================================
$title = "Beauty & Cosmetic online shopping at best cheap price";
$identifier = "home";
$stores = array($len);
$isActive = 1;
$rootTemplate = "one_column";
$sortOrder = 0;
$metaDescription = "Lafema best shopping online with best price. Beauty and cosmetic brands from around the world with discount prices and promotional sale. Free shipping.";
$layoutUpdateXml = <<<EOD
    <reference name="header">
    <block type="cms/block" name="slider_1">
        <!--
        The content of this block is taken from the database by its block_id.
        You can manage it in admin CMS -> Static Blocks
        -->
        <action method="setBlockId"><block_id>slider_1</block_id></action>
    </block>
    <block type="cms/block" name="banners_home">
        <!--
        The content of this block is taken from the database by its block_id.
        You can manage it in admin CMS -> Static Blocks
        -->
        <action method="setBlockId"><block_id>banners_home</block_id></action>
    </block>
</reference>
<reference name="head">
<block type="core/text" name="homepage.metadata" after="-">
<action method="addText"><text><![CDATA[<meta name="google-site-verification" content="_4n5alA2_c2rhcm4BQTl-KqLMwVGw_gyHr20g1fCalA" />]]></text></action>
</block>
</reference>
EOD;
$content = <<<EOD
<p>{{block type="core/template" name="subscription" as="subscription" template="subscription/subscribe.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="1580" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="1581" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="1582" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="1571" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="1583" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="cms/block" block_id="all-brands-slider"}}{{block type="cms/block" block_id="seo-keyword"}}{{block type="cms/block" block_id="banner_footer"}}</p>
<!--<p>{{block type="cms/block" block_id="banner-bottom-homepage"}}</p>-->
EOD;
/** @var Mage_Cms_Model_Page $page */
$page = Mage::getModel('cms/page')->getCollection()
    ->addStoreFilter($stores, $withAdmin = false)
    ->addFieldToFilter('identifier', $identifier)
    ->getFirstItem();
if ($page->getId()) $page->delete(); // if exists then delete
$page = Mage::getModel('cms/page');
$page->setTitle($title);
$page->setIdentifier($identifier);
$page->setStores($stores);
$page->setIsActive($isActive);
$page->setRootTemplate($rootTemplate);
$page->setSortOrder($sortOrder);
$page->setMetaDescription($metaDescription);
$page->setLayoutUpdateXml($layoutUpdateXml);
$page->setContent($content);
$page->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// Homepage (Lafema Thai)
//==========================================================================
$title = "ร้านขายเครื่องสำอางออนไลน์ ราคาถูก ส่งฟรีถึงบ้าน";
$identifier = "home";
$stores = array($lth);
$isActive = 1;
$rootTemplate = "one_column";
$sortOrder = 0;
$metaDescription = "ลาฟีมา ขายเครื่องสำอาง ครีมบำรุงผิวหน้า ผิวกาย และผลิตภัณฑ์ความงามออนไลน์ราคาถูก เพื่อผู้หญิงโดยเฉพาะ บริการจัดส่งฟรีถึงบ้าน พร้อมโปรโมชั่นราคาพิเศษ";
$layoutUpdateXml = <<<EOD
    <reference name="header">
    <block type="cms/block" name="slider_1">
        <!--
        The content of this block is taken from the database by its block_id.
        You can manage it in admin CMS -> Static Blocks
        -->
        <action method="setBlockId"><block_id>slider_1</block_id></action>
    </block>
    <block type="cms/block" name="banners_home">
        <!--
        The content of this block is taken from the database by its block_id.
        You can manage it in admin CMS -> Static Blocks
        -->
        <action method="setBlockId"><block_id>banners_home</block_id></action>
    </block>
</reference>
<reference name="head">
<block type="core/text" name="homepage.metadata" after="-">
<action method="addText"><text><![CDATA[<meta name="google-site-verification" content="_4n5alA2_c2rhcm4BQTl-KqLMwVGw_gyHr20g1fCalA" />]]></text></action>
</block>
</reference>
EOD;
$content = <<<EOD
<p>{{block type="core/template" name="subscription" as="subscription" template="subscription/subscribe.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="1580" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="1581" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="1582" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="1571" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="1583" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="cms/block" block_id="all-brands-slider"}}{{block type="cms/block" block_id="seo-keyword"}}{{block type="cms/block" block_id="banner_footer"}}</p>
EOD;
/** @var Mage_Cms_Model_Page $page */
$page = Mage::getModel('cms/page')->getCollection()
    ->addStoreFilter($stores, $withAdmin = false)
    ->addFieldToFilter('identifier', $identifier)
    ->getFirstItem();
if ($page->getId()) $page->delete(); // if exists then delete
$page = Mage::getModel('cms/page');
$page->setTitle($title);
$page->setIdentifier($identifier);
$page->setStores($stores);
$page->setIsActive($isActive);
$page->setRootTemplate($rootTemplate);
$page->setSortOrder($sortOrder);
$page->setMetaDescription($metaDescription);
$page->setLayoutUpdateXml($layoutUpdateXml);
$page->setContent($content);
$page->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// Homepage (Moxy English)
//==========================================================================
$title = "MOXY - Online Lifestyle Shopping";
$identifier = "home";
$stores = array($moxyen);
$isActive = 1;
$rootTemplate = "one_column";
$sortOrder = 0;
$metaDescription = "";
$layoutUpdateXml = <<<EOD
    <reference name="header">
    <block type="cms/block" name="slider_1">
        <!--
        The content of this block is taken from the database by its block_id.
        You can manage it in admin CMS -> Static Blocks
        -->
        <action method="setBlockId"><block_id>slider_1</block_id></action>
    </block>
    <block type="cms/block" name="banners_home">
        <!--
        The content of this block is taken from the database by its block_id.
        You can manage it in admin CMS -> Static Blocks
        -->
        <action method="setBlockId"><block_id>banners_home</block_id></action>
    </block>
</reference>
<reference name="head">
    <block type="core/text" name="homepage.metadata" after="-">
        <action method="addText"><text><![CDATA[<meta name="google-site-verification" content="_4n5alA2_c2rhcm4BQTl-KqLMwVGw_gyHr20g1fCalA" />]]></text></action>
    </block>
</reference>
EOD;
$content = <<<EOD
<p>{{block type="core/template" name="subscription" as="subscription" template="subscription/subscribe.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="2440" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="2138" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="2140" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="2141" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="2142" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="cms/block" block_id="all-brands-slider"}}{{block type="cms/block" block_id="seo-keyword"}}</p>
EOD;
/** @var Mage_Cms_Model_Page $page */
$page = Mage::getModel('cms/page')->getCollection()
    ->addStoreFilter($stores, $withAdmin = false)
    ->addFieldToFilter('identifier', $identifier)
    ->getFirstItem();
if ($page->getId()) $page->delete(); // if exists then delete
$page = Mage::getModel('cms/page');
$page->setTitle($title);
$page->setIdentifier($identifier);
$page->setStores($stores);
$page->setIsActive($isActive);
$page->setRootTemplate($rootTemplate);
$page->setSortOrder($sortOrder);
$page->setMetaDescription($metaDescription);
$page->setLayoutUpdateXml($layoutUpdateXml);
$page->setContent($content);
$page->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// Homepage (Moxy Thai)
//==========================================================================
$title = "MOXY - Online Lifestyle Shopping";
$identifier = "home";
$stores = array($moxyth);
$isActive = 1;
$rootTemplate = "one_column";
$sortOrder = 0;
$metaDescription = "";
$layoutUpdateXml = <<<EOD
    <reference name="header">
    <block type="cms/block" name="slider_1">
        <!--
        The content of this block is taken from the database by its block_id.
        You can manage it in admin CMS -> Static Blocks
        -->
        <action method="setBlockId"><block_id>slider_1</block_id></action>
    </block>
    <block type="cms/block" name="banners_home">
        <!--
        The content of this block is taken from the database by its block_id.
        You can manage it in admin CMS -> Static Blocks
        -->
        <action method="setBlockId"><block_id>banners_home</block_id></action>
    </block>
</reference>
<reference name="head">
    <block type="core/text" name="homepage.metadata" after="-">
        <action method="addText"><text><![CDATA[<meta name="google-site-verification" content="_4n5alA2_c2rhcm4BQTl-KqLMwVGw_gyHr20g1fCalA" />]]></text></action>
    </block>
</reference>
EOD;
$content = <<<EOD
<p>{{block type="core/template" name="subscription" as="subscription" template="subscription/subscribe.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="2440" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="2138" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="2140" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="2141" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="2142" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="cms/block" block_id="all-brands-slider"}}{{block type="cms/block" block_id="seo-keyword"}}</p>
EOD;
/** @var Mage_Cms_Model_Page $page */
$page = Mage::getModel('cms/page')->getCollection()
    ->addStoreFilter($stores, $withAdmin = false)
    ->addFieldToFilter('identifier', $identifier)
    ->getFirstItem();
if ($page->getId()) $page->delete(); // if exists then delete
$page = Mage::getModel('cms/page');
$page->setTitle($title);
$page->setIdentifier($identifier);
$page->setStores($stores);
$page->setIsActive($isActive);
$page->setRootTemplate($rootTemplate);
$page->setSortOrder($sortOrder);
$page->setMetaDescription($metaDescription);
$page->setLayoutUpdateXml($layoutUpdateXml);
$page->setContent($content);
$page->save();
//==========================================================================
//==========================================================================
//==========================================================================