<?php

// init Magento
Mage::app();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

// get store ids
$en     = Mage::getModel('core/store')->load('en', 'code')->getId();// Petloft
$th     = Mage::getModel('core/store')->load('th', 'code')->getId();
$len    = Mage::getModel('core/store')->load('len', 'code')->getId();// Lafema
$lth    = Mage::getModel('core/store')->load('lth', 'code')->getId();
$ven    = Mage::getModel('core/store')->load('ven', 'code')->getId();// Venbi
$vth    = Mage::getModel('core/store')->load('vth', 'code')->getId();
$sen    = Mage::getModel('core/store')->load('sen', 'code')->getId();// Sanoga
$sth    = Mage::getModel('core/store')->load('sth', 'code')->getId();
$moxyen = Mage::getModel('core/store')->load('moxyen', 'code')->getId();// Moxy
$moxyth = Mage::getModel('core/store')->load('moxyth', 'code')->getId();


// UPDATE ABOUT-US PAGE OTHER VERTICAL SITES EXCEPT MOXY


//==========================================================================
// About Us page (Petloft English)
//==========================================================================
$title = "About Us";
$stores = array($en);
$metaDescription = "About Us at PetLoft.com - Largest selection of pet food in thailand - Free shipping & delivery";
$identifier = "about-us";
$isActive = 1;
$rootTemplate = "one_column";
$sortOrder = 0;
$contentHeading = "About Us";
$content = <<<EOD
<div style="margin: 20px 0;">&nbsp;</div>
<div style="font: normal 14px 'Tahoma'; color: #505050; line-height: 22px;">
<p>Moxy is the only Women-focused shopping destination in Thailand.</p>
<p>Because modern Women are living very busy lives and have a lot of responsibilities, their time is precious. On Moxy, find everything you want, from daily essentials to coveted lifestyle needs. You can find tips and products in categories like Beauty,&nbsp;Fashion Accessories, Living, Electronics, Health and Mom &amp; Baby and Pets, with the convenience of it being delivered to your front door.&nbsp;</p>
<p>With a mix of both local and international brands and new items available every week, Moxy offers a large variety of quality products to suit your lifestyle, whether you are a student, a worker, a mother, a friend, a lover, a daughter&hellip; You CAN and SHOULD &ldquo;Have It All&rdquo;!</p>
<p>At Moxy, we believe that purchasing online should be easy, fun and safe. Our mission is to provide an entertaining shopping experience combined with a convenient home delivery service. Our customers can choose between various secure payment options, including cash-on-delivery everywhere in Thailand. At any moment, Customers can reach our support team at&nbsp;<span style="text-decoration: underline;"><a href="mailto:support@petloft.com">support@petloft.com</a></span>&nbsp;or by phone +65- 02-106-8222&nbsp;. Our customer support talents are at your service from Mon-Fri 09:00-18:00 and Sat 09:00-15:00.&nbsp;</p>
<p>&nbsp;</p>
<p>Stay connected with us on&nbsp;&nbsp;<span style="color: #333300;"><a href="https://www.facebook.com/petloftthai" target="_blank"><span style="color: #333300;"><strong><span style="text-decoration: underline;">Facebook</span></strong></span></a></span>,&nbsp;<span style="color: #333300;"><a href="http://webstagr.am/petloft" target="_blank"><span style="color: #333300;"><strong><span style="text-decoration: underline;">Instagram</span></strong></span></a>&nbsp;</span>and&nbsp;<span style="color: #333300;"><span style="color: #333300;"><strong><a href="https://twitter.com/venbithai" target="_blank"><span style="text-decoration: underline;">Twitter</span>&nbsp;</a>&nbsp;</strong></span></span>to get daily tips, our newest updates and the best shopping deals.</p>
<p>&nbsp;</p>
<p>We are looking forward to serving you !</p>
<p>With Love,</p>
<strong>The Moxy Team</strong></div>
EOD;
/** @var Mage_Cms_Model_Page $page */
$page = Mage::getModel('cms/page')->getCollection()
    ->addStoreFilter($stores, $withAdmin = false)
    ->addFieldToFilter('identifier', $identifier)
    ->getFirstItem();
if ($page->getId()) $page->delete(); // if exists then delete
$page = Mage::getModel('cms/page');
$page->setTitle($title);
$page->setIdentifier($identifier);
$page->setStores($stores);
$page->setIsActive($isActive);
$page->setRootTemplate($rootTemplate);
$page->setSortOrder($sortOrder);
$page->setContent($content);
$page->setContentHeading($contentHeading);
$page->setMetaDescription($metaDescription);
$page->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// About Us page (Petloft Thai)
//==========================================================================
$title = "About us | เกี่ยวกับเรา";
$stores = array($th);
$metaDescription = "PetLoft.com แหล่งรวมอาหารสัตว์เลี้ยง อุปกรณ์สัตว์เลี้ยงออนไลน์คุณภาพสูง ราคาถูก จัดส่งฟรีถึงบ้านในเขตกรุงเทพ จ่ายเงินเมื่อรับสินค้า";
$identifier = "about-us";
$isActive = 1;
$rootTemplate = "one_column";
$sortOrder = 0;
$contentHeading = "เกี่ยวกับ Petloft";
$content = <<<EOD
<div style="margin: 20px 0;">&nbsp;</div>
<div style="font: normal 14px 'Tahoma'; color: #505050; line-height: 22px;">
<p>MOXY เว็บไซต์ช้อปปิ้งออนไลน์สำหรับผู้หญิงแห่งเดียวของเมืองไทย</p>
<p>เรารู้ว่าผู้หญิงมีไลฟ์สไตล์ที่ยุ่งเหยิง และมีสิ่งที่ต้องทำมากมายในแต่ละวันเพื่อแข่งกับเวลา MOXY จึงรวบรวมของทุกอย่างที่ผู้หญิงต้องการในชีวิตประจำวัน ไม่ว่าจะเป็นเครื่องสำอาง เครื่องประดับ ของแต่งบ้าน แก็ตเจ็ตอีเล็คทรอนิกส์ สินค้าเพื่อสุขภาพ สินค้าสำหรับแม่และเด็ก และสินค้าสำหรับสัตว์เลี้ยงที่พร้อมจะส่งตรงถึงหน้าบ้านคุณ</p>
<p>เรามีสินค้าจากแบรนด์ดังทั่วโลก ทั้งไทยและต่างประเทศ และสินค้าใหม่ๆให้ได้ช้อปกันทุกสัปดาห์ เน้นเฉพาะสินค้าที่มีคุณภาพและเหมาะกับไลฟ์สไตล์ของผู้หญิงทุกคน ไม่ว่าคุณจะเป็นนักศึกษา คนทำงาน เป็นแม่ เป็นเพื่อน เป็นคนรัก หรือเป็นลูกสาว คุณสามารถมีได้ทุกอย่างที่คุณอยากได้อย่างแน่นอน &ldquo;Have it all&rdquo;</p>
<p style="font-weight: normal;">MOXY เชื่อว่าการช้อปปิ้งออนไลน์ควรเป็นเรื่องที่สนุก สะดวก และปลอดภัย เราจึงตั้งใจสร้างประสบการณ์ช้อปปิ้งออนไลน์ที่ดีที่สุดให้กับคุณ บริการส่งสินค้าทุกที่ทั่วประเทศถึงมือคุณ&nbsp; พร้อมทั้งการชำระเงินผ่านช่องทางต่างๆที่ไว้ในได้ในความปลอดภัย หรือจะชำระเงินเมื่อได้รับสินค้าก็ยังได้ คุณสามารถติดต่อ MOXY ได้ผ่านฝ่ายบริการลูกค้า อีเมล&nbsp;&nbsp;<span style="text-decoration: underline;"><a href="mailto:support@petloft.com">support@petloft.com</a></span>&nbsp; หรือโทร 02-106-8222&nbsp; วันจันทร์ถึงศุกร์ 9:00-18:00 น. และวันเสาร์ 09:00-15:00 น.</p>
<p style="font-weight: normal;">&nbsp;</p>
<p>อัพเดทข่าวสารและเทรนด์ใหม่ๆกับเราได้ผ่านทาง&nbsp;<a href="https://www.facebook.com/petloftthai" target="_blank"><strong><span style="text-decoration: underline;">Facebook</span></strong></a>,&nbsp;<span><a style="font-weight: bold; text-decoration: underline;" href="http://webstagr.am/petloft" target="_blank">Instagram</a></span><span style="color: #333300;"><span style="color: #333300;"><span>, และ&nbsp;&nbsp;</span></span></span><strong><span style="text-decoration: underline;"><a href="https://twitter.com/venbithai" target="_blank">Twitter</a></span></strong><strong>&nbsp;</strong>ได้ทุกวัน</p>
<p>MOXY พร้อมที่จะให้บริการคุณแล้ววันนี้</p>
<p>ด้วยรัก</p>
<p>ทีมงาน MOXY</p>
</div>
EOD;
/** @var Mage_Cms_Model_Page $page */
$page = Mage::getModel('cms/page')->getCollection()
    ->addStoreFilter($stores, $withAdmin = false)
    ->addFieldToFilter('identifier', $identifier)
    ->getFirstItem();
if ($page->getId()) $page->delete(); // if exists then delete
$page = Mage::getModel('cms/page');
$page->setTitle($title);
$page->setIdentifier($identifier);
$page->setStores($stores);
$page->setIsActive($isActive);
$page->setRootTemplate($rootTemplate);
$page->setSortOrder($sortOrder);
$page->setContent($content);
$page->setContentHeading($contentHeading);
$page->setMetaDescription($metaDescription);
$page->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// About Us page (Lafema English)
//==========================================================================
$title = "About Us";
$stores = array($len);
$metaDescription = "";
$identifier = "about-us";
$isActive = 1;
$rootTemplate = "one_column";
$sortOrder = 0;
$contentHeading = "About Us";
$content = <<<EOD
<div style="margin: 20px 0;">&nbsp;</div>
<div style="font: normal 14px 'Tahoma'; color: #505050; line-height: 22px;">
<p>Moxy is the only Women-focused shopping destination in Thailand.</p>
<p>Because modern Women are living very busy lives and have a lot of responsibilities, their time is precious. On Moxy, find everything you want, from daily essentials to coveted lifestyle needs. You can find tips and products in categories like Beauty,&nbsp;Fashion Accessories, Living, Electronics, Health and Mom &amp; Baby and Pets, with the convenience of it being delivered to your front door.&nbsp;</p>
<p>With a mix of both local and international brands and new items available every week, Moxy offers a large variety of quality products to suit your lifestyle, whether you are a student, a worker, a mother, a friend, a lover, a daughter&hellip; You CAN and SHOULD &ldquo;Have It All&rdquo;!</p>
<p>At Moxy, we believe that purchasing online should be easy, fun and safe. Our mission is to provide an entertaining shopping experience combined with a convenient home delivery service. Our customers can choose between various secure payment options, including cash-on-delivery everywhere in Thailand. At any moment, Customers can reach our support team at&nbsp;<span style="text-decoration: underline;"><a href="mailto:support@lafema.com">support@lafema.com</a></span>&nbsp;or by phone +65- 02-106-8222&nbsp;. Our customer support talents are at your service from Mon-Fri 09:00-18:00 and Sat 09:00-15:00.&nbsp;</p>
<p>&nbsp;</p>
<p>Stay connected with us on&nbsp;&nbsp;<span style="color: #333300;"><a href="https://www.facebook.com/lafemathai" target="_blank"><span style="color: #333300;"><strong><span style="text-decoration: underline;">Facebook</span></strong></span></a></span>,&nbsp;<span style="color: #333300;"><a href="http://webstagr.am/lafema.th" target="_blank"><span style="color: #333300;"><strong><span style="text-decoration: underline;">Instagram</span></strong></span></a>&nbsp;</span>and&nbsp;<span style="color: #333300;"><span style="color: #333300;"><strong><a href="https://twitter.com/venbithai" target="_blank"><span style="text-decoration: underline;">Twitter</span>&nbsp;</a>&nbsp;</strong></span></span>to get daily tips, our newest updates and the best shopping deals.</p>
<p>&nbsp;</p>
<p>We are looking forward to serving you !</p>
<p>With Love,</p>
<strong>The Moxy Team</strong></div>
EOD;
/** @var Mage_Cms_Model_Page $page */
$page = Mage::getModel('cms/page')->getCollection()
    ->addStoreFilter($stores, $withAdmin = false)
    ->addFieldToFilter('identifier', $identifier)
    ->getFirstItem();
if ($page->getId()) $page->delete(); // if exists then delete
$page = Mage::getModel('cms/page');
$page->setTitle($title);
$page->setIdentifier($identifier);
$page->setStores($stores);
$page->setIsActive($isActive);
$page->setRootTemplate($rootTemplate);
$page->setSortOrder($sortOrder);
$page->setContent($content);
$page->setContentHeading($contentHeading);
$page->setMetaDescription($metaDescription);
$page->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// About Us page (Lafema Thai)
//==========================================================================
$title = "About us | เกี่ยวกับเรา";
$stores = array($lth);
$metaDescription = "Lafema.com แหล่งรวมเครื่องสำอาง และผลิตภัณฑ์เพื่อความงามออนไลน์คุณภาพสูง ราคาถูก จัดส่งฟรีในกรุงเทพ จ่ายเงินเมื่อรับสินค้า";
$identifier = "about-us";
$isActive = 1;
$rootTemplate = "one_column";
$sortOrder = 0;
$contentHeading = "เกี่ยวกับ Lafema";
$content = <<<EOD
<div style="margin: 20px 0;">&nbsp;</div>
<div style="font: normal 14px 'Tahoma'; color: #505050; line-height: 22px;">
<p>MOXY เว็บไซต์ช้อปปิ้งออนไลน์สำหรับผู้หญิงแห่งเดียวของเมืองไทย</p>
<p>เรารู้ว่าผู้หญิงมีไลฟ์สไตล์ที่ยุ่งเหยิง และมีสิ่งที่ต้องทำมากมายในแต่ละวันเพื่อแข่งกับเวลา MOXY จึงรวบรวมของทุกอย่างที่ผู้หญิงต้องการในชีวิตประจำวัน ไม่ว่าจะเป็นเครื่องสำอาง เครื่องประดับ ของแต่งบ้าน แก็ตเจ็ตอีเล็คทรอนิกส์ สินค้าเพื่อสุขภาพ สินค้าสำหรับแม่และเด็ก และสินค้าสำหรับสัตว์เลี้ยงที่พร้อมจะส่งตรงถึงหน้าบ้านคุณ</p>
<p>เรามีสินค้าจากแบรนด์ดังทั่วโลก ทั้งไทยและต่างประเทศ และสินค้าใหม่ๆให้ได้ช้อปกันทุกสัปดาห์ เน้นเฉพาะสินค้าที่มีคุณภาพและเหมาะกับไลฟ์สไตล์ของผู้หญิงทุกคน ไม่ว่าคุณจะเป็นนักศึกษา คนทำงาน เป็นแม่ เป็นเพื่อน เป็นคนรัก หรือเป็นลูกสาว คุณสามารถมีได้ทุกอย่างที่คุณอยากได้อย่างแน่นอน &ldquo;Have it all&rdquo;</p>
<p style="font-weight: normal;">MOXY เชื่อว่าการช้อปปิ้งออนไลน์ควรเป็นเรื่องที่สนุก สะดวก และปลอดภัย เราจึงตั้งใจสร้างประสบการณ์ช้อปปิ้งออนไลน์ที่ดีที่สุดให้กับคุณ บริการส่งสินค้าทุกที่ทั่วประเทศถึงมือคุณ&nbsp; พร้อมทั้งการชำระเงินผ่านช่องทางต่างๆที่ไว้ในได้ในความปลอดภัย หรือจะชำระเงินเมื่อได้รับสินค้าก็ยังได้ คุณสามารถติดต่อ MOXY ได้ผ่านฝ่ายบริการลูกค้า อีเมล&nbsp;&nbsp;<span style="text-decoration: underline;"><a href="mailto:support@lafema.com">support@lafema.com</a></span>&nbsp; หรือโทร 02-106-8222&nbsp; วันจันทร์ถึงศุกร์ 9:00-18:00 น. และวันเสาร์ 09:00-15:00 น.</p>
<p style="font-weight: normal;">&nbsp;</p>
<p>อัพเดทข่าวสารและเทรนด์ใหม่ๆกับเราได้ผ่านทาง&nbsp;<a href="https://www.facebook.com/lafemathai" target="_blank"><strong><span style="text-decoration: underline;">Facebook</span></strong></a>,&nbsp;<span><a style="font-weight: bold; text-decoration: underline;" href="http://webstagr.am/lafema.th" target="_blank">Instagram</a></span><span style="color: #333300;"><span style="color: #333300;"><span>, และ&nbsp;&nbsp;</span></span></span><strong><span style="text-decoration: underline;"><a href="https://twitter.com/venbithai" target="_blank">Twitter</a></span></strong><strong>&nbsp;</strong>ได้ทุกวัน</p>
<p>MOXY พร้อมที่จะให้บริการคุณแล้ววันนี้</p>
<p>ด้วยรัก</p>
<p>ทีมงาน MOXY</p>
</div>
EOD;
$lm = Mage::getModel('core/store')->load('lm', 'code')->getId();
/** @var Mage_Cms_Model_Page $page */
$page = Mage::getModel('cms/page')->getCollection()
    ->addStoreFilter(array($lth, $lm), $withAdmin = false)
    ->addFieldToFilter('identifier', $identifier)
    ->getFirstItem();
if ($page->getId()) $page->delete(); // if exists then delete
$page = Mage::getModel('cms/page');
$page->setTitle($title);
$page->setIdentifier($identifier);
$page->setStores($stores);
$page->setIsActive($isActive);
$page->setRootTemplate($rootTemplate);
$page->setSortOrder($sortOrder);
$page->setContent($content);
$page->setContentHeading($contentHeading);
$page->setMetaDescription($metaDescription);
$page->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// About Us page (Sanoga English)
//==========================================================================
$title = "About Us";
$stores = array($sen);
$metaDescription = "";
$identifier = "about-us";
$isActive = 1;
$rootTemplate = "one_column";
$sortOrder = 0;
$contentHeading = "About Us";
$content = <<<EOD
<div style="margin: 20px 0;">&nbsp;</div>
<div style="font: normal 14px 'Tahoma'; color: #505050; line-height: 22px;">
<p>Moxy is the only Women-focused shopping destination in Thailand.</p>
<p>Because modern Women are living very busy lives and have a lot of responsibilities, their time is precious. On Moxy, find everything you want, from daily essentials to coveted lifestyle needs. You can find tips and products in categories like Beauty,&nbsp;Fashion Accessories, Living, Electronics, Health and Mom &amp; Baby and Pets, with the convenience of it being delivered to your front door.&nbsp;</p>
<p>With a mix of both local and international brands and new items available every week, Moxy offers a large variety of quality products to suit your lifestyle, whether you are a student, a worker, a mother, a friend, a lover, a daughter&hellip; You CAN and SHOULD &ldquo;Have It All&rdquo;!</p>
<p>At Moxy, we believe that purchasing online should be easy, fun and safe. Our mission is to provide an entertaining shopping experience combined with a convenient home delivery service. Our customers can choose between various secure payment options, including cash-on-delivery everywhere in Thailand. At any moment, Customers can reach our support team at&nbsp;<span style="text-decoration: underline;"><a href="mailto:support@sanoga.com">support@sanoga.com</a></span>&nbsp;or by phone +65- 02-106-8222&nbsp;. Our customer support talents are at your service from Mon-Fri 09:00-18:00 and Sat 09:00-15:00.&nbsp;</p>
<p>&nbsp;</p>
<p>Stay connected with us on&nbsp;&nbsp;<span style="color: #333300;"><a href="https://www.facebook.com/SanogaThailand" target="_blank"><span style="color: #333300;"><strong><span style="text-decoration: underline;">Facebook</span></strong></span></a></span>,&nbsp;<span style="color: #333300;"><a href="http://webstagr.am/venbi_thailand" target="_blank"><span style="color: #333300;"><strong><span style="text-decoration: underline;">Instagram</span></strong></span></a>&nbsp;</span>and&nbsp;<span style="color: #333300;"><span style="color: #333300;"><strong><a href="https://twitter.com/venbithai" target="_blank"><span style="text-decoration: underline;">Twitter</span>&nbsp;</a>&nbsp;</strong></span></span>to get daily tips, our newest updates and the best shopping deals.</p>
<p>&nbsp;</p>
<p>We are looking forward to serving you !</p>
<p>With Love,</p>
<strong>The Moxy Team</strong></div>
EOD;
/** @var Mage_Cms_Model_Page $page */
$page = Mage::getModel('cms/page')->getCollection()
    ->addStoreFilter($stores, $withAdmin = false)
    ->addFieldToFilter('identifier', $identifier)
    ->getFirstItem();
if ($page->getId()) $page->delete(); // if exists then delete
$page = Mage::getModel('cms/page');
$page->setTitle($title);
$page->setIdentifier($identifier);
$page->setStores($stores);
$page->setIsActive($isActive);
$page->setRootTemplate($rootTemplate);
$page->setSortOrder($sortOrder);
$page->setContent($content);
$page->setContentHeading($contentHeading);
$page->setMetaDescription($metaDescription);
$page->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// About Us page (Sanoga Thai)
//==========================================================================
$title = "About us | เกี่ยวกับเรา";
$stores = array($sth);
$metaDescription = "SANOGA คือศูนย์รวมผลิตภัณฑ์เพื่อสุขภาพและเครื่องสำอาง อาหารเสริม คอลลาเจน และวิตามินต่างๆ คุณภาพแท้ 100% สินค้ามี อย.ทุกชิ้น จากแบรนด์ชั้นนำทั่วโลก";
$identifier = "about-us";
$isActive = 1;
$rootTemplate = "one_column";
$sortOrder = 0;
$contentHeading = "เกี่ยวกับ Sanoga";
$content = <<<EOD
<div style="margin: 20px 0;">&nbsp;</div>
<div style="font: normal 14px 'Tahoma'; color: #505050; line-height: 22px;">
<p>MOXY เว็บไซต์ช้อปปิ้งออนไลน์สำหรับผู้หญิงแห่งเดียวของเมืองไทย</p>
<p>เรารู้ว่าผู้หญิงมีไลฟ์สไตล์ที่ยุ่งเหยิง และมีสิ่งที่ต้องทำมากมายในแต่ละวันเพื่อแข่งกับเวลา MOXY จึงรวบรวมของทุกอย่างที่ผู้หญิงต้องการในชีวิตประจำวัน ไม่ว่าจะเป็นเครื่องสำอาง เครื่องประดับ ของแต่งบ้าน แก็ตเจ็ตอีเล็คทรอนิกส์ สินค้าเพื่อสุขภาพ สินค้าสำหรับแม่และเด็ก และสินค้าสำหรับสัตว์เลี้ยงที่พร้อมจะส่งตรงถึงหน้าบ้านคุณ</p>
<p>เรามีสินค้าจากแบรนด์ดังทั่วโลก ทั้งไทยและต่างประเทศ และสินค้าใหม่ๆให้ได้ช้อปกันทุกสัปดาห์ เน้นเฉพาะสินค้าที่มีคุณภาพและเหมาะกับไลฟ์สไตล์ของผู้หญิงทุกคน ไม่ว่าคุณจะเป็นนักศึกษา คนทำงาน เป็นแม่ เป็นเพื่อน เป็นคนรัก หรือเป็นลูกสาว คุณสามารถมีได้ทุกอย่างที่คุณอยากได้อย่างแน่นอน &ldquo;Have it all&rdquo;</p>
<p style="font-weight: normal;">MOXY เชื่อว่าการช้อปปิ้งออนไลน์ควรเป็นเรื่องที่สนุก สะดวก และปลอดภัย เราจึงตั้งใจสร้างประสบการณ์ช้อปปิ้งออนไลน์ที่ดีที่สุดให้กับคุณ บริการส่งสินค้าทุกที่ทั่วประเทศถึงมือคุณ&nbsp; พร้อมทั้งการชำระเงินผ่านช่องทางต่างๆที่ไว้ในได้ในความปลอดภัย หรือจะชำระเงินเมื่อได้รับสินค้าก็ยังได้ คุณสามารถติดต่อ MOXY ได้ผ่านฝ่ายบริการลูกค้า อีเมล&nbsp;&nbsp;<span style="text-decoration: underline;"><a href="mailto:support@sanoga.com">support@sanoga.com</a></span>&nbsp; หรือโทร 02-106-8222&nbsp; วันจันทร์ถึงศุกร์ 9:00-18:00 น. และวันเสาร์ 09:00-15:00 น.</p>
<p style="font-weight: normal;">&nbsp;</p>
<p>อัพเดทข่าวสารและเทรนด์ใหม่ๆกับเราได้ผ่านทาง&nbsp;<a href="https://www.facebook.com/SanogaThailand" target="_blank"><strong><span style="text-decoration: underline;">Facebook</span></strong></a>,&nbsp;<span><a style="font-weight: bold; text-decoration: underline;" href="http://webstagr.am/venbi_thailand" target="_blank">Instagram</a></span><span style="color: #333300;"><span style="color: #333300;"><span>, และ&nbsp;&nbsp;</span></span></span><strong><span style="text-decoration: underline;"><a href="https://twitter.com/venbithai" target="_blank">Twitter</a></span></strong><strong>&nbsp;</strong>ได้ทุกวัน</p>
<p>MOXY พร้อมที่จะให้บริการคุณแล้ววันนี้</p>
<p>ด้วยรัก</p>
<p>ทีมงาน MOXY</p>
</div>
EOD;
$sm = Mage::getModel('core/store')->load('sm', 'code')->getId();
/** @var Mage_Cms_Model_Page $page */
$page = Mage::getModel('cms/page')->getCollection()
    ->addStoreFilter(array($sth, $sm), $withAdmin = false)
    ->addFieldToFilter('identifier', $identifier)
    ->getFirstItem();
if ($page->getId()) $page->delete(); // if exists then delete
$page = Mage::getModel('cms/page');
$page->setTitle($title);
$page->setIdentifier($identifier);
$page->setStores($stores);
$page->setIsActive($isActive);
$page->setRootTemplate($rootTemplate);
$page->setSortOrder($sortOrder);
$page->setContent($content);
$page->setContentHeading($contentHeading);
$page->setMetaDescription($metaDescription);
$page->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// About Us page (Venbi English)
//==========================================================================
$title = "About Us";
$stores = array($ven);
$metaDescription = "";
$identifier = "about-us";
$isActive = 1;
$rootTemplate = "one_column";
$sortOrder = 0;
$contentHeading = "About Us";
$content = <<<EOD
<div style="margin: 20px 0;">&nbsp;</div>
<div style="font: normal 14px 'Tahoma'; color: #505050; line-height: 22px;">
<p>Moxy is the only Women-focused shopping destination in Thailand.</p>
<p>Because modern Women are living very busy lives and have a lot of responsibilities, their time is precious. On Moxy, find everything you want, from daily essentials to coveted lifestyle needs. You can find tips and products in categories like Beauty,&nbsp;Fashion Accessories, Living, Electronics, Health and Mom &amp; Baby and Pets, with the convenience of it being delivered to your front door.&nbsp;</p>
<p>With a mix of both local and international brands and new items available every week, Moxy offers a large variety of quality products to suit your lifestyle, whether you are a student, a worker, a mother, a friend, a lover, a daughter&hellip; You CAN and SHOULD &ldquo;Have It All&rdquo;!</p>
<p>At Moxy, we believe that purchasing online should be easy, fun and safe. Our mission is to provide an entertaining shopping experience combined with a convenient home delivery service. Our customers can choose between various secure payment options, including cash-on-delivery everywhere in Thailand. At any moment, Customers can reach our support team at&nbsp;<span style="text-decoration: underline;"><a href="mailto:support@venbi.com">support@venbi.com</a></span>&nbsp;or by phone +65- 02-106-8222&nbsp;. Our customer support talents are at your service from Mon-Fri 09:00-18:00 and Sat 09:00-15:00.&nbsp;</p>
<p>&nbsp;</p>
<p>Stay connected with us on&nbsp;&nbsp;<span style="color: #333300;"><a href="https://www.facebook.com/venbithai" target="_blank"><span style="color: #333300;"><strong><span style="text-decoration: underline;">Facebook</span></strong></span></a></span>,&nbsp;<span style="color: #333300;"><a href="http://webstagr.am/venbi_thailand" target="_blank"><span style="color: #333300;"><strong><span style="text-decoration: underline;">Instagram</span></strong></span></a>&nbsp;</span>and&nbsp;<span style="color: #333300;"><span style="color: #333300;"><strong><a href="https://twitter.com/venbithai" target="_blank"><span style="text-decoration: underline;">Twitter</span>&nbsp;</a>&nbsp;</strong></span></span>to get daily tips, our newest updates and the best shopping deals.</p>
<p>&nbsp;</p>
<p>We are looking forward to serving you !</p>
<p>With Love,</p>
<strong>The Moxy Team</strong></div>
EOD;
/** @var Mage_Cms_Model_Page $page */
$page = Mage::getModel('cms/page')->getCollection()
    ->addStoreFilter($stores, $withAdmin = false)
    ->addFieldToFilter('identifier', $identifier)
    ->getFirstItem();
if ($page->getId()) $page->delete(); // if exists then delete
$page = Mage::getModel('cms/page');
$page->setTitle($title);
$page->setIdentifier($identifier);
$page->setStores($stores);
$page->setIsActive($isActive);
$page->setRootTemplate($rootTemplate);
$page->setSortOrder($sortOrder);
$page->setContent($content);
$page->setContentHeading($contentHeading);
$page->setMetaDescription($metaDescription);
$page->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// About Us page (Venbi Thai)
//==========================================================================
$title = "About us | เกี่ยวกับเรา";
$stores = array($vth);
$metaDescription = "Venbi.com แหล่งรวมสินค้าแม่และเด็กออนไลน์คุณภาพสูง ราคาถูก สินค้าทุกชิ้นได้มาตรฐานสากล ให้คุณได้มั่นใจในความปลอดภัยของลูกน้อย จัดส่งฟรีในกรุงเทพ";
$identifier = "about-us";
$isActive = 1;
$rootTemplate = "one_column";
$sortOrder = 0;
$contentHeading = "เกี่ยวกับ Venbi";
$content = <<<EOD
<div style="margin: 20px 0;">&nbsp;</div>
<div style="font: normal 14px 'Tahoma'; color: #505050; line-height: 22px;">
<p>MOXY เว็บไซต์ช้อปปิ้งออนไลน์สำหรับผู้หญิงแห่งเดียวของเมืองไทย</p>
<p>เรารู้ว่าผู้หญิงมีไลฟ์สไตล์ที่ยุ่งเหยิง และมีสิ่งที่ต้องทำมากมายในแต่ละวันเพื่อแข่งกับเวลา MOXY จึงรวบรวมของทุกอย่างที่ผู้หญิงต้องการในชีวิตประจำวัน ไม่ว่าจะเป็นเครื่องสำอาง เครื่องประดับ ของแต่งบ้าน แก็ตเจ็ตอีเล็คทรอนิกส์ สินค้าเพื่อสุขภาพ สินค้าสำหรับแม่และเด็ก และสินค้าสำหรับสัตว์เลี้ยงที่พร้อมจะส่งตรงถึงหน้าบ้านคุณ</p>
<p>เรามีสินค้าจากแบรนด์ดังทั่วโลก ทั้งไทยและต่างประเทศ และสินค้าใหม่ๆให้ได้ช้อปกันทุกสัปดาห์ เน้นเฉพาะสินค้าที่มีคุณภาพและเหมาะกับไลฟ์สไตล์ของผู้หญิงทุกคน ไม่ว่าคุณจะเป็นนักศึกษา คนทำงาน เป็นแม่ เป็นเพื่อน เป็นคนรัก หรือเป็นลูกสาว คุณสามารถมีได้ทุกอย่างที่คุณอยากได้อย่างแน่นอน &ldquo;Have it all&rdquo;</p>
<p style="font-weight: normal;">MOXY เชื่อว่าการช้อปปิ้งออนไลน์ควรเป็นเรื่องที่สนุก สะดวก และปลอดภัย เราจึงตั้งใจสร้างประสบการณ์ช้อปปิ้งออนไลน์ที่ดีที่สุดให้กับคุณ บริการส่งสินค้าทุกที่ทั่วประเทศถึงมือคุณ&nbsp; พร้อมทั้งการชำระเงินผ่านช่องทางต่างๆที่ไว้ในได้ในความปลอดภัย หรือจะชำระเงินเมื่อได้รับสินค้าก็ยังได้ คุณสามารถติดต่อ MOXY ได้ผ่านฝ่ายบริการลูกค้า อีเมล&nbsp;&nbsp;<span style="text-decoration: underline;"><a href="mailto:support@venbi.com">support@venbi.com</a></span>&nbsp; หรือโทร 02-106-8222&nbsp; วันจันทร์ถึงศุกร์ 9:00-18:00 น. และวันเสาร์ 09:00-15:00 น.</p>
<p style="font-weight: normal;">&nbsp;</p>
<p>อัพเดทข่าวสารและเทรนด์ใหม่ๆกับเราได้ผ่านทาง&nbsp;<a href="https://www.facebook.com/venbithai" target="_blank"><strong><span style="text-decoration: underline;">Facebook</span></strong></a>,&nbsp;<span><a style="font-weight: bold; text-decoration: underline;" href="http://webstagr.am/venbi_thailand" target="_blank">Instagram</a></span><span style="color: #333300;"><span style="color: #333300;"><span>, และ&nbsp;&nbsp;</span></span></span><strong><span style="text-decoration: underline;"><a href="https://twitter.com/venbithai" target="_blank">Twitter</a></span></strong><strong>&nbsp;</strong>ได้ทุกวัน</p>
<p>MOXY พร้อมที่จะให้บริการคุณแล้ววันนี้</p>
<p>ด้วยรัก</p>
<p>ทีมงาน MOXY</p>
</div>
EOD;
/** @var Mage_Cms_Model_Page $page */
$page = Mage::getModel('cms/page')->getCollection()
    ->addStoreFilter($stores, $withAdmin = false)
    ->addFieldToFilter('identifier', $identifier)
    ->getFirstItem();
if ($page->getId()) $page->delete(); // if exists then delete
$page = Mage::getModel('cms/page');
$page->setTitle($title);
$page->setIdentifier($identifier);
$page->setStores($stores);
$page->setIsActive($isActive);
$page->setRootTemplate($rootTemplate);
$page->setSortOrder($sortOrder);
$page->setContent($content);
$page->setContentHeading($contentHeading);
$page->setMetaDescription($metaDescription);
$page->save();
//==========================================================================
//==========================================================================
//==========================================================================