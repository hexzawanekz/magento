<?php

// init Magento
Mage::app();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

// get store ids
$en     = Mage::getModel('core/store')->load('en', 'code')->getId();// Petloft
$th     = Mage::getModel('core/store')->load('th', 'code')->getId();
$len    = Mage::getModel('core/store')->load('len', 'code')->getId();// Lafema
$lth    = Mage::getModel('core/store')->load('lth', 'code')->getId();
$ven    = Mage::getModel('core/store')->load('ven', 'code')->getId();// Venbi
$vth    = Mage::getModel('core/store')->load('vth', 'code')->getId();
$sen    = Mage::getModel('core/store')->load('sen', 'code')->getId();// Sanoga
$sth    = Mage::getModel('core/store')->load('sth', 'code')->getId();
$moxyen = Mage::getModel('core/store')->load('moxyen', 'code')->getId();// Moxy
$moxyth = Mage::getModel('core/store')->load('moxyth', 'code')->getId();



//==========================================================================
// SEO Keyword (Health English)
//==========================================================================
$blockTitle = "Health SEO Keyword";
$blockIdentifier = "seo-keyword";
$blockStores = array($sen);
$blockIsActive = 1;
$blockContent = <<<EOD
<div style="margin: 50px auto 30px;">
<p>SANOGA is MOXY’s Health & Sport online store in Thailand.</p>
<p>&nbsp;</p>
<p>No more looking around for the best deal, MOXY is a one-stop health store where you can find 100% genuine products from household names. It is cheaper, easier and you can select what’s best for you 24/7 with just one click!</p>
<p>&nbsp;</p>
<p>MOXY offers over 1,000 Health products and Sport gear from well-known brands including Health Care, Vitamins, Collagen, Beauty, Sexual Well-being, Workout & Fitness, Weight Loss including Blackmores, Beauty Wise, Body Shape, CLG500, Centrum, Collagen Star, Colly, Dr.Absolute, DYMATIZE, Durex, Eucerin, Genesis, Hi-Balanz, K-Palette, Meiji, Mega We Care, Okamoto, ProFlex, Real Elixir, Sebamed, Seoul Secret, Taurus, etc.</p>
<p>&nbsp;</p>
<p>All products on MOXY are certified by FDA Thailand so you can trust in quality and safety.</p>
<p>&nbsp;</p>
<p>As the online destination for Women in Thailand, MOXY is dedicated to provide the best customer service: easy, fast and reliable. Your satisfaction is our priority. Please feel free to contact our customer service for any information. Multiple payment methods are available such as credit card, Pay Pal, and cash-on-delivery. Your order will be delivered to your doorstep within 2 to 3 days in Bangkok and 3 to 5 days in other regions of Thailand. Make your shopping easier than ever!</p>
<p>&nbsp;</p>
<p>MOXY is looking forward to serving you.</p>
<p>&nbsp;</p>
<p>*Results from product usage depends on personal body type.</p>
</div>
EOD;
/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// SEO Keyword (Health Thai)
//==========================================================================
$blockTitle = "Health SEO Keyword";
$blockIdentifier = "seo-keyword";
$blockStores = array($sth);
$blockIsActive = 1;
$blockContent = <<<EOD
<div style="margin: 50px auto 30px;">
<p>Sanoga รวมสินค้าเพื่อสุขภาพและอุปกรณ์ออกกำลังกาย คัดสรรโดย MOXY</p>
<p>&nbsp;</p>
<p>คุณสามารถใช้เวลาได้อย่างเต็มที่ในการเลือกผลิตภัณฑ์ที่ต้องการ MOXY มีสินค้าให้คุณเลือกอย่างจุใจในราคาสุดคุ้ม และเราได้จัดทำข้อมูลอย่างละเอียดสำหรับสินค้าแต่ละชนิด พร้อมทั้งวิธีการสั่งซื้อที่สะดวกสบายตลอด 24 ชั่วโมง ทำให้การดูแลสุขภาพของคุณครั้งนี้ง่ายขึ้นกว่าเดิมในคลิ๊กเดียว</p>
<p>&nbsp;</p>
<p>MOXY มีสินค้าเพื่อสุขภาพและอุปกรณ์ออกกำลังกายครบครัน ไม่ว่าจะเป็นอาหารเสริม วิตามิน คอลลาเจน ผลิตภัณฑ์เสริมสมรรถภาพร่างกาย  อาหารเสริมเพื่อการออกกำลังกายและฟิตเนส เวย์โปรตีน และอาหารเสริมลดน้ำหนักจากกว่า 1,000 แบรนดํดังทั่วโลก เช่น Blackmores, Beauty Wise, Body Shape, CLG500, Centrum, Collagen Star, Colly, Dr.Absolute, DYMATIZE, Durex, Eucerin, Genesis, Hi-Balanz, K-Palette, Meiji, Mega We Care, Okamoto, ProFlex, Real Elixir, Sebamed, Seoul Secret, Taurus และอื่นๆ</p>
<p>&nbsp;</p>
<p>สินค้าทุกชิ้นของ MOXY ได้รับมาตราฐานการรับรองจากองค์การอาหารและยาแห่งประเทศไทย(อย.) จึงมั่นใจได้ในคุณภาพและความปลอดภัย</p>
<p>&nbsp;</p>
<p>สะดวก รวดเร็ว และมั่นใจได้ คือสิ่งที่ MOXY บริการให้คุณ เพราะความพึงพอใจของคุณคือสิ่งที่สำคัญที่สุดสำหรับเรา คุณสามารถติดต่อ MOXY ผ่านทางศูนย์บริการลูกค้าเพื่อสอบถามเกี่ยวกับข้อมูลต่างๆได้ตลอดเวลา และยังสามารถเลือกวิธีการชำระเงินได้ตามสะดวกไม่ว่าจะเป็น ชำระเงินผ่านบัตรเครดิตการ์ด Paypal หรือชำระเงินปลายทาง จากนั้นก็เตรียมตัวรับสินค้าที่จะถูกส่งตรงถึงหน้าบ้านคุณภายใน 2-3 วันทำการในเขตกรุงเทพฯ และ 3-5 วันทำการในเขตอื่น ไม่ว่าคุณจะอยู่ที่ไหน การช้อปปิ้งออนไลน์จะกลายเป็นเรื่องสนุกที่คุณทำได้ทุกวัน</p>
<p>&nbsp;</p>
<p>MOXY พร้อมที่จะให้บริการคุณแล้ววันนี้</p>
<p>&nbsp;</p>
<p>ผลลัพท์ที่ได้ขึ้นอยู่กับแต่ละบุคคล</p>
</div>
EOD;
/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================
