<?php

// init Magento
Mage::app();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

// get store ids
$en     = Mage::getModel('core/store')->load('en', 'code')->getId();// Petloft
$th     = Mage::getModel('core/store')->load('th', 'code')->getId();
$len    = Mage::getModel('core/store')->load('len', 'code')->getId();// Lafema
$lth    = Mage::getModel('core/store')->load('lth', 'code')->getId();
$ven    = Mage::getModel('core/store')->load('ven', 'code')->getId();// Venbi
$vth    = Mage::getModel('core/store')->load('vth', 'code')->getId();
$sen    = Mage::getModel('core/store')->load('sen', 'code')->getId();// Sanoga
$sth    = Mage::getModel('core/store')->load('sth', 'code')->getId();
$moxyen = Mage::getModel('core/store')->load('moxyen', 'code')->getId();// Moxy
$moxyth = Mage::getModel('core/store')->load('moxyth', 'code')->getId();

// UPDATE FOOTER LINKS, REMOVE LINE-CHAT BARCODE



// Petloft EN
//==========================================================================
//==========================================================================
$blockTitle = "Site Footer Links EN";
$blockIdentifier = "footer_links_1";
$blockStores = $en;
$blockIsActive = 1;

// delete old block if have
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');

$content = <<<EOD
<ul class="footer_links" style="margin-left: 0;">
<li class="title">Information</li>
<li><a href="{{store url='about-us'}}">About Us</a></li>
<li><a href="http://www.whatsnew.asia/">Corporate</a></li>
<li><a href="{{store url='contacts'}}">Contact Us</a></li>
</ul>

<ul class="footer_links" style="margin-left: 42px;">
<li class="title">Security and Privacy</li>
<li><a href="{{store url='terms-and-conditions'}}">Terms &amp; Conditions</a></li>
<li><a href="{{store url='privacy-policy'}}">Privacy Policy</a></li>
</ul>

<ul id="footer_help" class="footer_links" style="margin-left: 42px;">
<li class="title">Services and Support</li>
<li><a href="{{store url='help'}}">Help and FAQ</a></li>
<li><a href="{{store url='help'}}#howtoorder">How To Order</a></li>
<li><a href="{{store url='help'}}#payments">Payments</a></li>
<li><a href="{{store url='help'}}#rewardpoints">Reward Points</a></li>
</ul>

<ul class="footer_links" style="margin-left: -20px; margin-top: 24px;">
<li><a href="{{store url='help'}}#shipping">Shipping &amp; Delivery</a></li>
<li><a href="{{store url='help'}}#returns">Cancellation &amp; Returns</a></li>
<li><a href="{{store url='return-policy'}}">Return Policy</a></li>
</ul>

<!--<ul class="footer_links" style="margin-left: 34px;">
<li class="title">LINE CHAT</li>
<li><img style="width: 100px; height: 100px; margin-left: -15px;" src="{{media url="wysiwyg/PetLoft/PETLOFT_QR.jpg"}}" alt="" /></li>
</ul>-->
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$enBlock = Mage::getModel('cms/block');
$enBlock->setTitle($blockTitle);
$enBlock->setIdentifier($blockIdentifier);
$enBlock->setStores($blockStores);
$enBlock->setIsActive(1);
$enBlock->setContent($content);
$enBlock->save();
//==========================================================================
//==========================================================================



// Petloft TH
//==========================================================================
//==========================================================================
$blockTitle = "Site Footer Links TH";
$blockIdentifier = "footer_links_1";
$blockStores = $th;
$blockIsActive = 1;

// delete old block if have
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');

$content = <<<EOD
<ul class="footer_links" style="margin-left: 0;">
<li class="title">ข้อมูล</li>
<li><a href="{{store url='about-us'}}">เกี่ยวกับเรา</a></li>
<li><a href="http://www.whatsnew.asia/">องค์กร</a></li>
<li><a href="{{store url='contacts'}}">ติดต่อเรา</a></li>
</ul>

<ul class="footer_links" style="margin-left: 42px;">
<li class="title">นโยบายความเป็นส่วนตัว</li>
<li><a href="{{store url='terms-and-conditions'}}">ข้อกำหนดและเงื่อนไข</a></li>
<li><a href="{{store url='privacy-policy'}}">ความเป็นส่วนตัว</a></li>
</ul>

<ul id="footer_help" class="footer_links" style="margin-left: 42px;">
<li class="title">ช่วยเหลือ</li>
<li><a href="{{store url='help'}}">คำถามที่พบบ่อย</a></li>
<li><a href="{{store url='help'}}#howtoorder">วิธีการสั่งซื้อ</a></li>
<li><a href="{{store url='help'}}#payments">การชำระเงิน</a></li>
<li><a href="{{store url='help'}}#rewardpoints">คะแนนสะสม</a></li>
</ul>

<ul class="footer_links" style="margin-left: 0px; margin-top: 24px;">
<li><a href="{{store url='help'}}#shipping">การจัดส่งสินค้า</a></li>
<li><a href="{{store url='help'}}#returns">การคืนสินค้าและขอคืนเงิน</a></li>
<li><a href="{{store url='return-policy'}}">นโยบายการคืนสินค้า</a></li>
</ul>

<!--<ul class="footer_links" style="margin-left: 34px;">
<li class="title">LINE CHAT</li>
<li><img style="width: 100px; height: 100px; margin-left: -15px;" src="{{media url="wysiwyg/PetLoft/PETLOFT_QR.jpg"}}" alt="" /></li>
</ul>-->
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$enBlock = Mage::getModel('cms/block');
$enBlock->setTitle($blockTitle);
$enBlock->setIdentifier($blockIdentifier);
$enBlock->setStores($blockStores);
$enBlock->setIsActive(1);
$enBlock->setContent($content);
$enBlock->save();
//==========================================================================
//==========================================================================



// Venbi EN
//==========================================================================
//==========================================================================
$blockTitle = "Site Footer Links EN";
$blockIdentifier = "footer_links_1";
$blockStores = $ven;
$blockIsActive = 1;

// delete old block if have
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');

$content = <<<EOD
<ul class="footer_links" style="margin-left: 0;">
<li class="title">Information</li>
<li><a href="{{store url='about-us'}}">About Us</a></li>
<li><a href="http://www.whatsnew.asia/">Corporate</a></li>
<li><a href="{{store url='contacts'}}">Contact Us</a></li>
</ul>

<ul class="footer_links" style="margin-left: 42px;">
<li class="title">Security and Privacy</li>
<li><a href="{{store url='terms-and-conditions'}}">Terms &amp; Conditions</a></li>
<li><a href="{{store url='privacy-policy'}}">Privacy Policy</a></li>
</ul>

<ul id="footer_help" class="footer_links" style="margin-left: 42px;">
<li class="title">Services and Support</li>
<li><a href="{{store url='help'}}">Help and FAQ</a></li>
<li><a href="{{store url='help'}}#howtoorder">How To Order</a></li>
<li><a href="{{store url='help'}}#payments">Payments</a></li>
<li><a href="{{store url='help'}}#rewardpoints">Reward Points</a></li>
</ul>

<ul class="footer_links" style="margin-left: -20px; margin-top: 24px;">
<li><a href="{{store url='help'}}#shipping">Shipping &amp; Delivery</a></li>
<li><a href="{{store url='help'}}#returns">Cancellation &amp; Returns</a></li>
<li><a href="{{store url='return-policy'}}">Return Policy</a></li>
</ul>

<!--<ul class="footer_links" style="margin-left: 34px;">
<li class="title">LINE CHAT</li>
<li><img style="width: 100px; height: 100px; margin-left: -15px;" src="{{media url="wysiwyg/Venbi/VENBI_QR.jpg"}}" alt="" /></li>
</ul>-->
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$enBlock = Mage::getModel('cms/block');
$enBlock->setTitle($blockTitle);
$enBlock->setIdentifier($blockIdentifier);
$enBlock->setStores($blockStores);
$enBlock->setIsActive(1);
$enBlock->setContent($content);
$enBlock->save();
//==========================================================================
//==========================================================================



// Venbi TH
//==========================================================================
//==========================================================================
$blockTitle = "Site Footer Links TH";
$blockIdentifier = "footer_links_1";
$blockStores = $vth;
$blockIsActive = 1;

// delete old block if have
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');

$content = <<<EOD
<ul class="footer_links" style="margin-left: 0;">
<li class="title">ข้อมูล</li>
<li><a href="{{store url='about-us'}}">เกี่ยวกับเรา</a></li>
<li><a href="http://www.whatsnew.asia/">องค์กร</a></li>
<li><a href="{{store url='contacts'}}">ติดต่อเรา</a></li>
</ul>

<ul class="footer_links" style="margin-left: 42px;">
<li class="title">นโยบายความเป็นส่วนตัว</li>
<li><a href="{{store url='terms-and-conditions'}}">ข้อกำหนดและเงื่อนไข</a></li>
<li><a href="{{store url='privacy-policy'}}">ความเป็นส่วนตัว</a></li>
</ul>

<ul id="footer_help" class="footer_links" style="margin-left: 42px;">
<li class="title">ช่วยเหลือ</li>
<li><a href="{{store url='help'}}">คำถามที่พบบ่อย</a></li>
<li><a href="{{store url='help'}}#howtoorder">วิธีการสั่งซื้อ</a></li>
<li><a href="{{store url='help'}}#payments">การชำระเงิน</a></li>
<li><a href="{{store url='help'}}#rewardpoints">คะแนนสะสม</a></li>
</ul>

<ul class="footer_links" style="margin-left: 0px; margin-top: 24px;">
<li><a href="{{store url='help'}}#shipping">การจัดส่งสินค้า</a></li>
<li><a href="{{store url='help'}}#returns">การคืนสินค้าและขอคืนเงิน</a></li>
<li><a href="{{store url='return-policy'}}">นโยบายการคืนสินค้า</a></li>
</ul>

<!--<ul class="footer_links" style="margin-left: 34px;">
<li class="title">LINE CHAT</li>
<li><img style="width: 100px; height: 100px; margin-left: -15px;" src="{{media url="wysiwyg/Venbi/VENBI_QR.jpg"}}" alt="" /></li>
</ul>-->
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$enBlock = Mage::getModel('cms/block');
$enBlock->setTitle($blockTitle);
$enBlock->setIdentifier($blockIdentifier);
$enBlock->setStores($blockStores);
$enBlock->setIsActive(1);
$enBlock->setContent($content);
$enBlock->save();
//==========================================================================
//==========================================================================



// Sanoga EN
//==========================================================================
//==========================================================================
$blockTitle = "Site Footer Links EN";
$blockIdentifier = "footer_links_1";
$blockStores = $sen;
$blockIsActive = 1;

// delete old block if have
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');

$content = <<<EOD
<ul class="footer_links" style="margin-left: 0;">
<li class="title">Information</li>
<li><a href="{{store url='about-us'}}">About Us</a></li>
<li><a href="http://www.whatsnew.asia/">Corporate</a></li>
<li><a href="{{store url='contacts'}}">Contact Us</a></li>
</ul>

<ul class="footer_links" style="margin-left: 42px;">
<li class="title">Security and Privacy</li>
<li><a href="{{store url='terms-and-conditions'}}">Terms &amp; Conditions</a></li>
<li><a href="{{store url='privacy-policy'}}">Privacy Policy</a></li>
</ul>

<ul id="footer_help" class="footer_links" style="margin-left: 42px;">
<li class="title">Services and Support</li>
<li><a href="{{store url='help'}}">Help and FAQ</a></li>
<li><a href="{{store url='help'}}#howtoorder">How To Order</a></li>
<li><a href="{{store url='help'}}#payments">Payments</a></li>
<li><a href="{{store url='help'}}#rewardpoints">Reward Points</a></li>
</ul>

<ul class="footer_links" style="margin-left: -20px; margin-top: 24px;">
<li><a href="{{store url='help'}}#shipping">Shipping &amp; Delivery</a></li>
<li><a href="{{store url='help'}}#returns">Cancellation &amp; Returns</a></li>
<li><a href="{{store url='return-policy'}}">Return Policy</a></li>
</ul>

<!--<ul class="footer_links" style="margin-left: 34px;">
<li class="title">LINE CHAT</li>
<li><img style="width: 100px; height: 100px; margin-left: -15px;" src="{{media url="wysiwyg/Sanoga/SANOGA_QR.jpg"}}" alt="" /></li>
</ul>-->
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$enBlock = Mage::getModel('cms/block');
$enBlock->setTitle($blockTitle);
$enBlock->setIdentifier($blockIdentifier);
$enBlock->setStores($blockStores);
$enBlock->setIsActive(1);
$enBlock->setContent($content);
$enBlock->save();
//==========================================================================
//==========================================================================



// Sanoga TH
//==========================================================================
//==========================================================================
$blockTitle = "Site Footer Links TH";
$blockIdentifier = "footer_links_1";
$blockStores = $sth;
$blockIsActive = 1;

// delete old block if have
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');

$content = <<<EOD
<ul class="footer_links" style="margin-left: 0;">
<li class="title">ข้อมูล</li>
<li><a href="{{store url='about-us'}}">เกี่ยวกับเรา</a></li>
<li><a href="http://www.whatsnew.asia/">องค์กร</a></li>
<li><a href="{{store url='contacts'}}">ติดต่อเรา</a></li>
</ul>

<ul class="footer_links" style="margin-left: 42px;">
<li class="title">นโยบายความเป็นส่วนตัว</li>
<li><a href="{{store url='terms-and-conditions'}}">ข้อกำหนดและเงื่อนไข</a></li>
<li><a href="{{store url='privacy-policy'}}">ความเป็นส่วนตัว</a></li>
</ul>

<ul id="footer_help" class="footer_links" style="margin-left: 42px;">
<li class="title">ช่วยเหลือ</li>
<li><a href="{{store url='help'}}">คำถามที่พบบ่อย</a></li>
<li><a href="{{store url='help'}}#howtoorder">วิธีการสั่งซื้อ</a></li>
<li><a href="{{store url='help'}}#payments">การชำระเงิน</a></li>
<li><a href="{{store url='help'}}#rewardpoints">คะแนนสะสม</a></li>
</ul>

<ul class="footer_links" style="margin-left: 0px; margin-top: 24px;">
<li><a href="{{store url='help'}}#shipping">การจัดส่งสินค้า</a></li>
<li><a href="{{store url='help'}}#returns">การคืนสินค้าและขอคืนเงิน</a></li>
<li><a href="{{store url='return-policy'}}">นโยบายการคืนสินค้า</a></li>
</ul>

<!--<ul class="footer_links" style="margin-left: 34px;">
<li class="title">LINE CHAT</li>
<li><img style="width: 100px; height: 100px; margin-left: -15px;" src="{{media url="wysiwyg/Sanoga/SANOGA_QR.jpg"}}" alt="" /></li>
</ul>-->
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$enBlock = Mage::getModel('cms/block');
$enBlock->setTitle($blockTitle);
$enBlock->setIdentifier($blockIdentifier);
$enBlock->setStores($blockStores);
$enBlock->setIsActive(1);
$enBlock->setContent($content);
$enBlock->save();
//==========================================================================
//==========================================================================



// Lafema EN
//==========================================================================
//==========================================================================
$blockTitle = "Site Footer Links EN";
$blockIdentifier = "footer_links_1";
$blockStores = $len;
$blockIsActive = 1;

// delete old block if have
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');

$content = <<<EOD
<ul class="footer_links" style="margin-left: 0;">
<li class="title">Information</li>
<li><a href="{{store url='about-us'}}">About Us</a></li>
<li><a href="http://www.whatsnew.asia/">Corporate</a></li>
<li><a href="{{store url='contacts'}}">Contact Us</a></li>
</ul>

<ul class="footer_links" style="margin-left: 42px;">
<li class="title">Security and Privacy</li>
<li><a href="{{store url='terms-and-conditions'}}">Terms &amp; Conditions</a></li>
<li><a href="{{store url='privacy-policy'}}">Privacy Policy</a></li>
</ul>

<ul id="footer_help" class="footer_links" style="margin-left: 42px;">
<li class="title">Services and Support</li>
<li><a href="{{store url='help'}}">Help and FAQ</a></li>
<li><a href="{{store url='help'}}#howtoorder">How To Order</a></li>
<li><a href="{{store url='help'}}#payments">Payments</a></li>
<li><a href="{{store url='help'}}#rewardpoints">Reward Points</a></li>
</ul>

<ul class="footer_links" style="margin-left: -20px; margin-top: 24px;">
<li><a href="{{store url='help'}}#shipping">Shipping &amp; Delivery</a></li>
<li><a href="{{store url='help'}}#returns">Cancellation &amp; Returns</a></li>
<li><a href="{{store url='return-policy'}}">Return Policy</a></li>
</ul>

<!--<ul class="footer_links" style="margin-left: 34px;">
<li class="title">LINE CHAT</li>
<li><img style="width: 100px; height: 100px; margin-left: -15px;" src="{{media url="wysiwyg/Lafema/LAFEMA_QR.jpg"}}" alt="" /></li>
</ul>-->
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$enBlock = Mage::getModel('cms/block');
$enBlock->setTitle($blockTitle);
$enBlock->setIdentifier($blockIdentifier);
$enBlock->setStores($blockStores);
$enBlock->setIsActive(1);
$enBlock->setContent($content);
$enBlock->save();
//==========================================================================
//==========================================================================



// Lafema TH
//==========================================================================
//==========================================================================
$blockTitle = "Site Footer Links TH";
$blockIdentifier = "footer_links_1";
$blockStores = $lth;
$blockIsActive = 1;

// delete old block if have
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');

$content = <<<EOD
<ul class="footer_links" style="margin-left: 0;">
<li class="title">ข้อมูล</li>
<li><a href="{{store url='about-us'}}">เกี่ยวกับเรา</a></li>
<li><a href="http://www.whatsnew.asia/">องค์กร</a></li>
<li><a href="{{store url='contacts'}}">ติดต่อเรา</a></li>
</ul>

<ul class="footer_links" style="margin-left: 42px;">
<li class="title">นโยบายความเป็นส่วนตัว</li>
<li><a href="{{store url='terms-and-conditions'}}">ข้อกำหนดและเงื่อนไข</a></li>
<li><a href="{{store url='privacy-policy'}}">ความเป็นส่วนตัว</a></li>
</ul>

<ul id="footer_help" class="footer_links" style="margin-left: 42px;">
<li class="title">ช่วยเหลือ</li>
<li><a href="{{store url='help'}}">คำถามที่พบบ่อย</a></li>
<li><a href="{{store url='help'}}#howtoorder">วิธีการสั่งซื้อ</a></li>
<li><a href="{{store url='help'}}#payments">การชำระเงิน</a></li>
<li><a href="{{store url='help'}}#rewardpoints">คะแนนสะสม</a></li>
</ul>

<ul class="footer_links" style="margin-left: 0px; margin-top: 24px;">
<li><a href="{{store url='help'}}#shipping">การจัดส่งสินค้า</a></li>
<li><a href="{{store url='help'}}#returns">การคืนสินค้าและขอคืนเงิน</a></li>
<li><a href="{{store url='return-policy'}}">นโยบายการคืนสินค้า</a></li>
</ul>

<!--<ul class="footer_links" style="margin-left: 34px;">
<li class="title">LINE CHAT</li>
<li><img style="width: 100px; height: 100px; margin-left: -15px;" src="{{media url="wysiwyg/Lafema/LAFEMA_QR.jpg"}}" alt="" /></li>
</ul>-->
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$enBlock = Mage::getModel('cms/block');
$enBlock->setTitle($blockTitle);
$enBlock->setIdentifier($blockIdentifier);
$enBlock->setStores($blockStores);
$enBlock->setIsActive(1);
$enBlock->setContent($content);
$enBlock->save();
//==========================================================================
//==========================================================================



// Moxy EN
//==========================================================================
//==========================================================================
$blockTitle = "Site Footer Links EN";
$blockIdentifier = "footer_links_1";
$blockStores = $moxyen;
$blockIsActive = 1;

// delete old block if have
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');

$content = <<<EOD
<ul class="footer_links" style="margin-left: 0;">
<li class="title">Information</li>
<li><a href="{{store url='about-us'}}">About Us</a></li>
<li><a href="http://www.whatsnew.asia/">Corporate</a></li>
<li><a href="{{store url='contacts'}}">Contact Us</a></li>
</ul>

<ul class="footer_links" style="margin-left: 42px;">
<li class="title">Security and Privacy</li>
<li><a href="{{store url='terms-and-conditions'}}">Terms &amp; Conditions</a></li>
<li><a href="{{store url='privacy-policy'}}">Privacy Policy</a></li>
</ul>

<ul id="footer_help" class="footer_links" style="margin-left: 42px;">
<li class="title">Services and Support</li>
<li><a href="{{store url='help'}}">Help and FAQ</a></li>
<li><a href="{{store url='help'}}#howtoorder">How To Order</a></li>
<li><a href="{{store url='help'}}#payments">Payments</a></li>
<li><a href="{{store url='help'}}#rewardpoints">Reward Points</a></li>
</ul>

<ul class="footer_links" style="margin-left: -20px; margin-top: 24px;">
<li><a href="{{store url='help'}}#shipping">Shipping &amp; Delivery</a></li>
<li><a href="{{store url='help'}}#returns">Cancellation &amp; Returns</a></li>
<li><a href="{{store url='return-policy'}}">Return Policy</a></li>
</ul>

<!--<ul class="footer_links" style="margin-left: 34px;">
<li class="title">LINE CHAT</li>
<li><img style="width: 100px; height: 100px; margin-left: -15px;" src="{{media url="wysiwyg/Moxy/MOXY_QR.jpg"}}" alt="" /></li>
</ul>-->
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$enBlock = Mage::getModel('cms/block');
$enBlock->setTitle($blockTitle);
$enBlock->setIdentifier($blockIdentifier);
$enBlock->setStores($blockStores);
$enBlock->setIsActive(1);
$enBlock->setContent($content);
$enBlock->save();
//==========================================================================
//==========================================================================



// Moxy TH
//==========================================================================
//==========================================================================
$blockTitle = "Site Footer Links TH";
$blockIdentifier = "footer_links_1";
$blockStores = $moxyth;
$blockIsActive = 1;

// delete old block if have
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');

$content = <<<EOD
<ul class="footer_links" style="margin-left: 0;">
<li class="title">ข้อมูล</li>
<li><a href="{{store url='about-us'}}">เกี่ยวกับเรา</a></li>
<li><a href="http://www.whatsnew.asia/">องค์กร</a></li>
<li><a href="{{store url='contacts'}}">ติดต่อเรา</a></li>
</ul>

<ul class="footer_links" style="margin-left: 42px;">
<li class="title">นโยบายความเป็นส่วนตัว</li>
<li><a href="{{store url='terms-and-conditions'}}">ข้อกำหนดและเงื่อนไข</a></li>
<li><a href="{{store url='privacy-policy'}}">ความเป็นส่วนตัว</a></li>
</ul>

<ul id="footer_help" class="footer_links" style="margin-left: 42px;">
<li class="title">ช่วยเหลือ</li>
<li><a href="{{store url='help'}}">คำถามที่พบบ่อย</a></li>
<li><a href="{{store url='help'}}#howtoorder">วิธีการสั่งซื้อ</a></li>
<li><a href="{{store url='help'}}#payments">การชำระเงิน</a></li>
<li><a href="{{store url='help'}}#rewardpoints">คะแนนสะสม</a></li>
</ul>

<ul class="footer_links" style="margin-left: 0px; margin-top: 24px;">
<li><a href="{{store url='help'}}#shipping">การจัดส่งสินค้า</a></li>
<li><a href="{{store url='help'}}#returns">การคืนสินค้าและขอคืนเงิน</a></li>
<li><a href="{{store url='return-policy'}}">นโยบายการคืนสินค้า</a></li>
</ul>

<!--<ul class="footer_links" style="margin-left: 34px;">
<li class="title">LINE CHAT</li>
<li><img style="width: 100px; height: 100px; margin-left: -15px;" src="{{media url="wysiwyg/Moxy/MOXY_QR.jpg"}}" alt="" /></li>
</ul>-->
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$enBlock = Mage::getModel('cms/block');
$enBlock->setTitle($blockTitle);
$enBlock->setIdentifier($blockIdentifier);
$enBlock->setStores($blockStores);
$enBlock->setIsActive(1);
$enBlock->setContent($content);
$enBlock->save();
//==========================================================================
//==========================================================================



// UPDATE FOOTER LINKS FOR WELCOME PAGE



// ENG stores
//==========================================================================
//==========================================================================
$blockTitle = "Welcome Splash footer EN";
$blockIdentifier = "welcome-footer";
$blockStores = array($len,$moxyen,$en,$sen,$ven);
$blockIsActive = 1;

// delete old block if have
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');

$content = <<<EOD
<div class="footer-mobile hidden-md hidden-lg"><a href="http://moxy.co.th/"><img src="{{media url="wysiwyg/welcomePage/logo-mobile.png"}}" alt="2015, Moxy Co.Ltd, All right reserved" /></a>
<div class="txt-reserved"><em class="fa fa-copyright"></em>2015, Moxy Co.Ltd, All right reserved</div>
</div>
<div class="container hidden-xs hidden-sm" style="padding: 0 5px 15px;">
<div class="footer-box">
<h3 class="footer-title">Information</h3>
<ul class="footer_links">
<li><a href="{{config path="web/unsecure/base_url "}}about-us/">About Us</a></li>
<li><a href="http://www.whatsnew.asia/" target="_blank">Corporate</a></li>
<li><a href="{{config path="web/unsecure/base_url "}}contacts/">Contact Us</a></li>
</ul>
</div>
<div class="footer-box">
<h3 class="footer-title">Security and Privacy</h3>
<ul class="footer_links">
<li><a href="{{config path="web/unsecure/base_url "}}terms-and-conditions/">Terms &amp; Conditions</a></li>
<li><a href="{{config path="web/unsecure/base_url "}}privacy-policy/">Privacy Policy</a></li>
</ul>
</div>
<div class="footer-box" style="width: 300px;">
<h3 class="footer-title">Services and Support</h3>
<div class="col-md-6">
<ul class="footer_links">
<li><a href="{{config path="web/unsecure/base_url "}}help/">Help and FAQ</a></li>
<li><a href="{{config path="web/unsecure/base_url "}}help/#howtoorder">How To Order</a></li>
<li><a href="{{config path="web/unsecure/base_url "}}help/#payments">Payments</a></li>
<li><a href="{{config path="web/unsecure/base_url "}}help/#rewardpoints">Reward Points</a></li>
</ul>
</div>
<div class="col-md-6">
<ul class="footer_links">
<li><a href="{{config path="web/unsecure/base_url "}}help/#shipping">Shipping &amp; Delivery</a></li>
<li><a href="{{config path="web/unsecure/base_url "}}help/#returns">Cancellation &amp; Returns</a></li>
<li><a href="{{config path="web/unsecure/base_url "}}return-policy/">Return Policy</a></li>
</ul>
</div>
</div>
<!--<div class="footer-box">
<h3 class="footer-title">LINE CHAT</h3>
<ul class="footer_links">
<li><img style="width: 100px; height: 100px;" src="{{media url="wysiwyg/MOXY_QR.jpg"}}" alt="" /></li>
</ul>
</div>-->
<div class="footer-box" style="margin: 0px;float: right;">
<h3 class="footer-title">Connect</h3>
<div class="item_shares">
<p><a href="https://plus.google.com/+Moxyst" target="_blank"> <span class="google-footer"><img src="{{media url="wysiwyg/footer_follow/google_follow.png"}}" alt="" width="32px" /></span> </a> <a href="https://pinterest.com/#" target="_blank"> <span class="pinterest-footer"><img src="{{media url="wysiwyg/footer_follow/pinterest_follow.png"}}" alt="" width="32px" /></span> </a> <a href="https://webstagra.ms/moxyst/" target="_blank"> <span class="instagram-footer"><img src="{{media url="wysiwyg/footer_follow/instagram_follow.png"}}" alt="" width="32px" /></span> </a> <a href="https://twitter.com/moxyst" target="_blank"> <span class="twitter-footer"><img src="{{media url="wysiwyg/footer_follow/twitter_follow.png"}}" alt="" width="32px" /></span> </a> <a href="https://www.facebook.com/moxyst" target="_blank"> <span class="facebook-footer"><img src="{{media url="wysiwyg/footer_follow/facebook_follow.png"}}" alt="" width="32px" /></span> </a></p>
</div>
</div>
<div class="col-md-6 row">
<h3 class="footer-title">Payment Partners</h3>
<img src="{{media url="wysiwyg/welcomePage/bank_images_footer.png "}}" alt="" width="450px;" /></div>
<div class="col-md-6 row">
<h3 class="footer-title">Shipping Partners</h3>
<img src="{{media url="wysiwyg/acommerce_footer.png "}}" alt="" width="185px;" /></div>
</div>
<div class="copyright_box  hidden-xs hidden-sm">
<div class="container" style="padding: 0px 5px;">
<p class="copyright">&copy; 2015 Whatsnew Co. Ltd.</p>
</div>
</div>
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$enBlock = Mage::getModel('cms/block');
$enBlock->setTitle($blockTitle);
$enBlock->setIdentifier($blockIdentifier);
$enBlock->setStores($blockStores);
$enBlock->setIsActive(1);
$enBlock->setContent($content);
$enBlock->save();
//==========================================================================
//==========================================================================



// THAI stores
//==========================================================================
//==========================================================================
$blockTitle = "Welcome Splash footer TH";
$blockIdentifier = "welcome-footer";
$blockStores = array($lth,$moxyth,$th,$sth,$vth);
$blockIsActive = 1;

// delete old block if have
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');

$content = <<<EOD
<div class="footer-mobile hidden-md hidden-lg"><a href="http://moxy.co.th/"><img src="{{media url="wysiwyg/welcomePage/logo-mobile.png"}}" alt="2015, Moxy Co.Ltd, All right reserved" /></a>
<div class="txt-reserved"><em class="fa fa-copyright"></em>2015, Moxy Co.Ltd, All right reserved</div>
</div>
<div class="container hidden-xs hidden-sm" style="padding: 0 5px 15px;">
<div class="footer-box">
<h3 class="footer-title">ข้อมูล</h3>
<ul class="footer_links">
<li><a href="{{config path="web/unsecure/base_url "}}about-us/">เกี่ยวกับเรา</a></li>
<li><a href="http://www.whatsnew.asia/" target="_blank">องค์กร</a></li>
<li><a href="{{config path="web/unsecure/base_url "}}contacts/">ติดต่อเรา</a></li>
</ul>
</div>
<div class="footer-box">
<h3 class="footer-title">นโยบายความเป็นส่วนตัว</h3>
<ul class="footer_links">
<li><a href="{{config path="web/unsecure/base_url "}}terms-and-conditions/">ข้อกำหนดและเงื่อนไข</a></li>
<li><a href="{{config path="web/unsecure/base_url "}}privacy-policy/">ความเป็นส่วนตัว</a></li>
</ul>
</div>
<div class="footer-box" style="width: 300px;">
<h3 class="footer-title">ช่วยเหลือ</h3>
<div class="col-md-6">
<ul class="footer_links">
<li><a href="{{config path="web/unsecure/base_url "}}help/">คำถามที่พบบ่อย</a></li>
<li><a href="{{config path="web/unsecure/base_url "}}help/#howtoorder">วิธีการสั่งซื้อ</a></li>
<li><a href="{{config path="web/unsecure/base_url "}}help/#payments">การชำระเงิน</a></li>
<li><a href="{{config path="web/unsecure/base_url "}}help/#rewardpoints">คะแนนสะสม</a></li>
</ul>
</div>
<div class="col-md-6">
<ul class="footer_links">
<li><a href="{{config path="web/unsecure/base_url "}}help/#shipping">การจัดส่งสินค้า</a></li>
<li><a href="{{config path="web/unsecure/base_url "}}help/#returns">การคืนสินค้าและขอคืนเงิน</a></li>
<li><a href="{{config path="web/unsecure/base_url "}}return-policy/">นโยบายการคืนสินค้า</a></li>
</ul>
</div>
</div>
<!--<div class="footer-box">
<h3 class="footer-title">LINE CHAT</h3>
<ul class="footer_links">
<li><img style="width: 100px; height: 100px;" src="{{media url="wysiwyg/MOXY_QR.jpg"}}" alt="" /></li>
</ul>
</div>-->
<div class="footer-box" style="margin: 0px;float: right;">
<h3 class="footer-title">Connect</h3>
<div class="item_shares">
<p><a href="https://plus.google.com/+Moxyst" target="_blank"> <span class="google-footer"><img src="{{media url="wysiwyg/footer_follow/google_follow.png"}}" alt="" width="32px" /></span> </a> <a href="https://pinterest.com/#" target="_blank"> <span class="pinterest-footer"><img src="{{media url="wysiwyg/footer_follow/pinterest_follow.png"}}" alt="" width="32px" /></span> </a> <a href="https://webstagra.ms/moxyst/" target="_blank"> <span class="instagram-footer"><img src="{{media url="wysiwyg/footer_follow/instagram_follow.png"}}" alt="" width="32px" /></span> </a> <a href="https://twitter.com/moxyst" target="_blank"> <span class="twitter-footer"><img src="{{media url="wysiwyg/footer_follow/twitter_follow.png"}}" alt="" width="32px" /></span> </a> <a href="https://www.facebook.com/moxyst" target="_blank"> <span class="facebook-footer"><img src="{{media url="wysiwyg/footer_follow/facebook_follow.png"}}" alt="" width="32px" /></span> </a></p>
</div>
</div>
<div class="col-md-6 row">
<h3 class="footer-title">Payment Partners</h3>
<img src="{{media url="wysiwyg/welcomePage/bank_images_footer.png "}}" alt="" width="450px;" /></div>
<div class="col-md-6 row">
<h3 class="footer-title">Shipping Partners</h3>
<img src="{{media url="wysiwyg/acommerce_footer.png "}}" alt="" width="185px;" /></div>
</div>
<div class="copyright_box  hidden-xs hidden-sm">
<div class="container" style="padding: 0px 5px;">
<p class="copyright">&copy; 2015 Whatsnew Co. Ltd.</p>
</div>
</div>
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$enBlock = Mage::getModel('cms/block');
$enBlock->setTitle($blockTitle);
$enBlock->setIdentifier($blockIdentifier);
$enBlock->setStores($blockStores);
$enBlock->setIsActive(1);
$enBlock->setContent($content);
$enBlock->save();
//==========================================================================
//==========================================================================