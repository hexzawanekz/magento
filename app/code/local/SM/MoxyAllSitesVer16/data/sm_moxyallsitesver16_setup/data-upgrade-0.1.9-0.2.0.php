<?php

// init Magento
Mage::app();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

// get store ids
$en     = Mage::getModel('core/store')->load('en', 'code')->getId();// Petloft
$th     = Mage::getModel('core/store')->load('th', 'code')->getId();
$len    = Mage::getModel('core/store')->load('len', 'code')->getId();// Lafema
$lth    = Mage::getModel('core/store')->load('lth', 'code')->getId();
$ven    = Mage::getModel('core/store')->load('ven', 'code')->getId();// Venbi
$vth    = Mage::getModel('core/store')->load('vth', 'code')->getId();
$sen    = Mage::getModel('core/store')->load('sen', 'code')->getId();// Sanoga
$sth    = Mage::getModel('core/store')->load('sth', 'code')->getId();
$moxyen = Mage::getModel('core/store')->load('moxyen', 'code')->getId();// Moxy
$moxyth = Mage::getModel('core/store')->load('moxyth', 'code')->getId();


// EDIT HOMEPAGE OF MOXY CHILD STORES


//==========================================================================
// home_fashion (Moxy English)
//==========================================================================
$blockTitle = "Moxy Fashion Homepage";
$blockIdentifier = "home_fashion";
$blockStores = array($storeIdMoxyEn);
$blockIsActive = 1;
$blockContent = <<<EOD
<p>{{block type="core/template" name="subscription" as="subscription" template="subscription/subscribe.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="2440" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="2138" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="2140" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="2141" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="2142" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="cms/block" block_id="all-brands-slider-fashion"}}{{block type="cms/block" block_id="seo-keyword-fashion"}}</p>
EOD;
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// home_electronics (Moxy English)
//==========================================================================
$blockTitle = "Moxy Electronics Homepage";
$blockIdentifier = "home_electronics";
$blockStores = array($storeIdMoxyEn);
$blockIsActive = 1;
$blockContent = <<<EOD
<p>{{block type="core/template" name="subscription" as="subscription" template="subscription/subscribe.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="2440" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="2138" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="2140" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="2141" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="2142" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="cms/block" block_id="all-brands-slider-electronics"}}{{block type="cms/block" block_id="seo-keyword-electronics"}}</p>
EOD;
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// home_homeliving (Moxy English)
//==========================================================================
$blockTitle = "Moxy Living Homepage";
$blockIdentifier = "home_homeliving";
$blockStores = array($storeIdMoxyEn);
$blockIsActive = 1;
$blockContent = <<<EOD
<p>{{block type="core/template" name="subscription" as="subscription" template="subscription/subscribe.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="2440" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="2138" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="2140" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="2141" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="2142" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="cms/block" block_id="all-brands-slider-homeliving"}}{{block type="cms/block" block_id="seo-keyword-homeliving"}}</p>
EOD;
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// home_fashion (Moxy Thai)
//==========================================================================
$blockTitle = "Moxy Fashion Homepage";
$blockIdentifier = "home_fashion";
$blockStores = array($storeIdMoxyTh);
$blockIsActive = 1;
$blockContent = <<<EOD
<p>{{block type="core/template" name="subscription" as="subscription" template="subscription/subscribe.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="2440" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="2138" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="2140" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="2141" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="2142" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="cms/block" block_id="all-brands-slider-fashion"}}{{block type="cms/block" block_id="seo-keyword-fashion"}}</p>
EOD;
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// home_electronics (Moxy Thai)
//==========================================================================
$blockTitle = "Moxy Electronics Homepage";
$blockIdentifier = "home_electronics";
$blockStores = array($storeIdMoxyTh);
$blockIsActive = 1;
$blockContent = <<<EOD
<p>{{block type="core/template" name="subscription" as="subscription" template="subscription/subscribe.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="2440" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="2138" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="2140" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="2141" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="2142" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="cms/block" block_id="all-brands-slider-electronics"}}{{block type="cms/block" block_id="seo-keyword-electronics"}}</p>
EOD;
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// Site Product Sliders Home Moxy Living th block
//==========================================================================
$blockTitle = "Moxy Living Homepage";
$blockIdentifier = "home_homeliving";
$blockStores = array($storeIdMoxyTh);
$blockIsActive = 1;
$blockContent = <<<EOD
<p>{{block type="core/template" name="subscription" as="subscription" template="subscription/subscribe.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="2440" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="2138" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="2140" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="2141" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="2142" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="cms/block" block_id="all-brands-slider-homeliving"}}{{block type="cms/block" block_id="seo-keyword-homeliving"}}</p>
EOD;
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================



// ADD NEW "all-brands-slider" and "seo-keyword" for each Moxy child stores



//==========================================================================
// all-brands-slider-fashion (Moxy English)
//==========================================================================
$blockTitle = "Moxy Fashion brands slider";
$blockIdentifier = "all-brands-slider-fashion";
$blockStores = array($moxyen);
$blockIsActive = 0;

// delete old block if have
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete

$content = <<<EOD

EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$enBlock = Mage::getModel('cms/block');
$enBlock->setTitle($blockTitle);
$enBlock->setIdentifier($blockIdentifier);
$enBlock->setStores($blockStores);
$enBlock->setIsActive($blockIsActive);
$enBlock->setContent($content);
$enBlock->save();
//==========================================================================
//==========================================================================



//==========================================================================
// all-brands-slider-fashion (Moxy Thai)
//==========================================================================
$blockTitle = "Moxy Fashion brands slider";
$blockIdentifier = "all-brands-slider-fashion";
$blockStores = array($moxyth);
$blockIsActive = 0;

// delete old block if have
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete

$content = <<<EOD

EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$enBlock = Mage::getModel('cms/block');
$enBlock->setTitle($blockTitle);
$enBlock->setIdentifier($blockIdentifier);
$enBlock->setStores($blockStores);
$enBlock->setIsActive($blockIsActive);
$enBlock->setContent($content);
$enBlock->save();
//==========================================================================
//==========================================================================



//==========================================================================
// all-brands-slider-homeliving (Moxy English)
//==========================================================================
$blockTitle = "Moxy Home Living brands slider";
$blockIdentifier = "all-brands-slider-homeliving";
$blockStores = array($moxyen);
$blockIsActive = 0;

// delete old block if have
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete

$content = <<<EOD

EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$enBlock = Mage::getModel('cms/block');
$enBlock->setTitle($blockTitle);
$enBlock->setIdentifier($blockIdentifier);
$enBlock->setStores($blockStores);
$enBlock->setIsActive($blockIsActive);
$enBlock->setContent($content);
$enBlock->save();
//==========================================================================
//==========================================================================



//==========================================================================
// all-brands-slider-homeliving (Moxy Thai)
//==========================================================================
$blockTitle = "Moxy Home Living brands slider";
$blockIdentifier = "all-brands-slider-homeliving";
$blockStores = array($moxyth);
$blockIsActive = 0;

// delete old block if have
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete

$content = <<<EOD

EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$enBlock = Mage::getModel('cms/block');
$enBlock->setTitle($blockTitle);
$enBlock->setIdentifier($blockIdentifier);
$enBlock->setStores($blockStores);
$enBlock->setIsActive($blockIsActive);
$enBlock->setContent($content);
$enBlock->save();
//==========================================================================
//==========================================================================



//==========================================================================
// all-brands-slider-electronics (Moxy English)
//==========================================================================
$blockTitle = "Moxy Electronics brands slider";
$blockIdentifier = "all-brands-slider-electronics";
$blockStores = array($moxyen);
$blockIsActive = 0;

// delete old block if have
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete

$content = <<<EOD

EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$enBlock = Mage::getModel('cms/block');
$enBlock->setTitle($blockTitle);
$enBlock->setIdentifier($blockIdentifier);
$enBlock->setStores($blockStores);
$enBlock->setIsActive($blockIsActive);
$enBlock->setContent($content);
$enBlock->save();
//==========================================================================
//==========================================================================



//==========================================================================
// all-brands-slider-electronics (Moxy Thai)
//==========================================================================
$blockTitle = "Moxy Electronics brands slider";
$blockIdentifier = "all-brands-slider-electronics";
$blockStores = array($moxyth);
$blockIsActive = 0;

// delete old block if have
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete

$content = <<<EOD

EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$enBlock = Mage::getModel('cms/block');
$enBlock->setTitle($blockTitle);
$enBlock->setIdentifier($blockIdentifier);
$enBlock->setStores($blockStores);
$enBlock->setIsActive($blockIsActive);
$enBlock->setContent($content);
$enBlock->save();
//==========================================================================
//==========================================================================



// Add new "seo-keyword" for each Moxy child stores



//==========================================================================
// Fashion SEO Keyword (Moxy English)
//==========================================================================
$blockTitle = "Fashion SEO Keyword";
$blockIdentifier = "seo-keyword-fashion";
$blockStores = array($moxyen);
$blockIsActive = 1;
$blockContent = <<<EOD
<div style="margin: 50px auto 30px;">
<p>&nbsp;</p>
<h2>กล้า มีฝัน มุ่งมั่น</h2>
<p class="end_content_info">แล้วคุณมองเห็น ความเป็น Moxy หรือยังถ้าคุณคิดแบบ Moxy ก็แปลว่า คุณมีความคิดและความมุ่งมั่นว่าคุณจะต้องเป็นคนสำคัญ</p>
<p class="end_content_info">Moxy เป็นแบรนด์ที่ชัดเจนแต่เรียบง่าย และ บ่งบอกถึงแรงพลังที่จะสร้างสไตล์ให้กับตัวเอง นโยบายหมายเลขหนึ่งของ Moxy คือ การอยู่ร่วมโลกทุกวันนี้ เป็นโลกของการอยู่ร่วม</p>
<p class="end_content_info">ไม่ว่าจะกับเพื่อน ครอบครัว คนรู้จัก เมืองที่คุณอยู่ และจนถึงวัฒนธรรมร่วม ทิศทางที่จะเป็นประโยชน์สูงสุดแก่การอยู่ร่วม ไม่ใช่จะเป็นผลดีต่อแบรนด์เท่านั้น</p>
<p class="end_content_info">แต่มันจะกระทบทุกคนไปในทางบวก Moxy ไม่ได้มุ่งแต่กำไร แต่ยังตั้งใจที่จะให้ บริการที่ดีจริง และ ต้องการจะร่วมงานกับนักออกแบบและผู้ผลิตที่เป็นคนไทย บวกกับสินค้าที่ดีต่อสังคม เพราะ Moxy รู้ถึงประโยชน์ของการตอบแทน</p>
</div>
EOD;
/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// Fashion SEO Keyword (Moxy Thai)
//==========================================================================
$blockTitle = "Fashion SEO Keyword";
$blockIdentifier = "seo-keyword-fashion";
$blockStores = array($moxyth);
$blockIsActive = 1;
$blockContent = <<<EOD
<div style="margin: 50px auto 30px;">
<p>&nbsp;</p>
<h2>กล้า มีฝัน มุ่งมั่น</h2>
<p class="end_content_info">แล้วคุณมองเห็น ความเป็น Moxy หรือยังถ้าคุณคิดแบบ Moxy ก็แปลว่า คุณมีความคิดและความมุ่งมั่นว่าคุณจะต้องเป็นคนสำคัญ</p>
<p class="end_content_info">Moxy เป็นแบรนด์ที่ชัดเจนแต่เรียบง่าย และ บ่งบอกถึงแรงพลังที่จะสร้างสไตล์ให้กับตัวเอง นโยบายหมายเลขหนึ่งของ Moxy คือ การอยู่ร่วมโลกทุกวันนี้ เป็นโลกของการอยู่ร่วม</p>
<p class="end_content_info">ไม่ว่าจะกับเพื่อน ครอบครัว คนรู้จัก เมืองที่คุณอยู่ และจนถึงวัฒนธรรมร่วม ทิศทางที่จะเป็นประโยชน์สูงสุดแก่การอยู่ร่วม ไม่ใช่จะเป็นผลดีต่อแบรนด์เท่านั้น</p>
<p class="end_content_info">แต่มันจะกระทบทุกคนไปในทางบวก Moxy ไม่ได้มุ่งแต่กำไร แต่ยังตั้งใจที่จะให้ บริการที่ดีจริง และ ต้องการจะร่วมงานกับนักออกแบบและผู้ผลิตที่เป็นคนไทย บวกกับสินค้าที่ดีต่อสังคม เพราะ Moxy รู้ถึงประโยชน์ของการตอบแทน</p>
</div>
EOD;
/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// Home Living SEO Keyword (Moxy English)
//==========================================================================
$blockTitle = "Home Living SEO Keyword";
$blockIdentifier = "seo-keyword-homeliving";
$blockStores = array($moxyen);
$blockIsActive = 1;
$blockContent = <<<EOD
<div style="margin: 50px auto 30px;">
<p>&nbsp;</p>
<h2>กล้า มีฝัน มุ่งมั่น</h2>
<p class="end_content_info">แล้วคุณมองเห็น ความเป็น Moxy หรือยังถ้าคุณคิดแบบ Moxy ก็แปลว่า คุณมีความคิดและความมุ่งมั่นว่าคุณจะต้องเป็นคนสำคัญ</p>
<p class="end_content_info">Moxy เป็นแบรนด์ที่ชัดเจนแต่เรียบง่าย และ บ่งบอกถึงแรงพลังที่จะสร้างสไตล์ให้กับตัวเอง นโยบายหมายเลขหนึ่งของ Moxy คือ การอยู่ร่วมโลกทุกวันนี้ เป็นโลกของการอยู่ร่วม</p>
<p class="end_content_info">ไม่ว่าจะกับเพื่อน ครอบครัว คนรู้จัก เมืองที่คุณอยู่ และจนถึงวัฒนธรรมร่วม ทิศทางที่จะเป็นประโยชน์สูงสุดแก่การอยู่ร่วม ไม่ใช่จะเป็นผลดีต่อแบรนด์เท่านั้น</p>
<p class="end_content_info">แต่มันจะกระทบทุกคนไปในทางบวก Moxy ไม่ได้มุ่งแต่กำไร แต่ยังตั้งใจที่จะให้ บริการที่ดีจริง และ ต้องการจะร่วมงานกับนักออกแบบและผู้ผลิตที่เป็นคนไทย บวกกับสินค้าที่ดีต่อสังคม เพราะ Moxy รู้ถึงประโยชน์ของการตอบแทน</p>
</div>
EOD;
/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// Home Living SEO Keyword (Moxy Thai)
//==========================================================================
$blockTitle = "Home Living SEO Keyword";
$blockIdentifier = "seo-keyword-homeliving";
$blockStores = array($moxyth);
$blockIsActive = 1;
$blockContent = <<<EOD
<div style="margin: 50px auto 30px;">
<p>&nbsp;</p>
<h2>กล้า มีฝัน มุ่งมั่น</h2>
<p class="end_content_info">แล้วคุณมองเห็น ความเป็น Moxy หรือยังถ้าคุณคิดแบบ Moxy ก็แปลว่า คุณมีความคิดและความมุ่งมั่นว่าคุณจะต้องเป็นคนสำคัญ</p>
<p class="end_content_info">Moxy เป็นแบรนด์ที่ชัดเจนแต่เรียบง่าย และ บ่งบอกถึงแรงพลังที่จะสร้างสไตล์ให้กับตัวเอง นโยบายหมายเลขหนึ่งของ Moxy คือ การอยู่ร่วมโลกทุกวันนี้ เป็นโลกของการอยู่ร่วม</p>
<p class="end_content_info">ไม่ว่าจะกับเพื่อน ครอบครัว คนรู้จัก เมืองที่คุณอยู่ และจนถึงวัฒนธรรมร่วม ทิศทางที่จะเป็นประโยชน์สูงสุดแก่การอยู่ร่วม ไม่ใช่จะเป็นผลดีต่อแบรนด์เท่านั้น</p>
<p class="end_content_info">แต่มันจะกระทบทุกคนไปในทางบวก Moxy ไม่ได้มุ่งแต่กำไร แต่ยังตั้งใจที่จะให้ บริการที่ดีจริง และ ต้องการจะร่วมงานกับนักออกแบบและผู้ผลิตที่เป็นคนไทย บวกกับสินค้าที่ดีต่อสังคม เพราะ Moxy รู้ถึงประโยชน์ของการตอบแทน</p>
</div>
EOD;
/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// Electronics SEO Keyword (Moxy English)
//==========================================================================
$blockTitle = "Electronics SEO Keyword";
$blockIdentifier = "seo-keyword-electronics";
$blockStores = array($moxyen);
$blockIsActive = 1;
$blockContent = <<<EOD
<div style="margin: 50px auto 30px;">
<p>&nbsp;</p>
<h2>กล้า มีฝัน มุ่งมั่น</h2>
<p class="end_content_info">แล้วคุณมองเห็น ความเป็น Moxy หรือยังถ้าคุณคิดแบบ Moxy ก็แปลว่า คุณมีความคิดและความมุ่งมั่นว่าคุณจะต้องเป็นคนสำคัญ</p>
<p class="end_content_info">Moxy เป็นแบรนด์ที่ชัดเจนแต่เรียบง่าย และ บ่งบอกถึงแรงพลังที่จะสร้างสไตล์ให้กับตัวเอง นโยบายหมายเลขหนึ่งของ Moxy คือ การอยู่ร่วมโลกทุกวันนี้ เป็นโลกของการอยู่ร่วม</p>
<p class="end_content_info">ไม่ว่าจะกับเพื่อน ครอบครัว คนรู้จัก เมืองที่คุณอยู่ และจนถึงวัฒนธรรมร่วม ทิศทางที่จะเป็นประโยชน์สูงสุดแก่การอยู่ร่วม ไม่ใช่จะเป็นผลดีต่อแบรนด์เท่านั้น</p>
<p class="end_content_info">แต่มันจะกระทบทุกคนไปในทางบวก Moxy ไม่ได้มุ่งแต่กำไร แต่ยังตั้งใจที่จะให้ บริการที่ดีจริง และ ต้องการจะร่วมงานกับนักออกแบบและผู้ผลิตที่เป็นคนไทย บวกกับสินค้าที่ดีต่อสังคม เพราะ Moxy รู้ถึงประโยชน์ของการตอบแทน</p>
</div>
EOD;
/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// Electronics SEO Keyword (Moxy Thai)
//==========================================================================
$blockTitle = "Electronics SEO Keyword";
$blockIdentifier = "seo-keyword-electronics";
$blockStores = array($moxyth);
$blockIsActive = 1;
$blockContent = <<<EOD
<div style="margin: 50px auto 30px;">
<p>&nbsp;</p>
<h2>กล้า มีฝัน มุ่งมั่น</h2>
<p class="end_content_info">แล้วคุณมองเห็น ความเป็น Moxy หรือยังถ้าคุณคิดแบบ Moxy ก็แปลว่า คุณมีความคิดและความมุ่งมั่นว่าคุณจะต้องเป็นคนสำคัญ</p>
<p class="end_content_info">Moxy เป็นแบรนด์ที่ชัดเจนแต่เรียบง่าย และ บ่งบอกถึงแรงพลังที่จะสร้างสไตล์ให้กับตัวเอง นโยบายหมายเลขหนึ่งของ Moxy คือ การอยู่ร่วมโลกทุกวันนี้ เป็นโลกของการอยู่ร่วม</p>
<p class="end_content_info">ไม่ว่าจะกับเพื่อน ครอบครัว คนรู้จัก เมืองที่คุณอยู่ และจนถึงวัฒนธรรมร่วม ทิศทางที่จะเป็นประโยชน์สูงสุดแก่การอยู่ร่วม ไม่ใช่จะเป็นผลดีต่อแบรนด์เท่านั้น</p>
<p class="end_content_info">แต่มันจะกระทบทุกคนไปในทางบวก Moxy ไม่ได้มุ่งแต่กำไร แต่ยังตั้งใจที่จะให้ บริการที่ดีจริง และ ต้องการจะร่วมงานกับนักออกแบบและผู้ผลิตที่เป็นคนไทย บวกกับสินค้าที่ดีต่อสังคม เพราะ Moxy รู้ถึงประโยชน์ของการตอบแทน</p>
</div>
EOD;
/** @var Mage_Cms_Model_Block $block */
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================