<?php

$en     = Mage::getModel('core/store')->load('en', 'code')->getId();// Petloft
$th     = Mage::getModel('core/store')->load('th', 'code')->getId();
$len    = Mage::getModel('core/store')->load('len', 'code')->getId();// Lafema
$lth    = Mage::getModel('core/store')->load('lth', 'code')->getId();
$ven    = Mage::getModel('core/store')->load('ven', 'code')->getId();// Venbi
$vth    = Mage::getModel('core/store')->load('vth', 'code')->getId();
$sen    = Mage::getModel('core/store')->load('sen', 'code')->getId();// Sanoga
$sth    = Mage::getModel('core/store')->load('sth', 'code')->getId();
$moxyen = Mage::getModel('core/store')->load('moxyen', 'code')->getId();// Moxy
$moxyth = Mage::getModel('core/store')->load('moxyth', 'code')->getId();

//==========================================================================
// Slider Moxy -> Petloft TH
//==========================================================================
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($moxyth, $withAdmin = false)
    ->addFieldToFilter('identifier', 'slider_petloft')
    ->getFirstItem();

if ($block->getId()) $block->delete();// if exists then delete

$content = <<<EOD
<div class="homepage-main" style="width: 700px;">
<div id="container" style="width: 700px;">
<div id="slideshow" style="height: 300px; width: 700px;">
<ul id="slide-nav" style="display: none; visibility: hidden;">
<li id="prev"><a class="control-s pre-s" href="#">Previous</a></li>
<li id="next" style="left: 500px;"><a class="control-s nxt-s" href="#">Next</a></li>
</ul>
<ul id="home-slides">
<li><a style="width: 700px;" href="https://www.moxy.co.th/en/deal/all-deals.html?cat=3292"><img src="{{media url="wysiwyg/Pets-Homepage-Banner-BOGO.jpg"}}" alt="" /></a>
<div class="caption">buy 1 get 1</div>
</li>
<li><a style="width: 700px;" href="http://www.moxy.co.th/en/petloft/brands/cesar.html"><img src="{{media url="wysiwyg/Cesar_12packfree_pets_homepage.jpg"}}" alt="" /></a>
<div class="caption">Ceasar</div>
</li>
<li><a style="width: 700px;" href="http://www.moxy.co.th/en/petloft/brands/biosand.html"><img src="{{media url="wysiwyg/Pets-Homepage-Banner-Promotion-Biosand.jpg"}}" alt="" /></a>
<div class="caption">biosand 2 free 1</div>
</li>
<li><a style="width: 700px;" href="http://www.moxy.co.th/en/petloft/brands/garden-state.html"><img src="{{media url="wysiwyg/pets-homepage-banner-garden-state-2_1_.jpg"}}" alt="" /></a>
<div class="caption">Garden State</div>
</li>
<li><a style="width: 700px;" href="http://www.moxy.co.th/th/petloft/brands/affetto.html"><img src="{{media url="wysiwyg/pet-Affetto-homepage-201015-02.jpg"}}" alt="" /></a>
<div class="caption">affectto</div>
</li>
<li><a style="width: 700px;" href="http://www.moxy.co.th/en/greeny-grass-potty-patch-indoor-washroom-for-dogs.html"><img src="{{media url="wysiwyg/Pets-homepage-banner-GreenyGrass-261015.jpg"}}" alt="" /></a>
<div class="caption">greeny grass</div>
</li>
<li><a style="width: 700px;" href="http://www.moxy.co.th/en/petmate-programmable-portion-right-food-dispenser-10lbs.html"><img src="{{media url="wysiwyg/pets-homepage-banner-petmate.jpg"}}" alt="" /></a>
<div class="caption">petmate</div>
</li>
<li><a style="width: 700px;" href="http://www.moxy.co.th/en/petloft/brands/odour-lock.html"><img src="{{media url="wysiwyg/pets-homepage-banner-Odour-Lock.jpg"}}" alt="" /></a>
<div class="caption">Odour lock</div>
</li>
<li><a style="width: 700px;" href="http://www.moxy.co.th/en/catalogsearch/result/?q=Whiskas+Mackerel+%2885g%29"><img src="{{media url="wysiwyg/petloft-homepage-banner-whiskas-20150915.jpg"}}" alt="" /></a>
<div class="caption">whiskas</div>
</li>
</ul>
</div>
</div>
</div>
<div class="homepage-bannerl1 first"><a href="http://www.petloft.com/th/catalogsearch/result/?q=ciao+buy+4+get+1"><img src="{{media url="wysiwyg/pet-CIAO-side-banner-291015_1_.jpg"}}" alt="" /></a></div>
<div class="homepage-bannerl1"><a href="http://www.petloft.com/"><img src="{{media url="wysiwyg/InstallmentPlan_sidebanner_all.jpg"}}" alt="" /></a></div>
<div class="homepage-bannerl1 last"><a href="http://www.petloft.com/"><img src="{{media url="wysiwyg/Free-Shipping-Side-Banner_1.jpg"}}" alt="" /></a></div>
<p>{{block type="core/template" name="personal_widget" as="category_widget" template="emarsys/category.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="1568" template="catalog/product/recommend.phtml"}}</p>
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$block = Mage::getModel('cms/block');
$block->setTitle('Slider Petloft TH');
$block->setIdentifier('slider_petloft');
$block->setStores($moxyth);
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================

//==========================================================================
// Slider Moxy -> Petloft EN
//==========================================================================
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($moxyen, $withAdmin = false)
    ->addFieldToFilter('identifier', 'slider_petloft')
    ->getFirstItem();

if ($block->getId()) $block->delete();// if exists then delete

$content = <<<EOD
<div class="homepage-main" style="width: 700px;">
<div id="container" style="width: 700px;">
<div id="slideshow" style="height: 300px; width: 700px;">
<ul id="slide-nav" style="display: none; visibility: hidden;">
<li id="prev"><a class="control-s pre-s" href="#">Previous</a></li>
<li id="next" style="left: 500px;"><a class="control-s nxt-s" href="#">Next</a></li>
</ul>
<ul id="home-slides">
<li><a style="width: 700px;" href="https://www.moxy.co.th/en/deal/all-deals.html?cat=3292"><img src="{{media url="wysiwyg/Pets-Homepage-Banner-BOGO.jpg"}}" alt="" /></a>
<div class="caption">buy 1 get 1</div>
</li>
<li><a style="width: 700px;" href="http://www.moxy.co.th/en/petloft/brands/cesar.html"><img src="{{media url="wysiwyg/Cesar_12packfree_pets_homepage.jpg"}}" alt="" /></a>
<div class="caption">Ceasar</div>
</li>
<li><a style="width: 700px;" href="http://www.moxy.co.th/en/petloft/brands/biosand.html"><img src="{{media url="wysiwyg/Pets-Homepage-Banner-Promotion-Biosand.jpg"}}" alt="" /></a>
<div class="caption">biosand 2 free 1</div>
</li>
<li><a style="width: 700px;" href="http://www.moxy.co.th/en/petloft/brands/garden-state.html"><img src="{{media url="wysiwyg/pets-homepage-banner-garden-state-2_1_.jpg"}}" alt="" /></a>
<div class="caption">Garden State</div>
</li>
<li><a style="width: 700px;" href="http://www.moxy.co.th/th/petloft/brands/affetto.html"><img src="{{media url="wysiwyg/pet-Affetto-homepage-201015-02.jpg"}}" alt="" /></a>
<div class="caption">affectto</div>
</li>
<li><a style="width: 700px;" href="http://www.moxy.co.th/en/greeny-grass-potty-patch-indoor-washroom-for-dogs.html"><img src="{{media url="wysiwyg/Pets-homepage-banner-GreenyGrass-261015.jpg"}}" alt="" /></a>
<div class="caption">greeny grass</div>
</li>
<li><a style="width: 700px;" href="http://www.moxy.co.th/en/petmate-programmable-portion-right-food-dispenser-10lbs.html"><img src="{{media url="wysiwyg/pets-homepage-banner-petmate.jpg"}}" alt="" /></a>
<div class="caption">petmate</div>
</li>
<li><a style="width: 700px;" href="http://www.moxy.co.th/en/petloft/brands/odour-lock.html"><img src="{{media url="wysiwyg/pets-homepage-banner-Odour-Lock.jpg"}}" alt="" /></a>
<div class="caption">Odour lock</div>
</li>
<li><a style="width: 700px;" href="http://www.moxy.co.th/en/catalogsearch/result/?q=Whiskas+Mackerel+%2885g%29"><img src="{{media url="wysiwyg/petloft-homepage-banner-whiskas-20150915.jpg"}}" alt="" /></a>
<div class="caption">whiskas</div>
</li>
</ul>
</div>
</div>
</div>
<div class="homepage-bannerl1 first"><a href="http://www.petloft.com/en/catalogsearch/result/?q=ciao+buy+4+get+1"><img src="{{media url="wysiwyg/pet-CIAO-side-banner-291015_1_.jpg"}}" alt="" /></a></div>
<div class="homepage-bannerl1"><a href="http://www.petloft.com"><img src="{{media url="wysiwyg/InstallmentPlan_sidebanner_all.jpg"}}" alt="" /></a></div>
<div class="homepage-bannerl1 last"><a href="http://www.petloft.com/"><img src="{{media url="wysiwyg/Free-Shipping-Side-Banner_1.jpg"}}" alt="" /></a></div>
<p>{{block type="core/template" name="personal_widget" as="category_widget" template="emarsys/category.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="1568" template="catalog/product/recommend.phtml"}}</p>
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$block = Mage::getModel('cms/block');
$block->setTitle('Slider Petloft EN');
$block->setIdentifier('slider_petloft');
$block->setStores($moxyen);
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================

//==========================================================================
// Slider Moxy -> Sanoga TH
//==========================================================================
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($moxyth, $withAdmin = false)
    ->addFieldToFilter('identifier', 'slider_sanoga')
    ->getFirstItem();

if ($block->getId()) $block->delete();// if exists then delete

$content = <<<EOD
<div class="homepage-main" style="float: left; width: 700px;">
<div id="container" style="width: 700px;">
<div id="slideshow" style="height: 300px; width: 700px;">
<ul id="slide-nav" style="display: none; visibility: hidden;">
<li id="prev"><a class="control-s pre-s" href="#">Previous</a></li>
<li id="next"><a class="control-s nxt-s" href="#">Next</a></li>
</ul>
<ul id="home-slides">
<li><a style="width: 700px;" href="http://www.moxy.co.th/th/deal/all-deals.html?cat=3292"><img src="{{media url="wysiwyg/Health-homepage-banner-BOGO-161115.jpg"}}" alt="" /></a>
<div class="caption">buy 1 get 1</div>
</li>
<li><a style="width: 700px;" href="http://www.moxy.co.th/th/catalogsearch/result/?q=%E0%B8%A5%E0%B8%B9%E0%B9%88%E0%B8%A7%E0%B8%B4%E0%B9%88%E0%B8%87"><img src="{{media url="wysiwyg/homepagebanners-health-10months.jpg"}}" alt="" /></a>
<div class="caption">treadmill installment 10 months</div>
</li>
</ul>
</div>
</div>
</div>
<div class="homepage-bannerl1 first"><a href="http://www.sanoga.com/"><img src="{{media url="wysiwyg/Free-Shipping-Side-Banner_1.jpg"}}" alt="" /></a></div>
<div class="homepage-bannerl1"><a href="http://www.sanoga.com/"><img src="{{media url="wysiwyg/InstallmentPlan_sidebanner_all.jpg"}}" alt="" /></a></div>
<div class="homepage-bannerl1 last"><a href="http://www.sanoga.com/th/brands/dhc/dhc-collagen-tablets-2-050-mg-20-day.html"><img src="{{media url="wysiwyg/health-sidebanner-dhccollagen.jpg"}}" alt="" /></a></div>
<p>{{block type="core/template" name="personal_widget" as="category_widget" template="emarsys/category.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="1568" template="catalog/product/recommend.phtml"}}</p>
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$block = Mage::getModel('cms/block');
$block->setTitle('Slider Sanoga TH');
$block->setIdentifier('slider_sanoga');
$block->setStores($moxyth);
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================

//==========================================================================
// Slider Moxy -> Sanoga EN
//==========================================================================
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($moxyen, $withAdmin = false)
    ->addFieldToFilter('identifier', 'slider_sanoga')
    ->getFirstItem();

if ($block->getId()) $block->delete();// if exists then delete

$content = <<<EOD
<div class="homepage-main" style="float: left; width: 700px;">
<div id="container" style="width: 700px;">
<div id="slideshow" style="height: 300px; width: 700px;">
<ul id="slide-nav" style="display: none; visibility: hidden;">
<li id="prev"><a class="control-s pre-s" href="#">Previous</a></li>
<li id="next"><a class="control-s nxt-s" href="#">Next</a></li>
</ul>
<ul id="home-slides">
<li><a style="width: 700px;" href="http://www.moxy.co.th/th/deal/all-deals.html?cat=3292"><img src="{{media url="wysiwyg/Health-homepage-banner-BOGO-161115.jpg"}}" alt="" /></a>
<div class="caption">buy 1 get 1</div>
</li>
<li><a style="width: 700px;" href="http://www.moxy.co.th/th/catalogsearch/result/?q=%E0%B8%A5%E0%B8%B9%E0%B9%88%E0%B8%A7%E0%B8%B4%E0%B9%88%E0%B8%87"><img src="{{media url="wysiwyg/homepagebanners-health-10months.jpg"}}" alt="" /></a>
<div class="caption">treadmill installment 10 months</div>
</li>
</ul>
</div>
</div>
</div>
<div class="homepage-bannerl1 first"><a href="http://www.sanoga.com/"><img src="{{media url="wysiwyg/Free-Shipping-Side-Banner_1.jpg"}}" alt="" /></a></div>
<div class="homepage-bannerl1"><a href="http://www.sanoga.com/"><img src="{{media url="wysiwyg/InstallmentPlan_sidebanner_all.jpg"}}" alt="" /></a></div>
<div class="homepage-bannerl1 last"><a href="http://www.sanoga.com/en/brands/dhc/dhc-collagen-tablets-2-050-mg-20-day.html"><img src="{{media url="wysiwyg/health-sidebanner-dhccollagen.jpg"}}" alt="" /></a></div>
<p>{{block type="core/template" name="personal_widget" as="category_widget" template="emarsys/category.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="1568" template="catalog/product/recommend.phtml"}}</p>
EOD;
/** @var Mage_Cms_Model_Block */
$block = Mage::getModel('cms/block');
$block->setTitle('Slider Sanoga EN');
$block->setIdentifier('slider_sanoga');
$block->setStores($moxyen);
$block->setIsActive(1);
$block->setContent($content);
$block->save();

//==========================================================================
//==========================================================================
//==========================================================================


//==========================================================================
// Slider Moxy -> Venbi TH
//==========================================================================
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($moxyth, $withAdmin = false)
    ->addFieldToFilter('identifier', 'slider_venbi')
    ->getFirstItem();

if ($block->getId()) $block->delete();// if exists then delete

$content = <<<EOD
<div class="homepage-main" style="float: left; width: 700px;">
<div id="container" style="width: 700px;">
<div id="slideshow" style="height: 300px; width: 700px; overflow: hidden;">
<ul id="slide-nav" style="top: 190px; display: none; visibility: hidden;">
<li id="prev"><a class="control-s pre-s" href="#">Previous</a></li>
<li id="next" style="left: 500px;"><a class="control-s nxt-s" href="#">Next</a></li>
</ul>
<ul id="home-slides" style="position: relative; width: 700px; height: 300px;">
<li><a style="width: 700px;" href="https://www.moxy.co.th/th/deal/all-deals.html?cat=3292"><img src="{{media url="wysiwyg/MomBabiesedm_BOGO_nov.jpg"}}" alt="" /></a>
<div class="caption">Buy 1 get 1</div>
</li>
<li><a style="width: 700px;" href="http://www.moxy.co.th/th/venbi/diapers/popular-brands/baby-love.html?limit=all"><img src="{{media url="wysiwyg/Mom_Baby-homepage-banner-BabyLove-Promotion-041115.jpg"}}" alt="" /></a>
<div class="caption">babylove</div>
</li>
<li><a style="width: 700px;" href="http://www.moxy.co.th/th/catalogsearch/result/?q=huggies+%E0%B8%8B%E0%B8%B7%E0%B9%89%E0%B8%AD+6+%E0%B9%81%E0%B8%96%E0%B8%A1+6"><img src="{{media url="wysiwyg/Homepage-Banner-Huggies-Re-Aw-05-Aug-2015.jpg"}}" alt="" /></a>
<div class="caption">Huggies 6 free 6</div>
</li>
<li><a style="width: 700px;" href="http://www.moxy.co.th/th/catalogsearch/result/?q=dumex+dumilk"><img src="{{media url="wysiwyg/baby-homepage-banner-dumex-dumilk-3.jpg"}}" alt="" /></a>
<div class="caption">dumex</div>
</li>
<li><a style="width: 700px;" href="http://www.moxy.co.th/th/venbi/toys.html?baby_brands=1697"><img src="{{media url="wysiwyg/Mom_baby-homepage-banner-minion-161015.jpg"}}" alt="" /></a>
<div class="caption">minion one price</div>
</li>
</ul>
</div>
</div>
</div>
<div class="homepage-bannerl1 first" ><a href="http://www.venbi.com/"><img src="{{media url="wysiwyg/Free-Shipping-Side-Banner_1.jpg"}}" alt="" /></a></div>
<div class="homepage-bannerl1"><a href="http://www.venbi.com/"><img src="{{media url="wysiwyg/InstallmentPlan_sidebanner_all.jpg"}}" alt="" /></a></div>
<div class="homepage-bannerl1 last" ><a href="http://www.venbi.com/th/diapers.html"><img src="{{media url="wysiwyg/baby-side-banner-BABY-DIAPERS.jpg"}}" alt="" /></a></div>
<p>{{block type="core/template" name="personal_widget" as="category_widget" template="emarsys/category.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="1568" template="catalog/product/recommend.phtml"}}</p>
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$block = Mage::getModel('cms/block');
$block->setTitle('Slider Venbi TH');
$block->setIdentifier('slider_venbi');
$block->setStores($moxyth);
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================

//==========================================================================
// Slider Moxy -> Venbi EN
//==========================================================================
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($moxyen, $withAdmin = false)
    ->addFieldToFilter('identifier', 'slider_venbi')
    ->getFirstItem();

if ($block->getId()) $block->delete();// if exists then delete

$content = <<<EOD
<div class="homepage-main" style="float: left; width: 700px;">
<div id="container" style="width: 700px;">
<div id="slideshow" style="height: 300px; width: 700px; overflow: hidden;">
<ul id="slide-nav" style="top: 190px; display: none; visibility: hidden;">
<li id="prev"><a class="control-s pre-s" href="#">Previous</a></li>
<li id="next" style="left: 500px;"><a class="control-s nxt-s" href="#">Next</a></li>
</ul>
<ul id="home-slides" style="position: relative; width: 700px; height: 300px;">
<li><a style="width: 700px;" href="https://www.moxy.co.th/en/deal/all-deals.html?cat=3292"><img src="{{media url="wysiwyg/MomBabiesedm_BOGO_nov.jpg"}}" alt="" /></a>
<div class="caption">Buy 1 get 1</div>
</li>
<li><a style="width: 700px;" href="http://www.moxy.co.th/en/venbi/diapers/popular-brands/baby-love.html?limit=all"><img src="{{media url="wysiwyg/Mom_Baby-homepage-banner-BabyLove-Promotion-041115.jpg"}}" alt="" /></a>
<div class="caption">babylove</div>
</li>
<li><a style="width: 700px;" href="http://www.moxy.co.th/en/catalogsearch/result/?q=huggies+%E0%B8%8B%E0%B8%B7%E0%B9%89%E0%B8%AD+6+%E0%B9%81%E0%B8%96%E0%B8%A1+6"><img src="{{media url="wysiwyg/Homepage-Banner-Huggies-Re-Aw-05-Aug-2015.jpg"}}" alt="" /></a>
<div class="caption">Huggies 6 free 6</div>
</li>
<li><a style="width: 700px;" href="http://www.moxy.co.th/en/catalogsearch/result/?q=dumex+dumilk"><img src="{{media url="wysiwyg/baby-homepage-banner-dumex-dumilk-3.jpg"}}" alt="" /></a>
<div class="caption">dumex</div>
</li>
<li><a style="width: 700px;" href="http://www.moxy.co.th/en/venbi/toys.html?baby_brands=1697"><img src="{{media url="wysiwyg/Mom_baby-homepage-banner-minion-161015.jpg"}}" alt="" /></a>
<div class="caption">minion one price</div>
</li>
</ul>
</div>
</div>
</div>
<div class="homepage-bannerl1 first"><a href="http://www.venbi.com/"><img src="{{media url="wysiwyg/Free-Shipping-Side-Banner_1.jpg"}}" alt="" /></a></div>
<div class="homepage-bannerl1"><a href="http://www.venbi.com/"><img src="{{media url="wysiwyg/InstallmentPlan_sidebanner_all.jpg"}}" alt="" /></a></div>
<div class="homepage-bannerl1 last"><a href="http://www.venbi.com/th/diapers.html"><img src="{{media url="wysiwyg/baby-side-banner-BABY-DIAPERS.jpg"}}" alt="" /></a></div>
<p>{{block type="core/template" name="personal_widget" as="category_widget" template="emarsys/category.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="1568" template="catalog/product/recommend.phtml"}}</p>
EOD;
/** @var Mage_Cms_Model_Block */
$block = Mage::getModel('cms/block');
$block->setTitle('Slider Venbi EN');
$block->setIdentifier('slider_venbi');
$block->setStores($moxyen);
$block->setIsActive(1);
$block->setContent($content);
$block->save();

//==========================================================================
//==========================================================================
//==========================================================================

//==========================================================================
// Slider Moxy -> Lafema TH
//==========================================================================
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($moxyth, $withAdmin = false)
    ->addFieldToFilter('identifier', 'slider_lafema')
    ->getFirstItem();

if ($block->getId()) $block->delete();// if exists then delete

$content = <<<EOD
<div class="homepage-main" style="float: left; width: 700px;">
<div id="container" style="width: 700px;">
<div id="slideshow" style="height: 300px; width: 700px; overflow: hidden;">
<ul id="slide-nav" style="display: none; visibility: hidden;">
<li id="prev"><a class="control-s pre-s" href="#">Previous</a></li>
<li id="next"><a class="control-s nxt-s" href="#">Next</a></li>
</ul>
<ul id="home-slides" style="position: relative; width: 700px; height: 300px;">
<li style="position: absolute; top: 0px; left: 0px; display: none; z-index: 1; opacity: 1;"><a style="width: 700px;" href="https://www.moxy.co.th/th/deal/all-deals.html?cat=3292"><img src="{{media url="wysiwyg/beauty-homepage-banner-buy1-get1-sale-20151116.jpg"}}" alt="" /></a>
<div class="caption">buy 1 get 1</div>
</li>
<li style="position: absolute; top: 0px; left: 0px; display: none; z-index: 1; opacity: 1;"><a style="width: 700px;" href="http://www.moxy.co.th/th/lafema/brands/elizabeth-arden.html"><img src="{{media url="wysiwyg/Beauty-HomepageBanner-Elizabeth-Arden-10112015.jpg"}}" alt="" /></a>
<div class="caption">elizabeth 15</div>
</li>
<li style="position: absolute; top: 0px; left: 0px; display: none; z-index: 1; opacity: 1;"><a style="width: 700px;" href="http://www.moxy.co.th/th/lafema/brands/wet-n-wild.html"><img src="{{media url="wysiwyg/beauty-homepage-Wet-n-Wild-221015.jpg"}}" alt="" /></a>
<div class="caption">Wet n' Wild freebies</div>
</li>
<li style="position: absolute; top: 0px; left: 0px; display: none; z-index: 1; opacity: 1;"><a style="width: 700px;" href="http://www.moxy.co.th/th/lafema/deals/nyx-round-lips-promotion.html"><img src="{{media url="wysiwyg/beauty-NYX-homepage-banner-051015.jpg"}}" alt="" /></a>
<div class="caption">NYX Round Lipstick</div>
</li>
<li style="position: absolute; top: 0px; left: 0px; display: none; z-index: 1; opacity: 1;"><a style="width: 700px;" href="http://www.moxy.co.th/th/lafema/brands/w7.html"><img src="{{media url="wysiwyg/beauty-homepage-w7-081015.jpg"}}" alt="" /></a>
<div class="caption">W7</div>
</li>
</ul>
</div>
</div>
</div>
<div class="homepage-bannerl1 first"><a href="http://www.lafema.com/th/hair/hair-tools.html"><img src="{{media url="wysiwyg/Beauty-side-banner-hair-tools.jpg"}}" alt="" /></a></div>
<div class="homepage-bannerl1"><a href="http://www.lafema.com/"><img src="{{media url="wysiwyg/InstallmentPlan_sidebanner_all.jpg"}}" alt="" /></a></div>
<div class="homepage-bannerl1 last"><a href="http://www.lafema.com/th/help/"><img src="{{media url="wysiwyg/Free-Shipping-Side-Banner_1.jpg"}}" alt="" /></a></div>
<p>{{block type="core/template" name="personal_widget" as="category_widget" template="emarsys/category.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="1568" template="catalog/product/recommend.phtml"}}</p>
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$block = Mage::getModel('cms/block');
$block->setTitle('Slider Lafema TH');
$block->setIdentifier('slider_lafema');
$block->setStores($moxyth);
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================

//==========================================================================
// Slider Moxy -> Lafema EN
//==========================================================================
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($moxyen, $withAdmin = false)
    ->addFieldToFilter('identifier', 'slider_lafema')
    ->getFirstItem();

if ($block->getId()) $block->delete();// if exists then delete

$content = <<<EOD
<div class="homepage-main" style="float: left; width: 700px;">
<div id="container" style="width: 700px;">
<div id="slideshow" style="height: 300px; width: 700px; overflow: hidden;">
<ul id="slide-nav" style="display: none; visibility: hidden;">
<li id="prev"><a class="control-s pre-s" href="#">Previous</a></li>
<li id="next"><a class="control-s nxt-s" href="#">Next</a></li>
</ul>
<ul id="home-slides" style="position: relative; width: 700px; height: 300px;">
<li style="position: absolute; top: 0px; left: 0px; display: none; z-index: 1; opacity: 1;"><a style="width: 700px;" href="https://www.moxy.co.th/en/deal/all-deals.html?cat=3292"><img src="{{media url="wysiwyg/beauty-homepage-banner-buy1-get1-sale-20151116.jpg"}}" alt="" /></a>
<div class="caption">buy 1 get 1</div>
</li>
<li style="position: absolute; top: 0px; left: 0px; display: none; z-index: 1; opacity: 1;"><a style="width: 700px;" href="http://www.moxy.co.th/en/lafema/brands/elizabeth-arden.html"><img src="{{media url="wysiwyg/Beauty-HomepageBanner-Elizabeth-Arden-10112015.jpg"}}" alt="" /></a>
<div class="caption">elizabeth 15</div>
</li>
<li style="position: absolute; top: 0px; left: 0px; display: none; z-index: 1; opacity: 1;"><a style="width: 700px;" href="http://www.moxy.co.th/en/lafema/brands/wet-n-wild.html"><img src="{{media url="wysiwyg/beauty-homepage-Wet-n-Wild-221015.jpg"}}" alt="" /></a>
<div class="caption">Wet n' Wild freebies</div>
</li>
<li style="position: absolute; top: 0px; left: 0px; display: none; z-index: 1; opacity: 1;"><a style="width: 700px;" href="http://www.moxy.co.th/en/lafema/deals/nyx-round-lips-promotion.html"><img src="{{media url="wysiwyg/beauty-NYX-homepage-banner-051015.jpg"}}" alt="" /></a>
<div class="caption">NYX Round Lipstick</div>
</li>
<li style="position: absolute; top: 0px; left: 0px; display: none; z-index: 1; opacity: 1;"><a style="width: 700px;" href="http://www.moxy.co.th/en/lafema/brands/w7.html"><img src="{{media url="wysiwyg/beauty-homepage-w7-081015.jpg"}}" alt="" /></a>
<div class="caption">W7</div>
</li>
</ul>
</div>
</div>
</div>
<div class="homepage-bannerl1 first"><a href="http://www.lafema.com/th/hair/hair-tools.html"><img src="{{media url="wysiwyg/Beauty-side-banner-hair-tools.jpg"}}" alt="" /></a></div>
<div class="homepage-bannerl1"><a href="http://www.lafema.com/"><img src="{{media url="wysiwyg/InstallmentPlan_sidebanner_all.jpg"}}" alt="" /></a></div>
<div class="homepage-bannerl1 last"><a href="http://www.lafema.com/th/help/"><img src="{{media url="wysiwyg/Free-Shipping-Side-Banner_1.jpg"}}" alt="" /></a></div>
<p>{{block type="core/template" name="personal_widget" as="category_widget" template="emarsys/category.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="1568" template="catalog/product/recommend.phtml"}}</p>
EOD;
/** @var Mage_Cms_Model_Block */
$block = Mage::getModel('cms/block');
$block->setTitle('Slider Lafema EN');
$block->setIdentifier('slider_lafema');
$block->setStores($moxyen);
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================

//==========================================================================
// Slider Moxy -> Fashion TH
//==========================================================================
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($moxyth, $withAdmin = false)
    ->addFieldToFilter('identifier', 'slider_fashion')
    ->getFirstItem();

if ($block->getId()) $block->delete();// if exists then delete

$content = <<<EOD
<div class="homepage-main" style="float: left; width: 700px;">
<div id="container" style="width: 700px;">
<div id="slideshow" style="height: 300px; width: 700px;">
<ul id="slide-nav" style="display: none;">
<li id="prev"><a class="control-s pre-s" href="#">Previous</a></li>
<li id="next" style="left: 500px;"><a class="control-s nxt-s" href="#">Next</a></li>
</ul>
<ul id="home-slides">
<li><a style="width: 700px;" href="https://www.moxy.co.th/th/deal/all-deals.html?cat=3292"><img src="{{media url="wysiwyg/beauty-homepage-banner-buy1-get1-sale-20151116.jpg"}}" alt="" /></a>
<div class="caption">buy 1 get 1</div>
</li>
<li><a style="width: 700px;" href="http://www.moxy.co.th/th/brands/casio.html"><img src="{{media url="wysiwyg/homepage-fashion-casio.jpg"}}" alt="" /></a>
<div class="caption">Casio</div>
</li>
<li><a style="width: 700px;" href="http://www.moxy.co.th/th/brands/cromet.html"><img src="{{media url="wysiwyg/homepage-cromet-fashion.jpg"}}" alt="" /></a>
<div class="caption">Cromet</div>
</li>
</ul>
</div>
</div>
</div>
<div class="homepage-bannerl1 first"><a href="http://www.moxyst.com/th/help/#shipping"><img src="{{media url="wysiwyg/Free-Shipping-Side-Banner_1.jpg"}}" alt="" /></a></div>
<div class="homepage-bannerl1"><a href="http://www.moxy.co.th"> <img src="{{media url="wysiwyg/InstallmentPlan_sidebanner_all.jpg"}}" alt="" /></a></div>
<div class="homepage-bannerl1 last"><a href="http://www.moxy.co.th/th/brands/bsetbag.html"><img src="{{media url="wysiwyg/moxy-sidebanner-bsetbag-231115.jpg"}}" alt="" /></a></div>
<p>{{block type="core/template" name="personal_widget" as="category_widget" template="emarsys/category.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="1568" template="catalog/product/recommend.phtml"}}</p>
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$block = Mage::getModel('cms/block');
$block->setTitle('Slider Fashion TH');
$block->setIdentifier('slider_fashion');
$block->setStores($moxyth);
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================

//==========================================================================
// Slider Moxy -> Fashion EN
//==========================================================================
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($moxyen, $withAdmin = false)
    ->addFieldToFilter('identifier', 'slider_fashion')
    ->getFirstItem();

if ($block->getId()) $block->delete();// if exists then delete

$content = <<<EOD
<div class="homepage-main" style="float: left; width: 700px;">
<div id="container" style="width: 700px;">
<div id="slideshow" style="height: 300px; width: 700px;">
<ul id="slide-nav" style="display: none;">
<li id="prev"><a class="control-s pre-s" href="#">Previous</a></li>
<li id="next" style="left: 500px;"><a class="control-s nxt-s" href="#">Next</a></li>
</ul>
<ul id="home-slides">
<li><a style="width: 700px;" href="https://www.moxy.co.th/en/deal/all-deals.html?cat=3292"><img src="{{media url="wysiwyg/beauty-homepage-banner-buy1-get1-sale-20151116.jpg"}}" alt="" /></a>
<div class="caption">buy 1 get 1</div>
</li>
<li><a style="width: 700px;" href="http://www.moxy.co.th/en/brands/casio.html"><img src="{{media url="wysiwyg/homepage-fashion-casio.jpg"}}" alt="" /></a>
<div class="caption">Casio</div>
</li>
<li><a style="width: 700px;" href="http://www.moxy.co.th/en/brands/cromet.html"><img src="{{media url="wysiwyg/homepage-cromet-fashion.jpg"}}" alt="" /></a>
<div class="caption">Cromet</div>
</li>
</ul>
</div>
</div>
</div>
<div class="homepage-bannerl1 first"><a href="http://www.moxyst.com/th/help/#shipping"><img src="{{media url="wysiwyg/Free-Shipping-Side-Banner_1.jpg"}}" alt="" /></a></div>
<div class="homepage-bannerl1"><a href="http://www.moxy.co.th/th/help/#payments"><img src="{{media url="wysiwyg/InstallmentPlan_sidebanner_all.jpg"}}" alt="" /></a></div>
<div class="homepage-bannerl1 last"><a href="http://www.moxy.co.th/th/brands/bsetbag.html"><img src="{{media url="wysiwyg/moxy-sidebanner-bsetbag-231115.jpg"}}" alt="" /></a></div>
<p>{{block type="core/template" name="personal_widget" as="category_widget" template="emarsys/category.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="1568" template="catalog/product/recommend.phtml"}}</p>
EOD;
/** @var Mage_Cms_Model_Block */
$block = Mage::getModel('cms/block');
$block->setTitle('Slider Fashion EN');
$block->setIdentifier('slider_fashion');
$block->setStores($moxyen);
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================


//==========================================================================
// Slider Moxy -> Home Living TH
//==========================================================================
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($moxyth, $withAdmin = false)
    ->addFieldToFilter('identifier', 'slider_home-living')
    ->getFirstItem();

if ($block->getId()) $block->delete();// if exists then delete

$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($moxyth, $withAdmin = false)
    ->addFieldToFilter('identifier', 'slider_homeliving')
    ->getFirstItem();

if ($block->getId()) $block->delete();// if exists then delete


$content = <<<EOD
<div class="homepage-main" style="float: left; width: 700px;">
<div id="container" style="width: 700px;">
<div id="slideshow" style="height: 300px; width: 700px;">
<ul id="slide-nav" style="display: none;">
<li id="prev"><a class="control-s pre-s" href="#">Previous</a></li>
<li id="next" style="left: 500px;"><a class="control-s nxt-s" href="#">Next</a></li>
</ul>
<ul id="home-slides">
<li style="position: absolute; top: 0px; left: 0px; display: none; z-index: 1; opacity: 1;"><a style="width: 700px;" href="https://www.moxy.co.th/th/deal/all-deals.html?cat=3292"><img src="{{media url="wysiwyg/beauty-homepage-banner-buy1-get1-sale-20151116.jpg"}}" alt="" /></a>
<div class="caption">buy 1 get 1</div>
</li>
<li><a style="width: 700px;" href="http://www.moxy.co.th/th/brands/springmate.html"><img src="{{media url="wysiwyg/Moxy-HomepageBanner-SPRINGMOX-09112015.jpg"}}" alt="" /></a>
<div class="caption">Springmate</div>
</li>
<li><a style="width: 700px;" href="http://www.moxy.co.th/th/brands/lotus.html"><img src="{{media url="wysiwyg/Moxy-Homepage-LOTUS-07102015.jpg"}}" alt="" /></a>
<div class="caption">Lotus</div>
</li>
</ul>
</div>
</div>
</div>
<div class="homepage-bannerl1 first"><a href="http://www.moxyst.com/th/help/#shipping"><img src="{{media url="wysiwyg/Free-Shipping-Side-Banner_1.jpg"}}" alt="" /></a></div>
<div class="homepage-bannerl1"><a href="http://www.moxy.co.th"> <img src="{{media url="wysiwyg/InstallmentPlan_sidebanner_all.jpg"}}" alt="" /></a></div>
<div class="homepage-bannerl1 last"><a href="http://www.moxy.co.th/th/brands/tree-square.html"> <img src="{{media url="wysiwyg/Home-_-Decor-side-banner-Treesquare.jpg"}}" alt="" /></a></div>
<p>{{block type="core/template" name="personal_widget" as="category_widget" template="emarsys/category.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="1568" template="catalog/product/recommend.phtml"}}</p>
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$block = Mage::getModel('cms/block');
$block->setTitle('Slider Home Living TH');
$block->setIdentifier('slider_home-living');
$block->setStores($moxyth);
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================

//==========================================================================
// Slider Moxy -> Home Living EN
//==========================================================================
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($moxyen, $withAdmin = false)
    ->addFieldToFilter('identifier', 'slider_home-living')
    ->getFirstItem();

if ($block->getId()) $block->delete();// if exists then delete

$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($moxyen, $withAdmin = false)
    ->addFieldToFilter('identifier', 'slider_homeliving')
    ->getFirstItem();

if ($block->getId()) $block->delete();// if exists then delete

$content = <<<EOD
<div class="homepage-main" style="float: left; width: 700px;">
<div id="container" style="width: 700px;">
<div id="slideshow" style="height: 300px; width: 700px;">
<ul id="slide-nav" style="display: none;">
<li id="prev"><a class="control-s pre-s" href="#">Previous</a></li>
<li id="next" style="left: 500px;"><a class="control-s nxt-s" href="#">Next</a></li>
</ul>
<ul id="home-slides">
<li style="position: absolute; top: 0px; left: 0px; display: none; z-index: 1; opacity: 1;"><a style="width: 700px;" href="https://www.moxy.co.th/en/deal/all-deals.html?cat=3292"><img src="{{media url="wysiwyg/beauty-homepage-banner-buy1-get1-sale-20151116.jpg"}}" alt="" /></a>
<div class="caption">buy 1 get 1</div>
</li>
<li><a style="width: 700px;" href="http://www.moxy.co.th/en/brands/springmate.html"><img src="{{media url="wysiwyg/Moxy-HomepageBanner-SPRINGMOX-09112015.jpg"}}" alt="" /></a>
<div class="caption">Springmate</div>
</li>
<li><a style="width: 700px;" href="http://www.moxy.co.th/th/brands/lotus.html"><img src="{{media url="wysiwyg/Moxy-Homepage-LOTUS-07102015.jpg"}}" alt="" /></a>
<div class="caption">Lotus</div>
</li>
</ul>
</div>
</div>
</div>
<div class="homepage-bannerl1 first"><a href="http://www.moxyst.com/th/help/#shipping"><img src="{{media url="wysiwyg/Free-Shipping-Side-Banner_1.jpg"}}" alt="" /></a></div>
<div class="homepage-bannerl1"><a href="http://www.moxy.co.th"> <img src="{{media url="wysiwyg/InstallmentPlan_sidebanner_all.jpg"}}" alt="" /></a></div>
<div class="homepage-bannerl1 last"><a href="http://www.moxy.co.th/th/brands/tree-square.html"> <img src="{{media url="wysiwyg/Home-_-Decor-side-banner-Treesquare.jpg"}}" alt="" /></a></div>
<p>{{block type="core/template" name="personal_widget" as="category_widget" template="emarsys/category.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="1568" template="catalog/product/recommend.phtml"}}</p>
EOD;
/** @var Mage_Cms_Model_Block */
$block = Mage::getModel('cms/block');
$block->setTitle('Slider Home Living EN');
$block->setIdentifier('slider_home-living');
$block->setStores($moxyen);
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// Slider Moxy -> Electronics TH
//==========================================================================
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($moxyth, $withAdmin = false)
    ->addFieldToFilter('identifier', 'slider_gadgets-electronics')
    ->getFirstItem();

if ($block->getId()) $block->delete();// if exists then delete

$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($moxyth, $withAdmin = false)
    ->addFieldToFilter('identifier', 'slider_electronics')
    ->getFirstItem();

if ($block->getId()) $block->delete();// if exists then delete


$content = <<<EOD
<div class="homepage-main" style="width: 700px;">
<div id="container" style="width: 700px;">
<div id="slideshow" style="height: 300px; width: 700px;">
<ul id="slide-nav" style="display: none;">
<li id="prev"><a class="control-s pre-s" href="#">Previous</a></li>
<li id="next" style="left: 500px;"><a class="control-s nxt-s" href="#">Next</a></li>
</ul>
<ul id="home-slides">
<li style="position: absolute; top: 0px; left: 0px; display: none; z-index: 1; opacity: 1;"><a style="width: 700px;" href="https://www.moxy.co.th/th/deal/all-deals.html?cat=3292"><img src="{{media url="wysiwyg/beauty-homepage-banner-buy1-get1-sale-20151116.jpg"}}" alt="" /></a>
<div class="caption">buy 1 get 1</div>
</li>
<li><a style="width: 700px;" href="http://www.moxy.co.th/th/brands/iclebo.html"><img src="{{media url="wysiwyg/iclebo-electronics-discount_homepage.jpg"}}" alt="" /></a>
<div class="caption">iclebo 2nd item 50 off</div>
</li>
<li><a style="width: 700px;" href="http://www.moxy.co.th/th/brands/monowheel.html"><img src="{{media url="wysiwyg/monowheel_homepage_moxyelectronics.jpg"}}" alt="" /></a>
<div class="caption">monowheel 0% installment</div>
</li>
<li><a style="width: 700px;" href="http://www.moxy.co.th/th/catalogsearch/result/?store=17&amp;cate=2138&amp;q=iphone+6s"><img src="{{media url="wysiwyg/homepage-banner-iPhone-6s-NEW-041115.jpg"}}" alt="" /></a>
<div class="caption">iphone6s</div>
</li>
</ul>
</div>
</div>
</div>
<div class="homepage-bannerl1 first"><a href="http://www.moxyst.com/th/help/#shipping"><img src="{{media url="wysiwyg/Free-Shipping-Side-Banner_1.jpg"}}" alt="" /></a></div>
<div class="homepage-bannerl1"><a href="http://www.moxy.co.th"> <img src="{{media url="wysiwyg/InstallmentPlan_sidebanner_all.jpg"}}" alt="" /></a></div>
<div class="homepage-bannerl1 last"><a href="http://www.moxy.co.th/th/gadgets-electronics/mobiles-tablets/shop-by-brands/apple.html"><img src="{{media url="wysiwyg/moxy-Side-banner-iphone-6-181115-02.jpg"}}" alt="" /></a></div>
<p>{{block type="core/template" name="personal_widget" as="category_widget" template="emarsys/category.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="1568" template="catalog/product/recommend.phtml"}}</p>
EOD;
/** @var Mage_Cms_Model_Block $enBlock */
$block = Mage::getModel('cms/block');
$block->setTitle('Slider Electronics TH');
$block->setIdentifier('slider_gadgets-electronics');
$block->setStores($moxyth);
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================

//==========================================================================
// Slider Moxy -> Electronics EN
//==========================================================================
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($moxyen, $withAdmin = false)
    ->addFieldToFilter('identifier', 'slider_gadgets-electronics')
    ->getFirstItem();

if ($block->getId()) $block->delete();// if exists then delete

$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($moxyen, $withAdmin = false)
    ->addFieldToFilter('identifier', 'slider_electronics')
    ->getFirstItem();

if ($block->getId()) $block->delete();// if exists then delete

$content = <<<EOD
<div class="homepage-main" style="width: 700px;">
<div id="container" style="width: 700px;">
<div id="slideshow" style="height: 300px; width: 700px;">
<ul id="slide-nav" style="display: none;">
<li id="prev"><a class="control-s pre-s" href="#">Previous</a></li>
<li id="next" style="left: 500px;"><a class="control-s nxt-s" href="#">Next</a></li>
</ul>
<ul id="home-slides">
<li style="position: absolute; top: 0px; left: 0px; display: none; z-index: 1; opacity: 1;"><a style="width: 700px;" href="https://www.moxy.co.th/en/deal/all-deals.html?cat=3292"><img src="{{media url="wysiwyg/beauty-homepage-banner-buy1-get1-sale-20151116.jpg"}}" alt="" /></a>
<div class="caption">buy 1 get 1</div>
</li>
<li><a style="width: 700px;" href="http://www.moxy.co.th/en/brands/iclebo.html"><img src="{{media url="wysiwyg/iclebo-electronics-discount_homepage.jpg"}}" alt="" /></a>
<div class="caption">iclebo 2nd item 50 off</div>
</li>
<li><a style="width: 700px;" href="http://www.moxy.co.th/en/brands/monowheel.html"><img src="{{media url="wysiwyg/monowheel_homepage_moxyelectronics.jpg"}}" alt="" /></a>
<div class="caption">monowheel 0% installment</div>
</li>
<li><a style="width: 700px;" href="http://www.moxy.co.th/en/catalogsearch/result/?store=17&amp;cate=2138&amp;q=iphone+6s"><img src="{{media url="wysiwyg/homepage-banner-iPhone-6s-NEW-041115.jpg"}}" alt="" /></a>
<div class="caption">iphone6s</div>
</li>
</ul>
</div>
</div>
</div>
<div class="homepage-bannerl1 first"><a href="http://www.moxyst.com/th/help/#shipping"><img src="{{media url="wysiwyg/Free-Shipping-Side-Banner_1.jpg"}}" alt="" /></a></div>
<div class="homepage-bannerl1"><a href="http://www.moxy.co.th"><img src="{{media url="wysiwyg/InstallmentPlan_sidebanner_all.jpg"}}" alt="" /></a></div>
<div class="homepage-bannerl1 last"><a href="http://www.moxy.co.th/th/gadgets-electronics/mobiles-tablets/shop-by-brands/apple.html"><img src="{{media url="wysiwyg/moxy-Side-banner-iphone-6-181115-02.jpg"}}" alt="" /></a></div>
<p>{{block type="core/template" name="personal_widget" as="category_widget" template="emarsys/category.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="1568" template="catalog/product/recommend.phtml"}}</p>
EOD;
/** @var Mage_Cms_Model_Block */
$block = Mage::getModel('cms/block');
$block->setTitle('Slider Electronics EN');
$block->setIdentifier('slider_gadgets-electronics');
$block->setStores($moxyen);
$block->setIsActive(1);
$block->setContent($content);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================


//==========================================================================
// Configurations
//==========================================================================

$config = new Mage_Core_Model_Config();
$config->saveConfig('catalog/frontend/list_allow_all', 0, 'default', 0);

