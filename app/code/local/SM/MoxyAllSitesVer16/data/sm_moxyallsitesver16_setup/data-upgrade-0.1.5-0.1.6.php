<?php

// init Magento
Mage::app();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

$engStoreCode   = 'moxyen';
$thStoreCode    = 'moxyth';
$storeIdMoxyEn  = Mage::getModel('core/store')->load($engStoreCode, 'code')->getId();
$storeIdMoxyTh  = Mage::getModel('core/store')->load($thStoreCode, 'code')->getId();

// Fix Moxy's "slider_1" blocks

//==========================================================================
// slider_1 (Moxy English)
//==========================================================================
$blockTitle = "Site Banner Home Slider";
$blockIdentifier = "slider_1";
$blockStores = array($storeIdMoxyEn);
$blockIsActive = 1;
$blockContent = <<<EOD
<div class="homepage-main" style="float: left; width: 610px; margin: 0px 30px;">
<div id="container" style="width: 610px;">
<div id="slideshow" style="margin: 0px 15px 25px 15px; height: 400px; width: 610px;">
<ul id="slide-nav" style="display: none;">
<li id="prev"><a class="control-s pre-s" href="#">Previous</a></li>
<li id="next" style="left: 500px;"><a class="control-s nxt-s" href="#">Next</a></li>
</ul>
<ul id="home-slides">
<li><a style="width: 610px;" href="http://www.moxyst.com/en/deal/celebrate-first-love.html"><img src="{{media url="wysiwyg/MothersDay_Moxy_homepage.jpg"}}" alt="" /></a>
<div class="caption">mothersday discount</div>
</li>
<li><a style="width: 610px;" href="http://www.moxyst.com/"><img src="{{media url="wysiwyg/BigDiscount_Moxy.jpg"}}" alt="" /></a>
<div class="caption">special discount</div>
</li>
<li><a style="width: 610px;" href="http://www.moxyst.com/en/deal/celebrate-first-love.html"><img src="{{media url="wysiwyg/MothersDay_Moxy_homepage_products.jpg"}}" alt="" /></a>
<div class="caption">mothersday select</div>
</li>
<li><a style="width: 610px;" href="http://www.moxyst.com/en/moleskine-mycloud-tote-bag-payne-s-grey-21608.html"><img src="{{media url="wysiwyg/homepage_Moxy_8jul15-moleskine.jpg"}}" alt="" /></a>
<div class="caption">moleskine</div>
</li>
<li><a style="width: 610px;" href="http://www.moxyst.com/en/brands/garosu-girls.html"><img src="{{media url="wysiwyg/GarosuBanner2HomepageBanner.jpg"}}" alt="" /></a>
<div class="caption">Garosu</div>
</li>
<li><a style="width: 610px;" href="http://www.moxyst.com"><img src="{{media url="wysiwyg/moxy_henrycats2_1.jpg"}}" alt="" /></a>
<div class="caption">henry cats</div>
</li>
</ul>
</div>
</div>
</div>
<div class="homepage-bannerl1"><a href="http://www.moxyst.com/th/help/#shipping"><img src="{{media url="wysiwyg/freeshipping_no_minimum.jpg"}}" alt="" /> </a></div>
<!-- start small banner 1 -->
<div class="homepage-bannerl1"><a href="https://www.moxyst.com/th/customer/account/create/"> <img src="{{media url="wysiwyg/200baht_sidebanner_1.jpg"}}" alt="" /></a></div>
<!-- end small banner 1 -->
<p>&nbsp;</p>
<!-- start small banner 2 -->
<div class="homepage-bannerl2"><a href="http://www.moxyst.com/th/deal/celebrate-first-love.html"> <img src="{{media url="wysiwyg/MothersDay_Moxy_sidebanner.jpg"}}" alt="" /></a></div>
<!-- end small banner 2 -->
<p>&nbsp;</p>
EOD;
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// slider_1 (Moxy Thai)
//==========================================================================
$blockTitle = "Site Banner Home Slider";
$blockIdentifier = "slider_1";
$blockStores = array($storeIdMoxyTh);
$blockIsActive = 1;
$blockContent = <<<EOD
<div class="homepage-main" style="float: left; width: 610px; margin: 0px 30px;">
<div id="container" style="width: 610px;">
<div id="slideshow" style="margin: 0px 15px 25px 15px; height: 400px; width: 610px;">
<ul id="slide-nav" style="display: none;">
<li id="prev"><a class="control-s pre-s" href="#">Previous</a></li>
<li id="next" style="left: 500px;"><a class="control-s nxt-s" href="#">Next</a></li>
</ul>
<ul id="home-slides">
<li><a style="width: 610px;" href="http://www.moxyst.com/th/deal/celebrate-first-love.html"><img src="{{media url="wysiwyg/MothersDay_Moxy_homepage.jpg"}}" alt="" /></a>
<div class="caption">mothersday discount</div>
</li>
<li><a style="width: 610px;" href="http://www.moxyst.com/"><img src="{{media url="wysiwyg/BigDiscount_Moxy.jpg"}}" alt="" /></a>
<div class="caption">special discount</div>
</li>
<li><a style="width: 610px;" href="http://www.moxyst.com/th/deal/celebrate-first-love.html"><img src="{{media url="wysiwyg/MothersDay_Moxy_homepage_products.jpg"}}" alt="" /></a>
<div class="caption">mothersday select</div>
</li>
<li><a style="width: 610px;" href="http://www.moxyst.com/th/moleskine-mycloud-tote-bag-payne-s-grey-21608.html"><img src="{{media url="wysiwyg/homepage_Moxy_8jul15-moleskine.jpg"}}" alt="" /></a>
<div class="caption">moleskine</div>
</li>
<li><a style="width: 610px;" href="http://www.moxyst.com/en/brands/garosu-girls.html"><img src="{{media url="wysiwyg/GarosuBanner2HomepageBanner.jpg"}}" alt="" /></a>
<div class="caption">Garosu</div>
</li>
<li><a style="width: 610px;" href="http://www.moxyst.com"><img src="{{media url="wysiwyg/moxy_henrycats2_1.jpg"}}" alt="" /></a>
<div class="caption">henry cats</div>
</li>
</ul>
</div>
</div>
</div>
<div class="homepage-bannerl1"><a href="http://www.moxyst.com/th/help/#shipping"><img src="{{media url="wysiwyg/freeshipping_no_minimum.jpg"}}" alt="" /> </a></div>
<!-- start small banner 1 -->
<div class="homepage-bannerl1"><a href="http://www.moxyst.com/?popup=acommerce?popup=acommerce"> <img src="{{media url="wysiwyg/200baht_sidebanner_1.jpg"}}" alt="" /></a></div>
<!-- end small banner 1 -->
<p>&nbsp;</p>
<!-- start small banner 2 -->
<div class="homepage-bannerl2"><a href="http://www.moxyst.com/th/deal/celebrate-first-love.html"> <img src="{{media url="wysiwyg/MothersDay_Moxy_sidebanner.jpg"}}" alt="" /></a></div>
<!-- end small banner 2 -->
<p>&nbsp;</p>
EOD;
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================



// Create new sliders for Fashion, Living, Electronics



//==========================================================================
// slider_fashion (Moxy English)
//==========================================================================
$blockTitle = "Site Banner Moxy Fashion Slider";
$blockIdentifier = "slider_fashion";
$blockStores = array($storeIdMoxyEn);
$blockIsActive = 1;
$blockContent = <<<EOD
<div class="homepage-main" style="float: left; width: 610px; margin: 0px 30px;">
<div id="container" style="width: 610px;">
<div id="slideshow" style="margin: 0px 15px 25px 15px; height: 400px; width: 610px;">
<ul id="slide-nav" style="display: none;">
<li id="prev"><a class="control-s pre-s" href="#">Previous</a></li>
<li id="next" style="left: 500px;"><a class="control-s nxt-s" href="#">Next</a></li>
</ul>
<ul id="home-slides">
<li><a style="width: 610px;" href="http://www.moxyst.com/en/deal/celebrate-first-love.html"><img src="{{media url="wysiwyg/MothersDay_Moxy_homepage.jpg"}}" alt="" /></a>
<div class="caption">mothersday discount</div>
</li>
<li><a style="width: 610px;" href="http://www.moxyst.com/"><img src="{{media url="wysiwyg/BigDiscount_Moxy.jpg"}}" alt="" /></a>
<div class="caption">special discount</div>
</li>
<li><a style="width: 610px;" href="http://www.moxyst.com/en/deal/celebrate-first-love.html"><img src="{{media url="wysiwyg/MothersDay_Moxy_homepage_products.jpg"}}" alt="" /></a>
<div class="caption">mothersday select</div>
</li>
<li><a style="width: 610px;" href="http://www.moxyst.com/en/moleskine-mycloud-tote-bag-payne-s-grey-21608.html"><img src="{{media url="wysiwyg/homepage_Moxy_8jul15-moleskine.jpg"}}" alt="" /></a>
<div class="caption">moleskine</div>
</li>
<li><a style="width: 610px;" href="http://www.moxyst.com/en/brands/garosu-girls.html"><img src="{{media url="wysiwyg/GarosuBanner2HomepageBanner.jpg"}}" alt="" /></a>
<div class="caption">Garosu</div>
</li>
<li><a style="width: 610px;" href="http://www.moxyst.com"><img src="{{media url="wysiwyg/moxy_henrycats2_1.jpg"}}" alt="" /></a>
<div class="caption">henry cats</div>
</li>
</ul>
</div>
</div>
</div>
<div class="homepage-bannerl1"><a href="http://www.moxyst.com/th/help/#shipping"><img src="{{media url="wysiwyg/freeshipping_no_minimum.jpg"}}" alt="" /> </a></div>
<!-- start small banner 1 -->
<div class="homepage-bannerl1"><a href="https://www.moxyst.com/th/customer/account/create/"> <img src="{{media url="wysiwyg/200baht_sidebanner_1.jpg"}}" alt="" /></a></div>
<!-- end small banner 1 -->
<!-- start small banner 2 -->
<div class="homepage-bannerl2"><a href="http://www.moxyst.com/th/deal/celebrate-first-love.html"> <img src="{{media url="wysiwyg/MothersDay_Moxy_sidebanner.jpg"}}" alt="" /></a></div>
<!-- end small banner 2 -->
EOD;
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// slider_electronics (Moxy English)
//==========================================================================
$blockTitle = "Site Banner Moxy Electronics Slider";
$blockIdentifier = "slider_electronics";
$blockStores = array($storeIdMoxyEn);
$blockIsActive = 1;
$blockContent = <<<EOD
<div class="homepage-main" style="float: left; width: 610px; margin: 0px 30px;">
<div id="container" style="width: 610px;">
<div id="slideshow" style="margin: 0px 15px 25px 15px; height: 400px; width: 610px;">
<ul id="slide-nav" style="display: none;">
<li id="prev"><a class="control-s pre-s" href="#">Previous</a></li>
<li id="next" style="left: 500px;"><a class="control-s nxt-s" href="#">Next</a></li>
</ul>
<ul id="home-slides">
<li><a style="width: 610px;" href="http://www.moxyst.com/en/deal/celebrate-first-love.html"><img src="{{media url="wysiwyg/MothersDay_Moxy_homepage.jpg"}}" alt="" /></a>
<div class="caption">mothersday discount</div>
</li>
<li><a style="width: 610px;" href="http://www.moxyst.com/"><img src="{{media url="wysiwyg/BigDiscount_Moxy.jpg"}}" alt="" /></a>
<div class="caption">special discount</div>
</li>
<li><a style="width: 610px;" href="http://www.moxyst.com/en/deal/celebrate-first-love.html"><img src="{{media url="wysiwyg/MothersDay_Moxy_homepage_products.jpg"}}" alt="" /></a>
<div class="caption">mothersday select</div>
</li>
<li><a style="width: 610px;" href="http://www.moxyst.com/en/moleskine-mycloud-tote-bag-payne-s-grey-21608.html"><img src="{{media url="wysiwyg/homepage_Moxy_8jul15-moleskine.jpg"}}" alt="" /></a>
<div class="caption">moleskine</div>
</li>
<li><a style="width: 610px;" href="http://www.moxyst.com/en/brands/garosu-girls.html"><img src="{{media url="wysiwyg/GarosuBanner2HomepageBanner.jpg"}}" alt="" /></a>
<div class="caption">Garosu</div>
</li>
<li><a style="width: 610px;" href="http://www.moxyst.com"><img src="{{media url="wysiwyg/moxy_henrycats2_1.jpg"}}" alt="" /></a>
<div class="caption">henry cats</div>
</li>
</ul>
</div>
</div>
</div>
<div class="homepage-bannerl1"><a href="http://www.moxyst.com/th/help/#shipping"><img src="{{media url="wysiwyg/freeshipping_no_minimum.jpg"}}" alt="" /> </a></div>
<!-- start small banner 1 -->
<div class="homepage-bannerl1"><a href="https://www.moxyst.com/th/customer/account/create/"> <img src="{{media url="wysiwyg/200baht_sidebanner_1.jpg"}}" alt="" /></a></div>
<!-- end small banner 1 -->
<!-- start small banner 2 -->
<div class="homepage-bannerl2"><a href="http://www.moxyst.com/th/deal/celebrate-first-love.html"> <img src="{{media url="wysiwyg/MothersDay_Moxy_sidebanner.jpg"}}" alt="" /></a></div>
<!-- end small banner 2 -->
EOD;
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// slider_homeliving (Moxy English)
//==========================================================================
$blockTitle = "Site Banner Moxy Living Slider";
$blockIdentifier = "slider_homeliving";
$blockStores = array($storeIdMoxyEn);
$blockIsActive = 1;
$blockContent = <<<EOD
<div class="homepage-main" style="float: left; width: 610px; margin: 0px 30px;">
<div id="container" style="width: 610px;">
<div id="slideshow" style="margin: 0px 15px 25px 15px; height: 400px; width: 610px;">
<ul id="slide-nav" style="display: none;">
<li id="prev"><a class="control-s pre-s" href="#">Previous</a></li>
<li id="next" style="left: 500px;"><a class="control-s nxt-s" href="#">Next</a></li>
</ul>
<ul id="home-slides">
<li><a style="width: 610px;" href="http://www.moxyst.com/en/deal/celebrate-first-love.html"><img src="{{media url="wysiwyg/MothersDay_Moxy_homepage.jpg"}}" alt="" /></a>
<div class="caption">mothersday discount</div>
</li>
<li><a style="width: 610px;" href="http://www.moxyst.com/"><img src="{{media url="wysiwyg/BigDiscount_Moxy.jpg"}}" alt="" /></a>
<div class="caption">special discount</div>
</li>
<li><a style="width: 610px;" href="http://www.moxyst.com/en/deal/celebrate-first-love.html"><img src="{{media url="wysiwyg/MothersDay_Moxy_homepage_products.jpg"}}" alt="" /></a>
<div class="caption">mothersday select</div>
</li>
<li><a style="width: 610px;" href="http://www.moxyst.com/en/moleskine-mycloud-tote-bag-payne-s-grey-21608.html"><img src="{{media url="wysiwyg/homepage_Moxy_8jul15-moleskine.jpg"}}" alt="" /></a>
<div class="caption">moleskine</div>
</li>
<li><a style="width: 610px;" href="http://www.moxyst.com/en/brands/garosu-girls.html"><img src="{{media url="wysiwyg/GarosuBanner2HomepageBanner.jpg"}}" alt="" /></a>
<div class="caption">Garosu</div>
</li>
<li><a style="width: 610px;" href="http://www.moxyst.com"><img src="{{media url="wysiwyg/moxy_henrycats2_1.jpg"}}" alt="" /></a>
<div class="caption">henry cats</div>
</li>
</ul>
</div>
</div>
</div>
<div class="homepage-bannerl1"><a href="http://www.moxyst.com/th/help/#shipping"><img src="{{media url="wysiwyg/freeshipping_no_minimum.jpg"}}" alt="" /> </a></div>
<!-- start small banner 1 -->
<div class="homepage-bannerl1"><a href="https://www.moxyst.com/th/customer/account/create/"> <img src="{{media url="wysiwyg/200baht_sidebanner_1.jpg"}}" alt="" /></a></div>
<!-- end small banner 1 -->
<!-- start small banner 2 -->
<div class="homepage-bannerl2"><a href="http://www.moxyst.com/th/deal/celebrate-first-love.html"> <img src="{{media url="wysiwyg/MothersDay_Moxy_sidebanner.jpg"}}" alt="" /></a></div>
<!-- end small banner 2 -->
EOD;
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// slider_fashion (Moxy Thai)
//==========================================================================
$blockTitle = "Site Banner Moxy Fashion Slider";
$blockIdentifier = "slider_fashion";
$blockStores = array($storeIdMoxyTh);
$blockIsActive = 1;
$blockContent = <<<EOD
<div class="homepage-main" style="float: left; width: 610px; margin: 0px 30px;">
<div id="container" style="width: 610px;">
<div id="slideshow" style="margin: 0px 15px 25px 15px; height: 400px; width: 610px;">
<ul id="slide-nav" style="display: none;">
<li id="prev"><a class="control-s pre-s" href="#">Previous</a></li>
<li id="next" style="left: 500px;"><a class="control-s nxt-s" href="#">Next</a></li>
</ul>
<ul id="home-slides">
<li><a style="width: 610px;" href="http://www.moxyst.com/th/deal/celebrate-first-love.html"><img src="{{media url="wysiwyg/MothersDay_Moxy_homepage.jpg"}}" alt="" /></a>
<div class="caption">mothersday discount</div>
</li>
<li><a style="width: 610px;" href="http://www.moxyst.com/"><img src="{{media url="wysiwyg/BigDiscount_Moxy.jpg"}}" alt="" /></a>
<div class="caption">special discount</div>
</li>
<li><a style="width: 610px;" href="http://www.moxyst.com/th/deal/celebrate-first-love.html"><img src="{{media url="wysiwyg/MothersDay_Moxy_homepage_products.jpg"}}" alt="" /></a>
<div class="caption">mothersday select</div>
</li>
<li><a style="width: 610px;" href="http://www.moxyst.com/th/moleskine-mycloud-tote-bag-payne-s-grey-21608.html"><img src="{{media url="wysiwyg/homepage_Moxy_8jul15-moleskine.jpg"}}" alt="" /></a>
<div class="caption">moleskine</div>
</li>
<li><a style="width: 610px;" href="http://www.moxyst.com/en/brands/garosu-girls.html"><img src="{{media url="wysiwyg/GarosuBanner2HomepageBanner.jpg"}}" alt="" /></a>
<div class="caption">Garosu</div>
</li>
<li><a style="width: 610px;" href="http://www.moxyst.com"><img src="{{media url="wysiwyg/moxy_henrycats2_1.jpg"}}" alt="" /></a>
<div class="caption">henry cats</div>
</li>
</ul>
</div>
</div>
</div>
<div class="homepage-bannerl1"><a href="http://www.moxyst.com/th/help/#shipping"><img src="{{media url="wysiwyg/freeshipping_no_minimum.jpg"}}" alt="" /> </a></div>
<!-- start small banner 1 -->
<div class="homepage-bannerl1"><a href="http://www.moxyst.com/?popup=acommerce?popup=acommerce"> <img src="{{media url="wysiwyg/200baht_sidebanner_1.jpg"}}" alt="" /></a></div>
<!-- end small banner 1 -->
<!-- start small banner 2 -->
<div class="homepage-bannerl2"><a href="http://www.moxyst.com/th/deal/celebrate-first-love.html"> <img src="{{media url="wysiwyg/MothersDay_Moxy_sidebanner.jpg"}}" alt="" /></a></div>
<!-- end small banner 2 -->
EOD;
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// slider_electronics (Moxy Thai)
//==========================================================================
$blockTitle = "Site Banner Moxy Electronics Slider";
$blockIdentifier = "slider_electronics";
$blockStores = array($storeIdMoxyTh);
$blockIsActive = 1;
$blockContent = <<<EOD
<div class="homepage-main" style="float: left; width: 610px; margin: 0px 30px;">
<div id="container" style="width: 610px;">
<div id="slideshow" style="margin: 0px 15px 25px 15px; height: 400px; width: 610px;">
<ul id="slide-nav" style="display: none;">
<li id="prev"><a class="control-s pre-s" href="#">Previous</a></li>
<li id="next" style="left: 500px;"><a class="control-s nxt-s" href="#">Next</a></li>
</ul>
<ul id="home-slides">
<li><a style="width: 610px;" href="http://www.moxyst.com/th/deal/celebrate-first-love.html"><img src="{{media url="wysiwyg/MothersDay_Moxy_homepage.jpg"}}" alt="" /></a>
<div class="caption">mothersday discount</div>
</li>
<li><a style="width: 610px;" href="http://www.moxyst.com/"><img src="{{media url="wysiwyg/BigDiscount_Moxy.jpg"}}" alt="" /></a>
<div class="caption">special discount</div>
</li>
<li><a style="width: 610px;" href="http://www.moxyst.com/th/deal/celebrate-first-love.html"><img src="{{media url="wysiwyg/MothersDay_Moxy_homepage_products.jpg"}}" alt="" /></a>
<div class="caption">mothersday select</div>
</li>
<li><a style="width: 610px;" href="http://www.moxyst.com/th/moleskine-mycloud-tote-bag-payne-s-grey-21608.html"><img src="{{media url="wysiwyg/homepage_Moxy_8jul15-moleskine.jpg"}}" alt="" /></a>
<div class="caption">moleskine</div>
</li>
<li><a style="width: 610px;" href="http://www.moxyst.com/en/brands/garosu-girls.html"><img src="{{media url="wysiwyg/GarosuBanner2HomepageBanner.jpg"}}" alt="" /></a>
<div class="caption">Garosu</div>
</li>
<li><a style="width: 610px;" href="http://www.moxyst.com"><img src="{{media url="wysiwyg/moxy_henrycats2_1.jpg"}}" alt="" /></a>
<div class="caption">henry cats</div>
</li>
</ul>
</div>
</div>
</div>
<div class="homepage-bannerl1"><a href="http://www.moxyst.com/th/help/#shipping"><img src="{{media url="wysiwyg/freeshipping_no_minimum.jpg"}}" alt="" /> </a></div>
<!-- start small banner 1 -->
<div class="homepage-bannerl1"><a href="http://www.moxyst.com/?popup=acommerce?popup=acommerce"> <img src="{{media url="wysiwyg/200baht_sidebanner_1.jpg"}}" alt="" /></a></div>
<!-- end small banner 1 -->
<!-- start small banner 2 -->
<div class="homepage-bannerl2"><a href="http://www.moxyst.com/th/deal/celebrate-first-love.html"> <img src="{{media url="wysiwyg/MothersDay_Moxy_sidebanner.jpg"}}" alt="" /></a></div>
<!-- end small banner 2 -->
EOD;
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// slider_homeliving (Moxy Thai)
//==========================================================================
$blockTitle = "Site Banner Moxy Living Slider";
$blockIdentifier = "slider_homeliving";
$blockStores = array($storeIdMoxyTh);
$blockIsActive = 1;
$blockContent = <<<EOD
<div class="homepage-main" style="float: left; width: 610px; margin: 0px 30px;">
<div id="container" style="width: 610px;">
<div id="slideshow" style="margin: 0px 15px 25px 15px; height: 400px; width: 610px;">
<ul id="slide-nav" style="display: none;">
<li id="prev"><a class="control-s pre-s" href="#">Previous</a></li>
<li id="next" style="left: 500px;"><a class="control-s nxt-s" href="#">Next</a></li>
</ul>
<ul id="home-slides">
<li><a style="width: 610px;" href="http://www.moxyst.com/th/deal/celebrate-first-love.html"><img src="{{media url="wysiwyg/MothersDay_Moxy_homepage.jpg"}}" alt="" /></a>
<div class="caption">mothersday discount</div>
</li>
<li><a style="width: 610px;" href="http://www.moxyst.com/"><img src="{{media url="wysiwyg/BigDiscount_Moxy.jpg"}}" alt="" /></a>
<div class="caption">special discount</div>
</li>
<li><a style="width: 610px;" href="http://www.moxyst.com/th/deal/celebrate-first-love.html"><img src="{{media url="wysiwyg/MothersDay_Moxy_homepage_products.jpg"}}" alt="" /></a>
<div class="caption">mothersday select</div>
</li>
<li><a style="width: 610px;" href="http://www.moxyst.com/th/moleskine-mycloud-tote-bag-payne-s-grey-21608.html"><img src="{{media url="wysiwyg/homepage_Moxy_8jul15-moleskine.jpg"}}" alt="" /></a>
<div class="caption">moleskine</div>
</li>
<li><a style="width: 610px;" href="http://www.moxyst.com/en/brands/garosu-girls.html"><img src="{{media url="wysiwyg/GarosuBanner2HomepageBanner.jpg"}}" alt="" /></a>
<div class="caption">Garosu</div>
</li>
<li><a style="width: 610px;" href="http://www.moxyst.com"><img src="{{media url="wysiwyg/moxy_henrycats2_1.jpg"}}" alt="" /></a>
<div class="caption">henry cats</div>
</li>
</ul>
</div>
</div>
</div>
<div class="homepage-bannerl1"><a href="http://www.moxyst.com/th/help/#shipping"><img src="{{media url="wysiwyg/freeshipping_no_minimum.jpg"}}" alt="" /> </a></div>
<!-- start small banner 1 -->
<div class="homepage-bannerl1"><a href="http://www.moxyst.com/?popup=acommerce?popup=acommerce"> <img src="{{media url="wysiwyg/200baht_sidebanner_1.jpg"}}" alt="" /></a></div>
<!-- end small banner 1 -->
<!-- start small banner 2 -->
<div class="homepage-bannerl2"><a href="http://www.moxyst.com/th/deal/celebrate-first-love.html"> <img src="{{media url="wysiwyg/MothersDay_Moxy_sidebanner.jpg"}}" alt="" /></a></div>
<!-- end small banner 2 -->
EOD;
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// home_fashion (Moxy English)
//==========================================================================
$blockTitle = "Moxy Fashion Homepage";
$blockIdentifier = "home_fashion";
$blockStores = array($storeIdMoxyEn);
$blockIsActive = 1;
$blockContent = <<<EOD
<p>{{block type="core/template" name="subscription" as="subscription" template="subscription/subscribe.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="2440" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="2138" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="2140" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="2141" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="2142" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="cms/block" block_id="all-brands-slider"}}{{block type="cms/block" block_id="seo-keyword"}}</p>
EOD;
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// home_electronics (Moxy English)
//==========================================================================
$blockTitle = "Moxy Electronics Homepage";
$blockIdentifier = "home_electronics";
$blockStores = array($storeIdMoxyEn);
$blockIsActive = 1;
$blockContent = <<<EOD
<p>{{block type="core/template" name="subscription" as="subscription" template="subscription/subscribe.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="2440" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="2138" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="2140" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="2141" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="2142" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="cms/block" block_id="all-brands-slider"}}{{block type="cms/block" block_id="seo-keyword"}}</p>
EOD;
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// home_homeliving (Moxy English)
//==========================================================================
$blockTitle = "Moxy Living Homepage";
$blockIdentifier = "home_homeliving";
$blockStores = array($storeIdMoxyEn);
$blockIsActive = 1;
$blockContent = <<<EOD
<p>{{block type="core/template" name="subscription" as="subscription" template="subscription/subscribe.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="2440" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="2138" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="2140" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="2141" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="2142" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="cms/block" block_id="all-brands-slider"}}{{block type="cms/block" block_id="seo-keyword"}}</p>
EOD;
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// home_fashion (Moxy Thai)
//==========================================================================
$blockTitle = "Moxy Fashion Homepage";
$blockIdentifier = "home_fashion";
$blockStores = array($storeIdMoxyTh);
$blockIsActive = 1;
$blockContent = <<<EOD
<p>{{block type="core/template" name="subscription" as="subscription" template="subscription/subscribe.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="2440" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="2138" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="2140" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="2141" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="2142" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="cms/block" block_id="all-brands-slider"}}{{block type="cms/block" block_id="seo-keyword"}}</p>
EOD;
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// home_electronics (Moxy Thai)
//==========================================================================
$blockTitle = "Moxy Electronics Homepage";
$blockIdentifier = "home_electronics";
$blockStores = array($storeIdMoxyTh);
$blockIsActive = 1;
$blockContent = <<<EOD
<p>{{block type="core/template" name="subscription" as="subscription" template="subscription/subscribe.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="2440" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="2138" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="2140" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="2141" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="2142" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="cms/block" block_id="all-brands-slider"}}{{block type="cms/block" block_id="seo-keyword"}}</p>
EOD;
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// Site Product Sliders Home Moxy Living th block
//==========================================================================
$blockTitle = "Moxy Living Homepage";
$blockIdentifier = "home_homeliving";
$blockStores = array($storeIdMoxyTh);
$blockIsActive = 1;
$blockContent = <<<EOD
<p>{{block type="core/template" name="subscription" as="subscription" template="subscription/subscribe.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="2440" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="2138" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="2140" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="2141" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="petloftcatalog/product_recommend" name="home.catalog.product.list" alias="products_homepage" category_id="2142" template="catalog/product/recommend.phtml"}}</p>
<p>{{block type="cms/block" block_id="all-brands-slider"}}{{block type="cms/block" block_id="seo-keyword"}}</p>
EOD;
$block = Mage::getModel('cms/block')->getCollection()
    ->addStoreFilter($blockStores, $withAdmin = false)
    ->addFieldToFilter('identifier', $blockIdentifier)
    ->getFirstItem();
if ($block->getId()) $block->delete();// if exists then delete
$block = Mage::getModel('cms/block');
$block->setTitle($blockTitle);
$block->setIdentifier($blockIdentifier);
$block->setStores($blockStores);
$block->setIsActive($blockIsActive);
$block->setContent($blockContent);
$block->save();
//==========================================================================
//==========================================================================
//==========================================================================

