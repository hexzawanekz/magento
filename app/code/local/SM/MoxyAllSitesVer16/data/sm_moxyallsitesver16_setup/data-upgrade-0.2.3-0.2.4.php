<?php

// init Magento
Mage::app();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

// get store ids
$en     = Mage::getModel('core/store')->load('en', 'code')->getId();// Petloft
$th     = Mage::getModel('core/store')->load('th', 'code')->getId();
$len    = Mage::getModel('core/store')->load('len', 'code')->getId();// Lafema
$lth    = Mage::getModel('core/store')->load('lth', 'code')->getId();
$ven    = Mage::getModel('core/store')->load('ven', 'code')->getId();// Venbi
$vth    = Mage::getModel('core/store')->load('vth', 'code')->getId();
$sen    = Mage::getModel('core/store')->load('sen', 'code')->getId();// Sanoga
$sth    = Mage::getModel('core/store')->load('sth', 'code')->getId();
$moxyen = Mage::getModel('core/store')->load('moxyen', 'code')->getId();// Moxy
$moxyth = Mage::getModel('core/store')->load('moxyth', 'code')->getId();


// UPDATE HELP PAGE


//==========================================================================
// Help page (Petloft Thai)
//==========================================================================
$title = "ช่วยเหลือ | วิธีการสั่งซื้อ และคำถามที่พบบ่อย";
$stores = array($th);
$metaDescription = "PetLoft FAQ พร้อมถาม-ตอบปัญหาการใช้งานร้านค้าออนไลน์ของเรา ทั้งปัญหาการใช้งานเว็บไซต์ ปัญหาการสั่งซื้อสินค้า ปัญหาการชำระเงิน หรือปัญหาการจัดส่งสินค้า";
$identifier = "help";
$isActive = 1;
$rootTemplate = "one_column";
$sortOrder = 0;
$contentHeading = "ช่วยเหลือ";
$content = <<<EOD
<div>
<ul id="helpMenu">
<li><a href="javascript:void(0)" data-id="faq">คำถามที่พบบ่อย</a></li>
<li><a href="javascript:void(0)" data-id="howtoorder">วิธีการการสั่งซื้อ</a></li>
<li><a href="javascript:void(0)" data-id="payments">การชำระเงิน</a></li>
<li><a href="javascript:void(0)" data-id="shipping">การจัดส่งสินค้า</a></li>
<li><a href="javascript:void(0)" data-id="rewardpoints">คะแนนสะสม</a></li>
<li><a href="javascript:void(0)" data-id="returns">การคืนสินค้าและขอคืนเงิน</a></li>
</ul>
</div>
<div class="helpContent">
<div id="faq" class="subcontent">
<h2>FAQ</h2>
<ol class="accordion">
<li>
<div class="accord-header">Petloft คือใคร</div>
<div class="accord-content">&ldquo;Petloft&rdquo; เป็นร้านค้าออนไลน์ที่เน้นจำหน่ายผลิตภัณฑ์อาหารสุนัขและอาหารแมวคุณภาพสูงได้รับความนิยมอย่างแพร่หลาย โดยมีระบบที่ได้รับการพัฒนาเป็นอย่างดี ซึ่งจะช่วยให้ลูกค้าที่ต้องการซื้ออาหารให้กับสัตว์เลี้ยงที่รัก สามารถเลือกซื้อสินค้าได้ตลอด 24 ชั่วโมง ไร้ข้อจำกัดทางด้านเวลา ไม่เสียเวลาการเดินทางและไม่ต้องกังวลเกี่ยวกับการขนส่งสินค้าที่มีน้ำหนักมาก บริหารโดยทีมงานซึ่งมีประสบการณ์ทางด้านร้านค้าออนไลน์โดยตรง และมีบริษัทฯ ในเครืออยู่ในภูมิภาคเอเชียตะวันออกเฉียงใต้มากมาย</div>
</li>
<li>
<div class="accord-header">Petloft มีที่มาอย่างไร</div>
<div class="accord-content">ด้วยความรักทีมีต่อสัตว์เลี้ยง บวกกับการเล็งเห็นว่าสัตว์เลี้ยงไม่ว่าจะเป็นสุนัขหรือว่าแมว เป็นเพื่อนคนสำคัญของผู้คนในปัจจุบัน จึงทำให้ทีมงานของ Petloft ซึ่งมีความเชี่ยวชาญและมีประสบการณ์ทางด้านร้านค้าออนไลน์เป็นอย่างมาก ทำการจัดตั้งเว็บไซต์เพื่อจำหน่ายสินค้าเกี่ยวกับสัตว์เลี้ยงโดยเน้นไปที่อาหารสัตว์ เพื่อสร้างความสะดวกสบาย และความคุ้มค่าให้แก่เจ้าของสัตว์เลี้ยงมากยิ่งขึ้น</div>
</li>
<li>
<div class="accord-header">Petloft ตั้งอยู่ที่ไหน</div>
<div class="accord-content">บริษัทตั้งอยู่ที่ อาคารเศรษฐีวรรณ ชั้น 19<br /> 139 ถนนปั้น แขวงสีลม เขตบางรัก&nbsp;<br />กรุงเทพฯ ประเทศไทย 10500<br /> โทรศัพท์ 02-106-8222 <br /> อีเมล์ <a href="mailto:support@petloft.com">support@petloft.com</a></div>
</li>
<li>
<div class="accord-header">สนใจอยากร่วมธุรกิจกับ Petloft ทำอย่างไร</div>
<div class="accord-content">บริษัทยินดีต้อนรับผู้ที่สนใจเข้าเป็นส่วนหนึ่งกับเราได้ โดยท่านที่สนใจนำเสนอสินค้าอาหารสุนัขและแมว คุณภาพสูงผ่านทางหน้าเว็บไซต์ Petloft สามารถติดต่อได้ที่อีเมล์ <a href="mailto:support@petloft.com">support@petloft.com</a></div>
</li>
<li>
<div class="accord-header">โปรโมชั่นจัดส่งสินค้าฟรี</div>
<div class="accord-content">จัดส่งฟรีเมื่อซื้อสินค้าขั้นต่ำ 500 บาทต่อการสั่งซื้อ1ครั้ง เฉพาะเขตกรุงเทพ ในส่วนของเขตปริมณฑล และต่างจังหวัด คิดค่าบริการในการจัดส่ง 100 บาท ในกรณีซื้อสินค้าต่ำกว่า 500 บาทเสียค่าจัดส่ง 100 บาทในเขตกรุงเทพ และ 200 บาทในเขตปริมณฑลและต่างจังหวัด</div>
</li>
<li>
<div class="accord-header">ข้อเสนอพิเศษ</div>
<div class="accord-content">ในหน้าเว็บไซต์ของเรา จะมีข้อเสนอพิเศษต่างๆ รวมทั้งสินค้าที่มีส่วนลด 20% หรือมากกว่า ซึ่งเราได้ทำหน้าเฉพาะสำหรับผลิตภัณฑ์เหล่านี้เพื่อความง่ายในการเลือกซื้อหรือค้นหาสินค้าราคาพิเศษ หรือข้อเสนอพิเศษที่คุณต้องการ</div>
</li>
<li>
<div class="accord-header">วิธีขอรหัสผ่านใหม่</div>
<div class="accord-content">ถ้าคุณลืมรหัสผ่านของคุณ คุณสามารถขอรหัสผ่านใหม่ได้ <a href="{{store url='forgot-password'}}" target="_blank">คลิกที่นี่</a>เพื่อดูรายละเอียดเพิ่มเติม</div>
</li>
<li>
<div class="accord-header">ถ้าสินค้าที่ต้องการหมด จะทำอย่างไร</div>
<div class="accord-content">ถึงแม้ว่าทางเราจะพยายามจัดหาสินค้าให้มีจำนวนเหมาะสมกับระดับความต้องการของลูกค้า แต่สินค้าที่ได้รับความนิยมอย่างมากก็สามารถหมดลงได้อย่างรวดเร็ว แต่เราพยายามไม่ให้ปัญหานี้เกิดขึ้น หรือถ้าเกิดขึ้นก็จะพยายามหาทางแก้ไข และหาทางออกให้กับลูกค้าอย่างแน่นอน ถ้ามีข้อสงสัย หรือปัญหาเกี่ยวกับสินค้าสามารถติดต่อได้ที่ฝ่ายลูกค้าสัมพันธ์ได้ในเวลาทำการ</div>
</li>
<li>
<div class="accord-header">สามารถจองสินค้าไว้ก่อนเพื่อทำการสั่งซื้อในวันถัดไปได้หรือไม่</div>
<div class="accord-content">ต้องขออภัย ทางเราไม่มีนโยบายการจองสินค้าในลักษณะนี้ ยังงัยก็ตามเรามั่นใจว่าสินค้าของเราถูกกว่าท้องตลาด และเป็นเหตุผลที่ทำให้คุณตัดสินใจเลือกซื้อสินค้ากับทางเรา</div>
</li>
<li>
<div class="accord-header">คุณจะทราบได้อย่างไรว่ามีแบรนด์ไหนที่ขายอยู่บนเว็บไซต์ Petloft บ้าง</div>
<div class="accord-content">คุณสามารถค้นหาสินค้าที่ต้องการได้ โดยกดปุ่มค้นหาที่หน้าเว็บไซต์ Petloft ซึ่งอยู่ด้านบนขวาของหน้าเว็บไซต์ หรือคุณสามารถเข้าไปคลิกเลือกแบรนด์อาหารได้โดยเลือกที่ประเภทของสัตว์เลี้ยงว่าเป็นแมว หรือสุนัขเพื่อความสะดวกในการซื้อสินค้าของคุณค่ะ</div>
</li>
<li>
<div class="accord-header">จะทราบได้อย่างไร ว่ามีสินค้าใหม่เข้ามาในเวบไซต์</div>
<div class="accord-content">ทางเราจะทำการอัพเดทเวบไซต์ใหม่ทันที ที่มีสินค้าใหม่เข้ามาในคลังสินค้า ท่านสามารถตรวจสอบสินค้าใหม่ได้ที่เวบไซต์ของเรา</div>
</li>
<li class="last">
<div class="accord-header">ฉันสามารถแสดงความคิดเห็น หรือให้ข้อแนะนำเกี่ยวกับสินค้าได้อย่างไร</div>
<div class="accord-content">เรายินดีที่รับฟังความคิดเห็น และข้อเสนอแนะเกี่ยวกับผลิตภัณฑ์ของเรา คุณสามารถแสดงความคิดเห็นของคุณมาผ่านมางทางอีเมล์ <a href="mailto:support@petloft.com">support@petloft.com</a></div>
</li>
</ol></div>
<div id="howtoorder" class="subcontent">
<h2>วิธีการการสั่งซื้อ</h2>
<ol class="accordion">
<li>
<div class="accord-header">สั่งซื้อสินค้าผ่าน Petloft อย่างไร?</div>
<div class="accord-content">คุณสามารถสั่งซื้อสินค้ากับ Petloft.com ด้วยวิธีง่ายๆ <a href="{{store url='how-to-order'}}" target="_blank">คลิกที่นี่</a>เพื่อดูขั้นตอนการสั่งซื้อ</div>
</li>
<li>
<div class="accord-header">สมัครเพื่อลงทะเบียนบัญชีผู้ใช้ใหม่อย่างไร?</div>
<div class="accord-content">เพียงคลิกไปที่ &ldquo;ลงทะเบียน&rdquo; ซึ่งอยู่ด้านบนขวามือของเว็บไซต์ หลังจากนั้นจะเปลี่ยนไปหน้าสร้างบัญชีผู้ใช้ใหม่ซึ่งจะอยู่ทางด้านซ้ายมือ หลังจากกรอกรายละเอียดข้อมูลของท่านเสร็จเรียบร้อยแล้วคลิกที่ &ldquo;ยืนยัน&rdquo; เพื่อเสร็จสิ้นการสมัครสมาชิก หลังจากนั้นคุณจะได้รับอีเมล์ยืนยันการลงทะเบียนผู้ใช้ใหม่กับทาง Petloft.com เพียงเท่านี้คุณก็สามารถซื้อสินค้าผ่านทางเราได้อย่างสะดวกสบาย</div>
</li>
<li>
<div class="accord-header">มีค่าธรรมเนียมในการสมัครสมาชิกกับ Petloft หรือไม่</div>
<div class="accord-content">การสมัครสมาชิกไม่มีค่าธรรมเนียมใด ๆ ทั้งสิ้น และจะเกิดค่าใช้จ่ายในกรณีที่มีการสั่งซื้อสินค้าเท่านั้น</div>
</li>
<li>
<div class="accord-header">ถ้าฉันต้องการที่ปรึกษาส่วนตัวสำหรับการสั่งซื้อสามารถติดต่อได้ทางใด</div>
<div class="accord-content">แผนกลูกค้าสัมพันธ์ของเรายินดีเป็นอย่างยิ่งที่จะช่วยเหลือท่านผ่านทางอีเมล์ <a href="mailto:support@petloft.com">support@petloft.com</a> หรือทางหมายเลยโทรศัพท์ 02-106-8222</div>
</li>
<li class="last">
<div class="accord-header">ฉันสามารถสั่งซื้อสินค้าผ่านทางโทรศัพท์ได้หรือไม่?</div>
<div class="accord-content">คุณสามารถสั่งซื้อสินค้าผ่านทางโทรศัพท์ได้ค่ะ เพียงโทรเข้าไปที่แผนกลูกค้าสัมพันธ์ตามหมายเลขที่แจ้งไว้ และต้องชำระค่าสินค้าด้วยวิธีเก็บเงินปลายทางเท่านั้น คลิกที่นี่เพื่อดูรายละเอียดเพิ่มเติม</div>
</li>
</ol></div>
<div id="payments" class="subcontent">
<h2>การชำระเงิน</h2>
<ol class="accordion">
<li>
<div class="accord-header">การใช้บัตรเครดิตกับทาง Petloft ปลอดภัยหรือไม่</div>
<div class="accord-content">บริษัทดูแลข้อมูลของสมาชิก Petloft เป็นอย่างดี ข้อมูลทุกอย่างจะถูกเก็บไว้เป็นความลับและดูแลรักษาอย่างปลอดภัย ด้านข้อมูลเกี่ยวกับบัตรเครดิตทั้งหมดจะถูกส่งผ่านระบบ SSL (Secure Socket Layer) ซึ่งเป็นระบบรักษาความปลอดภัยมาตรฐานในการส่งข้อมูลทางอินตเตอร์เน็ตตรงไปยังระบบเซฟอิเล็กทรอนิกส์ของธนาคาร โดยจะไม่มีการเก็บข้อมูลเกี่ยวกับบัตรเครดิตไว้ที่ระบบเซิฟเวอร์ของเว็บไซต์แต่อย่างใด</div>
</li>
<li>
<div class="accord-header">ข้อมูลบัตรเครดิตไม่สามารถใช้ได้ เกิดจากอะไร</div>
<div class="accord-content">กรุณาตรวจสอบสถานะการเงินของท่านจากทางธนาคารหากยังไม่สามารถใช้ได้กรุณาติดต่อแผนกลูกค้าสัมพันธ์ของธนาคารเพื่อแจ้งกับแผนกเทคโนโลยีสารสนเทศเพื่อแก้ไขปัญหาด้านเทคนิค</div>
</li>
<li>
<div class="accord-header">หากเครื่องคอมพิวเตอร์ขัดข้องระหว่างขั้นตอนการชำระเงิน จะทราบได้อย่างไรว่าการชำระเงินเสร็จสิ้นเรียบร้อยแล้ว</div>
<div class="accord-content">เมื่อท่านชำระเงินเรียบร้อยแล้วคำยืนยันการสั่งซื้อจะถูกส่งไปยังอีเมล์ของท่านหากท่านไม่ได้รับการยืนยันการสั่งซื้อทางอีเมล์กรุณาลองทำการสั่งซื้ออีกครั้ง หากท่านมีข้อสงสัยกรุณาติดต่อแผนกลูกค้าสัมพันธ์เพื่อสอบถามสถานะการสั่งซื้อของท่านที่ อีเมล์ <a href="mailto:support@petloft.com">support@petloft.com</a> หรือทางโทรศัพท์หมายเลข 02-106-8222</div>
</li>
<li>
<div class="accord-header">วิธีการชำระเงินมีกี่ช่องทาง</div>
<div class="accord-content">
บริษัทมีช่องทางให้ลูกค้าเลือกชำระเงินหลายช่องทาง ตามด้านล่างนี้
<ul style="margin-left: 30px; list-style: square !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">บัตรเครดิต วีซ่า มาสเตอร์ ซึ่งผ่าน่ระบบ Payment ของบริษัท 2C2P</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">PayPal ช่วยให้คุณสามารถชำระค่าสินค้าของคุณหรือชำระด้วยบัตรเครดิตชั้นนำต่างๆ ผ่านทางหน้าเว็บไซด์ได้ทันที โดยจะทำรายการผ่านระบบของ PayPal ทำให้การชำระเงินเป็นไปได้อย่างสะดวก รวดเร็วและปลอดภัย หากท่านยังไม่มีบัญชีกับ Paypal สามารถ คลิกที่นี่เพื่อสมัครบริการ Paypal</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Cash on delivery เป็นการเก็บเงินปลายทางจากผู้รับสินค้า โดยจะมีค่าธรรมเนียมในการเก็บเงินปลายทาง 50 บาท ต่อการสั่งซื้อต่อครั้ง ค่าธรรมเนียมนี้ไม่รวมค่าสินค้า และค่าจัดส่งสินค้า</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">บริการผ่อนชำระสินค้า</li>
</ul><br/>
เมื่อซื้อสินค้าครบ 3,500 บาท สามารถผ่อนชำระได้นานสูงสุด 10 เดือนกับบัตรเครดิตที่ร่วมรายการ<br/><br/>
บัครเครดิตธนาคารกรุงเทพ<br/><br/>
<ul style="margin-left: 30px; list-style: disc !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">แบ่งชำระ 4 เดือน ดอกเบี้ย 0.8%</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">แบ่งชำระ 6 เดือน ดอกเบี้ย 0.8%</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">แบ่งชำระ 10 เดือน ดอกเบี้ย 0.8%</li>
</ul>
<br/>
บัตรเครดิตซิตี้แบงค์<br/><br/>
<ul style="margin-left: 30px; list-style: disc !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">แบ่งชำระ 4 เดือน ดอกเบี้ย 0.89%</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">แบ่งชำระ 6 เดือน ดอกเบี้ย 0.89%</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">แบ่งชำระ 10 เดือน ดอกเบี้ย 0.89%</li>
</ul>
<br/>
บัตรเครดิต KTC<br/><br/>
<ul style="margin-left: 30px; list-style: disc !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">แบ่งชำระ 4 เดือน ดอกเบี้ย 0.89%</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">แบ่งชำระ 6 เดือน ดอกเบี้ย 0.89%</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">แบ่งชำระ 10 เดือน ดอกเบี้ย 0.89%</li>
</ul>
<br/>
บัตรเครดิตธนาคารธนชาติ<br/><br/>
<ul style="margin-left: 30px; list-style: disc !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">แบ่งชำระ 4 เดือน ดอกเบี้ย 0.8%</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">แบ่งชำระ 6 เดือน ดอกเบี้ย 0.8%</li>
</ul>
<br/>
* หมายเหตุ<br/>
ยอดสั่งซื้อขั้นต่ำ 3,500 บาทต่อคำสั่งซื้อ โดยคำนวนราคาก่อนหักส่วนลดและไม่รวมค่าขนส่ง<br/>
อัตราดอกเบี้ยเป็นไปตามที่ธนาคารผู้ออกบัตรกำหนด<br/>
สงวนสิทธิ์การคืนเงินเในทุกกรณี<br/>
เงื่อนไขเป็นไปตามข้อกำหนด บริษัทขอสงวนสิทธิ์การเปลี่ยนแปลงโดยไม่แจ้งให้ทราบล่วงหน้า<br/>
</div>
</li>
<li>
<div class="accord-header">ราคาสินค้าใช้สกุลเงินไทยหรือไม่</div>
<div class="accord-content">ราคาสินค้าทุกชิ้นของเราใช้หน่วยเงินบาทไทย</div>
</li>
<li class="last">
<div class="accord-header">ราคาสินค้ารวมภาษีมูลค่าเพิ่มหรือยังค่ะ</div>
<div class="accord-content">ราคาสินค้าของเราได้รวมภาษีมูลค่าเพิ่มแล้ว</div>
</li>
</ol></div>
<div id="shipping" class="subcontent">
<h2>การจัดส่งสินค้า</h2>
<ol class="accordion">
<li>
<div class="accord-header">บริษัททำการจัดส่งสินค้าเมื่อไหร่</div>
<div class="accord-content">หากสินค้าที่คุณสั่งซื้อมีพร้อมในคลังของเรา บริษัทจะทำการจัดส่งสินค้าออกจากคลังสินค้าภายใน 24 ชั่วโมงหลังจากคำสั่งซื้อสินค้าสมบูรณ์เรียบร้อยแล้ว หรือในเวลาที่ไม่ช้าหรือเร็วไปกว่านั้น (เฉพาะในวันทำการ) สินค้าของ Petloft ทุกรายการจัดส่งโดยระบบที่มีประสิทธิภาพสูง (Premium Courier) ซึ่งจะดำเนินการจัดส่งสินค้าในช่วงเวลา 9.00 น. &ndash; 19.00 น. และในปัจจุบันเราให้บริการจัดส่งสินค้าเฉพาะวันจันทร์ &ndash; วันเสาร์เท่านั้น</div>
</li>
<li>
<div class="accord-header">ระยะเวลาการจัดส่งสินค้านานเท่าไหร่</div>
<div class="accord-content">การจัดส่งสินค้าดำเนินการด้วยระบบของบริษัท ร่วมกับผู้ให้บริการจัดส่งสินค้าอื่น ๆ ซึ่งมีประสิทธิภาพสูง (Premium Courier) เพื่อให้เราแน่ใจได้ว่าสินค้าจะจัดส่งถึงมือคุณอย่างรวดเร็วที่สุด หากคุณสั่งซื้อสินค้าภายในเวลา 16.00 น. สินค้าถึงมือคุณไม่เกิน 2-3 วันทำการเฉพาะในเขตกรุงเทพ และ 3-5 วันทำการ ในเขตปริมณฑล และต่างจังหวัด ทั้งนี้บริษัทไม่อาจรับประกันได้ว่าหลังจากคุณสั่งซื้อสินค้าแล้วจะได้รับสินค้าภายในวันถัดไปเลยทันที แม้ว่าการจัดส่งสินค้าไปยังหลายพื้นที่ลูกค้าจะสามารถรับสินค้าได้ในวันรุ่งขึ้นก็ตาม และในบางพื้นที่อาจจะใช้ระยะเวลาการจัดส่งที่นานกว่านั้นก็เป็นได้ <br /><br /> *เฉพาะ ปัตตานี ยะลา นราธิวาส แม่ฮ่องสอน เชียงราย อำเภอสังขระบุรี และบางพื้นที่ที่เป็นเกาะ อาจใช้เวลาในการจัดส่งมากกว่าปกติ 1-2 วันทำการ อย่างไรก็ตามทีมงานของเราจะจัดส่งถึงมือคุณอย่างเร็วที่สุด</div>
</li>
<li>
<div class="accord-header">วิธีการตรวจสอบและติดตามสถานะการจัดส่งสินค้า</div>
<div class="accord-content">เมื่อเราทำการจัดส่งสินค้า คุณจะได้รับอีเมล์เพื่อยืนยันการจัดส่งสินค้าและแจ้งหมายเลขการจัดส่งสินค้า คุณสามารถใช้หมายเลขการจัดส่งสินค้าเพื่อตรวจสอบสถานะการจัดส่งได้ที่เว็บไซต์ของเรา (ตรวจสอบสถานะการจัดส่ง) หรือสามารถล็อกอินเพื่อดูสถานะการจัดส่งสินค้าได้ที่หน้าใบสั่งซื้อของคุณ โดยคลิกที่ปุ่ม &ldquo;สถานะสินค้า&rdquo; ในกรณีที่สถานะการจัดส่งสินค้าขึ้นว่าได้ &ldquo;ส่งมอบสินค้าแล้ว&rdquo; และคุณยังไม่ได้รับสินค้ากรุณาตรวจสอบกับเพื่อนบ้านหรือผู้ดูแลอาคารก่อนเบื้องต้น และหากสถานะการจัดส่งสินค้าขึ้นว่า &ldquo;ส่งคืนบริษัท&rdquo; กรุณาติดต่อเราทางอีเมล์หรือโทรศัพท์ทันที เพราะอาจจะเกิดกรณีที่อยู่จัดส่งสินค้าไม่ถูกต้อง, เกิดเหตุสุดวิสัยที่ทำให้พนักงานไม่สามารถจัดส่งสินค้าได้ หรือสินค้าสูญหายระหว่างการจัดส่ง</div>
</li>
<li>
<div class="accord-header">กรณีที่พนักงานไปจัดส่งสินค้าแล้วไม่มีผู้รอรับสินค้าที่บ้าน</div>
<div class="accord-content">การจัดส่งสินค้าเมื่อไม่มีผู้รอรับสินค้าอยู่ที่บ้าน ขึ้นอยู่กับสถานการณ์และดุลยพินิจของพนักงานจัดส่งสินค้าว่าสามารถจะทำการวางสินค้าไว้ที่หน้าบ้านได้หรือไม่ ในกรณีที่พนักงานจัดส่งสินค้าเห็นว่าจำเป็นจะต้องมีผู้ลงชื่อรับสินค้า พนักงานจัดส่งสินค้าจะฝากสินค้าไว้ที่ผู้จัดการอาคารหรือพนักงานเฝ้าประตู โดยจะพิจารณาเลือกวิธีการที่ดีที่สุดอย่างใดอย่างหนึ่ง แต่หากไม่สามารถทำการจัดส่งสินค้าได้ สินค้าจะถูกตีคืนมาที่บริษัทซึ่งจะขึ้นสถานะการจัดส่งสินค้าว่า &ldquo;ไม่สามารถจัดส่ง&rdquo; คุณสามารถทำเรื่องขอคืนเงินค่าสินค้าเต็มจำนวน แต่ไม่รวมค่าจัดส่งสินค้าได้ (อ่านรายละเอียดได้ที่ <a href="{{store url='return-policy'}}" target="_blank">นโยบายการคืนเงิน</a> ) ในกรณีที่คำสั่งซื้อถูกปฏิเสธอย่างไม่ถูกต้อง ทางบริษัทจะไม่ทำการจัดส่งสินค้ากลับไปอีกครั้ง คุณจะต้องดำเนินการสั่งซื้อใหม่เพื่อที่จะได้รับสินค้าที่คุณต้องการ</div>
</li>
<li>
<div class="accord-header">สามารถแจ้งส่งสินค้าไปยังที่สถานที่อื่นๆ ของคุณได้หรือไม่</div>
<div class="accord-content">แน่นอน เราทำการจัดส่งสินค้าในวันจันทร์- ศุกร์ เวลา 09.00 น. &ndash; 19.00 น. และวันเสาร์ เวลา 09.00 น. &ndash; 12.00 น. หากต้องการให้เราจัดส่งสินค้าไปยังที่ทำงานของคุณ สามารถกรอกที่อยู่ที่ต้องการให้เราจัดส่งในช่องสถานที่จัดส่งสินค้า หากเกิดเหตุสุดวิสัยที่คุณไม่สามารถอยู่รอรับสินค้าได้ด้วยตนเอง กรุณาตรวจสอบสินค้ากับทางพนักงานต้อนรับ, ผู้จัดการอาคาร, พนักงานเฝ้าประตูก่อนเบื้องต้น และเพื่อป้องกันความผิดพลาดในการนัดหมายรับสินค้า กรุณารอรับการติดต่อทางโทรศัพท์จากพนักงานของเราด้วย</div>
</li>
<li>
<div class="accord-header">ค่าบริการจัดส่งสินค้า</div>
<div class="accord-content">จัดส่งฟรีเมื่อซื้อสินค้าขั้นต่ำ 500 บาทต่อการสั่งซื้อ1ครั้ง เฉพาะเขตกรุงเทพ ในส่วนของเขตปริมณฑล และต่างจังหวัด คิดค่าบริการในการจัดส่ง 100 บาท ในกรณีซื้อสินค้าต่ำกว่า 500 บาทเสียค่าจัดส่ง 100 บาทในเขตกรุงเทพ และ 200 บาทในเขตปริมณฑลและต่างจังหวัด</div>
</li>
<li>
<div class="accord-header">การจัดส่งสินค้าไปยังต่างประเทศ</div>
<div class="accord-content">ขออภัย...ณ ตอนนี้บริษัทยังไม่มีบริการจัดส่งสินค้าไปยังต่างประเทศ แต่ในอนาคตเรามีแพลนที่จะเปิดร้านค้าออนไลน์ที่ขายอาหารสัตว์ทั่วภาคพื้นเอเชียตะวันออกเฉียงใต้</div>
</li>
<li class="last">
<div class="accord-header">ทำไมการจัดส่งสินค้าถึงล่าช้า</div>
<div class="accord-content">หากทำการสั่งซื้อสินค้าภายในเวลา 16.00 น. ของวันทำการปกติ คุณจะได้รับสินค้าหลังจากคำสั่งซื้อสมบูรณ์แล้วภายใน 2-3 วันทำการ ซึ่งเราจะพยายามจัดส่งสินค้าอย่างเร็วที่สุดเท่าที่จะเป็นไปได้ แต่ก็อาจมีบางกรณีที่จะทำให้คุณได้รับสินค้าล่าช้ากว่ากำหนด<br />สาเหตุที่อาจจะทำให้การจัดส่งสินค้าล่าช้า :
<ul style="margin-left: 30px; list-style: square !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">ข้อมูลสถานที่จัดส่งสินค้าไม่ถูกต้อง</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">ข้อมูลเบอร์โทรศัพท์ของลูกค้าไม่ถูกต้อง</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">ไม่สามารถติดต่อลูกค้าตามเบอร์โทรศัพท์ที่ให้ไว้ได้</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">ไม่มีคนอยู่รับสินค้าตามสถานที่ได้นัดหมายเอาไว้</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">แจ้งสถานที่จัดส่งสินค้าไม่เหมือนกับที่อยู่ตามใบสั่งซื้อ</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">ลูกค้าชำระเงินล่าช้าหรือมีข้อผิดพลาดในขั้นตอนการชำระเงิน</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">คำสั่งซื้อที่จำเป็นต้องหยุดชะงักหรือล่าช้า อันเนื่องมาจากการสั่งซื้อสินค้าในรายการใดรายการหนึ่งเป็นจำนวนมากทำให้สินค้าในคลังของเราไม่เพียงพอต่อการจำหน่าย (เป็นกรณีที่เกิดขึ้นไม่บ่อยนัก)</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">เหตุสุดวิสัยที่ทำให้การจัดส่งสินค้าไม่เป็นไปตามกำหนด (เช่น ภัยธรรมชาติหรือสภาพอากาศที่ผิดปกติ เป็นต้น)</li>
</ul>
</div>
</li>
</ol></div>
<div id="rewardpoints" class="subcontent">
<h2>คะแนนสะสม</h2>
<ol class="accordion">
<li>
<div class="accord-header">รับเงินคืน 1% เมื่อช็อปปิ้งกับ Petloft</div>
<div class="accord-content">เมื่อท่านได้สั่งซื้อของกับทาง Petloft ท่านจะได้รับเงินคืนทันที 1 % โดยเงินที่ได้จะถูกเก็บไว้ที่บัญชี Petloft ของท่าน ท่านสามารถนำมาใช้ได้ในครั้งต่อไปเมื่อท่านสั่งซื้อของผ่านทาง Petloft โดยสามารถใช้แทนเงินสด ทุกครั้งที่ท่านต้องการใช้ ท่านสารมารถกรอกจำนวนเงินในช่อง ที่หน้าสั่งซื้อสินค้า ก่อนขั้นตอนสุดท้าย</div>
</li>
<li>
<div class="accord-header">คุณจะได้รับแต้มจากการแนะนำเว็บไซต์ให้กับเพื่อนๆ ได้อย่างไร</div>
<div class="accord-content">มีหลากหลายช่องทางที่คุณจะได้รับเครดิต 100 บาท จากการแนะนำเว็บไซต์ Petloft.com ให้กับเพื่อนๆ <a href="{{store url='rewardpoints/index/referral'}}" target="_blank">อ่านรายละเอียดเพิ่มเติมที่นี่</a>
<ul style="margin-left: 30px; list-style: square !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">คุณสามารถนำลิงค์จากในหน้าบัญชีผู้ใช้ของคุณในเว็บไซต์ Petloft.com แชร์ผ่านเฟสบุ๊ค ทวิสเตอร์ หรือส่งลิงค์ให้กับเพื่อนๆ ผ่านทางอีเมล</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">เมื่อเพื่อนของคุณคลิกลิงค์ดังกล่าวและซื้อสินค้าบนเว็บไซต์ Petloft.com คุณจะได้รับทันทีเครดิต 100 บาทในบัญชีของคุณ เครดิตดังกล่าวสามารถใช้แทนเงินสดหรือใช้เป็นส่วนลดในการซื้อสินค้าครั้งต่อๆ ไป</li>
</ul>
</div>
</li>
<li class="last">
<div class="accord-header">อายุการใช้งานของคะแนนสะสม (Reward Points)</div>
<div class="accord-content">เมื่อคุณซื้อสินค้ากับทางเว็บไซต์ของเราคุณจะได้รับคะแนนสะสม (Reward Points) โดยคะแนนสะสมที่คุณได้รับจากการซื้อสินค้าแต่ละครั้งนั้นจะนำมารวมกันเมื่อคุณซื้อสินค้าในครั้งต่อไปเพิ่มขึ้นเรื่อยๆ แต่ถ้าคุณเก็บคะแนนสะสมไว้เป็นเวลานาน คะแนนบางส่วนของคุณอาจหายไปได้ เนื่องจากอายุการใช้งานของคะแนนสะสมจะมีระยะเวลาไม่เท่ากัน กล่าวคือ คะแนนที่คุณได้รับหลังจากการซื้อสินค้าแต่ละครั้งจะมีอายุการใช้งานนาน 6 เดือน <br /><br /> เช่น คุณซื้อสินค้าและได้คะแนนสะสมครั้งแรกในเดือนมกราคม จำนวน 20 คะแนน ต่อมาคุณซื้อสินค้าและได้คะแนนสะสมอีกครั้งในเดือนมีนาคม จำนวน 30 คะแนน คุณจะมีคะแนนสะสมรวมเป็น 50 คะแนน แต่เมื่อถึงเดือนกรกฎาคม คะแนนของคุณจะเหลือเพียง 30 คะแนน (เนื่องจาก 20 คะแนนที่ได้จากเดือนมกราคมหมดอายุในเดือนกรกฎาคม) และถ้าคุณยังไม่ได้ใช้คะแนนที่เหลืออีกภายในเดือนกันยายนคะแนนสะสมของคุณจะกลายเป็น 0 ทันที (เนื่องจาก 30 คะแนนที่ได้จากเดือนมีนาคมหมดอายุในเดือนกันยายน) <br /><br /> ดังนั้นเพื่อเป็นการรักษาสิทธิและผลประโยชน์ของคุณ จึงควรนำคะแนนสะสมมาใช้ภายในระยะเวลาที่กำหนด</div>
</li>
</ol></div>
<div id="returns" class="subcontent">
<h2>การคืนสินค้าและขอคืนเงิน</h2>
<ol class="accordion">
<li>
<div class="accord-header">สามารถขอคืนสินค้าได้อย่างไร</div>
<div class="accord-content"><strong>บริการคืนสินค้าอย่างง่ายดาย รวดเร็ว และไม่มีค่าใช้จ่าย!</strong><br /> กรณีที่คุณต้องการคืนสินค้า กรุณาติดต่อแผนกลูกค้าสัมพันธ์และแจ้งความประสงค์ในการขอคืนสินค้าผ่านทางโทรศัพท์ 02-106-8222 หรือ อีเมล์ <a href="mailto:support@petloft.com">support@petloft.com</a> หากเงื่อนไขการขอคืนสินค้าของคุณเป็นไปตามนโยบายการขอคืนสินค้าแล้ว กรุณากรอกรายละเอียดลงในแบบฟอร์มการขอคืนสินค้าที่คุณได้รับไปพร้อมกับสินค้าเมื่อตอนจัดส่ง และแนบใบกำกับภาษี (invoice) ใส่ลงในกล่องพัสดุของ Petloft.com กลับมาพร้อมกันด้วย โดยจัดส่งสินค้ากลับมาให้ทางเรา ในแบบพัสดุลงทะเบียนผ่านทางที่ทำการไปรษณีย์ไทยใกล้บ้านคุณ <br /><br /> กรุณาจัดส่งสินค้ามาที่ :<br /> บริษัท เอคอมเมิร์ซ จำกัด<br /> 8/2 ถนนพระราม3 ซอย53<br /> แขวงบางโพงพาง เขตยานนาวา <br />กรุงเทพฯ 10120 <br /><br /> ทาง Petloft.com จะชดเชยค่าจัดส่งแบบลงทะเบียนพร้อมกับการคืนเงินค่าสินค้า โดยทางเราขอให้คุณเก็บใบเสร็จรับเงิน/slip ค่าจัดส่งที่ได้รับจากที่ทำการไปรษณีย์เพื่อเป็นหลักฐานในการขอคืนเงินค่าส่งสินค้ากลับไว้ด้วย กรุณาส่งใบเสร็จรับเงิน/slip นี้กลับมาทางแผนกลูกค้าสัมพันธ์ โดยส่งเป็นไฟล์รูปภาพกลับมาทางอีเมล์ <a href="mailto:support@petloft.com">support@petloft.com</a> <br /><br /> หมายเหตุ : ทาง petloft.com ต้องขออภัยในความไม่สะดวกที่เรายังไม่มีบริการในการเข้าไปรับสินค้าที่ลูกค้าต้องการส่งคืน</div>
</li>
<li>
<div class="accord-header">การแพ็คสินค้าเพื่อส่งคืนบริษัท</div>
<div class="accord-content">เราแนะนำให้คุณทำการแพ็คสินค้าคืนด้วยกล่องหรือบรรจุภัณฑ์ลักษณะเดียวกับที่เราทำการจัดส่งไปให้ เว้นแต่ว่ามันไม่ได้อยู่ในสภาพที่ดีพอสำหรับการนำมาใช้ใหม่ อย่าลืม! ที่จะแนบป้าย/ฉลากขอคืนสินค้า (return label) กลับมาด้วย จากนั้นใช้ป้ายชื่อ/ที่อยู่ในการจัดส่งสินค้า (Shipping label) ที่ได้รับจากพนักงานหรือจากเว็บไซต์ของเราเพื่อทำการจัดส่งสินค้าคืนบริษัทได้ที่ทำการไปรษณีย์ทุกแห่งทั่วประเทศไทย <br /><br /> หมายเหตุ : การจัดส่งสินค้ากลับคืนนั้น Petloft ไม่ได้เป็นผู้กำหนดระยะเวลาในการจัดส่งสินค้า จึงไม่สามารถบอกได้ว่าสินค้าที่ส่งคืนนั้นจะมาถึงบริษัทเมื่อไหร่ <br /><br /> หากมีข้อสงสัยกรุณาติดต่อสอบถามเราได้ที่ 02-106-8222 หรือ อีเมล์ <a href="mailto:support@petloft.com">support@petloft.com</a></div>
</li>
<li>
<div class="accord-header">ใช้ระยะเวลาดำเนินการคืนสินค้านานเท่าไหร่</div>
<div class="accord-content">เราจะใช้เวลาดำเนินการภายใน 14 วันทำการ นับตั้งแต่วันที่ได้รับสินค้าคืนมาเรียบร้อยแล้ว</div>
</li>
<li>
<div class="accord-header">เมื่อไหร่ถึงจะได้รับเงินค่าสินค้าคืน</div>
<div class="accord-content">เมื่อบริษัทได้รับสินค้าที่ส่งคืนเรียบร้อยแล้ว ขั้นตอนการคืนเงินจะเกิดขึ้นโดยทันที เราจะดำเนินการคืนเงินค่าสินค้ากลับไปยังบัญชีเงินฝากของคุณ กรุณารอและตรวจสอบยอดเงินหลังจากที่เราได้รับสินค้าคืนจากคุณภายใน 7 วันทำการ</div>
</li>
<li>
<div class="accord-header">ค่าใช้จ่ายในการจัดส่งสินค้าคืนบริษัท</div>
<div class="accord-content">ค่าใช้จ่ายในการจัดส่งสินค้าจะแตกต่างกันไปตามสถานที่และวิธีการจัดส่งที่คุณเลือก กรุณาตรวจสอบให้แน่ใจว่าคุณได้ใช้ระบบจัดส่งแบบลงทะเบียนซึ่งดำเนินการโดยไปรษณีย์ไทย และเราจะคืนเงินค่าจัดส่งหลังจากที่ได้รับสินค้าคืนและยืนยันว่าได้รับสินค้าคืนเรียบร้อยแล้ว</div>
</li>
<li class="last">
<div class="accord-header">กรณีที่การจัดส่งสินค้าเกิดข้อผิดพลาด</div>
<div class="accord-content">แผนกควบคุมคุณภาพของเราทำงานอย่างหนักเพื่อให้สินค้าทุกชิ้นอยู่ในสภาพที่ดีและมีคุณภาพสูงสุดเมื่อทำการจัดส่งออกจากคลังสินค้า แต่ก็มีในบางกรณีซึ่งเป็นไปได้ยากที่สินค้าของคุณจะเกิดตำหนิ,เสียหาย หรือมีข้อผิดพลาด ดังนั้นหากเกิดกรณีดังกล่าวแล้วคุณสามารถดำเนินการขอคืนสินค้าและเงินค่าสินค้าเต็มจำนวน ภายในระยะเวลา 30 วันหลังจากที่คุณได้รับสินค้าเรียบร้อยแล้ว</div>
</li>
</ol></div>
</div>
<script type="text/javascript">// <![CDATA[
 jQuery(document).ready(function()
 {
  var tmpUrl = "";
  var h=window.location.hash.substring(0);
  if(h == ""){
   //alert('Hash not found!!!');
   h = "#faq";
   //alert(heart);
   jQuery( "#helpMenu li:first-child" ).addClass( "helpactive");
   jQuery('.subcontent[id='+h.substring(1)+']').fadeIn();
  }else{
   jQuery('#helpMenu li a[data-id='+h.substring(1)+']').parent().addClass( "helpactive");
  jQuery('.subcontent[id='+h.substring(1)+']').fadeIn();
  }

  //////////////////////////////
  jQuery('#keyMess li').on('click','a',function()  {
   tmpUrl=jQuery(this).attr('href');
   h=tmpUrl.substring(tmpUrl.indexOf("#")+1);

   jQuery('.subcontent:visible').fadeOut(0);
   jQuery('.subcontent[id='+h+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
  });
  jQuery('#zyncNav li').on('click','a',function()
  {
   tmpUrl=jQuery(this).attr('href');
   h=tmpUrl.substring(tmpUrl.indexOf("#")+1);
   jQuery('.subcontent:visible').fadeOut(0);
   jQuery('.subcontent[id='+h+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
  });
  jQuery('#footerBanner div').on('click','a',function()
  {
   tmpUrl=jQuery(this).attr('href');
   h=tmpUrl.substring(tmpUrl.indexOf("#")+1);
   jQuery('.subcontent:visible').fadeOut(0);
   jQuery('.subcontent[id='+h+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
  });
  jQuery('#topMklink').on('click','a',function()
  {
   tmpUrl=jQuery(this).attr('href');
   h=tmpUrl.substring(tmpUrl.indexOf("#")+1);
   jQuery('.subcontent:visible').fadeOut(0);
   jQuery('.subcontent[id='+h+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
  });
  jQuery('#footer_help li').on('click','a',function()
  {
   tmpUrl=jQuery(this).attr('href');
   h=tmpUrl.substring(tmpUrl.indexOf("#")+1);
   jQuery('.subcontent:visible').fadeOut(0);
   jQuery('.subcontent[id='+h+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
  });
 ///////////////////////////////

     jQuery('#helpMenu').on('click','a',function()
     {
         jQuery('.subcontent:visible').fadeOut(0);
         jQuery('.subcontent[id='+jQuery(this).attr('data-id')+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery(this).parent().addClass( "helpactive");
     });

///////////////////////////////

     jQuery(".accordion li .accord-header").mouseenter(function() {
	    	jQuery(this).css("cursor","pointer");
	    });
	    jQuery(".accordion li .accord-header").click(function() {
	    	jQuery(this).find("span").removeClass('active');
	      if(jQuery(this).next("div").is(":visible")){
	        jQuery(this).next("div").slideUp(300);
	      } else {
	        jQuery(".accordion li .accord-content").slideUp(400);
	        jQuery(this).next("div").slideToggle(400);
	        jQuery(".accordion li .accord-header").find("span").removeClass('active');
	        jQuery(this).find("span").addClass('active');
	      }
	    });
	  });
// ]]></script>
EOD;
/** @var Mage_Cms_Model_Page $page */
$page = Mage::getModel('cms/page')->getCollection()
    ->addStoreFilter($stores, $withAdmin = false)
    ->addFieldToFilter('identifier', $identifier)
    ->getFirstItem();
if ($page->getId()) $page->delete(); // if exists then delete
$page = Mage::getModel('cms/page');
$page->setTitle($title);
$page->setIdentifier($identifier);
$page->setStores($stores);
$page->setIsActive($isActive);
$page->setRootTemplate($rootTemplate);
$page->setSortOrder($sortOrder);
$page->setContent($content);
$page->setContentHeading($contentHeading);
$page->setMetaDescription($metaDescription);
$page->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// Help page (Petloft English)
//==========================================================================
$title = "Help with FAQ, How To Order, Payments, Shipping";
$stores = array($en);
$metaDescription = "Help Center at PetLoft.com - Largest selection of pet food in Thailand - Free shipping & delivery";
$identifier = "help";
$isActive = 1;
$rootTemplate = "one_column";
$sortOrder = 0;
$contentHeading = "Help";
$content = <<<EOD
<div>
<ul id="helpMenu">
<li><a href="javascript:void(0)" data-id="faq">FAQ</a></li>
<li><a href="javascript:void(0)" data-id="howtoorder">How To Order</a></li>
<li><a href="javascript:void(0)" data-id="payments">Payments</a></li>
<li><a href="javascript:void(0)" data-id="shipping">Shipping</a></li>
<li><a href="javascript:void(0)" data-id="rewardpoints">Reward Points</a></li>
<li><a href="javascript:void(0)" data-id="returns">Cancellation and Returns</a></li>
</ul>
</div>
<div class="helpContent">
<div id="faq" class="subcontent">
<h2>FAQ</h2>
<ol class="accordion">
<li>
<div class="accord-header">Who is Petloft?</div>
<div class="accord-content">Petloft is an online pet store with a mission to offer what your pets need and deliver it straight to you. We focus on a broad selection of quality products, great logistics, and customer service to impress our local pet community. Our goal is to help you save time from going to the pet store and the burden of carrying heavy pet products back home. At Petloft, we do the heavy lifting for you. Our teams of pet lovers are all dedicated to you and your pet&rsquo;s needs.</div>
</li>
<li>
<div class="accord-header">How did Petloft come about?</div>
<div class="accord-content">Petloft was formed from our love for all types of pets. As pet owners, we want convenience in buying the best quality products. We decided to combine our passion with our vast ecommerce knowledge to offer the local pet community a great service.</div>
</li>
<li>
<div class="accord-header">Where is Petloft based?</div>
<div class="accord-content">We are based at<br /> Sethiwan Tower - 19th Floor <br /> 139 Pan Road, Silom, Bangrak, <br /> Bangkok, 10500 Thailand <br /> Tel: 02-106-8222 <br /> Email <a href="mailto:support@petloft.com">support@petloft.com</a></div>
</li>
<li>
<div class="accord-header">If I were interested in partnering with Petloft, how would I go about it?</div>
<div class="accord-content">The company welcomes any and all business ventures and ideas that you might have that will ultimately add value to our members. To discuss this further, please contact us on <a href="mailto:support@petloft.com">support@petloft.com</a></div>
</li>
<li>
<div class="accord-header">Free Shipping Promotion</div>
<div class="accord-content">Free Delivery anywhere in Bangkok with the minimun purchase of 500 baht for each order
<ul style="margin-left: 30px; list-style: square !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">฿100 flat fee for shipping nationwide exclude Bangkok</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Flat shipping fee of ฿100 will be applied to the order less than ฿500 in Bangkok and ฿200 for nationwide exclude Bangkok</li>
</ul>
</div>
</li>
<li>
<div class="accord-header">Deals Section</div>
<div class="accord-content">The deals section on our site includes all products that have a discount of 20% or greater. We have made a special page for these products so that it will be easier for our customers such as your self to find.</div>
</li>
<li>
<div class="accord-header">I forgot my password</div>
<div class="accord-content">If you have forgotten your password, don&rsquo;t worry. We have systems in place to help you recover them. <a href="{{store url='forgot-password'}}" target="_blank">Click here</a> for more information.</div>
</li>
<li>
<div class="accord-header">The item I want is out of stock. What now?</div>
<div class="accord-content">Although we try to maintain a stock level that matches expected demand, highly popular items may sometimes be out of stock. Please contact our Customer Service team, who will quickly find a solution for you. Happy customers and happy pets are important to us.</div>
</li>
<li>
<div class="accord-header">Can I place an item on hold for purchase at a later date?</div>
<div class="accord-content">Sorry, but we do not offer this option. Our goal is to ensure all customers can immediately purchase our great products.</div>
</li>
<li>
<div class="accord-header">How do I know if you carry a certain brand?</div>
<div class="accord-content">You can use the search function, which is available on the top right of the page or go through our brands list buy clicking on either dog or cat.</div>
</li>
<li>
<div class="accord-header">When can I expect new products to be listed on your website?</div>
<div class="accord-content">We constantly update our products with new lines and new brands. Please visit our website to see the latest items we have in stock.</div>
</li>
<li class="last">
<div class="accord-header">Is there somewhere I can go to view the product prior to purchasing?</div>
<div class="accord-content">Unfortunately, we do not have a retail store and for safety reasons we cannot allow customers in the warehouse. If you have any questions regarding the products, please do not hesitate to contact our Customer Service team.</div>
</li>
</ol></div>
<div id="howtoorder" class="subcontent">
<h2>How To Order</h2>
<ol class="accordion">
<li>
<div class="accord-header">How do I order?</div>
<div class="accord-content">Ordering at Petloft.com is very easy. The steps can be found <a href="{{store url='how-to-order'}}" target="_blank">here</a>.</div>
</li>
<li>
<div class="accord-header">How do I register a new account?</div>
<div class="accord-content">Please click on 'Register', which is located on the top right hand side of the page. You will then be prompted to a new page where you will find 'New Customer' on the left hand side. Fill in your details as requested before clicking 'submit'. Registration should now be complete and you should receive a confirmation e-mail to the address you registered with.</div>
</li>
<li>
<div class="accord-header">Is there any registration fee for Petloft?</div>
<div class="accord-content">Registration at Petloft is completely free and easy! Any form of charge will only come from purchasing of Petloft products.</div>
</li>
<li>
<div class="accord-header">I need personal assistance with my order. Who can I contact?</div>
<div class="accord-content">Our Customer Service team will be happy to assist you. Please e-mail us at <a href="mailto:support@petloft.com">support@petloft.com</a> or call us at 02-106-8222.</div>
</li>
<li class="last">
<div class="accord-header">Do you take orders over the phone?</div>
<div class="accord-content">Yes. We do take orders over phone. The payment mode possible for these orders is Cash On Delivery only. Click here for more details.</div>
</li>
</ol></div>
<div id="payments" class="subcontent">
<h2>Payments</h2>
<ol class="accordion">
<li>
<div class="accord-header">Is my credit card information safe at Petloft?</div>
<div class="accord-content">The company will not store any credit card information on our servers. At Petloft, we use systems set in place by banks that have the security protocols set in place to handle safe credit card transactions.</div>
</li>
<li>
<div class="accord-header">My credit card details are not being accepted. What's wrong?</div>
<div class="accord-content">Please check with your bank or financial institution to rule out errors on their behalf. If problems continue to persist, please contact Customer Service who will notify the IT department of technical difficulties.</div>
</li>
<li>
<div class="accord-header">My computer froze while processing payment. How will I know that my payment went through successfully?</div>
<div class="accord-content">All successful transactions will receive a confirmation email that contains an order tracking number. If you have not received confirmation via email, please try placing your order again. Alternatively, please contact our Customer Service team to confirm the placement of your order.</div>
</li>
<li>
<div class="accord-header">How many payment methods does Petloft have?</div>
<div class="accord-content">
At Petloft, we provide customers the ability to pay using the following facilities:
<ul style="margin-left: 30px; list-style: square !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Credit card and Debit card</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">PayPal</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Cash on delivery (pay when we deliver the product)</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Installment Plan</li>
</ul><br/>
Enjoy up to 10 months installment on a minimum purchase of 3,500฿ with MOXY allied bank<br/><br/>
Bangkok Bank credit card<br/><br/>
<ul style="margin-left: 30px; list-style: disc !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">4-Month plan +0.8%</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">6-Month plan +0.8%</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">10-Month plan +0.8%</li>
</ul>
<br/>
Citibank credit card<br/><br/>
<ul style="margin-left: 30px; list-style: disc !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">4-Month plan +0.89%</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">6-Month plan +0.89%</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">10-Month plan +0.89%</li>
</ul>
<br/>
KTC credit card<br/><br/>
<ul style="margin-left: 30px; list-style: disc !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">4-Month plan +0.89%</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">6-Month plan +0.89%</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">10-Month plan +0.89%</li>
</ul>
<br/>
Thanachart credit card<br/><br/>
<ul style="margin-left: 30px; list-style: disc !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">4-Month plan +0.8%</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">6-Month plan +0.8%</li>
</ul>
<br/>
* condition<br/>
Each installment purchase may only comprise of purchases made under the same invoice<br/>
The minimum purchase does not include discount and shipping fee<br/>
Interest rate is based on the authorized bank<br/>
The installment purchase cannot be refunded<br/>
MOXY reserves the right to amend and cancel orders that without prior notice<br/>
</div>
</li>
<li>
<div class="accord-header">Are your prices in Thai Baht (THB)?</div>
<div class="accord-content">All pricing is in Thai Baht.</div>
</li>
<li class="last">
<div class="accord-header">Does your prices include taxes?</div>
<div class="accord-content">All prices are inclusive of taxes.</div>
</li>
</ol></div>
<div id="shipping" class="subcontent">
<h2>Shipping</h2>
<ol class="accordion">
<li>
<div class="accord-header">When will my order ship?</div>
<div class="accord-content">All items are shipped within 24 hours after the purchase is made and most of the time even sooner (weekdays only). All Petloft deliveries are sent via Premium Courier. The deliveries are made between 9:00 am - 7:00 pm. Currently we deliver from Monday to Saturday.</div>
</li>
<li>
<div class="accord-header">How long will it take until I receive my order?</div>
<div class="accord-content">We ship via our own delivery fleet and several other premium carriers. If you order by 4 pm on a business day, you will receive it in 2-3 business days. We do not yet guaranteed overnight shipping, although many areas of the country will receive their orders the next day. <br /><br /> * For Sangkhlaburi district, islands and provinces including Pattani, Yala, Naradhiwas, Mae Hong Son and Chiang Rai, delivery may take 1-2 days longer due to difficult routes. We&rsquo;ll try our best to deliver the product to you as soon as possible.</div>
</li>
<li>
<div class="accord-header">How do I track my order?</div>
<div class="accord-content">When your order ships, you will receive an email with the shipping and tracking information. You can use your tracking number on our website to trace your order. You can also find your tracking number with your order details; Click the "Order Status" button under the "My Account" menu after you have logged on. <br /><br /> Please check with your neighbors or building manager if you have not received your product even though the tracking says that it has been delivered. If the tracking information doesn't how up or shows that the item has been returned to us, please contact us by email or phone. It is possible that we may have the wrong shipping address, the driver may have unsuccessfully attempted to deliver, or the shipping carrier may have misplaced the product.</div>
</li>
<li>
<div class="accord-header">What happens if I am not at home when my order is being delivered?</div>
<div class="accord-content">In the process of delivering the product to you, our driver will contact you on the number provided to ensure that you are at the address that you have given to us. If you are not home at the time, do inform someone else in your household to beaware of our delivery so they can sign for the product on your behalf. IIf we are still unable to deliver the product to you despite our best efforts, the money paid for the product will be returned to you, but not the shipping fee (For more detail please check our &ldquo;<a href="{{store url='return-policy'}}" target="_blank">refund policy</a>&rdquo;). Undeliverable orders will not be resent and you must place a new order.</div>
</li>
<li>
<div class="accord-header">Can I have my order shipped to my office?</div>
<div class="accord-content">We are happy to delivery to your office; If you would like us to do that, please input your office address at checkout. Your office delivery will be made from the 9am to 7pm on weekdays and 9am to 12 noon on weekends.</div>
</li>
<li>
<div class="accord-header">How much is the shipping fee?</div>
<div class="accord-content">Free Delivery anywhere in Bangkok with the minimun purchase of 500 baht for each order
<ul style="margin-left: 30px; list-style: square !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">฿100 flat fee for shipping nationwide exclude Bangkok</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Flat shipping fee of ฿100 will be applied to the order less than ฿500 in Bangkok and ฿200 for nationwide exclude Bangkok</li>
</ul>
</div>
</li>
<li>
<div class="accord-header">Do you ship outside of Thailand?</div>
<div class="accord-content">Sorry, we currently do not ship outside of Thailand.</div>
</li>
<li class="last">
<div class="accord-header">Why is my shipment delayed?</div>
<div class="accord-content">You must order by 4 p.m. for 1-2-day shipping to apply. We try our best to make sure that you receive your order as soon as possible, however, there are some situations beyond our control that can delay your shipment.<br />The following orders may result in delayed shipping:
<ul style="margin-left: 30px; list-style: square !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Incorrect shipping address</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Incorrect Mobile Phone number</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Unable to reach you at provided contact number</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Being absent after courier made an appointment</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Shipping address does not match billing address</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Payment delay or issue</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Courier failure to deliver (e.g. severe weather conditions)</li>
</ul>
</div>
</li>
</ol></div>
<div id="rewardpoints" class="subcontent">
<h2>Reward Points</h2>
<ol class="accordion">
<li>
<div class="accord-header">1% Cash-back policy</div>
<div class="accord-content">A 1% cash back means that for every purchase made, you will receive 1% cash-back, which will be stored in your Petloft account. You can then use this credit as a discount on your next purchase.</div>
</li>
<li>
<div class="accord-header">How do I get points for referring a friend</div>
<div class="accord-content">There are many ways where you can get 100B by <a href="{{store url='rewardpoints/index/referral'}}" target="_blank">referring a friend</a>at Petloft.com
<ul style="margin-left: 30px; list-style: square !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">You can share your unique link which can be found in My Accounts straight to your Facebook or Twitter page. You can also email this link straight to your friends.</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Once your friends have clicked on the link and have made a purchase on Petloft, you will automatically be credited with 100 Baht in your account in the form of credits.</li>
</ul>
</div>
</li>
<li class="last">
<div class="accord-header">Reward Points Terms &amp; Conditions</div>
<div class="accord-content">Each time you shop on our website, you will earn Reward Points on your purchases. The Reward Points will be accumulated each time you purchase the products on our website and it will be expired within 6 months from your purchasing date. <br /><br /> For example, if you earn 20 points on January from your first purchase and another 30 points on March, your Reward Point balance is 50 points. On July, your balance will be deducted to 30 points because 20 points on January are already expired. In case you have not used your point within September, you point balance will become 0 as your points earned previously on March are expired. <br /><br /> For your own interest, please kindly redeem your Reward Points in the available period of time.</div>
</li>
</ol></div>
<div id="returns" class="subcontent">
<h2>Cancellation and Returns</h2>
<ol class="accordion">
<li>
<div class="accord-header">How do I return my purchase?</div>
<div class="accord-content"><strong>Returns are FREE, fast and easy!</strong><br /> If you wish to return an item, please contact our customer care and request a return of the product(s) via phone 02-106-8222 or Email <a href="mailto:support@petloft.com">support@petloft.com</a>. If the conditions of the returned product aligned within our return policy, please fill in the Returns Slip you received with your shipment, enclose the invoice in the package then send it back to us in its original packaging. Please send the product back to Petloft.com at any Thailand Post location nearby your premise under domestic registered mail service. <br /><br /> NOTE: Sorry for any inconveniences may cause you, return shipments are not eligible for scheduled pick-ups by Petloft.com. <br /><br /> Our address is:<br /> Acommerce Co.,Ltd<br /> 8/2 Rama3 Rd, Soi 53, <br />Bang Phong Pang, Yarn-Nava, <br />Bangkok 10120<br /><br /> Petloft.com will absorb the incurred shipping costs in this registered returns back to you on the refund process. We ask that you keep the receipt/slip of this shipping cost issued by the post office as an evidence. And please send your receipt/slip back to Customer Care by submitting a photo file to the email <a href="mailto:support@petloft.com">support@petloft.com</a></div>
</li>
<li>
<div class="accord-header">Can I cancel my order?</div>
<div class="accord-content">If you wish to cancel your order, get in touch with customer service as soon as possible with your order number. If your order has been dispatched, please do not accept delivery and contact customer service. In case of Prepaid orders Refunds will be processed as per our Return policies. Any cashback credits used in the purchase of the order will be credited back to your account.</div>
</li>
<li>
<div class="accord-header">Packing your return</div>
<div class="accord-content">We suggest that you use the boxes that the products arrived in. Simply apply the shipping label acquired through the online return process or from our Customer Service team. Please put the return label to your box/package and drop it off at any Thailand Post location. NOTE: Return shipments are not eligible for scheduled pick-ups by Petloft. Please contact us with any questions or concerns at 02-106-8222 or send an e-mail to <a href="mailto:support@petloft.com">support@petloft.com</a></div>
</li>
<li>
<div class="accord-header">How long will it take for my return to be processed?</div>
<div class="accord-content">Once we receive the item(s), we will process your request within 14 business days.</div>
</li>
<li>
<div class="accord-header">When do I receive my refund?</div>
<div class="accord-content">Once we receive your item(s) and have checked that it is in original condition, a refund will be initiated immediately. Please allow up to 7 business days to receive your refund from the time we receive your return. Petloft will refund your money through Bank Transfer or in store credit, if preferred.</div>
</li>
<li>
<div class="accord-header">What are the shipping charges if I return my purchased items?</div>
<div class="accord-content">The shipping costs will vary according to your location and method of postage you choose. Please make sure to ship back to us via Thailand post by registered mail and we will refund the shipping fees after receiving the product.</div>
</li>
<li class="last">
<div class="accord-header">What if my item has a defect?</div>
<div class="accord-content">Our Quality Control department tries to ensure that all products are of a high quality when they leave the warehouse. In the rare circumstance that your item has a defect, you may send it back to us with a completed Returns Slip attached within 30 days upon receiving the item(s), a full refund will be issued to you.</div>
</li>
</ol></div>
</div>
<script type="text/javascript">// <![CDATA[
 jQuery(document).ready(function()
 {
  var tmpUrl = "";
  var h=window.location.hash.substring(0);
  if(h == ""){
   //alert('Hash not found!!!');
   h = "#faq";
   //alert(heart);
   jQuery( "#helpMenu li:first-child" ).addClass( "helpactive");
   jQuery('.subcontent[id='+h.substring(1)+']').fadeIn();
  }else{
   jQuery('#helpMenu li a[data-id='+h.substring(1)+']').parent().addClass( "helpactive");
  jQuery('.subcontent[id='+h.substring(1)+']').fadeIn();
  }

  //////////////////////////////
  jQuery('#keyMess li').on('click','a',function()  {
   tmpUrl=jQuery(this).attr('href');
   h=tmpUrl.substring(tmpUrl.indexOf("#")+1);

   jQuery('.subcontent:visible').fadeOut(0);
   jQuery('.subcontent[id='+h+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
  });
  jQuery('#zyncNav li').on('click','a',function()
  {
   tmpUrl=jQuery(this).attr('href');
   h=tmpUrl.substring(tmpUrl.indexOf("#")+1);
   jQuery('.subcontent:visible').fadeOut(0);
   jQuery('.subcontent[id='+h+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
  });
  jQuery('#footerBanner div').on('click','a',function()
  {
   tmpUrl=jQuery(this).attr('href');
   h=tmpUrl.substring(tmpUrl.indexOf("#")+1);
   jQuery('.subcontent:visible').fadeOut(0);
   jQuery('.subcontent[id='+h+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
  });
  jQuery('#topMklink').on('click','a',function()
  {
   tmpUrl=jQuery(this).attr('href');
   h=tmpUrl.substring(tmpUrl.indexOf("#")+1);
   jQuery('.subcontent:visible').fadeOut(0);
   jQuery('.subcontent[id='+h+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
  });
  jQuery('#footer_help li').on('click','a',function()
  {
   tmpUrl=jQuery(this).attr('href');
   h=tmpUrl.substring(tmpUrl.indexOf("#")+1);
   jQuery('.subcontent:visible').fadeOut(0);
   jQuery('.subcontent[id='+h+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
  });
 ///////////////////////////////

     jQuery('#helpMenu').on('click','a',function()
     {
         jQuery('.subcontent:visible').fadeOut(0);
         jQuery('.subcontent[id='+jQuery(this).attr('data-id')+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery(this).parent().addClass( "helpactive");
     });

///////////////////////////////

     jQuery(".accordion li .accord-header").mouseenter(function() {
	    	jQuery(this).css("cursor","pointer");
	    });
	    jQuery(".accordion li .accord-header").click(function() {
	    	jQuery(this).find("span").removeClass('active');
	      if(jQuery(this).next("div").is(":visible")){
	        jQuery(this).next("div").slideUp(300);
	      } else {
	        jQuery(".accordion li .accord-content").slideUp(400);
	        jQuery(this).next("div").slideToggle(400);
	        jQuery(".accordion li .accord-header").find("span").removeClass('active');
	        jQuery(this).find("span").addClass('active');
	      }
	    });
	  });
// ]]></script>
EOD;
/** @var Mage_Cms_Model_Page $page */
$page = Mage::getModel('cms/page')->getCollection()
    ->addStoreFilter($stores, $withAdmin = false)
    ->addFieldToFilter('identifier', $identifier)
    ->getFirstItem();
if ($page->getId()) $page->delete(); // if exists then delete
$page = Mage::getModel('cms/page');
$page->setTitle($title);
$page->setIdentifier($identifier);
$page->setStores($stores);
$page->setIsActive($isActive);
$page->setRootTemplate($rootTemplate);
$page->setSortOrder($sortOrder);
$page->setContent($content);
$page->setContentHeading($contentHeading);
$page->setMetaDescription($metaDescription);
$page->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// Help page (Venbi English)
//==========================================================================
$title = "Help & FAQ";
$stores = array($ven);
$metaDescription = "Help Center at Venbi.com - Largest selection of baby product in Thailand - Free shipping & delivery";
$identifier = "help";
$isActive = 1;
$rootTemplate = "one_column";
$sortOrder = 0;
$contentHeading = "Help";
$content = <<<EOD
<style>
    .faq-topic{border-bottom: 1px dotted #c7c7c7;height: 37px;}
  .faq-mid{background:url('http://cdn.whatsnew.asia/media/wysiwyg/mid-faq-venbi.jpg'); width:930px;height:410px;margin-top:20px;}
  .faq-content{float: left;width: 100%;margin-top: 33px;height: 33px;}
  .pop-hd{background-color:#f07495; padding: 15px;font-size: 18px;color: #FFF;}
  .faq-3key{margin: 20px;float: left;}
  .faq-3key ul li{float: left;width: 280px;font-size: 14px;line-height: 22px;padding-right: 10px;}
  .faq-3key ul li span{color:#f07495;font-weight:bold;}
  .faq-help-hd{width:48%;float:left;margin-top:20px;}
  .faq-help-hd span{background-color:#f07495; padding: 15px;font-size: 18px;color: #FFF;}
  .faq-baby-hd{width:48%;float:left;margin-top:20px;}
  .faq-baby-hd span{background-color:#00b6eb;  padding: 15px;font-size: 18px;color: #FFF;}
  .faq-gray-bg{width:100%;background-color:#f6f6f6; float: left;padding-bottom: 15px;}
  .ol45{width:45%;float:left; margin-top: 15px;}
  .ol48{width:48%;float:left; margin-top: 15px;}
  ul#helpMenu{width: 903px;margin: 0 auto;vertical-align: middle;margin-top: 2px;}
    ul#helpMenu li{float: left;display:list-item;}
    ul#helpMenu li:hover{background:#f07495;color:#ffffff;text-decoration: none;}
    ul#helpMenu li a{display:block;color:#4f78a7;text-decoration: none;width:100%;padding: 10px 35px;}
    ul#helpMenu li a:hover{color:#ffffff;text-decoration: none;}
    div.helpContent div.subcontent{display: none;float: left;width: 100%;margin-top: 20px;}
    div.accord-content{display: none;font-weight: normal;padding: 20px;}
    ol.accordion{list-style: none;width: 95%;margin: 0 auto;}
    ol.accordion li{padding: 10px 0;border-bottom:1px dotted #c7c7c7;font-weight: bold;}
    ol.accordion li.last{border-bottom:none;}
    div.subcontent h2{text-align: center; font-size: 20px; font-weight: bold;color:#f07495;}
    .helpactive{background:#f07495;color:#ffffff;}
    .helpactive a{color:#ffffff !important;}
    div.subcontent h2{text-align: center; font-size: 20px; font-weight: bold;}
    div.subcontent h2{text-align: center; font-size: 20px; font-weight: bold;}
    .helpactive{background:#f07495;color:#FFFFFF;}
    .helpactive a{color:#FFFFFF;}
</style>
<img src="{{media url="wysiwyg/top-faq-venbi.jpg"}}" alt="" usemap="#Map" wysiwyg="" top-faq-venbi.jpg="" border="0">
<map name="Map" id="Map">
  <area shape="rect" coords="637,298,874,348" href="{{store url='blog/'}}" alt="Get Free Advice" target="_blank">
</map>

<div class="faq-mid">
<div class="faq-content">
<span class="pop-hd">Popular topics</span>
</div>
<div class="faq-3key">
    <ul>
        <li class="inpage-link">
            <span>How to use my Referral Codes</span><br>
            <a href="{{store url='help'}}#rewardpoints">Got a referral or promo code? We can show you how to use it here. Read more. </a>
        </li>
        <li>
            <span>Who is Venbi? </span><br>
            <a href="{{store url='about-us/'}}">We are a company for moms and dads built by moms and dads </a>
        </li>
        <li>
            <span>Where is our office?</span><br>
            <a href="{{store url='contacts/'}}">We are based at Sethiwan Tower <br> 19th Floor 139 Pan Road, Silom, Bangrak, Bangkok, 10500 Thailand </a>
        </li>
    </ul>
    </div>
</div>
<div class="faq-help-hd">
<!-- <span>Help Topics</span>  -->
</div>
<div class="faq-baby-hd">
<!-- <span>Baby Topics</span> -->
</div>
<div class="faq-gray-bg">

    <div class="faq-topic">
    <ul id="helpMenu">
    <li class="helpactive"><a href="javascript:void(0)" data-id="faq">FAQ</a></li>
    <li><a href="javascript:void(0)" data-id="howtoorder">How To Order</a></li>
    <li><a href="javascript:void(0)" data-id="payments">Payments</a></li>
    <li><a href="javascript:void(0)" data-id="shipping">Shipping &amp; Delivery</a></li>
    <li><a href="javascript:void(0)" data-id="rewardpoints">Reward Points</a></li>
    <li><a href="javascript:void(0)" data-id="returns">Cancellation and Returns</a></li>
    </ul>
</div>
<div class="helpContent">
    <div id="faq" class="subcontent" style="display: block;">
        <h2>FAQ</h2>
        <ol class="accordion">
            <li>
            <div class="accord-header pnk-font"><span class="pnk-bg"></span>What is Venbi?</div>
            <div class="accord-content">
                Venbi is the center of high quality products for kids of all ages, new moms and family members with reasonable price, great logistics, and customer service. Our goal is to help you save time from going to the store and the burden of carrying heavy products back home.
            </div>
            </li>
            <li>
            <div class="accord-header gry-font"><span class="gry-bg"></span>How did Venbi come about?</div>
            <div class="accord-content">
               Venbi.com came about for one simple reason – we as parent look like you were tired of traveling long distances through hours of traffic to get to a store only to have to carry back heavy product for child.  Most of the time we settled for products we didn’t want because that store was out of stock or didn’t carry that brands that we wanted. We knew there had to be a better way to find great selections of quality product at a competitive price, delivered to home.
            </div>
            </li>
            <li>
            <div class="accord-header pnk-font"><span class="pnk-bg"></span>Where is Venbi based?</div>
            <div class="accord-content">
                We are based at<br>
                Sethiwan Tower - 19th Floor <br>
                139 Pan Road, Silom, Bangrak, <br>
                Bangkok, 10500 Thailand<br>
                Tel: 02-106-8222 <br>
                Email: <a href="mailto:support@venbi.com">support@venbi.com</a>
            </div>
            </li>
            <li>
            <div class="accord-header gry-font"><span class="gry-bg"></span>If I were interested in partnering with Venbi, how would I go about it?</div>
            <div class="accord-content">The company welcomes any and all business ventures and ideas that you might have that will ultimately add value to our members. To discuss this further, please contact us on <a href="mailto:support@venbi.com">support@venbi.com</a></div>
            </li>
            <li>
            <div class="accord-header pnk-font"><span class="pnk-bg"></span>Free Shipping Promotion</div>
            <div class="accord-content">
                Free Delivery anywhere in Bangkok with the minimun purchase of 500 baht for each order
                <ul style="margin-left: 30px; list-style: square !important;">
                    <li style="border-bottom: 0;padding: 5px;font-weight: normal;">฿100  flat fee for shipping nationwide exclude Bangkok</li>
                    <li style="border-bottom: 0;padding: 5px;font-weight: normal;">Flat shipping fee of ฿100 will be applied to the order less than ฿500 in Bangkok and ฿200 for nationwide exclude Bangkok</li>
                </ul>
            </div>
            </li>
            <li>
            <div class="accord-header gry-font"><span class="gry-bg"></span>Deals Section</div>
            <div class="accord-content">The deals section on our site includes all products that have a discount of 20% or greater. We have made a special page for these products so that it will be easier for our customers such as your self to find.</div>
            </li>
            <li>
            <div class="accord-header pnk-font"><span class="pnk-bg"></span>I forgot my password. What do I do?</div>
            <div class="accord-content">If you have forgotten your password, don’t worry. We have systems in place to help you recover them.  <a href="{{store url='forgot-password'}}" target="_blank">Click here for more information</a>.</div>
            </li>
            <li>
            <div class="accord-header gry-font"><span class="gry-bg"></span>The item I want is out of stock. What now?</div>
            <div class="accord-content">Although we try to maintain a stock level that matches expected demand, highly popular items may sometimes be out of stock. Please contact our Customer Service team, who will quickly find a solution for you. Happy customers and happy pets are important to us.</div>
            </li>
            <li>
            <div class="accord-header pnk-font"><span class="pnk-bg"></span>Can I place an item on hold for purchase at a later date?</div>
            <div class="accord-content">Sorry, but we do not offer this option. </div>
            </li>
            <li>
            <div class="accord-header gry-font"><span class="gry-bg"></span>How do I know if you carry a certain brand?</div>
            <div class="accord-content">You can use the search function, which is available on the top of the page or go through our brands list by clicking on categories in navigation bar.</div>
            </li>
            <li>
            <div class="accord-header pnk-font"><span class="pnk-bg"></span>When can I expect new products to be listed on your website?</div>
            <div class="accord-content">We constantly update our products with new lines and new brands. Please visit our website to see the latest items we have in stock.</div>
            </li>
            <li class="last">
            <div class="accord-header gry-font"><span class="gry-bg"></span>What do I do if I have some question about a product?</div>
            <div class="accord-content">If you have any questions regarding the products, please do not hesitate to contact us on <a href="mailto:support@venbi.com">support@venbi.com</a></div>
            </li>
        </ol>
    </div>

    <div id="howtoorder" class="subcontent">
        <h2>How To Order</h2>
        <ol class="accordion">
            <li>
            <div class="accord-header pnk-font"><span class="pnk-bg"></span>How do I order?</div>
            <div class="accord-content">Ordering at Venbi.com is very easy. The steps can be  <a href="{{store url='how-to-order'}}" target="_blank">found here</a>.</div>
            </li>
            <li>
            <div class="accord-header gry-font"><span class="gry-bg"></span>How do I register a new account?</div>
            <div class="accord-content">Please click on 'Register', which is located on the top right hand side of the page. You will then be prompted to a new page where you will find 'New Customer' on the left hand side. Fill in your details as requested before clicking 'submit'. Registration should now be complete and you should receive a confirmation e-mail to the address you registered with.</div>
            </li>
            <li>
            <div class="accord-header pnk-font"><span class="pnk-bg"></span>Is there any registration fee for Venbi?</div>
            <div class="accord-content">
                Registration at Venbi is completely free and easy! Any form of charge will only come from purchasing of Venbi products.
            </div>
            </li>
            <li>
            <div class="accord-header gry-font"><span class="gry-bg"></span>I need personal assistance with my order. Who can I contact?</div>
            <div class="accord-content">Our Customer Service team will be happy to assist you. Please e-mail us at <a href="mailto:support@venbi.com">support@venbi.com</a> or call us at 02-106-8222 </div>
            </li>
            <li class="last">
            <div class="accord-header pnk-font"><span class="pnk-bg"></span>Do you take orders over the phone?</div>
            <div class="accord-content">Yes. We do take orders over phone. The payment mode possible for these orders is Cash On Delivery only.</div>
            </li>
        </ol>
    </div>

    <div id="payments" class="subcontent">
        <h2>Payments</h2>
        <ol class="accordion">
            <li>
            <div class="accord-header pnk-font"><span class="pnk-bg"></span>Is my credit card information safe at Venbi?</div>
            <div class="accord-content">The company will not store any credit card information on our servers. At Venbi, we use systems set in place by banks that have the security protocols set in place to handle safe credit card transactions.</div>
            </li>
            <li>
            <div class="accord-header gry-font"><span class="gry-bg"></span>My credit card details are not being accepted. What's wrong?</div>
            <div class="accord-content">Please check with your bank or financial institution to rule out errors on their behalf. If problems continue to persist, please contact Customer Service who will notify the IT department of technical difficulties.</div>
            </li>
            <li>
            <div class="accord-header pnk-font"><span class="pnk-bg"></span>My computer froze while processing payment. How will I know that my payment went through successfully?</div>
            <div class="accord-content">
                All successful transactions will receive a confirmation email that contains an order tracking number. If you have not received confirmation via email, please try placing your order again. Alternatively, please contact our Customer Service team to confirm the placement of your order at <a href="mailto:support@venbi.com">support@venbi.com</a> or call us at 02-106-8222.

            </div>
            </li>
            <li>
            <div class="accord-header gry-font"><span class="gry-bg"></span>How many payment methods does Venbi have?</div>
            <div class="accord-content">
                At Venbi, we provide customers the ability to pay using the following facilities:
<ul style="margin-left: 30px; list-style: square !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Credit card and Debit card</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">PayPal</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Cash on delivery (pay when we deliver the product)</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Installment Plan</li>
</ul><br/>
Enjoy up to 10 months installment on a minimum purchase of 3,500฿ with MOXY allied bank<br/><br/>
Bangkok Bank credit card<br/><br/>
<ul style="margin-left: 30px; list-style: disc !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">4-Month plan +0.8%</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">6-Month plan +0.8%</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">10-Month plan +0.8%</li>
</ul>
<br/>
Citibank credit card<br/><br/>
<ul style="margin-left: 30px; list-style: disc !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">4-Month plan +0.89%</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">6-Month plan +0.89%</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">10-Month plan +0.89%</li>
</ul>
<br/>
KTC credit card<br/><br/>
<ul style="margin-left: 30px; list-style: disc !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">4-Month plan +0.89%</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">6-Month plan +0.89%</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">10-Month plan +0.89%</li>
</ul>
<br/>
Thanachart credit card<br/><br/>
<ul style="margin-left: 30px; list-style: disc !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">4-Month plan +0.8%</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">6-Month plan +0.8%</li>
</ul>
<br/>
* condition<br/>
Each installment purchase may only comprise of purchases made under the same invoice<br/>
The minimum purchase does not include discount and shipping fee<br/>
Interest rate is based on the authorized bank<br/>
The installment purchase cannot be refunded<br/>
MOXY reserves the right to amend and cancel orders that without prior notice<br/>
            </div>
            </li>
            <li>
            <div class="accord-header pnk-font"><span class="pnk-bg"></span>Are your prices in Thai Baht (THB)?</div>
            <div class="accord-content">
                All pricing is in Thai Baht.
            </div>
            </li>
            <li class="last">
            <div class="accord-header gry-font"><span class="gry-bg"></span>Does your prices include taxes?</div>
            <div class="accord-content">
                All prices are inclusive of taxes.
            </div>
            </li>
        </ol>
    </div>

    <div id="shipping" class="subcontent">
        <h2>Shipping &amp; Delivery</h2>
        <ol class="accordion">
            <li>
            <div class="accord-header pnk-font"><span class="pnk-bg"></span>When will my order ship?</div>
            <div class="accord-content">
                All items are shipped within 24 hours after the purchase is made and most of the time even sooner (weekdays only). All Venbi deliveries are sent via Premium Courier. The deliveries are made between 9:00 am - 7:00 pm. Currently we deliver from Monday to Saturday.
            </div>
            </li>
            <li>
            <div class="accord-header gry-font"><span class="gry-bg"></span>How long will it take until I receive my order?</div>
            <div class="accord-content">
                We ship via our own delivery fleet and several other premium carriers. If you order by 4 pm on a business day, you will receive it in 2-3 business days. We do not yet guaranteed overnight shipping, although many areas of the country will receive their orders the next day.
                <br><br>
                * For Sangkhlaburi district, islands and provinces including Pattani, Yala, Naradhiwas, Mae Hong Son and Chiang Rai, delivery may take 1-2 days longer due to difficult routes.  We’ll try our best to deliver the product to you as soon as possible.
            </div>
            </li>
            <li>
            <div class="accord-header pnk-font"><span class="pnk-bg"></span>How do I track my order?</div>
            <div class="accord-content">
                When your order ships, you will receive an email with the shipping and tracking information. You can use your tracking number on our website to trace your order. You can also find your tracking number with your order details; Click the ""Order Status"" button under the ""My Account"" menu after you have logged on.
<br><br>
Please check with your neighbors or building manager if you have not received your product even though the tracking says that it has been delivered. If the tracking information doesn't how up or shows that the item has been returned to us, please contact us by email or phone. It is possible that we may have the wrong shipping address, the driver may have unsuccessfully attempted to deliver, or the shipping carrier may have misplaced the product.
            </div>
            </li>
            <li>
            <div class="accord-header gry-font"><span class="gry-bg"></span>What happens if I am not at home when my order is being delivered?</div>
            <div class="accord-content">
                In the process of delivering the product to you, our driver will contact you on the number provided to ensure that you are at the address that you have given to us. If you are not home at the time, do inform someone else in your household to beaware of our delivery so they can sign for the product on your behalf. IIf we are still unable to deliver the product to you despite our best efforts, the money paid for the product will be returned to you, but not the shipping fee (For more detail please check our &ldquo;<a href="{{store url='return-policy'}}" target="_blank">refund policy</a>&rdquo;). Undeliverable orders will not be resent and you must place a new order.
            </div>
            </li>
            <li>
            <div class="accord-header pnk-font"><span class="pnk-bg"></span>Can I have my order shipped to my office?</div>
            <div class="accord-content">
               We are happy to delivery to your office; If you would like us to do that, please input your office address at checkout. Your office delivery will be made from the 9am to 7pm on weekdays and 9am to 12 noon on weekends.
            </div>
            </li>
            <li>
            <div class="accord-header gry-font"><span class="gry-bg"></span>How much is the shipping fee?</div>
            <div class="accord-content">
                Free Delivery anywhere in Bangkok with the minimun purchase of 500 baht for each order
                <ul style="margin-left: 30px; list-style: square !important;">
                    <li style="border-bottom: 0;padding: 5px;font-weight: normal;">฿100  flat fee for shipping nationwide exclude Bangkok</li>
                    <li style="border-bottom: 0;padding: 5px;font-weight: normal;">Flat shipping fee of ฿100 will be applied to the order less than ฿500 in Bangkok and ฿200 for nationwide exclude Bangkok</li>
                </ul>
            </div>
            </li>
            <li>
            <div class="accord-header pnk-font"><span class="pnk-bg"></span>Do you ship outside of Thailand?</div>
            <div class="accord-content">
                Sorry, we currently do not ship outside of Thailand.
            </div>
            </li>
            <li class="last">
            <div class="accord-header gry-font"><span class="gry-bg"></span>Why is my shipment delayed?</div>
            <div class="accord-content">
                You must order by 4 p.m. for 1-2-day shipping to apply. We try our best to make sure that you receive your order as soon as possible, however, there are some situations beyond our control that can delay your shipment.<br>
The following orders may result in delayed shipping:
                <ul style="margin-left: 30px; list-style: square !important;">
                    <li style="border-bottom: 0;padding: 5px;font-weight: normal;">Incorrect shipping address</li>
                    <li style="border-bottom: 0;padding: 5px;font-weight: normal;">Incorrect Mobile Phone number</li>
                    <li style="border-bottom: 0;padding: 5px;font-weight: normal;">Unable to reach you at provided contact number</li>
                    <li style="border-bottom: 0;padding: 5px;font-weight: normal;">Being absent after courier made an appointment</li>
                    <li style="border-bottom: 0;padding: 5px;font-weight: normal;">Shipping address does not match billing address</li>
                    <li style="border-bottom: 0;padding: 5px;font-weight: normal;">Payment delay or issue</li>
                    <li style="border-bottom: 0;padding: 5px;font-weight: normal;">Courier failure to deliver (e.g. severe weather conditions)</li>
                </ul>
            </div>
            </li>
        </ol>
    </div>

    <div id="rewardpoints" class="subcontent">
        <h2>Reward Points</h2>
        <ol class="accordion">
            <li>
            <div class="accord-header pnk-font"><span class="pnk-bg"></span>1% Cash-back policy</div>
            <div class="accord-content">
                A 1% cash back means that for every purchase made, you will receive 1% cash-back, which will be stored in your Venbi account. You can then use this credit as a discount on your next purchase.
            </div>
            </li>

            <li>
            <div class="accord-header gry-font"><span class="gry-bg"></span>How do I get points for referring a friend</div>
            <div class="accord-content">
                There are many ways where you can get 100B by <a href="{{store url='rewardpoints/index/referral'}}" target="_blank">referring a friend</a>  at Venbi.com

                <ul style="margin-left: 30px; list-style: square !important;">
                    <li style="border-bottom: 0;padding: 5px;font-weight: normal;">You can share your unique link which can be found in My Accounts straight to your Facebook or Twitter page. You can also email this link straight to your friends.</li>
                    <li style="border-bottom: 0;padding: 5px;font-weight: normal;">Once your friends have clicked on the link and have made a purchase on Venbi, you will automatically be credited with 100 Baht in your account in the form of credits.</li>
                </ul>
                These credits can be used on any future purchases that you make.
            </div>
            </li>

            <li class="last">
            <div class="accord-header pnk-font"><span class="pnk-bg"></span>Reward Points Terms &amp; Conditions</div>
            <div class="accord-content">
                Each time you shop on our website, you will earn Reward Points on your purchases. The Reward Points will be accumulated each time you purchase the products on our website and it will be expired within 6 months from your purchasing date.
                <br><br>
                For example, if you earn 20 points on January from your first purchase and another 30 points on March, your Reward Point balance is 50 points. On July, your balance will be deducted to 30 points because 20 points on January are already expired. In case you have not used your point within September, you point balance will become 0 as your points earned previously on March are expired.
                <br><br>
                For your own interest, please kindly redeem your Reward Points in the available period of time.


            </div>
            </li>

        </ol>
    </div>

    <div id="returns" class="subcontent">
        <h2>Cancellation and Returns</h2>
        <ol class="accordion">
            <li>
            <div class="accord-header pnk-font"><span class="pnk-bg"></span>How do I return my purchase?</div>
            <div class="accord-content">
                If you wish to return an item, please contact our customer care and request a return of the product(s) via phone 02-106-8222 or Email&nbsp;<a href="mailto:support@venbi.com">support@venbi.com</a>. If the conditions of the returned product aligned within our return policy, please fill in the Returns Slip you received with your shipment, enclose the invoice in the package then send it back to us in its original packaging. Please send the product back to venbi.com at any Thailand Post location nearby your premise under domestic registered mail service.&nbsp;
                 <br><br>
                Our address is:<br>
                Acommerce Co.,Ltd<br>
8/2 Rama3 Rd, Soi 53, <br>Bang Phong Pang, Yarn-Nava, <br>Bangkok 10120

                 <br><br>

                venbi.com will absorb the incurred shipping costs in this registered returns back to you on the refund process. We ask that you keep the receipt/slip of this shipping cost issued by the post office as an evidence. And please send your receipt/slip back to Customer Care by submitting a photo file to the email <a href="mailto:support@venbi.com">support@venbi.com</a>

            </div>
            </li>
            <li>
            <div class="accord-header gry-font"><span class="gry-bg"></span>Packing my product for return</div>
            <div class="accord-content">
                The box that the items were originally shipped in is strongly recommended to be reused except if it’s not in a good condition for repacking. Simply apply the shipping label acquired through the online return process or from our customer service team. As mentioned above, please put the return label in your box/package and drop it off at any Thailand Post location.
<br><br>
NOTE: Return shipments are not eligible for scheduled pick-ups by Venbi.
<br><br>
Please contact us with any questions or concerns at 02-106-8222 or send an e-mail to <a href="mailto:support@venbi.com">support@venbi.com</a>
            </div>
            </li>
            <li>
            <div class="accord-header pnk-font"><span class="pnk-bg"></span>How long would it take for my return to be processed?</div>
            <div class="accord-content">
                Once we receive the item(s), we will process your request within 14 business days.
            </div>
            </li>
            <li>
            <div class="accord-header gry-font"><span class="gry-bg"></span>When do I receive my refund?</div>
            <div class="accord-content">
                Once we receive your item(s) and have checked that it is in a sellable condition, a refund will be initiated immediately. venbi will refund your money through Bank Transfer. Please allow about 7 business days to receive your money back from the time we receive your return.
            </div>
            </li>
            <li>
            <div class="accord-header pnk-font"><span class="pnk-bg"></span>What are the shipping charges if I return my purchased items?</div>
            <div class="accord-content">
               The shipping costs will vary according to your location and method of postage you choose. Please make sure to ship back to us by Thailand post by registered mail and we will fully refund for shipping fee after receive the delivery receipt.
            </div>
            </li>
            <li class="last">
            <div class="accord-header gry-font"><span class="gry-bg"></span>What if my item is faulty?</div>
            <div class="accord-content">
                Our Quality Control department tries to ensure that all products are of a high quality when they leave the warehouse. In the rare circumstance that your item has a defect, you may send it back to us with a completed Returns Slip attached within 30 days upon receiving the item(s),a full refund will be issued to you.

            </div>
            </li>
        </ol>
    </div>
</div>

</div>
<img src="{{media url="wysiwyg/bottom-faq-venbi.jpg"}}" alt="" style="margin-top:20px;">

<script type="text/javascript">// <![CDATA[
 jQuery(document).ready(function()
 {
  var tmpUrl = "";
  var h=window.location.hash.substring(0);
  if(h == ""){
   //alert('Hash not found!!!');
   h = "#faq";
   //alert(heart);
   jQuery( "#helpMenu li:first-child" ).addClass( "helpactive");
   jQuery('.subcontent[id='+h.substring(1)+']').fadeIn();
  }else{
   jQuery('#helpMenu li a[data-id='+h.substring(1)+']').parent().addClass( "helpactive");
  jQuery('.subcontent[id='+h.substring(1)+']').fadeIn();
  }

  //////////////////////////////
  jQuery('#keyMess li').on('click','a',function()  {
   tmpUrl=jQuery(this).attr('href');
   h=tmpUrl.substring(tmpUrl.indexOf("#")+1);

   jQuery('.subcontent:visible').fadeOut(0);
   jQuery('.subcontent[id='+h+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
  });
  jQuery('#zyncNav li').on('click','a',function()
  {
   tmpUrl=jQuery(this).attr('href');
   h=tmpUrl.substring(tmpUrl.indexOf("#")+1);
   jQuery('.subcontent:visible').fadeOut(0);
   jQuery('.subcontent[id='+h+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
  });
  jQuery('#footerBanner div').on('click','a',function()
  {
   tmpUrl=jQuery(this).attr('href');
   h=tmpUrl.substring(tmpUrl.indexOf("#")+1);
   jQuery('.subcontent:visible').fadeOut(0);
   jQuery('.subcontent[id='+h+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
  });
  jQuery('#topMklink').on('click','a',function()
  {
   tmpUrl=jQuery(this).attr('href');
   h=tmpUrl.substring(tmpUrl.indexOf("#")+1);
   jQuery('.subcontent:visible').fadeOut(0);
   jQuery('.subcontent[id='+h+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
  });
  jQuery('#footer_help li').on('click','a',function()
  {
   tmpUrl=jQuery(this).attr('href');
   h=tmpUrl.substring(tmpUrl.indexOf("#")+1);
   jQuery('.subcontent:visible').fadeOut(0);
   jQuery('.subcontent[id='+h+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
  });

    jQuery('li.inpage-link').on('click','a',function()
  {
   tmpUrl=jQuery(this).attr('href');
   h=tmpUrl.substring(tmpUrl.indexOf("#")+1);
   jQuery('.subcontent:visible').fadeOut(0);
   jQuery('.subcontent[id='+h+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
  });
 ///////////////////////////////

     jQuery('#helpMenu').on('click','a',function()
     {
         jQuery('.subcontent:visible').fadeOut(0);
         jQuery('.subcontent[id='+jQuery(this).attr('data-id')+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery(this).parent().addClass( "helpactive");
     });

///////////////////////////////

     jQuery(".accordion li .accord-header").mouseenter(function() {
            jQuery(this).css("cursor","pointer");
        });
        jQuery(".accordion li .accord-header").click(function() {
            jQuery(this).find("span").removeClass('active');
          if(jQuery(this).next("div").is(":visible")){
            jQuery(this).next("div").slideUp(300);
          } else {
            jQuery(".accordion li .accord-content").slideUp(400);
            jQuery(this).next("div").slideToggle(400);
            jQuery(".accordion li .accord-header").find("span").removeClass('active');
            jQuery(this).find("span").addClass('active');
          }
        });
      });
// ]]></script>
EOD;
/** @var Mage_Cms_Model_Page $page */
$page = Mage::getModel('cms/page')->getCollection()
    ->addStoreFilter($stores, $withAdmin = false)
    ->addFieldToFilter('identifier', $identifier)
    ->getFirstItem();
if ($page->getId()) $page->delete(); // if exists then delete
$page = Mage::getModel('cms/page');
$page->setTitle($title);
$page->setIdentifier($identifier);
$page->setStores($stores);
$page->setIsActive($isActive);
$page->setRootTemplate($rootTemplate);
$page->setSortOrder($sortOrder);
$page->setContent($content);
$page->setContentHeading($contentHeading);
$page->setMetaDescription($metaDescription);
$page->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// Help page (Venbi Thai)
//==========================================================================
$title = "ช่วยเหลือ FAQ คำถามที่พบบ่อย";
$stores = array($vth);
$metaDescription = "รวมคำถามยอดฮิตในการสั่งซื้อสินค้า วิธีการการสั่งซื้อ ขั้นตอนการชำระเงิน การใช้รหัสส่วนลด การจัดส่งสินค้า และอื่นๆ อีกมากมาย";
$identifier = "help";
$isActive = 1;
$rootTemplate = "one_column";
$sortOrder = 0;
$contentHeading = "ช่วยเหลือ";
$content = <<<EOD
<style>
    .faq-topic{border-bottom: 1px dotted #c7c7c7;height: 37px;}
  .faq-mid{background:url('http://cdn.whatsnew.asia/media/wysiwyg/mid-faq-venbi.jpg'); width:930px;height:410px;margin-top:20px;}
  .faq-content{float: left;width: 100%;margin-top: 33px;height: 33px;}
  .pop-hd{background-color:#f07495; padding: 15px;font-size: 18px;color: #FFF;}
  .faq-3key{margin: 14px 20px 20px 20px;float: left;}
  .faq-3key ul li{float: left;width: 280px;font-size: 14px;line-height: 22px;padding-right: 10px;}
  .faq-3key ul li span{color:#f07495;font-weight:bold;}
  .faq-help-hd{width:48%;float:left;margin-top:20px;}
  .faq-help-hd span{background-color:#f07495; padding: 15px;font-size: 18px;color: #FFF;}
  .faq-baby-hd{width:48%;float:left;margin-top:20px;}
  .faq-baby-hd span{background-color:#00b6eb;  padding: 15px;font-size: 18px;color: #FFF;}
  .faq-gray-bg{width:100%;background-color:#f6f6f6; float: left;padding-bottom: 15px;}
  .ol45{width:45%;float:left; margin-top: 15px;}
  .ol48{width:48%;float:left; margin-top: 15px;}
  ul#helpMenu{width: 930px;margin: 0 auto;vertical-align: middle;margin-top: 2px;}
    ul#helpMenu li{float: left;display:list-item;}
    ul#helpMenu li:hover{background:#f07495;color:#ffffff;text-decoration: none;}
    ul#helpMenu li a{display:block;color:#4f78a7;text-decoration: none;width:100%;padding: 10px 38px;}
    ul#helpMenu li a:hover{color:#ffffff;text-decoration: none;}
    div.helpContent div.subcontent{display: none;float: left;width: 100%;margin-top: 20px;}
    div.accord-content{display: none;font-weight: normal;padding: 20px;}
    ol.accordion{list-style: none;width: 95%;margin: 0 auto;}
    ol.accordion li{padding: 10px 0;border-bottom:1px dotted #c7c7c7;font-weight: bold;}
    ol.accordion li.last{border-bottom:none;}
    div.subcontent h2{text-align: center; font-size: 20px; font-weight: bold;color:#f07495;}
    .helpactive{background:#f07495;color:#ffffff;}
    .helpactive a{color:#ffffff !important;}
    div.subcontent h2{text-align: center; font-size: 20px; font-weight: bold;}
    div.subcontent h2{text-align: center; font-size: 20px; font-weight: bold;}
    .helpactive{background:#f07495;color:#FFFFFF;}
    .helpactive a{color:#FFFFFF;}
</style>
<img src="{{media url="wysiwyg/top-faq-venbi.jpg"}}" alt="" usemap="#Map" wysiwyg="" top-faq-venbi.jpg="" border="0">
<map name="Map" id="Map">
  <area shape="rect" coords="637,298,874,348" href="{{store url='blog/'}}" alt="Get Free Advice" target="_blank">
</map>

<div class="faq-mid">
  <div class="faq-content">
<span class="pop-hd">คำถามยอดฮิต</span>
</div>
<div class="faq-3key">
    <ul>
        <li class="inpage-link">
            <span>คุณจะใช้รหัสส่วนลดได้อย่างไร</span><br>
            <a href="{{store url='help'}}#rewardpoints">คุณได้รับรหัสส่วนลดมาใช่หรือไม่    คุณสามารถดูวิธีการใช้รหัสส่วนลดได้ที่นี่ อ่านต่อ</a>
        </li>
        <li>
            <span>Venbi  คือใคร</span><br>
            <a href="{{store url='about-us/'}}">เราเป็นบริษัทสำหรับคุณพ่อคุณแม่ที่สร้างโดยกลุ่มพ่อแม่ผู้มากประสบการณ์ </a>
        </li>
        <li>
            <span>บริษัทของเราตั้งอยู่ที่ไหน</span><br>
           <a href="{{store url='contacts/'}}"> บริษัทตั้งอยู่ที่ อาคารเศรษฐีวรรณ ชั้น 19 <br>139 ถนนปั้น แขวงสีลม เขตบางรัก&nbsp;กรุงเทพฯ ประเทศไทย 10500 </a>
        </li>
    </ul>
    </div>
</div>
<div class="faq-help-hd">
<!-- <span>Help Topics</span>  -->
</div>
<div class="faq-baby-hd">
<!-- <span>Baby Topics</span> -->
</div>
<div class="faq-gray-bg">

    <div class="faq-topic">
    <ul id="helpMenu">
    <li class="helpactive"><a href="javascript:void(0)" data-id="faq">คำถามที่พบบ่อย</a></li>
    <li><a href="javascript:void(0)" data-id="howtoorder">วิธีการการสั่งซื้อ</a></li>
    <li><a href="javascript:void(0)" data-id="payments">การชำระเงิน</a></li>
    <li><a href="javascript:void(0)" data-id="shipping">การจัดส่งสินค้า</a></li>
    <li><a href="javascript:void(0)" data-id="rewardpoints">คะแนนสะสม</a></li>
    <li><a href="javascript:void(0)" data-id="returns">การคืนสินค้าและขอคืนเงิน</a></li>
    </ul>
</div>
<div class="helpContent">
    <div id="faq" class="subcontent" style="display: block;">
        <h2>FAQ</h2>
        <ol class="accordion">
            <li>
            <div class="accord-header pnk-font"><span class="pnk-bg"></span>Venbi คืออะไร</div>
            <div class="accord-content">
                Venbi เป็นศูนย์รวมสินค้าอุปโภค บริโภค คุณภาพสำหรับเด็กทุกวัย คุณแม่มือใหม่ และสมาชิกทุกคนในครอบครัว โดยมีระบบที่ได้รับการพัฒนาเป็นอย่างดี ซึ่งจะช่วยให้ลูกค้าสามารถเลือกซื้อสินค้าได้ตลอด 24 ชั่วโมง ไม่เสียเวลาการเดินทางและไม่ต้องกังวลเกี่ยวกับการขนส่งสินค้าที่มีน้ำหนักมากด้วยตัวเอง
            </div>
            </li>
            <li>
            <div class="accord-header gry-font"><span class="gry-bg"></span>Venbi มีที่มาอย่างไร</div>
            <div class="accord-content">
               Venbi.com เกิดขึ้นด้วยเหตุผลง่าย ๆ จากการที่เราเป็นหนึ่งในพ่อและแม่ที่เข้าใจในความลำบาก ความเหน็ดเหนื่อยในการซื้อสินค้าเพื่อลูกในแต่ละครั้งจะต้องพบเจอกับปัญหามากมายเพียงใด ทั้งปัญหารถติด ปัญหาในการขนส่งสินค้าที่มีน้ำหนักมาก และในบางครั้งก็อาจจะยังไม่ได้สินค้าที่ต้องการ เพราะว่าร้านค้าไม่ได้สต็อกสินค้าเอาไว้อย่างเพียงพอ ซึ่งนั่นก็ทำให้เสียเวลาการเดินทางโดยใช่เหตุ เราจึงมีไอเดียที่จะสร้างทางเลือกที่ดีกว่าให้แก่ทุกคนได้สามารถเลือกซื้ออาหารสัตว์สินค้าสำหรับเด็กคุณภาพสูง ในราคาที่ถูกลง ทั้งยังจัดส่งถึงบ้านอีกด้วย
            </div>
            </li>
            <li>
            <div class="accord-header pnk-font"><span class="pnk-bg"></span>Venbi ตั้งอยู่ที่ไหน</div>
            <div class="accord-content">
                บริษัทตั้งอยู่ที่ อาคารเศรษฐีวรรณ ชั้น 19<br>
                139 ถนนปั้น แขวงสีลม เขตบางรัก&nbsp;<br>
                กรุงเทพฯ ประเทศไทย 10500<br>
                โทรศัพท์ 02-106-8222<br>
                อีเมล์  <a href="mailto:support@venbi.com">support@venbi.com</a>
            </div>
            </li>
            <li>
            <div class="accord-header gry-font"><span class="gry-bg"></span>สนใจอยากร่วมธุรกิจกับ Venbi ทำอย่างไร</div>
            <div class="accord-content">บริษัทยินดีต้อนรับผู้ที่สนใจเข้าเป็นส่วนหนึ่งกับเราได้ โดยท่านที่สนใจนำเสนอสินค้าสำหรับเด็ก ผ่านทางหน้าเว็บไซต์ Venbi สามารถติดต่อได้ที่อีเมล์ <a href="mailto:support@venbi.com">support@venbi.com</a></div>
            </li>
            <li>
            <div class="accord-header pnk-font"><span class="pnk-bg"></span>โปรโมชั่นจัดส่งสินค้าฟรี</div>
            <div class="accord-content">จัดส่งฟรีเมื่อซื้อสินค้าขั้นต่ำ 500 บาทต่อการสั่งซื้อ1ครั้ง เฉพาะเขตกรุงเทพ ในส่วนของเขตปริมณฑล และต่างจังหวัด คิดค่าบริการในการจัดส่ง 100 บาท ในกรณีซื้อสินค้าต่ำกว่า 500 บาทเสียค่าจัดส่ง 100 บาทในเขตกรุงเทพ และ 200 บาทในเขตปริมณฑลและต่างจังหวัด</div>
            </li>
            <li>
            <div class="accord-header gry-font"><span class="gry-bg"></span>ข้อเสนอพิเศษ</div>
            <div class="accord-content">ในหน้าเว็บไซต์ของเรา จะมีข้อเสนอพิเศษต่างๆ รวมทั้งสินค้าที่มีส่วนลด 20% หรือมากกว่า ซึ่งเราได้ทำหน้าเฉพาะสำหรับผลิตภัณฑ์เหล่านี้เพื่อความง่ายในการเลือกซื้อหรือค้นหาสินค้าราคาพิเศษ หรือข้อเสนอพิเศษที่คุณต้องการ</div>
            </li>
            <li>
            <div class="accord-header pnk-font"><span class="pnk-bg"></span>ฉันลืมรหัสผ่าน จะทำอย่างไร</div>
            <div class="accord-content">ถ้าคุณลืมรหัสผ่านของคุณ คุณสามารถขอรหัสผ่านใหม่ได้ <a href="{{store url='forgot-password'}}" target="_blank">คลิกที่นี่</a> เพื่อดูรายละเอียดเพิ่มเติม</div>
            </li>
            <li>
            <div class="accord-header gry-font"><span class="gry-bg"></span>ถ้าสินค้าที่ต้องการหมด จะทำอย่างไร</div>
            <div class="accord-content">ถึงแม้ว่าทางเราจะพยายามจัดหาสินค้าให้มีจำนวนเหมาะสมกับระดับความต้องการของลูกค้า แต่สินค้าที่ได้รับความนิยมอย่างมากก็สามารถหมดลงได้อย่างรวดเร็ว แต่เราพยายามไม่ให้ปัญหานี้เกิดขึ้น หรือถ้าเกิดขึ้นก็จะพยายามหาทางแก้ไข และหาทางออกให้กับลูกค้าอย่างแน่นอน ถ้ามีข้อสงสัย หรือปัญหาเกี่ยวกับสินค้าสามารถติดต่อได้ที่ฝ่ายลูกค้าสัมพันธ์ได้ในเวลาทำการ</div>
            </li>
            <li>
            <div class="accord-header pnk-font"><span class="pnk-bg"></span>ฉันสามารถจองสินค้าไว้ก่อนเพื่อทำการสั่งซื้อในวันถัดไปได้หรือไม่</div>
            <div class="accord-content">ต้องขออภัย ทางเราไม่มีนโยบายการจองสินค้าในลักษณะนี้ </div>
            </li>
            <li>
            <div class="accord-header gry-font"><span class="gry-bg"></span>ฉันจะทราบได้อย่างไรว่ามีแบรนด์ไหนที่ขายอยู่บนเว็บไซต์ Venbi บ้าง</div>
            <div class="accord-content">คุณสามารถค้นหาสินค้าที่ต้องการได้ โดยกดปุ่มค้นหาที่หน้าเว็บไซต์ Venbi ซึ่งอยู่ด้านบนของหน้าเว็บไซต์ หรือคุณสามารถเข้าไปคลิกเลือกแบรนด์ต่าง ๆ ได้ บนแถบนำทาง</div>
            </li>
            <li>
            <div class="accord-header pnk-font"><span class="pnk-bg"></span>ฉันจะทราบได้อย่างไร ว่ามีสินค้าใหม่เข้ามาในเว็บไซต์</div>
            <div class="accord-content">ทางเราจะทำการอัพเดทเว็บไซต์ใหม่ทันที ที่มีสินค้าใหม่เข้ามาในคลังสินค้า คุณสามารถตรวจสอบสินค้าใหม่ได้ที่เว็บไซต์ของเรา</div>
            </li>
            <li class="last">
            <div class="accord-header gry-font"><span class="gry-bg"></span>ฉันสามารถสอบถามรายละเอียดเกี่ยวกับสินค้าได้อย่างไร</div>
            <div class="accord-content">หากคุณมีคำถามหรือมีข้อคิดเห็นเกี่ยวกับผลิตภัณฑ์ของเรา คุณสามารถแสดงความคิดเห็นของคุณมาผ่านมาทางอีเมล์  <a href="mailto:support@venbi.com">support@venbi.com</a></div>
            </li>
        </ol>
    </div>

    <div id="howtoorder" class="subcontent">
        <h2>วิธีการการสั่งซื้อ</h2>
        <ol class="accordion">
            <li>
            <div class="accord-header pnk-font"><span class="pnk-bg"></span>สั่งซื้อสินค้าผ่าน Venbi อย่างไร</div>
            <div class="accord-content">คุณสามารถสั่งซื้อสินค้ากับ Venbi.com ด้วยวิธีง่ายๆ <a href="{{store url='how-to-order'}}" target="_blank">คลิกที่นี่</a> เพื่อดูขั้นตอนการสั่งซื้อ</div>
            </li>
            <li>
            <div class="accord-header gry-font"><span class="gry-bg"></span>สมัครเพื่อลงทะเบียนบัญชีผู้ใช้ใหม่อย่างไร</div>
            <div class="accord-content">เพียงคลิกไปที่ “ลงทะเบียน” ซึ่งอยู่ด้านบนขวามือของเว็บไซต์ หลังจากนั้นจะเปลี่ยนไปหน้าสร้างบัญชีผู้ใช้ใหม่ซึ่งจะอยู่ทางด้านซ้ายมือ หลังจากกรอกรายละเอียดข้อมูลของท่านเสร็จเรียบร้อยแล้วคลิกที่ “ยืนยัน” เพื่อเสร็จสิ้นการสมัครสมาชิก หลังจากนั้นคุณจะได้รับอีเมล์ยืนยันการลงทะเบียนผู้ใช้ใหม่กับทาง Venbi.com เพียงเท่านี้คุณก็สามารถซื้อสินค้าผ่านทางเราได้อย่างสะดวกสบาย</div>
            </li>
            <li>
            <div class="accord-header pnk-font"><span class="pnk-bg"></span>มีค่าธรรมเนียมในการสมัครสมาชิกกับ Venbi หรือไม่</div>
            <div class="accord-content">
                การสมัครสมาชิกไม่มีค่าธรรมเนียมใด ๆ ทั้งสิ้น และจะเกิดค่าใช้จ่ายในกรณีที่มีการสั่งซื้อสินค้าเท่านั้น
            </div>
            </li>
            <li>
            <div class="accord-header gry-font"><span class="gry-bg"></span>ถ้าฉันต้องการที่ปรึกษาส่วนตัวสำหรับการสั่งซื้อสามารถติดต่อได้ทางใด</div>
            <div class="accord-content">แผนกลูกค้าสัมพันธ์ของเรายินดีเป็นอย่างยิ่งที่จะช่วยเหลือท่านผ่านทางอีเมล์ <a href="mailto:support@venbi.com">support@venbi.com</a> หรือทางหมายเลยโทรศัพท์ 02-106-8222 </div>
            </li>
            <li class="last">
            <div class="accord-header pnk-font"><span class="pnk-bg"></span>ฉันสามารถสั่งซื้อสินค้าผ่านทางโทรศัพท์ได้หรือไม่</div>
            <div class="accord-content">คุณสามารถสั่งซื้อสินค้าผ่านทางโทรศัพท์ได้ เพียงโทรเข้าไปที่แผนกลูกค้าสัมพันธ์ตามหมายเลขที่แจ้งไว้ และต้องชำระค่าสินค้าด้วยวิธีเก็บเงินปลายทางเท่านั้น </div>
            </li>
        </ol>
    </div>

    <div id="payments" class="subcontent">
        <h2>การชำระเงิน</h2>
        <ol class="accordion">
            <li>
            <div class="accord-header pnk-font"><span class="pnk-bg"></span>การใช้บัตรเครดิตกับทาง Venbi ปลอดภัยหรือไม่</div>
            <div class="accord-content">บริษัทดูแลข้อมูลของสมาชิก Venbi เป็นอย่างดี ข้อมูลทุกอย่างจะถูกเก็บไว้เป็นความลับและดูแลรักษาอย่างปลอดภัย ด้านข้อมูลเกี่ยวกับบัตรเครดิตทั้งหมดจะถูกส่งผ่านระบบ SSL (Secure Socket Layer) ซึ่งเป็นระบบรักษาความปลอดภัยมาตรฐานในการส่งข้อมูลทางอินตเตอร์เน็ตตรงไปยังระบบเซฟอิเล็กทรอนิกส์ของธนาคาร โดยจะไม่มีการเก็บข้อมูลเกี่ยวกับบัตรเครดิตไว้ที่ระบบเซิฟเวอร์ของเว็บไซต์แต่อย่างใด</div>
            </li>
            <li>
            <div class="accord-header gry-font"><span class="gry-bg"></span>ข้อมูลบัตรเครดิตไม่สามารถใช้ได้ เกิดจากอะไร</div>
            <div class="accord-content">กรุณาตรวจสอบสถานะการเงินของคุณจากทางธนาคารหากยังไม่สามารถใช้ได้กรุณาติดต่อแผนกลูกค้าสัมพันธ์ของธนาคารเพื่อแจ้งกับแผนกเทคโนโลยีสารสนเทศเพื่อแก้ไขปัญหาด้านเทคนิค</div>
            </li>
            <li>
            <div class="accord-header pnk-font"><span class="pnk-bg"></span>หากเครื่องคอมพิวเตอร์ขัดข้องระหว่างขั้นตอนการชำระเงิน จะทราบได้อย่างไรว่าการชำระเงินเสร็จสิ้นเรียบร้อยแล้ว</div>
            <div class="accord-content">
                เมื่อคุณชำระเงินเรียบร้อยแล้วคำยืนยันการสั่งซื้อจะถูกส่งไปยังอีเมล์ของคุณ หากคุณไม่ได้รับการยืนยันการสั่งซื้อทางอีเมล์กรุณาลองทำการสั่งซื้ออีกครั้ง หากคุณมีข้อสงสัยกรุณาติดต่อแผนกลูกค้าสัมพันธ์เพื่อสอบถามสถานะการสั่งซื้อของคุณที่ อีเมล์ <a href="mailto:support@venbi.com">support@venbi.com</a> หรือทางโทรศัพท์หมายเลข 02-106-8222
            </div>
            </li>
            <li>
            <div class="accord-header gry-font"><span class="gry-bg"></span>วิธีการชำระเงินมีกี่ช่องทาง</div>
            <div class="accord-content">
                บริษัทมีช่องทางให้ลูกค้าเลือกชำระเงินหลายช่องทาง ตามด้านล่างนี้
<ul style="margin-left: 30px; list-style: square !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">บัตรเครดิต วีซ่า มาสเตอร์ ซึ่งผ่าน่ระบบ Payment ของบริษัท 2C2P</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">PayPal ช่วยให้คุณสามารถชำระค่าสินค้าของคุณหรือชำระด้วยบัตรเครดิตชั้นนำต่างๆ ผ่านทางหน้าเว็บไซด์ได้ทันที โดยจะทำรายการผ่านระบบของ PayPal ทำให้การชำระเงินเป็นไปได้อย่างสะดวก รวดเร็วและปลอดภัย หากท่านยังไม่มีบัญชีกับ Paypal สามารถ คลิกที่นี่เพื่อสมัครบริการ Paypal</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Cash on delivery เป็นการเก็บเงินปลายทางจากผู้รับสินค้า โดยจะมีค่าธรรมเนียมในการเก็บเงินปลายทาง 50 บาท ต่อการสั่งซื้อต่อครั้ง ค่าธรรมเนียมนี้ไม่รวมค่าสินค้า และค่าจัดส่งสินค้า</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">บริการผ่อนชำระสินค้า</li>
</ul><br/>
เมื่อซื้อสินค้าครบ 3,500 บาท สามารถผ่อนชำระได้นานสูงสุด 10 เดือนกับบัตรเครดิตที่ร่วมรายการ<br/><br/>
บัครเครดิตธนาคารกรุงเทพ<br/><br/>
<ul style="margin-left: 30px; list-style: disc !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">แบ่งชำระ 4 เดือน ดอกเบี้ย 0.8%</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">แบ่งชำระ 6 เดือน ดอกเบี้ย 0.8%</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">แบ่งชำระ 10 เดือน ดอกเบี้ย 0.8%</li>
</ul>
<br/>
บัตรเครดิตซิตี้แบงค์<br/><br/>
<ul style="margin-left: 30px; list-style: disc !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">แบ่งชำระ 4 เดือน ดอกเบี้ย 0.89%</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">แบ่งชำระ 6 เดือน ดอกเบี้ย 0.89%</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">แบ่งชำระ 10 เดือน ดอกเบี้ย 0.89%</li>
</ul>
<br/>
บัตรเครดิต KTC<br/><br/>
<ul style="margin-left: 30px; list-style: disc !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">แบ่งชำระ 4 เดือน ดอกเบี้ย 0.89%</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">แบ่งชำระ 6 เดือน ดอกเบี้ย 0.89%</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">แบ่งชำระ 10 เดือน ดอกเบี้ย 0.89%</li>
</ul>
<br/>
บัตรเครดิตธนาคารธนชาติ<br/><br/>
<ul style="margin-left: 30px; list-style: disc !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">แบ่งชำระ 4 เดือน ดอกเบี้ย 0.8%</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">แบ่งชำระ 6 เดือน ดอกเบี้ย 0.8%</li>
</ul>
<br/>
* หมายเหตุ<br/>
ยอดสั่งซื้อขั้นต่ำ 3,500 บาทต่อคำสั่งซื้อ โดยคำนวนราคาก่อนหักส่วนลดและไม่รวมค่าขนส่ง<br/>
อัตราดอกเบี้ยเป็นไปตามที่ธนาคารผู้ออกบัตรกำหนด<br/>
สงวนสิทธิ์การคืนเงินเในทุกกรณี<br/>
เงื่อนไขเป็นไปตามข้อกำหนด บริษัทขอสงวนสิทธิ์การเปลี่ยนแปลงโดยไม่แจ้งให้ทราบล่วงหน้า<br/>
            </div>
            </li>
            <li>
            <div class="accord-header pnk-font"><span class="pnk-bg"></span>ราคาสินค้าใช้สกุลเงินไทยหรือไม่</div>
            <div class="accord-content">
                ราคาสินค้าทุกชิ้นของเราใช้หน่วยเงินบาทไทย
            </div>
            </li>
            <li class="last">
            <div class="accord-header gry-font"><span class="gry-bg"></span>ราคาสินค้ารวมภาษีมูลค่าเพิ่มหรือยัง</div>
            <div class="accord-content">
                ราคาสินค้าของเราได้รวมภาษีมูลค่าเพิ่มแล้ว
            </div>
            </li>
        </ol>
    </div>

    <div id="shipping" class="subcontent">
        <h2>การจัดส่งสินค้า </h2>
        <ol class="accordion">
            <li>
            <div class="accord-header pnk-font"><span class="pnk-bg"></span>บริษัททำการจัดส่งสินค้าเมื่อไหร่</div>
            <div class="accord-content">
                หากสินค้าที่คุณสั่งซื้อมีพร้อมในคลังของเรา บริษัทจะทำการจัดส่งสินค้าออกจากคลังสินค้าภายใน 24 ชั่วโมงหลังจากคำสั่งซื้อสินค้าสมบูรณ์เรียบร้อยแล้ว หรือในเวลาที่ไม่ช้าหรือเร็วไปกว่านั้น (เฉพาะในวันทำการ) สินค้าของ Venbi ทุกรายการจัดส่งโดยระบบที่มีประสิทธิภาพสูง (Premium Courier) ซึ่งจะดำเนินการจัดส่งสินค้าในช่วงเวลา 9.00 น. – 19.00 น. และในปัจจุบันเราให้บริการจัดส่งสินค้าเฉพาะวันจันทร์ – วันเสาร์เท่านั้น
            </div>
            </li>
            <li>
            <div class="accord-header gry-font"><span class="gry-bg"></span>ระยะเวลาการจัดส่งสินค้านานเท่าไหร่ </div>
            <div class="accord-content">
                การจัดส่งสินค้าดำเนินการด้วยระบบของบริษัท ร่วมกับผู้ให้บริการจัดส่งสินค้าอื่น ๆ ซึ่งมีประสิทธิภาพสูง (Premium Courier) เพื่อให้เราแน่ใจได้ว่าสินค้าจะจัดส่งถึงมือคุณอย่างรวดเร็วที่สุด หากคุณสั่งซื้อสินค้าภายในเวลา 16.00 น. สินค้าถึงมือคุณไม่เกิน 2-3 วันทำการเฉพาะในเขตกรุงเทพ และ 3-5 วันทำการ ในเขตปริมณฑล และต่างจังหวัด ทั้งนี้บริษัทไม่อาจรับประกันได้ว่าหลังจากคุณสั่งซื้อสินค้าแล้วจะได้รับสินค้าภายในวันถัดไปเลยทันที แม้ว่าการจัดส่งสินค้าไปยังหลายพื้นที่ลูกค้าจะสามารถรับสินค้าได้ในวันรุ่งขึ้นก็ตาม และในบางพื้นที่อาจจะใช้ระยะเวลาการจัดส่งที่นานกว่านั้นก็เป็นได้
                <br><br>
                *เฉพาะ ปัตตานี ยะลา นราธิวาส แม่ฮ่องสอน เชียงราย อำเภอสังขระบุรี และบางพื้นที่ที่เป็นเกาะ อาจใช้เวลาในการจัดส่งมากกว่าปกติ 1-2 วันทำการ อย่างไรก็ตามทีมงานของเราจะจัดส่งถึงมือคุณอย่างเร็วที่สุด
            </div>
            </li>
            <li>
            <div class="accord-header pnk-font"><span class="pnk-bg"></span>วิธีการตรวจสอบและติดตามสถานะการจัดส่งสินค้า</div>
            <div class="accord-content">
                เมื่อเราทำการจัดส่งสินค้า คุณจะได้รับอีเมล์เพื่อยืนยันการจัดส่งสินค้าและแจ้งหมายเลขการจัดส่งสินค้า คุณสามารถใช้หมายเลขการจัดส่งสินค้าเพื่อตรวจสอบสถานะการจัดส่งได้ที่เว็บไซต์ของเรา (ตรวจสอบสถานะการจัดส่ง) หรือสามารถล็อกอินเพื่อดูสถานะการจัดส่งสินค้าได้ที่หน้าใบสั่งซื้อของคุณ โดยคลิกที่ปุ่ม “สถานะสินค้า” ในกรณีที่สถานะการจัดส่งสินค้าขึ้นว่าได้ “ส่งมอบสินค้าแล้ว” และคุณยังไม่ได้รับสินค้ากรุณาตรวจสอบกับเพื่อนบ้านหรือผู้ดูแลอาคารก่อนเบื้องต้น และหากสถานะการจัดส่งสินค้าขึ้นว่า “ส่งคืนบริษัท” กรุณาติดต่อเราทางอีเมล์หรือโทรศัพท์ทันที เพราะอาจจะเกิดกรณีที่อยู่จัดส่งสินค้าไม่ถูกต้อง, เกิดเหตุสุดวิสัยที่ทำให้พนักงานไม่สามารถจัดส่งสินค้าได้ หรือสินค้าสูญหายระหว่างการจัดส่ง
            </div>
            </li>
            <li>
            <div class="accord-header gry-font"><span class="gry-bg"></span>กรณีที่พนักงานไปจัดส่งสินค้าแล้วไม่มีผู้รอรับสินค้าที่บ้าน</div>
            <div class="accord-content">
                การจัดส่งสินค้าเมื่อไม่มีผู้รอรับสินค้าอยู่ที่บ้าน ขึ้นอยู่กับสถานการณ์และดุลยพินิจของพนักงานจัดส่งสินค้าว่าสามารถจะทำการวางสินค้าไว้ที่หน้าบ้านได้หรือไม่ ในกรณีที่พนักงานจัดส่งสินค้าเห็นว่าจำเป็นจะต้องมีผู้ลงชื่อรับสินค้า พนักงานจัดส่งสินค้าจะฝากสินค้าไว้ที่ผู้จัดการอาคารหรือพนักงานเฝ้าประตู โดยจะพิจารณาเลือกวิธีการที่ดีที่สุดอย่างใดอย่างหนึ่ง แต่หากไม่สามารถทำการจัดส่งสินค้าได้ สินค้าจะถูกตีคืนมาที่บริษัทซึ่งจะขึ้นสถานะการจัดส่งสินค้าว่า “ไม่สามารถจัดส่ง” คุณสามารถทำเรื่องขอคืนเงินค่าสินค้าเต็มจำนวน แต่ไม่รวมค่าจัดส่งสินค้าได้ (อ่านรายละเอียดได้ที่&nbsp;<a href="{{store url='return-policy'}}" target="_blank">นโยบายการคืนเงิน</a>&nbsp;) ในกรณีที่คำสั่งซื้อถูกปฏิเสธอย่างไม่ถูกต้อง ทางบริษัทจะไม่ทำการจัดส่งสินค้ากลับไปอีกครั้ง คุณจะต้องดำเนินการสั่งซื้อใหม่เพื่อที่จะได้รับสินค้าที่คุณต้องการ
            </div>
            </li>
            <li>
            <div class="accord-header pnk-font"><span class="pnk-bg"></span>สามารถแจ้งส่งสินค้าไปยังที่ทำงานของฉันได้หรือไม่</div>
            <div class="accord-content">
                แน่นอน เราทำการจัดส่งสินค้าในวันจันทร์- ศุกร์ เวลา 09.00 น. – 19.00 น. และวันเสาร์ เวลา 09.00 น. – 12.00 น. หากต้องการให้เราจัดส่งสินค้าไปยังที่ทำงานของคุณ สามารถกรอกที่อยู่ที่ต้องการให้เราจัดส่งในช่องสถานที่จัดส่งสินค้า หากเกิดเหตุสุดวิสัยที่คุณไม่สามารถอยู่รอรับสินค้าได้ด้วยตนเอง กรุณาตรวจสอบสินค้ากับทางพนักงานต้อนรับ, ผู้จัดการอาคาร, พนักงานเฝ้าประตูก่อนเบื้องต้น และเพื่อป้องกันความผิดพลาดในการนัดหมายรับสินค้า กรุณารอรับการติดต่อทางโทรศัพท์จากพนักงานของเราด้วย
            </div>
            </li>
            <li>
            <div class="accord-header gry-font"><span class="gry-bg"></span>ค่าบริการจัดส่งสินค้า</div>
            <div class="accord-content">
                จัดส่งฟรีเมื่อซื้อสินค้าขั้นต่ำ 500 บาทต่อการสั่งซื้อ1ครั้ง เฉพาะเขตกรุงเทพ ในส่วนของเขตปริมณฑล และต่างจังหวัด คิดค่าบริการในการจัดส่ง 100 บาท ในกรณีซื้อสินค้าต่ำกว่า 500 บาทเสียค่าจัดส่ง 100 บาทในเขตกรุงเทพ และ 200 บาทในเขตปริมณฑลและต่างจังหวัด
            </div>
            </li>
            <li>
            <div class="accord-header pnk-font"><span class="pnk-bg"></span>การจัดส่งสินค้าไปยังต่างประเทศ</div>
            <div class="accord-content">
                ตอนนี้บริษัทยังไม่มีบริการจัดส่งสินค้าไปยังต่างประเทศ
            </div>
            </li>
            <li class="last">
            <div class="accord-header gry-font"><span class="gry-bg"></span>ทำไมการจัดส่งสินค้าถึงล่าช้า</div>
            <div class="accord-content">
                หากทำการสั่งซื้อสินค้าภายในเวลา 16.00 น. ของวันทำการปกติ คุณจะได้รับสินค้าหลังจากคำสั่งซื้อสมบูรณ์แล้วภายใน 2-3 วันทำการ ซึ่งเราจะพยายามจัดส่งสินค้าอย่างเร็วที่สุดเท่าที่จะเป็นไปได้ แต่ก็อาจมีบางกรณีที่จะทำให้คุณได้รับสินค้าล่าช้ากว่ากำหนด<br>
สาเหตุที่อาจจะทำให้การจัดส่งสินค้าล่าช้า :
                <ul style="margin-left: 30px; list-style: square !important;">
                    <li style="border-bottom: 0;padding: 5px;font-weight: normal;">ข้อมูลสถานที่จัดส่งสินค้าไม่ถูกต้อง</li>
                    <li style="border-bottom: 0;padding: 5px;font-weight: normal;">ข้อมูลเบอร์โทรศัพท์ของลูกค้าไม่ถูกต้อง</li>
                    <li style="border-bottom: 0;padding: 5px;font-weight: normal;">ไม่สามารถติดต่อลูกค้าตามเบอร์โทรศัพท์ที่ให้ไว้ได้</li>
                    <li style="border-bottom: 0;padding: 5px;font-weight: normal;">ไม่มีคนอยู่รับสินค้าตามสถานที่ได้นัดหมายเอาไว้</li>
                    <li style="border-bottom: 0;padding: 5px;font-weight: normal;">แจ้งสถานที่จัดส่งสินค้าไม่เหมือนกับที่อยู่ตามใบสั่งซื้อ</li>
                    <li style="border-bottom: 0;padding: 5px;font-weight: normal;">ลูกค้าชำระเงินล่าช้าหรือมีข้อผิดพลาดในขั้นตอนการชำระเงิน</li>
                    <li style="border-bottom: 0;padding: 5px;font-weight: normal;">คำสั่งซื้อที่จำเป็นต้องหยุดชะงักหรือล่าช้า </li>อันเนื่องมาจากการสั่งซื้อสินค้าในรายการใดรายการหนึ่งเป็นจำนวนมากทำให้สินค้าในคลังของเราไม่เพียงพอต่อการจำหน่าย (เป็นกรณีที่เกิดขึ้นไม่บ่อยนัก)
                    <li style="border-bottom: 0;padding: 5px;font-weight: normal;">เหตุสุดวิสัยที่ทำให้การจัดส่งสินค้าไม่เป็นไปตามกำหนด (เช่น ภัยธรรมชาติหรือสภาพอากาศที่ผิดปกติ เป็นต้น)</li>
                </ul>
            </div>
            </li>
        </ol>
    </div>

    <div id="rewardpoints" class="subcontent">
        <h2>คะแนนสะสม</h2>
        <ol class="accordion">
            <li>
            <div class="accord-header pnk-font"><span class="pnk-bg"></span>รับเงินคืน 1% เมื่อช็อปปิ้งกับ Venbi</div>
            <div class="accord-content">
                เมื่อคุณได้สั่งซื้อของกับทาง Venbi คุณจะได้รับเงินคืนทันที 1 % โดยเงินที่ได้จะถูกเก็บไว้ที่บัญชี Venbi ของคุณ คุณสามารถนำมาใช้ได้ในครั้งต่อไปเมื่อคุณสั่งซื้อของผ่านทาง Venbi โดยสามารถใช้แทนเงินสด ทุกครั้งที่คุณต้องการใช้ คุณสารมารถกรอกจำนวนเงินในช่อง ที่หน้าสั่งซื้อสินค้า ก่อนขั้นตอนสุดท้าย
            </div>
            </li>
            <li>
            <div class="accord-header gry-font"><span class="gry-bg"></span>คุณจะได้รับแต้มจากการแนะนำเว็บไซต์ให้กับเพื่อนๆ ได้อย่างไร</div>
            <div class="accord-content">
                มีหลากหลายช่องทางที่คุณจะได้รับเครดิต 100 บาท จากการแนะนำเว็บไซต์ Venbi.com ให้กับเพื่อนๆ <a href="{{store url='rewardpoints/index/referral'}}" target="_blank">อ่านรายละเอียดเพิ่มเติมที่</a>

                <ul style="margin-left: 30px; list-style: square !important;">
                    <li style="border-bottom: 0;padding: 5px;font-weight: normal;">คุณสามารถนำลิงค์จากในหน้าบัญชีผู้ใช้ของคุณในเว็บไซต์ Venbi.com แชร์ผ่านเฟสบุ๊ค ทวิสเตอร์ หรือส่งลิงค์ให้กับเพื่อนๆ ผ่านทางอีเมล</li>
                    <li style="border-bottom: 0;padding: 5px;font-weight: normal;">เมื่อเพื่อนของคุณคลิกลิงค์ดังกล่าวและซื้อสินค้าบนเว็บไซต์ Venbi.com คุณจะได้รับทันทีเครดิต 100 บาทในบัญชีของคุณ</li>
                </ul>
เครดิตดังกล่าวสามารถใช้แทนเงินสดหรือใช้เป็นส่วนลดในการซื้อสินค้าครั้งต่อๆ ไป
            </div>
            </li>


            <li class="last">
            <div class="accord-header pnk-font"><span class="pnk-bg"></span>อายุการใช้งานของคะแนนสะสม (Reward Points)</div>
            <div class="accord-content">

                เมื่อคุณซื้อสินค้ากับทางเว็บไซต์ของเราคุณจะได้รับคะแนนสะสม (Reward Points) โดยคะแนนสะสมที่คุณได้รับจากการซื้อสินค้าแต่ละครั้งนั้นจะนำมารวมกันเมื่อคุณซื้อสินค้าในครั้งต่อไปเพิ่มขึ้นเรื่อยๆ แต่ถ้าคุณเก็บคะแนนสะสมไว้เป็นเวลานาน คะแนนบางส่วนของคุณอาจหายไปได้ เนื่องจากอายุการใช้งานของคะแนนสะสมจะมีระยะเวลาไม่เท่ากัน กล่าวคือ คะแนนที่คุณได้รับหลังจากการซื้อสินค้าแต่ละครั้งจะมีอายุการใช้งานนาน 6 เดือน
                <br><br>
                เช่น คุณซื้อสินค้าและได้คะแนนสะสมครั้งแรกในเดือนมกราคม จำนวน 20 คะแนน ต่อมาคุณซื้อสินค้าและได้คะแนนสะสมอีกครั้งในเดือนมีนาคม จำนวน 30 คะแนน คุณจะมีคะแนนสะสมรวมเป็น 50 คะแนน แต่เมื่อถึงเดือนกรกฎาคม คะแนนของคุณจะเหลือเพียง 30 คะแนน (เนื่องจาก 20 คะแนนที่ได้จากเดือนมกราคมหมดอายุในเดือนกรกฎาคม) และถ้าคุณยังไม่ได้ใช้คะแนนที่เหลืออีกภายในเดือนกันยายนคะแนนสะสมของคุณจะกลายเป็น 0 ทันที (เนื่องจาก 30 คะแนนที่ได้จากเดือนมีนาคมหมดอายุในเดือนกันยายน)
                <br><br>
                ดังนั้นเพื่อเป็นการรักษาสิทธิและผลประโยชน์ของคุณ จึงควรนำคะแนนสะสมมาใช้ภายในระยะเวลาที่กำหนด


            </div>
            </li>



        </ol>
    </div>

    <div id="returns" class="subcontent">
        <h2>การคืนสินค้าและขอคืนเงิน</h2>
        <ol class="accordion">
            <li>
            <div class="accord-header pnk-font"><span class="pnk-bg"></span>สามารถขอคืนสินค้าได้อย่างไร</div>
            <div class="accord-content">
                กรณีที่คุณต้องการคืนสินค้า กรุณาติดต่อแผนกลูกค้าสัมพันธ์และแจ้งความประสงค์ในการขอคืนสินค้าผ่านทางโทรศัพท์ 02-106-8222  หรือ อีเมล์&nbsp;<a href="mailto:support@venbi.com">support@venbi.com</a>&nbsp;หากเงื่อนไขการขอคืนสินค้าของคุณเป็นไปตามนโยบายการขอคืนสินค้าแล้ว กรุณากรอกรายละเอียดลงในแบบฟอร์มการขอคืนสินค้าที่คุณได้รับไปพร้อมกับสินค้าเมื่อตอนจัดส่ง และแนบใบกำกับภาษี (invoice) ใส่ลงในกล่องพัสดุของ venbi.com กลับมาพร้อมกันด้วย โดยจัดส่งสินค้ากลับมาให้ทางเรา ในแบบพัสดุลงทะเบียนผ่านทางที่ทำการไปรษณีย์ไทยใกล้บ้านคุณ&nbsp;
                 <br><br>
                จัดส่งสินค้ามาที่ :<br>
กรุณาจัดส่งสินค้ามาที่ :<br>
                บริษัท เอคอมเมิร์ซ จำกัด<br>
                8/2 ถนนพระราม3 ซอย53<br> แขวงบางโพงพาง เขตยานนาวา <br>กรุงเทพฯ 10120

                 <br><br>

                ทาง venbi.com จะชดเชยค่าจัดส่งแบบลงทะเบียนพร้อมกับการคืนเงินค่าสินค้า โดยทางเราขอให้คุณเก็บใบเสร็จรับเงิน/slip ค่าจัดส่งที่ได้รับจากที่ทำการไปรษณีย์เพื่อเป็นหลักฐานในการขอคืนเงินค่าส่งสินค้ากลับไว้ด้วย กรุณาส่งใบเสร็จรับเงิน/slip นี้กลับมาทางแผนกลูกค้าสัมพันธ์ โดยส่งเป็นไฟล์รูปภาพกลับมาทางอีเมล์&nbsp;<a href="mailto:support@venbi.com">support@venbi.com</a>

            </div>
            </li>
            <li>
            <div class="accord-header gry-font"><span class="gry-bg"></span>การแพ็คสินค้าเพื่อส่งคืนบริษัท</div>
            <div class="accord-content">
                เราแนะนำให้คุณทำการแพ็คสินค้าคืนด้วยกล่องหรือบรรจุภัณฑ์ลักษณะเดียวกับที่เราทำการจัดส่งไปให้ เว้นแต่ว่ามันไม่ได้อยู่ในสภาพที่ดีพอสำหรับการนำมาใช้ใหม่ อย่าลืม! ที่จะแนบป้าย/ฉลากขอคืนสินค้า (return label) กลับมาด้วย จากนั้นใช้ป้ายชื่อ/ที่อยู่ในการจัดส่งสินค้า (Shipping label) ที่ได้รับจากพนักงานหรือจากเว็บไซต์ของเราเพื่อทำการจัดส่งสินค้าคืนบริษัทได้ที่ทำการไปรษณีย์ทุกแห่งทั่วประเทศไทย
                <br><br>
หมายเหตุ : การจัดส่งสินค้ากลับคืนนั้น Venbi ไม่ได้เป็นผู้กำหนดระยะเวลาในการจัดส่งสินค้า จึงไม่สามารถบอกได้ว่าสินค้าที่ส่งคืนนั้นจะมาถึงบริษัทเมื่อไหร่
                <br><br>
                หากมีข้อสงสัยกรุณาติดต่อสอบถามเราได้ที่ 02-106-8222 หรือ อีเมล์ <a href="mailto:support@venbi.com">support@venbi.com</a>
            </div>
            </li>
            <li>
            <div class="accord-header pnk-font"><span class="pnk-bg"></span>ใช้ระยะเวลาดำเนินการคืนสินค้านานเท่าไหร่</div>
            <div class="accord-content">
                เราจะใช้เวลาดำเนินการภายใน 14 วันทำการ นับตั้งแต่วันที่ได้รับสินค้าคืนมาเรียบร้อยแล้ว
            </div>
            </li>
            <li>
            <div class="accord-header gry-font"><span class="gry-bg"></span>เมื่อไหร่ถึงจะได้รับเงินค่าสินค้าคืน</div>
            <div class="accord-content">
                เมื่อบริษัทได้รับสินค้าที่ส่งคืนเรียบร้อยแล้ว ขั้นตอนการคืนเงินจะเกิดขึ้นโดยทันที เราจะดำเนินการคืนเงินค่าสินค้ากลับไปยังบัญชีเงินฝากของคุณ กรุณารอและตรวจสอบยอดเงินหลังจากที่เราได้รับสินค้าคืนจากคุณภายใน 7 วันทำการ
            </div>
            </li>
            <li>
            <div class="accord-header pnk-font"><span class="pnk-bg"></span>ค่าใช้จ่ายในการจัดส่งสินค้าคืนบริษัท</div>
            <div class="accord-content">
               ค่าใช้จ่ายในการจัดส่งสินค้าจะแตกต่างกันไปตามสถานที่และวิธีการจัดส่งที่คุณเลือก กรุณาตรวจสอบให้แน่ใจว่าคุณได้ใช้ระบบจัดส่งแบบลงทะเบียนซึ่งดำเนินการโดยไปรษณีย์ไทย และเราจะคืนเงินค่าจัดส่งหลังจากที่ได้รับสินค้าคืนและยืนยันว่าได้รับสินค้าคืนเรียบร้อยแล้ว
            </div>
            </li>
            <li class="last">
            <div class="accord-header gry-font"><span class="gry-bg"></span>กรณีที่การจัดส่งสินค้าเกิดข้อผิดพลาด</div>
            <div class="accord-content">
                แผนกควบคุมคุณภาพของเราทำงานอย่างหนักเพื่อให้สินค้าทุกชิ้นอยู่ในสภาพที่ดีและมีคุณภาพสูงสุดเมื่อทำการจัดส่งออกจากคลังสินค้า แต่ก็มีในบางกรณีซึ่งเป็นไปได้ยากที่สินค้าของคุณจะเกิดตำหนิ, เสียหาย หรือมีข้อผิดพลาด ดังนั้นหากเกิดกรณีดังกล่าวแล้วคุณสามารถดำเนินการขอคืนสินค้าและเงินค่าสินค้าเต็มจำนวน ภายในระยะเวลา 30 วัน หลังจากที่คุณได้รับสินค้าเรียบร้อยแล้ว
            </div>
            </li>
        </ol>
    </div>
</div>

</div>
<img src="{{media url="wysiwyg/bottom-faq-venbi.jpg"}}" alt="" style="margin-top:20px;">

<script type="text/javascript">// <![CDATA[
 jQuery(document).ready(function()
 {
  var tmpUrl = "";
  var h=window.location.hash.substring(0);
  if(h == ""){
   //alert('Hash not found!!!');
   h = "#faq";
   //alert(heart);
   jQuery( "#helpMenu li:first-child" ).addClass( "helpactive");
   jQuery('.subcontent[id='+h.substring(1)+']').fadeIn();
  }else{
   jQuery('#helpMenu li a[data-id='+h.substring(1)+']').parent().addClass( "helpactive");
  jQuery('.subcontent[id='+h.substring(1)+']').fadeIn();
  }

  //////////////////////////////
  jQuery('#keyMess li').on('click','a',function()  {
   tmpUrl=jQuery(this).attr('href');
   h=tmpUrl.substring(tmpUrl.indexOf("#")+1);

   jQuery('.subcontent:visible').fadeOut(0);
   jQuery('.subcontent[id='+h+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
  });
  jQuery('#zyncNav li').on('click','a',function()
  {
   tmpUrl=jQuery(this).attr('href');
   h=tmpUrl.substring(tmpUrl.indexOf("#")+1);
   jQuery('.subcontent:visible').fadeOut(0);
   jQuery('.subcontent[id='+h+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
  });
  jQuery('#footerBanner div').on('click','a',function()
  {
   tmpUrl=jQuery(this).attr('href');
   h=tmpUrl.substring(tmpUrl.indexOf("#")+1);
   jQuery('.subcontent:visible').fadeOut(0);
   jQuery('.subcontent[id='+h+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
  });
  jQuery('#topMklink').on('click','a',function()
  {
   tmpUrl=jQuery(this).attr('href');
   h=tmpUrl.substring(tmpUrl.indexOf("#")+1);
   jQuery('.subcontent:visible').fadeOut(0);
   jQuery('.subcontent[id='+h+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
  });
  jQuery('#footer_help li').on('click','a',function()
  {
   tmpUrl=jQuery(this).attr('href');
   h=tmpUrl.substring(tmpUrl.indexOf("#")+1);
   jQuery('.subcontent:visible').fadeOut(0);
   jQuery('.subcontent[id='+h+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
  });

//////

  jQuery('li.inpage-link').on('click','a',function()
  {
   tmpUrl=jQuery(this).attr('href');
   h=tmpUrl.substring(tmpUrl.indexOf("#")+1);
   jQuery('.subcontent:visible').fadeOut(0);
   jQuery('.subcontent[id='+h+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
  });
 ///////////////////////////////

     jQuery('#helpMenu').on('click','a',function()
     {
         jQuery('.subcontent:visible').fadeOut(0);
         jQuery('.subcontent[id='+jQuery(this).attr('data-id')+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery(this).parent().addClass( "helpactive");
     });

///////////////////////////////

     jQuery(".accordion li .accord-header").mouseenter(function() {
            jQuery(this).css("cursor","pointer");
        });
        jQuery(".accordion li .accord-header").click(function() {
            jQuery(this).find("span").removeClass('active');
          if(jQuery(this).next("div").is(":visible")){
            jQuery(this).next("div").slideUp(300);
          } else {
            jQuery(".accordion li .accord-content").slideUp(400);
            jQuery(this).next("div").slideToggle(400);
            jQuery(".accordion li .accord-header").find("span").removeClass('active');
            jQuery(this).find("span").addClass('active');
          }
        });
      });
// ]]></script>
EOD;
/** @var Mage_Cms_Model_Page $page */
$page = Mage::getModel('cms/page')->getCollection()
    ->addStoreFilter($stores, $withAdmin = false)
    ->addFieldToFilter('identifier', $identifier)
    ->getFirstItem();
if ($page->getId()) $page->delete(); // if exists then delete
$page = Mage::getModel('cms/page');
$page->setTitle($title);
$page->setIdentifier($identifier);
$page->setStores($stores);
$page->setIsActive($isActive);
$page->setRootTemplate($rootTemplate);
$page->setSortOrder($sortOrder);
$page->setContent($content);
$page->setContentHeading($contentHeading);
$page->setMetaDescription($metaDescription);
$page->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// Help page (Sanoga English)
//==========================================================================
$title = "Help";
$stores = array($sen);
$metaDescription = "";
$identifier = "help";
$isActive = 1;
$rootTemplate = "one_column";
$sortOrder = 0;
$contentHeading = "Help";
$content = <<<EOD
<div>
<ul id="helpMenu">
<li><a href="javascript:void(0)" data-id="faq">FAQ</a></li>
<li><a href="javascript:void(0)" data-id="howtoorder">How To Order</a></li>
<li><a href="javascript:void(0)" data-id="payments">Payments</a></li>
<li><a href="javascript:void(0)" data-id="shipping">Shipping</a></li>
<li><a href="javascript:void(0)" data-id="rewardpoints">Reward Points</a></li>
<li><a href="javascript:void(0)" data-id="returns">Cancellation and Returns</a></li>
</ul>
</div>
<div class="helpContent">
<div id="faq" class="subcontent">
<h2>FAQ</h2>
<ol class="accordion">
<li>
<div class="accord-header">Who is Sanoga?</div>
<div class="accord-content">Sanoga is an online health store with a mission to offer what&rsquo;s best for your health and deliver it straight to you. We focus on a broad selection of health essentials, great logistics, and customer service to impress our local pet community. Our goal is to help you save time from travelling, popping in and out of health store to buy your health essentials. At Sanoga, we do the legwork for you. Our teams of health experts are all dedicated to you and your health.</div>
</li>
<li>
<div class="accord-header">How did Sanoga come about?</div>
<div class="accord-content">Sanoga aims to make looking after your health easier. As health experts, we want convenience in buying the best quality products without all the hassles. We decided to combine our passion with our vast ecommerce knowledge to offer health-conscious community a great service.</div>
</li>
<li>
<div class="accord-header">Where is Sanoga based?</div>
<div class="accord-content">We are based at<br /> Sethiwan Tower - 19th Floor<br /> 139 Pan Road, Silom, Bangrak,<br /> Bangkok, 10500 Thailand<br /> Tel: 02-106-8222 <br /> Email <a href="mailto:support@sanoga.com">support@sanoga.com</a></div>
</li>
<li>
<div class="accord-header">If I were interested in partnering with Sanoga, how would I go about it?</div>
<div class="accord-content">The company welcomes any and all business ventures and ideas that you might have that will ultimately add value to our members. To discuss this further, please contact us on <a href="mailto:support@sanoga.com">support@sanoga.com</a></div>
</li>
<li>
<div class="accord-header">Free Shipping Promotion</div>
<div class="accord-content">Free Delivery anywhere in Bangkok with the minimun purchase of 500 baht for each order
<ul style="margin-left: 30px; list-style: square !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">฿100 flat fee for shipping nationwide exclude Bangkok</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Flat shipping fee of ฿100 will be applied to the order less than ฿500 in Bangkok and ฿200 for nationwide exclude Bangkok</li>
</ul>
</div>
</li>
<li>
<div class="accord-header">I forgot my password</div>
<div class="accord-content">If you have forgotten your password, don&rsquo;t worry. We have systems in place to help you recover them. <a href="{{store url='forgot-password'}}" target="_blank">Click here</a> for more information.</div>
</li>
<li>
<div class="accord-header">The item I want is out of stock. What now?</div>
<div class="accord-content">Although we try to maintain a stock level that matches expected demand, highly popular items may sometimes be out of stock. Please contact our Customer Service team, who will quickly find a solution for you. Happy and healthy customers are important to us.</div>
</li>
<li>
<div class="accord-header">Can I place an item on hold for purchase at a later date?</div>
<div class="accord-content">Sorry, but we do not offer this option. Our goal is to ensure all customers can immediately purchase our great products.</div>
</li>
<li>
<div class="accord-header">How do I know if you carry a certain brand?</div>
<div class="accord-content">You can use the search function, which is available on the top right of the page or go through our brands list buy clicking on either dog or cat.</div>
</li>
<li>
<div class="accord-header">When can I expect new products to be listed on your website?</div>
<div class="accord-content">We constantly update our products with new lines and new brands. Please visit our website to see the latest items we have in stock.</div>
</li>
<li class="last">
<div class="accord-header">Is there somewhere I can go to view the product prior to purchasing?</div>
<div class="accord-content">Unfortunately, we do not have a retail store and for safety reasons we cannot allow customers in the warehouse. If you have any questions regarding the products, please do not hesitate to contact our Customer Service team.</div>
</li>
</ol></div>
<div id="howtoorder" class="subcontent">
<h2>How To Order</h2>
<ol class="accordion">
<li>
<div class="accord-header">How do I order?</div>
<div class="accord-content">Ordering at Sanoga.com is very easy. The steps can be found <a href="{{store url='how-to-order'}}" target="_blank">here</a>.</div>
</li>
<li>
<div class="accord-header">How do I register a new account?</div>
<div class="accord-content">Please click on 'Register', which is located on the top right hand side of the page. You will then be prompted to a new page where you will find 'New Customer' on the left hand side. Fill in your details as requested before clicking 'submit'. Registration should now be complete and you should receive a confirmation e-mail to the address you registered with.</div>
</li>
<li>
<div class="accord-header">Is there any registration fee for Sanoga?</div>
<div class="accord-content">Registration at Sanoga is completely free and easy! Any form of charge will only come from purchasing of Sanoga products.</div>
</li>
<li>
<div class="accord-header">I need personal assistance with my order. Who can I contact?</div>
<div class="accord-content">Our Customer Service team will be happy to assist you. Please e-mail us at <a href="mailto:support@sanoga.com">support@sanoga.com</a> or call us at 02-106-8222.</div>
</li>
<li class="last">
<div class="accord-header">Do you take orders over the phone?</div>
<div class="accord-content">Yes. We do take orders over phone. The payment mode possible for these orders is Cash On Delivery only. Click here for more details.</div>
</li>
</ol></div>
<div id="payments" class="subcontent">
<h2>Payments</h2>
<ol class="accordion">
<li>
<div class="accord-header">Is my credit card information safe at Sanoga?</div>
<div class="accord-content">The company will not store any credit card information on our servers. At Sanoga, we use systems set in place by banks that have the security protocols set in place to handle safe credit card transactions.</div>
</li>
<li>
<div class="accord-header">My credit card details are not being accepted. What's wrong?</div>
<div class="accord-content">Please check with your bank or financial institution to rule out errors on their behalf. If problems continue to persist, please contact Customer Service who will notify the IT department of technical difficulties.</div>
</li>
<li>
<div class="accord-header">My computer froze while processing payment. How will I know that my payment went through successfully?</div>
<div class="accord-content">All successful transactions will receive a confirmation email that contains an order tracking number. If you have not received confirmation via email, please try placing your order again. Alternatively, please contact our Customer Service team to confirm the placement of your order.</div>
</li>
<li>
<div class="accord-header">How many payment methods does Sanoga have?</div>
<div class="accord-content">
At Sanoga, we provide customers the ability to pay using the following facilities:
<ul style="margin-left: 30px; list-style: square !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Credit card and Debit card</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">PayPal</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Cash on delivery (pay when we deliver the product)</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Installment Plan</li>
</ul><br/>
Enjoy up to 10 months installment on a minimum purchase of 3,500฿ with MOXY allied bank<br/><br/>
Bangkok Bank credit card<br/><br/>
<ul style="margin-left: 30px; list-style: disc !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">4-Month plan +0.8%</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">6-Month plan +0.8%</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">10-Month plan +0.8%</li>
</ul>
<br/>
Citibank credit card<br/><br/>
<ul style="margin-left: 30px; list-style: disc !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">4-Month plan +0.89%</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">6-Month plan +0.89%</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">10-Month plan +0.89%</li>
</ul>
<br/>
KTC credit card<br/><br/>
<ul style="margin-left: 30px; list-style: disc !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">4-Month plan +0.89%</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">6-Month plan +0.89%</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">10-Month plan +0.89%</li>
</ul>
<br/>
Thanachart credit card<br/><br/>
<ul style="margin-left: 30px; list-style: disc !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">4-Month plan +0.8%</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">6-Month plan +0.8%</li>
</ul>
<br/>
* condition<br/>
Each installment purchase may only comprise of purchases made under the same invoice<br/>
The minimum purchase does not include discount and shipping fee<br/>
Interest rate is based on the authorized bank<br/>
The installment purchase cannot be refunded<br/>
MOXY reserves the right to amend and cancel orders that without prior notice<br/>
</div>
</li>
<li>
<div class="accord-header">Are your prices in Thai Baht (THB)?</div>
<div class="accord-content">All pricing is in Thai Baht.</div>
</li>
<li class="last">
<div class="accord-header">Does your prices include taxes?</div>
<div class="accord-content">All prices are inclusive of taxes.</div>
</li>
</ol></div>
<div id="shipping" class="subcontent">
<h2>Shipping</h2>
<ol class="accordion">
<li>
<div class="accord-header">When will my order ship?</div>
<div class="accord-content">All items are shipped within 24 hours after the purchase is made and most of the time even sooner (weekdays only). All Sanoga deliveries are sent via Premium Courier. The deliveries are made between 9:00 am - 7:00 pm. Currently, we deliver from Monday to Saturday.</div>
</li>
<li>
<div class="accord-header">How long will it take until I receive my order?</div>
<div class="accord-content">We ship via our own delivery fleet and several other premium carriers. If you order by 4 pm on a business day, you will receive it in 2-3 business days. We do not yet guaranteed overnight shipping, although many areas of the country will receive their orders the next day. <br /><br /> * For Sangkhlaburi district, islands and provinces including Pattani, Yala, Naradhiwas, Mae Hong Son and Chiang Rai, delivery may take 1-2 days longer due to difficult routes. We&rsquo;ll try our best to deliver the product to you as soon as possible.</div>
</li>
<li>
<div class="accord-header">How do I track my order?</div>
<div class="accord-content">When your order ships, you will receive an email with the shipping and tracking information. You can use your tracking number on our website to trace your order. You can also find your tracking number with your order details; Click the "Order Status" button under the "My Account" menu after you have logged on. <br /><br /> Please check with your neighbors or building manager if you have not received your product even though the tracking says that it has been delivered. If the tracking information doesn't show up or shows that the item has been returned to us, please contact us by email or phone. It is possible that we may have the wrong shipping address, the driver may have unsuccessfully attempted to deliver, or the shipping carrier may have misplaced the product.</div>
</li>
<li>
<div class="accord-header">What happens if I am not at home when my order is being delivered?</div>
<div class="accord-content">In the process of delivering the product to you, our driver will contact you on the number provided to ensure that you are at the address that you have given to us. If you are not home at the time, do inform someone else in your household to beaware of our delivery so they can sign for the product on your behalf. IIf we are still unable to deliver the product to you despite our best efforts, the money paid for the product will be returned to you, but not the shipping fee (For more detail please check our &ldquo;<a href="{{store url='return-policy'}}" target="_blank">refund policy</a>&rdquo;). Undeliverable orders will not be resent and you must place a new order.</div>
</li>
<li>
<div class="accord-header">Can I have my order shipped to my office?</div>
<div class="accord-content">We are happy to delivery to your office. If you would like us to do that, please input your office address at checkout. Your office delivery will be made from the 9am to 7pm on weekdays and 9am to 12 noon on weekends.</div>
</li>
<li>
<div class="accord-header">How much is the shipping fee?</div>
<div class="accord-content">Free Delivery anywhere in Bangkok with the minimun purchase of 500 baht for each order
<ul style="margin-left: 30px; list-style: square !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">฿100 flat fee for shipping nationwide exclude Bangkok</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Flat shipping fee of ฿100 will be applied to the order less than ฿500 in Bangkok and ฿200 for nationwide exclude Bangkok</li>
</ul>
</div>
</li>
<li>
<div class="accord-header">Do you ship outside of Thailand?</div>
<div class="accord-content">Sorry, we currently do not ship outside of Thailand.</div>
</li>
<li class="last">
<div class="accord-header">Why is my shipment delayed?</div>
<div class="accord-content">You must order by 4 p.m. for 1-2-day shipping to apply. We try our best to make sure that you receive your order as soon as possible, however, there are some situations beyond our control that can delay your shipment.<br />The following orders may result in delayed shipping:
<ul style="margin-left: 30px; list-style: square !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Incorrect shipping address</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Incorrect Mobile Phone number</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Unable to reach you at provided contact number</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Being absent after courier made an appointment</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Shipping address does not match billing address</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Payment delay or issue</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Courier failure to deliver (e.g. severe weather conditions)</li>
</ul>
</div>
</li>
</ol></div>
<div id="rewardpoints" class="subcontent">
<h2>Reward Points</h2>
<ol class="accordion">
<li>
<div class="accord-header">1% Cash-back policy</div>
<div class="accord-content">A 1% cash back means that for every purchase made, you will receive 1% cash-back, which will be stored in your Sanoga account. You can then use this credit as a discount on your next purchase.</div>
</li>
<li>
<div class="accord-header">How do I get points for referring a friend?</div>
<div class="accord-content">There are many ways where you can get ฿100 by <a href="{{store url='rewardpoints/index/referral'}}" target="_blank">referring a friend</a>at Sanoga.com
<ul style="margin-left: 30px; list-style: square !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">You can share your unique link which can be found in My Accounts straight to your Facebook or Twitter page. You can also email this link straight to your friends.</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Once your friends have clicked on the link and have made a purchase on Sanoga, you will automatically be credited with ฿100 in your account in the form of credits.</li>
</ul>
</div>
</li>
<li class="last">
<div class="accord-header">Reward Points Terms &amp; Conditions</div>
<div class="accord-content">Each time you shop on our website, you will earn Reward Points on your purchases. The Reward Points will be accumulated each time you purchase the products on our website and it will be expired within 6 months from your purchasing date. <br /><br /> For example, if you earn 20 points on January from your first purchase and another 30 points on March, your Reward Point balance is 50 points. On July, your balance will be deducted to 30 points because 20 points on January are already expired. In case you have not used your point within September, you point balance will become 0 as your points earned previously on March are expired. <br /><br /> For your own interest, please kindly redeem your Reward Points in the available period of time.</div>
</li>
</ol></div>
<div id="returns" class="subcontent">
<h2>Cancellation and Returns</h2>
<ol class="accordion">
<li>
<div class="accord-header">How do I return my purchase?</div>
<div class="accord-content"><strong>Returns are FREE, fast and easy!</strong><br /> If you wish to return an item, please contact our customer care and request a return of the product(s) via phone 02-106-8222 or Email <a href="mailto:support@sanoga.com">support@sanoga.com</a>. If the conditions of the returned product aligned within our return policy, please fill in the Returns Slip you received with your shipment, enclose the invoice in the package then send it back to us in its original packaging. Please send the product back to Sanoga.com at any Thailand Post location nearby your premise under domestic registered mail service. <br /><br /> NOTE: Sorry for any inconveniences may cause you, return shipments are not eligible for scheduled pick-ups by Sanoga.com. <br /><br /> Our address is:<br /> Acommerce Co.,Ltd<br /> 8/2 Rama3 Rd, Soi 53, <br />Bang Phong Pang, Yarn-Nava, <br />Bangkok 10120<br /><br /> Sanoga.com will absorb the incurred shipping costs in this registered returns back to you on the refund process. We ask that you keep the receipt/slip of this shipping cost issued by the post office as an evidence. And please send your receipt/slip back to Customer Care by submitting a photo file to the email <a href="mailto:support@sanoga.com">support@sanoga.com</a></div>
</li>
<li>
<div class="accord-header">Can I cancel my order?</div>
<div class="accord-content">If you wish to cancel your order, get in touch with customer service as soon as possible with your order number. If your order has been dispatched, please do not accept delivery and contact customer service. In case of Prepaid orders Refunds will be processed as per our Return policies. Any cashback credits used in the purchase of the order will be credited back to your account.</div>
</li>
<li>
<div class="accord-header">Packing your return</div>
<div class="accord-content">We suggest that you use the boxes that the products arrived in. Simply apply the shipping label acquired through the online return process or from our Customer Service team. Please put the return label to your box/package and drop it off at any Thailand Post location. NOTE: Return shipments are not eligible for scheduled pick-ups by Sanoga. Please contact us with any questions or concerns at 02-106-8222 or send an e-mail to <a href="mailto:support@sanoga.com">support@sanoga.com</a></div>
</li>
<li>
<div class="accord-header">How long will it take for my return to be processed?</div>
<div class="accord-content">Once we receive the item(s), we will process your request within 14 business days.</div>
</li>
<li>
<div class="accord-header">When do I receive my refund?</div>
<div class="accord-content">Once we receive your item(s) and have checked that it is in original condition, a refund will be initiated immediately. Please allow up to 7 business days to receive your refund from the time we receive your return. Sanoga will refund your money through Bank Transfer or in store credit, if preferred.</div>
</li>
<li>
<div class="accord-header">What are the shipping charges if I return my purchased items?</div>
<div class="accord-content">The shipping costs will vary according to your location and method of postage you choose. Please make sure to ship back to us via Thailand post by registered mail and we will refund the shipping fees after receiving the product.</div>
</li>
<li class="last">
<div class="accord-header">What if my item has a defect?</div>
<div class="accord-content">Our Quality Control department tries to ensure that all products are of a high quality when they leave the warehouse. In the rare circumstance that your item has a defect, you may send it back to us with a completed Returns Slip attached within 30 days upon receiving the item(s), a full refund will be issued to you.</div>
</li>
</ol></div>
</div>
<script type="text/javascript">// <![CDATA[
    jQuery(document).ready(function()
    {
        var tmpUrl = "";
        var h=window.location.hash.substring(0);
        if(h == ""){
            //alert('Hash not found!!!');
            h = "#faq";
            //alert(heart);
            jQuery( "#helpMenu li:first-child" ).addClass( "helpactive");
            jQuery('.subcontent[id='+h.substring(1)+']').fadeIn();
        }else{
            jQuery('#helpMenu li a[data-id='+h.substring(1)+']').parent().addClass( "helpactive");
            jQuery('.subcontent[id='+h.substring(1)+']').fadeIn();
        }

        //////////////////////////////
        jQuery('#keyMess li').on('click','a',function()  {
            tmpUrl=jQuery(this).attr('href');
            h=tmpUrl.substring(tmpUrl.indexOf("#")+1);

            jQuery('.subcontent:visible').fadeOut(0);
            jQuery('.subcontent[id='+h+']').fadeIn();
            jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
            jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
        });
        jQuery('#zyncNav li').on('click','a',function()
        {
            tmpUrl=jQuery(this).attr('href');
            h=tmpUrl.substring(tmpUrl.indexOf("#")+1);
            jQuery('.subcontent:visible').fadeOut(0);
            jQuery('.subcontent[id='+h+']').fadeIn();
            jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
            jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
        });
        jQuery('#footerBanner div').on('click','a',function()
        {
            tmpUrl=jQuery(this).attr('href');
            h=tmpUrl.substring(tmpUrl.indexOf("#")+1);
            jQuery('.subcontent:visible').fadeOut(0);
            jQuery('.subcontent[id='+h+']').fadeIn();
            jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
            jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
        });
        jQuery('#topMklink').on('click','a',function()
        {
            tmpUrl=jQuery(this).attr('href');
            h=tmpUrl.substring(tmpUrl.indexOf("#")+1);
            jQuery('.subcontent:visible').fadeOut(0);
            jQuery('.subcontent[id='+h+']').fadeIn();
            jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
            jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
        });
        jQuery('#footer_help li').on('click','a',function()
        {
            tmpUrl=jQuery(this).attr('href');
            h=tmpUrl.substring(tmpUrl.indexOf("#")+1);
            jQuery('.subcontent:visible').fadeOut(0);
            jQuery('.subcontent[id='+h+']').fadeIn();
            jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
            jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
        });
        ///////////////////////////////

        jQuery('#helpMenu').on('click','a',function()
        {
            jQuery('.subcontent:visible').fadeOut(0);
            jQuery('.subcontent[id='+jQuery(this).attr('data-id')+']').fadeIn();
            jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
            jQuery(this).parent().addClass( "helpactive");
        });

///////////////////////////////

        jQuery(".accordion li .accord-header").mouseenter(function() {
            jQuery(this).css("cursor","pointer");
        });
        jQuery(".accordion li .accord-header").click(function() {
            jQuery(this).find("span").removeClass('active');
            if(jQuery(this).next("div").is(":visible")){
                jQuery(this).next("div").slideUp(300);
            } else {
                jQuery(".accordion li .accord-content").slideUp(400);
                jQuery(this).next("div").slideToggle(400);
                jQuery(".accordion li .accord-header").find("span").removeClass('active');
                jQuery(this).find("span").addClass('active');
            }
        });
    });
// ]]></script>
EOD;
/** @var Mage_Cms_Model_Page $page */
$page = Mage::getModel('cms/page')->getCollection()
    ->addStoreFilter($stores, $withAdmin = false)
    ->addFieldToFilter('identifier', $identifier)
    ->getFirstItem();
if ($page->getId()) $page->delete(); // if exists then delete
$page = Mage::getModel('cms/page');
$page->setTitle($title);
$page->setIdentifier($identifier);
$page->setStores($stores);
$page->setIsActive($isActive);
$page->setRootTemplate($rootTemplate);
$page->setSortOrder($sortOrder);
$page->setContent($content);
$page->setContentHeading($contentHeading);
$page->setMetaDescription($metaDescription);
$page->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// Help page (Sanoga Thai)
//==========================================================================
$title = "วิธีการสั่งซื้อ และคำถามที่พบบ่อย";
$stores = array($sth);
$metaDescription = "คำถามที่พบบ่อย เกี่ยวกับวิธีการการสั่งซื้อ การชำระเงิน และการจัดส่งสินค้า ตลอดจนนโยบายของ Sanoga.com ในการให้บริการลูกค้า";
$identifier = "help";
$isActive = 1;
$rootTemplate = "one_column";
$sortOrder = 0;
$contentHeading = "ช่วยเหลือ";
$content = <<<EOD
<div>
<ul id="helpMenu">
<li><a href="javascript:void(0)" data-id="faq">คำถามที่พบบ่อย</a></li>
<li><a href="javascript:void(0)" data-id="howtoorder">วิธีการการสั่งซื้อ</a></li>
<li><a href="javascript:void(0)" data-id="payments">การชำระเงิน</a></li>
<li><a href="javascript:void(0)" data-id="shipping">การจัดส่งสินค้า</a></li>
<li><a href="javascript:void(0)" data-id="rewardpoints">คะแนนสะสม</a></li>
<li><a href="javascript:void(0)" data-id="returns">การคืนสินค้าและขอคืนเงิน</a></li>
</ul>
</div>
<div class="helpContent">
<div id="faq" class="subcontent">
<h2>FAQ</h2>
<ol class="accordion">
<li>
<div class="accord-header">Sanoga คืออะไร</div>
<div class="accord-content">Sanoga เป็นศูนย์รวมสินค้าเพื่อสุขภาพเพื่อสมาชิกทุกคนในครอบครัว โดยมีระบบที่ได้รับการพัฒนาเป็นอย่างดี ซึ่งจะช่วยให้ลูกค้าสามารถเลือกซื้อสินค้าได้ตลอด 24 ชั่วโมง ไม่เสียเวลาในการเดินทางและตามหาสินค้าที่ต้องการ</div>
</li>
<li>
<div class="accord-header">Sanoga มีที่มาอย่างไร</div>
<div class="accord-content">ที่ Sanoga เราเชื่อว่า การดูแลสุขภาพเป็นหนึ่งในสิ่งที่สำคัญที่สุดในชีวิต จึงไม่ควรที่จะทำให้เป็นเรื่องยาก ไหนจะต้องฝ่ารถติดเป็นชั่วโมงๆ เพื่อเดินทางไปซื้อ เดินเข้าร้านนู้นออกร้านนี้ เพียงเพื่อหาสินค้าที่ต้องการเพื่อสุขภาพของคุณ เราจึงมีไอเดียดีๆ ที่จะช่วยคุณประหยัดเวลาและลดความยุ่งยากโดยการจัดหาสิ่งที่คุณต้องการมาให้..โดยคุณไม่ต้องออกแรง เราเฟ้นหาสินค้าเพื่อสุขภาพที่ดีที่สุด พร้อมกับให้ข้อมูลเพื่อช่วยให้คุณตัดสินใจได้ว่าอะไรเหมาะกับคุณที่สุด เรามีสินค้าจากแบรนด์ที่เป็นที่รู้จักจากทั้งในและต่างประเทศ ทั้งยังสามารถส่งตรงถึงมือคุณได้ภายใน 1-2 วัน</div>
</li>
<li>
<div class="accord-header">Sanoga ตั้งอยู่ที่ไหน</div>
<div class="accord-content">บริษัทตั้งอยู่ที่ อาคารเศรษฐีวรรณ ชั้น 19 <br /> 139 ถนนปั้น แขวงสีลม เขตบางรัก <br /> กรุงเทพฯ ประเทศไทย 10500 <br /> โทรศัพท์: 02-106-8222<br /> อีเมล์ <a href="mailto:support@sanoga.com">support@sanoga.com</a></div>
</li>
<li>
<div class="accord-header">สนใจอยากร่วมธุรกิจกับ Sanoga ทำอย่างไร</div>
<div class="accord-content">บริษัทยินดีต้อนรับผู้ที่สนใจเข้าเป็นส่วนหนึ่งกับเราได้ โดยท่านที่สนใจนำเสนอสินค้าเพื่อสุขภาพ คุณภาพสูงผ่านทางหน้าเว็บไซต์ Sanoga สามารถติดต่อได้ที่อีเมล <a href="mailto:support@sanoga.com">support@sanoga.com</a></div>
</li>
<li>
<div class="accord-header">โปรโมชั่นจัดส่งสินค้าฟรี</div>
<div class="accord-content">จัดส่งฟรีเมื่อซื้อสินค้าขั้นต่ำ 500 บาทต่อการสั่งซื้อ1ครั้ง เฉพาะเขตกรุงเทพ ในส่วนของเขตปริมณฑล และต่างจังหวัด คิดค่าบริการในการจัดส่ง 100 บาท ในกรณีซื้อสินค้าต่ำกว่า 500 บาทเสียค่าจัดส่ง 100 บาทในเขตกรุงเทพ และ 200 บาทในเขตปริมณฑลและต่างจังหวัด</div>
</li>
<li>
<div class="accord-header">วิธีขอรหัสผ่านใหม่</div>
<div class="accord-content">ถ้าคุณลืมรหัสผ่านของคุณ คุณสามารถขอรหัสผ่านใหม่ได้ <a href="{{store url='forgot-password'}}" target="_blank">คลิกที่นี่</a>เพื่อดูรายละเอียดเพิ่มเติม</div>
</li>
<li>
<div class="accord-header">ถ้าสินค้าที่ต้องการหมด จะทำอย่างไร</div>
<div class="accord-content">ถึงแม้ว่าทางเราจะพยายามจัดหาสินค้าให้มีจำนวนเหมาะสมกับระดับความต้องการของลูกค้า แต่สินค้าที่ได้รับความนิยมอย่างมากก็อาจหมดลงได้อย่างรวดเร็ว แต่เราพยายามไม่ให้ปัญหานี้เกิดขึ้น หรือถ้าเกิดขึ้นก็จะพยายามหาทางแก้ไข และหาทางออกให้กับลูกค้าอย่างแน่นอน ถ้ามีข้อสงสัย หรือปัญหาเกี่ยวกับสินค้าสามารถติดต่อได้ที่ฝ่ายลูกค้าสัมพันธ์ได้ในเวลาทำการ</div>
</li>
<li>
<div class="accord-header">ฉันสามารถจองสินค้าไว้ก่อนเพื่อทำการสั่งซื้อในวันถัดไปได้หรือไม่</div>
<div class="accord-content">ต้องขออภัย ทางเราไม่มีนโยบายการจองสินค้า</div>
</li>
<li>
<div class="accord-header">ฉันจะทราบได้อย่างไรว่ามีแบรนด์ไหนที่ขายอยู่บนเว็บไซต์ Sanoga บ้าง</div>
<div class="accord-content">คุณสามารถค้นหาสินค้าที่ต้องการได้ โดยกดปุ่มค้นหาที่หน้าเว็บไซต์ Sanoga ซึ่งอยู่ด้านบนของหน้าเว็บไซต์ หรือคุณสามารถเข้าไปคลิกเลือกแบรนด์ต่าง ๆ ได้ บนแถบเมนู</div>
</li>
<li>
<div class="accord-header">จะทราบได้อย่างไร ว่ามีสินค้าใหม่เข้ามาในเวบไซต์</div>
<div class="accord-content">ทางเราจะทำการอัพเดทเวบไซต์ใหม่ทันที ที่มีสินค้าใหม่เข้ามาในคลังสินค้า ท่านสามารถตรวจสอบสินค้าใหม่ได้ที่เวบไซต์ของเรา</div>
</li>
<li class="last">
<div class="accord-header">ฉันสามารถแสดงความคิดเห็น หรือให้ข้อแนะนำเกี่ยวกับสินค้าได้อย่างไร</div>
<div class="accord-content">เรายินดีที่รับฟังความคิดเห็น และข้อเสนอแนะเกี่ยวกับผลิตภัณฑ์ของเรา คุณสามารถแสดงความคิดเห็นของคุณมาผ่านมางทางอีเมล <a href="mailto:support@sanoga.com">support@sanoga.com</a></div>
</li>
</ol></div>
<div id="howtoorder" class="subcontent">
<h2>วิธีการการสั่งซื้อ</h2>
<ol class="accordion">
<li>
<div class="accord-header">สั่งซื้อสินค้าผ่าน Sanoga อย่างไร?</div>
<div class="accord-content">คุณสามารถสั่งซื้อสินค้ากับ Sanoga.com ด้วยวิธีง่ายๆ <a href="{{store url='how-to-order'}}" target="_blank">คลิกที่นี่</a>เพื่อดูขั้นตอนการสั่งซื้อ</div>
</li>
<li>
<div class="accord-header">สมัครเพื่อลงทะเบียนบัญชีผู้ใช้ใหม่อย่างไร?</div>
<div class="accord-content">เพียงคลิกไปที่ &ldquo;ลงทะเบียน&rdquo; ซึ่งอยู่ด้านบนขวามือของเว็บไซต์ หลังจากนั้นจะเปลี่ยนไปหน้าสร้างบัญชีผู้ใช้ใหม่ซึ่งจะอยู่ทางด้านซ้ายมือ หลังจากกรอกรายละเอียดข้อมูลของท่านเสร็จเรียบร้อยแล้วคลิกที่ &ldquo;ยืนยัน&rdquo; เพื่อเสร็จสิ้นการสมัครสมาชิก หลังจากนั้นคุณจะได้รับอีเมลยืนยันการลงทะเบียนผู้ใช้ใหม่กับทาง Sanoga เพียงเท่านี้คุณก็สามารถซื้อสินค้าผ่านทางเราได้อย่างสะดวกสบาย</div>
</li>
<li>
<div class="accord-header">มีค่าธรรมเนียมในการสมัครสมาชิกกับ Sanoga หรือไม่</div>
<div class="accord-content">การสมัครสมาชิกไม่มีค่าธรรมเนียมใด ๆ ทั้งสิ้น และจะเกิดค่าใช้จ่ายในกรณีที่มีการสั่งซื้อสินค้าเท่านั้น</div>
</li>
<li>
<div class="accord-header">ถ้าฉันต้องการที่ปรึกษาส่วนตัวสำหรับการสั่งซื้อสามารถติดต่อได้ทางใด</div>
<div class="accord-content">แผนกลูกค้าสัมพันธ์ของเรายินดีเป็นอย่างยิ่งที่จะช่วยเหลือท่านผ่านทางอีเมล <a href="mailto:support@sanoga.com">support@sanoga.com</a> หรือทางหมายเลยโทรศัพท์ 02-106-8222</div>
</li>
<li class="last">
<div class="accord-header">ฉันสามารถสั่งซื้อสินค้าผ่านทางโทรศัพท์ได้หรือไม่?</div>
<div class="accord-content">คุณสามารถสั่งซื้อสินค้าผ่านทางโทรศัพท์ได้ค่ะ เพียงโทรเข้าไปที่แผนกลูกค้าสัมพันธ์ตามหมายเลขที่แจ้งไว้ และต้องชำระค่าสินค้าด้วยวิธีเก็บเงินปลายทางเท่านั้น คลิกที่นี่เพื่อดูรายละเอียดเพิ่มเติม</div>
</li>
</ol></div>
<div id="payments" class="subcontent">
<h2>การชำระเงิน</h2>
<ol class="accordion">
<li>
<div class="accord-header">การใช้บัตรเครดิตกับทาง Sanoga ปลอดภัยหรือไม่</div>
<div class="accord-content">บริษัทดูแลข้อมูลของสมาชิก Sanoga เป็นอย่างดี ข้อมูลทุกอย่างจะถูกเก็บไว้เป็นความลับและดูแลรักษาอย่างปลอดภัย ด้านข้อมูลเกี่ยวกับบัตรเครดิตทั้งหมดจะถูกส่งผ่านระบบ SSL (Secure Socket Layer) ซึ่งเป็นระบบรักษาความปลอดภัยมาตรฐานในการส่งข้อมูลทางอินเตอร์เน็ตตรงไปยังระบบเซฟอิเล็กทรอนิกส์ของธนาคาร โดยจะไม่มีการเก็บข้อมูลเกี่ยวกับบัตรเครดิตไว้ที่ระบบเซิร์ฟเวอร์ของเว็บไซต์แต่อย่างใด</div>
</li>
<li>
<div class="accord-header">ข้อมูลบัตรเครดิตไม่สามารถใช้ได้ เกิดจากอะไร</div>
<div class="accord-content">กรุณาตรวจสอบสถานะการเงินของท่านจากทางธนาคารหากยังไม่สามารถใช้ได้กรุณาติดต่อแผนกลูกค้าสัมพันธ์ของธนาคารเพื่อแจ้งกับแผนกเทคโนโลยีสารสนเทศเพื่อแก้ไขปัญหาด้านเทคนิค</div>
</li>
<li>
<div class="accord-header">หากเครื่องคอมพิวเตอร์ขัดข้องระหว่างขั้นตอนการชำระเงิน จะทราบได้อย่างไรว่าการชำระเงินเสร็จสิ้นเรียบร้อยแล้ว</div>
<div class="accord-content">เมื่อท่านชำระเงินเรียบร้อยแล้วคำยืนยันการสั่งซื้อจะถูกส่งไปยังอีเมลของท่านหากท่านไม่ได้รับการยืนยันการสั่งซื้อทางอีเมลกรุณาลองทำการสั่งซื้ออีกครั้ง หากท่านมีข้อสงสัยกรุณาติดต่อแผนกลูกค้าสัมพันธ์เพื่อสอบถามสถานะการสั่งซื้อของท่านที่ อีเมล <a href="mailto:support@sanoga.com">support@sanoga.com</a> หรือทางโทรศัพท์หมายเลข 02-106-8222</div>
</li>
<li>
<div class="accord-header">วิธีการชำระเงินมีกี่ช่องทาง</div>
<div class="accord-content">
บริษัทมีช่องทางให้ลูกค้าเลือกชำระเงินหลายช่องทาง ตามด้านล่างนี้
<ul style="margin-left: 30px; list-style: square !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">บัตรเครดิต วีซ่า มาสเตอร์ ซึ่งผ่าน่ระบบ Payment ของบริษัท 2C2P</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">PayPal ช่วยให้คุณสามารถชำระค่าสินค้าของคุณหรือชำระด้วยบัตรเครดิตชั้นนำต่างๆ ผ่านทางหน้าเว็บไซด์ได้ทันที โดยจะทำรายการผ่านระบบของ PayPal ทำให้การชำระเงินเป็นไปได้อย่างสะดวก รวดเร็วและปลอดภัย หากท่านยังไม่มีบัญชีกับ Paypal สามารถ คลิกที่นี่เพื่อสมัครบริการ Paypal</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Cash on delivery เป็นการเก็บเงินปลายทางจากผู้รับสินค้า โดยจะมีค่าธรรมเนียมในการเก็บเงินปลายทาง 50 บาท ต่อการสั่งซื้อต่อครั้ง ค่าธรรมเนียมนี้ไม่รวมค่าสินค้า และค่าจัดส่งสินค้า</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">บริการผ่อนชำระสินค้า</li>
</ul><br/>
เมื่อซื้อสินค้าครบ 3,500 บาท สามารถผ่อนชำระได้นานสูงสุด 10 เดือนกับบัตรเครดิตที่ร่วมรายการ<br/><br/>
บัครเครดิตธนาคารกรุงเทพ<br/><br/>
<ul style="margin-left: 30px; list-style: disc !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">แบ่งชำระ 4 เดือน ดอกเบี้ย 0.8%</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">แบ่งชำระ 6 เดือน ดอกเบี้ย 0.8%</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">แบ่งชำระ 10 เดือน ดอกเบี้ย 0.8%</li>
</ul>
<br/>
บัตรเครดิตซิตี้แบงค์<br/><br/>
<ul style="margin-left: 30px; list-style: disc !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">แบ่งชำระ 4 เดือน ดอกเบี้ย 0.89%</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">แบ่งชำระ 6 เดือน ดอกเบี้ย 0.89%</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">แบ่งชำระ 10 เดือน ดอกเบี้ย 0.89%</li>
</ul>
<br/>
บัตรเครดิต KTC<br/><br/>
<ul style="margin-left: 30px; list-style: disc !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">แบ่งชำระ 4 เดือน ดอกเบี้ย 0.89%</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">แบ่งชำระ 6 เดือน ดอกเบี้ย 0.89%</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">แบ่งชำระ 10 เดือน ดอกเบี้ย 0.89%</li>
</ul>
<br/>
บัตรเครดิตธนาคารธนชาติ<br/><br/>
<ul style="margin-left: 30px; list-style: disc !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">แบ่งชำระ 4 เดือน ดอกเบี้ย 0.8%</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">แบ่งชำระ 6 เดือน ดอกเบี้ย 0.8%</li>
</ul>
<br/>
* หมายเหตุ<br/>
ยอดสั่งซื้อขั้นต่ำ 3,500 บาทต่อคำสั่งซื้อ โดยคำนวนราคาก่อนหักส่วนลดและไม่รวมค่าขนส่ง<br/>
อัตราดอกเบี้ยเป็นไปตามที่ธนาคารผู้ออกบัตรกำหนด<br/>
สงวนสิทธิ์การคืนเงินเในทุกกรณี<br/>
เงื่อนไขเป็นไปตามข้อกำหนด บริษัทขอสงวนสิทธิ์การเปลี่ยนแปลงโดยไม่แจ้งให้ทราบล่วงหน้า<br/>
</div>
</li>
<li>
<div class="accord-header">ราคาสินค้าใช้สกุลเงินไทยหรือไม่</div>
<div class="accord-content">ราคาสินค้าทุกชิ้นของเราใช้หน่วยเงินบาทไทย</div>
</li>
<li class="last">
<div class="accord-header">ราคาสินค้ารวมภาษีมูลค่าเพิ่มหรือยังค่ะ</div>
<div class="accord-content">ราคาสินค้าของเราได้รวมภาษีมูลค่าเพิ่มแล้ว</div>
</li>
</ol></div>
<div id="shipping" class="subcontent">
<h2>การจัดส่งสินค้า</h2>
<ol class="accordion">
<li>
<div class="accord-header">บริษัททำการจัดส่งสินค้าเมื่อไหร่</div>
<div class="accord-content">หากสินค้าที่คุณสั่งซื้อมีพร้อมในคลังของเรา บริษัทจะทำการจัดส่งสินค้าออกจากคลังสินค้าภายใน 24 ชั่วโมงหลังจากคำสั่งซื้อสินค้าสมบูรณ์เรียบร้อยแล้ว หรือในเวลาที่ไม่ช้าหรือเร็วไปกว่านั้น (เฉพาะในวันทำการ) สินค้าของ Sanoga ทุกรายการจัดส่งโดยระบบที่มีประสิทธิภาพสูง (Premium Courier) ซึ่งจะดำเนินการจัดส่งสินค้าในช่วงเวลา 9.00 น. &ndash; 19.00 น. และในปัจจุบันเราให้บริการจัดส่งสินค้าเฉพาะวันจันทร์ &ndash; วันเสาร์เท่านั้น</div>
</li>
<li>
<div class="accord-header">ระยะเวลาการจัดส่งสินค้านานเท่าไหร่</div>
<div class="accord-content">การจัดส่งสินค้าดำเนินการด้วยระบบของบริษัท ร่วมกับผู้ให้บริการจัดส่งสินค้า อื่น ๆ ซึ่งมีประสิทธิภาพสูง (Premium Courier) เพื่อให้เราแน่ใจได้ว่าสินค้าจะ จัดส่งถึงมือคุณอย่างรวดเร็วที่สุด หากคุณสั่งซื้อสินค้าภายในเวลา 16.00 น. สินค้าถึงมือคุณไม่เกิน 2-3 วันทำการเฉพาะในเขตกรุงเทพ และ 3-5 วันทำการ ในเขต ปริมณฑล และต่างจังหวัด ทั้งนี้บริษัทไม่อาจรับประกันได้ว่าหลังจากคุณสั่งซื้อ สินค้าแล้วจะได้รับสินค้าภายในวันถัดไปเลยทันที แม้ว่าการจัดส่งสินค้าไปยังหลาย พื้นที่ลูกค้าจะสามารถรับสินค้าได้ในวันรุ่งขึ้นก็ตาม และในบางพื้นที่อาจจะใช้ ระยะเวลาการจัดส่งที่นานกว่านั้นก็เป็นได้ <br /><br /> *เฉพาะ ปัตตานี ยะลา นราธิวาส แม่ฮ่องสอน เชียงราย อำเภอสังขระบุรี และบางพื้นที่ที่เป็นเกาะ อาจใช้เวลาในการจัดส่งมากกว่าปกติ 1-2 วันทำการ อย่างไรก็ตามทีมงานของเราจะจัดส่งถึงมือคุณอย่างเร็วที่สุด</div>
</li>
<li>
<div class="accord-header">วิธีการตรวจสอบและติดตามสถานะการจัดส่งสินค้า</div>
<div class="accord-content">เมื่อเราทำการจัดส่งสินค้า คุณจะได้รับอีเมลเพื่อยืนยันการจัดส่งสินค้าและแจ้งหมายเลขการจัดส่งสินค้า คุณสามารถใช้หมายเลขการจัดส่งสินค้าเพื่อตรวจสอบสถานะการจัดส่งได้ที่เว็บไซต์ของเรา (ตรวจสอบสถานะการจัดส่ง) หรือสามารถล็อกอินเพื่อดูสถานะการจัดส่งสินค้าได้ที่หน้าใบสั่งซื้อของคุณ โดยคลิกที่ปุ่ม &ldquo;สถานะสินค้า&rdquo; ในกรณีที่สถานะการจัดส่งสินค้าขึ้นว่าได้ &ldquo;ส่งมอบสินค้าแล้ว&rdquo; และคุณยังไม่ได้รับสินค้ากรุณาตรวจสอบกับเพื่อนบ้านหรือผู้ดูแลอาคารก่อนเบื้องต้น และหากสถานะการจัดส่งสินค้าขึ้นว่า &ldquo;ส่งคืนบริษัท&rdquo; กรุณาติดต่อเราทางอีเมลหรือโทรศัพท์ทันที เพราะอาจจะเกิดกรณีที่อยู่จัดส่งสินค้าไม่ถูกต้อง, เกิดเหตุสุดวิสัยที่ทำให้พนักงานไม่สามารถจัดส่งสินค้าได้ หรือสินค้าสูญหายระหว่างการจัดส่ง</div>
</li>
<li>
<div class="accord-header">กรณีที่พนักงานไปจัดส่งสินค้าแล้วไม่มีผู้รอรับสินค้าที่บ้าน</div>
<div class="accord-content">การจัดส่งสินค้าเมื่อไม่มีผู้รอรับสินค้าอยู่ที่บ้าน ขึ้นอยู่กับสถานการณ์และดุลยพินิจของพนักงานจัดส่งสินค้าว่าสามารถจะทำการวางสินค้าไว้ที่หน้าบ้านได้หรือไม่ ในกรณีที่พนักงานจัดส่งสินค้าเห็นว่าจำเป็นจะต้องมีผู้ลงชื่อรับสินค้า พนักงานจัดส่งสินค้าจะฝากสินค้าไว้ที่ผู้จัดการอาคารหรือพนักงานเฝ้าประตู โดยจะพิจารณาเลือกวิธีการที่ดีที่สุดอย่างใดอย่างหนึ่ง แต่หากไม่สามารถทำการจัดส่งสินค้าได้ สินค้าจะถูกตีคืนมาที่บริษัทซึ่งจะขึ้นสถานะการจัดส่งสินค้าว่า &ldquo;ไม่สามารถจัดส่ง&rdquo; คุณสามารถทำเรื่องขอคืนเงินค่าสินค้าเต็มจำนวน แต่ไม่รวมค่าจัดส่งสินค้าได้ (อ่านรายละเอียดได้ที่ <a href="{{store url='return-policy'}}" target="_blank">นโยบายการคืนสินค้า</a> ) ในกรณีที่คำสั่งซื้อถูกปฏิเสธอย่างไม่ถูกต้อง ทางบริษัทจะไม่ทำการจัดส่งสินค้ากลับไปอีกครั้ง คุณจะต้องดำเนินการสั่งซื้อใหม่เพื่อที่จะได้รับสินค้าที่คุณต้องการ</div>
</li>
<li>
<div class="accord-header">สามารถแจ้งส่งสินค้าไปยังที่สถานที่อื่นๆ ของคุณได้หรือไม่</div>
<div class="accord-content">แน่นอน เราทำการจัดส่งสินค้าในวันจันทร์- ศุกร์ เวลา 09.00 น. &ndash; 19.00 น. และวันเสาร์ เวลา 09.00 น. &ndash; 12.00 น. หากต้องการให้เราจัดส่งสินค้าไปยังที่ทำงานของคุณ สามารถกรอกที่อยู่ที่ต้องการให้เราจัดส่งในช่องสถานที่จัดส่งสินค้า หากเกิดเหตุสุดวิสัยที่คุณไม่สามารถอยู่รอรับสินค้าได้ด้วยตนเอง กรุณาตรวจสอบสินค้ากับทางพนักงานต้อนรับ, ผู้จัดการอาคาร, พนักงานเฝ้าประตูก่อนเบื้องต้น และเพื่อป้องกันความผิดพลาดในการนัดหมายรับสินค้า กรุณารอรับการติดต่อทางโทรศัพท์จากพนักงานของเราด้วย</div>
</li>
<li>
<div class="accord-header">ค่าบริการจัดส่งสินค้า</div>
<div class="accord-content">จัดส่งฟรีเมื่อซื้อสินค้าขั้นต่ำ 500 บาทต่อการสั่งซื้อ1ครั้ง เฉพาะเขตกรุงเทพ ในส่วนของเขตปริมณฑล และต่างจังหวัด คิดค่าบริการในการจัดส่ง 100 บาท ในกรณีซื้อสินค้าต่ำกว่า 500 บาทเสียค่าจัดส่ง 100 บาทในเขตกรุงเทพ และ 200 บาทในเขตปริมณฑลและต่างจังหวัด</div>
</li>
<li>
<div class="accord-header">การจัดส่งสินค้าไปยังต่างประเทศ</div>
<div class="accord-content">ขออภัย...ณ ตอนนี้บริษัทยังไม่มีบริการจัดส่งสินค้าไปยังต่างประเทศ แต่ในอนาคตเรามีแพลนที่จะเปิดร้านค้าออนไลน์ที่ขายอาหารสัตว์ทั่วภาคพื้นเอเชียตะวันออกเฉียงใต้</div>
</li>
<li class="last">
<div class="accord-header">ทำไมการจัดส่งสินค้าถึงล่าช้า</div>
<div class="accord-content">หากทำการสั่งซื้อสินค้าภายในเวลา 16.00 น. ของวันทำการปกติ คุณจะได้รับสินค้าหลังจากคำสั่งซื้อสมบูรณ์แล้วภายใน 2-3 วันทำการ ซึ่งเราจะพยายามจัดส่งสินค้าอย่างเร็วที่สุดเท่าที่จะเป็นไปได้ แต่ก็อาจมีบางกรณีที่จะทำให้คุณได้รับสินค้าล่าช้ากว่ากำหนด<br />สาเหตุที่อาจจะทำให้การจัดส่งสินค้าล่าช้า :
<ul style="margin-left: 30px; list-style: square !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">ข้อมูลสถานที่จัดส่งสินค้าไม่ถูกต้อง</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">ข้อมูลเบอร์โทรศัพท์ของลูกค้าไม่ถูกต้อง</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">ไม่สามารถติดต่อลูกค้าตามเบอร์โทรศัพท์ที่ให้ไว้ได้</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">ไม่มีคนอยู่รับสินค้าตามสถานที่ได้นัดหมายเอาไว้</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">แจ้งสถานที่จัดส่งสินค้าไม่เหมือนกับที่อยู่ตามใบสั่งซื้อ</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">ลูกค้าชำระเงินล่าช้าหรือมีข้อผิดพลาดในขั้นตอนการชำระเงิน</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">คำสั่งซื้อที่จำเป็นต้องหยุดชะงักหรือล่าช้า อันเนื่องมาจากการสั่งซื้อสินค้าในรายการใดรายการหนึ่งเป็นจำนวนมากทำให้สินค้าในคลังของเราไม่เพียงพอต่อการจำหน่าย (เป็นกรณีที่เกิดขึ้นไม่บ่อยนัก)</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">เหตุสุดวิสัยที่ทำให้การจัดส่งสินค้าไม่เป็นไปตามกำหนด (เช่น ภัยธรรมชาติหรือสภาพอากาศที่ผิดปกติ เป็นต้น)</li>
</ul>
</div>
</li>
</ol></div>
<div id="rewardpoints" class="subcontent">
<h2>คะแนนสะสม</h2>
<ol class="accordion">
<li>
<div class="accord-header">รับเงินคืน 1% เมื่อช็อปปิ้งกับ Sanoga</div>
<div class="accord-content">เมื่อท่านได้สั่งซื้อของกับทาง Sanoga ท่านจะได้รับเงินคืนทันที 1 % โดยเงินที่ได้จะถูกเก็บไว้ที่บัญชี Sanoga ของท่าน ท่านสามารถนำมาใช้ได้ในครั้งต่อไปเมื่อท่านสั่งซื้อของผ่านทาง Sanoga โดยสามารถใช้แทนเงินสด ทุกครั้งที่ท่านต้องการใช้ ท่านสามารถกรอกจำนวนเงินในช่อง ที่หน้าสั่งซื้อสินค้า ก่อนขั้นตอนสุดท้าย</div>
</li>
<li>
<div class="accord-header">คุณจะได้รับแต้มจากการแนะนำเว็บไซต์ให้กับเพื่อนๆ ได้อย่างไร</div>
<div class="accord-content">มีหลากหลายช่องทางที่คุณจะได้รับเครดิต 100 บาท จากการแนะนำเว็บไซต์ Sanoga.com ให้กับเพื่อนๆ <a href="{{store url='rewardpoints/index/referral'}}" target="_blank">อ่านรายละเอียดเพิ่มเติมที่นี่</a>
<ul style="margin-left: 30px; list-style: square !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">คุณสามารถนำลิงค์จากในหน้าบัญชีผู้ใช้ของคุณในเว็บไซต์ Sanoga.com แชร์ผ่านเฟสบุ๊ค ทวีทเตอร์ หรือส่งลิงค์ให้กับเพื่อนๆ ผ่านทางอีเมล</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">เมื่อเพื่อนของคุณคลิกลิงค์ดังกล่าวและซื้อสินค้าบนเว็บไซต์ Sanoga.com คุณจะได้รับทันทีเครดิต 100 บาทในบัญชีของคุณ เครดิตดังกล่าวสามารถใช้แทนเงินสดหรือใช้เป็นส่วนลดในการซื้อสินค้าครั้งต่อๆ ไป</li>
</ul>
</div>
</li>
<li class="last">
<div class="accord-header">อายุการใช้งานของคะแนนสะสม (Reward Points)</div>
<div class="accord-content">เมื่อคุณซื้อสินค้ากับทางเว็บไซต์ของเราคุณจะได้รับคะแนนสะสม (Reward Points) โดยคะแนนสะสมที่คุณได้รับจากการซื้อสินค้าแต่ละครั้งนั้นจะนำมารวมกันเมื่อคุณซื้อสินค้าในครั้งต่อไปเพิ่มขึ้นเรื่อยๆ แต่ถ้าคุณเก็บคะแนนสะสมไว้เป็นเวลานาน คะแนนบางส่วนของคุณอาจหายไปได้ เนื่องจากอายุการใช้งานของคะแนนสะสมจะมีระยะเวลาไม่เท่ากัน กล่าวคือ คะแนนที่คุณได้รับหลังจากการซื้อสินค้าแต่ละครั้งจะมีอายุการใช้งานนาน 6 เดือน <br /><br /> เช่น คุณซื้อสินค้าและได้คะแนนสะสมครั้งแรกในเดือนมกราคม จำนวน 20 คะแนน ต่อมาคุณซื้อสินค้าและได้คะแนนสะสมอีกครั้งในเดือนมีนาคม จำนวน 30 คะแนน คุณจะมีคะแนนสะสมรวมเป็น 50 คะแนน แต่เมื่อถึงเดือนกรกฎาคม คะแนนของคุณจะเหลือเพียง 30 คะแนน (เนื่องจาก 20 คะแนนที่ได้จากเดือนมกราคมหมดอายุในเดือนกรกฎาคม) และถ้าคุณยังไม่ได้ใช้คะแนนที่เหลืออีกภายในเดือนกันยายนคะแนนสะสมของคุณจะกลายเป็น 0 ทันที (เนื่องจาก 30 คะแนนที่ได้จากเดือนมีนาคมหมดอายุในเดือนกันยายน) <br /><br /> ดังนั้นเพื่อเป็นการรักษาสิทธิและผลประโยชน์ของคุณ จึงควรนำคะแนนสะสมมาใช้ภายในระยะเวลาที่กำหนด</div>
</li>
</ol></div>
<div id="returns" class="subcontent">
<h2>การคืนสินค้าและขอคืนเงิน</h2>
<ol class="accordion">
<li>
<div class="accord-header">สามารถขอคืนสินค้าได้อย่างไร</div>
<div class="accord-content"><strong>บริการคืนสินค้าอย่างง่ายดาย รวดเร็ว และไม่มีค่าใช้จ่าย!</strong><br /> กรณีที่คุณต้องการคืนสินค้า กรุณาติดต่อแผนกลูกค้าสัมพันธ์และแจ้งความประสงค์ในการขอคืนสินค้าผ่านทางโทรศัพท์ 02-106-8222 หรือ อีเมล์ <a href="mailto:support@sanoga.com">support@sanoga.com</a> หากเงื่อนไขการขอคืนสินค้าของคุณเป็นไปตามนโยบายการขอคืนสินค้าแล้ว กรุณากรอกรายละเอียดลงในแบบฟอร์มการขอคืนสินค้าที่คุณได้รับไปพร้อมกับสินค้าเมื่อตอนจัดส่ง และแนบใบกำกับภาษี (invoice) ใส่ลงในกล่องพัสดุของ Sanoga.com กลับมาพร้อมกันด้วย โดยจัดส่งสินค้ากลับมาให้ทางเรา ในแบบพัสดุลงทะเบียนผ่านทางที่ทำการไปรษณีย์ไทยใกล้บ้านคุณ <br /><br /> กรุณาจัดส่งสินค้ามาที่ :<br /> บริษัท เอคอมเมิร์ซ จำกัด<br /> 8/2 ถนนพระราม3 ซอย53<br /> แขวงบางโพงพาง เขตยานนาวา <br />กรุงเทพฯ 10120 <br /><br /> ทาง Sanoga.com จะชดเชยค่าจัดส่งแบบลงทะเบียนพร้อมกับการคืนเงินค่าสินค้า โดยทางเราขอให้คุณเก็บใบเสร็จรับเงิน/slip ค่าจัดส่งที่ได้รับจากที่ทำการไปรษณีย์เพื่อเป็นหลักฐานในการขอคืนเงินค่าส่งสินค้ากลับไว้ด้วย กรุณาส่งใบเสร็จรับเงิน/slip นี้กลับมาทางแผนกลูกค้าสัมพันธ์ โดยส่งเป็นไฟล์รูปภาพกลับมาทางอีเมล์ <a href="mailto:support@sanoga.com">support@sanoga.com</a> <br /><br /> หมายเหตุ : ทาง Sanoga.com ต้องขออภัยในความไม่สะดวกที่เรายังไม่มีบริการในการเข้าไปรับสินค้าที่ลูกค้าต้องการส่งคืน</div>
</li>
<li>
<div class="accord-header">การแพ็คสินค้าเพื่อส่งคืนบริษัท</div>
<div class="accord-content">เราแนะนำให้คุณทำการแพ็คสินค้าคืนด้วยกล่องหรือบรรจุภัณฑ์ลักษณะเดียวกับที่เราทำการจัดส่งไปให้ เว้นแต่ว่ามันไม่ได้อยู่ในสภาพที่ดีพอสำหรับการนำมาใช้ใหม่ อย่าลืม! ที่จะแนบป้าย/ฉลากขอคืนสินค้า (return label) กลับมาด้วย จากนั้นใช้ป้ายชื่อ/ที่อยู่ในการจัดส่งสินค้า (Shipping label) ที่ได้รับจากพนักงานหรือจากเว็บไซต์ของเราเพื่อทำการจัดส่งสินค้าคืนบริษัทได้ที่ทำการไปรษณีย์ทุกแห่งทั่วประเทศไทย <br /><br /> หมายเหตุ : การจัดส่งสินค้ากลับคืนนั้น Sanoga ไม่ได้เป็นผู้กำหนดระยะเวลาในการจัดส่งสินค้า จึงไม่สามารถบอกได้ว่าสินค้าที่ส่งคืนนั้นจะมาถึงบริษัทเมื่อไหร่ <br /><br /> หากมีข้อสงสัยกรุณาติดต่อสอบถามเราได้ที่ 02-106-8222 หรือ อีเมล์ <a href="mailto:support@sanoga.com">support@sanoga.com</a></div>
</li>
<li>
<div class="accord-header">ใช้ระยะเวลาดำเนินการคืนสินค้านานเท่าไหร่</div>
<div class="accord-content">เราจะใช้เวลาดำเนินการภายใน 14 วันทำการ นับตั้งแต่วันที่ได้รับสินค้าคืนมาเรียบร้อยแล้ว</div>
</li>
<li>
<div class="accord-header">เมื่อไหร่ถึงจะได้รับเงินค่าสินค้าคืน</div>
<div class="accord-content">เมื่อบริษัทได้รับสินค้าที่ส่งคืนเรียบร้อยแล้ว ขั้นตอนการคืนเงินจะเกิดขึ้นโดยทันที เราจะดำเนินการคืนเงินค่าสินค้ากลับไปยังบัญชีเงินฝากของคุณ กรุณารอและตรวจสอบยอดเงินหลังจากที่เราได้รับสินค้าคืนจากคุณภายใน 7 วันทำการ</div>
</li>
<li>
<div class="accord-header">ค่าใช้จ่ายในการจัดส่งสินค้าคืนบริษัท</div>
<div class="accord-content">ค่าใช้จ่ายในการจัดส่งสินค้าจะแตกต่างกันไปตามสถานที่และวิธีการจัดส่งที่คุณเลือก กรุณาตรวจสอบให้แน่ใจว่าคุณได้ใช้ระบบจัดส่งแบบลงทะเบียนซึ่งดำเนินการโดยไปรษณีย์ไทย และเราจะคืนเงินค่าจัดส่งหลังจากที่ได้รับสินค้าคืนและยืนยันว่าได้รับสินค้าคืนเรียบร้อยแล้ว</div>
</li>
<li class="last">
<div class="accord-header">กรณีที่การจัดส่งสินค้าเกิดข้อผิดพลาด</div>
<div class="accord-content">แผนกควบคุมคุณภาพของเราทำงานอย่างหนักเพื่อให้สินค้าทุกชิ้นอยู่ในสภาพที่ดีและมีคุณภาพสูงสุดเมื่อทำการจัดส่งออกจากคลังสินค้า แต่ก็มีในบางกรณีซึ่งเป็นไปได้ยากที่สินค้าของคุณจะเกิดตำหนิ,เสียหาย หรือมีข้อผิดพลาด ดังนั้นหากเกิดกรณีดังกล่าวแล้วคุณสามารถดำเนินการขอคืนสินค้าและเงินค่าสินค้าเต็มจำนวน ภายในระยะเวลา 30 วันหลังจากที่คุณได้รับสินค้าเรียบร้อยแล้ว</div>
</li>
</ol></div>
</div>
<script type="text/javascript">// <![CDATA[
    jQuery(document).ready(function()
    {
        var tmpUrl = "";
        var h=window.location.hash.substring(0);
        if(h == ""){
            //alert('Hash not found!!!');
            h = "#faq";
            //alert(heart);
            jQuery( "#helpMenu li:first-child" ).addClass( "helpactive");
            jQuery('.subcontent[id='+h.substring(1)+']').fadeIn();
        }else{
            jQuery('#helpMenu li a[data-id='+h.substring(1)+']').parent().addClass( "helpactive");
            jQuery('.subcontent[id='+h.substring(1)+']').fadeIn();
        }

        //////////////////////////////
        jQuery('#keyMess li').on('click','a',function()  {
            tmpUrl=jQuery(this).attr('href');
            h=tmpUrl.substring(tmpUrl.indexOf("#")+1);

            jQuery('.subcontent:visible').fadeOut(0);
            jQuery('.subcontent[id='+h+']').fadeIn();
            jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
            jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
        });
        jQuery('#zyncNav li').on('click','a',function()
        {
            tmpUrl=jQuery(this).attr('href');
            h=tmpUrl.substring(tmpUrl.indexOf("#")+1);
            jQuery('.subcontent:visible').fadeOut(0);
            jQuery('.subcontent[id='+h+']').fadeIn();
            jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
            jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
        });
        jQuery('#footerBanner div').on('click','a',function()
        {
            tmpUrl=jQuery(this).attr('href');
            h=tmpUrl.substring(tmpUrl.indexOf("#")+1);
            jQuery('.subcontent:visible').fadeOut(0);
            jQuery('.subcontent[id='+h+']').fadeIn();
            jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
            jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
        });
        jQuery('#topMklink').on('click','a',function()
        {
            tmpUrl=jQuery(this).attr('href');
            h=tmpUrl.substring(tmpUrl.indexOf("#")+1);
            jQuery('.subcontent:visible').fadeOut(0);
            jQuery('.subcontent[id='+h+']').fadeIn();
            jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
            jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
        });
        jQuery('#footer_help li').on('click','a',function()
        {
            tmpUrl=jQuery(this).attr('href');
            h=tmpUrl.substring(tmpUrl.indexOf("#")+1);
            jQuery('.subcontent:visible').fadeOut(0);
            jQuery('.subcontent[id='+h+']').fadeIn();
            jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
            jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
        });
        ///////////////////////////////

        jQuery('#helpMenu').on('click','a',function()
        {
            jQuery('.subcontent:visible').fadeOut(0);
            jQuery('.subcontent[id='+jQuery(this).attr('data-id')+']').fadeIn();
            jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
            jQuery(this).parent().addClass( "helpactive");
        });

///////////////////////////////

        jQuery(".accordion li .accord-header").mouseenter(function() {
            jQuery(this).css("cursor","pointer");
        });
        jQuery(".accordion li .accord-header").click(function() {
            jQuery(this).find("span").removeClass('active');
            if(jQuery(this).next("div").is(":visible")){
                jQuery(this).next("div").slideUp(300);
            } else {
                jQuery(".accordion li .accord-content").slideUp(400);
                jQuery(this).next("div").slideToggle(400);
                jQuery(".accordion li .accord-header").find("span").removeClass('active');
                jQuery(this).find("span").addClass('active');
            }
        });
    });
// ]]></script>
EOD;
/** @var Mage_Cms_Model_Page $page */
$page = Mage::getModel('cms/page')->getCollection()
    ->addStoreFilter($stores, $withAdmin = false)
    ->addFieldToFilter('identifier', $identifier)
    ->getFirstItem();
if ($page->getId()) $page->delete(); // if exists then delete
$page = Mage::getModel('cms/page');
$page->setTitle($title);
$page->setIdentifier($identifier);
$page->setStores($stores);
$page->setIsActive($isActive);
$page->setRootTemplate($rootTemplate);
$page->setSortOrder($sortOrder);
$page->setContent($content);
$page->setContentHeading($contentHeading);
$page->setMetaDescription($metaDescription);
$page->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// Help page (Lafema Thai)
//==========================================================================
$title = "วิธีการสั่งซื้อ และคำถามที่พบบ่อย";
$stores = array($lth);
$metaDescription = "คำถามที่พบบ่อย เกี่ยวกับวิธีการการสั่งซื้อ การชำระเงิน และการจัดส่งสินค้า ตลอดจนนโยบายของ Lafema.com ในการให้บริการลูกค้า";
$identifier = "help";
$isActive = 1;
$rootTemplate = "one_column";
$sortOrder = 0;
$contentHeading = "ช่วยเหลือ";
$content = <<<EOD
<div>
<ul id="helpMenu">
<li><a href="javascript:void(0)" data-id="faq">คำถามที่พบบ่อย</a></li>
<li><a href="javascript:void(0)" data-id="howtoorder">วิธีการการสั่งซื้อ</a></li>
<li><a href="javascript:void(0)" data-id="payments">การชำระเงิน</a></li>
<li><a href="javascript:void(0)" data-id="shipping">การจัดส่งสินค้า</a></li>
<li><a href="javascript:void(0)" data-id="rewardpoints">คะแนนสะสม</a></li>
<li><a href="javascript:void(0)" data-id="returns">การคืนสินค้าและขอคืนเงิน</a></li>
</ul>
</div>
<div class="helpContent">
<div id="faq" class="subcontent">
<h2>FAQ</h2>
<ol class="accordion">
<li>
<div class="accord-header">Lafema คืออะไร</div>
<div class="accord-content">Lafema เป็นศูนย์รวมสินค้าเพื่อความสวยความงามสำหรับคุณผู้หญิง และคุณผู้ชาย โดยมีระบบที่ได้รับการพัฒนาเป็นอย่างดี ซึ่งจะช่วยให้ลูกค้าสามารถเลือกซื้อสินค้าได้ตลอด 24 ชั่วโมง</div>
</li>
<li>
<div class="accord-header">Lafema มีที่มาอย่างไร</div>
<div class="accord-content">ที่ Lafema เราเชื่อว่า ความสวยความงามเป็นที่ปารถนาของทุกคน เราจึงรังสรรค์ความสวยความงามมาให้คุณเลือกเพียงแค่นิ้วคลิก ช่วยให้เรื่องยากกลายเป็นเรื่องง่ายสำหรับคุณ ให้คุณสนุกและเพลิดเพลินในการเลือกซื้อสินค้า หลากหลายแบรนด์ที่มีคุณภาพจากทั่วโลก พร้อมกับข้อมูลเพื่อช่วยให้คุณตัดสินใจได้ว่าอะไรเหมาะกับคุณที่สุด ทั้งยังสามารถส่งตรงถึงมือคุณได้ภายใน 1-2 วัน ทำการ</div>
</li>
<li>
<div class="accord-header">Lafema ตั้งอยู่ที่ไหน</div>
<div class="accord-content">บริษัทตั้งอยู่ที่ อาคารเศรษฐีวรรณ ชั้น 19 <br /> 139 ถนนปั้น แขวงสีลม เขตบางรัก <br /> กรุงเทพฯ ประเทศไทย 10500 <br /> โทรศัพท์: 02-106-8222<br /> อีเมล์ <a href="mailto:support@lafema.com">support@lafema.com</a></div>
</li>
<li>
<div class="accord-header">สนใจอยากร่วมธุรกิจกับ Lafema ทำอย่างไร</div>
<div class="accord-content">บริษัทยินดีต้อนรับผู้ที่สนใจเข้าเป็นส่วนหนึ่งกับเราได้ โดยท่านที่สนใจนำเสนอสินค้าเพื่อความสวยความงาม คุณภาพสูงผ่านทางหน้าเว็บไซต์ Lafema สามารถติดต่อได้ที่อีเมล <a href="mailto:support@lafema.com ">support@lafema.com</a></div>
</li>
<li>
<div class="accord-header">โปรโมชั่นจัดส่งสินค้าฟรี</div>
<div class="accord-content">จัดส่งฟรีเมื่อซื้อสินค้าขั้นต่ำ 500 บาทต่อการสั่งซื้อ1ครั้ง เฉพาะเขตกรุงเทพ ในส่วนของเขตปริมณฑล และต่างจังหวัด คิดค่าบริการในการจัดส่ง 100 บาท ในกรณีซื้อสินค้าต่ำกว่า 500 บาทเสียค่าจัดส่ง 100 บาทในเขตกรุงเทพ และ 200 บาทในเขตปริมณฑลและต่างจังหวัด</div>
</li>
<!-- 			<li>
			<div class="accord-header"><span></span>ข้อเสนอพิเศษ</div>
			<div class="accord-content">ในหน้าเว็บไซต์ของเรา จะมีข้อเสนอพิเศษต่างๆ รวมทั้งสินค้าที่มีส่วนลด 20% หรือมากกว่า ซึ่งเราได้ทำหน้าเฉพาะสำหรับผลิตภัณฑ์เหล่านี้เพื่อความง่ายในการเลือกซื้อหรือค้นหาสินค้าราคาพิเศษ หรือข้อเสนอพิเศษที่คุณต้องการ</div>
			</li> -->
<li>
<div class="accord-header">วิธีขอรหัสผ่านใหม่</div>
<div class="accord-content">ถ้าคุณลืมรหัสผ่านของคุณ คุณสามารถขอรหัสผ่านใหม่ได้ <a href="{{store url='forgot-password'}}" target="_blank">คลิกที่นี่</a> เพื่อดูรายละเอียดเพิ่มเติม</div>
</li>
<li>
<div class="accord-header">ถ้าสินค้าที่ต้องการหมดจะทำอย่างไร</div>
<div class="accord-content">ถึงแม้ว่าทางเราจะพยายามจัดหาสินค้าให้มีจำนวนเหมาะสมกับระดับความต้องการของลูกค้า แต่สินค้าที่ได้รับความนิยมอย่างมากก็อาจหมดลงได้อย่างรวดเร็ว แต่เราพยายามไม่ให้ปัญหานี้เกิดขึ้น หรือถ้าเกิดขึ้นก็จะพยายามหาทางแก้ไข และหาทางออกให้กับลูกค้าอย่างแน่นอน ถ้ามีข้อสงสัย หรือปัญหาเกี่ยวกับสินค้าสามารถติดต่อได้ที่ฝ่ายลูกค้าสัมพันธ์ได้ในเวลาทำการ</div>
</li>
<li>
<div class="accord-header">ฉันสามารถจองสินค้าไว้ก่อนเพื่อทำการสั่งซื้อในวันถัดไปได้หรือไม่</div>
<div class="accord-content">ต้องขออภัยทางเราไม่นโยบายการจองสินค้า</div>
</li>
<li>
<div class="accord-header">ฉันจะทราบได้อย่างไรว่ามีแบรนด์ไหนขายอยู่บนเว็บไซต์ Lafema บ้าง</div>
<div class="accord-content">คุณสามารถค้นหาสินค้าที่ต้องการได้ โดยกดปุ่มค้นหาที่หน้าเว็บไซต์ Lafema ซึ่งอยู่ด้านบนของหน้า เว็บไซต์ หรือคุณสามารถเข้าไปคลิกเลือกแบรนด์ต่าง ๆ ได้ บนแถบเมนู</div>
</li>
<li>
<div class="accord-header">จะทรายได้อย่างไรว่ามีสินค้าใหม่เข้ามาในเว็บไซต์</div>
<div class="accord-content">ทางเราจะทำการอัพเดทเว็บไซต์ใหม่ทันที ที่มีสินค้าใหม่เข้ามาในคลังสินค้า ท่านสามารถตรวจสอบสินค้า ใหม่ได้ที่เว็บไซต์ของเราในหน้าแรก</div>
</li>
<li class="last">
<div class="accord-header">ฉันสามารถแสดงความคิดเห็น หรือให้ข้อแนะนำเกี่ยวกับสินค้าได้อย่างไร</div>
<div class="accord-content">เรายินดีที่รับฟังความคิดเห็น และข้อเสนอแนะเกี่ยวกับผลิตภัณฑ์ของเรา คุณสามารถแสดงความคิดเห็นของคุณมาผ่านมางทางอีเมล <a href="mailto:support@lafema.com">support@lafema.com</a></div>
</li>
</ol></div>
<div id="howtoorder" class="subcontent">
<h2>วิธีการสั่งซื้อ</h2>
<ol class="accordion">
<li>
<div class="accord-header">สั่งซื้อสินค้าผ่าน Lafema อย่างไร?</div>
<div class="accord-content">คุณสามารถสั่งซื้อสินค้ากับ Lafema.com ด้วยวิธีง่ายๆ <a href="{{store url='how-to-order'}}" target="_blank">คลิกที่นี่</a>เพื่อดูขั้นตอนการสั่งซื้อ</div>
</li>
<li>
<div class="accord-header">สมัครเพื่อลงทะเบียนบัญชีผู้ใช้ใหม่อย่างไร?</div>
<div class="accord-content">เพียงคลิกไปที่ &ldquo;ลงทะเบียน&rdquo; ซึ่งอยู่ด้านบนขวามือของเว็บไซต์ หลังจากนั้นจะเปลี่ยนไปหน้าสร้างบัญชีผู้ใช้ใหม่ซึ่งจะอยู่ทางด้านซ้ายมือ หลังจากกรอกรายละเอียดข้อมูลของท่านเสร็จเรียบร้อยแล้วคลิกที่ &ldquo;ยืนยัน&rdquo; เพื่อเสร็จสิ้นการสมัครสมาชิก หลังจากนั้นคุณจะได้รับอีเมลยืนยันการลงทะเบียนผู้ใช้ใหม่กับทาง Lafema เพียงเท่านี้คุณก็สามารถซื้อสินค้าผ่านทางเราได้อย่างสะดวกสบาย</div>
</li>
<li>
<div class="accord-header">มีค่าธรรมเนียมในการสมัครสมาชิกกับ Lafema หรือไม่</div>
<div class="accord-content">การสมัครสมาชิกไม่มีค่าธรรมเนียมใด ๆ ทั้งสิ้น และจะเกิดค่าใช้จ่ายในกรณีที่มีการสั่งซื้อสินค้าเท่านั้น</div>
</li>
<li>
<div class="accord-header">ถ้าฉันต้องการที่ปรึกษาส่วนตัวสำหรับการสั่งซื้อสามารถติดต่อได้ทางใด</div>
<div class="accord-content">แผนกลูกค้าสัมพันธ์ของเรายินดีเป็นอย่างยิ่งที่จะช่วยเหลือท่านผ่านทางอีเมล <a href="mailto:support@lafema.com">support@lafema.com</a> หรือทางหมายเลยโทรศัพท์ 02-106-8222</div>
</li>
<li class="last">
<div class="accord-header">ฉันสามารถสั่งซื้อสินค้าผ่านทางโทรศัพท์ได้หรือไม่?</div>
<div class="accord-content">คุณสามารถสั่งซื้อสินค้าผ่านทางโทรศัพท์ได้ค่ะ เพียงโทรเข้าไปที่แผนกลูกค้าสัมพันธ์ตามหมายเลขที่แจ้งไว้ และต้องชำระค่าสินค้าด้วยวิธีเก็บเงินปลายทางเท่านั้นค่ะ</div>
</li>
</ol></div>
<div id="payments" class="subcontent">
<h2>การชำระเงิน</h2>
<ol class="accordion">
<li>
<div class="accord-header">การใช้บัตรเครดิตกับทาง Lafema ปลอดภัยหรือไม่</div>
<div class="accord-content">บริษัทดูแลข้อมูลของสมาชิก Lafema เป็นอย่างดี ข้อมูลทุกอย่างจะถูกเก็บไว้เป็นความลับและดูแลรักษาอย่างปลอดภัย ด้านข้อมูลเกี่ยวกับบัตรเครดิตทั้งหมดจะถูกส่งผ่านระบบ SSL (Secure Socket Layer) ซึ่งเป็นระบบรักษาความปลอดภัยมาตรฐานในการส่งข้อมูลทางอินเตอร์เน็ตตรงไปยังระบบเซฟอิเล็กทรอนิกส์ของธนาคาร โดยจะไม่มีการเก็บข้อมูลเกี่ยวกับบัตรเครดิตไว้ที่ระบบเซิร์ฟเวอร์ของเว็บไซต์แต่อย่างใด</div>
</li>
<li>
<div class="accord-header">ข้อมูลบัตรเครดิตไม่สามารถใช้ได้ เกิดจากอะไร</div>
<div class="accord-content">กรุณาตรวจสอบสถานะการเงินของท่านจากทางธนาคารหากยังไม่สามารถใช้ได้กรุณาติดต่อแผนกลูกค้าสัมพันธ์ของธนาคารเพื่อแจ้งกับแผนกเทคโนโลยีสารสนเทศเพื่อแก้ไขปัญหาด้านเทคนิค</div>
</li>
<li>
<div class="accord-header">หากเครื่องคอมพิวเตอร์ขัดข้องระหว่างขั้นตอนการชำระเงิน จะทราบได้อย่างไรว่าการชำระเงินเสร็จสิ้นเรียบร้อยแล้ว</div>
<div class="accord-content">เมื่อท่านชำระเงินเรียบร้อยแล้วคำยืนยันการสั่งซื้อจะถูกส่งไปยังอีเมลของท่านหากท่านไม่ได้รับการยืนยันการสั่งซื้อทางอีเมลกรุณาลองทำการสั่งซื้ออีกครั้ง หากท่านมีข้อสงสัยกรุณาติดต่อแผนกลูกค้าสัมพันธ์เพื่อสอบถามสถานะการสั่งซื้อของท่านที่ อีเมล <a href="mailto:support@petloft.com">support@lafema.com</a> หรือทางโทรศัพท์หมายเลข 02-106-8222</div>
</li>
<li>
<div class="accord-header">วิธีการชำระเงินมีกี่ช่องทาง</div>
<div class="accord-content">
บริษัทมีช่องทางให้ลูกค้าเลือกชำระเงินหลายช่องทาง ตามด้านล่างนี้
<ul style="margin-left: 30px; list-style: square !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">บัตรเครดิต วีซ่า มาสเตอร์ ซึ่งผ่าน่ระบบ Payment ของบริษัท 2C2P</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">PayPal ช่วยให้คุณสามารถชำระค่าสินค้าของคุณหรือชำระด้วยบัตรเครดิตชั้นนำต่างๆ ผ่านทางหน้าเว็บไซด์ได้ทันที โดยจะทำรายการผ่านระบบของ PayPal ทำให้การชำระเงินเป็นไปได้อย่างสะดวก รวดเร็วและปลอดภัย หากท่านยังไม่มีบัญชีกับ Paypal สามารถ คลิกที่นี่เพื่อสมัครบริการ Paypal</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Cash on delivery เป็นการเก็บเงินปลายทางจากผู้รับสินค้า โดยจะมีค่าธรรมเนียมในการเก็บเงินปลายทาง 50 บาท ต่อการสั่งซื้อต่อครั้ง ค่าธรรมเนียมนี้ไม่รวมค่าสินค้า และค่าจัดส่งสินค้า</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">บริการผ่อนชำระสินค้า</li>
</ul><br/>
เมื่อซื้อสินค้าครบ 3,500 บาท สามารถผ่อนชำระได้นานสูงสุด 10 เดือนกับบัตรเครดิตที่ร่วมรายการ<br/><br/>
บัครเครดิตธนาคารกรุงเทพ<br/><br/>
<ul style="margin-left: 30px; list-style: disc !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">แบ่งชำระ 4 เดือน ดอกเบี้ย 0.8%</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">แบ่งชำระ 6 เดือน ดอกเบี้ย 0.8%</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">แบ่งชำระ 10 เดือน ดอกเบี้ย 0.8%</li>
</ul>
<br/>
บัตรเครดิตซิตี้แบงค์<br/><br/>
<ul style="margin-left: 30px; list-style: disc !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">แบ่งชำระ 4 เดือน ดอกเบี้ย 0.89%</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">แบ่งชำระ 6 เดือน ดอกเบี้ย 0.89%</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">แบ่งชำระ 10 เดือน ดอกเบี้ย 0.89%</li>
</ul>
<br/>
บัตรเครดิต KTC<br/><br/>
<ul style="margin-left: 30px; list-style: disc !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">แบ่งชำระ 4 เดือน ดอกเบี้ย 0.89%</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">แบ่งชำระ 6 เดือน ดอกเบี้ย 0.89%</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">แบ่งชำระ 10 เดือน ดอกเบี้ย 0.89%</li>
</ul>
<br/>
บัตรเครดิตธนาคารธนชาติ<br/><br/>
<ul style="margin-left: 30px; list-style: disc !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">แบ่งชำระ 4 เดือน ดอกเบี้ย 0.8%</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">แบ่งชำระ 6 เดือน ดอกเบี้ย 0.8%</li>
</ul>
<br/>
* หมายเหตุ<br/>
ยอดสั่งซื้อขั้นต่ำ 3,500 บาทต่อคำสั่งซื้อ โดยคำนวนราคาก่อนหักส่วนลดและไม่รวมค่าขนส่ง<br/>
อัตราดอกเบี้ยเป็นไปตามที่ธนาคารผู้ออกบัตรกำหนด<br/>
สงวนสิทธิ์การคืนเงินเในทุกกรณี<br/>
เงื่อนไขเป็นไปตามข้อกำหนด บริษัทขอสงวนสิทธิ์การเปลี่ยนแปลงโดยไม่แจ้งให้ทราบล่วงหน้า<br/>
</div>
</li>
<li>
<div class="accord-header">ราคาสินค้าใช้สกุลเงินไทยหรือไม่</div>
<div class="accord-content">ราคาสินค้าทุกชิ้นของเราใช้หน่วยเงินบาทไทย</div>
</li>
<li class="last">
<div class="accord-header">ราคาสินค้ารวมภาษีมูลค่าเพิ่มหรือยังค่ะ</div>
<div class="accord-content">ราคาสินค้าของเราได้รวมภาษีมูลค่าเพิ่มแล้ว</div>
</li>
</ol></div>
<div id="shipping" class="subcontent">
<h2>การจัดส่งสินค้า</h2>
<ol class="accordion">
<li>
<div class="accord-header">บริษัททำการจัดส่งสินค้าเมื่อไหร่</div>
<div class="accord-content">หากสินค้าที่คุณสั่งซื้อมีพร้อมในคลังของเรา บริษัทจะทำการจัดส่งสินค้าออกจากคลังสินค้าภายใน 24 ชั่วโมงหลังจากคำสั่งซื้อสินค้าสมบูรณ์เรียบร้อยแล้ว หรือในเวลาที่ไม่ช้าหรือเร็วไปกว่านั้น (เฉพาะในวันทำการ) สินค้าของ Lafema ทุกรายการจัดส่งโดยระบบที่มีประสิทธิภาพสูง (Premium Courier) ซึ่งจะดำเนินการจัดส่งสินค้าในช่วงเวลา 9.00 น. &ndash; 19.00 น. และในปัจจุบันเราให้บริการจัดส่งสินค้าเฉพาะวันจันทร์ &ndash; วันเสาร์เท่านั้น</div>
</li>
<li>
<div class="accord-header">ระยะเวลาการจัดส่งสินค้านานเท่าไหร่</div>
<div class="accord-content">การจัดส่งสินค้าดำเนินการด้วยระบบของบริษัท ร่วมกับผู้ให้บริการจัดส่งสินค้าอื่น ๆ ซึ่งมีประสิทธิภาพสูง (Premium Courier) เพื่อให้เราแน่ใจได้ว่าสินค้าจะจัดส่งถึงมือคุณอย่างรวดเร็วที่สุด หากคุณสั่งซื้อสินค้าก่อนนเวลา 16.00 น. สินค้าถึงมือคุณไม่เกิน 3-5 วันทำการเฉพาะในเขตกรุงเทพ และ 5-7 วันทำการ ในเขตปริมณฑล และต่างจังหวัด ทั้งนี้บริษัทไม่อาจรับประกันได้ว่าหลังจากคุณสั่งซื้อสินค้าแล้วจะได้รับสินค้าภายในวันถัดไปเลยทันที แม้ว่าการจัดส่งสินค้าไปยังหลายพื้นที่ลูกค้าจะสามารถรับสินค้าได้ในวันรุ่งขึ้นก็ตาม และในบางพื้นที่อาจจะใช้ระยะเวลาการจัดส่งที่นานกว่านั้นก็เป็นได้ <br /><br /> *เฉพาะ ปัตตานี ยะลา นราธิวาส แม่ฮ่องสอน เชียงราย อำเภอสังขระบุรี และบางพื้นที่ที่เป็นเกาะ อาจใช้เวลาในการจัดส่งมากกว่าปกติ 3-5 วันทำการ อย่างไรก็ตามทีมงานของเราจะจัดส่งถึงมือคุณอย่างเร็วที่สุด</div>
</li>
<li>
<div class="accord-header">วิธีการตรวจสอบและติดตามสถานการณ์จัดส่งสินค้า</div>
<div class="accord-content">เมื่อเราทำการจัดส่งสินค้า คุณจะได้รับอีเมลเพื่อยืนยันการจัดส่งสินค้าและแจ้งหมายเลขการจัดส่งสินค้า คุณสามารถใช้หมายเลขการจัดส่งสินค้าเพื่อตรวจสอบสถานะการจัดส่งได้ที่เว็บไซต์ของเรา (ตรวจสอบสถานะการจัดส่ง) หรือสามารถล็อกอินเพื่อดูสถานะการจัดส่งสินค้าได้ที่หน้าใบสั่งซื้อของคุณ โดยคลิกที่ปุ่ม &ldquo;สถานะสินค้า&rdquo; ในกรณีที่สถานะการจัดส่งสินค้าขึ้นว่าได้ &ldquo;ส่งมอบสินค้าแล้ว&rdquo; และคุณยังไม่ได้รับสินค้ากรุณาตรวจสอบกับเพื่อนบ้านหรือผู้ดูแลอาคารก่อนเบื้องต้น และหากสถานะการจัดส่งสินค้าขึ้นว่า &ldquo;ส่งคืนบริษัท&rdquo; กรุณาติดต่อเราทางอีเมลหรือโทรศัพท์ทันที เพราะอาจจะเกิดกรณีที่อยู่จัดส่งสินค้าไม่ถูกต้อง, เกิดเหตุสุดวิสัยที่ทำให้พนักงานไม่สามารถจัดส่งสินค้าได้ หรือสินค้าสูญหายระหว่างการจัดส่ง</div>
</li>
<li>
<div class="accord-header">กรณีที่พนักงานไปจัดส่งสินค้าแล้วไม่มีผู้รอรับสินค้าที่บ้าน</div>
<div class="accord-content">การจัดส่งสินค้าเมื่อไม่มีผู้รอรับสินค้าอยู่ที่บ้าน ขึ้นอยู่กับสถานการณ์และดุลยพินิจของพนักงานจัดส่งสินค้าว่าสามารถจะทำการวางสินค้าไว้ที่หน้าบ้านได้หรือไม่ ในกรณีที่พนักงานจัดส่งสินค้าเห็นว่าจำเป็นจะต้องมีผู้ลงชื่อรับสินค้า พนักงานจัดส่งสินค้าจะฝากสินค้าไว้ที่ผู้จัดการอาคารหรือพนักงานเฝ้าประตู โดยจะพิจารณาเลือกวิธีการที่ดีที่สุดอย่างใดอย่างหนึ่ง แต่หากไม่สามารถทำการจัดส่งสินค้าได้ สินค้าจะถูกตีคืนมาที่บริษัทซึ่งจะขึ้นสถานะการจัดส่งสินค้าว่า &ldquo;ไม่สามารถจัดส่ง&rdquo; คุณสามารถทำเรื่องขอคืนเงินค่าสินค้าเต็มจำนวน แต่ไม่รวมค่าจัดส่งสินค้าได้ (อ่านรายละเอียดได้ที่ <a href="{{store url='return-policy'}}" target="_blank">นโยบายการคืนสินค้า</a> ) ในกรณีที่คำสั่งซื้อถูกปฏิเสธอย่างไม่ถูกต้อง ทางบริษัทจะไม่ทำการจัดส่งสินค้ากลับไปอีกครั้ง คุณจะต้องดำเนินการสั่งซื้อใหม่เพื่อที่จะได้รับสินค้าที่คุณต้องการ</div>
</li>
<li>
<div class="accord-header">สามารถแจ้งส่งสินค้าไปยังสถานที่อื่นๆ ของคุณได้หรือไม่</div>
<div class="accord-content">แน่นอน เราทำการจัดส่งสินค้าในวันจันทร์- ศุกร์ เวลา 09.00 น. &ndash; 19.00 น. และวันเสาร์ เวลา 09.00 น. &ndash; 12.00 น. หากต้องการให้เราจัดส่งสินค้าไปยังที่ทำงานของคุณ สามารถกรอกที่อยู่ที่ต้องการให้เราจัดส่งในช่องสถานที่จัดส่งสินค้า หากเกิดเหตุสุดวิสัยที่คุณไม่สามารถอยู่รอรับสินค้าได้ด้วยตนเอง กรุณาตรวจสอบสินค้ากับทางพนักงานต้อนรับ, ผู้จัดการอาคาร, พนักงานเฝ้าประตูก่อนเบื้องต้น และเพื่อป้องกันความผิดพลาดในการนัดหมายรับสินค้า กรุณารอรับการติดต่อทางโทรศัพท์จากพนักงานของเราด้วย</div>
</li>
<li>
<div class="accord-header">ค่าบริการจัดส่งสินค้า</div>
<div class="accord-content">จัดส่งฟรีเมื่อซื้อสินค้าขั้นต่ำ 500 บาทต่อการสั่งซื้อ1ครั้ง เฉพาะเขตกรุงเทพ ในส่วนของเขตปริมณฑล และต่างจังหวัด คิดค่าบริการในการจัดส่ง 100 บาท ในกรณีซื้อสินค้าต่ำกว่า 500 บาทเสียค่าจัดส่ง 100 บาทในเขตกรุงเทพ และ 200 บาทในเขตปริมณฑลและต่างจังหวัด</div>
</li>
<li>
<div class="accord-header">การจัดส่งสินค้าไปยังต่างประเทศ</div>
<div class="accord-content">ขออภัย...ณ ตอนนี้บริษัทยังไม่มีบริการจัดส่งสินค้าไปยังต่างประเทศ แต่ในอนาคตเรามีแพลนที่จะเปิดร้านค้าออนไลน์ทั่วภาคพื้นเอเชียตะวันออกเฉียงใต้</div>
</li>
<li class="last">
<div class="accord-header">ทำไมการจัดส่งสินค้าถึงล่าช้า</div>
<div class="accord-content">หากทำการสั่งซื้อสินค้าภายในเวลา 16.00 น. ของวันทำการปกติ คุณจะได้รับสินค้าหลังจากคำสั่งซื้อสมบูรณ์แล้วภายใน 3-5 วันทำการ ซึ่งเราจะพยายามจัดส่งสินค้าอย่างเร็วที่สุดเท่าที่จะเป็นไปได้ แต่ก็อาจมีบางกรณีที่จะทำให้คุณได้รับสินค้าล่าช้ากว่ากำหนด สาเหตุที่อาจจะทำให้การจัดส่งสินค้าล่าช้า
<ul style="margin-left: 30px; list-style: square !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">ข้อมูลสถานที่จัดส่งสินค้าไม่ถูกต้อง</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">ข้อมูลเบอร์โทรศัพท์ของลูกค้าไม่ถูกต้อง</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">ไม่สามารถติดต่อลูกค้าตามเบอร์โทรศัพท์ที่ให้ไว้ได้</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">ไม่มีคนอยู่รับสินค้าตามสถานที่ได้นัดหมายเอาไว้</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">แจ้งสถานที่จัดส่งสินค้าไม่เหมือนกับที่อยู่ตามใบสั่งซื้อ</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">ลูกค้าชำระเงินล่าช้าหรือมีข้อผิดพลาดในขั้นตอนการชำระเงิน</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">คำสั่งซื้อที่จำเป็นต้องหยุดชะงักหรือล่าช้า อันเนื่องมาจากการสั่งซื้อสินค้าในรายการใดรายการหนึ่งเป็นจำนวนมากทำให้สินค้าในคลังของเราไม่เพียงพอต่อการจำหน่าย (เป็นกรณีที่เกิดขึ้นไม่บ่อยนัก)</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">เหตุสุดวิสัยที่ทำให้การจัดส่งสินค้าไม่เป็นไปตามกำหนด (เช่น ภัยธรรมชาติหรือสภาพอากาศที่ผิดปกติ เป็นต้น)</li>
</ul>
</div>
</li>
</ol></div>
<div id="rewardpoints" class="subcontent">
<h2>คะแนนสะสม</h2>
<ol class="accordion">
<li>
<div class="accord-header">รับเงินคืน 1% เมื่อช้อปปิ้งกับ Lafema</div>
<div class="accord-content">เมื่อท่านได้สั่งซื้อของกับทาง Lafema ท่านจะได้รับเงินคืนทันที 1 % โดยเงินที่ได้จะถูกเก็บไว้ที่บัญชี Lafema ของท่าน ท่านสามารถนำมาใช้ได้ในครั้งต่อไปเมื่อท่านสั่งซื้อของผ่านทาง Lafema โดยสามารถใช้แทนเงินสด ทุกครั้งที่ท่านต้องการใช้ ท่านสามารถกรอกจำนวนเงินในช่อง ที่หน้าสั่งซื้อสินค้า ก่อนขั้นตอนสุดท้าย</div>
</li>
<!-- <li class="last">
			<div class="accord-header"><span></span>คุณจะได้รับแต้มจากการแนะนำเว็บไซต์ให้กับเพื่อนๆ ได้อย่างไร</div>
			<div class="accord-content">

				มีหลากหลายช่องทางที่คุณจะได้รับเครดิต 100 บาท จากการแนะนำเว็บไซต์ Petloft.com ให้กับเพื่อนๆ <a href="{{store url='rewardpoints/index/referral'}}" target="_blank">อ่านรายละเอียดเพิ่มเติมที่นี่</a>

				<ul style="margin-left: 30px; list-style: square !important;">
					<li style="border-bottom: 0;padding: 5px;font-weight: normal;">คุณสามารถนำลิงค์จากในหน้าบัญชีผู้ใช้ของคุณในเว็บไซต์ Petloft.com แชร์ผ่านเฟสบุ๊ค ทวิสเตอร์ หรือส่งลิงค์ให้กับเพื่อนๆ ผ่านทางอีเมล</li>
					<li style="border-bottom: 0;padding: 5px;font-weight: normal;">เมื่อเพื่อนของคุณคลิกลิงค์ดังกล่าวและซื้อสินค้าบนเว็บไซต์ Petloft.com คุณจะได้รับทันทีเครดิต 100 บาทในบัญชีของคุณ
เครดิตดังกล่าวสามารถใช้แทนเงินสดหรือใช้เป็นส่วนลดในการซื้อสินค้าครั้งต่อๆ ไป</li>
				</ul>

			</div>
			</li> --></ol></div>
<div id="returns" class="subcontent">
<h2>การคืนสินค้าและขอคืนเงิน</h2>
<ol class="accordion">
<li>
<div class="accord-header">สามารถขอคืนสินค้าได้อย่างไร</div>
<div class="accord-content">กรณีที่คุณต้องการคืนสินค้า กรุณาติดต่อแผนกลูกค้าสัมพันธ์ และแจ้งความประสงค์ในการขอคืนสินค้าผ่านทางโทรศัพท์ 02-106-8222 หรืออีเมล <a href="mailto:support@lafema.com">support@lafema.com</a> หากเงื่อนไขการขอคืนสินค้าของคุณเป็นไปตามนโยบายการขอคืนสินค้าแล้ว กรุณากรอกรายละเอียดลงในแบบฟอร์มการขอคืนสินค้าที่คุณได้รับไปปพร้อมกับสินค้าเมื่อตอนจัดส่ง และแนบใบกำกับภาษี (invoice) ใส่ลงในกล่องพัสดุของ lafema.com กลับมาพร้อมกันด้วยค่ะ โดยจัดส่งสินค้ากลับมาให้ทางเรา ในแบบพัสดุลงทะเบียนผ่านทางที่ทำการไปรษณีย์ไทยใกล้บ้านคุณค่ะ <br /><br /> กรุณาจัดส่งสินค้ามาที่ บริษัท เอคอมเมิร์ซ จำกัด 8/2 ถนนพระราม3 ซอย 53 แขวงบางโพงพาง เขตยานนาวา กรุงเทพฯ 10120 <br /><br /> ทาง Lafema.com จะชดเชยค่าจัดส่งแบบลงทะเบียนพร้อมกับการคืนเงินค่าสินค้า โดยทางเราขอให้คุณเก็บใบเสร็จรับเงิน/slip ค่าจัดส่งที่ได้รับจากที่ทำการไปรษณีย์เพื่อเป็นหลักฐานในการขอคืนเงินค่าส่งสินค้ากลับไว้ด้วย กรุณาส่งใบเสร็จรับเงิน/slip นี้กลับมาทางแผนกลูกค้าสัมพันธ์ โดยส่งเป็นไฟล์รูปภาพกลับมาทางอีเมล <a href="mailto:support@lafema.com">support@lafema.com</a></div>
</li>
<li>
<div class="accord-header">การแพ็คสินค้าเพื่อส่งคืน</div>
<div class="accord-content">เราแนะนำให้คุณทำการแพ็คสินค้าคืนด้วยกล่องหรือบรรจุภัณฑ์ลักษณะเดียวกับที่เราทำการจัดส่งไปให้ เว้นแต่ว่ามันไม่ได้อยู่ในสภาพที่ดีพอสำหรับการนำมาใช้ใหม่ อย่าลืม! ที่จะแนบป้าย/ฉลากขอคืนสินค้า(Return label) กลับมาด้วยจากนั้นใช้ป้ายชื่อ/ที่อยู่ในการจัดส่งสินค้า (Shipping label) ที่ได้รับจากพนักงานหรือจากเว็บไซต์ของเราเพื่อทำการจัดส่งสินค้าคืนบริษัทได้ที่ทำการไปรษณีย์ทุกแห่งทั่วประเทศไทย <br /><br /> หมายเหตุ : การจัดส่งสินค้ากลับคืนนั้น Lafema ไม่ได้เป็นผู้กำหนดระยะเวลาในการจัดส่งสินค้า จึงไม่สามารถบอกได้ว่าสินค้าที่ส่งคืนนั้นจะมาถึงบริษัทเมื่อไหร่ หากมีข้อสงสัยกรุณาติดต่อสอบถามเราได้ที่ 02-106-8222 หรืออีเมล <a href="mailto:support@lafema.com">support@lafema.com</a></div>
</li>
<li>
<div class="accord-header">ใช้ระยะเวลาดำเนินการคืนสินค้าเมื่อไหร่</div>
<div class="accord-content">เราจะใช้เวลาดำเนินการภายใน 14 วันทำการนับตั้งแต่วันที่ได้รับสินค้าคืนมาเรียบร้อยแล้ว</div>
</li>
<li>
<div class="accord-header">เมื่อไหร่ถึงจะได้รับเงินค่าสินค้าคืน</div>
<div class="accord-content">เมื่อบริษัทได้รับสินค้าที่ส่งคืนเรียบร้อยแล้ว ขั้นตอนการคืนเงินจะเกิดขึ้นโดยทันที เราจะดำเนินการคืนเงินค่าสินค้ากลับไปยังบัญชีเงินฝากของคุณ กรุณารอและตรวจสอบยอดเงินหลังจากที่เราได้รับสินค้าคืนจากคุณภายใน 7 วันทำการ</div>
</li>
<li>
<div class="accord-header">ค่าใช้จ่ายในการจัดส่งสินค้าคืนบริษัท</div>
<div class="accord-content">ค่าใช้จ่ายในการจัดส่งสินค้าจะแตกต่างกันไปตามสถานที่และวิธีการจัดส่งที่คุณเลือก กรุณาตรวจสอบให้แน่ใจว่าคุณได้ใช้ระบบจัดส่งแบบลงทะเบียน ซึ่งดำเนินการโดยไปรษณีย์ไทย และเราจะคืนเงินค่าจัดส่งหลังจากที่ได้รับสินค้าคืน และยืนยันว่าได้รับสินค้าคืนเรียบร้อยแล้วค่ะ</div>
</li>
<li class="last">
<div class="accord-header">กรณีที่การจัดส่งสินค้าเกิดข้อผิดพลาด</div>
<div class="accord-content">แผนกควบคุมคุณภาพของเราทำงานอย่างหนักเพื่อให้สินค้าทุกชิ้นอยู่ในสภาพที่ดีและมีคุณภาพสูงสุดเมื่อทำการจัดส่งออกจากคลังสินค้า แต่ก็มีในบางกรณีซึ่งเป็นไปได้ยากที่สินค้าของคุณจะเกิดตำหนิ, เสียหาย หรือมีข้อผิดพลาด ดังนั้นหากเกิดกรณีดังกล่าวแล้วคุณสามารถดำเนินการขอคืนสินค้า และเงินค่าสินค้าเต็มจำนวนภายในระยะเวลา 30 วัน หลังจากที่คุณได้รับสินค้าเรียบร้อยแล้ว</div>
</li>
</ol></div>
</div>
<script type="text/javascript">// <![CDATA[
 jQuery(document).ready(function()
 {
  var tmpUrl = "";
  var h=window.location.hash.substring(0);
  if(h == ""){
   //alert('Hash not found!!!');
   h = "#faq";
   //alert(heart);
   jQuery( "#helpMenu li:first-child" ).addClass( "helpactive");
   jQuery('.subcontent[id='+h.substring(1)+']').fadeIn();
  }else{
   jQuery('#helpMenu li a[data-id='+h.substring(1)+']').parent().addClass( "helpactive");
  jQuery('.subcontent[id='+h.substring(1)+']').fadeIn();
  }

  //////////////////////////////
  jQuery('#keyMess li').on('click','a',function()  {
   tmpUrl=jQuery(this).attr('href');
   h=tmpUrl.substring(tmpUrl.indexOf("#")+1);

   jQuery('.subcontent:visible').fadeOut(0);
   jQuery('.subcontent[id='+h+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
  });
  jQuery('#zyncNav li').on('click','a',function()
  {
   tmpUrl=jQuery(this).attr('href');
   h=tmpUrl.substring(tmpUrl.indexOf("#")+1);
   jQuery('.subcontent:visible').fadeOut(0);
   jQuery('.subcontent[id='+h+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
  });
  jQuery('#footerBanner div').on('click','a',function()
  {
   tmpUrl=jQuery(this).attr('href');
   h=tmpUrl.substring(tmpUrl.indexOf("#")+1);
   jQuery('.subcontent:visible').fadeOut(0);
   jQuery('.subcontent[id='+h+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
  });
  jQuery('#topMklink').on('click','a',function()
  {
   tmpUrl=jQuery(this).attr('href');
   h=tmpUrl.substring(tmpUrl.indexOf("#")+1);
   jQuery('.subcontent:visible').fadeOut(0);
   jQuery('.subcontent[id='+h+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
  });
  jQuery('#footer_help li').on('click','a',function()
  {
   tmpUrl=jQuery(this).attr('href');
   h=tmpUrl.substring(tmpUrl.indexOf("#")+1);
   jQuery('.subcontent:visible').fadeOut(0);
   jQuery('.subcontent[id='+h+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
  });
 ///////////////////////////////

     jQuery('#helpMenu').on('click','a',function()
     {
         jQuery('.subcontent:visible').fadeOut(0);
         jQuery('.subcontent[id='+jQuery(this).attr('data-id')+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery(this).parent().addClass( "helpactive");
     });

///////////////////////////////

     jQuery(".accordion li .accord-header").mouseenter(function() {
	    	jQuery(this).css("cursor","pointer");
	    });
	    jQuery(".accordion li .accord-header").click(function() {
	    	jQuery(this).find("span").removeClass('active');
	      if(jQuery(this).next("div").is(":visible")){
	        jQuery(this).next("div").slideUp(300);
	      } else {
	        jQuery(".accordion li .accord-content").slideUp(400);
	        jQuery(this).next("div").slideToggle(400);
	        jQuery(".accordion li .accord-header").find("span").removeClass('active');
	        jQuery(this).find("span").addClass('active');
	      }
	    });
	  });
// ]]></script>
EOD;
/** @var Mage_Cms_Model_Page $page */
$page = Mage::getModel('cms/page')->getCollection()
    ->addStoreFilter($stores, $withAdmin = false)
    ->addFieldToFilter('identifier', $identifier)
    ->getFirstItem();
if ($page->getId()) $page->delete(); // if exists then delete
$page = Mage::getModel('cms/page');
$page->setTitle($title);
$page->setIdentifier($identifier);
$page->setStores($stores);
$page->setIsActive($isActive);
$page->setRootTemplate($rootTemplate);
$page->setSortOrder($sortOrder);
$page->setContent($content);
$page->setContentHeading($contentHeading);
$page->setMetaDescription($metaDescription);
$page->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// Help page (Lafema English)
//==========================================================================
$title = "Help";
$stores = array($len);
$metaDescription = "";
$identifier = "help";
$isActive = 1;
$rootTemplate = "one_column";
$sortOrder = 0;
$contentHeading = "Help";
$content = <<<EOD
<div>
<ul id="helpMenu">
<li><a href="javascript:void(0)" data-id="faq">FAQ</a></li>
<li><a href="javascript:void(0)" data-id="howtoorder">How to order</a></li>
<li><a href="javascript:void(0)" data-id="payments">Payment</a></li>
<li><a href="javascript:void(0)" data-id="shipping">Shipping</a></li>
<li><a href="javascript:void(0)" data-id="rewardpoints">Reward Points</a></li>
<li><a href="javascript:void(0)" data-id="returns">Cancellation and Returns</a></li>
</ul>
</div>
<div class="helpContent">
<div id="faq" class="subcontent">
<h2>FAQ</h2>
<ol class="accordion">
<li>
<div class="accord-header">Who is Lafema?</div>
<div class="accord-content">Lafema is an online beauty store with a mission to offer what&rsquo;s best for your beauty and deliver it straight to you. We focus on a broad selection of beauty essentials, great logistics, and customer service to impress our local beauty community. Our goal is to help you save time from travelling, popping in and out of beauty store to buy your beauty essentials. At Lafema, we do the legwork for you. Our teams of beauty experts are all dedicated to you and your beauty.</div>
</li>
<li>
<div class="accord-header">How did Lafema come about?</div>
<div class="accord-content">Lafema aims to make looking after your beauty easier. As beauty experts, we want convenience in buying the best quality products without all the hassles. We decided to combine our passion with our vast ecommerce knowledge to offer beauty-conscious community a great service.</div>
</li>
<li>
<div class="accord-header">Where is Lafema based?</div>
<div class="accord-content">We are based at<br /> Sethiwan Tower - 19th Floor<br /> 139 Pan Road, Silom, Bangrak,<br /> Bangkok, 10500 Thailand<br /> Tel: 02-106-8222 <br /> Email <a href="mailto:support@lafema.com">support@lafema.com</a></div>
</li>
<li>
<div class="accord-header">If I were interested in partnering with Lafema, how would I go about it?</div>
<div class="accord-content">The company welcomes any and all business ventures and ideas that you might have that will ultimately add value to our members. To discuss this further, please contact us on <a href="mailto:support@lafema.com ">support@lafema.com</a></div>
</li>
<li>
<div class="accord-header">Free Shipping Promotion</div>
<div class="accord-content">Free Delivery anywhere in Bangkok with the minimun purchase of 500 baht for each order
<ul style="margin-left: 30px; list-style: square !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">฿100 flat fee for shipping nationwide exclude Bangkok</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Flat shipping fee of ฿100 will be applied to the order less than ฿500 in Bangkok and ฿200 for nationwide exclude Bangkok</li>
</ul>
</div>
</li>
<li>
<div class="accord-header">I forgot my password</div>
<div class="accord-content">If you have forgotten your password, don&rsquo;t worry. We have systems in place to help you recover them. <a href="{{store url='forgot-password'}}" target="_blank">Click here</a> for more information.</div>
</li>
<li>
<div class="accord-header">The item I want is out of stock. What now?</div>
<div class="accord-content">Although we try to maintain a stock level that matches expected demand, highly popular items may sometimes be out of stock. Please contact our Customer Service team, who will quickly find a solution for you. Happy and healthy customers are important to us.</div>
</li>
<li>
<div class="accord-header">Can I place an item on hold for purchase at a later date?</div>
<div class="accord-content">Sorry, but we do not offer this option. Our goal is to ensure all customers can immediately purchase our great products.</div>
</li>
<li>
<div class="accord-header">How do I know if you carry a certain brand?</div>
<div class="accord-content">You can use the search function, which is available on the top right of the page or go through our brands list buy clicking on either beauty or skincare.</div>
</li>
<li>
<div class="accord-header">When can I expect new products to be listed on your website?</div>
<div class="accord-content">We constantly update our products with new lines and new brands. Please visit our website to see the latest items we have in stock.</div>
</li>
<li class="last">
<div class="accord-header">Is there somewhere I can go to view the product prior to purchasing?</div>
<div class="accord-content">Unfortunately, we do not have a retail store and for safety reasons we cannot allow customers in the warehouse. If you have any questions regarding the products, please do not hesitate to contact our Customer Service team.</div>
</li>
</ol></div>
<div id="howtoorder" class="subcontent">
<h2>How to order</h2>
<ol class="accordion">
<li>
<div class="accord-header">How do I order?</div>
<div class="accord-content">Ordering at Lafema.com is very easy. The steps can be found <a href="{{store url='how-to-order'}}" target="_blank">here</a>.</div>
</li>
<li>
<div class="accord-header">How do I register a new account?</div>
<div class="accord-content">Please click on 'Register', which is located on the top right hand side of the page. You will then be prompted to a new page where you will find 'New Customer' on the left hand side. Fill in your details as requested before clicking 'submit'. Registration should now be complete and you should receive a confirmation e-mail to the address you registered with.</div>
</li>
<li>
<div class="accord-header">Is there any registration fee for Lafema?</div>
<div class="accord-content">Registration at Lafema is completely free and easy! Any form of charge will only come from purchasing of Lafema products.</div>
</li>
<li>
<div class="accord-header">I need personal assistance with my order. Who can I contact?</div>
<div class="accord-content">Our Customer Service team will be happy to assist you. Please e-mail us at <a href="mailto:support@lafema.com">support@lafema.com</a> or call us at 02-106-8222.</div>
</li>
<li class="last">
<div class="accord-header">Do you take orders over the phone?</div>
<div class="accord-content">Yes. We do take orders over phone. The payment mode possible for these orders is Cash On Delivery only. Click here for more details.</div>
</li>
</ol></div>
<div id="payments" class="subcontent">
<h2>Payment</h2>
<ol class="accordion">
<li>
<div class="accord-header">Is my credit card information safe at Lafema?</div>
<div class="accord-content">The company will not store any credit card information on our servers. At Lafema, we use systems set in place by banks that have the security protocols set in place to handle safe credit card transactions.</div>
</li>
<li>
<div class="accord-header">My credit card details are not being accepted. What's wrong?</div>
<div class="accord-content">Please check with your bank or financial institution to rule out errors on their behalf. If problems continue to persist, please contact Customer Service who will notify the IT department of technical difficulties.</div>
</li>
<li>
<div class="accord-header">My computer froze while processing payment. How will I know that my payment went through successfully?</div>
<div class="accord-content">All successful transactions will receive a confirmation email that contains an order tracking number. If you have not received confirmation via email, please try placing your order again. Alternatively, please contact our Customer Service team to confirm the placement of your order.</div>
</li>
<li>
<div class="accord-header">How many payment methods does Lafema have?</div>
<div class="accord-content">
At Lafema, we provide customers the ability to pay using the following facilities:
<ul style="margin-left: 30px; list-style: square !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Credit card and Debit card</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">PayPal</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Cash on delivery (pay when we deliver the product)</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Installment Plan</li>
</ul><br/>
Enjoy up to 10 months installment on a minimum purchase of 3,500฿ with MOXY allied bank<br/><br/>
Bangkok Bank credit card<br/><br/>
<ul style="margin-left: 30px; list-style: disc !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">4-Month plan +0.8%</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">6-Month plan +0.8%</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">10-Month plan +0.8%</li>
</ul>
<br/>
Citibank credit card<br/><br/>
<ul style="margin-left: 30px; list-style: disc !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">4-Month plan +0.89%</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">6-Month plan +0.89%</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">10-Month plan +0.89%</li>
</ul>
<br/>
KTC credit card<br/><br/>
<ul style="margin-left: 30px; list-style: disc !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">4-Month plan +0.89%</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">6-Month plan +0.89%</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">10-Month plan +0.89%</li>
</ul>
<br/>
Thanachart credit card<br/><br/>
<ul style="margin-left: 30px; list-style: disc !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">4-Month plan +0.8%</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">6-Month plan +0.8%</li>
</ul>
<br/>
* condition<br/>
Each installment purchase may only comprise of purchases made under the same invoice<br/>
The minimum purchase does not include discount and shipping fee<br/>
Interest rate is based on the authorized bank<br/>
The installment purchase cannot be refunded<br/>
MOXY reserves the right to amend and cancel orders that without prior notice<br/>
</div>
</li>
<li>
<div class="accord-header">Are your prices in Thai Baht (THB)?</div>
<div class="accord-content">All pricing is in Thai Baht.</div>
</li>
<li class="last">
<div class="accord-header">Does your prices include taxes?</div>
<div class="accord-content">All prices are inclusive of taxes.</div>
</li>
</ol></div>
<div id="shipping" class="subcontent">
<h2>Shipping</h2>
<ol class="accordion">
<li>
<div class="accord-header">When will my order ship?</div>
<div class="accord-content">All items are shipped within 24 hours after the purchase is made and most of the time even sooner (weekdays only). All Lafema deliveries are sent via Premium Courier. The deliveries are made between 9:00 am - 7:00 pm. Currently, we deliver from Monday to Saturday.</div>
</li>
<li>
<div class="accord-header">How long will it take until I receive my order?</div>
<div class="accord-content">We ship via our own delivery fleet and several other premium carriers. If you order by 4 pm on a business day, you will receive it in 3-5 business days. We do not yet guaranteed overnight shipping, although many areas of the country will receive their orders the next day. <br /><br /> * For Sangkhlaburi district, islands and provinces including Pattani, Yala, Naradhiwas, Mae Hong Son and Chiang Rai, delivery may take 1-2 days longer due to difficult routes. We&rsquo;ll try our best to deliver the product to you as soon as possible.</div>
</li>
<li>
<div class="accord-header">How do I track my order?</div>
<div class="accord-content">When your order ships, you will receive an email with the shipping and tracking information. You can use your tracking number on our website to trace your order. You can also find your tracking number with your order details; Click the "Order Status" button under the "My Account" menu after you have logged on. <br /><br /> Please check with your neighbors or building manager if you have not received your product even though the tracking says that it has been delivered. If the tracking information doesn't show up or shows that the item has been returned to us, please contact us by email or phone. It is possible that we may have the wrong shipping address, the driver may have unsuccessfully attempted to deliver, or the shipping carrier may have misplaced the product.</div>
</li>
<li>
<div class="accord-header">What happens if I am not at home when my order is being delivered?</div>
<div class="accord-content">In the process of delivering the product to you, our driver will contact you on the number provided to ensure that you are at the address that you have given to us. If you are not home at the time, do inform someone else in your household to beaware of our delivery so they can sign for the product on your behalf. If we are still unable to deliver the product to you despite our best efforts, the money paid for the product will be returned to you, but not the shipping fee (For more detail please check our &ldquo;<a href="{{store url='return-policy'}}" target="_blank">refund policy</a>&rdquo;). Undeliverable orders will not be resent and you must place a new order.</div>
</li>
<li>
<div class="accord-header">Can I have my order shipped to my office?</div>
<div class="accord-content">We are happy to delivery to your office. If you would like us to do that, please input your office address at checkout. Your office delivery will be made from the 9am to 7pm on weekdays and 9am to 12 noon on weekends.</div>
</li>
<li>
<div class="accord-header">How much is the shipping fee?</div>
<div class="accord-content">Free Delivery anywhere in Bangkok with the minimun purchase of 500 baht for each order
<ul style="margin-left: 30px; list-style: square !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">฿100 flat fee for shipping nationwide exclude Bangkok</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Flat shipping fee of ฿100 will be applied to the order less than ฿500 in Bangkok and ฿200 for nationwide exclude Bangkok</li>
</ul>
</div>
</li>
<li>
<div class="accord-header">Do you ship outside of Thailand?</div>
<div class="accord-content">Sorry, we currently do not ship outside of Thailand.</div>
</li>
<li class="last">
<div class="accord-header">Why is my shipment delayed?</div>
<div class="accord-content">You must order by 4 p.m. for 3-5-day shipping to apply. We try our best to make sure that you receive your order as soon as possible, however, there are some situations beyond our control that can delay your shipment. The following orders may result in delayed shipping:
<ul style="margin-left: 30px; list-style: square !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Incorrect shipping address</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Incorrect Mobile Phone number</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Unable to reach you at provided contact number</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Being absent after courier made an appointment</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Shipping address does not match billing address</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Payment delay or issue</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Courier failure to deliver (e.g. severe weather conditions)</li>
</ul>
</div>
</li>
</ol></div>
<div id="rewardpoints" class="subcontent">
<h2>Reward Points</h2>
<ol class="accordion">
<li>
<div class="accord-header">1% Cash-back policy</div>
<div class="accord-content">1. A 1% cash back means that for every purchase made, you will receive 1% cash-back, which will be stored in your Lafema account. You can then use this credit as a discount on your next purchase.</div>
</li>
<li class="last">
<div class="accord-header">How do I get points for referring a friend?</div>
<div class="accord-content">There are many ways where you can get ฿100 by referring a friend at Lafema.com
<ul style="margin-left: 30px; list-style: square !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">You can share your unique link which can be found in My Accounts straight to your Facebook or Twitter page. You can also email this link straight to your friends.</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Once your friends have clicked on the link and have made a purchase on Lafema, you will automatically be credited with ฿100 in your account in the form of credits.</li>
</ul>
</div>
</li>
</ol></div>
<div id="returns" class="subcontent">
<h2>Cancellation and Returns</h2>
<ol class="accordion">
<li>
<div class="accord-header">How do I return my purchase?</div>
<div class="accord-content">If you wish to return an item, please contact our customer care and request a return of the product(s) via phone 02-106-8222 or Email <a href="mailto:support@lafema.com">support@lafema.com</a>. If the conditions of the returned product aligned within our return policy, please fill in the Return Slip you received with your shipment, enclose the invoice in the package then send it back to us in its original packaging. Please send the product back to Lafema.com at any Thailand Post location nearby your premise under domestic registered mail service. <br /><br /> Our address is:<br /> Acommerce Co.,Ltd<br /> 8/2 Rama3 Rd, Soi 53, <br /> Bang Phong Pang, Yarn-Nava,<br /> Bangkok 10120<br /> Lafema.com will absorb the incurred shipping costs in this registered returns back to you on the refund process. We ask that you keep the receipt/slip back to Customer Care by submitting a photo file to the email <a href="mailto:support@lafema.com">support@lafema.com</a></div>
</li>
<li>
<div class="accord-header">Packing my product for return</div>
<div class="accord-content">The box that the items were originally shipped in is strongly recommended to be reused except if it&rsquo;s not in a good condition for repacking. Simply apply the shipping label acquired through the online return process or from our customer service team. As mentioned above, please put the return label in your box/package and drop it off at any Thailand Post location. <br /><br /> Note : Return shipments are not eligible for scheduled pick-ups by Lafema. <br /><br /> Please contact us with any question or concerns at 02-106-8222 or send an e-mail to <a href="mailto:support@lafema.com">support@lafema.com</a></div>
</li>
<li>
<div class="accord-header">How long would it take for my return to be processed?</div>
<div class="accord-content">Once we receive the item(s), we will process your request within 14 business days.</div>
</li>
<li>
<div class="accord-header">When do I receive my refund?</div>
<div class="accord-content">Once we receive your item(s) and have checked that it is in a sellable condition, a refund will be initiated immediately. Lafema will refund your money through Bank Transfer. Please allow about 7 business days to receive your money back from the time we receive your return.</div>
</li>
<li>
<div class="accord-header">What are the shipping charges if I return my purchased items?</div>
<div class="accord-content">The shipping costs will vary according to your location and method of postage you choose. Please make sure to ship back to us by Thailand post by registered mail and we will fully refund for shipping fee after receive the delivery receipt.</div>
</li>
<li class="last">
<div class="accord-header">What if my item is faulty?</div>
<div class="accord-content">Our Quality Control department tries to ensure that all products are of a high quality when they leave the warehouse. In the rare circumstance that your item has a defect, you may send it back to us with a completed Return Slip attached within 30 days upon receiving the item(s), a full refund will be issued to you.</div>
</li>
</ol></div>
</div>
<script type="text/javascript">// <![CDATA[
 jQuery(document).ready(function()
 {
  var tmpUrl = "";
  var h=window.location.hash.substring(0);
  if(h == ""){
   //alert('Hash not found!!!');
   h = "#faq";
   //alert(heart);
   jQuery( "#helpMenu li:first-child" ).addClass( "helpactive");
   jQuery('.subcontent[id='+h.substring(1)+']').fadeIn();
  }else{
   jQuery('#helpMenu li a[data-id='+h.substring(1)+']').parent().addClass( "helpactive");
  jQuery('.subcontent[id='+h.substring(1)+']').fadeIn();
  }

  //////////////////////////////
  jQuery('#keyMess li').on('click','a',function()  {
   tmpUrl=jQuery(this).attr('href');
   h=tmpUrl.substring(tmpUrl.indexOf("#")+1);

   jQuery('.subcontent:visible').fadeOut(0);
   jQuery('.subcontent[id='+h+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
  });
  jQuery('#zyncNav li').on('click','a',function()
  {
   tmpUrl=jQuery(this).attr('href');
   h=tmpUrl.substring(tmpUrl.indexOf("#")+1);
   jQuery('.subcontent:visible').fadeOut(0);
   jQuery('.subcontent[id='+h+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
  });
  jQuery('#footerBanner div').on('click','a',function()
  {
   tmpUrl=jQuery(this).attr('href');
   h=tmpUrl.substring(tmpUrl.indexOf("#")+1);
   jQuery('.subcontent:visible').fadeOut(0);
   jQuery('.subcontent[id='+h+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
  });
  jQuery('#topMklink').on('click','a',function()
  {
   tmpUrl=jQuery(this).attr('href');
   h=tmpUrl.substring(tmpUrl.indexOf("#")+1);
   jQuery('.subcontent:visible').fadeOut(0);
   jQuery('.subcontent[id='+h+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
  });
  jQuery('#footer_help li').on('click','a',function()
  {
   tmpUrl=jQuery(this).attr('href');
   h=tmpUrl.substring(tmpUrl.indexOf("#")+1);
   jQuery('.subcontent:visible').fadeOut(0);
   jQuery('.subcontent[id='+h+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
  });
 ///////////////////////////////

     jQuery('#helpMenu').on('click','a',function()
     {
         jQuery('.subcontent:visible').fadeOut(0);
         jQuery('.subcontent[id='+jQuery(this).attr('data-id')+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery(this).parent().addClass( "helpactive");
     });

///////////////////////////////

     jQuery(".accordion li .accord-header").mouseenter(function() {
	    	jQuery(this).css("cursor","pointer");
	    });
	    jQuery(".accordion li .accord-header").click(function() {
	    	jQuery(this).find("span").removeClass('active');
	      if(jQuery(this).next("div").is(":visible")){
	        jQuery(this).next("div").slideUp(300);
	      } else {
	        jQuery(".accordion li .accord-content").slideUp(400);
	        jQuery(this).next("div").slideToggle(400);
	        jQuery(".accordion li .accord-header").find("span").removeClass('active');
	        jQuery(this).find("span").addClass('active');
	      }
	    });
	  });
// ]]></script>
EOD;
/** @var Mage_Cms_Model_Page $page */
$page = Mage::getModel('cms/page')->getCollection()
    ->addStoreFilter($stores, $withAdmin = false)
    ->addFieldToFilter('identifier', $identifier)
    ->getFirstItem();
if ($page->getId()) $page->delete(); // if exists then delete
$page = Mage::getModel('cms/page');
$page->setTitle($title);
$page->setIdentifier($identifier);
$page->setStores($stores);
$page->setIsActive($isActive);
$page->setRootTemplate($rootTemplate);
$page->setSortOrder($sortOrder);
$page->setContent($content);
$page->setContentHeading($contentHeading);
$page->setMetaDescription($metaDescription);
$page->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// Help page (Moxy English)
//==========================================================================
$title = "Help";
$stores = array($moxyen);
$metaDescription = "";
$identifier = "help";
$isActive = 1;
$rootTemplate = "one_column";
$sortOrder = 0;
$contentHeading = "Help";
$content = <<<EOD
<div>
<ul id="helpMenu">
<li><a href="javascript:void(0)" data-id="faq">FAQ</a></li>
<li><a href="javascript:void(0)" data-id="howtoorder">How To Order</a></li>
<li><a href="javascript:void(0)" data-id="payments">Payments</a></li>
<li><a href="javascript:void(0)" data-id="shipping">Shipping</a></li>
<li><a href="javascript:void(0)" data-id="rewardpoints">Reward Points</a></li>
<li><a href="javascript:void(0)" data-id="returns">Cancellation and Returns</a></li>
</ul>
</div>
<div class="helpContent">
<div id="faq" class="subcontent">
<h2>FAQ</h2>
<ol class="accordion">
<li>
<div class="accord-header">Who is Moxy?</div>
<div class="accord-content">Moxy is a modern women&rsquo;s online companion in South East Asia curated for women to find inspiration, explore new ideas and discover unique products through a social shopping experience. We understand and deliver her a smarter shopping experience, tailored to her needs and interests. Because women want it all. With the best selection of lifestyle products, we guarantee seamless ordering and fast shipping to your doorstep 24 hours a day.</div>
</li>
<li>
<div class="accord-header">How did Moxy come about?</div>
<div class="accord-content">Moxy realizes that women have a diverse and expansive lifestyle. At Moxy, we curate a variety of products, from basics to every retail therapy for women providing exclusive lifestyle products at good prices. It&rsquo;s the beauty of the Moxy experience: women will find what they need and discover what they want. Moxy empowers women.</div>
</li>
<li>
<div class="accord-header">Where is Moxy based?</div>
<div class="accord-content">We are based at<br /> Sethiwan Tower - 19th Floor<br /> 139 Pan Road, Silom, Bangrak,<br /> Bangkok, 10500 Thailand<br /> Tel: 02-106-8222 <br /> Email <a href="mailto:support@moxyst.com">support@moxyst.com</a></div>
</li>
<li>
<div class="accord-header">If I were interested in partnering with Moxy, how would I go about it?</div>
<div class="accord-content">The company welcomes any and all business ventures and ideas that you might have that will ultimately add value to our members. To discuss this further, please contact us on <a href="mailto:support@moxyst.com">support@moxyst.com</a></div>
</li>
<li>
<div class="accord-header">Free Shipping Promotion</div>
<div class="accord-content">Free Delivery anywhere in Bangkok with the minimun purchase of 500 baht for each order
<ul style="margin-left: 30px; list-style: square !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">฿100 flat fee for shipping nationwide exclude Bangkok</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Flat shipping fee of ฿100 will be applied to the order less than ฿500 in Bangkok and ฿200 for nationwide exclude Bangkok</li>
</ul>
</div>
</li>
<li>
<div class="accord-header">I forgot my password</div>
<div class="accord-content">If you have forgotten your password, don&rsquo;t worry. We have systems in place to help you recover them. <a href="{{store url='forgot-password'}}" target="_blank">Click here</a> for more information.</div>
</li>
<li>
<div class="accord-header">The item I want is out of stock. What now?</div>
<div class="accord-content">Although we try to maintain a stock level that matches expected demand, highly popular items may sometimes be out of stock. Please contact our Customer Service team, who will quickly find a solution for you. Happy and healthy customers are important to us.</div>
</li>
<li>
<div class="accord-header">Can I place an item on hold for purchase at a later date?</div>
<div class="accord-content">Sorry, but we do not offer this option. Our goal is to ensure all customers can immediately purchase our great products.</div>
</li>
<li>
<div class="accord-header">How do I know if you carry a certain brand?</div>
<div class="accord-content">You can use the search function, which is available on the top right of the page or go through our brands list buy clicking on either dog or cat.</div>
</li>
<li>
<div class="accord-header">When can I expect new products to be listed on your website?</div>
<div class="accord-content">We constantly update our products with new lines and new brands. Please visit our website to see the latest items we have in stock.</div>
</li>
<li class="last">
<div class="accord-header">Is there somewhere I can go to view the product prior to purchasing?</div>
<div class="accord-content">Unfortunately, we do not have a retail store and for safety reasons we cannot allow customers in the warehouse. If you have any questions regarding the products, please do not hesitate to contact our Customer Service team.</div>
</li>
</ol></div>
<div id="howtoorder" class="subcontent">
<h2>How To Order</h2>
<ol class="accordion">
<li>
<div class="accord-header">How do I order?</div>
<div class="accord-content">Ordering at Moxyst.com is very easy. The steps can be found <a href="{{store url='how-to-order'}}" target="_blank">here</a>.</div>
</li>
<li>
<div class="accord-header">How do I register a new account?</div>
<div class="accord-content">Please click on 'Register', which is located on the top right hand side of the page. You will then be prompted to a new page where you will find 'New Customer' on the left hand side. Fill in your details as requested before clicking 'submit'. Registration should now be complete and you should receive a confirmation e-mail to the address you registered with.</div>
</li>
<li>
<div class="accord-header">Is there any registration fee for Moxy?</div>
<div class="accord-content">Registration at Moxy is completely free and easy! Any form of charge will only come from purchasing of Moxy products.</div>
</li>
<li>
<div class="accord-header">I need personal assistance with my order. Who can I contact?</div>
<div class="accord-content">Our Customer Service team will be happy to assist you. Please e-mail us at <a href="mailto:support@moxyst.com">support@moxyst.com</a> or call us at 02-106-8222.</div>
</li>
<li class="last">
<div class="accord-header">Do you take orders over the phone?</div>
<div class="accord-content">Yes. We do take orders over phone. The payment mode possible for these orders is Cash On Delivery only. Click here for more details.</div>
</li>
</ol></div>
<div id="payments" class="subcontent">
<h2>Payments</h2>
<ol class="accordion">
<li>
<div class="accord-header">Is my credit card information safe at Moxy?</div>
<div class="accord-content">The company will not store any credit card information on our servers. At Moxy, we use systems set in place by banks that have the security protocols set in place to handle safe credit card transactions.</div>
</li>
<li>
<div class="accord-header">My credit card details are not being accepted. What's wrong?</div>
<div class="accord-content">Please check with your bank or financial institution to rule out errors on their behalf. If problems continue to persist, please contact Customer Service who will notify the IT department of technical difficulties.</div>
</li>
<li>
<div class="accord-header">My computer froze while processing payment. How will I know that my payment went through successfully?</div>
<div class="accord-content">All successful transactions will receive a confirmation email that contains an order tracking number. If you have not received confirmation via email, please try placing your order again. Alternatively, please contact our Customer Service team to confirm the placement of your order.</div>
</li>
<li>
<div class="accord-header">How many payment methods does Moxy have?</div>
<div class="accord-content">
At Moxy, we provide customers the ability to pay using the following facilities:
<ul style="margin-left: 30px; list-style: square !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Credit card and Debit card</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">PayPal</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Cash on delivery (pay when we deliver the product)</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Installment Plan</li>
</ul><br/>
Enjoy up to 10 months installment on a minimum purchase of 3,500฿ with MOXY allied bank<br/><br/>
Bangkok Bank credit card<br/><br/>
<ul style="margin-left: 30px; list-style: disc !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">4-Month plan +0.8%</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">6-Month plan +0.8%</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">10-Month plan +0.8%</li>
</ul>
<br/>
Citibank credit card<br/><br/>
<ul style="margin-left: 30px; list-style: disc !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">4-Month plan +0.89%</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">6-Month plan +0.89%</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">10-Month plan +0.89%</li>
</ul>
<br/>
KTC credit card<br/><br/>
<ul style="margin-left: 30px; list-style: disc !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">4-Month plan +0.89%</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">6-Month plan +0.89%</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">10-Month plan +0.89%</li>
</ul>
<br/>
Thanachart credit card<br/><br/>
<ul style="margin-left: 30px; list-style: disc !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">4-Month plan +0.8%</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">6-Month plan +0.8%</li>
</ul>
<br/>
* condition<br/>
Each installment purchase may only comprise of purchases made under the same invoice<br/>
The minimum purchase does not include discount and shipping fee<br/>
Interest rate is based on the authorized bank<br/>
The installment purchase cannot be refunded<br/>
MOXY reserves the right to amend and cancel orders that without prior notice<br/>
</div>
</li>
<li>
<div class="accord-header">Are your prices in Thai Baht (THB)?</div>
<div class="accord-content">All pricing is in Thai Baht.</div>
</li>
<li class="last">
<div class="accord-header">Does your prices include taxes?</div>
<div class="accord-content">All prices are inclusive of taxes.</div>
</li>
</ol></div>
<div id="shipping" class="subcontent">
<h2>Shipping</h2>
<ol class="accordion">
<li>
<div class="accord-header">When will my order ship?</div>
<div class="accord-content">All items are shipped within 24 hours after the purchase is made and most of the time even sooner (weekdays only). All Moxy deliveries are sent via Premium Courier. The deliveries are made between 9:00 am - 7:00 pm. Currently, we deliver from Monday to Saturday.</div>
</li>
<li>
<div class="accord-header">How long will it take until I receive my order?</div>
<div class="accord-content">We ship via our own delivery fleet and several other premium carriers. If you order by 4 pm on a business day, you will receive it in 2-3 business days. We do not yet guaranteed overnight shipping, although many areas of the country will receive their orders the next day. <br /><br /> * For Sangkhlaburi district, islands and provinces including Pattani, Yala, Naradhiwas, Mae Hong Son and Chiang Rai, delivery may take 1-2 days longer due to difficult routes. We&rsquo;ll try our best to deliver the product to you as soon as possible.</div>
</li>
<li>
<div class="accord-header">How do I track my order?</div>
<div class="accord-content">When your order ships, you will receive an email with the shipping and tracking information. You can use your tracking number on our website to trace your order. You can also find your tracking number with your order details; Click the "Order Status" button under the "My Account" menu after you have logged on. <br /><br /> Please check with your neighbors or building manager if you have not received your product even though the tracking says that it has been delivered. If the tracking information doesn't show up or shows that the item has been returned to us, please contact us by email or phone. It is possible that we may have the wrong shipping address, the driver may have unsuccessfully attempted to deliver, or the shipping carrier may have misplaced the product.</div>
</li>
<li>
<div class="accord-header">What happens if I am not at home when my order is being delivered?</div>
<div class="accord-content">In the process of delivering the product to you, our driver will contact you on the number provided to ensure that you are at the address that you have given to us. If you are not home at the time, do inform someone else in your household to beaware of our delivery so they can sign for the product on your behalf. IIf we are still unable to deliver the product to you despite our best efforts, the money paid for the product will be returned to you, but not the shipping fee (For more detail please check our &ldquo;<a href="{{store url='return-policy'}}" target="_blank">refund policy</a>&rdquo;). Undeliverable orders will not be resent and you must place a new order.</div>
</li>
<li>
<div class="accord-header">Can I have my order shipped to my office?</div>
<div class="accord-content">We are happy to delivery to your office. If you would like us to do that, please input your office address at checkout. Your office delivery will be made from the 9am to 7pm on weekdays and 9am to 12 noon on weekends.</div>
</li>
<li>
<div class="accord-header">How much is the shipping fee?</div>
<div class="accord-content">Free Delivery anywhere in Bangkok with the minimun purchase of 500 baht for each order
<ul style="margin-left: 30px; list-style: square !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">฿100 flat fee for shipping nationwide exclude Bangkok</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Flat shipping fee of ฿100 will be applied to the order less than ฿500 in Bangkok and ฿200 for nationwide exclude Bangkok</li>
</ul>
</div>
</li>
<li>
<div class="accord-header">Do you ship outside of Thailand?</div>
<div class="accord-content">Sorry, we currently do not ship outside of Thailand.</div>
</li>
<li class="last">
<div class="accord-header">Why is my shipment delayed?</div>
<div class="accord-content">You must order by 4 p.m. for 1-2-day shipping to apply. We try our best to make sure that you receive your order as soon as possible, however, there are some situations beyond our control that can delay your shipment.<br />The following orders may result in delayed shipping:
<ul style="margin-left: 30px; list-style: square !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Incorrect shipping address</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Incorrect Mobile Phone number</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Unable to reach you at provided contact number</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Being absent after courier made an appointment</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Shipping address does not match billing address</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Payment delay or issue</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Courier failure to deliver (e.g. severe weather conditions)</li>
</ul>
</div>
</li>
</ol></div>
<div id="rewardpoints" class="subcontent">
<h2>Reward Points</h2>
<ol class="accordion">
<li>
<div class="accord-header">1% Cash-back policy</div>
<div class="accord-content">A 1% cash back means that for every purchase made, you will receive 1% cash-back, which will be stored in your Moxy account. You can then use this credit as a discount on your next purchase.</div>
</li>
<li>
<div class="accord-header">How do I get points for referring a friend?</div>
<div class="accord-content">There are many ways where you can get ฿100 by <a href="{{store url='rewardpoints/index/referral'}}" target="_blank">referring a friend</a>at Moxyst.com
<ul style="margin-left: 30px; list-style: square !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">You can share your unique link which can be found in My Accounts straight to your Facebook or Twitter page. You can also email this link straight to your friends.</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Once your friends have clicked on the link and have made a purchase on Moxy, you will automatically be credited with ฿100 in your account in the form of credits.</li>
</ul>
</div>
</li>
<li class="last">
<div class="accord-header">Reward Points Terms &amp; Conditions</div>
<div class="accord-content">Each time you shop on our website, you will earn Reward Points on your purchases. The Reward Points will be accumulated each time you purchase the products on our website and it will be expired within 6 months from your purchasing date. <br /><br /> For example, if you earn 20 points on January from your first purchase and another 30 points on March, your Reward Point balance is 50 points. On July, your balance will be deducted to 30 points because 20 points on January are already expired. In case you have not used your point within September, you point balance will become 0 as your points earned previously on March are expired. <br /><br /> For your own interest, please kindly redeem your Reward Points in the available period of time.</div>
</li>
</ol></div>
<div id="returns" class="subcontent">
<h2>Cancellation and Returns</h2>
<ol class="accordion">
<li>
<div class="accord-header">How do I return my purchase?</div>
<div class="accord-content"><strong>Returns are FREE, fast and easy!</strong><br /> If you wish to return an item, please contact our customer care and request a return of the product(s) via phone 02-106-8222 or Email <a href="mailto:support@moxyst.com">support@moxyst.com</a>. If the conditions of the returned product aligned within our return policy, please fill in the Returns Slip you received with your shipment, enclose the invoice in the package then send it back to us in its original packaging. Please send the product back to Moxyst.com at any Thailand Post location nearby your premise under domestic registered mail service. <br /><br /> NOTE: Sorry for any inconveniences may cause you, return shipments are not eligible for scheduled pick-ups by Moxyst.com. <br /><br /> Our address is:<br /> Acommerce Co.,Ltd<br /> 8/2 Rama3 Rd, Soi 53, <br />Bang Phong Pang, Yarn-Nava, <br />Bangkok 10120<br /><br /> Moxyst.com will absorb the incurred shipping costs in this registered returns back to you on the refund process. We ask that you keep the receipt/slip of this shipping cost issued by the post office as an evidence. And please send your receipt/slip back to Customer Care by submitting a photo file to the email <a href="mailto:support@moxyst.com">support@moxyst.com</a></div>
</li>
<li>
<div class="accord-header">Can I cancel my order?</div>
<div class="accord-content">If you wish to cancel your order, get in touch with customer service as soon as possible with your order number. If your order has been dispatched, please do not accept delivery and contact customer service. In case of Prepaid orders Refunds will be processed as per our Return policies. Any cashback credits used in the purchase of the order will be credited back to your account.</div>
</li>
<li>
<div class="accord-header">Packing your return</div>
<div class="accord-content">We suggest that you use the boxes that the products arrived in. Simply apply the shipping label acquired through the online return process or from our Customer Service team. Please put the return label to your box/package and drop it off at any Thailand Post location. NOTE: Return shipments are not eligible for scheduled pick-ups by Moxy. Please contact us with any questions or concerns at 02-106-8222 or send an e-mail to <a href="mailto:support@moxyst.com">support@moxyst.com</a></div>
</li>
<li>
<div class="accord-header">How long will it take for my return to be processed?</div>
<div class="accord-content">Once we receive the item(s), we will process your request within 14 business days.</div>
</li>
<li>
<div class="accord-header">When do I receive my refund?</div>
<div class="accord-content">Once we receive your item(s) and have checked that it is in original condition, a refund will be initiated immediately. Please allow up to 7 business days to receive your refund from the time we receive your return. Moxy will refund your money through Bank Transfer or in store credit, if preferred.</div>
</li>
<li>
<div class="accord-header">What are the shipping charges if I return my purchased items?</div>
<div class="accord-content">The shipping costs will vary according to your location and method of postage you choose. Please make sure to ship back to us via Thailand post by registered mail and we will refund the shipping fees after receiving the product.</div>
</li>
<li class="last">
<div class="accord-header">What if my item has a defect?</div>
<div class="accord-content">Our Quality Control department tries to ensure that all products are of a high quality when they leave the warehouse. In the rare circumstance that your item has a defect, you may send it back to us with a completed Returns Slip attached within 30 days upon receiving the item(s), a full refund will be issued to you.</div>
</li>
</ol></div>
</div>
<script type="text/javascript">// <![CDATA[
 jQuery(document).ready(function()
 {
  var tmpUrl = "";
  var h=window.location.hash.substring(0);
  if(h == ""){
   //alert('Hash not found!!!');
   h = "#faq";
   //alert(heart);
   jQuery( "#helpMenu li:first-child" ).addClass( "helpactive");
   jQuery('.subcontent[id='+h.substring(1)+']').fadeIn();
  }else{
   jQuery('#helpMenu li a[data-id='+h.substring(1)+']').parent().addClass( "helpactive");
  jQuery('.subcontent[id='+h.substring(1)+']').fadeIn();
  }

  //////////////////////////////
  jQuery('#keyMess li').on('click','a',function()  {
   tmpUrl=jQuery(this).attr('href');
   h=tmpUrl.substring(tmpUrl.indexOf("#")+1);

   jQuery('.subcontent:visible').fadeOut(0);
   jQuery('.subcontent[id='+h+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
  });
  jQuery('#zyncNav li').on('click','a',function()
  {
   tmpUrl=jQuery(this).attr('href');
   h=tmpUrl.substring(tmpUrl.indexOf("#")+1);
   jQuery('.subcontent:visible').fadeOut(0);
   jQuery('.subcontent[id='+h+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
  });
  jQuery('#footerBanner div').on('click','a',function()
  {
   tmpUrl=jQuery(this).attr('href');
   h=tmpUrl.substring(tmpUrl.indexOf("#")+1);
   jQuery('.subcontent:visible').fadeOut(0);
   jQuery('.subcontent[id='+h+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
  });
  jQuery('#topMklink').on('click','a',function()
  {
   tmpUrl=jQuery(this).attr('href');
   h=tmpUrl.substring(tmpUrl.indexOf("#")+1);
   jQuery('.subcontent:visible').fadeOut(0);
   jQuery('.subcontent[id='+h+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
  });
  jQuery('#footer_help li').on('click','a',function()
  {
   tmpUrl=jQuery(this).attr('href');
   h=tmpUrl.substring(tmpUrl.indexOf("#")+1);
   jQuery('.subcontent:visible').fadeOut(0);
   jQuery('.subcontent[id='+h+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
  });
 ///////////////////////////////

     jQuery('#helpMenu').on('click','a',function()
     {
         jQuery('.subcontent:visible').fadeOut(0);
         jQuery('.subcontent[id='+jQuery(this).attr('data-id')+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery(this).parent().addClass( "helpactive");
     });

///////////////////////////////

     jQuery(".accordion li .accord-header").mouseenter(function() {
	    	jQuery(this).css("cursor","pointer");
	    });
	    jQuery(".accordion li .accord-header").click(function() {
	    	jQuery(this).find("span").removeClass('active');
	      if(jQuery(this).next("div").is(":visible")){
	        jQuery(this).next("div").slideUp(300);
	      } else {
	        jQuery(".accordion li .accord-content").slideUp(400);
	        jQuery(this).next("div").slideToggle(400);
	        jQuery(".accordion li .accord-header").find("span").removeClass('active');
	        jQuery(this).find("span").addClass('active');
	      }
	    });
	  });
// ]]></script>
EOD;
/** @var Mage_Cms_Model_Page $page */
$page = Mage::getModel('cms/page')->getCollection()
    ->addStoreFilter($stores, $withAdmin = false)
    ->addFieldToFilter('identifier', $identifier)
    ->getFirstItem();
if ($page->getId()) $page->delete(); // if exists then delete
$page = Mage::getModel('cms/page');
$page->setTitle($title);
$page->setIdentifier($identifier);
$page->setStores($stores);
$page->setIsActive($isActive);
$page->setRootTemplate($rootTemplate);
$page->setSortOrder($sortOrder);
$page->setContent($content);
$page->setContentHeading($contentHeading);
$page->setMetaDescription($metaDescription);
$page->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// Help page (Moxy Thai)
//==========================================================================
$title = "วิธีการสั่งซื้อ และคำถามที่พบบ่อย";
$stores = array($moxyth);
$metaDescription = "คำถามที่พบบ่อย เกี่ยวกับวิธีการการสั่งซื้อ การชำระเงิน และการจัดส่งสินค้า ตลอดจนนโยบายของ Moxy.co.th ในการให้บริการลูกค้า";
$identifier = "help";
$isActive = 1;
$rootTemplate = "one_column";
$sortOrder = 0;
$contentHeading = "ช่วยเหลือ";
$content = <<<EOD
<div>
<ul id="helpMenu">
<li><a href="javascript:void(0)" data-id="faq">คำถามที่พบบ่อย</a></li>
<li><a href="javascript:void(0)" data-id="howtoorder">วิธีการการสั่งซื้อ</a></li>
<li><a href="javascript:void(0)" data-id="payments">การชำระเงิน</a></li>
<li><a href="javascript:void(0)" data-id="shipping">การจัดส่งสินค้า</a></li>
<li><a href="javascript:void(0)" data-id="rewardpoints">คะแนนสะสม</a></li>
<li><a href="javascript:void(0)" data-id="returns">การคืนสินค้าและขอคืนเงิน</a></li>
</ul>
</div>
<div class="helpContent">
<div id="faq" class="subcontent">
<h2>FAQ</h2>
<ol class="accordion">
<li>
<div class="accord-header">Moxy คืออะไร</div>
<div class="accord-content">Moxy เป็น Women&rsquo;s e-Commerce หนึ่งเดียวในภูมิภาคเอเชียตะวันออกเฉียงใต้ ที่ช่วยเพิ่มความสะดวกสบายในการช็อปปิ้งให้มากขึ้น โดยเฉพาะอย่างยิ่งสำหรับกลุ่มคนที่ไม่มีเวลา <br />ช็อปปิ้งออนไลน์จึงเป็นอีกทางเลือกหนึ่งที่ถูกออกแบบมาเพื่อตอบสนองความต้องการของผู้บริโภคในปัจจุบัน <br />นอกจากนี้เรายังให้ความสำคัญกับผลิตภัณฑ์สินค้าที่แบ่งออกตามความแตกต่างและไลฟสไตล์ของผู้หญิงในแบบต่างๆ <br />ทางเราได้เลือกสรรค์สินค้าที่ผู้หญิงต้องการไม่ว่าจะเป็นในแบบรายเดือนหรือชีวิตประจำวันที่มีความเป็นเอกลักษณ์เฉพาะตัวจากทั่วทุกมุมโลกและส่งตรงถึงมือลูกค้า</div>
</li>
<li>
<div class="accord-header">Moxy มีที่มาอย่างไร</div>
<div class="accord-content">จุดมุ่งหมายหลักของ Moxy คือให้ผู้หญิงได้เข้ามาค้นหา ความเป็นตัวเองในม็อกซี่ และได้รับแรงบัลดาลใจและอัพเดทสิ่งต่างๆที่เกี่ยวกับผู้หญิง ไม่เพียงแค่ในประเทศไทยเท่านั้น แต่เรายังเปน หนึ่งเดียวในภูมิภาคเอเชียตะวันออกเฉียงใต้ที่รวมเอาเรื่องราวต่างๆที่เกี่ยวกับผู้หญิงและผลิตภัณฑ์สินค้าต่างๆอีกมากมาย ที่คัดสรรเป็นอย่างดีจากทั่วทุกมุมโลก ผ่านโลกอินเตอร์เนต ที่จะช่วยคุณประหยัดเวลาและลดความยุ่งยากโดยการจัดหาสิ่งที่คุณต้องการมาให้ พร้อมกับให้ข้อมูลเพื่อช่วยให้คุณตัดสินใจได้ว่าอะไรเหมาะกับคุณที่สุด เรามีสินค้าจากแบรนด์ที่เป็นที่รู้จักจากทั้งในและต่างประเทศ ทั้งยังสามารถส่งตรงถึงมือคุณได้ภายใน 1-2 วัน</div>
</li>
<li>
<div class="accord-header">Moxy ตั้งอยู่ที่ไหน</div>
<div class="accord-content">บริษัทตั้งอยู่ที่ อาคารเศรษฐีวรรณ ชั้น 19 <br /> 139 ถนนปั้น แขวงสีลม เขตบางรัก <br /> กรุงเทพฯ ประเทศไทย 10500 <br /> โทรศัพท์: 02-106-8222<br /> อีเมล์ <a href="mailto:support@moxyst.com">support@moxyst.com</a></div>
</li>
<li>
<div class="accord-header">สนใจอยากร่วมธุรกิจกับ Moxy ทำอย่างไร</div>
<div class="accord-content">บริษัทยินดีต้อนรับผู้ที่สนใจเข้าเป็นส่วนหนึ่งกับเราได้ โดยท่านที่สนใจนำเสนอสินค้าเพื่อสุขภาพ คุณภาพสูงผ่านทางหน้าเว็บไซต์ Moxy สามารถติดต่อได้ที่อีเมล <a href="mailto:support@moxyst.com">support@moxyst.com</a></div>
</li>
<li>
<div class="accord-header">โปรโมชั่นจัดส่งสินค้าฟรี</div>
<div class="accord-content">จัดส่งฟรีเมื่อซื้อสินค้าขั้นต่ำ 500 บาทต่อการสั่งซื้อ1ครั้ง เฉพาะเขตกรุงเทพ ในส่วนของเขตปริมณฑล และต่างจังหวัด คิดค่าบริการในการจัดส่ง 100 บาท ในกรณีซื้อสินค้าต่ำกว่า 500 บาทเสียค่าจัดส่ง 100 บาทในเขตกรุงเทพ และ 200 บาทในเขตปริมณฑลและต่างจังหวัด</div>
</li>
<li>
<div class="accord-header">วิธีขอรหัสผ่านใหม่</div>
<div class="accord-content">ถ้าคุณลืมรหัสผ่านของคุณ คุณสามารถขอรหัสผ่านใหม่ได้ <a href="{{store url='forgot-password'}}" target="_blank">คลิกที่นี่</a>เพื่อดูรายละเอียดเพิ่มเติม</div>
</li>
<li>
<div class="accord-header">ถ้าสินค้าที่ต้องการหมด จะทำอย่างไร</div>
<div class="accord-content">ถึงแม้ว่าทางเราจะพยายามจัดหาสินค้าให้มีจำนวนเหมาะสมกับระดับความต้องการของลูกค้า แต่สินค้าที่ได้รับความนิยมอย่างมากก็อาจหมดลงได้อย่างรวดเร็ว แต่เราพยายามไม่ให้ปัญหานี้เกิดขึ้น หรือถ้าเกิดขึ้นก็จะพยายามหาทางแก้ไข และหาทางออกให้กับลูกค้าอย่างแน่นอน ถ้ามีข้อสงสัย หรือปัญหาเกี่ยวกับสินค้าสามารถติดต่อได้ที่ฝ่ายลูกค้าสัมพันธ์ได้ในเวลาทำการ</div>
</li>
<li>
<div class="accord-header">ฉันสามารถจองสินค้าไว้ก่อนเพื่อทำการสั่งซื้อในวันถัดไปได้หรือไม่</div>
<div class="accord-content">ต้องขออภัย ทางเราไม่มีนโยบายการจองสินค้า</div>
</li>
<li>
<div class="accord-header">ฉันจะทราบได้อย่างไรว่ามีแบรนด์ไหนที่ขายอยู่บนเว็บไซต์ Moxy บ้าง</div>
<div class="accord-content">คุณสามารถค้นหาสินค้าที่ต้องการได้ โดยกดปุ่มค้นหาที่หน้าเว็บไซต์ Moxy ซึ่งอยู่ด้านบนของหน้าเว็บไซต์ หรือคุณสามารถเข้าไปคลิกเลือกแบรนด์ต่าง ๆ ได้ บนแถบเมนู</div>
</li>
<li>
<div class="accord-header">จะทราบได้อย่างไร ว่ามีสินค้าใหม่เข้ามาในเวบไซต์</div>
<div class="accord-content">ทางเราจะทำการอัพเดทเวบไซต์ใหม่ทันที ที่มีสินค้าใหม่เข้ามาในคลังสินค้า ท่านสามารถตรวจสอบสินค้าใหม่ได้ที่เวบไซต์ของเรา</div>
</li>
<li class="last">
<div class="accord-header">ฉันสามารถแสดงความคิดเห็น หรือให้ข้อแนะนำเกี่ยวกับสินค้าได้อย่างไร</div>
<div class="accord-content">เรายินดีที่รับฟังความคิดเห็น และข้อเสนอแนะเกี่ยวกับผลิตภัณฑ์ของเรา คุณสามารถแสดงความคิดเห็นของคุณมาผ่านมางทางอีเมล <a href="mailto:support@moxyst.com">support@moxyst.com</a></div>
</li>
</ol></div>
<div id="howtoorder" class="subcontent">
<h2>วิธีการการสั่งซื้อ</h2>
<ol class="accordion">
<li>
<div class="accord-header">สั่งซื้อสินค้าผ่าน Moxy อย่างไร?</div>
<div class="accord-content">คุณสามารถสั่งซื้อสินค้ากับ Moxyst.com ด้วยวิธีง่ายๆ <a href="{{store url='how-to-order'}}" target="_blank">คลิกที่นี่</a>เพื่อดูขั้นตอนการสั่งซื้อ</div>
</li>
<li>
<div class="accord-header">สมัครเพื่อลงทะเบียนบัญชีผู้ใช้ใหม่อย่างไร?</div>
<div class="accord-content">เพียงคลิกไปที่ &ldquo;ลงทะเบียน&rdquo; ซึ่งอยู่ด้านบนขวามือของเว็บไซต์ หลังจากนั้นจะเปลี่ยนไปหน้าสร้างบัญชีผู้ใช้ใหม่ซึ่งจะอยู่ทางด้านซ้ายมือ หลังจากกรอกรายละเอียดข้อมูลของท่านเสร็จเรียบร้อยแล้วคลิกที่ &ldquo;ยืนยัน&rdquo; เพื่อเสร็จสิ้นการสมัครสมาชิก หลังจากนั้นคุณจะได้รับอีเมลยืนยันการลงทะเบียนผู้ใช้ใหม่กับทาง Moxy เพียงเท่านี้คุณก็สามารถซื้อสินค้าผ่านทางเราได้อย่างสะดวกสบาย</div>
</li>
<li>
<div class="accord-header">มีค่าธรรมเนียมในการสมัครสมาชิกกับ Moxy หรือไม่</div>
<div class="accord-content">การสมัครสมาชิกไม่มีค่าธรรมเนียมใด ๆ ทั้งสิ้น และจะเกิดค่าใช้จ่ายในกรณีที่มีการสั่งซื้อสินค้าเท่านั้น</div>
</li>
<li>
<div class="accord-header">ถ้าฉันต้องการที่ปรึกษาส่วนตัวสำหรับการสั่งซื้อสามารถติดต่อได้ทางใด</div>
<div class="accord-content">แผนกลูกค้าสัมพันธ์ของเรายินดีเป็นอย่างยิ่งที่จะช่วยเหลือท่านผ่านทางอีเมล <a href="mailto:support@moxyst.com">support@moxyst.com</a> หรือทางหมายเลยโทรศัพท์ 02-106-8222</div>
</li>
<li class="last">
<div class="accord-header">ฉันสามารถสั่งซื้อสินค้าผ่านทางโทรศัพท์ได้หรือไม่?</div>
<div class="accord-content">คุณสามารถสั่งซื้อสินค้าผ่านทางโทรศัพท์ได้ค่ะ เพียงโทรเข้าไปที่แผนกลูกค้าสัมพันธ์ตามหมายเลขที่แจ้งไว้ และต้องชำระค่าสินค้าด้วยวิธีเก็บเงินปลายทางเท่านั้น คลิกที่นี่เพื่อดูรายละเอียดเพิ่มเติม</div>
</li>
</ol></div>
<div id="payments" class="subcontent">
<h2>การชำระเงิน</h2>
<ol class="accordion">
<li>
<div class="accord-header">การใช้บัตรเครดิตกับทาง Moxy ปลอดภัยหรือไม่</div>
<div class="accord-content">บริษัทดูแลข้อมูลของสมาชิก Moxy เป็นอย่างดี ข้อมูลทุกอย่างจะถูกเก็บไว้เป็นความลับและดูแลรักษาอย่างปลอดภัย ด้านข้อมูลเกี่ยวกับบัตรเครดิตทั้งหมดจะถูกส่งผ่านระบบ SSL (Secure Socket Layer) ซึ่งเป็นระบบรักษาความปลอดภัยมาตรฐานในการส่งข้อมูลทางอินเตอร์เน็ตตรงไปยังระบบเซฟอิเล็กทรอนิกส์ของธนาคาร โดยจะไม่มีการเก็บข้อมูลเกี่ยวกับบัตรเครดิตไว้ที่ระบบเซิร์ฟเวอร์ของเว็บไซต์แต่อย่างใด</div>
</li>
<li>
<div class="accord-header">ข้อมูลบัตรเครดิตไม่สามารถใช้ได้ เกิดจากอะไร</div>
<div class="accord-content">กรุณาตรวจสอบสถานะการเงินของท่านจากทางธนาคารหากยังไม่สามารถใช้ได้กรุณาติดต่อแผนกลูกค้าสัมพันธ์ของธนาคารเพื่อแจ้งกับแผนกเทคโนโลยีสารสนเทศเพื่อแก้ไขปัญหาด้านเทคนิค</div>
</li>
<li>
<div class="accord-header">หากเครื่องคอมพิวเตอร์ขัดข้องระหว่างขั้นตอนการชำระเงิน จะทราบได้อย่างไรว่าการชำระเงินเสร็จสิ้นเรียบร้อยแล้ว</div>
<div class="accord-content">เมื่อท่านชำระเงินเรียบร้อยแล้วคำยืนยันการสั่งซื้อจะถูกส่งไปยังอีเมลของท่านหากท่านไม่ได้รับการยืนยันการสั่งซื้อทางอีเมลกรุณาลองทำการสั่งซื้ออีกครั้ง หากท่านมีข้อสงสัยกรุณาติดต่อแผนกลูกค้าสัมพันธ์เพื่อสอบถามสถานะการสั่งซื้อของท่านที่ อีเมล <a href="mailto:support@moxyst.com">support@moxyst.com</a> หรือทางโทรศัพท์หมายเลข 02-106-8222</div>
</li>
<li>
<div class="accord-header">วิธีการชำระเงินมีกี่ช่องทาง</div>
<div class="accord-content">
บริษัทมีช่องทางให้ลูกค้าเลือกชำระเงินหลายช่องทาง ตามด้านล่างนี้
<ul style="margin-left: 30px; list-style: square !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">บัตรเครดิต วีซ่า มาสเตอร์ ซึ่งผ่าน่ระบบ Payment ของบริษัท 2C2P</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">PayPal ช่วยให้คุณสามารถชำระค่าสินค้าของคุณหรือชำระด้วยบัตรเครดิตชั้นนำต่างๆ ผ่านทางหน้าเว็บไซด์ได้ทันที โดยจะทำรายการผ่านระบบของ PayPal ทำให้การชำระเงินเป็นไปได้อย่างสะดวก รวดเร็วและปลอดภัย หากท่านยังไม่มีบัญชีกับ Paypal สามารถ คลิกที่นี่เพื่อสมัครบริการ Paypal</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">Cash on delivery เป็นการเก็บเงินปลายทางจากผู้รับสินค้า โดยจะมีค่าธรรมเนียมในการเก็บเงินปลายทาง 50 บาท ต่อการสั่งซื้อต่อครั้ง ค่าธรรมเนียมนี้ไม่รวมค่าสินค้า และค่าจัดส่งสินค้า</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">บริการผ่อนชำระสินค้า</li>
</ul><br/>
เมื่อซื้อสินค้าครบ 3,500 บาท สามารถผ่อนชำระได้นานสูงสุด 10 เดือนกับบัตรเครดิตที่ร่วมรายการ<br/><br/>
บัครเครดิตธนาคารกรุงเทพ<br/><br/>
<ul style="margin-left: 30px; list-style: disc !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">แบ่งชำระ 4 เดือน ดอกเบี้ย 0.8%</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">แบ่งชำระ 6 เดือน ดอกเบี้ย 0.8%</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">แบ่งชำระ 10 เดือน ดอกเบี้ย 0.8%</li>
</ul>
<br/>
บัตรเครดิตซิตี้แบงค์<br/><br/>
<ul style="margin-left: 30px; list-style: disc !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">แบ่งชำระ 4 เดือน ดอกเบี้ย 0.89%</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">แบ่งชำระ 6 เดือน ดอกเบี้ย 0.89%</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">แบ่งชำระ 10 เดือน ดอกเบี้ย 0.89%</li>
</ul>
<br/>
บัตรเครดิต KTC<br/><br/>
<ul style="margin-left: 30px; list-style: disc !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">แบ่งชำระ 4 เดือน ดอกเบี้ย 0.89%</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">แบ่งชำระ 6 เดือน ดอกเบี้ย 0.89%</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">แบ่งชำระ 10 เดือน ดอกเบี้ย 0.89%</li>
</ul>
<br/>
บัตรเครดิตธนาคารธนชาติ<br/><br/>
<ul style="margin-left: 30px; list-style: disc !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">แบ่งชำระ 4 เดือน ดอกเบี้ย 0.8%</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">แบ่งชำระ 6 เดือน ดอกเบี้ย 0.8%</li>
</ul>
<br/>
* หมายเหตุ<br/>
ยอดสั่งซื้อขั้นต่ำ 3,500 บาทต่อคำสั่งซื้อ โดยคำนวนราคาก่อนหักส่วนลดและไม่รวมค่าขนส่ง<br/>
อัตราดอกเบี้ยเป็นไปตามที่ธนาคารผู้ออกบัตรกำหนด<br/>
สงวนสิทธิ์การคืนเงินเในทุกกรณี<br/>
เงื่อนไขเป็นไปตามข้อกำหนด บริษัทขอสงวนสิทธิ์การเปลี่ยนแปลงโดยไม่แจ้งให้ทราบล่วงหน้า<br/>
</div>
</li>
<li>
<div class="accord-header">ราคาสินค้าใช้สกุลเงินไทยหรือไม่</div>
<div class="accord-content">ราคาสินค้าทุกชิ้นของเราใช้หน่วยเงินบาทไทย</div>
</li>
<li class="last">
<div class="accord-header">ราคาสินค้ารวมภาษีมูลค่าเพิ่มหรือยังค่ะ</div>
<div class="accord-content">ราคาสินค้าของเราได้รวมภาษีมูลค่าเพิ่มแล้ว</div>
</li>
</ol></div>
<div id="shipping" class="subcontent">
<h2>การจัดส่งสินค้า</h2>
<ol class="accordion">
<li>
<div class="accord-header">บริษัททำการจัดส่งสินค้าเมื่อไหร่</div>
<div class="accord-content">หากสินค้าที่คุณสั่งซื้อมีพร้อมในคลังของเรา บริษัทจะทำการจัดส่งสินค้าออกจากคลังสินค้าภายใน 24 ชั่วโมงหลังจากคำสั่งซื้อสินค้าสมบูรณ์เรียบร้อยแล้ว หรือในเวลาที่ไม่ช้าหรือเร็วไปกว่านั้น (เฉพาะในวันทำการ) สินค้าของ Moxy ทุกรายการจัดส่งโดยระบบที่มีประสิทธิภาพสูง (Premium Courier) ซึ่งจะดำเนินการจัดส่งสินค้าในช่วงเวลา 9.00 น. &ndash; 19.00 น. และในปัจจุบันเราให้บริการจัดส่งสินค้าเฉพาะวันจันทร์ &ndash; วันเสาร์เท่านั้น</div>
</li>
<li>
<div class="accord-header">ระยะเวลาการจัดส่งสินค้านานเท่าไหร่</div>
<div class="accord-content">การจัดส่งสินค้าดำเนินการด้วยระบบของบริษัท ร่วมกับผู้ให้บริการจัดส่งสินค้า อื่น ๆ ซึ่งมีประสิทธิภาพสูง (Premium Courier) เพื่อให้เราแน่ใจได้ว่าสินค้าจะ จัดส่งถึงมือคุณอย่างรวดเร็วที่สุด หากคุณสั่งซื้อสินค้าภายในเวลา 16.00 น. สินค้าถึงมือคุณไม่เกิน 2-3 วันทำการเฉพาะในเขตกรุงเทพ และ 3-5 วันทำการ ในเขต ปริมณฑล และต่างจังหวัด ทั้งนี้บริษัทไม่อาจรับประกันได้ว่าหลังจากคุณสั่งซื้อ สินค้าแล้วจะได้รับสินค้าภายในวันถัดไปเลยทันที แม้ว่าการจัดส่งสินค้าไปยังหลาย พื้นที่ลูกค้าจะสามารถรับสินค้าได้ในวันรุ่งขึ้นก็ตาม และในบางพื้นที่อาจจะใช้ ระยะเวลาการจัดส่งที่นานกว่านั้นก็เป็นได้ <br /><br /> *เฉพาะ ปัตตานี ยะลา นราธิวาส แม่ฮ่องสอน เชียงราย อำเภอสังขระบุรี และบางพื้นที่ที่เป็นเกาะ อาจใช้เวลาในการจัดส่งมากกว่าปกติ 1-2 วันทำการ อย่างไรก็ตามทีมงานของเราจะจัดส่งถึงมือคุณอย่างเร็วที่สุด</div>
</li>
<li>
<div class="accord-header">วิธีการตรวจสอบและติดตามสถานะการจัดส่งสินค้า</div>
<div class="accord-content">เมื่อเราทำการจัดส่งสินค้า คุณจะได้รับอีเมลเพื่อยืนยันการจัดส่งสินค้าและแจ้งหมายเลขการจัดส่งสินค้า คุณสามารถใช้หมายเลขการจัดส่งสินค้าเพื่อตรวจสอบสถานะการจัดส่งได้ที่เว็บไซต์ของเรา (ตรวจสอบสถานะการจัดส่ง) หรือสามารถล็อกอินเพื่อดูสถานะการจัดส่งสินค้าได้ที่หน้าใบสั่งซื้อของคุณ โดยคลิกที่ปุ่ม &ldquo;สถานะสินค้า&rdquo; ในกรณีที่สถานะการจัดส่งสินค้าขึ้นว่าได้ &ldquo;ส่งมอบสินค้าแล้ว&rdquo; และคุณยังไม่ได้รับสินค้ากรุณาตรวจสอบกับเพื่อนบ้านหรือผู้ดูแลอาคารก่อนเบื้องต้น และหากสถานะการจัดส่งสินค้าขึ้นว่า &ldquo;ส่งคืนบริษัท&rdquo; กรุณาติดต่อเราทางอีเมลหรือโทรศัพท์ทันที เพราะอาจจะเกิดกรณีที่อยู่จัดส่งสินค้าไม่ถูกต้อง, เกิดเหตุสุดวิสัยที่ทำให้พนักงานไม่สามารถจัดส่งสินค้าได้ หรือสินค้าสูญหายระหว่างการจัดส่ง</div>
</li>
<li>
<div class="accord-header">กรณีที่พนักงานไปจัดส่งสินค้าแล้วไม่มีผู้รอรับสินค้าที่บ้าน</div>
<div class="accord-content">การจัดส่งสินค้าเมื่อไม่มีผู้รอรับสินค้าอยู่ที่บ้าน ขึ้นอยู่กับสถานการณ์และดุลยพินิจของพนักงานจัดส่งสินค้าว่าสามารถจะทำการวางสินค้าไว้ที่หน้าบ้านได้หรือไม่ ในกรณีที่พนักงานจัดส่งสินค้าเห็นว่าจำเป็นจะต้องมีผู้ลงชื่อรับสินค้า พนักงานจัดส่งสินค้าจะฝากสินค้าไว้ที่ผู้จัดการอาคารหรือพนักงานเฝ้าประตู โดยจะพิจารณาเลือกวิธีการที่ดีที่สุดอย่างใดอย่างหนึ่ง แต่หากไม่สามารถทำการจัดส่งสินค้าได้ สินค้าจะถูกตีคืนมาที่บริษัทซึ่งจะขึ้นสถานะการจัดส่งสินค้าว่า &ldquo;ไม่สามารถจัดส่ง&rdquo; คุณสามารถทำเรื่องขอคืนเงินค่าสินค้าเต็มจำนวน แต่ไม่รวมค่าจัดส่งสินค้าได้ (อ่านรายละเอียดได้ที่ <a href="{{store url='return-policy'}}" target="_blank">นโยบายการคืนสินค้า</a> ) ในกรณีที่คำสั่งซื้อถูกปฏิเสธอย่างไม่ถูกต้อง ทางบริษัทจะไม่ทำการจัดส่งสินค้ากลับไปอีกครั้ง คุณจะต้องดำเนินการสั่งซื้อใหม่เพื่อที่จะได้รับสินค้าที่คุณต้องการ</div>
</li>
<li>
<div class="accord-header">สามารถแจ้งส่งสินค้าไปยังที่สถานที่อื่นๆ ของคุณได้หรือไม่</div>
<div class="accord-content">แน่นอน เราทำการจัดส่งสินค้าในวันจันทร์- ศุกร์ เวลา 09.00 น. &ndash; 19.00 น. และวันเสาร์ เวลา 09.00 น. &ndash; 12.00 น. หากต้องการให้เราจัดส่งสินค้าไปยังที่ทำงานของคุณ สามารถกรอกที่อยู่ที่ต้องการให้เราจัดส่งในช่องสถานที่จัดส่งสินค้า หากเกิดเหตุสุดวิสัยที่คุณไม่สามารถอยู่รอรับสินค้าได้ด้วยตนเอง กรุณาตรวจสอบสินค้ากับทางพนักงานต้อนรับ, ผู้จัดการอาคาร, พนักงานเฝ้าประตูก่อนเบื้องต้น และเพื่อป้องกันความผิดพลาดในการนัดหมายรับสินค้า กรุณารอรับการติดต่อทางโทรศัพท์จากพนักงานของเราด้วย</div>
</li>
<li>
<div class="accord-header">ค่าบริการจัดส่งสินค้า</div>
<div class="accord-content">จัดส่งฟรีเมื่อซื้อสินค้าขั้นต่ำ 500 บาทต่อการสั่งซื้อ1ครั้ง เฉพาะเขตกรุงเทพ ในส่วนของเขตปริมณฑล และต่างจังหวัด คิดค่าบริการในการจัดส่ง 100 บาท ในกรณีซื้อสินค้าต่ำกว่า 500 บาทเสียค่าจัดส่ง 100 บาทในเขตกรุงเทพ และ 200 บาทในเขตปริมณฑลและต่างจังหวัด</div>
</li>
<li>
<div class="accord-header">การจัดส่งสินค้าไปยังต่างประเทศ&nbsp;</div>
<div class="accord-content">ขออภัย...ณ ตอนนี้บริษัทยังไม่มีบริการจัดส่งสินค้าไปยังต่างประเทศ แต่ในอนาคตเรามีแพลนที่จะเปิดร้านค้าออนไลน์ที่ขายอาหารสัตว์ทั่วภาคพื้นเอเชียตะวันออกเฉียงใต้</div>
</li>
<li class="last">
<div class="accord-header">ทำไมการจัดส่งสินค้าถึงล่าช้า</div>
<div class="accord-content">หากทำการสั่งซื้อสินค้าภายในเวลา 16.00 น. ของวันทำการปกติ คุณจะได้รับสินค้าหลังจากคำสั่งซื้อสมบูรณ์แล้วภายใน 2-3 วันทำการ ซึ่งเราจะพยายามจัดส่งสินค้าอย่างเร็วที่สุดเท่าที่จะเป็นไปได้ แต่ก็อาจมีบางกรณีที่จะทำให้คุณได้รับสินค้าล่าช้ากว่ากำหนด<br />สาเหตุที่อาจจะทำให้การจัดส่งสินค้าล่าช้า :
<ul style="margin-left: 30px; list-style: square !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">ข้อมูลสถานที่จัดส่งสินค้าไม่ถูกต้อง</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">ข้อมูลเบอร์โทรศัพท์ของลูกค้าไม่ถูกต้อง</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">ไม่สามารถติดต่อลูกค้าตามเบอร์โทรศัพท์ที่ให้ไว้ได้</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">ไม่มีคนอยู่รับสินค้าตามสถานที่ได้นัดหมายเอาไว้</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">แจ้งสถานที่จัดส่งสินค้าไม่เหมือนกับที่อยู่ตามใบสั่งซื้อ</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">ลูกค้าชำระเงินล่าช้าหรือมีข้อผิดพลาดในขั้นตอนการชำระเงิน</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">คำสั่งซื้อที่จำเป็นต้องหยุดชะงักหรือล่าช้า อันเนื่องมาจากการสั่งซื้อสินค้าในรายการใดรายการหนึ่งเป็นจำนวนมากทำให้สินค้าในคลังของเราไม่เพียงพอต่อการจำหน่าย (เป็นกรณีที่เกิดขึ้นไม่บ่อยนัก)</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">เหตุสุดวิสัยที่ทำให้การจัดส่งสินค้าไม่เป็นไปตามกำหนด (เช่น ภัยธรรมชาติหรือสภาพอากาศที่ผิดปกติ เป็นต้น)</li>
</ul>
</div>
</li>
</ol></div>
<div id="rewardpoints" class="subcontent">
<h2>คะแนนสะสม</h2>
<ol class="accordion">
<li>
<div class="accord-header">รับเงินคืน 1% เมื่อช็อปปิ้งกับ Moxy</div>
<div class="accord-content">เมื่อท่านได้สั่งซื้อของกับทาง Moxy ท่านจะได้รับเงินคืนทันที 1 % โดยเงินที่ได้จะถูกเก็บไว้ที่บัญชี Moxy ของท่าน ท่านสามารถนำมาใช้ได้ในครั้งต่อไปเมื่อท่านสั่งซื้อของผ่านทาง Moxy โดยสามารถใช้แทนเงินสด ทุกครั้งที่ท่านต้องการใช้ ท่านสามารถกรอกจำนวนเงินในช่อง ที่หน้าสั่งซื้อสินค้า ก่อนขั้นตอนสุดท้าย</div>
</li>
<li>
<div class="accord-header">คุณจะได้รับแต้มจากการแนะนำเว็บไซต์ให้กับเพื่อนๆ ได้อย่างไร</div>
<div class="accord-content">มีหลากหลายช่องทางที่คุณจะได้รับเครดิต 100 บาท จากการแนะนำเว็บไซต์ Moxyst.com ให้กับเพื่อนๆ <a href="{{store url='rewardpoints/index/referral'}}" target="_blank">อ่านรายละเอียดเพิ่มเติมที่นี่</a>
<ul style="margin-left: 30px; list-style: square !important;">
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">คุณสามารถนำลิงค์จากในหน้าบัญชีผู้ใช้ของคุณในเว็บไซต์ Moxyst.com แชร์ผ่านเฟสบุ๊ค ทวีทเตอร์ หรือส่งลิงค์ให้กับเพื่อนๆ ผ่านทางอีเมล</li>
<li style="border-bottom: 0; padding: 5px; font-weight: normal;">เมื่อเพื่อนของคุณคลิกลิงค์ดังกล่าวและซื้อสินค้าบนเว็บไซต์ Moxyst.com คุณจะได้รับทันทีเครดิต 100 บาทในบัญชีของคุณ เครดิตดังกล่าวสามารถใช้แทนเงินสดหรือใช้เป็นส่วนลดในการซื้อสินค้าครั้งต่อๆ ไป</li>
</ul>
</div>
</li>
<li class="last">
<div class="accord-header">อายุการใช้งานของคะแนนสะสม (Reward Points)</div>
<div class="accord-content">เมื่อคุณซื้อสินค้ากับทางเว็บไซต์ของเราคุณจะได้รับคะแนนสะสม (Reward Points) โดยคะแนนสะสมที่คุณได้รับจากการซื้อสินค้าแต่ละครั้งนั้นจะนำมารวมกันเมื่อคุณซื้อสินค้าในครั้งต่อไปเพิ่มขึ้นเรื่อยๆ แต่ถ้าคุณเก็บคะแนนสะสมไว้เป็นเวลานาน คะแนนบางส่วนของคุณอาจหายไปได้ เนื่องจากอายุการใช้งานของคะแนนสะสมจะมีระยะเวลาไม่เท่ากัน กล่าวคือ คะแนนที่คุณได้รับหลังจากการซื้อสินค้าแต่ละครั้งจะมีอายุการใช้งานนาน 6 เดือน <br /><br /> เช่น คุณซื้อสินค้าและได้คะแนนสะสมครั้งแรกในเดือนมกราคม จำนวน 20 คะแนน ต่อมาคุณซื้อสินค้าและได้คะแนนสะสมอีกครั้งในเดือนมีนาคม จำนวน 30 คะแนน คุณจะมีคะแนนสะสมรวมเป็น 50 คะแนน แต่เมื่อถึงเดือนกรกฎาคม คะแนนของคุณจะเหลือเพียง 30 คะแนน (เนื่องจาก 20 คะแนนที่ได้จากเดือนมกราคมหมดอายุในเดือนกรกฎาคม) และถ้าคุณยังไม่ได้ใช้คะแนนที่เหลืออีกภายในเดือนกันยายนคะแนนสะสมของคุณจะกลายเป็น 0 ทันที (เนื่องจาก 30 คะแนนที่ได้จากเดือนมีนาคมหมดอายุในเดือนกันยายน) <br /><br /> ดังนั้นเพื่อเป็นการรักษาสิทธิและผลประโยชน์ของคุณ จึงควรนำคะแนนสะสมมาใช้ภายในระยะเวลาที่กำหนด</div>
</li>
</ol></div>
<div id="returns" class="subcontent">
<h2>การคืนสินค้าและขอคืนเงิน</h2>
<ol class="accordion">
<li>
<div class="accord-header">สามารถขอคืนสินค้าได้อย่างไร</div>
<div class="accord-content"><strong>บริการคืนสินค้าอย่างง่ายดาย รวดเร็ว และไม่มีค่าใช้จ่าย!</strong><br /> กรณีที่คุณต้องการคืนสินค้า กรุณาติดต่อแผนกลูกค้าสัมพันธ์และแจ้งความประสงค์ในการขอคืนสินค้าผ่านทางโทรศัพท์ 02-106-8222 หรือ อีเมล์ <a href="mailto:support@moxyst.com">support@moxyst.com</a> หากเงื่อนไขการขอคืนสินค้าของคุณเป็นไปตามนโยบายการขอคืนสินค้าแล้ว กรุณากรอกรายละเอียดลงในแบบฟอร์มการขอคืนสินค้าที่คุณได้รับไปพร้อมกับสินค้าเมื่อตอนจัดส่ง และแนบใบกำกับภาษี (invoice) ใส่ลงในกล่องพัสดุของ Moxyst.com กลับมาพร้อมกันด้วย โดยจัดส่งสินค้ากลับมาให้ทางเรา ในแบบพัสดุลงทะเบียนผ่านทางที่ทำการไปรษณีย์ไทยใกล้บ้านคุณ <br /><br /> กรุณาจัดส่งสินค้ามาที่ :<br /> บริษัท เอคอมเมิร์ซ จำกัด<br /> 8/2 ถนนพระราม3 ซอย53<br /> แขวงบางโพงพาง เขตยานนาวา <br />กรุงเทพฯ 10120 <br /><br /> ทาง Moxyst.com จะชดเชยค่าจัดส่งแบบลงทะเบียนพร้อมกับการคืนเงินค่าสินค้า โดยทางเราขอให้คุณเก็บใบเสร็จรับเงิน/slip ค่าจัดส่งที่ได้รับจากที่ทำการไปรษณีย์เพื่อเป็นหลักฐานในการขอคืนเงินค่าส่งสินค้ากลับไว้ด้วย กรุณาส่งใบเสร็จรับเงิน/slip นี้กลับมาทางแผนกลูกค้าสัมพันธ์ โดยส่งเป็นไฟล์รูปภาพกลับมาทางอีเมล์ <a href="mailto:support@moxyst.com">support@moxyst.com</a> <br /><br /> หมายเหตุ : ทาง Moxyst.com ต้องขออภัยในความไม่สะดวกที่เรายังไม่มีบริการในการเข้าไปรับสินค้าที่ลูกค้าต้องการส่งคืน</div>
</li>
<li>
<div class="accord-header">การแพ็คสินค้าเพื่อส่งคืนบริษัท</div>
<div class="accord-content">เราแนะนำให้คุณทำการแพ็คสินค้าคืนด้วยกล่องหรือบรรจุภัณฑ์ลักษณะเดียวกับที่เราทำการจัดส่งไปให้ เว้นแต่ว่ามันไม่ได้อยู่ในสภาพที่ดีพอสำหรับการนำมาใช้ใหม่ อย่าลืม! ที่จะแนบป้าย/ฉลากขอคืนสินค้า (return label) กลับมาด้วย จากนั้นใช้ป้ายชื่อ/ที่อยู่ในการจัดส่งสินค้า (Shipping label) ที่ได้รับจากพนักงานหรือจากเว็บไซต์ของเราเพื่อทำการจัดส่งสินค้าคืนบริษัทได้ที่ทำการไปรษณีย์ทุกแห่งทั่วประเทศไทย <br /><br /> <a title="antalya escort" href="http://www.antalyaescortlari.info/">antalya escort</a> หมายเหตุ : การจัดส่งสินค้ากลับคืนนั้น Moxy ไม่ได้เป็นผู้กำหนดระยะเวลาในการจัดส่งสินค้า จึงไม่สามารถบอกได้ว่าสินค้าที่ส่งคืนนั้นจะมาถึงบริษัทเมื่อไหร่ <br /><br /> หากมีข้อสงสัยกรุณาติดต่อสอบถามเราได้ที่ 02-106-8222 หรือ อีเมล์ <a href="mailto:support@moxyst.com">support@moxyst.com</a></div>
</li>
<li>
<div class="accord-header">ใช้ระยะเวลาดำเนินการคืนสินค้านานเท่าไหร่</div>
<div class="accord-content">เราจะใช้เวลาดำเนินการภายใน 14 วันทำการ นับตั้งแต่วันที่ได้รับสินค้าคืนมาเรียบร้อยแล้ว</div>
</li>
<li>
<div class="accord-header">เมื่อไหร่ถึงจะได้รับเงินค่าสินค้าคืน</div>
<div class="accord-content">เมื่อบริษัทได้รับสินค้าที่ส่งคืนเรียบร้อยแล้ว ขั้นตอนการคืนเงินจะเกิดขึ้นโดยทันที เราจะดำเนินการคืนเงินค่าสินค้ากลับไปยังบัญชีเงินฝากของคุณ กรุณารอและตรวจสอบยอดเงินหลังจากที่เราได้รับสินค้าคืนจากคุณภายใน 7 วันทำการ</div>
</li>
<li>
<div class="accord-header">ค่าใช้จ่ายในการจัดส่งสินค้าคืนบริษัท</div>
<div class="accord-content">ค่าใช้จ่ายในการจัดส่งสินค้าจะแตกต่างกันไปตามสถานที่และวิธีการจัดส่งที่คุณเลือก กรุณาตรวจสอบให้แน่ใจว่าคุณได้ใช้ระบบจัดส่งแบบลงทะเบียนซึ่งดำเนินการโดยไปรษณีย์ไทย และเราจะคืนเงินค่าจัดส่งหลังจากที่ได้รับสินค้าคืนและยืนยันว่าได้รับสินค้าคืนเรียบร้อยแล้ว</div>
</li>
<li class="last">
<div class="accord-header">กรณีที่การจัดส่งสินค้าเกิดข้อผิดพลาด</div>
<div class="accord-content">แผนกควบคุมคุณภาพของเราทำงานอย่างหนักเพื่อให้สินค้าทุกชิ้นอยู่ในสภาพที่ดีและมีคุณภาพสูงสุดเมื่อทำการจัดส่งออกจากคลังสินค้า แต่ก็มีในบางกรณีซึ่งเป็นไปได้ยากที่สินค้าของคุณจะเกิดตำหนิ,เสียหาย หรือมีข้อผิดพลาด ดังนั้นหากเกิดกรณีดังกล่าวแล้วคุณสามารถดำเนินการขอคืนสินค้าและเงินค่าสินค้าเต็มจำนวน ภายในระยะเวลา 30 วันหลังจากที่คุณได้รับสินค้าเรียบร้อยแล้ว</div>
</li>
</ol></div>
</div>
<script type="text/javascript">// <![CDATA[
 jQuery(document).ready(function()
 {
  var tmpUrl = "";
  var h=window.location.hash.substring(0);
  if(h == ""){
   //alert('Hash not found!!!');
   h = "#faq";
   //alert(heart);
   jQuery( "#helpMenu li:first-child" ).addClass( "helpactive");
   jQuery('.subcontent[id='+h.substring(1)+']').fadeIn();
  }else{
   jQuery('#helpMenu li a[data-id='+h.substring(1)+']').parent().addClass( "helpactive");
  jQuery('.subcontent[id='+h.substring(1)+']').fadeIn();
  }

  //////////////////////////////
  jQuery('#keyMess li').on('click','a',function()  {
   tmpUrl=jQuery(this).attr('href');
   h=tmpUrl.substring(tmpUrl.indexOf("#")+1);

   jQuery('.subcontent:visible').fadeOut(0);
   jQuery('.subcontent[id='+h+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
  });
  jQuery('#zyncNav li').on('click','a',function()
  {
   tmpUrl=jQuery(this).attr('href');
   h=tmpUrl.substring(tmpUrl.indexOf("#")+1);
   jQuery('.subcontent:visible').fadeOut(0);
   jQuery('.subcontent[id='+h+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
  });
  jQuery('#footerBanner div').on('click','a',function()
  {
   tmpUrl=jQuery(this).attr('href');
   h=tmpUrl.substring(tmpUrl.indexOf("#")+1);
   jQuery('.subcontent:visible').fadeOut(0);
   jQuery('.subcontent[id='+h+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
  });
  jQuery('#topMklink').on('click','a',function()
  {
   tmpUrl=jQuery(this).attr('href');
   h=tmpUrl.substring(tmpUrl.indexOf("#")+1);
   jQuery('.subcontent:visible').fadeOut(0);
   jQuery('.subcontent[id='+h+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
  });
  jQuery('#footer_help li').on('click','a',function()
  {
   tmpUrl=jQuery(this).attr('href');
   h=tmpUrl.substring(tmpUrl.indexOf("#")+1);
   jQuery('.subcontent:visible').fadeOut(0);
   jQuery('.subcontent[id='+h+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery('#helpMenu li a[data-id='+h+']').parent().addClass( "helpactive");
  });
 ///////////////////////////////

     jQuery('#helpMenu').on('click','a',function()
     {
         jQuery('.subcontent:visible').fadeOut(0);
         jQuery('.subcontent[id='+jQuery(this).attr('data-id')+']').fadeIn();
   jQuery( "ul#helpMenu li" ).removeClass( "helpactive");
   jQuery(this).parent().addClass( "helpactive");
     });

///////////////////////////////

     jQuery(".accordion li .accord-header").mouseenter(function() {
	    	jQuery(this).css("cursor","pointer");
	    });
	    jQuery(".accordion li .accord-header").click(function() {
	    	jQuery(this).find("span").removeClass('active');
	      if(jQuery(this).next("div").is(":visible")){
	        jQuery(this).next("div").slideUp(300);
	      } else {
	        jQuery(".accordion li .accord-content").slideUp(400);
	        jQuery(this).next("div").slideToggle(400);
	        jQuery(".accordion li .accord-header").find("span").removeClass('active');
	        jQuery(this).find("span").addClass('active');
	      }
	    });
	  });
// ]]></script>
EOD;
/** @var Mage_Cms_Model_Page $page */
$page = Mage::getModel('cms/page')->getCollection()
    ->addStoreFilter($stores, $withAdmin = false)
    ->addFieldToFilter('identifier', $identifier)
    ->getFirstItem();
if ($page->getId()) $page->delete(); // if exists then delete
$page = Mage::getModel('cms/page');
$page->setTitle($title);
$page->setIdentifier($identifier);
$page->setStores($stores);
$page->setIsActive($isActive);
$page->setRootTemplate($rootTemplate);
$page->setSortOrder($sortOrder);
$page->setContent($content);
$page->setContentHeading($contentHeading);
$page->setMetaDescription($metaDescription);
$page->save();
//==========================================================================
//==========================================================================
//==========================================================================