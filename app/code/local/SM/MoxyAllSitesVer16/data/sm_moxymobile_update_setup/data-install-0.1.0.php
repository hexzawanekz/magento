<?php
$moxyen = Mage::getModel('core/store')->load('moxyen', 'code')->getId();// Moxy
$moxyth = Mage::getModel('core/store')->load('moxyth', 'code')->getId();

$arrayList = Mage::helper('sm_moxymobile')->getLinkArray();

foreach($arrayList as $mainCategory) {

//==========================================================================
// Mobile Home Petloft TH
//==========================================================================
    $block = Mage::getModel('cms/block')->getCollection()
        ->addStoreFilter($moxyth, $withAdmin = false)
        ->addFieldToFilter('identifier', 'mobile_home_'.$mainCategory)
        ->getFirstItem();

    if ($block->getId()) $block->delete();// if exists then delete
    $content = <<<EOD
<ul class="bx-mobile">
    <li><a href="//petloft.com/demo1.html"> <img src="{{media url="wysiwyg//mobile_slider/DEMO_MOBILE_1.jpg"}}" alt="MOBILE DEMO" /> </a></li>
    <li><a href="//petloft.com/demo2.html"> <img src="{{media url="wysiwyg//mobile_slider/DEMO_MOBILE_2.jpg"}}" alt="MOBILE DEMO" /> </a></li>
</ul>
EOD;
    /** @var Mage_Cms_Model_Block $enBlock */
    $block = Mage::getModel('cms/block');
    $block->setTitle('Mobile Home '.$mainCategory.' TH');
    $block->setIdentifier('mobile_home_'.$mainCategory);
    $block->setStores($moxyth);
    $block->setIsActive(1);
    $block->setContent($content);
    $block->save();
//==========================================================================
//==========================================================================
//==========================================================================



//==========================================================================
// Mobile Home EN
//==========================================================================
    $block = Mage::getModel('cms/block')->getCollection()
        ->addStoreFilter($moxyen, $withAdmin = false)
        ->addFieldToFilter('identifier', 'mobile_home_'.$mainCategory)
        ->getFirstItem();

    if ($block->getId()) $block->delete();// if exists then delete
    $content = <<<EOD
<ul class="bx-mobile">
    <li><a href="//petloft.com/demo1.html"> <img src="{{media url="wysiwyg//mobile_slider/DEMO_MOBILE_1.jpg"}}" alt="MOBILE DEMO" /> </a></li>
    <li><a href="//petloft.com/demo2.html"> <img src="{{media url="wysiwyg//mobile_slider/DEMO_MOBILE_2.jpg"}}" alt="MOBILE DEMO" /> </a></li>
</ul>
EOD;
    /** @var Mage_Cms_Model_Block $enBlock */
    $block = Mage::getModel('cms/block');
    $block->setTitle('Mobile Home '.$mainCategory.' EN');
    $block->setIdentifier('mobile_home_'.$mainCategory);
    $block->setStores($moxyen);
    $block->setIsActive(1);
    $block->setContent($content);
    $block->save();
//==========================================================================
//==========================================================================
//==========================================================================

}