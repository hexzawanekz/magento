<?php

class SM_RecommendationAnalysis_IndexController extends Mage_Core_Controller_Front_Action
{
    public function saveClickAction()
    {
        $request = $this->getRequest();
        if ($request->isAjax() && $request->isPost()) {

            $message = '';
            $pid = $request->getParam('product');
            $section = $request->getParam('section');
            $referrer = null;
            if($request->getParam('referrer') != 0){
            $referrer = $request->getParam('referrer');
            }

            if ($pid) {
                try {
                    /** @var SM_RecommendationAnalysis_Model_Click $obj */
                    $obj = Mage::getModel('sm_recommendationanalysis/click')->getCollection()
                        ->addFieldToSelect('*')
                        ->addFieldToFilter('referent_product_id', $pid)
                        ->addFieldToFilter('website_section', $section)
                        ->getFirstItem();

                    if (!$obj->getId()) {
                        $obj = Mage::getModel('sm_recommendationanalysis/click');
                    }

                    $obj->setData('date', date('Y-m-d H:i:s'));
                    $obj->setData('website_section', $section);
                    $obj->setData('position ', 0);
                    $obj->setData('referrer_product_id', $referrer);
                    $obj->setData('referent_product_id', $pid);
                    if ((int)$totalClicks = $obj->getData('clicks')) {
                        $obj->setData('clicks', $totalClicks + 1);
                    } else {
                        $obj->setData('clicks', 1);
                    }

                    $obj->save();

                    $message = 'Successfully recorded';
                    $cookieArray = array(
                        'website_section'=> $section,
                        'position'=> 0 ,
                        'referrer_product_id'=> $referrer,
                        'referent_product_id'=> $pid

                    );
                    $cookie = Mage::getSingleton('core/cookie');
                    $cookie->set('rec_analysis_'.$pid.'_'.$section, serialize($cookieArray) ,time()+86400,'/');
                } catch (Exception $e) {
                    $message = $e->getMessage();
                }
            } else {
                $message = 'Cannot retrieve form data.';
            }

            $jsonData = array('message' => $message);
            $this->getResponse()->setHeader('Content-type', 'application/json');
            $this->getResponse()->setBody(json_encode($jsonData));
        }
    }
}