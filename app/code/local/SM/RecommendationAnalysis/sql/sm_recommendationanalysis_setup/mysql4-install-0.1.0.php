<?php

try {
    /* @var $installer Mage_Core_Model_Resource_Setup */
    $installer = $this;

    $installer->startSetup();

    // recommendation_clicks
    $sql = <<<SQLTEXT
CREATE TABLE {$this->getTable('sm_recommendationanalysis/click')} (
    `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `date` DATETIME NOT NULL,
    `website_section` VARCHAR(200) NOT NULL,
    `position` INT NOT NULL,
    `referrer_product_id` INT NULL,
    `referent_product_id` INT NOT NULL,
    `clicks` INT NOT NULL
) ENGINE = InnoDB CHARACTER SET utf8;
SQLTEXT;

    $installer->run($sql);



    // recommendation_impressions
    $sql = <<<SQLTEXT
CREATE TABLE {$this->getTable('sm_recommendationanalysis/impression')} (
    `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `date` DATETIME NOT NULL,
    `website_section` VARCHAR(200) NOT NULL,
    `position` INT NOT NULL,
    `referrer_product_id` INT NULL,
    `referent_product_id` INT NOT NULL,
    `impressions` INT NOT NULL
) ENGINE = InnoDB CHARACTER SET utf8;
SQLTEXT;

    $installer->run($sql);



    // recommendation_orders
    $sql = <<<SQLTEXT
CREATE TABLE {$this->getTable('sm_recommendationanalysis/order')} (
    `order_id` INT NOT NULL,
    `website_section` VARCHAR(200) NOT NULL,
    `position` INT NOT NULL,
    `referrer_product_id` INT NULL,
    `referent_product_id` INT NOT NULL
) ENGINE = InnoDB CHARACTER SET utf8;
SQLTEXT;

    $installer->run($sql);

    $installer->endSetup();
} catch (Exception $e) {
    throw $e;
}