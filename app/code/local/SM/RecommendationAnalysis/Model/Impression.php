<?php

class SM_RecommendationAnalysis_Model_Impression extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init("sm_recommendationanalysis/impression");
    }
}