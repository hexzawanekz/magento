<?php

class SM_RecommendationAnalysis_Model_Mysql4_Click extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct()
    {
        $this->_init("sm_recommendationanalysis/click", "id");
    }
}