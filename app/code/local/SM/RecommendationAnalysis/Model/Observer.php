<?php


class SM_RecommendationAnalysis_Model_Observer {


    public function analysisCookie($observer)
    {
        $arraySection = array(
            'category',
            'shopping_cart',
            //'viewed_also_bought',
            'viewed_also_viewed',
            //'viewed_also_viewed_emarsys',
            //'viewed_also_viewed_inhouse',
            'viewed_also_bought_emarsys',
            'viewed_also_bought_inhouse',
            'category_oramipicks',
            'category_bestsellers',
            'frontpage_bestsellers',
            'frontpage_oramipicks',
        );

        /** @var Mage_Sales_Model_Quote $quote */
        $quote = $observer->getEvent()->getQuote();
        /** @var Mage_Sales_Model_Order $order */
        $order = $observer->getEvent()->getOrder();

        $cookie = Mage::getSingleton('core/cookie');
        foreach ($quote->getAllItems() as $item) {
            /** @var Mage_Sales_Model_Quote_Item $item */
            $product = $item->getProduct();
            foreach ($arraySection as $section){
                if($cookie->get('rec_analysis_'.$product->getId().'_'.$section)){
                        $rec = unserialize($cookie->get('rec_analysis_'.$product->getId().'_'.$section));
                        $obj = Mage::getModel('sm_recommendationanalysis/order');
                        $obj->setData('order_id',$order->getId())
                            ->setData('website_section',$rec['website_section'])
                            ->setData('position ',$rec['position'])
                            ->setData('referrer_product_id',$rec['referrer_product_id'])
                            ->setData('referent_product_id',$rec['referent_product_id']);
                        $obj->save();
                    Mage::getSingleton('core/cookie')->delete('rec_analysis_'.$product->getId().'_'.$section);
                }
            }
        }



    }
}