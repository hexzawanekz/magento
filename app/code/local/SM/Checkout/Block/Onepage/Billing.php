<?php

/**
 * Created by PhpStorm.
 * User: smartosc
 * Date: 4/8/2016
 * Time: 10:38 AM
 */
class SM_Checkout_Block_Onepage_Billing extends Mage_Checkout_Block_Onepage_Billing
{
    /**
     * @return mixed
     * Do not show Indonesia in checkout page
     */
    public function getCountryCollection()
    {
        if (!$this->_countryCollection) {
            $this->_countryCollection = Mage::getSingleton('directory/country')->getResourceCollection()
                ->addFieldToFilter('country_id', array('neq' => 'ID'))
                ->loadByStore();
        }
        return $this->_countryCollection;
    }
}