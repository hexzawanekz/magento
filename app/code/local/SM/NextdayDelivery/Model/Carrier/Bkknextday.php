<?php
class SM_NextdayDelivery_Model_Carrier_Bkknextday extends Mage_Shipping_Model_Carrier_Flatrate
{
    protected $_code = 'bkk_nextday';

    public function getAllowedMethods()
    {
        return array('bkk_nextday' => $this->getConfigData('name'));
    }

    public function collectRates(Mage_Shipping_Model_Rate_Request $request)
    {
        if (!$this->getConfigFlag('active')) {
            return false;
        }

        if (strcmp($request->getData('dest_region_code'), 'BKK') != 0) {
            return false;
        }

        $productRestrictionsData = $this->getConfigData('product_restrictions');
        if (!empty($productRestrictionsData) && $request->getAllItems()) {
            $productRestrictions = explode(",", $productRestrictionsData);
            $restrictSKUs = array();
            foreach ($productRestrictions as $restrictSKU) {
                $restrictSKUs[] = trim($restrictSKU);
            }
            foreach ($request->getAllItems() as $item) {
                if (in_array($item->getProduct()->getSku(), $restrictSKUs)) {
                    return false;
                }
            }
        }

        $freeBoxes = 0;
        if ($request->getAllItems()) {
            foreach ($request->getAllItems() as $item) {

                if ($item->getProduct()->isVirtual() || $item->getParentItem()) {
                    continue;
                }

                if ($item->getHasChildren() && $item->isShipSeparately()) {
                    foreach ($item->getChildren() as $child) {
                        if ($child->getFreeShipping() && !$child->getProduct()->isVirtual()) {
                            $freeBoxes += $item->getQty() * $child->getQty();
                        }
                    }
                } elseif ($item->getFreeShipping()) {
                    $freeBoxes += $item->getQty();
                }
            }
        }
        $this->setFreeBoxes($freeBoxes);

        $result = Mage::getModel('shipping/rate_result');

        $shippingPrice = $this->getConfigData('handling_fee');

        if ($shippingPrice !== false) {
            $method = Mage::getModel('shipping/rate_result_method');

            $method->setCarrier('bkk_nextday');
            $method->setCarrierTitle($this->getConfigData('title'));

            $method->setMethod('bkk_nextday');
            $method->setMethodTitle($this->getConfigData('name'));

            if ($request->getFreeShipping() === true || $request->getPackageQty() == $this->getFreeBoxes()) {
                $shippingPrice = '0.00';
            }

            if (strlen(trim($this->getConfigData('special_shipping_fee'))) &&
                $this->getConfigData('special_shipping_fee') >= 0 &&
                $this->getConfigData('minimum_spend') >= 0 &&
                $request->getData('package_value_with_discount') >= $this->getConfigData('minimum_spend')) {
                $shippingPrice = $this->getConfigData('special_shipping_fee');
            }

            $method->setPrice($shippingPrice);
            $method->setCost($shippingPrice);
            $result->append($method);
        }
        return $result;
    }
}