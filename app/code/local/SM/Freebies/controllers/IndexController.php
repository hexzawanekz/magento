<?php

class SM_Freebies_IndexController extends Mage_Core_Controller_Front_Action
{
    /**
     * Add freebie product to session
     */
    public function addAction()
    {
        if ($this->getRequest()->isAjax()) {
            $id = $this->getRequest()->getParam('id');
            $action = $this->getRequest()->getParam('action');
            if ($action == 'add') {
                if (Mage::getSingleton('core/session')->getSelectedFreebie() != null) {
                    $freebieIds = explode(',', Mage::getSingleton('core/session')->getSelectedFreebie());
                    $freebieIds[] = $id;
                    Mage::getSingleton('core/session')->setSelectedFreebie(implode(',', $freebieIds));
                } else {
                    Mage::getSingleton('core/session')->setSelectedFreebie($id);
                }
            } else {
                $freebieIds1 = explode(',', Mage::getSingleton('core/session')->getSelectedFreebie());
                if(count($freebieIds1) > 1){
                    $key = array_search($id, $freebieIds1);
                    unset($freebieIds1[$key]);
                    Mage::getSingleton('core/session')->setSelectedFreebie(implode(',', $freebieIds1));
                }else{
                    Mage::getSingleton('core/session')->unsSelectedFreebie();
                }
            }
            echo 'true';
        } else {
            echo 'false';
        }
        exit();
    }
}