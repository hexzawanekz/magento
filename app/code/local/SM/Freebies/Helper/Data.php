<?php

class SM_Freebies_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * Check freebies limit and get freebie quantity selected
     */
    public function getFreebiesLimit($storeId)
    {
        $total = Mage::helper('checkout/cart')->getQuote()->getGrandTotal();
        $rules = $this->_getCollection($storeId)->addFieldToFilter('price_from', array('lteq' => $total))->setOrder('price_from');
        if($rules->count() > 0)
            return $rules->getFirstItem()->getData('freebie_num');
        return 0;
    }

    /**
     * Get minimum payment to spend
     */
    public function getMinimumToSpend($storeId)
    {
        $rules = $this->_getCollection($storeId)->setOrder('price_from', 'ASC');
        if($rules->count() > 0)
            return $rules->getFirstItem()->getData('price_from');
        return 0;
    }

    /**
     * Check that current store view has freebie rule or not
     *
     * return: bool
     */
    public function checkRule($storeId)
    {
        $rules = $this->_getCollection($storeId);

        // check that has rule
        if($rules->count() <= 0)
            return false;

        // check additional condition
        $rule = $rules->getFirstItem();
        $ruleCateList = $rule->getData('category_ids');
        if(!is_null($ruleCateList) || $ruleCateList != ''){
            $rulesCategoryIds = explode(',', $ruleCateList);
            $exMode = $rule->getData('exception_mode');
            $proCategoryIds = array();
            $cart = Mage::helper('checkout/cart')->getQuote();
            foreach($cart->getAllItems() as $item){
                $cateIds = $item->getProduct()->getCategoryIds();
                foreach($cateIds as $id){
                    $proCategoryIds[] = $id;
                }
            }
            $proCategoryIds = array_unique($proCategoryIds);
            //
            $result = $exMode ? true : false;
            foreach($proCategoryIds as $cate_id){
                if(in_array($cate_id, $rulesCategoryIds)){
                    $result = $exMode ? false : true;
                    break;
                }
            }
            return $result;
        }
        return true;
    }

    /**
     * Get freebie rule's collection by store id
     */
    protected function _getCollection($storeId)
    {
        return Mage::getModel('freebies/freebies')->getCollection()->addFieldToSelect('*')->addFieldToFilter('store_id', $storeId);
    }

    /*get root catergory id from name group*/
    public function getInfoGroupFromNameGroup () {
        $return = null;
        foreach (Mage::app()->getWebsites() as $website) {
            foreach ($website->getGroups() as $group) {
                if($group->getName() == 'Petloft')  $return['petloft']  = $group;
                if($group->getName() == 'Venbi')    $return['venbi']    = $group;
                if($group->getName() == 'Sanoga')   $return['sanoga']   = $group;
                if($group->getName() == 'Lafema')   $return['lafema']   = $group;
                if($group->getName() == 'Moxy')     $return['moxy']     = $group;
            }
        }
        return $return;
    }
}
	 