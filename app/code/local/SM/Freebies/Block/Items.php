<?php

class SM_Freebies_Block_Items extends Mage_Core_Block_Template
{
    /**
     * Get selected freebies
     */
    public function getProduct()
    {
        if($strIds = Mage::getSingleton('core/session')->getSelectedFreebie()){
            $arrIds = explode(',', $strIds);
            $products = Mage::getModel('catalog/product')->getCollection()->addAttributeToSelect('*')->addAttributeToFilter('entity_id', array('in'=>$arrIds));
            return $products;
        }
        return null;
    }
}