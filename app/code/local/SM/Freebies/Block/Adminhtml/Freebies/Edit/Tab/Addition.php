<?php

class SM_Freebies_Block_Adminhtml_Freebies_Edit_Tab_Addition extends Mage_Adminhtml_Block_Catalog_Product_Edit_Tab_Categories
{
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('freebies/categories.phtml');
    }

    public function getExceptionMode()
    {
        return $this->_getRule()->getData('exception_mode');
    }

    public function getProduct()
    {
        return Mage::getModel('catalog/product');
    }

    protected function _getRule()
    {
        if($id = $this->getRequest()->getParam('id'))
            return Mage::getModel('freebies/freebies')->load($id);
        return null;
    }

    public function getIdsString()
    {
        return ','.$this->_getRule()->getData('category_ids');
    }

    protected function getCategoryIds()
    {
        return explode(',', $this->_getRule()->getData('category_ids'));
    }

    public function getRoot($parentNodeCategory = null, $recursionLevel = 3)
    {
        if (!is_null($parentNodeCategory) && $parentNodeCategory->getId()) {
            return $this->getNode($parentNodeCategory, $recursionLevel);
        }
        $root = null;
        if ($storeId = $this->_getRule()->getData('store_id')) {
            $store = Mage::app()->getStore($storeId);
            $rootId = $store->getRootCategoryId();

            $ids = $this->getSelectedCategoriesPathIds($rootId);
            $tree = Mage::getResourceSingleton('catalog/category_tree')
                ->loadByIds($ids, false, false);

            if ($this->getCategory()) {
                $tree->loadEnsuredNodes($this->getCategory(), $tree->getNodeById($rootId));
            }

            $tree->addCollectionData($this->_getCategoryCollection($storeId));

            $root = $tree->getNodeById($rootId);

            if ($root && $rootId != Mage_Catalog_Model_Category::TREE_ROOT_ID) {
                $root->setIsVisible(true);
                if ($this->isReadonly()) {
                    $root->setDisabled(true);
                }
            }
            elseif($root && $root->getId() == Mage_Catalog_Model_Category::TREE_ROOT_ID) {
                $root->setName(Mage::helper('catalog')->__('Root'));
            }
        }

        return $root;
    }

    protected function _getCategoryCollection($storeId)
    {
        $collection = Mage::getModel('catalog/category')->getCollection();
        /* @var $collection Mage_Catalog_Model_Resource_Eav_Mysql4_Category_Collection */
        $collection->addAttributeToSelect('name')
            ->addAttributeToSelect('is_active')
            ->setProductStoreId($storeId)
            ->setLoadProductCount($this->_withProductCount)
            ->setStoreId($storeId);
        return $collection;
    }
}