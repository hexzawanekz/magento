<?php

class SM_Freebies_Block_Adminhtml_Freebies_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setId("freebiesGrid");
        $this->setDefaultSort("price_from");
        $this->setDefaultDir("ASC");
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel("freebies/freebies")->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        /*$this->addColumn("id", array(
            "header"            => Mage::helper("freebies")->__("ID"),
            "align"             => "right",
            "width"             => "50px",
            "type"              => "number",
            "index"             => "id",
        ));*/

        $this->addColumn("price_from", array(
            "header"            => Mage::helper("freebies")->__("Minimum Product's Total Price"),
            "index"             => "price_from",
            "type"              => "number",
        ));
        $this->addColumn("freebie_num", array(
            "header"            => Mage::helper("freebies")->__("Able to pick Freebie Products"),
            "index"             => "freebie_num",
            "type"              => "number",
        ));
        $this->addColumn('store_id', array(
            'header'            => Mage::helper("freebies")->__('Store View'),
            'type'              => 'store',
            'index'             => 'store_id',
            'store_view'        => true,
            /*'display_deleted'   => true,*/
            'width'             => '35%',
        ));
        $this->addColumn('action', array(
            'header'            => Mage::helper("freebies")->__('Action'),
            'width'             => '5%',
            'type'              => 'action',
            'actions'           => array(
                array(
                    'caption' => Mage::helper("freebies")->__('Edit'),
                    'url' => array(
                        'base' => 'freebies/adminhtml_freebies/edit',
                    ),
                    'field' => 'id'
                ),
            ),
            'filter'            => false,
            'sortable'          => false,
            'index'             => 'id',
        ));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return $this->getUrl("*/*/edit", array("id" => $row->getId()));
    }


    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('ids');
        $this->getMassactionBlock()->setUseSelectAll(true);
        $this->getMassactionBlock()->addItem('remove_freebies', array(
            'label'     => Mage::helper('freebies')->__('Delete'),
            'url'       => $this->getUrl('*/adminhtml_freebies/massRemove'),
            'confirm'   => Mage::helper('freebies')->__('Are you sure?')
        ));
        return $this;
    }


}