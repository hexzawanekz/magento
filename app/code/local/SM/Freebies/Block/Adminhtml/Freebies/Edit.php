<?php

class SM_Freebies_Block_Adminhtml_Freebies_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {

        parent::__construct();
        $this->_objectId = "id";
        $this->_blockGroup = "freebies";
        $this->_controller = "adminhtml_freebies";
        $this->_updateButton("save", "label", Mage::helper("freebies")->__("Save Rule"));
        $this->_updateButton("delete", "label", Mage::helper("freebies")->__("Delete Rule"));

        $this->_addButton("saveandcontinue", array(
            "label" => Mage::helper("freebies")->__("Save And Continue Edit"),
            "onclick" => "saveAndContinueEdit()",
            "class" => "save",
        ), -100);


        $this->_formScripts[] = "

							function saveAndContinueEdit(){
								editForm.submit($('edit_form').action+'back/edit/');
							}
						";
    }

    public function getHeaderText()
    {
        if (Mage::registry("freebies_data") && Mage::registry("freebies_data")->getId()) {

            return Mage::helper("freebies")->__("Edit Freebie's Rule (ID: %s)", $this->htmlEscape(Mage::registry("freebies_data")->getId()));

        } else {

            return Mage::helper("freebies")->__("Add New Freebie's Rule");

        }
    }
}