<?php

class SM_Freebies_Block_Products extends Mage_Core_Block_Template
{
    public function _construct()
    {
        // get current store
        $storeId = Mage::app()->getStore()->getStoreId();
        // retrieve old store
        if($oldStoreId = Mage::getSingleton('core/session')->getCurrentFreebiesStore()){
            // check that current store is not old store, then remove selected freebies
            if($storeId != $oldStoreId)
                Mage::getSingleton('core/session')->unsSelectedFreebie();
        }
        // save new store
        Mage::getSingleton('core/session')->setCurrentFreebiesStore($storeId);
        // check cart total keeps freebie price rule
        if($this->getFreebiesLimit() == 0){
            Mage::getSingleton('core/session')->unsSelectedFreebie();
        }else{
            // check limit changed
            if($strIds = Mage::getSingleton('core/session')->getSelectedFreebie()){
                $arrIds = explode(',', $strIds);
                if($this->getFreebiesLimit() < count($arrIds))
                    Mage::getSingleton('core/session')->unsSelectedFreebie();
            }
        }
    }

    /**
     * Get Freebies's products by store view
     */
    public function getProducts()
    {
        $infoGroups = Mage::helper('freebies/data')->getInfoGroupFromNameGroup();
        // get model object
        $category = Mage::getModel('catalog/category');
        // get store's group id
        $storeGroupId = Mage::app()->getStore()->getGroupId();
        // check group id to load root category
        if($storeGroupId == 1)
            $category->load($infoGroups['petloft']->getRootCategoryId());
        elseif($storeGroupId == 2)
            $category->load($infoGroups['venbi']->getRootCategoryId());
        elseif($storeGroupId == 4)
            $category->load($infoGroups['sanoga']->getRootCategoryId());
        elseif($storeGroupId == 5)
            $category->load($infoGroups['lafema']->getRootCategoryId());
        elseif($storeGroupId == $infoGroups['moxy']->getId())
            $category->load($infoGroups['moxy']->getRootCategoryId());
        // get collection
        $childCategory = Mage::getModel('catalog/category');
        $childCategoryIds = $category->getChildren();
        foreach(explode(',', $childCategoryIds) as $id){
            $childCategory->load($id);
            if(is_object($childCategory) && $childCategory->getIsActive() && $childCategory->getName() === 'Freebies'){
                $collection = $childCategory->getProductCollection();
                $collection->addAttributeToSelect('*');
                return $collection;
            }
        }
        return null;
    }

    /**
     * Check that current store view has freebie rule or not
     *
     * return: bool
     */
    public function hasRule()
    {
        $storeId = Mage::app()->getStore()->getStoreId();
        return Mage::helper('freebies/data')->checkRule($storeId);
    }

    /**
     * Get Freebies limit product to check condition
     */
    public function getFreebiesLimit()
    {
        $storeId = Mage::app()->getStore()->getStoreId();
        return Mage::helper('freebies/data')->getFreebiesLimit($storeId);
    }

    /**
     * Get minimum to spend
     */
    public function getMinimumToSpend()
    {
        $storeId = Mage::app()->getStore()->getStoreId();
        return Mage::helper('freebies/data')->getMinimumToSpend($storeId);
    }

    /**
     * Get selected freebies from session
     */
    public function getSelectedFreebies()
    {
        if ($strIds = Mage::getSingleton('core/session')->getSelectedFreebie()) {
            return explode(',', $strIds);
        }
        return array();
    }
}