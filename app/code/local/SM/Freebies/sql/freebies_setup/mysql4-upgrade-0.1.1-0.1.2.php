<?php
$installer = $this;
/* @var $installer Mage_Catalog_Model_Resource_Setup */
$installer->startSetup();

$sql = <<<SQLTEXT
DELETE FROM `freebies`;
ALTER TABLE `freebies` ADD COLUMN `store_id` INT(11) NOT NULL;
SQLTEXT;

$installer->run($sql);

$installer->endSetup();