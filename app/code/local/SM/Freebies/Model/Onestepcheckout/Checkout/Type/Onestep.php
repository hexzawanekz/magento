<?php

class SM_Freebies_Model_Onestepcheckout_Checkout_Type_Onestep extends EMST_Onestepcheckout_Model_Checkout_Type_Onestep {

    /**
     * Create order based on checkout type. Create customer if necessary.
     *
     * @return Mage_Checkout_Model_Type_Onepage
     */
    public function saveOrder()
    {
        $this->validate();
        $isNewCustomer = false;
        switch ($this->getCheckoutMethod()) {
            case self::METHOD_GUEST:
                $this->_prepareGuestQuote();
                break;
            case self::METHOD_REGISTER:
                $this->_prepareNewCustomerQuote();
                $isNewCustomer = true;
                break;
            default:
                $this->_prepareCustomerQuote();
                break;
        }

        $service = Mage::getModel('sales/service_quote', $this->getQuote());
        $service->submitAll();

        if ($isNewCustomer) {
            try {
                $this->_involveNewCustomer();
            } catch (Exception $e) {
                Mage::logException($e);
            }
        }

        $this->_checkoutSession->setLastQuoteId($this->getQuote()->getId())
            ->setLastSuccessQuoteId($this->getQuote()->getId())
            ->clearHelperData();

        $order = $service->getOrder();

        /*------------- start customization --------------*/

        if($order){
            // get selected freebie's products
            if($strIds = Mage::getSingleton('core/session')->getSelectedFreebie()){
                $arrIds = explode(',', $strIds);
                $product = Mage::getModel('catalog/product');
                foreach($arrIds as $id){
                    // load product
                    $product->load($id);
                    // create and attach an order item into order
                    $order_item = Mage::getModel('sales/order_item')
                        ->setStoreId($order->getStore()->getStoreId())
                        ->setQuoteItemId(NULL)
                        ->setQuoteParentItemId(NULL)
                        ->setProductId($product->getId())
                        ->setProductType($product->getTypeId())
                        ->setQtyBackordered(NULL)
                        ->setTotalQtyOrdered(1)
                        ->setQtyOrdered(1)
                        ->setName($product->getName())
                        ->setSku($product->getSku())
                        ->setPrice($product->getPrice())
                        ->setBasePrice($product->getPrice())
                        ->setOriginalPrice($product->getPrice())
                        ->setRowTotal(0)
                        ->setBaseRowTotal(0)
                        ->setOrder($order);
                    $order_item->save();
                }
                Mage::getSingleton('core/session')->unsSelectedFreebie();
            }
        }
        $order = Mage::getModel('sales/order')->load($order->getId());

        /*------------- end customization --------------*/

        if ($order) {
            Mage::dispatchEvent('checkout_type_onepage_save_order_after',
                array('order'=>$order, 'quote'=>$this->getQuote()));

            /**
             * a flag to set that there will be redirect to third party after confirmation
             * eg: paypal standard ipn
             */
            $redirectUrl = $this->getQuote()->getPayment()->getOrderPlaceRedirectUrl();
            /**
             * we only want to send to customer about new order when there is no redirect to third party
             */
            if (!$redirectUrl && $order->getCanSendNewEmailFlag()) {
                try {
                    $order->sendNewOrderEmail();
                } catch (Exception $e) {
                    Mage::logException($e);
                }
            }

            // add order information to the session
            $this->_checkoutSession->setLastOrderId($order->getId())
                ->setRedirectUrl($redirectUrl)
                ->setLastRealOrderId($order->getIncrementId());

            // as well a billing agreement can be created
            $agreement = $order->getPayment()->getBillingAgreement();
            if ($agreement) {
                $this->_checkoutSession->setLastBillingAgreementId($agreement->getId());
            }
        }

        // add recurring profiles information to the session
        $profiles = $service->getRecurringPaymentProfiles();
        if ($profiles) {
            $ids = array();
            foreach ($profiles as $profile) {
                $ids[] = $profile->getId();
            }
            $this->_checkoutSession->setLastRecurringProfileIds($ids);
            // TODO: send recurring profile emails
        }

        Mage::dispatchEvent(
            'checkout_submit_all_after',
            array('order' => $order, 'quote' => $this->getQuote(), 'recurring_profiles' => $profiles)
        );

        return $this;
    }
}