<?php
/**
 * Smartosc Ha Noi
 * ductm1@smartosc.com
 * Back in stock (PETLOFT THAILAND)
 */

    $template = Mage::getModel('core/email_template');

    $template_text = <<<HTML
<div style="width:650px;" >
    <h1 style="font-size:150%;text-align:center">{{var product}}</h1>
    <a href="{{var product_url}}?utm_source=edm&utm_medium=reminder&utm_campaign=petloft_outofstock_reminder" target="_blank"><img style="width:650px; border:none;" src="{{skin url="images/backinstock/plth_header.png"}}" alt="petloft-email-header" /></a>
    <div style="display: block;float: left;width: 100%;padding: 0 11px;" >
        <p style="font-size:110%">เรียน คุณ  {{var customer}},</p>
		<p>เนื่องจากในวันที่ {{var date}}  คุณได้อีกเมลถามเกี่ยวกับสินค้า {{var product}} ทาง Petloft</p>
		<p>มีความยินดีจะแจ้งให้ท่านทราบว่า ขณะนี้เรามีสินค้าตัวนี้ในสต็อกแล้วค่ะ</p><br/>
		<p>ซื้อเลยตอนนี้ !</p>
    </div>

    <div style="float: left; clear: both; padding: 15px; width: 618px;min-height: 200px;">
        <div style="width: 195px; text-align: center;float: left;" ><a href="{{var product_url}}?utm_source=edm&utm_medium=reminder&utm_campaign=petloft_outofstock_reminder" target="_blank"><img height="195" src="{{var product_image}}" alt="{{var product}}" style="width: auto; height: 195px; float: left;" /></a></div>
        <div style="width: 260px; text-align: center; margin-top:60px; float: left; margin-bottom:10px; min-height:140px; line-height:30px;">{{var product}}</div>
        <div style="background: rgb(15, 77, 143) none repeat scroll 0% 0%; float: left; min-height: 180px; color: rgb(255, 255, 255); font-size: 20px; text-align: center; font-family: tahoma; padding: 10px; width: 138px;">
            <p class="strong" style="font-size: 27px;font-weight: bold;margin: 0;padding: 0;">ด่วน!</p>
            <p class="medium">สินค้ามรจำนวนจำกัด</p>

            <div style="width:120px;padding:10px; border-radius: 5px;background: #f6921e; margin-top:28px;"><strong><a href="{{var product_url}}?utm_source=edm&utm_medium=reminder&utm_campaign=petloft_outofstock_reminder" style="color: #fff; text-decoration: none">ซื้อเลย!</a></strong></div>
        </div>
    </div>
    <div style="clear:both;"></div>
    <a href="{{var product_url}}?utm_source=edm&utm_medium=reminder&utm_campaign=petloft_outofstock_reminder" target="_blank"><img style="width:650px; border:none;" src="{{skin url="images/backinstock/ptth_footer.png"}}" alt="petloft-email-footer" /></a>
</div>
HTML;

/** @var Mage_Core_Model_Email_Template $model */
$model = Mage::getModel('core/email_template');
$model->loadByCode('Back in stock (PETLOFT THAILAND)')
    ->setTemplateCode('Back in stock (PETLOFT THAILAND)')
    ->setTemplateText($template_text)
    ->setTemplateSubject('{{var product}} is back in stock!')
    ->setTemplateType(Mage_Core_Model_Email_Template::TYPE_HTML);

try {
    $model->save();
} catch (Mage_Exception $e) {
    throw $e;
}