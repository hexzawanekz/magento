<?php
$start = microtime(true);
// init Magento
ini_set('max_execution_time', 0);
ini_set('display_error', 1);
ini_set('memory_limit', -1);

require_once 'app/Mage.php';
//Mage::getIsDeveloperMode(true);
Mage::app();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);


$storeId = isset($_REQUEST['store_id']) ? $_REQUEST['store_id'] : "1";
$categoryId = Mage::app()->getStore($storeId)->getRootCategoryId();



/**
 * Get the resource model
 */
$resource = Mage::getSingleton('core/resource');

/**
 * Retrieve the read connection
 */
$readConnection = $resource->getConnection('core_read');

/**
 * Retrieve the write connection
 */
$writeConnection = $resource->getConnection('core_write');



/* Get Table Name */
$tableName = $resource->getTableName('sales_flat_order_item');

//Get information about eav attribute
$eavAttrTableName = $resource->getTableName('eav_attribute');
$query = "SELECT `attribute_id` FROM `".$eavAttrTableName."` WHERE `attribute_code` = 'sold_quantity'";
$eavAttrRS = $readConnection->fetchRow($query);
$eavAttrId = $eavAttrRS['attribute_id'];

$eavValueTable = $resource->getTableName('catalog_product_entity_int');
if(!is_numeric($eavAttrId) || !($eavAttrId > 0)) {
    Mage::log('EAV Attribute Not Found!', null, 'BestSellerSort.log');
    die();
}

/** @var Mage_Catalog_Model_Resource_Product_Collection $productList */
$productList = Mage::getModel('catalog/product')->getCollection();
$productList->getSelect()
    ->joinLeft('sales_flat_order_item AS sfoi',
        'e.entity_id = sfoi.product_id',
        'SUM(sfoi.qty_ordered) AS ordered_qty')
    ->group('e.entity_id');

$count = 0;

foreach($productList as $_product) {
    $productId = $_product->getId();

    /** @var Mage_Catalog_Model_Product $product */
    $product = Mage::getModel('catalog/product')->load($productId);

    Mage::log("Product ".$productId." Processing.", null, 'BestSellerSort.log');

    /* Query Sum all sold quantity of current product */
    //$query = "SELECT SUM(qty_ordered) as `sold_qty` FROM `sales_flat_order_item` WHERE (product_id = '".$productId."')";
    //$result = $readConnection->fetchRow($query);

    //$sold_qty = (is_numeric($result['sold_qty'])) ? (int)$result['sold_qty'] : 0;

    $sold_qty = (int)$_product->getOrderedQty();

    if($sold_qty > 0) {
        //$product->setSoldQuantity($sold_qty);
        //$product->save();

        $query = "INSERT INTO `".$eavValueTable."` (`entity_type_id`, `attribute_id`, `store_id`, `entity_id`, `value`) VALUES (10, ".$eavAttrId.", 0, ".$productId.",".$sold_qty.")";
        $writeConnection->query($query);

        Mage::log("Product " . $productId . " QTY = " . $sold_qty . " save OK.", null, 'BestSellerSort.log');
        $count++;
    }
}

Mage::log("Success.", null, 'BestSellerSort.log');
Mage::log($count.' items', null, 'BestSellerSort.log');
$time_elapsed_secs = microtime(true) - $start;
Mage::log("Time ".$time_elapsed_secs, null, 'BestSellerSort.log');
